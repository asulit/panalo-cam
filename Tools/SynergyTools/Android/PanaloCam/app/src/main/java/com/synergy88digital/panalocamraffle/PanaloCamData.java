package com.synergy88digital.panalocamraffle;

import com.synergy88digital.data.Synergy88Data;

import org.json.JSONException;
import org.json.JSONObject;

public class PanaloCamData extends Synergy88Data {
    public static final String CLASS_NAME = "PanaloCamData";
    public static final String PLAYER_NAME_KEY = "playerName";
    public static final String PLAYER_ID_KEY = "payerId";

    private static PanaloCamData instance = null;
    public String playerName = "Android";
    public int playerId = 1;

    // constructor
    public PanaloCamData() {
        try {
            this.jsonObject = new JSONObject();
            jsonObject.put(MESSAGE_TYPE, CLASS_NAME);
            jsonObject.put(PLAYER_NAME_KEY, this.playerName);
            jsonObject.put(PLAYER_ID_KEY, this.playerId);
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public static PanaloCamData Instance() {
        if (instance == null) {
            instance = new PanaloCamData();
        }

        return instance;
    }
}
