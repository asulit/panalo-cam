package com.synergy88digital.panalocamraffle;

import android.os.Bundle;
import android.util.Log;

import com.unity3d.player.UnityPlayerActivity;

public class PanaloCam extends UnityPlayerActivity {

    static {
        /*
        try {
            System.load("libACRCloudEngine.so");
        } catch (UnsatisfiedLinkError e) {
            Log.d("panalocam", "Native code library failed to load. libACRCloudEngine.so \n");
            System.err.println("Native code library failed to load. libACRCloudEngine.so\n" + e);
            System.exit(1);
        }
        //*/
    }

    private PanaloCamShare panaloCamShare = null;
    private PanaloCamSms panaloCamSms = null;
    private PanaloCamRecognizer recognizer = null;
    private PanaloCamData panaloCamData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("panalocam", "PanaloCam::onCreate");
        this.panaloCamShare = PanaloCamShare.Instance(this);
        this.panaloCamSms = PanaloCamSms.Instance();
        this.panaloCamData = PanaloCamData.Instance();
        this.recognizer = PanaloCamRecognizer.Instance(this);
    }

    @Override
    protected void onDestroy() {
        Log.d("panalocam", "onDestroy");
        super.onDestroy();
        this.recognizer.OnDestroy();
        this.panaloCamShare = null;
        this.panaloCamSms = null;
        this.panaloCamData = null;
        this.recognizer = null;
    }
}