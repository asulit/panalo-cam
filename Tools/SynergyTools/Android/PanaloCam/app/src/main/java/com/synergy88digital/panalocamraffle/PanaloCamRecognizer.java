package com.synergy88digital.panalocamraffle;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.acrcloud.rec.sdk.ACRCloudClient;
import com.acrcloud.rec.sdk.ACRCloudConfig;
import com.acrcloud.rec.sdk.IACRCloudListener;
import com.synergy88digital.data.Synergy88Data;
import com.unity3d.player.UnityPlayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class PanaloCamRecognizer implements IACRCloudListener {
    // Account
    private static final String ACCOUNT_ACCESS_KEY = "ef3befb3598fdff1";
    private static final String ACCOUNT_ACCESS_SECRET = "a55efec5c9a100690f0770f3c0a78428";

    // Project[]
    private static final String PROJECT_ACCESS_KEY = "7f03d8cf6d39745f987ff7e820c5b48a";                    // 7f03d8cf6d39745f987ff7e820c5b48a
    private static final String PROJECT_ACCESS_SECRET = "U90Yg2Ipx9yfuS6tTTT0573GSw8o8i0y2PbpU8Iq";         // U90Yg2Ipx9yfuS6tTTT0573GSw8o8i0y2PbpU8Iq

    private static PanaloCamRecognizer instance = null;

    private PanaloCam context = null;

    private ACRCloudClient arcCloudClient = null;
    private ACRCloudConfig arcCloudConfig = null;

    private boolean isProcessing = false;
    private boolean initState = false;

    private String arcCloudPath = null;

    public PanaloCamRecognizer(Context context) {
        Log.d("panalocam", "PanaloCamRecognizer");
        this.context = (PanaloCam)context;
        this.arcCloudPath = Environment.getExternalStorageDirectory().toString() + "/acrcloud/model";

        // generate acrcloud directory
        File file = new File(this.arcCloudPath);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static PanaloCamRecognizer Instance() {
        return instance;
    }

    public static PanaloCamRecognizer Instance(Context context) {
        if (instance == null) {
            instance = new PanaloCamRecognizer(context);
        }

        return instance;
    }

    public void StartRecord() {
        if (!this.initState) {
            // online db setting exaple.
            //*
            this.arcCloudConfig = new ACRCloudConfig();
            this.arcCloudConfig.acrcloudListener = this;
            this.arcCloudConfig.context = this.context;
            this.arcCloudConfig.host = "ap-southeast-1.api.acrcloud.com";
            this.arcCloudConfig.accessKey = PROJECT_ACCESS_KEY;
            this.arcCloudConfig.accessSecret = PROJECT_ACCESS_SECRET;
            this.arcCloudConfig.reqMode = ACRCloudConfig.ACRCloudRecMode.REC_MODE_REMOTE;
            //*/

            // offline db setting example.
            /*
            this.arcCloudConfig = new ACRCloudConfig();
            this.arcCloudConfig.acrcloudListener = this;
            this.arcCloudConfig.context = this.context;
            this.arcCloudConfig.host = this.arcCloudPath; // you can change it with other path.
            this.arcCloudConfig.accessKey = PROJECT_ACCESS_KEY;
            this.arcCloudConfig.accessSecret = PROJECT_ACCESS_SECRET;
            this.arcCloudConfig.reqMode = ACRCloudConfig.ACRCloudRecMode.REC_MODE_LOCAL;
            //*/

            this.arcCloudClient = new ACRCloudClient();
            this.initState = this.arcCloudClient.initWithConfig(this.arcCloudConfig);
            if (!this.initState) {
                Log.d("panalocam", "StartRecognize Init Error");
                return;
            }
        }

        if (!this.isProcessing) {
            this.isProcessing = true;
            if (this.arcCloudClient == null || !this.arcCloudClient.startRecognize()) {
                this.isProcessing = false;
                Log.d("panalocam", "StartRecognize Start Error");
            }
        }
    }

    public void StopRecord() {
        if (this.isProcessing && this.arcCloudClient != null) {
            this.arcCloudClient.stopRecordToRecognize();
        }

        this.isProcessing = false;
    }

    public void CancelRecord() {
        if (this.isProcessing && this.arcCloudClient != null) {
            this.isProcessing = false;
            this.arcCloudClient.cancel();
        }
    }

    public void OnDestroy() {
        if (this.arcCloudClient != null) {
            this.arcCloudClient.release();
            this.initState = false;
            this.arcCloudClient = null;
        }
    }

    /************************************************************************************************
     * IACRCloudListener Inteface Methods
     **/
    @Override
    public void onResult(String result) {
        if (this.arcCloudClient != null) {
            this.arcCloudClient.cancel();
            this.isProcessing = false;
        }

        try {
            JSONObject jResult = new JSONObject(result);
            Log.d("panalocam", "onResult Result:" + jResult.toString());
            UnityPlayer.UnitySendMessage(Synergy88Data.UNITY_BRIDGE, Synergy88Data.UNITY_BRIDGE_MESSAGE, jResult.toString());
        } catch (JSONException e) {
            Log.d("panalocam", "onResult Error:" + e.getMessage());
            UnityPlayer.UnitySendMessage(Synergy88Data.UNITY_BRIDGE, Synergy88Data.UNITY_BRIDGE_MESSAGE, e.getMessage());
        }
    }

    @Override
    public void onVolumeChanged(double volume) {
    }
}
