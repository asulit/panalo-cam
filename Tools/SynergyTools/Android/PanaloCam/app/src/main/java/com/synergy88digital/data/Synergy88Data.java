package com.synergy88digital.data;

import android.content.Context;

import com.synergy88digital.panalocamraffle.PanaloCam;
import com.unity3d.player.UnityPlayer;

import org.json.JSONException;
import org.json.JSONObject;

public class Synergy88Data {
    public static final String UNITY_BRIDGE = "Bridge";
    public static final String UNITY_BRIDGE_MESSAGE = "HandleBridgeMessage";
    public static final String MESSAGE_TYPE = "MessageType";

    protected PanaloCam context;
    protected JSONObject jsonObject;

    public void SetContext(Context context) {
        this.context = (PanaloCam)context;
    }

    // commands get
    public void GetData() {
        UnityPlayer.UnitySendMessage(UNITY_BRIDGE, UNITY_BRIDGE_MESSAGE, this.jsonObject.toString());
    }

    public void GetDataBool(String key) {
        try {
            JSONObject jsonData = new JSONObject();
            jsonData.put(key, this.jsonObject.getBoolean(key));
            UnityPlayer.UnitySendMessage(UNITY_BRIDGE, UNITY_BRIDGE_MESSAGE, jsonData.toString());
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void GetDataString(String key) {
        try {
            UnityPlayer.UnitySendMessage(UNITY_BRIDGE, UNITY_BRIDGE_MESSAGE, this.jsonObject.getString(key));
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void GetDataInt(String key) {
        try {
            JSONObject jsonData = new JSONObject();
            jsonData.put(key, this.jsonObject.getInt(key));
            UnityPlayer.UnitySendMessage(UNITY_BRIDGE, UNITY_BRIDGE_MESSAGE, jsonData.toString());
            jsonData = null;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void GetDataLong(String key) {
        try {
            JSONObject jsonData = new JSONObject();
            jsonData.put(key, this.jsonObject.getLong(key));
            UnityPlayer.UnitySendMessage(UNITY_BRIDGE, UNITY_BRIDGE_MESSAGE, jsonData.toString());
            jsonData = null;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void GetDataDouble(String key) {
        try {
            JSONObject jsonData = new JSONObject();
            jsonData.put(key, this.jsonObject.getDouble(key));
            UnityPlayer.UnitySendMessage(UNITY_BRIDGE, UNITY_BRIDGE_MESSAGE, jsonData.toString());
            jsonData = null;
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void GetDataJson(String key) {
        try {
            JSONObject nestedObject = this.jsonObject.getJSONObject(key);
            UnityPlayer.UnitySendMessage(UNITY_BRIDGE, UNITY_BRIDGE_MESSAGE, nestedObject.toString());
            nestedObject = null;
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    // commands set
    public void SetDataBool(String key, boolean value) {
        try {
            this.jsonObject.put(key, value);
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void SetDataString(String key, String value) {
        try {
            this.jsonObject.put(key, value);
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void SetDataInt(String key, int value) {
        try {
            this.jsonObject.put(key, value);
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void SetDataLong(String key, long value) {
        try {
            this.jsonObject.put(key, value);
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void SetDataDouble(String key, double value) {
        try {
            this.jsonObject.put(key, value);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }
}
