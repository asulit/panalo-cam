package com.synergy88digital.panalocamraffle;

import android.telephony.SmsManager;

public class PanaloCamSms {
    private static PanaloCamSms instance = null;

    public static PanaloCamSms Instance() {
        if (instance == null) {
            instance = new PanaloCamSms();
        }

        return instance;
    }

    public void SendSMS(String receiverNumber, String message) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(receiverNumber, null, message, null, null);
    }
}
