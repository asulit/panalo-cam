package com.synergy88digital.panalocamraffle;

import android.content.Context;
import android.content.Intent;

public class PanaloCamShare {
    private static PanaloCamShare instance = null;

    private PanaloCam context = null;

    public PanaloCamShare(Context context) {
        this.context = (PanaloCam)context;
    }

    public static PanaloCamShare Instance() {
        return instance;
    }

    public static PanaloCamShare Instance(Context context) {
        if (instance == null) {
            instance = new PanaloCamShare(context);
        }

        return instance;
    }

    public void Share(String subject, String body) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
        this.context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }
}
