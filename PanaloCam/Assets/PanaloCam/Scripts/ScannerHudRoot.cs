﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class ScannerHudRoot : MonoBehaviour {

	[SerializeField]
	private Text focusText;

	[SerializeField]
	private Image border;

	private void Awake() {
		/*
		Assertion.Assert(this.focusText);
		Assertion.Assert(this.border);
		*/
	}

	private void Start() {
		// set focus to auto
		GameSignals.ON_UPDATE_CAMERA_FOCUS.ClearParameters();
		GameSignals.ON_UPDATE_CAMERA_FOCUS.AddParameter(Params.CAMERA_FOCUS_SETTINGS, (object)Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		GameSignals.ON_UPDATE_CAMERA_FOCUS.Dispatch();
	}

	public void OnClickedScan() {
		/*
		Debug.Log("ScannerHudRoot::OnClickedScan\n");
		GameSignals.ON_LOAD_OLD_SCANNER.Dispatch();
		*/
	}

	public void OnClickedFlash() {
		/*
		Debug.LogFormat("ScannerHudRoot::OnClickedFlash");
		GameSignals.ON_UPDATE_CAMERA_FLASH.ClearParameters();
		GameSignals.ON_UPDATE_CAMERA_FLASH.Dispatch();
		*/
	}

	private int focusModeIndex = -1;
	private List<Vuforia.CameraDevice.FocusMode> focusModes = new List<Vuforia.CameraDevice.FocusMode>() {
		Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO,
		Vuforia.CameraDevice.FocusMode.FOCUS_MODE_INFINITY,
		Vuforia.CameraDevice.FocusMode.FOCUS_MODE_MACRO,
		Vuforia.CameraDevice.FocusMode.FOCUS_MODE_NORMAL,
		Vuforia.CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO,
	};

	public void OnClickedFocus() {
		/*
		Debug.LogFormat("ScannerHudRoot::OnClickedFocus");
		//CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.
		if (++this.focusModeIndex >= this.focusModes.Count) {
			this.focusModeIndex = 0;
		}

		this.focusText.text = string.Format("FOCUS: {0}", this.focusModes[this.focusModeIndex]);
		
		GameSignals.ON_UPDATE_CAMERA_FOCUS.ClearParameters();
		GameSignals.ON_UPDATE_CAMERA_FOCUS.AddParameter(Params.CAMERA_FOCUS_SETTINGS, (object)this.focusModes[this.focusModeIndex]);
		GameSignals.ON_UPDATE_CAMERA_FOCUS.Dispatch();
		*/
	}
}
