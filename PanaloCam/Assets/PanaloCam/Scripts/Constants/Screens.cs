﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

public enum EScreens {
	REGISTER_SCREEN,				// RegisterScene
	LOADING_SCREEN,					// LoadingScene
	TITLE_SCREEN,					// TitleScene
	PANALO_SCREEN,					// PanaloCam
	RAFFLE_ENTRY,					// RaffleEntry
};

public enum ESubScreens {
	SCANNER_SCREEN, 				// ScanScene
	SCANNER_HUD_SCREEN, 			// ScanHud
	SCANNER_COMPLETE_SCREEN,		// ScanCompleted
	PANALO_VIDEO_PLAYER,			// PanaloCamPlayer
	//RAFFLE_ENTRY,					// RaffleEntry
};

public static class Screens {
	private static readonly Dictionary<EScreens, string> SCREENS = new Dictionary<EScreens, string>() {
		{ EScreens.REGISTER_SCREEN,	"RegisterScene" },
		{ EScreens.LOADING_SCREEN,	"LoadingScene" },
		{ EScreens.TITLE_SCREEN,	"TitleScene" },
		{ EScreens.PANALO_SCREEN,	"PanaloCam" },
		{ EScreens.RAFFLE_ENTRY,	"RaffleEntry" },
	};

	private static readonly Dictionary<ESubScreens, string> SUB_SCREENS = new Dictionary<ESubScreens, string>() {
		{ ESubScreens.SCANNER_SCREEN, 				"ScanScene" },
		{ ESubScreens.SCANNER_HUD_SCREEN, 			"ScanHud" },
		{ ESubScreens.SCANNER_COMPLETE_SCREEN,		"ScanCompleted" },
		{ ESubScreens.PANALO_VIDEO_PLAYER,			"PanaloCamPlayer" },
		//{ ESubScreens.RAFFLE_ENTRY,				"RaffleEntry" },
	};

	public static string GetScreen(EScreens screen) {
		Assertion.Assert(SCREENS.ContainsKey(screen));
		return SCREENS[screen];
	}

	public static string GetSubScreen(ESubScreens screen) {
		Assertion.Assert(SUB_SCREENS.ContainsKey(screen));
		return SUB_SCREENS[screen];
	}

	public static void Load(EScreens screen) {
		Application.LoadLevel(GetScreen(screen));
	}

	public static void LoadAdditive(ESubScreens screen) {
		Application.LoadLevelAdditive(GetSubScreen(screen));
	}
}
