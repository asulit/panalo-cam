﻿using UnityEngine;
using System.Collections;

public class AudioRecognizer : MonoBehaviour {
	[SerializeField]
	private BridgeHandler nativeCalls;

	public void Share() {
		Debug.Log("Share\n");
		this.nativeCalls.Share();
	}
	
	public void SendSMS() {
		Debug.Log("SendSMS\n");
		this.nativeCalls.SendSMS();
	}

	public void StartRecord() {
		Debug.Log("StartRecord\n");
		this.nativeCalls.StartRecord();
	}

	public void StopRecord() {
		Debug.Log("StopRecord\n");
		this.nativeCalls.StopRecord();
	}

	public void CancelRecord() {
		Debug.Log("CancelRecord\n");
		this.nativeCalls.CancelRecord();
	}

	public void CheckData() {
		Debug.Log("CheckData\n");
		this.nativeCalls.GetUserData();
	}
}
