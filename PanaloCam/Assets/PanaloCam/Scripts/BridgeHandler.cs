﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using MiniJSON;

using Common.Signal;
using Common.Utils;

#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

public class BridgeHandler : MonoBehaviour {
	// object name
	public static readonly string BRIDGE = "Bridge";

	// cross platform comands (iOS & Android)
	public static readonly string SHARE = "Share";
	public static readonly string SMS = "SendSMS";
	public static readonly string START_RECORD = "StartRecord";
	public static readonly string STOP_RECORD = "StopRecord";
	public static readonly string CANCEL_RECORD = "CancelRecord";

	// sms constants (values)
	public static readonly string RECEIVER = "09177136991";
	public static readonly string MESSAGE = "<message>Panalo Cam SMS</message>";

	private INativeCalls nativeCalls;

	// debug
	//[SerializeField]
	//private bool initializeBridge;
	
	private void Awake() {
		// set object name
		this.name = BRIDGE;

		//if (Debug.isDebugBuild) {
		//	this.initializeBridge = false;
		//}
		//else {
		//	this.initializeBridge = true;
		//}

		// setup signal listeners
		GameSignals.ON_SHARE.AddListener(this.OnShare);
		GameSignals.ON_SEND_SMS.AddListener(this.OnSendSMS);
		GameSignals.ON_START_AUDIO_RECORD.AddListener(this.OnStartAudioRecord);
		GameSignals.ON_STOP_AUDIO_RECORD.AddListener(this.OnStopAudioRecord);
		GameSignals.ON_CANCEL_AUDIO_RECORD.AddListener(this.OnCancelAudioRecord);

#if UNITY_ANDROID
			this.nativeCalls = new NativeCallsAndroid();
#elif UNITY_IOS
			this.nativeCalls = new NativeCallsIOS();
#endif
	}

	private void Start() {
		Assertion.AssertNotNull(this.nativeCalls);

		if (!Factory.Get<Platform>().IsEditor()) {
			this.nativeCalls.Start();
		}
	}

	private void OnDestroy() {
		GameSignals.ON_SHARE.RemoveListener(this.OnShare);
		GameSignals.ON_SEND_SMS.RemoveListener(this.OnSendSMS);
		GameSignals.ON_START_AUDIO_RECORD.RemoveListener(this.OnStartAudioRecord);
		GameSignals.ON_STOP_AUDIO_RECORD.RemoveListener(this.OnStopAudioRecord);
		GameSignals.ON_CANCEL_AUDIO_RECORD.RemoveListener(this.OnCancelAudioRecord);

		if (!Factory.Get<Platform>().IsEditor()) {
			this.nativeCalls.OnDestroy();
		}
	}

	#region Signal Callbacks
	private void OnShare(ISignalParameters parameters) {
		string subject = (string)parameters.GetParameter(Params.SHARE_SUBJECT);
		string body = (string)parameters.GetParameter(Params.SHARE_BODY);
		this.nativeCalls.Share(subject, body);
	}

	private void OnSendSMS(ISignalParameters parameters) {
		string smsNumber = (string)parameters.GetParameter(Params.SMS_NUMBER);
		string message = (string)parameters.GetParameter(Params.MESSAGE);
		this.nativeCalls.SendSMS(smsNumber, message);
	}

	private void OnStartAudioRecord(ISignalParameters parameters) {
		this.StartCoroutine(this.OnStartAudioRecord());
	}

	private IEnumerator OnStartAudioRecord() {
		yield return new WaitForSeconds(1f);
		this.nativeCalls.StartRecord();
	}

	private void OnStopAudioRecord(ISignalParameters parameters) {
		this.nativeCalls.StopRecord();
	}

	private void OnCancelAudioRecord(ISignalParameters parameters) {
		this.nativeCalls.CancelRecord();
	}
	#endregion

	#region Tests
	public void Share() {
		this.nativeCalls.Share("PanaloCam Subject", "PanaloCam Body");
	}

	public void SendSMS() {
		this.nativeCalls.SendSMS(RECEIVER, MESSAGE);
	}

	public void StartRecord() {
		this.nativeCalls.StartRecord();
	}

	public void StopRecord() {
		this.nativeCalls.StopRecord();
	}

	public void CancelRecord() {
		this.nativeCalls.CancelRecord();
	}

	public void GetUserData() {
		this.nativeCalls.GetUserData();
	}
	#endregion

	// Android Message Receiver
#if UNITY_ANDROID
	public void HandleBridgeMessage(string message) {
		Debug.LogErrorFormat("BridgeHandler::HandleBridgeMessage Message:{0}\n", message);
		Dictionary<string, object> result = (Dictionary<string, object>)Json.Deserialize(message);

		string STATUS_KEY = "status";
		string MESSAGE_KEY = "msg";
		string MESSAGE_SUCCESS = "Success";

		// TODO: Optimize and create a general format for message handling
		if (result != null) {
			if (result.ContainsKey(STATUS_KEY)) {
				Dictionary<string, object> status = (Dictionary<string, object>)result[STATUS_KEY];
				if (status.ContainsKey(MESSAGE_KEY)) {
					if (status[MESSAGE_KEY].ToString().Equals(MESSAGE_SUCCESS)) {
						// move to other screen
						Debug.LogErrorFormat("BridgeHandler::HandleBridgeMessage Recognized!\n");
						GameSignals.ON_AUDIO_RECOGNIZED.Dispatch();
					}
				}
			}
		}
	}
#endif
}

interface INativeCalls {
	void Start();
	void Share(string header, string message);
	void SendSMS(string number, string message);
	void StartRecord();
	void StopRecord();
	void CancelRecord();
	void GetUserData();
	void OnDestroy();
}

#if UNITY_ANDROID
class NativeCallsAndroid : INativeCalls {
	// Unity3D Android Constants
	public static readonly string UNITY_PLAYER = "com.unity3d.player.UnityPlayer";
	public static readonly string UNITY_ACTIVITY = "currentActivity";
	public static readonly string RUN_ON_UI = "runOnUiThread";

	// panalocam java package/classes
	public static readonly string PANALO_CAM_ANDROID_PACKAGE = "com.synergy88digital.panalocamdev";
	public static readonly string PANALO_CAM_SHARE = "PanaloCamShare";
	public static readonly string PANALO_CAM_SMS = "PanaloCamSms";
	public static readonly string PANALO_CAM_RECOGNIZER = "PanaloCamRecognizer";
	public static readonly string PANALO_CAM_DATA = "PanaloCamData";

	// panalocam java methods/commands
	public static readonly string INSTANCE = "Instance";

	// generic android getters (PanaloCamData)
	public static readonly string GET_DATA = "GetData";
	public static readonly string GET_DATA_BOOL = "GetDataBool";
	public static readonly string GET_DATA_STRING = "GetDataString";
	public static readonly string GET_DATA_INT = "GetDataInt";
	public static readonly string GET_DATA_LONG = "GetDataLong";
	public static readonly string GET_DATA_FLOAT = "GetDataFloat";
	public static readonly string GET_DATA_DOUBLE = "GetDataDouble";
	public static readonly string GET_DATA_JSON = "GetDataJson";
	
	// generic android setters (PanaloCamData)
	public static readonly string SET_DATA_BOOL = "SetDataBool";
	public static readonly string SET_DATA_STRING = "SetDataString";
	public static readonly string SET_DATA_INT = "SetDataInt";
	public static readonly string SET_DATA_LONG = "SetDataLong";
	public static readonly string SET_DATA_FLOAT = "SetDataFloat";
	public static readonly string SET_DATA_DOUBLE = "SetDataDouble";
	
	// fields/keys per classes  (PanaloCamData)
	public static readonly string PLAYER_NAME_KEY = "playerName";
	public static readonly string PLAYER_ID_KEY = "payerId";

	private AndroidJavaObject panaloCamContext = null;
	private AndroidJavaObject panaloCamShare = null;
	private AndroidJavaObject panaloCamSms = null;
	private AndroidJavaObject panaloCamRecognizer = null;
	private AndroidJavaObject panaloCamData = null;

	public NativeCallsAndroid() {

	}

	public void Start() {
		using (AndroidJavaClass activityClass = new AndroidJavaClass(UNITY_PLAYER)) {
			this.panaloCamContext = activityClass.GetStatic<AndroidJavaObject>(UNITY_ACTIVITY);
		}
		
		using (AndroidJavaClass pluginClass = new AndroidJavaClass(PANALO_CAM_ANDROID_PACKAGE + "." + PANALO_CAM_SHARE)) {
			this.panaloCamShare = pluginClass.CallStatic<AndroidJavaObject>(INSTANCE);
		}
		
		using (AndroidJavaClass pluginClass = new AndroidJavaClass(PANALO_CAM_ANDROID_PACKAGE + "." + PANALO_CAM_SMS)) {
			this.panaloCamSms = pluginClass.CallStatic<AndroidJavaObject>(INSTANCE);
		}
		
		using (AndroidJavaClass pluginClass = new AndroidJavaClass(PANALO_CAM_ANDROID_PACKAGE + "." + PANALO_CAM_RECOGNIZER)) {
			this.panaloCamRecognizer = pluginClass.CallStatic<AndroidJavaObject>(INSTANCE);
		}
		
		using (AndroidJavaClass pluginClass = new AndroidJavaClass(PANALO_CAM_ANDROID_PACKAGE + "." + PANALO_CAM_DATA)) {
			this.panaloCamData = pluginClass.CallStatic<AndroidJavaObject>(INSTANCE);
		}
	}

	public void Share(string header, string message) {
		this.AndroidProcess(() => this.panaloCamShare.Call(BridgeHandler.SHARE, header, message));
	}
	
	public void SendSMS(string number, string messages) {
		this.AndroidProcess(() => this.panaloCamSms.Call(BridgeHandler.SMS, number, messages));
	}

	public void StartRecord() {
		Debug.Log("BridgeHandler::NativeCallsAndroid::StartRecord\n");
		this.AndroidProcess(() => this.panaloCamRecognizer.Call(BridgeHandler.START_RECORD));
	}
	
	public void StopRecord() {
		Debug.Log("BridgeHandler::NativeCallsAndroid::StopRecord\n");
		this.AndroidProcess(() => this.panaloCamRecognizer.Call(BridgeHandler.STOP_RECORD));
	}
	
	public void CancelRecord() {
		Debug.Log("BridgeHandler::NativeCallsAndroid::CancelRecord\n");
		this.AndroidProcess(() => this.panaloCamRecognizer.Call(BridgeHandler.CANCEL_RECORD));
	}

	public void GetUserData() {
		this.AndroidProcess(() => {
			this.panaloCamData.Call(GET_DATA);
			this.panaloCamData.Call(SET_DATA_STRING, PLAYER_NAME_KEY, "UnityPlayerName");
			this.panaloCamData.Call(SET_DATA_INT, PLAYER_ID_KEY, 1);
			this.panaloCamData.Call(GET_DATA);
		});
	}

	public void OnDestroy() {
		this.panaloCamContext.Dispose();
		this.panaloCamShare.Dispose();
		this.panaloCamSms.Dispose();
		this.panaloCamRecognizer.Dispose();
		this.panaloCamData.Dispose();
	}

	private void AndroidProcess(Action action) {
		this.panaloCamContext.Call(RUN_ON_UI, new AndroidJavaRunnable(action));
	}
}
#elif UNITY_IOS
class NativeCallsIOS : INativeCalls {

	[DllImport ("__Internal")]
	private static extern void SendSMS(); //debug

	[DllImport ("__Internal")]
	private static extern void SendSMSWithParameters(string number, string message);

	public void Start() {
	}

	public void Share(string header, string message) {
	}
	
	public void SendSMS(string number, string messages) {
		// Call plugin only when running on real device
		if (Application.platform != RuntimePlatform.OSXEditor) {
			Debug.Log("Synergy88::OnClickedSendSMS Mobile");
			//SendSMS();
			SendSMSWithParameters(number, messages);
		}
		else {
			Debug.Log("Synergy88::OnClickedSendSMS Editor");
		}
	}

	public void StartRecord() {
	}

	public void StopRecord() {
	}

	public void CancelRecord() {
	}

	public void GetUserData() {
	}

	public void OnDestroy() {
	}

}
#endif