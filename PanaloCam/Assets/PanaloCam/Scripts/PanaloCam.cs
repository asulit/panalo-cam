﻿using UnityEngine;
using System.Collections;
using Common.Signal;
using Common.Fsm;
using Common.Fsm.Action;

public class PanaloCam : MonoBehaviour {

	private const string SCANNER = "ScanScene";
	private const string SCANNER_HUD = "ScanHud";
	private const string SCAN_COMPLETE = "ScanCompleted";
	private const string PANALO_PLAYER = "PanaloCamPlayer";

	private void Awake() {
		this.PrepareFsm();
	}

	private void Start() {
		GameSignals.ON_TRACKING_DETECTED.AddListener(this.OnTrackingDetected);
		GameSignals.ON_TRACKING_LOST.AddListener(this.OnTrackingLost);
		GameSignals.ON_LOAD_OLD_SCANNER.AddListener(this.OnLoadOldScanner);
		GameSignals.ON_VIDEO_PLAYER_DONE.AddListener(this.OnVideoPlayerDone);

		// set background process to false
		Application.runInBackground = false;
	}

	private void Update() {
		this.fsm.Update();
	}

	private void OnDestroy() {
		GameSignals.ON_TRACKING_DETECTED.RemoveListener(this.OnTrackingDetected);
		GameSignals.ON_TRACKING_LOST.RemoveListener(this.OnTrackingLost);
		GameSignals.ON_LOAD_OLD_SCANNER.RemoveListener(this.OnLoadOldScanner);
		GameSignals.ON_VIDEO_PLAYER_DONE.RemoveListener(this.OnVideoPlayerDone);
		Debug.Log("PanaloCam::OnDestroy\n");
	}
	
	#region Fsm
	private Fsm fsm;

	// initial state
	private const string IDLE = "Idle";

	// events
	private const string TRACKING_DETECTED = "TrackingDetected";
	private const string TRACKING_LOST = "TrackingLost";
	private const string FINISHED = "Finished";
	private const string EXIT = "Exit";

	private void PrepareFsm() {
		this.fsm = new Fsm("PanaloCam");

		// states
		FsmState idle = fsm.AddState(IDLE);
		FsmState tracking = this.fsm.AddState("Tracking");
		FsmState recognized = this.fsm.AddState("Recognized");
		FsmState lost = this.fsm.AddState("Lost");
		FsmState exit = this.fsm.AddState("Exit");
		
		// actions
		// Idle
		{
			idle.AddAction(new FsmDelegateAction(idle, delegate(FsmState owner) {
				Debug.Log("PanaloCam::Idle\n");
				Application.LoadLevelAdditive(SCANNER);
				Application.LoadLevelAdditive(SCANNER_HUD);
			}));

			TimedWaitAction wait = new TimedWaitAction(idle, string.Empty, FINISHED);
			wait.Init(1f); // wait 1 sec
			idle.AddAction(wait);
		}

		tracking.AddAction(new FsmDelegateAction(tracking, delegate(FsmState owner) {
			Debug.Log("PanaloCam::Tracking\n");
			// tracking
			GameSignals.ON_START_SCANNING.Dispatch();
		}));

		recognized.AddAction(new FsmDelegateAction(recognized, delegate(FsmState owner) {
			Debug.Log("PanaloCam::Recognized\n");
			// stop tracking
			GameSignals.ON_EXIT_SCANNING.Dispatch();

			// unload tracking scene
			// load the play video scene
			//Application.LoadLevelAdditive(SCAN_COMPLETE);
			//Application.LoadLevelAdditive(PANALO_PLAYER);

			// move back to title screen
			//Screens.Load(EScreens.LOADING_SCREEN);

			// show popup screen
			//Screens.LoadAdditive(ESubScreens.RAFFLE_ENTRY);
			Screens.Load(EScreens.RAFFLE_ENTRY);
		}));

		lost.AddAction(new FsmDelegateAction(lost, delegate(FsmState owner) {
			Debug.Log("PanaloCam::Lost\n");
			// do nothing for now
			owner.SendEvent(FINISHED);
		}));

		exit.AddAction(new FsmDelegateAction(exit, delegate(FsmState owner) {
			Debug.Log("PanaloCam::Exit\n");
			// exit scene/go to main menu
		}));

		// transitions
		idle.AddTransition(FINISHED, tracking);

		tracking.AddTransition(TRACKING_DETECTED, recognized);
		tracking.AddTransition(TRACKING_LOST, lost);

		//recognized.AddTransition(FINISHED, exit);

		lost.AddTransition(FINISHED, tracking);

		// auto start fsm
		this.fsm.Start(IDLE);
	}
	#endregion

	#region Signal Callbacks
	private void OnTrackingDetected(ISignalParameters parameters) {
		string scannedId = (string)parameters.GetParameter(Params.SCANNED_ID);
		Debug.LogFormat("PanaloCam::OnTrackingDetected ScannedId:{0}\n", scannedId);
		this.fsm.SendEvent(TRACKING_DETECTED);
	}

	private void OnTrackingLost(ISignalParameters parameters) {
		Debug.Log("PanaloCam::OnTrackingLost\n");
		this.fsm.SendEvent(TRACKING_LOST);
	}

	// rerty
	private void OnLoadOldScanner(ISignalParameters parameters) {
#if UNITY_EDITOR
		GameSignals.ON_TRACKING_DETECTED.AddParameter(Params.SCANNED_ID, "DebugScanner");
		GameSignals.ON_TRACKING_DETECTED.Dispatch();
#else
		Screens.Load(EScreens.PANALO_SCREEN);
#endif
	}

	private void OnVideoPlayerDone(ISignalParameters parameters) {
		// TODO Open text messenger here
		Screens.Load(EScreens.TITLE_SCREEN);

		GameSignals.ON_SEND_SMS.ClearParameters();
		GameSignals.ON_SEND_SMS.AddParameter(Params.SMS_NUMBER, BridgeHandler.RECEIVER);
		GameSignals.ON_SEND_SMS.AddParameter(Params.MESSAGE, BridgeHandler.MESSAGE);
		GameSignals.ON_SEND_SMS.Dispatch();
	}
	#endregion
}
