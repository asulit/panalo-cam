﻿using UnityEngine;
using System.Collections;

public class Params {
	public static readonly string SCANNED_ID = "ScannedId";
	public static readonly string CAMERA_FOCUS_SETTINGS = "CameraFocusSettings";

	// share/sms
	public static readonly string SMS_NUMBER = "SMSNumber";
	public static readonly string MESSAGE = "Message";
	public static readonly string SHARE_SUBJECT = "ShareSubject";
	public static readonly string SHARE_BODY = "ShareBuddy";

	// raffle params
	public static readonly string PROMO_ID = "PromoId";
	public static readonly string RAFFLE_RESULT = "RaffleResult";
}
