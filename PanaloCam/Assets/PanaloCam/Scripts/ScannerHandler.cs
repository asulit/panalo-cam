﻿using UnityEngine;
using Vuforia;
using Common.Signal;

public class ScannerHandler : MonoBehaviour, ITrackableEventHandler {

	private ImageTargetBehaviour targetBehaviour;
	private TrackableBehaviour trackBehaviour;
	private bool isScanning = false;
	private bool flashFlag = true;

	private void Awake() {
		this.targetBehaviour = this.GetComponent<ImageTargetBehaviour>();
		this.trackBehaviour = this.GetComponent<TrackableBehaviour>();
		Assertion.AssertNotNull(this.targetBehaviour);
		Assertion.AssertNotNull(this.trackBehaviour);
		this.trackBehaviour.RegisterTrackableEventHandler(this);
		GameSignals.ON_START_SCANNING.AddListener(this.OnStartScanning);
		GameSignals.ON_EXIT_SCANNING.AddListener(this.OnExitScanning);
		GameSignals.ON_UPDATE_CAMERA_FOCUS.AddListener(this.OnUpdateCameraFocus);
		GameSignals.ON_UPDATE_CAMERA_FLASH.AddListener(this.OnUpdateCameraFlash);
		//Debug.LogFormat("ScannerHandler::Awake TrackableName:{0}\n", this.targetBehaviour.TrackableName);
	}

	private void Start() {

	}

	private void OnDestroy() {
		GameSignals.ON_START_SCANNING.RemoveListener(this.OnStartScanning);
		GameSignals.ON_EXIT_SCANNING.RemoveListener(this.OnExitScanning);
		GameSignals.ON_UPDATE_CAMERA_FOCUS.RemoveListener(this.OnUpdateCameraFocus);
		GameSignals.ON_UPDATE_CAMERA_FLASH.RemoveListener(this.OnUpdateCameraFlash);
	}

	public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus) {
		if (!this.isScanning) {
			return;
		}

		if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
			// tracking detected
			Debug.Log("Tracking detected!\n");
			GameSignals.ON_TRACKING_DETECTED.ClearParameters();
			GameSignals.ON_TRACKING_DETECTED.AddParameter(Params.SCANNED_ID, this.trackBehaviour.TrackableName);
			GameSignals.ON_TRACKING_DETECTED.Dispatch();
		}
		else {
			// tracking lost
			Debug.Log("Tracking lost!\n");
			GameSignals.ON_TRACKING_LOST.Dispatch();
		}
	}

	private void OnStartScanning(ISignalParameters parameters) {
		Debug.Log("ScannerHandler::OnStartScanning");
		this.isScanning = true;
	}

	private void OnExitScanning(ISignalParameters parameters) {
		Debug.Log("ScannerHandler::OnExitScanning");
		this.isScanning = false;
	}

	private void OnUpdateCameraFocus(ISignalParameters parameters) {
		CameraDevice.FocusMode mode = (CameraDevice.FocusMode)parameters.GetParameter(Params.CAMERA_FOCUS_SETTINGS);
		CameraDevice.Instance.SetFocusMode(mode);
	}

	private void OnUpdateCameraFlash(ISignalParameters parameters) {
		if (CameraDevice.Instance.SetFlashTorchMode(this.flashFlag)) {
			this.flashFlag = !this.flashFlag;
		}
	}
}
