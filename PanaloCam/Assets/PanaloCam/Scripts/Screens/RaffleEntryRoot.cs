﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;

using MiniJSON;

using Common.Signal;
using Common.Utils;

public class RaffleEntryRoot : MonoBehaviour {

	[SerializeField]
	private GameObject button;

	[SerializeField]
	private Text label;

	private void Awake() {
		Assertion.AssertNotNull(this.button);
		Assertion.AssertNotNull(this.label);
		
		this.button.SetActive(false);
	}

	private void Start() {
		Factory.Get<Raffle>().RegisterPlayerToPromo();
	}

	private void OnEnable() {
		GameSignals.ON_REGISTER_RAFFLE_RESULT.AddListener(this.OnRaffleEntryResult);
	}

	private void OnDisable() {
		GameSignals.ON_REGISTER_RAFFLE_RESULT.RemoveListener(this.OnRaffleEntryResult);
	}
	
	private void OnRaffleEntryResult(ISignalParameters parameters) {
		string message = (string)parameters.GetParameter(Params.MESSAGE);
		this.button.SetActive(true);
		this.label.text = message;

		// signal.AddParameter(Params.PROMO_ID, this.raffleData.PromoId);
		if (parameters.HasParameter(Params.PROMO_ID)) {
			int promoId = (int)parameters.GetParameter(Params.PROMO_ID);
			Factory.Get<RaffleData>().RecordRaffleIdsToListen(promoId);
		}
	}

	public void OnClickedRetryButton() {
		Screens.Load(EScreens.LOADING_SCREEN);
	}

}
