﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using MiniJSON;

using Common.Fsm;
using Common.Utils;

public delegate void RequestHandler(WWW request);

public enum EStatus : int {
	Success 			= 200,
	NoAvailablePromo 	= 400,
}

public class LoadingSceneRoot : MonoBehaviour {

	[SerializeField]
	private Window window;

	[SerializeField]
	private Window errorWindow;

	private Dictionary<int, ERaffleResult> results;

//	#region debug
//	private void Start() {
//		this.StartCoroutine(this.ShowWindow());
//	}
//
//	private IEnumerator ShowWindow() {
//		yield return new WaitForEndOfFrame();
//		string message = string.Empty;
//		bool toggle = true;
//		while (true) {
//			yield return new WaitForSeconds(1.0f);
//
//			this.window.gameObject.SetActive(false);
//
//			yield return new WaitForEndOfFrame();
//			
//			if (toggle) {
//				message = "#PanaloKaKapamilya\n\nCongratulations, you won the raffle!";
//			}
//			else {
//				message = "Thank you for joining!";
//			}
//			
//			this.window.HandleMessage(message, () => {});
//			this.window.gameObject.SetActive(true);
//			toggle = !toggle;
//		}
//	}
//	#endregion

	private void Awake() {
		// set background process to false
		Application.runInBackground = false;

		Assertion.AssertNotNull(this.window);
		Assertion.AssertNotNull(this.errorWindow);

		this.results = new Dictionary<int, ERaffleResult>();
		this.PrepareFsm();
	}
	
	private void Start() {
		Factory.Get<RaffleData>().LoadCache();
	}

	private void OnEnable() {
		Factory.Get<RaffleData>().LoadCache();
		this.StopAllCoroutines();
		this.StartCoroutine(this.CheckRaffleResults());
		this.StartCoroutine(this.CheckRegisterMessage());
	}

	private void OnDisable() {
		this.StopAllCoroutines();
		Factory.Get<RaffleData>().CacheRaffles();
	}

	private void Update() {
		this.fsm.Update();
	}

	private void OnDestroy() {
		Factory.Get<RaffleData>().CacheRaffles();
	}

	#region Fsm

	private Fsm fsm;

	// initial state
	private const string IDLE = "Idle";

	// events
	private const string ON_START_SCANNING = "StartScan";
	private const string ON_RAFFLE_RESULT = "RaffleResult";
	private const string ON_RAFFLE_RESULT_DONE = "RaffleResultDone";

	private void PrepareFsm() {
		this.fsm = new Fsm("Raffle");

		// states
		FsmState idle = fsm.AddState(IDLE);
		FsmState scan = fsm.AddState("Scan");
		FsmState raffle = fsm.AddState("Raffle");

		// actions
		idle.AddAction(new FsmDelegateAction(idle, delegate(FsmState owner) {
			Debug.Log("LoadingSceneRoot::Fsm Idle state\n");
		}));

		scan.AddAction(new FsmDelegateAction(scan, delegate(FsmState owner) {
			Debug.Log("LoadingSceneRoot::Fsm Scane state\n");
			GameSignals.ON_LOAD_SCAN_SCREEN.Dispatch();
		}));

		raffle.AddAction(new FsmDelegateAction(raffle, delegate(FsmState owner) {
			Debug.Log("LoadingSceneRoot::Fsm Raffle state\n");

			int raffleId = this.results.First(r => r.Value == ERaffleResult.Won || r.Value == ERaffleResult.Lose).Key;
			ERaffleResult status = Factory.Get<RaffleData>().RaffleStatus(raffleId);

			Debug.LogFormat("LoadingSceneRoot::Fsm Raffle state RaffleId:{0} Status:{1}\n", raffleId, status);

			if (status == ERaffleResult.Won || status == ERaffleResult.Lose) {

				//string message = string.Format("{0} {1}", status.ToString(), raffleId);
				string message = string.Empty;
				if (status == ERaffleResult.Won) {
					message = "#PanaloKaKapamilya\n\nCongratulations, you won the raffle!";
				}
				else {
					message = "Thank you for joining!";
				}
				
				this.window.HandleMessage(message, delegate() {
					int cachedRaffleId = raffleId;
					Debug.LogFormat("LoadingSceneRoot::Fsm Raffle state RaffleWindowClicked:{0}\n", cachedRaffleId);
					Factory.Get<RaffleData>().RaffleDone(cachedRaffleId);
					this.results[cachedRaffleId] = ERaffleResult.Done;
					this.fsm.SendEvent(ON_RAFFLE_RESULT_DONE);
				});

				this.window.gameObject.SetActive(true);
			}
			else {
				owner.SendEvent(ON_RAFFLE_RESULT_DONE);
			}
		}));

		// transitions
		idle.AddTransition(ON_START_SCANNING, scan);
		idle.AddTransition(ON_RAFFLE_RESULT, raffle);
		
		raffle.AddTransition(ON_RAFFLE_RESULT_DONE, idle);

		// auto start fsm
		this.fsm.Start(IDLE);
	}

	#endregion

	private IEnumerator CheckRaffleResults() {
		yield return new WaitForSeconds(Poller.INITIAL_POLL_START);
		yield return new WaitForEndOfFrame();
		
		while (true) {
			
			Raffle raffle = Factory.Get<Raffle>();
			RaffleData raffleData = Factory.Get<RaffleData>();
			
			raffleData.GetRaffleResult(delegate(int raffleId, ERaffleResult result) {
				if (!this.results.ContainsKey(raffleId)) {
					this.results[raffleId] = result;
				}
			});
			
			Debug.LogFormat("LoadingSceneRoot::CheckRaffleResults Results:{0}\n", Json.Serialize(this.results));

			//if (results.Count > 0) {
			if (results.Count(r => r.Value == ERaffleResult.Won || r.Value == ERaffleResult.Lose) > 0) {
				this.fsm.SendEvent(ON_RAFFLE_RESULT);
			}
			
			yield return new WaitForSeconds(Poller.DEFAULT_POLL_DURATION);
			yield return new WaitForEndOfFrame();
		}
	}

	private IEnumerator CheckRegisterMessage() {
		yield return new WaitForEndOfFrame();
		string message = Factory.Get<Raffle>().RegisterUserMessage;

		if (string.IsNullOrEmpty(message)) {
			yield break;
		}

		// check for error window
		while (this.errorWindow.gameObject.activeSelf) {
			yield return null;
		}

		// show error window
		this.errorWindow.HandleMessage(message, delegate() {
			Factory.Get<Raffle>().RegisterUserMessage = string.Empty;
		});

		this.errorWindow.gameObject.SetActive(true);
	}

	#region BUTTONS

	public void OnClickedRegister() {
		//Factory.Get<Raffle>().RegisterUser();
	}

	public void OnClickedGetPlayerInformation() {
		Factory.Get<Raffle>().GetPlayerInformation();
	}

	public void OnClickedRegisterPlayerToPromo() {
		Factory.Get<Raffle>().RegisterPlayerToPromo();
	}

	public void OnClickedGetPromoWinner() {
		Factory.Get<Raffle>().GetPromoWinner();
	}

	public void OnClickedRaffle() {
		//Factory.Get<Raffle>().RegisterPlayerToPromo();
		//GameSignals.ON_LOAD_SCAN_SCREEN.Dispatch();
		this.fsm.SendEvent(ON_START_SCANNING);
	}

	#endregion


}