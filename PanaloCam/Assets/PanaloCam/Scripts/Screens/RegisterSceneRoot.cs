﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;

using Common.Signal;
using Common.Utils;

public class RegisterSceneRoot : MonoBehaviour {
	
	[SerializeField]
	private InputField firstnameField;
	[SerializeField]
	private InputField lastnameField;
	[SerializeField]
	private InputField mobileField;
	[SerializeField]
	private InputField companyField;

	private Dictionary<InputField, bool> prevFocusValue;
	private Dictionary<InputField, string> defaultValue;

	private bool hasValidFields = false;
	
	private void Awake() {
		Assertion.AssertNotNull(this.firstnameField);
		Assertion.AssertNotNull(this.lastnameField);
		Assertion.AssertNotNull(this.mobileField);
		Assertion.AssertNotNull(this.companyField);
		
		this.prevFocusValue = new Dictionary<InputField, bool>() {
			{ this.firstnameField, this.firstnameField.isFocused },
			{ this.lastnameField, this.lastnameField.isFocused },
			{ this.mobileField, this.mobileField.isFocused },
			{ this.companyField, this.companyField.isFocused },
		};
		
		this.defaultValue = new Dictionary<InputField, string>() {
			{ this.firstnameField, "First name" },
			{ this.lastnameField, "Last name" },
			{ this.mobileField, "Mobile number" },
			{ this.companyField, "Company name" },
		};

		// set background process to false
		Application.runInBackground = false;
	}
	
	private void Start() {
		// stop audio record
		//GameSignals.ON_START_AUDIO_RECORD.Dispatch();

		RaffleData raffleData = Factory.Get<RaffleData>();
		if (raffleData.HasData) {
			this.firstnameField.text = raffleData.FirstName;
			this.lastnameField.text = raffleData.LastName;
			this.mobileField.text = raffleData.MobileNumber;
			this.companyField.text = raffleData.CompanyName;
		}
	}

	private void OnEnable() {
		GameSignals.ON_REGISTER_USER_SUCCESSFUL.AddListener(this.OnRegisterSuccessful);
	}

	private void OnDisable() {
		GameSignals.ON_REGISTER_USER_SUCCESSFUL.RemoveListener(this.OnRegisterSuccessful);
	}
	
	private void Update() {
		if (this.prevFocusValue[this.firstnameField] != this.firstnameField.isFocused) {
			this.prevFocusValue[this.firstnameField] = this.firstnameField.isFocused;
			if (this.firstnameField.isFocused) {
				this.ClearText(this.firstnameField);
			}
		}
		
		if (this.prevFocusValue[this.lastnameField] != this.lastnameField.isFocused) {
			this.prevFocusValue[this.lastnameField] = this.lastnameField.isFocused;
			if (this.lastnameField.isFocused) {
				this.ClearText(this.lastnameField);
			}
		}

		if (this.prevFocusValue[this.mobileField] != this.mobileField.isFocused) {
			this.prevFocusValue[this.mobileField] = this.mobileField.isFocused;
			if (this.mobileField.isFocused) {
				this.ClearText(this.mobileField);
			}
		}

		if (this.prevFocusValue[this.companyField] != this.companyField.isFocused) {
			this.prevFocusValue[this.companyField] = this.companyField.isFocused;
			if (this.companyField.isFocused) {
				this.ClearText(this.companyField);
			}
		}
	}
	
	public void OnClickedPlay() {	
		Debug.Log("TitleSceneRoot::OnClickedPlay\n");
		// TODO: Remove this one
		//Application.LoadLevel("PanaloCam");
		//GameSignals.ON_LOAD_SCAN_SCREEN.Dispatch();
	}
	
	public void OnClickedSignUp() {
		Debug.Log("TitleSceneRoot::OnClickedSignUp\n");
	}

	#region Signal Listeners

	private void OnRegisterSuccessful(ISignalParameters parameters) {
		Screens.Load(EScreens.LOADING_SCREEN);
	}

	#endregion

	#region InputField/Button Events

	public void OnFirstnameEndEdit(string text) {
		//Debug.LogFormat("TitleSceneRoot::OnUsernameEndEdit Password:{0} Text:{1}\n", this.usernameField.text, text);
		this.ValidateText(this.firstnameField, text);
	}
	
	public void OnLastnameEndEdit(string text) {
		//Debug.LogFormat("TitleSceneRoot::OnPasswordEndEdit Password:{0} Text:{1}\n", this.passwordField.text, text);
		this.ValidateText(this.lastnameField, text);
	}

	public void OnMobileEndEdit(string text) {
		//Debug.LogFormat("TitleSceneRoot::OnPasswordEndEdit Password:{0} Text:{1}\n", this.passwordField.text, text);
		this.ValidateText(this.mobileField, text);
	}

	public void OnCompanyEndEdit(string text) {
		//Debug.LogFormat("TitleSceneRoot::OnPasswordEndEdit Password:{0} Text:{1}\n", this.passwordField.text, text);
		this.ValidateText(this.companyField, text);
	}

	public void OnClickedRegister() {
		this.hasValidFields = this.HasValidFields();
		if (this.hasValidFields) {
			Factory.Get<Raffle>().RegisterUser(this.firstnameField.text, this.lastnameField.text, this.mobileField.text, this.companyField.text);
		}
	}

	#endregion
	
	private void ValidateText(InputField field, string defaultText) {
		Assertion.AssertNotNull(field);
		string text = field.text;
		if (text.Equals(string.Empty)) {
			field.text = defaultText;
		}
	}
	
	private void ClearText(InputField field) {
		Assertion.AssertNotNull(field);
		if (field.text.Trim().ToLower().Equals(this.defaultValue[field].Trim().ToLower())) {
			field.text = string.Empty;
		}
	}

	private bool HasValidFields() {
		if (!this.HasValidField(this.firstnameField)) {
			return false;
		}

		if (!this.HasValidField(this.lastnameField)) {
			return false;
		}

		if (!this.HasValidField(this.mobileField)) {
			return false;
		}

		if (!this.HasValidField(this.companyField)) {
			return false;
		}

		return true;
	}

	private bool HasValidField(InputField input) { 
		string stringValue = input.text;
		if (string.IsNullOrEmpty(stringValue) || stringValue.Equals(defaultValue[input])) {
			iTween.ShakeScale(input.gameObject, Vector3.one * 0.25f, 0.25f);
			return false;
		}

		return true;
	}
}
