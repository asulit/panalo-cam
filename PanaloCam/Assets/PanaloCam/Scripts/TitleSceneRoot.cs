﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class TitleSceneRoot : MonoBehaviour {

	[SerializeField]
	private InputField usernameField;
	[SerializeField]
	private InputField passwordField;
		
	[SerializeField]
	private Image usernameLabelImage;
	[SerializeField]
	private Image passwordLabelImage;

	private Dictionary<InputField, bool> prevFocusValue;
	private Dictionary<InputField, string> defaultValue;
	private Dictionary<InputField, Image> defaultLabel;

	private void Awake() {
		Assertion.AssertNotNull(this.usernameField);
		Assertion.AssertNotNull(this.passwordField);
		Assertion.AssertNotNull(this.usernameLabelImage);
		Assertion.AssertNotNull(this.passwordLabelImage);

		this.prevFocusValue = new Dictionary<InputField, bool>() {
			{ this.usernameField, this.usernameField.isFocused },
			{ this.passwordField, this.passwordField.isFocused },
		};

		this.defaultValue = new Dictionary<InputField, string>() {
			{ this.usernameField, "username or email" },
			{ this.passwordField, "password" },
		};

		this.defaultLabel = new Dictionary<InputField, Image>() {
			{ this.usernameField, this.usernameLabelImage },
			{ this.passwordField, this.passwordLabelImage },
		};

		// set background process to false
		Application.runInBackground = false;
	}

	private void Start() {
		// stop audio record
		//GameSignals.ON_START_AUDIO_RECORD.Dispatch();
	}
	
	private void Update() {
		if (this.prevFocusValue[this.usernameField] != this.usernameField.isFocused) {
			Debug.LogFormat("Username Field changed focus! Prev:{0} New:{1}\n", this.prevFocusValue[this.usernameField], this.usernameField.isFocused);
			this.prevFocusValue[this.usernameField] = this.usernameField.isFocused;
			if (this.usernameField.isFocused) {
				this.ClearText(this.usernameField);
			}
		}

		if (this.prevFocusValue[this.passwordField] != this.passwordField.isFocused) {
			Debug.LogFormat("Password Field changed focus! Prev:{0} New:{1}\n", this.prevFocusValue[this.passwordField], this.passwordField.isFocused);
			this.prevFocusValue[this.passwordField] = this.passwordField.isFocused;
			if (this.passwordField.isFocused) {
				this.ClearText(this.passwordField);
			}
		}
	}

	public void OnClickedPlay() {	
		Debug.Log("TitleSceneRoot::OnClickedPlay\n");
		// TODO: Remove this one
		//Application.LoadLevel("PanaloCam");
		GameSignals.ON_LOAD_SCAN_SCREEN.Dispatch();
	}

	public void OnClickedSignUp() {
		Debug.Log("TitleSceneRoot::OnClickedSignUp\n");
	}

	#region InputField Events
	public void OnUsernameValueChanged() {
		Debug.LogFormat("TitleSceneRoot::OnUsernameValueChanged Username:{0}\n", this.usernameField.text);
	}

	public void OnPasswordValueChanged() {
		Debug.LogFormat("TitleSceneRoot::OnPasswordValueChanged Password:{0}\n", this.passwordField.text);
	}

	public void OnUsernameEndEdit(string text) {
		Debug.LogFormat("TitleSceneRoot::OnUsernameEndEdit Password:{0} Text:{1}\n", this.usernameField.text, text);
		this.ValidateText(this.usernameField, text);
	}

	public void OnPasswordEndEdit(string text) {
		Debug.LogFormat("TitleSceneRoot::OnPasswordEndEdit Password:{0} Text:{1}\n", this.passwordField.text, text);
		this.ValidateText(this.passwordField, text);
	}
	#endregion

	private void ValidateText(InputField field, string defaultText) {
		Assertion.AssertNotNull(field);
		Assertion.Assert(this.defaultLabel.ContainsKey(field));
		string text = field.text;
		if (text.Contains(" ") || text.Equals(string.Empty)) {
			//field.text = defaultText;
			field.text = string.Empty;
			this.defaultLabel[field].gameObject.SetActive(true);
		}
	}

	private void ClearText(InputField field) {
		Assertion.AssertNotNull(field);
		Assertion.Assert(this.defaultLabel.ContainsKey(field));
		this.defaultLabel[field].gameObject.SetActive(false);
		if (field.text.Trim().ToLower().Equals(this.defaultValue[field].Trim().ToLower())) {
			field.text = string.Empty;
		}
	}
}
