﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(GUITexture))]
public class VideoPlayer : MonoBehaviour {

	private readonly string MOVIE_TEXTURE_PATH_GLOBAL = "http://unity3d.com/files/docs/sample.ogg";
//#if UNITY_IOS
//	private readonly string MOVIE_TEXTURE_PATH_LOCAL = "MovieTextures/panalocam2_english_v1";
//#elif UNITY_ANDROID
//	private readonly string MOVIE_TEXTURE_PATH_LOCAL = "MovieTextures/panalocam2_taglish_v1";
//#else
//	private readonly string MOVIE_TEXTURE_PATH_LOCAL = "MovieTextures/Sample";
//#endif
	private readonly string MOVIE_TEXTURE_ENG_1 = "MovieTextures/panalocam_english_1";
	private readonly string MOVIE_TEXTURE_ENG_2 = "MovieTextures/panalocam_english_2";
	private readonly string MOVIE_TEXTURE_TAG_1 = "MovieTextures/panalocam_taglish_1";
	private readonly string MOVIE_TEXTURE_TAG_2 = "MovieTextures/panalocam_taglish_2";

	private readonly Vector3 POSITION = new Vector3(0.5f, 0.5f, 0.0f);

	[SerializeField]
	private bool ready;
	[SerializeField]
	private PanaloVideoPlayer player;
	private AudioSource audioSource;
	private GUITexture guiTexture;
	[SerializeField]
	private List<string> movies;

	private void Awake() {
		this.ready = false;
		this.audioSource = this.GetComponent<AudioSource>();
		this.guiTexture = this.GetComponent<GUITexture>();
		this.movies = new List<string>();
		Assertion.AssertNotNull(this.audioSource);
		Assertion.AssertNotNull(this.guiTexture);

		// initialize videos
		this.movies.Add(MOVIE_TEXTURE_ENG_1);
		this.movies.Add(MOVIE_TEXTURE_ENG_2);
		this.movies.Add(MOVIE_TEXTURE_TAG_1);
		this.movies.Add(MOVIE_TEXTURE_TAG_2);

		// initialize player
#if UNITY_ANDROID || UNITY_IOS
		//this.player = new PanaloVideoPlayer(MOVIE_TEXTURE_PATH_LOCAL);
		this.player = new PanaloVideoPlayer();
#else
		// other platforms
		MovieTexture movieTexture = Resources.Load<MovieTexture>(MOVIE_TEXTURE_PATH_LOCAL);
		this.player = new PanaloVideoPlayer(this.transform, movieTexture, this.audioSource, this.guiTexture);
#endif
	}

	private void Start() {		
		this.player.Calculate();
		this.StartCoroutine(this.player.Play(this.GetVideo()));
	}

	private void OnApplicationPause(bool pauseStatus) {
		Debug.LogFormat("VideoPlayer::OnApplicationPause State:{0}\n", pauseStatus);
		if (pauseStatus && this.player.IsPlaying) {
			// game is paused because of the video
		}
		else if(!pauseStatus && this.player.IsPlaying) {
			// game has resumed and the video has done playing! trigger something here!
			this.player.OnVideoPlayerDone();
		}
	}

	private void Update() {
		if (this.ready) {
			this.ready = false;
			this.player.Calculate();
		}

		// update
		this.player.Update();
	}

	private void OnDestroy() {
#if UNITY_ANDROID || UNITY_IOS
		Handheld.ClearShaderCache();
#endif
	}

	private string GetVideo() {
		int index = UnityEngine.Random.Range(0, this.movies.Count);
		Assertion.Assert(index >= 0, "index:" + index + " count:" + this.movies.Count);
		Assertion.Assert(index < this.movies.Count, "index:" + index + " count:" + this.movies.Count);
		return this.movies[index];
	}

	public void Play() {
		this.StartCoroutine(this.player.Play(this.GetVideo()));
	}
}

#if UNITY_ANDROID || UNITY_IOS
[SerializeField]
class PanaloVideoPlayer {

	private readonly string EXT = ".mp4"; 

	[SerializeField]
	private string movieTexturePath;
	private bool isPlaying;

	//public PanaloVideoPlayer(string movieTexturePath) {
	public PanaloVideoPlayer() {
		//this.movieTexturePath = movieTexturePath + EXT;
		this.isPlaying = false;
	}

	public void Calculate() {
	}

	public void Update() {
	}

	//public IEnumerator Play() {
	public IEnumerator Play(string movieTexturePath) {
		this.movieTexturePath = movieTexturePath + EXT;

		Debug.LogFormat("VidePlayer::Mobile::Play Video:{0}\n", this.movieTexturePath);
		this.isPlaying = true;
		Handheld.PlayFullScreenMovie(this.movieTexturePath, Color.clear, FullScreenMovieControlMode.Hidden);
#if UNITY_ANDROID 
		Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
		Handheld.StartActivityIndicator();
#elif UNITY_IOS
		Handheld.SetActivityIndicatorStyle(UnityEngine.iOS.ActivityIndicatorStyle.Gray);
		Handheld.StartActivityIndicator();
		// temp fix
		this.isPlaying = true;
		yield return new WaitForSeconds(2.5f);
		//yield return new WaitForEndOfFrame();
		this.OnVideoPlayerDone();
#endif
		yield return new WaitForEndOfFrame();
	}

	public void OnVideoPlayerDone() {
		if (!this.isPlaying) {
			return;
		}

		Debug.Log("VidePlayer::Mobile Video State:DONE\n");
		this.isPlaying = false;
		GameSignals.ON_VIDEO_PLAYER_DONE.ClearParameters();
		GameSignals.ON_VIDEO_PLAYER_DONE.Dispatch();
	}

	public bool IsPlaying {
		get {
			return this.isPlaying;
		}
	}
}
#else
[Serializable]
class PanaloVideoPlayer {

	[SerializeField]
	private Transform transform;
	[SerializeField]
	private MovieTexture movieTexture;
	[SerializeField]
	private AudioSource audioSource;
	[SerializeField]
	private GUITexture guiTexture;
	private bool isPlaying;

	public PanaloVideoPlayer(Transform transform, MovieTexture movieTexture, AudioSource audioSource, GUITexture guiTexture) {
		this.transform = transform;
		this.movieTexture = movieTexture;
		this.audioSource = audioSource;
		this.guiTexture = guiTexture;
		Assertion.AssertNotNull(this.transform);
		Assertion.AssertNotNull(this.movieTexture);
		Assertion.AssertNotNull(this.audioSource);
		Assertion.AssertNotNull(this.guiTexture);
	}

	public void Calculate() {
		// set movie texture in gui texture
		this.guiTexture.texture = this.movieTexture;
		
		// height basis
		/*
		float scaleFactor = 0.75f; // 75% of the screen
		float ratio = this.movieTexture.width / this.movieTexture.height;
		float screenHeight = Screen.height * scaleFactor;
		float screenWidth = screenHeight * ratio;
		float insetY = (screenHeight / 2) * -1;
		float insetX = (screenWidth / 2) * -1;
		*/
		// width basis
		float scaleFactor = 0.75f; // 75% of the screen
		float ratio = this.movieTexture.width / this.movieTexture.height;
		float screenWidth = Screen.width * scaleFactor;
		float screenHeight = screenWidth * ratio;
		float insetY = (screenHeight / 2) * -1;
		float insetX = (screenWidth / 2) * -1;
		
		this.transform.localScale = Vector3.zero;//new Vector3(0.25f, 0.25f, 1f);
		this.transform.position = new Vector3(0.5f, 0.5f, 0f);
		this.guiTexture.pixelInset = new Rect(insetX, insetY, screenWidth, screenHeight);
	}
	
	public void Update() {
		//if (this.movieTexture != null) {
		//	Debug.LogFormat("VideoPlayer::Update Duration:{0}\n", this.movieTexture.duration);
		//}
	}

	public IEnumerator Play() {
		yield return new WaitForEndOfFrame();
		this.isPlaying = true;
		this.movieTexture.Play();
		this.audioSource.Play();
		yield return new WaitForSeconds(this.movieTexture.duration);
		yield return new WaitForEndOfFrame();
		this.OnVideoPlayerDone();
	}

	public void OnVideoPlayerDone() {
		if (!this.isPlaying) {
			return;
		}

		Debug.Log("VidePlayer::Mobile Video State:DONE\n");
		this.isPlaying = false;
		GameSignals.ON_VIDEO_PLAYER_DONE.ClearParameters();
		GameSignals.ON_VIDEO_PLAYER_DONE.Dispatch();
	}
	
	public bool IsPlaying {
		get {
			return this.isPlaying;
		}
	}
}
#endif
