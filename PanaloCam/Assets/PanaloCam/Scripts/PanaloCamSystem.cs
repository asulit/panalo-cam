﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using Common.Signal;
using Common.Utils;

public class PanaloCamSystem : MonoBehaviour {
	[SerializeField]
	private string version = string.Empty;

	[SerializeField]
	private Text buildText;

	private void Awake() {
		Assertion.AssertNotNull(this.buildText);
	}

	private void Start() {
		this.buildText.text = this.version;

		GameSignals.ON_LOAD_TITLE_SCREEN.AddListener(this.OnLoadTitleScreen);
		GameSignals.ON_LOAD_SCAN_SCREEN.AddListener(this.OnLoadScanScreen);
		GameSignals.ON_AUDIO_RECOGNIZED.AddListener(this.OnAudioRecognized);

		//GameSignals.ON_LOAD_TITLE_SCREEN.Dispatch();

		Debug.LogErrorFormat("PanaloCamSystem::Start HasData:{0}\n", Factory.Get<RaffleData>().HasData);

		// TODO: Add screen checking here
		if (!Factory.Get<RaffleData>().HasData) {
			Screens.Load(EScreens.REGISTER_SCREEN);
		}
		else {
			Screens.Load(EScreens.LOADING_SCREEN);
		}
	}

	private void OnEnable() {
		Factory.Register<Platform>(new Platform());
	}

	private void OnDisable() {
		Factory.Clean<Platform>();
	}

	private void OnLoadTitleScreen(ISignalParameters parameters) {
		// TODO: Remove this one
		Screens.Load(EScreens.LOADING_SCREEN);
	}

	private void OnLoadScanScreen(ISignalParameters parameters) {
		// TODO: Remove this one
		Screens.Load(EScreens.PANALO_SCREEN);
		
		// stop audio record
		GameSignals.ON_STOP_AUDIO_RECORD.Dispatch();
	}

	private void OnAudioRecognized(ISignalParameters parameters) {
		// TODO: Remove this one
		Screens.Load(EScreens.PANALO_SCREEN);

		// stop audio record
		GameSignals.ON_STOP_AUDIO_RECORD.Dispatch();
	}
}
