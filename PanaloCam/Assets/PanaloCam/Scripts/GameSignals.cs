﻿using UnityEngine;
using System.Collections;
using Common.Signal;

public class GameSignals {

	// screen switching
	public static readonly Signal ON_LOAD_TITLE_SCREEN = new Signal("OnLoadTitleScreen");
	public static readonly Signal ON_LOAD_SCAN_SCREEN = new Signal("OnLoadScanScreen");

	// scan flags
	public static readonly Signal ON_START_SCANNING = new Signal("OnStartScanning");
	public static readonly Signal ON_EXIT_SCANNING = new Signal("OnExitScannint");

	public static readonly Signal ON_TRACKING_DETECTED = new Signal("OnTrackingDetected");
	public static readonly Signal ON_TRACKING_LOST = new Signal("OnTrackingLost");

	// camera settings
	public static readonly Signal ON_UPDATE_CAMERA_FOCUS = new Signal("OnUpdateCameraFocus");
	public static readonly Signal ON_UPDATE_CAMERA_FLASH = new Signal("OnUpdateCameraFlash");

	// messenger
	public static readonly Signal ON_SHARE = new Signal("OnShare");
	public static readonly Signal ON_SEND_SMS = new Signal("OnSendSMS");

	// audio fingerprint
	public static readonly Signal ON_START_AUDIO_RECORD = new Signal("OnStartAudioRecord");
	public static readonly Signal ON_STOP_AUDIO_RECORD = new Signal("OnStopAudioRecord");
	public static readonly Signal ON_CANCEL_AUDIO_RECORD = new Signal("OnCancelAudioRecord");
	public static readonly Signal ON_AUDIO_RECOGNIZED = new Signal("OnAudioRecognized");

	// raffle
	public static readonly Signal ON_REGISTER_USER_SUCCESSFUL = new Signal("OnRegisterUserSuccessful");
	public static readonly Signal ON_REGISTER_RESULT = new Signal("OnRegisterResult");
	public static readonly Signal ON_REGISTER_RAFFLE_RESULT = new Signal("OnRegisterRaffleResult");
	public static readonly Signal ON_RAFFLE_WINNERS_RESULT = new Signal("OnRaffleWinnersResult");

	// debug
	public static readonly Signal ON_LOAD_OLD_SCANNER = new Signal("OnLoadOldScanner");
	public static readonly Signal ON_VIDEO_PLAYER_DONE = new Signal("OnVideoPlayerDone");
}
