﻿using UnityEngine;
using System.Collections;

public class Alive : MonoBehaviour {
	private void Awake() {
		GameObject.DontDestroyOnLoad(this.gameObject);
	}
}
