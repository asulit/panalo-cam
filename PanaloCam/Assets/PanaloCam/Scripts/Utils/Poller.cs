﻿using UnityEngine;

using System.Collections;

using Common.Utils;

public class Poller : MonoBehaviour {

	public const float INITIAL_POLL_START = 2.0f;
	public const float DEFAULT_POLL_DURATION = 5.0f;

	[SerializeField]
	private float duration = DEFAULT_POLL_DURATION;

	private void Start() {
		this.StopAllCoroutines();
		this.StartCoroutine(this.StartPoll());
	}

	private void OnEnable() {
		this.StopAllCoroutines();
		this.StartCoroutine(this.StartPoll());
	}

	private void OnDisable() {
		this.StopAllCoroutines();
	}

	/*
	I/Unity   (32506): Raffle::GetPromoWinner API:http://202.147.26.33/panalo-cam/public/api/v1/promo/winner?api_key=f2bae1929bb1724d74a241d74a14ed09&player_id=111&promo_id=24&t=0.7101147 Result:{
		I/Unity   (32506):     "data": [],
		I/Unity   (32506):     "status": 200,
	I/Unity   (32506):     "message": {
		I/Unity   (32506):         "won": "you won"
		I/Unity   (32506):     }
	I/Unity   (32506): }
	*/


	private IEnumerator StartPoll() {
		yield return new WaitForSeconds(INITIAL_POLL_START);
		yield return new WaitForEndOfFrame();

		while (true) {

			Debug.Log("Poller::Polling..\n");

			Raffle raffle = Factory.Get<Raffle>();
			RaffleData raffleData = Factory.Get<RaffleData>();

			raffleData.ListenToRaffle(delegate(int raffleId) {
				Debug.LogFormat("Poller::Polling RaffleId:{0}\n", raffleId);
				raffle.GetPromoWinner(raffleId);
			});

			yield return new WaitForSeconds(DEFAULT_POLL_DURATION);
			yield return new WaitForEndOfFrame();
		}
	}

}
