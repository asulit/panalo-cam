﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;

// alias
using UObject = UnityEngine.Object;

[RequireComponent(typeof(RawImage))]
public class Animator : MonoBehaviour {
	[SerializeField]
	private bool loop = true;

	[SerializeField]
	private RawImage image;

	[SerializeField]
	private List<AnimatorFrame> frames;
	
	private void Awake() {
		Assertion.AssertNotNull(this.image);
		Assertion.AssertNotNull(this.frames);
		Assertion.Assert(this.frames.Count > 0);
	}

	private void OnEnable() {
		this.StopAllCoroutines();
		this.PlayAnimation();
	}

	private void OnDisable() {
		this.StopAllCoroutines();
	}

	private IEnumerator PlayAnimationLoop(float delay = 0.0f) {
		if (delay > 0.0f) {
			yield return new WaitForSeconds(delay);
		}

		while (this.frames.Count > 0) {
			foreach (AnimatorFrame frame in this.frames) {
				this.image.texture = frame.Texture;
				yield return new WaitForSeconds(frame.Duration);
			}

			if (!this.loop) {
				yield break;
			}
		}
	}

	public void PlayAnimation(float delay = 0.0f) {
		this.StartCoroutine(this.PlayAnimationLoop(delay));
	}

	public bool Loop {
		get { return this.loop; }
		set { this.loop = value; }
	}
}

[Serializable]
public struct AnimatorFrame {
	public float Duration;
	public Texture Texture;
};