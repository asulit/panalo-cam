﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public delegate void Handle();

public class Window : MonoBehaviour {

	[SerializeField]
	private Text message;

	[SerializeField]
	private GameObject button;

	private string body = string.Empty;
	private Handle handler = null;

	private void Awake() {
		Assertion.AssertNotNull(this.message);
		Assertion.AssertNotNull(this.button);
	}

	private void OnEnable() {
		this.message.text = this.body;
	}

	public void OnClickedButton() {
		if (this.handler != null) {
			this.handler();
			this.handler = null;
		}
		this.gameObject.SetActive(false);
	}

	public void HandleMessage(string message, Handle handler) {
		this.body = message;
		this.handler = handler;
	}
}
