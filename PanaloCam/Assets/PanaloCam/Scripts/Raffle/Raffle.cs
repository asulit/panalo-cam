﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using MiniJSON;

using Common.Signal;
using Common.Utils;

// alias
using URand = UnityEngine.Random;

public class Raffle : MonoBehaviour {

	// api
	//public const string API_URL = "http://202.147.26.33/panalo-cam/public";
	public const string API_URL = "http://52.11.174.128/panalo-cam/public";
	public const string API_KEY_VALUE = "f2bae1929bb1724d74a241d74a14ed09";
	public const string REGISTER_PLAYER = "{0}/api/v1/player/create"; 							// POST : URL/API (api_key, device_id)
	public const string GET_PLAYER_INFO = "{0}/api/v1/player/{1}";								// GET	: URL/API/PLAYERID
	public const string REGISTER_PLAYER_TO_PROMO = "{0}/api/v1/promo/register";					// POST	: URL/API (api_key, player_id)
	public const string GET_PROMO_WINNER = "{0}/api/v1/promo/winner"; 							// GET	: URL/API (api_key, promo_id, player_id)

	// keys
	public const string DATA = "data";
	public const string STATUS = "status";
	public const string MESSAGE = "message";
	public const string ERROR_MESSAGE = "error_message";
	public const string REGISTERED_PLAYER = "registered_player";
	public const string PLAYER_EXIST = "player_exist";
	public const string NO_AVAILABLE_PROMO = "no_available_promo";
	public const string REGISTRATION_SUCCESS = "regestration_success";
	public const string PROMO_ONGOING = "promo_ongoing";
	public const string WON = "won";
	public const string LOSE = "lose";
	
	public const string API_KEY = "api_key";
	public const string DEVICE_ID = "device_id";
	public const string PLAYER_ID = "player_id";
	public const string PROMO_ID = "promo_id";
	public const string FIRST_NAME = "first_name";
	public const string LAST_NAME = "last_name";
	public const string GENDER = "gender";
	public const string RANDOM = "t";

	// new keys (because of the inconsistencies of the new keys)
	public const string NEW_FIRST_NAME = "fname";
	public const string NEW_LAST_NAME = "lname";
	public const string NEW_MOBILE = "mobile";
	public const string NEW_MOBILE_NUMBER = "mobile_number";
	public const string NEW_COMPANY = "company_name";

	private RaffleData raffleData = null;

	private void OnEnable() {
		Factory.Register<Raffle>(this);
	}

	private void OnDisable() {
		Factory.Clean<Raffle>();
	}

	/*
	{
		"data": {
			"player_id": 105,
			"first_name": "Guest",
			"last_name": "",
			"gender": "",
			"device_id": "0001",
			"mobile_number": "09129057654",
			"company_name": "Synergy88",
			"device_id": "34243242345"
		},
		"status": 200,
		"error_message": {
			"player_exist": "Existing Player Information has been updated"
 		}	
	}
	*/

	public string RegisterUserMessage = string.Empty;
	public void RegisterUser(string firstName, string lastName, string mobileNumber, string company) {
		this.ResolveRaffleData();

		string deviceId = Factory.Get<Platform>().DeviceId;
		
		WWWForm form = new WWWForm();
		form.AddField(API_KEY, API_KEY_VALUE);
		form.AddField(DEVICE_ID, deviceId);
		form.AddField(NEW_FIRST_NAME, firstName);
		form.AddField(NEW_LAST_NAME, lastName);
		form.AddField(NEW_MOBILE, mobileNumber);
		form.AddField(NEW_COMPANY, company);
		
		string api = string.Format(REGISTER_PLAYER, API_URL);
		WWW request = new WWW(api, form);
		
		this.StartCoroutine(this.ProcessRequest(request, delegate(WWW result) {
			if (result.error != null) {
				Debug.LogFormat("Raffle::RegisterPlayerApi API:{0} ERROR:{1}\n", api, result.error);
			}
			else {
				Debug.LogFormat("Raffle::RegisterPlayerApi API:{0} Result:{1}\n", api, result.text);
				Dictionary<string, object> resultData = (Dictionary<string, object>)Json.Deserialize(result.text);
				
				// player data
				Dictionary<string, object> playerData = (Dictionary<string, object>)resultData[DATA];

				this.raffleData.PlayerId = int.Parse(playerData[PLAYER_ID].ToString());
				this.raffleData.FirstName = (string)playerData[FIRST_NAME];
				this.raffleData.LastName = (string)playerData[LAST_NAME];
				this.raffleData.MobileNumber = (string)playerData[NEW_MOBILE_NUMBER];
				this.raffleData.CompanyName = (string)playerData[NEW_COMPANY];
				this.raffleData.Gender = (string)playerData[GENDER];
				this.raffleData.DeviceId = (string)playerData[DEVICE_ID];
				this.raffleData.CachePlayerData();

				// status
				int status = -1;
				if (resultData.ContainsKey(STATUS)) {
					status = int.Parse(resultData[STATUS].ToString());
				}
				
				// error message
				this.RegisterUserMessage = string.Empty;
				if (resultData.ContainsKey(ERROR_MESSAGE)) {
					Dictionary<string, object> message = (Dictionary<string, object>)resultData[ERROR_MESSAGE];
					if (message.ContainsKey(PLAYER_EXIST)) {
						this.RegisterUserMessage = (string)message[PLAYER_EXIST];
					}
				}

				// trigger register successful
				Signal signal = GameSignals.ON_REGISTER_USER_SUCCESSFUL;
				signal.Dispatch();
			}
		}));
	}
	
	/*
	{
		"data": {
			"player_id": 105,
			"first_name": "Guest",
			"last_name": "",
			"gender": "",
			"device_id": "0001"
		},
		"status": 200
	}
	*/
	
	public void GetPlayerInformation() {
		this.ResolveRaffleData();

		int playerId = this.raffleData.PlayerId;
		string api = string.Format(GET_PLAYER_INFO, API_URL, playerId);
		WWW request = new WWW(api);
		
		this.StartCoroutine(this.ProcessRequest(request, delegate(WWW result) {
			if (result.error != null) {
				Debug.LogFormat("Raffle::GetPlayerInformation API:{0} ERROR:{1}\n", api, result.error);
			}
			else {
				Debug.LogFormat("Raffle::GetPlayerInformation API:{0} Result:{1}\n", api, result.text);
				Dictionary<string, object> resultData = (Dictionary<string, object>)Json.Deserialize(result.text);
				resultData = (Dictionary<string, object>)resultData[DATA];

				this.raffleData.PlayerId = int.Parse(resultData[PLAYER_ID].ToString());
				this.raffleData.FirstName = (string)resultData[FIRST_NAME];
				this.raffleData.LastName = (string)resultData[LAST_NAME];
				this.raffleData.Gender = (string)resultData[GENDER];
				this.raffleData.DeviceId = (string)resultData[DEVICE_ID];
			}
		}));
	}
	
	// panalocamcam_register_player_to_promo
	/*
	{
		"data": [],
		"status": 200,
		"promo_id": 12,
		"message": {
			"regestration_success": "success" 
		}
	}
	
	{
	 	"data": [],
	  	"status": 400,
	  	"error_message": {
	    	"no_available_promo": "no available promo"
	  	}
	}

	{
	    "data": [],
	    "status": 400,
	    "error_message": {
	        "registered_player": "player already registered"
	    }
	}
	*/
	
	public void RegisterPlayerToPromo() {
		this.ResolveRaffleData();

		int playerId = this.raffleData.PlayerId;
		
		WWWForm form = new WWWForm();
		form.AddField(API_KEY, API_KEY_VALUE);
		form.AddField(PLAYER_ID, playerId);
		
		string api = string.Format(REGISTER_PLAYER_TO_PROMO, API_URL);
		WWW request = new WWW(api, form);
		
		this.StartCoroutine(this.ProcessRequest(request, delegate(WWW result) {
			if (result.error != null) {
				this.raffleData.PromoId = -1;

				// Raffle::RegisterPlayerToPromo API:http://202.147.26.33/panalo-cam/public/api/v1/promo/register/ ERROR:necessary data rewind wasn't possible
				Debug.LogFormat("Raffle::RegisterPlayerToPromo API:{0} ERROR:{1}\n", api, result.error);

				Signal signal = GameSignals.ON_REGISTER_RAFFLE_RESULT;
				signal.ClearParameters();
				signal.AddParameter(Params.MESSAGE, "ERROR");
				signal.Dispatch();
			}
			else {
				Debug.LogFormat("Raffle::RegisterPlayerToPromo API:{0} Result:{1}\n", api, result.text);
				Dictionary<string, object> resultData = (Dictionary<string, object>)Json.Deserialize(result.text);
				//resultData = (Dictionary<string, object>)resultData[DATA];
				
				int status = int.Parse(resultData[STATUS].ToString());
				if (status == (int)EStatus.Success) {
					this.raffleData.PromoId = int.Parse(resultData[PROMO_ID].ToString());
					Dictionary<string, object> message = (Dictionary<string, object>)resultData[MESSAGE];
					if (message.ContainsKey(REGISTRATION_SUCCESS)) {
						Debug.LogFormat("Raffle::RegisterPlayerToPromo Message:{0}\n", (string)message[REGISTRATION_SUCCESS]);

						Signal signal = GameSignals.ON_REGISTER_RAFFLE_RESULT;
						signal.ClearParameters();
						signal.AddParameter(Params.MESSAGE, (string)message[REGISTRATION_SUCCESS]);
						signal.AddParameter(Params.PROMO_ID, this.raffleData.PromoId);
						signal.Dispatch();
					}
					else {
						Signal signal = GameSignals.ON_REGISTER_RAFFLE_RESULT;
						signal.ClearParameters();
						signal.AddParameter(Params.MESSAGE, "ERROR");
						signal.Dispatch();

						Assertion.Assert(false);
					}
				}
				else if (status == (int)EStatus.NoAvailablePromo) {
					this.raffleData.PromoId = -1;
					Dictionary<string, object> message = (Dictionary<string, object>)resultData[ERROR_MESSAGE];
					if (message.ContainsKey(NO_AVAILABLE_PROMO)) {
						Debug.LogFormat("Raffle::RegisterPlayerToPromo Message:{0}\n", (string)message[NO_AVAILABLE_PROMO]);

						Signal signal = GameSignals.ON_REGISTER_RAFFLE_RESULT;
						signal.ClearParameters();
						signal.AddParameter(Params.MESSAGE, (string)message[NO_AVAILABLE_PROMO]);
						signal.Dispatch();
					}
					else if (message.ContainsKey(REGISTERED_PLAYER)) {
						Debug.LogFormat("Raffle::RegisterPlayerToPromo Message:{0}\n", (string)message[REGISTERED_PLAYER]);

						Signal signal = GameSignals.ON_REGISTER_RAFFLE_RESULT;
						signal.ClearParameters();
						signal.AddParameter(Params.MESSAGE, (string)message[REGISTERED_PLAYER]);
						signal.Dispatch();
					}
					else {
						Signal signal = GameSignals.ON_REGISTER_RAFFLE_RESULT;
						signal.ClearParameters();
						signal.AddParameter(Params.MESSAGE, "ERROR");
						signal.Dispatch();

						Assertion.Assert(false);
					}
				}
				else {
					this.raffleData.PromoId = -1;

					Signal signal = GameSignals.ON_REGISTER_RAFFLE_RESULT;
					signal.ClearParameters();
					signal.AddParameter(Params.MESSAGE, "ERROR");
					signal.Dispatch();

					Assertion.Assert(false);
				}
			}
		}));
	}
	
	/*
	{
		"data": [],
		"status": 200,
		"message": {
			"won": "you won"
		}
	}

	{
	  	"data": [],
	  	"status": 200,
	  	"message": {
	    	"lose": "you lose"
	  	}
	}

	{
	    "data": [],
	    "status": 400,
	    "error_message": {
	        "promo_ongoing": "promo is currently on going"
	    }
	}

	*/
	
	public void GetPromoWinner() {
		this.GetPromoWinner(this.raffleData.PromoId);
	}

	public void GetPromoWinner(int raffleId) {
		this.ResolveRaffleData();

		string promoId = raffleId.ToString();
		string playerId = this.raffleData.PlayerId.ToString();
		string random = URand.value.ToString();

		/*
		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add(API_KEY, API_KEY_VALUE);
		headers.Add(PLAYER_ID, playerId);
		headers.Add(PROMO_ID, promoId);
		headers.Add(RANDOM, random);
		headers.Add("cache-control", "no-cache");
		
		string api = string.Format(GET_PROMO_WINNER, API_URL);
		api = string.Format("{0}?t={1}", api, random);

		WWW request = new WWW(api, null, headers);
		*/

		string api = string.Format(GET_PROMO_WINNER, API_URL);
		api = api + "?" + API_KEY + "=" + API_KEY_VALUE;
		api = api + "&" + PLAYER_ID + "=" + playerId;
		api = api + "&" + PROMO_ID + "=" + promoId;
		api = api + "&" + RANDOM + "=" + random;

		WWW request = new WWW(api);

		Debug.LogFormat("Raffle::GetPromoWinner URL:{0} API:{1} ApiKey:{2} PlayerId:{3} PromoId:{4}\n", request.url, api, API_KEY_VALUE, playerId, promoId);

		this.StartCoroutine(this.ProcessRequest(request, delegate(WWW result) {
			int promoToCheck = raffleId;

			if (result.error != null) {
				Debug.LogFormat("Raffle::GetPromoWinner API:{0} ERROR:{1}\n", api, result.error);
			}
			else {
				Debug.LogFormat("Raffle::GetPromoWinner API:{0} Result:{1}\n", api, result.text);
				Dictionary<string, object> resultData = (Dictionary<string, object>)Json.Deserialize(result.text);
				//resultData = (Dictionary<string, object>)resultData[DATA];
				
				int status = int.Parse(resultData[STATUS].ToString());
				if (status == (int)EStatus.Success) {
					Dictionary<string, object> message = (Dictionary<string, object>)resultData[MESSAGE];
					if (message.ContainsKey(WON)) {
						Debug.LogFormat("Raffle::GetPromoWinner Message:{0}\n", (string)message[WON]);

						Signal signal = GameSignals.ON_RAFFLE_WINNERS_RESULT;
						signal.ClearParameters();
						signal.AddParameter(Params.RAFFLE_RESULT, ERaffleResult.Won);
						signal.AddParameter(Params.PROMO_ID, promoToCheck);
						signal.Dispatch();
					}
					else if (message.ContainsKey(LOSE)) {
						Debug.LogFormat("Raffle::GetPromoWinner Message:{0}\n", (string)message[LOSE]);

						Signal signal = GameSignals.ON_RAFFLE_WINNERS_RESULT;
						signal.ClearParameters();
						signal.AddParameter(Params.RAFFLE_RESULT, ERaffleResult.Lose);
						signal.AddParameter(Params.PROMO_ID, promoToCheck);
						signal.Dispatch();
					}
					else {
						Assertion.Assert(false);
					}
				}
				else if (status == (int)EStatus.NoAvailablePromo) {
					Dictionary<string, object> message = (Dictionary<string, object>)resultData[ERROR_MESSAGE];
					if (message.ContainsKey(PROMO_ONGOING)) {
						Debug.LogFormat("Raffle::GetPromoWinner Message:{0}\n", (string)message[PROMO_ONGOING]);
					}
					else {
						Assertion.Assert(false);
					}
				}
				else {
					Debug.LogFormat("Raffle::GetPromoWinner Sorry");
				}
			}
		}));
	}
	
	private IEnumerator ProcessRequest(WWW request, RequestHandler handler) {
		// wait for request to complete
		yield return request;
		
		// handle result
		handler(request);
	}
	
	private void ResolveRaffleData() {
		if (this.raffleData == null) {
			this.raffleData = Factory.Get<RaffleData>();
		}

		// clear cache
		Caching.CleanCache();
	}
}
