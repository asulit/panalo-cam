﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using MiniJSON;

using Common.Extensions;
using Common.Signal;
using Common.Utils;

// alias
using RaffleId = System.Int32;

[Flags]
public enum ERaffleResult {
	Invalid = -1,
	Waiting	= 0,
	Won		= 1,
	Lose	= 2,
	Done	= 3,
};

public delegate void ListenRaffle(RaffleId raffleId);
public delegate void ListenRaffleResult(RaffleId raffleId, ERaffleResult result);

[Serializable]
public class RaffleData : MonoBehaviour {

	// prefs keys
	public const string PLAYER_DATA = "RafflePlayerData";
	public const string RECORDED_RAFFLES = "recordedRaffles";

	public int PlayerId = -1;
	public int PromoId = -1;
	public string FirstName = string.Empty;
	public string LastName = string.Empty;
	public string MobileNumber = string.Empty;
	public string CompanyName = string.Empty;
	public string Gender = string.Empty;
	public string DeviceId = string.Empty;

	// registered to raffles
	private Dictionary<RaffleId, ERaffleResult> Raffles;

	private void Awake() {
		this.Raffles = new Dictionary<int, ERaffleResult>();
		this.HasData = false;
	}

	private void Start() {
		this.LoadCache();
	}

	private void OnEnable() {
		Factory.Register<RaffleData>(this);
		this.LoadCache();

		GameSignals.ON_RAFFLE_WINNERS_RESULT.AddListener(this.OnRaffleWinners);
	}
	
	private void OnDisable() {
		Factory.Clean<RaffleData>();
		this.CacheRaffles();

		GameSignals.ON_RAFFLE_WINNERS_RESULT.RemoveListener(this.OnRaffleWinners);
	}

	#region Signals

	private void OnRaffleWinners(ISignalParameters parameters) {
		ERaffleResult result = (ERaffleResult)parameters.GetParameter(Params.RAFFLE_RESULT);
		RaffleId promoId = (RaffleId)parameters.GetParameter(Params.PROMO_ID);

		if (this.Raffles.ContainsKey(promoId)) {
			if (result != this.Raffles[promoId]) {
				this.Raffles[promoId] = result;
				this.CacheRaffles();
			}
		}
	}

	#endregion

	public void LoadCache() {
		if (PlayerPrefs.HasKey(RECORDED_RAFFLES)) {
			string raffles = PlayerPrefs.GetString(RECORDED_RAFFLES, string.Empty);
			Debug.LogFormat("~~~~ RaffleData::LoadCache Raffles:{0}\n", raffles);

			if (!string.IsNullOrEmpty(raffles)) {
				Dictionary<string, object> raffleDict = (Dictionary<string, object>)Json.Deserialize(raffles);
				if (raffleDict == null || raffleDict.Count > 0) {
					foreach (KeyValuePair<string, object> pair in raffleDict) {
						int raffleId = int.Parse(pair.Key.ToString());
						ERaffleResult result = pair.Value.ToString().ToEnum<ERaffleResult>();

						this.Raffles[raffleId] = result;
					}

					// save raffles
					this.CacheRaffles();
				}
				else {
					Debug.LogWarning("[WARNING] RaffleData::LoadCache Raffles dictionary is null!\n");
				}
			}
		}

		// reset data flag
		this.HasData = false;

		if (PlayerPrefs.HasKey(PLAYER_DATA)) {
			string raffleData = PlayerPrefs.GetString(PLAYER_DATA, string.Empty);

			if (!string.IsNullOrEmpty(raffleData)) {
				Dictionary<string, object> raffleDataDict = (Dictionary<string, object>)Json.Deserialize(raffleData);
				if (raffleDataDict == null || raffleDataDict.Count > 0) {
					//raffleDataDict.TryGetValue("", this.PlayerId);
					
					object obj = null;
					raffleDataDict.TryGetValue("PlayerId", out obj);
					int.TryParse(obj.ToString(), out this.PlayerId);

					raffleDataDict.TryGetValue("FirstName", out obj);
					this.FirstName = obj.ToString();

					raffleDataDict.TryGetValue("LastName", out obj);
					this.LastName = obj.ToString();

					raffleDataDict.TryGetValue("MobileNumber", out obj);
					this.MobileNumber = obj.ToString();

					raffleDataDict.TryGetValue("CompanyName", out obj);
					this.CompanyName = obj.ToString();

					raffleDataDict.TryGetValue("Gender", out obj);
					this.Gender = obj.ToString();

					raffleDataDict.TryGetValue("DeviceId", out obj);
					this.DeviceId = obj.ToString();

					this.HasData = true;

					/*
					raffleDataDict.TryGetValue("PlayerId", out this.PlayerId);
					raffleDataDict.TryGetValue("FirstName", out this.FirstName);
					raffleDataDict.TryGetValue("LastName", out this.LastName);
					raffleDataDict.TryGetValue("MobileNumber", out this.MobileNumber);
					raffleDataDict.TryGetValue("CompanyName", out this.CompanyName);
					raffleDataDict.TryGetValue("Gender", out this.Gender);
					raffleDataDict.TryGetValue("DeviceId", out this.DeviceId);
					*/
				}
			}
		}
	}

	public void CachePlayerData() {
		Dictionary<string, string> playerData = new Dictionary<string, string>() {
			{ "PlayerId", this.PlayerId.ToString() },
			{ "FirstName", this.FirstName },
			{ "LastName", this.LastName },
			{ "MobileNumber", this.MobileNumber },
			{ "CompanyName", this.CompanyName },
			{ "Gender", this.Gender },
			{ "DeviceId", this.DeviceId },
		};

		string raffleData = Json.Serialize(playerData);
		PlayerPrefs.SetString(PLAYER_DATA, raffleData);
		PlayerPrefs.Save();
	}

	public void CacheRaffles() {
		string raffles = Json.Serialize(this.Raffles);
		PlayerPrefs.SetString(RECORDED_RAFFLES, raffles);
		PlayerPrefs.Save();

		Debug.LogFormat("~~~~ RaffleData::CacheRaffles Raffles:{0}\n", raffles);
	}

	public void RecordRaffleIdsToListen(RaffleId raffle) {
		if (!this.Raffles.ContainsKey(raffle)) {
			this.Raffles.Add(raffle, ERaffleResult.Waiting);
			this.CacheRaffles();
		}
	}

	public void RaffleDone(RaffleId raffleId) {
		this.Raffles[raffleId] = ERaffleResult.Done;
		this.CacheRaffles();
	}

	public ERaffleResult RaffleStatus(RaffleId raffleId) {
		if (this.Raffles.ContainsKey(raffleId)) {
			return this.Raffles[raffleId];
		}

		return ERaffleResult.Invalid;
	}

	public void ListenToRaffle(ListenRaffle listen) {
		foreach (KeyValuePair<RaffleId, ERaffleResult> pair in this.Raffles) {
			if (pair.Value == ERaffleResult.Waiting) {
				listen(pair.Key);
			}
		}
	}

	public void GetRaffleResult(ListenRaffleResult listen) {
		foreach (KeyValuePair<RaffleId, ERaffleResult> pair in this.Raffles) {
			if (pair.Value == ERaffleResult.Lose || pair.Value == ERaffleResult.Won) {
				listen(pair.Key, pair.Value);
			}
		}
	}

	public bool HasData { get; private set; }
}
