#import <Foundation/Foundation.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface Synergy88Delegate : NSObject<MFMessageComposeViewControllerDelegate>
{
}

@property (readwrite, retain) UIViewController* unityView;

- (instancetype) initWithView:(UIViewController*)p_unityView;

- (void) messageComposeViewController:(MFMessageComposeViewController*)controller
                  didFinishWithResult:(MessageComposeResult)result;

@end



