#import "Synergy88Digital.h"
#import "Synergy88DigitalVC.h"
#import "Synergy88Delegate.h"

@implementation Synergy88Digital

#pragma mark -
#pragma mark Helpers
NSString* ToNSString(const char* p_str)
{
    if (p_str)
    {
        return [NSString stringWithUTF8String:p_str];
    }
    else
    {
        return [NSString stringWithUTF8String:""];
    }
}

#pragma mark -
#pragma mark Bridge
extern "C" 
{
    void SendSMSWithParameters(const char* p_number, const char* p_message)
    {
        NSString* number = ToNSString(p_number);
        NSString* message = ToNSString(p_message);
        [Synergy88Digital SendSMSWith:number Message:message];
    }
    
    //*
	void SendSMS()
	{
    	[Synergy88Digital SendSMSWith:nil Message:nil];
        //SendSMSWithParameters("09177136991", "This is a test message!");
    }
    //*/
}

#pragma mark -
#pragma mark Objective-C Method Calls
+ (void) SendSMSWith:(NSString*)p_number
			 Message:(NSString*)p_message
{
	NSLog(@"Synergy88Digital::SendSMS~ Number:%@ Message:%@", p_number, p_message);
    
    Synergy88DigitalVC* synergyView = [[Synergy88DigitalVC alloc] init];
    synergyView.smsNumber = p_number;
    synergyView.smsMessage = p_message;
    UIViewController* unityViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    [unityViewController presentViewController:synergyView animated:YES completion:nil];
}


@end
