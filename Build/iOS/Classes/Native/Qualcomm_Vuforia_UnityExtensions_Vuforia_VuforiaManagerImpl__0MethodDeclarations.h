﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void TrackableResultData_t1134_marshal(const TrackableResultData_t1134& unmarshaled, TrackableResultData_t1134_marshaled& marshaled);
extern "C" void TrackableResultData_t1134_marshal_back(const TrackableResultData_t1134_marshaled& marshaled, TrackableResultData_t1134& unmarshaled);
extern "C" void TrackableResultData_t1134_marshal_cleanup(TrackableResultData_t1134_marshaled& marshaled);
