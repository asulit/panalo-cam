﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.LocalDataStoreSlot
struct LocalDataStoreSlot_t2347;

// System.Void System.LocalDataStoreSlot::.ctor(System.Boolean)
extern "C" void LocalDataStoreSlot__ctor_m14192 (LocalDataStoreSlot_t2347 * __this, bool ___in_thread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.LocalDataStoreSlot::.cctor()
extern "C" void LocalDataStoreSlot__cctor_m14193 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.LocalDataStoreSlot::Finalize()
extern "C" void LocalDataStoreSlot_Finalize_m14194 (LocalDataStoreSlot_t2347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
