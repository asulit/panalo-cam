﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t385;
// System.IO.TextWriter
struct TextWriter_t1761;
// System.Object
#include "mscorlib_System_Object.h"
// System.IO.TextWriter
struct  TextWriter_t1761  : public Object_t
{
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t385* ___CoreNewLine_0;
};
struct TextWriter_t1761_StaticFields{
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t1761 * ___Null_1;
};
