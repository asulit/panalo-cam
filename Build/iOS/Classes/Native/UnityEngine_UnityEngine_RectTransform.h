﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t769;
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.RectTransform
struct  RectTransform_t556  : public Transform_t35
{
};
struct RectTransform_t556_StaticFields{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t769 * ___reapplyDrivenProperties_2;
};
