﻿#pragma once
#include <stdint.h>
// Common.Signal.Signal/SignalListener
struct SignalListener_t114;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Common.Signal.Signal/SignalListener>
struct  Comparison_1_t2607  : public MulticastDelegate_t28
{
};
