﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_86.h"
// Vuforia.VuforiaManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26029_gshared (InternalEnumerator_1_t3315 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26029(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3315 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26029_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26030_gshared (InternalEnumerator_1_t3315 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26030(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3315 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26030_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26031_gshared (InternalEnumerator_1_t3315 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26031(__this, method) (( void (*) (InternalEnumerator_1_t3315 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26031_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26032_gshared (InternalEnumerator_1_t3315 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26032(__this, method) (( bool (*) (InternalEnumerator_1_t3315 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26032_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" TrackableResultData_t1134  InternalEnumerator_1_get_Current_m26033_gshared (InternalEnumerator_1_t3315 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26033(__this, method) (( TrackableResultData_t1134  (*) (InternalEnumerator_1_t3315 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26033_gshared)(__this, method)
