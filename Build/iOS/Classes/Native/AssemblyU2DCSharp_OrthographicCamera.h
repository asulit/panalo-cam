﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t6;
// System.Collections.Generic.IList`1<OrthographicCameraObserver>
struct IList_1_t7;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// OrthographicCamera
struct  OrthographicCamera_t4  : public MonoBehaviour_t5
{
	// UnityEngine.Camera OrthographicCamera::selfCamera
	Camera_t6 * ___selfCamera_2;
	// System.Collections.Generic.IList`1<OrthographicCameraObserver> OrthographicCamera::observerList
	Object_t* ___observerList_3;
};
