﻿#pragma once
#include <stdint.h>
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerator`1<System.Enum>
struct IEnumerator_1_t22;
// System.Enum
struct Enum_t21;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0
struct  U3CGetFlagValuesU3Ec__Iterator0_t20  : public Object_t
{
	// System.UInt64 Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::<flag>__0
	uint64_t ___U3CflagU3E__0_0;
	// System.Type Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::p_enumType
	Type_t * ___p_enumType_1;
	// System.Collections.Generic.IEnumerator`1<System.Enum> Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::<$s_14>__1
	Object_t* ___U3CU24s_14U3E__1_2;
	// System.Enum Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::<value>__2
	Enum_t21 * ___U3CvalueU3E__2_3;
	// System.UInt64 Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::<bits>__3
	uint64_t ___U3CbitsU3E__3_4;
	// System.Int32 Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::$PC
	int32_t ___U24PC_5;
	// System.Enum Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::$current
	Enum_t21 * ___U24current_6;
	// System.Type Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::<$>p_enumType
	Type_t * ___U3CU24U3Ep_enumType_7;
};
