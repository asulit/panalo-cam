﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.AsymmetricSignatureFormatter
struct AsymmetricSignatureFormatter_t1518;

// System.Void System.Security.Cryptography.AsymmetricSignatureFormatter::.ctor()
extern "C" void AsymmetricSignatureFormatter__ctor_m8553 (AsymmetricSignatureFormatter_t1518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
