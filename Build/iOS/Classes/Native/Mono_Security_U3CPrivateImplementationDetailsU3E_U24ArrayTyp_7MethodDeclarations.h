﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU244_t1558_marshal(const U24ArrayTypeU244_t1558& unmarshaled, U24ArrayTypeU244_t1558_marshaled& marshaled);
extern "C" void U24ArrayTypeU244_t1558_marshal_back(const U24ArrayTypeU244_t1558_marshaled& marshaled, U24ArrayTypeU244_t1558& unmarshaled);
extern "C" void U24ArrayTypeU244_t1558_marshal_cleanup(U24ArrayTypeU244_t1558_marshaled& marshaled);
