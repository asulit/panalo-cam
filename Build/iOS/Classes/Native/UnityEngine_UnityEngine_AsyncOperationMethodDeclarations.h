﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AsyncOperation
struct AsyncOperation_t783;
struct AsyncOperation_t783_marshaled;

// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C" void AsyncOperation__ctor_m4241 (AsyncOperation_t783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C" void AsyncOperation_InternalDestroy_m4242 (AsyncOperation_t783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C" void AsyncOperation_Finalize_m4243 (AsyncOperation_t783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void AsyncOperation_t783_marshal(const AsyncOperation_t783& unmarshaled, AsyncOperation_t783_marshaled& marshaled);
extern "C" void AsyncOperation_t783_marshal_back(const AsyncOperation_t783_marshaled& marshaled, AsyncOperation_t783& unmarshaled);
extern "C" void AsyncOperation_t783_marshal_cleanup(AsyncOperation_t783_marshaled& marshaled);
