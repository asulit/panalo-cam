﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName
struct StrongName_t2246;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Security.Policy.StrongName>
struct  Predicate_1_t3598  : public MulticastDelegate_t28
{
};
