﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Vuforia.Image
struct Image_t1109;
// Vuforia.RectangleData[]
struct RectangleDataU5BU5D_t1178;
// Vuforia.TrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableImpl.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Vuforia.VuforiaManagerImpl/ImageHeaderData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__6.h"
// Vuforia.WordImpl
struct  WordImpl_t1177  : public TrackableImpl_t1071
{
	// System.String Vuforia.WordImpl::mText
	String_t* ___mText_2;
	// UnityEngine.Vector2 Vuforia.WordImpl::mSize
	Vector2_t2  ___mSize_3;
	// Vuforia.Image Vuforia.WordImpl::mLetterMask
	Image_t1109 * ___mLetterMask_4;
	// Vuforia.VuforiaManagerImpl/ImageHeaderData Vuforia.WordImpl::mLetterImageHeader
	ImageHeaderData_t1140  ___mLetterImageHeader_5;
	// Vuforia.RectangleData[] Vuforia.WordImpl::mLetterBoundingBoxes
	RectangleDataU5BU5D_t1178* ___mLetterBoundingBoxes_6;
};
