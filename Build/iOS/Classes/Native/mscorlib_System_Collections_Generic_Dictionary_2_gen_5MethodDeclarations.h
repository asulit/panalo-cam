﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_41MethodDeclarations.h"
#define Dictionary_2__ctor_m1541(__this, method) (( void (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2__ctor_m15007_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m16561(__this, ___comparer, method) (( void (*) (Dictionary_2_t134 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15009_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::.ctor(System.Int32)
#define Dictionary_2__ctor_m16562(__this, ___capacity, method) (( void (*) (Dictionary_2_t134 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m15011_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m16563(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t134 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m15013_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m16564(__this, method) (( Object_t * (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m15015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16565(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t134 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m15017_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16566(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t134 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m15019_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m16567(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t134 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m15021_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m16568(__this, ___key, method) (( bool (*) (Dictionary_2_t134 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m15023_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m16569(__this, ___key, method) (( void (*) (Dictionary_2_t134 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m15025_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16570(__this, method) (( bool (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15027_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16571(__this, method) (( Object_t * (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15029_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16572(__this, method) (( bool (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15031_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16573(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t134 *, KeyValuePair_2_t2628 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15033_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16574(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t134 *, KeyValuePair_2_t2628 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15035_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16575(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t134 *, KeyValuePair_2U5BU5D_t3642*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15037_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16576(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t134 *, KeyValuePair_2_t2628 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15039_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16577(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t134 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m15041_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16578(__this, method) (( Object_t * (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15043_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16579(__this, method) (( Object_t* (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15045_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Type,System.Object>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16580(__this, method) (( Object_t * (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15047_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Type,System.Object>::get_Count()
#define Dictionary_2_get_Count_m16581(__this, method) (( int32_t (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_get_Count_m15049_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.Object>::get_Item(TKey)
#define Dictionary_2_get_Item_m16582(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t134 *, Type_t *, const MethodInfo*))Dictionary_2_get_Item_m15051_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m16583(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t134 *, Type_t *, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m15053_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m16584(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t134 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m15055_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m16585(__this, ___size, method) (( void (*) (Dictionary_2_t134 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m15057_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m16586(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t134 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m15059_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Object>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m16587(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2628  (*) (Object_t * /* static, unused */, Type_t *, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m15061_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,System.Object>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m16588(__this /* static, unused */, ___key, ___value, method) (( Type_t * (*) (Object_t * /* static, unused */, Type_t *, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m15063_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.Object>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m16589(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Type_t *, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m15065_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m16590(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t134 *, KeyValuePair_2U5BU5D_t3642*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m15067_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::Resize()
#define Dictionary_2_Resize_m16591(__this, method) (( void (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_Resize_m15069_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::Add(TKey,TValue)
#define Dictionary_2_Add_m16592(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t134 *, Type_t *, Object_t *, const MethodInfo*))Dictionary_2_Add_m15071_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::Clear()
#define Dictionary_2_Clear_m16593(__this, method) (( void (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_Clear_m15073_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Object>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m16594(__this, ___key, method) (( bool (*) (Dictionary_2_t134 *, Type_t *, const MethodInfo*))Dictionary_2_ContainsKey_m15075_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Object>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m16595(__this, ___value, method) (( bool (*) (Dictionary_2_t134 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m15077_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m16596(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t134 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m15079_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m16597(__this, ___sender, method) (( void (*) (Dictionary_2_t134 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m15081_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Object>::Remove(TKey)
#define Dictionary_2_Remove_m16598(__this, ___key, method) (( bool (*) (Dictionary_2_t134 *, Type_t *, const MethodInfo*))Dictionary_2_Remove_m15083_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Object>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m16599(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t134 *, Type_t *, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m15085_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Object>::get_Keys()
#define Dictionary_2_get_Keys_m16600(__this, method) (( KeyCollection_t2629 * (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_get_Keys_m15087_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Object>::get_Values()
#define Dictionary_2_get_Values_m16601(__this, method) (( ValueCollection_t2630 * (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_get_Values_m15089_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,System.Object>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m16602(__this, ___key, method) (( Type_t * (*) (Dictionary_2_t134 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m15091_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.Object>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m16603(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t134 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m15093_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m16604(__this, ___pair, method) (( bool (*) (Dictionary_2_t134 *, KeyValuePair_2_t2628 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m15095_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Object>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m16605(__this, method) (( Enumerator_t2631  (*) (Dictionary_2_t134 *, const MethodInfo*))Dictionary_2_GetEnumerator_m15097_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Type,System.Object>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m16606(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, Type_t *, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m15099_gshared)(__this /* static, unused */, ___key, ___value, method)
