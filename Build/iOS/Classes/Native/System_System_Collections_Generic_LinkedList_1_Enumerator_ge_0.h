﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>
struct LinkedList_1_t90;
// System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>
struct LinkedListNode_1_t406;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.LinkedList`1/Enumerator<Common.Notification.NotificationHandler>
struct  Enumerator_t2586 
{
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedList`1/Enumerator<Common.Notification.NotificationHandler>::list
	LinkedList_1_t90 * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1/Enumerator<Common.Notification.NotificationHandler>::current
	LinkedListNode_1_t406 * ___current_1;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator<Common.Notification.NotificationHandler>::index
	int32_t ___index_2;
	// System.UInt32 System.Collections.Generic.LinkedList`1/Enumerator<Common.Notification.NotificationHandler>::version
	uint32_t ___version_3;
};
