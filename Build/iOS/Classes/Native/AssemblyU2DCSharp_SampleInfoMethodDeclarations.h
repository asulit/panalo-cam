﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SampleInfo
struct SampleInfo_t248;

// System.Void SampleInfo::.ctor()
extern "C" void SampleInfo__ctor_m892 (SampleInfo_t248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SampleInfo::OnGUI()
extern "C" void SampleInfo_OnGUI_m893 (SampleInfo_t248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
