﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t2462;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m14603_gshared (GenericEqualityComparer_1_t2462 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m14603(__this, method) (( void (*) (GenericEqualityComparer_1_t2462 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m14603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m29988_gshared (GenericEqualityComparer_1_t2462 * __this, DateTimeOffset_t2323  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m29988(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2462 *, DateTimeOffset_t2323 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m29988_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m29989_gshared (GenericEqualityComparer_1_t2462 * __this, DateTimeOffset_t2323  ___x, DateTimeOffset_t2323  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m29989(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2462 *, DateTimeOffset_t2323 , DateTimeOffset_t2323 , const MethodInfo*))GenericEqualityComparer_1_Equals_m29989_gshared)(__this, ___x, ___y, method)
