﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.InputField
struct InputField_t226;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.Boolean>
struct  KeyValuePair_2_t2755 
{
	// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.Boolean>::key
	InputField_t226 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.Boolean>::value
	bool ___value_1;
};
