﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ShimEnumerator_t3489;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3476;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m28825_gshared (ShimEnumerator_t3489 * __this, Dictionary_2_t3476 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m28825(__this, ___host, method) (( void (*) (ShimEnumerator_t3489 *, Dictionary_2_t3476 *, const MethodInfo*))ShimEnumerator__ctor_m28825_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m28826_gshared (ShimEnumerator_t3489 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m28826(__this, method) (( bool (*) (ShimEnumerator_t3489 *, const MethodInfo*))ShimEnumerator_MoveNext_m28826_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m28827_gshared (ShimEnumerator_t3489 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m28827(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t3489 *, const MethodInfo*))ShimEnumerator_get_Entry_m28827_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m28828_gshared (ShimEnumerator_t3489 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m28828(__this, method) (( Object_t * (*) (ShimEnumerator_t3489 *, const MethodInfo*))ShimEnumerator_get_Key_m28828_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m28829_gshared (ShimEnumerator_t3489 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m28829(__this, method) (( Object_t * (*) (ShimEnumerator_t3489 *, const MethodInfo*))ShimEnumerator_get_Value_m28829_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m28830_gshared (ShimEnumerator_t3489 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m28830(__this, method) (( Object_t * (*) (ShimEnumerator_t3489 *, const MethodInfo*))ShimEnumerator_get_Current_m28830_gshared)(__this, method)
