﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Raffle/<RegisterPlayerToPromo>c__AnonStorey19
struct U3CRegisterPlayerToPromoU3Ec__AnonStorey19_t204;
// UnityEngine.WWW
struct WWW_t199;

// System.Void Raffle/<RegisterPlayerToPromo>c__AnonStorey19::.ctor()
extern "C" void U3CRegisterPlayerToPromoU3Ec__AnonStorey19__ctor_m708 (U3CRegisterPlayerToPromoU3Ec__AnonStorey19_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle/<RegisterPlayerToPromo>c__AnonStorey19::<>m__13(UnityEngine.WWW)
extern "C" void U3CRegisterPlayerToPromoU3Ec__AnonStorey19_U3CU3Em__13_m709 (U3CRegisterPlayerToPromoU3Ec__AnonStorey19_t204 * __this, WWW_t199 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
