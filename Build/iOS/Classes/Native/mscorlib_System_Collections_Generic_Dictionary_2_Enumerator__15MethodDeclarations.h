﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14MethodDeclarations.h"
#define Enumerator__ctor_m17314(__this, ___dictionary, method) (( void (*) (Enumerator_t2684 *, Dictionary_2_t191 *, const MethodInfo*))Enumerator__ctor_m17215_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17315(__this, method) (( Object_t * (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17216_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17316(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17217_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17317(__this, method) (( Object_t * (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17218_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17318(__this, method) (( Object_t * (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17219_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::MoveNext()
#define Enumerator_MoveNext_m17319(__this, method) (( bool (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_MoveNext_m17220_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::get_Current()
#define Enumerator_get_Current_m17320(__this, method) (( KeyValuePair_2_t2681  (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_get_Current_m17221_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17321(__this, method) (( int32_t (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_get_CurrentKey_m17222_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17322(__this, method) (( String_t* (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_get_CurrentValue_m17223_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::VerifyState()
#define Enumerator_VerifyState_m17323(__this, method) (( void (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_VerifyState_m17224_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17324(__this, method) (( void (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_VerifyCurrent_m17225_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::Dispose()
#define Enumerator_Dispose_m17325(__this, method) (( void (*) (Enumerator_t2684 *, const MethodInfo*))Enumerator_Dispose_m17226_gshared)(__this, method)
