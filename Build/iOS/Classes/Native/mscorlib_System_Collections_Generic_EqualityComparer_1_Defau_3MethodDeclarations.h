﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ERaffleResult>
struct DefaultComparer_t2722;
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ERaffleResult>::.ctor()
extern "C" void DefaultComparer__ctor_m17699_gshared (DefaultComparer_t2722 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m17699(__this, method) (( void (*) (DefaultComparer_t2722 *, const MethodInfo*))DefaultComparer__ctor_m17699_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ERaffleResult>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m17700_gshared (DefaultComparer_t2722 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m17700(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2722 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m17700_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ERaffleResult>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m17701_gshared (DefaultComparer_t2722 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m17701(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2722 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m17701_gshared)(__this, ___x, ___y, method)
