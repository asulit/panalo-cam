﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t403;
// UnityEngine.Transform[]
struct TransformU5BU5D_t354;
// UnityEngine.Mesh
struct Mesh_t104;

// System.Void UnityEngine.SkinnedMeshRenderer::set_bones(UnityEngine.Transform[])
extern "C" void SkinnedMeshRenderer_set_bones_m1485 (SkinnedMeshRenderer_t403 * __this, TransformU5BU5D_t354* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SkinnedMeshRenderer::set_sharedMesh(UnityEngine.Mesh)
extern "C" void SkinnedMeshRenderer_set_sharedMesh_m1484 (SkinnedMeshRenderer_t403 * __this, Mesh_t104 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
