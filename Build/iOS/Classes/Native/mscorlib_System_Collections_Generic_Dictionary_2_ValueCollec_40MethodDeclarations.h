﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>
struct ValueCollection_t2674;
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
struct Dictionary_2_t2665;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_41.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m17231_gshared (ValueCollection_t2674 * __this, Dictionary_2_t2665 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m17231(__this, ___dictionary, method) (( void (*) (ValueCollection_t2674 *, Dictionary_2_t2665 *, const MethodInfo*))ValueCollection__ctor_m17231_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17232_gshared (ValueCollection_t2674 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17232(__this, ___item, method) (( void (*) (ValueCollection_t2674 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17232_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17233_gshared (ValueCollection_t2674 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17233(__this, method) (( void (*) (ValueCollection_t2674 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17233_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17234_gshared (ValueCollection_t2674 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17234(__this, ___item, method) (( bool (*) (ValueCollection_t2674 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17234_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17235_gshared (ValueCollection_t2674 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17235(__this, ___item, method) (( bool (*) (ValueCollection_t2674 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17235_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17236_gshared (ValueCollection_t2674 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17236(__this, method) (( Object_t* (*) (ValueCollection_t2674 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17236_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m17237_gshared (ValueCollection_t2674 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m17237(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2674 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m17237_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17238_gshared (ValueCollection_t2674 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17238(__this, method) (( Object_t * (*) (ValueCollection_t2674 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17238_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17239_gshared (ValueCollection_t2674 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17239(__this, method) (( bool (*) (ValueCollection_t2674 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17239_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17240_gshared (ValueCollection_t2674 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17240(__this, method) (( bool (*) (ValueCollection_t2674 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17240_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m17241_gshared (ValueCollection_t2674 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m17241(__this, method) (( Object_t * (*) (ValueCollection_t2674 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m17241_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m17242_gshared (ValueCollection_t2674 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m17242(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2674 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m17242_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::GetEnumerator()
extern "C" Enumerator_t2675  ValueCollection_GetEnumerator_m17243_gshared (ValueCollection_t2674 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m17243(__this, method) (( Enumerator_t2675  (*) (ValueCollection_t2674 *, const MethodInfo*))ValueCollection_GetEnumerator_m17243_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m17244_gshared (ValueCollection_t2674 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m17244(__this, method) (( int32_t (*) (ValueCollection_t2674 *, const MethodInfo*))ValueCollection_get_Count_m17244_gshared)(__this, method)
