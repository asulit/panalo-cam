﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Signal.Signal
struct Signal_t116;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,Common.Signal.Signal,System.Collections.DictionaryEntry>
struct  Transform_1_t2610  : public MulticastDelegate_t28
{
};
