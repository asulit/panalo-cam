﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.MeshFilter
struct MeshFilter_t398;
// UnityEngine.Mesh
struct Mesh_t104;

// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C" Mesh_t104 * MeshFilter_get_mesh_m1464 (MeshFilter_t398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_mesh_m1503 (MeshFilter_t398 * __this, Mesh_t104 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C" Mesh_t104 * MeshFilter_get_sharedMesh_m1893 (MeshFilter_t398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern "C" void MeshFilter_set_sharedMesh_m3907 (MeshFilter_t398 * __this, Mesh_t104 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
