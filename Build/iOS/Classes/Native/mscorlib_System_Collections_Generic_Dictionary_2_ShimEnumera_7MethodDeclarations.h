﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct ShimEnumerator_t3268;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t3255;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m25176_gshared (ShimEnumerator_t3268 * __this, Dictionary_2_t3255 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m25176(__this, ___host, method) (( void (*) (ShimEnumerator_t3268 *, Dictionary_2_t3255 *, const MethodInfo*))ShimEnumerator__ctor_m25176_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m25177_gshared (ShimEnumerator_t3268 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m25177(__this, method) (( bool (*) (ShimEnumerator_t3268 *, const MethodInfo*))ShimEnumerator_MoveNext_m25177_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m25178_gshared (ShimEnumerator_t3268 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m25178(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t3268 *, const MethodInfo*))ShimEnumerator_get_Entry_m25178_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m25179_gshared (ShimEnumerator_t3268 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m25179(__this, method) (( Object_t * (*) (ShimEnumerator_t3268 *, const MethodInfo*))ShimEnumerator_get_Key_m25179_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m25180_gshared (ShimEnumerator_t3268 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m25180(__this, method) (( Object_t * (*) (ShimEnumerator_t3268 *, const MethodInfo*))ShimEnumerator_get_Value_m25180_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m25181_gshared (ShimEnumerator_t3268 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m25181(__this, method) (( Object_t * (*) (ShimEnumerator_t3268 *, const MethodInfo*))ShimEnumerator_get_Current_m25181_gshared)(__this, method)
