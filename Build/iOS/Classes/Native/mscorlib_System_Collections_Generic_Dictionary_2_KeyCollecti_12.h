﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>
struct Dictionary_2_t119;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Common.Signal.Signal>
struct  KeyCollection_t2612  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Common.Signal.Signal>::dictionary
	Dictionary_2_t119 * ___dictionary_0;
};
