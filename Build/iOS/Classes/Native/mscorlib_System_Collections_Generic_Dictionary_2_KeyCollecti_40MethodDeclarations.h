﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t3181;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_40.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m24184_gshared (Enumerator_t3187 * __this, Dictionary_2_t3181 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m24184(__this, ___host, method) (( void (*) (Enumerator_t3187 *, Dictionary_2_t3181 *, const MethodInfo*))Enumerator__ctor_m24184_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m24185_gshared (Enumerator_t3187 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m24185(__this, method) (( Object_t * (*) (Enumerator_t3187 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24185_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m24186_gshared (Enumerator_t3187 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m24186(__this, method) (( void (*) (Enumerator_t3187 *, const MethodInfo*))Enumerator_Dispose_m24186_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m24187_gshared (Enumerator_t3187 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m24187(__this, method) (( bool (*) (Enumerator_t3187 *, const MethodInfo*))Enumerator_MoveNext_m24187_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m24188_gshared (Enumerator_t3187 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m24188(__this, method) (( Object_t * (*) (Enumerator_t3187 *, const MethodInfo*))Enumerator_get_Current_m24188_gshared)(__this, method)
