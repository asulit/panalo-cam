﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GameSignals
struct GameSignals_t193;

// System.Void GameSignals::.ctor()
extern "C" void GameSignals__ctor_m671 (GameSignals_t193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameSignals::.cctor()
extern "C" void GameSignals__cctor_m672 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
