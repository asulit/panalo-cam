﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t675;

// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::.cctor()
extern "C" void ListPool_1__cctor_m22837_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m22837(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m22837_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Get()
extern "C" List_1_t675 * ListPool_1_Get_m3779_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m3779(__this /* static, unused */, method) (( List_1_t675 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m3779_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m3802_gshared (Object_t * __this /* static, unused */, List_1_t675 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m3802(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t675 *, const MethodInfo*))ListPool_1_Release_m3802_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::<s_ListPool>m__15(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__15_m22838_gshared (Object_t * __this /* static, unused */, List_1_t675 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__15_m22838(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t675 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__15_m22838_gshared)(__this /* static, unused */, ___l, method)
