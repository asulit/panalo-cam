﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<AnimatorFrame>
struct Predicate_1_t2777;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"

// System.Void System.Predicate`1<AnimatorFrame>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m18423_gshared (Predicate_1_t2777 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m18423(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2777 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m18423_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<AnimatorFrame>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m18424_gshared (Predicate_1_t2777 * __this, AnimatorFrame_t235  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m18424(__this, ___obj, method) (( bool (*) (Predicate_1_t2777 *, AnimatorFrame_t235 , const MethodInfo*))Predicate_1_Invoke_m18424_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<AnimatorFrame>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m18425_gshared (Predicate_1_t2777 * __this, AnimatorFrame_t235  ___obj, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m18425(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2777 *, AnimatorFrame_t235 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m18425_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<AnimatorFrame>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m18426_gshared (Predicate_1_t2777 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m18426(__this, ___result, method) (( bool (*) (Predicate_1_t2777 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m18426_gshared)(__this, ___result, method)
