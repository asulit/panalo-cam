﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Outline
struct Outline_t683;
// UnityEngine.Mesh
struct Mesh_t104;

// System.Void UnityEngine.UI.Outline::.ctor()
extern "C" void Outline__ctor_m3255 (Outline_t683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Outline::ModifyMesh(UnityEngine.Mesh)
extern "C" void Outline_ModifyMesh_m3256 (Outline_t683 * __this, Mesh_t104 * ___mesh, const MethodInfo* method) IL2CPP_METHOD_ATTR;
