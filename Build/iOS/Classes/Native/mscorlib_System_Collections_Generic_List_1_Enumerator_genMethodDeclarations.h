﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<AnimatorFrame>
struct List_1_t238;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<AnimatorFrame>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"

// System.Void System.Collections.Generic.List`1/Enumerator<AnimatorFrame>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m18345_gshared (Enumerator_t234 * __this, List_1_t238 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m18345(__this, ___l, method) (( void (*) (Enumerator_t234 *, List_1_t238 *, const MethodInfo*))Enumerator__ctor_m18345_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<AnimatorFrame>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18346_gshared (Enumerator_t234 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18346(__this, method) (( Object_t * (*) (Enumerator_t234 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18346_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AnimatorFrame>::Dispose()
extern "C" void Enumerator_Dispose_m18347_gshared (Enumerator_t234 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18347(__this, method) (( void (*) (Enumerator_t234 *, const MethodInfo*))Enumerator_Dispose_m18347_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AnimatorFrame>::VerifyState()
extern "C" void Enumerator_VerifyState_m18348_gshared (Enumerator_t234 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m18348(__this, method) (( void (*) (Enumerator_t234 *, const MethodInfo*))Enumerator_VerifyState_m18348_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<AnimatorFrame>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1701_gshared (Enumerator_t234 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1701(__this, method) (( bool (*) (Enumerator_t234 *, const MethodInfo*))Enumerator_MoveNext_m1701_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<AnimatorFrame>::get_Current()
extern "C" AnimatorFrame_t235  Enumerator_get_Current_m1699_gshared (Enumerator_t234 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1699(__this, method) (( AnimatorFrame_t235  (*) (Enumerator_t234 *, const MethodInfo*))Enumerator_get_Current_m1699_gshared)(__this, method)
