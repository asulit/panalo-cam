﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t707;
// UnityEngine.UI.CoroutineTween.FloatTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatTween.h"

// System.Single UnityEngine.UI.CoroutineTween.FloatTween::get_startValue()
extern "C" float FloatTween_get_startValue_m2204 (FloatTween_t539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::set_startValue(System.Single)
extern "C" void FloatTween_set_startValue_m2205 (FloatTween_t539 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.FloatTween::get_targetValue()
extern "C" float FloatTween_get_targetValue_m2206 (FloatTween_t539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::set_targetValue(System.Single)
extern "C" void FloatTween_set_targetValue_m2207 (FloatTween_t539 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.FloatTween::get_duration()
extern "C" float FloatTween_get_duration_m2208 (FloatTween_t539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::set_duration(System.Single)
extern "C" void FloatTween_set_duration_m2209 (FloatTween_t539 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::get_ignoreTimeScale()
extern "C" bool FloatTween_get_ignoreTimeScale_m2210 (FloatTween_t539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::set_ignoreTimeScale(System.Boolean)
extern "C" void FloatTween_set_ignoreTimeScale_m2211 (FloatTween_t539 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::TweenValue(System.Single)
extern "C" void FloatTween_TweenValue_m2212 (FloatTween_t539 * __this, float ___floatPercentage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::AddOnChangedCallback(UnityEngine.Events.UnityAction`1<System.Single>)
extern "C" void FloatTween_AddOnChangedCallback_m2213 (FloatTween_t539 * __this, UnityAction_1_t707 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::GetIgnoreTimescale()
extern "C" bool FloatTween_GetIgnoreTimescale_m2214 (FloatTween_t539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.FloatTween::GetDuration()
extern "C" float FloatTween_GetDuration_m2215 (FloatTween_t539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::ValidTarget()
extern "C" bool FloatTween_ValidTarget_m2216 (FloatTween_t539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
