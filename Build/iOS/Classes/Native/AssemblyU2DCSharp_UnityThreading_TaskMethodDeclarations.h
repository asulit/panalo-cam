﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.Task
struct Task_t165;
// System.Action
struct Action_t16;
// System.Collections.IEnumerator
struct IEnumerator_t337;

// System.Void UnityThreading.Task::.ctor(System.Action)
extern "C" void Task__ctor_m562 (Task_t165 * __this, Action_t16 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityThreading.Task::Do()
extern "C" Object_t * Task_Do_m563 (Task_t165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
