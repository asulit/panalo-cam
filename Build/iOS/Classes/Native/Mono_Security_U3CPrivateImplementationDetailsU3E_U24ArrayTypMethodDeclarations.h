﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU243132_t1550_marshal(const U24ArrayTypeU243132_t1550& unmarshaled, U24ArrayTypeU243132_t1550_marshaled& marshaled);
extern "C" void U24ArrayTypeU243132_t1550_marshal_back(const U24ArrayTypeU243132_t1550_marshaled& marshaled, U24ArrayTypeU243132_t1550& unmarshaled);
extern "C" void U24ArrayTypeU243132_t1550_marshal_cleanup(U24ArrayTypeU243132_t1550_marshaled& marshaled);
