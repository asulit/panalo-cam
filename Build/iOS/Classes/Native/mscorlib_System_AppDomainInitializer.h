﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t137;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.AppDomainInitializer
struct  AppDomainInitializer_t2313  : public MulticastDelegate_t28
{
};
