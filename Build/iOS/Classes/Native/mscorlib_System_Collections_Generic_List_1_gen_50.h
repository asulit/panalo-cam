﻿#pragma once
#include <stdint.h>
// Vuforia.ITrackableEventHandler[]
struct ITrackableEventHandlerU5BU5D_t3230;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct  List_1_t1059  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::_items
	ITrackableEventHandlerU5BU5D_t3230* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t1059_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::EmptyArray
	ITrackableEventHandlerU5BU5D_t3230* ___EmptyArray_4;
};
