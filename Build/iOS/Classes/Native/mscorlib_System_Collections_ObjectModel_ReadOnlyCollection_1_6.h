﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Common.Xml.SimpleXmlNode>
struct IList_1_t2638;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Common.Xml.SimpleXmlNode>
struct  ReadOnlyCollection_1_t2637  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Common.Xml.SimpleXmlNode>::list
	Object_t* ___list_0;
};
