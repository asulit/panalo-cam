﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.VuforiaManagerImpl/ImageHeaderData
#pragma pack(push, tp, 1)
struct  ImageHeaderData_t1140 
{
	// System.IntPtr Vuforia.VuforiaManagerImpl/ImageHeaderData::data
	IntPtr_t ___data_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::width
	int32_t ___width_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::height
	int32_t ___height_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::stride
	int32_t ___stride_3;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::bufferWidth
	int32_t ___bufferWidth_4;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::bufferHeight
	int32_t ___bufferHeight_5;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::format
	int32_t ___format_6;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::reallocate
	int32_t ___reallocate_7;
	// System.Int32 Vuforia.VuforiaManagerImpl/ImageHeaderData::updated
	int32_t ___updated_8;
};
#pragma pack(pop, tp)
