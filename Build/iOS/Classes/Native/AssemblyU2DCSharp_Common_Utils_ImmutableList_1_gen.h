﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.Object>
struct List_1_t344;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Utils.ImmutableList`1<System.Object>
struct  ImmutableList_1_t2632  : public Object_t
{
	// System.Collections.Generic.List`1<T> Common.Utils.ImmutableList`1<System.Object>::list
	List_1_t344 * ___list_0;
};
