﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// iTween
struct iTween_t258;
// System.Object
#include "mscorlib_System_Object.h"
// iTween/<Start>c__IteratorE
struct  U3CStartU3Ec__IteratorE_t260  : public Object_t
{
	// System.Int32 iTween/<Start>c__IteratorE::$PC
	int32_t ___U24PC_0;
	// System.Object iTween/<Start>c__IteratorE::$current
	Object_t * ___U24current_1;
	// iTween iTween/<Start>c__IteratorE::<>f__this
	iTween_t258 * ___U3CU3Ef__this_2;
};
