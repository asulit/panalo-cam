﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.RectMask2D
struct RectMask2D_t609;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.RectMask2D>
struct  Predicate_1_t2977  : public MulticastDelegate_t28
{
};
