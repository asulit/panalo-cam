﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.DefaultUriParser
struct DefaultUriParser_t1737;
// System.String
struct String_t;

// System.Void System.DefaultUriParser::.ctor()
extern "C" void DefaultUriParser__ctor_m9392 (DefaultUriParser_t1737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DefaultUriParser::.ctor(System.String)
extern "C" void DefaultUriParser__ctor_m9393 (DefaultUriParser_t1737 * __this, String_t* ___scheme, const MethodInfo* method) IL2CPP_METHOD_ATTR;
