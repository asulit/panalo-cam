﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22MethodDeclarations.h"
#define Enumerator__ctor_m27534(__this, ___dictionary, method) (( void (*) (Enumerator_t3401 *, Dictionary_2_t1201 *, const MethodInfo*))Enumerator__ctor_m19836_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27535(__this, method) (( Object_t * (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19837_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27536(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19838_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27537(__this, method) (( Object_t * (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19839_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27538(__this, method) (( Object_t * (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19840_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m27539(__this, method) (( bool (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_MoveNext_m19841_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m27540(__this, method) (( KeyValuePair_2_t3399  (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_get_Current_m19842_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m27541(__this, method) (( int32_t (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_get_CurrentKey_m19843_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m27542(__this, method) (( SurfaceAbstractBehaviour_t306 * (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_get_CurrentValue_m19844_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m27543(__this, method) (( void (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_VerifyState_m19845_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m27544(__this, method) (( void (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_VerifyCurrent_m19846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m27545(__this, method) (( void (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_Dispose_m19847_gshared)(__this, method)
