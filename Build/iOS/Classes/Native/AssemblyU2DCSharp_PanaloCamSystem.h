﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t196;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// PanaloCamSystem
struct  PanaloCamSystem_t195  : public MonoBehaviour_t5
{
	// System.String PanaloCamSystem::version
	String_t* ___version_2;
	// UnityEngine.UI.Text PanaloCamSystem::buildText
	Text_t196 * ___buildText_3;
};
