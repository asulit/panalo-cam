﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void DSAParameters_t1561_marshal(const DSAParameters_t1561& unmarshaled, DSAParameters_t1561_marshaled& marshaled);
extern "C" void DSAParameters_t1561_marshal_back(const DSAParameters_t1561_marshaled& marshaled, DSAParameters_t1561& unmarshaled);
extern "C" void DSAParameters_t1561_marshal_cleanup(DSAParameters_t1561_marshaled& marshaled);
