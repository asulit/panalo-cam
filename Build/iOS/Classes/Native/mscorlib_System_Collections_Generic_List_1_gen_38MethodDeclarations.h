﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t677;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t3716;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t3717;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector2>
struct ICollection_1_t3718;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>
struct ReadOnlyCollection_1_t3050;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t262;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t3055;
// System.Comparison`1<UnityEngine.Vector2>
struct Comparison_1_t3058;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_63.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
extern "C" void List_1__ctor_m22409_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1__ctor_m22409(__this, method) (( void (*) (List_1_t677 *, const MethodInfo*))List_1__ctor_m22409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m22410_gshared (List_1_t677 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m22410(__this, ___collection, method) (( void (*) (List_1_t677 *, Object_t*, const MethodInfo*))List_1__ctor_m22410_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor(System.Int32)
extern "C" void List_1__ctor_m22411_gshared (List_1_t677 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m22411(__this, ___capacity, method) (( void (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1__ctor_m22411_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.cctor()
extern "C" void List_1__cctor_m22412_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m22412(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m22412_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22413_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22413(__this, method) (( Object_t* (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22414_gshared (List_1_t677 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m22414(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t677 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m22414_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m22415_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22415(__this, method) (( Object_t * (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m22415_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m22416_gshared (List_1_t677 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m22416(__this, ___item, method) (( int32_t (*) (List_1_t677 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m22416_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m22417_gshared (List_1_t677 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m22417(__this, ___item, method) (( bool (*) (List_1_t677 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m22417_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m22418_gshared (List_1_t677 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m22418(__this, ___item, method) (( int32_t (*) (List_1_t677 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m22418_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m22419_gshared (List_1_t677 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m22419(__this, ___index, ___item, method) (( void (*) (List_1_t677 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m22419_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m22420_gshared (List_1_t677 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m22420(__this, ___item, method) (( void (*) (List_1_t677 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m22420_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22421_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22421(__this, method) (( bool (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22421_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m22422_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22422(__this, method) (( bool (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m22422_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m22423_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m22423(__this, method) (( Object_t * (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m22423_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m22424_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m22424(__this, method) (( bool (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m22424_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m22425_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m22425(__this, method) (( bool (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m22425_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m22426_gshared (List_1_t677 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m22426(__this, ___index, method) (( Object_t * (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m22426_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m22427_gshared (List_1_t677 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m22427(__this, ___index, ___value, method) (( void (*) (List_1_t677 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m22427_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(T)
extern "C" void List_1_Add_m22428_gshared (List_1_t677 * __this, Vector2_t2  ___item, const MethodInfo* method);
#define List_1_Add_m22428(__this, ___item, method) (( void (*) (List_1_t677 *, Vector2_t2 , const MethodInfo*))List_1_Add_m22428_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m22429_gshared (List_1_t677 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m22429(__this, ___newCount, method) (( void (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m22429_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m22430_gshared (List_1_t677 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m22430(__this, ___idx, ___count, method) (( void (*) (List_1_t677 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m22430_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m22431_gshared (List_1_t677 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m22431(__this, ___collection, method) (( void (*) (List_1_t677 *, Object_t*, const MethodInfo*))List_1_AddCollection_m22431_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m22432_gshared (List_1_t677 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m22432(__this, ___enumerable, method) (( void (*) (List_1_t677 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m22432_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m3787_gshared (List_1_t677 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m3787(__this, ___collection, method) (( void (*) (List_1_t677 *, Object_t*, const MethodInfo*))List_1_AddRange_m3787_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3050 * List_1_AsReadOnly_m22433_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m22433(__this, method) (( ReadOnlyCollection_1_t3050 * (*) (List_1_t677 *, const MethodInfo*))List_1_AsReadOnly_m22433_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear()
extern "C" void List_1_Clear_m22434_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_Clear_m22434(__this, method) (( void (*) (List_1_t677 *, const MethodInfo*))List_1_Clear_m22434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Contains(T)
extern "C" bool List_1_Contains_m22435_gshared (List_1_t677 * __this, Vector2_t2  ___item, const MethodInfo* method);
#define List_1_Contains_m22435(__this, ___item, method) (( bool (*) (List_1_t677 *, Vector2_t2 , const MethodInfo*))List_1_Contains_m22435_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m22436_gshared (List_1_t677 * __this, Vector2U5BU5D_t262* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m22436(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t677 *, Vector2U5BU5D_t262*, int32_t, const MethodInfo*))List_1_CopyTo_m22436_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::Find(System.Predicate`1<T>)
extern "C" Vector2_t2  List_1_Find_m22437_gshared (List_1_t677 * __this, Predicate_1_t3055 * ___match, const MethodInfo* method);
#define List_1_Find_m22437(__this, ___match, method) (( Vector2_t2  (*) (List_1_t677 *, Predicate_1_t3055 *, const MethodInfo*))List_1_Find_m22437_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m22438_gshared (Object_t * __this /* static, unused */, Predicate_1_t3055 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m22438(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3055 *, const MethodInfo*))List_1_CheckMatch_m22438_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m22439_gshared (List_1_t677 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3055 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m22439(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t677 *, int32_t, int32_t, Predicate_1_t3055 *, const MethodInfo*))List_1_GetIndex_m22439_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector2>::GetEnumerator()
extern "C" Enumerator_t3049  List_1_GetEnumerator_m22440_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m22440(__this, method) (( Enumerator_t3049  (*) (List_1_t677 *, const MethodInfo*))List_1_GetEnumerator_m22440_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m22441_gshared (List_1_t677 * __this, Vector2_t2  ___item, const MethodInfo* method);
#define List_1_IndexOf_m22441(__this, ___item, method) (( int32_t (*) (List_1_t677 *, Vector2_t2 , const MethodInfo*))List_1_IndexOf_m22441_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m22442_gshared (List_1_t677 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m22442(__this, ___start, ___delta, method) (( void (*) (List_1_t677 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m22442_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m22443_gshared (List_1_t677 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m22443(__this, ___index, method) (( void (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_CheckIndex_m22443_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m22444_gshared (List_1_t677 * __this, int32_t ___index, Vector2_t2  ___item, const MethodInfo* method);
#define List_1_Insert_m22444(__this, ___index, ___item, method) (( void (*) (List_1_t677 *, int32_t, Vector2_t2 , const MethodInfo*))List_1_Insert_m22444_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m22445_gshared (List_1_t677 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m22445(__this, ___collection, method) (( void (*) (List_1_t677 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m22445_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector2>::Remove(T)
extern "C" bool List_1_Remove_m22446_gshared (List_1_t677 * __this, Vector2_t2  ___item, const MethodInfo* method);
#define List_1_Remove_m22446(__this, ___item, method) (( bool (*) (List_1_t677 *, Vector2_t2 , const MethodInfo*))List_1_Remove_m22446_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m22447_gshared (List_1_t677 * __this, Predicate_1_t3055 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m22447(__this, ___match, method) (( int32_t (*) (List_1_t677 *, Predicate_1_t3055 *, const MethodInfo*))List_1_RemoveAll_m22447_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m22448_gshared (List_1_t677 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m22448(__this, ___index, method) (( void (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_RemoveAt_m22448_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m22449_gshared (List_1_t677 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m22449(__this, ___index, ___count, method) (( void (*) (List_1_t677 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m22449_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Reverse()
extern "C" void List_1_Reverse_m22450_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_Reverse_m22450(__this, method) (( void (*) (List_1_t677 *, const MethodInfo*))List_1_Reverse_m22450_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort()
extern "C" void List_1_Sort_m22451_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_Sort_m22451(__this, method) (( void (*) (List_1_t677 *, const MethodInfo*))List_1_Sort_m22451_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m22452_gshared (List_1_t677 * __this, Comparison_1_t3058 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m22452(__this, ___comparison, method) (( void (*) (List_1_t677 *, Comparison_1_t3058 *, const MethodInfo*))List_1_Sort_m22452_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
extern "C" Vector2U5BU5D_t262* List_1_ToArray_m22453_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_ToArray_m22453(__this, method) (( Vector2U5BU5D_t262* (*) (List_1_t677 *, const MethodInfo*))List_1_ToArray_m22453_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::TrimExcess()
extern "C" void List_1_TrimExcess_m22454_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m22454(__this, method) (( void (*) (List_1_t677 *, const MethodInfo*))List_1_TrimExcess_m22454_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m22455_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m22455(__this, method) (( int32_t (*) (List_1_t677 *, const MethodInfo*))List_1_get_Capacity_m22455_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m22456_gshared (List_1_t677 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m22456(__this, ___value, method) (( void (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_set_Capacity_m22456_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Count()
extern "C" int32_t List_1_get_Count_m22457_gshared (List_1_t677 * __this, const MethodInfo* method);
#define List_1_get_Count_m22457(__this, method) (( int32_t (*) (List_1_t677 *, const MethodInfo*))List_1_get_Count_m22457_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C" Vector2_t2  List_1_get_Item_m22458_gshared (List_1_t677 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m22458(__this, ___index, method) (( Vector2_t2  (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_get_Item_m22458_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m22459_gshared (List_1_t677 * __this, int32_t ___index, Vector2_t2  ___value, const MethodInfo* method);
#define List_1_set_Item_m22459(__this, ___index, ___value, method) (( void (*) (List_1_t677 *, int32_t, Vector2_t2 , const MethodInfo*))List_1_set_Item_m22459_gshared)(__this, ___index, ___value, method)
