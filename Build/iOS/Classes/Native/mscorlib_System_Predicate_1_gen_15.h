﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"
// System.Predicate`1<AnimatorFrame>
struct  Predicate_1_t2777  : public MulticastDelegate_t28
{
};
