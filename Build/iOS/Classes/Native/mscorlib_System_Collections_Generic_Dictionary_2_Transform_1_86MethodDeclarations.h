﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
struct Transform_1_t3488;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_48.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m28821_gshared (Transform_1_t3488 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m28821(__this, ___object, ___method, method) (( void (*) (Transform_1_t3488 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m28821_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t3478  Transform_1_Invoke_m28822_gshared (Transform_1_t3488 * __this, Object_t * ___key, ProfileData_t1226  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m28822(__this, ___key, ___value, method) (( KeyValuePair_2_t3478  (*) (Transform_1_t3488 *, Object_t *, ProfileData_t1226 , const MethodInfo*))Transform_1_Invoke_m28822_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m28823_gshared (Transform_1_t3488 * __this, Object_t * ___key, ProfileData_t1226  ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m28823(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3488 *, Object_t *, ProfileData_t1226 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m28823_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t3478  Transform_1_EndInvoke_m28824_gshared (Transform_1_t3488 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m28824(__this, ___result, method) (( KeyValuePair_2_t3478  (*) (Transform_1_t3488 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m28824_gshared)(__this, ___result, method)
