﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Utils.SimpleList`1<System.Object>
struct  SimpleList_1_t2598  : public Object_t
{
	// T[] Common.Utils.SimpleList`1<System.Object>::buffer
	ObjectU5BU5D_t356* ___buffer_1;
	// System.Int32 Common.Utils.SimpleList`1<System.Object>::size
	int32_t ___size_2;
};
