﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Window
struct Window_t223;
// System.String
struct String_t;
// Handle
struct Handle_t221;

// System.Void Window::.ctor()
extern "C" void Window__ctor_m863 (Window_t223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Window::Awake()
extern "C" void Window_Awake_m864 (Window_t223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Window::OnEnable()
extern "C" void Window_OnEnable_m865 (Window_t223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Window::OnClickedButton()
extern "C" void Window_OnClickedButton_m866 (Window_t223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Window::HandleMessage(System.String,Handle)
extern "C" void Window_HandleMessage_m867 (Window_t223 * __this, String_t* ___message, Handle_t221 * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
