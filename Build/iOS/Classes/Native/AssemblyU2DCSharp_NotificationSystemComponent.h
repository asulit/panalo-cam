﻿#pragma once
#include <stdint.h>
// NotificationHandlerComponent[]
struct NotificationHandlerComponentU5BU5D_t92;
// Common.Notification.NotificationSystem
struct NotificationSystem_t89;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// NotificationSystemComponent
struct  NotificationSystemComponent_t91  : public MonoBehaviour_t5
{
	// NotificationHandlerComponent[] NotificationSystemComponent::handlerList
	NotificationHandlerComponentU5BU5D_t92* ___handlerList_2;
	// Common.Notification.NotificationSystem NotificationSystemComponent::system
	NotificationSystem_t89 * ___system_3;
};
