﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.Dispatcher/<CreateSafeFunction>c__AnonStorey10`1<System.Object>
struct U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t2651;
// System.Object
struct Object_t;

// System.Void UnityThreading.Dispatcher/<CreateSafeFunction>c__AnonStorey10`1<System.Object>::.ctor()
extern "C" void U3CCreateSafeFunctionU3Ec__AnonStorey10_1__ctor_m16952_gshared (U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t2651 * __this, const MethodInfo* method);
#define U3CCreateSafeFunctionU3Ec__AnonStorey10_1__ctor_m16952(__this, method) (( void (*) (U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t2651 *, const MethodInfo*))U3CCreateSafeFunctionU3Ec__AnonStorey10_1__ctor_m16952_gshared)(__this, method)
// T UnityThreading.Dispatcher/<CreateSafeFunction>c__AnonStorey10`1<System.Object>::<>m__4()
extern "C" Object_t * U3CCreateSafeFunctionU3Ec__AnonStorey10_1_U3CU3Em__4_m16953_gshared (U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t2651 * __this, const MethodInfo* method);
#define U3CCreateSafeFunctionU3Ec__AnonStorey10_1_U3CU3Em__4_m16953(__this, method) (( Object_t * (*) (U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t2651 *, const MethodInfo*))U3CCreateSafeFunctionU3Ec__AnonStorey10_1_U3CU3Em__4_m16953_gshared)(__this, method)
