﻿#pragma once
#include <stdint.h>
// UnityEngine.MeshFilter[]
struct MeshFilterU5BU5D_t81;
// UnityEngine.Material
struct Material_t82;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// MeshMerger
struct  MeshMerger_t80  : public MonoBehaviour_t5
{
	// UnityEngine.MeshFilter[] MeshMerger::meshFilters
	MeshFilterU5BU5D_t81* ___meshFilters_2;
	// UnityEngine.Material MeshMerger::material
	Material_t82 * ___material_3;
};
