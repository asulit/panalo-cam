﻿#pragma once
#include <stdint.h>
// CountdownTimer
struct CountdownTimer_t13;
// System.String
struct String_t;
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapter.h"
// Common.Fsm.Action.TimedWaitAction
struct  TimedWaitAction_t45  : public FsmActionAdapter_t33
{
	// System.Single Common.Fsm.Action.TimedWaitAction::waitTime
	float ___waitTime_1;
	// CountdownTimer Common.Fsm.Action.TimedWaitAction::timer
	CountdownTimer_t13 * ___timer_2;
	// System.String Common.Fsm.Action.TimedWaitAction::timeReference
	String_t* ___timeReference_3;
	// System.String Common.Fsm.Action.TimedWaitAction::finishEvent
	String_t* ___finishEvent_4;
};
