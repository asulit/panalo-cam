﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.LinkedListNode`1<SwarmItem>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
// System.Collections.Generic.LinkedListNode`1<System.Object>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_2MethodDeclarations.h"
#define LinkedListNode_1__ctor_m16207(__this, ___list, ___value, method) (( void (*) (LinkedListNode_1_t408 *, LinkedList_1_t152 *, SwarmItem_t124 *, const MethodInfo*))LinkedListNode_1__ctor_m16022_gshared)(__this, ___list, ___value, method)
// System.Void System.Collections.Generic.LinkedListNode`1<SwarmItem>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m16208(__this, ___list, ___value, ___previousNode, ___nextNode, method) (( void (*) (LinkedListNode_1_t408 *, LinkedList_1_t152 *, SwarmItem_t124 *, LinkedListNode_1_t408 *, LinkedListNode_1_t408 *, const MethodInfo*))LinkedListNode_1__ctor_m16023_gshared)(__this, ___list, ___value, ___previousNode, ___nextNode, method)
// System.Void System.Collections.Generic.LinkedListNode`1<SwarmItem>::Detach()
#define LinkedListNode_1_Detach_m16209(__this, method) (( void (*) (LinkedListNode_1_t408 *, const MethodInfo*))LinkedListNode_1_Detach_m16024_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<SwarmItem>::get_List()
#define LinkedListNode_1_get_List_m16210(__this, method) (( LinkedList_1_t152 * (*) (LinkedListNode_1_t408 *, const MethodInfo*))LinkedListNode_1_get_List_m16025_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<SwarmItem>::get_Next()
#define LinkedListNode_1_get_Next_m1512(__this, method) (( LinkedListNode_1_t408 * (*) (LinkedListNode_1_t408 *, const MethodInfo*))LinkedListNode_1_get_Next_m16026_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<SwarmItem>::get_Value()
#define LinkedListNode_1_get_Value_m1513(__this, method) (( SwarmItem_t124 * (*) (LinkedListNode_1_t408 *, const MethodInfo*))LinkedListNode_1_get_Value_m16027_gshared)(__this, method)
