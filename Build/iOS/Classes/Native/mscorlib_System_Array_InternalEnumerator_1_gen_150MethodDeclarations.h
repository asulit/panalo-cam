﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Decimal>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_150.h"
// System.Decimal
#include "mscorlib_System_Decimal.h"

// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29849_gshared (InternalEnumerator_1_t3592 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29849(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3592 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29849_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850_gshared (InternalEnumerator_1_t3592 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3592 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29850_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29851_gshared (InternalEnumerator_1_t3592 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29851(__this, method) (( void (*) (InternalEnumerator_1_t3592 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29851_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29852_gshared (InternalEnumerator_1_t3592 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29852(__this, method) (( bool (*) (InternalEnumerator_1_t3592 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29852_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern "C" Decimal_t395  InternalEnumerator_1_get_Current_m29853_gshared (InternalEnumerator_1_t3592 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29853(__this, method) (( Decimal_t395  (*) (InternalEnumerator_1_t3592 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29853_gshared)(__this, method)
