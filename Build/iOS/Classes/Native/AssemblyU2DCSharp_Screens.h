﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<EScreens,System.String>
struct Dictionary_2_t191;
// System.Collections.Generic.Dictionary`2<ESubScreens,System.String>
struct Dictionary_2_t192;
// System.Object
#include "mscorlib_System_Object.h"
// Screens
struct  Screens_t190  : public Object_t
{
};
struct Screens_t190_StaticFields{
	// System.Collections.Generic.Dictionary`2<EScreens,System.String> Screens::SCREENS
	Dictionary_2_t191 * ___SCREENS_0;
	// System.Collections.Generic.Dictionary`2<ESubScreens,System.String> Screens::SUB_SCREENS
	Dictionary_2_t192 * ___SUB_SCREENS_1;
};
