﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2456_t2385_marshal(const U24ArrayTypeU2456_t2385& unmarshaled, U24ArrayTypeU2456_t2385_marshaled& marshaled);
extern "C" void U24ArrayTypeU2456_t2385_marshal_back(const U24ArrayTypeU2456_t2385_marshaled& marshaled, U24ArrayTypeU2456_t2385& unmarshaled);
extern "C" void U24ArrayTypeU2456_t2385_marshal_cleanup(U24ArrayTypeU2456_t2385_marshaled& marshaled);
