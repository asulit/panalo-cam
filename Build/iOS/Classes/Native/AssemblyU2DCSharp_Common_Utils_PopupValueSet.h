﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t137;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Utils.PopupValueSet
struct  PopupValueSet_t136  : public Object_t
{
	// System.String[] Common.Utils.PopupValueSet::valueList
	StringU5BU5D_t137* ___valueList_0;
};
