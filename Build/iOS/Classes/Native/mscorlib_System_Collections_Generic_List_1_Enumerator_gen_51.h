﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t738;
// UnityEngine.UI.Text
struct Text_t196;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>
struct  Enumerator_t2932 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::l
	List_1_t738 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::current
	Text_t196 * ___current_3;
};
