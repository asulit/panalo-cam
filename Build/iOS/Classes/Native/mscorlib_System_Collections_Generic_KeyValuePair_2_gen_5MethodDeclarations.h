﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22MethodDeclarations.h"
#define KeyValuePair_2__ctor_m27411(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1349 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m19811_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::get_Key()
#define KeyValuePair_2_get_Key_m27412(__this, method) (( int32_t (*) (KeyValuePair_2_t1349 *, const MethodInfo*))KeyValuePair_2_get_Key_m19812_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m27413(__this, ___value, method) (( void (*) (KeyValuePair_2_t1349 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m19813_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::get_Value()
#define KeyValuePair_2_get_Value_m7393(__this, method) (( Object_t * (*) (KeyValuePair_2_t1349 *, const MethodInfo*))KeyValuePair_2_get_Value_m19814_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m27414(__this, ___value, method) (( void (*) (KeyValuePair_2_t1349 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m19815_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>::ToString()
#define KeyValuePair_2_ToString_m27415(__this, method) (( String_t* (*) (KeyValuePair_2_t1349 *, const MethodInfo*))KeyValuePair_2_ToString_m19816_gshared)(__this, method)
