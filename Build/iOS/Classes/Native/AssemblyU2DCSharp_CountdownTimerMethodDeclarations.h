﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CountdownTimer
struct CountdownTimer_t13;
// Common.Time.TimeReference
struct TimeReference_t14;
// System.String
struct String_t;

// System.Void CountdownTimer::.ctor(System.Single,Common.Time.TimeReference)
extern "C" void CountdownTimer__ctor_m61 (CountdownTimer_t13 * __this, float ___countdownTime, TimeReference_t14 * ___timeReference, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::.ctor(System.Single,System.String)
extern "C" void CountdownTimer__ctor_m62 (CountdownTimer_t13 * __this, float ___countdownTime, String_t* ___timeReferenceName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::.ctor(System.Single)
extern "C" void CountdownTimer__ctor_m63 (CountdownTimer_t13 * __this, float ___countdownTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::Update()
extern "C" void CountdownTimer_Update_m64 (CountdownTimer_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::Reset()
extern "C" void CountdownTimer_Reset_m65 (CountdownTimer_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::Reset(System.Single)
extern "C" void CountdownTimer_Reset_m66 (CountdownTimer_t13 * __this, float ___countdownTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CountdownTimer::HasElapsed()
extern "C" bool CountdownTimer_HasElapsed_m67 (CountdownTimer_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CountdownTimer::GetRatio()
extern "C" float CountdownTimer_GetRatio_m68 (CountdownTimer_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CountdownTimer::GetPolledTime()
extern "C" float CountdownTimer_GetPolledTime_m69 (CountdownTimer_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::SetPolledTime(System.Single)
extern "C" void CountdownTimer_SetPolledTime_m70 (CountdownTimer_t13 * __this, float ___polledTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::EndTimer()
extern "C" void CountdownTimer_EndTimer_m71 (CountdownTimer_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::SetCountdownTime(System.Single)
extern "C" void CountdownTimer_SetCountdownTime_m72 (CountdownTimer_t13 * __this, float ___newTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CountdownTimer::GetCountdownTime()
extern "C" float CountdownTimer_GetCountdownTime_m73 (CountdownTimer_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CountdownTimer::GetCountdownTimeString()
extern "C" String_t* CountdownTimer_GetCountdownTimeString_m74 (CountdownTimer_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
