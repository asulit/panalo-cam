﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t517;
// System.Object
struct Object_t;
// UnityEngine.UI.InputField
struct InputField_t226;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4
struct  U3CMouseDragOutsideRectU3Ec__Iterator4_t601  : public Object_t
{
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::eventData
	PointerEventData_t517 * ___eventData_0;
	// UnityEngine.Vector2 UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::<localMousePos>__0
	Vector2_t2  ___U3ClocalMousePosU3E__0_1;
	// UnityEngine.Rect UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::<rect>__1
	Rect_t267  ___U3CrectU3E__1_2;
	// System.Single UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::<delay>__2
	float ___U3CdelayU3E__2_3;
	// System.Int32 UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::$PC
	int32_t ___U24PC_4;
	// System.Object UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::$current
	Object_t * ___U24current_5;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::<$>eventData
	PointerEventData_t517 * ___U3CU24U3EeventData_6;
	// UnityEngine.UI.InputField UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::<>f__this
	InputField_t226 * ___U3CU3Ef__this_7;
};
