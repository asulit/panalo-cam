﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t1989;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
extern "C" void AssemblyCompanyAttribute__ctor_m12012 (AssemblyCompanyAttribute_t1989 * __this, String_t* ___company, const MethodInfo* method) IL2CPP_METHOD_ATTR;
