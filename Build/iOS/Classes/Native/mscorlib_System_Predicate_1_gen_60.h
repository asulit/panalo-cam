﻿#pragma once
#include <stdint.h>
// Vuforia.DataSet
struct DataSet_t1088;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.DataSet>
struct  Predicate_1_t3303  : public MulticastDelegate_t28
{
};
