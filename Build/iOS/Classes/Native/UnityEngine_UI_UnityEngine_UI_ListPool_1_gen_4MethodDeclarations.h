﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t678;

// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::.cctor()
extern "C" void ListPool_1__cctor_m22909_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m22909(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m22909_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Get()
extern "C" List_1_t678 * ListPool_1_Get_m3782_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m3782(__this /* static, unused */, method) (( List_1_t678 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m3782_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m3805_gshared (Object_t * __this /* static, unused */, List_1_t678 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m3805(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t678 *, const MethodInfo*))ListPool_1_Release_m3805_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::<s_ListPool>m__15(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__15_m22910_gshared (Object_t * __this /* static, unused */, List_1_t678 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__15_m22910(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t678 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__15_m22910_gshared)(__this /* static, unused */, ___l, method)
