﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.Fsm/StateActionProcessor
struct StateActionProcessor_t50;
// System.Object
struct Object_t;
// Common.Fsm.FsmAction
struct FsmAction_t51;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Common.Fsm.Fsm/StateActionProcessor::.ctor(System.Object,System.IntPtr)
extern "C" void StateActionProcessor__ctor_m158 (StateActionProcessor_t50 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm/StateActionProcessor::Invoke(Common.Fsm.FsmAction)
extern "C" void StateActionProcessor_Invoke_m159 (StateActionProcessor_t50 * __this, Object_t * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_StateActionProcessor_t50(Il2CppObject* delegate, Object_t * ___action);
// System.IAsyncResult Common.Fsm.Fsm/StateActionProcessor::BeginInvoke(Common.Fsm.FsmAction,System.AsyncCallback,System.Object)
extern "C" Object_t * StateActionProcessor_BeginInvoke_m160 (StateActionProcessor_t50 * __this, Object_t * ___action, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm/StateActionProcessor::EndInvoke(System.IAsyncResult)
extern "C" void StateActionProcessor_EndInvoke_m161 (StateActionProcessor_t50 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
