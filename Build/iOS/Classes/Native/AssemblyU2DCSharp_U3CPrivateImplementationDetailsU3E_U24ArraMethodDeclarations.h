﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2424_t333_marshal(const U24ArrayTypeU2424_t333& unmarshaled, U24ArrayTypeU2424_t333_marshaled& marshaled);
extern "C" void U24ArrayTypeU2424_t333_marshal_back(const U24ArrayTypeU2424_t333_marshaled& marshaled, U24ArrayTypeU2424_t333& unmarshaled);
extern "C" void U24ArrayTypeU2424_t333_marshal_cleanup(U24ArrayTypeU2424_t333_marshaled& marshaled);
