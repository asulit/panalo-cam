﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Raffle/<GetPlayerInformation>c__AnonStorey18
struct U3CGetPlayerInformationU3Ec__AnonStorey18_t203;
// UnityEngine.WWW
struct WWW_t199;

// System.Void Raffle/<GetPlayerInformation>c__AnonStorey18::.ctor()
extern "C" void U3CGetPlayerInformationU3Ec__AnonStorey18__ctor_m706 (U3CGetPlayerInformationU3Ec__AnonStorey18_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle/<GetPlayerInformation>c__AnonStorey18::<>m__12(UnityEngine.WWW)
extern "C" void U3CGetPlayerInformationU3Ec__AnonStorey18_U3CU3Em__12_m707 (U3CGetPlayerInformationU3Ec__AnonStorey18_t203 * __this, WWW_t199 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
