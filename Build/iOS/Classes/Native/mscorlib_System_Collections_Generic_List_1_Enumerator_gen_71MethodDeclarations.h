﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t1104;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_71.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m25290_gshared (Enumerator_t3274 * __this, List_1_t1104 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m25290(__this, ___l, method) (( void (*) (Enumerator_t3274 *, List_1_t1104 *, const MethodInfo*))Enumerator__ctor_m25290_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25291_gshared (Enumerator_t3274 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25291(__this, method) (( Object_t * (*) (Enumerator_t3274 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25291_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::Dispose()
extern "C" void Enumerator_Dispose_m25292_gshared (Enumerator_t3274 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25292(__this, method) (( void (*) (Enumerator_t3274 *, const MethodInfo*))Enumerator_Dispose_m25292_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::VerifyState()
extern "C" void Enumerator_VerifyState_m25293_gshared (Enumerator_t3274 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m25293(__this, method) (( void (*) (Enumerator_t3274 *, const MethodInfo*))Enumerator_VerifyState_m25293_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25294_gshared (Enumerator_t3274 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25294(__this, method) (( bool (*) (Enumerator_t3274 *, const MethodInfo*))Enumerator_MoveNext_m25294_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::get_Current()
extern "C" int32_t Enumerator_get_Current_m25295_gshared (Enumerator_t3274 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25295(__this, method) (( int32_t (*) (Enumerator_t3274 *, const MethodInfo*))Enumerator_get_Current_m25295_gshared)(__this, method)
