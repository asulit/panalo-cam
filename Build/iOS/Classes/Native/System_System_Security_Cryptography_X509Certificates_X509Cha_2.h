﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t450;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
struct  X509ChainElementCollection_t1657  : public Object_t
{
	// System.Collections.ArrayList System.Security.Cryptography.X509Certificates.X509ChainElementCollection::_list
	ArrayList_t450 * ____list_0;
};
