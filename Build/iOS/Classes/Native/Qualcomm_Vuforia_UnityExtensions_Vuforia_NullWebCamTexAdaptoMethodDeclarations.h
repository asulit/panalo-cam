﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.NullWebCamTexAdaptor
struct NullWebCamTexAdaptor_t1126;
// UnityEngine.Texture
struct Texture_t103;
// Vuforia.VuforiaRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec.h"

// System.Boolean Vuforia.NullWebCamTexAdaptor::get_DidUpdateThisFrame()
extern "C" bool NullWebCamTexAdaptor_get_DidUpdateThisFrame_m5717 (NullWebCamTexAdaptor_t1126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.NullWebCamTexAdaptor::get_IsPlaying()
extern "C" bool NullWebCamTexAdaptor_get_IsPlaying_m5718 (NullWebCamTexAdaptor_t1126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture Vuforia.NullWebCamTexAdaptor::get_Texture()
extern "C" Texture_t103 * NullWebCamTexAdaptor_get_Texture_m5719 (NullWebCamTexAdaptor_t1126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::.ctor(System.Int32,Vuforia.VuforiaRenderer/Vec2I)
extern "C" void NullWebCamTexAdaptor__ctor_m5720 (NullWebCamTexAdaptor_t1126 * __this, int32_t ___requestedFPS, Vec2I_t1157  ___requestedTextureSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::Play()
extern "C" void NullWebCamTexAdaptor_Play_m5721 (NullWebCamTexAdaptor_t1126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::Stop()
extern "C" void NullWebCamTexAdaptor_Stop_m5722 (NullWebCamTexAdaptor_t1126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
