﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2738;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2506;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.ICollection
struct ICollection_t1751;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3667;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IEnumerator_1_t3668;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1606;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>
struct KeyCollection_t2743;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>
struct ValueCollection_t2747;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__18.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C" void Dictionary_2__ctor_m17855_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m17855(__this, method) (( void (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2__ctor_m17855_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17857_gshared (Dictionary_2_t2738 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17857(__this, ___comparer, method) (( void (*) (Dictionary_2_t2738 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17857_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m17859_gshared (Dictionary_2_t2738 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m17859(__this, ___capacity, method) (( void (*) (Dictionary_2_t2738 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m17859_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m17861_gshared (Dictionary_2_t2738 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m17861(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2738 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m17861_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m17863_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m17863(__this, method) (( Object_t * (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m17863_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17865_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17865(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2738 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m17865_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17867_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17867(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2738 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m17867_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17869_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m17869(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2738 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m17869_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17871_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m17871(__this, ___key, method) (( bool (*) (Dictionary_2_t2738 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m17871_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17873_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m17873(__this, ___key, method) (( void (*) (Dictionary_2_t2738 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m17873_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17875_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17875(__this, method) (( bool (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17875_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17877_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17877(__this, method) (( Object_t * (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17877_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17879_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17879(__this, method) (( bool (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17879_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17881_gshared (Dictionary_2_t2738 * __this, KeyValuePair_2_t2740  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17881(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2738 *, KeyValuePair_2_t2740 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17881_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17883_gshared (Dictionary_2_t2738 * __this, KeyValuePair_2_t2740  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17883(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2738 *, KeyValuePair_2_t2740 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17883_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17885_gshared (Dictionary_2_t2738 * __this, KeyValuePair_2U5BU5D_t3667* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17885(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2738 *, KeyValuePair_2U5BU5D_t3667*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17885_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17887_gshared (Dictionary_2_t2738 * __this, KeyValuePair_2_t2740  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17887(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2738 *, KeyValuePair_2_t2740 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17887_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17889_gshared (Dictionary_2_t2738 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17889(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2738 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m17889_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17891_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17891(__this, method) (( Object_t * (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17891_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17893_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17893(__this, method) (( Object_t* (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17893_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17895_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17895(__this, method) (( Object_t * (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17895_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17897_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m17897(__this, method) (( int32_t (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_get_Count_m17897_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Item(TKey)
extern "C" bool Dictionary_2_get_Item_m17899_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m17899(__this, ___key, method) (( bool (*) (Dictionary_2_t2738 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m17899_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m17901_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m17901(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2738 *, Object_t *, bool, const MethodInfo*))Dictionary_2_set_Item_m17901_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m17903_gshared (Dictionary_2_t2738 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m17903(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2738 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m17903_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m17905_gshared (Dictionary_2_t2738 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m17905(__this, ___size, method) (( void (*) (Dictionary_2_t2738 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m17905_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m17907_gshared (Dictionary_2_t2738 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m17907(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2738 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m17907_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2740  Dictionary_2_make_pair_m17909_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m17909(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2740  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_make_pair_m17909_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m17911_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m17911(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_key_m17911_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_value(TKey,TValue)
extern "C" bool Dictionary_2_pick_value_m17913_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m17913(__this /* static, unused */, ___key, ___value, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_value_m17913_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17915_gshared (Dictionary_2_t2738 * __this, KeyValuePair_2U5BU5D_t3667* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m17915(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2738 *, KeyValuePair_2U5BU5D_t3667*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m17915_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Resize()
extern "C" void Dictionary_2_Resize_m17917_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m17917(__this, method) (( void (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_Resize_m17917_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m17919_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_Add_m17919(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2738 *, Object_t *, bool, const MethodInfo*))Dictionary_2_Add_m17919_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Clear()
extern "C" void Dictionary_2_Clear_m17921_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m17921(__this, method) (( void (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_Clear_m17921_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m17923_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m17923(__this, ___key, method) (( bool (*) (Dictionary_2_t2738 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m17923_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17925_gshared (Dictionary_2_t2738 * __this, bool ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m17925(__this, ___value, method) (( bool (*) (Dictionary_2_t2738 *, bool, const MethodInfo*))Dictionary_2_ContainsValue_m17925_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m17927_gshared (Dictionary_2_t2738 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m17927(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2738 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m17927_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m17929_gshared (Dictionary_2_t2738 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m17929(__this, ___sender, method) (( void (*) (Dictionary_2_t2738 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m17929_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m17931_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m17931(__this, ___key, method) (( bool (*) (Dictionary_2_t2738 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m17931_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m17933_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, bool* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m17933(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2738 *, Object_t *, bool*, const MethodInfo*))Dictionary_2_TryGetValue_m17933_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Keys()
extern "C" KeyCollection_t2743 * Dictionary_2_get_Keys_m17935_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m17935(__this, method) (( KeyCollection_t2743 * (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_get_Keys_m17935_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Values()
extern "C" ValueCollection_t2747 * Dictionary_2_get_Values_m17937_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m17937(__this, method) (( ValueCollection_t2747 * (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_get_Values_m17937_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m17939_gshared (Dictionary_2_t2738 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m17939(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2738 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m17939_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTValue(System.Object)
extern "C" bool Dictionary_2_ToTValue_m17941_gshared (Dictionary_2_t2738 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m17941(__this, ___value, method) (( bool (*) (Dictionary_2_t2738 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m17941_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17943_gshared (Dictionary_2_t2738 * __this, KeyValuePair_2_t2740  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m17943(__this, ___pair, method) (( bool (*) (Dictionary_2_t2738 *, KeyValuePair_2_t2740 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m17943_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetEnumerator()
extern "C" Enumerator_t2745  Dictionary_2_GetEnumerator_m17945_gshared (Dictionary_2_t2738 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m17945(__this, method) (( Enumerator_t2745  (*) (Dictionary_2_t2738 *, const MethodInfo*))Dictionary_2_GetEnumerator_m17945_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m17947_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m17947(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m17947_gshared)(__this /* static, unused */, ___key, ___value, method)
