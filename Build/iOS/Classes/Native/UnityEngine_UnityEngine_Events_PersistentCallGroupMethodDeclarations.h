﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t985;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t987;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t989;

// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C" void PersistentCallGroup__ctor_m5060 (PersistentCallGroup_t985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C" void PersistentCallGroup_Initialize_m5061 (PersistentCallGroup_t985 * __this, InvokableCallList_t987 * ___invokableList, UnityEventBase_t989 * ___unityEventBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
