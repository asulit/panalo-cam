﻿#pragma once
#include <stdint.h>
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t300;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct  KeyValuePair_2_t3405 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::value
	PropAbstractBehaviour_t300 * ___value_1;
};
