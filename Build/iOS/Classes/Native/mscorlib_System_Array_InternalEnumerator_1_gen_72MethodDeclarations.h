﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_72.h"
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23340_gshared (InternalEnumerator_1_t3131 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m23340(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3131 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m23340_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23341_gshared (InternalEnumerator_1_t3131 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23341(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3131 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23341_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23342_gshared (InternalEnumerator_1_t3131 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23342(__this, method) (( void (*) (InternalEnumerator_1_t3131 *, const MethodInfo*))InternalEnumerator_1_Dispose_m23342_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23343_gshared (InternalEnumerator_1_t3131 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23343(__this, method) (( bool (*) (InternalEnumerator_1_t3131 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m23343_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::get_Current()
extern "C" CharacterInfo_t892  InternalEnumerator_1_get_Current_m23344_gshared (InternalEnumerator_1_t3131 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23344(__this, method) (( CharacterInfo_t892  (*) (InternalEnumerator_1_t3131 *, const MethodInfo*))InternalEnumerator_1_get_Current_m23344_gshared)(__this, method)
