﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t2519;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2504;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m15188_gshared (ShimEnumerator_t2519 * __this, Dictionary_2_t2504 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m15188(__this, ___host, method) (( void (*) (ShimEnumerator_t2519 *, Dictionary_2_t2504 *, const MethodInfo*))ShimEnumerator__ctor_m15188_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m15189_gshared (ShimEnumerator_t2519 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m15189(__this, method) (( bool (*) (ShimEnumerator_t2519 *, const MethodInfo*))ShimEnumerator_MoveNext_m15189_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m15190_gshared (ShimEnumerator_t2519 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m15190(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t2519 *, const MethodInfo*))ShimEnumerator_get_Entry_m15190_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m15191_gshared (ShimEnumerator_t2519 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m15191(__this, method) (( Object_t * (*) (ShimEnumerator_t2519 *, const MethodInfo*))ShimEnumerator_get_Key_m15191_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m15192_gshared (ShimEnumerator_t2519 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m15192(__this, method) (( Object_t * (*) (ShimEnumerator_t2519 *, const MethodInfo*))ShimEnumerator_get_Value_m15192_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m15193_gshared (ShimEnumerator_t2519 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m15193(__this, method) (( Object_t * (*) (ShimEnumerator_t2519 *, const MethodInfo*))ShimEnumerator_get_Current_m15193_gshared)(__this, method)
