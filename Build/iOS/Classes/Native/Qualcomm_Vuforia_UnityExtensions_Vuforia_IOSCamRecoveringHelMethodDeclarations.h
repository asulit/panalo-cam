﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void Vuforia.IOSCamRecoveringHelper::SetHasJustResumed()
extern "C" void IOSCamRecoveringHelper_SetHasJustResumed_m5230 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.IOSCamRecoveringHelper::TryToRecover()
extern "C" bool IOSCamRecoveringHelper_TryToRecover_m5231 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSCamRecoveringHelper::SetSuccessfullyRecovered()
extern "C" void IOSCamRecoveringHelper_SetSuccessfullyRecovered_m5232 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSCamRecoveringHelper::.cctor()
extern "C" void IOSCamRecoveringHelper__cctor_m5233 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
