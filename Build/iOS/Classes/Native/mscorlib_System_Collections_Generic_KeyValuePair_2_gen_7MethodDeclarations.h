﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m15198(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2520 *, String_t*, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m15105_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>::get_Key()
#define KeyValuePair_2_get_Key_m15199(__this, method) (( String_t* (*) (KeyValuePair_2_t2520 *, const MethodInfo*))KeyValuePair_2_get_Key_m15106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15200(__this, ___value, method) (( void (*) (KeyValuePair_2_t2520 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15107_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>::get_Value()
#define KeyValuePair_2_get_Value_m15201(__this, method) (( Object_t * (*) (KeyValuePair_2_t2520 *, const MethodInfo*))KeyValuePair_2_get_Value_m15108_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15202(__this, ___value, method) (( void (*) (KeyValuePair_2_t2520 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m15109_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>::ToString()
#define KeyValuePair_2_ToString_m15203(__this, method) (( String_t* (*) (KeyValuePair_2_t2520 *, const MethodInfo*))KeyValuePair_2_ToString_m15110_gshared)(__this, method)
