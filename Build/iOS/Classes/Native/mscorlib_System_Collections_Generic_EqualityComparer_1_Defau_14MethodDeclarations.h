﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t3145;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m23618_gshared (DefaultComparer_t3145 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m23618(__this, method) (( void (*) (DefaultComparer_t3145 *, const MethodInfo*))DefaultComparer__ctor_m23618_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m23619_gshared (DefaultComparer_t3145 * __this, UILineInfo_t752  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m23619(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3145 *, UILineInfo_t752 , const MethodInfo*))DefaultComparer_GetHashCode_m23619_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m23620_gshared (DefaultComparer_t3145 * __this, UILineInfo_t752  ___x, UILineInfo_t752  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m23620(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3145 *, UILineInfo_t752 , UILineInfo_t752 , const MethodInfo*))DefaultComparer_Equals_m23620_gshared)(__this, ___x, ___y, method)
