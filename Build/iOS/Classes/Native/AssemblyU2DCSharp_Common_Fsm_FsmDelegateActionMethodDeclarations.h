﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.FsmDelegateAction
struct FsmDelegateAction_t32;
// Common.Fsm.FsmState
struct FsmState_t29;
// Common.Fsm.FsmDelegateAction/FsmActionRoutine
struct FsmActionRoutine_t27;

// System.Void Common.Fsm.FsmDelegateAction::.ctor(Common.Fsm.FsmState,Common.Fsm.FsmDelegateAction/FsmActionRoutine)
extern "C" void FsmDelegateAction__ctor_m101 (FsmDelegateAction_t32 * __this, Object_t * ___owner, FsmActionRoutine_t27 * ___onEnterRoutine, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.FsmDelegateAction::.ctor(Common.Fsm.FsmState,Common.Fsm.FsmDelegateAction/FsmActionRoutine,Common.Fsm.FsmDelegateAction/FsmActionRoutine,Common.Fsm.FsmDelegateAction/FsmActionRoutine)
extern "C" void FsmDelegateAction__ctor_m102 (FsmDelegateAction_t32 * __this, Object_t * ___owner, FsmActionRoutine_t27 * ___onEnterRoutine, FsmActionRoutine_t27 * ___onUpdateRoutine, FsmActionRoutine_t27 * ___onExitRoutine, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.FsmDelegateAction::OnEnter()
extern "C" void FsmDelegateAction_OnEnter_m103 (FsmDelegateAction_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.FsmDelegateAction::OnUpdate()
extern "C" void FsmDelegateAction_OnUpdate_m104 (FsmDelegateAction_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.FsmDelegateAction::OnExit()
extern "C" void FsmDelegateAction_OnExit_m105 (FsmDelegateAction_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
