﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8MethodDeclarations.h"
#define KeyValuePair_2__ctor_m21134(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2962 *, Graphic_t572 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m15642_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m21135(__this, method) (( Graphic_t572 * (*) (KeyValuePair_2_t2962 *, const MethodInfo*))KeyValuePair_2_get_Key_m15643_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21136(__this, ___value, method) (( void (*) (KeyValuePair_2_t2962 *, Graphic_t572 *, const MethodInfo*))KeyValuePair_2_set_Key_m15644_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m21137(__this, method) (( int32_t (*) (KeyValuePair_2_t2962 *, const MethodInfo*))KeyValuePair_2_get_Value_m15645_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21138(__this, ___value, method) (( void (*) (KeyValuePair_2_t2962 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m15646_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m21139(__this, method) (( String_t* (*) (KeyValuePair_2_t2962 *, const MethodInfo*))KeyValuePair_2_ToString_m15647_gshared)(__this, method)
