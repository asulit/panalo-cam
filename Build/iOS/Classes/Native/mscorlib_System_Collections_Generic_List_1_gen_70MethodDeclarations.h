﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t1216;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerable_1_t1276;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t1292;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ICollection_1_t3789;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ReadOnlyCollection_1_t3461;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t3458;
// System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>
struct Predicate_1_t3466;
// System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>
struct Comparison_1_t3469;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_75.h"

// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void List_1__ctor_m7444_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1__ctor_m7444(__this, method) (( void (*) (List_1_t1216 *, const MethodInfo*))List_1__ctor_m7444_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m28403_gshared (List_1_t1216 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m28403(__this, ___collection, method) (( void (*) (List_1_t1216 *, Object_t*, const MethodInfo*))List_1__ctor_m28403_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m28404_gshared (List_1_t1216 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m28404(__this, ___capacity, method) (( void (*) (List_1_t1216 *, int32_t, const MethodInfo*))List_1__ctor_m28404_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.cctor()
extern "C" void List_1__cctor_m28405_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m28405(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m28405_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m28406_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m28406(__this, method) (( Object_t* (*) (List_1_t1216 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m28406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m28407_gshared (List_1_t1216 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m28407(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1216 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m28407_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m28408_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m28408(__this, method) (( Object_t * (*) (List_1_t1216 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m28408_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m28409_gshared (List_1_t1216 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m28409(__this, ___item, method) (( int32_t (*) (List_1_t1216 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m28409_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m28410_gshared (List_1_t1216 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m28410(__this, ___item, method) (( bool (*) (List_1_t1216 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m28410_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m28411_gshared (List_1_t1216 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m28411(__this, ___item, method) (( int32_t (*) (List_1_t1216 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m28411_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m28412_gshared (List_1_t1216 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m28412(__this, ___index, ___item, method) (( void (*) (List_1_t1216 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m28412_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m28413_gshared (List_1_t1216 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m28413(__this, ___item, method) (( void (*) (List_1_t1216 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m28413_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28414_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28414(__this, method) (( bool (*) (List_1_t1216 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28414_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m28415_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m28415(__this, method) (( bool (*) (List_1_t1216 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m28415_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m28416_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m28416(__this, method) (( Object_t * (*) (List_1_t1216 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m28416_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m28417_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m28417(__this, method) (( bool (*) (List_1_t1216 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m28417_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m28418_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m28418(__this, method) (( bool (*) (List_1_t1216 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m28418_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m28419_gshared (List_1_t1216 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m28419(__this, ___index, method) (( Object_t * (*) (List_1_t1216 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m28419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m28420_gshared (List_1_t1216 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m28420(__this, ___index, ___value, method) (( void (*) (List_1_t1216 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m28420_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C" void List_1_Add_m28421_gshared (List_1_t1216 * __this, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define List_1_Add_m28421(__this, ___item, method) (( void (*) (List_1_t1216 *, TargetSearchResult_t1212 , const MethodInfo*))List_1_Add_m28421_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m28422_gshared (List_1_t1216 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m28422(__this, ___newCount, method) (( void (*) (List_1_t1216 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m28422_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m28423_gshared (List_1_t1216 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m28423(__this, ___idx, ___count, method) (( void (*) (List_1_t1216 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m28423_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m28424_gshared (List_1_t1216 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m28424(__this, ___collection, method) (( void (*) (List_1_t1216 *, Object_t*, const MethodInfo*))List_1_AddCollection_m28424_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m28425_gshared (List_1_t1216 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m28425(__this, ___enumerable, method) (( void (*) (List_1_t1216 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m28425_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m28426_gshared (List_1_t1216 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m28426(__this, ___collection, method) (( void (*) (List_1_t1216 *, Object_t*, const MethodInfo*))List_1_AddRange_m28426_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3461 * List_1_AsReadOnly_m28427_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m28427(__this, method) (( ReadOnlyCollection_1_t3461 * (*) (List_1_t1216 *, const MethodInfo*))List_1_AsReadOnly_m28427_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C" void List_1_Clear_m28428_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_Clear_m28428(__this, method) (( void (*) (List_1_t1216 *, const MethodInfo*))List_1_Clear_m28428_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C" bool List_1_Contains_m28429_gshared (List_1_t1216 * __this, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define List_1_Contains_m28429(__this, ___item, method) (( bool (*) (List_1_t1216 *, TargetSearchResult_t1212 , const MethodInfo*))List_1_Contains_m28429_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m28430_gshared (List_1_t1216 * __this, TargetSearchResultU5BU5D_t3458* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m28430(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1216 *, TargetSearchResultU5BU5D_t3458*, int32_t, const MethodInfo*))List_1_CopyTo_m28430_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Find(System.Predicate`1<T>)
extern "C" TargetSearchResult_t1212  List_1_Find_m28431_gshared (List_1_t1216 * __this, Predicate_1_t3466 * ___match, const MethodInfo* method);
#define List_1_Find_m28431(__this, ___match, method) (( TargetSearchResult_t1212  (*) (List_1_t1216 *, Predicate_1_t3466 *, const MethodInfo*))List_1_Find_m28431_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m28432_gshared (Object_t * __this /* static, unused */, Predicate_1_t3466 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m28432(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3466 *, const MethodInfo*))List_1_CheckMatch_m28432_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m28433_gshared (List_1_t1216 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3466 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m28433(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1216 *, int32_t, int32_t, Predicate_1_t3466 *, const MethodInfo*))List_1_GetIndex_m28433_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C" Enumerator_t3460  List_1_GetEnumerator_m28434_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m28434(__this, method) (( Enumerator_t3460  (*) (List_1_t1216 *, const MethodInfo*))List_1_GetEnumerator_m28434_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m28435_gshared (List_1_t1216 * __this, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define List_1_IndexOf_m28435(__this, ___item, method) (( int32_t (*) (List_1_t1216 *, TargetSearchResult_t1212 , const MethodInfo*))List_1_IndexOf_m28435_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m28436_gshared (List_1_t1216 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m28436(__this, ___start, ___delta, method) (( void (*) (List_1_t1216 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m28436_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m28437_gshared (List_1_t1216 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m28437(__this, ___index, method) (( void (*) (List_1_t1216 *, int32_t, const MethodInfo*))List_1_CheckIndex_m28437_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m28438_gshared (List_1_t1216 * __this, int32_t ___index, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define List_1_Insert_m28438(__this, ___index, ___item, method) (( void (*) (List_1_t1216 *, int32_t, TargetSearchResult_t1212 , const MethodInfo*))List_1_Insert_m28438_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m28439_gshared (List_1_t1216 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m28439(__this, ___collection, method) (( void (*) (List_1_t1216 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m28439_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C" bool List_1_Remove_m28440_gshared (List_1_t1216 * __this, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define List_1_Remove_m28440(__this, ___item, method) (( bool (*) (List_1_t1216 *, TargetSearchResult_t1212 , const MethodInfo*))List_1_Remove_m28440_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m28441_gshared (List_1_t1216 * __this, Predicate_1_t3466 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m28441(__this, ___match, method) (( int32_t (*) (List_1_t1216 *, Predicate_1_t3466 *, const MethodInfo*))List_1_RemoveAll_m28441_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m28442_gshared (List_1_t1216 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m28442(__this, ___index, method) (( void (*) (List_1_t1216 *, int32_t, const MethodInfo*))List_1_RemoveAt_m28442_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m28443_gshared (List_1_t1216 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m28443(__this, ___index, ___count, method) (( void (*) (List_1_t1216 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m28443_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Reverse()
extern "C" void List_1_Reverse_m28444_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_Reverse_m28444(__this, method) (( void (*) (List_1_t1216 *, const MethodInfo*))List_1_Reverse_m28444_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Sort()
extern "C" void List_1_Sort_m28445_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_Sort_m28445(__this, method) (( void (*) (List_1_t1216 *, const MethodInfo*))List_1_Sort_m28445_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m28446_gshared (List_1_t1216 * __this, Comparison_1_t3469 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m28446(__this, ___comparison, method) (( void (*) (List_1_t1216 *, Comparison_1_t3469 *, const MethodInfo*))List_1_Sort_m28446_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::ToArray()
extern "C" TargetSearchResultU5BU5D_t3458* List_1_ToArray_m28447_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_ToArray_m28447(__this, method) (( TargetSearchResultU5BU5D_t3458* (*) (List_1_t1216 *, const MethodInfo*))List_1_ToArray_m28447_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m28448_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m28448(__this, method) (( void (*) (List_1_t1216 *, const MethodInfo*))List_1_TrimExcess_m28448_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m28449_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m28449(__this, method) (( int32_t (*) (List_1_t1216 *, const MethodInfo*))List_1_get_Capacity_m28449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m28450_gshared (List_1_t1216 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m28450(__this, ___value, method) (( void (*) (List_1_t1216 *, int32_t, const MethodInfo*))List_1_set_Capacity_m28450_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C" int32_t List_1_get_Count_m28451_gshared (List_1_t1216 * __this, const MethodInfo* method);
#define List_1_get_Count_m28451(__this, method) (( int32_t (*) (List_1_t1216 *, const MethodInfo*))List_1_get_Count_m28451_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern "C" TargetSearchResult_t1212  List_1_get_Item_m28452_gshared (List_1_t1216 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m28452(__this, ___index, method) (( TargetSearchResult_t1212  (*) (List_1_t1216 *, int32_t, const MethodInfo*))List_1_get_Item_m28452_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m28453_gshared (List_1_t1216 * __this, int32_t ___index, TargetSearchResult_t1212  ___value, const MethodInfo* method);
#define List_1_set_Item_m28453(__this, ___index, ___value, method) (( void (*) (List_1_t1216 *, int32_t, TargetSearchResult_t1212 , const MethodInfo*))List_1_set_Item_m28453_gshared)(__this, ___index, ___value, method)
