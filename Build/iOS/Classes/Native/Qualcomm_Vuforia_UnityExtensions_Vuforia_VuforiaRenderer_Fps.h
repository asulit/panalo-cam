﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.VuforiaRenderer/FpsHint
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Fps.h"
// Vuforia.VuforiaRenderer/FpsHint
struct  FpsHint_t1154 
{
	// System.Int32 Vuforia.VuforiaRenderer/FpsHint::value__
	int32_t ___value___1;
};
