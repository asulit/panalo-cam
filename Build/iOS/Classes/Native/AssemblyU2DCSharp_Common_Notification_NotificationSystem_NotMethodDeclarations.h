﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Notification.NotificationSystem/NotificationInitializer
struct NotificationInitializer_t88;
// System.Object
struct Object_t;
// Common.Notification.NotificationInstance
struct NotificationInstance_t85;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Common.Notification.NotificationSystem/NotificationInitializer::.ctor(System.Object,System.IntPtr)
extern "C" void NotificationInitializer__ctor_m298 (NotificationInitializer_t88 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationSystem/NotificationInitializer::Invoke(Common.Notification.NotificationInstance)
extern "C" void NotificationInitializer_Invoke_m299 (NotificationInitializer_t88 * __this, NotificationInstance_t85 * ___notif, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_NotificationInitializer_t88(Il2CppObject* delegate, NotificationInstance_t85 * ___notif);
// System.IAsyncResult Common.Notification.NotificationSystem/NotificationInitializer::BeginInvoke(Common.Notification.NotificationInstance,System.AsyncCallback,System.Object)
extern "C" Object_t * NotificationInitializer_BeginInvoke_m300 (NotificationInitializer_t88 * __this, NotificationInstance_t85 * ___notif, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationSystem/NotificationInitializer::EndInvoke(System.IAsyncResult)
extern "C" void NotificationInitializer_EndInvoke_m301 (NotificationInitializer_t88 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
