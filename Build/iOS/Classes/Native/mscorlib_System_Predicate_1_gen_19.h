﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t479;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.BaseInputModule>
struct  Predicate_1_t2804  : public MulticastDelegate_t28
{
};
