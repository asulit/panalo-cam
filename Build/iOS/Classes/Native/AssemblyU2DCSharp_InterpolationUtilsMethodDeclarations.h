﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InterpolationUtils
struct InterpolationUtils_t64;

// System.Void InterpolationUtils::.ctor()
extern "C" void InterpolationUtils__ctor_m220 (InterpolationUtils_t64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single InterpolationUtils::Lerp(System.Single,System.Single,System.Single)
extern "C" float InterpolationUtils_Lerp_m221 (Object_t * __this /* static, unused */, float ___from, float ___to, float ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single InterpolationUtils::SmoothStep(System.Single)
extern "C" float InterpolationUtils_SmoothStep_m222 (Object_t * __this /* static, unused */, float ___interpolationValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single InterpolationUtils::GetParabolicPos(System.Single,System.Single,System.Single,System.Single)
extern "C" float InterpolationUtils_GetParabolicPos_m223 (Object_t * __this /* static, unused */, float ___t, float ___y0, float ___y1, float ___dh, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single InterpolationUtils::GetParabolicPos(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" float InterpolationUtils_GetParabolicPos_m224 (Object_t * __this /* static, unused */, float ___t, float ___y0, float ___y1, float ___dh, float ___tFracIncrease, const MethodInfo* method) IL2CPP_METHOD_ATTR;
