﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Boolean UnityEngine.UI.SetPropertyUtility::SetColor(UnityEngine.Color&,UnityEngine.Color)
extern "C" bool SetPropertyUtility_SetColor_m2900 (Object_t * __this /* static, unused */, Color_t9 * ___currentValue, Color_t9  ___newValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
