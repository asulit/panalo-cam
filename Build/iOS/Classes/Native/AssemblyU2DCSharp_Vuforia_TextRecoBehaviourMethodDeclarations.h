﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TextRecoBehaviour
struct TextRecoBehaviour_t307;

// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern "C" void TextRecoBehaviour__ctor_m1261 (TextRecoBehaviour_t307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
