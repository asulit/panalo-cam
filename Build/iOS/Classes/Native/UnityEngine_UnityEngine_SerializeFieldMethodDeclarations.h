﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SerializeField
struct SerializeField_t831;

// System.Void UnityEngine.SerializeField::.ctor()
extern "C" void SerializeField__ctor_m4181 (SerializeField_t831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
