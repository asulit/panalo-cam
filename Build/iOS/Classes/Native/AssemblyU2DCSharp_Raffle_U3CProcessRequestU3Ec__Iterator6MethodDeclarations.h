﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Raffle/<ProcessRequest>c__Iterator6
struct U3CProcessRequestU3Ec__Iterator6_t198;
// System.Object
struct Object_t;

// System.Void Raffle/<ProcessRequest>c__Iterator6::.ctor()
extern "C" void U3CProcessRequestU3Ec__Iterator6__ctor_m698 (U3CProcessRequestU3Ec__Iterator6_t198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Raffle/<ProcessRequest>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CProcessRequestU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m699 (U3CProcessRequestU3Ec__Iterator6_t198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Raffle/<ProcessRequest>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CProcessRequestU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m700 (U3CProcessRequestU3Ec__Iterator6_t198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Raffle/<ProcessRequest>c__Iterator6::MoveNext()
extern "C" bool U3CProcessRequestU3Ec__Iterator6_MoveNext_m701 (U3CProcessRequestU3Ec__Iterator6_t198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle/<ProcessRequest>c__Iterator6::Dispose()
extern "C" void U3CProcessRequestU3Ec__Iterator6_Dispose_m702 (U3CProcessRequestU3Ec__Iterator6_t198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle/<ProcessRequest>c__Iterator6::Reset()
extern "C" void U3CProcessRequestU3Ec__Iterator6_Reset_m703 (U3CProcessRequestU3Ec__Iterator6_t198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
