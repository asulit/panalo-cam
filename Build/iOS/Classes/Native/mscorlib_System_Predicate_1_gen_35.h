﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Selectable
struct Selectable_t545;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Selectable>
struct  Predicate_1_t2991  : public MulticastDelegate_t28
{
};
