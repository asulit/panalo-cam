﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28MethodDeclarations.h"
#define ValueCollection__ctor_m16132(__this, ___dictionary, method) (( void (*) (ValueCollection_t2595 *, Dictionary_2_t110 *, const MethodInfo*))ValueCollection__ctor_m15156_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16133(__this, ___item, method) (( void (*) (ValueCollection_t2595 *, QueryResultResolver_t328 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15157_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16134(__this, method) (( void (*) (ValueCollection_t2595 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15158_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16135(__this, ___item, method) (( bool (*) (ValueCollection_t2595 *, QueryResultResolver_t328 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15159_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16136(__this, ___item, method) (( bool (*) (ValueCollection_t2595 *, QueryResultResolver_t328 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15160_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16137(__this, method) (( Object_t* (*) (ValueCollection_t2595 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15161_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m16138(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2595 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m15162_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16139(__this, method) (( Object_t * (*) (ValueCollection_t2595 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15163_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16140(__this, method) (( bool (*) (ValueCollection_t2595 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15164_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16141(__this, method) (( bool (*) (ValueCollection_t2595 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15165_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m16142(__this, method) (( Object_t * (*) (ValueCollection_t2595 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m15166_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m16143(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2595 *, QueryResultResolverU5BU5D_t2591*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m15167_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::GetEnumerator()
#define ValueCollection_GetEnumerator_m16144(__this, method) (( Enumerator_t3635  (*) (ValueCollection_t2595 *, const MethodInfo*))ValueCollection_GetEnumerator_m15168_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Query.QueryResultResolver>::get_Count()
#define ValueCollection_get_Count_m16145(__this, method) (( int32_t (*) (ValueCollection_t2595 *, const MethodInfo*))ValueCollection_get_Count_m15169_gshared)(__this, method)
