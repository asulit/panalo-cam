﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTracker
struct SmartTerrainTracker_t1168;

// System.Void Vuforia.SmartTerrainTracker::.ctor()
extern "C" void SmartTerrainTracker__ctor_m5828 (SmartTerrainTracker_t1168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
