﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t139;
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Utils.SimpleEncryption
struct  SimpleEncryption_t138  : public Object_t
{
	// System.Byte[] Common.Utils.SimpleEncryption::salt
	ByteU5BU5D_t139* ___salt_0;
	// System.String Common.Utils.SimpleEncryption::password
	String_t* ___password_1;
};
