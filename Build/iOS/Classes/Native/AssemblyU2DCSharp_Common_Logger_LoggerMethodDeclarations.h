﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Logger.Logger
struct Logger_t75;
// System.String
struct String_t;
// Common.Logger.LogLevel
struct LogLevel_t73;

// System.Void Common.Logger.Logger::.ctor()
extern "C" void Logger__ctor_m256 (Logger_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Logger.Logger::.cctor()
extern "C" void Logger__cctor_m257 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Logger.Logger Common.Logger.Logger::GetInstance()
extern "C" Logger_t75 * Logger_GetInstance_m258 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Logger.Logger::SetName(System.String)
extern "C" void Logger_SetName_m259 (Logger_t75 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Logger.Logger::Log(System.String)
extern "C" void Logger_Log_m260 (Logger_t75 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Logger.Logger::LogWarning(System.String)
extern "C" void Logger_LogWarning_m261 (Logger_t75 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Logger.Logger::LogError(System.String)
extern "C" void Logger_LogError_m262 (Logger_t75 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Logger.Logger::Log(Common.Logger.LogLevel,System.String)
extern "C" void Logger_Log_m263 (Logger_t75 * __this, LogLevel_t73 * ___level, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Logger.Logger::EnqueueWriteTask()
extern "C" void Logger_EnqueueWriteTask_m264 (Logger_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Logger.Logger::WriteRemainingLogs()
extern "C" void Logger_WriteRemainingLogs_m265 (Logger_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Logger.Logger::Update()
extern "C" void Logger_Update_m266 (Logger_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Logger.Logger::ProcessTaskQueue()
extern "C" void Logger_ProcessTaskQueue_m267 (Logger_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Logger.Logger::get_LogFilePath()
extern "C" String_t* Logger_get_LogFilePath_m268 (Logger_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
