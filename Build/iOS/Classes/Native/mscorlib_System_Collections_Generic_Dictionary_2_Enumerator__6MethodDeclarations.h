﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2504;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15140_gshared (Enumerator_t2513 * __this, Dictionary_2_t2504 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m15140(__this, ___dictionary, method) (( void (*) (Enumerator_t2513 *, Dictionary_2_t2504 *, const MethodInfo*))Enumerator__ctor_m15140_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15141_gshared (Enumerator_t2513 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15141(__this, method) (( Object_t * (*) (Enumerator_t2513 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15141_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15142_gshared (Enumerator_t2513 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15142(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t2513 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15142_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15143_gshared (Enumerator_t2513 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15143(__this, method) (( Object_t * (*) (Enumerator_t2513 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15143_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15144_gshared (Enumerator_t2513 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15144(__this, method) (( Object_t * (*) (Enumerator_t2513 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15145_gshared (Enumerator_t2513 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15145(__this, method) (( bool (*) (Enumerator_t2513 *, const MethodInfo*))Enumerator_MoveNext_m15145_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2507  Enumerator_get_Current_m15146_gshared (Enumerator_t2513 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15146(__this, method) (( KeyValuePair_2_t2507  (*) (Enumerator_t2513 *, const MethodInfo*))Enumerator_get_Current_m15146_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m15147_gshared (Enumerator_t2513 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m15147(__this, method) (( Object_t * (*) (Enumerator_t2513 *, const MethodInfo*))Enumerator_get_CurrentKey_m15147_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m15148_gshared (Enumerator_t2513 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m15148(__this, method) (( Object_t * (*) (Enumerator_t2513 *, const MethodInfo*))Enumerator_get_CurrentValue_m15148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m15149_gshared (Enumerator_t2513 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15149(__this, method) (( void (*) (Enumerator_t2513 *, const MethodInfo*))Enumerator_VerifyState_m15149_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m15150_gshared (Enumerator_t2513 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m15150(__this, method) (( void (*) (Enumerator_t2513 *, const MethodInfo*))Enumerator_VerifyCurrent_m15150_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15151_gshared (Enumerator_t2513 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15151(__this, method) (( void (*) (Enumerator_t2513 *, const MethodInfo*))Enumerator_Dispose_m15151_gshared)(__this, method)
