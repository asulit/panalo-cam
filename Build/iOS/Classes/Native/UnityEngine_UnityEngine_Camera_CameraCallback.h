﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t6;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Camera/CameraCallback
struct  CameraCallback_t842  : public MulticastDelegate_t28
{
};
