﻿#pragma once
#include <stdint.h>
// UnityEngine.BoxCollider
struct BoxCollider_t126;
// System.Collections.Generic.IList`1<Common.Command>
struct IList_1_t127;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// TouchCollider
struct  TouchCollider_t125  : public MonoBehaviour_t5
{
	// UnityEngine.BoxCollider TouchCollider::boxCollider
	BoxCollider_t126 * ___boxCollider_2;
	// System.Collections.Generic.IList`1<Common.Command> TouchCollider::commandList
	Object_t* ___commandList_3;
};
