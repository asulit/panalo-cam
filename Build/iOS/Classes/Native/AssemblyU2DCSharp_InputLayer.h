﻿#pragma once
#include <stdint.h>
// InputLayerElement[]
struct InputLayerElementU5BU5D_t57;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// InputLayer
struct  InputLayer_t56  : public MonoBehaviour_t5
{
	// System.Boolean InputLayer::modal
	bool ___modal_2;
	// System.Boolean InputLayer::active
	bool ___active_3;
	// InputLayerElement[] InputLayer::elements
	InputLayerElementU5BU5D_t57* ___elements_4;
};
