﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t537;

// System.Void UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback::.ctor()
extern "C" void FloatTweenCallback__ctor_m2203 (FloatTweenCallback_t537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
