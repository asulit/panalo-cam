﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22MethodDeclarations.h"
#define KeyValuePair_2__ctor_m27683(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3405 *, int32_t, PropAbstractBehaviour_t300 *, const MethodInfo*))KeyValuePair_2__ctor_m19811_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m27684(__this, method) (( int32_t (*) (KeyValuePair_2_t3405 *, const MethodInfo*))KeyValuePair_2_get_Key_m19812_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m27685(__this, ___value, method) (( void (*) (KeyValuePair_2_t3405 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m19813_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m27686(__this, method) (( PropAbstractBehaviour_t300 * (*) (KeyValuePair_2_t3405 *, const MethodInfo*))KeyValuePair_2_get_Value_m19814_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m27687(__this, ___value, method) (( void (*) (KeyValuePair_2_t3405 *, PropAbstractBehaviour_t300 *, const MethodInfo*))KeyValuePair_2_set_Value_m19815_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m27688(__this, method) (( String_t* (*) (KeyValuePair_2_t3405 *, const MethodInfo*))KeyValuePair_2_ToString_m19816_gshared)(__this, method)
