﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>
struct U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;

// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::.ctor()
extern "C" void U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m15000_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m15000(__this, method) (( void (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1__ctor_m15000_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C" Object_t * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m15001_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m15001(__this, method) (( Object_t * (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m15001_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m15002_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m15002(__this, method) (( Object_t * (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerator_get_Current_m15002_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m15003_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m15003(__this, method) (( Object_t * (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_IEnumerable_GetEnumerator_m15003_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C" Object_t* U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m15004_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m15004(__this, method) (( Object_t* (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m15004_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::MoveNext()
extern "C" bool U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m15005_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m15005(__this, method) (( bool (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_MoveNext_m15005_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateTakeIterator>c__Iterator19`1<System.Object>::Dispose()
extern "C" void U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m15006_gshared (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 * __this, const MethodInfo* method);
#define U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m15006(__this, method) (( void (*) (U3CCreateTakeIteratorU3Ec__Iterator19_1_t2498 *, const MethodInfo*))U3CCreateTakeIteratorU3Ec__Iterator19_1_Dispose_m15006_gshared)(__this, method)
