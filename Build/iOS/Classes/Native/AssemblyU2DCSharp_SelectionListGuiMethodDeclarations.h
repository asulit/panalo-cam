﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t341;
// UnityEngine.GUIStyle
struct GUIStyle_t342;
// SelectionListGui/DoubleClickCallback
struct DoubleClickCallback_t53;
// System.String[]
struct StringU5BU5D_t137;

// System.Int32 SelectionListGui::SelectionList(System.Int32,UnityEngine.GUIContent[])
extern "C" int32_t SelectionListGui_SelectionList_m188 (Object_t * __this /* static, unused */, int32_t ___selected, GUIContentU5BU5D_t341* ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SelectionListGui::SelectionList(System.Int32,UnityEngine.GUIContent[],UnityEngine.GUIStyle)
extern "C" int32_t SelectionListGui_SelectionList_m189 (Object_t * __this /* static, unused */, int32_t ___selected, GUIContentU5BU5D_t341* ___list, GUIStyle_t342 * ___elementStyle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SelectionListGui::SelectionList(System.Int32,UnityEngine.GUIContent[],SelectionListGui/DoubleClickCallback)
extern "C" int32_t SelectionListGui_SelectionList_m190 (Object_t * __this /* static, unused */, int32_t ___selected, GUIContentU5BU5D_t341* ___list, DoubleClickCallback_t53 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SelectionListGui::SelectionList(System.Int32,UnityEngine.GUIContent[],UnityEngine.GUIStyle,SelectionListGui/DoubleClickCallback)
extern "C" int32_t SelectionListGui_SelectionList_m191 (Object_t * __this /* static, unused */, int32_t ___selected, GUIContentU5BU5D_t341* ___list, GUIStyle_t342 * ___elementStyle, DoubleClickCallback_t53 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SelectionListGui::SelectionList(System.Int32,System.String[])
extern "C" int32_t SelectionListGui_SelectionList_m192 (Object_t * __this /* static, unused */, int32_t ___selected, StringU5BU5D_t137* ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SelectionListGui::SelectionList(System.Int32,System.String[],UnityEngine.GUIStyle)
extern "C" int32_t SelectionListGui_SelectionList_m193 (Object_t * __this /* static, unused */, int32_t ___selected, StringU5BU5D_t137* ___list, GUIStyle_t342 * ___elementStyle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SelectionListGui::SelectionList(System.Int32,System.String[],SelectionListGui/DoubleClickCallback)
extern "C" int32_t SelectionListGui_SelectionList_m194 (Object_t * __this /* static, unused */, int32_t ___selected, StringU5BU5D_t137* ___list, DoubleClickCallback_t53 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SelectionListGui::SelectionList(System.Int32,System.String[],UnityEngine.GUIStyle,SelectionListGui/DoubleClickCallback)
extern "C" int32_t SelectionListGui_SelectionList_m195 (Object_t * __this /* static, unused */, int32_t ___selected, StringU5BU5D_t137* ___list, GUIStyle_t342 * ___elementStyle, DoubleClickCallback_t53 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
