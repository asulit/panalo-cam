﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.VuforiaManagerImpl/MeshData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__7.h"
// Vuforia.VuforiaManagerImpl/Obb3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__3.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Vuforia.VuforiaManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_.h"
// Vuforia.VuforiaManagerImpl/PropData
#pragma pack(push, tp, 1)
struct  PropData_t1144 
{
	// Vuforia.VuforiaManagerImpl/MeshData Vuforia.VuforiaManagerImpl/PropData::meshData
	MeshData_t1141  ___meshData_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/PropData::id
	int32_t ___id_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/PropData::parentID
	int32_t ___parentID_2;
	// Vuforia.VuforiaManagerImpl/Obb3D Vuforia.VuforiaManagerImpl/PropData::boundingBox
	Obb3D_t1137  ___boundingBox_3;
	// UnityEngine.Vector2 Vuforia.VuforiaManagerImpl/PropData::localPosition
	Vector2_t2  ___localPosition_4;
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.VuforiaManagerImpl/PropData::localPose
	PoseData_t1133  ___localPose_5;
	// System.Int32 Vuforia.VuforiaManagerImpl/PropData::revision
	int32_t ___revision_6;
	// System.Int32 Vuforia.VuforiaManagerImpl/PropData::unused
	int32_t ___unused_7;
};
#pragma pack(pop, tp)
