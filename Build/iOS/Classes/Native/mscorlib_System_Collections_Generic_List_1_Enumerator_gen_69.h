﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct List_1_t988;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t981;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>
struct  Enumerator_t3217 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::l
	List_1_t988 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::current
	BaseInvokableCall_t981 * ___current_3;
};
