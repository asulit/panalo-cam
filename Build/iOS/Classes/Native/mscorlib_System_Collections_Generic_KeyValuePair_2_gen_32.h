﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t6;
// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t315;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>
struct  KeyValuePair_2_t3227 
{
	// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::key
	Camera_t6 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::value
	VideoBackgroundAbstractBehaviour_t315 * ___value_1;
};
