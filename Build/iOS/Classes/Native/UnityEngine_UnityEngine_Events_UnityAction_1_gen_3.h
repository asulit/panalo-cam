﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t365;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct  UnityAction_1_t669  : public MulticastDelegate_t28
{
};
