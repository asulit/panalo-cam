﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// FrameRateView
struct FrameRateView_t25;

// System.Void FrameRateView::.ctor()
extern "C" void FrameRateView__ctor_m94 (FrameRateView_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrameRateView::Start()
extern "C" void FrameRateView_Start_m95 (FrameRateView_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrameRateView::Update()
extern "C" void FrameRateView_Update_m96 (FrameRateView_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
