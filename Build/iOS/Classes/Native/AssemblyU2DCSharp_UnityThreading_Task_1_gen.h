﻿#pragma once
#include <stdint.h>
// System.Func`1<System.Object>
struct Func_1_t2650;
// System.Object
struct Object_t;
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBase.h"
// UnityThreading.Task`1<System.Object>
struct  Task_1_t2649  : public TaskBase_t163
{
	// System.Func`1<T> UnityThreading.Task`1<System.Object>::function
	Func_1_t2650 * ___function_4;
	// T UnityThreading.Task`1<System.Object>::result
	Object_t * ___result_5;
};
