﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// HudOrthoAutoScale
struct HudOrthoAutoScale_t55;

// System.Void HudOrthoAutoScale::.ctor()
extern "C" void HudOrthoAutoScale__ctor_m196 (HudOrthoAutoScale_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HudOrthoAutoScale::Start()
extern "C" void HudOrthoAutoScale_Start_m197 (HudOrthoAutoScale_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HudOrthoAutoScale::Update()
extern "C" void HudOrthoAutoScale_Update_m198 (HudOrthoAutoScale_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HudOrthoAutoScale::OnChangeOrthoSize(System.Single)
extern "C" void HudOrthoAutoScale_OnChangeOrthoSize_m199 (HudOrthoAutoScale_t55 * __this, float ___newSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
