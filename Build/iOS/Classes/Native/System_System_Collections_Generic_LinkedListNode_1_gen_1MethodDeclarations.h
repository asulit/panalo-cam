﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t1312;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t1152;

// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C" void LinkedListNode_1__ctor_m26063_gshared (LinkedListNode_1_t1312 * __this, LinkedList_1_t1152 * ___list, int32_t ___value, const MethodInfo* method);
#define LinkedListNode_1__ctor_m26063(__this, ___list, ___value, method) (( void (*) (LinkedListNode_1_t1312 *, LinkedList_1_t1152 *, int32_t, const MethodInfo*))LinkedListNode_1__ctor_m26063_gshared)(__this, ___list, ___value, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedListNode_1__ctor_m26064_gshared (LinkedListNode_1_t1312 * __this, LinkedList_1_t1152 * ___list, int32_t ___value, LinkedListNode_1_t1312 * ___previousNode, LinkedListNode_1_t1312 * ___nextNode, const MethodInfo* method);
#define LinkedListNode_1__ctor_m26064(__this, ___list, ___value, ___previousNode, ___nextNode, method) (( void (*) (LinkedListNode_1_t1312 *, LinkedList_1_t1152 *, int32_t, LinkedListNode_1_t1312 *, LinkedListNode_1_t1312 *, const MethodInfo*))LinkedListNode_1__ctor_m26064_gshared)(__this, ___list, ___value, ___previousNode, ___nextNode, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::Detach()
extern "C" void LinkedListNode_1_Detach_m26065_gshared (LinkedListNode_1_t1312 * __this, const MethodInfo* method);
#define LinkedListNode_1_Detach_m26065(__this, method) (( void (*) (LinkedListNode_1_t1312 *, const MethodInfo*))LinkedListNode_1_Detach_m26065_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_List()
extern "C" LinkedList_1_t1152 * LinkedListNode_1_get_List_m26066_gshared (LinkedListNode_1_t1312 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_List_m26066(__this, method) (( LinkedList_1_t1152 * (*) (LinkedListNode_1_t1312 *, const MethodInfo*))LinkedListNode_1_get_List_m26066_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Next()
extern "C" LinkedListNode_1_t1312 * LinkedListNode_1_get_Next_m7428_gshared (LinkedListNode_1_t1312 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Next_m7428(__this, method) (( LinkedListNode_1_t1312 * (*) (LinkedListNode_1_t1312 *, const MethodInfo*))LinkedListNode_1_get_Next_m7428_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Value()
extern "C" int32_t LinkedListNode_1_get_Value_m7291_gshared (LinkedListNode_1_t1312 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Value_m7291(__this, method) (( int32_t (*) (LinkedListNode_1_t1312 *, const MethodInfo*))LinkedListNode_1_get_Value_m7291_gshared)(__this, method)
