﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.Action.NonResettingTimedWait
struct NonResettingTimedWait_t39;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.String
struct String_t;

// System.Void Common.Fsm.Action.NonResettingTimedWait::.ctor(Common.Fsm.FsmState,System.String,System.String)
extern "C" void NonResettingTimedWait__ctor_m123 (NonResettingTimedWait_t39 * __this, Object_t * ___owner, String_t* ___timeReferenceName, String_t* ___finishEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.NonResettingTimedWait::Init(System.Single)
extern "C" void NonResettingTimedWait_Init_m124 (NonResettingTimedWait_t39 * __this, float ___waitTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.NonResettingTimedWait::OnEnter()
extern "C" void NonResettingTimedWait_OnEnter_m125 (NonResettingTimedWait_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.NonResettingTimedWait::OnUpdate()
extern "C" void NonResettingTimedWait_OnUpdate_m126 (NonResettingTimedWait_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.NonResettingTimedWait::Finish()
extern "C" void NonResettingTimedWait_Finish_m127 (NonResettingTimedWait_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Common.Fsm.Action.NonResettingTimedWait::GetRatio()
extern "C" float NonResettingTimedWait_GetRatio_m128 (NonResettingTimedWait_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
