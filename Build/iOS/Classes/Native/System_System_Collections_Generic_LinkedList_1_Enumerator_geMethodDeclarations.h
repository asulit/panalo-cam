﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t2583;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C" void Enumerator__ctor_m16028_gshared (Enumerator_t2585 * __this, LinkedList_1_t2583 * ___parent, const MethodInfo* method);
#define Enumerator__ctor_m16028(__this, ___parent, method) (( void (*) (Enumerator_t2585 *, LinkedList_1_t2583 *, const MethodInfo*))Enumerator__ctor_m16028_gshared)(__this, ___parent, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16029_gshared (Enumerator_t2585 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16029(__this, method) (( Object_t * (*) (Enumerator_t2585 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16029_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m16030_gshared (Enumerator_t2585 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16030(__this, method) (( Object_t * (*) (Enumerator_t2585 *, const MethodInfo*))Enumerator_get_Current_m16030_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16031_gshared (Enumerator_t2585 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16031(__this, method) (( bool (*) (Enumerator_t2585 *, const MethodInfo*))Enumerator_MoveNext_m16031_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16032_gshared (Enumerator_t2585 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16032(__this, method) (( void (*) (Enumerator_t2585 *, const MethodInfo*))Enumerator_Dispose_m16032_gshared)(__this, method)
