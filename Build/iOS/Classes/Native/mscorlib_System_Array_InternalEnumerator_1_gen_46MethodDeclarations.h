﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_46.h"
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19175_gshared (InternalEnumerator_1_t2832 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19175(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2832 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19175_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19176_gshared (InternalEnumerator_1_t2832 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19176(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2832 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19176_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19177_gshared (InternalEnumerator_1_t2832 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19177(__this, method) (( void (*) (InternalEnumerator_1_t2832 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19177_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19178_gshared (InternalEnumerator_1_t2832 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19178(__this, method) (( bool (*) (InternalEnumerator_1_t2832 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19178_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t512  InternalEnumerator_1_get_Current_m19179_gshared (InternalEnumerator_1_t2832 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19179(__this, method) (( RaycastResult_t512  (*) (InternalEnumerator_1_t2832 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19179_gshared)(__this, method)
