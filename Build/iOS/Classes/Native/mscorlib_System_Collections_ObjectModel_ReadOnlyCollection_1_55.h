﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.Trackable>
struct IList_1_t3288;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>
struct  ReadOnlyCollection_1_t3287  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>::list
	Object_t* ___list_0;
};
