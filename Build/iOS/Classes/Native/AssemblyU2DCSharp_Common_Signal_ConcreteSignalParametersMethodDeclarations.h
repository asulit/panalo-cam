﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Signal.ConcreteSignalParameters
struct ConcreteSignalParameters_t113;
// System.String
struct String_t;
// System.Object
struct Object_t;

// System.Void Common.Signal.ConcreteSignalParameters::.ctor()
extern "C" void ConcreteSignalParameters__ctor_m386 (ConcreteSignalParameters_t113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Signal.ConcreteSignalParameters::AddParameter(System.String,System.Object)
extern "C" void ConcreteSignalParameters_AddParameter_m387 (ConcreteSignalParameters_t113 * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Common.Signal.ConcreteSignalParameters::GetParameter(System.String)
extern "C" Object_t * ConcreteSignalParameters_GetParameter_m388 (ConcreteSignalParameters_t113 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Signal.ConcreteSignalParameters::HasParameter(System.String)
extern "C" bool ConcreteSignalParameters_HasParameter_m389 (ConcreteSignalParameters_t113 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Signal.ConcreteSignalParameters::Clear()
extern "C" void ConcreteSignalParameters_Clear_m390 (ConcreteSignalParameters_t113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
