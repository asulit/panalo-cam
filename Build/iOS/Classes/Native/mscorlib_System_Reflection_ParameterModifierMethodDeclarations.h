﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void ParameterModifier_t2024_marshal(const ParameterModifier_t2024& unmarshaled, ParameterModifier_t2024_marshaled& marshaled);
extern "C" void ParameterModifier_t2024_marshal_back(const ParameterModifier_t2024_marshaled& marshaled, ParameterModifier_t2024& unmarshaled);
extern "C" void ParameterModifier_t2024_marshal_cleanup(ParameterModifier_t2024_marshaled& marshaled);
