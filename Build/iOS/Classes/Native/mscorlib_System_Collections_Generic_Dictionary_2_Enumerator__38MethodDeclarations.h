﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3335;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__38.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_38.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m26415_gshared (Enumerator_t3342 * __this, Dictionary_2_t3335 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m26415(__this, ___dictionary, method) (( void (*) (Enumerator_t3342 *, Dictionary_2_t3335 *, const MethodInfo*))Enumerator__ctor_m26415_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26416_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26416(__this, method) (( Object_t * (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26416_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26417_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26417(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26417_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26418_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26418(__this, method) (( Object_t * (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26418_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26419_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26419(__this, method) (( Object_t * (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26419_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26420_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26420(__this, method) (( bool (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_MoveNext_m26420_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C" KeyValuePair_2_t3337  Enumerator_get_Current_m26421_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26421(__this, method) (( KeyValuePair_2_t3337  (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_get_Current_m26421_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m26422_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m26422(__this, method) (( Object_t * (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_get_CurrentKey_m26422_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentValue()
extern "C" uint16_t Enumerator_get_CurrentValue_m26423_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m26423(__this, method) (( uint16_t (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_get_CurrentValue_m26423_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyState()
extern "C" void Enumerator_VerifyState_m26424_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m26424(__this, method) (( void (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_VerifyState_m26424_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m26425_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m26425(__this, method) (( void (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_VerifyCurrent_m26425_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m26426_gshared (Enumerator_t3342 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m26426(__this, method) (( void (*) (Enumerator_t3342 *, const MethodInfo*))Enumerator_Dispose_m26426_gshared)(__this, method)
