﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t401;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t2500;
// EScreens[]
struct EScreensU5BU5D_t2662;
// System.String[]
struct StringU5BU5D_t137;
// System.Collections.Generic.IEqualityComparer`1<EScreens>
struct IEqualityComparer_1_t2664;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t2663;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2<EScreens,System.String>
struct  Dictionary_2_t191  : public Object_t
{
	// System.Int32[] System.Collections.Generic.Dictionary`2<EScreens,System.String>::table
	Int32U5BU5D_t401* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2<EScreens,System.String>::linkSlots
	LinkU5BU5D_t2500* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2<EScreens,System.String>::keySlots
	EScreensU5BU5D_t2662* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2<EScreens,System.String>::valueSlots
	StringU5BU5D_t137* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2<EScreens,System.String>::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2<EScreens,System.String>::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2<EScreens,System.String>::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2<EScreens,System.String>::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<EScreens,System.String>::hcp
	Object_t* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2<EScreens,System.String>::serialization_info
	SerializationInfo_t1012 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2<EScreens,System.String>::generation
	int32_t ___generation_14;
};
struct Dictionary_2_t191_StaticFields{
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2<EScreens,System.String>::<>f__am$cacheB
	Transform_1_t2663 * ___U3CU3Ef__amU24cacheB_15;
};
