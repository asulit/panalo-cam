﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// OrthographicCameraObserver[]
// OrthographicCameraObserver[]
struct  OrthographicCameraObserverU5BU5D_t2468  : public Array_t
{
};
// Common.Fsm.FsmState[]
// Common.Fsm.FsmState[]
struct  FsmStateU5BU5D_t2501  : public Array_t
{
};
// Common.Fsm.FsmAction[]
// Common.Fsm.FsmAction[]
struct  FsmActionU5BU5D_t2524  : public Array_t
{
};
// InputLayerElement[]
// InputLayerElement[]
struct  InputLayerElementU5BU5D_t57  : public Array_t
{
};
// InputLayer[]
// InputLayer[]
struct  InputLayerU5BU5D_t62  : public Array_t
{
};
// Common.Logger.Log[]
// Common.Logger.Log[]
struct  LogU5BU5D_t2567  : public Array_t
{
};
// Common.Notification.NotificationInstance[]
// Common.Notification.NotificationInstance[]
struct  NotificationInstanceU5BU5D_t2579  : public Array_t
{
};
struct NotificationInstanceU5BU5D_t2579_StaticFields{
};
// Common.Notification.NotificationHandler[]
// Common.Notification.NotificationHandler[]
struct  NotificationHandlerU5BU5D_t3632  : public Array_t
{
};
// NotificationHandlerComponent[]
// NotificationHandlerComponent[]
struct  NotificationHandlerComponentU5BU5D_t92  : public Array_t
{
};
// SwarmItemManager/PrefabItem[]
// SwarmItemManager/PrefabItem[]
struct  PrefabItemU5BU5D_t157  : public Array_t
{
};
// PrefabManager/PruneData[]
// PrefabManager/PruneData[]
struct  PruneDataU5BU5D_t98  : public Array_t
{
};
// PrefabManager/PreloadData[]
// PrefabManager/PreloadData[]
struct  PreloadDataU5BU5D_t100  : public Array_t
{
};
// Common.Query.QueryResultResolver[]
// Common.Query.QueryResultResolver[]
struct  QueryResultResolverU5BU5D_t2591  : public Array_t
{
};
// SwarmItem[]
// SwarmItem[]
struct  SwarmItemU5BU5D_t2597  : public Array_t
{
};
// SwarmItemManager/PrefabItemLists[]
// SwarmItemManager/PrefabItemLists[]
struct  PrefabItemListsU5BU5D_t156  : public Array_t
{
};
// Common.Signal.Signal/SignalListener[]
// Common.Signal.Signal/SignalListener[]
struct  SignalListenerU5BU5D_t2606  : public Array_t
{
};
// Common.Signal.Signal[]
// Common.Signal.Signal[]
struct  SignalU5BU5D_t2609  : public Array_t
{
};
// Common.Time.TimeReference[]
// Common.Time.TimeReference[]
struct  TimeReferenceU5BU5D_t2615  : public Array_t
{
};
struct TimeReferenceU5BU5D_t2615_StaticFields{
};
// Common.Command[]
// Common.Command[]
struct  CommandU5BU5D_t2621  : public Array_t
{
};
// Common.Xml.SimpleXmlNode[]
// Common.Xml.SimpleXmlNode[]
struct  SimpleXmlNodeU5BU5D_t2636  : public Array_t
{
};
// UnityThreading.TaskBase[]
// UnityThreading.TaskBase[]
struct  TaskBaseU5BU5D_t2644  : public Array_t
{
};
// UnityThreading.TaskWorker[]
// UnityThreading.TaskWorker[]
struct  TaskWorkerU5BU5D_t167  : public Array_t
{
};
// UnityThreading.ThreadBase[]
// UnityThreading.ThreadBase[]
struct  ThreadBaseU5BU5D_t430  : public Array_t
{
};
struct ThreadBaseU5BU5D_t430_ThreadStaticFields{
};
// EScreens[]
// EScreens[]
struct  EScreensU5BU5D_t2662  : public Array_t
{
};
// ESubScreens[]
// ESubScreens[]
struct  ESubScreensU5BU5D_t2685  : public Array_t
{
};
// ERaffleResult[]
// ERaffleResult[]
struct  ERaffleResultU5BU5D_t2708  : public Array_t
{
};
// AnimatorFrame[]
// AnimatorFrame[]
struct  AnimatorFrameU5BU5D_t2770  : public Array_t
{
};
// iTween[]
// iTween[]
struct  iTweenU5BU5D_t449  : public Array_t
{
};
struct iTweenU5BU5D_t449_StaticFields{
};
// Vuforia.WireframeBehaviour[]
// Vuforia.WireframeBehaviour[]
struct  WireframeBehaviourU5BU5D_t473  : public Array_t
{
};
