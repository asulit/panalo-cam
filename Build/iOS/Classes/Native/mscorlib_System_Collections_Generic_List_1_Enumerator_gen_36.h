﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Common.Command>
struct List_1_t411;
// Common.Command
struct Command_t348;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Common.Command>
struct  Enumerator_t2624 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Common.Command>::l
	List_1_t411 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Common.Command>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Common.Command>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Common.Command>::current
	Object_t * ___current_3;
};
