﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.TextEditor
struct TextEditor_t977;

// System.Void UnityEngine.TextEditor::.ctor()
extern "C" void TextEditor__ctor_m5028 (TextEditor_t977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
