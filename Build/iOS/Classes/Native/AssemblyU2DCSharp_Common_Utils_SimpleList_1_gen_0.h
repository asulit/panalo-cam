﻿#pragma once
#include <stdint.h>
// Common.Signal.Signal/SignalListener[]
struct SignalListenerU5BU5D_t2606;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>
struct  SimpleList_1_t117  : public Object_t
{
	// T[] Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::buffer
	SignalListenerU5BU5D_t2606* ___buffer_1;
	// System.Int32 Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::size
	int32_t ___size_2;
};
