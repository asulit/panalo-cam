﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// VideoPlayer
struct VideoPlayer_t241;
// System.String
struct String_t;

// System.Void VideoPlayer::.ctor()
extern "C" void VideoPlayer__ctor_m868 (VideoPlayer_t241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoPlayer::Awake()
extern "C" void VideoPlayer_Awake_m869 (VideoPlayer_t241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoPlayer::Start()
extern "C" void VideoPlayer_Start_m870 (VideoPlayer_t241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoPlayer::OnApplicationPause(System.Boolean)
extern "C" void VideoPlayer_OnApplicationPause_m871 (VideoPlayer_t241 * __this, bool ___pauseStatus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoPlayer::Update()
extern "C" void VideoPlayer_Update_m872 (VideoPlayer_t241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoPlayer::OnDestroy()
extern "C" void VideoPlayer_OnDestroy_m873 (VideoPlayer_t241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VideoPlayer::GetVideo()
extern "C" String_t* VideoPlayer_GetVideo_m874 (VideoPlayer_t241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoPlayer::Play()
extern "C" void VideoPlayer_Play_m875 (VideoPlayer_t241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
