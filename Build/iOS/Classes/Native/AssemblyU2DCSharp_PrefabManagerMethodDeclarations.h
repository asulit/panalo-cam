﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PrefabManager
struct PrefabManager_t96;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// UnityEngine.GameObject
struct GameObject_t155;
// System.String
struct String_t;
// SelfManagingSwarmItemManager
struct SelfManagingSwarmItemManager_t99;
// PrefabManager/PruneData[]
struct PruneDataU5BU5D_t98;
// PrefabManager/PreloadData[]
struct PreloadDataU5BU5D_t100;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void PrefabManager::.ctor()
extern "C" void PrefabManager__ctor_m340 (PrefabManager_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager::Awake()
extern "C" void PrefabManager_Awake_m341 (PrefabManager_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PrefabManager::Preload()
extern "C" Object_t * PrefabManager_Preload_m342 (PrefabManager_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager::Prune()
extern "C" void PrefabManager_Prune_m343 (PrefabManager_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PrefabManager::PruneItems()
extern "C" Object_t * PrefabManager_PruneItems_m344 (PrefabManager_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PrefabManager::Request(System.String)
extern "C" GameObject_t155 * PrefabManager_Request_m345 (PrefabManager_t96 * __this, String_t* ___prefabName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PrefabManager::Request(System.String,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" GameObject_t155 * PrefabManager_Request_m346 (PrefabManager_t96 * __this, String_t* ___prefabName, Vector3_t36  ___position, Quaternion_t41  ___rotation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PrefabManager::Request(System.Int32)
extern "C" GameObject_t155 * PrefabManager_Request_m347 (PrefabManager_t96 * __this, int32_t ___prefabIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager::Preload(System.String,System.Int32)
extern "C" void PrefabManager_Preload_m348 (PrefabManager_t96 * __this, String_t* ___prefabName, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager::KillAllActiveItems()
extern "C" void PrefabManager_KillAllActiveItems_m349 (PrefabManager_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SelfManagingSwarmItemManager PrefabManager::get_ItemManager()
extern "C" SelfManagingSwarmItemManager_t99 * PrefabManager_get_ItemManager_m350 (PrefabManager_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager::SetPruneDataList(PrefabManager/PruneData[])
extern "C" void PrefabManager_SetPruneDataList_m351 (PrefabManager_t96 * __this, PruneDataU5BU5D_t98* ___pruneDataList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager::SetPreloadDataList(PrefabManager/PreloadData[])
extern "C" void PrefabManager_SetPreloadDataList_m352 (PrefabManager_t96 * __this, PreloadDataU5BU5D_t100* ___dataList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PrefabManager::get_PrefabCount()
extern "C" int32_t PrefabManager_get_PrefabCount_m353 (PrefabManager_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
