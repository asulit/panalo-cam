﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__48MethodDeclarations.h"
#define Enumerator__ctor_m28877(__this, ___dictionary, method) (( void (*) (Enumerator_t3495 *, Dictionary_2_t1228 *, const MethodInfo*))Enumerator__ctor_m28778_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m28878(__this, method) (( Object_t * (*) (Enumerator_t3495 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m28779_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m28879(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3495 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m28780_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m28880(__this, method) (( Object_t * (*) (Enumerator_t3495 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m28781_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m28881(__this, method) (( Object_t * (*) (Enumerator_t3495 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m28782_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::MoveNext()
#define Enumerator_MoveNext_m28882(__this, method) (( bool (*) (Enumerator_t3495 *, const MethodInfo*))Enumerator_MoveNext_m28783_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_Current()
#define Enumerator_get_Current_m28883(__this, method) (( KeyValuePair_2_t3492  (*) (Enumerator_t3495 *, const MethodInfo*))Enumerator_get_Current_m28784_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m28884(__this, method) (( String_t* (*) (Enumerator_t3495 *, const MethodInfo*))Enumerator_get_CurrentKey_m28785_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m28885(__this, method) (( ProfileData_t1226  (*) (Enumerator_t3495 *, const MethodInfo*))Enumerator_get_CurrentValue_m28786_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::VerifyState()
#define Enumerator_VerifyState_m28886(__this, method) (( void (*) (Enumerator_t3495 *, const MethodInfo*))Enumerator_VerifyState_m28787_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m28887(__this, method) (( void (*) (Enumerator_t3495 *, const MethodInfo*))Enumerator_VerifyCurrent_m28788_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::Dispose()
#define Enumerator_Dispose_m28888(__this, method) (( void (*) (Enumerator_t3495 *, const MethodInfo*))Enumerator_Dispose_m28789_gshared)(__this, method)
