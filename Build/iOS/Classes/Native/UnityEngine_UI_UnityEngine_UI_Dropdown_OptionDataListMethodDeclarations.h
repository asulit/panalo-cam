﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t559;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_t560;

// System.Void UnityEngine.UI.Dropdown/OptionDataList::.ctor()
extern "C" void OptionDataList__ctor_m2308 (OptionDataList_t559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> UnityEngine.UI.Dropdown/OptionDataList::get_options()
extern "C" List_1_t560 * OptionDataList_get_options_m2309 (OptionDataList_t559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/OptionDataList::set_options(System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>)
extern "C" void OptionDataList_set_options_m2310 (OptionDataList_t559 * __this, List_1_t560 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
