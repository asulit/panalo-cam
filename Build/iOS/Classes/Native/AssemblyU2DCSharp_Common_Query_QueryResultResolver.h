﻿#pragma once
#include <stdint.h>
// Common.Query.IQueryRequest
struct IQueryRequest_t329;
// Common.Query.IMutableQueryResult
struct IMutableQueryResult_t330;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// Common.Query.QueryResultResolver
struct  QueryResultResolver_t328  : public MulticastDelegate_t28
{
};
