﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>
struct  Transform_1_t2677  : public MulticastDelegate_t28
{
};
