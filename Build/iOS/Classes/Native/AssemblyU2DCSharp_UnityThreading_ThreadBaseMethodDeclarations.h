﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.ThreadBase
struct ThreadBase_t169;
// UnityThreading.Dispatcher
struct Dispatcher_t162;
// UnityThreading.Task
struct Task_t165;
// System.Action
struct Action_t16;
// UnityThreading.TaskBase
struct TaskBase_t163;

// System.Void UnityThreading.ThreadBase::.ctor()
extern "C" void ThreadBase__ctor_m576 (ThreadBase_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::.ctor(System.Boolean)
extern "C" void ThreadBase__ctor_m577 (ThreadBase_t169 * __this, bool ___autoStartThread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::.ctor(UnityThreading.Dispatcher)
extern "C" void ThreadBase__ctor_m578 (ThreadBase_t169 * __this, Dispatcher_t162 * ___targetDispatcher, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::.ctor(UnityThreading.Dispatcher,System.Boolean)
extern "C" void ThreadBase__ctor_m579 (ThreadBase_t169 * __this, Dispatcher_t162 * ___targetDispatcher, bool ___autoStartThread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.ThreadBase UnityThreading.ThreadBase::get_CurrentThread()
extern "C" ThreadBase_t169 * ThreadBase_get_CurrentThread_m580 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityThreading.ThreadBase::get_IsAlive()
extern "C" bool ThreadBase_get_IsAlive_m581 (ThreadBase_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityThreading.ThreadBase::get_ShouldStop()
extern "C" bool ThreadBase_get_ShouldStop_m582 (ThreadBase_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::Start()
extern "C" void ThreadBase_Start_m583 (ThreadBase_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::Exit()
extern "C" void ThreadBase_Exit_m584 (ThreadBase_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::Abort()
extern "C" void ThreadBase_Abort_m585 (ThreadBase_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::AbortWaitForSeconds(System.Single)
extern "C" void ThreadBase_AbortWaitForSeconds_m586 (ThreadBase_t169 * __this, float ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.Task UnityThreading.ThreadBase::Dispatch(System.Action)
extern "C" Task_t165 * ThreadBase_Dispatch_m587 (ThreadBase_t169 * __this, Action_t16 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::DispatchAndWait(System.Action)
extern "C" void ThreadBase_DispatchAndWait_m588 (ThreadBase_t169 * __this, Action_t16 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::DispatchAndWait(System.Action,System.Single)
extern "C" void ThreadBase_DispatchAndWait_m589 (ThreadBase_t169 * __this, Action_t16 * ___action, float ___timeOutSeconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.TaskBase UnityThreading.ThreadBase::Dispatch(UnityThreading.TaskBase)
extern "C" TaskBase_t163 * ThreadBase_Dispatch_m590 (ThreadBase_t169 * __this, TaskBase_t163 * ___taskBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::DispatchAndWait(UnityThreading.TaskBase)
extern "C" void ThreadBase_DispatchAndWait_m591 (ThreadBase_t169 * __this, TaskBase_t163 * ___taskBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::DispatchAndWait(UnityThreading.TaskBase,System.Single)
extern "C" void ThreadBase_DispatchAndWait_m592 (ThreadBase_t169 * __this, TaskBase_t163 * ___taskBase, float ___timeOutSeconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::DoInternal()
extern "C" void ThreadBase_DoInternal_m593 (ThreadBase_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ThreadBase::Dispose()
extern "C" void ThreadBase_Dispose_m594 (ThreadBase_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
