﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ProceduralTexturedQuad
struct ProceduralTexturedQuad_t102;
// UnityEngine.Texture
struct Texture_t103;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void ProceduralTexturedQuad::.ctor()
extern "C" void ProceduralTexturedQuad__ctor_m354 (ProceduralTexturedQuad_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralTexturedQuad::Start()
extern "C" void ProceduralTexturedQuad_Start_m355 (ProceduralTexturedQuad_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralTexturedQuad::GenerateMesh()
extern "C" void ProceduralTexturedQuad_GenerateMesh_m356 (ProceduralTexturedQuad_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProceduralTexturedQuad::HasTexture()
extern "C" bool ProceduralTexturedQuad_HasTexture_m357 (ProceduralTexturedQuad_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralTexturedQuad::SetTexture(UnityEngine.Texture)
extern "C" void ProceduralTexturedQuad_SetTexture_m358 (ProceduralTexturedQuad_t102 * __this, Texture_t103 * ___texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralTexturedQuad::SetModulationColor(UnityEngine.Color)
extern "C" void ProceduralTexturedQuad_SetModulationColor_m359 (ProceduralTexturedQuad_t102 * __this, Color_t9  ___color, const MethodInfo* method) IL2CPP_METHOD_ATTR;
