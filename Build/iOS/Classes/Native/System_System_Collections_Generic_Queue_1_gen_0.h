﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Queue`1<Common.Logger.Log>[]
struct Queue_1U5BU5D_t2571;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>
struct  Queue_1_t77  : public Object_t
{
	// T[] System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::_array
	Queue_1U5BU5D_t2571* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::_version
	int32_t ____version_4;
};
