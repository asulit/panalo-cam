﻿#pragma once
#include <stdint.h>
// System.Collections.Stack
struct Stack_t995;
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRef.h"
// System.Text.RegularExpressions.LinkStack
struct  LinkStack_t1702  : public LinkRef_t1698
{
	// System.Collections.Stack System.Text.RegularExpressions.LinkStack::stack
	Stack_t995 * ___stack_0;
};
