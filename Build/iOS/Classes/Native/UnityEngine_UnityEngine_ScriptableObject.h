﻿#pragma once
#include <stdint.h>
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.ScriptableObject
struct  ScriptableObject_t793  : public Object_t335
{
};
// Native definition for marshalling of: UnityEngine.ScriptableObject
struct ScriptableObject_t793_marshaled
{
};
