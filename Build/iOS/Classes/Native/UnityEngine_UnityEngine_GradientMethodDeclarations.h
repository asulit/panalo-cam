﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Gradient
struct Gradient_t818;
struct Gradient_t818_marshaled;

// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m4002 (Gradient_t818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m4003 (Gradient_t818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m4004 (Gradient_t818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m4005 (Gradient_t818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void Gradient_t818_marshal(const Gradient_t818& unmarshaled, Gradient_t818_marshaled& marshaled);
extern "C" void Gradient_t818_marshal_back(const Gradient_t818_marshaled& marshaled, Gradient_t818& unmarshaled);
extern "C" void Gradient_t818_marshal_cleanup(Gradient_t818_marshaled& marshaled);
