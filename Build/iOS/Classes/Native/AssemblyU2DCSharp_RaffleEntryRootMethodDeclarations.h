﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RaffleEntryRoot
struct RaffleEntryRoot_t224;
// Common.Signal.ISignalParameters
struct ISignalParameters_t115;

// System.Void RaffleEntryRoot::.ctor()
extern "C" void RaffleEntryRoot__ctor_m791 (RaffleEntryRoot_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleEntryRoot::Awake()
extern "C" void RaffleEntryRoot_Awake_m792 (RaffleEntryRoot_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleEntryRoot::Start()
extern "C" void RaffleEntryRoot_Start_m793 (RaffleEntryRoot_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleEntryRoot::OnEnable()
extern "C" void RaffleEntryRoot_OnEnable_m794 (RaffleEntryRoot_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleEntryRoot::OnDisable()
extern "C" void RaffleEntryRoot_OnDisable_m795 (RaffleEntryRoot_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleEntryRoot::OnRaffleEntryResult(Common.Signal.ISignalParameters)
extern "C" void RaffleEntryRoot_OnRaffleEntryResult_m796 (RaffleEntryRoot_t224 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleEntryRoot::OnClickedRetryButton()
extern "C" void RaffleEntryRoot_OnClickedRetryButton_m797 (RaffleEntryRoot_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
