﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_103.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m28454_gshared (InternalEnumerator_1_t3459 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m28454(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3459 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m28454_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28455_gshared (InternalEnumerator_1_t3459 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28455(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3459 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28455_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m28456_gshared (InternalEnumerator_1_t3459 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m28456(__this, method) (( void (*) (InternalEnumerator_1_t3459 *, const MethodInfo*))InternalEnumerator_1_Dispose_m28456_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m28457_gshared (InternalEnumerator_1_t3459 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m28457(__this, method) (( bool (*) (InternalEnumerator_1_t3459 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m28457_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C" TargetSearchResult_t1212  InternalEnumerator_1_get_Current_m28458_gshared (InternalEnumerator_1_t3459 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m28458(__this, method) (( TargetSearchResult_t1212  (*) (InternalEnumerator_1_t3459 *, const MethodInfo*))InternalEnumerator_1_get_Current_m28458_gshared)(__this, method)
