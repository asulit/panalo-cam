﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Object
struct Object_t335;
struct Object_t335_marshaled;

// System.Void UnityEngine.UI.Misc::Destroy(UnityEngine.Object)
extern "C" void Misc_Destroy_m2684 (Object_t * __this /* static, unused */, Object_t335 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Misc::DestroyImmediate(UnityEngine.Object)
extern "C" void Misc_DestroyImmediate_m2685 (Object_t * __this /* static, unused */, Object_t335 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
