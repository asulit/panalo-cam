﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordResult
struct WordResult_t1188;

// System.Void Vuforia.WordResult::.ctor()
extern "C" void WordResult__ctor_m5884 (WordResult_t1188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
