﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t2583;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t2584;
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern "C" void LinkedList_1__ctor_m15979_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m15979(__this, method) (( void (*) (LinkedList_1_t2583 *, const MethodInfo*))LinkedList_1__ctor_m15979_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1__ctor_m15981_gshared (LinkedList_1_t2583 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define LinkedList_1__ctor_m15981(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t2583 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))LinkedList_1__ctor_m15981_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15983_gshared (LinkedList_1_t2583 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15983(__this, ___value, method) (( void (*) (LinkedList_1_t2583 *, Object_t *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15983_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m15985_gshared (LinkedList_1_t2583 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m15985(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t2583 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m15985_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15987_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15987(__this, method) (( Object_t* (*) (LinkedList_1_t2583 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15987_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m15989_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m15989(__this, method) (( Object_t * (*) (LinkedList_1_t2583 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m15989_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15991_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15991(__this, method) (( bool (*) (LinkedList_1_t2583 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15991_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m15993_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m15993(__this, method) (( bool (*) (LinkedList_1_t2583 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m15993_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m15995_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m15995(__this, method) (( Object_t * (*) (LinkedList_1_t2583 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m15995_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_VerifyReferencedNode_m15997_gshared (LinkedList_1_t2583 * __this, LinkedListNode_1_t2584 * ___node, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m15997(__this, ___node, method) (( void (*) (LinkedList_1_t2583 *, LinkedListNode_1_t2584 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m15997_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C" LinkedListNode_1_t2584 * LinkedList_1_AddLast_m15998_gshared (LinkedList_1_t2583 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_AddLast_m15998(__this, ___value, method) (( LinkedListNode_1_t2584 * (*) (LinkedList_1_t2583 *, Object_t *, const MethodInfo*))LinkedList_1_AddLast_m15998_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C" void LinkedList_1_Clear_m16000_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m16000(__this, method) (( void (*) (LinkedList_1_t2583 *, const MethodInfo*))LinkedList_1_Clear_m16000_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C" bool LinkedList_1_Contains_m16002_gshared (LinkedList_1_t2583 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Contains_m16002(__this, ___value, method) (( bool (*) (LinkedList_1_t2583 *, Object_t *, const MethodInfo*))LinkedList_1_Contains_m16002_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void LinkedList_1_CopyTo_m16004_gshared (LinkedList_1_t2583 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_CopyTo_m16004(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t2583 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m16004_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C" LinkedListNode_1_t2584 * LinkedList_1_Find_m16006_gshared (LinkedList_1_t2583 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Find_m16006(__this, ___value, method) (( LinkedListNode_1_t2584 * (*) (LinkedList_1_t2583 *, Object_t *, const MethodInfo*))LinkedList_1_Find_m16006_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2585  LinkedList_1_GetEnumerator_m16008_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m16008(__this, method) (( Enumerator_t2585  (*) (LinkedList_1_t2583 *, const MethodInfo*))LinkedList_1_GetEnumerator_m16008_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1_GetObjectData_m16010_gshared (LinkedList_1_t2583 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m16010(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t2583 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))LinkedList_1_GetObjectData_m16010_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern "C" void LinkedList_1_OnDeserialization_m16012_gshared (LinkedList_1_t2583 * __this, Object_t * ___sender, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m16012(__this, ___sender, method) (( void (*) (LinkedList_1_t2583 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m16012_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C" bool LinkedList_1_Remove_m16014_gshared (LinkedList_1_t2583 * __this, Object_t * ___value, const MethodInfo* method);
#define LinkedList_1_Remove_m16014(__this, ___value, method) (( bool (*) (LinkedList_1_t2583 *, Object_t *, const MethodInfo*))LinkedList_1_Remove_m16014_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m16016_gshared (LinkedList_1_t2583 * __this, LinkedListNode_1_t2584 * ___node, const MethodInfo* method);
#define LinkedList_1_Remove_m16016(__this, ___node, method) (( void (*) (LinkedList_1_t2583 *, LinkedListNode_1_t2584 *, const MethodInfo*))LinkedList_1_Remove_m16016_gshared)(__this, ___node, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C" void LinkedList_1_RemoveLast_m16018_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveLast_m16018(__this, method) (( void (*) (LinkedList_1_t2583 *, const MethodInfo*))LinkedList_1_RemoveLast_m16018_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m16020_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m16020(__this, method) (( int32_t (*) (LinkedList_1_t2583 *, const MethodInfo*))LinkedList_1_get_Count_m16020_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C" LinkedListNode_1_t2584 * LinkedList_1_get_First_m16021_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m16021(__this, method) (( LinkedListNode_1_t2584 * (*) (LinkedList_1_t2583 *, const MethodInfo*))LinkedList_1_get_First_m16021_gshared)(__this, method)
