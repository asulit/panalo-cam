﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_102.h"
// Vuforia.VuforiaManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m28226_gshared (InternalEnumerator_1_t3442 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m28226(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3442 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m28226_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28227_gshared (InternalEnumerator_1_t3442 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28227(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3442 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28227_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m28228_gshared (InternalEnumerator_1_t3442 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m28228(__this, method) (( void (*) (InternalEnumerator_1_t3442 *, const MethodInfo*))InternalEnumerator_1_Dispose_m28228_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m28229_gshared (InternalEnumerator_1_t3442 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m28229(__this, method) (( bool (*) (InternalEnumerator_1_t3442 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m28229_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C" VirtualButtonData_t1135  InternalEnumerator_1_get_Current_m28230_gshared (InternalEnumerator_1_t3442 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m28230(__this, method) (( VirtualButtonData_t1135  (*) (InternalEnumerator_1_t3442 *, const MethodInfo*))InternalEnumerator_1_get_Current_m28230_gshared)(__this, method)
