﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2432_t1553_marshal(const U24ArrayTypeU2432_t1553& unmarshaled, U24ArrayTypeU2432_t1553_marshaled& marshaled);
extern "C" void U24ArrayTypeU2432_t1553_marshal_back(const U24ArrayTypeU2432_t1553_marshaled& marshaled, U24ArrayTypeU2432_t1553& unmarshaled);
extern "C" void U24ArrayTypeU2432_t1553_marshal_cleanup(U24ArrayTypeU2432_t1553_marshaled& marshaled);
