﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Xml.SimpleXmlNodeList
struct SimpleXmlNodeList_t145;
// Common.Xml.SimpleXmlNode
struct SimpleXmlNode_t142;

// System.Void Common.Xml.SimpleXmlNodeList::.ctor()
extern "C" void SimpleXmlNodeList__ctor_m479 (SimpleXmlNodeList_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlNodeList::Pop()
extern "C" SimpleXmlNode_t142 * SimpleXmlNodeList_Pop_m480 (SimpleXmlNodeList_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Xml.SimpleXmlNodeList::Push(Common.Xml.SimpleXmlNode)
extern "C" void SimpleXmlNodeList_Push_m481 (SimpleXmlNodeList_t145 * __this, SimpleXmlNode_t142 * ___node, const MethodInfo* method) IL2CPP_METHOD_ATTR;
