﻿#pragma once
#include <stdint.h>
// Common.Query.QuerySystemImplementation
struct QuerySystemImplementation_t109;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Query.QuerySystem
struct  QuerySystem_t108  : public Object_t
{
};
struct QuerySystem_t108_StaticFields{
	// Common.Query.QuerySystemImplementation Common.Query.QuerySystem::systemInstance
	QuerySystemImplementation_t109 * ___systemInstance_0;
};
