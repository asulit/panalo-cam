﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30MethodDeclarations.h"
#define Enumerator__ctor_m26712(__this, ___l, method) (( void (*) (Enumerator_t1333 *, List_1_t1183 *, const MethodInfo*))Enumerator__ctor_m14718_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26713(__this, method) (( Object_t * (*) (Enumerator_t1333 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14719_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::Dispose()
#define Enumerator_Dispose_m7342(__this, method) (( void (*) (Enumerator_t1333 *, const MethodInfo*))Enumerator_Dispose_m14720_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::VerifyState()
#define Enumerator_VerifyState_m26714(__this, method) (( void (*) (Enumerator_t1333 *, const MethodInfo*))Enumerator_VerifyState_m14721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::MoveNext()
#define Enumerator_MoveNext_m7341(__this, method) (( bool (*) (Enumerator_t1333 *, const MethodInfo*))Enumerator_MoveNext_m14722_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::get_Current()
#define Enumerator_get_Current_m7340(__this, method) (( WordResult_t1188 * (*) (Enumerator_t1333 *, const MethodInfo*))Enumerator_get_Current_m14723_gshared)(__this, method)
