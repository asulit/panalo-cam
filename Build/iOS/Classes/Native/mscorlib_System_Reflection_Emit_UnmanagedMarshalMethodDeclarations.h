﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1961;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1779;

// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
extern "C" MarshalAsAttribute_t1779 * UnmanagedMarshal_ToMarshalAsAttribute_m11981 (UnmanagedMarshal_t1961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
