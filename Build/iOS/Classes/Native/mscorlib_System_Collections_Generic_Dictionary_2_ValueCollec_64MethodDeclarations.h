﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct ValueCollection_t3264;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t3255;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_65.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m25145_gshared (ValueCollection_t3264 * __this, Dictionary_2_t3255 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m25145(__this, ___dictionary, method) (( void (*) (ValueCollection_t3264 *, Dictionary_2_t3255 *, const MethodInfo*))ValueCollection__ctor_m25145_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25146_gshared (ValueCollection_t3264 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25146(__this, ___item, method) (( void (*) (ValueCollection_t3264 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25146_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25147_gshared (ValueCollection_t3264 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25147(__this, method) (( void (*) (ValueCollection_t3264 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25147_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25148_gshared (ValueCollection_t3264 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25148(__this, ___item, method) (( bool (*) (ValueCollection_t3264 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25148_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25149_gshared (ValueCollection_t3264 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25149(__this, ___item, method) (( bool (*) (ValueCollection_t3264 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25149_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25150_gshared (ValueCollection_t3264 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25150(__this, method) (( Object_t* (*) (ValueCollection_t3264 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25150_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m25151_gshared (ValueCollection_t3264 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m25151(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3264 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m25151_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25152_gshared (ValueCollection_t3264 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25152(__this, method) (( Object_t * (*) (ValueCollection_t3264 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25152_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25153_gshared (ValueCollection_t3264 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25153(__this, method) (( bool (*) (ValueCollection_t3264 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25153_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25154_gshared (ValueCollection_t3264 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25154(__this, method) (( bool (*) (ValueCollection_t3264 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25154_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m25155_gshared (ValueCollection_t3264 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m25155(__this, method) (( Object_t * (*) (ValueCollection_t3264 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m25155_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m25156_gshared (ValueCollection_t3264 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m25156(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3264 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m25156_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::GetEnumerator()
extern "C" Enumerator_t3265  ValueCollection_GetEnumerator_m25157_gshared (ValueCollection_t3264 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m25157(__this, method) (( Enumerator_t3265  (*) (ValueCollection_t3264 *, const MethodInfo*))ValueCollection_GetEnumerator_m25157_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m25158_gshared (ValueCollection_t3264 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m25158(__this, method) (( int32_t (*) (ValueCollection_t3264 *, const MethodInfo*))ValueCollection_get_Count_m25158_gshared)(__this, method)
