﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.CameraDevice/FocusMode>
struct EqualityComparer_1_t2729;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.CameraDevice/FocusMode>
struct  EqualityComparer_1_t2729  : public Object_t
{
};
struct EqualityComparer_1_t2729_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.CameraDevice/FocusMode>::_default
	EqualityComparer_1_t2729 * ____default_0;
};
