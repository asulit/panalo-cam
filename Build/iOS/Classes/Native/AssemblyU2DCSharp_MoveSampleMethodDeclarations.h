﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MoveSample
struct MoveSample_t246;

// System.Void MoveSample::.ctor()
extern "C" void MoveSample__ctor_m888 (MoveSample_t246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveSample::Start()
extern "C" void MoveSample_Start_m889 (MoveSample_t246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
