﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.ServerObjectTerminatorSink
struct ServerObjectTerminatorSink_t2120;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t2077;

// System.Void System.Runtime.Remoting.Messaging.ServerObjectTerminatorSink::.ctor(System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" void ServerObjectTerminatorSink__ctor_m12573 (ServerObjectTerminatorSink_t2120 * __this, Object_t * ___nextSink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
