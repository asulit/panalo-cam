﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Transform_1_t3449;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.VuforiaManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m28285_gshared (Transform_1_t3449 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m28285(__this, ___object, ___method, method) (( void (*) (Transform_1_t3449 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m28285_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Invoke(TKey,TValue)
extern "C" VirtualButtonData_t1135  Transform_1_Invoke_m28286_gshared (Transform_1_t3449 * __this, int32_t ___key, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m28286(__this, ___key, ___value, method) (( VirtualButtonData_t1135  (*) (Transform_1_t3449 *, int32_t, VirtualButtonData_t1135 , const MethodInfo*))Transform_1_Invoke_m28286_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m28287_gshared (Transform_1_t3449 * __this, int32_t ___key, VirtualButtonData_t1135  ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m28287(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3449 *, int32_t, VirtualButtonData_t1135 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m28287_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,Vuforia.VuforiaManagerImpl/VirtualButtonData>::EndInvoke(System.IAsyncResult)
extern "C" VirtualButtonData_t1135  Transform_1_EndInvoke_m28288_gshared (Transform_1_t3449 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m28288(__this, ___result, method) (( VirtualButtonData_t1135  (*) (Transform_1_t3449 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m28288_gshared)(__this, ___result, method)
