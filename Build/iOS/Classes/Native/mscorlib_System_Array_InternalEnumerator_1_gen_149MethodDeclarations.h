﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.DateTime>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_149.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29844_gshared (InternalEnumerator_1_t3591 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29844(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3591 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29844_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845_gshared (InternalEnumerator_1_t3591 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3591 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29845_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29846_gshared (InternalEnumerator_1_t3591 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29846(__this, method) (( void (*) (InternalEnumerator_1_t3591 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29847_gshared (InternalEnumerator_1_t3591 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29847(__this, method) (( bool (*) (InternalEnumerator_1_t3591 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29847_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern "C" DateTime_t74  InternalEnumerator_1_get_Current_m29848_gshared (InternalEnumerator_1_t3591 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29848(__this, method) (( DateTime_t74  (*) (InternalEnumerator_1_t3591 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29848_gshared)(__this, method)
