﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.PasswordDeriveBytes
struct PasswordDeriveBytes_t419;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t139;

// System.Void System.Security.Cryptography.PasswordDeriveBytes::.ctor(System.String,System.Byte[])
extern "C" void PasswordDeriveBytes__ctor_m1548 (PasswordDeriveBytes_t419 * __this, String_t* ___strPassword, ByteU5BU5D_t139* ___rgbSalt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::Finalize()
extern "C" void PasswordDeriveBytes_Finalize_m13066 (PasswordDeriveBytes_t419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::Prepare(System.String,System.Byte[],System.String,System.Int32)
extern "C" void PasswordDeriveBytes_Prepare_m13067 (PasswordDeriveBytes_t419 * __this, String_t* ___strPassword, ByteU5BU5D_t139* ___rgbSalt, String_t* ___strHashName, int32_t ___iterations, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::Prepare(System.Byte[],System.Byte[],System.String,System.Int32)
extern "C" void PasswordDeriveBytes_Prepare_m13068 (PasswordDeriveBytes_t419 * __this, ByteU5BU5D_t139* ___password, ByteU5BU5D_t139* ___rgbSalt, String_t* ___strHashName, int32_t ___iterations, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::set_HashName(System.String)
extern "C" void PasswordDeriveBytes_set_HashName_m13069 (PasswordDeriveBytes_t419 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::set_IterationCount(System.Int32)
extern "C" void PasswordDeriveBytes_set_IterationCount_m13070 (PasswordDeriveBytes_t419 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::set_Salt(System.Byte[])
extern "C" void PasswordDeriveBytes_set_Salt_m13071 (PasswordDeriveBytes_t419 * __this, ByteU5BU5D_t139* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::GetBytes(System.Int32)
extern "C" ByteU5BU5D_t139* PasswordDeriveBytes_GetBytes_m13072 (PasswordDeriveBytes_t419 * __this, int32_t ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.PasswordDeriveBytes::Reset()
extern "C" void PasswordDeriveBytes_Reset_m13073 (PasswordDeriveBytes_t419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
