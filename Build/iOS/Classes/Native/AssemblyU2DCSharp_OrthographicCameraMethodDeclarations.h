﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// OrthographicCamera
struct OrthographicCamera_t4;
// OrthographicCameraObserver
struct OrthographicCameraObserver_t336;

// System.Void OrthographicCamera::.ctor()
extern "C" void OrthographicCamera__ctor_m29 (OrthographicCamera_t4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrthographicCamera::Awake()
extern "C" void OrthographicCamera_Awake_m30 (OrthographicCamera_t4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrthographicCamera::Start()
extern "C" void OrthographicCamera_Start_m31 (OrthographicCamera_t4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrthographicCamera::AddObserver(OrthographicCameraObserver)
extern "C" void OrthographicCamera_AddObserver_m32 (OrthographicCamera_t4 * __this, Object_t * ___observer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrthographicCamera::SetOrthoSize(System.Single)
extern "C" void OrthographicCamera_SetOrthoSize_m33 (OrthographicCamera_t4 * __this, float ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
