﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.CameraDevice/FocusMode>
struct DefaultComparer_t2730;
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.CameraDevice/FocusMode>::.ctor()
extern "C" void DefaultComparer__ctor_m17835_gshared (DefaultComparer_t2730 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m17835(__this, method) (( void (*) (DefaultComparer_t2730 *, const MethodInfo*))DefaultComparer__ctor_m17835_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.CameraDevice/FocusMode>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m17836_gshared (DefaultComparer_t2730 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m17836(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2730 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m17836_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.CameraDevice/FocusMode>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m17837_gshared (DefaultComparer_t2730 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m17837(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2730 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m17837_gshared)(__this, ___x, ___y, method)
