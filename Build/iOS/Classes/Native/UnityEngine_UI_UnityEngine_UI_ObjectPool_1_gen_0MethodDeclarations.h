﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t2815;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t2816;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C" void ObjectPool_1__ctor_m18958_gshared (ObjectPool_1_t2815 * __this, UnityAction_1_t2816 * ___actionOnGet, UnityAction_1_t2816 * ___actionOnRelease, const MethodInfo* method);
#define ObjectPool_1__ctor_m18958(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t2815 *, UnityAction_1_t2816 *, UnityAction_1_t2816 *, const MethodInfo*))ObjectPool_1__ctor_m18958_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
extern "C" int32_t ObjectPool_1_get_countAll_m18960_gshared (ObjectPool_1_t2815 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countAll_m18960(__this, method) (( int32_t (*) (ObjectPool_1_t2815 *, const MethodInfo*))ObjectPool_1_get_countAll_m18960_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
extern "C" void ObjectPool_1_set_countAll_m18962_gshared (ObjectPool_1_t2815 * __this, int32_t ___value, const MethodInfo* method);
#define ObjectPool_1_set_countAll_m18962(__this, ___value, method) (( void (*) (ObjectPool_1_t2815 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m18962_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
extern "C" int32_t ObjectPool_1_get_countActive_m18964_gshared (ObjectPool_1_t2815 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countActive_m18964(__this, method) (( int32_t (*) (ObjectPool_1_t2815 *, const MethodInfo*))ObjectPool_1_get_countActive_m18964_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
extern "C" int32_t ObjectPool_1_get_countInactive_m18966_gshared (ObjectPool_1_t2815 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countInactive_m18966(__this, method) (( int32_t (*) (ObjectPool_1_t2815 *, const MethodInfo*))ObjectPool_1_get_countInactive_m18966_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
extern "C" Object_t * ObjectPool_1_Get_m18968_gshared (ObjectPool_1_t2815 * __this, const MethodInfo* method);
#define ObjectPool_1_Get_m18968(__this, method) (( Object_t * (*) (ObjectPool_1_t2815 *, const MethodInfo*))ObjectPool_1_Get_m18968_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
extern "C" void ObjectPool_1_Release_m18970_gshared (ObjectPool_1_t2815 * __this, Object_t * ___element, const MethodInfo* method);
#define ObjectPool_1_Release_m18970(__this, ___element, method) (( void (*) (ObjectPool_1_t2815 *, Object_t *, const MethodInfo*))ObjectPool_1_Release_m18970_gshared)(__this, ___element, method)
