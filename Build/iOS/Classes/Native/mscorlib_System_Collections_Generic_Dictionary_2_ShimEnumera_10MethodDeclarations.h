﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct ShimEnumerator_t3451;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1372;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m28297_gshared (ShimEnumerator_t3451 * __this, Dictionary_2_t1372 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m28297(__this, ___host, method) (( void (*) (ShimEnumerator_t3451 *, Dictionary_2_t1372 *, const MethodInfo*))ShimEnumerator__ctor_m28297_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m28298_gshared (ShimEnumerator_t3451 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m28298(__this, method) (( bool (*) (ShimEnumerator_t3451 *, const MethodInfo*))ShimEnumerator_MoveNext_m28298_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m28299_gshared (ShimEnumerator_t3451 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m28299(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t3451 *, const MethodInfo*))ShimEnumerator_get_Entry_m28299_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m28300_gshared (ShimEnumerator_t3451 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m28300(__this, method) (( Object_t * (*) (ShimEnumerator_t3451 *, const MethodInfo*))ShimEnumerator_get_Key_m28300_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m28301_gshared (ShimEnumerator_t3451 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m28301(__this, method) (( Object_t * (*) (ShimEnumerator_t3451 *, const MethodInfo*))ShimEnumerator_get_Value_m28301_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m28302_gshared (ShimEnumerator_t3451 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m28302(__this, method) (( Object_t * (*) (ShimEnumerator_t3451 *, const MethodInfo*))ShimEnumerator_get_Current_m28302_gshared)(__this, method)
