﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.Eyewear/EyeID
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_EyeID.h"
// Vuforia.Eyewear/EyeID
struct  EyeID_t1079 
{
	// System.Int32 Vuforia.Eyewear/EyeID::value__
	int32_t ___value___1;
};
