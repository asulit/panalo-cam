﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.TextAsset
struct TextAsset_t148;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t144;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t68;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Common.XmlVariables
struct  XmlVariables_t149  : public MonoBehaviour_t5
{
	// UnityEngine.TextAsset Common.XmlVariables::xmlFile
	TextAsset_t148 * ___xmlFile_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Common.XmlVariables::varMap
	Dictionary_2_t144 * ___varMap_8;
};
struct XmlVariables_t149_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Common.XmlVariables::<>f__switch$map1
	Dictionary_2_t68 * ___U3CU3Ef__switchU24map1_9;
};
