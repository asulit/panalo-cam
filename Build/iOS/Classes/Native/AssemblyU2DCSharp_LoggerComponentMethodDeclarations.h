﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LoggerComponent
struct LoggerComponent_t78;

// System.Void LoggerComponent::.ctor()
extern "C" void LoggerComponent__ctor_m269 (LoggerComponent_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoggerComponent::Awake()
extern "C" void LoggerComponent_Awake_m270 (LoggerComponent_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoggerComponent::Update()
extern "C" void LoggerComponent_Update_m271 (LoggerComponent_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
