﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void TargetFinderState_t1213_marshal(const TargetFinderState_t1213& unmarshaled, TargetFinderState_t1213_marshaled& marshaled);
extern "C" void TargetFinderState_t1213_marshal_back(const TargetFinderState_t1213_marshaled& marshaled, TargetFinderState_t1213& unmarshaled);
extern "C" void TargetFinderState_t1213_marshal_cleanup(TargetFinderState_t1213_marshaled& marshaled);
