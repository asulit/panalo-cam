﻿#pragma once
#include <stdint.h>
// SwarmItem[]
struct SwarmItemU5BU5D_t2597;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Stack`1<SwarmItem>
struct  Stack_1_t153  : public Object_t
{
	// T[] System.Collections.Generic.Stack`1<SwarmItem>::_array
	SwarmItemU5BU5D_t2597* ____array_1;
	// System.Int32 System.Collections.Generic.Stack`1<SwarmItem>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.Stack`1<SwarmItem>::_version
	int32_t ____version_3;
};
