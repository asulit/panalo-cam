﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Xml.SimpleXmlNode
struct SimpleXmlNode_t142;
// System.String
struct String_t;
// Common.Xml.SimpleXmlNode/NodeProcessor
struct NodeProcessor_t141;

// System.Void Common.Xml.SimpleXmlNode::.ctor()
extern "C" void SimpleXmlNode__ctor_m469 (SimpleXmlNode_t142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlNode::FindFirstNodeInChildren(System.String)
extern "C" SimpleXmlNode_t142 * SimpleXmlNode_FindFirstNodeInChildren_m470 (SimpleXmlNode_t142 * __this, String_t* ___tagName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlNode::FindFirstNodeInChildren(Common.Xml.SimpleXmlNode,System.String)
extern "C" SimpleXmlNode_t142 * SimpleXmlNode_FindFirstNodeInChildren_m471 (SimpleXmlNode_t142 * __this, SimpleXmlNode_t142 * ___node, String_t* ___tagName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Xml.SimpleXmlNode::HasAttribute(System.String)
extern "C" bool SimpleXmlNode_HasAttribute_m472 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Xml.SimpleXmlNode::GetAttribute(System.String)
extern "C" String_t* SimpleXmlNode_GetAttribute_m473 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Common.Xml.SimpleXmlNode::GetAttributeAsInt(System.String)
extern "C" int32_t SimpleXmlNode_GetAttributeAsInt_m474 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Common.Xml.SimpleXmlNode::GetAttributeAsFloat(System.String)
extern "C" float SimpleXmlNode_GetAttributeAsFloat_m475 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Xml.SimpleXmlNode::GetAttributeAsBool(System.String)
extern "C" bool SimpleXmlNode_GetAttributeAsBool_m476 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Xml.SimpleXmlNode::ContainsAttribute(System.String)
extern "C" bool SimpleXmlNode_ContainsAttribute_m477 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Xml.SimpleXmlNode::TraverseChildren(Common.Xml.SimpleXmlNode/NodeProcessor)
extern "C" void SimpleXmlNode_TraverseChildren_m478 (SimpleXmlNode_t142 * __this, NodeProcessor_t141 * ___visitor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
