﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t82;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.MaskOutAbstractBehaviour
struct  MaskOutAbstractBehaviour_t295  : public MonoBehaviour_t5
{
	// UnityEngine.Material Vuforia.MaskOutAbstractBehaviour::maskMaterial
	Material_t82 * ___maskMaterial_2;
};
