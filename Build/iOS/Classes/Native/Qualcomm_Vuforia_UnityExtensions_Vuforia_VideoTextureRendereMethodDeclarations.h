﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoTextureRendererAbstractBehaviour
struct VideoTextureRendererAbstractBehaviour_t317;

// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
extern "C" void VideoTextureRendererAbstractBehaviour_Awake_m7110 (VideoTextureRendererAbstractBehaviour_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
extern "C" void VideoTextureRendererAbstractBehaviour_Start_m7111 (VideoTextureRendererAbstractBehaviour_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
extern "C" void VideoTextureRendererAbstractBehaviour_Update_m7112 (VideoTextureRendererAbstractBehaviour_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
extern "C" void VideoTextureRendererAbstractBehaviour_OnDestroy_m7113 (VideoTextureRendererAbstractBehaviour_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C" void VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m7114 (VideoTextureRendererAbstractBehaviour_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
extern "C" void VideoTextureRendererAbstractBehaviour__ctor_m1876 (VideoTextureRendererAbstractBehaviour_t317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
