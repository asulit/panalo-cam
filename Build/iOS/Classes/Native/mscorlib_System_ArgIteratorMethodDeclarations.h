﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Object
struct Object_t;
// System.ArgIterator
#include "mscorlib_System_ArgIterator.h"

// System.Boolean System.ArgIterator::Equals(System.Object)
extern "C" bool ArgIterator_Equals_m10393 (ArgIterator_t1793 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ArgIterator::GetHashCode()
extern "C" int32_t ArgIterator_GetHashCode_m10394 (ArgIterator_t1793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
