﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
struct Dictionary_2_t2688;
// System.Collections.Generic.IEqualityComparer`1<ESubScreens>
struct IEqualityComparer_1_t2687;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.ICollection
struct ICollection_t1751;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>[]
struct KeyValuePair_2U5BU5D_t3655;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>
struct IEnumerator_1_t3656;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1606;
// System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>
struct KeyCollection_t2693;
// System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>
struct ValueCollection_t2697;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
// System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m17326_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m17326(__this, method) (( void (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2__ctor_m17326_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17328_gshared (Dictionary_2_t2688 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17328(__this, ___comparer, method) (( void (*) (Dictionary_2_t2688 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17328_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m17330_gshared (Dictionary_2_t2688 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m17330(__this, ___capacity, method) (( void (*) (Dictionary_2_t2688 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m17330_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m17332_gshared (Dictionary_2_t2688 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m17332(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2688 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m17332_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m17334_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m17334(__this, method) (( Object_t * (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m17334_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17336_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17336(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m17336_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17338_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17338(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2688 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m17338_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17340_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m17340(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2688 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m17340_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17342_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m17342(__this, ___key, method) (( bool (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m17342_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17344_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m17344(__this, ___key, method) (( void (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m17344_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17346_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17346(__this, method) (( bool (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17346_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17348_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17348(__this, method) (( Object_t * (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17348_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17350_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17350(__this, method) (( bool (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17350_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17352_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2_t2690  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17352(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2688 *, KeyValuePair_2_t2690 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17352_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17354_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2_t2690  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17354(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2688 *, KeyValuePair_2_t2690 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17354_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17356_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2U5BU5D_t3655* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17356(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2688 *, KeyValuePair_2U5BU5D_t3655*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17356_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17358_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2_t2690  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17358(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2688 *, KeyValuePair_2_t2690 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17358_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17360_gshared (Dictionary_2_t2688 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17360(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m17360_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17362_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17362(__this, method) (( Object_t * (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17362_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17364_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17364(__this, method) (( Object_t* (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17364_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17366_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17366(__this, method) (( Object_t * (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17366_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17368_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m17368(__this, method) (( int32_t (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_get_Count_m17368_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m17370_gshared (Dictionary_2_t2688 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m17370(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2688 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m17370_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m17372_gshared (Dictionary_2_t2688 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m17372(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2688 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m17372_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m17374_gshared (Dictionary_2_t2688 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m17374(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2688 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m17374_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m17376_gshared (Dictionary_2_t2688 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m17376(__this, ___size, method) (( void (*) (Dictionary_2_t2688 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m17376_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m17378_gshared (Dictionary_2_t2688 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m17378(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m17378_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2690  Dictionary_2_make_pair_m17380_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m17380(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2690  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m17380_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m17382_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m17382(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m17382_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m17384_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m17384(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m17384_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17386_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2U5BU5D_t3655* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m17386(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2688 *, KeyValuePair_2U5BU5D_t3655*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m17386_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m17388_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m17388(__this, method) (( void (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_Resize_m17388_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m17390_gshared (Dictionary_2_t2688 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m17390(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2688 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m17390_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m17392_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m17392(__this, method) (( void (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_Clear_m17392_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m17394_gshared (Dictionary_2_t2688 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m17394(__this, ___key, method) (( bool (*) (Dictionary_2_t2688 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m17394_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17396_gshared (Dictionary_2_t2688 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m17396(__this, ___value, method) (( bool (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m17396_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m17398_gshared (Dictionary_2_t2688 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m17398(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2688 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m17398_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m17400_gshared (Dictionary_2_t2688 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m17400(__this, ___sender, method) (( void (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m17400_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m17402_gshared (Dictionary_2_t2688 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m17402(__this, ___key, method) (( bool (*) (Dictionary_2_t2688 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m17402_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m17404_gshared (Dictionary_2_t2688 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m17404(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2688 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m17404_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Keys()
extern "C" KeyCollection_t2693 * Dictionary_2_get_Keys_m17406_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m17406(__this, method) (( KeyCollection_t2693 * (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_get_Keys_m17406_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Values()
extern "C" ValueCollection_t2697 * Dictionary_2_get_Values_m17408_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m17408(__this, method) (( ValueCollection_t2697 * (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_get_Values_m17408_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m17410_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m17410(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m17410_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m17412_gshared (Dictionary_2_t2688 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m17412(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m17412_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17414_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2_t2690  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m17414(__this, ___pair, method) (( bool (*) (Dictionary_2_t2688 *, KeyValuePair_2_t2690 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m17414_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::GetEnumerator()
extern "C" Enumerator_t2695  Dictionary_2_GetEnumerator_m17416_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m17416(__this, method) (( Enumerator_t2695  (*) (Dictionary_2_t2688 *, const MethodInfo*))Dictionary_2_GetEnumerator_m17416_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m17418_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m17418(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m17418_gshared)(__this /* static, unused */, ___key, ___value, method)
