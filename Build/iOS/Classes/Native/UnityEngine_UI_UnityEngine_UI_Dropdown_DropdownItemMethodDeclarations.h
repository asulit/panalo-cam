﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Dropdown/DropdownItem
struct DropdownItem_t555;
// UnityEngine.UI.Text
struct Text_t196;
// UnityEngine.UI.Image
struct Image_t213;
// UnityEngine.RectTransform
struct RectTransform_t556;
// UnityEngine.UI.Toggle
struct Toggle_t557;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t517;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t480;

// System.Void UnityEngine.UI.Dropdown/DropdownItem::.ctor()
extern "C" void DropdownItem__ctor_m2289 (DropdownItem_t555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Text UnityEngine.UI.Dropdown/DropdownItem::get_text()
extern "C" Text_t196 * DropdownItem_get_text_m2290 (DropdownItem_t555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_text(UnityEngine.UI.Text)
extern "C" void DropdownItem_set_text_m2291 (DropdownItem_t555 * __this, Text_t196 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Image UnityEngine.UI.Dropdown/DropdownItem::get_image()
extern "C" Image_t213 * DropdownItem_get_image_m2292 (DropdownItem_t555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_image(UnityEngine.UI.Image)
extern "C" void DropdownItem_set_image_m2293 (DropdownItem_t555 * __this, Image_t213 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.Dropdown/DropdownItem::get_rectTransform()
extern "C" RectTransform_t556 * DropdownItem_get_rectTransform_m2294 (DropdownItem_t555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_rectTransform(UnityEngine.RectTransform)
extern "C" void DropdownItem_set_rectTransform_m2295 (DropdownItem_t555 * __this, RectTransform_t556 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Toggle UnityEngine.UI.Dropdown/DropdownItem::get_toggle()
extern "C" Toggle_t557 * DropdownItem_get_toggle_m2296 (DropdownItem_t555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_toggle(UnityEngine.UI.Toggle)
extern "C" void DropdownItem_set_toggle_m2297 (DropdownItem_t555 * __this, Toggle_t557 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C" void DropdownItem_OnPointerEnter_m2298 (DropdownItem_t555 * __this, PointerEventData_t517 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::OnCancel(UnityEngine.EventSystems.BaseEventData)
extern "C" void DropdownItem_OnCancel_m2299 (DropdownItem_t555 * __this, BaseEventData_t480 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
