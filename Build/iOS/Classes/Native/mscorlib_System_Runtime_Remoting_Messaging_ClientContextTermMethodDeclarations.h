﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.ClientContextTerminatorSink
struct ClientContextTerminatorSink_t2101;
// System.Runtime.Remoting.Contexts.Context
struct Context_t2074;

// System.Void System.Runtime.Remoting.Messaging.ClientContextTerminatorSink::.ctor(System.Runtime.Remoting.Contexts.Context)
extern "C" void ClientContextTerminatorSink__ctor_m12458 (ClientContextTerminatorSink_t2101 * __this, Context_t2074 * ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
