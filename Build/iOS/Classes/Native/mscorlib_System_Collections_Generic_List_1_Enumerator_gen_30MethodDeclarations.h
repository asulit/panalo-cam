﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t344;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m14718_gshared (Enumerator_t2469 * __this, List_1_t344 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m14718(__this, ___l, method) (( void (*) (Enumerator_t2469 *, List_1_t344 *, const MethodInfo*))Enumerator__ctor_m14718_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14719_gshared (Enumerator_t2469 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14719(__this, method) (( Object_t * (*) (Enumerator_t2469 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14719_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m14720_gshared (Enumerator_t2469 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14720(__this, method) (( void (*) (Enumerator_t2469 *, const MethodInfo*))Enumerator_Dispose_m14720_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m14721_gshared (Enumerator_t2469 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m14721(__this, method) (( void (*) (Enumerator_t2469 *, const MethodInfo*))Enumerator_VerifyState_m14721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14722_gshared (Enumerator_t2469 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14722(__this, method) (( bool (*) (Enumerator_t2469 *, const MethodInfo*))Enumerator_MoveNext_m14722_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m14723_gshared (Enumerator_t2469 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14723(__this, method) (( Object_t * (*) (Enumerator_t2469 *, const MethodInfo*))Enumerator_get_Current_m14723_gshared)(__this, method)
