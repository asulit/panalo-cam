﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t344;

// System.Void UnityEngine.UI.ListPool`1<System.Object>::.cctor()
extern "C" void ListPool_1__cctor_m19084_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m19084(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m19084_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Object>::Get()
extern "C" List_1_t344 * ListPool_1_Get_m19085_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m19085(__this /* static, unused */, method) (( List_1_t344 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m19085_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m19086_gshared (Object_t * __this /* static, unused */, List_1_t344 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m19086(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t344 *, const MethodInfo*))ListPool_1_Release_m19086_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<System.Object>::<s_ListPool>m__15(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__15_m19088_gshared (Object_t * __this /* static, unused */, List_1_t344 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__15_m19088(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t344 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__15_m19088_gshared)(__this /* static, unused */, ___l, method)
