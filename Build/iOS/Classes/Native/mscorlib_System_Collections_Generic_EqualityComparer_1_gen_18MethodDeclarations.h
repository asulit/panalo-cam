﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct EqualityComparer_1_t3436;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor()
extern "C" void EqualityComparer_1__ctor_m28161_gshared (EqualityComparer_1_t3436 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m28161(__this, method) (( void (*) (EqualityComparer_1_t3436 *, const MethodInfo*))EqualityComparer_1__ctor_m28161_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::.cctor()
extern "C" void EqualityComparer_1__cctor_m28162_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m28162(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m28162_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m28163_gshared (EqualityComparer_1_t3436 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m28163(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3436 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m28163_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m28164_gshared (EqualityComparer_1_t3436 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m28164(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3436 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m28164_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Default()
extern "C" EqualityComparer_1_t3436 * EqualityComparer_1_get_Default_m28165_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m28165(__this /* static, unused */, method) (( EqualityComparer_1_t3436 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m28165_gshared)(__this /* static, unused */, method)
