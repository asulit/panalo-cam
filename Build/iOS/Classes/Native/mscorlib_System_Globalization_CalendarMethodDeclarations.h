﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Globalization.Calendar
struct Calendar_t1902;
// System.String[]
struct StringU5BU5D_t137;

// System.Void System.Globalization.Calendar::.ctor()
extern "C" void Calendar__ctor_m11231 (Calendar_t1902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::CheckReadOnly()
extern "C" void Calendar_CheckReadOnly_m11232 (Calendar_t1902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.Calendar::get_EraNames()
extern "C" StringU5BU5D_t137* Calendar_get_EraNames_m11233 (Calendar_t1902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
