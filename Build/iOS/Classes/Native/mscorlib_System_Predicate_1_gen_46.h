﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t139;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Byte[]>
struct  Predicate_1_t3115  : public MulticastDelegate_t28
{
};
