﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern const Il2CppType Object_t_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Types[] = { &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0 = { 1, GenInst_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 4, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType OrthographicCameraObserver_t336_0_0_0;
static const Il2CppType* GenInst_OrthographicCameraObserver_t336_0_0_0_Types[] = { &OrthographicCameraObserver_t336_0_0_0 };
extern const Il2CppGenericInst GenInst_OrthographicCameraObserver_t336_0_0_0 = { 1, GenInst_OrthographicCameraObserver_t336_0_0_0_Types };
extern const Il2CppType Camera_t6_0_0_0;
static const Il2CppType* GenInst_Camera_t6_0_0_0_Types[] = { &Camera_t6_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t6_0_0_0 = { 1, GenInst_Camera_t6_0_0_0_Types };
extern const Il2CppType Renderer_t367_0_0_0;
static const Il2CppType* GenInst_Renderer_t367_0_0_0_Types[] = { &Renderer_t367_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t367_0_0_0 = { 1, GenInst_Renderer_t367_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t5_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t5_0_0_0_Types[] = { &MonoBehaviour_t5_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t5_0_0_0 = { 1, GenInst_MonoBehaviour_t5_0_0_0_Types };
extern const Il2CppType Collider_t369_0_0_0;
static const Il2CppType* GenInst_Collider_t369_0_0_0_Types[] = { &Collider_t369_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t369_0_0_0 = { 1, GenInst_Collider_t369_0_0_0_Types };
extern const Il2CppType Enum_t21_0_0_0;
static const Il2CppType* GenInst_Enum_t21_0_0_0_Types[] = { &Enum_t21_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t21_0_0_0 = { 1, GenInst_Enum_t21_0_0_0_Types };
extern const Il2CppType TextMesh_t26_0_0_0;
static const Il2CppType* GenInst_TextMesh_t26_0_0_0_Types[] = { &TextMesh_t26_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMesh_t26_0_0_0 = { 1, GenInst_TextMesh_t26_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType FsmState_t29_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FsmState_t29_0_0_0_Types[] = { &String_t_0_0_0, &FsmState_t29_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FsmState_t29_0_0_0 = { 2, GenInst_String_t_0_0_0_FsmState_t29_0_0_0_Types };
extern const Il2CppType FsmAction_t51_0_0_0;
static const Il2CppType* GenInst_FsmAction_t51_0_0_0_Types[] = { &FsmAction_t51_0_0_0 };
extern const Il2CppGenericInst GenInst_FsmAction_t51_0_0_0 = { 1, GenInst_FsmAction_t51_0_0_0_Types };
extern const Il2CppType OrthographicCamera_t4_0_0_0;
static const Il2CppType* GenInst_OrthographicCamera_t4_0_0_0_Types[] = { &OrthographicCamera_t4_0_0_0 };
extern const Il2CppGenericInst GenInst_OrthographicCamera_t4_0_0_0 = { 1, GenInst_OrthographicCamera_t4_0_0_0_Types };
extern const Il2CppType InputLayer_t56_0_0_0;
static const Il2CppType* GenInst_InputLayer_t56_0_0_0_Types[] = { &InputLayer_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_InputLayer_t56_0_0_0 = { 1, GenInst_InputLayer_t56_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType Int32_t372_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t372_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t372_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t372_0_0_0_Types };
extern const Il2CppType Log_t72_0_0_0;
static const Il2CppType* GenInst_Log_t72_0_0_0_Types[] = { &Log_t72_0_0_0 };
extern const Il2CppGenericInst GenInst_Log_t72_0_0_0 = { 1, GenInst_Log_t72_0_0_0_Types };
extern const Il2CppType Queue_1_t76_0_0_0;
static const Il2CppType* GenInst_Queue_1_t76_0_0_0_Types[] = { &Queue_1_t76_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t76_0_0_0 = { 1, GenInst_Queue_1_t76_0_0_0_Types };
extern const Il2CppType NotificationInstance_t85_0_0_0;
static const Il2CppType* GenInst_NotificationInstance_t85_0_0_0_Types[] = { &NotificationInstance_t85_0_0_0 };
extern const Il2CppGenericInst GenInst_NotificationInstance_t85_0_0_0 = { 1, GenInst_NotificationInstance_t85_0_0_0_Types };
extern const Il2CppType NotificationHandler_t347_0_0_0;
static const Il2CppType* GenInst_NotificationHandler_t347_0_0_0_Types[] = { &NotificationHandler_t347_0_0_0 };
extern const Il2CppGenericInst GenInst_NotificationHandler_t347_0_0_0 = { 1, GenInst_NotificationHandler_t347_0_0_0_Types };
extern const Il2CppType MeshFilter_t398_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t398_0_0_0_Types[] = { &MeshFilter_t398_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t398_0_0_0 = { 1, GenInst_MeshFilter_t398_0_0_0_Types };
extern const Il2CppType MeshRenderer_t402_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t402_0_0_0_Types[] = { &MeshRenderer_t402_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t402_0_0_0 = { 1, GenInst_MeshRenderer_t402_0_0_0_Types };
extern const Il2CppType QueryResultResolver_t328_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_QueryResultResolver_t328_0_0_0_Types[] = { &String_t_0_0_0, &QueryResultResolver_t328_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_QueryResultResolver_t328_0_0_0 = { 2, GenInst_String_t_0_0_0_QueryResultResolver_t328_0_0_0_Types };
extern const Il2CppType SwarmItem_t124_0_0_0;
static const Il2CppType* GenInst_SwarmItem_t124_0_0_0_Types[] = { &SwarmItem_t124_0_0_0 };
extern const Il2CppGenericInst GenInst_SwarmItem_t124_0_0_0 = { 1, GenInst_SwarmItem_t124_0_0_0_Types };
extern const Il2CppType SignalListener_t114_0_0_0;
static const Il2CppType* GenInst_SignalListener_t114_0_0_0_Types[] = { &SignalListener_t114_0_0_0 };
extern const Il2CppGenericInst GenInst_SignalListener_t114_0_0_0 = { 1, GenInst_SignalListener_t114_0_0_0_Types };
extern const Il2CppType Signal_t116_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Signal_t116_0_0_0_Types[] = { &String_t_0_0_0, &Signal_t116_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Signal_t116_0_0_0 = { 2, GenInst_String_t_0_0_0_Signal_t116_0_0_0_Types };
extern const Il2CppType TimeReference_t14_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_TimeReference_t14_0_0_0_Types[] = { &String_t_0_0_0, &TimeReference_t14_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TimeReference_t14_0_0_0 = { 2, GenInst_String_t_0_0_0_TimeReference_t14_0_0_0_Types };
extern const Il2CppType Command_t348_0_0_0;
static const Il2CppType* GenInst_Command_t348_0_0_0_Types[] = { &Command_t348_0_0_0 };
extern const Il2CppGenericInst GenInst_Command_t348_0_0_0 = { 1, GenInst_Command_t348_0_0_0_Types };
extern const Il2CppType BoxCollider_t126_0_0_0;
static const Il2CppType* GenInst_BoxCollider_t126_0_0_0_Types[] = { &BoxCollider_t126_0_0_0 };
extern const Il2CppGenericInst GenInst_BoxCollider_t126_0_0_0 = { 1, GenInst_BoxCollider_t126_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Object_t_0_0_0_Types[] = { &Type_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType SimpleXmlNode_t142_0_0_0;
static const Il2CppType* GenInst_SimpleXmlNode_t142_0_0_0_Types[] = { &SimpleXmlNode_t142_0_0_0 };
extern const Il2CppGenericInst GenInst_SimpleXmlNode_t142_0_0_0 = { 1, GenInst_SimpleXmlNode_t142_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType GameObject_t155_0_0_0;
static const Il2CppType* GenInst_GameObject_t155_0_0_0_Types[] = { &GameObject_t155_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t155_0_0_0 = { 1, GenInst_GameObject_t155_0_0_0_Types };
extern const Il2CppType TaskBase_t163_0_0_0;
static const Il2CppType* GenInst_TaskBase_t163_0_0_0_Types[] = { &TaskBase_t163_0_0_0 };
extern const Il2CppGenericInst GenInst_TaskBase_t163_0_0_0 = { 1, GenInst_TaskBase_t163_0_0_0_Types };
extern const Il2CppType ActionThread_t171_0_0_0;
static const Il2CppType* GenInst_ActionThread_t171_0_0_0_Types[] = { &ActionThread_t171_0_0_0 };
extern const Il2CppGenericInst GenInst_ActionThread_t171_0_0_0 = { 1, GenInst_ActionThread_t171_0_0_0_Types };
extern const Il2CppType ThreadBase_t169_0_0_0;
extern const Il2CppType IEnumerator_t337_0_0_0;
static const Il2CppType* GenInst_ThreadBase_t169_0_0_0_IEnumerator_t337_0_0_0_Types[] = { &ThreadBase_t169_0_0_0, &IEnumerator_t337_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadBase_t169_0_0_0_IEnumerator_t337_0_0_0 = { 2, GenInst_ThreadBase_t169_0_0_0_IEnumerator_t337_0_0_0_Types };
static const Il2CppType* GenInst_IEnumerator_t337_0_0_0_Types[] = { &IEnumerator_t337_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerator_t337_0_0_0 = { 1, GenInst_IEnumerator_t337_0_0_0_Types };
static const Il2CppType* GenInst_ThreadBase_t169_0_0_0_Types[] = { &ThreadBase_t169_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadBase_t169_0_0_0 = { 1, GenInst_ThreadBase_t169_0_0_0_Types };
extern const Il2CppType UnityThreadHelper_t181_0_0_0;
static const Il2CppType* GenInst_UnityThreadHelper_t181_0_0_0_Types[] = { &UnityThreadHelper_t181_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityThreadHelper_t181_0_0_0 = { 1, GenInst_UnityThreadHelper_t181_0_0_0_Types };
extern const Il2CppType Boolean_t384_0_0_0;
static const Il2CppType* GenInst_ThreadBase_t169_0_0_0_Boolean_t384_0_0_0_Types[] = { &ThreadBase_t169_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadBase_t169_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_ThreadBase_t169_0_0_0_Boolean_t384_0_0_0_Types };
extern const Il2CppType Platform_t135_0_0_0;
static const Il2CppType* GenInst_Platform_t135_0_0_0_Types[] = { &Platform_t135_0_0_0 };
extern const Il2CppGenericInst GenInst_Platform_t135_0_0_0 = { 1, GenInst_Platform_t135_0_0_0_Types };
extern const Il2CppType EScreens_t188_0_0_0;
static const Il2CppType* GenInst_EScreens_t188_0_0_0_String_t_0_0_0_Types[] = { &EScreens_t188_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EScreens_t188_0_0_0_String_t_0_0_0 = { 2, GenInst_EScreens_t188_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ESubScreens_t189_0_0_0;
static const Il2CppType* GenInst_ESubScreens_t189_0_0_0_String_t_0_0_0_Types[] = { &ESubScreens_t189_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ESubScreens_t189_0_0_0_String_t_0_0_0 = { 2, GenInst_ESubScreens_t189_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType RaffleData_t206_0_0_0;
static const Il2CppType* GenInst_RaffleData_t206_0_0_0_Types[] = { &RaffleData_t206_0_0_0 };
extern const Il2CppGenericInst GenInst_RaffleData_t206_0_0_0 = { 1, GenInst_RaffleData_t206_0_0_0_Types };
extern const Il2CppType Raffle_t202_0_0_0;
static const Il2CppType* GenInst_Raffle_t202_0_0_0_Types[] = { &Raffle_t202_0_0_0 };
extern const Il2CppGenericInst GenInst_Raffle_t202_0_0_0 = { 1, GenInst_Raffle_t202_0_0_0_Types };
extern const Il2CppType ERaffleResult_t207_0_0_0;
static const Il2CppType* GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_Types[] = { &Int32_t372_0_0_0, &ERaffleResult_t207_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_Types };
static const Il2CppType* GenInst_ERaffleResult_t207_0_0_0_Types[] = { &ERaffleResult_t207_0_0_0 };
extern const Il2CppGenericInst GenInst_ERaffleResult_t207_0_0_0 = { 1, GenInst_ERaffleResult_t207_0_0_0_Types };
extern const Il2CppType ImageTargetBehaviour_t210_0_0_0;
static const Il2CppType* GenInst_ImageTargetBehaviour_t210_0_0_0_Types[] = { &ImageTargetBehaviour_t210_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetBehaviour_t210_0_0_0 = { 1, GenInst_ImageTargetBehaviour_t210_0_0_0_Types };
extern const Il2CppType TrackableBehaviour_t211_0_0_0;
static const Il2CppType* GenInst_TrackableBehaviour_t211_0_0_0_Types[] = { &TrackableBehaviour_t211_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableBehaviour_t211_0_0_0 = { 1, GenInst_TrackableBehaviour_t211_0_0_0_Types };
extern const Il2CppType FocusMode_t438_0_0_0;
static const Il2CppType* GenInst_FocusMode_t438_0_0_0_Types[] = { &FocusMode_t438_0_0_0 };
extern const Il2CppGenericInst GenInst_FocusMode_t438_0_0_0 = { 1, GenInst_FocusMode_t438_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t352_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t352_0_0_0_Boolean_t384_0_0_0_Types[] = { &KeyValuePair_2_t352_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t352_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_KeyValuePair_2_t352_0_0_0_Boolean_t384_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t352_0_0_0_Types[] = { &KeyValuePair_2_t352_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t352_0_0_0 = { 1, GenInst_KeyValuePair_2_t352_0_0_0_Types };
extern const Il2CppType InputField_t226_0_0_0;
static const Il2CppType* GenInst_InputField_t226_0_0_0_Boolean_t384_0_0_0_Types[] = { &InputField_t226_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t226_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_InputField_t226_0_0_0_Boolean_t384_0_0_0_Types };
static const Il2CppType* GenInst_InputField_t226_0_0_0_String_t_0_0_0_Types[] = { &InputField_t226_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t226_0_0_0_String_t_0_0_0 = { 2, GenInst_InputField_t226_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType Image_t213_0_0_0;
static const Il2CppType* GenInst_InputField_t226_0_0_0_Image_t213_0_0_0_Types[] = { &InputField_t226_0_0_0, &Image_t213_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t226_0_0_0_Image_t213_0_0_0 = { 2, GenInst_InputField_t226_0_0_0_Image_t213_0_0_0_Types };
extern const Il2CppType AnimatorFrame_t235_0_0_0;
static const Il2CppType* GenInst_AnimatorFrame_t235_0_0_0_Types[] = { &AnimatorFrame_t235_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorFrame_t235_0_0_0 = { 1, GenInst_AnimatorFrame_t235_0_0_0_Types };
extern const Il2CppType AudioSource_t44_0_0_0;
static const Il2CppType* GenInst_AudioSource_t44_0_0_0_Types[] = { &AudioSource_t44_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t44_0_0_0 = { 1, GenInst_AudioSource_t44_0_0_0_Types };
extern const Il2CppType GUITexture_t243_0_0_0;
static const Il2CppType* GenInst_GUITexture_t243_0_0_0_Types[] = { &GUITexture_t243_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t243_0_0_0 = { 1, GenInst_GUITexture_t243_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType Hashtable_t261_0_0_0;
static const Il2CppType* GenInst_Hashtable_t261_0_0_0_Types[] = { &Hashtable_t261_0_0_0 };
extern const Il2CppGenericInst GenInst_Hashtable_t261_0_0_0 = { 1, GenInst_Hashtable_t261_0_0_0_Types };
extern const Il2CppType GUIText_t443_0_0_0;
static const Il2CppType* GenInst_GUIText_t443_0_0_0_Types[] = { &GUIText_t443_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIText_t443_0_0_0 = { 1, GenInst_GUIText_t443_0_0_0_Types };
extern const Il2CppType Light_t444_0_0_0;
static const Il2CppType* GenInst_Light_t444_0_0_0_Types[] = { &Light_t444_0_0_0 };
extern const Il2CppGenericInst GenInst_Light_t444_0_0_0 = { 1, GenInst_Light_t444_0_0_0_Types };
extern const Il2CppType Vector3_t36_0_0_0;
static const Il2CppType* GenInst_Vector3_t36_0_0_0_Types[] = { &Vector3_t36_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t36_0_0_0 = { 1, GenInst_Vector3_t36_0_0_0_Types };
extern const Il2CppType Rigidbody_t447_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t447_0_0_0_Types[] = { &Rigidbody_t447_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t447_0_0_0 = { 1, GenInst_Rigidbody_t447_0_0_0_Types };
extern const Il2CppType iTween_t258_0_0_0;
static const Il2CppType* GenInst_iTween_t258_0_0_0_Types[] = { &iTween_t258_0_0_0 };
extern const Il2CppGenericInst GenInst_iTween_t258_0_0_0 = { 1, GenInst_iTween_t258_0_0_0_Types };
extern const Il2CppType InitError_t1233_0_0_0;
static const Il2CppType* GenInst_InitError_t1233_0_0_0_Types[] = { &InitError_t1233_0_0_0 };
extern const Il2CppGenericInst GenInst_InitError_t1233_0_0_0 = { 1, GenInst_InitError_t1233_0_0_0_Types };
extern const Il2CppType ReconstructionBehaviour_t278_0_0_0;
static const Il2CppType* GenInst_ReconstructionBehaviour_t278_0_0_0_Types[] = { &ReconstructionBehaviour_t278_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionBehaviour_t278_0_0_0 = { 1, GenInst_ReconstructionBehaviour_t278_0_0_0_Types };
extern const Il2CppType Prop_t357_0_0_0;
static const Il2CppType* GenInst_Prop_t357_0_0_0_Types[] = { &Prop_t357_0_0_0 };
extern const Il2CppGenericInst GenInst_Prop_t357_0_0_0 = { 1, GenInst_Prop_t357_0_0_0_Types };
extern const Il2CppType Surface_t358_0_0_0;
static const Il2CppType* GenInst_Surface_t358_0_0_0_Types[] = { &Surface_t358_0_0_0 };
extern const Il2CppGenericInst GenInst_Surface_t358_0_0_0 = { 1, GenInst_Surface_t358_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType MaskOutBehaviour_t294_0_0_0;
static const Il2CppType* GenInst_MaskOutBehaviour_t294_0_0_0_Types[] = { &MaskOutBehaviour_t294_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskOutBehaviour_t294_0_0_0 = { 1, GenInst_MaskOutBehaviour_t294_0_0_0_Types };
extern const Il2CppType VirtualButtonBehaviour_t318_0_0_0;
static const Il2CppType* GenInst_VirtualButtonBehaviour_t318_0_0_0_Types[] = { &VirtualButtonBehaviour_t318_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonBehaviour_t318_0_0_0 = { 1, GenInst_VirtualButtonBehaviour_t318_0_0_0_Types };
extern const Il2CppType TurnOffBehaviour_t309_0_0_0;
static const Il2CppType* GenInst_TurnOffBehaviour_t309_0_0_0_Types[] = { &TurnOffBehaviour_t309_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnOffBehaviour_t309_0_0_0 = { 1, GenInst_TurnOffBehaviour_t309_0_0_0_Types };
extern const Il2CppType MarkerBehaviour_t292_0_0_0;
static const Il2CppType* GenInst_MarkerBehaviour_t292_0_0_0_Types[] = { &MarkerBehaviour_t292_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerBehaviour_t292_0_0_0 = { 1, GenInst_MarkerBehaviour_t292_0_0_0_Types };
extern const Il2CppType MultiTargetBehaviour_t296_0_0_0;
static const Il2CppType* GenInst_MultiTargetBehaviour_t296_0_0_0_Types[] = { &MultiTargetBehaviour_t296_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiTargetBehaviour_t296_0_0_0 = { 1, GenInst_MultiTargetBehaviour_t296_0_0_0_Types };
extern const Il2CppType CylinderTargetBehaviour_t272_0_0_0;
static const Il2CppType* GenInst_CylinderTargetBehaviour_t272_0_0_0_Types[] = { &CylinderTargetBehaviour_t272_0_0_0 };
extern const Il2CppGenericInst GenInst_CylinderTargetBehaviour_t272_0_0_0 = { 1, GenInst_CylinderTargetBehaviour_t272_0_0_0_Types };
extern const Il2CppType WordBehaviour_t326_0_0_0;
static const Il2CppType* GenInst_WordBehaviour_t326_0_0_0_Types[] = { &WordBehaviour_t326_0_0_0 };
extern const Il2CppGenericInst GenInst_WordBehaviour_t326_0_0_0 = { 1, GenInst_WordBehaviour_t326_0_0_0_Types };
extern const Il2CppType TextRecoBehaviour_t307_0_0_0;
static const Il2CppType* GenInst_TextRecoBehaviour_t307_0_0_0_Types[] = { &TextRecoBehaviour_t307_0_0_0 };
extern const Il2CppGenericInst GenInst_TextRecoBehaviour_t307_0_0_0 = { 1, GenInst_TextRecoBehaviour_t307_0_0_0_Types };
extern const Il2CppType ObjectTargetBehaviour_t298_0_0_0;
static const Il2CppType* GenInst_ObjectTargetBehaviour_t298_0_0_0_Types[] = { &ObjectTargetBehaviour_t298_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTargetBehaviour_t298_0_0_0 = { 1, GenInst_ObjectTargetBehaviour_t298_0_0_0_Types };
extern const Il2CppType ComponentFactoryStarterBehaviour_t287_0_0_0;
static const Il2CppType* GenInst_ComponentFactoryStarterBehaviour_t287_0_0_0_Types[] = { &ComponentFactoryStarterBehaviour_t287_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactoryStarterBehaviour_t287_0_0_0 = { 1, GenInst_ComponentFactoryStarterBehaviour_t287_0_0_0_Types };
extern const Il2CppType VuforiaBehaviour_t320_0_0_0;
static const Il2CppType* GenInst_VuforiaBehaviour_t320_0_0_0_Types[] = { &VuforiaBehaviour_t320_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaBehaviour_t320_0_0_0 = { 1, GenInst_VuforiaBehaviour_t320_0_0_0_Types };
extern const Il2CppType WireframeBehaviour_t324_0_0_0;
static const Il2CppType* GenInst_WireframeBehaviour_t324_0_0_0_Types[] = { &WireframeBehaviour_t324_0_0_0 };
extern const Il2CppGenericInst GenInst_WireframeBehaviour_t324_0_0_0 = { 1, GenInst_WireframeBehaviour_t324_0_0_0_Types };
extern const Il2CppType BaseInputModule_t479_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t479_0_0_0_Types[] = { &BaseInputModule_t479_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t479_0_0_0 = { 1, GenInst_BaseInputModule_t479_0_0_0_Types };
extern const Il2CppType RaycastResult_t512_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t512_0_0_0_Types[] = { &RaycastResult_t512_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t512_0_0_0 = { 1, GenInst_RaycastResult_t512_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t700_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t700_0_0_0_Types[] = { &IDeselectHandler_t700_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t700_0_0_0 = { 1, GenInst_IDeselectHandler_t700_0_0_0_Types };
extern const Il2CppType ISelectHandler_t699_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t699_0_0_0_Types[] = { &ISelectHandler_t699_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t699_0_0_0 = { 1, GenInst_ISelectHandler_t699_0_0_0_Types };
extern const Il2CppType BaseEventData_t480_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t480_0_0_0_Types[] = { &BaseEventData_t480_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t480_0_0_0 = { 1, GenInst_BaseEventData_t480_0_0_0_Types };
extern const Il2CppType Entry_t484_0_0_0;
static const Il2CppType* GenInst_Entry_t484_0_0_0_Types[] = { &Entry_t484_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t484_0_0_0 = { 1, GenInst_Entry_t484_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t687_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t687_0_0_0_Types[] = { &IPointerEnterHandler_t687_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t687_0_0_0 = { 1, GenInst_IPointerEnterHandler_t687_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t688_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t688_0_0_0_Types[] = { &IPointerExitHandler_t688_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t688_0_0_0 = { 1, GenInst_IPointerExitHandler_t688_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t689_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t689_0_0_0_Types[] = { &IPointerDownHandler_t689_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t689_0_0_0 = { 1, GenInst_IPointerDownHandler_t689_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t690_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t690_0_0_0_Types[] = { &IPointerUpHandler_t690_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t690_0_0_0 = { 1, GenInst_IPointerUpHandler_t690_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t691_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t691_0_0_0_Types[] = { &IPointerClickHandler_t691_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t691_0_0_0 = { 1, GenInst_IPointerClickHandler_t691_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t692_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t692_0_0_0_Types[] = { &IInitializePotentialDragHandler_t692_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t692_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t692_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t693_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t693_0_0_0_Types[] = { &IBeginDragHandler_t693_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t693_0_0_0 = { 1, GenInst_IBeginDragHandler_t693_0_0_0_Types };
extern const Il2CppType IDragHandler_t694_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t694_0_0_0_Types[] = { &IDragHandler_t694_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t694_0_0_0 = { 1, GenInst_IDragHandler_t694_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t695_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t695_0_0_0_Types[] = { &IEndDragHandler_t695_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t695_0_0_0 = { 1, GenInst_IEndDragHandler_t695_0_0_0_Types };
extern const Il2CppType IDropHandler_t696_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t696_0_0_0_Types[] = { &IDropHandler_t696_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t696_0_0_0 = { 1, GenInst_IDropHandler_t696_0_0_0_Types };
extern const Il2CppType IScrollHandler_t697_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t697_0_0_0_Types[] = { &IScrollHandler_t697_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t697_0_0_0 = { 1, GenInst_IScrollHandler_t697_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t698_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t698_0_0_0_Types[] = { &IUpdateSelectedHandler_t698_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t698_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t698_0_0_0_Types };
extern const Il2CppType IMoveHandler_t701_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t701_0_0_0_Types[] = { &IMoveHandler_t701_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t701_0_0_0 = { 1, GenInst_IMoveHandler_t701_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t702_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t702_0_0_0_Types[] = { &ISubmitHandler_t702_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t702_0_0_0 = { 1, GenInst_ISubmitHandler_t702_0_0_0_Types };
extern const Il2CppType ICancelHandler_t703_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t703_0_0_0_Types[] = { &ICancelHandler_t703_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t703_0_0_0 = { 1, GenInst_ICancelHandler_t703_0_0_0_Types };
extern const Il2CppType List_1_t686_0_0_0;
static const Il2CppType* GenInst_List_1_t686_0_0_0_Types[] = { &List_1_t686_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t686_0_0_0 = { 1, GenInst_List_1_t686_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t2811_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t2811_0_0_0_Types[] = { &IEventSystemHandler_t2811_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2811_0_0_0 = { 1, GenInst_IEventSystemHandler_t2811_0_0_0_Types };
extern const Il2CppType Transform_t35_0_0_0;
static const Il2CppType* GenInst_Transform_t35_0_0_0_Types[] = { &Transform_t35_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t35_0_0_0 = { 1, GenInst_Transform_t35_0_0_0_Types };
extern const Il2CppType PointerEventData_t517_0_0_0;
static const Il2CppType* GenInst_PointerEventData_t517_0_0_0_Types[] = { &PointerEventData_t517_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t517_0_0_0 = { 1, GenInst_PointerEventData_t517_0_0_0_Types };
extern const Il2CppType AxisEventData_t514_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t514_0_0_0_Types[] = { &AxisEventData_t514_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t514_0_0_0 = { 1, GenInst_AxisEventData_t514_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t513_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t513_0_0_0_Types[] = { &BaseRaycaster_t513_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t513_0_0_0 = { 1, GenInst_BaseRaycaster_t513_0_0_0_Types };
extern const Il2CppType EventSystem_t476_0_0_0;
static const Il2CppType* GenInst_EventSystem_t476_0_0_0_Types[] = { &EventSystem_t476_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t476_0_0_0 = { 1, GenInst_EventSystem_t476_0_0_0_Types };
extern const Il2CppType ButtonState_t520_0_0_0;
static const Il2CppType* GenInst_ButtonState_t520_0_0_0_Types[] = { &ButtonState_t520_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t520_0_0_0 = { 1, GenInst_ButtonState_t520_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_PointerEventData_t517_0_0_0_Types[] = { &Int32_t372_0_0_0, &PointerEventData_t517_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_PointerEventData_t517_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_PointerEventData_t517_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t727_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t727_0_0_0_Types[] = { &SpriteRenderer_t727_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t727_0_0_0 = { 1, GenInst_SpriteRenderer_t727_0_0_0_Types };
extern const Il2CppType RaycastHit_t60_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t60_0_0_0_Types[] = { &RaycastHit_t60_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t60_0_0_0 = { 1, GenInst_RaycastHit_t60_0_0_0_Types };
extern const Il2CppType Color_t9_0_0_0;
static const Il2CppType* GenInst_Color_t9_0_0_0_Types[] = { &Color_t9_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t9_0_0_0 = { 1, GenInst_Color_t9_0_0_0_Types };
extern const Il2CppType Single_t388_0_0_0;
static const Il2CppType* GenInst_Single_t388_0_0_0_Types[] = { &Single_t388_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t388_0_0_0 = { 1, GenInst_Single_t388_0_0_0_Types };
extern const Il2CppType ICanvasElement_t708_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t708_0_0_0_Types[] = { &ICanvasElement_t708_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t708_0_0_0 = { 1, GenInst_ICanvasElement_t708_0_0_0_Types };
extern const Il2CppType RectTransform_t556_0_0_0;
static const Il2CppType* GenInst_RectTransform_t556_0_0_0_Types[] = { &RectTransform_t556_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t556_0_0_0 = { 1, GenInst_RectTransform_t556_0_0_0_Types };
static const Il2CppType* GenInst_Image_t213_0_0_0_Types[] = { &Image_t213_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t213_0_0_0 = { 1, GenInst_Image_t213_0_0_0_Types };
extern const Il2CppType Button_t544_0_0_0;
static const Il2CppType* GenInst_Button_t544_0_0_0_Types[] = { &Button_t544_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t544_0_0_0 = { 1, GenInst_Button_t544_0_0_0_Types };
extern const Il2CppType Text_t196_0_0_0;
static const Il2CppType* GenInst_Text_t196_0_0_0_Types[] = { &Text_t196_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t196_0_0_0 = { 1, GenInst_Text_t196_0_0_0_Types };
extern const Il2CppType RawImage_t237_0_0_0;
static const Il2CppType* GenInst_RawImage_t237_0_0_0_Types[] = { &RawImage_t237_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t237_0_0_0 = { 1, GenInst_RawImage_t237_0_0_0_Types };
extern const Il2CppType Slider_t637_0_0_0;
static const Il2CppType* GenInst_Slider_t637_0_0_0_Types[] = { &Slider_t637_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t637_0_0_0 = { 1, GenInst_Slider_t637_0_0_0_Types };
extern const Il2CppType Scrollbar_t621_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t621_0_0_0_Types[] = { &Scrollbar_t621_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t621_0_0_0 = { 1, GenInst_Scrollbar_t621_0_0_0_Types };
extern const Il2CppType Toggle_t557_0_0_0;
static const Il2CppType* GenInst_Toggle_t557_0_0_0_Types[] = { &Toggle_t557_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t557_0_0_0 = { 1, GenInst_Toggle_t557_0_0_0_Types };
static const Il2CppType* GenInst_InputField_t226_0_0_0_Types[] = { &InputField_t226_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t226_0_0_0 = { 1, GenInst_InputField_t226_0_0_0_Types };
extern const Il2CppType ScrollRect_t627_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t627_0_0_0_Types[] = { &ScrollRect_t627_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t627_0_0_0 = { 1, GenInst_ScrollRect_t627_0_0_0_Types };
extern const Il2CppType Mask_t606_0_0_0;
static const Il2CppType* GenInst_Mask_t606_0_0_0_Types[] = { &Mask_t606_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t606_0_0_0 = { 1, GenInst_Mask_t606_0_0_0_Types };
extern const Il2CppType Dropdown_t564_0_0_0;
static const Il2CppType* GenInst_Dropdown_t564_0_0_0_Types[] = { &Dropdown_t564_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t564_0_0_0 = { 1, GenInst_Dropdown_t564_0_0_0_Types };
extern const Il2CppType OptionData_t558_0_0_0;
static const Il2CppType* GenInst_OptionData_t558_0_0_0_Types[] = { &OptionData_t558_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t558_0_0_0 = { 1, GenInst_OptionData_t558_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Types[] = { &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0 = { 1, GenInst_Int32_t372_0_0_0_Types };
extern const Il2CppType DropdownItem_t555_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t555_0_0_0_Types[] = { &DropdownItem_t555_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t555_0_0_0 = { 1, GenInst_DropdownItem_t555_0_0_0_Types };
extern const Il2CppType FloatTween_t539_0_0_0;
static const Il2CppType* GenInst_FloatTween_t539_0_0_0_Types[] = { &FloatTween_t539_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t539_0_0_0 = { 1, GenInst_FloatTween_t539_0_0_0_Types };
extern const Il2CppType Canvas_t574_0_0_0;
static const Il2CppType* GenInst_Canvas_t574_0_0_0_Types[] = { &Canvas_t574_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t574_0_0_0 = { 1, GenInst_Canvas_t574_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t578_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t578_0_0_0_Types[] = { &GraphicRaycaster_t578_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t578_0_0_0 = { 1, GenInst_GraphicRaycaster_t578_0_0_0_Types };
extern const Il2CppType CanvasGroup_t733_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t733_0_0_0_Types[] = { &CanvasGroup_t733_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t733_0_0_0 = { 1, GenInst_CanvasGroup_t733_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t384_0_0_0_Types[] = { &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t384_0_0_0 = { 1, GenInst_Boolean_t384_0_0_0_Types };
extern const Il2CppType Font_t569_0_0_0;
extern const Il2CppType List_1_t738_0_0_0;
static const Il2CppType* GenInst_Font_t569_0_0_0_List_1_t738_0_0_0_Types[] = { &Font_t569_0_0_0, &List_1_t738_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t569_0_0_0_List_1_t738_0_0_0 = { 2, GenInst_Font_t569_0_0_0_List_1_t738_0_0_0_Types };
static const Il2CppType* GenInst_Font_t569_0_0_0_Types[] = { &Font_t569_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t569_0_0_0 = { 1, GenInst_Font_t569_0_0_0_Types };
extern const Il2CppType ColorTween_t536_0_0_0;
static const Il2CppType* GenInst_ColorTween_t536_0_0_0_Types[] = { &ColorTween_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t536_0_0_0 = { 1, GenInst_ColorTween_t536_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t573_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t573_0_0_0_Types[] = { &CanvasRenderer_t573_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t573_0_0_0 = { 1, GenInst_CanvasRenderer_t573_0_0_0_Types };
extern const Il2CppType Component_t365_0_0_0;
static const Il2CppType* GenInst_Component_t365_0_0_0_Types[] = { &Component_t365_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t365_0_0_0 = { 1, GenInst_Component_t365_0_0_0_Types };
extern const Il2CppType Graphic_t572_0_0_0;
static const Il2CppType* GenInst_Graphic_t572_0_0_0_Types[] = { &Graphic_t572_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t572_0_0_0 = { 1, GenInst_Graphic_t572_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t747_0_0_0;
static const Il2CppType* GenInst_Canvas_t574_0_0_0_IndexedSet_1_t747_0_0_0_Types[] = { &Canvas_t574_0_0_0, &IndexedSet_1_t747_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t574_0_0_0_IndexedSet_1_t747_0_0_0 = { 2, GenInst_Canvas_t574_0_0_0_IndexedSet_1_t747_0_0_0_Types };
extern const Il2CppType Sprite_t553_0_0_0;
static const Il2CppType* GenInst_Sprite_t553_0_0_0_Types[] = { &Sprite_t553_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t553_0_0_0 = { 1, GenInst_Sprite_t553_0_0_0_Types };
extern const Il2CppType Type_t583_0_0_0;
static const Il2CppType* GenInst_Type_t583_0_0_0_Types[] = { &Type_t583_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t583_0_0_0 = { 1, GenInst_Type_t583_0_0_0_Types };
extern const Il2CppType FillMethod_t584_0_0_0;
static const Il2CppType* GenInst_FillMethod_t584_0_0_0_Types[] = { &FillMethod_t584_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t584_0_0_0 = { 1, GenInst_FillMethod_t584_0_0_0_Types };
extern const Il2CppType SubmitEvent_t595_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t595_0_0_0_Types[] = { &SubmitEvent_t595_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t595_0_0_0 = { 1, GenInst_SubmitEvent_t595_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t597_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t597_0_0_0_Types[] = { &OnChangeEvent_t597_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t597_0_0_0 = { 1, GenInst_OnChangeEvent_t597_0_0_0_Types };
extern const Il2CppType OnValidateInput_t599_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t599_0_0_0_Types[] = { &OnValidateInput_t599_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t599_0_0_0 = { 1, GenInst_OnValidateInput_t599_0_0_0_Types };
extern const Il2CppType ContentType_t591_0_0_0;
static const Il2CppType* GenInst_ContentType_t591_0_0_0_Types[] = { &ContentType_t591_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t591_0_0_0 = { 1, GenInst_ContentType_t591_0_0_0_Types };
extern const Il2CppType LineType_t594_0_0_0;
static const Il2CppType* GenInst_LineType_t594_0_0_0_Types[] = { &LineType_t594_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t594_0_0_0 = { 1, GenInst_LineType_t594_0_0_0_Types };
extern const Il2CppType InputType_t592_0_0_0;
static const Il2CppType* GenInst_InputType_t592_0_0_0_Types[] = { &InputType_t592_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t592_0_0_0 = { 1, GenInst_InputType_t592_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t749_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t749_0_0_0_Types[] = { &TouchScreenKeyboardType_t749_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t749_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t749_0_0_0_Types };
extern const Il2CppType CharacterValidation_t593_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t593_0_0_0_Types[] = { &CharacterValidation_t593_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t593_0_0_0 = { 1, GenInst_CharacterValidation_t593_0_0_0_Types };
extern const Il2CppType Char_t383_0_0_0;
static const Il2CppType* GenInst_Char_t383_0_0_0_Types[] = { &Char_t383_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t383_0_0_0 = { 1, GenInst_Char_t383_0_0_0_Types };
extern const Il2CppType UILineInfo_t752_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t752_0_0_0_Types[] = { &UILineInfo_t752_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t752_0_0_0 = { 1, GenInst_UILineInfo_t752_0_0_0_Types };
extern const Il2CppType UICharInfo_t754_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t754_0_0_0_Types[] = { &UICharInfo_t754_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t754_0_0_0 = { 1, GenInst_UICharInfo_t754_0_0_0_Types };
extern const Il2CppType LayoutElement_t665_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t665_0_0_0_Types[] = { &LayoutElement_t665_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t665_0_0_0 = { 1, GenInst_LayoutElement_t665_0_0_0_Types };
extern const Il2CppType IClippable_t713_0_0_0;
static const Il2CppType* GenInst_IClippable_t713_0_0_0_Types[] = { &IClippable_t713_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t713_0_0_0 = { 1, GenInst_IClippable_t713_0_0_0_Types };
extern const Il2CppType RectMask2D_t609_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t609_0_0_0_Types[] = { &RectMask2D_t609_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t609_0_0_0 = { 1, GenInst_RectMask2D_t609_0_0_0_Types };
extern const Il2CppType Direction_t617_0_0_0;
static const Il2CppType* GenInst_Direction_t617_0_0_0_Types[] = { &Direction_t617_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t617_0_0_0 = { 1, GenInst_Direction_t617_0_0_0_Types };
extern const Il2CppType Vector2_t2_0_0_0;
static const Il2CppType* GenInst_Vector2_t2_0_0_0_Types[] = { &Vector2_t2_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2_0_0_0 = { 1, GenInst_Vector2_t2_0_0_0_Types };
extern const Il2CppType Selectable_t545_0_0_0;
static const Il2CppType* GenInst_Selectable_t545_0_0_0_Types[] = { &Selectable_t545_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t545_0_0_0 = { 1, GenInst_Selectable_t545_0_0_0_Types };
extern const Il2CppType Navigation_t613_0_0_0;
static const Il2CppType* GenInst_Navigation_t613_0_0_0_Types[] = { &Navigation_t613_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t613_0_0_0 = { 1, GenInst_Navigation_t613_0_0_0_Types };
extern const Il2CppType Transition_t628_0_0_0;
static const Il2CppType* GenInst_Transition_t628_0_0_0_Types[] = { &Transition_t628_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t628_0_0_0 = { 1, GenInst_Transition_t628_0_0_0_Types };
extern const Il2CppType ColorBlock_t551_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t551_0_0_0_Types[] = { &ColorBlock_t551_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t551_0_0_0 = { 1, GenInst_ColorBlock_t551_0_0_0_Types };
extern const Il2CppType SpriteState_t630_0_0_0;
static const Il2CppType* GenInst_SpriteState_t630_0_0_0_Types[] = { &SpriteState_t630_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t630_0_0_0 = { 1, GenInst_SpriteState_t630_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t540_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t540_0_0_0_Types[] = { &AnimationTriggers_t540_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t540_0_0_0 = { 1, GenInst_AnimationTriggers_t540_0_0_0_Types };
extern const Il2CppType Animator_t714_0_0_0;
static const Il2CppType* GenInst_Animator_t714_0_0_0_Types[] = { &Animator_t714_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t714_0_0_0 = { 1, GenInst_Animator_t714_0_0_0_Types };
extern const Il2CppType Direction_t634_0_0_0;
static const Il2CppType* GenInst_Direction_t634_0_0_0_Types[] = { &Direction_t634_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t634_0_0_0 = { 1, GenInst_Direction_t634_0_0_0_Types };
extern const Il2CppType MatEntry_t638_0_0_0;
static const Il2CppType* GenInst_MatEntry_t638_0_0_0_Types[] = { &MatEntry_t638_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t638_0_0_0 = { 1, GenInst_MatEntry_t638_0_0_0_Types };
extern const Il2CppType UIVertex_t605_0_0_0;
static const Il2CppType* GenInst_UIVertex_t605_0_0_0_Types[] = { &UIVertex_t605_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t605_0_0_0 = { 1, GenInst_UIVertex_t605_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t557_0_0_0_Boolean_t384_0_0_0_Types[] = { &Toggle_t557_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t557_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_Toggle_t557_0_0_0_Boolean_t384_0_0_0_Types };
extern const Il2CppType IClipper_t717_0_0_0;
static const Il2CppType* GenInst_IClipper_t717_0_0_0_Types[] = { &IClipper_t717_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t717_0_0_0 = { 1, GenInst_IClipper_t717_0_0_0_Types };
extern const Il2CppType AspectMode_t650_0_0_0;
static const Il2CppType* GenInst_AspectMode_t650_0_0_0_Types[] = { &AspectMode_t650_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t650_0_0_0 = { 1, GenInst_AspectMode_t650_0_0_0_Types };
extern const Il2CppType FitMode_t656_0_0_0;
static const Il2CppType* GenInst_FitMode_t656_0_0_0_Types[] = { &FitMode_t656_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t656_0_0_0 = { 1, GenInst_FitMode_t656_0_0_0_Types };
extern const Il2CppType Corner_t658_0_0_0;
static const Il2CppType* GenInst_Corner_t658_0_0_0_Types[] = { &Corner_t658_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t658_0_0_0 = { 1, GenInst_Corner_t658_0_0_0_Types };
extern const Il2CppType Axis_t659_0_0_0;
static const Il2CppType* GenInst_Axis_t659_0_0_0_Types[] = { &Axis_t659_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t659_0_0_0 = { 1, GenInst_Axis_t659_0_0_0_Types };
extern const Il2CppType Constraint_t660_0_0_0;
static const Il2CppType* GenInst_Constraint_t660_0_0_0_Types[] = { &Constraint_t660_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t660_0_0_0 = { 1, GenInst_Constraint_t660_0_0_0_Types };
extern const Il2CppType RectOffset_t666_0_0_0;
static const Il2CppType* GenInst_RectOffset_t666_0_0_0_Types[] = { &RectOffset_t666_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t666_0_0_0 = { 1, GenInst_RectOffset_t666_0_0_0_Types };
extern const Il2CppType TextAnchor_t766_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t766_0_0_0_Types[] = { &TextAnchor_t766_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t766_0_0_0 = { 1, GenInst_TextAnchor_t766_0_0_0_Types };
extern const Il2CppType ILayoutElement_t719_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t719_0_0_0_Single_t388_0_0_0_Types[] = { &ILayoutElement_t719_0_0_0, &Single_t388_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t719_0_0_0_Single_t388_0_0_0 = { 2, GenInst_ILayoutElement_t719_0_0_0_Single_t388_0_0_0_Types };
extern const Il2CppType Color32_t711_0_0_0;
static const Il2CppType* GenInst_Color32_t711_0_0_0_Types[] = { &Color32_t711_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t711_0_0_0 = { 1, GenInst_Color32_t711_0_0_0_Types };
extern const Il2CppType Vector4_t680_0_0_0;
static const Il2CppType* GenInst_Vector4_t680_0_0_0_Types[] = { &Vector4_t680_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t680_0_0_0 = { 1, GenInst_Vector4_t680_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t805_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t805_0_0_0_Types[] = { &GcLeaderboard_t805_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t805_0_0_0 = { 1, GenInst_GcLeaderboard_t805_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t1017_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t1017_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t1017_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t1017_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t1017_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t1019_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t1019_0_0_0_Types[] = { &IAchievementU5BU5D_t1019_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1019_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t1019_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t956_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t956_0_0_0_Types[] = { &IScoreU5BU5D_t956_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t956_0_0_0 = { 1, GenInst_IScoreU5BU5D_t956_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t952_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t952_0_0_0_Types[] = { &IUserProfileU5BU5D_t952_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t952_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t952_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t139_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t139_0_0_0_Types[] = { &ByteU5BU5D_t139_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t139_0_0_0 = { 1, GenInst_ByteU5BU5D_t139_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t384_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t384_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t868_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t868_0_0_0_Types[] = { &Rigidbody2D_t868_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t868_0_0_0 = { 1, GenInst_Rigidbody2D_t868_0_0_0_Types };
extern const Il2CppType LayoutCache_t904_0_0_0;
static const Il2CppType* GenInst_Int32_t372_0_0_0_LayoutCache_t904_0_0_0_Types[] = { &Int32_t372_0_0_0, &LayoutCache_t904_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_LayoutCache_t904_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_LayoutCache_t904_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t907_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t907_0_0_0_Types[] = { &GUILayoutEntry_t907_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t907_0_0_0 = { 1, GenInst_GUILayoutEntry_t907_0_0_0_Types };
extern const Il2CppType GUIStyle_t342_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t342_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t342_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t342_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t342_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType GUILayer_t810_0_0_0;
static const Il2CppType* GenInst_GUILayer_t810_0_0_0_Types[] = { &GUILayer_t810_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t810_0_0_0 = { 1, GenInst_GUILayer_t810_0_0_0_Types };
extern const Il2CppType PersistentCall_t984_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t984_0_0_0_Types[] = { &PersistentCall_t984_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t984_0_0_0 = { 1, GenInst_PersistentCall_t984_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t981_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t981_0_0_0_Types[] = { &BaseInvokableCall_t981_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t981_0_0_0 = { 1, GenInst_BaseInvokableCall_t981_0_0_0_Types };
extern const Il2CppType VideoBackgroundAbstractBehaviour_t315_0_0_0;
static const Il2CppType* GenInst_Camera_t6_0_0_0_VideoBackgroundAbstractBehaviour_t315_0_0_0_Types[] = { &Camera_t6_0_0_0, &VideoBackgroundAbstractBehaviour_t315_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t6_0_0_0_VideoBackgroundAbstractBehaviour_t315_0_0_0 = { 2, GenInst_Camera_t6_0_0_0_VideoBackgroundAbstractBehaviour_t315_0_0_0_Types };
extern const Il2CppType BackgroundPlaneAbstractBehaviour_t269_0_0_0;
static const Il2CppType* GenInst_BackgroundPlaneAbstractBehaviour_t269_0_0_0_Types[] = { &BackgroundPlaneAbstractBehaviour_t269_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundPlaneAbstractBehaviour_t269_0_0_0 = { 1, GenInst_BackgroundPlaneAbstractBehaviour_t269_0_0_0_Types };
static const Il2CppType* GenInst_VideoBackgroundAbstractBehaviour_t315_0_0_0_Types[] = { &VideoBackgroundAbstractBehaviour_t315_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoBackgroundAbstractBehaviour_t315_0_0_0 = { 1, GenInst_VideoBackgroundAbstractBehaviour_t315_0_0_0_Types };
extern const Il2CppType ITrackableEventHandler_t1252_0_0_0;
static const Il2CppType* GenInst_ITrackableEventHandler_t1252_0_0_0_Types[] = { &ITrackableEventHandler_t1252_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackableEventHandler_t1252_0_0_0 = { 1, GenInst_ITrackableEventHandler_t1252_0_0_0_Types };
extern const Il2CppType SmartTerrainTracker_t1168_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTracker_t1168_0_0_0_Types[] = { &SmartTerrainTracker_t1168_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTracker_t1168_0_0_0 = { 1, GenInst_SmartTerrainTracker_t1168_0_0_0_Types };
extern const Il2CppType ReconstructionAbstractBehaviour_t301_0_0_0;
static const Il2CppType* GenInst_ReconstructionAbstractBehaviour_t301_0_0_0_Types[] = { &ReconstructionAbstractBehaviour_t301_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionAbstractBehaviour_t301_0_0_0 = { 1, GenInst_ReconstructionAbstractBehaviour_t301_0_0_0_Types };
extern const Il2CppType ICloudRecoEventHandler_t1253_0_0_0;
static const Il2CppType* GenInst_ICloudRecoEventHandler_t1253_0_0_0_Types[] = { &ICloudRecoEventHandler_t1253_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloudRecoEventHandler_t1253_0_0_0 = { 1, GenInst_ICloudRecoEventHandler_t1253_0_0_0_Types };
extern const Il2CppType TargetSearchResult_t1212_0_0_0;
static const Il2CppType* GenInst_TargetSearchResult_t1212_0_0_0_Types[] = { &TargetSearchResult_t1212_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t1212_0_0_0 = { 1, GenInst_TargetSearchResult_t1212_0_0_0_Types };
extern const Il2CppType ObjectTracker_t1066_0_0_0;
static const Il2CppType* GenInst_ObjectTracker_t1066_0_0_0_Types[] = { &ObjectTracker_t1066_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTracker_t1066_0_0_0 = { 1, GenInst_ObjectTracker_t1066_0_0_0_Types };
extern const Il2CppType HideExcessAreaAbstractBehaviour_t284_0_0_0;
static const Il2CppType* GenInst_HideExcessAreaAbstractBehaviour_t284_0_0_0_Types[] = { &HideExcessAreaAbstractBehaviour_t284_0_0_0 };
extern const Il2CppGenericInst GenInst_HideExcessAreaAbstractBehaviour_t284_0_0_0 = { 1, GenInst_HideExcessAreaAbstractBehaviour_t284_0_0_0_Types };
extern const Il2CppType ReconstructionFromTarget_t1077_0_0_0;
static const Il2CppType* GenInst_ReconstructionFromTarget_t1077_0_0_0_Types[] = { &ReconstructionFromTarget_t1077_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionFromTarget_t1077_0_0_0 = { 1, GenInst_ReconstructionFromTarget_t1077_0_0_0_Types };
extern const Il2CppType VirtualButton_t1223_0_0_0;
static const Il2CppType* GenInst_VirtualButton_t1223_0_0_0_Types[] = { &VirtualButton_t1223_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButton_t1223_0_0_0 = { 1, GenInst_VirtualButton_t1223_0_0_0_Types };
extern const Il2CppType PIXEL_FORMAT_t1108_0_0_0;
extern const Il2CppType Image_t1109_0_0_0;
static const Il2CppType* GenInst_PIXEL_FORMAT_t1108_0_0_0_Image_t1109_0_0_0_Types[] = { &PIXEL_FORMAT_t1108_0_0_0, &Image_t1109_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t1108_0_0_0_Image_t1109_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t1108_0_0_0_Image_t1109_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t1108_0_0_0_Types[] = { &PIXEL_FORMAT_t1108_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t1108_0_0_0 = { 1, GenInst_PIXEL_FORMAT_t1108_0_0_0_Types };
extern const Il2CppType Trackable_t1060_0_0_0;
static const Il2CppType* GenInst_Int32_t372_0_0_0_Trackable_t1060_0_0_0_Types[] = { &Int32_t372_0_0_0, &Trackable_t1060_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Trackable_t1060_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_Trackable_t1060_0_0_0_Types };
static const Il2CppType* GenInst_Trackable_t1060_0_0_0_Types[] = { &Trackable_t1060_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t1060_0_0_0 = { 1, GenInst_Trackable_t1060_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_VirtualButton_t1223_0_0_0_Types[] = { &Int32_t372_0_0_0, &VirtualButton_t1223_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_VirtualButton_t1223_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_VirtualButton_t1223_0_0_0_Types };
extern const Il2CppType DataSet_t1088_0_0_0;
static const Il2CppType* GenInst_DataSet_t1088_0_0_0_Types[] = { &DataSet_t1088_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSet_t1088_0_0_0 = { 1, GenInst_DataSet_t1088_0_0_0_Types };
extern const Il2CppType DataSetImpl_t1073_0_0_0;
static const Il2CppType* GenInst_DataSetImpl_t1073_0_0_0_Types[] = { &DataSetImpl_t1073_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetImpl_t1073_0_0_0 = { 1, GenInst_DataSetImpl_t1073_0_0_0_Types };
extern const Il2CppType Marker_t1231_0_0_0;
static const Il2CppType* GenInst_Int32_t372_0_0_0_Marker_t1231_0_0_0_Types[] = { &Int32_t372_0_0_0, &Marker_t1231_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Marker_t1231_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_Marker_t1231_0_0_0_Types };
static const Il2CppType* GenInst_Marker_t1231_0_0_0_Types[] = { &Marker_t1231_0_0_0 };
extern const Il2CppGenericInst GenInst_Marker_t1231_0_0_0 = { 1, GenInst_Marker_t1231_0_0_0_Types };
extern const Il2CppType TrackableResultData_t1134_0_0_0;
static const Il2CppType* GenInst_TrackableResultData_t1134_0_0_0_Types[] = { &TrackableResultData_t1134_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t1134_0_0_0 = { 1, GenInst_TrackableResultData_t1134_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackable_t1085_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackable_t1085_0_0_0_Types[] = { &SmartTerrainTrackable_t1085_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackable_t1085_0_0_0 = { 1, GenInst_SmartTerrainTrackable_t1085_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackableBehaviour_t1084_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackableBehaviour_t1084_0_0_0_Types[] = { &SmartTerrainTrackableBehaviour_t1084_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackableBehaviour_t1084_0_0_0 = { 1, GenInst_SmartTerrainTrackableBehaviour_t1084_0_0_0_Types };
extern const Il2CppType UInt16_t393_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t393_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t393_0_0_0 = { 2, GenInst_Type_t_0_0_0_UInt16_t393_0_0_0_Types };
extern const Il2CppType WordResult_t1188_0_0_0;
static const Il2CppType* GenInst_Int32_t372_0_0_0_WordResult_t1188_0_0_0_Types[] = { &Int32_t372_0_0_0, &WordResult_t1188_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_WordResult_t1188_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_WordResult_t1188_0_0_0_Types };
extern const Il2CppType WordAbstractBehaviour_t327_0_0_0;
static const Il2CppType* GenInst_WordAbstractBehaviour_t327_0_0_0_Types[] = { &WordAbstractBehaviour_t327_0_0_0 };
extern const Il2CppGenericInst GenInst_WordAbstractBehaviour_t327_0_0_0 = { 1, GenInst_WordAbstractBehaviour_t327_0_0_0_Types };
extern const Il2CppType List_1_t1186_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1186_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1186_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1186_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t1186_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_WordAbstractBehaviour_t327_0_0_0_Types[] = { &Int32_t372_0_0_0, &WordAbstractBehaviour_t327_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_WordAbstractBehaviour_t327_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_WordAbstractBehaviour_t327_0_0_0_Types };
extern const Il2CppType WordData_t1139_0_0_0;
static const Il2CppType* GenInst_WordData_t1139_0_0_0_Types[] = { &WordData_t1139_0_0_0 };
extern const Il2CppGenericInst GenInst_WordData_t1139_0_0_0 = { 1, GenInst_WordData_t1139_0_0_0_Types };
extern const Il2CppType WordResultData_t1138_0_0_0;
static const Il2CppType* GenInst_WordResultData_t1138_0_0_0_Types[] = { &WordResultData_t1138_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResultData_t1138_0_0_0 = { 1, GenInst_WordResultData_t1138_0_0_0_Types };
extern const Il2CppType Word_t1190_0_0_0;
static const Il2CppType* GenInst_Word_t1190_0_0_0_Types[] = { &Word_t1190_0_0_0 };
extern const Il2CppGenericInst GenInst_Word_t1190_0_0_0 = { 1, GenInst_Word_t1190_0_0_0_Types };
static const Il2CppType* GenInst_WordResult_t1188_0_0_0_Types[] = { &WordResult_t1188_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResult_t1188_0_0_0 = { 1, GenInst_WordResult_t1188_0_0_0_Types };
extern const Il2CppType ILoadLevelEventHandler_t1268_0_0_0;
static const Il2CppType* GenInst_ILoadLevelEventHandler_t1268_0_0_0_Types[] = { &ILoadLevelEventHandler_t1268_0_0_0 };
extern const Il2CppGenericInst GenInst_ILoadLevelEventHandler_t1268_0_0_0 = { 1, GenInst_ILoadLevelEventHandler_t1268_0_0_0_Types };
extern const Il2CppType SmartTerrainInitializationInfo_t1083_0_0_0;
static const Il2CppType* GenInst_SmartTerrainInitializationInfo_t1083_0_0_0_Types[] = { &SmartTerrainInitializationInfo_t1083_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainInitializationInfo_t1083_0_0_0 = { 1, GenInst_SmartTerrainInitializationInfo_t1083_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Prop_t357_0_0_0_Types[] = { &Int32_t372_0_0_0, &Prop_t357_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Prop_t357_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_Prop_t357_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Surface_t358_0_0_0_Types[] = { &Int32_t372_0_0_0, &Surface_t358_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Surface_t358_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_Surface_t358_0_0_0_Types };
extern const Il2CppType ISmartTerrainEventHandler_t1271_0_0_0;
static const Il2CppType* GenInst_ISmartTerrainEventHandler_t1271_0_0_0_Types[] = { &ISmartTerrainEventHandler_t1271_0_0_0 };
extern const Il2CppGenericInst GenInst_ISmartTerrainEventHandler_t1271_0_0_0 = { 1, GenInst_ISmartTerrainEventHandler_t1271_0_0_0_Types };
extern const Il2CppType PropAbstractBehaviour_t300_0_0_0;
static const Il2CppType* GenInst_Int32_t372_0_0_0_PropAbstractBehaviour_t300_0_0_0_Types[] = { &Int32_t372_0_0_0, &PropAbstractBehaviour_t300_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_PropAbstractBehaviour_t300_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_PropAbstractBehaviour_t300_0_0_0_Types };
extern const Il2CppType SurfaceAbstractBehaviour_t306_0_0_0;
static const Il2CppType* GenInst_Int32_t372_0_0_0_SurfaceAbstractBehaviour_t306_0_0_0_Types[] = { &Int32_t372_0_0_0, &SurfaceAbstractBehaviour_t306_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_SurfaceAbstractBehaviour_t306_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_SurfaceAbstractBehaviour_t306_0_0_0_Types };
static const Il2CppType* GenInst_PropAbstractBehaviour_t300_0_0_0_Types[] = { &PropAbstractBehaviour_t300_0_0_0 };
extern const Il2CppGenericInst GenInst_PropAbstractBehaviour_t300_0_0_0 = { 1, GenInst_PropAbstractBehaviour_t300_0_0_0_Types };
static const Il2CppType* GenInst_SurfaceAbstractBehaviour_t306_0_0_0_Types[] = { &SurfaceAbstractBehaviour_t306_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceAbstractBehaviour_t306_0_0_0 = { 1, GenInst_SurfaceAbstractBehaviour_t306_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_TrackableBehaviour_t211_0_0_0_Types[] = { &Int32_t372_0_0_0, &TrackableBehaviour_t211_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_TrackableBehaviour_t211_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_TrackableBehaviour_t211_0_0_0_Types };
extern const Il2CppType MarkerTracker_t1121_0_0_0;
static const Il2CppType* GenInst_MarkerTracker_t1121_0_0_0_Types[] = { &MarkerTracker_t1121_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerTracker_t1121_0_0_0 = { 1, GenInst_MarkerTracker_t1121_0_0_0_Types };
extern const Il2CppType DataSetTrackableBehaviour_t1061_0_0_0;
static const Il2CppType* GenInst_DataSetTrackableBehaviour_t1061_0_0_0_Types[] = { &DataSetTrackableBehaviour_t1061_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetTrackableBehaviour_t1061_0_0_0 = { 1, GenInst_DataSetTrackableBehaviour_t1061_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_Types[] = { &Int32_t372_0_0_0, &TrackableResultData_t1134_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_Types };
extern const Il2CppType VirtualButtonData_t1135_0_0_0;
static const Il2CppType* GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_Types[] = { &Int32_t372_0_0_0, &VirtualButtonData_t1135_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_Types };
extern const Il2CppType VirtualButtonAbstractBehaviour_t319_0_0_0;
static const Il2CppType* GenInst_VirtualButtonAbstractBehaviour_t319_0_0_0_Types[] = { &VirtualButtonAbstractBehaviour_t319_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonAbstractBehaviour_t319_0_0_0 = { 1, GenInst_VirtualButtonAbstractBehaviour_t319_0_0_0_Types };
extern const Il2CppType ImageTarget_t1225_0_0_0;
static const Il2CppType* GenInst_Int32_t372_0_0_0_ImageTarget_t1225_0_0_0_Types[] = { &Int32_t372_0_0_0, &ImageTarget_t1225_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_ImageTarget_t1225_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_ImageTarget_t1225_0_0_0_Types };
static const Il2CppType* GenInst_ImageTarget_t1225_0_0_0_Types[] = { &ImageTarget_t1225_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTarget_t1225_0_0_0 = { 1, GenInst_ImageTarget_t1225_0_0_0_Types };
extern const Il2CppType ImageTargetAbstractBehaviour_t285_0_0_0;
static const Il2CppType* GenInst_ImageTargetAbstractBehaviour_t285_0_0_0_Types[] = { &ImageTargetAbstractBehaviour_t285_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetAbstractBehaviour_t285_0_0_0 = { 1, GenInst_ImageTargetAbstractBehaviour_t285_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_VirtualButtonAbstractBehaviour_t319_0_0_0_Types[] = { &Int32_t372_0_0_0, &VirtualButtonAbstractBehaviour_t319_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_VirtualButtonAbstractBehaviour_t319_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_VirtualButtonAbstractBehaviour_t319_0_0_0_Types };
extern const Il2CppType ITrackerEventHandler_t1279_0_0_0;
static const Il2CppType* GenInst_ITrackerEventHandler_t1279_0_0_0_Types[] = { &ITrackerEventHandler_t1279_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackerEventHandler_t1279_0_0_0 = { 1, GenInst_ITrackerEventHandler_t1279_0_0_0_Types };
extern const Il2CppType TextTracker_t1170_0_0_0;
static const Il2CppType* GenInst_TextTracker_t1170_0_0_0_Types[] = { &TextTracker_t1170_0_0_0 };
extern const Il2CppGenericInst GenInst_TextTracker_t1170_0_0_0 = { 1, GenInst_TextTracker_t1170_0_0_0_Types };
extern const Il2CppType WebCamAbstractBehaviour_t323_0_0_0;
static const Il2CppType* GenInst_WebCamAbstractBehaviour_t323_0_0_0_Types[] = { &WebCamAbstractBehaviour_t323_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamAbstractBehaviour_t323_0_0_0 = { 1, GenInst_WebCamAbstractBehaviour_t323_0_0_0_Types };
extern const Il2CppType IVideoBackgroundEventHandler_t1280_0_0_0;
static const Il2CppType* GenInst_IVideoBackgroundEventHandler_t1280_0_0_0_Types[] = { &IVideoBackgroundEventHandler_t1280_0_0_0 };
extern const Il2CppGenericInst GenInst_IVideoBackgroundEventHandler_t1280_0_0_0 = { 1, GenInst_IVideoBackgroundEventHandler_t1280_0_0_0_Types };
extern const Il2CppType ITextRecoEventHandler_t1281_0_0_0;
static const Il2CppType* GenInst_ITextRecoEventHandler_t1281_0_0_0_Types[] = { &ITextRecoEventHandler_t1281_0_0_0 };
extern const Il2CppGenericInst GenInst_ITextRecoEventHandler_t1281_0_0_0 = { 1, GenInst_ITextRecoEventHandler_t1281_0_0_0_Types };
extern const Il2CppType IUserDefinedTargetEventHandler_t1282_0_0_0;
static const Il2CppType* GenInst_IUserDefinedTargetEventHandler_t1282_0_0_0_Types[] = { &IUserDefinedTargetEventHandler_t1282_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserDefinedTargetEventHandler_t1282_0_0_0 = { 1, GenInst_IUserDefinedTargetEventHandler_t1282_0_0_0_Types };
extern const Il2CppType VuforiaAbstractBehaviour_t321_0_0_0;
static const Il2CppType* GenInst_VuforiaAbstractBehaviour_t321_0_0_0_Types[] = { &VuforiaAbstractBehaviour_t321_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaAbstractBehaviour_t321_0_0_0 = { 1, GenInst_VuforiaAbstractBehaviour_t321_0_0_0_Types };
extern const Il2CppType IVirtualButtonEventHandler_t1283_0_0_0;
static const Il2CppType* GenInst_IVirtualButtonEventHandler_t1283_0_0_0_Types[] = { &IVirtualButtonEventHandler_t1283_0_0_0 };
extern const Il2CppGenericInst GenInst_IVirtualButtonEventHandler_t1283_0_0_0 = { 1, GenInst_IVirtualButtonEventHandler_t1283_0_0_0_Types };
extern const Il2CppType StrongName_t2246_0_0_0;
static const Il2CppType* GenInst_StrongName_t2246_0_0_0_Types[] = { &StrongName_t2246_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2246_0_0_0 = { 1, GenInst_StrongName_t2246_0_0_0_Types };
extern const Il2CppType DateTime_t74_0_0_0;
static const Il2CppType* GenInst_DateTime_t74_0_0_0_Types[] = { &DateTime_t74_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t74_0_0_0 = { 1, GenInst_DateTime_t74_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t2323_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t2323_0_0_0_Types[] = { &DateTimeOffset_t2323_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t2323_0_0_0 = { 1, GenInst_DateTimeOffset_t2323_0_0_0_Types };
extern const Il2CppType TimeSpan_t427_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t427_0_0_0_Types[] = { &TimeSpan_t427_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t427_0_0_0 = { 1, GenInst_TimeSpan_t427_0_0_0_Types };
extern const Il2CppType Guid_t452_0_0_0;
static const Il2CppType* GenInst_Guid_t452_0_0_0_Types[] = { &Guid_t452_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t452_0_0_0 = { 1, GenInst_Guid_t452_0_0_0_Types };
extern const Il2CppType IReflect_t4017_0_0_0;
static const Il2CppType* GenInst_IReflect_t4017_0_0_0_Types[] = { &IReflect_t4017_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t4017_0_0_0 = { 1, GenInst_IReflect_t4017_0_0_0_Types };
extern const Il2CppType _Type_t4015_0_0_0;
static const Il2CppType* GenInst__Type_t4015_0_0_0_Types[] = { &_Type_t4015_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t4015_0_0_0 = { 1, GenInst__Type_t4015_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t2427_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t2427_0_0_0_Types[] = { &ICustomAttributeProvider_t2427_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t2427_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t2427_0_0_0_Types };
extern const Il2CppType _MemberInfo_t4016_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t4016_0_0_0_Types[] = { &_MemberInfo_t4016_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t4016_0_0_0 = { 1, GenInst__MemberInfo_t4016_0_0_0_Types };
extern const Il2CppType IFormattable_t2429_0_0_0;
static const Il2CppType* GenInst_IFormattable_t2429_0_0_0_Types[] = { &IFormattable_t2429_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t2429_0_0_0 = { 1, GenInst_IFormattable_t2429_0_0_0_Types };
extern const Il2CppType IConvertible_t2432_0_0_0;
static const Il2CppType* GenInst_IConvertible_t2432_0_0_0_Types[] = { &IConvertible_t2432_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t2432_0_0_0 = { 1, GenInst_IConvertible_t2432_0_0_0_Types };
extern const Il2CppType IComparable_t2431_0_0_0;
static const Il2CppType* GenInst_IComparable_t2431_0_0_0_Types[] = { &IComparable_t2431_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t2431_0_0_0 = { 1, GenInst_IComparable_t2431_0_0_0_Types };
extern const Il2CppType IComparable_1_t4233_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4233_0_0_0_Types[] = { &IComparable_1_t4233_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4233_0_0_0 = { 1, GenInst_IComparable_1_t4233_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4238_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4238_0_0_0_Types[] = { &IEquatable_1_t4238_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4238_0_0_0 = { 1, GenInst_IEquatable_1_t4238_0_0_0_Types };
extern const Il2CppType ValueType_t1763_0_0_0;
static const Il2CppType* GenInst_ValueType_t1763_0_0_0_Types[] = { &ValueType_t1763_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1763_0_0_0 = { 1, GenInst_ValueType_t1763_0_0_0_Types };
extern const Il2CppType Double_t387_0_0_0;
static const Il2CppType* GenInst_Double_t387_0_0_0_Types[] = { &Double_t387_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t387_0_0_0 = { 1, GenInst_Double_t387_0_0_0_Types };
extern const Il2CppType IComparable_1_t4250_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4250_0_0_0_Types[] = { &IComparable_1_t4250_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4250_0_0_0 = { 1, GenInst_IComparable_1_t4250_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4255_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4255_0_0_0_Types[] = { &IEquatable_1_t4255_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4255_0_0_0 = { 1, GenInst_IEquatable_1_t4255_0_0_0_Types };
extern const Il2CppType IComparable_1_t4263_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4263_0_0_0_Types[] = { &IComparable_1_t4263_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4263_0_0_0 = { 1, GenInst_IComparable_1_t4263_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4268_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4268_0_0_0_Types[] = { &IEquatable_1_t4268_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4268_0_0_0 = { 1, GenInst_IEquatable_1_t4268_0_0_0_Types };
extern const Il2CppType Object_t335_0_0_0;
static const Il2CppType* GenInst_Object_t335_0_0_0_Types[] = { &Object_t335_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t335_0_0_0 = { 1, GenInst_Object_t335_0_0_0_Types };
extern const Il2CppType Behaviour_t772_0_0_0;
static const Il2CppType* GenInst_Behaviour_t772_0_0_0_Types[] = { &Behaviour_t772_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t772_0_0_0 = { 1, GenInst_Behaviour_t772_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2507_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2507_0_0_0_Types[] = { &KeyValuePair_2_t2507_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2507_0_0_0 = { 1, GenInst_KeyValuePair_2_t2507_0_0_0_Types };
extern const Il2CppType IEnumerable_t375_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t375_0_0_0_Types[] = { &IEnumerable_t375_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t375_0_0_0 = { 1, GenInst_IEnumerable_t375_0_0_0_Types };
extern const Il2CppType ICloneable_t4008_0_0_0;
static const Il2CppType* GenInst_ICloneable_t4008_0_0_0_Types[] = { &ICloneable_t4008_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t4008_0_0_0 = { 1, GenInst_ICloneable_t4008_0_0_0_Types };
extern const Il2CppType IComparable_1_t4309_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4309_0_0_0_Types[] = { &IComparable_1_t4309_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4309_0_0_0 = { 1, GenInst_IComparable_1_t4309_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4314_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4314_0_0_0_Types[] = { &IEquatable_1_t4314_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4314_0_0_0 = { 1, GenInst_IEquatable_1_t4314_0_0_0_Types };
extern const Il2CppType Link_t1869_0_0_0;
static const Il2CppType* GenInst_Link_t1869_0_0_0_Types[] = { &Link_t1869_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1869_0_0_0 = { 1, GenInst_Link_t1869_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t451_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t451_0_0_0_Types[] = { &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t451_0_0_0 = { 1, GenInst_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2507_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2507_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2507_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2507_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_FsmState_t29_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &String_t_0_0_0, &FsmState_t29_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FsmState_t29_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_String_t_0_0_0_FsmState_t29_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2520_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2520_0_0_0_Types[] = { &KeyValuePair_2_t2520_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2520_0_0_0 = { 1, GenInst_KeyValuePair_2_t2520_0_0_0_Types };
extern const Il2CppType GUIContent_t379_0_0_0;
static const Il2CppType* GenInst_GUIContent_t379_0_0_0_Types[] = { &GUIContent_t379_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIContent_t379_0_0_0 = { 1, GenInst_GUIContent_t379_0_0_0_Types };
extern const Il2CppType InputLayerElement_t58_0_0_0;
static const Il2CppType* GenInst_InputLayerElement_t58_0_0_0_Types[] = { &InputLayerElement_t58_0_0_0 };
extern const Il2CppGenericInst GenInst_InputLayerElement_t58_0_0_0 = { 1, GenInst_InputLayerElement_t58_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t372_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t372_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int32_t372_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2546_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2546_0_0_0_Types[] = { &KeyValuePair_2_t2546_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2546_0_0_0 = { 1, GenInst_KeyValuePair_2_t2546_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t372_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t372_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t372_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t372_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t372_0_0_0_Int32_t372_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t372_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t372_0_0_0_Int32_t372_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t372_0_0_0_Int32_t372_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t372_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t372_0_0_0_KeyValuePair_2_t2546_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t372_0_0_0, &KeyValuePair_2_t2546_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t372_0_0_0_KeyValuePair_2_t2546_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t372_0_0_0_KeyValuePair_2_t2546_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t372_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2560_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2560_0_0_0_Types[] = { &KeyValuePair_2_t2560_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2560_0_0_0 = { 1, GenInst_KeyValuePair_2_t2560_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t434_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t434_0_0_0_Types[] = { &KeyValuePair_2_t434_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t434_0_0_0 = { 1, GenInst_KeyValuePair_2_t434_0_0_0_Types };
extern const Il2CppType Matrix4x4_t404_0_0_0;
static const Il2CppType* GenInst_Matrix4x4_t404_0_0_0_Types[] = { &Matrix4x4_t404_0_0_0 };
extern const Il2CppGenericInst GenInst_Matrix4x4_t404_0_0_0 = { 1, GenInst_Matrix4x4_t404_0_0_0_Types };
extern const Il2CppType BoneWeight_t405_0_0_0;
static const Il2CppType* GenInst_BoneWeight_t405_0_0_0_Types[] = { &BoneWeight_t405_0_0_0 };
extern const Il2CppGenericInst GenInst_BoneWeight_t405_0_0_0 = { 1, GenInst_BoneWeight_t405_0_0_0_Types };
extern const Il2CppType NotificationHandlerComponent_t84_0_0_0;
static const Il2CppType* GenInst_NotificationHandlerComponent_t84_0_0_0_Types[] = { &NotificationHandlerComponent_t84_0_0_0 };
extern const Il2CppGenericInst GenInst_NotificationHandlerComponent_t84_0_0_0 = { 1, GenInst_NotificationHandlerComponent_t84_0_0_0_Types };
extern const Il2CppType PrefabItem_t154_0_0_0;
static const Il2CppType* GenInst_PrefabItem_t154_0_0_0_Types[] = { &PrefabItem_t154_0_0_0 };
extern const Il2CppGenericInst GenInst_PrefabItem_t154_0_0_0 = { 1, GenInst_PrefabItem_t154_0_0_0_Types };
extern const Il2CppType PruneData_t93_0_0_0;
static const Il2CppType* GenInst_PruneData_t93_0_0_0_Types[] = { &PruneData_t93_0_0_0 };
extern const Il2CppGenericInst GenInst_PruneData_t93_0_0_0 = { 1, GenInst_PruneData_t93_0_0_0_Types };
extern const Il2CppType PreloadData_t94_0_0_0;
static const Il2CppType* GenInst_PreloadData_t94_0_0_0_Types[] = { &PreloadData_t94_0_0_0 };
extern const Il2CppGenericInst GenInst_PreloadData_t94_0_0_0 = { 1, GenInst_PreloadData_t94_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_QueryResultResolver_t328_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &String_t_0_0_0, &QueryResultResolver_t328_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_QueryResultResolver_t328_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_String_t_0_0_0_QueryResultResolver_t328_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2593_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2593_0_0_0_Types[] = { &KeyValuePair_2_t2593_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2593_0_0_0 = { 1, GenInst_KeyValuePair_2_t2593_0_0_0_Types };
extern const Il2CppType PrefabItemLists_t151_0_0_0;
static const Il2CppType* GenInst_PrefabItemLists_t151_0_0_0_Types[] = { &PrefabItemLists_t151_0_0_0 };
extern const Il2CppGenericInst GenInst_PrefabItemLists_t151_0_0_0 = { 1, GenInst_PrefabItemLists_t151_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Signal_t116_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &String_t_0_0_0, &Signal_t116_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Signal_t116_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_String_t_0_0_0_Signal_t116_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2611_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2611_0_0_0_Types[] = { &KeyValuePair_2_t2611_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2611_0_0_0 = { 1, GenInst_KeyValuePair_2_t2611_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_TimeReference_t14_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &String_t_0_0_0, &TimeReference_t14_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TimeReference_t14_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_String_t_0_0_0_TimeReference_t14_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2617_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2617_0_0_0_Types[] = { &KeyValuePair_2_t2617_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2617_0_0_0 = { 1, GenInst_KeyValuePair_2_t2617_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Type_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Type_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2628_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2628_0_0_0_Types[] = { &KeyValuePair_2_t2628_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2628_0_0_0 = { 1, GenInst_KeyValuePair_2_t2628_0_0_0_Types };
extern const Il2CppType Byte_t391_0_0_0;
static const Il2CppType* GenInst_Byte_t391_0_0_0_Types[] = { &Byte_t391_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t391_0_0_0 = { 1, GenInst_Byte_t391_0_0_0_Types };
extern const Il2CppType IComparable_1_t4395_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4395_0_0_0_Types[] = { &IComparable_1_t4395_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4395_0_0_0 = { 1, GenInst_IComparable_1_t4395_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4400_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4400_0_0_0_Types[] = { &IEquatable_1_t4400_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4400_0_0_0 = { 1, GenInst_IEquatable_1_t4400_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t422_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t422_0_0_0_Types[] = { &KeyValuePair_2_t422_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t422_0_0_0 = { 1, GenInst_KeyValuePair_2_t422_0_0_0_Types };
extern const Il2CppType WaitHandle_t351_0_0_0;
static const Il2CppType* GenInst_WaitHandle_t351_0_0_0_Types[] = { &WaitHandle_t351_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitHandle_t351_0_0_0 = { 1, GenInst_WaitHandle_t351_0_0_0_Types };
extern const Il2CppType IDisposable_t364_0_0_0;
static const Il2CppType* GenInst_IDisposable_t364_0_0_0_Types[] = { &IDisposable_t364_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t364_0_0_0 = { 1, GenInst_IDisposable_t364_0_0_0_Types };
extern const Il2CppType MarshalByRefObject_t1643_0_0_0;
static const Il2CppType* GenInst_MarshalByRefObject_t1643_0_0_0_Types[] = { &MarshalByRefObject_t1643_0_0_0 };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t1643_0_0_0 = { 1, GenInst_MarshalByRefObject_t1643_0_0_0_Types };
extern const Il2CppType TaskWorker_t168_0_0_0;
static const Il2CppType* GenInst_TaskWorker_t168_0_0_0_Types[] = { &TaskWorker_t168_0_0_0 };
extern const Il2CppGenericInst GenInst_TaskWorker_t168_0_0_0 = { 1, GenInst_TaskWorker_t168_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_Types };
static const Il2CppType* GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_Types[] = { &EScreens_t188_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EScreens_t188_0_0_0_Object_t_0_0_0 = { 2, GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2667_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2667_0_0_0_Types[] = { &KeyValuePair_2_t2667_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2667_0_0_0 = { 1, GenInst_KeyValuePair_2_t2667_0_0_0_Types };
static const Il2CppType* GenInst_EScreens_t188_0_0_0_Types[] = { &EScreens_t188_0_0_0 };
extern const Il2CppGenericInst GenInst_EScreens_t188_0_0_0 = { 1, GenInst_EScreens_t188_0_0_0_Types };
static const Il2CppType* GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_EScreens_t188_0_0_0_Types[] = { &EScreens_t188_0_0_0, &Object_t_0_0_0, &EScreens_t188_0_0_0 };
extern const Il2CppGenericInst GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_EScreens_t188_0_0_0 = { 3, GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_EScreens_t188_0_0_0_Types };
static const Il2CppType* GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &EScreens_t188_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &EScreens_t188_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2667_0_0_0_Types[] = { &EScreens_t188_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2667_0_0_0 };
extern const Il2CppGenericInst GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2667_0_0_0 = { 3, GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2667_0_0_0_Types };
static const Il2CppType* GenInst_EScreens_t188_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &EScreens_t188_0_0_0, &String_t_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_EScreens_t188_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_EScreens_t188_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2681_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2681_0_0_0_Types[] = { &KeyValuePair_2_t2681_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2681_0_0_0 = { 1, GenInst_KeyValuePair_2_t2681_0_0_0_Types };
static const Il2CppType* GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_Types[] = { &ESubScreens_t189_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0 = { 2, GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2690_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2690_0_0_0_Types[] = { &KeyValuePair_2_t2690_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2690_0_0_0 = { 1, GenInst_KeyValuePair_2_t2690_0_0_0_Types };
static const Il2CppType* GenInst_ESubScreens_t189_0_0_0_Types[] = { &ESubScreens_t189_0_0_0 };
extern const Il2CppGenericInst GenInst_ESubScreens_t189_0_0_0 = { 1, GenInst_ESubScreens_t189_0_0_0_Types };
static const Il2CppType* GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_ESubScreens_t189_0_0_0_Types[] = { &ESubScreens_t189_0_0_0, &Object_t_0_0_0, &ESubScreens_t189_0_0_0 };
extern const Il2CppGenericInst GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_ESubScreens_t189_0_0_0 = { 3, GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_ESubScreens_t189_0_0_0_Types };
static const Il2CppType* GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &ESubScreens_t189_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &ESubScreens_t189_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2690_0_0_0_Types[] = { &ESubScreens_t189_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2690_0_0_0 };
extern const Il2CppGenericInst GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2690_0_0_0 = { 3, GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2690_0_0_0_Types };
static const Il2CppType* GenInst_ESubScreens_t189_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &ESubScreens_t189_0_0_0, &String_t_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_ESubScreens_t189_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_ESubScreens_t189_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2704_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2704_0_0_0_Types[] = { &KeyValuePair_2_t2704_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2704_0_0_0 = { 1, GenInst_KeyValuePair_2_t2704_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_Int32_t372_0_0_0_Types[] = { &Int32_t372_0_0_0, &ERaffleResult_t207_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_Int32_t372_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_Int32_t372_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_ERaffleResult_t207_0_0_0_Types[] = { &Int32_t372_0_0_0, &ERaffleResult_t207_0_0_0, &ERaffleResult_t207_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_ERaffleResult_t207_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_ERaffleResult_t207_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &ERaffleResult_t207_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_KeyValuePair_2_t352_0_0_0_Types[] = { &Int32_t372_0_0_0, &ERaffleResult_t207_0_0_0, &KeyValuePair_2_t352_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_KeyValuePair_2_t352_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_KeyValuePair_2_t352_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2740_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2740_0_0_0_Types[] = { &KeyValuePair_2_t2740_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2740_0_0_0 = { 1, GenInst_KeyValuePair_2_t2740_0_0_0_Types };
extern const Il2CppType IComparable_1_t4464_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4464_0_0_0_Types[] = { &IComparable_1_t4464_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4464_0_0_0 = { 1, GenInst_IComparable_1_t4464_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4469_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4469_0_0_0_Types[] = { &IEquatable_1_t4469_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4469_0_0_0 = { 1, GenInst_IEquatable_1_t4469_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t384_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_Boolean_t384_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t384_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_Boolean_t384_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_Boolean_t384_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t384_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_KeyValuePair_2_t2740_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t384_0_0_0, &KeyValuePair_2_t2740_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_KeyValuePair_2_t2740_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_KeyValuePair_2_t2740_0_0_0_Types };
static const Il2CppType* GenInst_InputField_t226_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &InputField_t226_0_0_0, &Boolean_t384_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t226_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_InputField_t226_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2755_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2755_0_0_0_Types[] = { &KeyValuePair_2_t2755_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2755_0_0_0 = { 1, GenInst_KeyValuePair_2_t2755_0_0_0_Types };
static const Il2CppType* GenInst_InputField_t226_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &InputField_t226_0_0_0, &String_t_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t226_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_InputField_t226_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2760_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2760_0_0_0_Types[] = { &KeyValuePair_2_t2760_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2760_0_0_0 = { 1, GenInst_KeyValuePair_2_t2760_0_0_0_Types };
static const Il2CppType* GenInst_InputField_t226_0_0_0_Image_t213_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &InputField_t226_0_0_0, &Image_t213_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t226_0_0_0_Image_t213_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_InputField_t226_0_0_0_Image_t213_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2766_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2766_0_0_0_Types[] = { &KeyValuePair_2_t2766_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2766_0_0_0 = { 1, GenInst_KeyValuePair_2_t2766_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t912_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t912_0_0_0_Types[] = { &GUILayoutOption_t912_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t912_0_0_0 = { 1, GenInst_GUILayoutOption_t912_0_0_0_Types };
extern const Il2CppType IComparable_1_t4494_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4494_0_0_0_Types[] = { &IComparable_1_t4494_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4494_0_0_0 = { 1, GenInst_IComparable_1_t4494_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4499_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4499_0_0_0_Types[] = { &IEquatable_1_t4499_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4499_0_0_0 = { 1, GenInst_IEquatable_1_t4499_0_0_0_Types };
extern const Il2CppType Rect_t267_0_0_0;
static const Il2CppType* GenInst_Rect_t267_0_0_0_Types[] = { &Rect_t267_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t267_0_0_0 = { 1, GenInst_Rect_t267_0_0_0_Types };
extern const Il2CppType Material_t82_0_0_0;
static const Il2CppType* GenInst_Material_t82_0_0_0_Types[] = { &Material_t82_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t82_0_0_0 = { 1, GenInst_Material_t82_0_0_0_Types };
extern const Il2CppType List_1_t344_0_0_0;
static const Il2CppType* GenInst_List_1_t344_0_0_0_Types[] = { &List_1_t344_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t344_0_0_0 = { 1, GenInst_List_1_t344_0_0_0_Types };
extern const Il2CppType List_1_t718_0_0_0;
static const Il2CppType* GenInst_List_1_t718_0_0_0_Types[] = { &List_1_t718_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t718_0_0_0 = { 1, GenInst_List_1_t718_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t372_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2871_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2871_0_0_0_Types[] = { &KeyValuePair_2_t2871_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2871_0_0_0 = { 1, GenInst_KeyValuePair_2_t2871_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Object_t_0_0_0_Int32_t372_0_0_0_Types[] = { &Int32_t372_0_0_0, &Object_t_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Object_t_0_0_0_Int32_t372_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_Object_t_0_0_0_Int32_t372_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t372_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2871_0_0_0_Types[] = { &Int32_t372_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2871_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2871_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2871_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_PointerEventData_t517_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &PointerEventData_t517_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_PointerEventData_t517_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_PointerEventData_t517_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t724_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t724_0_0_0_Types[] = { &KeyValuePair_2_t724_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t724_0_0_0 = { 1, GenInst_KeyValuePair_2_t724_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t729_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t729_0_0_0_Types[] = { &RaycastHit2D_t729_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t729_0_0_0 = { 1, GenInst_RaycastHit2D_t729_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t708_0_0_0_Int32_t372_0_0_0_Types[] = { &ICanvasElement_t708_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t708_0_0_0_Int32_t372_0_0_0 = { 2, GenInst_ICanvasElement_t708_0_0_0_Int32_t372_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t708_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &ICanvasElement_t708_0_0_0, &Int32_t372_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t708_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_ICanvasElement_t708_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType List_1_t734_0_0_0;
static const Il2CppType* GenInst_List_1_t734_0_0_0_Types[] = { &List_1_t734_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t734_0_0_0 = { 1, GenInst_List_1_t734_0_0_0_Types };
static const Il2CppType* GenInst_Font_t569_0_0_0_List_1_t738_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Font_t569_0_0_0, &List_1_t738_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t569_0_0_0_List_1_t738_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Font_t569_0_0_0_List_1_t738_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2934_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2934_0_0_0_Types[] = { &KeyValuePair_2_t2934_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2934_0_0_0 = { 1, GenInst_KeyValuePair_2_t2934_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t572_0_0_0_Int32_t372_0_0_0_Types[] = { &Graphic_t572_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t572_0_0_0_Int32_t372_0_0_0 = { 2, GenInst_Graphic_t572_0_0_0_Int32_t372_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t572_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Graphic_t572_0_0_0, &Int32_t372_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t572_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Graphic_t572_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t574_0_0_0_IndexedSet_1_t747_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Canvas_t574_0_0_0, &IndexedSet_1_t747_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t574_0_0_0_IndexedSet_1_t747_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Canvas_t574_0_0_0_IndexedSet_1_t747_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2958_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2958_0_0_0_Types[] = { &KeyValuePair_2_t2958_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2958_0_0_0 = { 1, GenInst_KeyValuePair_2_t2958_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2962_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2962_0_0_0_Types[] = { &KeyValuePair_2_t2962_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2962_0_0_0 = { 1, GenInst_KeyValuePair_2_t2962_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2966_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2966_0_0_0_Types[] = { &KeyValuePair_2_t2966_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2966_0_0_0 = { 1, GenInst_KeyValuePair_2_t2966_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t717_0_0_0_Int32_t372_0_0_0_Types[] = { &IClipper_t717_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t717_0_0_0_Int32_t372_0_0_0 = { 2, GenInst_IClipper_t717_0_0_0_Int32_t372_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t717_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &IClipper_t717_0_0_0, &Int32_t372_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t717_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_IClipper_t717_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3017_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3017_0_0_0_Types[] = { &KeyValuePair_2_t3017_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3017_0_0_0 = { 1, GenInst_KeyValuePair_2_t3017_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t668_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t668_0_0_0_Types[] = { &LayoutRebuilder_t668_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t668_0_0_0 = { 1, GenInst_LayoutRebuilder_t668_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t388_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t388_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t388_0_0_0 = { 2, GenInst_Object_t_0_0_0_Single_t388_0_0_0_Types };
extern const Il2CppType List_1_t675_0_0_0;
static const Il2CppType* GenInst_List_1_t675_0_0_0_Types[] = { &List_1_t675_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t675_0_0_0 = { 1, GenInst_List_1_t675_0_0_0_Types };
extern const Il2CppType List_1_t676_0_0_0;
static const Il2CppType* GenInst_List_1_t676_0_0_0_Types[] = { &List_1_t676_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t676_0_0_0 = { 1, GenInst_List_1_t676_0_0_0_Types };
extern const Il2CppType List_1_t677_0_0_0;
static const Il2CppType* GenInst_List_1_t677_0_0_0_Types[] = { &List_1_t677_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t677_0_0_0 = { 1, GenInst_List_1_t677_0_0_0_Types };
extern const Il2CppType List_1_t678_0_0_0;
static const Il2CppType* GenInst_List_1_t678_0_0_0_Types[] = { &List_1_t678_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t678_0_0_0 = { 1, GenInst_List_1_t678_0_0_0_Types };
extern const Il2CppType List_1_t679_0_0_0;
static const Il2CppType* GenInst_List_1_t679_0_0_0_Types[] = { &List_1_t679_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t679_0_0_0 = { 1, GenInst_List_1_t679_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3969_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3969_0_0_0_Types[] = { &IAchievementDescription_t3969_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3969_0_0_0 = { 1, GenInst_IAchievementDescription_t3969_0_0_0_Types };
extern const Il2CppType IAchievement_t1003_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1003_0_0_0_Types[] = { &IAchievement_t1003_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1003_0_0_0 = { 1, GenInst_IAchievement_t1003_0_0_0_Types };
extern const Il2CppType IScore_t958_0_0_0;
static const Il2CppType* GenInst_IScore_t958_0_0_0_Types[] = { &IScore_t958_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t958_0_0_0 = { 1, GenInst_IScore_t958_0_0_0_Types };
extern const Il2CppType IUserProfile_t3968_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t3968_0_0_0_Types[] = { &IUserProfile_t3968_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t3968_0_0_0 = { 1, GenInst_IUserProfile_t3968_0_0_0_Types };
extern const Il2CppType AchievementDescription_t954_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t954_0_0_0_Types[] = { &AchievementDescription_t954_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t954_0_0_0 = { 1, GenInst_AchievementDescription_t954_0_0_0_Types };
extern const Il2CppType UserProfile_t951_0_0_0;
static const Il2CppType* GenInst_UserProfile_t951_0_0_0_Types[] = { &UserProfile_t951_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t951_0_0_0 = { 1, GenInst_UserProfile_t951_0_0_0_Types };
extern const Il2CppType GcAchievementData_t939_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t939_0_0_0_Types[] = { &GcAchievementData_t939_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t939_0_0_0 = { 1, GenInst_GcAchievementData_t939_0_0_0_Types };
extern const Il2CppType Achievement_t953_0_0_0;
static const Il2CppType* GenInst_Achievement_t953_0_0_0_Types[] = { &Achievement_t953_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t953_0_0_0 = { 1, GenInst_Achievement_t953_0_0_0_Types };
extern const Il2CppType GcScoreData_t940_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t940_0_0_0_Types[] = { &GcScoreData_t940_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t940_0_0_0 = { 1, GenInst_GcScoreData_t940_0_0_0_Types };
extern const Il2CppType Score_t955_0_0_0;
static const Il2CppType* GenInst_Score_t955_0_0_0_Types[] = { &Score_t955_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t955_0_0_0 = { 1, GenInst_Score_t955_0_0_0_Types };
extern const Il2CppType Display_t845_0_0_0;
static const Il2CppType* GenInst_Display_t845_0_0_0_Types[] = { &Display_t845_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t845_0_0_0 = { 1, GenInst_Display_t845_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType ISerializable_t2454_0_0_0;
static const Il2CppType* GenInst_ISerializable_t2454_0_0_0_Types[] = { &ISerializable_t2454_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t2454_0_0_0 = { 1, GenInst_ISerializable_t2454_0_0_0_Types };
extern const Il2CppType ContactPoint_t863_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t863_0_0_0_Types[] = { &ContactPoint_t863_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t863_0_0_0 = { 1, GenInst_ContactPoint_t863_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t869_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t869_0_0_0_Types[] = { &ContactPoint2D_t869_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t869_0_0_0 = { 1, GenInst_ContactPoint2D_t869_0_0_0_Types };
extern const Il2CppType WebCamDevice_t876_0_0_0;
static const Il2CppType* GenInst_WebCamDevice_t876_0_0_0_Types[] = { &WebCamDevice_t876_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamDevice_t876_0_0_0 = { 1, GenInst_WebCamDevice_t876_0_0_0_Types };
extern const Il2CppType Keyframe_t883_0_0_0;
static const Il2CppType* GenInst_Keyframe_t883_0_0_0_Types[] = { &Keyframe_t883_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t883_0_0_0 = { 1, GenInst_Keyframe_t883_0_0_0_Types };
extern const Il2CppType CharacterInfo_t892_0_0_0;
static const Il2CppType* GenInst_CharacterInfo_t892_0_0_0_Types[] = { &CharacterInfo_t892_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterInfo_t892_0_0_0 = { 1, GenInst_CharacterInfo_t892_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_LayoutCache_t904_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &LayoutCache_t904_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_LayoutCache_t904_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_LayoutCache_t904_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3152_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3152_0_0_0_Types[] = { &KeyValuePair_2_t3152_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3152_0_0_0 = { 1, GenInst_KeyValuePair_2_t3152_0_0_0_Types };
static const Il2CppType* GenInst_GUIStyle_t342_0_0_0_Types[] = { &GUIStyle_t342_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t342_0_0_0 = { 1, GenInst_GUIStyle_t342_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t342_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t342_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t342_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t342_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3163_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3163_0_0_0_Types[] = { &KeyValuePair_2_t3163_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3163_0_0_0 = { 1, GenInst_KeyValuePair_2_t3163_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t929_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t929_0_0_0_Types[] = { &DisallowMultipleComponent_t929_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t929_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t929_0_0_0_Types };
extern const Il2CppType Attribute_t463_0_0_0;
static const Il2CppType* GenInst_Attribute_t463_0_0_0_Types[] = { &Attribute_t463_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t463_0_0_0 = { 1, GenInst_Attribute_t463_0_0_0_Types };
extern const Il2CppType _Attribute_t4004_0_0_0;
static const Il2CppType* GenInst__Attribute_t4004_0_0_0_Types[] = { &_Attribute_t4004_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t4004_0_0_0 = { 1, GenInst__Attribute_t4004_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t932_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t932_0_0_0_Types[] = { &ExecuteInEditMode_t932_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t932_0_0_0 = { 1, GenInst_ExecuteInEditMode_t932_0_0_0_Types };
extern const Il2CppType RequireComponent_t930_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t930_0_0_0_Types[] = { &RequireComponent_t930_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t930_0_0_0 = { 1, GenInst_RequireComponent_t930_0_0_0_Types };
extern const Il2CppType ParameterModifier_t2024_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t2024_0_0_0_Types[] = { &ParameterModifier_t2024_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t2024_0_0_0 = { 1, GenInst_ParameterModifier_t2024_0_0_0_Types };
extern const Il2CppType HitInfo_t959_0_0_0;
static const Il2CppType* GenInst_HitInfo_t959_0_0_0_Types[] = { &HitInfo_t959_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t959_0_0_0 = { 1, GenInst_HitInfo_t959_0_0_0_Types };
extern const Il2CppType ParameterInfo_t1035_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t1035_0_0_0_Types[] = { &ParameterInfo_t1035_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t1035_0_0_0 = { 1, GenInst_ParameterInfo_t1035_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t4058_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t4058_0_0_0_Types[] = { &_ParameterInfo_t4058_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t4058_0_0_0 = { 1, GenInst__ParameterInfo_t4058_0_0_0_Types };
extern const Il2CppType Event_t381_0_0_0;
extern const Il2CppType TextEditOp_t976_0_0_0;
static const Il2CppType* GenInst_Event_t381_0_0_0_TextEditOp_t976_0_0_0_Types[] = { &Event_t381_0_0_0, &TextEditOp_t976_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t381_0_0_0_TextEditOp_t976_0_0_0 = { 2, GenInst_Event_t381_0_0_0_TextEditOp_t976_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t976_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0 = { 2, GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3183_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3183_0_0_0_Types[] = { &KeyValuePair_2_t3183_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3183_0_0_0 = { 1, GenInst_KeyValuePair_2_t3183_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t976_0_0_0_Types[] = { &TextEditOp_t976_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t976_0_0_0 = { 1, GenInst_TextEditOp_t976_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t976_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_TextEditOp_t976_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t976_0_0_0, &TextEditOp_t976_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_TextEditOp_t976_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_TextEditOp_t976_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t976_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_KeyValuePair_2_t3183_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t976_0_0_0, &KeyValuePair_2_t3183_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_KeyValuePair_2_t3183_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_KeyValuePair_2_t3183_0_0_0_Types };
static const Il2CppType* GenInst_Event_t381_0_0_0_Types[] = { &Event_t381_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t381_0_0_0 = { 1, GenInst_Event_t381_0_0_0_Types };
static const Il2CppType* GenInst_Event_t381_0_0_0_TextEditOp_t976_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Event_t381_0_0_0, &TextEditOp_t976_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t381_0_0_0_TextEditOp_t976_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Event_t381_0_0_0_TextEditOp_t976_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3197_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3197_0_0_0_Types[] = { &KeyValuePair_2_t3197_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3197_0_0_0 = { 1, GenInst_KeyValuePair_2_t3197_0_0_0_Types };
extern const Il2CppType EyewearCalibrationReading_t1080_0_0_0;
static const Il2CppType* GenInst_EyewearCalibrationReading_t1080_0_0_0_Types[] = { &EyewearCalibrationReading_t1080_0_0_0 };
extern const Il2CppGenericInst GenInst_EyewearCalibrationReading_t1080_0_0_0 = { 1, GenInst_EyewearCalibrationReading_t1080_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t6_0_0_0_VideoBackgroundAbstractBehaviour_t315_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Camera_t6_0_0_0, &VideoBackgroundAbstractBehaviour_t315_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t6_0_0_0_VideoBackgroundAbstractBehaviour_t315_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Camera_t6_0_0_0_VideoBackgroundAbstractBehaviour_t315_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3227_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3227_0_0_0_Types[] = { &KeyValuePair_2_t3227_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3227_0_0_0 = { 1, GenInst_KeyValuePair_2_t3227_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_Types[] = { &PIXEL_FORMAT_t1108_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3257_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3257_0_0_0_Types[] = { &KeyValuePair_2_t3257_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3257_0_0_0 = { 1, GenInst_KeyValuePair_2_t3257_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t1108_0_0_0_Types[] = { &PIXEL_FORMAT_t1108_0_0_0, &Object_t_0_0_0, &PIXEL_FORMAT_t1108_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t1108_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t1108_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &PIXEL_FORMAT_t1108_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &PIXEL_FORMAT_t1108_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3257_0_0_0_Types[] = { &PIXEL_FORMAT_t1108_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t3257_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3257_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3257_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t1108_0_0_0_Image_t1109_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &PIXEL_FORMAT_t1108_0_0_0, &Image_t1109_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t1108_0_0_0_Image_t1109_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t1108_0_0_0_Image_t1109_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3271_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3271_0_0_0_Types[] = { &KeyValuePair_2_t3271_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3271_0_0_0 = { 1, GenInst_KeyValuePair_2_t3271_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Trackable_t1060_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &Trackable_t1060_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Trackable_t1060_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_Trackable_t1060_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3284_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3284_0_0_0_Types[] = { &KeyValuePair_2_t3284_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3284_0_0_0 = { 1, GenInst_KeyValuePair_2_t3284_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_VirtualButton_t1223_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &VirtualButton_t1223_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_VirtualButton_t1223_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_VirtualButton_t1223_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3292_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3292_0_0_0_Types[] = { &KeyValuePair_2_t3292_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3292_0_0_0 = { 1, GenInst_KeyValuePair_2_t3292_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Marker_t1231_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &Marker_t1231_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Marker_t1231_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_Marker_t1231_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3308_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3308_0_0_0_Types[] = { &KeyValuePair_2_t3308_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3308_0_0_0 = { 1, GenInst_KeyValuePair_2_t3308_0_0_0_Types };
extern const Il2CppType SmartTerrainRevisionData_t1142_0_0_0;
static const Il2CppType* GenInst_SmartTerrainRevisionData_t1142_0_0_0_Types[] = { &SmartTerrainRevisionData_t1142_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainRevisionData_t1142_0_0_0 = { 1, GenInst_SmartTerrainRevisionData_t1142_0_0_0_Types };
extern const Il2CppType SurfaceData_t1143_0_0_0;
static const Il2CppType* GenInst_SurfaceData_t1143_0_0_0_Types[] = { &SurfaceData_t1143_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceData_t1143_0_0_0 = { 1, GenInst_SurfaceData_t1143_0_0_0_Types };
extern const Il2CppType PropData_t1144_0_0_0;
static const Il2CppType* GenInst_PropData_t1144_0_0_0_Types[] = { &PropData_t1144_0_0_0 };
extern const Il2CppGenericInst GenInst_PropData_t1144_0_0_0 = { 1, GenInst_PropData_t1144_0_0_0_Types };
extern const Il2CppType IEditorTrackableBehaviour_t1315_0_0_0;
static const Il2CppType* GenInst_IEditorTrackableBehaviour_t1315_0_0_0_Types[] = { &IEditorTrackableBehaviour_t1315_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorTrackableBehaviour_t1315_0_0_0 = { 1, GenInst_IEditorTrackableBehaviour_t1315_0_0_0_Types };
static const Il2CppType* GenInst_Image_t1109_0_0_0_Types[] = { &Image_t1109_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t1109_0_0_0 = { 1, GenInst_Image_t1109_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t393_0_0_0 = { 2, GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3337_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3337_0_0_0_Types[] = { &KeyValuePair_2_t3337_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3337_0_0_0 = { 1, GenInst_KeyValuePair_2_t3337_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t393_0_0_0_Types[] = { &UInt16_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t393_0_0_0 = { 1, GenInst_UInt16_t393_0_0_0_Types };
extern const Il2CppType IComparable_1_t4819_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4819_0_0_0_Types[] = { &IComparable_1_t4819_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4819_0_0_0 = { 1, GenInst_IComparable_1_t4819_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4824_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4824_0_0_0_Types[] = { &IEquatable_1_t4824_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4824_0_0_0 = { 1, GenInst_IEquatable_1_t4824_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t393_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_UInt16_t393_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t393_0_0_0, &UInt16_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_UInt16_t393_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_UInt16_t393_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t393_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_KeyValuePair_2_t3337_0_0_0_Types[] = { &Object_t_0_0_0, &UInt16_t393_0_0_0, &KeyValuePair_2_t3337_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_KeyValuePair_2_t3337_0_0_0 = { 3, GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_KeyValuePair_2_t3337_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t393_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t393_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t393_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Type_t_0_0_0_UInt16_t393_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3352_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3352_0_0_0_Types[] = { &KeyValuePair_2_t3352_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3352_0_0_0 = { 1, GenInst_KeyValuePair_2_t3352_0_0_0_Types };
extern const Il2CppType RectangleData_t1089_0_0_0;
static const Il2CppType* GenInst_RectangleData_t1089_0_0_0_Types[] = { &RectangleData_t1089_0_0_0 };
extern const Il2CppGenericInst GenInst_RectangleData_t1089_0_0_0 = { 1, GenInst_RectangleData_t1089_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_WordResult_t1188_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &WordResult_t1188_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_WordResult_t1188_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_WordResult_t1188_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3359_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3359_0_0_0_Types[] = { &KeyValuePair_2_t3359_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3359_0_0_0 = { 1, GenInst_KeyValuePair_2_t3359_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_WordAbstractBehaviour_t327_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &WordAbstractBehaviour_t327_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_WordAbstractBehaviour_t327_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_WordAbstractBehaviour_t327_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1334_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1334_0_0_0_Types[] = { &KeyValuePair_2_t1334_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1334_0_0_0 = { 1, GenInst_KeyValuePair_2_t1334_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1186_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1186_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1186_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1186_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3378_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3378_0_0_0_Types[] = { &KeyValuePair_2_t3378_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3378_0_0_0 = { 1, GenInst_KeyValuePair_2_t3378_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1186_0_0_0_Types[] = { &List_1_t1186_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1186_0_0_0 = { 1, GenInst_List_1_t1186_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Surface_t358_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &Surface_t358_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Surface_t358_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_Surface_t358_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1349_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1349_0_0_0_Types[] = { &KeyValuePair_2_t1349_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1349_0_0_0 = { 1, GenInst_KeyValuePair_2_t1349_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_SurfaceAbstractBehaviour_t306_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &SurfaceAbstractBehaviour_t306_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_SurfaceAbstractBehaviour_t306_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_SurfaceAbstractBehaviour_t306_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3399_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3399_0_0_0_Types[] = { &KeyValuePair_2_t3399_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3399_0_0_0 = { 1, GenInst_KeyValuePair_2_t3399_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Prop_t357_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &Prop_t357_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Prop_t357_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_Prop_t357_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1348_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1348_0_0_0_Types[] = { &KeyValuePair_2_t1348_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1348_0_0_0 = { 1, GenInst_KeyValuePair_2_t1348_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_PropAbstractBehaviour_t300_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &PropAbstractBehaviour_t300_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_PropAbstractBehaviour_t300_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_PropAbstractBehaviour_t300_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3405_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3405_0_0_0_Types[] = { &KeyValuePair_2_t3405_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3405_0_0_0 = { 1, GenInst_KeyValuePair_2_t3405_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_TrackableBehaviour_t211_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &TrackableBehaviour_t211_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_TrackableBehaviour_t211_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_TrackableBehaviour_t211_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3419_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3419_0_0_0_Types[] = { &KeyValuePair_2_t3419_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3419_0_0_0 = { 1, GenInst_KeyValuePair_2_t3419_0_0_0_Types };
extern const Il2CppType MarkerAbstractBehaviour_t293_0_0_0;
static const Il2CppType* GenInst_MarkerAbstractBehaviour_t293_0_0_0_Types[] = { &MarkerAbstractBehaviour_t293_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerAbstractBehaviour_t293_0_0_0 = { 1, GenInst_MarkerAbstractBehaviour_t293_0_0_0_Types };
extern const Il2CppType IEditorMarkerBehaviour_t1364_0_0_0;
static const Il2CppType* GenInst_IEditorMarkerBehaviour_t1364_0_0_0_Types[] = { &IEditorMarkerBehaviour_t1364_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorMarkerBehaviour_t1364_0_0_0 = { 1, GenInst_IEditorMarkerBehaviour_t1364_0_0_0_Types };
extern const Il2CppType WorldCenterTrackableBehaviour_t1153_0_0_0;
static const Il2CppType* GenInst_WorldCenterTrackableBehaviour_t1153_0_0_0_Types[] = { &WorldCenterTrackableBehaviour_t1153_0_0_0 };
extern const Il2CppGenericInst GenInst_WorldCenterTrackableBehaviour_t1153_0_0_0 = { 1, GenInst_WorldCenterTrackableBehaviour_t1153_0_0_0_Types };
extern const Il2CppType IEditorDataSetTrackableBehaviour_t1366_0_0_0;
static const Il2CppType* GenInst_IEditorDataSetTrackableBehaviour_t1366_0_0_0_Types[] = { &IEditorDataSetTrackableBehaviour_t1366_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorDataSetTrackableBehaviour_t1366_0_0_0 = { 1, GenInst_IEditorDataSetTrackableBehaviour_t1366_0_0_0_Types };
extern const Il2CppType IEditorVirtualButtonBehaviour_t1381_0_0_0;
static const Il2CppType* GenInst_IEditorVirtualButtonBehaviour_t1381_0_0_0_Types[] = { &IEditorVirtualButtonBehaviour_t1381_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorVirtualButtonBehaviour_t1381_0_0_0 = { 1, GenInst_IEditorVirtualButtonBehaviour_t1381_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3425_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3425_0_0_0_Types[] = { &KeyValuePair_2_t3425_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3425_0_0_0 = { 1, GenInst_KeyValuePair_2_t3425_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_Int32_t372_0_0_0_Types[] = { &Int32_t372_0_0_0, &TrackableResultData_t1134_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_Int32_t372_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_Int32_t372_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_TrackableResultData_t1134_0_0_0_Types[] = { &Int32_t372_0_0_0, &TrackableResultData_t1134_0_0_0, &TrackableResultData_t1134_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_TrackableResultData_t1134_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_TrackableResultData_t1134_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &TrackableResultData_t1134_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_KeyValuePair_2_t3425_0_0_0_Types[] = { &Int32_t372_0_0_0, &TrackableResultData_t1134_0_0_0, &KeyValuePair_2_t3425_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_KeyValuePair_2_t3425_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_KeyValuePair_2_t3425_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3440_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3440_0_0_0_Types[] = { &KeyValuePair_2_t3440_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3440_0_0_0 = { 1, GenInst_KeyValuePair_2_t3440_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t1135_0_0_0_Types[] = { &VirtualButtonData_t1135_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t1135_0_0_0 = { 1, GenInst_VirtualButtonData_t1135_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_Int32_t372_0_0_0_Types[] = { &Int32_t372_0_0_0, &VirtualButtonData_t1135_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_Int32_t372_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_Int32_t372_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_VirtualButtonData_t1135_0_0_0_Types[] = { &Int32_t372_0_0_0, &VirtualButtonData_t1135_0_0_0, &VirtualButtonData_t1135_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_VirtualButtonData_t1135_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_VirtualButtonData_t1135_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &VirtualButtonData_t1135_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_KeyValuePair_2_t3440_0_0_0_Types[] = { &Int32_t372_0_0_0, &VirtualButtonData_t1135_0_0_0, &KeyValuePair_2_t3440_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_KeyValuePair_2_t3440_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_KeyValuePair_2_t3440_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_ImageTarget_t1225_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &ImageTarget_t1225_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_ImageTarget_t1225_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_ImageTarget_t1225_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3471_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3471_0_0_0_Types[] = { &KeyValuePair_2_t3471_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3471_0_0_0 = { 1, GenInst_KeyValuePair_2_t3471_0_0_0_Types };
extern const Il2CppType ProfileData_t1226_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t1226_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t1226_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t1226_0_0_0 = { 2, GenInst_String_t_0_0_0_ProfileData_t1226_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t1226_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0 = { 2, GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3478_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3478_0_0_0_Types[] = { &KeyValuePair_2_t3478_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3478_0_0_0 = { 1, GenInst_KeyValuePair_2_t3478_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1226_0_0_0_Types[] = { &ProfileData_t1226_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1226_0_0_0 = { 1, GenInst_ProfileData_t1226_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t1226_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_ProfileData_t1226_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t1226_0_0_0, &ProfileData_t1226_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_ProfileData_t1226_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_ProfileData_t1226_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t1226_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_KeyValuePair_2_t3478_0_0_0_Types[] = { &Object_t_0_0_0, &ProfileData_t1226_0_0_0, &KeyValuePair_2_t3478_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_KeyValuePair_2_t3478_0_0_0 = { 3, GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_KeyValuePair_2_t3478_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t1226_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t1226_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t1226_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_String_t_0_0_0_ProfileData_t1226_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3492_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3492_0_0_0_Types[] = { &KeyValuePair_2_t3492_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3492_0_0_0 = { 1, GenInst_KeyValuePair_2_t3492_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_VirtualButtonAbstractBehaviour_t319_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Int32_t372_0_0_0, &VirtualButtonAbstractBehaviour_t319_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_VirtualButtonAbstractBehaviour_t319_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Int32_t372_0_0_0_VirtualButtonAbstractBehaviour_t319_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3497_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3497_0_0_0_Types[] = { &KeyValuePair_2_t3497_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497_0_0_0 = { 1, GenInst_KeyValuePair_2_t3497_0_0_0_Types };
extern const Il2CppType Link_t3526_0_0_0;
static const Il2CppType* GenInst_Link_t3526_0_0_0_Types[] = { &Link_t3526_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t3526_0_0_0 = { 1, GenInst_Link_t3526_0_0_0_Types };
extern const Il2CppType KeySizes_t1423_0_0_0;
static const Il2CppType* GenInst_KeySizes_t1423_0_0_0_Types[] = { &KeySizes_t1423_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t1423_0_0_0 = { 1, GenInst_KeySizes_t1423_0_0_0_Types };
extern const Il2CppType UInt32_t389_0_0_0;
static const Il2CppType* GenInst_UInt32_t389_0_0_0_Types[] = { &UInt32_t389_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t389_0_0_0 = { 1, GenInst_UInt32_t389_0_0_0_Types };
extern const Il2CppType IComparable_1_t4938_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4938_0_0_0_Types[] = { &IComparable_1_t4938_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4938_0_0_0 = { 1, GenInst_IComparable_1_t4938_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4943_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4943_0_0_0_Types[] = { &IEquatable_1_t4943_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4943_0_0_0 = { 1, GenInst_IEquatable_1_t4943_0_0_0_Types };
extern const Il2CppType BigInteger_t1428_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1428_0_0_0_Types[] = { &BigInteger_t1428_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1428_0_0_0 = { 1, GenInst_BigInteger_t1428_0_0_0_Types };
extern const Il2CppType X509Certificate_t1531_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t1531_0_0_0_Types[] = { &X509Certificate_t1531_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t1531_0_0_0 = { 1, GenInst_X509Certificate_t1531_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t2457_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t2457_0_0_0_Types[] = { &IDeserializationCallback_t2457_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t2457_0_0_0 = { 1, GenInst_IDeserializationCallback_t2457_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t1535_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t1535_0_0_0_Types[] = { &ClientCertificateType_t1535_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1535_0_0_0 = { 1, GenInst_ClientCertificateType_t1535_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t384_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3541_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3541_0_0_0_Types[] = { &KeyValuePair_2_t3541_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3541_0_0_0 = { 1, GenInst_KeyValuePair_2_t3541_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t1662_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t1662_0_0_0_Types[] = { &X509ChainStatus_t1662_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t1662_0_0_0 = { 1, GenInst_X509ChainStatus_t1662_0_0_0_Types };
extern const Il2CppType Capture_t1681_0_0_0;
static const Il2CppType* GenInst_Capture_t1681_0_0_0_Types[] = { &Capture_t1681_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t1681_0_0_0 = { 1, GenInst_Capture_t1681_0_0_0_Types };
extern const Il2CppType Group_t1597_0_0_0;
static const Il2CppType* GenInst_Group_t1597_0_0_0_Types[] = { &Group_t1597_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t1597_0_0_0 = { 1, GenInst_Group_t1597_0_0_0_Types };
extern const Il2CppType Mark_t1704_0_0_0;
static const Il2CppType* GenInst_Mark_t1704_0_0_0_Types[] = { &Mark_t1704_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t1704_0_0_0 = { 1, GenInst_Mark_t1704_0_0_0_Types };
extern const Il2CppType UriScheme_t1740_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1740_0_0_0_Types[] = { &UriScheme_t1740_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1740_0_0_0 = { 1, GenInst_UriScheme_t1740_0_0_0_Types };
extern const Il2CppType Int64_t386_0_0_0;
static const Il2CppType* GenInst_Int64_t386_0_0_0_Types[] = { &Int64_t386_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t386_0_0_0 = { 1, GenInst_Int64_t386_0_0_0_Types };
extern const Il2CppType UInt64_t394_0_0_0;
static const Il2CppType* GenInst_UInt64_t394_0_0_0_Types[] = { &UInt64_t394_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t394_0_0_0 = { 1, GenInst_UInt64_t394_0_0_0_Types };
extern const Il2CppType SByte_t390_0_0_0;
static const Il2CppType* GenInst_SByte_t390_0_0_0_Types[] = { &SByte_t390_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t390_0_0_0 = { 1, GenInst_SByte_t390_0_0_0_Types };
extern const Il2CppType Int16_t392_0_0_0;
static const Il2CppType* GenInst_Int16_t392_0_0_0_Types[] = { &Int16_t392_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t392_0_0_0 = { 1, GenInst_Int16_t392_0_0_0_Types };
extern const Il2CppType Decimal_t395_0_0_0;
static const Il2CppType* GenInst_Decimal_t395_0_0_0_Types[] = { &Decimal_t395_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t395_0_0_0 = { 1, GenInst_Decimal_t395_0_0_0_Types };
extern const Il2CppType Delegate_t466_0_0_0;
static const Il2CppType* GenInst_Delegate_t466_0_0_0_Types[] = { &Delegate_t466_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t466_0_0_0 = { 1, GenInst_Delegate_t466_0_0_0_Types };
extern const Il2CppType IComparable_1_t4982_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4982_0_0_0_Types[] = { &IComparable_1_t4982_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4982_0_0_0 = { 1, GenInst_IComparable_1_t4982_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4983_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4983_0_0_0_Types[] = { &IEquatable_1_t4983_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4983_0_0_0 = { 1, GenInst_IEquatable_1_t4983_0_0_0_Types };
extern const Il2CppType IComparable_1_t4986_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4986_0_0_0_Types[] = { &IComparable_1_t4986_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4986_0_0_0 = { 1, GenInst_IComparable_1_t4986_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4987_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4987_0_0_0_Types[] = { &IEquatable_1_t4987_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4987_0_0_0 = { 1, GenInst_IEquatable_1_t4987_0_0_0_Types };
extern const Il2CppType IComparable_1_t4984_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4984_0_0_0_Types[] = { &IComparable_1_t4984_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4984_0_0_0 = { 1, GenInst_IComparable_1_t4984_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4985_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4985_0_0_0_Types[] = { &IEquatable_1_t4985_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4985_0_0_0 = { 1, GenInst_IEquatable_1_t4985_0_0_0_Types };
extern const Il2CppType IComparable_1_t4980_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4980_0_0_0_Types[] = { &IComparable_1_t4980_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4980_0_0_0 = { 1, GenInst_IComparable_1_t4980_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4981_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4981_0_0_0_Types[] = { &IEquatable_1_t4981_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4981_0_0_0 = { 1, GenInst_IEquatable_1_t4981_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t4054_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t4054_0_0_0_Types[] = { &_FieldInfo_t4054_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t4054_0_0_0 = { 1, GenInst__FieldInfo_t4054_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t1042_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t1042_0_0_0_Types[] = { &ConstructorInfo_t1042_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t1042_0_0_0 = { 1, GenInst_ConstructorInfo_t1042_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t4052_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t4052_0_0_0_Types[] = { &_ConstructorInfo_t4052_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t4052_0_0_0 = { 1, GenInst__ConstructorInfo_t4052_0_0_0_Types };
extern const Il2CppType MethodBase_t1033_0_0_0;
static const Il2CppType* GenInst_MethodBase_t1033_0_0_0_Types[] = { &MethodBase_t1033_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t1033_0_0_0 = { 1, GenInst_MethodBase_t1033_0_0_0_Types };
extern const Il2CppType _MethodBase_t4055_0_0_0;
static const Il2CppType* GenInst__MethodBase_t4055_0_0_0_Types[] = { &_MethodBase_t4055_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t4055_0_0_0 = { 1, GenInst__MethodBase_t4055_0_0_0_Types };
extern const Il2CppType TableRange_t1802_0_0_0;
static const Il2CppType* GenInst_TableRange_t1802_0_0_0_Types[] = { &TableRange_t1802_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t1802_0_0_0 = { 1, GenInst_TableRange_t1802_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1805_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1805_0_0_0_Types[] = { &TailoringInfo_t1805_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1805_0_0_0 = { 1, GenInst_TailoringInfo_t1805_0_0_0_Types };
extern const Il2CppType Contraction_t1806_0_0_0;
static const Il2CppType* GenInst_Contraction_t1806_0_0_0_Types[] = { &Contraction_t1806_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1806_0_0_0 = { 1, GenInst_Contraction_t1806_0_0_0_Types };
extern const Il2CppType Level2Map_t1808_0_0_0;
static const Il2CppType* GenInst_Level2Map_t1808_0_0_0_Types[] = { &Level2Map_t1808_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t1808_0_0_0 = { 1, GenInst_Level2Map_t1808_0_0_0_Types };
extern const Il2CppType BigInteger_t1829_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1829_0_0_0_Types[] = { &BigInteger_t1829_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1829_0_0_0 = { 1, GenInst_BigInteger_t1829_0_0_0_Types };
extern const Il2CppType Slot_t1879_0_0_0;
static const Il2CppType* GenInst_Slot_t1879_0_0_0_Types[] = { &Slot_t1879_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1879_0_0_0 = { 1, GenInst_Slot_t1879_0_0_0_Types };
extern const Il2CppType Slot_t1888_0_0_0;
static const Il2CppType* GenInst_Slot_t1888_0_0_0_Types[] = { &Slot_t1888_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1888_0_0_0 = { 1, GenInst_Slot_t1888_0_0_0_Types };
extern const Il2CppType StackFrame_t1032_0_0_0;
static const Il2CppType* GenInst_StackFrame_t1032_0_0_0_Types[] = { &StackFrame_t1032_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t1032_0_0_0 = { 1, GenInst_StackFrame_t1032_0_0_0_Types };
extern const Il2CppType Calendar_t1902_0_0_0;
static const Il2CppType* GenInst_Calendar_t1902_0_0_0_Types[] = { &Calendar_t1902_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t1902_0_0_0 = { 1, GenInst_Calendar_t1902_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t1974_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t1974_0_0_0_Types[] = { &ModuleBuilder_t1974_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1974_0_0_0 = { 1, GenInst_ModuleBuilder_t1974_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t4047_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t4047_0_0_0_Types[] = { &_ModuleBuilder_t4047_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t4047_0_0_0 = { 1, GenInst__ModuleBuilder_t4047_0_0_0_Types };
extern const Il2CppType Module_t1970_0_0_0;
static const Il2CppType* GenInst_Module_t1970_0_0_0_Types[] = { &Module_t1970_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t1970_0_0_0 = { 1, GenInst_Module_t1970_0_0_0_Types };
extern const Il2CppType _Module_t4057_0_0_0;
static const Il2CppType* GenInst__Module_t4057_0_0_0_Types[] = { &_Module_t4057_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t4057_0_0_0 = { 1, GenInst__Module_t4057_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t1980_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t1980_0_0_0_Types[] = { &ParameterBuilder_t1980_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1980_0_0_0 = { 1, GenInst_ParameterBuilder_t1980_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t4048_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t4048_0_0_0_Types[] = { &_ParameterBuilder_t4048_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t4048_0_0_0 = { 1, GenInst__ParameterBuilder_t4048_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1005_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1005_0_0_0_Types[] = { &TypeU5BU5D_t1005_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1005_0_0_0 = { 1, GenInst_TypeU5BU5D_t1005_0_0_0_Types };
extern const Il2CppType Array_t_0_0_0;
static const Il2CppType* GenInst_Array_t_0_0_0_Types[] = { &Array_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_t_0_0_0 = { 1, GenInst_Array_t_0_0_0_Types };
extern const Il2CppType ICollection_t1751_0_0_0;
static const Il2CppType* GenInst_ICollection_t1751_0_0_0_Types[] = { &ICollection_t1751_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t1751_0_0_0 = { 1, GenInst_ICollection_t1751_0_0_0_Types };
extern const Il2CppType IList_t346_0_0_0;
static const Il2CppType* GenInst_IList_t346_0_0_0_Types[] = { &IList_t346_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t346_0_0_0 = { 1, GenInst_IList_t346_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t1964_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t1964_0_0_0_Types[] = { &ILTokenInfo_t1964_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1964_0_0_0 = { 1, GenInst_ILTokenInfo_t1964_0_0_0_Types };
extern const Il2CppType LabelData_t1966_0_0_0;
static const Il2CppType* GenInst_LabelData_t1966_0_0_0_Types[] = { &LabelData_t1966_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t1966_0_0_0 = { 1, GenInst_LabelData_t1966_0_0_0_Types };
extern const Il2CppType LabelFixup_t1965_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t1965_0_0_0_Types[] = { &LabelFixup_t1965_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t1965_0_0_0 = { 1, GenInst_LabelFixup_t1965_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1962_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1962_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1962_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1962_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1962_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1956_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1956_0_0_0_Types[] = { &TypeBuilder_t1956_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1956_0_0_0 = { 1, GenInst_TypeBuilder_t1956_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t4049_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t4049_0_0_0_Types[] = { &_TypeBuilder_t4049_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t4049_0_0_0 = { 1, GenInst__TypeBuilder_t4049_0_0_0_Types };
extern const Il2CppType MethodBuilder_t1963_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t1963_0_0_0_Types[] = { &MethodBuilder_t1963_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t1963_0_0_0 = { 1, GenInst_MethodBuilder_t1963_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t4046_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t4046_0_0_0_Types[] = { &_MethodBuilder_t4046_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t4046_0_0_0 = { 1, GenInst__MethodBuilder_t4046_0_0_0_Types };
extern const Il2CppType _MethodInfo_t4056_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t4056_0_0_0_Types[] = { &_MethodInfo_t4056_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t4056_0_0_0 = { 1, GenInst__MethodInfo_t4056_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t1954_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t1954_0_0_0_Types[] = { &ConstructorBuilder_t1954_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1954_0_0_0 = { 1, GenInst_ConstructorBuilder_t1954_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t4042_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t4042_0_0_0_Types[] = { &_ConstructorBuilder_t4042_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t4042_0_0_0 = { 1, GenInst__ConstructorBuilder_t4042_0_0_0_Types };
extern const Il2CppType FieldBuilder_t1960_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t1960_0_0_0_Types[] = { &FieldBuilder_t1960_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1960_0_0_0 = { 1, GenInst_FieldBuilder_t1960_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t4044_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t4044_0_0_0_Types[] = { &_FieldBuilder_t4044_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t4044_0_0_0 = { 1, GenInst__FieldBuilder_t4044_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t4059_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t4059_0_0_0_Types[] = { &_PropertyInfo_t4059_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t4059_0_0_0 = { 1, GenInst__PropertyInfo_t4059_0_0_0_Types };
extern const Il2CppType IContextProperty_t2422_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t2422_0_0_0_Types[] = { &IContextProperty_t2422_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t2422_0_0_0 = { 1, GenInst_IContextProperty_t2422_0_0_0_Types };
extern const Il2CppType Header_t2107_0_0_0;
static const Il2CppType* GenInst_Header_t2107_0_0_0_Types[] = { &Header_t2107_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t2107_0_0_0 = { 1, GenInst_Header_t2107_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2449_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2449_0_0_0_Types[] = { &ITrackingHandler_t2449_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2449_0_0_0 = { 1, GenInst_ITrackingHandler_t2449_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2436_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2436_0_0_0_Types[] = { &IContextAttribute_t2436_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2436_0_0_0 = { 1, GenInst_IContextAttribute_t2436_0_0_0_Types };
extern const Il2CppType IComparable_1_t5212_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t5212_0_0_0_Types[] = { &IComparable_1_t5212_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t5212_0_0_0 = { 1, GenInst_IComparable_1_t5212_0_0_0_Types };
extern const Il2CppType IEquatable_1_t5217_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t5217_0_0_0_Types[] = { &IEquatable_1_t5217_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t5217_0_0_0 = { 1, GenInst_IEquatable_1_t5217_0_0_0_Types };
extern const Il2CppType IComparable_1_t4988_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4988_0_0_0_Types[] = { &IComparable_1_t4988_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4988_0_0_0 = { 1, GenInst_IComparable_1_t4988_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4989_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4989_0_0_0_Types[] = { &IEquatable_1_t4989_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4989_0_0_0 = { 1, GenInst_IEquatable_1_t4989_0_0_0_Types };
extern const Il2CppType IComparable_1_t5236_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t5236_0_0_0_Types[] = { &IComparable_1_t5236_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t5236_0_0_0 = { 1, GenInst_IComparable_1_t5236_0_0_0_Types };
extern const Il2CppType IEquatable_1_t5241_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t5241_0_0_0_Types[] = { &IEquatable_1_t5241_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t5241_0_0_0 = { 1, GenInst_IEquatable_1_t5241_0_0_0_Types };
extern const Il2CppType TypeTag_t2163_0_0_0;
static const Il2CppType* GenInst_TypeTag_t2163_0_0_0_Types[] = { &TypeTag_t2163_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t2163_0_0_0 = { 1, GenInst_TypeTag_t2163_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType Version_t1633_0_0_0;
static const Il2CppType* GenInst_Version_t1633_0_0_0_Types[] = { &Version_t1633_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1633_0_0_0 = { 1, GenInst_Version_t1633_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t451_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &DictionaryEntry_t451_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t451_0_0_0_DictionaryEntry_t451_0_0_0 = { 2, GenInst_DictionaryEntry_t451_0_0_0_DictionaryEntry_t451_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2507_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2507_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2507_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2507_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2507_0_0_0_KeyValuePair_2_t2507_0_0_0_Types[] = { &KeyValuePair_2_t2507_0_0_0, &KeyValuePair_2_t2507_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2507_0_0_0_KeyValuePair_2_t2507_0_0_0 = { 2, GenInst_KeyValuePair_2_t2507_0_0_0_KeyValuePair_2_t2507_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t372_0_0_0_Int32_t372_0_0_0_Types[] = { &Int32_t372_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t372_0_0_0_Int32_t372_0_0_0 = { 2, GenInst_Int32_t372_0_0_0_Int32_t372_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2546_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2546_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2546_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2546_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2546_0_0_0_KeyValuePair_2_t2546_0_0_0_Types[] = { &KeyValuePair_2_t2546_0_0_0, &KeyValuePair_2_t2546_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2546_0_0_0_KeyValuePair_2_t2546_0_0_0 = { 2, GenInst_KeyValuePair_2_t2546_0_0_0_KeyValuePair_2_t2546_0_0_0_Types };
static const Il2CppType* GenInst_EScreens_t188_0_0_0_EScreens_t188_0_0_0_Types[] = { &EScreens_t188_0_0_0, &EScreens_t188_0_0_0 };
extern const Il2CppGenericInst GenInst_EScreens_t188_0_0_0_EScreens_t188_0_0_0 = { 2, GenInst_EScreens_t188_0_0_0_EScreens_t188_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2667_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2667_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2667_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2667_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2667_0_0_0_KeyValuePair_2_t2667_0_0_0_Types[] = { &KeyValuePair_2_t2667_0_0_0, &KeyValuePair_2_t2667_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2667_0_0_0_KeyValuePair_2_t2667_0_0_0 = { 2, GenInst_KeyValuePair_2_t2667_0_0_0_KeyValuePair_2_t2667_0_0_0_Types };
static const Il2CppType* GenInst_ESubScreens_t189_0_0_0_ESubScreens_t189_0_0_0_Types[] = { &ESubScreens_t189_0_0_0, &ESubScreens_t189_0_0_0 };
extern const Il2CppGenericInst GenInst_ESubScreens_t189_0_0_0_ESubScreens_t189_0_0_0 = { 2, GenInst_ESubScreens_t189_0_0_0_ESubScreens_t189_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2690_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2690_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2690_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2690_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2690_0_0_0_KeyValuePair_2_t2690_0_0_0_Types[] = { &KeyValuePair_2_t2690_0_0_0, &KeyValuePair_2_t2690_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2690_0_0_0_KeyValuePair_2_t2690_0_0_0 = { 2, GenInst_KeyValuePair_2_t2690_0_0_0_KeyValuePair_2_t2690_0_0_0_Types };
static const Il2CppType* GenInst_ERaffleResult_t207_0_0_0_Object_t_0_0_0_Types[] = { &ERaffleResult_t207_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ERaffleResult_t207_0_0_0_Object_t_0_0_0 = { 2, GenInst_ERaffleResult_t207_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_ERaffleResult_t207_0_0_0_ERaffleResult_t207_0_0_0_Types[] = { &ERaffleResult_t207_0_0_0, &ERaffleResult_t207_0_0_0 };
extern const Il2CppGenericInst GenInst_ERaffleResult_t207_0_0_0_ERaffleResult_t207_0_0_0 = { 2, GenInst_ERaffleResult_t207_0_0_0_ERaffleResult_t207_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t352_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t352_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t352_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t352_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t352_0_0_0_KeyValuePair_2_t352_0_0_0_Types[] = { &KeyValuePair_2_t352_0_0_0, &KeyValuePair_2_t352_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t352_0_0_0_KeyValuePair_2_t352_0_0_0 = { 2, GenInst_KeyValuePair_2_t352_0_0_0_KeyValuePair_2_t352_0_0_0_Types };
static const Il2CppType* GenInst_FocusMode_t438_0_0_0_FocusMode_t438_0_0_0_Types[] = { &FocusMode_t438_0_0_0, &FocusMode_t438_0_0_0 };
extern const Il2CppGenericInst GenInst_FocusMode_t438_0_0_0_FocusMode_t438_0_0_0 = { 2, GenInst_FocusMode_t438_0_0_0_FocusMode_t438_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t384_0_0_0_Object_t_0_0_0_Types[] = { &Boolean_t384_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t384_0_0_0_Object_t_0_0_0 = { 2, GenInst_Boolean_t384_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t384_0_0_0_Boolean_t384_0_0_0_Types[] = { &Boolean_t384_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t384_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_Boolean_t384_0_0_0_Boolean_t384_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2740_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2740_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2740_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2740_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2740_0_0_0_KeyValuePair_2_t2740_0_0_0_Types[] = { &KeyValuePair_2_t2740_0_0_0, &KeyValuePair_2_t2740_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2740_0_0_0_KeyValuePair_2_t2740_0_0_0 = { 2, GenInst_KeyValuePair_2_t2740_0_0_0_KeyValuePair_2_t2740_0_0_0_Types };
static const Il2CppType* GenInst_AnimatorFrame_t235_0_0_0_AnimatorFrame_t235_0_0_0_Types[] = { &AnimatorFrame_t235_0_0_0, &AnimatorFrame_t235_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorFrame_t235_0_0_0_AnimatorFrame_t235_0_0_0 = { 2, GenInst_AnimatorFrame_t235_0_0_0_AnimatorFrame_t235_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t512_0_0_0_RaycastResult_t512_0_0_0_Types[] = { &RaycastResult_t512_0_0_0, &RaycastResult_t512_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t512_0_0_0_RaycastResult_t512_0_0_0 = { 2, GenInst_RaycastResult_t512_0_0_0_RaycastResult_t512_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2871_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2871_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2871_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2871_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2871_0_0_0_KeyValuePair_2_t2871_0_0_0_Types[] = { &KeyValuePair_2_t2871_0_0_0, &KeyValuePair_2_t2871_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2871_0_0_0_KeyValuePair_2_t2871_0_0_0 = { 2, GenInst_KeyValuePair_2_t2871_0_0_0_KeyValuePair_2_t2871_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t605_0_0_0_UIVertex_t605_0_0_0_Types[] = { &UIVertex_t605_0_0_0, &UIVertex_t605_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t605_0_0_0_UIVertex_t605_0_0_0 = { 2, GenInst_UIVertex_t605_0_0_0_UIVertex_t605_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t36_0_0_0_Vector3_t36_0_0_0_Types[] = { &Vector3_t36_0_0_0, &Vector3_t36_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t36_0_0_0_Vector3_t36_0_0_0 = { 2, GenInst_Vector3_t36_0_0_0_Vector3_t36_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t711_0_0_0_Color32_t711_0_0_0_Types[] = { &Color32_t711_0_0_0, &Color32_t711_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t711_0_0_0_Color32_t711_0_0_0 = { 2, GenInst_Color32_t711_0_0_0_Color32_t711_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2_0_0_0_Vector2_t2_0_0_0_Types[] = { &Vector2_t2_0_0_0, &Vector2_t2_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2_0_0_0_Vector2_t2_0_0_0 = { 2, GenInst_Vector2_t2_0_0_0_Vector2_t2_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t680_0_0_0_Vector4_t680_0_0_0_Types[] = { &Vector4_t680_0_0_0, &Vector4_t680_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t680_0_0_0_Vector4_t680_0_0_0 = { 2, GenInst_Vector4_t680_0_0_0_Vector4_t680_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t754_0_0_0_UICharInfo_t754_0_0_0_Types[] = { &UICharInfo_t754_0_0_0, &UICharInfo_t754_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t754_0_0_0_UICharInfo_t754_0_0_0 = { 2, GenInst_UICharInfo_t754_0_0_0_UICharInfo_t754_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t752_0_0_0_UILineInfo_t752_0_0_0_Types[] = { &UILineInfo_t752_0_0_0, &UILineInfo_t752_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t752_0_0_0_UILineInfo_t752_0_0_0 = { 2, GenInst_UILineInfo_t752_0_0_0_UILineInfo_t752_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t976_0_0_0_Object_t_0_0_0_Types[] = { &TextEditOp_t976_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t976_0_0_0_Object_t_0_0_0 = { 2, GenInst_TextEditOp_t976_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t976_0_0_0_TextEditOp_t976_0_0_0_Types[] = { &TextEditOp_t976_0_0_0, &TextEditOp_t976_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t976_0_0_0_TextEditOp_t976_0_0_0 = { 2, GenInst_TextEditOp_t976_0_0_0_TextEditOp_t976_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3183_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3183_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3183_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3183_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3183_0_0_0_KeyValuePair_2_t3183_0_0_0_Types[] = { &KeyValuePair_2_t3183_0_0_0, &KeyValuePair_2_t3183_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3183_0_0_0_KeyValuePair_2_t3183_0_0_0 = { 2, GenInst_KeyValuePair_2_t3183_0_0_0_KeyValuePair_2_t3183_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t1108_0_0_0_PIXEL_FORMAT_t1108_0_0_0_Types[] = { &PIXEL_FORMAT_t1108_0_0_0, &PIXEL_FORMAT_t1108_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t1108_0_0_0_PIXEL_FORMAT_t1108_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t1108_0_0_0_PIXEL_FORMAT_t1108_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3257_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3257_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3257_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3257_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3257_0_0_0_KeyValuePair_2_t3257_0_0_0_Types[] = { &KeyValuePair_2_t3257_0_0_0, &KeyValuePair_2_t3257_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3257_0_0_0_KeyValuePair_2_t3257_0_0_0 = { 2, GenInst_KeyValuePair_2_t3257_0_0_0_KeyValuePair_2_t3257_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t393_0_0_0_Object_t_0_0_0_Types[] = { &UInt16_t393_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t393_0_0_0_Object_t_0_0_0 = { 2, GenInst_UInt16_t393_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t393_0_0_0_UInt16_t393_0_0_0_Types[] = { &UInt16_t393_0_0_0, &UInt16_t393_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t393_0_0_0_UInt16_t393_0_0_0 = { 2, GenInst_UInt16_t393_0_0_0_UInt16_t393_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3337_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3337_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3337_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3337_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3337_0_0_0_KeyValuePair_2_t3337_0_0_0_Types[] = { &KeyValuePair_2_t3337_0_0_0, &KeyValuePair_2_t3337_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3337_0_0_0_KeyValuePair_2_t3337_0_0_0 = { 2, GenInst_KeyValuePair_2_t3337_0_0_0_KeyValuePair_2_t3337_0_0_0_Types };
static const Il2CppType* GenInst_TrackableResultData_t1134_0_0_0_Object_t_0_0_0_Types[] = { &TrackableResultData_t1134_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t1134_0_0_0_Object_t_0_0_0 = { 2, GenInst_TrackableResultData_t1134_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TrackableResultData_t1134_0_0_0_TrackableResultData_t1134_0_0_0_Types[] = { &TrackableResultData_t1134_0_0_0, &TrackableResultData_t1134_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t1134_0_0_0_TrackableResultData_t1134_0_0_0 = { 2, GenInst_TrackableResultData_t1134_0_0_0_TrackableResultData_t1134_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3425_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3425_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3425_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3425_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3425_0_0_0_KeyValuePair_2_t3425_0_0_0_Types[] = { &KeyValuePair_2_t3425_0_0_0, &KeyValuePair_2_t3425_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3425_0_0_0_KeyValuePair_2_t3425_0_0_0 = { 2, GenInst_KeyValuePair_2_t3425_0_0_0_KeyValuePair_2_t3425_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t1135_0_0_0_Object_t_0_0_0_Types[] = { &VirtualButtonData_t1135_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t1135_0_0_0_Object_t_0_0_0 = { 2, GenInst_VirtualButtonData_t1135_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t1135_0_0_0_VirtualButtonData_t1135_0_0_0_Types[] = { &VirtualButtonData_t1135_0_0_0, &VirtualButtonData_t1135_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t1135_0_0_0_VirtualButtonData_t1135_0_0_0 = { 2, GenInst_VirtualButtonData_t1135_0_0_0_VirtualButtonData_t1135_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3440_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3440_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3440_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3440_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3440_0_0_0_KeyValuePair_2_t3440_0_0_0_Types[] = { &KeyValuePair_2_t3440_0_0_0, &KeyValuePair_2_t3440_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3440_0_0_0_KeyValuePair_2_t3440_0_0_0 = { 2, GenInst_KeyValuePair_2_t3440_0_0_0_KeyValuePair_2_t3440_0_0_0_Types };
static const Il2CppType* GenInst_TargetSearchResult_t1212_0_0_0_TargetSearchResult_t1212_0_0_0_Types[] = { &TargetSearchResult_t1212_0_0_0, &TargetSearchResult_t1212_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t1212_0_0_0_TargetSearchResult_t1212_0_0_0 = { 2, GenInst_TargetSearchResult_t1212_0_0_0_TargetSearchResult_t1212_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1226_0_0_0_Object_t_0_0_0_Types[] = { &ProfileData_t1226_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1226_0_0_0_Object_t_0_0_0 = { 2, GenInst_ProfileData_t1226_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1226_0_0_0_ProfileData_t1226_0_0_0_Types[] = { &ProfileData_t1226_0_0_0, &ProfileData_t1226_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1226_0_0_0_ProfileData_t1226_0_0_0 = { 2, GenInst_ProfileData_t1226_0_0_0_ProfileData_t1226_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3478_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t3478_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3478_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t3478_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3478_0_0_0_KeyValuePair_2_t3478_0_0_0_Types[] = { &KeyValuePair_2_t3478_0_0_0, &KeyValuePair_2_t3478_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3478_0_0_0_KeyValuePair_2_t3478_0_0_0 = { 2, GenInst_KeyValuePair_2_t3478_0_0_0_KeyValuePair_2_t3478_0_0_0_Types };
extern const Il2CppType CommonUtils_FindComponentThroughParent_m31364_gp_0_0_0_0;
static const Il2CppType* GenInst_CommonUtils_FindComponentThroughParent_m31364_gp_0_0_0_0_Types[] = { &CommonUtils_FindComponentThroughParent_m31364_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonUtils_FindComponentThroughParent_m31364_gp_0_0_0_0 = { 1, GenInst_CommonUtils_FindComponentThroughParent_m31364_gp_0_0_0_0_Types };
extern const Il2CppType CommonUtils_GetRequiredComponent_m31365_gp_0_0_0_0;
static const Il2CppType* GenInst_CommonUtils_GetRequiredComponent_m31365_gp_0_0_0_0_Types[] = { &CommonUtils_GetRequiredComponent_m31365_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonUtils_GetRequiredComponent_m31365_gp_0_0_0_0 = { 1, GenInst_CommonUtils_GetRequiredComponent_m31365_gp_0_0_0_0_Types };
extern const Il2CppType CommonUtils_GetComponent_m31366_gp_0_0_0_0;
static const Il2CppType* GenInst_CommonUtils_GetComponent_m31366_gp_0_0_0_0_Types[] = { &CommonUtils_GetComponent_m31366_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonUtils_GetComponent_m31366_gp_0_0_0_0 = { 1, GenInst_CommonUtils_GetComponent_m31366_gp_0_0_0_0_Types };
extern const Il2CppType QuerySystem_Query_m31401_gp_0_0_0_0;
static const Il2CppType* GenInst_QuerySystem_Query_m31401_gp_0_0_0_0_Types[] = { &QuerySystem_Query_m31401_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuerySystem_Query_m31401_gp_0_0_0_0 = { 1, GenInst_QuerySystem_Query_m31401_gp_0_0_0_0_Types };
extern const Il2CppType QuerySystem_Complete_m31402_gp_0_0_0_0;
static const Il2CppType* GenInst_QuerySystem_Complete_m31402_gp_0_0_0_0_Types[] = { &QuerySystem_Complete_m31402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuerySystem_Complete_m31402_gp_0_0_0_0 = { 1, GenInst_QuerySystem_Complete_m31402_gp_0_0_0_0_Types };
extern const Il2CppType QuerySystemImplementation_Query_m31403_gp_0_0_0_0;
static const Il2CppType* GenInst_QuerySystemImplementation_Query_m31403_gp_0_0_0_0_Types[] = { &QuerySystemImplementation_Query_m31403_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuerySystemImplementation_Query_m31403_gp_0_0_0_0 = { 1, GenInst_QuerySystemImplementation_Query_m31403_gp_0_0_0_0_Types };
extern const Il2CppType QuerySystemImplementation_Complete_m31404_gp_0_0_0_0;
static const Il2CppType* GenInst_QuerySystemImplementation_Complete_m31404_gp_0_0_0_0_Types[] = { &QuerySystemImplementation_Complete_m31404_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuerySystemImplementation_Complete_m31404_gp_0_0_0_0 = { 1, GenInst_QuerySystemImplementation_Complete_m31404_gp_0_0_0_0_Types };
extern const Il2CppType GameObjectExtensions_GetRequiredComponent_m31409_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObjectExtensions_GetRequiredComponent_m31409_gp_0_0_0_0_Types[] = { &GameObjectExtensions_GetRequiredComponent_m31409_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObjectExtensions_GetRequiredComponent_m31409_gp_0_0_0_0 = { 1, GenInst_GameObjectExtensions_GetRequiredComponent_m31409_gp_0_0_0_0_Types };
extern const Il2CppType UnityUtils_GetRequiredComponent_m31410_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityUtils_GetRequiredComponent_m31410_gp_0_0_0_0_Types[] = { &UnityUtils_GetRequiredComponent_m31410_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityUtils_GetRequiredComponent_m31410_gp_0_0_0_0 = { 1, GenInst_UnityUtils_GetRequiredComponent_m31410_gp_0_0_0_0_Types };
extern const Il2CppType Factory_Get_m31413_gp_0_0_0_0;
static const Il2CppType* GenInst_Factory_Get_m31413_gp_0_0_0_0_Types[] = { &Factory_Get_m31413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Factory_Get_m31413_gp_0_0_0_0 = { 1, GenInst_Factory_Get_m31413_gp_0_0_0_0_Types };
extern const Il2CppType ImmutableList_1_t3945_gp_0_0_0_0;
static const Il2CppType* GenInst_ImmutableList_1_t3945_gp_0_0_0_0_Types[] = { &ImmutableList_1_t3945_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableList_1_t3945_gp_0_0_0_0 = { 1, GenInst_ImmutableList_1_t3945_gp_0_0_0_0_Types };
extern const Il2CppType Pool_1_t3946_gp_0_0_0_0;
static const Il2CppType* GenInst_Pool_1_t3946_gp_0_0_0_0_Types[] = { &Pool_1_t3946_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pool_1_t3946_gp_0_0_0_0 = { 1, GenInst_Pool_1_t3946_gp_0_0_0_0_Types };
extern const Il2CppType SimpleList_1_t3948_gp_0_0_0_0;
static const Il2CppType* GenInst_SimpleList_1_t3948_gp_0_0_0_0_Types[] = { &SimpleList_1_t3948_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SimpleList_1_t3948_gp_0_0_0_0 = { 1, GenInst_SimpleList_1_t3948_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator4_t3950_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator4_t3950_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator4_t3950_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator4_t3950_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator4_t3950_gp_0_0_0_0_Types };
extern const Il2CppType DispatcherBase_Dispatch_m31458_gp_0_0_0_0;
static const Il2CppType* GenInst_DispatcherBase_Dispatch_m31458_gp_0_0_0_0_Types[] = { &DispatcherBase_Dispatch_m31458_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherBase_Dispatch_m31458_gp_0_0_0_0 = { 1, GenInst_DispatcherBase_Dispatch_m31458_gp_0_0_0_0_Types };
extern const Il2CppType Dispatcher_CreateSafeFunction_m31460_gp_0_0_0_0;
static const Il2CppType* GenInst_Dispatcher_CreateSafeFunction_m31460_gp_0_0_0_0_Types[] = { &Dispatcher_CreateSafeFunction_m31460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dispatcher_CreateSafeFunction_m31460_gp_0_0_0_0 = { 1, GenInst_Dispatcher_CreateSafeFunction_m31460_gp_0_0_0_0_Types };
extern const Il2CppType Dispatcher_CreateSafeAction_m31461_gp_0_0_0_0;
static const Il2CppType* GenInst_Dispatcher_CreateSafeAction_m31461_gp_0_0_0_0_Types[] = { &Dispatcher_CreateSafeAction_m31461_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dispatcher_CreateSafeAction_m31461_gp_0_0_0_0 = { 1, GenInst_Dispatcher_CreateSafeAction_m31461_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t3951_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t3951_gp_0_0_0_0_Types[] = { &U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t3951_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t3951_gp_0_0_0_0 = { 1, GenInst_U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t3951_gp_0_0_0_0_Types };
extern const Il2CppType Task_1_t3953_gp_0_0_0_0;
static const Il2CppType* GenInst_Task_1_t3953_gp_0_0_0_0_Types[] = { &Task_1_t3953_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t3953_gp_0_0_0_0 = { 1, GenInst_Task_1_t3953_gp_0_0_0_0_Types };
extern const Il2CppType Task_1_WaitForSeconds_m31473_gp_0_0_0_0;
static const Il2CppType* GenInst_Task_1_WaitForSeconds_m31473_gp_0_0_0_0_Types[] = { &Task_1_WaitForSeconds_m31473_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_WaitForSeconds_m31473_gp_0_0_0_0 = { 1, GenInst_Task_1_WaitForSeconds_m31473_gp_0_0_0_0_Types };
extern const Il2CppType ThreadBase_Dispatch_m31476_gp_0_0_0_0;
static const Il2CppType* GenInst_ThreadBase_Dispatch_m31476_gp_0_0_0_0_Types[] = { &ThreadBase_Dispatch_m31476_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadBase_Dispatch_m31476_gp_0_0_0_0 = { 1, GenInst_ThreadBase_Dispatch_m31476_gp_0_0_0_0_Types };
extern const Il2CppType ThreadBase_DispatchAndWait_m31477_gp_0_0_0_0;
static const Il2CppType* GenInst_ThreadBase_DispatchAndWait_m31477_gp_0_0_0_0_Types[] = { &ThreadBase_DispatchAndWait_m31477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadBase_DispatchAndWait_m31477_gp_0_0_0_0 = { 1, GenInst_ThreadBase_DispatchAndWait_m31477_gp_0_0_0_0_Types };
extern const Il2CppType ThreadBase_DispatchAndWait_m31478_gp_0_0_0_0;
static const Il2CppType* GenInst_ThreadBase_DispatchAndWait_m31478_gp_0_0_0_0_Types[] = { &ThreadBase_DispatchAndWait_m31478_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadBase_DispatchAndWait_m31478_gp_0_0_0_0 = { 1, GenInst_ThreadBase_DispatchAndWait_m31478_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m31506_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m31506_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m31506_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m31506_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m31506_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m31507_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m31507_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m31507_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m31507_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m31507_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m31509_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m31509_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m31509_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m31509_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m31509_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m31510_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m31510_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m31510_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m31510_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m31510_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m31511_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m31511_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m31511_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m31511_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m31511_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t3956_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t3956_gp_0_0_0_0_Types[] = { &TweenRunner_1_t3956_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t3956_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t3956_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m31536_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m31536_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m31536_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m31536_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m31536_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t3960_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t3960_gp_0_0_0_0_Types[] = { &IndexedSet_1_t3960_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t3960_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t3960_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t3960_gp_0_0_0_0_Int32_t372_0_0_0_Types[] = { &IndexedSet_1_t3960_gp_0_0_0_0, &Int32_t372_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t3960_gp_0_0_0_0_Int32_t372_0_0_0 = { 2, GenInst_IndexedSet_1_t3960_gp_0_0_0_0_Int32_t372_0_0_0_Types };
extern const Il2CppType ListPool_1_t3961_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t3961_gp_0_0_0_0_Types[] = { &ListPool_1_t3961_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t3961_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t3961_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t5307_0_0_0;
static const Il2CppType* GenInst_List_1_t5307_0_0_0_Types[] = { &List_1_t5307_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t5307_0_0_0 = { 1, GenInst_List_1_t5307_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t3962_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t3962_gp_0_0_0_0_Types[] = { &ObjectPool_1_t3962_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t3962_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t3962_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m31602_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m31602_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m31602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m31602_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m31602_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m31606_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m31606_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m31606_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m31606_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m31606_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m31607_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m31607_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m31607_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m31607_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m31607_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m31608_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m31608_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m31608_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m31608_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m31608_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m31609_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m31609_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m31609_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m31609_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m31609_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m31611_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m31611_gp_0_0_0_0_Types[] = { &Component_GetComponents_m31611_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m31611_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m31611_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m31612_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m31612_gp_0_0_0_0_Types[] = { &Component_GetComponents_m31612_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m31612_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m31612_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m31616_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m31616_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m31616_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m31616_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m31616_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m31618_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m31618_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m31618_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m31618_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m31618_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m31619_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m31619_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m31619_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m31619_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m31619_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m31620_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m31620_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m31620_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m31620_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m31620_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t3970_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t3970_gp_0_0_0_0_Types[] = { &InvokableCall_1_t3970_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t3970_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t3970_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t3971_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t3971_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t3971_gp_0_0_0_0_InvokableCall_2_t3971_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3971_gp_0_0_0_0, &InvokableCall_2_t3971_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3971_gp_0_0_0_0_InvokableCall_2_t3971_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t3971_gp_0_0_0_0_InvokableCall_2_t3971_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3971_gp_0_0_0_0_Types[] = { &InvokableCall_2_t3971_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3971_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t3971_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3971_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3971_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3971_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t3971_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3972_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3972_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3972_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3972_gp_0_0_0_0_InvokableCall_3_t3972_gp_1_0_0_0_InvokableCall_3_t3972_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3972_gp_0_0_0_0, &InvokableCall_3_t3972_gp_1_0_0_0, &InvokableCall_3_t3972_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3972_gp_0_0_0_0_InvokableCall_3_t3972_gp_1_0_0_0_InvokableCall_3_t3972_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3972_gp_0_0_0_0_InvokableCall_3_t3972_gp_1_0_0_0_InvokableCall_3_t3972_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3972_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3972_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3972_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3972_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3972_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3972_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3972_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3972_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3972_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3972_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3972_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3972_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t3973_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3973_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3973_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3973_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t3973_gp_0_0_0_0_InvokableCall_4_t3973_gp_1_0_0_0_InvokableCall_4_t3973_gp_2_0_0_0_InvokableCall_4_t3973_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3973_gp_0_0_0_0, &InvokableCall_4_t3973_gp_1_0_0_0, &InvokableCall_4_t3973_gp_2_0_0_0, &InvokableCall_4_t3973_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3973_gp_0_0_0_0_InvokableCall_4_t3973_gp_1_0_0_0_InvokableCall_4_t3973_gp_2_0_0_0_InvokableCall_4_t3973_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t3973_gp_0_0_0_0_InvokableCall_4_t3973_gp_1_0_0_0_InvokableCall_4_t3973_gp_2_0_0_0_InvokableCall_4_t3973_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3973_gp_0_0_0_0_Types[] = { &InvokableCall_4_t3973_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3973_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t3973_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3973_gp_1_0_0_0_Types[] = { &InvokableCall_4_t3973_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3973_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t3973_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3973_gp_2_0_0_0_Types[] = { &InvokableCall_4_t3973_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3973_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t3973_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3973_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3973_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3973_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t3973_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t1043_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t1043_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t1043_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t1043_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t1043_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t3974_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t3974_gp_0_0_0_0_Types[] = { &UnityEvent_1_t3974_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t3974_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t3974_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t3975_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t3975_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t3975_gp_0_0_0_0_UnityEvent_2_t3975_gp_1_0_0_0_Types[] = { &UnityEvent_2_t3975_gp_0_0_0_0, &UnityEvent_2_t3975_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t3975_gp_0_0_0_0_UnityEvent_2_t3975_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t3975_gp_0_0_0_0_UnityEvent_2_t3975_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t3976_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t3976_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t3976_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t3976_gp_0_0_0_0_UnityEvent_3_t3976_gp_1_0_0_0_UnityEvent_3_t3976_gp_2_0_0_0_Types[] = { &UnityEvent_3_t3976_gp_0_0_0_0, &UnityEvent_3_t3976_gp_1_0_0_0, &UnityEvent_3_t3976_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t3976_gp_0_0_0_0_UnityEvent_3_t3976_gp_1_0_0_0_UnityEvent_3_t3976_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t3976_gp_0_0_0_0_UnityEvent_3_t3976_gp_1_0_0_0_UnityEvent_3_t3976_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t3977_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t3977_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t3977_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t3977_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t3977_gp_0_0_0_0_UnityEvent_4_t3977_gp_1_0_0_0_UnityEvent_4_t3977_gp_2_0_0_0_UnityEvent_4_t3977_gp_3_0_0_0_Types[] = { &UnityEvent_4_t3977_gp_0_0_0_0, &UnityEvent_4_t3977_gp_1_0_0_0, &UnityEvent_4_t3977_gp_2_0_0_0, &UnityEvent_4_t3977_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t3977_gp_0_0_0_0_UnityEvent_4_t3977_gp_1_0_0_0_UnityEvent_4_t3977_gp_2_0_0_0_UnityEvent_4_t3977_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t3977_gp_0_0_0_0_UnityEvent_4_t3977_gp_1_0_0_0_UnityEvent_4_t3977_gp_2_0_0_0_UnityEvent_4_t3977_gp_3_0_0_0_Types };
extern const Il2CppType SmartTerrainBuilderImpl_CreateReconstruction_m32065_gp_0_0_0_0;
static const Il2CppType* GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m32065_gp_0_0_0_0_Types[] = { &SmartTerrainBuilderImpl_CreateReconstruction_m32065_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m32065_gp_0_0_0_0 = { 1, GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m32065_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t3986_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t3986_gp_0_0_0_0_Types[] = { &HashSet_1_t3986_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t3986_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t3986_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3988_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3988_gp_0_0_0_0_Types[] = { &Enumerator_t3988_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3988_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3988_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3989_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3989_gp_0_0_0_0_Types[] = { &PrimeHelper_t3989_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3989_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3989_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m32350_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m32350_gp_0_0_0_0_Types[] = { &Enumerable_Any_m32350_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m32350_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m32350_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Cast_m32351_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Cast_m32351_gp_0_0_0_0_Types[] = { &Enumerable_Cast_m32351_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m32351_gp_0_0_0_0 = { 1, GenInst_Enumerable_Cast_m32351_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateCastIterator_m32352_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateCastIterator_m32352_gp_0_0_0_0_Types[] = { &Enumerable_CreateCastIterator_m32352_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m32352_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateCastIterator_m32352_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m32353_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m32353_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m32353_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m32353_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m32353_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m32354_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m32354_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m32354_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m32354_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m32354_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m32355_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m32355_gp_0_0_0_0_Types[] = { &Enumerable_Count_m32355_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m32355_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m32355_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Count_m32355_gp_0_0_0_0_Boolean_t384_0_0_0_Types[] = { &Enumerable_Count_m32355_gp_0_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m32355_gp_0_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_Enumerable_Count_m32355_gp_0_0_0_0_Boolean_t384_0_0_0_Types };
extern const Il2CppType Enumerable_Empty_m32356_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Empty_m32356_gp_0_0_0_0_Types[] = { &Enumerable_Empty_m32356_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Empty_m32356_gp_0_0_0_0 = { 1, GenInst_Enumerable_Empty_m32356_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m32357_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m32357_gp_0_0_0_0_Types[] = { &Enumerable_First_m32357_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m32357_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m32357_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m32357_gp_0_0_0_0_Boolean_t384_0_0_0_Types[] = { &Enumerable_First_m32357_gp_0_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m32357_gp_0_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_Enumerable_First_m32357_gp_0_0_0_0_Boolean_t384_0_0_0_Types };
extern const Il2CppType Enumerable_First_m32358_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m32358_gp_0_0_0_0_Types[] = { &Enumerable_First_m32358_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m32358_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m32358_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m32359_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m32359_gp_0_0_0_0_Types[] = { &Enumerable_First_m32359_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m32359_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m32359_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m32359_gp_0_0_0_0_Boolean_t384_0_0_0_Types[] = { &Enumerable_First_m32359_gp_0_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m32359_gp_0_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_Enumerable_First_m32359_gp_0_0_0_0_Boolean_t384_0_0_0_Types };
extern const Il2CppType Enumerable_Reverse_m32360_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Reverse_m32360_gp_0_0_0_0_Types[] = { &Enumerable_Reverse_m32360_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Reverse_m32360_gp_0_0_0_0 = { 1, GenInst_Enumerable_Reverse_m32360_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateReverseIterator_m32361_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateReverseIterator_m32361_gp_0_0_0_0_Types[] = { &Enumerable_CreateReverseIterator_m32361_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateReverseIterator_m32361_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateReverseIterator_m32361_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Take_m32362_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Take_m32362_gp_0_0_0_0_Types[] = { &Enumerable_Take_m32362_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Take_m32362_gp_0_0_0_0 = { 1, GenInst_Enumerable_Take_m32362_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateTakeIterator_m32363_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateTakeIterator_m32363_gp_0_0_0_0_Types[] = { &Enumerable_CreateTakeIterator_m32363_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateTakeIterator_m32363_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateTakeIterator_m32363_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m32364_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m32364_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m32364_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m32364_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m32364_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m32365_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m32365_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m32365_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m32365_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m32365_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m32366_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m32366_gp_0_0_0_0_Types[] = { &Enumerable_Where_m32366_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m32366_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m32366_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m32366_gp_0_0_0_0_Boolean_t384_0_0_0_Types[] = { &Enumerable_Where_m32366_gp_0_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m32366_gp_0_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_Enumerable_Where_m32366_gp_0_0_0_0_Boolean_t384_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m32367_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m32367_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m32367_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m32367_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m32367_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m32367_gp_0_0_0_0_Boolean_t384_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m32367_gp_0_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m32367_gp_0_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m32367_gp_0_0_0_0_Boolean_t384_0_0_0_Types };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t3990_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3990_gp_0_0_0_0_Types[] = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t3990_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3990_gp_0_0_0_0 = { 1, GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3990_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateReverseIteratorU3Ec__IteratorF_1_t3991_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateReverseIteratorU3Ec__IteratorF_1_t3991_gp_0_0_0_0_Types[] = { &U3CCreateReverseIteratorU3Ec__IteratorF_1_t3991_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateReverseIteratorU3Ec__IteratorF_1_t3991_gp_0_0_0_0 = { 1, GenInst_U3CCreateReverseIteratorU3Ec__IteratorF_1_t3991_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateTakeIteratorU3Ec__Iterator19_1_t3992_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t3992_gp_0_0_0_0_Types[] = { &U3CCreateTakeIteratorU3Ec__Iterator19_1_t3992_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t3992_gp_0_0_0_0 = { 1, GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t3992_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3993_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3993_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3993_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3993_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3993_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3993_gp_0_0_0_0_Boolean_t384_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3993_gp_0_0_0_0, &Boolean_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3993_gp_0_0_0_0_Boolean_t384_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3993_gp_0_0_0_0_Boolean_t384_0_0_0_Types };
extern const Il2CppType LinkedList_1_t3996_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t3996_gp_0_0_0_0_Types[] = { &LinkedList_1_t3996_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t3996_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t3996_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3997_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3997_gp_0_0_0_0_Types[] = { &Enumerator_t3997_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3997_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3997_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3998_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t3998_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t3998_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3998_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t3998_gp_0_0_0_0_Types };
extern const Il2CppType Queue_1_t3999_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t3999_gp_0_0_0_0_Types[] = { &Queue_1_t3999_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t3999_gp_0_0_0_0 = { 1, GenInst_Queue_1_t3999_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4000_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4000_gp_0_0_0_0_Types[] = { &Enumerator_t4000_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4000_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4000_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t4001_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4001_gp_0_0_0_0_Types[] = { &Stack_1_t4001_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4001_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4001_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4002_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4002_gp_0_0_0_0_Types[] = { &Enumerator_t4002_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4002_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4002_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4009_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4009_gp_0_0_0_0_Types[] = { &IEnumerable_1_t4009_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4009_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t4009_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m32563_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m32563_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m32563_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m32563_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m32563_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32575_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32575_gp_0_0_0_0_Array_Sort_m32575_gp_0_0_0_0_Types[] = { &Array_Sort_m32575_gp_0_0_0_0, &Array_Sort_m32575_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32575_gp_0_0_0_0_Array_Sort_m32575_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m32575_gp_0_0_0_0_Array_Sort_m32575_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32576_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m32576_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32576_gp_0_0_0_0_Array_Sort_m32576_gp_1_0_0_0_Types[] = { &Array_Sort_m32576_gp_0_0_0_0, &Array_Sort_m32576_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32576_gp_0_0_0_0_Array_Sort_m32576_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m32576_gp_0_0_0_0_Array_Sort_m32576_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m32577_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32577_gp_0_0_0_0_Types[] = { &Array_Sort_m32577_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32577_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32577_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m32577_gp_0_0_0_0_Array_Sort_m32577_gp_0_0_0_0_Types[] = { &Array_Sort_m32577_gp_0_0_0_0, &Array_Sort_m32577_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32577_gp_0_0_0_0_Array_Sort_m32577_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m32577_gp_0_0_0_0_Array_Sort_m32577_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32578_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32578_gp_0_0_0_0_Types[] = { &Array_Sort_m32578_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32578_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32578_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32578_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32578_gp_0_0_0_0_Array_Sort_m32578_gp_1_0_0_0_Types[] = { &Array_Sort_m32578_gp_0_0_0_0, &Array_Sort_m32578_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32578_gp_0_0_0_0_Array_Sort_m32578_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m32578_gp_0_0_0_0_Array_Sort_m32578_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m32579_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32579_gp_0_0_0_0_Array_Sort_m32579_gp_0_0_0_0_Types[] = { &Array_Sort_m32579_gp_0_0_0_0, &Array_Sort_m32579_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32579_gp_0_0_0_0_Array_Sort_m32579_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m32579_gp_0_0_0_0_Array_Sort_m32579_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32580_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m32580_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32580_gp_0_0_0_0_Array_Sort_m32580_gp_1_0_0_0_Types[] = { &Array_Sort_m32580_gp_0_0_0_0, &Array_Sort_m32580_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32580_gp_0_0_0_0_Array_Sort_m32580_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m32580_gp_0_0_0_0_Array_Sort_m32580_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m32581_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32581_gp_0_0_0_0_Types[] = { &Array_Sort_m32581_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32581_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32581_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m32581_gp_0_0_0_0_Array_Sort_m32581_gp_0_0_0_0_Types[] = { &Array_Sort_m32581_gp_0_0_0_0, &Array_Sort_m32581_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32581_gp_0_0_0_0_Array_Sort_m32581_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m32581_gp_0_0_0_0_Array_Sort_m32581_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32582_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32582_gp_0_0_0_0_Types[] = { &Array_Sort_m32582_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32582_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32582_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32582_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32582_gp_1_0_0_0_Types[] = { &Array_Sort_m32582_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32582_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m32582_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m32582_gp_0_0_0_0_Array_Sort_m32582_gp_1_0_0_0_Types[] = { &Array_Sort_m32582_gp_0_0_0_0, &Array_Sort_m32582_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32582_gp_0_0_0_0_Array_Sort_m32582_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m32582_gp_0_0_0_0_Array_Sort_m32582_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m32583_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32583_gp_0_0_0_0_Types[] = { &Array_Sort_m32583_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32583_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32583_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m32584_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m32584_gp_0_0_0_0_Types[] = { &Array_Sort_m32584_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m32584_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m32584_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m32585_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m32585_gp_0_0_0_0_Types[] = { &Array_qsort_m32585_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m32585_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m32585_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m32585_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m32585_gp_0_0_0_0_Array_qsort_m32585_gp_1_0_0_0_Types[] = { &Array_qsort_m32585_gp_0_0_0_0, &Array_qsort_m32585_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m32585_gp_0_0_0_0_Array_qsort_m32585_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m32585_gp_0_0_0_0_Array_qsort_m32585_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m32586_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m32586_gp_0_0_0_0_Types[] = { &Array_compare_m32586_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m32586_gp_0_0_0_0 = { 1, GenInst_Array_compare_m32586_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m32587_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m32587_gp_0_0_0_0_Types[] = { &Array_qsort_m32587_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m32587_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m32587_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m32590_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m32590_gp_0_0_0_0_Types[] = { &Array_Resize_m32590_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m32590_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m32590_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m32592_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m32592_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m32592_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m32592_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m32592_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m32593_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m32593_gp_0_0_0_0_Types[] = { &Array_ForEach_m32593_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m32593_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m32593_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m32594_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m32594_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m32594_gp_0_0_0_0_Array_ConvertAll_m32594_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m32594_gp_0_0_0_0, &Array_ConvertAll_m32594_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m32594_gp_0_0_0_0_Array_ConvertAll_m32594_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m32594_gp_0_0_0_0_Array_ConvertAll_m32594_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m32595_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m32595_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m32595_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m32595_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m32595_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m32596_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m32596_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m32596_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m32596_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m32596_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m32597_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m32597_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m32597_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m32597_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m32597_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m32598_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m32598_gp_0_0_0_0_Types[] = { &Array_FindIndex_m32598_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m32598_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m32598_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m32599_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m32599_gp_0_0_0_0_Types[] = { &Array_FindIndex_m32599_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m32599_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m32599_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m32600_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m32600_gp_0_0_0_0_Types[] = { &Array_FindIndex_m32600_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m32600_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m32600_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m32601_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m32601_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m32601_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m32601_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m32601_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m32602_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m32602_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m32602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m32602_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m32602_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m32603_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m32603_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m32603_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m32603_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m32603_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m32604_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m32604_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m32604_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m32604_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m32604_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m32605_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m32605_gp_0_0_0_0_Types[] = { &Array_IndexOf_m32605_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m32605_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m32605_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m32606_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m32606_gp_0_0_0_0_Types[] = { &Array_IndexOf_m32606_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m32606_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m32606_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m32607_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m32607_gp_0_0_0_0_Types[] = { &Array_IndexOf_m32607_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m32607_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m32607_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m32608_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m32608_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m32608_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m32608_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m32608_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m32609_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m32609_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m32609_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m32609_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m32609_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m32610_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m32610_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m32610_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m32610_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m32610_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m32611_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m32611_gp_0_0_0_0_Types[] = { &Array_FindAll_m32611_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m32611_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m32611_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m32612_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m32612_gp_0_0_0_0_Types[] = { &Array_Exists_m32612_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m32612_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m32612_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m32613_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m32613_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m32613_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m32613_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m32613_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m32614_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m32614_gp_0_0_0_0_Types[] = { &Array_Find_m32614_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m32614_gp_0_0_0_0 = { 1, GenInst_Array_Find_m32614_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m32615_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m32615_gp_0_0_0_0_Types[] = { &Array_FindLast_m32615_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m32615_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m32615_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t4010_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t4010_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t4010_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t4010_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t4010_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t4011_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t4011_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t4011_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t4011_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t4011_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t4012_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4012_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t4012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4012_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4012_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t4013_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t4013_gp_0_0_0_0_Types[] = { &IList_1_t4013_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t4013_gp_0_0_0_0 = { 1, GenInst_IList_1_t4013_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t4014_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t4014_gp_0_0_0_0_Types[] = { &ICollection_1_t4014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t4014_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t4014_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t2433_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t2433_gp_0_0_0_0_Types[] = { &Nullable_1_t2433_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t2433_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t2433_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t4021_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t4021_gp_0_0_0_0_Types[] = { &Comparer_1_t4021_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t4021_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t4021_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t4022_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t4022_gp_0_0_0_0_Types[] = { &DefaultComparer_t4022_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t4022_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t4022_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t3939_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t3939_gp_0_0_0_0_Types[] = { &GenericComparer_1_t3939_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t3939_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t3939_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t4023_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t4023_gp_0_0_0_0_Types[] = { &Dictionary_2_t4023_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4023_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t4023_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t4023_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_Types[] = { &Dictionary_2_t4023_gp_0_0_0_0, &Dictionary_2_t4023_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t5474_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t5474_0_0_0_Types[] = { &KeyValuePair_2_t5474_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t5474_0_0_0 = { 1, GenInst_KeyValuePair_2_t5474_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m32763_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m32763_gp_0_0_0_0_Types[] = { &Dictionary_2_t4023_gp_0_0_0_0, &Dictionary_2_t4023_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m32763_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m32763_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m32763_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m32768_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m32768_gp_0_0_0_0_Types[] = { &Dictionary_2_t4023_gp_0_0_0_0, &Dictionary_2_t4023_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m32768_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m32768_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m32768_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m32768_gp_0_0_0_0_Object_t_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m32768_gp_0_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m32768_gp_0_0_0_0_Object_t_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m32768_gp_0_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_DictionaryEntry_t451_0_0_0_Types[] = { &Dictionary_2_t4023_gp_0_0_0_0, &Dictionary_2_t4023_gp_1_0_0_0, &DictionaryEntry_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_DictionaryEntry_t451_0_0_0 = { 3, GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_DictionaryEntry_t451_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t4024_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t4024_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t4024_gp_0_0_0_0_ShimEnumerator_t4024_gp_1_0_0_0_Types[] = { &ShimEnumerator_t4024_gp_0_0_0_0, &ShimEnumerator_t4024_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t4024_gp_0_0_0_0_ShimEnumerator_t4024_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t4024_gp_0_0_0_0_ShimEnumerator_t4024_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t4025_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4025_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4025_gp_0_0_0_0_Enumerator_t4025_gp_1_0_0_0_Types[] = { &Enumerator_t4025_gp_0_0_0_0, &Enumerator_t4025_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4025_gp_0_0_0_0_Enumerator_t4025_gp_1_0_0_0 = { 2, GenInst_Enumerator_t4025_gp_0_0_0_0_Enumerator_t4025_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t5488_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t5488_0_0_0_Types[] = { &KeyValuePair_2_t5488_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t5488_0_0_0 = { 1, GenInst_KeyValuePair_2_t5488_0_0_0_Types };
extern const Il2CppType KeyCollection_t4026_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t4026_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_1_0_0_0_Types[] = { &KeyCollection_t4026_gp_0_0_0_0, &KeyCollection_t4026_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t4026_gp_0_0_0_0_Types[] = { &KeyCollection_t4026_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t4026_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t4026_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4027_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4027_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4027_gp_0_0_0_0_Enumerator_t4027_gp_1_0_0_0_Types[] = { &Enumerator_t4027_gp_0_0_0_0, &Enumerator_t4027_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4027_gp_0_0_0_0_Enumerator_t4027_gp_1_0_0_0 = { 2, GenInst_Enumerator_t4027_gp_0_0_0_0_Enumerator_t4027_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t4027_gp_0_0_0_0_Types[] = { &Enumerator_t4027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4027_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4027_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_1_0_0_0_KeyCollection_t4026_gp_0_0_0_0_Types[] = { &KeyCollection_t4026_gp_0_0_0_0, &KeyCollection_t4026_gp_1_0_0_0, &KeyCollection_t4026_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_1_0_0_0_KeyCollection_t4026_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_1_0_0_0_KeyCollection_t4026_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_0_0_0_0_Types[] = { &KeyCollection_t4026_gp_0_0_0_0, &KeyCollection_t4026_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t4028_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t4028_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t4028_gp_0_0_0_0_ValueCollection_t4028_gp_1_0_0_0_Types[] = { &ValueCollection_t4028_gp_0_0_0_0, &ValueCollection_t4028_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t4028_gp_0_0_0_0_ValueCollection_t4028_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t4028_gp_0_0_0_0_ValueCollection_t4028_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t4028_gp_1_0_0_0_Types[] = { &ValueCollection_t4028_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t4028_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t4028_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t4029_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4029_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4029_gp_0_0_0_0_Enumerator_t4029_gp_1_0_0_0_Types[] = { &Enumerator_t4029_gp_0_0_0_0, &Enumerator_t4029_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4029_gp_0_0_0_0_Enumerator_t4029_gp_1_0_0_0 = { 2, GenInst_Enumerator_t4029_gp_0_0_0_0_Enumerator_t4029_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t4029_gp_1_0_0_0_Types[] = { &Enumerator_t4029_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4029_gp_1_0_0_0 = { 1, GenInst_Enumerator_t4029_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t4028_gp_0_0_0_0_ValueCollection_t4028_gp_1_0_0_0_ValueCollection_t4028_gp_1_0_0_0_Types[] = { &ValueCollection_t4028_gp_0_0_0_0, &ValueCollection_t4028_gp_1_0_0_0, &ValueCollection_t4028_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t4028_gp_0_0_0_0_ValueCollection_t4028_gp_1_0_0_0_ValueCollection_t4028_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t4028_gp_0_0_0_0_ValueCollection_t4028_gp_1_0_0_0_ValueCollection_t4028_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t4028_gp_1_0_0_0_ValueCollection_t4028_gp_1_0_0_0_Types[] = { &ValueCollection_t4028_gp_1_0_0_0, &ValueCollection_t4028_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t4028_gp_1_0_0_0_ValueCollection_t4028_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t4028_gp_1_0_0_0_ValueCollection_t4028_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_KeyValuePair_2_t5474_0_0_0_Types[] = { &Dictionary_2_t4023_gp_0_0_0_0, &Dictionary_2_t4023_gp_1_0_0_0, &KeyValuePair_2_t5474_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_KeyValuePair_2_t5474_0_0_0 = { 3, GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_KeyValuePair_2_t5474_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t5474_0_0_0_KeyValuePair_2_t5474_0_0_0_Types[] = { &KeyValuePair_2_t5474_0_0_0, &KeyValuePair_2_t5474_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t5474_0_0_0_KeyValuePair_2_t5474_0_0_0 = { 2, GenInst_KeyValuePair_2_t5474_0_0_0_KeyValuePair_2_t5474_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t4023_gp_1_0_0_0_Types[] = { &Dictionary_2_t4023_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t4023_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t4023_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t4031_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t4031_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t4031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t4031_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t4031_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t4032_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t4032_gp_0_0_0_0_Types[] = { &DefaultComparer_t4032_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t4032_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t4032_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t3938_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t3938_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t3938_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t3938_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t3938_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t5524_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t5524_0_0_0_Types[] = { &KeyValuePair_2_t5524_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t5524_0_0_0 = { 1, GenInst_KeyValuePair_2_t5524_0_0_0_Types };
extern const Il2CppType IDictionary_2_t4034_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t4034_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t4034_gp_0_0_0_0_IDictionary_2_t4034_gp_1_0_0_0_Types[] = { &IDictionary_2_t4034_gp_0_0_0_0, &IDictionary_2_t4034_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t4034_gp_0_0_0_0_IDictionary_2_t4034_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t4034_gp_0_0_0_0_IDictionary_2_t4034_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4036_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t4036_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4036_gp_0_0_0_0_KeyValuePair_2_t4036_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t4036_gp_0_0_0_0, &KeyValuePair_2_t4036_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4036_gp_0_0_0_0_KeyValuePair_2_t4036_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t4036_gp_0_0_0_0_KeyValuePair_2_t4036_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t4037_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t4037_gp_0_0_0_0_Types[] = { &List_1_t4037_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t4037_gp_0_0_0_0 = { 1, GenInst_List_1_t4037_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4038_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4038_gp_0_0_0_0_Types[] = { &Enumerator_t4038_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4038_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4038_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t4039_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t4039_gp_0_0_0_0_Types[] = { &Collection_1_t4039_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t4039_gp_0_0_0_0 = { 1, GenInst_Collection_1_t4039_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t4040_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t4040_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t4040_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t4040_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t4040_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m33045_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m33045_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m33045_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m33045_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m33045_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m33045_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m33045_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m33045_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m33045_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m33045_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m33046_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m33046_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m33046_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m33046_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m33046_gp_0_0_0_0_Types };
extern const Il2CppGenericInst* g_Il2CppGenericInstTable[862] = 
{
	&GenInst_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_OrthographicCameraObserver_t336_0_0_0,
	&GenInst_Camera_t6_0_0_0,
	&GenInst_Renderer_t367_0_0_0,
	&GenInst_MonoBehaviour_t5_0_0_0,
	&GenInst_Collider_t369_0_0_0,
	&GenInst_Enum_t21_0_0_0,
	&GenInst_TextMesh_t26_0_0_0,
	&GenInst_String_t_0_0_0_FsmState_t29_0_0_0,
	&GenInst_FsmAction_t51_0_0_0,
	&GenInst_OrthographicCamera_t4_0_0_0,
	&GenInst_InputLayer_t56_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t372_0_0_0,
	&GenInst_Log_t72_0_0_0,
	&GenInst_Queue_1_t76_0_0_0,
	&GenInst_NotificationInstance_t85_0_0_0,
	&GenInst_NotificationHandler_t347_0_0_0,
	&GenInst_MeshFilter_t398_0_0_0,
	&GenInst_MeshRenderer_t402_0_0_0,
	&GenInst_String_t_0_0_0_QueryResultResolver_t328_0_0_0,
	&GenInst_SwarmItem_t124_0_0_0,
	&GenInst_SignalListener_t114_0_0_0,
	&GenInst_String_t_0_0_0_Signal_t116_0_0_0,
	&GenInst_String_t_0_0_0_TimeReference_t14_0_0_0,
	&GenInst_Command_t348_0_0_0,
	&GenInst_BoxCollider_t126_0_0_0,
	&GenInst_Type_t_0_0_0_Object_t_0_0_0,
	&GenInst_SimpleXmlNode_t142_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_GameObject_t155_0_0_0,
	&GenInst_TaskBase_t163_0_0_0,
	&GenInst_ActionThread_t171_0_0_0,
	&GenInst_ThreadBase_t169_0_0_0_IEnumerator_t337_0_0_0,
	&GenInst_IEnumerator_t337_0_0_0,
	&GenInst_ThreadBase_t169_0_0_0,
	&GenInst_UnityThreadHelper_t181_0_0_0,
	&GenInst_ThreadBase_t169_0_0_0_Boolean_t384_0_0_0,
	&GenInst_Platform_t135_0_0_0,
	&GenInst_EScreens_t188_0_0_0_String_t_0_0_0,
	&GenInst_ESubScreens_t189_0_0_0_String_t_0_0_0,
	&GenInst_RaffleData_t206_0_0_0,
	&GenInst_Raffle_t202_0_0_0,
	&GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0,
	&GenInst_ERaffleResult_t207_0_0_0,
	&GenInst_ImageTargetBehaviour_t210_0_0_0,
	&GenInst_TrackableBehaviour_t211_0_0_0,
	&GenInst_FocusMode_t438_0_0_0,
	&GenInst_KeyValuePair_2_t352_0_0_0_Boolean_t384_0_0_0,
	&GenInst_KeyValuePair_2_t352_0_0_0,
	&GenInst_InputField_t226_0_0_0_Boolean_t384_0_0_0,
	&GenInst_InputField_t226_0_0_0_String_t_0_0_0,
	&GenInst_InputField_t226_0_0_0_Image_t213_0_0_0,
	&GenInst_AnimatorFrame_t235_0_0_0,
	&GenInst_AudioSource_t44_0_0_0,
	&GenInst_GUITexture_t243_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_Hashtable_t261_0_0_0,
	&GenInst_GUIText_t443_0_0_0,
	&GenInst_Light_t444_0_0_0,
	&GenInst_Vector3_t36_0_0_0,
	&GenInst_Rigidbody_t447_0_0_0,
	&GenInst_iTween_t258_0_0_0,
	&GenInst_InitError_t1233_0_0_0,
	&GenInst_ReconstructionBehaviour_t278_0_0_0,
	&GenInst_Prop_t357_0_0_0,
	&GenInst_Surface_t358_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst_MaskOutBehaviour_t294_0_0_0,
	&GenInst_VirtualButtonBehaviour_t318_0_0_0,
	&GenInst_TurnOffBehaviour_t309_0_0_0,
	&GenInst_MarkerBehaviour_t292_0_0_0,
	&GenInst_MultiTargetBehaviour_t296_0_0_0,
	&GenInst_CylinderTargetBehaviour_t272_0_0_0,
	&GenInst_WordBehaviour_t326_0_0_0,
	&GenInst_TextRecoBehaviour_t307_0_0_0,
	&GenInst_ObjectTargetBehaviour_t298_0_0_0,
	&GenInst_ComponentFactoryStarterBehaviour_t287_0_0_0,
	&GenInst_VuforiaBehaviour_t320_0_0_0,
	&GenInst_WireframeBehaviour_t324_0_0_0,
	&GenInst_BaseInputModule_t479_0_0_0,
	&GenInst_RaycastResult_t512_0_0_0,
	&GenInst_IDeselectHandler_t700_0_0_0,
	&GenInst_ISelectHandler_t699_0_0_0,
	&GenInst_BaseEventData_t480_0_0_0,
	&GenInst_Entry_t484_0_0_0,
	&GenInst_IPointerEnterHandler_t687_0_0_0,
	&GenInst_IPointerExitHandler_t688_0_0_0,
	&GenInst_IPointerDownHandler_t689_0_0_0,
	&GenInst_IPointerUpHandler_t690_0_0_0,
	&GenInst_IPointerClickHandler_t691_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t692_0_0_0,
	&GenInst_IBeginDragHandler_t693_0_0_0,
	&GenInst_IDragHandler_t694_0_0_0,
	&GenInst_IEndDragHandler_t695_0_0_0,
	&GenInst_IDropHandler_t696_0_0_0,
	&GenInst_IScrollHandler_t697_0_0_0,
	&GenInst_IUpdateSelectedHandler_t698_0_0_0,
	&GenInst_IMoveHandler_t701_0_0_0,
	&GenInst_ISubmitHandler_t702_0_0_0,
	&GenInst_ICancelHandler_t703_0_0_0,
	&GenInst_List_1_t686_0_0_0,
	&GenInst_IEventSystemHandler_t2811_0_0_0,
	&GenInst_Transform_t35_0_0_0,
	&GenInst_PointerEventData_t517_0_0_0,
	&GenInst_AxisEventData_t514_0_0_0,
	&GenInst_BaseRaycaster_t513_0_0_0,
	&GenInst_EventSystem_t476_0_0_0,
	&GenInst_ButtonState_t520_0_0_0,
	&GenInst_Int32_t372_0_0_0_PointerEventData_t517_0_0_0,
	&GenInst_SpriteRenderer_t727_0_0_0,
	&GenInst_RaycastHit_t60_0_0_0,
	&GenInst_Color_t9_0_0_0,
	&GenInst_Single_t388_0_0_0,
	&GenInst_ICanvasElement_t708_0_0_0,
	&GenInst_RectTransform_t556_0_0_0,
	&GenInst_Image_t213_0_0_0,
	&GenInst_Button_t544_0_0_0,
	&GenInst_Text_t196_0_0_0,
	&GenInst_RawImage_t237_0_0_0,
	&GenInst_Slider_t637_0_0_0,
	&GenInst_Scrollbar_t621_0_0_0,
	&GenInst_Toggle_t557_0_0_0,
	&GenInst_InputField_t226_0_0_0,
	&GenInst_ScrollRect_t627_0_0_0,
	&GenInst_Mask_t606_0_0_0,
	&GenInst_Dropdown_t564_0_0_0,
	&GenInst_OptionData_t558_0_0_0,
	&GenInst_Int32_t372_0_0_0,
	&GenInst_DropdownItem_t555_0_0_0,
	&GenInst_FloatTween_t539_0_0_0,
	&GenInst_Canvas_t574_0_0_0,
	&GenInst_GraphicRaycaster_t578_0_0_0,
	&GenInst_CanvasGroup_t733_0_0_0,
	&GenInst_Boolean_t384_0_0_0,
	&GenInst_Font_t569_0_0_0_List_1_t738_0_0_0,
	&GenInst_Font_t569_0_0_0,
	&GenInst_ColorTween_t536_0_0_0,
	&GenInst_CanvasRenderer_t573_0_0_0,
	&GenInst_Component_t365_0_0_0,
	&GenInst_Graphic_t572_0_0_0,
	&GenInst_Canvas_t574_0_0_0_IndexedSet_1_t747_0_0_0,
	&GenInst_Sprite_t553_0_0_0,
	&GenInst_Type_t583_0_0_0,
	&GenInst_FillMethod_t584_0_0_0,
	&GenInst_SubmitEvent_t595_0_0_0,
	&GenInst_OnChangeEvent_t597_0_0_0,
	&GenInst_OnValidateInput_t599_0_0_0,
	&GenInst_ContentType_t591_0_0_0,
	&GenInst_LineType_t594_0_0_0,
	&GenInst_InputType_t592_0_0_0,
	&GenInst_TouchScreenKeyboardType_t749_0_0_0,
	&GenInst_CharacterValidation_t593_0_0_0,
	&GenInst_Char_t383_0_0_0,
	&GenInst_UILineInfo_t752_0_0_0,
	&GenInst_UICharInfo_t754_0_0_0,
	&GenInst_LayoutElement_t665_0_0_0,
	&GenInst_IClippable_t713_0_0_0,
	&GenInst_RectMask2D_t609_0_0_0,
	&GenInst_Direction_t617_0_0_0,
	&GenInst_Vector2_t2_0_0_0,
	&GenInst_Selectable_t545_0_0_0,
	&GenInst_Navigation_t613_0_0_0,
	&GenInst_Transition_t628_0_0_0,
	&GenInst_ColorBlock_t551_0_0_0,
	&GenInst_SpriteState_t630_0_0_0,
	&GenInst_AnimationTriggers_t540_0_0_0,
	&GenInst_Animator_t714_0_0_0,
	&GenInst_Direction_t634_0_0_0,
	&GenInst_MatEntry_t638_0_0_0,
	&GenInst_UIVertex_t605_0_0_0,
	&GenInst_Toggle_t557_0_0_0_Boolean_t384_0_0_0,
	&GenInst_IClipper_t717_0_0_0,
	&GenInst_AspectMode_t650_0_0_0,
	&GenInst_FitMode_t656_0_0_0,
	&GenInst_Corner_t658_0_0_0,
	&GenInst_Axis_t659_0_0_0,
	&GenInst_Constraint_t660_0_0_0,
	&GenInst_RectOffset_t666_0_0_0,
	&GenInst_TextAnchor_t766_0_0_0,
	&GenInst_ILayoutElement_t719_0_0_0_Single_t388_0_0_0,
	&GenInst_Color32_t711_0_0_0,
	&GenInst_Vector4_t680_0_0_0,
	&GenInst_GcLeaderboard_t805_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t1017_0_0_0,
	&GenInst_IAchievementU5BU5D_t1019_0_0_0,
	&GenInst_IScoreU5BU5D_t956_0_0_0,
	&GenInst_IUserProfileU5BU5D_t952_0_0_0,
	&GenInst_ByteU5BU5D_t139_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t384_0_0_0,
	&GenInst_Rigidbody2D_t868_0_0_0,
	&GenInst_Int32_t372_0_0_0_LayoutCache_t904_0_0_0,
	&GenInst_GUILayoutEntry_t907_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t342_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_GUILayer_t810_0_0_0,
	&GenInst_PersistentCall_t984_0_0_0,
	&GenInst_BaseInvokableCall_t981_0_0_0,
	&GenInst_Camera_t6_0_0_0_VideoBackgroundAbstractBehaviour_t315_0_0_0,
	&GenInst_BackgroundPlaneAbstractBehaviour_t269_0_0_0,
	&GenInst_VideoBackgroundAbstractBehaviour_t315_0_0_0,
	&GenInst_ITrackableEventHandler_t1252_0_0_0,
	&GenInst_SmartTerrainTracker_t1168_0_0_0,
	&GenInst_ReconstructionAbstractBehaviour_t301_0_0_0,
	&GenInst_ICloudRecoEventHandler_t1253_0_0_0,
	&GenInst_TargetSearchResult_t1212_0_0_0,
	&GenInst_ObjectTracker_t1066_0_0_0,
	&GenInst_HideExcessAreaAbstractBehaviour_t284_0_0_0,
	&GenInst_ReconstructionFromTarget_t1077_0_0_0,
	&GenInst_VirtualButton_t1223_0_0_0,
	&GenInst_PIXEL_FORMAT_t1108_0_0_0_Image_t1109_0_0_0,
	&GenInst_PIXEL_FORMAT_t1108_0_0_0,
	&GenInst_Int32_t372_0_0_0_Trackable_t1060_0_0_0,
	&GenInst_Trackable_t1060_0_0_0,
	&GenInst_Int32_t372_0_0_0_VirtualButton_t1223_0_0_0,
	&GenInst_DataSet_t1088_0_0_0,
	&GenInst_DataSetImpl_t1073_0_0_0,
	&GenInst_Int32_t372_0_0_0_Marker_t1231_0_0_0,
	&GenInst_Marker_t1231_0_0_0,
	&GenInst_TrackableResultData_t1134_0_0_0,
	&GenInst_SmartTerrainTrackable_t1085_0_0_0,
	&GenInst_SmartTerrainTrackableBehaviour_t1084_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t393_0_0_0,
	&GenInst_Int32_t372_0_0_0_WordResult_t1188_0_0_0,
	&GenInst_WordAbstractBehaviour_t327_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1186_0_0_0,
	&GenInst_Int32_t372_0_0_0_WordAbstractBehaviour_t327_0_0_0,
	&GenInst_WordData_t1139_0_0_0,
	&GenInst_WordResultData_t1138_0_0_0,
	&GenInst_Word_t1190_0_0_0,
	&GenInst_WordResult_t1188_0_0_0,
	&GenInst_ILoadLevelEventHandler_t1268_0_0_0,
	&GenInst_SmartTerrainInitializationInfo_t1083_0_0_0,
	&GenInst_Int32_t372_0_0_0_Prop_t357_0_0_0,
	&GenInst_Int32_t372_0_0_0_Surface_t358_0_0_0,
	&GenInst_ISmartTerrainEventHandler_t1271_0_0_0,
	&GenInst_Int32_t372_0_0_0_PropAbstractBehaviour_t300_0_0_0,
	&GenInst_Int32_t372_0_0_0_SurfaceAbstractBehaviour_t306_0_0_0,
	&GenInst_PropAbstractBehaviour_t300_0_0_0,
	&GenInst_SurfaceAbstractBehaviour_t306_0_0_0,
	&GenInst_Int32_t372_0_0_0_TrackableBehaviour_t211_0_0_0,
	&GenInst_MarkerTracker_t1121_0_0_0,
	&GenInst_DataSetTrackableBehaviour_t1061_0_0_0,
	&GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0,
	&GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0,
	&GenInst_VirtualButtonAbstractBehaviour_t319_0_0_0,
	&GenInst_Int32_t372_0_0_0_ImageTarget_t1225_0_0_0,
	&GenInst_ImageTarget_t1225_0_0_0,
	&GenInst_ImageTargetAbstractBehaviour_t285_0_0_0,
	&GenInst_Int32_t372_0_0_0_VirtualButtonAbstractBehaviour_t319_0_0_0,
	&GenInst_ITrackerEventHandler_t1279_0_0_0,
	&GenInst_TextTracker_t1170_0_0_0,
	&GenInst_WebCamAbstractBehaviour_t323_0_0_0,
	&GenInst_IVideoBackgroundEventHandler_t1280_0_0_0,
	&GenInst_ITextRecoEventHandler_t1281_0_0_0,
	&GenInst_IUserDefinedTargetEventHandler_t1282_0_0_0,
	&GenInst_VuforiaAbstractBehaviour_t321_0_0_0,
	&GenInst_IVirtualButtonEventHandler_t1283_0_0_0,
	&GenInst_StrongName_t2246_0_0_0,
	&GenInst_DateTime_t74_0_0_0,
	&GenInst_DateTimeOffset_t2323_0_0_0,
	&GenInst_TimeSpan_t427_0_0_0,
	&GenInst_Guid_t452_0_0_0,
	&GenInst_IReflect_t4017_0_0_0,
	&GenInst__Type_t4015_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t2427_0_0_0,
	&GenInst__MemberInfo_t4016_0_0_0,
	&GenInst_IFormattable_t2429_0_0_0,
	&GenInst_IConvertible_t2432_0_0_0,
	&GenInst_IComparable_t2431_0_0_0,
	&GenInst_IComparable_1_t4233_0_0_0,
	&GenInst_IEquatable_1_t4238_0_0_0,
	&GenInst_ValueType_t1763_0_0_0,
	&GenInst_Double_t387_0_0_0,
	&GenInst_IComparable_1_t4250_0_0_0,
	&GenInst_IEquatable_1_t4255_0_0_0,
	&GenInst_IComparable_1_t4263_0_0_0,
	&GenInst_IEquatable_1_t4268_0_0_0,
	&GenInst_Object_t335_0_0_0,
	&GenInst_Behaviour_t772_0_0_0,
	&GenInst_KeyValuePair_2_t2507_0_0_0,
	&GenInst_IEnumerable_t375_0_0_0,
	&GenInst_ICloneable_t4008_0_0_0,
	&GenInst_IComparable_1_t4309_0_0_0,
	&GenInst_IEquatable_1_t4314_0_0_0,
	&GenInst_Link_t1869_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_DictionaryEntry_t451_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2507_0_0_0,
	&GenInst_String_t_0_0_0_FsmState_t29_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2520_0_0_0,
	&GenInst_GUIContent_t379_0_0_0,
	&GenInst_InputLayerElement_t58_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t372_0_0_0,
	&GenInst_KeyValuePair_2_t2546_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t372_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t372_0_0_0_Int32_t372_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t372_0_0_0_KeyValuePair_2_t2546_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2560_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t434_0_0_0,
	&GenInst_Matrix4x4_t404_0_0_0,
	&GenInst_BoneWeight_t405_0_0_0,
	&GenInst_NotificationHandlerComponent_t84_0_0_0,
	&GenInst_PrefabItem_t154_0_0_0,
	&GenInst_PruneData_t93_0_0_0,
	&GenInst_PreloadData_t94_0_0_0,
	&GenInst_String_t_0_0_0_QueryResultResolver_t328_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2593_0_0_0,
	&GenInst_PrefabItemLists_t151_0_0_0,
	&GenInst_String_t_0_0_0_Signal_t116_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2611_0_0_0,
	&GenInst_String_t_0_0_0_TimeReference_t14_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2617_0_0_0,
	&GenInst_Type_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2628_0_0_0,
	&GenInst_Byte_t391_0_0_0,
	&GenInst_IComparable_1_t4395_0_0_0,
	&GenInst_IEquatable_1_t4400_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t422_0_0_0,
	&GenInst_WaitHandle_t351_0_0_0,
	&GenInst_IDisposable_t364_0_0_0,
	&GenInst_MarshalByRefObject_t1643_0_0_0,
	&GenInst_TaskWorker_t168_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t384_0_0_0,
	&GenInst_EScreens_t188_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2667_0_0_0,
	&GenInst_EScreens_t188_0_0_0,
	&GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_EScreens_t188_0_0_0,
	&GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_EScreens_t188_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2667_0_0_0,
	&GenInst_EScreens_t188_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2681_0_0_0,
	&GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2690_0_0_0,
	&GenInst_ESubScreens_t189_0_0_0,
	&GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_ESubScreens_t189_0_0_0,
	&GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_ESubScreens_t189_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2690_0_0_0,
	&GenInst_ESubScreens_t189_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2704_0_0_0,
	&GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_Int32_t372_0_0_0,
	&GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_ERaffleResult_t207_0_0_0,
	&GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_Int32_t372_0_0_0_ERaffleResult_t207_0_0_0_KeyValuePair_2_t352_0_0_0,
	&GenInst_KeyValuePair_2_t2740_0_0_0,
	&GenInst_IComparable_1_t4464_0_0_0,
	&GenInst_IEquatable_1_t4469_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_Boolean_t384_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t384_0_0_0_KeyValuePair_2_t2740_0_0_0,
	&GenInst_InputField_t226_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2755_0_0_0,
	&GenInst_InputField_t226_0_0_0_String_t_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2760_0_0_0,
	&GenInst_InputField_t226_0_0_0_Image_t213_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2766_0_0_0,
	&GenInst_GUILayoutOption_t912_0_0_0,
	&GenInst_IComparable_1_t4494_0_0_0,
	&GenInst_IEquatable_1_t4499_0_0_0,
	&GenInst_Rect_t267_0_0_0,
	&GenInst_Material_t82_0_0_0,
	&GenInst_List_1_t344_0_0_0,
	&GenInst_List_1_t718_0_0_0,
	&GenInst_Int32_t372_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2871_0_0_0,
	&GenInst_Int32_t372_0_0_0_Object_t_0_0_0_Int32_t372_0_0_0,
	&GenInst_Int32_t372_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Int32_t372_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_Int32_t372_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2871_0_0_0,
	&GenInst_Int32_t372_0_0_0_PointerEventData_t517_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t724_0_0_0,
	&GenInst_RaycastHit2D_t729_0_0_0,
	&GenInst_ICanvasElement_t708_0_0_0_Int32_t372_0_0_0,
	&GenInst_ICanvasElement_t708_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_List_1_t734_0_0_0,
	&GenInst_Font_t569_0_0_0_List_1_t738_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2934_0_0_0,
	&GenInst_Graphic_t572_0_0_0_Int32_t372_0_0_0,
	&GenInst_Graphic_t572_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_Canvas_t574_0_0_0_IndexedSet_1_t747_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2958_0_0_0,
	&GenInst_KeyValuePair_2_t2962_0_0_0,
	&GenInst_KeyValuePair_2_t2966_0_0_0,
	&GenInst_IClipper_t717_0_0_0_Int32_t372_0_0_0,
	&GenInst_IClipper_t717_0_0_0_Int32_t372_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3017_0_0_0,
	&GenInst_LayoutRebuilder_t668_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t388_0_0_0,
	&GenInst_List_1_t675_0_0_0,
	&GenInst_List_1_t676_0_0_0,
	&GenInst_List_1_t677_0_0_0,
	&GenInst_List_1_t678_0_0_0,
	&GenInst_List_1_t679_0_0_0,
	&GenInst_IAchievementDescription_t3969_0_0_0,
	&GenInst_IAchievement_t1003_0_0_0,
	&GenInst_IScore_t958_0_0_0,
	&GenInst_IUserProfile_t3968_0_0_0,
	&GenInst_AchievementDescription_t954_0_0_0,
	&GenInst_UserProfile_t951_0_0_0,
	&GenInst_GcAchievementData_t939_0_0_0,
	&GenInst_Achievement_t953_0_0_0,
	&GenInst_GcScoreData_t940_0_0_0,
	&GenInst_Score_t955_0_0_0,
	&GenInst_Display_t845_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_ISerializable_t2454_0_0_0,
	&GenInst_ContactPoint_t863_0_0_0,
	&GenInst_ContactPoint2D_t869_0_0_0,
	&GenInst_WebCamDevice_t876_0_0_0,
	&GenInst_Keyframe_t883_0_0_0,
	&GenInst_CharacterInfo_t892_0_0_0,
	&GenInst_Int32_t372_0_0_0_LayoutCache_t904_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3152_0_0_0,
	&GenInst_GUIStyle_t342_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t342_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3163_0_0_0,
	&GenInst_DisallowMultipleComponent_t929_0_0_0,
	&GenInst_Attribute_t463_0_0_0,
	&GenInst__Attribute_t4004_0_0_0,
	&GenInst_ExecuteInEditMode_t932_0_0_0,
	&GenInst_RequireComponent_t930_0_0_0,
	&GenInst_ParameterModifier_t2024_0_0_0,
	&GenInst_HitInfo_t959_0_0_0,
	&GenInst_ParameterInfo_t1035_0_0_0,
	&GenInst__ParameterInfo_t4058_0_0_0,
	&GenInst_Event_t381_0_0_0_TextEditOp_t976_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0,
	&GenInst_KeyValuePair_2_t3183_0_0_0,
	&GenInst_TextEditOp_t976_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_TextEditOp_t976_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t976_0_0_0_KeyValuePair_2_t3183_0_0_0,
	&GenInst_Event_t381_0_0_0,
	&GenInst_Event_t381_0_0_0_TextEditOp_t976_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3197_0_0_0,
	&GenInst_EyewearCalibrationReading_t1080_0_0_0,
	&GenInst_Camera_t6_0_0_0_VideoBackgroundAbstractBehaviour_t315_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3227_0_0_0,
	&GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3257_0_0_0,
	&GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_PIXEL_FORMAT_t1108_0_0_0,
	&GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_PIXEL_FORMAT_t1108_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3257_0_0_0,
	&GenInst_PIXEL_FORMAT_t1108_0_0_0_Image_t1109_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3271_0_0_0,
	&GenInst_Int32_t372_0_0_0_Trackable_t1060_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3284_0_0_0,
	&GenInst_Int32_t372_0_0_0_VirtualButton_t1223_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3292_0_0_0,
	&GenInst_Int32_t372_0_0_0_Marker_t1231_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3308_0_0_0,
	&GenInst_SmartTerrainRevisionData_t1142_0_0_0,
	&GenInst_SurfaceData_t1143_0_0_0,
	&GenInst_PropData_t1144_0_0_0,
	&GenInst_IEditorTrackableBehaviour_t1315_0_0_0,
	&GenInst_Image_t1109_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t393_0_0_0,
	&GenInst_KeyValuePair_2_t3337_0_0_0,
	&GenInst_UInt16_t393_0_0_0,
	&GenInst_IComparable_1_t4819_0_0_0,
	&GenInst_IEquatable_1_t4824_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_UInt16_t393_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_Object_t_0_0_0_UInt16_t393_0_0_0_KeyValuePair_2_t3337_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t393_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3352_0_0_0,
	&GenInst_RectangleData_t1089_0_0_0,
	&GenInst_Int32_t372_0_0_0_WordResult_t1188_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3359_0_0_0,
	&GenInst_Int32_t372_0_0_0_WordAbstractBehaviour_t327_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t1334_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1186_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3378_0_0_0,
	&GenInst_List_1_t1186_0_0_0,
	&GenInst_Int32_t372_0_0_0_Surface_t358_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t1349_0_0_0,
	&GenInst_Int32_t372_0_0_0_SurfaceAbstractBehaviour_t306_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3399_0_0_0,
	&GenInst_Int32_t372_0_0_0_Prop_t357_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t1348_0_0_0,
	&GenInst_Int32_t372_0_0_0_PropAbstractBehaviour_t300_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3405_0_0_0,
	&GenInst_Int32_t372_0_0_0_TrackableBehaviour_t211_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3419_0_0_0,
	&GenInst_MarkerAbstractBehaviour_t293_0_0_0,
	&GenInst_IEditorMarkerBehaviour_t1364_0_0_0,
	&GenInst_WorldCenterTrackableBehaviour_t1153_0_0_0,
	&GenInst_IEditorDataSetTrackableBehaviour_t1366_0_0_0,
	&GenInst_IEditorVirtualButtonBehaviour_t1381_0_0_0,
	&GenInst_KeyValuePair_2_t3425_0_0_0,
	&GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_Int32_t372_0_0_0,
	&GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_TrackableResultData_t1134_0_0_0,
	&GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_Int32_t372_0_0_0_TrackableResultData_t1134_0_0_0_KeyValuePair_2_t3425_0_0_0,
	&GenInst_KeyValuePair_2_t3440_0_0_0,
	&GenInst_VirtualButtonData_t1135_0_0_0,
	&GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_Int32_t372_0_0_0,
	&GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_VirtualButtonData_t1135_0_0_0,
	&GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_Int32_t372_0_0_0_VirtualButtonData_t1135_0_0_0_KeyValuePair_2_t3440_0_0_0,
	&GenInst_Int32_t372_0_0_0_ImageTarget_t1225_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3471_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t1226_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0,
	&GenInst_KeyValuePair_2_t3478_0_0_0,
	&GenInst_ProfileData_t1226_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_ProfileData_t1226_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_Object_t_0_0_0_ProfileData_t1226_0_0_0_KeyValuePair_2_t3478_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t1226_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3492_0_0_0,
	&GenInst_Int32_t372_0_0_0_VirtualButtonAbstractBehaviour_t319_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3497_0_0_0,
	&GenInst_Link_t3526_0_0_0,
	&GenInst_KeySizes_t1423_0_0_0,
	&GenInst_UInt32_t389_0_0_0,
	&GenInst_IComparable_1_t4938_0_0_0,
	&GenInst_IEquatable_1_t4943_0_0_0,
	&GenInst_BigInteger_t1428_0_0_0,
	&GenInst_X509Certificate_t1531_0_0_0,
	&GenInst_IDeserializationCallback_t2457_0_0_0,
	&GenInst_ClientCertificateType_t1535_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t384_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t3541_0_0_0,
	&GenInst_X509ChainStatus_t1662_0_0_0,
	&GenInst_Capture_t1681_0_0_0,
	&GenInst_Group_t1597_0_0_0,
	&GenInst_Mark_t1704_0_0_0,
	&GenInst_UriScheme_t1740_0_0_0,
	&GenInst_Int64_t386_0_0_0,
	&GenInst_UInt64_t394_0_0_0,
	&GenInst_SByte_t390_0_0_0,
	&GenInst_Int16_t392_0_0_0,
	&GenInst_Decimal_t395_0_0_0,
	&GenInst_Delegate_t466_0_0_0,
	&GenInst_IComparable_1_t4982_0_0_0,
	&GenInst_IEquatable_1_t4983_0_0_0,
	&GenInst_IComparable_1_t4986_0_0_0,
	&GenInst_IEquatable_1_t4987_0_0_0,
	&GenInst_IComparable_1_t4984_0_0_0,
	&GenInst_IEquatable_1_t4985_0_0_0,
	&GenInst_IComparable_1_t4980_0_0_0,
	&GenInst_IEquatable_1_t4981_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t4054_0_0_0,
	&GenInst_ConstructorInfo_t1042_0_0_0,
	&GenInst__ConstructorInfo_t4052_0_0_0,
	&GenInst_MethodBase_t1033_0_0_0,
	&GenInst__MethodBase_t4055_0_0_0,
	&GenInst_TableRange_t1802_0_0_0,
	&GenInst_TailoringInfo_t1805_0_0_0,
	&GenInst_Contraction_t1806_0_0_0,
	&GenInst_Level2Map_t1808_0_0_0,
	&GenInst_BigInteger_t1829_0_0_0,
	&GenInst_Slot_t1879_0_0_0,
	&GenInst_Slot_t1888_0_0_0,
	&GenInst_StackFrame_t1032_0_0_0,
	&GenInst_Calendar_t1902_0_0_0,
	&GenInst_ModuleBuilder_t1974_0_0_0,
	&GenInst__ModuleBuilder_t4047_0_0_0,
	&GenInst_Module_t1970_0_0_0,
	&GenInst__Module_t4057_0_0_0,
	&GenInst_ParameterBuilder_t1980_0_0_0,
	&GenInst__ParameterBuilder_t4048_0_0_0,
	&GenInst_TypeU5BU5D_t1005_0_0_0,
	&GenInst_Array_t_0_0_0,
	&GenInst_ICollection_t1751_0_0_0,
	&GenInst_IList_t346_0_0_0,
	&GenInst_ILTokenInfo_t1964_0_0_0,
	&GenInst_LabelData_t1966_0_0_0,
	&GenInst_LabelFixup_t1965_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1962_0_0_0,
	&GenInst_TypeBuilder_t1956_0_0_0,
	&GenInst__TypeBuilder_t4049_0_0_0,
	&GenInst_MethodBuilder_t1963_0_0_0,
	&GenInst__MethodBuilder_t4046_0_0_0,
	&GenInst__MethodInfo_t4056_0_0_0,
	&GenInst_ConstructorBuilder_t1954_0_0_0,
	&GenInst__ConstructorBuilder_t4042_0_0_0,
	&GenInst_FieldBuilder_t1960_0_0_0,
	&GenInst__FieldBuilder_t4044_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t4059_0_0_0,
	&GenInst_IContextProperty_t2422_0_0_0,
	&GenInst_Header_t2107_0_0_0,
	&GenInst_ITrackingHandler_t2449_0_0_0,
	&GenInst_IContextAttribute_t2436_0_0_0,
	&GenInst_IComparable_1_t5212_0_0_0,
	&GenInst_IEquatable_1_t5217_0_0_0,
	&GenInst_IComparable_1_t4988_0_0_0,
	&GenInst_IEquatable_1_t4989_0_0_0,
	&GenInst_IComparable_1_t5236_0_0_0,
	&GenInst_IEquatable_1_t5241_0_0_0,
	&GenInst_TypeTag_t2163_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_Version_t1633_0_0_0,
	&GenInst_DictionaryEntry_t451_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_KeyValuePair_2_t2507_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2507_0_0_0_KeyValuePair_2_t2507_0_0_0,
	&GenInst_Int32_t372_0_0_0_Int32_t372_0_0_0,
	&GenInst_KeyValuePair_2_t2546_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2546_0_0_0_KeyValuePair_2_t2546_0_0_0,
	&GenInst_EScreens_t188_0_0_0_EScreens_t188_0_0_0,
	&GenInst_KeyValuePair_2_t2667_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2667_0_0_0_KeyValuePair_2_t2667_0_0_0,
	&GenInst_ESubScreens_t189_0_0_0_ESubScreens_t189_0_0_0,
	&GenInst_KeyValuePair_2_t2690_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2690_0_0_0_KeyValuePair_2_t2690_0_0_0,
	&GenInst_ERaffleResult_t207_0_0_0_Object_t_0_0_0,
	&GenInst_ERaffleResult_t207_0_0_0_ERaffleResult_t207_0_0_0,
	&GenInst_KeyValuePair_2_t352_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t352_0_0_0_KeyValuePair_2_t352_0_0_0,
	&GenInst_FocusMode_t438_0_0_0_FocusMode_t438_0_0_0,
	&GenInst_Boolean_t384_0_0_0_Object_t_0_0_0,
	&GenInst_Boolean_t384_0_0_0_Boolean_t384_0_0_0,
	&GenInst_KeyValuePair_2_t2740_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2740_0_0_0_KeyValuePair_2_t2740_0_0_0,
	&GenInst_AnimatorFrame_t235_0_0_0_AnimatorFrame_t235_0_0_0,
	&GenInst_RaycastResult_t512_0_0_0_RaycastResult_t512_0_0_0,
	&GenInst_KeyValuePair_2_t2871_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2871_0_0_0_KeyValuePair_2_t2871_0_0_0,
	&GenInst_UIVertex_t605_0_0_0_UIVertex_t605_0_0_0,
	&GenInst_Vector3_t36_0_0_0_Vector3_t36_0_0_0,
	&GenInst_Color32_t711_0_0_0_Color32_t711_0_0_0,
	&GenInst_Vector2_t2_0_0_0_Vector2_t2_0_0_0,
	&GenInst_Vector4_t680_0_0_0_Vector4_t680_0_0_0,
	&GenInst_UICharInfo_t754_0_0_0_UICharInfo_t754_0_0_0,
	&GenInst_UILineInfo_t752_0_0_0_UILineInfo_t752_0_0_0,
	&GenInst_TextEditOp_t976_0_0_0_Object_t_0_0_0,
	&GenInst_TextEditOp_t976_0_0_0_TextEditOp_t976_0_0_0,
	&GenInst_KeyValuePair_2_t3183_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3183_0_0_0_KeyValuePair_2_t3183_0_0_0,
	&GenInst_PIXEL_FORMAT_t1108_0_0_0_PIXEL_FORMAT_t1108_0_0_0,
	&GenInst_KeyValuePair_2_t3257_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3257_0_0_0_KeyValuePair_2_t3257_0_0_0,
	&GenInst_UInt16_t393_0_0_0_Object_t_0_0_0,
	&GenInst_UInt16_t393_0_0_0_UInt16_t393_0_0_0,
	&GenInst_KeyValuePair_2_t3337_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3337_0_0_0_KeyValuePair_2_t3337_0_0_0,
	&GenInst_TrackableResultData_t1134_0_0_0_Object_t_0_0_0,
	&GenInst_TrackableResultData_t1134_0_0_0_TrackableResultData_t1134_0_0_0,
	&GenInst_KeyValuePair_2_t3425_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3425_0_0_0_KeyValuePair_2_t3425_0_0_0,
	&GenInst_VirtualButtonData_t1135_0_0_0_Object_t_0_0_0,
	&GenInst_VirtualButtonData_t1135_0_0_0_VirtualButtonData_t1135_0_0_0,
	&GenInst_KeyValuePair_2_t3440_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3440_0_0_0_KeyValuePair_2_t3440_0_0_0,
	&GenInst_TargetSearchResult_t1212_0_0_0_TargetSearchResult_t1212_0_0_0,
	&GenInst_ProfileData_t1226_0_0_0_Object_t_0_0_0,
	&GenInst_ProfileData_t1226_0_0_0_ProfileData_t1226_0_0_0,
	&GenInst_KeyValuePair_2_t3478_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t3478_0_0_0_KeyValuePair_2_t3478_0_0_0,
	&GenInst_CommonUtils_FindComponentThroughParent_m31364_gp_0_0_0_0,
	&GenInst_CommonUtils_GetRequiredComponent_m31365_gp_0_0_0_0,
	&GenInst_CommonUtils_GetComponent_m31366_gp_0_0_0_0,
	&GenInst_QuerySystem_Query_m31401_gp_0_0_0_0,
	&GenInst_QuerySystem_Complete_m31402_gp_0_0_0_0,
	&GenInst_QuerySystemImplementation_Query_m31403_gp_0_0_0_0,
	&GenInst_QuerySystemImplementation_Complete_m31404_gp_0_0_0_0,
	&GenInst_GameObjectExtensions_GetRequiredComponent_m31409_gp_0_0_0_0,
	&GenInst_UnityUtils_GetRequiredComponent_m31410_gp_0_0_0_0,
	&GenInst_Factory_Get_m31413_gp_0_0_0_0,
	&GenInst_ImmutableList_1_t3945_gp_0_0_0_0,
	&GenInst_Pool_1_t3946_gp_0_0_0_0,
	&GenInst_SimpleList_1_t3948_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator4_t3950_gp_0_0_0_0,
	&GenInst_DispatcherBase_Dispatch_m31458_gp_0_0_0_0,
	&GenInst_Dispatcher_CreateSafeFunction_m31460_gp_0_0_0_0,
	&GenInst_Dispatcher_CreateSafeAction_m31461_gp_0_0_0_0,
	&GenInst_U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t3951_gp_0_0_0_0,
	&GenInst_Task_1_t3953_gp_0_0_0_0,
	&GenInst_Task_1_WaitForSeconds_m31473_gp_0_0_0_0,
	&GenInst_ThreadBase_Dispatch_m31476_gp_0_0_0_0,
	&GenInst_ThreadBase_DispatchAndWait_m31477_gp_0_0_0_0,
	&GenInst_ThreadBase_DispatchAndWait_m31478_gp_0_0_0_0,
	&GenInst_ExecuteEvents_Execute_m31506_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m31507_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m31509_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m31510_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m31511_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t3956_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m31536_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t3960_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t3960_gp_0_0_0_0_Int32_t372_0_0_0,
	&GenInst_ListPool_1_t3961_gp_0_0_0_0,
	&GenInst_List_1_t5307_0_0_0,
	&GenInst_ObjectPool_1_t3962_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m31602_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m31606_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m31607_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m31608_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m31609_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m31611_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m31612_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m31616_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m31618_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m31619_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m31620_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t3970_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3971_gp_0_0_0_0_InvokableCall_2_t3971_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t3971_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3971_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3972_gp_0_0_0_0_InvokableCall_3_t3972_gp_1_0_0_0_InvokableCall_3_t3972_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3972_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3972_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3972_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3973_gp_0_0_0_0_InvokableCall_4_t3973_gp_1_0_0_0_InvokableCall_4_t3973_gp_2_0_0_0_InvokableCall_4_t3973_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t3973_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t3973_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t3973_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3973_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t1043_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t3974_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t3975_gp_0_0_0_0_UnityEvent_2_t3975_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t3976_gp_0_0_0_0_UnityEvent_3_t3976_gp_1_0_0_0_UnityEvent_3_t3976_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t3977_gp_0_0_0_0_UnityEvent_4_t3977_gp_1_0_0_0_UnityEvent_4_t3977_gp_2_0_0_0_UnityEvent_4_t3977_gp_3_0_0_0,
	&GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m32065_gp_0_0_0_0,
	&GenInst_HashSet_1_t3986_gp_0_0_0_0,
	&GenInst_Enumerator_t3988_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3989_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m32350_gp_0_0_0_0,
	&GenInst_Enumerable_Cast_m32351_gp_0_0_0_0,
	&GenInst_Enumerable_CreateCastIterator_m32352_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m32353_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m32354_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m32355_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m32355_gp_0_0_0_0_Boolean_t384_0_0_0,
	&GenInst_Enumerable_Empty_m32356_gp_0_0_0_0,
	&GenInst_Enumerable_First_m32357_gp_0_0_0_0,
	&GenInst_Enumerable_First_m32357_gp_0_0_0_0_Boolean_t384_0_0_0,
	&GenInst_Enumerable_First_m32358_gp_0_0_0_0,
	&GenInst_Enumerable_First_m32359_gp_0_0_0_0,
	&GenInst_Enumerable_First_m32359_gp_0_0_0_0_Boolean_t384_0_0_0,
	&GenInst_Enumerable_Reverse_m32360_gp_0_0_0_0,
	&GenInst_Enumerable_CreateReverseIterator_m32361_gp_0_0_0_0,
	&GenInst_Enumerable_Take_m32362_gp_0_0_0_0,
	&GenInst_Enumerable_CreateTakeIterator_m32363_gp_0_0_0_0,
	&GenInst_Enumerable_ToArray_m32364_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m32365_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m32366_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m32366_gp_0_0_0_0_Boolean_t384_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m32367_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m32367_gp_0_0_0_0_Boolean_t384_0_0_0,
	&GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t3990_gp_0_0_0_0,
	&GenInst_U3CCreateReverseIteratorU3Ec__IteratorF_1_t3991_gp_0_0_0_0,
	&GenInst_U3CCreateTakeIteratorU3Ec__Iterator19_1_t3992_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3993_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3993_gp_0_0_0_0_Boolean_t384_0_0_0,
	&GenInst_LinkedList_1_t3996_gp_0_0_0_0,
	&GenInst_Enumerator_t3997_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t3998_gp_0_0_0_0,
	&GenInst_Queue_1_t3999_gp_0_0_0_0,
	&GenInst_Enumerator_t4000_gp_0_0_0_0,
	&GenInst_Stack_1_t4001_gp_0_0_0_0,
	&GenInst_Enumerator_t4002_gp_0_0_0_0,
	&GenInst_IEnumerable_1_t4009_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m32563_gp_0_0_0_0,
	&GenInst_Array_Sort_m32575_gp_0_0_0_0_Array_Sort_m32575_gp_0_0_0_0,
	&GenInst_Array_Sort_m32576_gp_0_0_0_0_Array_Sort_m32576_gp_1_0_0_0,
	&GenInst_Array_Sort_m32577_gp_0_0_0_0,
	&GenInst_Array_Sort_m32577_gp_0_0_0_0_Array_Sort_m32577_gp_0_0_0_0,
	&GenInst_Array_Sort_m32578_gp_0_0_0_0,
	&GenInst_Array_Sort_m32578_gp_0_0_0_0_Array_Sort_m32578_gp_1_0_0_0,
	&GenInst_Array_Sort_m32579_gp_0_0_0_0_Array_Sort_m32579_gp_0_0_0_0,
	&GenInst_Array_Sort_m32580_gp_0_0_0_0_Array_Sort_m32580_gp_1_0_0_0,
	&GenInst_Array_Sort_m32581_gp_0_0_0_0,
	&GenInst_Array_Sort_m32581_gp_0_0_0_0_Array_Sort_m32581_gp_0_0_0_0,
	&GenInst_Array_Sort_m32582_gp_0_0_0_0,
	&GenInst_Array_Sort_m32582_gp_1_0_0_0,
	&GenInst_Array_Sort_m32582_gp_0_0_0_0_Array_Sort_m32582_gp_1_0_0_0,
	&GenInst_Array_Sort_m32583_gp_0_0_0_0,
	&GenInst_Array_Sort_m32584_gp_0_0_0_0,
	&GenInst_Array_qsort_m32585_gp_0_0_0_0,
	&GenInst_Array_qsort_m32585_gp_0_0_0_0_Array_qsort_m32585_gp_1_0_0_0,
	&GenInst_Array_compare_m32586_gp_0_0_0_0,
	&GenInst_Array_qsort_m32587_gp_0_0_0_0,
	&GenInst_Array_Resize_m32590_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m32592_gp_0_0_0_0,
	&GenInst_Array_ForEach_m32593_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m32594_gp_0_0_0_0_Array_ConvertAll_m32594_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m32595_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m32596_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m32597_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m32598_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m32599_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m32600_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m32601_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m32602_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m32603_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m32604_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m32605_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m32606_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m32607_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m32608_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m32609_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m32610_gp_0_0_0_0,
	&GenInst_Array_FindAll_m32611_gp_0_0_0_0,
	&GenInst_Array_Exists_m32612_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m32613_gp_0_0_0_0,
	&GenInst_Array_Find_m32614_gp_0_0_0_0,
	&GenInst_Array_FindLast_m32615_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t4010_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t4011_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4012_gp_0_0_0_0,
	&GenInst_IList_1_t4013_gp_0_0_0_0,
	&GenInst_ICollection_1_t4014_gp_0_0_0_0,
	&GenInst_Nullable_1_t2433_gp_0_0_0_0,
	&GenInst_Comparer_1_t4021_gp_0_0_0_0,
	&GenInst_DefaultComparer_t4022_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t3939_gp_0_0_0_0,
	&GenInst_Dictionary_2_t4023_gp_0_0_0_0,
	&GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t5474_0_0_0,
	&GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m32763_gp_0_0_0_0,
	&GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m32768_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m32768_gp_0_0_0_0_Object_t_0_0_0,
	&GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_DictionaryEntry_t451_0_0_0,
	&GenInst_ShimEnumerator_t4024_gp_0_0_0_0_ShimEnumerator_t4024_gp_1_0_0_0,
	&GenInst_Enumerator_t4025_gp_0_0_0_0_Enumerator_t4025_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t5488_0_0_0,
	&GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_1_0_0_0,
	&GenInst_KeyCollection_t4026_gp_0_0_0_0,
	&GenInst_Enumerator_t4027_gp_0_0_0_0_Enumerator_t4027_gp_1_0_0_0,
	&GenInst_Enumerator_t4027_gp_0_0_0_0,
	&GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_1_0_0_0_KeyCollection_t4026_gp_0_0_0_0,
	&GenInst_KeyCollection_t4026_gp_0_0_0_0_KeyCollection_t4026_gp_0_0_0_0,
	&GenInst_ValueCollection_t4028_gp_0_0_0_0_ValueCollection_t4028_gp_1_0_0_0,
	&GenInst_ValueCollection_t4028_gp_1_0_0_0,
	&GenInst_Enumerator_t4029_gp_0_0_0_0_Enumerator_t4029_gp_1_0_0_0,
	&GenInst_Enumerator_t4029_gp_1_0_0_0,
	&GenInst_ValueCollection_t4028_gp_0_0_0_0_ValueCollection_t4028_gp_1_0_0_0_ValueCollection_t4028_gp_1_0_0_0,
	&GenInst_ValueCollection_t4028_gp_1_0_0_0_ValueCollection_t4028_gp_1_0_0_0,
	&GenInst_Dictionary_2_t4023_gp_0_0_0_0_Dictionary_2_t4023_gp_1_0_0_0_KeyValuePair_2_t5474_0_0_0,
	&GenInst_KeyValuePair_2_t5474_0_0_0_KeyValuePair_2_t5474_0_0_0,
	&GenInst_Dictionary_2_t4023_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t4031_gp_0_0_0_0,
	&GenInst_DefaultComparer_t4032_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t3938_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t5524_0_0_0,
	&GenInst_IDictionary_2_t4034_gp_0_0_0_0_IDictionary_2_t4034_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4036_gp_0_0_0_0_KeyValuePair_2_t4036_gp_1_0_0_0,
	&GenInst_List_1_t4037_gp_0_0_0_0,
	&GenInst_Enumerator_t4038_gp_0_0_0_0,
	&GenInst_Collection_1_t4039_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t4040_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m33045_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m33045_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m33046_gp_0_0_0_0,
};
