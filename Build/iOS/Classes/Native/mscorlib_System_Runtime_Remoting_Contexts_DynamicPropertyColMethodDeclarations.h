﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Contexts.DynamicPropertyCollection/DynamicPropertyReg
struct DynamicPropertyReg_t2078;

// System.Void System.Runtime.Remoting.Contexts.DynamicPropertyCollection/DynamicPropertyReg::.ctor()
extern "C" void DynamicPropertyReg__ctor_m12397 (DynamicPropertyReg_t2078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
