﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
#include "mscorlib_System_Array.h"

// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviourMethodDeclarations.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
#include "mscorlib_ArrayTypes.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Vuforia.VuforiaManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManager.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// Vuforia.VuforiaManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
struct CameraU5BU5D_t471;
struct ObjectU5BU5D_t356;
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t356* GameObject_GetComponentsInChildren_TisObject_t_m1355_gshared (GameObject_t155 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisObject_t_m1355(__this, method) (( ObjectU5BU5D_t356* (*) (GameObject_t155 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m1355_gshared)(__this, method)
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
#define GameObject_GetComponentsInChildren_TisCamera_t6_m1891(__this, method) (( CameraU5BU5D_t471* (*) (GameObject_t155 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m1355_gshared)(__this, method)
struct MeshFilter_t398;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m1326_gshared (Component_t365 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m1326(__this, method) (( Object_t * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t398_m1502(__this, method) (( MeshFilter_t398 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void Vuforia.WireframeBehaviour::.ctor()
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void WireframeBehaviour__ctor_m1275 (WireframeBehaviour_t324 * __this, const MethodInfo* method)
{
	{
		__this->___ShowLines_3 = 1;
		Color_t9  L_0 = Color_get_green_m1887(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___LineColor_4 = L_0;
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t388_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Material_t82_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral386;
extern Il2CppCodeGenString* _stringLiteral387;
extern Il2CppCodeGenString* _stringLiteral388;
extern "C" void WireframeBehaviour_CreateLineMaterial_m1276 (WireframeBehaviour_t324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Single_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Material_t82_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		_stringLiteral386 = il2cpp_codegen_string_literal_from_index(386);
		_stringLiteral387 = il2cpp_codegen_string_literal_from_index(387);
		_stringLiteral388 = il2cpp_codegen_string_literal_from_index(388);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t356* L_0 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, ((int32_t)9)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral386);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral386;
		ObjectU5BU5D_t356* L_1 = L_0;
		Color_t9 * L_2 = &(__this->___LineColor_4);
		float L_3 = (L_2->___r_0);
		float L_4 = L_3;
		Object_t * L_5 = Box(Single_t388_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t356* L_6 = L_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral387);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral387;
		ObjectU5BU5D_t356* L_7 = L_6;
		Color_t9 * L_8 = &(__this->___LineColor_4);
		float L_9 = (L_8->___g_1);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t388_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t356* L_12 = L_7;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral387);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral387;
		ObjectU5BU5D_t356* L_13 = L_12;
		Color_t9 * L_14 = &(__this->___LineColor_4);
		float L_15 = (L_14->___b_2);
		float L_16 = L_15;
		Object_t * L_17 = Box(Single_t388_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 5, sizeof(Object_t *))) = (Object_t *)L_17;
		ObjectU5BU5D_t356* L_18 = L_13;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		ArrayElementTypeCheck (L_18, _stringLiteral387);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral387;
		ObjectU5BU5D_t356* L_19 = L_18;
		Color_t9 * L_20 = &(__this->___LineColor_4);
		float L_21 = (L_20->___a_3);
		float L_22 = L_21;
		Object_t * L_23 = Box(Single_t388_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 7, sizeof(Object_t *))) = (Object_t *)L_23;
		ObjectU5BU5D_t356* L_24 = L_19;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 8);
		ArrayElementTypeCheck (L_24, _stringLiteral388);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 8, sizeof(Object_t *))) = (Object_t *)_stringLiteral388;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m1316(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Material_t82 * L_26 = (Material_t82 *)il2cpp_codegen_object_new (Material_t82_il2cpp_TypeInfo_var);
		Material__ctor_m1888(L_26, L_25, /*hidden argument*/NULL);
		__this->___mLineMaterial_2 = L_26;
		Material_t82 * L_27 = (__this->___mLineMaterial_2);
		NullCheck(L_27);
		Object_set_hideFlags_m1619(L_27, ((int32_t)61), /*hidden argument*/NULL);
		Material_t82 * L_28 = (__this->___mLineMaterial_2);
		NullCheck(L_28);
		Shader_t407 * L_29 = Material_get_shader_m1889(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Object_set_hideFlags_m1619(L_29, ((int32_t)61), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
// Vuforia.VuforiaManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviourMethodDeclarations.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern TypeInfo* VuforiaManager_t472_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisCamera_t6_m1891_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t398_m1502_MethodInfo_var;
extern "C" void WireframeBehaviour_OnRenderObject_m1277 (WireframeBehaviour_t324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaManager_t472_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(312);
		GameObject_GetComponentsInChildren_TisCamera_t6_m1891_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483932);
		Component_GetComponent_TisMeshFilter_t398_m1502_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483691);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t155 * V_0 = {0};
	CameraU5BU5D_t471* V_1 = {0};
	bool V_2 = false;
	Camera_t6 * V_3 = {0};
	CameraU5BU5D_t471* V_4 = {0};
	int32_t V_5 = 0;
	MeshFilter_t398 * V_6 = {0};
	Mesh_t104 * V_7 = {0};
	Vector3U5BU5D_t254* V_8 = {0};
	Int32U5BU5D_t401* V_9 = {0};
	int32_t V_10 = 0;
	Vector3_t36  V_11 = {0};
	Vector3_t36  V_12 = {0};
	Vector3_t36  V_13 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t472_il2cpp_TypeInfo_var);
		VuforiaManager_t472 * L_0 = VuforiaManager_get_Instance_m1890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t35 * L_1 = (Transform_t35 *)VirtFuncInvoker0< Transform_t35 * >::Invoke(8 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t155 * L_2 = Component_get_gameObject_m1337(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t155 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t471* L_4 = GameObject_GetComponentsInChildren_TisCamera_t6_m1891(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t6_m1891_MethodInfo_var);
		V_1 = L_4;
		V_2 = 0;
		CameraU5BU5D_t471* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t471* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_3 = (*(Camera_t6 **)(Camera_t6 **)SZArrayLdElema(L_6, L_8, sizeof(Camera_t6 *)));
		Camera_t6 * L_9 = Camera_get_current_m1892(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t6 * L_10 = V_3;
		bool L_11 = Object_op_Equality_m1413(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = 1;
	}

IL_003c:
	{
		int32_t L_12 = V_5;
		V_5 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_13 = V_5;
		CameraU5BU5D_t471* L_14 = V_4;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)(((Array_t *)L_14)->max_length))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_15 = V_2;
		if (L_15)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_16 = (__this->___ShowLines_3);
		if (L_16)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t398 * L_17 = Component_GetComponent_TisMeshFilter_t398_m1502(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t398_m1502_MethodInfo_var);
		V_6 = L_17;
		MeshFilter_t398 * L_18 = V_6;
		bool L_19 = Object_op_Implicit_m1320(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t82 * L_20 = (__this->___mLineMaterial_2);
		bool L_21 = Object_op_Equality_m1413(NULL /*static, unused*/, L_20, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_008c;
		}
	}
	{
		WireframeBehaviour_CreateLineMaterial_m1276(__this, /*hidden argument*/NULL);
	}

IL_008c:
	{
		MeshFilter_t398 * L_22 = V_6;
		NullCheck(L_22);
		Mesh_t104 * L_23 = MeshFilter_get_sharedMesh_m1893(L_22, /*hidden argument*/NULL);
		V_7 = L_23;
		Mesh_t104 * L_24 = V_7;
		NullCheck(L_24);
		Vector3U5BU5D_t254* L_25 = Mesh_get_vertices_m1465(L_24, /*hidden argument*/NULL);
		V_8 = L_25;
		Mesh_t104 * L_26 = V_7;
		NullCheck(L_26);
		Int32U5BU5D_t401* L_27 = Mesh_get_triangles_m1467(L_26, /*hidden argument*/NULL);
		V_9 = L_27;
		GL_PushMatrix_m1894(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t35 * L_28 = Component_get_transform_m1417(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Matrix4x4_t404  L_29 = Transform_get_localToWorldMatrix_m1895(L_28, /*hidden argument*/NULL);
		GL_MultMatrix_m1896(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		Material_t82 * L_30 = (__this->___mLineMaterial_2);
		NullCheck(L_30);
		Material_SetPass_m1897(L_30, 0, /*hidden argument*/NULL);
		GL_Begin_m1898(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_0144;
	}

IL_00d7:
	{
		Vector3U5BU5D_t254* L_31 = V_8;
		Int32U5BU5D_t401* L_32 = V_9;
		int32_t L_33 = V_10;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		int32_t L_34 = L_33;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, (*(int32_t*)(int32_t*)SZArrayLdElema(L_32, L_34, sizeof(int32_t))));
		V_11 = (*(Vector3_t36 *)((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_31, (*(int32_t*)(int32_t*)SZArrayLdElema(L_32, L_34, sizeof(int32_t))), sizeof(Vector3_t36 ))));
		Vector3U5BU5D_t254* L_35 = V_8;
		Int32U5BU5D_t401* L_36 = V_9;
		int32_t L_37 = V_10;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)((int32_t)L_37+(int32_t)1)));
		int32_t L_38 = ((int32_t)((int32_t)L_37+(int32_t)1));
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, (*(int32_t*)(int32_t*)SZArrayLdElema(L_36, L_38, sizeof(int32_t))));
		V_12 = (*(Vector3_t36 *)((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_35, (*(int32_t*)(int32_t*)SZArrayLdElema(L_36, L_38, sizeof(int32_t))), sizeof(Vector3_t36 ))));
		Vector3U5BU5D_t254* L_39 = V_8;
		Int32U5BU5D_t401* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)((int32_t)L_41+(int32_t)2)));
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)2));
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, (*(int32_t*)(int32_t*)SZArrayLdElema(L_40, L_42, sizeof(int32_t))));
		V_13 = (*(Vector3_t36 *)((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_39, (*(int32_t*)(int32_t*)SZArrayLdElema(L_40, L_42, sizeof(int32_t))), sizeof(Vector3_t36 ))));
		Vector3_t36  L_43 = V_11;
		GL_Vertex_m1899(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		Vector3_t36  L_44 = V_12;
		GL_Vertex_m1899(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		Vector3_t36  L_45 = V_12;
		GL_Vertex_m1899(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Vector3_t36  L_46 = V_13;
		GL_Vertex_m1899(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		Vector3_t36  L_47 = V_13;
		GL_Vertex_m1899(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		Vector3_t36  L_48 = V_11;
		GL_Vertex_m1899(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		int32_t L_49 = V_10;
		V_10 = ((int32_t)((int32_t)L_49+(int32_t)3));
	}

IL_0144:
	{
		int32_t L_50 = V_10;
		Int32U5BU5D_t401* L_51 = V_9;
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)(((int32_t)(((Array_t *)L_51)->max_length))))))
		{
			goto IL_00d7;
		}
	}
	{
		GL_End_m1900(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m1901(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t398_m1502_MethodInfo_var;
extern "C" void WireframeBehaviour_OnDrawGizmos_m1278 (WireframeBehaviour_t324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisMeshFilter_t398_m1502_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483691);
		s_Il2CppMethodIntialized = true;
	}
	MeshFilter_t398 * V_0 = {0};
	Mesh_t104 * V_1 = {0};
	Vector3U5BU5D_t254* V_2 = {0};
	Int32U5BU5D_t401* V_3 = {0};
	int32_t V_4 = 0;
	Vector3_t36  V_5 = {0};
	Vector3_t36  V_6 = {0};
	Vector3_t36  V_7 = {0};
	{
		bool L_0 = (__this->___ShowLines_3);
		if (!L_0)
		{
			goto IL_00ed;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m1902(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ed;
		}
	}
	{
		MeshFilter_t398 * L_2 = Component_GetComponent_TisMeshFilter_t398_m1502(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t398_m1502_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t398 * L_3 = V_0;
		bool L_4 = Object_op_Implicit_m1320(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t155 * L_5 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t35 * L_6 = GameObject_get_transform_m1349(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t36  L_7 = Transform_get_position_m1388(L_6, /*hidden argument*/NULL);
		GameObject_t155 * L_8 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t35 * L_9 = GameObject_get_transform_m1349(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t41  L_10 = Transform_get_rotation_m1903(L_9, /*hidden argument*/NULL);
		GameObject_t155 * L_11 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t35 * L_12 = GameObject_get_transform_m1349(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t36  L_13 = Transform_get_lossyScale_m1904(L_12, /*hidden argument*/NULL);
		Matrix4x4_t404  L_14 = Matrix4x4_TRS_m1905(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1906(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t9  L_15 = (__this->___LineColor_4);
		Gizmos_set_color_m1792(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t398 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t104 * L_17 = MeshFilter_get_sharedMesh_m1893(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t104 * L_18 = V_1;
		NullCheck(L_18);
		Vector3U5BU5D_t254* L_19 = Mesh_get_vertices_m1465(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		Mesh_t104 * L_20 = V_1;
		NullCheck(L_20);
		Int32U5BU5D_t401* L_21 = Mesh_get_triangles_m1467(L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		V_4 = 0;
		goto IL_00e3;
	}

IL_008b:
	{
		Vector3U5BU5D_t254* L_22 = V_2;
		Int32U5BU5D_t401* L_23 = V_3;
		int32_t L_24 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, (*(int32_t*)(int32_t*)SZArrayLdElema(L_23, L_25, sizeof(int32_t))));
		V_5 = (*(Vector3_t36 *)((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_22, (*(int32_t*)(int32_t*)SZArrayLdElema(L_23, L_25, sizeof(int32_t))), sizeof(Vector3_t36 ))));
		Vector3U5BU5D_t254* L_26 = V_2;
		Int32U5BU5D_t401* L_27 = V_3;
		int32_t L_28 = V_4;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)L_28+(int32_t)1)));
		int32_t L_29 = ((int32_t)((int32_t)L_28+(int32_t)1));
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, (*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29, sizeof(int32_t))));
		V_6 = (*(Vector3_t36 *)((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_26, (*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29, sizeof(int32_t))), sizeof(Vector3_t36 ))));
		Vector3U5BU5D_t254* L_30 = V_2;
		Int32U5BU5D_t401* L_31 = V_3;
		int32_t L_32 = V_4;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)L_32+(int32_t)2)));
		int32_t L_33 = ((int32_t)((int32_t)L_32+(int32_t)2));
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, (*(int32_t*)(int32_t*)SZArrayLdElema(L_31, L_33, sizeof(int32_t))));
		V_7 = (*(Vector3_t36 *)((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_30, (*(int32_t*)(int32_t*)SZArrayLdElema(L_31, L_33, sizeof(int32_t))), sizeof(Vector3_t36 ))));
		Vector3_t36  L_34 = V_5;
		Vector3_t36  L_35 = V_6;
		Gizmos_DrawLine_m1793(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		Vector3_t36  L_36 = V_6;
		Vector3_t36  L_37 = V_7;
		Gizmos_DrawLine_m1793(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		Vector3_t36  L_38 = V_7;
		Vector3_t36  L_39 = V_5;
		Gizmos_DrawLine_m1793(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		int32_t L_40 = V_4;
		V_4 = ((int32_t)((int32_t)L_40+(int32_t)3));
	}

IL_00e3:
	{
		int32_t L_41 = V_4;
		Int32U5BU5D_t401* L_42 = V_3;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)(((Array_t *)L_42)->max_length))))))
		{
			goto IL_008b;
		}
	}

IL_00ed:
	{
		return;
	}
}
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandlerMethodDeclarations.h"
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
#include "Assembly-CSharp_ArrayTypes.h"
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviourMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
struct TrackableBehaviour_t211;
// Declaration !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t211_m1679(__this, method) (( TrackableBehaviour_t211 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
struct RendererU5BU5D_t366;
struct ObjectU5BU5D_t356;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C" ObjectU5BU5D_t356* Component_GetComponentsInChildren_TisObject_t_m1830_gshared (Component_t365 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m1830(__this, p0, method) (( ObjectU5BU5D_t356* (*) (Component_t365 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m1830_gshared)(__this, p0, method)
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t367_m1828(__this, p0, method) (( RendererU5BU5D_t366* (*) (Component_t365 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m1830_gshared)(__this, p0, method)
struct ColliderU5BU5D_t59;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t369_m1829(__this, p0, method) (( ColliderU5BU5D_t59* (*) (Component_t365 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m1830_gshared)(__this, p0, method)
struct WireframeBehaviourU5BU5D_t473;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
#define Component_GetComponentsInChildren_TisWireframeBehaviour_t324_m1907(__this, p0, method) (( WireframeBehaviourU5BU5D_t473* (*) (Component_t365 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m1830_gshared)(__this, p0, method)
// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void WireframeTrackableEventHandler__ctor_m1279 (WireframeTrackableEventHandler_t325 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviourMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t211_m1679_MethodInfo_var;
extern "C" void WireframeTrackableEventHandler_Start_m1280 (WireframeTrackableEventHandler_t325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisTrackableBehaviour_t211_m1679_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483802);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t211 * L_0 = Component_GetComponent_TisTrackableBehaviour_t211_m1679(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t211_m1679_MethodInfo_var);
		__this->___mTrackableBehaviour_2 = L_0;
		TrackableBehaviour_t211 * L_1 = (__this->___mTrackableBehaviour_2);
		bool L_2 = Object_op_Implicit_m1320(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t211 * L_3 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1680(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandlerMethodDeclarations.h"
extern "C" void WireframeTrackableEventHandler_OnTrackableStateChanged_m1281 (WireframeTrackableEventHandler_t325 * __this, int32_t ___previousStatus, int32_t ___newStatus, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___newStatus;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_OnTrackingFound_m1282(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_OnTrackingLost_m1283(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t367_m1828_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t369_m1829_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t324_m1907_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral380;
extern Il2CppCodeGenString* _stringLiteral381;
extern "C" void WireframeTrackableEventHandler_OnTrackingFound_m1282 (WireframeTrackableEventHandler_t325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Component_GetComponentsInChildren_TisRenderer_t367_m1828_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483912);
		Component_GetComponentsInChildren_TisCollider_t369_m1829_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483913);
		Component_GetComponentsInChildren_TisWireframeBehaviour_t324_m1907_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		_stringLiteral380 = il2cpp_codegen_string_literal_from_index(380);
		_stringLiteral381 = il2cpp_codegen_string_literal_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t366* V_0 = {0};
	ColliderU5BU5D_t59* V_1 = {0};
	WireframeBehaviourU5BU5D_t473* V_2 = {0};
	Renderer_t367 * V_3 = {0};
	RendererU5BU5D_t366* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t369 * V_6 = {0};
	ColliderU5BU5D_t59* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t324 * V_9 = {0};
	WireframeBehaviourU5BU5D_t473* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t366* L_0 = Component_GetComponentsInChildren_TisRenderer_t367_m1828(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t367_m1828_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t59* L_1 = Component_GetComponentsInChildren_TisCollider_t369_m1829(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t369_m1829_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t473* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t324_m1907(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t324_m1907_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t366* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t366* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = (*(Renderer_t367 **)(Renderer_t367 **)SZArrayLdElema(L_4, L_6, sizeof(Renderer_t367 *)));
		Renderer_t367 * L_7 = V_3;
		NullCheck(L_7);
		Renderer_set_enabled_m1335(L_7, 1, /*hidden argument*/NULL);
		int32_t L_8 = V_5;
		V_5 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_5;
		RendererU5BU5D_t366* L_10 = V_4;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t59* L_11 = V_1;
		V_7 = L_11;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t59* L_12 = V_7;
		int32_t L_13 = V_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_6 = (*(Collider_t369 **)(Collider_t369 **)SZArrayLdElema(L_12, L_14, sizeof(Collider_t369 *)));
		Collider_t369 * L_15 = V_6;
		NullCheck(L_15);
		Collider_set_enabled_m1348(L_15, 1, /*hidden argument*/NULL);
		int32_t L_16 = V_8;
		V_8 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_17 = V_8;
		ColliderU5BU5D_t59* L_18 = V_7;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)(((Array_t *)L_18)->max_length))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t473* L_19 = V_2;
		V_10 = L_19;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t473* L_20 = V_10;
		int32_t L_21 = V_11;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_9 = (*(WireframeBehaviour_t324 **)(WireframeBehaviour_t324 **)SZArrayLdElema(L_20, L_22, sizeof(WireframeBehaviour_t324 *)));
		WireframeBehaviour_t324 * L_23 = V_9;
		NullCheck(L_23);
		Behaviour_set_enabled_m1346(L_23, 1, /*hidden argument*/NULL);
		int32_t L_24 = V_11;
		V_11 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_25 = V_11;
		WireframeBehaviourU5BU5D_t473* L_26 = V_10;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)(((Array_t *)L_26)->max_length))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t211 * L_27 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_27);
		String_t* L_28 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m1575(NULL /*static, unused*/, _stringLiteral380, L_28, _stringLiteral381, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t367_m1828_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t369_m1829_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t324_m1907_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral380;
extern Il2CppCodeGenString* _stringLiteral382;
extern "C" void WireframeTrackableEventHandler_OnTrackingLost_m1283 (WireframeTrackableEventHandler_t325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Component_GetComponentsInChildren_TisRenderer_t367_m1828_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483912);
		Component_GetComponentsInChildren_TisCollider_t369_m1829_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483913);
		Component_GetComponentsInChildren_TisWireframeBehaviour_t324_m1907_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		_stringLiteral380 = il2cpp_codegen_string_literal_from_index(380);
		_stringLiteral382 = il2cpp_codegen_string_literal_from_index(382);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t366* V_0 = {0};
	ColliderU5BU5D_t59* V_1 = {0};
	WireframeBehaviourU5BU5D_t473* V_2 = {0};
	Renderer_t367 * V_3 = {0};
	RendererU5BU5D_t366* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t369 * V_6 = {0};
	ColliderU5BU5D_t59* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t324 * V_9 = {0};
	WireframeBehaviourU5BU5D_t473* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t366* L_0 = Component_GetComponentsInChildren_TisRenderer_t367_m1828(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t367_m1828_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t59* L_1 = Component_GetComponentsInChildren_TisCollider_t369_m1829(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t369_m1829_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t473* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t324_m1907(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t324_m1907_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t366* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t366* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = (*(Renderer_t367 **)(Renderer_t367 **)SZArrayLdElema(L_4, L_6, sizeof(Renderer_t367 *)));
		Renderer_t367 * L_7 = V_3;
		NullCheck(L_7);
		Renderer_set_enabled_m1335(L_7, 0, /*hidden argument*/NULL);
		int32_t L_8 = V_5;
		V_5 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_5;
		RendererU5BU5D_t366* L_10 = V_4;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t59* L_11 = V_1;
		V_7 = L_11;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t59* L_12 = V_7;
		int32_t L_13 = V_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_6 = (*(Collider_t369 **)(Collider_t369 **)SZArrayLdElema(L_12, L_14, sizeof(Collider_t369 *)));
		Collider_t369 * L_15 = V_6;
		NullCheck(L_15);
		Collider_set_enabled_m1348(L_15, 0, /*hidden argument*/NULL);
		int32_t L_16 = V_8;
		V_8 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_17 = V_8;
		ColliderU5BU5D_t59* L_18 = V_7;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)(((Array_t *)L_18)->max_length))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t473* L_19 = V_2;
		V_10 = L_19;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t473* L_20 = V_10;
		int32_t L_21 = V_11;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_9 = (*(WireframeBehaviour_t324 **)(WireframeBehaviour_t324 **)SZArrayLdElema(L_20, L_22, sizeof(WireframeBehaviour_t324 *)));
		WireframeBehaviour_t324 * L_23 = V_9;
		NullCheck(L_23);
		Behaviour_set_enabled_m1346(L_23, 0, /*hidden argument*/NULL);
		int32_t L_24 = V_11;
		V_11 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_25 = V_11;
		WireframeBehaviourU5BU5D_t473* L_26 = V_10;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)(((Array_t *)L_26)->max_length))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t211 * L_27 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_27);
		String_t* L_28 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m1575(NULL /*static, unused*/, _stringLiteral380, L_28, _stringLiteral382, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviourMethodDeclarations.h"
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavioMethodDeclarations.h"
// System.Void Vuforia.WordBehaviour::.ctor()
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavioMethodDeclarations.h"
extern "C" void WordBehaviour__ctor_m1284 (WordBehaviour_t326 * __this, const MethodInfo* method)
{
	{
		WordAbstractBehaviour__ctor_m1908(__this, /*hidden argument*/NULL);
		return;
	}
}
// Common.Query.QueryResultResolver
#include "AssemblyU2DCSharp_Common_Query_QueryResultResolver.h"
// Common.Query.QueryResultResolver
#include "AssemblyU2DCSharp_Common_Query_QueryResultResolverMethodDeclarations.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
// System.Void Common.Query.QueryResultResolver::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void QueryResultResolver__ctor_m1285 (QueryResultResolver_t328 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void Common.Query.QueryResultResolver::Invoke(Common.Query.IQueryRequest,Common.Query.IMutableQueryResult)
extern "C" void QueryResultResolver_Invoke_m1286 (QueryResultResolver_t328 * __this, Object_t * ___request, Object_t * ___result, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		QueryResultResolver_Invoke_m1286((QueryResultResolver_t328 *)__this->___prev_9,___request, ___result, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___request, Object_t * ___result, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___request, ___result,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___request, Object_t * ___result, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___request, ___result,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___result, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___request, ___result,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_QueryResultResolver_t328(Il2CppObject* delegate, Object_t * ___request, Object_t * ___result)
{
	// Marshaling of parameter '___request' to native representation
	Object_t * ____request_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'Common.Query.IQueryRequest'."));
}
// System.IAsyncResult Common.Query.QueryResultResolver::BeginInvoke(Common.Query.IQueryRequest,Common.Query.IMutableQueryResult,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * QueryResultResolver_BeginInvoke_m1287 (QueryResultResolver_t328 * __this, Object_t * ___request, Object_t * ___result, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___request;
	__d_args[1] = ___result;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void Common.Query.QueryResultResolver::EndInvoke(System.IAsyncResult)
extern "C" void QueryResultResolver_EndInvoke_m1288 (QueryResultResolver_t328 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// ListenRaffle
#include "AssemblyU2DCSharp_ListenRaffle.h"
// ListenRaffle
#include "AssemblyU2DCSharp_ListenRaffleMethodDeclarations.h"
// System.Void ListenRaffle::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void ListenRaffle__ctor_m1289 (ListenRaffle_t331 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void ListenRaffle::Invoke(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void ListenRaffle_Invoke_m1290 (ListenRaffle_t331 * __this, int32_t ___raffleId, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ListenRaffle_Invoke_m1290((ListenRaffle_t331 *)__this->___prev_9,___raffleId, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___raffleId, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___raffleId,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___raffleId, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___raffleId,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ListenRaffle_t331(Il2CppObject* delegate, int32_t ___raffleId)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___raffleId' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___raffleId);

	// Marshaling cleanup of parameter '___raffleId' native representation

}
// System.IAsyncResult ListenRaffle::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern "C" Object_t * ListenRaffle_BeginInvoke_m1291 (ListenRaffle_t331 * __this, int32_t ___raffleId, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t372_il2cpp_TypeInfo_var, &___raffleId);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void ListenRaffle::EndInvoke(System.IAsyncResult)
extern "C" void ListenRaffle_EndInvoke_m1292 (ListenRaffle_t331 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// ListenRaffleResult
#include "AssemblyU2DCSharp_ListenRaffleResult.h"
// ListenRaffleResult
#include "AssemblyU2DCSharp_ListenRaffleResultMethodDeclarations.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
// System.Void ListenRaffleResult::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void ListenRaffleResult__ctor_m1293 (ListenRaffleResult_t332 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void ListenRaffleResult::Invoke(System.Int32,ERaffleResult)
// System.Int32
#include "mscorlib_System_Int32.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
extern "C" void ListenRaffleResult_Invoke_m1294 (ListenRaffleResult_t332 * __this, int32_t ___raffleId, int32_t ___result, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ListenRaffleResult_Invoke_m1294((ListenRaffleResult_t332 *)__this->___prev_9,___raffleId, ___result, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___raffleId, int32_t ___result, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___raffleId, ___result,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___raffleId, int32_t ___result, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___raffleId, ___result,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ListenRaffleResult_t332(Il2CppObject* delegate, int32_t ___raffleId, int32_t ___result)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___raffleId' to native representation

	// Marshaling of parameter '___result' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___raffleId, ___result);

	// Marshaling cleanup of parameter '___raffleId' native representation

	// Marshaling cleanup of parameter '___result' native representation

}
// System.IAsyncResult ListenRaffleResult::BeginInvoke(System.Int32,ERaffleResult,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* ERaffleResult_t207_il2cpp_TypeInfo_var;
extern "C" Object_t * ListenRaffleResult_BeginInvoke_m1295 (ListenRaffleResult_t332 * __this, int32_t ___raffleId, int32_t ___result, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		ERaffleResult_t207_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(203);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t372_il2cpp_TypeInfo_var, &___raffleId);
	__d_args[1] = Box(ERaffleResult_t207_il2cpp_TypeInfo_var, &___result);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void ListenRaffleResult::EndInvoke(System.IAsyncResult)
extern "C" void ListenRaffleResult_EndInvoke_m1296 (ListenRaffleResult_t332 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// RequestHandler
#include "AssemblyU2DCSharp_RequestHandler.h"
// RequestHandler
#include "AssemblyU2DCSharp_RequestHandlerMethodDeclarations.h"
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWW.h"
// System.Void RequestHandler::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void RequestHandler__ctor_m1297 (RequestHandler_t200 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void RequestHandler::Invoke(UnityEngine.WWW)
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWW.h"
extern "C" void RequestHandler_Invoke_m1298 (RequestHandler_t200 * __this, WWW_t199 * ___request, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		RequestHandler_Invoke_m1298((RequestHandler_t200 *)__this->___prev_9,___request, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, WWW_t199 * ___request, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___request,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, WWW_t199 * ___request, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___request,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___request,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_RequestHandler_t200(Il2CppObject* delegate, WWW_t199 * ___request)
{
	// Marshaling of parameter '___request' to native representation
	WWW_t199 * ____request_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.WWW'."));
}
// System.IAsyncResult RequestHandler::BeginInvoke(UnityEngine.WWW,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * RequestHandler_BeginInvoke_m1299 (RequestHandler_t200 * __this, WWW_t199 * ___request, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___request;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void RequestHandler::EndInvoke(System.IAsyncResult)
extern "C" void RequestHandler_EndInvoke_m1300 (RequestHandler_t200 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Handle
#include "AssemblyU2DCSharp_Handle.h"
// Handle
#include "AssemblyU2DCSharp_HandleMethodDeclarations.h"
// System.Void Handle::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Handle__ctor_m1301 (Handle_t221 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void Handle::Invoke()
extern "C" void Handle_Invoke_m1302 (Handle_t221 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Handle_Invoke_m1302((Handle_t221 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_Handle_t221(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult Handle::BeginInvoke(System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * Handle_BeginInvoke_m1303 (Handle_t221 * __this, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void Handle::EndInvoke(System.IAsyncResult)
extern "C" void Handle_EndInvoke_m1304 (Handle_t221 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// <PrivateImplementationDetails>/$ArrayType$24
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24Arra.h"
// <PrivateImplementationDetails>/$ArrayType$24
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24ArraMethodDeclarations.h"
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$24
extern "C" void U24ArrayTypeU2424_t333_marshal(const U24ArrayTypeU2424_t333& unmarshaled, U24ArrayTypeU2424_t333_marshaled& marshaled)
{
}
extern "C" void U24ArrayTypeU2424_t333_marshal_back(const U24ArrayTypeU2424_t333_marshaled& marshaled, U24ArrayTypeU2424_t333& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$24
extern "C" void U24ArrayTypeU2424_t333_marshal_cleanup(U24ArrayTypeU2424_t333_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E.h"
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Void <PrivateImplementationDetails>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void U3CPrivateImplementationDetailsU3E__ctor_m1305 (U3CPrivateImplementationDetailsU3E_t334 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
