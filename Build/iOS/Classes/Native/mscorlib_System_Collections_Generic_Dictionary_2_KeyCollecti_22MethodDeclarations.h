﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>
struct KeyCollection_t2713;
// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>
struct Dictionary_2_t208;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3626;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Int32[]
struct Int32U5BU5D_t401;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_23.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void KeyCollection__ctor_m17624_gshared (KeyCollection_t2713 * __this, Dictionary_2_t208 * ___dictionary, const MethodInfo* method);
#define KeyCollection__ctor_m17624(__this, ___dictionary, method) (( void (*) (KeyCollection_t2713 *, Dictionary_2_t208 *, const MethodInfo*))KeyCollection__ctor_m17624_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17625_gshared (KeyCollection_t2713 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17625(__this, ___item, method) (( void (*) (KeyCollection_t2713 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17625_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17626_gshared (KeyCollection_t2713 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17626(__this, method) (( void (*) (KeyCollection_t2713 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17626_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17627_gshared (KeyCollection_t2713 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17627(__this, ___item, method) (( bool (*) (KeyCollection_t2713 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17627_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17628_gshared (KeyCollection_t2713 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17628(__this, ___item, method) (( bool (*) (KeyCollection_t2713 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17628_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17629_gshared (KeyCollection_t2713 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17629(__this, method) (( Object_t* (*) (KeyCollection_t2713 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17629_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m17630_gshared (KeyCollection_t2713 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m17630(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2713 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m17630_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17631_gshared (KeyCollection_t2713 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17631(__this, method) (( Object_t * (*) (KeyCollection_t2713 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17631_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17632_gshared (KeyCollection_t2713 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17632(__this, method) (( bool (*) (KeyCollection_t2713 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17632_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17633_gshared (KeyCollection_t2713 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17633(__this, method) (( bool (*) (KeyCollection_t2713 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17633_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m17634_gshared (KeyCollection_t2713 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m17634(__this, method) (( Object_t * (*) (KeyCollection_t2713 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m17634_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m17635_gshared (KeyCollection_t2713 * __this, Int32U5BU5D_t401* ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_CopyTo_m17635(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2713 *, Int32U5BU5D_t401*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m17635_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::GetEnumerator()
extern "C" Enumerator_t2714  KeyCollection_GetEnumerator_m17636_gshared (KeyCollection_t2713 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m17636(__this, method) (( Enumerator_t2714  (*) (KeyCollection_t2713 *, const MethodInfo*))KeyCollection_GetEnumerator_m17636_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m17637_gshared (KeyCollection_t2713 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m17637(__this, method) (( int32_t (*) (KeyCollection_t2713 *, const MethodInfo*))KeyCollection_get_Count_m17637_gshared)(__this, method)
