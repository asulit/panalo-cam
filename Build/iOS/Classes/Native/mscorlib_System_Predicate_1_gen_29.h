﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas
struct Canvas_t574;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Canvas>
struct  Predicate_1_t2916  : public MulticastDelegate_t28
{
};
