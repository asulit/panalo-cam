﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m23950(__this, ___t, method) (( void (*) (Enumerator_t3169 *, Stack_1_t1027 *, const MethodInfo*))Enumerator__ctor_m15969_gshared)(__this, ___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23951(__this, method) (( Object_t * (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15970_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m23952(__this, method) (( void (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_Dispose_m15971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m23953(__this, method) (( bool (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_MoveNext_m15972_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m23954(__this, method) (( Type_t * (*) (Enumerator_t3169 *, const MethodInfo*))Enumerator_get_Current_m15973_gshared)(__this, method)
