﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"
#define List_1__ctor_m7433(__this, method) (( void (*) (List_1_t1373 *, const MethodInfo*))List_1__ctor_m1429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m7459(__this, ___collection, method) (( void (*) (List_1_t1373 *, Object_t*, const MethodInfo*))List_1__ctor_m14617_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Int32)
#define List_1__ctor_m28311(__this, ___capacity, method) (( void (*) (List_1_t1373 *, int32_t, const MethodInfo*))List_1__ctor_m14619_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.cctor()
#define List_1__cctor_m28312(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14621_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m28313(__this, method) (( Object_t* (*) (List_1_t1373 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14623_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m28314(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1373 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m14625_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m28315(__this, method) (( Object_t * (*) (List_1_t1373 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m14627_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m28316(__this, ___item, method) (( int32_t (*) (List_1_t1373 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m14629_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m28317(__this, ___item, method) (( bool (*) (List_1_t1373 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m14631_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m28318(__this, ___item, method) (( int32_t (*) (List_1_t1373 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m14633_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m28319(__this, ___index, ___item, method) (( void (*) (List_1_t1373 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m14635_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m28320(__this, ___item, method) (( void (*) (List_1_t1373 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m14637_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28321(__this, method) (( bool (*) (List_1_t1373 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14639_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m28322(__this, method) (( bool (*) (List_1_t1373 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m14641_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m28323(__this, method) (( Object_t * (*) (List_1_t1373 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m14643_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m28324(__this, method) (( bool (*) (List_1_t1373 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m14645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m28325(__this, method) (( bool (*) (List_1_t1373 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m14647_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m28326(__this, ___index, method) (( Object_t * (*) (List_1_t1373 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m14649_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m28327(__this, ___index, ___value, method) (( void (*) (List_1_t1373 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m14651_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Add(T)
#define List_1_Add_m28328(__this, ___item, method) (( void (*) (List_1_t1373 *, VirtualButtonAbstractBehaviour_t319 *, const MethodInfo*))List_1_Add_m14653_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m28329(__this, ___newCount, method) (( void (*) (List_1_t1373 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m14655_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m28330(__this, ___idx, ___count, method) (( void (*) (List_1_t1373 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m14657_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m28331(__this, ___collection, method) (( void (*) (List_1_t1373 *, Object_t*, const MethodInfo*))List_1_AddCollection_m14659_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m28332(__this, ___enumerable, method) (( void (*) (List_1_t1373 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m14661_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m28333(__this, ___collection, method) (( void (*) (List_1_t1373 *, Object_t*, const MethodInfo*))List_1_AddRange_m14663_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AsReadOnly()
#define List_1_AsReadOnly_m28334(__this, method) (( ReadOnlyCollection_1_t3454 * (*) (List_1_t1373 *, const MethodInfo*))List_1_AsReadOnly_m14665_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Clear()
#define List_1_Clear_m28335(__this, method) (( void (*) (List_1_t1373 *, const MethodInfo*))List_1_Clear_m14667_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Contains(T)
#define List_1_Contains_m28336(__this, ___item, method) (( bool (*) (List_1_t1373 *, VirtualButtonAbstractBehaviour_t319 *, const MethodInfo*))List_1_Contains_m14669_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m28337(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1373 *, VirtualButtonAbstractBehaviourU5BU5D_t1274*, int32_t, const MethodInfo*))List_1_CopyTo_m14671_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Find(System.Predicate`1<T>)
#define List_1_Find_m28338(__this, ___match, method) (( VirtualButtonAbstractBehaviour_t319 * (*) (List_1_t1373 *, Predicate_1_t3456 *, const MethodInfo*))List_1_Find_m14673_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m28339(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3456 *, const MethodInfo*))List_1_CheckMatch_m14675_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m28340(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1373 *, int32_t, int32_t, Predicate_1_t3456 *, const MethodInfo*))List_1_GetIndex_m14677_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::GetEnumerator()
#define List_1_GetEnumerator_m7438(__this, method) (( Enumerator_t1375  (*) (List_1_t1373 *, const MethodInfo*))List_1_GetEnumerator_m14679_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::IndexOf(T)
#define List_1_IndexOf_m28341(__this, ___item, method) (( int32_t (*) (List_1_t1373 *, VirtualButtonAbstractBehaviour_t319 *, const MethodInfo*))List_1_IndexOf_m14681_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m28342(__this, ___start, ___delta, method) (( void (*) (List_1_t1373 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m14683_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m28343(__this, ___index, method) (( void (*) (List_1_t1373 *, int32_t, const MethodInfo*))List_1_CheckIndex_m14685_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Insert(System.Int32,T)
#define List_1_Insert_m28344(__this, ___index, ___item, method) (( void (*) (List_1_t1373 *, int32_t, VirtualButtonAbstractBehaviour_t319 *, const MethodInfo*))List_1_Insert_m14687_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m28345(__this, ___collection, method) (( void (*) (List_1_t1373 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m14689_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Remove(T)
#define List_1_Remove_m28346(__this, ___item, method) (( bool (*) (List_1_t1373 *, VirtualButtonAbstractBehaviour_t319 *, const MethodInfo*))List_1_Remove_m14691_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m28347(__this, ___match, method) (( int32_t (*) (List_1_t1373 *, Predicate_1_t3456 *, const MethodInfo*))List_1_RemoveAll_m14693_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m28348(__this, ___index, method) (( void (*) (List_1_t1373 *, int32_t, const MethodInfo*))List_1_RemoveAt_m14695_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m28349(__this, ___index, ___count, method) (( void (*) (List_1_t1373 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m14697_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Reverse()
#define List_1_Reverse_m28350(__this, method) (( void (*) (List_1_t1373 *, const MethodInfo*))List_1_Reverse_m14699_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Sort()
#define List_1_Sort_m28351(__this, method) (( void (*) (List_1_t1373 *, const MethodInfo*))List_1_Sort_m14701_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m28352(__this, ___comparison, method) (( void (*) (List_1_t1373 *, Comparison_1_t3457 *, const MethodInfo*))List_1_Sort_m14703_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::ToArray()
#define List_1_ToArray_m28353(__this, method) (( VirtualButtonAbstractBehaviourU5BU5D_t1274* (*) (List_1_t1373 *, const MethodInfo*))List_1_ToArray_m14705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::TrimExcess()
#define List_1_TrimExcess_m28354(__this, method) (( void (*) (List_1_t1373 *, const MethodInfo*))List_1_TrimExcess_m14707_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Capacity()
#define List_1_get_Capacity_m28355(__this, method) (( int32_t (*) (List_1_t1373 *, const MethodInfo*))List_1_get_Capacity_m14709_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m28356(__this, ___value, method) (( void (*) (List_1_t1373 *, int32_t, const MethodInfo*))List_1_set_Capacity_m14711_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Count()
#define List_1_get_Count_m28357(__this, method) (( int32_t (*) (List_1_t1373 *, const MethodInfo*))List_1_get_Count_m14713_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Item(System.Int32)
#define List_1_get_Item_m28358(__this, ___index, method) (( VirtualButtonAbstractBehaviour_t319 * (*) (List_1_t1373 *, int32_t, const MethodInfo*))List_1_get_Item_m14715_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::set_Item(System.Int32,T)
#define List_1_set_Item_m28359(__this, ___index, ___value, method) (( void (*) (List_1_t1373 *, int32_t, VirtualButtonAbstractBehaviour_t319 *, const MethodInfo*))List_1_set_Item_m14717_gshared)(__this, ___index, ___value, method)
