﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor()
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_49MethodDeclarations.h"
#define Dictionary_2__ctor_m7237(__this, method) (( void (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2__ctor_m25002_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m25003(__this, ___comparer, method) (( void (*) (Dictionary_2_t1103 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m25004_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Int32)
#define Dictionary_2__ctor_m25005(__this, ___capacity, method) (( void (*) (Dictionary_2_t1103 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m25006_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m25007(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1103 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m25008_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m25009(__this, method) (( Object_t * (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m25010_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m25011(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1103 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m25012_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m25013(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1103 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m25014_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m25015(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1103 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m25016_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m25017(__this, ___key, method) (( bool (*) (Dictionary_2_t1103 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m25018_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m25019(__this, ___key, method) (( void (*) (Dictionary_2_t1103 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m25020_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25021(__this, method) (( bool (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25022_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25023(__this, method) (( Object_t * (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25024_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25025(__this, method) (( bool (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25026_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25027(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1103 *, KeyValuePair_2_t3271 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25028_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25029(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1103 *, KeyValuePair_2_t3271 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25030_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25031(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1103 *, KeyValuePair_2U5BU5D_t3746*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25032_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25033(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1103 *, KeyValuePair_2_t3271 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25034_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m25035(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1103 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m25036_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25037(__this, method) (( Object_t * (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25038_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25039(__this, method) (( Object_t* (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25040_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25041(__this, method) (( Object_t * (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25042_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Count()
#define Dictionary_2_get_Count_m25043(__this, method) (( int32_t (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_get_Count_m25044_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Item(TKey)
#define Dictionary_2_get_Item_m25045(__this, ___key, method) (( Image_t1109 * (*) (Dictionary_2_t1103 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m25046_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m25047(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1103 *, int32_t, Image_t1109 *, const MethodInfo*))Dictionary_2_set_Item_m25048_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m25049(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1103 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m25050_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m25051(__this, ___size, method) (( void (*) (Dictionary_2_t1103 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m25052_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m25053(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1103 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m25054_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m25055(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3271  (*) (Object_t * /* static, unused */, int32_t, Image_t1109 *, const MethodInfo*))Dictionary_2_make_pair_m25056_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m25057(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Image_t1109 *, const MethodInfo*))Dictionary_2_pick_key_m25058_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m25059(__this /* static, unused */, ___key, ___value, method) (( Image_t1109 * (*) (Object_t * /* static, unused */, int32_t, Image_t1109 *, const MethodInfo*))Dictionary_2_pick_value_m25060_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m25061(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1103 *, KeyValuePair_2U5BU5D_t3746*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m25062_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Resize()
#define Dictionary_2_Resize_m25063(__this, method) (( void (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_Resize_m25064_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Add(TKey,TValue)
#define Dictionary_2_Add_m25065(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1103 *, int32_t, Image_t1109 *, const MethodInfo*))Dictionary_2_Add_m25066_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Clear()
#define Dictionary_2_Clear_m25067(__this, method) (( void (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_Clear_m25068_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m25069(__this, ___key, method) (( bool (*) (Dictionary_2_t1103 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m25070_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m25071(__this, ___value, method) (( bool (*) (Dictionary_2_t1103 *, Image_t1109 *, const MethodInfo*))Dictionary_2_ContainsValue_m25072_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m25073(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1103 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m25074_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m25075(__this, ___sender, method) (( void (*) (Dictionary_2_t1103 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m25076_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Remove(TKey)
#define Dictionary_2_Remove_m25077(__this, ___key, method) (( bool (*) (Dictionary_2_t1103 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m25078_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m25079(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1103 *, int32_t, Image_t1109 **, const MethodInfo*))Dictionary_2_TryGetValue_m25080_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Keys()
#define Dictionary_2_get_Keys_m25081(__this, method) (( KeyCollection_t3272 * (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_get_Keys_m25082_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Values()
#define Dictionary_2_get_Values_m7293(__this, method) (( ValueCollection_t1317 * (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_get_Values_m25083_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m25084(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1103 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m25085_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m25086(__this, ___value, method) (( Image_t1109 * (*) (Dictionary_2_t1103 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m25087_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m25088(__this, ___pair, method) (( bool (*) (Dictionary_2_t1103 *, KeyValuePair_2_t3271 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m25089_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m25090(__this, method) (( Enumerator_t3273  (*) (Dictionary_2_t1103 *, const MethodInfo*))Dictionary_2_GetEnumerator_m25091_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m25092(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, int32_t, Image_t1109 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m25093_gshared)(__this /* static, unused */, ___key, ___value, method)
