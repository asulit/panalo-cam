﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InputLayer
struct InputLayer_t56;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void InputLayer::.ctor()
extern "C" void InputLayer__ctor_m200 (InputLayer_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputLayer::get_IsModal()
extern "C" bool InputLayer_get_IsModal_m201 (InputLayer_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputLayer::get_IsActive()
extern "C" bool InputLayer_get_IsActive_m202 (InputLayer_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputLayer::Activate()
extern "C" void InputLayer_Activate_m203 (InputLayer_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputLayer::Deactivate()
extern "C" void InputLayer_Deactivate_m204 (InputLayer_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputLayer::RespondsToTouchPosition(UnityEngine.Vector3,InputLayer)
extern "C" bool InputLayer_RespondsToTouchPosition_m205 (InputLayer_t56 * __this, Vector3_t36  ___touchPos, InputLayer_t56 * ___requesterLayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
