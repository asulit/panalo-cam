﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableImpl.h"
// Vuforia.MarkerImpl
struct  MarkerImpl_t1120  : public TrackableImpl_t1071
{
	// System.Single Vuforia.MarkerImpl::mSize
	float ___mSize_2;
	// System.Int32 Vuforia.MarkerImpl::<MarkerID>k__BackingField
	int32_t ___U3CMarkerIDU3Ek__BackingField_3;
};
