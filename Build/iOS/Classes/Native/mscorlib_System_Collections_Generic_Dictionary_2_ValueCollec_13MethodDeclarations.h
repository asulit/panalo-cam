﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_29MethodDeclarations.h"
#define Enumerator__ctor_m27080(__this, ___host, method) (( void (*) (Enumerator_t1322 *, Dictionary_2_t1187 *, const MethodInfo*))Enumerator__ctor_m15170_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27081(__this, method) (( Object_t * (*) (Enumerator_t1322 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15171_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Dispose()
#define Enumerator_Dispose_m7317(__this, method) (( void (*) (Enumerator_t1322 *, const MethodInfo*))Enumerator_Dispose_m15172_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::MoveNext()
#define Enumerator_MoveNext_m7316(__this, method) (( bool (*) (Enumerator_t1322 *, const MethodInfo*))Enumerator_MoveNext_m15173_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Current()
#define Enumerator_get_Current_m7314(__this, method) (( List_1_t1186 * (*) (Enumerator_t1322 *, const MethodInfo*))Enumerator_get_Current_m15174_gshared)(__this, method)
