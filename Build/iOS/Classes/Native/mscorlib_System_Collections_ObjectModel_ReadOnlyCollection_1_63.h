﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>
struct IList_1_t3373;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>
struct  ReadOnlyCollection_1_t3372  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::list
	Object_t* ___list_0;
};
