﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SelectionListGui/DoubleClickCallback
struct DoubleClickCallback_t53;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void SelectionListGui/DoubleClickCallback::.ctor(System.Object,System.IntPtr)
extern "C" void DoubleClickCallback__ctor_m184 (DoubleClickCallback_t53 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectionListGui/DoubleClickCallback::Invoke(System.Int32)
extern "C" void DoubleClickCallback_Invoke_m185 (DoubleClickCallback_t53 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_DoubleClickCallback_t53(Il2CppObject* delegate, int32_t ___index);
// System.IAsyncResult SelectionListGui/DoubleClickCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C" Object_t * DoubleClickCallback_BeginInvoke_m186 (DoubleClickCallback_t53 * __this, int32_t ___index, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectionListGui/DoubleClickCallback::EndInvoke(System.IAsyncResult)
extern "C" void DoubleClickCallback_EndInvoke_m187 (DoubleClickCallback_t53 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
