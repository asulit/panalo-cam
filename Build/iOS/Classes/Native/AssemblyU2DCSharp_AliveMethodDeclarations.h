﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Alive
struct Alive_t232;

// System.Void Alive::.ctor()
extern "C" void Alive__ctor_m835 (Alive_t232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Alive::Awake()
extern "C" void Alive_Awake_m836 (Alive_t232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
