﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2448_t1554_marshal(const U24ArrayTypeU2448_t1554& unmarshaled, U24ArrayTypeU2448_t1554_marshaled& marshaled);
extern "C" void U24ArrayTypeU2448_t1554_marshal_back(const U24ArrayTypeU2448_t1554_marshaled& marshaled, U24ArrayTypeU2448_t1554& unmarshaled);
extern "C" void U24ArrayTypeU2448_t1554_marshal_cleanup(U24ArrayTypeU2448_t1554_marshaled& marshaled);
