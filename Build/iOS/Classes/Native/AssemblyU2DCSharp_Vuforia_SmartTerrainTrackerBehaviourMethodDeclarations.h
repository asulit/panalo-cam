﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerBehaviour
struct SmartTerrainTrackerBehaviour_t304;

// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
extern "C" void SmartTerrainTrackerBehaviour__ctor_m1259 (SmartTerrainTrackerBehaviour_t304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
