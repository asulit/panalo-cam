﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_19MethodDeclarations.h"
#define KeyCollection__ctor_m17525(__this, ___dictionary, method) (( void (*) (KeyCollection_t2705 *, Dictionary_2_t192 *, const MethodInfo*))KeyCollection__ctor_m17435_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17526(__this, ___item, method) (( void (*) (KeyCollection_t2705 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17436_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17527(__this, method) (( void (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17437_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17528(__this, ___item, method) (( bool (*) (KeyCollection_t2705 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17438_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17529(__this, ___item, method) (( bool (*) (KeyCollection_t2705 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17439_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17530(__this, method) (( Object_t* (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17440_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m17531(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2705 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m17441_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17532(__this, method) (( Object_t * (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17442_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17533(__this, method) (( bool (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17443_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17534(__this, method) (( bool (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17444_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m17535(__this, method) (( Object_t * (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m17445_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m17536(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2705 *, ESubScreensU5BU5D_t2685*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m17446_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::GetEnumerator()
#define KeyCollection_GetEnumerator_m17537(__this, method) (( Enumerator_t3658  (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_GetEnumerator_m17447_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.String>::get_Count()
#define KeyCollection_get_Count_m17538(__this, method) (( int32_t (*) (KeyCollection_t2705 *, const MethodInfo*))KeyCollection_get_Count_m17448_gshared)(__this, method)
