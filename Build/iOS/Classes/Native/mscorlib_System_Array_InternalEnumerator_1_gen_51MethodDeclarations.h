﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_51.h"
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m21230_gshared (InternalEnumerator_1_t2970 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m21230(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2970 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m21230_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21231_gshared (InternalEnumerator_1_t2970 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21231(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2970 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21231_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m21232_gshared (InternalEnumerator_1_t2970 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m21232(__this, method) (( void (*) (InternalEnumerator_1_t2970 *, const MethodInfo*))InternalEnumerator_1_Dispose_m21232_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m21233_gshared (InternalEnumerator_1_t2970 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m21233(__this, method) (( bool (*) (InternalEnumerator_1_t2970 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m21233_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m21234_gshared (InternalEnumerator_1_t2970 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m21234(__this, method) (( int32_t (*) (InternalEnumerator_1_t2970 *, const MethodInfo*))InternalEnumerator_1_get_Current_m21234_gshared)(__this, method)
