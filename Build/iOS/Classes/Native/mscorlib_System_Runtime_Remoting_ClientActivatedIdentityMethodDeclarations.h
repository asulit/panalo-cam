﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ClientActivatedIdentity
struct ClientActivatedIdentity_t2152;
// System.MarshalByRefObject
struct MarshalByRefObject_t1643;

// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
extern "C" MarshalByRefObject_t1643 * ClientActivatedIdentity_GetServerObject_m12736 (ClientActivatedIdentity_t2152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
