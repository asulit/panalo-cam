﻿#pragma once
#include <stdint.h>
// UnityEngine.WWW
struct WWW_t199;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// RequestHandler
struct  RequestHandler_t200  : public MulticastDelegate_t28
{
};
