﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t82;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// Vuforia.WireframeBehaviour
struct  WireframeBehaviour_t324  : public MonoBehaviour_t5
{
	// UnityEngine.Material Vuforia.WireframeBehaviour::mLineMaterial
	Material_t82 * ___mLineMaterial_2;
	// System.Boolean Vuforia.WireframeBehaviour::ShowLines
	bool ___ShowLines_3;
	// UnityEngine.Color Vuforia.WireframeBehaviour::LineColor
	Color_t9  ___LineColor_4;
};
