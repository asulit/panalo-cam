﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t261;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.SoapServices/TypeInfo
struct  TypeInfo_t2155  : public Object_t
{
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices/TypeInfo::Attributes
	Hashtable_t261 * ___Attributes_0;
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices/TypeInfo::Elements
	Hashtable_t261 * ___Elements_1;
};
