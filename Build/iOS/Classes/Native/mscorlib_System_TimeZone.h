﻿#pragma once
#include <stdint.h>
// System.TimeZone
struct TimeZone_t2373;
// System.Object
#include "mscorlib_System_Object.h"
// System.TimeZone
struct  TimeZone_t2373  : public Object_t
{
};
struct TimeZone_t2373_StaticFields{
	// System.TimeZone System.TimeZone::currentTimeZone
	TimeZone_t2373 * ___currentTimeZone_0;
};
