﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.VuforiaRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRendererImpl.h"
// Vuforia.VuforiaRendererImpl/RenderEvent
struct  RenderEvent_t1159 
{
	// System.Int32 Vuforia.VuforiaRendererImpl/RenderEvent::value__
	int32_t ___value___1;
};
