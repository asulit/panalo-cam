﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t894;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_67.h"
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m23396_gshared (Enumerator_t3132 * __this, List_1_t894 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m23396(__this, ___l, method) (( void (*) (Enumerator_t3132 *, List_1_t894 *, const MethodInfo*))Enumerator__ctor_m23396_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23397_gshared (Enumerator_t3132 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23397(__this, method) (( Object_t * (*) (Enumerator_t3132 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23397_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C" void Enumerator_Dispose_m23398_gshared (Enumerator_t3132 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m23398(__this, method) (( void (*) (Enumerator_t3132 *, const MethodInfo*))Enumerator_Dispose_m23398_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m23399_gshared (Enumerator_t3132 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m23399(__this, method) (( void (*) (Enumerator_t3132 *, const MethodInfo*))Enumerator_VerifyState_m23399_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23400_gshared (Enumerator_t3132 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m23400(__this, method) (( bool (*) (Enumerator_t3132 *, const MethodInfo*))Enumerator_MoveNext_m23400_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t754  Enumerator_get_Current_m23401_gshared (Enumerator_t3132 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m23401(__this, method) (( UICharInfo_t754  (*) (Enumerator_t3132 *, const MethodInfo*))Enumerator_get_Current_m23401_gshared)(__this, method)
