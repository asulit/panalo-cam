﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityThreading.ThreadBase>
struct List_1_t182;
// UnityThreading.ThreadBase
struct ThreadBase_t169;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityThreading.ThreadBase>
struct  Enumerator_t429 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityThreading.ThreadBase>::l
	List_1_t182 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityThreading.ThreadBase>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityThreading.ThreadBase>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityThreading.ThreadBase>::current
	ThreadBase_t169 * ___current_3;
};
