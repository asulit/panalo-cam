﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Type
struct Type_t;

// System.UInt16 Vuforia.TypeMapping::GetTypeID(System.Type)
extern "C" uint16_t TypeMapping_GetTypeID_m5844 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TypeMapping::.cctor()
extern "C" void TypeMapping__cctor_m5845 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
