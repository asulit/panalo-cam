﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IBeginDragHandler
struct IBeginDragHandler_t693;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t480;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct  EventFunction_1_t495  : public MulticastDelegate_t28
{
};
