﻿#pragma once
#include <stdint.h>
// UnityEngine.Collider
struct Collider_t369;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Collider>
struct  Predicate_1_t2534  : public MulticastDelegate_t28
{
};
