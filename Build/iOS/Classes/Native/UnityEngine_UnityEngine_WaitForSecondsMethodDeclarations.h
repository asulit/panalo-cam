﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForSeconds
struct WaitForSeconds_t410;
struct WaitForSeconds_t410_marshaled;

// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m1530 (WaitForSeconds_t410 * __this, float ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void WaitForSeconds_t410_marshal(const WaitForSeconds_t410& unmarshaled, WaitForSeconds_t410_marshaled& marshaled);
extern "C" void WaitForSeconds_t410_marshal_back(const WaitForSeconds_t410_marshaled& marshaled, WaitForSeconds_t410& unmarshaled);
extern "C" void WaitForSeconds_t410_marshal_cleanup(WaitForSeconds_t410_marshaled& marshaled);
