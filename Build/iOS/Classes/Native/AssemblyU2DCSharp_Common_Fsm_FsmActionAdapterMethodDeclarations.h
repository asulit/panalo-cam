﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.FsmActionAdapter
struct FsmActionAdapter_t33;
// Common.Fsm.FsmState
struct FsmState_t29;

// System.Void Common.Fsm.FsmActionAdapter::.ctor(Common.Fsm.FsmState)
extern "C" void FsmActionAdapter__ctor_m179 (FsmActionAdapter_t33 * __this, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::GetOwner()
extern "C" Object_t * FsmActionAdapter_GetOwner_m180 (FsmActionAdapter_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.FsmActionAdapter::OnEnter()
extern "C" void FsmActionAdapter_OnEnter_m181 (FsmActionAdapter_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.FsmActionAdapter::OnUpdate()
extern "C" void FsmActionAdapter_OnUpdate_m182 (FsmActionAdapter_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.FsmActionAdapter::OnExit()
extern "C" void FsmActionAdapter_OnExit_m183 (FsmActionAdapter_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
