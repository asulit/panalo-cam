﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Common.Time.TimeReference>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m16420(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2617 *, String_t*, TimeReference_t14 *, const MethodInfo*))KeyValuePair_2__ctor_m15105_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Common.Time.TimeReference>::get_Key()
#define KeyValuePair_2_get_Key_m16421(__this, method) (( String_t* (*) (KeyValuePair_2_t2617 *, const MethodInfo*))KeyValuePair_2_get_Key_m15106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Common.Time.TimeReference>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16422(__this, ___value, method) (( void (*) (KeyValuePair_2_t2617 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15107_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Common.Time.TimeReference>::get_Value()
#define KeyValuePair_2_get_Value_m16423(__this, method) (( TimeReference_t14 * (*) (KeyValuePair_2_t2617 *, const MethodInfo*))KeyValuePair_2_get_Value_m15108_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Common.Time.TimeReference>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16424(__this, ___value, method) (( void (*) (KeyValuePair_2_t2617 *, TimeReference_t14 *, const MethodInfo*))KeyValuePair_2_set_Value_m15109_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Common.Time.TimeReference>::ToString()
#define KeyValuePair_2_ToString_m16425(__this, method) (( String_t* (*) (KeyValuePair_2_t2617 *, const MethodInfo*))KeyValuePair_2_ToString_m15110_gshared)(__this, method)
