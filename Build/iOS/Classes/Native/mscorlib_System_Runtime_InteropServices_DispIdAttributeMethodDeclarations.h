﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.DispIdAttribute
struct DispIdAttribute_t2051;

// System.Void System.Runtime.InteropServices.DispIdAttribute::.ctor(System.Int32)
extern "C" void DispIdAttribute__ctor_m12292 (DispIdAttribute_t2051 * __this, int32_t ___dispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
