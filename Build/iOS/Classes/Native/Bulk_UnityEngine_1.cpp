﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
#include "mscorlib_System_Array.h"

// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_ArrayTypes.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_gen_27.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// System.Void UnityEngine.Component::.ctor()
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern "C" void Component__ctor_m4349 (Component_t365 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m4335(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" Transform_t35 * Component_get_transform_m1417 (Component_t365 * __this, const MethodInfo* method)
{
	typedef Transform_t35 * (*Component_get_transform_m1417_ftn) (Component_t365 *);
	static Component_get_transform_m1417_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m1417_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" GameObject_t155 * Component_get_gameObject_m1337 (Component_t365 * __this, const MethodInfo* method)
{
	typedef GameObject_t155 * (*Component_get_gameObject_m1337_ftn) (Component_t365 *);
	static Component_get_gameObject_m1337_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m1337_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
// System.Type
#include "mscorlib_System_Type.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
extern "C" Component_t365 * Component_GetComponent_m3768 (Component_t365 * __this, Type_t * ___type, const MethodInfo* method)
{
	{
		GameObject_t155 * L_0 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type;
		NullCheck(L_0);
		Component_t365 * L_2 = GameObject_GetComponent_m1474(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Component_GetComponentFastPath_m4350 (Component_t365 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, const MethodInfo* method)
{
	typedef void (*Component_GetComponentFastPath_m4350_ftn) (Component_t365 *, Type_t *, IntPtr_t);
	static Component_GetComponentFastPath_m4350_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m4350_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type)
extern "C" Component_t365 * Component_GetComponentInChildren_m4351 (Component_t365 * __this, Type_t * ___t, const MethodInfo* method)
{
	{
		GameObject_t155 * L_0 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t;
		NullCheck(L_0);
		Component_t365 * L_2 = GameObject_GetComponentInChildren_m4361(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type)
extern "C" ComponentU5BU5D_t397* Component_GetComponentsInChildren_m1463 (Component_t365 * __this, Type_t * ___t, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		Type_t * L_0 = ___t;
		bool L_1 = V_0;
		ComponentU5BU5D_t397* L_2 = Component_GetComponentsInChildren_m4352(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type,System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" ComponentU5BU5D_t397* Component_GetComponentsInChildren_m4352 (Component_t365 * __this, Type_t * ___t, bool ___includeInactive, const MethodInfo* method)
{
	{
		GameObject_t155 * L_0 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t;
		bool L_2 = ___includeInactive;
		NullCheck(L_0);
		ComponentU5BU5D_t397* L_3 = GameObject_GetComponentsInChildren_m4363(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern "C" Component_t365 * Component_GetComponentInParent_m4353 (Component_t365 * __this, Type_t * ___t, const MethodInfo* method)
{
	{
		GameObject_t155 * L_0 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t;
		NullCheck(L_0);
		Component_t365 * L_2 = GameObject_GetComponentInParent_m4362(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" void Component_GetComponentsForListInternal_m4354 (Component_t365 * __this, Type_t * ___searchType, Object_t * ___resultList, const MethodInfo* method)
{
	typedef void (*Component_GetComponentsForListInternal_m4354_ftn) (Component_t365 *, Type_t *, Object_t *);
	static Component_GetComponentsForListInternal_m4354_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m4354_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType, ___resultList);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
// System.Collections.Generic.List`1<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_gen_27.h"
extern "C" void Component_GetComponents_m3508 (Component_t365 * __this, Type_t * ___type, List_1_t718 * ___results, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		List_1_t718 * L_1 = ___results;
		Component_GetComponentsForListInternal_m4354(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Light
#include "UnityEngine_UnityEngine_Light.h"
// UnityEngine.Light
#include "UnityEngine_UnityEngine_LightMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Color UnityEngine.Light::get_color()
// UnityEngine.Light
#include "UnityEngine_UnityEngine_LightMethodDeclarations.h"
extern "C" Color_t9  Light_get_color_m1728 (Light_t444 * __this, const MethodInfo* method)
{
	Color_t9  V_0 = {0};
	{
		Light_INTERNAL_get_color_m4355(__this, (&V_0), /*hidden argument*/NULL);
		Color_t9  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Light::set_color(UnityEngine.Color)
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
extern "C" void Light_set_color_m1730 (Light_t444 * __this, Color_t9  ___value, const MethodInfo* method)
{
	{
		Light_INTERNAL_set_color_m4356(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void Light_INTERNAL_get_color_m4355 (Light_t444 * __this, Color_t9 * ___value, const MethodInfo* method)
{
	typedef void (*Light_INTERNAL_get_color_m4355_ftn) (Light_t444 *, Color_t9 *);
	static Light_INTERNAL_get_color_m4355_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_INTERNAL_get_color_m4355_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void Light_INTERNAL_set_color_m4356 (Light_t444 * __this, Color_t9 * ___value, const MethodInfo* method)
{
	typedef void (*Light_INTERNAL_set_color_m4356_ftn) (Light_t444 *, Color_t9 *);
	static Light_INTERNAL_set_color_m4356_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_INTERNAL_set_color_m4356_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String
#include "mscorlib_System_String.h"
#include "mscorlib_ArrayTypes.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.PrimitiveType
#include "UnityEngine_UnityEngine_PrimitiveType.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// System.Void UnityEngine.GameObject::.ctor(System.String)
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
extern "C" void GameObject__ctor_m1577 (GameObject_t155 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Object__ctor_m4335(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		GameObject_Internal_CreateGameObject_m4366(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor()
extern "C" void GameObject__ctor_m4357 (GameObject_t155 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m4335(__this, /*hidden argument*/NULL);
		GameObject_Internal_CreateGameObject_m4366(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
#include "mscorlib_ArrayTypes.h"
extern "C" void GameObject__ctor_m4358 (GameObject_t155 * __this, String_t* ___name, TypeU5BU5D_t1005* ___components, const MethodInfo* method)
{
	Type_t * V_0 = {0};
	TypeU5BU5D_t1005* V_1 = {0};
	int32_t V_2 = 0;
	{
		Object__ctor_m4335(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		GameObject_Internal_CreateGameObject_m4366(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		TypeU5BU5D_t1005* L_1 = ___components;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0026;
	}

IL_0016:
	{
		TypeU5BU5D_t1005* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(Type_t **)(Type_t **)SZArrayLdElema(L_2, L_4, sizeof(Type_t *)));
		Type_t * L_5 = V_0;
		GameObject_AddComponent_m1483(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_7 = V_2;
		TypeU5BU5D_t1005* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)(((Array_t *)L_8)->max_length))))))
		{
			goto IL_0016;
		}
	}
	{
		return;
	}
}
// UnityEngine.GameObject UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)
// UnityEngine.PrimitiveType
#include "UnityEngine_UnityEngine_PrimitiveType.h"
extern "C" GameObject_t155 * GameObject_CreatePrimitive_m4359 (Object_t * __this /* static, unused */, int32_t ___type, const MethodInfo* method)
{
	typedef GameObject_t155 * (*GameObject_CreatePrimitive_m4359_ftn) (int32_t);
	static GameObject_CreatePrimitive_m4359_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_CreatePrimitive_m4359_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)");
	return _il2cpp_icall_func(___type);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
// System.Type
#include "mscorlib_System_Type.h"
extern "C" Component_t365 * GameObject_GetComponent_m1474 (GameObject_t155 * __this, Type_t * ___type, const MethodInfo* method)
{
	typedef Component_t365 * (*GameObject_GetComponent_m1474_ftn) (GameObject_t155 *, Type_t *);
	static GameObject_GetComponent_m1474_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponent_m1474_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	return _il2cpp_icall_func(__this, ___type);
}
// System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void GameObject_GetComponentFastPath_m4360 (GameObject_t155 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, const MethodInfo* method)
{
	typedef void (*GameObject_GetComponentFastPath_m4360_ftn) (GameObject_t155 *, Type_t *, IntPtr_t);
	static GameObject_GetComponentFastPath_m4360_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentFastPath_m4360_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type)
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t35_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" Component_t365 * GameObject_GetComponentInChildren_m4361 (GameObject_t155 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		Transform_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Component_t365 * V_0 = {0};
	Transform_t35 * V_1 = {0};
	Transform_t35 * V_2 = {0};
	Object_t * V_3 = {0};
	Component_t365 * V_4 = {0};
	Component_t365 * V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameObject_get_activeInHierarchy_m3317(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_1 = ___type;
		Component_t365 * L_2 = GameObject_GetComponent_m1474(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Component_t365 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_3, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		Component_t365 * L_5 = V_0;
		return L_5;
	}

IL_0021:
	{
		Transform_t35 * L_6 = GameObject_get_transform_m1349(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		Transform_t35 * L_7 = V_1;
		bool L_8 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_7, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0095;
		}
	}
	{
		Transform_t35 * L_9 = V_1;
		NullCheck(L_9);
		Object_t * L_10 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_9);
		V_3 = L_10;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0070;
		}

IL_0040:
		{
			Object_t * L_11 = V_3;
			NullCheck(L_11);
			Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_11);
			V_2 = ((Transform_t35 *)CastclassClass(L_12, Transform_t35_il2cpp_TypeInfo_var));
			Transform_t35 * L_13 = V_2;
			NullCheck(L_13);
			GameObject_t155 * L_14 = Component_get_gameObject_m1337(L_13, /*hidden argument*/NULL);
			Type_t * L_15 = ___type;
			NullCheck(L_14);
			Component_t365 * L_16 = GameObject_GetComponentInChildren_m4361(L_14, L_15, /*hidden argument*/NULL);
			V_4 = L_16;
			Component_t365 * L_17 = V_4;
			bool L_18 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_17, (Object_t335 *)NULL, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_0070;
			}
		}

IL_0067:
		{
			Component_t365 * L_19 = V_4;
			V_5 = L_19;
			IL2CPP_LEAVE(0x97, FINALLY_0080);
		}

IL_0070:
		{
			Object_t * L_20 = V_3;
			NullCheck(L_20);
			bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0040;
			}
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x95, FINALLY_0080);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0080;
	}

FINALLY_0080:
	{ // begin finally (depth: 1)
		{
			Object_t * L_22 = V_3;
			V_6 = ((Object_t *)IsInst(L_22, IDisposable_t364_il2cpp_TypeInfo_var));
			Object_t * L_23 = V_6;
			if (L_23)
			{
				goto IL_008d;
			}
		}

IL_008c:
		{
			IL2CPP_END_FINALLY(128)
		}

IL_008d:
		{
			Object_t * L_24 = V_6;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_24);
			IL2CPP_END_FINALLY(128)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(128)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0095:
	{
		return (Component_t365 *)NULL;
	}

IL_0097:
	{
		Component_t365 * L_25 = V_5;
		return L_25;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern "C" Component_t365 * GameObject_GetComponentInParent_m4362 (GameObject_t155 * __this, Type_t * ___type, const MethodInfo* method)
{
	Component_t365 * V_0 = {0};
	Transform_t35 * V_1 = {0};
	Component_t365 * V_2 = {0};
	{
		bool L_0 = GameObject_get_activeInHierarchy_m3317(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_1 = ___type;
		Component_t365 * L_2 = GameObject_GetComponent_m1474(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Component_t365 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_3, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		Component_t365 * L_5 = V_0;
		return L_5;
	}

IL_0021:
	{
		Transform_t35 * L_6 = GameObject_get_transform_m1349(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t35 * L_7 = Transform_get_parent_m1342(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Transform_t35 * L_8 = V_1;
		bool L_9 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_8, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007c;
		}
	}
	{
		goto IL_0070;
	}

IL_003e:
	{
		Transform_t35 * L_10 = V_1;
		NullCheck(L_10);
		GameObject_t155 * L_11 = Component_get_gameObject_m1337(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		bool L_12 = GameObject_get_activeInHierarchy_m3317(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		Transform_t35 * L_13 = V_1;
		NullCheck(L_13);
		GameObject_t155 * L_14 = Component_get_gameObject_m1337(L_13, /*hidden argument*/NULL);
		Type_t * L_15 = ___type;
		NullCheck(L_14);
		Component_t365 * L_16 = GameObject_GetComponent_m1474(L_14, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		Component_t365 * L_17 = V_2;
		bool L_18 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_17, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0069;
		}
	}
	{
		Component_t365 * L_19 = V_2;
		return L_19;
	}

IL_0069:
	{
		Transform_t35 * L_20 = V_1;
		NullCheck(L_20);
		Transform_t35 * L_21 = Transform_get_parent_m1342(L_20, /*hidden argument*/NULL);
		V_1 = L_21;
	}

IL_0070:
	{
		Transform_t35 * L_22 = V_1;
		bool L_23 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_22, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_003e;
		}
	}

IL_007c:
	{
		return (Component_t365 *)NULL;
	}
}
// UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInChildren(System.Type,System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo* ComponentU5BU5D_t397_il2cpp_TypeInfo_var;
extern "C" ComponentU5BU5D_t397* GameObject_GetComponentsInChildren_m4363 (GameObject_t155 * __this, Type_t * ___type, bool ___includeInactive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentU5BU5D_t397_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(583);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___type;
		bool L_1 = ___includeInactive;
		Array_t * L_2 = GameObject_GetComponentsInternal_m4364(__this, L_0, 0, 1, L_1, 0, NULL, /*hidden argument*/NULL);
		return ((ComponentU5BU5D_t397*)Castclass(L_2, ComponentU5BU5D_t397_il2cpp_TypeInfo_var));
	}
}
// System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" Array_t * GameObject_GetComponentsInternal_m4364 (GameObject_t155 * __this, Type_t * ___type, bool ___useSearchTypeAsArrayReturnType, bool ___recursive, bool ___includeInactive, bool ___reverse, Object_t * ___resultList, const MethodInfo* method)
{
	typedef Array_t * (*GameObject_GetComponentsInternal_m4364_ftn) (GameObject_t155 *, Type_t *, bool, bool, bool, bool, Object_t *);
	static GameObject_GetComponentsInternal_m4364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsInternal_m4364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	return _il2cpp_icall_func(__this, ___type, ___useSearchTypeAsArrayReturnType, ___recursive, ___includeInactive, ___reverse, ___resultList);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" Transform_t35 * GameObject_get_transform_m1349 (GameObject_t155 * __this, const MethodInfo* method)
{
	typedef Transform_t35 * (*GameObject_get_transform_m1349_ftn) (GameObject_t155 *);
	static GameObject_get_transform_m1349_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_transform_m1349_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_transform()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C" int32_t GameObject_get_layer_m3414 (GameObject_t155 * __this, const MethodInfo* method)
{
	typedef int32_t (*GameObject_get_layer_m3414_ftn) (GameObject_t155 *);
	static GameObject_get_layer_m3414_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_layer_m3414_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_layer()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void GameObject_set_layer_m1351 (GameObject_t155 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GameObject_set_layer_m1351_ftn) (GameObject_t155 *, int32_t);
	static GameObject_set_layer_m1351_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_layer_m1351_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" void GameObject_SetActive_m1538 (GameObject_t155 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GameObject_SetActive_m1538_ftn) (GameObject_t155 *, bool);
	static GameObject_SetActive_m1538_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SetActive_m1538_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SetActive(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GameObject::get_activeSelf()
extern "C" bool GameObject_get_activeSelf_m1687 (GameObject_t155 * __this, const MethodInfo* method)
{
	typedef bool (*GameObject_get_activeSelf_m1687_ftn) (GameObject_t155 *);
	static GameObject_get_activeSelf_m1687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeSelf_m1687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeSelf()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C" bool GameObject_get_activeInHierarchy_m3317 (GameObject_t155 * __this, const MethodInfo* method)
{
	typedef bool (*GameObject_get_activeInHierarchy_m3317_ftn) (GameObject_t155 *);
	static GameObject_get_activeInHierarchy_m3317_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeInHierarchy_m3317_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeInHierarchy()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::SetActiveRecursively(System.Boolean)
extern "C" void GameObject_SetActiveRecursively_m1354 (GameObject_t155 * __this, bool ___state, const MethodInfo* method)
{
	typedef void (*GameObject_SetActiveRecursively_m1354_ftn) (GameObject_t155 *, bool);
	static GameObject_SetActiveRecursively_m1354_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SetActiveRecursively_m1354_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SetActiveRecursively(System.Boolean)");
	_il2cpp_icall_func(__this, ___state);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
extern "C" void GameObject_SendMessage_m1803 (GameObject_t155 * __this, String_t* ___methodName, Object_t * ___value, int32_t ___options, const MethodInfo* method)
{
	typedef void (*GameObject_SendMessage_m1803_ftn) (GameObject_t155 *, String_t*, Object_t *, int32_t);
	static GameObject_SendMessage_m1803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m1803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName, ___value, ___options);
}
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C" Component_t365 * GameObject_Internal_AddComponentWithType_m4365 (GameObject_t155 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	typedef Component_t365 * (*GameObject_Internal_AddComponentWithType_m4365_ftn) (GameObject_t155 *, Type_t *);
	static GameObject_Internal_AddComponentWithType_m4365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_AddComponentWithType_m4365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)");
	return _il2cpp_icall_func(__this, ___componentType);
}
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C" Component_t365 * GameObject_AddComponent_m1483 (GameObject_t155 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___componentType;
		Component_t365 * L_1 = GameObject_Internal_AddComponentWithType_m4365(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
extern "C" void GameObject_Internal_CreateGameObject_m4366 (Object_t * __this /* static, unused */, GameObject_t155 * ___mono, String_t* ___name, const MethodInfo* method)
{
	typedef void (*GameObject_Internal_CreateGameObject_m4366_ftn) (GameObject_t155 *, String_t*);
	static GameObject_Internal_CreateGameObject_m4366_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_CreateGameObject_m4366_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(___mono, ___name);
}
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" GameObject_t155 * GameObject_Find_m4367 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef GameObject_t155 * (*GameObject_Find_m4367_ftn) (String_t*);
	static GameObject_Find_m4367_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Find_m4367_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Find(System.String)");
	return _il2cpp_icall_func(___name);
}
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_Enumerator.h"
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_EnumeratorMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void Enumerator__ctor_m4368 (Enumerator_t850 * __this, Transform_t35 * ___outer, const MethodInfo* method)
{
	{
		__this->___currentIndex_1 = (-1);
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Transform_t35 * L_0 = ___outer;
		__this->___outer_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern "C" Object_t * Enumerator_get_Current_m4369 (Enumerator_t850 * __this, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = (__this->___outer_0);
		int32_t L_1 = (__this->___currentIndex_1);
		NullCheck(L_0);
		Transform_t35 * L_2 = Transform_GetChild_m3415(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m4370 (Enumerator_t850 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Transform_t35 * L_0 = (__this->___outer_0);
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m3416(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (__this->___currentIndex_1);
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->___currentIndex_1 = L_3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		return ((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
	}
}
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern "C" Vector3_t36  Transform_get_position_m1388 (Transform_t35 * __this, const MethodInfo* method)
{
	Vector3_t36  V_0 = {0};
	{
		Transform_INTERNAL_get_position_m4371(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t36  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
extern "C" void Transform_set_position_m1383 (Transform_t35 * __this, Vector3_t36  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_position_m4372(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_position_m4371 (Transform_t35 * __this, Vector3_t36 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_position_m4371_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_get_position_m4371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m4371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_position_m4372 (Transform_t35 * __this, Vector3_t36 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_position_m4372_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_set_position_m4372_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m4372_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C" Vector3_t36  Transform_get_localPosition_m1740 (Transform_t35 * __this, const MethodInfo* method)
{
	Vector3_t36  V_0 = {0};
	{
		Transform_INTERNAL_get_localPosition_m4373(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t36  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" void Transform_set_localPosition_m1384 (Transform_t35 * __this, Vector3_t36  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localPosition_m4374(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localPosition_m4373 (Transform_t35 * __this, Vector3_t36 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m4373_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_get_localPosition_m4373_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m4373_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localPosition_m4374 (Transform_t35 * __this, Vector3_t36 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m4374_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_set_localPosition_m4374_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m4374_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
extern "C" Vector3_t36  Transform_get_eulerAngles_m1539 (Transform_t35 * __this, const MethodInfo* method)
{
	Quaternion_t41  V_0 = {0};
	{
		Quaternion_t41  L_0 = Transform_get_rotation_m1903(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t36  L_1 = Quaternion_get_eulerAngles_m4057((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C" void Transform_set_eulerAngles_m1540 (Transform_t35 * __this, Vector3_t36  ___value, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = ___value;
		Quaternion_t41  L_1 = Quaternion_Euler_m1770(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Transform_set_rotation_m1391(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localEulerAngles()
extern "C" Vector3_t36  Transform_get_localEulerAngles_m1741 (Transform_t35 * __this, const MethodInfo* method)
{
	Vector3_t36  V_0 = {0};
	{
		Transform_INTERNAL_get_localEulerAngles_m4375(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t36  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern "C" void Transform_set_localEulerAngles_m1742 (Transform_t35 * __this, Vector3_t36  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localEulerAngles_m4376(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localEulerAngles(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localEulerAngles_m4375 (Transform_t35 * __this, Vector3_t36 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localEulerAngles_m4375_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_get_localEulerAngles_m4375_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localEulerAngles_m4375_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localEulerAngles(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localEulerAngles(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localEulerAngles_m4376 (Transform_t35 * __this, Vector3_t36 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localEulerAngles_m4376_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_set_localEulerAngles_m4376_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localEulerAngles_m4376_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localEulerAngles(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
extern "C" Vector3_t36  Transform_get_forward_m3543 (Transform_t35 * __this, const MethodInfo* method)
{
	{
		Quaternion_t41  L_0 = Transform_get_rotation_m1903(__this, /*hidden argument*/NULL);
		Vector3_t36  L_1 = Vector3_get_forward_m3540(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t36  L_2 = Quaternion_op_Multiply_m3541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" Quaternion_t41  Transform_get_rotation_m1903 (Transform_t35 * __this, const MethodInfo* method)
{
	Quaternion_t41  V_0 = {0};
	{
		Transform_INTERNAL_get_rotation_m4377(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t41  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
extern "C" void Transform_set_rotation_m1391 (Transform_t35 * __this, Quaternion_t41  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_rotation_m4378(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_rotation_m4377 (Transform_t35 * __this, Quaternion_t41 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m4377_ftn) (Transform_t35 *, Quaternion_t41 *);
	static Transform_INTERNAL_get_rotation_m4377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m4377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_rotation_m4378 (Transform_t35 * __this, Quaternion_t41 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m4378_ftn) (Transform_t35 *, Quaternion_t41 *);
	static Transform_INTERNAL_set_rotation_m4378_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m4378_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C" Quaternion_t41  Transform_get_localRotation_m3634 (Transform_t35 * __this, const MethodInfo* method)
{
	Quaternion_t41  V_0 = {0};
	{
		Transform_INTERNAL_get_localRotation_m4379(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t41  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" void Transform_set_localRotation_m1580 (Transform_t35 * __this, Quaternion_t41  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localRotation_m4380(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_localRotation_m4379 (Transform_t35 * __this, Quaternion_t41 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m4379_ftn) (Transform_t35 *, Quaternion_t41 *);
	static Transform_INTERNAL_get_localRotation_m4379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m4379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_localRotation_m4380 (Transform_t35 * __this, Quaternion_t41 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m4380_ftn) (Transform_t35 *, Quaternion_t41 *);
	static Transform_INTERNAL_set_localRotation_m4380_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m4380_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C" Vector3_t36  Transform_get_localScale_m1419 (Transform_t35 * __this, const MethodInfo* method)
{
	Vector3_t36  V_0 = {0};
	{
		Transform_INTERNAL_get_localScale_m4381(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t36  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" void Transform_set_localScale_m1393 (Transform_t35 * __this, Vector3_t36  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localScale_m4382(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localScale_m4381 (Transform_t35 * __this, Vector3_t36 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m4381_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_get_localScale_m4381_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m4381_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localScale_m4382 (Transform_t35 * __this, Vector3_t36 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m4382_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_set_localScale_m4382_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m4382_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" Transform_t35 * Transform_get_parent_m1342 (Transform_t35 * __this, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = Transform_get_parentInternal_m4383(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern TypeInfo* RectTransform_t556_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral562;
extern "C" void Transform_set_parent_m1329 (Transform_t35 * __this, Transform_t35 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		_stringLiteral562 = il2cpp_codegen_string_literal_from_index(562);
		s_Il2CppMethodIntialized = true;
	}
	{
		if (!((RectTransform_t556 *)IsInstSealed(__this, RectTransform_t556_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogWarning_m3722(NULL /*static, unused*/, _stringLiteral562, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Transform_t35 * L_0 = ___value;
		Transform_set_parentInternal_m4384(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C" Transform_t35 * Transform_get_parentInternal_m4383 (Transform_t35 * __this, const MethodInfo* method)
{
	typedef Transform_t35 * (*Transform_get_parentInternal_m4383_ftn) (Transform_t35 *);
	static Transform_get_parentInternal_m4383_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m4383_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C" void Transform_set_parentInternal_m4384 (Transform_t35 * __this, Transform_t35 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_set_parentInternal_m4384_ftn) (Transform_t35 *, Transform_t35 *);
	static Transform_set_parentInternal_m4384_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m4384_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C" void Transform_SetParent_m3629 (Transform_t35 * __this, Transform_t35 * ___parent, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = ___parent;
		Transform_SetParent_m3413(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void Transform_SetParent_m3413 (Transform_t35 * __this, Transform_t35 * ___parent, bool ___worldPositionStays, const MethodInfo* method)
{
	typedef void (*Transform_SetParent_m3413_ftn) (Transform_t35 *, Transform_t35 *, bool);
	static Transform_SetParent_m3413_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m3413_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent, ___worldPositionStays);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C" Matrix4x4_t404  Transform_get_worldToLocalMatrix_m3692 (Transform_t35 * __this, const MethodInfo* method)
{
	Matrix4x4_t404  V_0 = {0};
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m4385(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t404  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C" void Transform_INTERNAL_get_worldToLocalMatrix_m4385 (Transform_t35 * __this, Matrix4x4_t404 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m4385_ftn) (Transform_t35 *, Matrix4x4_t404 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m4385_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m4385_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C" Matrix4x4_t404  Transform_get_localToWorldMatrix_m1895 (Transform_t35 * __this, const MethodInfo* method)
{
	Matrix4x4_t404  V_0 = {0};
	{
		Transform_INTERNAL_get_localToWorldMatrix_m4386(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t404  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
extern "C" void Transform_INTERNAL_get_localToWorldMatrix_m4386 (Transform_t35 * __this, Matrix4x4_t404 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localToWorldMatrix_m4386_ftn) (Transform_t35 *, Matrix4x4_t404 *);
	static Transform_INTERNAL_get_localToWorldMatrix_m4386_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localToWorldMatrix_m4386_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
extern "C" void Transform_Translate_m1756 (Transform_t35 * __this, Vector3_t36  ___translation, int32_t ___relativeTo, const MethodInfo* method)
{
	{
		int32_t L_0 = ___relativeTo;
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t36  L_1 = Transform_get_position_m1388(__this, /*hidden argument*/NULL);
		Vector3_t36  L_2 = ___translation;
		Vector3_t36  L_3 = Vector3_op_Addition_m1389(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Transform_set_position_m1383(__this, L_3, /*hidden argument*/NULL);
		goto IL_0035;
	}

IL_001d:
	{
		Vector3_t36  L_4 = Transform_get_position_m1388(__this, /*hidden argument*/NULL);
		Vector3_t36  L_5 = ___translation;
		Vector3_t36  L_6 = Transform_TransformDirection_m4388(__this, L_5, /*hidden argument*/NULL);
		Vector3_t36  L_7 = Vector3_op_Addition_m1389(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Transform_set_position_m1383(__this, L_7, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C" void Transform_Rotate_m1772 (Transform_t35 * __this, Vector3_t36  ___eulerAngles, int32_t ___relativeTo, const MethodInfo* method)
{
	Quaternion_t41  V_0 = {0};
	{
		float L_0 = ((&___eulerAngles)->___x_1);
		float L_1 = ((&___eulerAngles)->___y_2);
		float L_2 = ((&___eulerAngles)->___z_3);
		Quaternion_t41  L_3 = Quaternion_Euler_m4058(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = ___relativeTo;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0039;
		}
	}
	{
		Quaternion_t41  L_5 = Transform_get_localRotation_m3634(__this, /*hidden argument*/NULL);
		Quaternion_t41  L_6 = V_0;
		Quaternion_t41  L_7 = Quaternion_op_Multiply_m4067(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Transform_set_localRotation_m1580(__this, L_7, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_0039:
	{
		Quaternion_t41  L_8 = Transform_get_rotation_m1903(__this, /*hidden argument*/NULL);
		Quaternion_t41  L_9 = Transform_get_rotation_m1903(__this, /*hidden argument*/NULL);
		Quaternion_t41  L_10 = Quaternion_Inverse_m3709(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Quaternion_t41  L_11 = V_0;
		Quaternion_t41  L_12 = Quaternion_op_Multiply_m4067(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Quaternion_t41  L_13 = Transform_get_rotation_m1903(__this, /*hidden argument*/NULL);
		Quaternion_t41  L_14 = Quaternion_op_Multiply_m4067(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Quaternion_t41  L_15 = Quaternion_op_Multiply_m4067(NULL /*static, unused*/, L_8, L_14, /*hidden argument*/NULL);
		Transform_set_rotation_m1391(__this, L_15, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform,UnityEngine.Vector3)
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern "C" void Transform_LookAt_m1737 (Transform_t35 * __this, Transform_t35 * ___target, Vector3_t36  ___worldUp, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = ___target;
		bool L_1 = Object_op_Implicit_m1320(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Transform_t35 * L_2 = ___target;
		NullCheck(L_2);
		Vector3_t36  L_3 = Transform_get_position_m1388(L_2, /*hidden argument*/NULL);
		Vector3_t36  L_4 = ___worldUp;
		Transform_LookAt_m1738(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Transform_LookAt_m1738 (Transform_t35 * __this, Vector3_t36  ___worldPosition, Vector3_t36  ___worldUp, const MethodInfo* method)
{
	{
		Transform_INTERNAL_CALL_LookAt_m4387(NULL /*static, unused*/, __this, (&___worldPosition), (&___worldUp), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_CALL_LookAt_m4387 (Object_t * __this /* static, unused */, Transform_t35 * ___self, Vector3_t36 * ___worldPosition, Vector3_t36 * ___worldUp, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_LookAt_m4387_ftn) (Transform_t35 *, Vector3_t36 *, Vector3_t36 *);
	static Transform_INTERNAL_CALL_LookAt_m4387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_LookAt_m4387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self, ___worldPosition, ___worldUp);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C" Vector3_t36  Transform_TransformDirection_m4388 (Transform_t35 * __this, Vector3_t36  ___direction, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = Transform_INTERNAL_CALL_TransformDirection_m4389(NULL /*static, unused*/, __this, (&___direction), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t36  Transform_INTERNAL_CALL_TransformDirection_m4389 (Object_t * __this /* static, unused */, Transform_t35 * ___self, Vector3_t36 * ___direction, const MethodInfo* method)
{
	typedef Vector3_t36  (*Transform_INTERNAL_CALL_TransformDirection_m4389_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_CALL_TransformDirection_m4389_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformDirection_m4389_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___direction);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t36  Transform_TransformPoint_m3710 (Transform_t35 * __this, Vector3_t36  ___position, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = Transform_INTERNAL_CALL_TransformPoint_m4390(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t36  Transform_INTERNAL_CALL_TransformPoint_m4390 (Object_t * __this /* static, unused */, Transform_t35 * ___self, Vector3_t36 * ___position, const MethodInfo* method)
{
	typedef Vector3_t36  (*Transform_INTERNAL_CALL_TransformPoint_m4390_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_CALL_TransformPoint_m4390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m4390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t36  Transform_InverseTransformPoint_m3470 (Transform_t35 * __this, Vector3_t36  ___position, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = Transform_INTERNAL_CALL_InverseTransformPoint_m4391(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t36  Transform_INTERNAL_CALL_InverseTransformPoint_m4391 (Object_t * __this /* static, unused */, Transform_t35 * ___self, Vector3_t36 * ___position, const MethodInfo* method)
{
	typedef Vector3_t36  (*Transform_INTERNAL_CALL_InverseTransformPoint_m4391_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m4391_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m4391_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Transform UnityEngine.Transform::get_root()
extern "C" Transform_t35 * Transform_get_root_m4392 (Transform_t35 * __this, const MethodInfo* method)
{
	typedef Transform_t35 * (*Transform_get_root_m4392_ftn) (Transform_t35 *);
	static Transform_get_root_m4392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_root_m4392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_root()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" int32_t Transform_get_childCount_m3416 (Transform_t35 * __this, const MethodInfo* method)
{
	typedef int32_t (*Transform_get_childCount_m3416_ftn) (Transform_t35 *);
	static Transform_get_childCount_m3416_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m3416_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C" void Transform_SetAsFirstSibling_m3630 (Transform_t35 * __this, const MethodInfo* method)
{
	typedef void (*Transform_SetAsFirstSibling_m3630_ftn) (Transform_t35 *);
	static Transform_SetAsFirstSibling_m3630_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m3630_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" Transform_t35 * Transform_Find_m1340 (Transform_t35 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef Transform_t35 * (*Transform_Find_m1340_ftn) (Transform_t35 *, String_t*);
	static Transform_Find_m1340_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_Find_m1340_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::Find(System.String)");
	return _il2cpp_icall_func(__this, ___name);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C" Vector3_t36  Transform_get_lossyScale_m1904 (Transform_t35 * __this, const MethodInfo* method)
{
	Vector3_t36  V_0 = {0};
	{
		Transform_INTERNAL_get_lossyScale_m4393(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t36  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_lossyScale_m4393 (Transform_t35 * __this, Vector3_t36 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_lossyScale_m4393_ftn) (Transform_t35 *, Vector3_t36 *);
	static Transform_INTERNAL_get_lossyScale_m4393_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_lossyScale_m4393_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern "C" bool Transform_IsChildOf_m3452 (Transform_t35 * __this, Transform_t35 * ___parent, const MethodInfo* method)
{
	typedef bool (*Transform_IsChildOf_m3452_ftn) (Transform_t35 *, Transform_t35 *);
	static Transform_IsChildOf_m3452_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_IsChildOf_m3452_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::IsChildOf(UnityEngine.Transform)");
	return _il2cpp_icall_func(__this, ___parent);
}
// UnityEngine.Transform UnityEngine.Transform::FindChild(System.String)
extern "C" Transform_t35 * Transform_FindChild_m1873 (Transform_t35 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Transform_t35 * L_1 = Transform_Find_m1340(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_EnumeratorMethodDeclarations.h"
extern TypeInfo* Enumerator_t850_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_GetEnumerator_m4394 (Transform_t35 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t850_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(584);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t850 * L_0 = (Enumerator_t850 *)il2cpp_codegen_object_new (Enumerator_t850_il2cpp_TypeInfo_var);
		Enumerator__ctor_m4368(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" Transform_t35 * Transform_GetChild_m3415 (Transform_t35 * __this, int32_t ___index, const MethodInfo* method)
{
	typedef Transform_t35 * (*Transform_GetChild_m3415_ftn) (Transform_t35 *, int32_t);
	static Transform_GetChild_m3415_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m3415_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, ___index);
}
// UnityEngine.Time
#include "UnityEngine_UnityEngine_Time.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// System.Single UnityEngine.Time::get_time()
extern "C" float Time_get_time_m1330 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_time_m1330_ftn) ();
	static Time_get_time_m1330_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_time_m1330_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_time()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" float Time_get_deltaTime_m1379 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_deltaTime_m1379_ftn) ();
	static Time_get_deltaTime_m1379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m1379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C" float Time_get_unscaledTime_m3347 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledTime_m3347_ftn) ();
	static Time_get_unscaledTime_m3347_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m3347_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C" float Time_get_unscaledDeltaTime_m3398 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m3398_ftn) ();
	static Time_get_unscaledDeltaTime_m3398_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m3398_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_fixedDeltaTime()
extern "C" float Time_get_fixedDeltaTime_m1528 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_fixedDeltaTime_m1528_ftn) ();
	static Time_get_fixedDeltaTime_m1528_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_fixedDeltaTime_m1528_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_fixedDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_timeScale()
extern "C" float Time_get_timeScale_m1526 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_timeScale_m1526_ftn) ();
	static Time_get_timeScale_m1526_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeScale_m1526_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeScale()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Time::set_timeScale(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void Time_set_timeScale_m1527 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method)
{
	typedef void (*Time_set_timeScale_m1527_ftn) (float);
	static Time_set_timeScale_m1527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_set_timeScale_m1527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::set_timeScale(System.Single)");
	_il2cpp_icall_func(___value);
}
// System.Int32 UnityEngine.Time::get_frameCount()
extern "C" int32_t Time_get_frameCount_m1572 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Time_get_frameCount_m1572_ftn) ();
	static Time_get_frameCount_m1572_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_frameCount_m1572_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_frameCount()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C" float Time_get_realtimeSinceStartup_m1791 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m1791_ftn) ();
	static Time_get_realtimeSinceStartup_m1791_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m1791_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	return _il2cpp_icall_func();
}
// UnityEngine.Random
#include "UnityEngine_UnityEngine_Random.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
// System.Void UnityEngine.Random::set_seed(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void Random_set_seed_m1331 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Random_set_seed_m1331_ftn) (int32_t);
	static Random_set_seed_m1331_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_set_seed_m1331_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::set_seed(System.Int32)");
	_il2cpp_icall_func(___value);
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" float Random_Range_m1571 (Object_t * __this /* static, unused */, float ___min, float ___max, const MethodInfo* method)
{
	typedef float (*Random_Range_m1571_ftn) (float, float);
	static Random_Range_m1571_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m1571_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min, ___max);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
extern "C" int32_t Random_Range_m1332 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		int32_t L_0 = ___min;
		int32_t L_1 = ___max;
		int32_t L_2 = Random_RandomRangeInt_m4395(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C" int32_t Random_RandomRangeInt_m4395 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m4395_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m4395_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m4395_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min, ___max);
}
// System.Single UnityEngine.Random::get_value()
extern "C" float Random_get_value_m1654 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Random_get_value_m1654_ftn) ();
	static Random_get_value_m1654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_get_value_m1654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::get_value()");
	return _il2cpp_icall_func();
}
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstruction.h"
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstructionMethodDeclarations.h"
// System.Void UnityEngine.YieldInstruction::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void YieldInstruction__ctor_m4396 (YieldInstruction_t791 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t791_marshal(const YieldInstruction_t791& unmarshaled, YieldInstruction_t791_marshaled& marshaled)
{
}
extern "C" void YieldInstruction_t791_marshal_back(const YieldInstruction_t791_marshaled& marshaled, YieldInstruction_t791& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t791_marshal_cleanup(YieldInstruction_t791_marshaled& marshaled)
{
}
// UnityEngine.PlayerPrefsException
#include "UnityEngine_UnityEngine_PlayerPrefsException.h"
// UnityEngine.PlayerPrefsException
#include "UnityEngine_UnityEngine_PlayerPrefsExceptionMethodDeclarations.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
// System.String
#include "mscorlib_System_String.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
extern "C" void PlayerPrefsException__ctor_m4397 (PlayerPrefsException_t853 * __this, String_t* ___error, const MethodInfo* method)
{
	{
		String_t* L_0 = ___error;
		Exception__ctor_m1318(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefs.h"
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
// System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" bool PlayerPrefs_TrySetSetString_m4398 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___value, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetSetString_m4398_ftn) (String_t*, String_t*);
	static PlayerPrefs_TrySetSetString_m4398_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetSetString_m4398_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)");
	return _il2cpp_icall_func(___key, ___value);
}
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
// UnityEngine.PlayerPrefsException
#include "UnityEngine_UnityEngine_PlayerPrefsExceptionMethodDeclarations.h"
extern TypeInfo* PlayerPrefsException_t853_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral563;
extern "C" void PlayerPrefs_SetString_m1671 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerPrefsException_t853_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(585);
		_stringLiteral563 = il2cpp_codegen_string_literal_from_index(563);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key;
		String_t* L_1 = ___value;
		bool L_2 = PlayerPrefs_TrySetSetString_m4398(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t853 * L_3 = (PlayerPrefsException_t853 *)il2cpp_codegen_object_new (PlayerPrefsException_t853_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m4397(L_3, _stringLiteral563, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C" String_t* PlayerPrefs_GetString_m1662 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___defaultValue, const MethodInfo* method)
{
	typedef String_t* (*PlayerPrefs_GetString_m1662_ftn) (String_t*, String_t*);
	static PlayerPrefs_GetString_m1662_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetString_m1662_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetString(System.String,System.String)");
	return _il2cpp_icall_func(___key, ___defaultValue);
}
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C" bool PlayerPrefs_HasKey_m1661 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_HasKey_m1661_ftn) (String_t*);
	static PlayerPrefs_HasKey_m1661_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_HasKey_m1661_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::HasKey(System.String)");
	return _il2cpp_icall_func(___key);
}
// System.Void UnityEngine.PlayerPrefs::Save()
extern "C" void PlayerPrefs_Save_m1672 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_Save_m1672_ftn) ();
	static PlayerPrefs_Save_m1672_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_Save_m1672_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::Save()");
	_il2cpp_icall_func();
}
// UnityEngine.iOS.ActivityIndicatorStyle
#include "UnityEngine_UnityEngine_iOS_ActivityIndicatorStyle.h"
// UnityEngine.iOS.ActivityIndicatorStyle
#include "UnityEngine_UnityEngine_iOS_ActivityIndicatorStyleMethodDeclarations.h"
// UnityEngine.Experimental.Director.DirectorPlayer
#include "UnityEngine_UnityEngine_Experimental_Director_DirectorPlayer.h"
// UnityEngine.Experimental.Director.DirectorPlayer
#include "UnityEngine_UnityEngine_Experimental_Director_DirectorPlayerMethodDeclarations.h"
// UnityEngine.Advertisements.UnityAdsInternal
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsInternal.h"
// UnityEngine.Advertisements.UnityAdsInternal
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsInternalMethodDeclarations.h"
// UnityEngine.Advertisements.UnityAdsDelegate
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate_2_ge.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// UnityEngine.Advertisements.UnityAdsDelegate
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegateMethodDeclarations.h"
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate_2_geMethodDeclarations.h"
// System.Void UnityEngine.Advertisements.UnityAdsInternal::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void UnityAdsInternal__ctor_m4399 (UnityAdsInternal_t857 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onCampaignsAvailable(UnityEngine.Advertisements.UnityAdsDelegate)
// UnityEngine.Advertisements.UnityAdsDelegate
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t858_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onCampaignsAvailable_m4400 (Object_t * __this /* static, unused */, UnityAdsDelegate_t858 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_t858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0;
		UnityAdsDelegate_t858 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Combine_m3530(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0 = ((UnityAdsDelegate_t858 *)CastclassSealed(L_2, UnityAdsDelegate_t858_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onCampaignsAvailable(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t858_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onCampaignsAvailable_m4401 (Object_t * __this /* static, unused */, UnityAdsDelegate_t858 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_t858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0;
		UnityAdsDelegate_t858 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Remove_m3531(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0 = ((UnityAdsDelegate_t858 *)CastclassSealed(L_2, UnityAdsDelegate_t858_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onCampaignsFetchFailed(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t858_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onCampaignsFetchFailed_m4402 (Object_t * __this /* static, unused */, UnityAdsDelegate_t858 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_t858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1;
		UnityAdsDelegate_t858 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Combine_m3530(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1 = ((UnityAdsDelegate_t858 *)CastclassSealed(L_2, UnityAdsDelegate_t858_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onCampaignsFetchFailed(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t858_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onCampaignsFetchFailed_m4403 (Object_t * __this /* static, unused */, UnityAdsDelegate_t858 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_t858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1;
		UnityAdsDelegate_t858 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Remove_m3531(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1 = ((UnityAdsDelegate_t858 *)CastclassSealed(L_2, UnityAdsDelegate_t858_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onShow(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t858_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onShow_m4404 (Object_t * __this /* static, unused */, UnityAdsDelegate_t858 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_t858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onShow_2;
		UnityAdsDelegate_t858 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Combine_m3530(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onShow_2 = ((UnityAdsDelegate_t858 *)CastclassSealed(L_2, UnityAdsDelegate_t858_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onShow(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t858_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onShow_m4405 (Object_t * __this /* static, unused */, UnityAdsDelegate_t858 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_t858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onShow_2;
		UnityAdsDelegate_t858 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Remove_m3531(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onShow_2 = ((UnityAdsDelegate_t858 *)CastclassSealed(L_2, UnityAdsDelegate_t858_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onHide(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t858_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onHide_m4406 (Object_t * __this /* static, unused */, UnityAdsDelegate_t858 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_t858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onHide_3;
		UnityAdsDelegate_t858 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Combine_m3530(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onHide_3 = ((UnityAdsDelegate_t858 *)CastclassSealed(L_2, UnityAdsDelegate_t858_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onHide(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t858_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onHide_m4407 (Object_t * __this /* static, unused */, UnityAdsDelegate_t858 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_t858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onHide_3;
		UnityAdsDelegate_t858 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Remove_m3531(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onHide_3 = ((UnityAdsDelegate_t858 *)CastclassSealed(L_2, UnityAdsDelegate_t858_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onVideoCompleted(UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>)
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate_2_ge.h"
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_2_t859_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onVideoCompleted_m4408 (Object_t * __this /* static, unused */, UnityAdsDelegate_2_t859 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_2_t859_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(588);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_2_t859 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4;
		UnityAdsDelegate_2_t859 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Combine_m3530(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4 = ((UnityAdsDelegate_2_t859 *)CastclassSealed(L_2, UnityAdsDelegate_2_t859_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onVideoCompleted(UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>)
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_2_t859_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onVideoCompleted_m4409 (Object_t * __this /* static, unused */, UnityAdsDelegate_2_t859 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_2_t859_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(588);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_2_t859 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4;
		UnityAdsDelegate_2_t859 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Remove_m3531(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4 = ((UnityAdsDelegate_2_t859 *)CastclassSealed(L_2, UnityAdsDelegate_2_t859_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onVideoStarted(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t858_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onVideoStarted_m4410 (Object_t * __this /* static, unused */, UnityAdsDelegate_t858 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_t858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5;
		UnityAdsDelegate_t858 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Combine_m3530(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5 = ((UnityAdsDelegate_t858 *)CastclassSealed(L_2, UnityAdsDelegate_t858_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onVideoStarted(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t858_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onVideoStarted_m4411 (Object_t * __this /* static, unused */, UnityAdsDelegate_t858 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_t858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5;
		UnityAdsDelegate_t858 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Remove_m3531(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5 = ((UnityAdsDelegate_t858 *)CastclassSealed(L_2, UnityAdsDelegate_t858_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::RegisterNative()
extern "C" void UnityAdsInternal_RegisterNative_m4412 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_RegisterNative_m4412_ftn) ();
	static UnityAdsInternal_RegisterNative_m4412_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_RegisterNative_m4412_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::RegisterNative()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::Init(System.String,System.Boolean,System.Boolean,System.String)
// System.String
#include "mscorlib_System_String.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void UnityAdsInternal_Init_m4413 (Object_t * __this /* static, unused */, String_t* ___gameId, bool ___testModeEnabled, bool ___debugModeEnabled, String_t* ___unityVersion, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_Init_m4413_ftn) (String_t*, bool, bool, String_t*);
	static UnityAdsInternal_Init_m4413_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_Init_m4413_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::Init(System.String,System.Boolean,System.Boolean,System.String)");
	_il2cpp_icall_func(___gameId, ___testModeEnabled, ___debugModeEnabled, ___unityVersion);
}
// System.Boolean UnityEngine.Advertisements.UnityAdsInternal::Show(System.String,System.String,System.String)
extern "C" bool UnityAdsInternal_Show_m4414 (Object_t * __this /* static, unused */, String_t* ___zoneId, String_t* ___rewardItemKey, String_t* ___options, const MethodInfo* method)
{
	typedef bool (*UnityAdsInternal_Show_m4414_ftn) (String_t*, String_t*, String_t*);
	static UnityAdsInternal_Show_m4414_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_Show_m4414_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::Show(System.String,System.String,System.String)");
	return _il2cpp_icall_func(___zoneId, ___rewardItemKey, ___options);
}
// System.Boolean UnityEngine.Advertisements.UnityAdsInternal::CanShowAds(System.String)
extern "C" bool UnityAdsInternal_CanShowAds_m4415 (Object_t * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	typedef bool (*UnityAdsInternal_CanShowAds_m4415_ftn) (String_t*);
	static UnityAdsInternal_CanShowAds_m4415_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_CanShowAds_m4415_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::CanShowAds(System.String)");
	return _il2cpp_icall_func(___zoneId);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::SetLogLevel(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void UnityAdsInternal_SetLogLevel_m4416 (Object_t * __this /* static, unused */, int32_t ___logLevel, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_SetLogLevel_m4416_ftn) (int32_t);
	static UnityAdsInternal_SetLogLevel_m4416_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_SetLogLevel_m4416_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::SetLogLevel(System.Int32)");
	_il2cpp_icall_func(___logLevel);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::SetCampaignDataURL(System.String)
extern "C" void UnityAdsInternal_SetCampaignDataURL_m4417 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_SetCampaignDataURL_m4417_ftn) (String_t*);
	static UnityAdsInternal_SetCampaignDataURL_m4417_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_SetCampaignDataURL_m4417_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::SetCampaignDataURL(System.String)");
	_il2cpp_icall_func(___url);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::RemoveAllEventHandlers()
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_RemoveAllEventHandlers_m4418 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		s_Il2CppMethodIntialized = true;
	}
	{
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0 = (UnityAdsDelegate_t858 *)NULL;
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1 = (UnityAdsDelegate_t858 *)NULL;
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onShow_2 = (UnityAdsDelegate_t858 *)NULL;
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onHide_3 = (UnityAdsDelegate_t858 *)NULL;
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4 = (UnityAdsDelegate_2_t859 *)NULL;
		((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5 = (UnityAdsDelegate_t858 *)NULL;
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsCampaignsAvailable()
// UnityEngine.Advertisements.UnityAdsDelegate
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegateMethodDeclarations.h"
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsAvailable_m4419 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t858 * V_0 = {0};
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0;
		V_0 = L_0;
		UnityAdsDelegate_t858 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t858 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m5096(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsCampaignsFetchFailed()
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m4420 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t858 * V_0 = {0};
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1;
		V_0 = L_0;
		UnityAdsDelegate_t858 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t858 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m5096(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsShow()
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsShow_m4421 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t858 * V_0 = {0};
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onShow_2;
		V_0 = L_0;
		UnityAdsDelegate_t858 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t858 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m5096(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsHide()
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsHide_m4422 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t858 * V_0 = {0};
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onHide_3;
		V_0 = L_0;
		UnityAdsDelegate_t858 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t858 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m5096(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsVideoCompleted(System.String,System.Boolean)
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate_2_geMethodDeclarations.h"
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAdsDelegate_2_Invoke_m5137_MethodInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsVideoCompleted_m4423 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, bool ___skipped, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		UnityAdsDelegate_2_Invoke_m5137_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484223);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_2_t859 * V_0 = {0};
	{
		UnityAdsDelegate_2_t859 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4;
		V_0 = L_0;
		UnityAdsDelegate_2_t859 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		UnityAdsDelegate_2_t859 * L_2 = V_0;
		String_t* L_3 = ___rewardItemKey;
		bool L_4 = ___skipped;
		NullCheck(L_2);
		UnityAdsDelegate_2_Invoke_m5137(L_2, L_3, L_4, /*hidden argument*/UnityAdsDelegate_2_Invoke_m5137_MethodInfo_var);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsVideoStarted()
extern TypeInfo* UnityAdsInternal_t857_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsVideoStarted_m4424 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t857_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(586);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t858 * V_0 = {0};
	{
		UnityAdsDelegate_t858 * L_0 = ((UnityAdsInternal_t857_StaticFields*)UnityAdsInternal_t857_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5;
		V_0 = L_0;
		UnityAdsDelegate_t858 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t858 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m5096(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.Particle
#include "UnityEngine_UnityEngine_Particle.h"
// UnityEngine.Particle
#include "UnityEngine_UnityEngine_ParticleMethodDeclarations.h"
// UnityEngine.Vector3 UnityEngine.Particle::get_position()
extern "C" Vector3_t36  Particle_get_position_m4425 (Particle_t860 * __this, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = (__this->___m_Position_0);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_position(UnityEngine.Vector3)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
extern "C" void Particle_set_position_m4426 (Particle_t860 * __this, Vector3_t36  ___value, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = ___value;
		__this->___m_Position_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Particle::get_velocity()
extern "C" Vector3_t36  Particle_get_velocity_m4427 (Particle_t860 * __this, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = (__this->___m_Velocity_1);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_velocity(UnityEngine.Vector3)
extern "C" void Particle_set_velocity_m4428 (Particle_t860 * __this, Vector3_t36  ___value, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = ___value;
		__this->___m_Velocity_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_energy()
extern "C" float Particle_get_energy_m4429 (Particle_t860 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Energy_5);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_energy(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void Particle_set_energy_m4430 (Particle_t860 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Energy_5 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_startEnergy()
extern "C" float Particle_get_startEnergy_m4431 (Particle_t860 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_StartEnergy_6);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_startEnergy(System.Single)
extern "C" void Particle_set_startEnergy_m4432 (Particle_t860 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_StartEnergy_6 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_size()
extern "C" float Particle_get_size_m4433 (Particle_t860 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Size_2);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_size(System.Single)
extern "C" void Particle_set_size_m4434 (Particle_t860 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Size_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_rotation()
extern "C" float Particle_get_rotation_m4435 (Particle_t860 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Rotation_3);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_rotation(System.Single)
extern "C" void Particle_set_rotation_m4436 (Particle_t860 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Rotation_3 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_angularVelocity()
extern "C" float Particle_get_angularVelocity_m4437 (Particle_t860 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_AngularVelocity_4);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_angularVelocity(System.Single)
extern "C" void Particle_set_angularVelocity_m4438 (Particle_t860 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_AngularVelocity_4 = L_0;
		return;
	}
}
// UnityEngine.Color UnityEngine.Particle::get_color()
extern "C" Color_t9  Particle_get_color_m4439 (Particle_t860 * __this, const MethodInfo* method)
{
	{
		Color_t9  L_0 = (__this->___m_Color_7);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_color(UnityEngine.Color)
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
extern "C" void Particle_set_color_m4440 (Particle_t860 * __this, Color_t9  ___value, const MethodInfo* method)
{
	{
		Color_t9  L_0 = ___value;
		__this->___m_Color_7 = L_0;
		return;
	}
}
// UnityEngine.Collision
#include "UnityEngine_UnityEngine_Collision.h"
// UnityEngine.Collision
#include "UnityEngine_UnityEngine_CollisionMethodDeclarations.h"
// UnityEngine.QueryTriggerInteraction
#include "UnityEngine_UnityEngine_QueryTriggerInteraction.h"
// UnityEngine.QueryTriggerInteraction
#include "UnityEngine_UnityEngine_QueryTriggerInteractionMethodDeclarations.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_Physics.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// System.Single
#include "mscorlib_System_Single.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.QueryTriggerInteraction
#include "UnityEngine_UnityEngine_QueryTriggerInteraction.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
extern "C" bool Physics_Raycast_m4441 (Object_t * __this /* static, unused */, Vector3_t36  ___origin, Vector3_t36  ___direction, RaycastHit_t60 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = ___origin;
		Vector3_t36  L_1 = ___direction;
		RaycastHit_t60 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		int32_t L_5 = ___queryTriggerInteraction;
		bool L_6 = Physics_Internal_Raycast_m4446(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
extern "C" bool Physics_Raycast_m3537 (Object_t * __this /* static, unused */, Ray_t382  ___ray, RaycastHit_t60 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		Ray_t382  L_0 = ___ray;
		RaycastHit_t60 * L_1 = ___hitInfo;
		float L_2 = ___maxDistance;
		int32_t L_3 = ___layerMask;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m4442(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
extern "C" bool Physics_Raycast_m4442 (Object_t * __this /* static, unused */, Ray_t382  ___ray, RaycastHit_t60 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = Ray_get_origin_m3370((&___ray), /*hidden argument*/NULL);
		Vector3_t36  L_1 = Ray_get_direction_m3371((&___ray), /*hidden argument*/NULL);
		RaycastHit_t60 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		int32_t L_5 = ___queryTriggerInteraction;
		bool L_6 = Physics_Raycast_m4441(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t731* Physics_RaycastAll_m3383 (Object_t * __this /* static, unused */, Ray_t382  ___ray, float ___maxDistance, int32_t ___layerMask, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		Ray_t382  L_0 = ___ray;
		float L_1 = ___maxDistance;
		int32_t L_2 = ___layerMask;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t731* L_4 = Physics_RaycastAll_m4443(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" RaycastHitU5BU5D_t731* Physics_RaycastAll_m4443 (Object_t * __this /* static, unused */, Ray_t382  ___ray, float ___maxDistance, int32_t ___layerMask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = Ray_get_origin_m3370((&___ray), /*hidden argument*/NULL);
		Vector3_t36  L_1 = Ray_get_direction_m3371((&___ray), /*hidden argument*/NULL);
		float L_2 = ___maxDistance;
		int32_t L_3 = ___layerMask;
		int32_t L_4 = ___queryTriggerInteraction;
		RaycastHitU5BU5D_t731* L_5 = Physics_RaycastAll_m4444(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" RaycastHitU5BU5D_t731* Physics_RaycastAll_m4444 (Object_t * __this /* static, unused */, Vector3_t36  ___origin, Vector3_t36  ___direction, float ___maxDistance, int32_t ___layermask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		float L_0 = ___maxDistance;
		int32_t L_1 = ___layermask;
		int32_t L_2 = ___queryTriggerInteraction;
		RaycastHitU5BU5D_t731* L_3 = Physics_INTERNAL_CALL_RaycastAll_m4445(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" RaycastHitU5BU5D_t731* Physics_INTERNAL_CALL_RaycastAll_m4445 (Object_t * __this /* static, unused */, Vector3_t36 * ___origin, Vector3_t36 * ___direction, float ___maxDistance, int32_t ___layermask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t731* (*Physics_INTERNAL_CALL_RaycastAll_m4445_ftn) (Vector3_t36 *, Vector3_t36 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m4445_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m4445_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin, ___direction, ___maxDistance, ___layermask, ___queryTriggerInteraction);
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" bool Physics_Internal_Raycast_m4446 (Object_t * __this /* static, unused */, Vector3_t36  ___origin, Vector3_t36  ___direction, RaycastHit_t60 * ___hitInfo, float ___maxDistance, int32_t ___layermask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		RaycastHit_t60 * L_0 = ___hitInfo;
		float L_1 = ___maxDistance;
		int32_t L_2 = ___layermask;
		int32_t L_3 = ___queryTriggerInteraction;
		bool L_4 = Physics_INTERNAL_CALL_Internal_Raycast_m4447(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" bool Physics_INTERNAL_CALL_Internal_Raycast_m4447 (Object_t * __this /* static, unused */, Vector3_t36 * ___origin, Vector3_t36 * ___direction, RaycastHit_t60 * ___hitInfo, float ___maxDistance, int32_t ___layermask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m4447_ftn) (Vector3_t36 *, Vector3_t36 *, RaycastHit_t60 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m4447_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m4447_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin, ___direction, ___hitInfo, ___maxDistance, ___layermask, ___queryTriggerInteraction);
}
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPoint.h"
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPointMethodDeclarations.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
extern "C" void Rigidbody_MovePosition_m1769 (Rigidbody_t447 * __this, Vector3_t36  ___position, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MovePosition_m4448(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"
extern "C" void Rigidbody_INTERNAL_CALL_MovePosition_m4448 (Object_t * __this /* static, unused */, Rigidbody_t447 * ___self, Vector3_t36 * ___position, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MovePosition_m4448_ftn) (Rigidbody_t447 *, Vector3_t36 *);
	static Rigidbody_INTERNAL_CALL_MovePosition_m4448_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MovePosition_m4448_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self, ___position);
}
// System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
extern "C" void Rigidbody_MoveRotation_m1771 (Rigidbody_t447 * __this, Quaternion_t41  ___rot, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MoveRotation_m4449(NULL /*static, unused*/, __this, (&___rot), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C" void Rigidbody_INTERNAL_CALL_MoveRotation_m4449 (Object_t * __this /* static, unused */, Rigidbody_t447 * ___self, Quaternion_t41 * ___rot, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MoveRotation_m4449_ftn) (Rigidbody_t447 *, Quaternion_t41 *);
	static Rigidbody_INTERNAL_CALL_MoveRotation_m4449_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MoveRotation_m4449_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self, ___rot);
}
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void Collider_set_enabled_m1348 (Collider_t369 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Collider_set_enabled_m1348_ftn) (Collider_t369 *, bool);
	static Collider_set_enabled_m1348_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_set_enabled_m1348_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Collider::Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
extern "C" bool Collider_Internal_Raycast_m4450 (Object_t * __this /* static, unused */, Collider_t369 * ___col, Ray_t382  ___ray, RaycastHit_t60 * ___hitInfo, float ___maxDistance, const MethodInfo* method)
{
	{
		Collider_t369 * L_0 = ___col;
		RaycastHit_t60 * L_1 = ___hitInfo;
		float L_2 = ___maxDistance;
		bool L_3 = Collider_INTERNAL_CALL_Internal_Raycast_m4451(NULL /*static, unused*/, L_0, (&___ray), L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Collider::INTERNAL_CALL_Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray&,UnityEngine.RaycastHit&,System.Single)
extern "C" bool Collider_INTERNAL_CALL_Internal_Raycast_m4451 (Object_t * __this /* static, unused */, Collider_t369 * ___col, Ray_t382 * ___ray, RaycastHit_t60 * ___hitInfo, float ___maxDistance, const MethodInfo* method)
{
	typedef bool (*Collider_INTERNAL_CALL_Internal_Raycast_m4451_ftn) (Collider_t369 *, Ray_t382 *, RaycastHit_t60 *, float);
	static Collider_INTERNAL_CALL_Internal_Raycast_m4451_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_INTERNAL_CALL_Internal_Raycast_m4451_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::INTERNAL_CALL_Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray&,UnityEngine.RaycastHit&,System.Single)");
	return _il2cpp_icall_func(___col, ___ray, ___hitInfo, ___maxDistance);
}
// System.Boolean UnityEngine.Collider::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C" bool Collider_Raycast_m1421 (Collider_t369 * __this, Ray_t382  ___ray, RaycastHit_t60 * ___hitInfo, float ___maxDistance, const MethodInfo* method)
{
	{
		Ray_t382  L_0 = ___ray;
		RaycastHit_t60 * L_1 = ___hitInfo;
		float L_2 = ___maxDistance;
		bool L_3 = Collider_Internal_Raycast_m4450(NULL /*static, unused*/, __this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.BoxCollider
#include "UnityEngine_UnityEngine_BoxCollider.h"
// UnityEngine.BoxCollider
#include "UnityEngine_UnityEngine_BoxColliderMethodDeclarations.h"
// UnityEngine.MeshCollider
#include "UnityEngine_UnityEngine_MeshCollider.h"
// UnityEngine.MeshCollider
#include "UnityEngine_UnityEngine_MeshColliderMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
extern "C" void MeshCollider_set_sharedMesh_m4452 (MeshCollider_t866 * __this, Mesh_t104 * ___value, const MethodInfo* method)
{
	typedef void (*MeshCollider_set_sharedMesh_m4452_ftn) (MeshCollider_t866 *, Mesh_t104 *);
	static MeshCollider_set_sharedMesh_m4452_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshCollider_set_sharedMesh_m4452_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C" Vector3_t36  RaycastHit_get_point_m3388 (RaycastHit_t60 * __this, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = (__this->___m_Point_0);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C" Vector3_t36  RaycastHit_get_normal_m3389 (RaycastHit_t60 * __this, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = (__this->___m_Normal_1);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C" float RaycastHit_get_distance_m3387 (RaycastHit_t60 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Distance_3);
		return L_0;
	}
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C" Collider_t369 * RaycastHit_get_collider_m3386 (RaycastHit_t60 * __this, const MethodInfo* method)
{
	{
		Collider_t369 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// UnityEngine.Physics2D
#include "UnityEngine_UnityEngine_Physics2D.h"
// UnityEngine.Physics2D
#include "UnityEngine_UnityEngine_Physics2DMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_43.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_43MethodDeclarations.h"
// System.Void UnityEngine.Physics2D::.cctor()
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_43MethodDeclarations.h"
extern TypeInfo* List_1_t867_il2cpp_TypeInfo_var;
extern TypeInfo* Physics2D_t728_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m5138_MethodInfo_var;
extern "C" void Physics2D__cctor_m4453 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t867_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(590);
		Physics2D_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		List_1__ctor_m5138_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484224);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t867 * L_0 = (List_1_t867 *)il2cpp_codegen_object_new (List_1_t867_il2cpp_TypeInfo_var);
		List_1__ctor_m5138(L_0, /*hidden argument*/List_1__ctor_m5138_MethodInfo_var);
		((Physics2D_t728_StaticFields*)Physics2D_t728_il2cpp_TypeInfo_var->static_fields)->___m_LastDisabledRigidbody2D_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// System.Single
#include "mscorlib_System_Single.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Physics2D
#include "UnityEngine_UnityEngine_Physics2DMethodDeclarations.h"
extern TypeInfo* Physics2D_t728_il2cpp_TypeInfo_var;
extern "C" void Physics2D_Internal_Raycast_m4454 (Object_t * __this /* static, unused */, Vector2_t2  ___origin, Vector2_t2  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t729 * ___raycastHit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = ___minDepth;
		float L_3 = ___maxDepth;
		RaycastHit2D_t729 * L_4 = ___raycastHit;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t728_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m4455(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C" void Physics2D_INTERNAL_CALL_Internal_Raycast_m4455 (Object_t * __this /* static, unused */, Vector2_t2 * ___origin, Vector2_t2 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t729 * ___raycastHit, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m4455_ftn) (Vector2_t2 *, Vector2_t2 *, float, int32_t, float, float, RaycastHit2D_t729 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m4455_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m4455_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth, ___raycastHit);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t728_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t729  Physics2D_Raycast_m3538 (Object_t * __this /* static, unused */, Vector2_t2  ___origin, Vector2_t2  ___direction, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t2  L_0 = ___origin;
		Vector2_t2  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t728_il2cpp_TypeInfo_var);
		RaycastHit2D_t729  L_6 = Physics2D_Raycast_m4456(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern TypeInfo* Physics2D_t728_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t729  Physics2D_Raycast_m4456 (Object_t * __this /* static, unused */, Vector2_t2  ___origin, Vector2_t2  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t729  V_0 = {0};
	{
		Vector2_t2  L_0 = ___origin;
		Vector2_t2  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = ___minDepth;
		float L_5 = ___maxDepth;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t728_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m4454(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t729  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t728_il2cpp_TypeInfo_var;
extern "C" RaycastHit2DU5BU5D_t726* Physics2D_RaycastAll_m3372 (Object_t * __this /* static, unused */, Vector2_t2  ___origin, Vector2_t2  ___direction, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t728_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = V_1;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t728_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t726* L_4 = Physics2D_INTERNAL_CALL_RaycastAll_m4457(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C" RaycastHit2DU5BU5D_t726* Physics2D_INTERNAL_CALL_RaycastAll_m4457 (Object_t * __this /* static, unused */, Vector2_t2 * ___origin, Vector2_t2 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t726* (*Physics2D_INTERNAL_CALL_RaycastAll_m4457_ftn) (Vector2_t2 *, Vector2_t2 *, float, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_RaycastAll_m4457_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_RaycastAll_m4457_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth);
}
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2DMethodDeclarations.h"
// UnityEngine.Collider2D
#include "UnityEngine_UnityEngine_Collider2D.h"
// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
// UnityEngine.Collider2D
#include "UnityEngine_UnityEngine_Collider2DMethodDeclarations.h"
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C" Vector2_t2  RaycastHit2D_get_point_m3376 (RaycastHit2D_t729 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___m_Point_1);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C" Vector2_t2  RaycastHit2D_get_normal_m3377 (RaycastHit2D_t729 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___m_Normal_2);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C" float RaycastHit2D_get_fraction_m3539 (RaycastHit2D_t729 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Fraction_4);
		return L_0;
	}
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C" Collider2D_t730 * RaycastHit2D_get_collider_m3373 (RaycastHit2D_t729 * __this, const MethodInfo* method)
{
	{
		Collider2D_t730 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2DMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Collider2D
#include "UnityEngine_UnityEngine_Collider2DMethodDeclarations.h"
extern "C" Rigidbody2D_t868 * RaycastHit2D_get_rigidbody_m4458 (RaycastHit2D_t729 * __this, const MethodInfo* method)
{
	Rigidbody2D_t868 * G_B3_0 = {0};
	{
		Collider2D_t730 * L_0 = RaycastHit2D_get_collider_m3373(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider2D_t730 * L_2 = RaycastHit2D_get_collider_m3373(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t868 * L_3 = Collider2D_get_attachedRigidbody_m4459(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody2D_t868 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern "C" Transform_t35 * RaycastHit2D_get_transform_m3375 (RaycastHit2D_t729 * __this, const MethodInfo* method)
{
	Rigidbody2D_t868 * V_0 = {0};
	{
		Rigidbody2D_t868 * L_0 = RaycastHit2D_get_rigidbody_m4458(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody2D_t868 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_1, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody2D_t868 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t35 * L_4 = Component_get_transform_m1417(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t730 * L_5 = RaycastHit2D_get_collider_m3373(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_5, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider2D_t730 * L_7 = RaycastHit2D_get_collider_m3373(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t35 * L_8 = Component_get_transform_m1417(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t35 *)NULL;
	}
}
// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2DMethodDeclarations.h"
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C" Rigidbody2D_t868 * Collider2D_get_attachedRigidbody_m4459 (Collider2D_t730 * __this, const MethodInfo* method)
{
	typedef Rigidbody2D_t868 * (*Collider2D_get_attachedRigidbody_m4459_ftn) (Collider2D_t730 *);
	static Collider2D_get_attachedRigidbody_m4459_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_attachedRigidbody_m4459_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.ContactPoint2D
#include "UnityEngine_UnityEngine_ContactPoint2D.h"
// UnityEngine.ContactPoint2D
#include "UnityEngine_UnityEngine_ContactPoint2DMethodDeclarations.h"
// UnityEngine.Collision2D
#include "UnityEngine_UnityEngine_Collision2D.h"
// UnityEngine.Collision2D
#include "UnityEngine_UnityEngine_Collision2DMethodDeclarations.h"
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChan.h"
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChanMethodDeclarations.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void AudioConfigurationChangeHandler__ctor_m4460 (AudioConfigurationChangeHandler_t872 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void AudioConfigurationChangeHandler_Invoke_m4461 (AudioConfigurationChangeHandler_t872 * __this, bool ___deviceWasChanged, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AudioConfigurationChangeHandler_Invoke_m4461((AudioConfigurationChangeHandler_t872 *)__this->___prev_9,___deviceWasChanged, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, bool ___deviceWasChanged, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ___deviceWasChanged, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t872(Il2CppObject* delegate, bool ___deviceWasChanged)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___deviceWasChanged' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___deviceWasChanged);

	// Marshaling cleanup of parameter '___deviceWasChanged' native representation

}
// System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* Boolean_t384_il2cpp_TypeInfo_var;
extern "C" Object_t * AudioConfigurationChangeHandler_BeginInvoke_m4462 (AudioConfigurationChangeHandler_t872 * __this, bool ___deviceWasChanged, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t384_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t384_il2cpp_TypeInfo_var, &___deviceWasChanged);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m4463 (AudioConfigurationChangeHandler_t872 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.AudioSettings
#include "UnityEngine_UnityEngine_AudioSettings.h"
// UnityEngine.AudioSettings
#include "UnityEngine_UnityEngine_AudioSettingsMethodDeclarations.h"
// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChanMethodDeclarations.h"
extern TypeInfo* AudioSettings_t873_il2cpp_TypeInfo_var;
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m4464 (Object_t * __this /* static, unused */, bool ___deviceWasChanged, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AudioSettings_t873_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(591);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioConfigurationChangeHandler_t872 * L_0 = ((AudioSettings_t873_StaticFields*)AudioSettings_t873_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AudioConfigurationChangeHandler_t872 * L_1 = ((AudioSettings_t873_StaticFields*)AudioSettings_t873_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		bool L_2 = ___deviceWasChanged;
		NullCheck(L_1);
		AudioConfigurationChangeHandler_Invoke_m4461(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback.h"
// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbackMethodDeclarations.h"
// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void PCMReaderCallback__ctor_m4465 (PCMReaderCallback_t874 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
#include "mscorlib_ArrayTypes.h"
extern "C" void PCMReaderCallback_Invoke_m4466 (PCMReaderCallback_t874 * __this, SingleU5BU5D_t264* ___data, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMReaderCallback_Invoke_m4466((PCMReaderCallback_t874 *)__this->___prev_9,___data, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, SingleU5BU5D_t264* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, SingleU5BU5D_t264* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t874(Il2CppObject* delegate, SingleU5BU5D_t264* ___data)
{
	typedef void (STDCALL *native_function_ptr_type)(float*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___data' to native representation
	float* ____data_marshaled = { 0 };
	____data_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___data);

	// Native function invocation
	_il2cpp_pinvoke_func(____data_marshaled);

	// Marshaling cleanup of parameter '___data' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * PCMReaderCallback_BeginInvoke_m4467 (PCMReaderCallback_t874 * __this, SingleU5BU5D_t264* ___data, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMReaderCallback_EndInvoke_m4468 (PCMReaderCallback_t874 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallback.h"
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallbackMethodDeclarations.h"
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void PCMSetPositionCallback__ctor_m4469 (PCMSetPositionCallback_t875 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void PCMSetPositionCallback_Invoke_m4470 (PCMSetPositionCallback_t875 * __this, int32_t ___position, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMSetPositionCallback_Invoke_m4470((PCMSetPositionCallback_t875 *)__this->___prev_9,___position, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t875(Il2CppObject* delegate, int32_t ___position)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___position' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___position);

	// Marshaling cleanup of parameter '___position' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern "C" Object_t * PCMSetPositionCallback_BeginInvoke_m4471 (PCMSetPositionCallback_t875 * __this, int32_t ___position, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t372_il2cpp_TypeInfo_var, &___position);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMSetPositionCallback_EndInvoke_m4472 (PCMSetPositionCallback_t875 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClipMethodDeclarations.h"
// System.Single UnityEngine.AudioClip::get_length()
extern "C" float AudioClip_get_length_m1753 (AudioClip_t353 * __this, const MethodInfo* method)
{
	typedef float (*AudioClip_get_length_m1753_ftn) (AudioClip_t353 *);
	static AudioClip_get_length_m1753_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_length_m1753_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_length()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
#include "mscorlib_ArrayTypes.h"
// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbackMethodDeclarations.h"
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m4473 (AudioClip_t353 * __this, SingleU5BU5D_t264* ___data, const MethodInfo* method)
{
	{
		PCMReaderCallback_t874 * L_0 = (__this->___m_PCMReaderCallback_2);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMReaderCallback_t874 * L_1 = (__this->___m_PCMReaderCallback_2);
		SingleU5BU5D_t264* L_2 = ___data;
		NullCheck(L_1);
		PCMReaderCallback_Invoke_m4466(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallbackMethodDeclarations.h"
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m4474 (AudioClip_t353 * __this, int32_t ___position, const MethodInfo* method)
{
	{
		PCMSetPositionCallback_t875 * L_0 = (__this->___m_PCMSetPositionCallback_3);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMSetPositionCallback_t875 * L_1 = (__this->___m_PCMSetPositionCallback_3);
		int32_t L_2 = ___position;
		NullCheck(L_1);
		PCMSetPositionCallback_Invoke_m4470(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
// System.Single UnityEngine.AudioSource::get_volume()
extern "C" float AudioSource_get_volume_m1732 (AudioSource_t44 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_volume_m1732_ftn) (AudioSource_t44 *);
	static AudioSource_get_volume_m1732_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_volume_m1732_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_volume()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void AudioSource_set_volume_m1394 (AudioSource_t44 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_volume_m1394_ftn) (AudioSource_t44 *, float);
	static AudioSource_set_volume_m1394_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_volume_m1394_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_volume(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.AudioSource::get_pitch()
extern "C" float AudioSource_get_pitch_m1733 (AudioSource_t44 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_pitch_m1733_ftn) (AudioSource_t44 *);
	static AudioSource_get_pitch_m1733_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_pitch_m1733_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_pitch()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern "C" void AudioSource_set_pitch_m1734 (AudioSource_t44 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_pitch_m1734_ftn) (AudioSource_t44 *, float);
	static AudioSource_set_pitch_m1734_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_pitch_m1734_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_pitch(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C" AudioClip_t353 * AudioSource_get_clip_m1752 (AudioSource_t44 * __this, const MethodInfo* method)
{
	typedef AudioClip_t353 * (*AudioSource_get_clip_m1752_ftn) (AudioSource_t44 *);
	static AudioSource_get_clip_m1752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_clip_m1752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_clip()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"
extern "C" void AudioSource_set_clip_m1751 (AudioSource_t44 * __this, AudioClip_t353 * ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_clip_m1751_ftn) (AudioSource_t44 *, AudioClip_t353 *);
	static AudioSource_set_clip_m1751_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_clip_m1751_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern "C" void AudioSource_PlayOneShot_m4475 (AudioSource_t44 * __this, AudioClip_t353 * ___clip, float ___volumeScale, const MethodInfo* method)
{
	typedef void (*AudioSource_PlayOneShot_m4475_ftn) (AudioSource_t44 *, AudioClip_t353 *, float);
	static AudioSource_PlayOneShot_m4475_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_PlayOneShot_m4475_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)");
	_il2cpp_icall_func(__this, ___clip, ___volumeScale);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
extern "C" void AudioSource_PlayOneShot_m1774 (AudioSource_t44 * __this, AudioClip_t353 * ___clip, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		AudioClip_t353 * L_0 = ___clip;
		float L_1 = V_0;
		AudioSource_PlayOneShot_m4475(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void AudioSource_set_playOnAwake_m1750 (AudioSource_t44 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_playOnAwake_m1750_ftn) (AudioSource_t44 *, bool);
	static AudioSource_set_playOnAwake_m1750_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_playOnAwake_m1750_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_playOnAwake(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDeviceMethodDeclarations.h"
// System.String UnityEngine.WebCamDevice::get_name()
extern "C" String_t* WebCamDevice_get_name_m4476 (WebCamDevice_t876 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C" bool WebCamDevice_get_isFrontFacing_m4477 (WebCamDevice_t876 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Flags_1);
		return ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)1))? 1 : 0);
	}
}
// System.String
#include "mscorlib_System_String.h"
// Conversion methods for marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t876_marshal(const WebCamDevice_t876& unmarshaled, WebCamDevice_t876_marshaled& marshaled)
{
	marshaled.___m_Name_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Name_0);
	marshaled.___m_Flags_1 = unmarshaled.___m_Flags_1;
}
extern "C" void WebCamDevice_t876_marshal_back(const WebCamDevice_t876_marshaled& marshaled, WebCamDevice_t876& unmarshaled)
{
	unmarshaled.___m_Name_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Name_0);
	unmarshaled.___m_Flags_1 = marshaled.___m_Flags_1;
}
// Conversion method for clean up from marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t876_marshal_cleanup(WebCamDevice_t876_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Name_0);
	marshaled.___m_Name_0 = NULL;
}
// UnityEngine.WebCamTexture
#include "UnityEngine_UnityEngine_WebCamTexture.h"
// UnityEngine.WebCamTexture
#include "UnityEngine_UnityEngine_WebCamTextureMethodDeclarations.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.Void UnityEngine.WebCamTexture::.ctor()
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
// UnityEngine.WebCamTexture
#include "UnityEngine_UnityEngine_WebCamTextureMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void WebCamTexture__ctor_m4478 (WebCamTexture_t877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m3959(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		WebCamTexture_Internal_CreateWebCamTexture_m4479(NULL /*static, unused*/, __this, L_0, 0, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)
// UnityEngine.WebCamTexture
#include "UnityEngine_UnityEngine_WebCamTexture.h"
// System.String
#include "mscorlib_System_String.h"
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void WebCamTexture_Internal_CreateWebCamTexture_m4479 (Object_t * __this /* static, unused */, WebCamTexture_t877 * ___self, String_t* ___scriptingDevice, int32_t ___requestedWidth, int32_t ___requestedHeight, int32_t ___maxFramerate, const MethodInfo* method)
{
	typedef void (*WebCamTexture_Internal_CreateWebCamTexture_m4479_ftn) (WebCamTexture_t877 *, String_t*, int32_t, int32_t, int32_t);
	static WebCamTexture_Internal_CreateWebCamTexture_m4479_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_Internal_CreateWebCamTexture_m4479_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___self, ___scriptingDevice, ___requestedWidth, ___requestedHeight, ___maxFramerate);
}
// System.Void UnityEngine.WebCamTexture::Play()
extern "C" void WebCamTexture_Play_m4480 (WebCamTexture_t877 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_INTERNAL_CALL_Play_m4481(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)
extern "C" void WebCamTexture_INTERNAL_CALL_Play_m4481 (Object_t * __this /* static, unused */, WebCamTexture_t877 * ___self, const MethodInfo* method)
{
	typedef void (*WebCamTexture_INTERNAL_CALL_Play_m4481_ftn) (WebCamTexture_t877 *);
	static WebCamTexture_INTERNAL_CALL_Play_m4481_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_INTERNAL_CALL_Play_m4481_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)");
	_il2cpp_icall_func(___self);
}
// System.Void UnityEngine.WebCamTexture::Stop()
extern "C" void WebCamTexture_Stop_m4482 (WebCamTexture_t877 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_INTERNAL_CALL_Stop_m4483(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)
extern "C" void WebCamTexture_INTERNAL_CALL_Stop_m4483 (Object_t * __this /* static, unused */, WebCamTexture_t877 * ___self, const MethodInfo* method)
{
	typedef void (*WebCamTexture_INTERNAL_CALL_Stop_m4483_ftn) (WebCamTexture_t877 *);
	static WebCamTexture_INTERNAL_CALL_Stop_m4483_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_INTERNAL_CALL_Stop_m4483_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)");
	_il2cpp_icall_func(___self);
}
// System.Boolean UnityEngine.WebCamTexture::get_isPlaying()
extern "C" bool WebCamTexture_get_isPlaying_m4484 (WebCamTexture_t877 * __this, const MethodInfo* method)
{
	typedef bool (*WebCamTexture_get_isPlaying_m4484_ftn) (WebCamTexture_t877 *);
	static WebCamTexture_get_isPlaying_m4484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_isPlaying_m4484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_isPlaying()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.WebCamTexture::set_deviceName(System.String)
extern "C" void WebCamTexture_set_deviceName_m4485 (WebCamTexture_t877 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_deviceName_m4485_ftn) (WebCamTexture_t877 *, String_t*);
	static WebCamTexture_set_deviceName_m4485_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_deviceName_m4485_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_deviceName(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.WebCamTexture::set_requestedFPS(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void WebCamTexture_set_requestedFPS_m4486 (WebCamTexture_t877 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedFPS_m4486_ftn) (WebCamTexture_t877 *, float);
	static WebCamTexture_set_requestedFPS_m4486_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedFPS_m4486_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedFPS(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)
extern "C" void WebCamTexture_set_requestedWidth_m4487 (WebCamTexture_t877 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedWidth_m4487_ftn) (WebCamTexture_t877 *, int32_t);
	static WebCamTexture_set_requestedWidth_m4487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedWidth_m4487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)
extern "C" void WebCamTexture_set_requestedHeight_m4488 (WebCamTexture_t877 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*WebCamTexture_set_requestedHeight_m4488_ftn) (WebCamTexture_t877 *, int32_t);
	static WebCamTexture_set_requestedHeight_m4488_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_set_requestedHeight_m4488_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
extern "C" WebCamDeviceU5BU5D_t1006* WebCamTexture_get_devices_m4489 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef WebCamDeviceU5BU5D_t1006* (*WebCamTexture_get_devices_m4489_ftn) ();
	static WebCamTexture_get_devices_m4489_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_devices_m4489_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_devices()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.WebCamTexture::get_didUpdateThisFrame()
extern "C" bool WebCamTexture_get_didUpdateThisFrame_m4490 (WebCamTexture_t877 * __this, const MethodInfo* method)
{
	typedef bool (*WebCamTexture_get_didUpdateThisFrame_m4490_ftn) (WebCamTexture_t877 *);
	static WebCamTexture_get_didUpdateThisFrame_m4490_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WebCamTexture_get_didUpdateThisFrame_m4490_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WebCamTexture::get_didUpdateThisFrame()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSource.h"
// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSourceMethodDeclarations.h"
// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEvent.h"
// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEventMethodDeclarations.h"
// UnityEngine.AnimationState
#include "UnityEngine_UnityEngine_AnimationState.h"
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
// System.Void UnityEngine.AnimationEvent::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void AnimationEvent__ctor_m4491 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		__this->___m_Time_0 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_FunctionName_1 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_StringParameter_2 = L_1;
		__this->___m_ObjectReferenceParameter_3 = (Object_t335 *)NULL;
		__this->___m_FloatParameter_4 = (0.0f);
		__this->___m_IntParameter_5 = 0;
		__this->___m_MessageOptions_6 = 0;
		__this->___m_Source_7 = 0;
		__this->___m_StateSender_8 = (AnimationState_t880 *)NULL;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_data()
extern "C" String_t* AnimationEvent_get_data_m4492 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" void AnimationEvent_set_data_m4493 (AnimationEvent_t879 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_stringParameter()
extern "C" String_t* AnimationEvent_get_stringParameter_m4494 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
extern "C" void AnimationEvent_set_stringParameter_m4495 (AnimationEvent_t879 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
extern "C" float AnimationEvent_get_floatParameter_m4496 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FloatParameter_4);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void AnimationEvent_set_floatParameter_m4497 (AnimationEvent_t879 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_FloatParameter_4 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
extern "C" int32_t AnimationEvent_get_intParameter_m4498 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_IntParameter_5);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void AnimationEvent_set_intParameter_m4499 (AnimationEvent_t879 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_IntParameter_5 = L_0;
		return;
	}
}
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
extern "C" Object_t335 * AnimationEvent_get_objectReferenceParameter_m4500 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	{
		Object_t335 * L_0 = (__this->___m_ObjectReferenceParameter_3);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
extern "C" void AnimationEvent_set_objectReferenceParameter_m4501 (AnimationEvent_t879 * __this, Object_t335 * ___value, const MethodInfo* method)
{
	{
		Object_t335 * L_0 = ___value;
		__this->___m_ObjectReferenceParameter_3 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C" String_t* AnimationEvent_get_functionName_m4502 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_FunctionName_1);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
extern "C" void AnimationEvent_set_functionName_m4503 (AnimationEvent_t879 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_FunctionName_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C" float AnimationEvent_get_time_m4504 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Time_0);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
extern "C" void AnimationEvent_set_time_m4505 (AnimationEvent_t879 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Time_0 = L_0;
		return;
	}
}
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
extern "C" int32_t AnimationEvent_get_messageOptions_m4506 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_MessageOptions_6);
		return (int32_t)(L_0);
	}
}
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
extern "C" void AnimationEvent_set_messageOptions_m4507 (AnimationEvent_t879 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_MessageOptions_6 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
extern "C" bool AnimationEvent_get_isFiredByLegacy_m4508 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
extern "C" bool AnimationEvent_get_isFiredByAnimator_m4509 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEventMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern Il2CppCodeGenString* _stringLiteral564;
extern "C" AnimationState_t880 * AnimationEvent_get_animationState_m4510 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral564 = il2cpp_codegen_string_literal_from_index(564);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByLegacy_m4508(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m1317(NULL /*static, unused*/, _stringLiteral564, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimationState_t880 * L_1 = (__this->___m_StateSender_8);
		return L_1;
	}
}
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
extern Il2CppCodeGenString* _stringLiteral565;
extern "C" AnimatorStateInfo_t881  AnimationEvent_get_animatorStateInfo_m4511 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral565 = il2cpp_codegen_string_literal_from_index(565);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m4509(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m1317(NULL /*static, unused*/, _stringLiteral565, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorStateInfo_t881  L_1 = (__this->___m_AnimatorStateInfo_9);
		return L_1;
	}
}
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
extern Il2CppCodeGenString* _stringLiteral566;
extern "C" AnimatorClipInfo_t882  AnimationEvent_get_animatorClipInfo_m4512 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral566 = il2cpp_codegen_string_literal_from_index(566);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m4509(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m1317(NULL /*static, unused*/, _stringLiteral566, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorClipInfo_t882  L_1 = (__this->___m_AnimatorClipInfo_10);
		return L_1;
	}
}
// System.Int32 UnityEngine.AnimationEvent::GetHash()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
extern "C" int32_t AnimationEvent_GetHash_m4513 (AnimationEvent_t879 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		String_t* L_0 = AnimationEvent_get_functionName_m4502(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m5139(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		float L_3 = AnimationEvent_get_time_m4504(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Single_GetHashCode_m5117((&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)33)*(int32_t)L_2))+(int32_t)L_4));
		int32_t L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurve.h"
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
#include "UnityEngine_ArrayTypes.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"
extern "C" void AnimationCurve__ctor_m4514 (AnimationCurve_t884 * __this, KeyframeU5BU5D_t1007* ___keys, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t1007* L_0 = ___keys;
		AnimationCurve_Init_m4518(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m4515 (AnimationCurve_t884 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m4518(__this, (KeyframeU5BU5D_t1007*)(KeyframeU5BU5D_t1007*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m4516 (AnimationCurve_t884 * __this, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Cleanup_m4516_ftn) (AnimationCurve_t884 *);
	static AnimationCurve_Cleanup_m4516_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m4516_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m4517 (AnimationCurve_t884 * __this, const MethodInfo* method)
{
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m4516(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m5102(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m4518 (AnimationCurve_t884 * __this, KeyframeU5BU5D_t1007* ___keys, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Init_m4518_ftn) (AnimationCurve_t884 *, KeyframeU5BU5D_t1007*);
	static AnimationCurve_Init_m4518_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m4518_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t884_marshal(const AnimationCurve_t884& unmarshaled, AnimationCurve_t884_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void AnimationCurve_t884_marshal_back(const AnimationCurve_t884_marshaled& marshaled, AnimationCurve_t884& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t884_marshal_cleanup(AnimationCurve_t884_marshaled& marshaled)
{
}
// UnityEngine.AnimationState
#include "UnityEngine_UnityEngine_AnimationStateMethodDeclarations.h"
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfoMethodDeclarations.h"
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfoMethodDeclarations.h"
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
extern "C" bool AnimatorStateInfo_IsName_m4519 (AnimatorStateInfo_t881 * __this, String_t* ___name, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m4539(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = (__this->___m_FullPath_2);
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = (__this->___m_Name_0);
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = (__this->___m_Path_1);
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		return G_B4_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C" int32_t AnimatorStateInfo_get_fullPathHash_m4520 (AnimatorStateInfo_t881 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C" int32_t AnimatorStateInfo_get_nameHash_m4521 (AnimatorStateInfo_t881 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Path_1);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C" int32_t AnimatorStateInfo_get_shortNameHash_m4522 (AnimatorStateInfo_t881 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C" float AnimatorStateInfo_get_normalizedTime_m4523 (AnimatorStateInfo_t881 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C" float AnimatorStateInfo_get_length_m4524 (AnimatorStateInfo_t881 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Length_4);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_speed()
extern "C" float AnimatorStateInfo_get_speed_m4525 (AnimatorStateInfo_t881 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Speed_5);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_speedMultiplier()
extern "C" float AnimatorStateInfo_get_speedMultiplier_m4526 (AnimatorStateInfo_t881 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_SpeedMultiplier_6);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C" int32_t AnimatorStateInfo_get_tagHash_m4527 (AnimatorStateInfo_t881 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Tag_7);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C" bool AnimatorStateInfo_IsTag_m4528 (AnimatorStateInfo_t881 * __this, String_t* ___tag, const MethodInfo* method)
{
	{
		String_t* L_0 = ___tag;
		int32_t L_1 = Animator_StringToHash_m4539(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Tag_7);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C" bool AnimatorStateInfo_get_loop_m4529 (AnimatorStateInfo_t881 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Loop_8);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.AnimatorTransitionInfo
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo.h"
// UnityEngine.AnimatorTransitionInfo
#include "UnityEngine_UnityEngine_AnimatorTransitionInfoMethodDeclarations.h"
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
extern "C" bool AnimatorTransitionInfo_IsName_m4530 (AnimatorTransitionInfo_t886 * __this, String_t* ___name, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m4539(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Name_2);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___name;
		int32_t L_4 = Animator_StringToHash_m4539(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_5 = (__this->___m_FullPath_0);
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C" bool AnimatorTransitionInfo_IsUserName_m4531 (AnimatorTransitionInfo_t886 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m4539(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_UserName_1);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_fullPathHash()
extern "C" int32_t AnimatorTransitionInfo_get_fullPathHash_m4532 (AnimatorTransitionInfo_t886 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_0);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C" int32_t AnimatorTransitionInfo_get_nameHash_m4533 (AnimatorTransitionInfo_t886 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C" int32_t AnimatorTransitionInfo_get_userNameHash_m4534 (AnimatorTransitionInfo_t886 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_UserName_1);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C" float AnimatorTransitionInfo_get_normalizedTime_m4535 (AnimatorTransitionInfo_t886 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_anyState()
extern "C" bool AnimatorTransitionInfo_get_anyState_m4536 (AnimatorTransitionInfo_t886 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_AnyState_4);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_entry()
extern "C" bool AnimatorTransitionInfo_get_entry_m4537 (AnimatorTransitionInfo_t886 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_exit()
extern "C" bool AnimatorTransitionInfo_get_exit_m4538 (AnimatorTransitionInfo_t886 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t886_marshal(const AnimatorTransitionInfo_t886& unmarshaled, AnimatorTransitionInfo_t886_marshaled& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.___m_FullPath_0;
	marshaled.___m_UserName_1 = unmarshaled.___m_UserName_1;
	marshaled.___m_Name_2 = unmarshaled.___m_Name_2;
	marshaled.___m_NormalizedTime_3 = unmarshaled.___m_NormalizedTime_3;
	marshaled.___m_AnyState_4 = unmarshaled.___m_AnyState_4;
	marshaled.___m_TransitionType_5 = unmarshaled.___m_TransitionType_5;
}
extern "C" void AnimatorTransitionInfo_t886_marshal_back(const AnimatorTransitionInfo_t886_marshaled& marshaled, AnimatorTransitionInfo_t886& unmarshaled)
{
	unmarshaled.___m_FullPath_0 = marshaled.___m_FullPath_0;
	unmarshaled.___m_UserName_1 = marshaled.___m_UserName_1;
	unmarshaled.___m_Name_2 = marshaled.___m_Name_2;
	unmarshaled.___m_NormalizedTime_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.___m_AnyState_4 = marshaled.___m_AnyState_4;
	unmarshaled.___m_TransitionType_5 = marshaled.___m_TransitionType_5;
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t886_marshal_cleanup(AnimatorTransitionInfo_t886_marshaled& marshaled)
{
}
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
// UnityEngine.RuntimeAnimatorController
#include "UnityEngine_UnityEngine_RuntimeAnimatorController.h"
// System.Void UnityEngine.Animator::SetTrigger(System.String)
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
extern "C" void Animator_SetTrigger_m3717 (Animator_t714 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_SetTriggerString_m4540(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C" void Animator_ResetTrigger_m3716 (Animator_t714 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_ResetTriggerString_m4541(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C" RuntimeAnimatorController_t758 * Animator_get_runtimeAnimatorController_m3715 (Animator_t714 * __this, const MethodInfo* method)
{
	typedef RuntimeAnimatorController_t758 * (*Animator_get_runtimeAnimatorController_m3715_ftn) (Animator_t714 *);
	static Animator_get_runtimeAnimatorController_m3715_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_runtimeAnimatorController_m3715_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_runtimeAnimatorController()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C" int32_t Animator_StringToHash_m4539 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef int32_t (*Animator_StringToHash_m4539_ftn) (String_t*);
	static Animator_StringToHash_m4539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m4539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C" void Animator_SetTriggerString_m4540 (Animator_t714 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Animator_SetTriggerString_m4540_ftn) (Animator_t714 *, String_t*);
	static Animator_SetTriggerString_m4540_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m4540_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C" void Animator_ResetTriggerString_m4541 (Animator_t714 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Animator_ResetTriggerString_m4541_ftn) (Animator_t714 *, String_t*);
	static Animator_ResetTriggerString_m4541_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_ResetTriggerString_m4541_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// UnityEngine.SkeletonBone
#include "UnityEngine_UnityEngine_SkeletonBone.h"
// UnityEngine.SkeletonBone
#include "UnityEngine_UnityEngine_SkeletonBoneMethodDeclarations.h"
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t887_marshal(const SkeletonBone_t887& unmarshaled, SkeletonBone_t887_marshaled& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.___name_0);
	marshaled.___position_1 = unmarshaled.___position_1;
	marshaled.___rotation_2 = unmarshaled.___rotation_2;
	marshaled.___scale_3 = unmarshaled.___scale_3;
	marshaled.___transformModified_4 = unmarshaled.___transformModified_4;
}
extern "C" void SkeletonBone_t887_marshal_back(const SkeletonBone_t887_marshaled& marshaled, SkeletonBone_t887& unmarshaled)
{
	unmarshaled.___name_0 = il2cpp_codegen_marshal_string_result(marshaled.___name_0);
	unmarshaled.___position_1 = marshaled.___position_1;
	unmarshaled.___rotation_2 = marshaled.___rotation_2;
	unmarshaled.___scale_3 = marshaled.___scale_3;
	unmarshaled.___transformModified_4 = marshaled.___transformModified_4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t887_marshal_cleanup(SkeletonBone_t887_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimit.h"
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimitMethodDeclarations.h"
// UnityEngine.HumanBone
#include "UnityEngine_UnityEngine_HumanBone.h"
// UnityEngine.HumanBone
#include "UnityEngine_UnityEngine_HumanBoneMethodDeclarations.h"
// System.String UnityEngine.HumanBone::get_boneName()
extern "C" String_t* HumanBone_get_boneName_m4542 (HumanBone_t889 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_BoneName_0);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_boneName(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" void HumanBone_set_boneName_m4543 (HumanBone_t889 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_BoneName_0 = L_0;
		return;
	}
}
// System.String UnityEngine.HumanBone::get_humanName()
extern "C" String_t* HumanBone_get_humanName_m4544 (HumanBone_t889 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_HumanName_1);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_humanName(System.String)
extern "C" void HumanBone_set_humanName_m4545 (HumanBone_t889 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_HumanName_1 = L_0;
		return;
	}
}
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimit.h"
// Conversion methods for marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t889_marshal(const HumanBone_t889& unmarshaled, HumanBone_t889_marshaled& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_BoneName_0);
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string(unmarshaled.___m_HumanName_1);
	marshaled.___limit_2 = unmarshaled.___limit_2;
}
extern "C" void HumanBone_t889_marshal_back(const HumanBone_t889_marshaled& marshaled, HumanBone_t889& unmarshaled)
{
	unmarshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_BoneName_0);
	unmarshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string_result(marshaled.___m_HumanName_1);
	unmarshaled.___limit_2 = marshaled.___limit_2;
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t889_marshal_cleanup(HumanBone_t889_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
// UnityEngine.RuntimeAnimatorController
#include "UnityEngine_UnityEngine_RuntimeAnimatorControllerMethodDeclarations.h"
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchorMethodDeclarations.h"
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapModeMethodDeclarations.h"
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapModeMethodDeclarations.h"
// UnityEngine.GUIText
#include "UnityEngine_UnityEngine_GUIText.h"
// UnityEngine.GUIText
#include "UnityEngine_UnityEngine_GUITextMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Material UnityEngine.GUIText::get_material()
extern "C" Material_t82 * GUIText_get_material_m1725 (GUIText_t443 * __this, const MethodInfo* method)
{
	typedef Material_t82 * (*GUIText_get_material_m1725_ftn) (GUIText_t443 *);
	static GUIText_get_material_m1725_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIText_get_material_m1725_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIText::get_material()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.TextMesh
#include "UnityEngine_UnityEngine_TextMesh.h"
// UnityEngine.TextMesh
#include "UnityEngine_UnityEngine_TextMeshMethodDeclarations.h"
// System.Void UnityEngine.TextMesh::set_text(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" void TextMesh_set_text_m1381 (TextMesh_t26 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*TextMesh_set_text_m1381_ftn) (TextMesh_t26 *, String_t*);
	static TextMesh_set_text_m1381_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextMesh_set_text_m1381_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextMesh::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfo.h"
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfoMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// System.Int32 UnityEngine.CharacterInfo::get_advance()
extern "C" int32_t CharacterInfo_get_advance_m4546 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___width_3);
		return (((int32_t)L_0));
	}
}
// System.Void UnityEngine.CharacterInfo::set_advance(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void CharacterInfo_set_advance_m4547 (CharacterInfo_t892 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___width_3 = (((float)L_0));
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphWidth()
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
extern "C" int32_t CharacterInfo_get_glyphWidth_m4548 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_width_m1762(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Void UnityEngine.CharacterInfo::set_glyphWidth(System.Int32)
extern "C" void CharacterInfo_set_glyphWidth_m4549 (CharacterInfo_t892 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		Rect_set_width_m1763(L_0, (((float)L_1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphHeight()
extern "C" int32_t CharacterInfo_get_glyphHeight_m4550 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_height_m1764(L_0, /*hidden argument*/NULL);
		return (((int32_t)((-L_1))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_glyphHeight(System.Int32)
extern "C" void CharacterInfo_set_glyphHeight_m4551 (CharacterInfo_t892 * __this, int32_t ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_height_m1764(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Rect_t267 * L_2 = &(__this->___vert_2);
		int32_t L_3 = ___value;
		Rect_set_height_m1765(L_2, (((float)((-L_3)))), /*hidden argument*/NULL);
		Rect_t267 * L_4 = &(__this->___vert_2);
		Rect_t267 * L_5 = L_4;
		float L_6 = Rect_get_y_m1760(L_5, /*hidden argument*/NULL);
		float L_7 = V_0;
		Rect_t267 * L_8 = &(__this->___vert_2);
		float L_9 = Rect_get_height_m1764(L_8, /*hidden argument*/NULL);
		Rect_set_y_m1761(L_5, ((float)((float)L_6+(float)((float)((float)L_7-(float)L_9)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_bearing()
extern "C" int32_t CharacterInfo_get_bearing_m4552 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m1758(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Void UnityEngine.CharacterInfo::set_bearing(System.Int32)
extern "C" void CharacterInfo_set_bearing_m4553 (CharacterInfo_t892 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		Rect_set_x_m1759(L_0, (((float)L_1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minY()
extern "C" int32_t CharacterInfo_get_minY_m4554 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t267 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m1760(L_1, /*hidden argument*/NULL);
		Rect_t267 * L_3 = &(__this->___vert_2);
		float L_4 = Rect_get_height_m1764(L_3, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)((float)((float)L_2+(float)L_4))))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_minY(System.Int32)
extern "C" void CharacterInfo_set_minY_m4555 (CharacterInfo_t892 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		int32_t L_2 = (__this->___ascent_7);
		Rect_t267 * L_3 = &(__this->___vert_2);
		float L_4 = Rect_get_y_m1760(L_3, /*hidden argument*/NULL);
		Rect_set_height_m1765(L_0, ((float)((float)(((float)((int32_t)((int32_t)L_1-(int32_t)L_2))))-(float)L_4)), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxY()
extern "C" int32_t CharacterInfo_get_maxY_m4556 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t267 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m1760(L_1, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)L_2))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_maxY(System.Int32)
extern "C" void CharacterInfo_set_maxY_m4557 (CharacterInfo_t892 * __this, int32_t ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_y_m1760(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Rect_t267 * L_2 = &(__this->___vert_2);
		int32_t L_3 = ___value;
		int32_t L_4 = (__this->___ascent_7);
		Rect_set_y_m1761(L_2, (((float)((int32_t)((int32_t)L_3-(int32_t)L_4)))), /*hidden argument*/NULL);
		Rect_t267 * L_5 = &(__this->___vert_2);
		Rect_t267 * L_6 = L_5;
		float L_7 = Rect_get_height_m1764(L_6, /*hidden argument*/NULL);
		float L_8 = V_0;
		Rect_t267 * L_9 = &(__this->___vert_2);
		float L_10 = Rect_get_y_m1760(L_9, /*hidden argument*/NULL);
		Rect_set_height_m1765(L_6, ((float)((float)L_7+(float)((float)((float)L_8-(float)L_10)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minX()
extern "C" int32_t CharacterInfo_get_minX_m4558 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m1758(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Void UnityEngine.CharacterInfo::set_minX(System.Int32)
extern "C" void CharacterInfo_set_minX_m4559 (CharacterInfo_t892 * __this, int32_t ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m1758(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Rect_t267 * L_2 = &(__this->___vert_2);
		int32_t L_3 = ___value;
		Rect_set_x_m1759(L_2, (((float)L_3)), /*hidden argument*/NULL);
		Rect_t267 * L_4 = &(__this->___vert_2);
		Rect_t267 * L_5 = L_4;
		float L_6 = Rect_get_width_m1762(L_5, /*hidden argument*/NULL);
		float L_7 = V_0;
		Rect_t267 * L_8 = &(__this->___vert_2);
		float L_9 = Rect_get_x_m1758(L_8, /*hidden argument*/NULL);
		Rect_set_width_m1763(L_5, ((float)((float)L_6+(float)((float)((float)L_7-(float)L_9)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxX()
extern "C" int32_t CharacterInfo_get_maxX_m4560 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m1758(L_0, /*hidden argument*/NULL);
		Rect_t267 * L_2 = &(__this->___vert_2);
		float L_3 = Rect_get_width_m1762(L_2, /*hidden argument*/NULL);
		return (((int32_t)((float)((float)L_1+(float)L_3))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_maxX(System.Int32)
extern "C" void CharacterInfo_set_maxX_m4561 (CharacterInfo_t892 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		Rect_t267 * L_2 = &(__this->___vert_2);
		float L_3 = Rect_get_x_m1758(L_2, /*hidden argument*/NULL);
		Rect_set_width_m1763(L_0, ((float)((float)(((float)L_1))-(float)L_3)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeftUnFlipped()
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
extern "C" Vector2_t2  CharacterInfo_get_uvBottomLeftUnFlipped_m4562 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m1758(L_0, /*hidden argument*/NULL);
		Rect_t267 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m1760(L_2, /*hidden argument*/NULL);
		Vector2_t2  L_4 = {0};
		Vector2__ctor_m1309(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomLeftUnFlipped(UnityEngine.Vector2)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfoMethodDeclarations.h"
extern "C" void CharacterInfo_set_uvBottomLeftUnFlipped_m4563 (CharacterInfo_t892 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	Vector2_t2  V_0 = {0};
	{
		Vector2_t2  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m4566(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rect_t267 * L_1 = &(__this->___uv_1);
		float L_2 = ((&___value)->___x_1);
		Rect_set_x_m1759(L_1, L_2, /*hidden argument*/NULL);
		Rect_t267 * L_3 = &(__this->___uv_1);
		float L_4 = ((&___value)->___y_2);
		Rect_set_y_m1761(L_3, L_4, /*hidden argument*/NULL);
		Rect_t267 * L_5 = &(__this->___uv_1);
		float L_6 = ((&V_0)->___x_1);
		Rect_t267 * L_7 = &(__this->___uv_1);
		float L_8 = Rect_get_x_m1758(L_7, /*hidden argument*/NULL);
		Rect_set_width_m1763(L_5, ((float)((float)L_6-(float)L_8)), /*hidden argument*/NULL);
		Rect_t267 * L_9 = &(__this->___uv_1);
		float L_10 = ((&V_0)->___y_2);
		Rect_t267 * L_11 = &(__this->___uv_1);
		float L_12 = Rect_get_y_m1760(L_11, /*hidden argument*/NULL);
		Rect_set_height_m1765(L_9, ((float)((float)L_10-(float)L_12)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRightUnFlipped()
extern "C" Vector2_t2  CharacterInfo_get_uvBottomRightUnFlipped_m4564 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m1758(L_0, /*hidden argument*/NULL);
		Rect_t267 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m1762(L_2, /*hidden argument*/NULL);
		Rect_t267 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m1760(L_4, /*hidden argument*/NULL);
		Vector2_t2  L_6 = {0};
		Vector2__ctor_m1309(&L_6, ((float)((float)L_1+(float)L_3)), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomRightUnFlipped(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvBottomRightUnFlipped_m4565 (CharacterInfo_t892 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	Vector2_t2  V_0 = {0};
	{
		Vector2_t2  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m4566(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rect_t267 * L_1 = &(__this->___uv_1);
		float L_2 = ((&___value)->___x_1);
		Rect_t267 * L_3 = &(__this->___uv_1);
		float L_4 = Rect_get_x_m1758(L_3, /*hidden argument*/NULL);
		Rect_set_width_m1763(L_1, ((float)((float)L_2-(float)L_4)), /*hidden argument*/NULL);
		Rect_t267 * L_5 = &(__this->___uv_1);
		float L_6 = ((&___value)->___y_2);
		Rect_set_y_m1761(L_5, L_6, /*hidden argument*/NULL);
		Rect_t267 * L_7 = &(__this->___uv_1);
		float L_8 = ((&V_0)->___y_2);
		Rect_t267 * L_9 = &(__this->___uv_1);
		float L_10 = Rect_get_y_m1760(L_9, /*hidden argument*/NULL);
		Rect_set_height_m1765(L_7, ((float)((float)L_8-(float)L_10)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRightUnFlipped()
extern "C" Vector2_t2  CharacterInfo_get_uvTopRightUnFlipped_m4566 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m1758(L_0, /*hidden argument*/NULL);
		Rect_t267 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m1762(L_2, /*hidden argument*/NULL);
		Rect_t267 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m1760(L_4, /*hidden argument*/NULL);
		Rect_t267 * L_6 = &(__this->___uv_1);
		float L_7 = Rect_get_height_m1764(L_6, /*hidden argument*/NULL);
		Vector2_t2  L_8 = {0};
		Vector2__ctor_m1309(&L_8, ((float)((float)L_1+(float)L_3)), ((float)((float)L_5+(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopRightUnFlipped(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopRightUnFlipped_m4567 (CharacterInfo_t892 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___uv_1);
		float L_1 = ((&___value)->___x_1);
		Rect_t267 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_x_m1758(L_2, /*hidden argument*/NULL);
		Rect_set_width_m1763(L_0, ((float)((float)L_1-(float)L_3)), /*hidden argument*/NULL);
		Rect_t267 * L_4 = &(__this->___uv_1);
		float L_5 = ((&___value)->___y_2);
		Rect_t267 * L_6 = &(__this->___uv_1);
		float L_7 = Rect_get_y_m1760(L_6, /*hidden argument*/NULL);
		Rect_set_height_m1765(L_4, ((float)((float)L_5-(float)L_7)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeftUnFlipped()
extern "C" Vector2_t2  CharacterInfo_get_uvTopLeftUnFlipped_m4568 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m1758(L_0, /*hidden argument*/NULL);
		Rect_t267 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m1760(L_2, /*hidden argument*/NULL);
		Rect_t267 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_height_m1764(L_4, /*hidden argument*/NULL);
		Vector2_t2  L_6 = {0};
		Vector2__ctor_m1309(&L_6, L_1, ((float)((float)L_3+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopLeftUnFlipped(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopLeftUnFlipped_m4569 (CharacterInfo_t892 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	Vector2_t2  V_0 = {0};
	{
		Vector2_t2  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m4566(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rect_t267 * L_1 = &(__this->___uv_1);
		float L_2 = ((&___value)->___x_1);
		Rect_set_x_m1759(L_1, L_2, /*hidden argument*/NULL);
		Rect_t267 * L_3 = &(__this->___uv_1);
		float L_4 = ((&___value)->___y_2);
		Rect_t267 * L_5 = &(__this->___uv_1);
		float L_6 = Rect_get_y_m1760(L_5, /*hidden argument*/NULL);
		Rect_set_height_m1765(L_3, ((float)((float)L_4-(float)L_6)), /*hidden argument*/NULL);
		Rect_t267 * L_7 = &(__this->___uv_1);
		float L_8 = ((&V_0)->___x_1);
		Rect_t267 * L_9 = &(__this->___uv_1);
		float L_10 = Rect_get_x_m1758(L_9, /*hidden argument*/NULL);
		Rect_set_width_m1763(L_7, ((float)((float)L_8-(float)L_10)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeft()
extern "C" Vector2_t2  CharacterInfo_get_uvBottomLeft_m4570 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = CharacterInfo_get_uvBottomLeftUnFlipped_m4562(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomLeft(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvBottomLeft_m4571 (CharacterInfo_t892 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = ___value;
		CharacterInfo_set_uvBottomLeftUnFlipped_m4563(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRight()
extern "C" Vector2_t2  CharacterInfo_get_uvBottomRight_m4572 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	Vector2_t2  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t2  L_1 = CharacterInfo_get_uvTopLeftUnFlipped_m4568(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t2  L_2 = CharacterInfo_get_uvBottomRightUnFlipped_m4564(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomRight(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvBottomRight_m4573 (CharacterInfo_t892 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Vector2_t2  L_1 = ___value;
		CharacterInfo_set_uvTopLeftUnFlipped_m4569(__this, L_1, /*hidden argument*/NULL);
		goto IL_001e;
	}

IL_0017:
	{
		Vector2_t2  L_2 = ___value;
		CharacterInfo_set_uvBottomRightUnFlipped_m4565(__this, L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRight()
extern "C" Vector2_t2  CharacterInfo_get_uvTopRight_m4574 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m4566(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopRight(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopRight_m4575 (CharacterInfo_t892 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = ___value;
		CharacterInfo_set_uvTopRightUnFlipped_m4567(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeft()
extern "C" Vector2_t2  CharacterInfo_get_uvTopLeft_m4576 (CharacterInfo_t892 * __this, const MethodInfo* method)
{
	Vector2_t2  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t2  L_1 = CharacterInfo_get_uvBottomRightUnFlipped_m4564(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t2  L_2 = CharacterInfo_get_uvTopLeftUnFlipped_m4568(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopLeft(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopLeft_m4577 (CharacterInfo_t892 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Vector2_t2  L_1 = ___value;
		CharacterInfo_set_uvBottomRightUnFlipped_m4565(__this, L_1, /*hidden argument*/NULL);
		goto IL_001e;
	}

IL_0017:
	{
		Vector2_t2  L_2 = ___value;
		CharacterInfo_set_uvTopLeftUnFlipped_m4569(__this, L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// Conversion methods for marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t892_marshal(const CharacterInfo_t892& unmarshaled, CharacterInfo_t892_marshaled& marshaled)
{
	marshaled.___index_0 = unmarshaled.___index_0;
	marshaled.___uv_1 = unmarshaled.___uv_1;
	marshaled.___vert_2 = unmarshaled.___vert_2;
	marshaled.___width_3 = unmarshaled.___width_3;
	marshaled.___size_4 = unmarshaled.___size_4;
	marshaled.___style_5 = unmarshaled.___style_5;
	marshaled.___flipped_6 = unmarshaled.___flipped_6;
	marshaled.___ascent_7 = unmarshaled.___ascent_7;
}
extern "C" void CharacterInfo_t892_marshal_back(const CharacterInfo_t892_marshaled& marshaled, CharacterInfo_t892& unmarshaled)
{
	unmarshaled.___index_0 = marshaled.___index_0;
	unmarshaled.___uv_1 = marshaled.___uv_1;
	unmarshaled.___vert_2 = marshaled.___vert_2;
	unmarshaled.___width_3 = marshaled.___width_3;
	unmarshaled.___size_4 = marshaled.___size_4;
	unmarshaled.___style_5 = marshaled.___style_5;
	unmarshaled.___flipped_6 = marshaled.___flipped_6;
	unmarshaled.___ascent_7 = marshaled.___ascent_7;
}
// Conversion method for clean up from marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t892_marshal_cleanup(CharacterInfo_t892_marshaled& marshaled)
{
}
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallback.h"
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallbackMethodDeclarations.h"
// System.Void UnityEngine.Font/FontTextureRebuildCallback::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void FontTextureRebuildCallback__ctor_m4578 (FontTextureRebuildCallback_t893 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::Invoke()
extern "C" void FontTextureRebuildCallback_Invoke_m4579 (FontTextureRebuildCallback_t893 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FontTextureRebuildCallback_Invoke_m4579((FontTextureRebuildCallback_t893 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t893(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Font/FontTextureRebuildCallback::BeginInvoke(System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * FontTextureRebuildCallback_BeginInvoke_m4580 (FontTextureRebuildCallback_t893 * __this, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::EndInvoke(System.IAsyncResult)
extern "C" void FontTextureRebuildCallback_EndInvoke_m4581 (FontTextureRebuildCallback_t893 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
// UnityEngine.Font
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
// System.Action`1<UnityEngine.Font>
#include "mscorlib_System_Action_1_gen_3.h"
// System.Char
#include "mscorlib_System_Char.h"
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// System.Action`1<UnityEngine.Font>
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"
// System.Void UnityEngine.Font::.ctor()
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Font
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
extern "C" void Font__ctor_m4582 (Font_t569 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m4335(__this, /*hidden argument*/NULL);
		Font_Internal_CreateFont_m4589(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::.ctor(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" void Font__ctor_m4583 (Font_t569 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Object__ctor_m4335(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		Font_Internal_CreateFont_m4589(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::.ctor(System.String[],System.Int32)
#include "mscorlib_ArrayTypes.h"
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void Font__ctor_m4584 (Font_t569 * __this, StringU5BU5D_t137* ___names, int32_t ___size, const MethodInfo* method)
{
	{
		Object__ctor_m4335(__this, /*hidden argument*/NULL);
		StringU5BU5D_t137* L_0 = ___names;
		int32_t L_1 = ___size;
		Font_Internal_CreateDynamicFont_m4590(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::add_textureRebuilt(System.Action`1<UnityEngine.Font>)
// System.Action`1<UnityEngine.Font>
#include "mscorlib_System_Action_1_gen_3.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
extern TypeInfo* Font_t569_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t739_il2cpp_TypeInfo_var;
extern "C" void Font_add_textureRebuilt_m3499 (Object_t * __this /* static, unused */, Action_1_t739 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(425);
		Action_1_t739_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(429);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t739 * L_0 = ((Font_t569_StaticFields*)Font_t569_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t739 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Combine_m3530(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t569_StaticFields*)Font_t569_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t739 *)CastclassSealed(L_2, Action_1_t739_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::remove_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t569_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t739_il2cpp_TypeInfo_var;
extern "C" void Font_remove_textureRebuilt_m4585 (Object_t * __this /* static, unused */, Action_1_t739 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(425);
		Action_1_t739_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(429);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t739 * L_0 = ((Font_t569_StaticFields*)Font_t569_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t739 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Remove_m3531(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t569_StaticFields*)Font_t569_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t739 *)CastclassSealed(L_2, Action_1_t739_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::add_m_FontTextureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallback.h"
extern TypeInfo* FontTextureRebuildCallback_t893_il2cpp_TypeInfo_var;
extern "C" void Font_add_m_FontTextureRebuildCallback_m4586 (Font_t569 * __this, FontTextureRebuildCallback_t893 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontTextureRebuildCallback_t893_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(592);
		s_Il2CppMethodIntialized = true;
	}
	{
		FontTextureRebuildCallback_t893 * L_0 = (__this->___m_FontTextureRebuildCallback_3);
		FontTextureRebuildCallback_t893 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Combine_m3530(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___m_FontTextureRebuildCallback_3 = ((FontTextureRebuildCallback_t893 *)CastclassSealed(L_2, FontTextureRebuildCallback_t893_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::remove_m_FontTextureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
extern TypeInfo* FontTextureRebuildCallback_t893_il2cpp_TypeInfo_var;
extern "C" void Font_remove_m_FontTextureRebuildCallback_m4587 (Font_t569 * __this, FontTextureRebuildCallback_t893 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontTextureRebuildCallback_t893_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(592);
		s_Il2CppMethodIntialized = true;
	}
	{
		FontTextureRebuildCallback_t893 * L_0 = (__this->___m_FontTextureRebuildCallback_3);
		FontTextureRebuildCallback_t893 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Remove_m3531(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___m_FontTextureRebuildCallback_3 = ((FontTextureRebuildCallback_t893 *)CastclassSealed(L_2, FontTextureRebuildCallback_t893_il2cpp_TypeInfo_var));
		return;
	}
}
// System.String[] UnityEngine.Font::GetOSInstalledFontNames()
extern "C" StringU5BU5D_t137* Font_GetOSInstalledFontNames_m4588 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef StringU5BU5D_t137* (*Font_GetOSInstalledFontNames_m4588_ftn) ();
	static Font_GetOSInstalledFontNames_m4588_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_GetOSInstalledFontNames_m4588_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::GetOSInstalledFontNames()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Font::Internal_CreateFont(UnityEngine.Font,System.String)
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
extern "C" void Font_Internal_CreateFont_m4589 (Object_t * __this /* static, unused */, Font_t569 * ____font, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Font_Internal_CreateFont_m4589_ftn) (Font_t569 *, String_t*);
	static Font_Internal_CreateFont_m4589_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_Internal_CreateFont_m4589_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::Internal_CreateFont(UnityEngine.Font,System.String)");
	_il2cpp_icall_func(____font, ___name);
}
// System.Void UnityEngine.Font::Internal_CreateDynamicFont(UnityEngine.Font,System.String[],System.Int32)
extern "C" void Font_Internal_CreateDynamicFont_m4590 (Object_t * __this /* static, unused */, Font_t569 * ____font, StringU5BU5D_t137* ____names, int32_t ___size, const MethodInfo* method)
{
	typedef void (*Font_Internal_CreateDynamicFont_m4590_ftn) (Font_t569 *, StringU5BU5D_t137*, int32_t);
	static Font_Internal_CreateDynamicFont_m4590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_Internal_CreateDynamicFont_m4590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::Internal_CreateDynamicFont(UnityEngine.Font,System.String[],System.Int32)");
	_il2cpp_icall_func(____font, ____names, ___size);
}
// UnityEngine.Font UnityEngine.Font::CreateDynamicFontFromOSFont(System.String,System.Int32)
extern TypeInfo* StringU5BU5D_t137_il2cpp_TypeInfo_var;
extern TypeInfo* Font_t569_il2cpp_TypeInfo_var;
extern "C" Font_t569 * Font_CreateDynamicFontFromOSFont_m4591 (Object_t * __this /* static, unused */, String_t* ___fontname, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		Font_t569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(425);
		s_Il2CppMethodIntialized = true;
	}
	Font_t569 * V_0 = {0};
	{
		StringU5BU5D_t137* L_0 = ((StringU5BU5D_t137*)SZArrayNew(StringU5BU5D_t137_il2cpp_TypeInfo_var, 1));
		String_t* L_1 = ___fontname;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)L_1;
		int32_t L_2 = ___size;
		Font_t569 * L_3 = (Font_t569 *)il2cpp_codegen_object_new (Font_t569_il2cpp_TypeInfo_var);
		Font__ctor_m4584(L_3, L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Font_t569 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Font UnityEngine.Font::CreateDynamicFontFromOSFont(System.String[],System.Int32)
extern TypeInfo* Font_t569_il2cpp_TypeInfo_var;
extern "C" Font_t569 * Font_CreateDynamicFontFromOSFont_m4592 (Object_t * __this /* static, unused */, StringU5BU5D_t137* ___fontnames, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(425);
		s_Il2CppMethodIntialized = true;
	}
	Font_t569 * V_0 = {0};
	{
		StringU5BU5D_t137* L_0 = ___fontnames;
		int32_t L_1 = ___size;
		Font_t569 * L_2 = (Font_t569 *)il2cpp_codegen_object_new (Font_t569_il2cpp_TypeInfo_var);
		Font__ctor_m4584(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Font_t569 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Material UnityEngine.Font::get_material()
extern "C" Material_t82 * Font_get_material_m3727 (Font_t569 * __this, const MethodInfo* method)
{
	typedef Material_t82 * (*Font_get_material_m3727_ftn) (Font_t569 *);
	static Font_get_material_m3727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_material_m3727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Font::set_material(UnityEngine.Material)
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
extern "C" void Font_set_material_m4593 (Font_t569 * __this, Material_t82 * ___value, const MethodInfo* method)
{
	typedef void (*Font_set_material_m4593_ftn) (Font_t569 *, Material_t82 *);
	static Font_set_material_m4593_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_set_material_m4593_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Font::HasCharacter(System.Char)
// System.Char
#include "mscorlib_System_Char.h"
extern "C" bool Font_HasCharacter_m3615 (Font_t569 * __this, uint16_t ___c, const MethodInfo* method)
{
	typedef bool (*Font_HasCharacter_m3615_ftn) (Font_t569 *, uint16_t);
	static Font_HasCharacter_m3615_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_HasCharacter_m3615_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::HasCharacter(System.Char)");
	return _il2cpp_icall_func(__this, ___c);
}
// System.String[] UnityEngine.Font::get_fontNames()
extern "C" StringU5BU5D_t137* Font_get_fontNames_m4594 (Font_t569 * __this, const MethodInfo* method)
{
	typedef StringU5BU5D_t137* (*Font_get_fontNames_m4594_ftn) (Font_t569 *);
	static Font_get_fontNames_m4594_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_fontNames_m4594_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_fontNames()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Font::set_fontNames(System.String[])
extern "C" void Font_set_fontNames_m4595 (Font_t569 * __this, StringU5BU5D_t137* ___value, const MethodInfo* method)
{
	typedef void (*Font_set_fontNames_m4595_ftn) (Font_t569 *, StringU5BU5D_t137*);
	static Font_set_fontNames_m4595_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_set_fontNames_m4595_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::set_fontNames(System.String[])");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.CharacterInfo[] UnityEngine.Font::get_characterInfo()
extern "C" CharacterInfoU5BU5D_t1008* Font_get_characterInfo_m4596 (Font_t569 * __this, const MethodInfo* method)
{
	typedef CharacterInfoU5BU5D_t1008* (*Font_get_characterInfo_m4596_ftn) (Font_t569 *);
	static Font_get_characterInfo_m4596_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_characterInfo_m4596_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_characterInfo()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Font::set_characterInfo(UnityEngine.CharacterInfo[])
#include "UnityEngine_ArrayTypes.h"
extern "C" void Font_set_characterInfo_m4597 (Font_t569 * __this, CharacterInfoU5BU5D_t1008* ___value, const MethodInfo* method)
{
	typedef void (*Font_set_characterInfo_m4597_ftn) (Font_t569 *, CharacterInfoU5BU5D_t1008*);
	static Font_set_characterInfo_m4597_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_set_characterInfo_m4597_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::set_characterInfo(UnityEngine.CharacterInfo[])");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32,UnityEngine.FontStyle)
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
extern "C" void Font_RequestCharactersInTexture_m4598 (Font_t569 * __this, String_t* ___characters, int32_t ___size, int32_t ___style, const MethodInfo* method)
{
	typedef void (*Font_RequestCharactersInTexture_m4598_ftn) (Font_t569 *, String_t*, int32_t, int32_t);
	static Font_RequestCharactersInTexture_m4598_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_RequestCharactersInTexture_m4598_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32,UnityEngine.FontStyle)");
	_il2cpp_icall_func(__this, ___characters, ___size, ___style);
}
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32)
extern "C" void Font_RequestCharactersInTexture_m4599 (Font_t569 * __this, String_t* ___characters, int32_t ___size, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		String_t* L_0 = ___characters;
		int32_t L_1 = ___size;
		int32_t L_2 = V_0;
		Font_RequestCharactersInTexture_m4598(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String)
extern "C" void Font_RequestCharactersInTexture_m4600 (Font_t569 * __this, String_t* ___characters, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		String_t* L_0 = ___characters;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		Font_RequestCharactersInTexture_m4598(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::InvokeTextureRebuilt_Internal(UnityEngine.Font)
// System.Action`1<UnityEngine.Font>
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallbackMethodDeclarations.h"
extern TypeInfo* Font_t569_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m5140_MethodInfo_var;
extern "C" void Font_InvokeTextureRebuilt_Internal_m4601 (Object_t * __this /* static, unused */, Font_t569 * ___font, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(425);
		Action_1_Invoke_m5140_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484225);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t739 * V_0 = {0};
	{
		Action_1_t739 * L_0 = ((Font_t569_StaticFields*)Font_t569_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		V_0 = L_0;
		Action_1_t739 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t739 * L_2 = V_0;
		Font_t569 * L_3 = ___font;
		NullCheck(L_2);
		Action_1_Invoke_m5140(L_2, L_3, /*hidden argument*/Action_1_Invoke_m5140_MethodInfo_var);
	}

IL_0013:
	{
		Font_t569 * L_4 = ___font;
		NullCheck(L_4);
		FontTextureRebuildCallback_t893 * L_5 = (L_4->___m_FontTextureRebuildCallback_3);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		Font_t569 * L_6 = ___font;
		NullCheck(L_6);
		FontTextureRebuildCallback_t893 * L_7 = (L_6->___m_FontTextureRebuildCallback_3);
		NullCheck(L_7);
		FontTextureRebuildCallback_Invoke_m4579(L_7, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::get_textureRebuildCallback()
extern "C" FontTextureRebuildCallback_t893 * Font_get_textureRebuildCallback_m4602 (Font_t569 * __this, const MethodInfo* method)
{
	{
		FontTextureRebuildCallback_t893 * L_0 = (__this->___m_FontTextureRebuildCallback_3);
		return L_0;
	}
}
// System.Void UnityEngine.Font::set_textureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
extern "C" void Font_set_textureRebuildCallback_m4603 (Font_t569 * __this, FontTextureRebuildCallback_t893 * ___value, const MethodInfo* method)
{
	{
		FontTextureRebuildCallback_t893 * L_0 = ___value;
		__this->___m_FontTextureRebuildCallback_3 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Font::GetMaxVertsForString(System.String)
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern "C" int32_t Font_GetMaxVertsForString_m4604 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1336(L_0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1*(int32_t)4))+(int32_t)4));
	}
}
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32,UnityEngine.FontStyle)
extern "C" bool Font_GetCharacterInfo_m4605 (Font_t569 * __this, uint16_t ___ch, CharacterInfo_t892 * ___info, int32_t ___size, int32_t ___style, const MethodInfo* method)
{
	typedef bool (*Font_GetCharacterInfo_m4605_ftn) (Font_t569 *, uint16_t, CharacterInfo_t892 *, int32_t, int32_t);
	static Font_GetCharacterInfo_m4605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_GetCharacterInfo_m4605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32,UnityEngine.FontStyle)");
	return _il2cpp_icall_func(__this, ___ch, ___info, ___size, ___style);
}
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32)
extern "C" bool Font_GetCharacterInfo_m4606 (Font_t569 * __this, uint16_t ___ch, CharacterInfo_t892 * ___info, int32_t ___size, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		uint16_t L_0 = ___ch;
		CharacterInfo_t892 * L_1 = ___info;
		int32_t L_2 = ___size;
		int32_t L_3 = V_0;
		bool L_4 = Font_GetCharacterInfo_m4605(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&)
extern "C" bool Font_GetCharacterInfo_m4607 (Font_t569 * __this, uint16_t ___ch, CharacterInfo_t892 * ___info, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		uint16_t L_0 = ___ch;
		CharacterInfo_t892 * L_1 = ___info;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Font_GetCharacterInfo_m4605(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Font::get_dynamic()
extern "C" bool Font_get_dynamic_m3730 (Font_t569 * __this, const MethodInfo* method)
{
	typedef bool (*Font_get_dynamic_m3730_ftn) (Font_t569 *);
	static Font_get_dynamic_m3730_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_dynamic_m3730_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_dynamic()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_ascent()
extern "C" int32_t Font_get_ascent_m4608 (Font_t569 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_ascent_m4608_ftn) (Font_t569 *);
	static Font_get_ascent_m4608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_ascent_m4608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_ascent()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_lineHeight()
extern "C" int32_t Font_get_lineHeight_m4609 (Font_t569 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_lineHeight_m4609_ftn) (Font_t569 *);
	static Font_get_lineHeight_m4609_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_lineHeight_m4609_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_lineHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_fontSize()
extern "C" int32_t Font_get_fontSize_m3732 (Font_t569 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_fontSize_m3732_ftn) (Font_t569 *);
	static Font_get_fontSize_m3732_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_fontSize_m3732_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_fontSize()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfoMethodDeclarations.h"
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfoMethodDeclarations.h"
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGenerator.h"
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_26.h"
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_44.h"
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_45.h"
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_26MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_44MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_45MethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
// System.Void UnityEngine.TextGenerator::.ctor()
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"
extern "C" void TextGenerator__ctor_m3583 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	{
		TextGenerator__ctor_m3726(__this, ((int32_t)50), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_26MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_44MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_45MethodDeclarations.h"
extern TypeInfo* List_1_t709_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t894_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t895_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m5141_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m5142_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m5143_MethodInfo_var;
extern "C" void TextGenerator__ctor_m3726 (TextGenerator_t603 * __this, int32_t ___initialCapacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(541);
		List_1_t894_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(593);
		List_1_t895_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(594);
		List_1__ctor_m5141_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484226);
		List_1__ctor_m5142_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484227);
		List_1__ctor_m5143_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484228);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialCapacity;
		List_1_t709 * L_1 = (List_1_t709 *)il2cpp_codegen_object_new (List_1_t709_il2cpp_TypeInfo_var);
		List_1__ctor_m5141(L_1, ((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))*(int32_t)4)), /*hidden argument*/List_1__ctor_m5141_MethodInfo_var);
		__this->___m_Verts_5 = L_1;
		int32_t L_2 = ___initialCapacity;
		List_1_t894 * L_3 = (List_1_t894 *)il2cpp_codegen_object_new (List_1_t894_il2cpp_TypeInfo_var);
		List_1__ctor_m5142(L_3, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/List_1__ctor_m5142_MethodInfo_var);
		__this->___m_Characters_6 = L_3;
		List_1_t895 * L_4 = (List_1_t895 *)il2cpp_codegen_object_new (List_1_t895_il2cpp_TypeInfo_var);
		List_1__ctor_m5143(L_4, ((int32_t)20), /*hidden argument*/List_1__ctor_m5143_MethodInfo_var);
		__this->___m_Lines_7 = L_4;
		TextGenerator_Init_m4611(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::System.IDisposable.Dispose()
extern "C" void TextGenerator_System_IDisposable_Dispose_m4610 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	{
		TextGenerator_Dispose_cpp_m4612(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::Init()
extern "C" void TextGenerator_Init_m4611 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Init_m4611_ftn) (TextGenerator_t603 *);
	static TextGenerator_Init_m4611_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Init_m4611_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Dispose_cpp()
extern "C" void TextGenerator_Dispose_cpp_m4612 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Dispose_cpp_m4612_ftn) (TextGenerator_t603 *);
	static TextGenerator_Dispose_cpp_m4612_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Dispose_cpp_m4612_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Dispose_cpp()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
extern "C" bool TextGenerator_Populate_Internal_m4613 (TextGenerator_t603 * __this, String_t* ___str, Font_t569 * ___font, Color_t9  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, Vector2_t2  ___extents, Vector2_t2  ___pivot, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t569 * L_1 = ___font;
		Color_t9  L_2 = ___color;
		int32_t L_3 = ___fontSize;
		float L_4 = ___scaleFactor;
		float L_5 = ___lineSpacing;
		int32_t L_6 = ___style;
		bool L_7 = ___richText;
		bool L_8 = ___resizeTextForBestFit;
		int32_t L_9 = ___resizeTextMinSize;
		int32_t L_10 = ___resizeTextMaxSize;
		int32_t L_11 = ___verticalOverFlow;
		int32_t L_12 = ___horizontalOverflow;
		bool L_13 = ___updateBounds;
		int32_t L_14 = ___anchor;
		float L_15 = ((&___extents)->___x_1);
		float L_16 = ((&___extents)->___y_2);
		float L_17 = ((&___pivot)->___x_1);
		float L_18 = ((&___pivot)->___y_2);
		bool L_19 = ___generateOutOfBounds;
		bool L_20 = TextGenerator_Populate_Internal_cpp_m4614(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal_cpp(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_cpp_m4614 (TextGenerator_t603 * __this, String_t* ___str, Font_t569 * ___font, Color_t9  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t569 * L_1 = ___font;
		int32_t L_2 = ___fontSize;
		float L_3 = ___scaleFactor;
		float L_4 = ___lineSpacing;
		int32_t L_5 = ___style;
		bool L_6 = ___richText;
		bool L_7 = ___resizeTextForBestFit;
		int32_t L_8 = ___resizeTextMinSize;
		int32_t L_9 = ___resizeTextMaxSize;
		int32_t L_10 = ___verticalOverFlow;
		int32_t L_11 = ___horizontalOverflow;
		bool L_12 = ___updateBounds;
		int32_t L_13 = ___anchor;
		float L_14 = ___extentsX;
		float L_15 = ___extentsY;
		float L_16 = ___pivotX;
		float L_17 = ___pivotY;
		bool L_18 = ___generateOutOfBounds;
		bool L_19 = TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m4615(NULL /*static, unused*/, __this, L_0, L_1, (&___color), L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGenerator.h"
extern "C" bool TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m4615 (Object_t * __this /* static, unused */, TextGenerator_t603 * ___self, String_t* ___str, Font_t569 * ___font, Color_t9 * ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	typedef bool (*TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m4615_ftn) (TextGenerator_t603 *, String_t*, Font_t569 *, Color_t9 *, int32_t, float, float, int32_t, bool, bool, int32_t, int32_t, int32_t, int32_t, bool, int32_t, float, float, float, float, bool);
	static TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m4615_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m4615_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)");
	return _il2cpp_icall_func(___self, ___str, ___font, ___color, ___fontSize, ___scaleFactor, ___lineSpacing, ___style, ___richText, ___resizeTextForBestFit, ___resizeTextMinSize, ___resizeTextMaxSize, ___verticalOverFlow, ___horizontalOverflow, ___updateBounds, ___anchor, ___extentsX, ___extentsY, ___pivotX, ___pivotY, ___generateOutOfBounds);
}
// UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern "C" Rect_t267  TextGenerator_get_rectExtents_m3628 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	Rect_t267  V_0 = {0};
	{
		TextGenerator_INTERNAL_get_rectExtents_m4616(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t267  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
extern "C" void TextGenerator_INTERNAL_get_rectExtents_m4616 (TextGenerator_t603 * __this, Rect_t267 * ___value, const MethodInfo* method)
{
	typedef void (*TextGenerator_INTERNAL_get_rectExtents_m4616_ftn) (TextGenerator_t603 *, Rect_t267 *);
	static TextGenerator_INTERNAL_get_rectExtents_m4616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_get_rectExtents_m4616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.TextGenerator::get_vertexCount()
extern "C" int32_t TextGenerator_get_vertexCount_m4617 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_vertexCount_m4617_ftn) (TextGenerator_t603 *);
	static TextGenerator_get_vertexCount_m4617_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_vertexCount_m4617_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" void TextGenerator_GetVerticesInternal_m4618 (TextGenerator_t603 * __this, Object_t * ___vertices, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetVerticesInternal_m4618_ftn) (TextGenerator_t603 *, Object_t *);
	static TextGenerator_GetVerticesInternal_m4618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesInternal_m4618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// UnityEngine.UIVertex[] UnityEngine.TextGenerator::GetVerticesArray()
extern "C" UIVertexU5BU5D_t602* TextGenerator_GetVerticesArray_m4619 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	typedef UIVertexU5BU5D_t602* (*TextGenerator_GetVerticesArray_m4619_ftn) (TextGenerator_t603 *);
	static TextGenerator_GetVerticesArray_m4619_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesArray_m4619_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern "C" int32_t TextGenerator_get_characterCount_m4620 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_characterCount_m4620_ftn) (TextGenerator_t603 *);
	static TextGenerator_get_characterCount_m4620_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_characterCount_m4620_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_characterCount()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCountVisible()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" int32_t TextGenerator_get_characterCountVisible_m3609 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0036;
	}

IL_0016:
	{
		String_t* L_2 = (__this->___m_LastString_1);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1336(L_2, /*hidden argument*/NULL);
		int32_t L_4 = TextGenerator_get_vertexCount_m4617(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Max_m3620(NULL /*static, unused*/, 0, ((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)4))/(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_6 = Mathf_Min_m1717(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern "C" void TextGenerator_GetCharactersInternal_m4621 (TextGenerator_t603 * __this, Object_t * ___characters, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetCharactersInternal_m4621_ftn) (TextGenerator_t603 *, Object_t *);
	static TextGenerator_GetCharactersInternal_m4621_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersInternal_m4621_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersInternal(System.Object)");
	_il2cpp_icall_func(__this, ___characters);
}
// UnityEngine.UICharInfo[] UnityEngine.TextGenerator::GetCharactersArray()
extern "C" UICharInfoU5BU5D_t1009* TextGenerator_GetCharactersArray_m4622 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	typedef UICharInfoU5BU5D_t1009* (*TextGenerator_GetCharactersArray_m4622_ftn) (TextGenerator_t603 *);
	static TextGenerator_GetCharactersArray_m4622_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersArray_m4622_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_lineCount()
extern "C" int32_t TextGenerator_get_lineCount_m3608 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_lineCount_m3608_ftn) (TextGenerator_t603 *);
	static TextGenerator_get_lineCount_m3608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_lineCount_m3608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_lineCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern "C" void TextGenerator_GetLinesInternal_m4623 (TextGenerator_t603 * __this, Object_t * ___lines, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetLinesInternal_m4623_ftn) (TextGenerator_t603 *, Object_t *);
	static TextGenerator_GetLinesInternal_m4623_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesInternal_m4623_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___lines);
}
// UnityEngine.UILineInfo[] UnityEngine.TextGenerator::GetLinesArray()
extern "C" UILineInfoU5BU5D_t1010* TextGenerator_GetLinesArray_m4624 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	typedef UILineInfoU5BU5D_t1010* (*TextGenerator_GetLinesArray_m4624_ftn) (TextGenerator_t603 *);
	static TextGenerator_GetLinesArray_m4624_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesArray_m4624_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()
extern "C" int32_t TextGenerator_get_fontSizeUsedForBestFit_m3638 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_fontSizeUsedForBestFit_m3638_ftn) (TextGenerator_t603 *);
	static TextGenerator_get_fontSizeUsedForBestFit_m3638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_fontSizeUsedForBestFit_m3638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Finalize()
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void TextGenerator_Finalize_m4625 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m5102(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Font
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern Il2CppCodeGenString* _stringLiteral567;
extern Il2CppCodeGenString* _stringLiteral568;
extern "C" TextGenerationSettings_t715  TextGenerator_ValidatedSettings_m4626 (TextGenerator_t603 * __this, TextGenerationSettings_t715  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral567 = il2cpp_codegen_string_literal_from_index(567);
		_stringLiteral568 = il2cpp_codegen_string_literal_from_index(568);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t569 * L_0 = ((&___settings)->___font_0);
		bool L_1 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Font_t569 * L_2 = ((&___settings)->___font_0);
		NullCheck(L_2);
		bool L_3 = Font_get_dynamic_m3730(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		TextGenerationSettings_t715  L_4 = ___settings;
		return L_4;
	}

IL_0025:
	{
		int32_t L_5 = ((&___settings)->___fontSize_2);
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_6 = ((&___settings)->___fontStyle_6);
		if (!L_6)
		{
			goto IL_0057;
		}
	}

IL_003d:
	{
		Debug_LogWarning_m1399(NULL /*static, unused*/, _stringLiteral567, /*hidden argument*/NULL);
		(&___settings)->___fontSize_2 = 0;
		(&___settings)->___fontStyle_6 = 0;
	}

IL_0057:
	{
		bool L_7 = ((&___settings)->___resizeTextForBestFit_8);
		if (!L_7)
		{
			goto IL_0075;
		}
	}
	{
		Debug_LogWarning_m1399(NULL /*static, unused*/, _stringLiteral568, /*hidden argument*/NULL);
		(&___settings)->___resizeTextForBestFit_8 = 0;
	}

IL_0075:
	{
		TextGenerationSettings_t715  L_8 = ___settings;
		return L_8;
	}
}
// System.Void UnityEngine.TextGenerator::Invalidate()
extern "C" void TextGenerator_Invalidate_m3729 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	{
		__this->___m_HasGenerated_3 = 0;
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_44.h"
extern "C" void TextGenerator_GetCharacters_m4627 (TextGenerator_t603 * __this, List_1_t894 * ___characters, const MethodInfo* method)
{
	{
		List_1_t894 * L_0 = ___characters;
		TextGenerator_GetCharactersInternal_m4621(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_45.h"
extern "C" void TextGenerator_GetLines_m4628 (TextGenerator_t603 * __this, List_1_t895 * ___lines, const MethodInfo* method)
{
	{
		List_1_t895 * L_0 = ___lines;
		TextGenerator_GetLinesInternal_m4623(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_26.h"
extern "C" void TextGenerator_GetVertices_m4629 (TextGenerator_t603 * __this, List_1_t709 * ___vertices, const MethodInfo* method)
{
	{
		List_1_t709 * L_0 = ___vertices;
		TextGenerator_GetVerticesInternal_m4618(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredWidth(System.String,UnityEngine.TextGenerationSettings)
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
extern "C" float TextGenerator_GetPreferredWidth_m3734 (TextGenerator_t603 * __this, String_t* ___str, TextGenerationSettings_t715  ___settings, const MethodInfo* method)
{
	Rect_t267  V_0 = {0};
	{
		(&___settings)->___horizontalOverflow_13 = 1;
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t715  L_1 = ___settings;
		TextGenerator_Populate_m3627(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t267  L_2 = TextGenerator_get_rectExtents_m3628(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m1762((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredHeight(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredHeight_m3735 (TextGenerator_t603 * __this, String_t* ___str, TextGenerationSettings_t715  ___settings, const MethodInfo* method)
{
	Rect_t267  V_0 = {0};
	{
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t715  L_1 = ___settings;
		TextGenerator_Populate_m3627(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t267  L_2 = TextGenerator_get_rectExtents_m3628(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_height_m1764((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextGenerator_Populate_m3627 (TextGenerator_t603 * __this, String_t* ___str, TextGenerationSettings_t715  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_HasGenerated_3);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_1 = ___str;
		String_t* L_2 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1773(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		TextGenerationSettings_t715  L_4 = (__this->___m_LastSettings_2);
		bool L_5 = TextGenerationSettings_Equals_m5031((&___settings), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		bool L_6 = (__this->___m_LastValid_4);
		return L_6;
	}

IL_0035:
	{
		String_t* L_7 = ___str;
		TextGenerationSettings_t715  L_8 = ___settings;
		bool L_9 = TextGenerator_PopulateAlways_m4630(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern "C" bool TextGenerator_PopulateAlways_m4630 (TextGenerator_t603 * __this, String_t* ___str, TextGenerationSettings_t715  ___settings, const MethodInfo* method)
{
	TextGenerationSettings_t715  V_0 = {0};
	{
		String_t* L_0 = ___str;
		__this->___m_LastString_1 = L_0;
		__this->___m_HasGenerated_3 = 1;
		__this->___m_CachedVerts_8 = 0;
		__this->___m_CachedCharacters_9 = 0;
		__this->___m_CachedLines_10 = 0;
		TextGenerationSettings_t715  L_1 = ___settings;
		__this->___m_LastSettings_2 = L_1;
		TextGenerationSettings_t715  L_2 = ___settings;
		TextGenerationSettings_t715  L_3 = TextGenerator_ValidatedSettings_m4626(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ___str;
		Font_t569 * L_5 = ((&V_0)->___font_0);
		Color_t9  L_6 = ((&V_0)->___color_1);
		int32_t L_7 = ((&V_0)->___fontSize_2);
		float L_8 = ((&V_0)->___scaleFactor_5);
		float L_9 = ((&V_0)->___lineSpacing_3);
		int32_t L_10 = ((&V_0)->___fontStyle_6);
		bool L_11 = ((&V_0)->___richText_4);
		bool L_12 = ((&V_0)->___resizeTextForBestFit_8);
		int32_t L_13 = ((&V_0)->___resizeTextMinSize_9);
		int32_t L_14 = ((&V_0)->___resizeTextMaxSize_10);
		int32_t L_15 = ((&V_0)->___verticalOverflow_12);
		int32_t L_16 = ((&V_0)->___horizontalOverflow_13);
		bool L_17 = ((&V_0)->___updateBounds_11);
		int32_t L_18 = ((&V_0)->___textAnchor_7);
		Vector2_t2  L_19 = ((&V_0)->___generationExtents_14);
		Vector2_t2  L_20 = ((&V_0)->___pivot_15);
		bool L_21 = ((&V_0)->___generateOutOfBounds_16);
		bool L_22 = TextGenerator_Populate_Internal_m4613(__this, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		__this->___m_LastValid_4 = L_22;
		bool L_23 = (__this->___m_LastValid_4);
		return L_23;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::get_verts()
extern "C" Object_t* TextGenerator_get_verts_m3733 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedVerts_8);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t709 * L_1 = (__this->___m_Verts_5);
		TextGenerator_GetVertices_m4629(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedVerts_8 = 1;
	}

IL_001e:
	{
		List_1_t709 * L_2 = (__this->___m_Verts_5);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::get_characters()
extern "C" Object_t* TextGenerator_get_characters_m3610 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedCharacters_9);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t894 * L_1 = (__this->___m_Characters_6);
		TextGenerator_GetCharacters_m4627(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedCharacters_9 = 1;
	}

IL_001e:
	{
		List_1_t894 * L_2 = (__this->___m_Characters_6);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::get_lines()
extern "C" Object_t* TextGenerator_get_lines_m3607 (TextGenerator_t603 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedLines_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t895 * L_1 = (__this->___m_Lines_7);
		TextGenerator_GetLines_m4628(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedLines_10 = 1;
	}

IL_001e:
	{
		List_1_t895 * L_2 = (__this->___m_Lines_7);
		return L_2;
	}
}
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderModeMethodDeclarations.h"
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases.h"
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvasesMethodDeclarations.h"
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void WillRenderCanvases__ctor_m3401 (WillRenderCanvases_t732 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C" void WillRenderCanvases_Invoke_m4631 (WillRenderCanvases_t732 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WillRenderCanvases_Invoke_m4631((WillRenderCanvases_t732 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t732(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Canvas/WillRenderCanvases::BeginInvoke(System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * WillRenderCanvases_BeginInvoke_m4632 (WillRenderCanvases_t732 * __this, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::EndInvoke(System.IAsyncResult)
extern "C" void WillRenderCanvases_EndInvoke_m4633 (WillRenderCanvases_t732 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
extern TypeInfo* Canvas_t574_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t732_il2cpp_TypeInfo_var;
extern "C" void Canvas_add_willRenderCanvases_m3402 (Object_t * __this /* static, unused */, WillRenderCanvases_t732 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t574_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(414);
		WillRenderCanvases_t732_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(389);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t732 * L_0 = ((Canvas_t574_StaticFields*)Canvas_t574_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		WillRenderCanvases_t732 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Combine_m3530(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t574_StaticFields*)Canvas_t574_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2 = ((WillRenderCanvases_t732 *)CastclassSealed(L_2, WillRenderCanvases_t732_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern TypeInfo* Canvas_t574_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t732_il2cpp_TypeInfo_var;
extern "C" void Canvas_remove_willRenderCanvases_m4634 (Object_t * __this /* static, unused */, WillRenderCanvases_t732 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t574_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(414);
		WillRenderCanvases_t732_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(389);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t732 * L_0 = ((Canvas_t574_StaticFields*)Canvas_t574_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		WillRenderCanvases_t732 * L_1 = ___value;
		Delegate_t466 * L_2 = Delegate_Remove_m3531(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t574_StaticFields*)Canvas_t574_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2 = ((WillRenderCanvases_t732 *)CastclassSealed(L_2, WillRenderCanvases_t732_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C" int32_t Canvas_get_renderMode_m3533 (Canvas_t574 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderMode_m3533_ftn) (Canvas_t574 *);
	static Canvas_get_renderMode_m3533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderMode_m3533_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderMode()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_isRootCanvas()
extern "C" bool Canvas_get_isRootCanvas_m3746 (Canvas_t574 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_isRootCanvas_m3746_ftn) (Canvas_t574 *);
	static Canvas_get_isRootCanvas_m3746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_isRootCanvas_m3746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_isRootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C" Camera_t6 * Canvas_get_worldCamera_m3545 (Canvas_t574 * __this, const MethodInfo* method)
{
	typedef Camera_t6 * (*Canvas_get_worldCamera_m3545_ftn) (Canvas_t574 *);
	static Canvas_get_worldCamera_m3545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_worldCamera_m3545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_worldCamera()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Canvas::get_scaleFactor()
extern "C" float Canvas_get_scaleFactor_m3731 (Canvas_t574 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_scaleFactor_m3731_ftn) (Canvas_t574 *);
	static Canvas_get_scaleFactor_m3731_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_scaleFactor_m3731_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_scaleFactor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void Canvas_set_scaleFactor_m3749 (Canvas_t574 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_scaleFactor_m3749_ftn) (Canvas_t574 *, float);
	static Canvas_set_scaleFactor_m3749_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_scaleFactor_m3749_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_scaleFactor(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
extern "C" float Canvas_get_referencePixelsPerUnit_m3561 (Canvas_t574 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_referencePixelsPerUnit_m3561_ftn) (Canvas_t574 *);
	static Canvas_get_referencePixelsPerUnit_m3561_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_referencePixelsPerUnit_m3561_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_referencePixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
extern "C" void Canvas_set_referencePixelsPerUnit_m3750 (Canvas_t574 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_referencePixelsPerUnit_m3750_ftn) (Canvas_t574 *, float);
	static Canvas_set_referencePixelsPerUnit_m3750_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_referencePixelsPerUnit_m3750_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Canvas::get_pixelPerfect()
extern "C" bool Canvas_get_pixelPerfect_m3522 (Canvas_t574 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_pixelPerfect_m3522_ftn) (Canvas_t574 *);
	static Canvas_get_pixelPerfect_m3522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_pixelPerfect_m3522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_pixelPerfect()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_renderOrder()
extern "C" int32_t Canvas_get_renderOrder_m3534 (Canvas_t574 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderOrder_m3534_ftn) (Canvas_t574 *);
	static Canvas_get_renderOrder_m3534_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderOrder_m3534_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_overrideSorting()
extern "C" bool Canvas_get_overrideSorting_m3663 (Canvas_t574 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_overrideSorting_m3663_ftn) (Canvas_t574 *);
	static Canvas_get_overrideSorting_m3663_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_overrideSorting_m3663_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_overrideSorting()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_overrideSorting(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void Canvas_set_overrideSorting_m3455 (Canvas_t574 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_overrideSorting_m3455_ftn) (Canvas_t574 *, bool);
	static Canvas_set_overrideSorting_m3455_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_overrideSorting_m3455_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_overrideSorting(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Canvas::get_sortingOrder()
extern "C" int32_t Canvas_get_sortingOrder_m3481 (Canvas_t574 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingOrder_m3481_ftn) (Canvas_t574 *);
	static Canvas_get_sortingOrder_m3481_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingOrder_m3481_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_sortingOrder(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void Canvas_set_sortingOrder_m3456 (Canvas_t574 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_sortingOrder_m3456_ftn) (Canvas_t574 *, int32_t);
	static Canvas_set_sortingOrder_m3456_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingOrder_m3456_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingOrder(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Canvas::get_sortingLayerID()
extern "C" int32_t Canvas_get_sortingLayerID_m3479 (Canvas_t574 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingLayerID_m3479_ftn) (Canvas_t574 *);
	static Canvas_get_sortingLayerID_m3479_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingLayerID_m3479_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_sortingLayerID(System.Int32)
extern "C" void Canvas_set_sortingLayerID_m3480 (Canvas_t574 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_sortingLayerID_m3480_ftn) (Canvas_t574 *, int32_t);
	static Canvas_set_sortingLayerID_m3480_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingLayerID_m3480_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingLayerID(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Canvas::get_cachedSortingLayerValue()
extern "C" int32_t Canvas_get_cachedSortingLayerValue_m3544 (Canvas_t574 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_cachedSortingLayerValue_m3544_ftn) (Canvas_t574 *);
	static Canvas_get_cachedSortingLayerValue_m3544_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_cachedSortingLayerValue_m3544_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_cachedSortingLayerValue()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
extern "C" Material_t82 * Canvas_GetDefaultCanvasMaterial_m3502 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t82 * (*Canvas_GetDefaultCanvasMaterial_m3502_ftn) ();
	static Canvas_GetDefaultCanvasMaterial_m3502_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasMaterial_m3502_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasMaterial()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvasesMethodDeclarations.h"
extern TypeInfo* Canvas_t574_il2cpp_TypeInfo_var;
extern "C" void Canvas_SendWillRenderCanvases_m4635 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t574_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(414);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t732 * L_0 = ((Canvas_t574_StaticFields*)Canvas_t574_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		WillRenderCanvases_t732 * L_1 = ((Canvas_t574_StaticFields*)Canvas_t574_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		NullCheck(L_1);
		WillRenderCanvases_Invoke_m4631(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::ForceUpdateCanvases()
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
extern "C" void Canvas_ForceUpdateCanvases_m3680 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Canvas_SendWillRenderCanvases_m4635(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroup.h"
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroupMethodDeclarations.h"
// System.Single UnityEngine.CanvasGroup::get_alpha()
extern "C" float CanvasGroup_get_alpha_m3487 (CanvasGroup_t733 * __this, const MethodInfo* method)
{
	typedef float (*CanvasGroup_get_alpha_m3487_ftn) (CanvasGroup_t733 *);
	static CanvasGroup_get_alpha_m3487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_alpha_m3487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_alpha()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void CanvasGroup_set_alpha_m3491 (CanvasGroup_t733 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_alpha_m3491_ftn) (CanvasGroup_t733 *, float);
	static CanvasGroup_set_alpha_m3491_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_alpha_m3491_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_alpha(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.CanvasGroup::get_interactable()
extern "C" bool CanvasGroup_get_interactable_m3706 (CanvasGroup_t733 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_interactable_m3706_ftn) (CanvasGroup_t733 *);
	static CanvasGroup_get_interactable_m3706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_interactable_m3706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_interactable()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C" bool CanvasGroup_get_blocksRaycasts_m4636 (CanvasGroup_t733 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_blocksRaycasts_m4636_ftn) (CanvasGroup_t733 *);
	static CanvasGroup_get_blocksRaycasts_m4636_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_blocksRaycasts_m4636_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_blocksRaycasts()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
extern "C" bool CanvasGroup_get_ignoreParentGroups_m3521 (CanvasGroup_t733 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_ignoreParentGroups_m3521_ftn) (CanvasGroup_t733 *);
	static CanvasGroup_get_ignoreParentGroups_m3521_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_ignoreParentGroups_m3521_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_ignoreParentGroups()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroupMethodDeclarations.h"
extern "C" bool CanvasGroup_IsRaycastLocationValid_m4637 (CanvasGroup_t733 * __this, Vector2_t2  ___sp, Camera_t6 * ___eventCamera, const MethodInfo* method)
{
	{
		bool L_0 = CanvasGroup_get_blocksRaycasts_m4636(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertexMethodDeclarations.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
// System.Void UnityEngine.UIVertex::.cctor()
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
extern TypeInfo* UIVertex_t605_il2cpp_TypeInfo_var;
extern "C" void UIVertex__cctor_m4638 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIVertex_t605_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(453);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t605  V_0 = {0};
	{
		Color32_t711  L_0 = {0};
		Color32__ctor_m3408(&L_0, ((int32_t)255), ((int32_t)255), ((int32_t)255), ((int32_t)255), /*hidden argument*/NULL);
		((UIVertex_t605_StaticFields*)UIVertex_t605_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6 = L_0;
		Vector4_t680  L_1 = {0};
		Vector4__ctor_m3517(&L_1, (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((UIVertex_t605_StaticFields*)UIVertex_t605_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7 = L_1;
		Initobj (UIVertex_t605_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t36  L_2 = Vector3_get_zero_m1578(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___position_0 = L_2;
		Vector3_t36  L_3 = Vector3_get_back_m3793(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___normal_1 = L_3;
		Vector4_t680  L_4 = ((UIVertex_t605_StaticFields*)UIVertex_t605_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7;
		(&V_0)->___tangent_5 = L_4;
		Color32_t711  L_5 = ((UIVertex_t605_StaticFields*)UIVertex_t605_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6;
		(&V_0)->___color_2 = L_5;
		Vector2_t2  L_6 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv0_3 = L_6;
		Vector2_t2  L_7 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv1_4 = L_7;
		UIVertex_t605  L_8 = V_0;
		((UIVertex_t605_StaticFields*)UIVertex_t605_il2cpp_TypeInfo_var->static_fields)->___simpleVert_8 = L_8;
		return;
	}
}
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRendererMethodDeclarations.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
// System.Void UnityEngine.CanvasRenderer::SetColor(UnityEngine.Color)
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRendererMethodDeclarations.h"
extern "C" void CanvasRenderer_SetColor_m3527 (CanvasRenderer_t573 * __this, Color_t9  ___color, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_SetColor_m4639(NULL /*static, unused*/, __this, (&___color), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
extern "C" void CanvasRenderer_INTERNAL_CALL_SetColor_m4639 (Object_t * __this /* static, unused */, CanvasRenderer_t573 * ___self, Color_t9 * ___color, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_SetColor_m4639_ftn) (CanvasRenderer_t573 *, Color_t9 *);
	static CanvasRenderer_INTERNAL_CALL_SetColor_m4639_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_SetColor_m4639_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self, ___color);
}
// UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
extern "C" Color_t9  CanvasRenderer_GetColor_m3525 (CanvasRenderer_t573 * __this, const MethodInfo* method)
{
	typedef Color_t9  (*CanvasRenderer_GetColor_m3525_ftn) (CanvasRenderer_t573 *);
	static CanvasRenderer_GetColor_m3525_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_GetColor_m3525_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::GetColor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::EnableRectClipping(UnityEngine.Rect)
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
extern "C" void CanvasRenderer_EnableRectClipping_m3660 (CanvasRenderer_t573 * __this, Rect_t267  ___rect, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m4640(NULL /*static, unused*/, __this, (&___rect), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
extern "C" void CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m4640 (Object_t * __this /* static, unused */, CanvasRenderer_t573 * ___self, Rect_t267 * ___rect, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m4640_ftn) (CanvasRenderer_t573 *, Rect_t267 *);
	static CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m4640_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m4640_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self, ___rect);
}
// System.Void UnityEngine.CanvasRenderer::DisableRectClipping()
extern "C" void CanvasRenderer_DisableRectClipping_m3661 (CanvasRenderer_t573 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_DisableRectClipping_m3661_ftn) (CanvasRenderer_t573 *);
	static CanvasRenderer_DisableRectClipping_m3661_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_DisableRectClipping_m3661_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::DisableRectClipping()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void CanvasRenderer_set_hasPopInstruction_m3651 (CanvasRenderer_t573 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_hasPopInstruction_m3651_ftn) (CanvasRenderer_t573 *, bool);
	static CanvasRenderer_set_hasPopInstruction_m3651_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_hasPopInstruction_m3651_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
extern "C" int32_t CanvasRenderer_get_materialCount_m4641 (CanvasRenderer_t573 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_materialCount_m4641_ftn) (CanvasRenderer_t573 *);
	static CanvasRenderer_get_materialCount_m4641_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_materialCount_m4641_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_materialCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void CanvasRenderer_set_materialCount_m3513 (CanvasRenderer_t573 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_materialCount_m3513_ftn) (CanvasRenderer_t573 *, int32_t);
	static CanvasRenderer_set_materialCount_m3513_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_materialCount_m3513_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_materialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
extern "C" void CanvasRenderer_SetMaterial_m3514 (CanvasRenderer_t573 * __this, Material_t82 * ___material, int32_t ___index, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMaterial_m3514_ftn) (CanvasRenderer_t573 *, Material_t82 *, int32_t);
	static CanvasRenderer_SetMaterial_m3514_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMaterial_m3514_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material, ___index);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
extern "C" void CanvasRenderer_SetMaterial_m3632 (CanvasRenderer_t573 * __this, Material_t82 * ___material, Texture_t103 * ___texture, const MethodInfo* method)
{
	{
		int32_t L_0 = CanvasRenderer_get_materialCount_m4641(__this, /*hidden argument*/NULL);
		int32_t L_1 = Math_Max_m5144(NULL /*static, unused*/, 1, L_0, /*hidden argument*/NULL);
		CanvasRenderer_set_materialCount_m3513(__this, L_1, /*hidden argument*/NULL);
		Material_t82 * L_2 = ___material;
		CanvasRenderer_SetMaterial_m3514(__this, L_2, 0, /*hidden argument*/NULL);
		Texture_t103 * L_3 = ___texture;
		CanvasRenderer_SetTexture_m3515(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)
extern "C" void CanvasRenderer_set_popMaterialCount_m3652 (CanvasRenderer_t573 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_popMaterialCount_m3652_ftn) (CanvasRenderer_t573 *, int32_t);
	static CanvasRenderer_set_popMaterialCount_m3652_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_popMaterialCount_m3652_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)
extern "C" void CanvasRenderer_SetPopMaterial_m3653 (CanvasRenderer_t573 * __this, Material_t82 * ___material, int32_t ___index, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetPopMaterial_m3653_ftn) (CanvasRenderer_t573 *, Material_t82 *, int32_t);
	static CanvasRenderer_SetPopMaterial_m3653_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetPopMaterial_m3653_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material, ___index);
}
// System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
extern "C" void CanvasRenderer_SetTexture_m3515 (CanvasRenderer_t573 * __this, Texture_t103 * ___texture, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetTexture_m3515_ftn) (CanvasRenderer_t573 *, Texture_t103 *);
	static CanvasRenderer_SetTexture_m3515_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetTexture_m3515_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___texture);
}
// System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
extern "C" void CanvasRenderer_SetMesh_m3516 (CanvasRenderer_t573 * __this, Mesh_t104 * ___mesh, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMesh_m3516_ftn) (CanvasRenderer_t573 *, Mesh_t104 *);
	static CanvasRenderer_SetMesh_m3516_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMesh_m3516_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___mesh);
}
// System.Void UnityEngine.CanvasRenderer::Clear()
extern "C" void CanvasRenderer_Clear_m3511 (CanvasRenderer_t573 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_Clear_m3511_ftn) (CanvasRenderer_t573 *);
	static CanvasRenderer_Clear_m3511_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_Clear_m3511_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::Clear()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasRenderer::get_cull()
extern "C" bool CanvasRenderer_get_cull_m3512 (CanvasRenderer_t573 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasRenderer_get_cull_m3512_ftn) (CanvasRenderer_t573 *);
	static CanvasRenderer_get_cull_m3512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_cull_m3512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_cull()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_cull(System.Boolean)
extern "C" void CanvasRenderer_set_cull_m3658 (CanvasRenderer_t573 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_cull_m3658_ftn) (CanvasRenderer_t573 *, bool);
	static CanvasRenderer_set_cull_m3658_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_cull_m3658_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_cull(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
extern "C" int32_t CanvasRenderer_get_absoluteDepth_m3504 (CanvasRenderer_t573 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_absoluteDepth_m3504_ftn) (CanvasRenderer_t573 *);
	static CanvasRenderer_get_absoluteDepth_m3504_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_absoluteDepth_m3504_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_absoluteDepth()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasRenderer::get_hasMoved()
extern "C" bool CanvasRenderer_get_hasMoved_m3656 (CanvasRenderer_t573 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasRenderer_get_hasMoved_m3656_ftn) (CanvasRenderer_t573 *);
	static CanvasRenderer_get_hasMoved_m3656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_hasMoved_m3656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_hasMoved()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtility.h"
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtilityMethodDeclarations.h"
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_Plane.h"
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern TypeInfo* Vector3U5BU5D_t254_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t737_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility__cctor_m4642 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(91);
		RectTransformUtility_t737_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(420);
		s_Il2CppMethodIntialized = true;
	}
	{
		((RectTransformUtility_t737_StaticFields*)RectTransformUtility_t737_il2cpp_TypeInfo_var->static_fields)->___s_Corners_0 = ((Vector3U5BU5D_t254*)SZArrayNew(Vector3U5BU5D_t254_il2cpp_TypeInfo_var, 4));
		return;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtilityMethodDeclarations.h"
extern TypeInfo* RectTransformUtility_t737_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_RectangleContainsScreenPoint_m3546 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, Vector2_t2  ___screenPoint, Camera_t6 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t737_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(420);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t556 * L_0 = ___rect;
		Camera_t6 * L_1 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t737_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4643(NULL /*static, unused*/, L_0, (&___screenPoint), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C" bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4643 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, Vector2_t2 * ___screenPoint, Camera_t6 * ___cam, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4643_ftn) (RectTransform_t556 *, Vector2_t2 *, Camera_t6 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4643_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m4643_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect, ___screenPoint, ___cam);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
extern TypeInfo* RectTransformUtility_t737_il2cpp_TypeInfo_var;
extern "C" Vector2_t2  RectTransformUtility_PixelAdjustPoint_m3523 (Object_t * __this /* static, unused */, Vector2_t2  ___point, Transform_t35 * ___elementTransform, Canvas_t574 * ___canvas, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t737_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(420);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2  V_0 = {0};
	{
		Vector2_t2  L_0 = ___point;
		Transform_t35 * L_1 = ___elementTransform;
		Canvas_t574 * L_2 = ___canvas;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t737_il2cpp_TypeInfo_var);
		RectTransformUtility_PixelAdjustPoint_m4644(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		Vector2_t2  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t737_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_PixelAdjustPoint_m4644 (Object_t * __this /* static, unused */, Vector2_t2  ___point, Transform_t35 * ___elementTransform, Canvas_t574 * ___canvas, Vector2_t2 * ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t737_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(420);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t35 * L_0 = ___elementTransform;
		Canvas_t574 * L_1 = ___canvas;
		Vector2_t2 * L_2 = ___output;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t737_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4645(NULL /*static, unused*/, (&___point), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C" void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4645 (Object_t * __this /* static, unused */, Vector2_t2 * ___point, Transform_t35 * ___elementTransform, Canvas_t574 * ___canvas, Vector2_t2 * ___output, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4645_ftn) (Vector2_t2 *, Transform_t35 *, Canvas_t574 *, Vector2_t2 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4645_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m4645_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point, ___elementTransform, ___canvas, ___output);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C" Rect_t267  RectTransformUtility_PixelAdjustRect_m3524 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rectTransform, Canvas_t574 * ___canvas, const MethodInfo* method)
{
	typedef Rect_t267  (*RectTransformUtility_PixelAdjustRect_m3524_ftn) (RectTransform_t556 *, Canvas_t574 *);
	static RectTransformUtility_PixelAdjustRect_m3524_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_PixelAdjustRect_m3524_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)");
	return _il2cpp_icall_func(___rectTransform, ___canvas);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
extern TypeInfo* RectTransformUtility_t737_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m4646 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, Vector2_t2  ___screenPoint, Camera_t6 * ___cam, Vector3_t36 * ___worldPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t737_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(420);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t382  V_0 = {0};
	Plane_t751  V_1 = {0};
	float V_2 = 0.0f;
	{
		Vector3_t36 * L_0 = ___worldPoint;
		Vector2_t2  L_1 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t36  L_2 = Vector2_op_Implicit_m3367(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		*L_0 = L_2;
		Camera_t6 * L_3 = ___cam;
		Vector2_t2  L_4 = ___screenPoint;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t737_il2cpp_TypeInfo_var);
		Ray_t382  L_5 = RectTransformUtility_ScreenPointToRay_m4647(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t556 * L_6 = ___rect;
		NullCheck(L_6);
		Quaternion_t41  L_7 = Transform_get_rotation_m1903(L_6, /*hidden argument*/NULL);
		Vector3_t36  L_8 = Vector3_get_back_m3793(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t36  L_9 = Quaternion_op_Multiply_m3541(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t556 * L_10 = ___rect;
		NullCheck(L_10);
		Vector3_t36  L_11 = Transform_get_position_m1388(L_10, /*hidden argument*/NULL);
		Plane__ctor_m3604((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t382  L_12 = V_0;
		bool L_13 = Plane_Raycast_m3605((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0046;
		}
	}
	{
		return 0;
	}

IL_0046:
	{
		Vector3_t36 * L_14 = ___worldPoint;
		float L_15 = V_2;
		Vector3_t36  L_16 = Ray_GetPoint_m3606((&V_0), L_15, /*hidden argument*/NULL);
		*L_14 = L_16;
		return 1;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t737_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m3574 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, Vector2_t2  ___screenPoint, Camera_t6 * ___cam, Vector2_t2 * ___localPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t737_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(420);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t36  V_0 = {0};
	{
		Vector2_t2 * L_0 = ___localPoint;
		Vector2_t2  L_1 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		*L_0 = L_1;
		RectTransform_t556 * L_2 = ___rect;
		Vector2_t2  L_3 = ___screenPoint;
		Camera_t6 * L_4 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t737_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m4646(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Vector2_t2 * L_6 = ___localPoint;
		RectTransform_t556 * L_7 = ___rect;
		Vector3_t36  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t36  L_9 = Transform_InverseTransformPoint_m3470(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t2  L_10 = Vector2_op_Implicit_m3325(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		*L_6 = L_10;
		return 1;
	}

IL_002e:
	{
		return 0;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
extern "C" Ray_t382  RectTransformUtility_ScreenPointToRay_m4647 (Object_t * __this /* static, unused */, Camera_t6 * ___cam, Vector2_t2  ___screenPos, const MethodInfo* method)
{
	Vector3_t36  V_0 = {0};
	{
		Camera_t6 * L_0 = ___cam;
		bool L_1 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Camera_t6 * L_2 = ___cam;
		Vector2_t2  L_3 = ___screenPos;
		Vector3_t36  L_4 = Vector2_op_Implicit_m3367(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t382  L_5 = Camera_ScreenPointToRay_m1420(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector2_t2  L_6 = ___screenPos;
		Vector3_t36  L_7 = Vector2_op_Implicit_m3367(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t36 * L_8 = (&V_0);
		float L_9 = (L_8->___z_3);
		L_8->___z_3 = ((float)((float)L_9-(float)(100.0f)));
		Vector3_t36  L_10 = V_0;
		Vector3_t36  L_11 = Vector3_get_forward_m3540(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t382  L_12 = {0};
		Ray__ctor_m4139(&L_12, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
extern TypeInfo* RectTransform_t556_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t737_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutOnAxis_m3472 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, int32_t ___axis, bool ___keepPositioning, bool ___recursive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		RectTransformUtility_t737_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(420);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t556 * V_1 = {0};
	Vector2_t2  V_2 = {0};
	Vector2_t2  V_3 = {0};
	Vector2_t2  V_4 = {0};
	Vector2_t2  V_5 = {0};
	float V_6 = 0.0f;
	{
		RectTransform_t556 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_001a:
	{
		RectTransform_t556 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t35 * L_5 = Transform_GetChild_m3415(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t556 *)IsInstSealed(L_5, RectTransform_t556_il2cpp_TypeInfo_var));
		RectTransform_t556 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_6, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RectTransform_t556 * L_8 = V_1;
		int32_t L_9 = ___axis;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t737_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m3472(NULL /*static, unused*/, L_8, L_9, 0, 1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		RectTransform_t556 * L_12 = ___rect;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m3416(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}

IL_004c:
	{
		RectTransform_t556 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t2  L_15 = RectTransform_get_pivot_m3476(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis;
		int32_t L_17 = ___axis;
		float L_18 = Vector2_get_Item_m3571((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m3577((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t556 * L_19 = ___rect;
		Vector2_t2  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m3434(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		RectTransform_t556 * L_22 = ___rect;
		NullCheck(L_22);
		Vector2_t2  L_23 = RectTransform_get_anchoredPosition_m3475(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis;
		int32_t L_25 = ___axis;
		float L_26 = Vector2_get_Item_m3571((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m3577((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t556 * L_27 = ___rect;
		Vector2_t2  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m3421(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t556 * L_29 = ___rect;
		NullCheck(L_29);
		Vector2_t2  L_30 = RectTransform_get_anchorMin_m3473(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t556 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t2  L_32 = RectTransform_get_anchorMax_m3474(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis;
		float L_34 = Vector2_get_Item_m3571((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis;
		int32_t L_36 = ___axis;
		float L_37 = Vector2_get_Item_m3571((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m3577((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis;
		float L_39 = V_6;
		Vector2_set_Item_m3577((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t556 * L_40 = ___rect;
		Vector2_t2  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m3418(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t556 * L_42 = ___rect;
		Vector2_t2  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m3420(L_42, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern TypeInfo* RectTransform_t556_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t737_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutAxes_m3677 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, bool ___keepPositioning, bool ___recursive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		RectTransformUtility_t737_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(420);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t556 * V_1 = {0};
	{
		RectTransform_t556 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_001a:
	{
		RectTransform_t556 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t35 * L_5 = Transform_GetChild_m3415(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t556 *)IsInstSealed(L_5, RectTransform_t556_il2cpp_TypeInfo_var));
		RectTransform_t556 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_6, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		RectTransform_t556 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t737_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m3677(NULL /*static, unused*/, L_8, 0, 1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		RectTransform_t556 * L_11 = ___rect;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m3416(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}

IL_004b:
	{
		RectTransform_t556 * L_13 = ___rect;
		RectTransform_t556 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t2  L_15 = RectTransform_get_pivot_m3476(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t737_il2cpp_TypeInfo_var);
		Vector2_t2  L_16 = RectTransformUtility_GetTransposed_m4648(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m3434(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t556 * L_17 = ___rect;
		RectTransform_t556 * L_18 = ___rect;
		NullCheck(L_18);
		Vector2_t2  L_19 = RectTransform_get_sizeDelta_m3435(L_18, /*hidden argument*/NULL);
		Vector2_t2  L_20 = RectTransformUtility_GetTransposed_m4648(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m3411(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		RectTransform_t556 * L_22 = ___rect;
		RectTransform_t556 * L_23 = ___rect;
		NullCheck(L_23);
		Vector2_t2  L_24 = RectTransform_get_anchoredPosition_m3475(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t737_il2cpp_TypeInfo_var);
		Vector2_t2  L_25 = RectTransformUtility_GetTransposed_m4648(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m3421(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t556 * L_26 = ___rect;
		RectTransform_t556 * L_27 = ___rect;
		NullCheck(L_27);
		Vector2_t2  L_28 = RectTransform_get_anchorMin_m3473(L_27, /*hidden argument*/NULL);
		Vector2_t2  L_29 = RectTransformUtility_GetTransposed_m4648(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m3418(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t556 * L_30 = ___rect;
		RectTransform_t556 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t2  L_32 = RectTransform_get_anchorMax_m3474(L_31, /*hidden argument*/NULL);
		Vector2_t2  L_33 = RectTransformUtility_GetTransposed_m4648(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m3420(L_30, L_33, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C" Vector2_t2  RectTransformUtility_GetTransposed_m4648 (Object_t * __this /* static, unused */, Vector2_t2  ___input, const MethodInfo* method)
{
	{
		float L_0 = ((&___input)->___y_2);
		float L_1 = ((&___input)->___x_1);
		Vector2_t2  L_2 = {0};
		Vector2__ctor_m1309(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Event
#include "UnityEngine_UnityEngine_Event.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1.h"
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
// System.Void UnityEngine.Event::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
extern "C" void Event__ctor_m3582 (Event_t381 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Event_Init_m4678(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::.ctor(UnityEngine.Event)
// UnityEngine.Event
#include "UnityEngine_UnityEngine_Event.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral569;
extern "C" void Event__ctor_m4649 (Event_t381 * __this, Event_t381 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral569 = il2cpp_codegen_string_literal_from_index(569);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Event_t381 * L_0 = ___other;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentException_t764 * L_1 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_1, _stringLiteral569, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Event_t381 * L_2 = ___other;
		Event_InitCopy_m4680(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::.ctor(System.IntPtr)
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Event__ctor_m4650 (Event_t381 * __this, IntPtr_t ___ptr, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___ptr;
		Event_InitPtr_m4681(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Finalize()
extern "C" void Event_Finalize_m4651 (Event_t381 * __this, const MethodInfo* method)
{
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Event_Cleanup_m4679(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m5102(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Event::get_mousePosition()
extern "C" Vector2_t2  Event_get_mousePosition_m1403 (Event_t381 * __this, const MethodInfo* method)
{
	Vector2_t2  V_0 = {0};
	{
		Event_Internal_GetMousePosition_m4686(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_mousePosition(UnityEngine.Vector2)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
extern "C" void Event_set_mousePosition_m4652 (Event_t381 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = ___value;
		Event_Internal_SetMousePosition_m4684(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Event::get_delta()
extern "C" Vector2_t2  Event_get_delta_m4653 (Event_t381 * __this, const MethodInfo* method)
{
	Vector2_t2  V_0 = {0};
	{
		Event_Internal_GetMouseDelta_m4689(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_delta(UnityEngine.Vector2)
extern "C" void Event_set_delta_m4654 (Event_t381 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = ___value;
		Event_Internal_SetMouseDelta_m4687(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Ray UnityEngine.Event::get_mouseRay()
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
extern "C" Ray_t382  Event_get_mouseRay_m4655 (Event_t381 * __this, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = Vector3_get_up_m1715(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t36  L_1 = Vector3_get_up_m1715(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t382  L_2 = {0};
		Ray__ctor_m4139(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Event::set_mouseRay(UnityEngine.Ray)
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
extern "C" void Event_set_mouseRay_m4656 (Event_t381 * __this, Ray_t382  ___value, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_shift()
extern "C" bool Event_get_shift_m4657 (Event_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_shift(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void Event_set_shift_m4658 (Event_t381 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-2))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_control()
extern "C" bool Event_get_control_m4659 (Event_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_control(System.Boolean)
extern "C" void Event_set_control_m4660 (Event_t381 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-3))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_2|(int32_t)2)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_alt()
extern "C" bool Event_get_alt_m4661 (Event_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_alt(System.Boolean)
extern "C" void Event_set_alt_m4662 (Event_t381 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-5))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_2|(int32_t)4)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_command()
extern "C" bool Event_get_command_m4663 (Event_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)8))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_command(System.Boolean)
extern "C" void Event_set_command_m4664 (Event_t381 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-9))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_2|(int32_t)8)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_capsLock()
extern "C" bool Event_get_capsLock_m4665 (Event_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)32)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_capsLock(System.Boolean)
extern "C" void Event_set_capsLock_m4666 (Event_t381 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-33))), /*hidden argument*/NULL);
		goto IL_0029;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_2|(int32_t)((int32_t)32))), /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_numeric()
extern "C" bool Event_get_numeric_m4667 (Event_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_numeric(System.Boolean)
extern "C" void Event_set_numeric_m4668 (Event_t381 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-2))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m4692(__this, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_functionKey()
extern "C" bool Event_get_functionKey_m4669 (Event_t381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)64)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Event UnityEngine.Event::get_current()
extern TypeInfo* Event_t381_il2cpp_TypeInfo_var;
extern "C" Event_t381 * Event_get_current_m1402 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(457);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t381 * L_0 = ((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___s_Current_1;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_current(UnityEngine.Event)
extern TypeInfo* Event_t381_il2cpp_TypeInfo_var;
extern "C" void Event_set_current_m4670 (Object_t * __this /* static, unused */, Event_t381 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(457);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t381 * L_0 = ___value;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Event_t381 * L_1 = ___value;
		((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_1;
		goto IL_001b;
	}

IL_0011:
	{
		Event_t381 * L_2 = ((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_2;
	}

IL_001b:
	{
		Event_t381 * L_3 = ((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___s_Current_1;
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		Event_Internal_SetNativeEvent_m4699(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Internal_MakeMasterEventCurrent()
extern TypeInfo* Event_t381_il2cpp_TypeInfo_var;
extern "C" void Event_Internal_MakeMasterEventCurrent_m4671 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(457);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t381 * L_0 = ((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Event_t381 * L_1 = (Event_t381 *)il2cpp_codegen_object_new (Event_t381_il2cpp_TypeInfo_var);
		Event__ctor_m3582(L_1, /*hidden argument*/NULL);
		((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2 = L_1;
	}

IL_0014:
	{
		Event_t381 * L_2 = ((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_2;
		Event_t381 * L_3 = ((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		Event_Internal_SetNativeEvent_m4699(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Event::get_isKey()
extern "C" bool Event_get_isKey_m4672 (Event_t381 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Event_get_type_m1405(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)5))? 1 : 0);
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
	}

IL_0015:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.Event::get_isMouse()
extern "C" bool Event_get_isMouse_m4673 (Event_t381 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = Event_get_type_m1405(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B5_0 = ((((int32_t)L_4) == ((int32_t)3))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B5_0 = 1;
	}

IL_0022:
	{
		return G_B5_0;
	}
}
// UnityEngine.Event UnityEngine.Event::KeyboardEvent(System.String)
// System.String
#include "mscorlib_System_String.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern const Il2CppType* KeyCode_t897_0_0_0_var;
extern TypeInfo* Event_t381_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t68_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t21_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1438_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral570;
extern Il2CppCodeGenString* _stringLiteral571;
extern Il2CppCodeGenString* _stringLiteral572;
extern Il2CppCodeGenString* _stringLiteral573;
extern Il2CppCodeGenString* _stringLiteral574;
extern Il2CppCodeGenString* _stringLiteral575;
extern Il2CppCodeGenString* _stringLiteral576;
extern Il2CppCodeGenString* _stringLiteral577;
extern Il2CppCodeGenString* _stringLiteral578;
extern Il2CppCodeGenString* _stringLiteral579;
extern Il2CppCodeGenString* _stringLiteral580;
extern Il2CppCodeGenString* _stringLiteral581;
extern Il2CppCodeGenString* _stringLiteral582;
extern Il2CppCodeGenString* _stringLiteral583;
extern Il2CppCodeGenString* _stringLiteral584;
extern Il2CppCodeGenString* _stringLiteral585;
extern Il2CppCodeGenString* _stringLiteral586;
extern Il2CppCodeGenString* _stringLiteral317;
extern Il2CppCodeGenString* _stringLiteral587;
extern Il2CppCodeGenString* _stringLiteral588;
extern Il2CppCodeGenString* _stringLiteral589;
extern Il2CppCodeGenString* _stringLiteral590;
extern Il2CppCodeGenString* _stringLiteral591;
extern Il2CppCodeGenString* _stringLiteral592;
extern Il2CppCodeGenString* _stringLiteral593;
extern Il2CppCodeGenString* _stringLiteral594;
extern Il2CppCodeGenString* _stringLiteral595;
extern Il2CppCodeGenString* _stringLiteral596;
extern Il2CppCodeGenString* _stringLiteral597;
extern Il2CppCodeGenString* _stringLiteral598;
extern Il2CppCodeGenString* _stringLiteral599;
extern Il2CppCodeGenString* _stringLiteral600;
extern Il2CppCodeGenString* _stringLiteral601;
extern Il2CppCodeGenString* _stringLiteral602;
extern Il2CppCodeGenString* _stringLiteral603;
extern Il2CppCodeGenString* _stringLiteral604;
extern Il2CppCodeGenString* _stringLiteral605;
extern Il2CppCodeGenString* _stringLiteral606;
extern Il2CppCodeGenString* _stringLiteral607;
extern Il2CppCodeGenString* _stringLiteral608;
extern Il2CppCodeGenString* _stringLiteral609;
extern Il2CppCodeGenString* _stringLiteral610;
extern Il2CppCodeGenString* _stringLiteral611;
extern Il2CppCodeGenString* _stringLiteral612;
extern Il2CppCodeGenString* _stringLiteral613;
extern Il2CppCodeGenString* _stringLiteral614;
extern Il2CppCodeGenString* _stringLiteral615;
extern Il2CppCodeGenString* _stringLiteral616;
extern Il2CppCodeGenString* _stringLiteral362;
extern Il2CppCodeGenString* _stringLiteral617;
extern "C" Event_t381 * Event_KeyboardEvent_m4674 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyCode_t897_0_0_0_var = il2cpp_codegen_type_from_index(595);
		Event_t381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(457);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Dictionary_2_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(64);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		Enum_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Dictionary_2__ctor_m1438_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483672);
		_stringLiteral570 = il2cpp_codegen_string_literal_from_index(570);
		_stringLiteral571 = il2cpp_codegen_string_literal_from_index(571);
		_stringLiteral572 = il2cpp_codegen_string_literal_from_index(572);
		_stringLiteral573 = il2cpp_codegen_string_literal_from_index(573);
		_stringLiteral574 = il2cpp_codegen_string_literal_from_index(574);
		_stringLiteral575 = il2cpp_codegen_string_literal_from_index(575);
		_stringLiteral576 = il2cpp_codegen_string_literal_from_index(576);
		_stringLiteral577 = il2cpp_codegen_string_literal_from_index(577);
		_stringLiteral578 = il2cpp_codegen_string_literal_from_index(578);
		_stringLiteral579 = il2cpp_codegen_string_literal_from_index(579);
		_stringLiteral580 = il2cpp_codegen_string_literal_from_index(580);
		_stringLiteral581 = il2cpp_codegen_string_literal_from_index(581);
		_stringLiteral582 = il2cpp_codegen_string_literal_from_index(582);
		_stringLiteral583 = il2cpp_codegen_string_literal_from_index(583);
		_stringLiteral584 = il2cpp_codegen_string_literal_from_index(584);
		_stringLiteral585 = il2cpp_codegen_string_literal_from_index(585);
		_stringLiteral586 = il2cpp_codegen_string_literal_from_index(586);
		_stringLiteral317 = il2cpp_codegen_string_literal_from_index(317);
		_stringLiteral587 = il2cpp_codegen_string_literal_from_index(587);
		_stringLiteral588 = il2cpp_codegen_string_literal_from_index(588);
		_stringLiteral589 = il2cpp_codegen_string_literal_from_index(589);
		_stringLiteral590 = il2cpp_codegen_string_literal_from_index(590);
		_stringLiteral591 = il2cpp_codegen_string_literal_from_index(591);
		_stringLiteral592 = il2cpp_codegen_string_literal_from_index(592);
		_stringLiteral593 = il2cpp_codegen_string_literal_from_index(593);
		_stringLiteral594 = il2cpp_codegen_string_literal_from_index(594);
		_stringLiteral595 = il2cpp_codegen_string_literal_from_index(595);
		_stringLiteral596 = il2cpp_codegen_string_literal_from_index(596);
		_stringLiteral597 = il2cpp_codegen_string_literal_from_index(597);
		_stringLiteral598 = il2cpp_codegen_string_literal_from_index(598);
		_stringLiteral599 = il2cpp_codegen_string_literal_from_index(599);
		_stringLiteral600 = il2cpp_codegen_string_literal_from_index(600);
		_stringLiteral601 = il2cpp_codegen_string_literal_from_index(601);
		_stringLiteral602 = il2cpp_codegen_string_literal_from_index(602);
		_stringLiteral603 = il2cpp_codegen_string_literal_from_index(603);
		_stringLiteral604 = il2cpp_codegen_string_literal_from_index(604);
		_stringLiteral605 = il2cpp_codegen_string_literal_from_index(605);
		_stringLiteral606 = il2cpp_codegen_string_literal_from_index(606);
		_stringLiteral607 = il2cpp_codegen_string_literal_from_index(607);
		_stringLiteral608 = il2cpp_codegen_string_literal_from_index(608);
		_stringLiteral609 = il2cpp_codegen_string_literal_from_index(609);
		_stringLiteral610 = il2cpp_codegen_string_literal_from_index(610);
		_stringLiteral611 = il2cpp_codegen_string_literal_from_index(611);
		_stringLiteral612 = il2cpp_codegen_string_literal_from_index(612);
		_stringLiteral613 = il2cpp_codegen_string_literal_from_index(613);
		_stringLiteral614 = il2cpp_codegen_string_literal_from_index(614);
		_stringLiteral615 = il2cpp_codegen_string_literal_from_index(615);
		_stringLiteral616 = il2cpp_codegen_string_literal_from_index(616);
		_stringLiteral362 = il2cpp_codegen_string_literal_from_index(362);
		_stringLiteral617 = il2cpp_codegen_string_literal_from_index(617);
		s_Il2CppMethodIntialized = true;
	}
	Event_t381 * V_0 = {0};
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = {0};
	uint16_t V_4 = 0x0;
	String_t* V_5 = {0};
	Dictionary_2_t68 * V_6 = {0};
	int32_t V_7 = 0;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Event_t381 * L_0 = (Event_t381 *)il2cpp_codegen_object_new (Event_t381_il2cpp_TypeInfo_var);
		Event__ctor_m3582(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t381 * L_1 = V_0;
		NullCheck(L_1);
		Event_set_type_m4682(L_1, 4, /*hidden argument*/NULL);
		String_t* L_2 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		Event_t381 * L_4 = V_0;
		return L_4;
	}

IL_001a:
	{
		V_1 = 0;
		V_2 = 0;
	}

IL_001e:
	{
		V_2 = 1;
		int32_t L_5 = V_1;
		String_t* L_6 = ___key;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1336(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0033;
		}
	}
	{
		V_2 = 0;
		goto IL_00cd;
	}

IL_0033:
	{
		String_t* L_8 = ___key;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		uint16_t L_10 = String_get_Chars_m1559(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		uint16_t L_11 = V_4;
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 0)
		{
			goto IL_00a9;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 1)
		{
			goto IL_0056;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 2)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 3)
		{
			goto IL_0064;
		}
	}

IL_0056:
	{
		uint16_t L_12 = V_4;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)94))))
		{
			goto IL_007b;
		}
	}
	{
		goto IL_00c0;
	}

IL_0064:
	{
		Event_t381 * L_13 = V_0;
		Event_t381 * L_14 = L_13;
		NullCheck(L_14);
		int32_t L_15 = Event_get_modifiers_m3612(L_14, /*hidden argument*/NULL);
		NullCheck(L_14);
		Event_set_modifiers_m4692(L_14, ((int32_t)((int32_t)L_15|(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
		goto IL_00c7;
	}

IL_007b:
	{
		Event_t381 * L_17 = V_0;
		Event_t381 * L_18 = L_17;
		NullCheck(L_18);
		int32_t L_19 = Event_get_modifiers_m3612(L_18, /*hidden argument*/NULL);
		NullCheck(L_18);
		Event_set_modifiers_m4692(L_18, ((int32_t)((int32_t)L_19|(int32_t)2)), /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		V_1 = ((int32_t)((int32_t)L_20+(int32_t)1));
		goto IL_00c7;
	}

IL_0092:
	{
		Event_t381 * L_21 = V_0;
		Event_t381 * L_22 = L_21;
		NullCheck(L_22);
		int32_t L_23 = Event_get_modifiers_m3612(L_22, /*hidden argument*/NULL);
		NullCheck(L_22);
		Event_set_modifiers_m4692(L_22, ((int32_t)((int32_t)L_23|(int32_t)8)), /*hidden argument*/NULL);
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
		goto IL_00c7;
	}

IL_00a9:
	{
		Event_t381 * L_25 = V_0;
		Event_t381 * L_26 = L_25;
		NullCheck(L_26);
		int32_t L_27 = Event_get_modifiers_m3612(L_26, /*hidden argument*/NULL);
		NullCheck(L_26);
		Event_set_modifiers_m4692(L_26, ((int32_t)((int32_t)L_27|(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28+(int32_t)1));
		goto IL_00c7;
	}

IL_00c0:
	{
		V_2 = 0;
		goto IL_00c7;
	}

IL_00c7:
	{
		bool L_29 = V_2;
		if (L_29)
		{
			goto IL_001e;
		}
	}

IL_00cd:
	{
		String_t* L_30 = ___key;
		int32_t L_31 = V_1;
		String_t* L_32 = ___key;
		NullCheck(L_32);
		int32_t L_33 = String_get_Length_m1336(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_1;
		NullCheck(L_30);
		String_t* L_35 = String_Substring_m1558(L_30, L_31, ((int32_t)((int32_t)L_33-(int32_t)L_34)), /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_36 = String_ToLower_m1695(L_35, /*hidden argument*/NULL);
		V_3 = L_36;
		String_t* L_37 = V_3;
		V_5 = L_37;
		String_t* L_38 = V_5;
		if (!L_38)
		{
			goto IL_09e5;
		}
	}
	{
		Dictionary_2_t68 * L_39 = ((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3;
		if (L_39)
		{
			goto IL_03ab;
		}
	}
	{
		Dictionary_2_t68 * L_40 = (Dictionary_2_t68 *)il2cpp_codegen_object_new (Dictionary_2_t68_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1438(L_40, ((int32_t)49), /*hidden argument*/Dictionary_2__ctor_m1438_MethodInfo_var);
		V_6 = L_40;
		Dictionary_2_t68 * L_41 = V_6;
		NullCheck(L_41);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_41, _stringLiteral570, 0);
		Dictionary_2_t68 * L_42 = V_6;
		NullCheck(L_42);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_42, _stringLiteral571, 1);
		Dictionary_2_t68 * L_43 = V_6;
		NullCheck(L_43);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_43, _stringLiteral572, 2);
		Dictionary_2_t68 * L_44 = V_6;
		NullCheck(L_44);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_44, _stringLiteral573, 3);
		Dictionary_2_t68 * L_45 = V_6;
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_45, _stringLiteral574, 4);
		Dictionary_2_t68 * L_46 = V_6;
		NullCheck(L_46);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_46, _stringLiteral575, 5);
		Dictionary_2_t68 * L_47 = V_6;
		NullCheck(L_47);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_47, _stringLiteral576, 6);
		Dictionary_2_t68 * L_48 = V_6;
		NullCheck(L_48);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_48, _stringLiteral577, 7);
		Dictionary_2_t68 * L_49 = V_6;
		NullCheck(L_49);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_49, _stringLiteral578, 8);
		Dictionary_2_t68 * L_50 = V_6;
		NullCheck(L_50);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_50, _stringLiteral579, ((int32_t)9));
		Dictionary_2_t68 * L_51 = V_6;
		NullCheck(L_51);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_51, _stringLiteral580, ((int32_t)10));
		Dictionary_2_t68 * L_52 = V_6;
		NullCheck(L_52);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_52, _stringLiteral581, ((int32_t)11));
		Dictionary_2_t68 * L_53 = V_6;
		NullCheck(L_53);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_53, _stringLiteral582, ((int32_t)12));
		Dictionary_2_t68 * L_54 = V_6;
		NullCheck(L_54);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_54, _stringLiteral583, ((int32_t)13));
		Dictionary_2_t68 * L_55 = V_6;
		NullCheck(L_55);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_55, _stringLiteral584, ((int32_t)14));
		Dictionary_2_t68 * L_56 = V_6;
		NullCheck(L_56);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_56, _stringLiteral585, ((int32_t)15));
		Dictionary_2_t68 * L_57 = V_6;
		NullCheck(L_57);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_57, _stringLiteral586, ((int32_t)16));
		Dictionary_2_t68 * L_58 = V_6;
		NullCheck(L_58);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_58, _stringLiteral317, ((int32_t)17));
		Dictionary_2_t68 * L_59 = V_6;
		NullCheck(L_59);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_59, _stringLiteral587, ((int32_t)18));
		Dictionary_2_t68 * L_60 = V_6;
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_60, _stringLiteral588, ((int32_t)19));
		Dictionary_2_t68 * L_61 = V_6;
		NullCheck(L_61);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_61, _stringLiteral589, ((int32_t)20));
		Dictionary_2_t68 * L_62 = V_6;
		NullCheck(L_62);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_62, _stringLiteral590, ((int32_t)21));
		Dictionary_2_t68 * L_63 = V_6;
		NullCheck(L_63);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_63, _stringLiteral591, ((int32_t)22));
		Dictionary_2_t68 * L_64 = V_6;
		NullCheck(L_64);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_64, _stringLiteral592, ((int32_t)23));
		Dictionary_2_t68 * L_65 = V_6;
		NullCheck(L_65);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_65, _stringLiteral593, ((int32_t)24));
		Dictionary_2_t68 * L_66 = V_6;
		NullCheck(L_66);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_66, _stringLiteral594, ((int32_t)25));
		Dictionary_2_t68 * L_67 = V_6;
		NullCheck(L_67);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_67, _stringLiteral595, ((int32_t)26));
		Dictionary_2_t68 * L_68 = V_6;
		NullCheck(L_68);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_68, _stringLiteral596, ((int32_t)27));
		Dictionary_2_t68 * L_69 = V_6;
		NullCheck(L_69);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_69, _stringLiteral597, ((int32_t)28));
		Dictionary_2_t68 * L_70 = V_6;
		NullCheck(L_70);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_70, _stringLiteral598, ((int32_t)29));
		Dictionary_2_t68 * L_71 = V_6;
		NullCheck(L_71);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_71, _stringLiteral599, ((int32_t)30));
		Dictionary_2_t68 * L_72 = V_6;
		NullCheck(L_72);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_72, _stringLiteral600, ((int32_t)31));
		Dictionary_2_t68 * L_73 = V_6;
		NullCheck(L_73);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_73, _stringLiteral601, ((int32_t)32));
		Dictionary_2_t68 * L_74 = V_6;
		NullCheck(L_74);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_74, _stringLiteral602, ((int32_t)33));
		Dictionary_2_t68 * L_75 = V_6;
		NullCheck(L_75);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_75, _stringLiteral603, ((int32_t)34));
		Dictionary_2_t68 * L_76 = V_6;
		NullCheck(L_76);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_76, _stringLiteral604, ((int32_t)35));
		Dictionary_2_t68 * L_77 = V_6;
		NullCheck(L_77);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_77, _stringLiteral605, ((int32_t)36));
		Dictionary_2_t68 * L_78 = V_6;
		NullCheck(L_78);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_78, _stringLiteral606, ((int32_t)37));
		Dictionary_2_t68 * L_79 = V_6;
		NullCheck(L_79);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_79, _stringLiteral607, ((int32_t)38));
		Dictionary_2_t68 * L_80 = V_6;
		NullCheck(L_80);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_80, _stringLiteral608, ((int32_t)39));
		Dictionary_2_t68 * L_81 = V_6;
		NullCheck(L_81);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_81, _stringLiteral609, ((int32_t)40));
		Dictionary_2_t68 * L_82 = V_6;
		NullCheck(L_82);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_82, _stringLiteral610, ((int32_t)41));
		Dictionary_2_t68 * L_83 = V_6;
		NullCheck(L_83);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_83, _stringLiteral611, ((int32_t)42));
		Dictionary_2_t68 * L_84 = V_6;
		NullCheck(L_84);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_84, _stringLiteral612, ((int32_t)43));
		Dictionary_2_t68 * L_85 = V_6;
		NullCheck(L_85);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_85, _stringLiteral613, ((int32_t)44));
		Dictionary_2_t68 * L_86 = V_6;
		NullCheck(L_86);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_86, _stringLiteral614, ((int32_t)45));
		Dictionary_2_t68 * L_87 = V_6;
		NullCheck(L_87);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_87, _stringLiteral615, ((int32_t)46));
		Dictionary_2_t68 * L_88 = V_6;
		NullCheck(L_88);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_88, _stringLiteral616, ((int32_t)47));
		Dictionary_2_t68 * L_89 = V_6;
		NullCheck(L_89);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_89, _stringLiteral362, ((int32_t)48));
		Dictionary_2_t68 * L_90 = V_6;
		((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3 = L_90;
	}

IL_03ab:
	{
		Dictionary_2_t68 * L_91 = ((Event_t381_StaticFields*)Event_t381_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3;
		String_t* L_92 = V_5;
		NullCheck(L_91);
		bool L_93 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(33 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_91, L_92, (&V_7));
		if (!L_93)
		{
			goto IL_09e5;
		}
	}
	{
		int32_t L_94 = V_7;
		if (L_94 == 0)
		{
			goto IL_048e;
		}
		if (L_94 == 1)
		{
			goto IL_04a6;
		}
		if (L_94 == 2)
		{
			goto IL_04be;
		}
		if (L_94 == 3)
		{
			goto IL_04d6;
		}
		if (L_94 == 4)
		{
			goto IL_04ee;
		}
		if (L_94 == 5)
		{
			goto IL_0506;
		}
		if (L_94 == 6)
		{
			goto IL_051e;
		}
		if (L_94 == 7)
		{
			goto IL_0536;
		}
		if (L_94 == 8)
		{
			goto IL_054e;
		}
		if (L_94 == 9)
		{
			goto IL_0566;
		}
		if (L_94 == 10)
		{
			goto IL_057e;
		}
		if (L_94 == 11)
		{
			goto IL_0596;
		}
		if (L_94 == 12)
		{
			goto IL_05ae;
		}
		if (L_94 == 13)
		{
			goto IL_05c6;
		}
		if (L_94 == 14)
		{
			goto IL_05de;
		}
		if (L_94 == 15)
		{
			goto IL_05f6;
		}
		if (L_94 == 16)
		{
			goto IL_060e;
		}
		if (L_94 == 17)
		{
			goto IL_0626;
		}
		if (L_94 == 18)
		{
			goto IL_0645;
		}
		if (L_94 == 19)
		{
			goto IL_0664;
		}
		if (L_94 == 20)
		{
			goto IL_0683;
		}
		if (L_94 == 21)
		{
			goto IL_06a2;
		}
		if (L_94 == 22)
		{
			goto IL_06c1;
		}
		if (L_94 == 23)
		{
			goto IL_06e0;
		}
		if (L_94 == 24)
		{
			goto IL_06ff;
		}
		if (L_94 == 25)
		{
			goto IL_071e;
		}
		if (L_94 == 26)
		{
			goto IL_073d;
		}
		if (L_94 == 27)
		{
			goto IL_075c;
		}
		if (L_94 == 28)
		{
			goto IL_077b;
		}
		if (L_94 == 29)
		{
			goto IL_0796;
		}
		if (L_94 == 30)
		{
			goto IL_07b2;
		}
		if (L_94 == 31)
		{
			goto IL_07bf;
		}
		if (L_94 == 32)
		{
			goto IL_07de;
		}
		if (L_94 == 33)
		{
			goto IL_07fd;
		}
		if (L_94 == 34)
		{
			goto IL_081c;
		}
		if (L_94 == 35)
		{
			goto IL_083b;
		}
		if (L_94 == 36)
		{
			goto IL_085a;
		}
		if (L_94 == 37)
		{
			goto IL_0879;
		}
		if (L_94 == 38)
		{
			goto IL_0898;
		}
		if (L_94 == 39)
		{
			goto IL_08b7;
		}
		if (L_94 == 40)
		{
			goto IL_08d6;
		}
		if (L_94 == 41)
		{
			goto IL_08f5;
		}
		if (L_94 == 42)
		{
			goto IL_0914;
		}
		if (L_94 == 43)
		{
			goto IL_0933;
		}
		if (L_94 == 44)
		{
			goto IL_0952;
		}
		if (L_94 == 45)
		{
			goto IL_0971;
		}
		if (L_94 == 46)
		{
			goto IL_0990;
		}
		if (L_94 == 47)
		{
			goto IL_099d;
		}
		if (L_94 == 48)
		{
			goto IL_09c1;
		}
	}
	{
		goto IL_09e5;
	}

IL_048e:
	{
		Event_t381 * L_95 = V_0;
		NullCheck(L_95);
		Event_set_character_m4696(L_95, ((int32_t)48), /*hidden argument*/NULL);
		Event_t381 * L_96 = V_0;
		NullCheck(L_96);
		Event_set_keyCode_m4698(L_96, ((int32_t)256), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04a6:
	{
		Event_t381 * L_97 = V_0;
		NullCheck(L_97);
		Event_set_character_m4696(L_97, ((int32_t)49), /*hidden argument*/NULL);
		Event_t381 * L_98 = V_0;
		NullCheck(L_98);
		Event_set_keyCode_m4698(L_98, ((int32_t)257), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04be:
	{
		Event_t381 * L_99 = V_0;
		NullCheck(L_99);
		Event_set_character_m4696(L_99, ((int32_t)50), /*hidden argument*/NULL);
		Event_t381 * L_100 = V_0;
		NullCheck(L_100);
		Event_set_keyCode_m4698(L_100, ((int32_t)258), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04d6:
	{
		Event_t381 * L_101 = V_0;
		NullCheck(L_101);
		Event_set_character_m4696(L_101, ((int32_t)51), /*hidden argument*/NULL);
		Event_t381 * L_102 = V_0;
		NullCheck(L_102);
		Event_set_keyCode_m4698(L_102, ((int32_t)259), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04ee:
	{
		Event_t381 * L_103 = V_0;
		NullCheck(L_103);
		Event_set_character_m4696(L_103, ((int32_t)52), /*hidden argument*/NULL);
		Event_t381 * L_104 = V_0;
		NullCheck(L_104);
		Event_set_keyCode_m4698(L_104, ((int32_t)260), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0506:
	{
		Event_t381 * L_105 = V_0;
		NullCheck(L_105);
		Event_set_character_m4696(L_105, ((int32_t)53), /*hidden argument*/NULL);
		Event_t381 * L_106 = V_0;
		NullCheck(L_106);
		Event_set_keyCode_m4698(L_106, ((int32_t)261), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_051e:
	{
		Event_t381 * L_107 = V_0;
		NullCheck(L_107);
		Event_set_character_m4696(L_107, ((int32_t)54), /*hidden argument*/NULL);
		Event_t381 * L_108 = V_0;
		NullCheck(L_108);
		Event_set_keyCode_m4698(L_108, ((int32_t)262), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0536:
	{
		Event_t381 * L_109 = V_0;
		NullCheck(L_109);
		Event_set_character_m4696(L_109, ((int32_t)55), /*hidden argument*/NULL);
		Event_t381 * L_110 = V_0;
		NullCheck(L_110);
		Event_set_keyCode_m4698(L_110, ((int32_t)263), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_054e:
	{
		Event_t381 * L_111 = V_0;
		NullCheck(L_111);
		Event_set_character_m4696(L_111, ((int32_t)56), /*hidden argument*/NULL);
		Event_t381 * L_112 = V_0;
		NullCheck(L_112);
		Event_set_keyCode_m4698(L_112, ((int32_t)264), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0566:
	{
		Event_t381 * L_113 = V_0;
		NullCheck(L_113);
		Event_set_character_m4696(L_113, ((int32_t)57), /*hidden argument*/NULL);
		Event_t381 * L_114 = V_0;
		NullCheck(L_114);
		Event_set_keyCode_m4698(L_114, ((int32_t)265), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_057e:
	{
		Event_t381 * L_115 = V_0;
		NullCheck(L_115);
		Event_set_character_m4696(L_115, ((int32_t)46), /*hidden argument*/NULL);
		Event_t381 * L_116 = V_0;
		NullCheck(L_116);
		Event_set_keyCode_m4698(L_116, ((int32_t)266), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0596:
	{
		Event_t381 * L_117 = V_0;
		NullCheck(L_117);
		Event_set_character_m4696(L_117, ((int32_t)47), /*hidden argument*/NULL);
		Event_t381 * L_118 = V_0;
		NullCheck(L_118);
		Event_set_keyCode_m4698(L_118, ((int32_t)267), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05ae:
	{
		Event_t381 * L_119 = V_0;
		NullCheck(L_119);
		Event_set_character_m4696(L_119, ((int32_t)45), /*hidden argument*/NULL);
		Event_t381 * L_120 = V_0;
		NullCheck(L_120);
		Event_set_keyCode_m4698(L_120, ((int32_t)269), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05c6:
	{
		Event_t381 * L_121 = V_0;
		NullCheck(L_121);
		Event_set_character_m4696(L_121, ((int32_t)43), /*hidden argument*/NULL);
		Event_t381 * L_122 = V_0;
		NullCheck(L_122);
		Event_set_keyCode_m4698(L_122, ((int32_t)270), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05de:
	{
		Event_t381 * L_123 = V_0;
		NullCheck(L_123);
		Event_set_character_m4696(L_123, ((int32_t)61), /*hidden argument*/NULL);
		Event_t381 * L_124 = V_0;
		NullCheck(L_124);
		Event_set_keyCode_m4698(L_124, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05f6:
	{
		Event_t381 * L_125 = V_0;
		NullCheck(L_125);
		Event_set_character_m4696(L_125, ((int32_t)61), /*hidden argument*/NULL);
		Event_t381 * L_126 = V_0;
		NullCheck(L_126);
		Event_set_keyCode_m4698(L_126, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_060e:
	{
		Event_t381 * L_127 = V_0;
		NullCheck(L_127);
		Event_set_character_m4696(L_127, ((int32_t)10), /*hidden argument*/NULL);
		Event_t381 * L_128 = V_0;
		NullCheck(L_128);
		Event_set_keyCode_m4698(L_128, ((int32_t)271), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0626:
	{
		Event_t381 * L_129 = V_0;
		NullCheck(L_129);
		Event_set_keyCode_m4698(L_129, ((int32_t)273), /*hidden argument*/NULL);
		Event_t381 * L_130 = V_0;
		Event_t381 * L_131 = L_130;
		NullCheck(L_131);
		int32_t L_132 = Event_get_modifiers_m3612(L_131, /*hidden argument*/NULL);
		NullCheck(L_131);
		Event_set_modifiers_m4692(L_131, ((int32_t)((int32_t)L_132|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0645:
	{
		Event_t381 * L_133 = V_0;
		NullCheck(L_133);
		Event_set_keyCode_m4698(L_133, ((int32_t)274), /*hidden argument*/NULL);
		Event_t381 * L_134 = V_0;
		Event_t381 * L_135 = L_134;
		NullCheck(L_135);
		int32_t L_136 = Event_get_modifiers_m3612(L_135, /*hidden argument*/NULL);
		NullCheck(L_135);
		Event_set_modifiers_m4692(L_135, ((int32_t)((int32_t)L_136|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0664:
	{
		Event_t381 * L_137 = V_0;
		NullCheck(L_137);
		Event_set_keyCode_m4698(L_137, ((int32_t)276), /*hidden argument*/NULL);
		Event_t381 * L_138 = V_0;
		Event_t381 * L_139 = L_138;
		NullCheck(L_139);
		int32_t L_140 = Event_get_modifiers_m3612(L_139, /*hidden argument*/NULL);
		NullCheck(L_139);
		Event_set_modifiers_m4692(L_139, ((int32_t)((int32_t)L_140|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0683:
	{
		Event_t381 * L_141 = V_0;
		NullCheck(L_141);
		Event_set_keyCode_m4698(L_141, ((int32_t)275), /*hidden argument*/NULL);
		Event_t381 * L_142 = V_0;
		Event_t381 * L_143 = L_142;
		NullCheck(L_143);
		int32_t L_144 = Event_get_modifiers_m3612(L_143, /*hidden argument*/NULL);
		NullCheck(L_143);
		Event_set_modifiers_m4692(L_143, ((int32_t)((int32_t)L_144|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06a2:
	{
		Event_t381 * L_145 = V_0;
		NullCheck(L_145);
		Event_set_keyCode_m4698(L_145, ((int32_t)277), /*hidden argument*/NULL);
		Event_t381 * L_146 = V_0;
		Event_t381 * L_147 = L_146;
		NullCheck(L_147);
		int32_t L_148 = Event_get_modifiers_m3612(L_147, /*hidden argument*/NULL);
		NullCheck(L_147);
		Event_set_modifiers_m4692(L_147, ((int32_t)((int32_t)L_148|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06c1:
	{
		Event_t381 * L_149 = V_0;
		NullCheck(L_149);
		Event_set_keyCode_m4698(L_149, ((int32_t)278), /*hidden argument*/NULL);
		Event_t381 * L_150 = V_0;
		Event_t381 * L_151 = L_150;
		NullCheck(L_151);
		int32_t L_152 = Event_get_modifiers_m3612(L_151, /*hidden argument*/NULL);
		NullCheck(L_151);
		Event_set_modifiers_m4692(L_151, ((int32_t)((int32_t)L_152|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06e0:
	{
		Event_t381 * L_153 = V_0;
		NullCheck(L_153);
		Event_set_keyCode_m4698(L_153, ((int32_t)279), /*hidden argument*/NULL);
		Event_t381 * L_154 = V_0;
		Event_t381 * L_155 = L_154;
		NullCheck(L_155);
		int32_t L_156 = Event_get_modifiers_m3612(L_155, /*hidden argument*/NULL);
		NullCheck(L_155);
		Event_set_modifiers_m4692(L_155, ((int32_t)((int32_t)L_156|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06ff:
	{
		Event_t381 * L_157 = V_0;
		NullCheck(L_157);
		Event_set_keyCode_m4698(L_157, ((int32_t)281), /*hidden argument*/NULL);
		Event_t381 * L_158 = V_0;
		Event_t381 * L_159 = L_158;
		NullCheck(L_159);
		int32_t L_160 = Event_get_modifiers_m3612(L_159, /*hidden argument*/NULL);
		NullCheck(L_159);
		Event_set_modifiers_m4692(L_159, ((int32_t)((int32_t)L_160|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_071e:
	{
		Event_t381 * L_161 = V_0;
		NullCheck(L_161);
		Event_set_keyCode_m4698(L_161, ((int32_t)280), /*hidden argument*/NULL);
		Event_t381 * L_162 = V_0;
		Event_t381 * L_163 = L_162;
		NullCheck(L_163);
		int32_t L_164 = Event_get_modifiers_m3612(L_163, /*hidden argument*/NULL);
		NullCheck(L_163);
		Event_set_modifiers_m4692(L_163, ((int32_t)((int32_t)L_164|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_073d:
	{
		Event_t381 * L_165 = V_0;
		NullCheck(L_165);
		Event_set_keyCode_m4698(L_165, ((int32_t)280), /*hidden argument*/NULL);
		Event_t381 * L_166 = V_0;
		Event_t381 * L_167 = L_166;
		NullCheck(L_167);
		int32_t L_168 = Event_get_modifiers_m3612(L_167, /*hidden argument*/NULL);
		NullCheck(L_167);
		Event_set_modifiers_m4692(L_167, ((int32_t)((int32_t)L_168|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_075c:
	{
		Event_t381 * L_169 = V_0;
		NullCheck(L_169);
		Event_set_keyCode_m4698(L_169, ((int32_t)281), /*hidden argument*/NULL);
		Event_t381 * L_170 = V_0;
		Event_t381 * L_171 = L_170;
		NullCheck(L_171);
		int32_t L_172 = Event_get_modifiers_m3612(L_171, /*hidden argument*/NULL);
		NullCheck(L_171);
		Event_set_modifiers_m4692(L_171, ((int32_t)((int32_t)L_172|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_077b:
	{
		Event_t381 * L_173 = V_0;
		NullCheck(L_173);
		Event_set_keyCode_m4698(L_173, 8, /*hidden argument*/NULL);
		Event_t381 * L_174 = V_0;
		Event_t381 * L_175 = L_174;
		NullCheck(L_175);
		int32_t L_176 = Event_get_modifiers_m3612(L_175, /*hidden argument*/NULL);
		NullCheck(L_175);
		Event_set_modifiers_m4692(L_175, ((int32_t)((int32_t)L_176|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0796:
	{
		Event_t381 * L_177 = V_0;
		NullCheck(L_177);
		Event_set_keyCode_m4698(L_177, ((int32_t)127), /*hidden argument*/NULL);
		Event_t381 * L_178 = V_0;
		Event_t381 * L_179 = L_178;
		NullCheck(L_179);
		int32_t L_180 = Event_get_modifiers_m3612(L_179, /*hidden argument*/NULL);
		NullCheck(L_179);
		Event_set_modifiers_m4692(L_179, ((int32_t)((int32_t)L_180|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07b2:
	{
		Event_t381 * L_181 = V_0;
		NullCheck(L_181);
		Event_set_keyCode_m4698(L_181, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07bf:
	{
		Event_t381 * L_182 = V_0;
		NullCheck(L_182);
		Event_set_keyCode_m4698(L_182, ((int32_t)282), /*hidden argument*/NULL);
		Event_t381 * L_183 = V_0;
		Event_t381 * L_184 = L_183;
		NullCheck(L_184);
		int32_t L_185 = Event_get_modifiers_m3612(L_184, /*hidden argument*/NULL);
		NullCheck(L_184);
		Event_set_modifiers_m4692(L_184, ((int32_t)((int32_t)L_185|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07de:
	{
		Event_t381 * L_186 = V_0;
		NullCheck(L_186);
		Event_set_keyCode_m4698(L_186, ((int32_t)283), /*hidden argument*/NULL);
		Event_t381 * L_187 = V_0;
		Event_t381 * L_188 = L_187;
		NullCheck(L_188);
		int32_t L_189 = Event_get_modifiers_m3612(L_188, /*hidden argument*/NULL);
		NullCheck(L_188);
		Event_set_modifiers_m4692(L_188, ((int32_t)((int32_t)L_189|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07fd:
	{
		Event_t381 * L_190 = V_0;
		NullCheck(L_190);
		Event_set_keyCode_m4698(L_190, ((int32_t)284), /*hidden argument*/NULL);
		Event_t381 * L_191 = V_0;
		Event_t381 * L_192 = L_191;
		NullCheck(L_192);
		int32_t L_193 = Event_get_modifiers_m3612(L_192, /*hidden argument*/NULL);
		NullCheck(L_192);
		Event_set_modifiers_m4692(L_192, ((int32_t)((int32_t)L_193|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_081c:
	{
		Event_t381 * L_194 = V_0;
		NullCheck(L_194);
		Event_set_keyCode_m4698(L_194, ((int32_t)285), /*hidden argument*/NULL);
		Event_t381 * L_195 = V_0;
		Event_t381 * L_196 = L_195;
		NullCheck(L_196);
		int32_t L_197 = Event_get_modifiers_m3612(L_196, /*hidden argument*/NULL);
		NullCheck(L_196);
		Event_set_modifiers_m4692(L_196, ((int32_t)((int32_t)L_197|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_083b:
	{
		Event_t381 * L_198 = V_0;
		NullCheck(L_198);
		Event_set_keyCode_m4698(L_198, ((int32_t)286), /*hidden argument*/NULL);
		Event_t381 * L_199 = V_0;
		Event_t381 * L_200 = L_199;
		NullCheck(L_200);
		int32_t L_201 = Event_get_modifiers_m3612(L_200, /*hidden argument*/NULL);
		NullCheck(L_200);
		Event_set_modifiers_m4692(L_200, ((int32_t)((int32_t)L_201|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_085a:
	{
		Event_t381 * L_202 = V_0;
		NullCheck(L_202);
		Event_set_keyCode_m4698(L_202, ((int32_t)287), /*hidden argument*/NULL);
		Event_t381 * L_203 = V_0;
		Event_t381 * L_204 = L_203;
		NullCheck(L_204);
		int32_t L_205 = Event_get_modifiers_m3612(L_204, /*hidden argument*/NULL);
		NullCheck(L_204);
		Event_set_modifiers_m4692(L_204, ((int32_t)((int32_t)L_205|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0879:
	{
		Event_t381 * L_206 = V_0;
		NullCheck(L_206);
		Event_set_keyCode_m4698(L_206, ((int32_t)288), /*hidden argument*/NULL);
		Event_t381 * L_207 = V_0;
		Event_t381 * L_208 = L_207;
		NullCheck(L_208);
		int32_t L_209 = Event_get_modifiers_m3612(L_208, /*hidden argument*/NULL);
		NullCheck(L_208);
		Event_set_modifiers_m4692(L_208, ((int32_t)((int32_t)L_209|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0898:
	{
		Event_t381 * L_210 = V_0;
		NullCheck(L_210);
		Event_set_keyCode_m4698(L_210, ((int32_t)289), /*hidden argument*/NULL);
		Event_t381 * L_211 = V_0;
		Event_t381 * L_212 = L_211;
		NullCheck(L_212);
		int32_t L_213 = Event_get_modifiers_m3612(L_212, /*hidden argument*/NULL);
		NullCheck(L_212);
		Event_set_modifiers_m4692(L_212, ((int32_t)((int32_t)L_213|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_08b7:
	{
		Event_t381 * L_214 = V_0;
		NullCheck(L_214);
		Event_set_keyCode_m4698(L_214, ((int32_t)290), /*hidden argument*/NULL);
		Event_t381 * L_215 = V_0;
		Event_t381 * L_216 = L_215;
		NullCheck(L_216);
		int32_t L_217 = Event_get_modifiers_m3612(L_216, /*hidden argument*/NULL);
		NullCheck(L_216);
		Event_set_modifiers_m4692(L_216, ((int32_t)((int32_t)L_217|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_08d6:
	{
		Event_t381 * L_218 = V_0;
		NullCheck(L_218);
		Event_set_keyCode_m4698(L_218, ((int32_t)291), /*hidden argument*/NULL);
		Event_t381 * L_219 = V_0;
		Event_t381 * L_220 = L_219;
		NullCheck(L_220);
		int32_t L_221 = Event_get_modifiers_m3612(L_220, /*hidden argument*/NULL);
		NullCheck(L_220);
		Event_set_modifiers_m4692(L_220, ((int32_t)((int32_t)L_221|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_08f5:
	{
		Event_t381 * L_222 = V_0;
		NullCheck(L_222);
		Event_set_keyCode_m4698(L_222, ((int32_t)292), /*hidden argument*/NULL);
		Event_t381 * L_223 = V_0;
		Event_t381 * L_224 = L_223;
		NullCheck(L_224);
		int32_t L_225 = Event_get_modifiers_m3612(L_224, /*hidden argument*/NULL);
		NullCheck(L_224);
		Event_set_modifiers_m4692(L_224, ((int32_t)((int32_t)L_225|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0914:
	{
		Event_t381 * L_226 = V_0;
		NullCheck(L_226);
		Event_set_keyCode_m4698(L_226, ((int32_t)293), /*hidden argument*/NULL);
		Event_t381 * L_227 = V_0;
		Event_t381 * L_228 = L_227;
		NullCheck(L_228);
		int32_t L_229 = Event_get_modifiers_m3612(L_228, /*hidden argument*/NULL);
		NullCheck(L_228);
		Event_set_modifiers_m4692(L_228, ((int32_t)((int32_t)L_229|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0933:
	{
		Event_t381 * L_230 = V_0;
		NullCheck(L_230);
		Event_set_keyCode_m4698(L_230, ((int32_t)294), /*hidden argument*/NULL);
		Event_t381 * L_231 = V_0;
		Event_t381 * L_232 = L_231;
		NullCheck(L_232);
		int32_t L_233 = Event_get_modifiers_m3612(L_232, /*hidden argument*/NULL);
		NullCheck(L_232);
		Event_set_modifiers_m4692(L_232, ((int32_t)((int32_t)L_233|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0952:
	{
		Event_t381 * L_234 = V_0;
		NullCheck(L_234);
		Event_set_keyCode_m4698(L_234, ((int32_t)295), /*hidden argument*/NULL);
		Event_t381 * L_235 = V_0;
		Event_t381 * L_236 = L_235;
		NullCheck(L_236);
		int32_t L_237 = Event_get_modifiers_m3612(L_236, /*hidden argument*/NULL);
		NullCheck(L_236);
		Event_set_modifiers_m4692(L_236, ((int32_t)((int32_t)L_237|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0971:
	{
		Event_t381 * L_238 = V_0;
		NullCheck(L_238);
		Event_set_keyCode_m4698(L_238, ((int32_t)296), /*hidden argument*/NULL);
		Event_t381 * L_239 = V_0;
		Event_t381 * L_240 = L_239;
		NullCheck(L_240);
		int32_t L_241 = Event_get_modifiers_m3612(L_240, /*hidden argument*/NULL);
		NullCheck(L_240);
		Event_set_modifiers_m4692(L_240, ((int32_t)((int32_t)L_241|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0990:
	{
		Event_t381 * L_242 = V_0;
		NullCheck(L_242);
		Event_set_keyCode_m4698(L_242, ((int32_t)27), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_099d:
	{
		Event_t381 * L_243 = V_0;
		NullCheck(L_243);
		Event_set_character_m4696(L_243, ((int32_t)10), /*hidden argument*/NULL);
		Event_t381 * L_244 = V_0;
		NullCheck(L_244);
		Event_set_keyCode_m4698(L_244, ((int32_t)13), /*hidden argument*/NULL);
		Event_t381 * L_245 = V_0;
		Event_t381 * L_246 = L_245;
		NullCheck(L_246);
		int32_t L_247 = Event_get_modifiers_m3612(L_246, /*hidden argument*/NULL);
		NullCheck(L_246);
		Event_set_modifiers_m4692(L_246, ((int32_t)((int32_t)L_247&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_09c1:
	{
		Event_t381 * L_248 = V_0;
		NullCheck(L_248);
		Event_set_keyCode_m4698(L_248, ((int32_t)32), /*hidden argument*/NULL);
		Event_t381 * L_249 = V_0;
		NullCheck(L_249);
		Event_set_character_m4696(L_249, ((int32_t)32), /*hidden argument*/NULL);
		Event_t381 * L_250 = V_0;
		Event_t381 * L_251 = L_250;
		NullCheck(L_251);
		int32_t L_252 = Event_get_modifiers_m3612(L_251, /*hidden argument*/NULL);
		NullCheck(L_251);
		Event_set_modifiers_m4692(L_251, ((int32_t)((int32_t)L_252&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_09e5:
	{
		String_t* L_253 = V_3;
		NullCheck(L_253);
		int32_t L_254 = String_get_Length_m1336(L_253, /*hidden argument*/NULL);
		if ((((int32_t)L_254) == ((int32_t)1)))
		{
			goto IL_0a36;
		}
	}

IL_09f1:
	try
	{ // begin try (depth: 1)
		Event_t381 * L_255 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_256 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(KeyCode_t897_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_257 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t21_il2cpp_TypeInfo_var);
		Object_t * L_258 = Enum_Parse_m1802(NULL /*static, unused*/, L_256, L_257, 1, /*hidden argument*/NULL);
		NullCheck(L_255);
		Event_set_keyCode_m4698(L_255, ((*(int32_t*)((int32_t*)UnBox (L_258, Int32_t372_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0a31;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t359 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t764_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0a12;
		throw e;
	}

CATCH_0a12:
	{ // begin catch(System.ArgumentException)
		ObjectU5BU5D_t356* L_259 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 1));
		String_t* L_260 = V_3;
		NullCheck(L_259);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_259, 0);
		ArrayElementTypeCheck (L_259, L_260);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_259, 0, sizeof(Object_t *))) = (Object_t *)L_260;
		String_t* L_261 = UnityString_Format_m4240(NULL /*static, unused*/, _stringLiteral617, L_259, /*hidden argument*/NULL);
		Debug_LogError_m1317(NULL /*static, unused*/, L_261, /*hidden argument*/NULL);
		goto IL_0a31;
	} // end catch (depth: 1)

IL_0a31:
	{
		goto IL_0a66;
	}

IL_0a36:
	{
		Event_t381 * L_262 = V_0;
		String_t* L_263 = V_3;
		NullCheck(L_263);
		String_t* L_264 = String_ToLower_m1695(L_263, /*hidden argument*/NULL);
		NullCheck(L_264);
		uint16_t L_265 = String_get_Chars_m1559(L_264, 0, /*hidden argument*/NULL);
		NullCheck(L_262);
		Event_set_character_m4696(L_262, L_265, /*hidden argument*/NULL);
		Event_t381 * L_266 = V_0;
		Event_t381 * L_267 = V_0;
		NullCheck(L_267);
		uint16_t L_268 = Event_get_character_m3614(L_267, /*hidden argument*/NULL);
		NullCheck(L_266);
		Event_set_keyCode_m4698(L_266, L_268, /*hidden argument*/NULL);
		Event_t381 * L_269 = V_0;
		NullCheck(L_269);
		int32_t L_270 = Event_get_modifiers_m3612(L_269, /*hidden argument*/NULL);
		if (!L_270)
		{
			goto IL_0a66;
		}
	}
	{
		Event_t381 * L_271 = V_0;
		NullCheck(L_271);
		Event_set_character_m4696(L_271, 0, /*hidden argument*/NULL);
	}

IL_0a66:
	{
		goto IL_0a6b;
	}

IL_0a6b:
	{
		Event_t381 * L_272 = V_0;
		return L_272;
	}
}
// System.Int32 UnityEngine.Event::GetHashCode()
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
extern "C" int32_t Event_GetHashCode_m4675 (Event_t381 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t2  V_1 = {0};
	{
		V_0 = 1;
		bool L_0 = Event_get_isKey_m4672(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = Event_get_keyCode_m3613(__this, /*hidden argument*/NULL);
		V_0 = (((uint16_t)L_1));
	}

IL_0015:
	{
		bool L_2 = Event_get_isMouse_m4673(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector2_t2  L_3 = Event_get_mousePosition_m1403(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Vector2_GetHashCode_m4025((&V_1), /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002f:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)((int32_t)37)))|(int32_t)L_6));
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Event::Equals(System.Object)
// System.Object
#include "mscorlib_System_Object.h"
extern TypeInfo* Event_t381_il2cpp_TypeInfo_var;
extern "C" bool Event_Equals_m4676 (Event_t381 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t381_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(457);
		s_Il2CppMethodIntialized = true;
	}
	Event_t381 * V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___obj;
		bool L_2 = Object_ReferenceEquals_m5145(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return 1;
	}

IL_0016:
	{
		Object_t * L_3 = ___obj;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m1368(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = Object_GetType_m1368(__this, /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_4) == ((Object_t*)(Type_t *)L_5)))
		{
			goto IL_0029;
		}
	}
	{
		return 0;
	}

IL_0029:
	{
		Object_t * L_6 = ___obj;
		V_0 = ((Event_t381 *)CastclassSealed(L_6, Event_t381_il2cpp_TypeInfo_var));
		int32_t L_7 = Event_get_type_m1405(__this, /*hidden argument*/NULL);
		Event_t381 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = Event_get_type_m1405(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)L_9))))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_10 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		Event_t381 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = Event_get_modifiers_m3612(L_11, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)-33)))) == ((int32_t)((int32_t)((int32_t)L_12&(int32_t)((int32_t)-33))))))
		{
			goto IL_005a;
		}
	}

IL_0058:
	{
		return 0;
	}

IL_005a:
	{
		bool L_13 = Event_get_isKey_m4672(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = Event_get_keyCode_m3613(__this, /*hidden argument*/NULL);
		Event_t381 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = Event_get_keyCode_m3613(L_15, /*hidden argument*/NULL);
		return ((((int32_t)L_14) == ((int32_t)L_16))? 1 : 0);
	}

IL_0074:
	{
		bool L_17 = Event_get_isMouse_m4673(__this, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0091;
		}
	}
	{
		Vector2_t2  L_18 = Event_get_mousePosition_m1403(__this, /*hidden argument*/NULL);
		Event_t381 * L_19 = V_0;
		NullCheck(L_19);
		Vector2_t2  L_20 = Event_get_mousePosition_m1403(L_19, /*hidden argument*/NULL);
		bool L_21 = Vector2_op_Equality_m3811(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_0091:
	{
		return 0;
	}
}
// System.String UnityEngine.Event::ToString()
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t898_il2cpp_TypeInfo_var;
extern TypeInfo* EventModifiers_t899_il2cpp_TypeInfo_var;
extern TypeInfo* KeyCode_t897_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2_t2_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral618;
extern Il2CppCodeGenString* _stringLiteral619;
extern Il2CppCodeGenString* _stringLiteral620;
extern Il2CppCodeGenString* _stringLiteral621;
extern Il2CppCodeGenString* _stringLiteral622;
extern Il2CppCodeGenString* _stringLiteral623;
extern Il2CppCodeGenString* _stringLiteral624;
extern "C" String_t* Event_ToString_m4677 (Event_t381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		EventType_t898_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(596);
		EventModifiers_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(597);
		KeyCode_t897_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(595);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Vector2_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		_stringLiteral618 = il2cpp_codegen_string_literal_from_index(618);
		_stringLiteral619 = il2cpp_codegen_string_literal_from_index(619);
		_stringLiteral620 = il2cpp_codegen_string_literal_from_index(620);
		_stringLiteral621 = il2cpp_codegen_string_literal_from_index(621);
		_stringLiteral622 = il2cpp_codegen_string_literal_from_index(622);
		_stringLiteral623 = il2cpp_codegen_string_literal_from_index(623);
		_stringLiteral624 = il2cpp_codegen_string_literal_from_index(624);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Event_get_isKey_m4672(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00b5;
		}
	}
	{
		uint16_t L_1 = Event_get_character_m3614(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		ObjectU5BU5D_t356* L_2 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 3));
		int32_t L_3 = Event_get_type_m1405(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(EventType_t898_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t356* L_6 = L_2;
		int32_t L_7 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(EventModifiers_t899_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 1, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t356* L_10 = L_6;
		int32_t L_11 = Event_get_keyCode_m3613(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(KeyCode_t897_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 2, sizeof(Object_t *))) = (Object_t *)L_13;
		String_t* L_14 = UnityString_Format_m4240(NULL /*static, unused*/, _stringLiteral618, L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0051:
	{
		ObjectU5BU5D_t356* L_15 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 8));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, _stringLiteral619);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral619;
		ObjectU5BU5D_t356* L_16 = L_15;
		int32_t L_17 = Event_get_type_m1405(__this, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Object_t * L_19 = Box(EventType_t898_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 1, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t356* L_20 = L_16;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		ArrayElementTypeCheck (L_20, _stringLiteral620);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral620;
		ObjectU5BU5D_t356* L_21 = L_20;
		uint16_t L_22 = Event_get_character_m3614(__this, /*hidden argument*/NULL);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 3);
		ArrayElementTypeCheck (L_21, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 3, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t356* L_25 = L_21;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, _stringLiteral621);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral621;
		ObjectU5BU5D_t356* L_26 = L_25;
		int32_t L_27 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		int32_t L_28 = L_27;
		Object_t * L_29 = Box(EventModifiers_t899_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 5);
		ArrayElementTypeCheck (L_26, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_26, 5, sizeof(Object_t *))) = (Object_t *)L_29;
		ObjectU5BU5D_t356* L_30 = L_26;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 6);
		ArrayElementTypeCheck (L_30, _stringLiteral622);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral622;
		ObjectU5BU5D_t356* L_31 = L_30;
		int32_t L_32 = Event_get_keyCode_m3613(__this, /*hidden argument*/NULL);
		int32_t L_33 = L_32;
		Object_t * L_34 = Box(KeyCode_t897_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 7);
		ArrayElementTypeCheck (L_31, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_31, 7, sizeof(Object_t *))) = (Object_t *)L_34;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m1316(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		return L_35;
	}

IL_00b5:
	{
		bool L_36 = Event_get_isMouse_m4673(__this, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00fb;
		}
	}
	{
		ObjectU5BU5D_t356* L_37 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 3));
		int32_t L_38 = Event_get_type_m1405(__this, /*hidden argument*/NULL);
		int32_t L_39 = L_38;
		Object_t * L_40 = Box(EventType_t898_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 0);
		ArrayElementTypeCheck (L_37, L_40);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_37, 0, sizeof(Object_t *))) = (Object_t *)L_40;
		ObjectU5BU5D_t356* L_41 = L_37;
		Vector2_t2  L_42 = Event_get_mousePosition_m1403(__this, /*hidden argument*/NULL);
		Vector2_t2  L_43 = L_42;
		Object_t * L_44 = Box(Vector2_t2_il2cpp_TypeInfo_var, &L_43);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 1);
		ArrayElementTypeCheck (L_41, L_44);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 1, sizeof(Object_t *))) = (Object_t *)L_44;
		ObjectU5BU5D_t356* L_45 = L_41;
		int32_t L_46 = Event_get_modifiers_m3612(__this, /*hidden argument*/NULL);
		int32_t L_47 = L_46;
		Object_t * L_48 = Box(EventModifiers_t899_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 2);
		ArrayElementTypeCheck (L_45, L_48);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_45, 2, sizeof(Object_t *))) = (Object_t *)L_48;
		String_t* L_49 = UnityString_Format_m4240(NULL /*static, unused*/, _stringLiteral623, L_45, /*hidden argument*/NULL);
		return L_49;
	}

IL_00fb:
	{
		int32_t L_50 = Event_get_type_m1405(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)((int32_t)14))))
		{
			goto IL_0115;
		}
	}
	{
		int32_t L_51 = Event_get_type_m1405(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_51) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_013d;
		}
	}

IL_0115:
	{
		ObjectU5BU5D_t356* L_52 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 2));
		int32_t L_53 = Event_get_type_m1405(__this, /*hidden argument*/NULL);
		int32_t L_54 = L_53;
		Object_t * L_55 = Box(EventType_t898_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 0);
		ArrayElementTypeCheck (L_52, L_55);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, 0, sizeof(Object_t *))) = (Object_t *)L_55;
		ObjectU5BU5D_t356* L_56 = L_52;
		String_t* L_57 = Event_get_commandName_m3617(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		ArrayElementTypeCheck (L_56, L_57);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, 1, sizeof(Object_t *))) = (Object_t *)L_57;
		String_t* L_58 = UnityString_Format_m4240(NULL /*static, unused*/, _stringLiteral624, L_56, /*hidden argument*/NULL);
		return L_58;
	}

IL_013d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		int32_t L_60 = Event_get_type_m1405(__this, /*hidden argument*/NULL);
		int32_t L_61 = L_60;
		Object_t * L_62 = Box(EventType_t898_il2cpp_TypeInfo_var, &L_61);
		String_t* L_63 = String_Concat_m1410(NULL /*static, unused*/, L_59, L_62, /*hidden argument*/NULL);
		return L_63;
	}
}
// System.Void UnityEngine.Event::Init()
extern "C" void Event_Init_m4678 (Event_t381 * __this, const MethodInfo* method)
{
	typedef void (*Event_Init_m4678_ftn) (Event_t381 *);
	static Event_Init_m4678_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Init_m4678_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::Cleanup()
extern "C" void Event_Cleanup_m4679 (Event_t381 * __this, const MethodInfo* method)
{
	typedef void (*Event_Cleanup_m4679_ftn) (Event_t381 *);
	static Event_Cleanup_m4679_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Cleanup_m4679_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::InitCopy(UnityEngine.Event)
extern "C" void Event_InitCopy_m4680 (Event_t381 * __this, Event_t381 * ___other, const MethodInfo* method)
{
	typedef void (*Event_InitCopy_m4680_ftn) (Event_t381 *, Event_t381 *);
	static Event_InitCopy_m4680_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_InitCopy_m4680_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::InitCopy(UnityEngine.Event)");
	_il2cpp_icall_func(__this, ___other);
}
// System.Void UnityEngine.Event::InitPtr(System.IntPtr)
extern "C" void Event_InitPtr_m4681 (Event_t381 * __this, IntPtr_t ___ptr, const MethodInfo* method)
{
	typedef void (*Event_InitPtr_m4681_ftn) (Event_t381 *, IntPtr_t);
	static Event_InitPtr_m4681_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_InitPtr_m4681_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::InitPtr(System.IntPtr)");
	_il2cpp_icall_func(__this, ___ptr);
}
// UnityEngine.EventType UnityEngine.Event::get_rawType()
extern "C" int32_t Event_get_rawType_m3616 (Event_t381 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_rawType_m3616_ftn) (Event_t381 *);
	static Event_get_rawType_m3616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_rawType_m3616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_rawType()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C" int32_t Event_get_type_m1405 (Event_t381 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_type_m1405_ftn) (Event_t381 *);
	static Event_get_type_m1405_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_type_m1405_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_type()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_type(UnityEngine.EventType)
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
extern "C" void Event_set_type_m4682 (Event_t381 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_type_m4682_ftn) (Event_t381 *, int32_t);
	static Event_set_type_m4682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_type_m4682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_type(UnityEngine.EventType)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.EventType UnityEngine.Event::GetTypeForControl(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" int32_t Event_GetTypeForControl_m4683 (Event_t381 * __this, int32_t ___controlID, const MethodInfo* method)
{
	typedef int32_t (*Event_GetTypeForControl_m4683_ftn) (Event_t381 *, int32_t);
	static Event_GetTypeForControl_m4683_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_GetTypeForControl_m4683_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::GetTypeForControl(System.Int32)");
	return _il2cpp_icall_func(__this, ___controlID);
}
// System.Void UnityEngine.Event::Internal_SetMousePosition(UnityEngine.Vector2)
extern "C" void Event_Internal_SetMousePosition_m4684 (Event_t381 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	{
		Event_INTERNAL_CALL_Internal_SetMousePosition_m4685(NULL /*static, unused*/, __this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::INTERNAL_CALL_Internal_SetMousePosition(UnityEngine.Event,UnityEngine.Vector2&)
extern "C" void Event_INTERNAL_CALL_Internal_SetMousePosition_m4685 (Object_t * __this /* static, unused */, Event_t381 * ___self, Vector2_t2 * ___value, const MethodInfo* method)
{
	typedef void (*Event_INTERNAL_CALL_Internal_SetMousePosition_m4685_ftn) (Event_t381 *, Vector2_t2 *);
	static Event_INTERNAL_CALL_Internal_SetMousePosition_m4685_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_INTERNAL_CALL_Internal_SetMousePosition_m4685_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::INTERNAL_CALL_Internal_SetMousePosition(UnityEngine.Event,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self, ___value);
}
// System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
extern "C" void Event_Internal_GetMousePosition_m4686 (Event_t381 * __this, Vector2_t2 * ___value, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMousePosition_m4686_ftn) (Event_t381 *, Vector2_t2 *);
	static Event_Internal_GetMousePosition_m4686_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMousePosition_m4686_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Event::Internal_SetMouseDelta(UnityEngine.Vector2)
extern "C" void Event_Internal_SetMouseDelta_m4687 (Event_t381 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	{
		Event_INTERNAL_CALL_Internal_SetMouseDelta_m4688(NULL /*static, unused*/, __this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::INTERNAL_CALL_Internal_SetMouseDelta(UnityEngine.Event,UnityEngine.Vector2&)
extern "C" void Event_INTERNAL_CALL_Internal_SetMouseDelta_m4688 (Object_t * __this /* static, unused */, Event_t381 * ___self, Vector2_t2 * ___value, const MethodInfo* method)
{
	typedef void (*Event_INTERNAL_CALL_Internal_SetMouseDelta_m4688_ftn) (Event_t381 *, Vector2_t2 *);
	static Event_INTERNAL_CALL_Internal_SetMouseDelta_m4688_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_INTERNAL_CALL_Internal_SetMouseDelta_m4688_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::INTERNAL_CALL_Internal_SetMouseDelta(UnityEngine.Event,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self, ___value);
}
// System.Void UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)
extern "C" void Event_Internal_GetMouseDelta_m4689 (Event_t381 * __this, Vector2_t2 * ___value, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMouseDelta_m4689_ftn) (Event_t381 *, Vector2_t2 *);
	static Event_Internal_GetMouseDelta_m4689_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMouseDelta_m4689_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Event::get_button()
extern "C" int32_t Event_get_button_m4690 (Event_t381 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_button_m4690_ftn) (Event_t381 *);
	static Event_get_button_m4690_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_button_m4690_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_button()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_button(System.Int32)
extern "C" void Event_set_button_m4691 (Event_t381 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_button_m4691_ftn) (Event_t381 *, int32_t);
	static Event_set_button_m4691_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_button_m4691_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_button(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
extern "C" int32_t Event_get_modifiers_m3612 (Event_t381 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_modifiers_m3612_ftn) (Event_t381 *);
	static Event_get_modifiers_m3612_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_modifiers_m3612_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_modifiers()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
extern "C" void Event_set_modifiers_m4692 (Event_t381 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_modifiers_m4692_ftn) (Event_t381 *, int32_t);
	static Event_set_modifiers_m4692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_modifiers_m4692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Event::get_pressure()
extern "C" float Event_get_pressure_m4693 (Event_t381 * __this, const MethodInfo* method)
{
	typedef float (*Event_get_pressure_m4693_ftn) (Event_t381 *);
	static Event_get_pressure_m4693_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_pressure_m4693_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_pressure()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_pressure(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void Event_set_pressure_m4694 (Event_t381 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Event_set_pressure_m4694_ftn) (Event_t381 *, float);
	static Event_set_pressure_m4694_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_pressure_m4694_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_pressure(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Event::get_clickCount()
extern "C" int32_t Event_get_clickCount_m1407 (Event_t381 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_clickCount_m1407_ftn) (Event_t381 *);
	static Event_get_clickCount_m1407_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_clickCount_m1407_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_clickCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_clickCount(System.Int32)
extern "C" void Event_set_clickCount_m4695 (Event_t381 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_clickCount_m4695_ftn) (Event_t381 *, int32_t);
	static Event_set_clickCount_m4695_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_clickCount_m4695_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_clickCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Char UnityEngine.Event::get_character()
extern "C" uint16_t Event_get_character_m3614 (Event_t381 * __this, const MethodInfo* method)
{
	typedef uint16_t (*Event_get_character_m3614_ftn) (Event_t381 *);
	static Event_get_character_m3614_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_character_m3614_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_character()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_character(System.Char)
// System.Char
#include "mscorlib_System_Char.h"
extern "C" void Event_set_character_m4696 (Event_t381 * __this, uint16_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_character_m4696_ftn) (Event_t381 *, uint16_t);
	static Event_set_character_m4696_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_character_m4696_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_character(System.Char)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.Event::get_commandName()
extern "C" String_t* Event_get_commandName_m3617 (Event_t381 * __this, const MethodInfo* method)
{
	typedef String_t* (*Event_get_commandName_m3617_ftn) (Event_t381 *);
	static Event_get_commandName_m3617_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_commandName_m3617_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_commandName()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_commandName(System.String)
extern "C" void Event_set_commandName_m4697 (Event_t381 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*Event_set_commandName_m4697_ftn) (Event_t381 *, String_t*);
	static Event_set_commandName_m4697_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_commandName_m4697_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_commandName(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
extern "C" int32_t Event_get_keyCode_m3613 (Event_t381 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_keyCode_m3613_ftn) (Event_t381 *);
	static Event_get_keyCode_m3613_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_keyCode_m3613_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_keyCode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
extern "C" void Event_set_keyCode_m4698 (Event_t381 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_keyCode_m4698_ftn) (Event_t381 *, int32_t);
	static Event_set_keyCode_m4698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_keyCode_m4698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
extern "C" void Event_Internal_SetNativeEvent_m4699 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	typedef void (*Event_Internal_SetNativeEvent_m4699_ftn) (IntPtr_t);
	static Event_Internal_SetNativeEvent_m4699_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_SetNativeEvent_m4699_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)");
	_il2cpp_icall_func(___ptr);
}
// System.Void UnityEngine.Event::Use()
extern "C" void Event_Use_m1406 (Event_t381 * __this, const MethodInfo* method)
{
	typedef void (*Event_Use_m1406_ftn) (Event_t381 *);
	static Event_Use_m1406_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Use_m1406_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Use()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
extern "C" bool Event_PopEvent_m3618 (Object_t * __this /* static, unused */, Event_t381 * ___outEvent, const MethodInfo* method)
{
	typedef bool (*Event_PopEvent_m3618_ftn) (Event_t381 *);
	static Event_PopEvent_m3618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_PopEvent_m3618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::PopEvent(UnityEngine.Event)");
	return _il2cpp_icall_func(___outEvent);
}
// System.Int32 UnityEngine.Event::GetEventCount()
extern "C" int32_t Event_GetEventCount_m4700 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Event_GetEventCount_m4700_ftn) ();
	static Event_GetEventCount_m4700_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_GetEventCount_m4700_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::GetEventCount()");
	return _il2cpp_icall_func();
}
// Conversion methods for marshalling of: UnityEngine.Event
extern "C" void Event_t381_marshal(const Event_t381& unmarshaled, Event_t381_marshaled& marshaled)
{
	Il2CppCodeGenException* ___s_Current_1Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field 's_Current' of type 'Event': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)___s_Current_1Exception);
}
extern "C" void Event_t381_marshal_back(const Event_t381_marshaled& marshaled, Event_t381& unmarshaled)
{
	Il2CppCodeGenException* ___s_Current_1Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field 's_Current' of type 'Event': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)___s_Current_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Event
extern "C" void Event_t381_marshal_cleanup(Event_t381_marshaled& marshaled)
{
}
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCodeMethodDeclarations.h"
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventTypeMethodDeclarations.h"
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiersMethodDeclarations.h"
// UnityEngine.GUI/ScrollViewState
#include "UnityEngine_UnityEngine_GUI_ScrollViewState.h"
// UnityEngine.GUI/ScrollViewState
#include "UnityEngine_UnityEngine_GUI_ScrollViewStateMethodDeclarations.h"
// System.Void UnityEngine.GUI/ScrollViewState::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void ScrollViewState__ctor_m4701 (ScrollViewState_t900 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
// System.Void UnityEngine.GUI/WindowFunction::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void WindowFunction__ctor_m1813 (WindowFunction_t456 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.GUI/WindowFunction::Invoke(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void WindowFunction_Invoke_m4702 (WindowFunction_t456 * __this, int32_t ___id, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WindowFunction_Invoke_m4702((WindowFunction_t456 *)__this->___prev_9,___id, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___id, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___id,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___id, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___id,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t456(Il2CppObject* delegate, int32_t ___id)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___id' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___id);

	// Marshaling cleanup of parameter '___id' native representation

}
// System.IAsyncResult UnityEngine.GUI/WindowFunction::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern "C" Object_t * WindowFunction_BeginInvoke_m4703 (WindowFunction_t456 * __this, int32_t ___id, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t372_il2cpp_TypeInfo_var, &___id);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.GUI/WindowFunction::EndInvoke(System.IAsyncResult)
extern "C" void WindowFunction_EndInvoke_m4704 (WindowFunction_t456 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUI.h"
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkin.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// UnityEngine.FocusType
#include "UnityEngine_UnityEngine_FocusType.h"
// UnityEngine.GUILayoutOption
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
// UnityEngine.GUILayoutUtility/LayoutCache
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCache.h"
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
// System.Void UnityEngine.GUI::.cctor()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern TypeInfo* GenericStack_t902_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t74_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral625;
extern Il2CppCodeGenString* _stringLiteral626;
extern Il2CppCodeGenString* _stringLiteral446;
extern Il2CppCodeGenString* _stringLiteral627;
extern Il2CppCodeGenString* _stringLiteral438;
extern Il2CppCodeGenString* _stringLiteral628;
extern Il2CppCodeGenString* _stringLiteral629;
extern "C" void GUI__cctor_m4705 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		GenericStack_t902_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(598);
		DateTime_t74_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(77);
		_stringLiteral625 = il2cpp_codegen_string_literal_from_index(625);
		_stringLiteral626 = il2cpp_codegen_string_literal_from_index(626);
		_stringLiteral446 = il2cpp_codegen_string_literal_from_index(446);
		_stringLiteral627 = il2cpp_codegen_string_literal_from_index(627);
		_stringLiteral438 = il2cpp_codegen_string_literal_from_index(438);
		_stringLiteral628 = il2cpp_codegen_string_literal_from_index(628);
		_stringLiteral629 = il2cpp_codegen_string_literal_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_ScrollStepSize_0 = (10.0f);
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_HotTextField_2 = (-1);
		NullCheck(_stringLiteral625);
		int32_t L_0 = String_GetHashCode_m5139(_stringLiteral625, /*hidden argument*/NULL);
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_BoxHash_3 = L_0;
		NullCheck(_stringLiteral626);
		int32_t L_1 = String_GetHashCode_m5139(_stringLiteral626, /*hidden argument*/NULL);
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4 = L_1;
		NullCheck(_stringLiteral446);
		int32_t L_2 = String_GetHashCode_m5139(_stringLiteral446, /*hidden argument*/NULL);
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_ToggleHash_5 = L_2;
		NullCheck(_stringLiteral627);
		int32_t L_3 = String_GetHashCode_m5139(_stringLiteral627, /*hidden argument*/NULL);
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_ButtonGridHash_6 = L_3;
		NullCheck(_stringLiteral438);
		int32_t L_4 = String_GetHashCode_m5139(_stringLiteral438, /*hidden argument*/NULL);
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7 = L_4;
		NullCheck(_stringLiteral628);
		int32_t L_5 = String_GetHashCode_m5139(_stringLiteral628, /*hidden argument*/NULL);
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_BeginGroupHash_8 = L_5;
		NullCheck(_stringLiteral629);
		int32_t L_6 = String_GetHashCode_m5139(_stringLiteral629, /*hidden argument*/NULL);
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_ScrollviewHash_9 = L_6;
		GenericStack_t902 * L_7 = (GenericStack_t902 *)il2cpp_codegen_object_new (GenericStack_t902_il2cpp_TypeInfo_var);
		GenericStack__ctor_m5092(L_7, /*hidden argument*/NULL);
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_ScrollViewStates_12 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t74_il2cpp_TypeInfo_var);
		DateTime_t74  L_8 = DateTime_get_Now_m1448(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_nextScrollStepTime_m4706(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::set_nextScrollStepTime(System.DateTime)
// System.DateTime
#include "mscorlib_System_DateTime.h"
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" void GUI_set_nextScrollStepTime_m4706 (Object_t * __this /* static, unused */, DateTime_t74  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t74  L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___U3CnextScrollStepTimeU3Ek__BackingField_13 = L_0;
		return;
	}
}
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkin.h"
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" void GUI_set_skin_m4707 (Object_t * __this /* static, unused */, GUISkin_t901 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m4904(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUISkin_t901 * L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUI_DoSetSkin_m4709(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" GUISkin_t901 * GUI_get_skin_m4708 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m4904(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUISkin_t901 * L_0 = ((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_Skin_10;
		return L_0;
	}
}
// System.Void UnityEngine.GUI::DoSetSkin(UnityEngine.GUISkin)
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" void GUI_DoSetSkin_m4709 (Object_t * __this /* static, unused */, GUISkin_t901 * ___newSkin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISkin_t901 * L_0 = ___newSkin;
		bool L_1 = Object_op_Implicit_m1320(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		GUISkin_t901 * L_2 = GUIUtility_GetDefaultSkin_m4900(NULL /*static, unused*/, /*hidden argument*/NULL);
		___newSkin = L_2;
	}

IL_0012:
	{
		GUISkin_t901 * L_3 = ___newSkin;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_Skin_10 = L_3;
		GUISkin_t901 * L_4 = ___newSkin;
		NullCheck(L_4);
		GUISkin_MakeCurrent_m4841(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String)
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m1816 (Object_t * __this /* static, unused */, Rect_t267  ___position, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t267  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent_t379 * L_2 = GUIContent_Temp_m4724(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUISkin_t901 * L_3 = ((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_Skin_10;
		NullCheck(L_3);
		GUIStyle_t342 * L_4 = GUISkin_get_label_m4795(L_3, /*hidden argument*/NULL);
		GUI_Label_m4710(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m4710 (Object_t * __this /* static, unused */, Rect_t267  ___position, GUIContent_t379 * ___content, GUIStyle_t342 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t267  L_0 = ___position;
		GUIContent_t379 * L_1 = ___content;
		GUIStyle_t342 * L_2 = ___style;
		NullCheck(L_2);
		IntPtr_t L_3 = (L_2->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUI_DoLabel_m4715(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" void GUI_Box_m4711 (Object_t * __this /* static, unused */, Rect_t267  ___position, GUIContent_t379 * ___content, GUIStyle_t342 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m4904(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_BoxHash_3;
		int32_t L_1 = GUIUtility_GetControlID_m4906(NULL /*static, unused*/, L_0, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t381 * L_2 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m1405(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)7))))
		{
			goto IL_002a;
		}
	}
	{
		GUIStyle_t342 * L_4 = ___style;
		Rect_t267  L_5 = ___position;
		GUIContent_t379 * L_6 = ___content;
		int32_t L_7 = V_0;
		NullCheck(L_4);
		GUIStyle_Draw_m4869(L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" bool GUI_Button_m1817 (Object_t * __this /* static, unused */, Rect_t267  ___position, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t267  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent_t379 * L_2 = GUIContent_Temp_m4724(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUISkin_t901 * L_3 = ((GUI_t457_StaticFields*)GUI_t457_il2cpp_TypeInfo_var->static_fields)->___s_Skin_10;
		NullCheck(L_3);
		GUIStyle_t342 * L_4 = GUISkin_get_button_m4801(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		IntPtr_t L_5 = (L_4->___m_Ptr_0);
		bool L_6 = GUI_DoButton_m4717(NULL /*static, unused*/, L_0, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" bool GUI_Button_m4712 (Object_t * __this /* static, unused */, Rect_t267  ___position, GUIContent_t379 * ___content, GUIStyle_t342 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t267  L_0 = ___position;
		GUIContent_t379 * L_1 = ___content;
		GUIStyle_t342 * L_2 = ___style;
		NullCheck(L_2);
		IntPtr_t L_3 = (L_2->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		bool L_4 = GUI_DoButton_m4717(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Rect UnityEngine.GUI::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String)
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" Rect_t267  GUI_Window_m1814 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t267  ___clientRect, WindowFunction_t456 * ___func, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id;
		Rect_t267  L_1 = ___clientRect;
		WindowFunction_t456 * L_2 = ___func;
		String_t* L_3 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent_t379 * L_4 = GUIContent_Temp_m4724(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUISkin_t901 * L_5 = GUI_get_skin_m4708(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyle_t342 * L_6 = GUISkin_get_window_m4805(L_5, /*hidden argument*/NULL);
		GUISkin_t901 * L_7 = GUI_get_skin_m4708(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t267  L_8 = GUI_DoWindow_m4719(NULL /*static, unused*/, L_0, L_1, L_2, L_4, L_6, L_7, 1, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.GUI::CallWindowDelegate(UnityEngine.GUI/WindowFunction,System.Int32,UnityEngine.GUISkin,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle)
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t441_il2cpp_TypeInfo_var;
extern "C" void GUI_CallWindowDelegate_m4713 (Object_t * __this /* static, unused */, WindowFunction_t456 * ___func, int32_t ___id, GUISkin_t901 * ____skin, int32_t ___forceRect, float ___width, float ___height, GUIStyle_t342 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		GUILayoutOptionU5BU5D_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(246);
		s_Il2CppMethodIntialized = true;
	}
	GUISkin_t901 * V_0 = {0};
	GUILayoutOptionU5BU5D_t441* V_1 = {0};
	{
		int32_t L_0 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		GUILayoutUtility_SelectIDList_m4733(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUISkin_t901 * L_1 = GUI_get_skin_m4708(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t381 * L_2 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m1405(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_4 = ___forceRect;
		if (!L_4)
		{
			goto IL_004d;
		}
	}
	{
		GUILayoutOptionU5BU5D_t441* L_5 = ((GUILayoutOptionU5BU5D_t441*)SZArrayNew(GUILayoutOptionU5BU5D_t441_il2cpp_TypeInfo_var, 2));
		float L_6 = ___width;
		GUILayoutOption_t912 * L_7 = GUILayout_Width_m4729(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		*((GUILayoutOption_t912 **)(GUILayoutOption_t912 **)SZArrayLdElema(L_5, 0, sizeof(GUILayoutOption_t912 *))) = (GUILayoutOption_t912 *)L_7;
		GUILayoutOptionU5BU5D_t441* L_8 = L_5;
		float L_9 = ___height;
		GUILayoutOption_t912 * L_10 = GUILayout_Height_m4730(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		*((GUILayoutOption_t912 **)(GUILayoutOption_t912 **)SZArrayLdElema(L_8, 1, sizeof(GUILayoutOption_t912 *))) = (GUILayoutOption_t912 *)L_10;
		V_1 = L_8;
		int32_t L_11 = ___id;
		GUIStyle_t342 * L_12 = ___style;
		GUILayoutOptionU5BU5D_t441* L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		GUILayoutUtility_BeginWindow_m4735(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_004d:
	{
		int32_t L_14 = ___id;
		GUIStyle_t342 * L_15 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		GUILayoutUtility_BeginWindow_m4735(NULL /*static, unused*/, L_14, L_15, (GUILayoutOptionU5BU5D_t441*)(GUILayoutOptionU5BU5D_t441*)NULL, /*hidden argument*/NULL);
	}

IL_0056:
	{
		GUISkin_t901 * L_16 = ____skin;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUI_set_skin_m4707(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		WindowFunction_t456 * L_17 = ___func;
		int32_t L_18 = ___id;
		NullCheck(L_17);
		WindowFunction_Invoke_m4702(L_17, L_18, /*hidden argument*/NULL);
		Event_t381 * L_19 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = Event_get_type_m1405(L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)8))))
		{
			goto IL_0078;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		GUILayoutUtility_Layout_m4737(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0078:
	{
		GUISkin_t901 * L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUI_set_skin_m4707(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::set_changed(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void GUI_set_changed_m4714 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GUI_set_changed_m4714_ftn) (bool);
	static GUI_set_changed_m4714_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_set_changed_m4714_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::set_changed(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUI::DoLabel(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" void GUI_DoLabel_m4715 (Object_t * __this /* static, unused */, Rect_t267  ___position, GUIContent_t379 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t379 * L_0 = ___content;
		IntPtr_t L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUI_INTERNAL_CALL_DoLabel_m4716(NULL /*static, unused*/, (&___position), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" void GUI_INTERNAL_CALL_DoLabel_m4716 (Object_t * __this /* static, unused */, Rect_t267 * ___position, GUIContent_t379 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef void (*GUI_INTERNAL_CALL_DoLabel_m4716_ftn) (Rect_t267 *, GUIContent_t379 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoLabel_m4716_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoLabel_m4716_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)");
	_il2cpp_icall_func(___position, ___content, ___style);
}
// System.Boolean UnityEngine.GUI::DoButton(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" bool GUI_DoButton_m4717 (Object_t * __this /* static, unused */, Rect_t267  ___position, GUIContent_t379 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t379 * L_0 = ___content;
		IntPtr_t L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		bool L_2 = GUI_INTERNAL_CALL_DoButton_m4718(NULL /*static, unused*/, (&___position), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoButton_m4718 (Object_t * __this /* static, unused */, Rect_t267 * ___position, GUIContent_t379 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef bool (*GUI_INTERNAL_CALL_DoButton_m4718_ftn) (Rect_t267 *, GUIContent_t379 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoButton_m4718_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoButton_m4718_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)");
	return _il2cpp_icall_func(___position, ___content, ___style);
}
// UnityEngine.Rect UnityEngine.GUI::DoWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" Rect_t267  GUI_DoWindow_m4719 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t267  ___clientRect, WindowFunction_t456 * ___func, GUIContent_t379 * ___title, GUIStyle_t342 * ___style, GUISkin_t901 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id;
		WindowFunction_t456 * L_1 = ___func;
		GUIContent_t379 * L_2 = ___title;
		GUIStyle_t342 * L_3 = ___style;
		GUISkin_t901 * L_4 = ___skin;
		bool L_5 = ___forceRectOnLayout;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		Rect_t267  L_6 = GUI_INTERNAL_CALL_DoWindow_m4720(NULL /*static, unused*/, L_0, (&___clientRect), L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Rect UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern "C" Rect_t267  GUI_INTERNAL_CALL_DoWindow_m4720 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t267 * ___clientRect, WindowFunction_t456 * ___func, GUIContent_t379 * ___title, GUIStyle_t342 * ___style, GUISkin_t901 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method)
{
	typedef Rect_t267  (*GUI_INTERNAL_CALL_DoWindow_m4720_ftn) (int32_t, Rect_t267 *, WindowFunction_t456 *, GUIContent_t379 *, GUIStyle_t342 *, GUISkin_t901 *, bool);
	static GUI_INTERNAL_CALL_DoWindow_m4720_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoWindow_m4720_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)");
	return _il2cpp_icall_func(___id, ___clientRect, ___func, ___title, ___style, ___skin, ___forceRectOnLayout);
}
// System.Void UnityEngine.GUIContent::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m4721 (GUIContent_t379 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIContent::.ctor(System.String)
// System.String
#include "mscorlib_System_String.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m1409 (GUIContent_t379 * __this, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___text;
		__this->___m_Text_0 = L_2;
		return;
	}
}
// System.Void UnityEngine.GUIContent::.ctor(UnityEngine.GUIContent)
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m4722 (GUIContent_t379 * __this, GUIContent_t379 * ___src, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		GUIContent_t379 * L_2 = ___src;
		NullCheck(L_2);
		String_t* L_3 = (L_2->___m_Text_0);
		__this->___m_Text_0 = L_3;
		GUIContent_t379 * L_4 = ___src;
		NullCheck(L_4);
		Texture_t103 * L_5 = (L_4->___m_Image_1);
		__this->___m_Image_1 = L_5;
		GUIContent_t379 * L_6 = ___src;
		NullCheck(L_6);
		String_t* L_7 = (L_6->___m_Tooltip_2);
		__this->___m_Tooltip_2 = L_7;
		return;
	}
}
// System.Void UnityEngine.GUIContent::.cctor()
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__cctor_m4723 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t379 * L_0 = (GUIContent_t379 *)il2cpp_codegen_object_new (GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent__ctor_m4721(L_0, /*hidden argument*/NULL);
		((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_Text_3 = L_0;
		GUIContent_t379 * L_1 = (GUIContent_t379 *)il2cpp_codegen_object_new (GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent__ctor_m4721(L_1, /*hidden argument*/NULL);
		((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_Image_4 = L_1;
		GUIContent_t379 * L_2 = (GUIContent_t379 *)il2cpp_codegen_object_new (GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent__ctor_m4721(L_2, /*hidden argument*/NULL);
		((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_5 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		GUIContent_t379 * L_4 = (GUIContent_t379 *)il2cpp_codegen_object_new (GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1409(L_4, L_3, /*hidden argument*/NULL);
		((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___none_6 = L_4;
		return;
	}
}
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(System.String)
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" GUIContent_t379 * GUIContent_Temp_m4724 (Object_t * __this /* static, unused */, String_t* ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent_t379 * L_0 = ((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		String_t* L_1 = ___t;
		NullCheck(L_0);
		L_0->___m_Text_0 = L_1;
		GUIContent_t379 * L_2 = ((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_2);
		L_2->___m_Tooltip_2 = L_3;
		GUIContent_t379 * L_4 = ((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		return L_4;
	}
}
// System.Void UnityEngine.GUIContent::ClearStaticCache()
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent_ClearStaticCache_m4725 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent_t379 * L_0 = ((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		NullCheck(L_0);
		L_0->___m_Text_0 = (String_t*)NULL;
		GUIContent_t379 * L_1 = ((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_1);
		L_1->___m_Tooltip_2 = L_2;
		GUIContent_t379 * L_3 = ((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_Image_4;
		NullCheck(L_3);
		L_3->___m_Image_1 = (Texture_t103 *)NULL;
		GUIContent_t379 * L_4 = ((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_Image_4;
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_4);
		L_4->___m_Tooltip_2 = L_5;
		GUIContent_t379 * L_6 = ((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_5;
		NullCheck(L_6);
		L_6->___m_Text_0 = (String_t*)NULL;
		GUIContent_t379 * L_7 = ((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_5;
		NullCheck(L_7);
		L_7->___m_Image_1 = (Texture_t103 *)NULL;
		return;
	}
}
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayout.h"
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroup.h"
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_Type.h"
// UnityEngine.GUILayoutOption
#include "UnityEngine_UnityEngine_GUILayoutOptionMethodDeclarations.h"
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUILayoutOption[])
// System.String
#include "mscorlib_System_String.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" void GUILayout_Label_m1710 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent_t379 * L_1 = GUIContent_Temp_m4724(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUISkin_t901 * L_2 = GUI_get_skin_m4708(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t342 * L_3 = GUISkin_get_label_m4795(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t441* L_4 = ___options;
		GUILayout_DoLabel_m4726(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::DoLabel(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" void GUILayout_DoLabel_m4726 (Object_t * __this /* static, unused */, GUIContent_t379 * ___content, GUIStyle_t342 * ___style, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t379 * L_0 = ___content;
		GUIStyle_t342 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t441* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		Rect_t267  L_3 = GUILayoutUtility_GetRect_m4744(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		GUIContent_t379 * L_4 = ___content;
		GUIStyle_t342 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUI_Label_m4710(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_Button_m1712 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent_t379 * L_1 = GUIContent_Temp_m4724(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUISkin_t901 * L_2 = GUI_get_skin_m4708(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t342 * L_3 = GUISkin_get_button_m4801(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t441* L_4 = ___options;
		bool L_5 = GUILayout_DoButton_m4727(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.GUILayout::DoButton(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_DoButton_m4727 (Object_t * __this /* static, unused */, GUIContent_t379 * ___content, GUIStyle_t342 * ___style, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t379 * L_0 = ___content;
		GUIStyle_t342 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t441* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		Rect_t267  L_3 = GUILayoutUtility_GetRect_m4744(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		GUIContent_t379 * L_4 = ___content;
		GUIStyle_t342 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		bool L_6 = GUI_Button_m4712(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUILayoutOption[])
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginHorizontal_m1711 (Object_t * __this /* static, unused */, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent_t379 * L_0 = ((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___none_6;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_1 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t441* L_2 = ___options;
		GUILayout_BeginHorizontal_m4728(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
extern const Il2CppType* GUILayoutGroup_t905_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginHorizontal_m4728 (Object_t * __this /* static, unused */, GUIContent_t379 * ___content, GUIStyle_t342 * ___style, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t905_0_0_0_var = il2cpp_codegen_type_from_index(599);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t905 * V_0 = {0};
	{
		GUIStyle_t342 * L_0 = ___style;
		GUILayoutOptionU5BU5D_t441* L_1 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t905_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		GUILayoutGroup_t905 * L_3 = GUILayoutUtility_BeginLayoutGroup_m4742(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GUILayoutGroup_t905 * L_4 = V_0;
		NullCheck(L_4);
		L_4->___isVertical_11 = 0;
		GUIStyle_t342 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_6 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t342 *)L_5) == ((Object_t*)(GUIStyle_t342 *)L_6))))
		{
			goto IL_002f;
		}
	}
	{
		GUIContent_t379 * L_7 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent_t379 * L_8 = ((GUIContent_t379_StaticFields*)GUIContent_t379_il2cpp_TypeInfo_var->static_fields)->___none_6;
		if ((((Object_t*)(GUIContent_t379 *)L_7) == ((Object_t*)(GUIContent_t379 *)L_8)))
		{
			goto IL_003c;
		}
	}

IL_002f:
	{
		GUILayoutGroup_t905 * L_9 = V_0;
		NullCheck(L_9);
		Rect_t267  L_10 = (((GUILayoutEntry_t907 *)L_9)->___rect_4);
		GUIContent_t379 * L_11 = ___content;
		GUIStyle_t342 * L_12 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUI_Box_m4711(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndHorizontal()
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral630;
extern "C" void GUILayout_EndHorizontal_m1714 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral630 = il2cpp_codegen_string_literal_from_index(630);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		GUILayoutUtility_EndGroup_m4736(NULL /*static, unused*/, _stringLiteral630, /*hidden argument*/NULL);
		GUILayoutUtility_EndLayoutGroup_m4743(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Width(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.GUILayoutOption
#include "UnityEngine_UnityEngine_GUILayoutOptionMethodDeclarations.h"
extern TypeInfo* Single_t388_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t912_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t912 * GUILayout_Width_m4729 (Object_t * __this /* static, unused */, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		GUILayoutOption_t912_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(247);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___width;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t388_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t912 * L_3 = (GUILayoutOption_t912 *)il2cpp_codegen_object_new (GUILayoutOption_t912_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m4783(L_3, 0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Height(System.Single)
extern TypeInfo* Single_t388_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t912_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t912 * GUILayout_Height_m4730 (Object_t * __this /* static, unused */, float ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		GUILayoutOption_t912_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(247);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___height;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t388_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t912 * L_3 = (GUILayoutOption_t912 *)il2cpp_codegen_object_new (GUILayoutOption_t912_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m4783(L_3, 1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutUtility/LayoutCache
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCacheMethodDeclarations.h"
// System.Collections.Stack
#include "mscorlib_System_Collections_Stack.h"
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroupMethodDeclarations.h"
// System.Collections.Stack
#include "mscorlib_System_Collections_StackMethodDeclarations.h"
// System.Void UnityEngine.GUILayoutUtility/LayoutCache::.ctor()
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroupMethodDeclarations.h"
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern TypeInfo* GUILayoutGroup_t905_il2cpp_TypeInfo_var;
extern TypeInfo* GenericStack_t902_il2cpp_TypeInfo_var;
extern "C" void LayoutCache__ctor_m4731 (LayoutCache_t904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		GenericStack_t902_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(598);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutGroup_t905 * L_0 = (GUILayoutGroup_t905 *)il2cpp_codegen_object_new (GUILayoutGroup_t905_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m4763(L_0, /*hidden argument*/NULL);
		__this->___topLevel_0 = L_0;
		GenericStack_t902 * L_1 = (GenericStack_t902 *)il2cpp_codegen_object_new (GenericStack_t902_il2cpp_TypeInfo_var);
		GenericStack__ctor_m5092(L_1, /*hidden argument*/NULL);
		__this->___layoutGroups_1 = L_1;
		GUILayoutGroup_t905 * L_2 = (GUILayoutGroup_t905 *)il2cpp_codegen_object_new (GUILayoutGroup_t905_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m4763(L_2, /*hidden argument*/NULL);
		__this->___windows_2 = L_2;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		GenericStack_t902 * L_3 = (__this->___layoutGroups_1);
		GUILayoutGroup_t905 * L_4 = (__this->___topLevel_0);
		NullCheck(L_3);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_3, L_4);
		return;
	}
}
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtility.h"
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_19.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_gen_46.h"
// UnityEngine.GUIWordWrapSizer
#include "UnityEngine_UnityEngine_GUIWordWrapSizer.h"
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_19MethodDeclarations.h"
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntryMethodDeclarations.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_gen_46MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5MethodDeclarations.h"
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
// UnityEngine.GUIWordWrapSizer
#include "UnityEngine_UnityEngine_GUIWordWrapSizerMethodDeclarations.h"
// System.Void UnityEngine.GUILayoutUtility::.cctor()
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_19MethodDeclarations.h"
// UnityEngine.GUILayoutUtility/LayoutCache
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCacheMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
extern TypeInfo* Dictionary_2_t906_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutCache_t904_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m5146_MethodInfo_var;
extern "C" void GUILayoutUtility__cctor_m4732 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(601);
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		LayoutCache_t904_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Dictionary_2__ctor_m5146_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484229);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t906 * L_0 = (Dictionary_2_t906 *)il2cpp_codegen_object_new (Dictionary_2_t906_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m5146(L_0, /*hidden argument*/Dictionary_2__ctor_m5146_MethodInfo_var);
		((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___s_StoredLayouts_0 = L_0;
		Dictionary_2_t906 * L_1 = (Dictionary_2_t906 *)il2cpp_codegen_object_new (Dictionary_2_t906_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m5146(L_1, /*hidden argument*/Dictionary_2__ctor_m5146_MethodInfo_var);
		((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___s_StoredWindows_1 = L_1;
		LayoutCache_t904 * L_2 = (LayoutCache_t904 *)il2cpp_codegen_object_new (LayoutCache_t904_il2cpp_TypeInfo_var);
		LayoutCache__ctor_m4731(L_2, /*hidden argument*/NULL);
		((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2 = L_2;
		Rect_t267  L_3 = {0};
		Rect__ctor_m1775(&L_3, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3 = L_3;
		return;
	}
}
// UnityEngine.GUILayoutUtility/LayoutCache UnityEngine.GUILayoutUtility::SelectIDList(System.Int32,System.Boolean)
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutCache_t904_il2cpp_TypeInfo_var;
extern "C" LayoutCache_t904 * GUILayoutUtility_SelectIDList_m4733 (Object_t * __this /* static, unused */, int32_t ___instanceID, bool ___isWindow, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		LayoutCache_t904_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t906 * V_0 = {0};
	LayoutCache_t904 * V_1 = {0};
	Dictionary_2_t906 * G_B3_0 = {0};
	{
		bool L_0 = ___isWindow;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		Dictionary_2_t906 * L_1 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___s_StoredWindows_1;
		G_B3_0 = L_1;
		goto IL_0015;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		Dictionary_2_t906 * L_2 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___s_StoredLayouts_0;
		G_B3_0 = L_2;
	}

IL_0015:
	{
		V_0 = G_B3_0;
		Dictionary_2_t906 * L_3 = V_0;
		int32_t L_4 = ___instanceID;
		NullCheck(L_3);
		bool L_5 = (bool)VirtFuncInvoker2< bool, int32_t, LayoutCache_t904 ** >::Invoke(33 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::TryGetValue(!0,!1&) */, L_3, L_4, (&V_1));
		if (L_5)
		{
			goto IL_0037;
		}
	}
	{
		LayoutCache_t904 * L_6 = (LayoutCache_t904 *)il2cpp_codegen_object_new (LayoutCache_t904_il2cpp_TypeInfo_var);
		LayoutCache__ctor_m4731(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		Dictionary_2_t906 * L_7 = V_0;
		int32_t L_8 = ___instanceID;
		LayoutCache_t904 * L_9 = V_1;
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, LayoutCache_t904 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Item(!0,!1) */, L_7, L_8, L_9);
		goto IL_0037;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_10 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_11 = V_1;
		NullCheck(L_11);
		GUILayoutGroup_t905 * L_12 = (L_11->___topLevel_0);
		NullCheck(L_10);
		L_10->___topLevel_0 = L_12;
		LayoutCache_t904 * L_13 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_14 = V_1;
		NullCheck(L_14);
		GenericStack_t902 * L_15 = (L_14->___layoutGroups_1);
		NullCheck(L_13);
		L_13->___layoutGroups_1 = L_15;
		LayoutCache_t904 * L_16 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_17 = V_1;
		NullCheck(L_17);
		GUILayoutGroup_t905 * L_18 = (L_17->___windows_2);
		NullCheck(L_16);
		L_16->___windows_2 = L_18;
		LayoutCache_t904 * L_19 = V_1;
		return L_19;
	}
}
// System.Void UnityEngine.GUILayoutUtility::Begin(System.Int32)
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroupMethodDeclarations.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t905_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Begin_m4734 (Object_t * __this /* static, unused */, int32_t ___instanceID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUILayoutGroup_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		s_Il2CppMethodIntialized = true;
	}
	LayoutCache_t904 * V_0 = {0};
	GUILayoutGroup_t905 * V_1 = {0};
	{
		int32_t L_0 = ___instanceID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_1 = GUILayoutUtility_SelectIDList_m4733(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t381 * L_2 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m1405(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_4 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_5 = V_0;
		GUILayoutGroup_t905 * L_6 = (GUILayoutGroup_t905 *)il2cpp_codegen_object_new (GUILayoutGroup_t905_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m4763(L_6, /*hidden argument*/NULL);
		GUILayoutGroup_t905 * L_7 = L_6;
		V_1 = L_7;
		NullCheck(L_5);
		L_5->___topLevel_0 = L_7;
		GUILayoutGroup_t905 * L_8 = V_1;
		NullCheck(L_4);
		L_4->___topLevel_0 = L_8;
		LayoutCache_t904 * L_9 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GenericStack_t902 * L_10 = (L_9->___layoutGroups_1);
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Collections.Stack::Clear() */, L_10);
		LayoutCache_t904 * L_11 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GenericStack_t902 * L_12 = (L_11->___layoutGroups_1);
		LayoutCache_t904 * L_13 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_13);
		GUILayoutGroup_t905 * L_14 = (L_13->___topLevel_0);
		NullCheck(L_12);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_12, L_14);
		LayoutCache_t904 * L_15 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_16 = V_0;
		GUILayoutGroup_t905 * L_17 = (GUILayoutGroup_t905 *)il2cpp_codegen_object_new (GUILayoutGroup_t905_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m4763(L_17, /*hidden argument*/NULL);
		GUILayoutGroup_t905 * L_18 = L_17;
		V_1 = L_18;
		NullCheck(L_16);
		L_16->___windows_2 = L_18;
		GUILayoutGroup_t905 * L_19 = V_1;
		NullCheck(L_15);
		L_15->___windows_2 = L_19;
		goto IL_00a5;
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_20 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_21 = V_0;
		NullCheck(L_21);
		GUILayoutGroup_t905 * L_22 = (L_21->___topLevel_0);
		NullCheck(L_20);
		L_20->___topLevel_0 = L_22;
		LayoutCache_t904 * L_23 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_24 = V_0;
		NullCheck(L_24);
		GenericStack_t902 * L_25 = (L_24->___layoutGroups_1);
		NullCheck(L_23);
		L_23->___layoutGroups_1 = L_25;
		LayoutCache_t904 * L_26 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_27 = V_0;
		NullCheck(L_27);
		GUILayoutGroup_t905 * L_28 = (L_27->___windows_2);
		NullCheck(L_26);
		L_26->___windows_2 = L_28;
	}

IL_00a5:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::BeginWindow(System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntryMethodDeclarations.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t905_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_BeginWindow_m4735 (Object_t * __this /* static, unused */, int32_t ___windowID, GUIStyle_t342 * ___style, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUILayoutGroup_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		s_Il2CppMethodIntialized = true;
	}
	LayoutCache_t904 * V_0 = {0};
	GUILayoutGroup_t905 * V_1 = {0};
	{
		int32_t L_0 = ___windowID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_1 = GUILayoutUtility_SelectIDList_m4733(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t381 * L_2 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m1405(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_00ab;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_4 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_5 = V_0;
		GUILayoutGroup_t905 * L_6 = (GUILayoutGroup_t905 *)il2cpp_codegen_object_new (GUILayoutGroup_t905_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m4763(L_6, /*hidden argument*/NULL);
		GUILayoutGroup_t905 * L_7 = L_6;
		V_1 = L_7;
		NullCheck(L_5);
		L_5->___topLevel_0 = L_7;
		GUILayoutGroup_t905 * L_8 = V_1;
		NullCheck(L_4);
		L_4->___topLevel_0 = L_8;
		LayoutCache_t904 * L_9 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GUILayoutGroup_t905 * L_10 = (L_9->___topLevel_0);
		GUIStyle_t342 * L_11 = ___style;
		NullCheck(L_10);
		GUILayoutEntry_set_style_m4754(L_10, L_11, /*hidden argument*/NULL);
		LayoutCache_t904 * L_12 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_12);
		GUILayoutGroup_t905 * L_13 = (L_12->___topLevel_0);
		int32_t L_14 = ___windowID;
		NullCheck(L_13);
		L_13->___windowID_16 = L_14;
		GUILayoutOptionU5BU5D_t441* L_15 = ___options;
		if (!L_15)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_16 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_16);
		GUILayoutGroup_t905 * L_17 = (L_16->___topLevel_0);
		GUILayoutOptionU5BU5D_t441* L_18 = ___options;
		NullCheck(L_17);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t441* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_17, L_18);
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_19 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_19);
		GenericStack_t902 * L_20 = (L_19->___layoutGroups_1);
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Collections.Stack::Clear() */, L_20);
		LayoutCache_t904 * L_21 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_21);
		GenericStack_t902 * L_22 = (L_21->___layoutGroups_1);
		LayoutCache_t904 * L_23 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_23);
		GUILayoutGroup_t905 * L_24 = (L_23->___topLevel_0);
		NullCheck(L_22);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_22, L_24);
		LayoutCache_t904 * L_25 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_26 = V_0;
		GUILayoutGroup_t905 * L_27 = (GUILayoutGroup_t905 *)il2cpp_codegen_object_new (GUILayoutGroup_t905_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m4763(L_27, /*hidden argument*/NULL);
		GUILayoutGroup_t905 * L_28 = L_27;
		V_1 = L_28;
		NullCheck(L_26);
		L_26->___windows_2 = L_28;
		GUILayoutGroup_t905 * L_29 = V_1;
		NullCheck(L_25);
		L_25->___windows_2 = L_29;
		goto IL_00db;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_30 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_31 = V_0;
		NullCheck(L_31);
		GUILayoutGroup_t905 * L_32 = (L_31->___topLevel_0);
		NullCheck(L_30);
		L_30->___topLevel_0 = L_32;
		LayoutCache_t904 * L_33 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_34 = V_0;
		NullCheck(L_34);
		GenericStack_t902 * L_35 = (L_34->___layoutGroups_1);
		NullCheck(L_33);
		L_33->___layoutGroups_1 = L_35;
		LayoutCache_t904 * L_36 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_37 = V_0;
		NullCheck(L_37);
		GUILayoutGroup_t905 * L_38 = (L_37->___windows_2);
		NullCheck(L_36);
		L_36->___windows_2 = L_38;
	}

IL_00db:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::EndGroup(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" void GUILayoutUtility_EndGroup_m4736 (Object_t * __this /* static, unused */, String_t* ___groupName, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::Layout()
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Layout_m4737 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_0 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t905 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		int32_t L_2 = (L_1->___windowID_16);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_00af;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_3 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_3);
		GUILayoutGroup_t905 * L_4 = (L_3->___topLevel_0);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_4);
		LayoutCache_t904 * L_5 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GUILayoutGroup_t905 * L_6 = (L_5->___topLevel_0);
		int32_t L_7 = Screen_get_width_m1779(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		float L_8 = GUIUtility_get_pixelsPerPoint_m4899(NULL /*static, unused*/, /*hidden argument*/NULL);
		LayoutCache_t904 * L_9 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GUILayoutGroup_t905 * L_10 = (L_9->___topLevel_0);
		NullCheck(L_10);
		float L_11 = (((GUILayoutEntry_t907 *)L_10)->___maxWidth_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Min_m1767(NULL /*static, unused*/, ((float)((float)(((float)L_7))/(float)L_8)), L_11, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_6, (0.0f), L_12);
		LayoutCache_t904 * L_13 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_13);
		GUILayoutGroup_t905 * L_14 = (L_13->___topLevel_0);
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_14);
		LayoutCache_t904 * L_15 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_15);
		GUILayoutGroup_t905 * L_16 = (L_15->___topLevel_0);
		int32_t L_17 = Screen_get_height_m1780(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = GUIUtility_get_pixelsPerPoint_m4899(NULL /*static, unused*/, /*hidden argument*/NULL);
		LayoutCache_t904 * L_19 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_19);
		GUILayoutGroup_t905 * L_20 = (L_19->___topLevel_0);
		NullCheck(L_20);
		float L_21 = (((GUILayoutEntry_t907 *)L_20)->___maxHeight_3);
		float L_22 = Mathf_Min_m1767(NULL /*static, unused*/, ((float)((float)(((float)L_17))/(float)L_18)), L_21, /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_16, (0.0f), L_22);
		LayoutCache_t904 * L_23 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_23);
		GUILayoutGroup_t905 * L_24 = (L_23->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m4739(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00af:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_25 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_25);
		GUILayoutGroup_t905 * L_26 = (L_25->___topLevel_0);
		GUILayoutUtility_LayoutSingleGroup_m4740(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		LayoutCache_t904 * L_27 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_27);
		GUILayoutGroup_t905 * L_28 = (L_27->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m4739(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
	}

IL_00cd:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutFromEditorWindow()
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_LayoutFromEditorWindow_m4738 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_0 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t905 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_1);
		LayoutCache_t904 * L_2 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GUILayoutGroup_t905 * L_3 = (L_2->___topLevel_0);
		int32_t L_4 = Screen_get_width_m1779(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		float L_5 = GUIUtility_get_pixelsPerPoint_m4899(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_3, (0.0f), ((float)((float)(((float)L_4))/(float)L_5)));
		LayoutCache_t904 * L_6 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_6);
		GUILayoutGroup_t905 * L_7 = (L_6->___topLevel_0);
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_7);
		LayoutCache_t904 * L_8 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_8);
		GUILayoutGroup_t905 * L_9 = (L_8->___topLevel_0);
		int32_t L_10 = Screen_get_height_m1780(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = GUIUtility_get_pixelsPerPoint_m4899(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_9, (0.0f), ((float)((float)(((float)L_10))/(float)L_11)));
		LayoutCache_t904 * L_12 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_12);
		GUILayoutGroup_t905 * L_13 = (L_12->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m4739(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutFreeGroup(UnityEngine.GUILayoutGroup)
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroup.h"
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_gen_46MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5MethodDeclarations.h"
extern TypeInfo* GUILayoutGroup_t905_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1023_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m5147_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m5148_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m5149_MethodInfo_var;
extern "C" void GUILayoutUtility_LayoutFreeGroup_m4739 (Object_t * __this /* static, unused */, GUILayoutGroup_t905 * ___toplevel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Enumerator_t1023_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		List_1_GetEnumerator_m5147_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484230);
		Enumerator_get_Current_m5148_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484231);
		Enumerator_MoveNext_m5149_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t905 * V_0 = {0};
	Enumerator_t1023  V_1 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GUILayoutGroup_t905 * L_0 = ___toplevel;
		NullCheck(L_0);
		List_1_t908 * L_1 = (L_0->___entries_10);
		NullCheck(L_1);
		Enumerator_t1023  L_2 = List_1_GetEnumerator_m5147(L_1, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_1 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			GUILayoutEntry_t907 * L_3 = Enumerator_get_Current_m5148((&V_1), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_0 = ((GUILayoutGroup_t905 *)CastclassClass(L_3, GUILayoutGroup_t905_il2cpp_TypeInfo_var));
			GUILayoutGroup_t905 * L_4 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
			GUILayoutUtility_LayoutSingleGroup_m4740(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		}

IL_0024:
		{
			bool L_5 = Enumerator_MoveNext_m5149((&V_1), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_6 = V_1;
		Enumerator_t1023  L_7 = L_6;
		Object_t * L_8 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_8);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0041:
	{
		GUILayoutGroup_t905 * L_9 = ___toplevel;
		NullCheck(L_9);
		GUILayoutGroup_ResetCursor_m4767(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutSingleGroup(UnityEngine.GUILayoutGroup)
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_LayoutSingleGroup_m4740 (Object_t * __this /* static, unused */, GUILayoutGroup_t905 * ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Rect_t267  V_4 = {0};
	{
		GUILayoutGroup_t905 * L_0 = ___i;
		NullCheck(L_0);
		bool L_1 = (L_0->___isWindow_15);
		if (L_1)
		{
			goto IL_0074;
		}
	}
	{
		GUILayoutGroup_t905 * L_2 = ___i;
		NullCheck(L_2);
		float L_3 = (((GUILayoutEntry_t907 *)L_2)->___minWidth_0);
		V_0 = L_3;
		GUILayoutGroup_t905 * L_4 = ___i;
		NullCheck(L_4);
		float L_5 = (((GUILayoutEntry_t907 *)L_4)->___maxWidth_1);
		V_1 = L_5;
		GUILayoutGroup_t905 * L_6 = ___i;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_6);
		GUILayoutGroup_t905 * L_7 = ___i;
		GUILayoutGroup_t905 * L_8 = ___i;
		NullCheck(L_8);
		Rect_t267 * L_9 = &(((GUILayoutEntry_t907 *)L_8)->___rect_4);
		float L_10 = Rect_get_x_m1758(L_9, /*hidden argument*/NULL);
		GUILayoutGroup_t905 * L_11 = ___i;
		NullCheck(L_11);
		float L_12 = (((GUILayoutEntry_t907 *)L_11)->___maxWidth_1);
		float L_13 = V_0;
		float L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_15 = Mathf_Clamp_m1358(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_7, L_10, L_15);
		GUILayoutGroup_t905 * L_16 = ___i;
		NullCheck(L_16);
		float L_17 = (((GUILayoutEntry_t907 *)L_16)->___minHeight_2);
		V_2 = L_17;
		GUILayoutGroup_t905 * L_18 = ___i;
		NullCheck(L_18);
		float L_19 = (((GUILayoutEntry_t907 *)L_18)->___maxHeight_3);
		V_3 = L_19;
		GUILayoutGroup_t905 * L_20 = ___i;
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_20);
		GUILayoutGroup_t905 * L_21 = ___i;
		GUILayoutGroup_t905 * L_22 = ___i;
		NullCheck(L_22);
		Rect_t267 * L_23 = &(((GUILayoutEntry_t907 *)L_22)->___rect_4);
		float L_24 = Rect_get_y_m1760(L_23, /*hidden argument*/NULL);
		GUILayoutGroup_t905 * L_25 = ___i;
		NullCheck(L_25);
		float L_26 = (((GUILayoutEntry_t907 *)L_25)->___maxHeight_3);
		float L_27 = V_2;
		float L_28 = V_3;
		float L_29 = Mathf_Clamp_m1358(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_21, L_24, L_29);
		goto IL_00e8;
	}

IL_0074:
	{
		GUILayoutGroup_t905 * L_30 = ___i;
		NullCheck(L_30);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_30);
		GUILayoutGroup_t905 * L_31 = ___i;
		NullCheck(L_31);
		int32_t L_32 = (L_31->___windowID_16);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		Rect_t267  L_33 = GUILayoutUtility_Internal_GetWindowRect_m4747(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		V_4 = L_33;
		GUILayoutGroup_t905 * L_34 = ___i;
		float L_35 = Rect_get_x_m1758((&V_4), /*hidden argument*/NULL);
		float L_36 = Rect_get_width_m1762((&V_4), /*hidden argument*/NULL);
		GUILayoutGroup_t905 * L_37 = ___i;
		NullCheck(L_37);
		float L_38 = (((GUILayoutEntry_t907 *)L_37)->___minWidth_0);
		GUILayoutGroup_t905 * L_39 = ___i;
		NullCheck(L_39);
		float L_40 = (((GUILayoutEntry_t907 *)L_39)->___maxWidth_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_41 = Mathf_Clamp_m1358(NULL /*static, unused*/, L_36, L_38, L_40, /*hidden argument*/NULL);
		NullCheck(L_34);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_34, L_35, L_41);
		GUILayoutGroup_t905 * L_42 = ___i;
		NullCheck(L_42);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_42);
		GUILayoutGroup_t905 * L_43 = ___i;
		float L_44 = Rect_get_y_m1760((&V_4), /*hidden argument*/NULL);
		float L_45 = Rect_get_height_m1764((&V_4), /*hidden argument*/NULL);
		GUILayoutGroup_t905 * L_46 = ___i;
		NullCheck(L_46);
		float L_47 = (((GUILayoutEntry_t907 *)L_46)->___minHeight_2);
		GUILayoutGroup_t905 * L_48 = ___i;
		NullCheck(L_48);
		float L_49 = (((GUILayoutEntry_t907 *)L_48)->___maxHeight_3);
		float L_50 = Mathf_Clamp_m1358(NULL /*static, unused*/, L_45, L_47, L_49, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_43, L_44, L_50);
		GUILayoutGroup_t905 * L_51 = ___i;
		NullCheck(L_51);
		int32_t L_52 = (L_51->___windowID_16);
		GUILayoutGroup_t905 * L_53 = ___i;
		NullCheck(L_53);
		Rect_t267  L_54 = (((GUILayoutEntry_t907 *)L_53)->___rect_4);
		GUILayoutUtility_Internal_MoveWindow_m4748(NULL /*static, unused*/, L_52, L_54, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		return;
	}
}
// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility::CreateGUILayoutGroupInstanceOfType(System.Type)
// System.Type
#include "mscorlib_System_Type.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
extern const Il2CppType* GUILayoutGroup_t905_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t905_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral631;
extern "C" GUILayoutGroup_t905 * GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m4741 (Object_t * __this /* static, unused */, Type_t * ___LayoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t905_0_0_0_var = il2cpp_codegen_type_from_index(599);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		GUILayoutGroup_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		_stringLiteral631 = il2cpp_codegen_string_literal_from_index(631);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t905_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_1 = ___LayoutType;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t764 * L_3 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_3, _stringLiteral631, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0020:
	{
		Type_t * L_4 = ___LayoutType;
		Object_t * L_5 = Activator_CreateInstance_m5150(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return ((GUILayoutGroup_t905 *)CastclassClass(L_5, GUILayoutGroup_t905_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility::BeginLayoutGroup(UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[],System.Type)
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t905_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t898_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral632;
extern "C" GUILayoutGroup_t905 * GUILayoutUtility_BeginLayoutGroup_m4742 (Object_t * __this /* static, unused */, GUIStyle_t342 * ___style, GUILayoutOptionU5BU5D_t441* ___options, Type_t * ___layoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUILayoutGroup_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		EventType_t898_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(596);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral632 = il2cpp_codegen_string_literal_from_index(632);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t905 * V_0 = {0};
	int32_t V_1 = {0};
	{
		Event_t381 * L_0 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m1405(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_004f;
	}

IL_001f:
	{
		Type_t * L_4 = ___layoutType;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		GUILayoutGroup_t905 * L_5 = GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m4741(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		GUILayoutGroup_t905 * L_6 = V_0;
		GUIStyle_t342 * L_7 = ___style;
		NullCheck(L_6);
		GUILayoutEntry_set_style_m4754(L_6, L_7, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t441* L_8 = ___options;
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		GUILayoutGroup_t905 * L_9 = V_0;
		GUILayoutOptionU5BU5D_t441* L_10 = ___options;
		NullCheck(L_9);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t441* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_9, L_10);
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_11 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GUILayoutGroup_t905 * L_12 = (L_11->___topLevel_0);
		GUILayoutGroup_t905 * L_13 = V_0;
		NullCheck(L_12);
		GUILayoutGroup_Add_m4769(L_12, L_13, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_14 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_14);
		GUILayoutGroup_t905 * L_15 = (L_14->___topLevel_0);
		NullCheck(L_15);
		GUILayoutEntry_t907 * L_16 = GUILayoutGroup_GetNext_m4768(L_15, /*hidden argument*/NULL);
		V_0 = ((GUILayoutGroup_t905 *)IsInstClass(L_16, GUILayoutGroup_t905_il2cpp_TypeInfo_var));
		GUILayoutGroup_t905 * L_17 = V_0;
		if (L_17)
		{
			goto IL_0089;
		}
	}
	{
		Event_t381 * L_18 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = Event_get_type_m1405(L_18, /*hidden argument*/NULL);
		int32_t L_20 = L_19;
		Object_t * L_21 = Box(EventType_t898_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m1410(NULL /*static, unused*/, _stringLiteral632, L_21, /*hidden argument*/NULL);
		ArgumentException_t764 * L_23 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_23, L_22, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_23);
	}

IL_0089:
	{
		GUILayoutGroup_t905 * L_24 = V_0;
		NullCheck(L_24);
		GUILayoutGroup_ResetCursor_m4767(L_24, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_0094:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_25 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_25);
		GenericStack_t902 * L_26 = (L_25->___layoutGroups_1);
		GUILayoutGroup_t905 * L_27 = V_0;
		NullCheck(L_26);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_26, L_27);
		LayoutCache_t904 * L_28 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		GUILayoutGroup_t905 * L_29 = V_0;
		NullCheck(L_28);
		L_28->___topLevel_0 = L_29;
		GUILayoutGroup_t905 * L_30 = V_0;
		return L_30;
	}
}
// System.Void UnityEngine.GUILayoutUtility::EndLayoutGroup()
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t905_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_EndLayoutGroup_m4743 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUILayoutGroup_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		Event_t381 * L_0 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m1405(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_2 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GenericStack_t902 * L_3 = (L_2->___layoutGroups_1);
		NullCheck(L_3);
		VirtFuncInvoker0< Object_t * >::Invoke(16 /* System.Object System.Collections.Stack::Pop() */, L_3);
		LayoutCache_t904 * L_4 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t904 * L_5 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GenericStack_t902 * L_6 = (L_5->___layoutGroups_1);
		NullCheck(L_6);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(15 /* System.Object System.Collections.Stack::Peek() */, L_6);
		NullCheck(L_4);
		L_4->___topLevel_0 = ((GUILayoutGroup_t905 *)CastclassClass(L_7, GUILayoutGroup_t905_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::GetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle)
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern "C" Rect_t267  GUILayoutUtility_GetRect_m1401 (Object_t * __this /* static, unused */, GUIContent_t379 * ___content, GUIStyle_t342 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t379 * L_0 = ___content;
		GUIStyle_t342 * L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		Rect_t267  L_2 = GUILayoutUtility_DoGetRect_m4745(NULL /*static, unused*/, L_0, L_1, (GUILayoutOptionU5BU5D_t441*)(GUILayoutOptionU5BU5D_t441*)NULL, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::GetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern "C" Rect_t267  GUILayoutUtility_GetRect_m4744 (Object_t * __this /* static, unused */, GUIContent_t379 * ___content, GUIStyle_t342 * ___style, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t379 * L_0 = ___content;
		GUIStyle_t342 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t441* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		Rect_t267  L_3 = GUILayoutUtility_DoGetRect_m4745(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::DoGetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// UnityEngine.GUIWordWrapSizer
#include "UnityEngine_UnityEngine_GUIWordWrapSizerMethodDeclarations.h"
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUIWordWrapSizer_t910_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t907_il2cpp_TypeInfo_var;
extern "C" Rect_t267  GUILayoutUtility_DoGetRect_m4745 (Object_t * __this /* static, unused */, GUIContent_t379 * ___content, GUIStyle_t342 * ___style, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUIWordWrapSizer_t910_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		GUILayoutEntry_t907_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(602);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2  V_0 = {0};
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m4904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Event_t381 * L_0 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m1405(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0091;
	}

IL_0024:
	{
		GUIStyle_t342 * L_4 = ___style;
		NullCheck(L_4);
		bool L_5 = GUIStyle_get_isHeightDependantOnWidth_m4874(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_6 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_6);
		GUILayoutGroup_t905 * L_7 = (L_6->___topLevel_0);
		GUIStyle_t342 * L_8 = ___style;
		GUIContent_t379 * L_9 = ___content;
		GUILayoutOptionU5BU5D_t441* L_10 = ___options;
		GUIWordWrapSizer_t910 * L_11 = (GUIWordWrapSizer_t910 *)il2cpp_codegen_object_new (GUIWordWrapSizer_t910_il2cpp_TypeInfo_var);
		GUIWordWrapSizer__ctor_m4780(L_11, L_8, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		GUILayoutGroup_Add_m4769(L_7, L_11, /*hidden argument*/NULL);
		goto IL_0085;
	}

IL_004b:
	{
		GUIStyle_t342 * L_12 = ___style;
		GUIContent_t379 * L_13 = ___content;
		NullCheck(L_12);
		Vector2_t2  L_14 = GUIStyle_CalcSize_m4872(L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_15 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_15);
		GUILayoutGroup_t905 * L_16 = (L_15->___topLevel_0);
		float L_17 = ((&V_0)->___x_1);
		float L_18 = ((&V_0)->___x_1);
		float L_19 = ((&V_0)->___y_2);
		float L_20 = ((&V_0)->___y_2);
		GUIStyle_t342 * L_21 = ___style;
		GUILayoutOptionU5BU5D_t441* L_22 = ___options;
		GUILayoutEntry_t907 * L_23 = (GUILayoutEntry_t907 *)il2cpp_codegen_object_new (GUILayoutEntry_t907_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m4751(L_23, L_17, L_18, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUILayoutGroup_Add_m4769(L_16, L_23, /*hidden argument*/NULL);
	}

IL_0085:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		Rect_t267  L_24 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_24;
	}

IL_008b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		Rect_t267  L_25 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_25;
	}

IL_0091:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		LayoutCache_t904 * L_26 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_26);
		GUILayoutGroup_t905 * L_27 = (L_26->___topLevel_0);
		NullCheck(L_27);
		GUILayoutEntry_t907 * L_28 = GUILayoutGroup_GetNext_m4768(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Rect_t267  L_29 = (L_28->___rect_4);
		return L_29;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUILayoutUtility::get_spaceStyle()
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t342 * GUILayoutUtility_get_spaceStyle_m4746 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_0 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t342 * L_1 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_2 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		NullCheck(L_2);
		GUIStyle_set_stretchWidth_m4888(L_2, 0, /*hidden argument*/NULL);
		GUIStyle_t342 * L_3 = ((GUILayoutUtility_t380_StaticFields*)GUILayoutUtility_t380_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		return L_3;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::Internal_GetWindowRect(System.Int32)
extern "C" Rect_t267  GUILayoutUtility_Internal_GetWindowRect_m4747 (Object_t * __this /* static, unused */, int32_t ___windowID, const MethodInfo* method)
{
	typedef Rect_t267  (*GUILayoutUtility_Internal_GetWindowRect_m4747_ftn) (int32_t);
	static GUILayoutUtility_Internal_GetWindowRect_m4747_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayoutUtility_Internal_GetWindowRect_m4747_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayoutUtility::Internal_GetWindowRect(System.Int32)");
	return _il2cpp_icall_func(___windowID);
}
// System.Void UnityEngine.GUILayoutUtility::Internal_MoveWindow(System.Int32,UnityEngine.Rect)
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Internal_MoveWindow_m4748 (Object_t * __this /* static, unused */, int32_t ___windowID, Rect_t267  ___r, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___windowID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m4749(NULL /*static, unused*/, L_0, (&___r), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)
extern "C" void GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m4749 (Object_t * __this /* static, unused */, int32_t ___windowID, Rect_t267 * ___r, const MethodInfo* method)
{
	typedef void (*GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m4749_ftn) (int32_t, Rect_t267 *);
	static GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m4749_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m4749_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)");
	_il2cpp_icall_func(___windowID, ___r);
}
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
// System.Void UnityEngine.GUILayoutEntry::.ctor(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntryMethodDeclarations.h"
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__ctor_m4750 (GUILayoutEntry_t907 * __this, float ____minWidth, float ____maxWidth, float ____minHeight, float ____maxHeight, GUIStyle_t342 * ____style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t267  L_0 = {0};
		Rect__ctor_m1775(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rect_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_1 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Style_7 = L_1;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		float L_2 = ____minWidth;
		__this->___minWidth_0 = L_2;
		float L_3 = ____maxWidth;
		__this->___maxWidth_1 = L_3;
		float L_4 = ____minHeight;
		__this->___minHeight_2 = L_4;
		float L_5 = ____maxHeight;
		__this->___maxHeight_3 = L_5;
		GUIStyle_t342 * L_6 = ____style;
		if (L_6)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_7 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		____style = L_7;
	}

IL_005b:
	{
		GUIStyle_t342 * L_8 = ____style;
		GUILayoutEntry_set_style_m4754(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::.ctor(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
#include "UnityEngine_ArrayTypes.h"
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__ctor_m4751 (GUILayoutEntry_t907 * __this, float ____minWidth, float ____maxWidth, float ____minHeight, float ____maxHeight, GUIStyle_t342 * ____style, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t267  L_0 = {0};
		Rect__ctor_m1775(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rect_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_1 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Style_7 = L_1;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		float L_2 = ____minWidth;
		__this->___minWidth_0 = L_2;
		float L_3 = ____maxWidth;
		__this->___maxWidth_1 = L_3;
		float L_4 = ____minHeight;
		__this->___minHeight_2 = L_4;
		float L_5 = ____maxHeight;
		__this->___maxHeight_3 = L_5;
		GUIStyle_t342 * L_6 = ____style;
		GUILayoutEntry_set_style_m4754(__this, L_6, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t441* L_7 = ___options;
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t441* >::Invoke(10 /* System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[]) */, __this, L_7);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::.cctor()
extern TypeInfo* GUILayoutEntry_t907_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__cctor_m4752 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutEntry_t907_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(602);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t267  L_0 = {0};
		Rect__ctor_m1775(&L_0, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((GUILayoutEntry_t907_StaticFields*)GUILayoutEntry_t907_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_8 = L_0;
		((GUILayoutEntry_t907_StaticFields*)GUILayoutEntry_t907_il2cpp_TypeInfo_var->static_fields)->___indent_9 = 0;
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUILayoutEntry::get_style()
extern "C" GUIStyle_t342 * GUILayoutEntry_get_style_m4753 (GUILayoutEntry_t907 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_Style_7);
		return L_0;
	}
}
// System.Void UnityEngine.GUILayoutEntry::set_style(UnityEngine.GUIStyle)
extern "C" void GUILayoutEntry_set_style_m4754 (GUILayoutEntry_t907 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_Style_7 = L_0;
		GUIStyle_t342 * L_1 = ___value;
		VirtActionInvoker1< GUIStyle_t342 * >::Invoke(9 /* System.Void UnityEngine.GUILayoutEntry::ApplyStyleSettings(UnityEngine.GUIStyle) */, __this, L_1);
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin()
extern "C" RectOffset_t666 * GUILayoutEntry_get_margin_m4755 (GUILayoutEntry_t907 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectOffset_t666 * L_1 = GUIStyle_get_margin_m4866(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GUILayoutEntry::CalcWidth()
extern "C" void GUILayoutEntry_CalcWidth_m4756 (GUILayoutEntry_t907 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::CalcHeight()
extern "C" void GUILayoutEntry_CalcHeight_m4757 (GUILayoutEntry_t907 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single)
extern "C" void GUILayoutEntry_SetHorizontal_m4758 (GUILayoutEntry_t907 * __this, float ___x, float ___width, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___rect_4);
		float L_1 = ___x;
		Rect_set_x_m1759(L_0, L_1, /*hidden argument*/NULL);
		Rect_t267 * L_2 = &(__this->___rect_4);
		float L_3 = ___width;
		Rect_set_width_m1763(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single)
extern "C" void GUILayoutEntry_SetVertical_m4759 (GUILayoutEntry_t907 * __this, float ___y, float ___height, const MethodInfo* method)
{
	{
		Rect_t267 * L_0 = &(__this->___rect_4);
		float L_1 = ___y;
		Rect_set_y_m1761(L_0, L_1, /*hidden argument*/NULL);
		Rect_t267 * L_2 = &(__this->___rect_4);
		float L_3 = ___height;
		Rect_set_height_m1765(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::ApplyStyleSettings(UnityEngine.GUIStyle)
extern "C" void GUILayoutEntry_ApplyStyleSettings_m4760 (GUILayoutEntry_t907 * __this, GUIStyle_t342 * ___style, const MethodInfo* method)
{
	GUILayoutEntry_t907 * G_B3_0 = {0};
	GUILayoutEntry_t907 * G_B1_0 = {0};
	GUILayoutEntry_t907 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	GUILayoutEntry_t907 * G_B4_1 = {0};
	GUILayoutEntry_t907 * G_B7_0 = {0};
	GUILayoutEntry_t907 * G_B5_0 = {0};
	GUILayoutEntry_t907 * G_B6_0 = {0};
	int32_t G_B8_0 = 0;
	GUILayoutEntry_t907 * G_B8_1 = {0};
	{
		GUIStyle_t342 * L_0 = ___style;
		NullCheck(L_0);
		float L_1 = GUIStyle_get_fixedWidth_m4885(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			G_B3_0 = __this;
			goto IL_0022;
		}
	}
	{
		GUIStyle_t342 * L_2 = ___style;
		NullCheck(L_2);
		bool L_3 = GUIStyle_get_stretchWidth_m4887(L_2, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
		if (!L_3)
		{
			G_B3_0 = G_B1_0;
			goto IL_0022;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0023:
	{
		NullCheck(G_B4_1);
		G_B4_1->___stretchWidth_5 = G_B4_0;
		GUIStyle_t342 * L_4 = ___style;
		NullCheck(L_4);
		float L_5 = GUIStyle_get_fixedHeight_m4886(L_4, /*hidden argument*/NULL);
		G_B5_0 = __this;
		if ((!(((float)L_5) == ((float)(0.0f)))))
		{
			G_B7_0 = __this;
			goto IL_004a;
		}
	}
	{
		GUIStyle_t342 * L_6 = ___style;
		NullCheck(L_6);
		bool L_7 = GUIStyle_get_stretchHeight_m4889(L_6, /*hidden argument*/NULL);
		G_B6_0 = G_B5_0;
		if (!L_7)
		{
			G_B7_0 = G_B5_0;
			goto IL_004a;
		}
	}
	{
		G_B8_0 = 1;
		G_B8_1 = G_B6_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B8_0 = 0;
		G_B8_1 = G_B7_0;
	}

IL_004b:
	{
		NullCheck(G_B8_1);
		G_B8_1->___stretchHeight_6 = G_B8_0;
		GUIStyle_t342 * L_8 = ___style;
		__this->___m_Style_7 = L_8;
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[])
extern TypeInfo* Single_t388_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry_ApplyOptions_m4761 (GUILayoutEntry_t907 * __this, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t912 * V_0 = {0};
	GUILayoutOptionU5BU5D_t441* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	float V_4 = 0.0f;
	{
		GUILayoutOptionU5BU5D_t441* L_0 = ___options;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		GUILayoutOptionU5BU5D_t441* L_1 = ___options;
		V_1 = L_1;
		V_2 = 0;
		goto IL_01a0;
	}

IL_0010:
	{
		GUILayoutOptionU5BU5D_t441* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(GUILayoutOption_t912 **)(GUILayoutOption_t912 **)SZArrayLdElema(L_2, L_4, sizeof(GUILayoutOption_t912 *)));
		GUILayoutOption_t912 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = (L_5->___type_0);
		V_3 = L_6;
		int32_t L_7 = V_3;
		if (L_7 == 0)
		{
			goto IL_0046;
		}
		if (L_7 == 1)
		{
			goto IL_006e;
		}
		if (L_7 == 2)
		{
			goto IL_0096;
		}
		if (L_7 == 3)
		{
			goto IL_00c9;
		}
		if (L_7 == 4)
		{
			goto IL_0103;
		}
		if (L_7 == 5)
		{
			goto IL_0136;
		}
		if (L_7 == 6)
		{
			goto IL_0170;
		}
		if (L_7 == 7)
		{
			goto IL_0186;
		}
	}
	{
		goto IL_019c;
	}

IL_0046:
	{
		GUILayoutOption_t912 * L_8 = V_0;
		NullCheck(L_8);
		Object_t * L_9 = (L_8->___value_1);
		float L_10 = ((*(float*)((float*)UnBox (L_9, Single_t388_il2cpp_TypeInfo_var))));
		V_4 = L_10;
		__this->___maxWidth_1 = L_10;
		float L_11 = V_4;
		__this->___minWidth_0 = L_11;
		__this->___stretchWidth_5 = 0;
		goto IL_019c;
	}

IL_006e:
	{
		GUILayoutOption_t912 * L_12 = V_0;
		NullCheck(L_12);
		Object_t * L_13 = (L_12->___value_1);
		float L_14 = ((*(float*)((float*)UnBox (L_13, Single_t388_il2cpp_TypeInfo_var))));
		V_4 = L_14;
		__this->___maxHeight_3 = L_14;
		float L_15 = V_4;
		__this->___minHeight_2 = L_15;
		__this->___stretchHeight_6 = 0;
		goto IL_019c;
	}

IL_0096:
	{
		GUILayoutOption_t912 * L_16 = V_0;
		NullCheck(L_16);
		Object_t * L_17 = (L_16->___value_1);
		__this->___minWidth_0 = ((*(float*)((float*)UnBox (L_17, Single_t388_il2cpp_TypeInfo_var))));
		float L_18 = (__this->___maxWidth_1);
		float L_19 = (__this->___minWidth_0);
		if ((!(((float)L_18) < ((float)L_19))))
		{
			goto IL_00c4;
		}
	}
	{
		float L_20 = (__this->___minWidth_0);
		__this->___maxWidth_1 = L_20;
	}

IL_00c4:
	{
		goto IL_019c;
	}

IL_00c9:
	{
		GUILayoutOption_t912 * L_21 = V_0;
		NullCheck(L_21);
		Object_t * L_22 = (L_21->___value_1);
		__this->___maxWidth_1 = ((*(float*)((float*)UnBox (L_22, Single_t388_il2cpp_TypeInfo_var))));
		float L_23 = (__this->___minWidth_0);
		float L_24 = (__this->___maxWidth_1);
		if ((!(((float)L_23) > ((float)L_24))))
		{
			goto IL_00f7;
		}
	}
	{
		float L_25 = (__this->___maxWidth_1);
		__this->___minWidth_0 = L_25;
	}

IL_00f7:
	{
		__this->___stretchWidth_5 = 0;
		goto IL_019c;
	}

IL_0103:
	{
		GUILayoutOption_t912 * L_26 = V_0;
		NullCheck(L_26);
		Object_t * L_27 = (L_26->___value_1);
		__this->___minHeight_2 = ((*(float*)((float*)UnBox (L_27, Single_t388_il2cpp_TypeInfo_var))));
		float L_28 = (__this->___maxHeight_3);
		float L_29 = (__this->___minHeight_2);
		if ((!(((float)L_28) < ((float)L_29))))
		{
			goto IL_0131;
		}
	}
	{
		float L_30 = (__this->___minHeight_2);
		__this->___maxHeight_3 = L_30;
	}

IL_0131:
	{
		goto IL_019c;
	}

IL_0136:
	{
		GUILayoutOption_t912 * L_31 = V_0;
		NullCheck(L_31);
		Object_t * L_32 = (L_31->___value_1);
		__this->___maxHeight_3 = ((*(float*)((float*)UnBox (L_32, Single_t388_il2cpp_TypeInfo_var))));
		float L_33 = (__this->___minHeight_2);
		float L_34 = (__this->___maxHeight_3);
		if ((!(((float)L_33) > ((float)L_34))))
		{
			goto IL_0164;
		}
	}
	{
		float L_35 = (__this->___maxHeight_3);
		__this->___minHeight_2 = L_35;
	}

IL_0164:
	{
		__this->___stretchHeight_6 = 0;
		goto IL_019c;
	}

IL_0170:
	{
		GUILayoutOption_t912 * L_36 = V_0;
		NullCheck(L_36);
		Object_t * L_37 = (L_36->___value_1);
		__this->___stretchWidth_5 = ((*(int32_t*)((int32_t*)UnBox (L_37, Int32_t372_il2cpp_TypeInfo_var))));
		goto IL_019c;
	}

IL_0186:
	{
		GUILayoutOption_t912 * L_38 = V_0;
		NullCheck(L_38);
		Object_t * L_39 = (L_38->___value_1);
		__this->___stretchHeight_6 = ((*(int32_t*)((int32_t*)UnBox (L_39, Int32_t372_il2cpp_TypeInfo_var))));
		goto IL_019c;
	}

IL_019c:
	{
		int32_t L_40 = V_2;
		V_2 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_01a0:
	{
		int32_t L_41 = V_2;
		GUILayoutOptionU5BU5D_t441* L_42 = V_1;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)(((Array_t *)L_42)->max_length))))))
		{
			goto IL_0010;
		}
	}
	{
		float L_43 = (__this->___maxWidth_1);
		if ((((float)L_43) == ((float)(0.0f))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_44 = (__this->___maxWidth_1);
		float L_45 = (__this->___minWidth_0);
		if ((!(((float)L_44) < ((float)L_45))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_46 = (__this->___minWidth_0);
		__this->___maxWidth_1 = L_46;
	}

IL_01d6:
	{
		float L_47 = (__this->___maxHeight_3);
		if ((((float)L_47) == ((float)(0.0f))))
		{
			goto IL_0203;
		}
	}
	{
		float L_48 = (__this->___maxHeight_3);
		float L_49 = (__this->___minHeight_2);
		if ((!(((float)L_48) < ((float)L_49))))
		{
			goto IL_0203;
		}
	}
	{
		float L_50 = (__this->___minHeight_2);
		__this->___maxHeight_3 = L_50;
	}

IL_0203:
	{
		return;
	}
}
// System.String UnityEngine.GUILayoutEntry::ToString()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t907_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t388_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral633;
extern Il2CppCodeGenString* _stringLiteral634;
extern Il2CppCodeGenString* _stringLiteral635;
extern Il2CppCodeGenString* _stringLiteral636;
extern Il2CppCodeGenString* _stringLiteral637;
extern Il2CppCodeGenString* _stringLiteral638;
extern "C" String_t* GUILayoutEntry_ToString_m4762 (GUILayoutEntry_t907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUILayoutEntry_t907_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(602);
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Single_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		_stringLiteral44 = il2cpp_codegen_string_literal_from_index(44);
		_stringLiteral633 = il2cpp_codegen_string_literal_from_index(633);
		_stringLiteral634 = il2cpp_codegen_string_literal_from_index(634);
		_stringLiteral635 = il2cpp_codegen_string_literal_from_index(635);
		_stringLiteral636 = il2cpp_codegen_string_literal_from_index(636);
		_stringLiteral637 = il2cpp_codegen_string_literal_from_index(637);
		_stringLiteral638 = il2cpp_codegen_string_literal_from_index(638);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t356* G_B5_1 = {0};
	ObjectU5BU5D_t356* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B5_4 = 0;
	ObjectU5BU5D_t356* G_B5_5 = {0};
	ObjectU5BU5D_t356* G_B5_6 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t356* G_B4_1 = {0};
	ObjectU5BU5D_t356* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	int32_t G_B4_4 = 0;
	ObjectU5BU5D_t356* G_B4_5 = {0};
	ObjectU5BU5D_t356* G_B4_6 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t356* G_B6_2 = {0};
	ObjectU5BU5D_t356* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	int32_t G_B6_5 = 0;
	ObjectU5BU5D_t356* G_B6_6 = {0};
	ObjectU5BU5D_t356* G_B6_7 = {0};
	int32_t G_B8_0 = 0;
	ObjectU5BU5D_t356* G_B8_1 = {0};
	ObjectU5BU5D_t356* G_B8_2 = {0};
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t356* G_B7_1 = {0};
	ObjectU5BU5D_t356* G_B7_2 = {0};
	String_t* G_B9_0 = {0};
	int32_t G_B9_1 = 0;
	ObjectU5BU5D_t356* G_B9_2 = {0};
	ObjectU5BU5D_t356* G_B9_3 = {0};
	int32_t G_B11_0 = 0;
	ObjectU5BU5D_t356* G_B11_1 = {0};
	ObjectU5BU5D_t356* G_B11_2 = {0};
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t356* G_B10_1 = {0};
	ObjectU5BU5D_t356* G_B10_2 = {0};
	String_t* G_B12_0 = {0};
	int32_t G_B12_1 = 0;
	ObjectU5BU5D_t356* G_B12_2 = {0};
	ObjectU5BU5D_t356* G_B12_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		goto IL_001d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1319(NULL /*static, unused*/, L_1, _stringLiteral44, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_1;
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t907_il2cpp_TypeInfo_var);
		int32_t L_5 = ((GUILayoutEntry_t907_StaticFields*)GUILayoutEntry_t907_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_000d;
		}
	}
	{
		ObjectU5BU5D_t356* L_6 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, ((int32_t)12)));
		String_t* L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 0, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t356* L_8 = L_6;
		ObjectU5BU5D_t356* L_9 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 6));
		GUIStyle_t342 * L_10 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		G_B4_0 = 0;
		G_B4_1 = L_9;
		G_B4_2 = L_9;
		G_B4_3 = _stringLiteral633;
		G_B4_4 = 1;
		G_B4_5 = L_8;
		G_B4_6 = L_8;
		if (!L_10)
		{
			G_B5_0 = 0;
			G_B5_1 = L_9;
			G_B5_2 = L_9;
			G_B5_3 = _stringLiteral633;
			G_B5_4 = 1;
			G_B5_5 = L_8;
			G_B5_6 = L_8;
			goto IL_005d;
		}
	}
	{
		GUIStyle_t342 * L_11 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = GUIStyle_get_name_m4879(L_11, /*hidden argument*/NULL);
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		G_B6_5 = G_B4_4;
		G_B6_6 = G_B4_5;
		G_B6_7 = G_B4_6;
		goto IL_0062;
	}

IL_005d:
	{
		G_B6_0 = _stringLiteral634;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
		G_B6_5 = G_B5_4;
		G_B6_6 = G_B5_5;
		G_B6_7 = G_B5_6;
	}

IL_0062:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)G_B6_0;
		ObjectU5BU5D_t356* L_13 = G_B6_3;
		Type_t * L_14 = Object_GetType_m1368(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 1, sizeof(Object_t *))) = (Object_t *)L_14;
		ObjectU5BU5D_t356* L_15 = L_13;
		Rect_t267 * L_16 = &(__this->___rect_4);
		float L_17 = Rect_get_x_m1758(L_16, /*hidden argument*/NULL);
		float L_18 = L_17;
		Object_t * L_19 = Box(Single_t388_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 2, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t356* L_20 = L_15;
		Rect_t267 * L_21 = &(__this->___rect_4);
		float L_22 = Rect_get_xMax_m1310(L_21, /*hidden argument*/NULL);
		float L_23 = L_22;
		Object_t * L_24 = Box(Single_t388_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 3);
		ArrayElementTypeCheck (L_20, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 3, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t356* L_25 = L_20;
		Rect_t267 * L_26 = &(__this->___rect_4);
		float L_27 = Rect_get_y_m1760(L_26, /*hidden argument*/NULL);
		float L_28 = L_27;
		Object_t * L_29 = Box(Single_t388_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 4, sizeof(Object_t *))) = (Object_t *)L_29;
		ObjectU5BU5D_t356* L_30 = L_25;
		Rect_t267 * L_31 = &(__this->___rect_4);
		float L_32 = Rect_get_yMax_m1311(L_31, /*hidden argument*/NULL);
		float L_33 = L_32;
		Object_t * L_34 = Box(Single_t388_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 5);
		ArrayElementTypeCheck (L_30, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 5, sizeof(Object_t *))) = (Object_t *)L_34;
		String_t* L_35 = UnityString_Format_m4240(NULL /*static, unused*/, G_B6_4, L_30, /*hidden argument*/NULL);
		NullCheck(G_B6_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_6, G_B6_5);
		ArrayElementTypeCheck (G_B6_6, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_6, G_B6_5, sizeof(Object_t *))) = (Object_t *)L_35;
		ObjectU5BU5D_t356* L_36 = G_B6_7;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 2);
		ArrayElementTypeCheck (L_36, _stringLiteral635);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral635;
		ObjectU5BU5D_t356* L_37 = L_36;
		float L_38 = (__this->___minWidth_0);
		float L_39 = L_38;
		Object_t * L_40 = Box(Single_t388_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 3);
		ArrayElementTypeCheck (L_37, L_40);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_37, 3, sizeof(Object_t *))) = (Object_t *)L_40;
		ObjectU5BU5D_t356* L_41 = L_37;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 4);
		ArrayElementTypeCheck (L_41, _stringLiteral636);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral636;
		ObjectU5BU5D_t356* L_42 = L_41;
		float L_43 = (__this->___maxWidth_1);
		float L_44 = L_43;
		Object_t * L_45 = Box(Single_t388_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 5);
		ArrayElementTypeCheck (L_42, L_45);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_42, 5, sizeof(Object_t *))) = (Object_t *)L_45;
		ObjectU5BU5D_t356* L_46 = L_42;
		int32_t L_47 = (__this->___stretchWidth_5);
		G_B7_0 = 6;
		G_B7_1 = L_46;
		G_B7_2 = L_46;
		if (!L_47)
		{
			G_B8_0 = 6;
			G_B8_1 = L_46;
			G_B8_2 = L_46;
			goto IL_0101;
		}
	}
	{
		G_B9_0 = _stringLiteral637;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_0106;
	}

IL_0101:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B9_0 = L_48;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_0106:
	{
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		ArrayElementTypeCheck (G_B9_2, G_B9_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B9_2, G_B9_1, sizeof(Object_t *))) = (Object_t *)G_B9_0;
		ObjectU5BU5D_t356* L_49 = G_B9_3;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, 7);
		ArrayElementTypeCheck (L_49, _stringLiteral638);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_49, 7, sizeof(Object_t *))) = (Object_t *)_stringLiteral638;
		ObjectU5BU5D_t356* L_50 = L_49;
		float L_51 = (__this->___minHeight_2);
		float L_52 = L_51;
		Object_t * L_53 = Box(Single_t388_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 8);
		ArrayElementTypeCheck (L_50, L_53);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_50, 8, sizeof(Object_t *))) = (Object_t *)L_53;
		ObjectU5BU5D_t356* L_54 = L_50;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)9));
		ArrayElementTypeCheck (L_54, _stringLiteral636);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_54, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)_stringLiteral636;
		ObjectU5BU5D_t356* L_55 = L_54;
		float L_56 = (__this->___maxHeight_3);
		float L_57 = L_56;
		Object_t * L_58 = Box(Single_t388_il2cpp_TypeInfo_var, &L_57);
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)10));
		ArrayElementTypeCheck (L_55, L_58);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_55, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_58;
		ObjectU5BU5D_t356* L_59 = L_55;
		int32_t L_60 = (__this->___stretchHeight_6);
		G_B10_0 = ((int32_t)11);
		G_B10_1 = L_59;
		G_B10_2 = L_59;
		if (!L_60)
		{
			G_B11_0 = ((int32_t)11);
			G_B11_1 = L_59;
			G_B11_2 = L_59;
			goto IL_014d;
		}
	}
	{
		G_B12_0 = _stringLiteral637;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_0152;
	}

IL_014d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_61 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B12_0 = L_61;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_0152:
	{
		NullCheck(G_B12_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B12_2, G_B12_1);
		ArrayElementTypeCheck (G_B12_2, G_B12_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B12_2, G_B12_1, sizeof(Object_t *))) = (Object_t *)G_B12_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_62 = String_Concat_m1316(NULL /*static, unused*/, G_B12_3, /*hidden argument*/NULL);
		return L_62;
	}
}
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// System.Void UnityEngine.GUILayoutGroup::.ctor()
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_gen_46MethodDeclarations.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntryMethodDeclarations.h"
extern TypeInfo* List_1_t908_il2cpp_TypeInfo_var;
extern TypeInfo* RectOffset_t666_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t907_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m5151_MethodInfo_var;
extern "C" void GUILayoutGroup__ctor_m4763 (GUILayoutGroup_t905 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t908_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(605);
		RectOffset_t666_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(520);
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		GUILayoutEntry_t907_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(602);
		List_1__ctor_m5151_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484233);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t908 * L_0 = (List_1_t908 *)il2cpp_codegen_object_new (List_1_t908_il2cpp_TypeInfo_var);
		List_1__ctor_m5151(L_0, /*hidden argument*/List_1__ctor_m5151_MethodInfo_var);
		__this->___entries_10 = L_0;
		__this->___isVertical_11 = 1;
		__this->___sameSize_14 = 1;
		__this->___windowID_16 = (-1);
		__this->___m_StretchableCountX_18 = ((int32_t)100);
		__this->___m_StretchableCountY_19 = ((int32_t)100);
		__this->___m_ChildMinWidth_22 = (100.0f);
		__this->___m_ChildMaxWidth_23 = (100.0f);
		__this->___m_ChildMinHeight_24 = (100.0f);
		__this->___m_ChildMaxHeight_25 = (100.0f);
		RectOffset_t666 * L_1 = (RectOffset_t666 *)il2cpp_codegen_object_new (RectOffset_t666_il2cpp_TypeInfo_var);
		RectOffset__ctor_m3764(L_1, /*hidden argument*/NULL);
		__this->___m_Margin_26 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_2 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t907_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m4750(__this, (0.0f), (0.0f), (0.0f), (0.0f), L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin()
extern "C" RectOffset_t666 * GUILayoutGroup_get_margin_m4764 (GUILayoutGroup_t905 * __this, const MethodInfo* method)
{
	{
		RectOffset_t666 * L_0 = (__this->___m_Margin_26);
		return L_0;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[])
#include "UnityEngine_ArrayTypes.h"
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern "C" void GUILayoutGroup_ApplyOptions_m4765 (GUILayoutGroup_t905 * __this, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t912 * V_0 = {0};
	GUILayoutOptionU5BU5D_t441* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	{
		GUILayoutOptionU5BU5D_t441* L_0 = ___options;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		GUILayoutOptionU5BU5D_t441* L_1 = ___options;
		GUILayoutEntry_ApplyOptions_m4761(__this, L_1, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t441* L_2 = ___options;
		V_1 = L_2;
		V_2 = 0;
		goto IL_0098;
	}

IL_0017:
	{
		GUILayoutOptionU5BU5D_t441* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_0 = (*(GUILayoutOption_t912 **)(GUILayoutOption_t912 **)SZArrayLdElema(L_3, L_5, sizeof(GUILayoutOption_t912 *)));
		GUILayoutOption_t912 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___type_0);
		V_3 = L_7;
		int32_t L_8 = V_3;
		if (L_8 == 0)
		{
			goto IL_0065;
		}
		if (L_8 == 1)
		{
			goto IL_0071;
		}
		if (L_8 == 2)
		{
			goto IL_0065;
		}
		if (L_8 == 3)
		{
			goto IL_0065;
		}
		if (L_8 == 4)
		{
			goto IL_0071;
		}
		if (L_8 == 5)
		{
			goto IL_0071;
		}
		if (L_8 == 6)
		{
			goto IL_0094;
		}
		if (L_8 == 7)
		{
			goto IL_0094;
		}
		if (L_8 == 8)
		{
			goto IL_0094;
		}
		if (L_8 == 9)
		{
			goto IL_0094;
		}
		if (L_8 == 10)
		{
			goto IL_0094;
		}
		if (L_8 == 11)
		{
			goto IL_0094;
		}
		if (L_8 == 12)
		{
			goto IL_0094;
		}
		if (L_8 == 13)
		{
			goto IL_007d;
		}
	}
	{
		goto IL_0094;
	}

IL_0065:
	{
		__this->___m_UserSpecifiedHeight_21 = 1;
		goto IL_0094;
	}

IL_0071:
	{
		__this->___m_UserSpecifiedWidth_20 = 1;
		goto IL_0094;
	}

IL_007d:
	{
		GUILayoutOption_t912 * L_9 = V_0;
		NullCheck(L_9);
		Object_t * L_10 = (L_9->___value_1);
		__this->___spacing_13 = (((float)((*(int32_t*)((int32_t*)UnBox (L_10, Int32_t372_il2cpp_TypeInfo_var))))));
		goto IL_0094;
	}

IL_0094:
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0098:
	{
		int32_t L_12 = V_2;
		GUILayoutOptionU5BU5D_t441* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)(((Array_t *)L_13)->max_length))))))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ApplyStyleSettings(UnityEngine.GUIStyle)
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
extern "C" void GUILayoutGroup_ApplyStyleSettings_m4766 (GUILayoutGroup_t905 * __this, GUIStyle_t342 * ___style, const MethodInfo* method)
{
	RectOffset_t666 * V_0 = {0};
	{
		GUIStyle_t342 * L_0 = ___style;
		GUILayoutEntry_ApplyStyleSettings_m4760(__this, L_0, /*hidden argument*/NULL);
		GUIStyle_t342 * L_1 = ___style;
		NullCheck(L_1);
		RectOffset_t666 * L_2 = GUIStyle_get_margin_m4866(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		RectOffset_t666 * L_3 = (__this->___m_Margin_26);
		RectOffset_t666 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_left_m3762(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectOffset_set_left_m4856(L_3, L_5, /*hidden argument*/NULL);
		RectOffset_t666 * L_6 = (__this->___m_Margin_26);
		RectOffset_t666 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = RectOffset_get_right_m4857(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectOffset_set_right_m4858(L_6, L_8, /*hidden argument*/NULL);
		RectOffset_t666 * L_9 = (__this->___m_Margin_26);
		RectOffset_t666 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_top_m3763(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		RectOffset_set_top_m4859(L_9, L_11, /*hidden argument*/NULL);
		RectOffset_t666 * L_12 = (__this->___m_Margin_26);
		RectOffset_t666 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_bottom_m4860(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		RectOffset_set_bottom_m4861(L_12, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ResetCursor()
extern "C" void GUILayoutGroup_ResetCursor_m4767 (GUILayoutGroup_t905 * __this, const MethodInfo* method)
{
	{
		__this->___m_Cursor_17 = 0;
		return;
	}
}
// UnityEngine.GUILayoutEntry UnityEngine.GUILayoutGroup::GetNext()
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t898_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral639;
extern Il2CppCodeGenString* _stringLiteral640;
extern Il2CppCodeGenString* _stringLiteral641;
extern Il2CppCodeGenString* _stringLiteral642;
extern "C" GUILayoutEntry_t907 * GUILayoutGroup_GetNext_m4768 (GUILayoutGroup_t905 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		EventType_t898_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(596);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral639 = il2cpp_codegen_string_literal_from_index(639);
		_stringLiteral640 = il2cpp_codegen_string_literal_from_index(640);
		_stringLiteral641 = il2cpp_codegen_string_literal_from_index(641);
		_stringLiteral642 = il2cpp_codegen_string_literal_from_index(642);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutEntry_t907 * V_0 = {0};
	{
		int32_t L_0 = (__this->___m_Cursor_17);
		List_1_t908 * L_1 = (__this->___entries_10);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_1);
		if ((((int32_t)L_0) >= ((int32_t)L_2)))
		{
			goto IL_0038;
		}
	}
	{
		List_1_t908 * L_3 = (__this->___entries_10);
		int32_t L_4 = (__this->___m_Cursor_17);
		NullCheck(L_3);
		GUILayoutEntry_t907 * L_5 = (GUILayoutEntry_t907 *)VirtFuncInvoker1< GUILayoutEntry_t907 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_3, L_4);
		V_0 = L_5;
		int32_t L_6 = (__this->___m_Cursor_17);
		__this->___m_Cursor_17 = ((int32_t)((int32_t)L_6+(int32_t)1));
		GUILayoutEntry_t907 * L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		ObjectU5BU5D_t356* L_8 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 7));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, _stringLiteral639);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral639;
		ObjectU5BU5D_t356* L_9 = L_8;
		int32_t L_10 = (__this->___m_Cursor_17);
		int32_t L_11 = L_10;
		Object_t * L_12 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		ArrayElementTypeCheck (L_9, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 1, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t356* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		ArrayElementTypeCheck (L_13, _stringLiteral640);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral640;
		ObjectU5BU5D_t356* L_14 = L_13;
		List_1_t908 * L_15 = (__this->___entries_10);
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_15);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 3, sizeof(Object_t *))) = (Object_t *)L_18;
		ObjectU5BU5D_t356* L_19 = L_14;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 4);
		ArrayElementTypeCheck (L_19, _stringLiteral641);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral641;
		ObjectU5BU5D_t356* L_20 = L_19;
		Event_t381 * L_21 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = Event_get_rawType_m3616(L_21, /*hidden argument*/NULL);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(EventType_t898_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t356* L_25 = L_20;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 6);
		ArrayElementTypeCheck (L_25, _stringLiteral642);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral642;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1316(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		ArgumentException_t764 * L_27 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_27, L_26, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_27);
	}
}
// System.Void UnityEngine.GUILayoutGroup::Add(UnityEngine.GUILayoutEntry)
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
extern "C" void GUILayoutGroup_Add_m4769 (GUILayoutGroup_t905 * __this, GUILayoutEntry_t907 * ___e, const MethodInfo* method)
{
	{
		List_1_t908 * L_0 = (__this->___entries_10);
		GUILayoutEntry_t907 * L_1 = ___e;
		NullCheck(L_0);
		VirtActionInvoker1< GUILayoutEntry_t907 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::CalcWidth()
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5MethodDeclarations.h"
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1023_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m5147_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m5148_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m5149_MethodInfo_var;
extern "C" void GUILayoutGroup_CalcWidth_m4770 (GUILayoutGroup_t905 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		Enumerator_t1023_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		List_1_GetEnumerator_m5147_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484230);
		Enumerator_get_Current_m5148_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484231);
		Enumerator_MoveNext_m5149_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	GUILayoutEntry_t907 * V_3 = {0};
	Enumerator_t1023  V_4 = {0};
	RectOffset_t666 * V_5 = {0};
	int32_t V_6 = 0;
	GUILayoutEntry_t907 * V_7 = {0};
	Enumerator_t1023  V_8 = {0};
	RectOffset_t666 * V_9 = {0};
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B22_0 = 0;
	int32_t G_B39_0 = 0;
	int32_t G_B39_1 = 0;
	GUILayoutGroup_t905 * G_B39_2 = {0};
	int32_t G_B38_0 = 0;
	int32_t G_B38_1 = 0;
	GUILayoutGroup_t905 * G_B38_2 = {0};
	int32_t G_B40_0 = 0;
	int32_t G_B40_1 = 0;
	int32_t G_B40_2 = 0;
	GUILayoutGroup_t905 * G_B40_3 = {0};
	{
		List_1_t908 * L_0 = (__this->___entries_10);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		GUIStyle_t342 * L_2 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectOffset_t666 * L_3 = GUIStyle_get_padding_m4867(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_horizontal_m3758(L_3, /*hidden argument*/NULL);
		float L_5 = (((float)L_4));
		V_13 = L_5;
		((GUILayoutEntry_t907 *)__this)->___minWidth_0 = L_5;
		float L_6 = V_13;
		((GUILayoutEntry_t907 *)__this)->___maxWidth_1 = L_6;
		return;
	}

IL_0033:
	{
		V_0 = 0;
		V_1 = 0;
		__this->___m_ChildMinWidth_22 = (0.0f);
		__this->___m_ChildMaxWidth_23 = (0.0f);
		__this->___m_StretchableCountX_18 = 0;
		V_2 = 1;
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_016a;
		}
	}
	{
		List_1_t908 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1023  L_9 = List_1_GetEnumerator_m5147(L_8, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_4 = L_9;
	}

IL_006e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0125;
		}

IL_0073:
		{
			GUILayoutEntry_t907 * L_10 = Enumerator_get_Current_m5148((&V_4), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_3 = L_10;
			GUILayoutEntry_t907 * L_11 = V_3;
			NullCheck(L_11);
			VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutEntry::CalcWidth() */, L_11);
			GUILayoutEntry_t907 * L_12 = V_3;
			NullCheck(L_12);
			RectOffset_t666 * L_13 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_12);
			V_5 = L_13;
			GUILayoutEntry_t907 * L_14 = V_3;
			NullCheck(L_14);
			GUIStyle_t342 * L_15 = GUILayoutEntry_get_style_m4753(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
			GUIStyle_t342 * L_16 = GUILayoutUtility_get_spaceStyle_m4746(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t342 *)L_15) == ((Object_t*)(GUIStyle_t342 *)L_16)))
			{
				goto IL_0112;
			}
		}

IL_0099:
		{
			bool L_17 = V_2;
			if (L_17)
			{
				goto IL_00c0;
			}
		}

IL_009f:
		{
			RectOffset_t666 * L_18 = V_5;
			NullCheck(L_18);
			int32_t L_19 = RectOffset_get_left_m3762(L_18, /*hidden argument*/NULL);
			int32_t L_20 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			int32_t L_21 = Mathf_Min_m1717(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
			V_0 = L_21;
			RectOffset_t666 * L_22 = V_5;
			NullCheck(L_22);
			int32_t L_23 = RectOffset_get_right_m4857(L_22, /*hidden argument*/NULL);
			int32_t L_24 = V_1;
			int32_t L_25 = Mathf_Min_m1717(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
			V_1 = L_25;
			goto IL_00d2;
		}

IL_00c0:
		{
			RectOffset_t666 * L_26 = V_5;
			NullCheck(L_26);
			int32_t L_27 = RectOffset_get_left_m3762(L_26, /*hidden argument*/NULL);
			V_0 = L_27;
			RectOffset_t666 * L_28 = V_5;
			NullCheck(L_28);
			int32_t L_29 = RectOffset_get_right_m4857(L_28, /*hidden argument*/NULL);
			V_1 = L_29;
			V_2 = 0;
		}

IL_00d2:
		{
			GUILayoutEntry_t907 * L_30 = V_3;
			NullCheck(L_30);
			float L_31 = (L_30->___minWidth_0);
			RectOffset_t666 * L_32 = V_5;
			NullCheck(L_32);
			int32_t L_33 = RectOffset_get_horizontal_m3758(L_32, /*hidden argument*/NULL);
			float L_34 = (__this->___m_ChildMinWidth_22);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			float L_35 = Mathf_Max_m3711(NULL /*static, unused*/, ((float)((float)L_31+(float)(((float)L_33)))), L_34, /*hidden argument*/NULL);
			__this->___m_ChildMinWidth_22 = L_35;
			GUILayoutEntry_t907 * L_36 = V_3;
			NullCheck(L_36);
			float L_37 = (L_36->___maxWidth_1);
			RectOffset_t666 * L_38 = V_5;
			NullCheck(L_38);
			int32_t L_39 = RectOffset_get_horizontal_m3758(L_38, /*hidden argument*/NULL);
			float L_40 = (__this->___m_ChildMaxWidth_23);
			float L_41 = Mathf_Max_m3711(NULL /*static, unused*/, ((float)((float)L_37+(float)(((float)L_39)))), L_40, /*hidden argument*/NULL);
			__this->___m_ChildMaxWidth_23 = L_41;
		}

IL_0112:
		{
			int32_t L_42 = (__this->___m_StretchableCountX_18);
			GUILayoutEntry_t907 * L_43 = V_3;
			NullCheck(L_43);
			int32_t L_44 = (L_43->___stretchWidth_5);
			__this->___m_StretchableCountX_18 = ((int32_t)((int32_t)L_42+(int32_t)L_44));
		}

IL_0125:
		{
			bool L_45 = Enumerator_MoveNext_m5149((&V_4), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_45)
			{
				goto IL_0073;
			}
		}

IL_0131:
		{
			IL2CPP_LEAVE(0x143, FINALLY_0136);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0136;
	}

FINALLY_0136:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_46 = V_4;
		Enumerator_t1023  L_47 = L_46;
		Object_t * L_48 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_48);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_48);
		IL2CPP_END_FINALLY(310)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(310)
	{
		IL2CPP_JUMP_TBL(0x143, IL_0143)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0143:
	{
		float L_49 = (__this->___m_ChildMinWidth_22);
		int32_t L_50 = V_0;
		int32_t L_51 = V_1;
		__this->___m_ChildMinWidth_22 = ((float)((float)L_49-(float)(((float)((int32_t)((int32_t)L_50+(int32_t)L_51))))));
		float L_52 = (__this->___m_ChildMaxWidth_23);
		int32_t L_53 = V_0;
		int32_t L_54 = V_1;
		__this->___m_ChildMaxWidth_23 = ((float)((float)L_52-(float)(((float)((int32_t)((int32_t)L_53+(int32_t)L_54))))));
		goto IL_02ea;
	}

IL_016a:
	{
		V_6 = 0;
		List_1_t908 * L_55 = (__this->___entries_10);
		NullCheck(L_55);
		Enumerator_t1023  L_56 = List_1_GetEnumerator_m5147(L_55, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_8 = L_56;
	}

IL_017a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0273;
		}

IL_017f:
		{
			GUILayoutEntry_t907 * L_57 = Enumerator_get_Current_m5148((&V_8), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_7 = L_57;
			GUILayoutEntry_t907 * L_58 = V_7;
			NullCheck(L_58);
			VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutEntry::CalcWidth() */, L_58);
			GUILayoutEntry_t907 * L_59 = V_7;
			NullCheck(L_59);
			RectOffset_t666 * L_60 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_59);
			V_9 = L_60;
			GUILayoutEntry_t907 * L_61 = V_7;
			NullCheck(L_61);
			GUIStyle_t342 * L_62 = GUILayoutEntry_get_style_m4753(L_61, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
			GUIStyle_t342 * L_63 = GUILayoutUtility_get_spaceStyle_m4746(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t342 *)L_62) == ((Object_t*)(GUIStyle_t342 *)L_63)))
			{
				goto IL_0237;
			}
		}

IL_01a9:
		{
			bool L_64 = V_2;
			if (L_64)
			{
				goto IL_01d2;
			}
		}

IL_01af:
		{
			int32_t L_65 = V_6;
			RectOffset_t666 * L_66 = V_9;
			NullCheck(L_66);
			int32_t L_67 = RectOffset_get_left_m3762(L_66, /*hidden argument*/NULL);
			if ((((int32_t)L_65) <= ((int32_t)L_67)))
			{
				goto IL_01c4;
			}
		}

IL_01bd:
		{
			int32_t L_68 = V_6;
			G_B22_0 = L_68;
			goto IL_01cb;
		}

IL_01c4:
		{
			RectOffset_t666 * L_69 = V_9;
			NullCheck(L_69);
			int32_t L_70 = RectOffset_get_left_m3762(L_69, /*hidden argument*/NULL);
			G_B22_0 = L_70;
		}

IL_01cb:
		{
			V_10 = G_B22_0;
			goto IL_01d7;
		}

IL_01d2:
		{
			V_10 = 0;
			V_2 = 0;
		}

IL_01d7:
		{
			float L_71 = (__this->___m_ChildMinWidth_22);
			GUILayoutEntry_t907 * L_72 = V_7;
			NullCheck(L_72);
			float L_73 = (L_72->___minWidth_0);
			float L_74 = (__this->___spacing_13);
			int32_t L_75 = V_10;
			__this->___m_ChildMinWidth_22 = ((float)((float)L_71+(float)((float)((float)((float)((float)L_73+(float)L_74))+(float)(((float)L_75))))));
			float L_76 = (__this->___m_ChildMaxWidth_23);
			GUILayoutEntry_t907 * L_77 = V_7;
			NullCheck(L_77);
			float L_78 = (L_77->___maxWidth_1);
			float L_79 = (__this->___spacing_13);
			int32_t L_80 = V_10;
			__this->___m_ChildMaxWidth_23 = ((float)((float)L_76+(float)((float)((float)((float)((float)L_78+(float)L_79))+(float)(((float)L_80))))));
			RectOffset_t666 * L_81 = V_9;
			NullCheck(L_81);
			int32_t L_82 = RectOffset_get_right_m4857(L_81, /*hidden argument*/NULL);
			V_6 = L_82;
			int32_t L_83 = (__this->___m_StretchableCountX_18);
			GUILayoutEntry_t907 * L_84 = V_7;
			NullCheck(L_84);
			int32_t L_85 = (L_84->___stretchWidth_5);
			__this->___m_StretchableCountX_18 = ((int32_t)((int32_t)L_83+(int32_t)L_85));
			goto IL_0273;
		}

IL_0237:
		{
			float L_86 = (__this->___m_ChildMinWidth_22);
			GUILayoutEntry_t907 * L_87 = V_7;
			NullCheck(L_87);
			float L_88 = (L_87->___minWidth_0);
			__this->___m_ChildMinWidth_22 = ((float)((float)L_86+(float)L_88));
			float L_89 = (__this->___m_ChildMaxWidth_23);
			GUILayoutEntry_t907 * L_90 = V_7;
			NullCheck(L_90);
			float L_91 = (L_90->___maxWidth_1);
			__this->___m_ChildMaxWidth_23 = ((float)((float)L_89+(float)L_91));
			int32_t L_92 = (__this->___m_StretchableCountX_18);
			GUILayoutEntry_t907 * L_93 = V_7;
			NullCheck(L_93);
			int32_t L_94 = (L_93->___stretchWidth_5);
			__this->___m_StretchableCountX_18 = ((int32_t)((int32_t)L_92+(int32_t)L_94));
		}

IL_0273:
		{
			bool L_95 = Enumerator_MoveNext_m5149((&V_8), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_95)
			{
				goto IL_017f;
			}
		}

IL_027f:
		{
			IL2CPP_LEAVE(0x291, FINALLY_0284);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0284;
	}

FINALLY_0284:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_96 = V_8;
		Enumerator_t1023  L_97 = L_96;
		Object_t * L_98 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_97);
		NullCheck(L_98);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_98);
		IL2CPP_END_FINALLY(644)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(644)
	{
		IL2CPP_JUMP_TBL(0x291, IL_0291)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0291:
	{
		float L_99 = (__this->___m_ChildMinWidth_22);
		float L_100 = (__this->___spacing_13);
		__this->___m_ChildMinWidth_22 = ((float)((float)L_99-(float)L_100));
		float L_101 = (__this->___m_ChildMaxWidth_23);
		float L_102 = (__this->___spacing_13);
		__this->___m_ChildMaxWidth_23 = ((float)((float)L_101-(float)L_102));
		List_1_t908 * L_103 = (__this->___entries_10);
		NullCheck(L_103);
		int32_t L_104 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_103);
		if (!L_104)
		{
			goto IL_02e6;
		}
	}
	{
		List_1_t908 * L_105 = (__this->___entries_10);
		NullCheck(L_105);
		GUILayoutEntry_t907 * L_106 = (GUILayoutEntry_t907 *)VirtFuncInvoker1< GUILayoutEntry_t907 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_105, 0);
		NullCheck(L_106);
		RectOffset_t666 * L_107 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_106);
		NullCheck(L_107);
		int32_t L_108 = RectOffset_get_left_m3762(L_107, /*hidden argument*/NULL);
		V_0 = L_108;
		int32_t L_109 = V_6;
		V_1 = L_109;
		goto IL_02ea;
	}

IL_02e6:
	{
		int32_t L_110 = 0;
		V_1 = L_110;
		V_0 = L_110;
	}

IL_02ea:
	{
		V_11 = (0.0f);
		V_12 = (0.0f);
		GUIStyle_t342 * L_111 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_112 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t342 *)L_111) == ((Object_t*)(GUIStyle_t342 *)L_112))))
		{
			goto IL_0313;
		}
	}
	{
		bool L_113 = (__this->___m_UserSpecifiedWidth_20);
		if (!L_113)
		{
			goto IL_034a;
		}
	}

IL_0313:
	{
		GUIStyle_t342 * L_114 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_114);
		RectOffset_t666 * L_115 = GUIStyle_get_padding_m4867(L_114, /*hidden argument*/NULL);
		NullCheck(L_115);
		int32_t L_116 = RectOffset_get_left_m3762(L_115, /*hidden argument*/NULL);
		int32_t L_117 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_118 = Mathf_Max_m3620(NULL /*static, unused*/, L_116, L_117, /*hidden argument*/NULL);
		V_11 = (((float)L_118));
		GUIStyle_t342 * L_119 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_119);
		RectOffset_t666 * L_120 = GUIStyle_get_padding_m4867(L_119, /*hidden argument*/NULL);
		NullCheck(L_120);
		int32_t L_121 = RectOffset_get_right_m4857(L_120, /*hidden argument*/NULL);
		int32_t L_122 = V_1;
		int32_t L_123 = Mathf_Max_m3620(NULL /*static, unused*/, L_121, L_122, /*hidden argument*/NULL);
		V_12 = (((float)L_123));
		goto IL_036c;
	}

IL_034a:
	{
		RectOffset_t666 * L_124 = (__this->___m_Margin_26);
		int32_t L_125 = V_0;
		NullCheck(L_124);
		RectOffset_set_left_m4856(L_124, L_125, /*hidden argument*/NULL);
		RectOffset_t666 * L_126 = (__this->___m_Margin_26);
		int32_t L_127 = V_1;
		NullCheck(L_126);
		RectOffset_set_right_m4858(L_126, L_127, /*hidden argument*/NULL);
		float L_128 = (0.0f);
		V_12 = L_128;
		V_11 = L_128;
	}

IL_036c:
	{
		float L_129 = (((GUILayoutEntry_t907 *)__this)->___minWidth_0);
		float L_130 = (__this->___m_ChildMinWidth_22);
		float L_131 = V_11;
		float L_132 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_133 = Mathf_Max_m3711(NULL /*static, unused*/, L_129, ((float)((float)((float)((float)L_130+(float)L_131))+(float)L_132)), /*hidden argument*/NULL);
		((GUILayoutEntry_t907 *)__this)->___minWidth_0 = L_133;
		float L_134 = (((GUILayoutEntry_t907 *)__this)->___maxWidth_1);
		if ((!(((float)L_134) == ((float)(0.0f)))))
		{
			goto IL_03db;
		}
	}
	{
		int32_t L_135 = (((GUILayoutEntry_t907 *)__this)->___stretchWidth_5);
		int32_t L_136 = (__this->___m_StretchableCountX_18);
		GUIStyle_t342 * L_137 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_137);
		bool L_138 = GUIStyle_get_stretchWidth_m4887(L_137, /*hidden argument*/NULL);
		G_B38_0 = L_136;
		G_B38_1 = L_135;
		G_B38_2 = __this;
		if (!L_138)
		{
			G_B39_0 = L_136;
			G_B39_1 = L_135;
			G_B39_2 = __this;
			goto IL_03bc;
		}
	}
	{
		G_B40_0 = 1;
		G_B40_1 = G_B38_0;
		G_B40_2 = G_B38_1;
		G_B40_3 = G_B38_2;
		goto IL_03bd;
	}

IL_03bc:
	{
		G_B40_0 = 0;
		G_B40_1 = G_B39_0;
		G_B40_2 = G_B39_1;
		G_B40_3 = G_B39_2;
	}

IL_03bd:
	{
		NullCheck(G_B40_3);
		((GUILayoutEntry_t907 *)G_B40_3)->___stretchWidth_5 = ((int32_t)((int32_t)G_B40_2+(int32_t)((int32_t)((int32_t)G_B40_1+(int32_t)G_B40_0))));
		float L_139 = (__this->___m_ChildMaxWidth_23);
		float L_140 = V_11;
		float L_141 = V_12;
		((GUILayoutEntry_t907 *)__this)->___maxWidth_1 = ((float)((float)((float)((float)L_139+(float)L_140))+(float)L_141));
		goto IL_03e2;
	}

IL_03db:
	{
		((GUILayoutEntry_t907 *)__this)->___stretchWidth_5 = 0;
	}

IL_03e2:
	{
		float L_142 = (((GUILayoutEntry_t907 *)__this)->___maxWidth_1);
		float L_143 = (((GUILayoutEntry_t907 *)__this)->___minWidth_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_144 = Mathf_Max_m3711(NULL /*static, unused*/, L_142, L_143, /*hidden argument*/NULL);
		((GUILayoutEntry_t907 *)__this)->___maxWidth_1 = L_144;
		GUIStyle_t342 * L_145 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_145);
		float L_146 = GUIStyle_get_fixedWidth_m4885(L_145, /*hidden argument*/NULL);
		if ((((float)L_146) == ((float)(0.0f))))
		{
			goto IL_0431;
		}
	}
	{
		GUIStyle_t342 * L_147 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_147);
		float L_148 = GUIStyle_get_fixedWidth_m4885(L_147, /*hidden argument*/NULL);
		float L_149 = L_148;
		V_13 = L_149;
		((GUILayoutEntry_t907 *)__this)->___minWidth_0 = L_149;
		float L_150 = V_13;
		((GUILayoutEntry_t907 *)__this)->___maxWidth_1 = L_150;
		((GUILayoutEntry_t907 *)__this)->___stretchWidth_5 = 0;
	}

IL_0431:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1023_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m5147_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m5148_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m5149_MethodInfo_var;
extern "C" void GUILayoutGroup_SetHorizontal_m4771 (GUILayoutGroup_t905 * __this, float ___x, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		Enumerator_t1023_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		List_1_GetEnumerator_m5147_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484230);
		Enumerator_get_Current_m5148_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484231);
		Enumerator_MoveNext_m5149_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		s_Il2CppMethodIntialized = true;
	}
	RectOffset_t666 * V_0 = {0};
	GUILayoutEntry_t907 * V_1 = {0};
	Enumerator_t1023  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	GUILayoutEntry_t907 * V_8 = {0};
	Enumerator_t1023  V_9 = {0};
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	int32_t V_15 = 0;
	bool V_16 = false;
	GUILayoutEntry_t907 * V_17 = {0};
	Enumerator_t1023  V_18 = {0};
	float V_19 = 0.0f;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B39_0 = 0;
	{
		float L_0 = ___x;
		float L_1 = ___width;
		GUILayoutEntry_SetHorizontal_m4758(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = (__this->___resetCoords_12);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		___x = (0.0f);
	}

IL_001a:
	{
		GUIStyle_t342 * L_3 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectOffset_t666 * L_4 = GUIStyle_get_padding_m4867(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = (__this->___isVertical_11);
		if (!L_5)
		{
			goto IL_01bb;
		}
	}
	{
		GUIStyle_t342 * L_6 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_7 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t342 *)L_6) == ((Object_t*)(GUIStyle_t342 *)L_7)))
		{
			goto IL_00eb;
		}
	}
	{
		List_1_t908 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1023  L_9 = List_1_GetEnumerator_m5147(L_8, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_2 = L_9;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c9;
		}

IL_0052:
		{
			GUILayoutEntry_t907 * L_10 = Enumerator_get_Current_m5148((&V_2), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_1 = L_10;
			GUILayoutEntry_t907 * L_11 = V_1;
			NullCheck(L_11);
			RectOffset_t666 * L_12 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_11);
			NullCheck(L_12);
			int32_t L_13 = RectOffset_get_left_m3762(L_12, /*hidden argument*/NULL);
			RectOffset_t666 * L_14 = V_0;
			NullCheck(L_14);
			int32_t L_15 = RectOffset_get_left_m3762(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			int32_t L_16 = Mathf_Max_m3620(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
			V_3 = (((float)L_16));
			float L_17 = ___x;
			float L_18 = V_3;
			V_4 = ((float)((float)L_17+(float)L_18));
			float L_19 = ___width;
			GUILayoutEntry_t907 * L_20 = V_1;
			NullCheck(L_20);
			RectOffset_t666 * L_21 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_20);
			NullCheck(L_21);
			int32_t L_22 = RectOffset_get_right_m4857(L_21, /*hidden argument*/NULL);
			RectOffset_t666 * L_23 = V_0;
			NullCheck(L_23);
			int32_t L_24 = RectOffset_get_right_m4857(L_23, /*hidden argument*/NULL);
			int32_t L_25 = Mathf_Max_m3620(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
			float L_26 = V_3;
			V_5 = ((float)((float)((float)((float)L_19-(float)(((float)L_25))))-(float)L_26));
			GUILayoutEntry_t907 * L_27 = V_1;
			NullCheck(L_27);
			int32_t L_28 = (L_27->___stretchWidth_5);
			if (!L_28)
			{
				goto IL_00ae;
			}
		}

IL_009f:
		{
			GUILayoutEntry_t907 * L_29 = V_1;
			float L_30 = V_4;
			float L_31 = V_5;
			NullCheck(L_29);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_29, L_30, L_31);
			goto IL_00c9;
		}

IL_00ae:
		{
			GUILayoutEntry_t907 * L_32 = V_1;
			float L_33 = V_4;
			float L_34 = V_5;
			GUILayoutEntry_t907 * L_35 = V_1;
			NullCheck(L_35);
			float L_36 = (L_35->___minWidth_0);
			GUILayoutEntry_t907 * L_37 = V_1;
			NullCheck(L_37);
			float L_38 = (L_37->___maxWidth_1);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			float L_39 = Mathf_Clamp_m1358(NULL /*static, unused*/, L_34, L_36, L_38, /*hidden argument*/NULL);
			NullCheck(L_32);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_32, L_33, L_39);
		}

IL_00c9:
		{
			bool L_40 = Enumerator_MoveNext_m5149((&V_2), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_40)
			{
				goto IL_0052;
			}
		}

IL_00d5:
		{
			IL2CPP_LEAVE(0xE6, FINALLY_00da);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_00da;
	}

FINALLY_00da:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_41 = V_2;
		Enumerator_t1023  L_42 = L_41;
		Object_t * L_43 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_43);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_43);
		IL2CPP_END_FINALLY(218)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(218)
	{
		IL2CPP_JUMP_TBL(0xE6, IL_00e6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_00e6:
	{
		goto IL_01b6;
	}

IL_00eb:
	{
		float L_44 = ___x;
		RectOffset_t666 * L_45 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_45);
		int32_t L_46 = RectOffset_get_left_m3762(L_45, /*hidden argument*/NULL);
		V_6 = ((float)((float)L_44-(float)(((float)L_46))));
		float L_47 = ___width;
		RectOffset_t666 * L_48 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_48);
		int32_t L_49 = RectOffset_get_horizontal_m3758(L_48, /*hidden argument*/NULL);
		V_7 = ((float)((float)L_47+(float)(((float)L_49))));
		List_1_t908 * L_50 = (__this->___entries_10);
		NullCheck(L_50);
		Enumerator_t1023  L_51 = List_1_GetEnumerator_m5147(L_50, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_9 = L_51;
	}

IL_0118:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0198;
		}

IL_011d:
		{
			GUILayoutEntry_t907 * L_52 = Enumerator_get_Current_m5148((&V_9), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_8 = L_52;
			GUILayoutEntry_t907 * L_53 = V_8;
			NullCheck(L_53);
			int32_t L_54 = (L_53->___stretchWidth_5);
			if (!L_54)
			{
				goto IL_015e;
			}
		}

IL_0132:
		{
			GUILayoutEntry_t907 * L_55 = V_8;
			float L_56 = V_6;
			GUILayoutEntry_t907 * L_57 = V_8;
			NullCheck(L_57);
			RectOffset_t666 * L_58 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_57);
			NullCheck(L_58);
			int32_t L_59 = RectOffset_get_left_m3762(L_58, /*hidden argument*/NULL);
			float L_60 = V_7;
			GUILayoutEntry_t907 * L_61 = V_8;
			NullCheck(L_61);
			RectOffset_t666 * L_62 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_61);
			NullCheck(L_62);
			int32_t L_63 = RectOffset_get_horizontal_m3758(L_62, /*hidden argument*/NULL);
			NullCheck(L_55);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_55, ((float)((float)L_56+(float)(((float)L_59)))), ((float)((float)L_60-(float)(((float)L_63)))));
			goto IL_0198;
		}

IL_015e:
		{
			GUILayoutEntry_t907 * L_64 = V_8;
			float L_65 = V_6;
			GUILayoutEntry_t907 * L_66 = V_8;
			NullCheck(L_66);
			RectOffset_t666 * L_67 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_66);
			NullCheck(L_67);
			int32_t L_68 = RectOffset_get_left_m3762(L_67, /*hidden argument*/NULL);
			float L_69 = V_7;
			GUILayoutEntry_t907 * L_70 = V_8;
			NullCheck(L_70);
			RectOffset_t666 * L_71 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_70);
			NullCheck(L_71);
			int32_t L_72 = RectOffset_get_horizontal_m3758(L_71, /*hidden argument*/NULL);
			GUILayoutEntry_t907 * L_73 = V_8;
			NullCheck(L_73);
			float L_74 = (L_73->___minWidth_0);
			GUILayoutEntry_t907 * L_75 = V_8;
			NullCheck(L_75);
			float L_76 = (L_75->___maxWidth_1);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			float L_77 = Mathf_Clamp_m1358(NULL /*static, unused*/, ((float)((float)L_69-(float)(((float)L_72)))), L_74, L_76, /*hidden argument*/NULL);
			NullCheck(L_64);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_64, ((float)((float)L_65+(float)(((float)L_68)))), L_77);
		}

IL_0198:
		{
			bool L_78 = Enumerator_MoveNext_m5149((&V_9), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_78)
			{
				goto IL_011d;
			}
		}

IL_01a4:
		{
			IL2CPP_LEAVE(0x1B6, FINALLY_01a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_01a9;
	}

FINALLY_01a9:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_79 = V_9;
		Enumerator_t1023  L_80 = L_79;
		Object_t * L_81 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_80);
		NullCheck(L_81);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_81);
		IL2CPP_END_FINALLY(425)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(425)
	{
		IL2CPP_JUMP_TBL(0x1B6, IL_01b6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_01b6:
	{
		goto IL_03b0;
	}

IL_01bb:
	{
		GUIStyle_t342 * L_82 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_83 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t342 *)L_82) == ((Object_t*)(GUIStyle_t342 *)L_83)))
		{
			goto IL_0248;
		}
	}
	{
		RectOffset_t666 * L_84 = V_0;
		NullCheck(L_84);
		int32_t L_85 = RectOffset_get_left_m3762(L_84, /*hidden argument*/NULL);
		V_10 = (((float)L_85));
		RectOffset_t666 * L_86 = V_0;
		NullCheck(L_86);
		int32_t L_87 = RectOffset_get_right_m4857(L_86, /*hidden argument*/NULL);
		V_11 = (((float)L_87));
		List_1_t908 * L_88 = (__this->___entries_10);
		NullCheck(L_88);
		int32_t L_89 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_88);
		if (!L_89)
		{
			goto IL_0239;
		}
	}
	{
		float L_90 = V_10;
		List_1_t908 * L_91 = (__this->___entries_10);
		NullCheck(L_91);
		GUILayoutEntry_t907 * L_92 = (GUILayoutEntry_t907 *)VirtFuncInvoker1< GUILayoutEntry_t907 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_91, 0);
		NullCheck(L_92);
		RectOffset_t666 * L_93 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_92);
		NullCheck(L_93);
		int32_t L_94 = RectOffset_get_left_m3762(L_93, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_95 = Mathf_Max_m3711(NULL /*static, unused*/, L_90, (((float)L_94)), /*hidden argument*/NULL);
		V_10 = L_95;
		float L_96 = V_11;
		List_1_t908 * L_97 = (__this->___entries_10);
		List_1_t908 * L_98 = (__this->___entries_10);
		NullCheck(L_98);
		int32_t L_99 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_98);
		NullCheck(L_97);
		GUILayoutEntry_t907 * L_100 = (GUILayoutEntry_t907 *)VirtFuncInvoker1< GUILayoutEntry_t907 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_97, ((int32_t)((int32_t)L_99-(int32_t)1)));
		NullCheck(L_100);
		RectOffset_t666 * L_101 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_100);
		NullCheck(L_101);
		int32_t L_102 = RectOffset_get_right_m4857(L_101, /*hidden argument*/NULL);
		float L_103 = Mathf_Max_m3711(NULL /*static, unused*/, L_96, (((float)L_102)), /*hidden argument*/NULL);
		V_11 = L_103;
	}

IL_0239:
	{
		float L_104 = ___x;
		float L_105 = V_10;
		___x = ((float)((float)L_104+(float)L_105));
		float L_106 = ___width;
		float L_107 = V_11;
		float L_108 = V_10;
		___width = ((float)((float)L_106-(float)((float)((float)L_107+(float)L_108))));
	}

IL_0248:
	{
		float L_109 = ___width;
		float L_110 = (__this->___spacing_13);
		List_1_t908 * L_111 = (__this->___entries_10);
		NullCheck(L_111);
		int32_t L_112 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_111);
		V_12 = ((float)((float)L_109-(float)((float)((float)L_110*(float)(((float)((int32_t)((int32_t)L_112-(int32_t)1))))))));
		V_13 = (0.0f);
		float L_113 = (__this->___m_ChildMinWidth_22);
		float L_114 = (__this->___m_ChildMaxWidth_23);
		if ((((float)L_113) == ((float)L_114)))
		{
			goto IL_02a1;
		}
	}
	{
		float L_115 = V_12;
		float L_116 = (__this->___m_ChildMinWidth_22);
		float L_117 = (__this->___m_ChildMaxWidth_23);
		float L_118 = (__this->___m_ChildMinWidth_22);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_119 = Mathf_Clamp_m1358(NULL /*static, unused*/, ((float)((float)((float)((float)L_115-(float)L_116))/(float)((float)((float)L_117-(float)L_118)))), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_13 = L_119;
	}

IL_02a1:
	{
		V_14 = (0.0f);
		float L_120 = V_12;
		float L_121 = (__this->___m_ChildMaxWidth_23);
		if ((!(((float)L_120) > ((float)L_121))))
		{
			goto IL_02d4;
		}
	}
	{
		int32_t L_122 = (__this->___m_StretchableCountX_18);
		if ((((int32_t)L_122) <= ((int32_t)0)))
		{
			goto IL_02d4;
		}
	}
	{
		float L_123 = V_12;
		float L_124 = (__this->___m_ChildMaxWidth_23);
		int32_t L_125 = (__this->___m_StretchableCountX_18);
		V_14 = ((float)((float)((float)((float)L_123-(float)L_124))/(float)(((float)L_125))));
	}

IL_02d4:
	{
		V_15 = 0;
		V_16 = 1;
		List_1_t908 * L_126 = (__this->___entries_10);
		NullCheck(L_126);
		Enumerator_t1023  L_127 = List_1_GetEnumerator_m5147(L_126, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_18 = L_127;
	}

IL_02e7:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0392;
		}

IL_02ec:
		{
			GUILayoutEntry_t907 * L_128 = Enumerator_get_Current_m5148((&V_18), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_17 = L_128;
			GUILayoutEntry_t907 * L_129 = V_17;
			NullCheck(L_129);
			float L_130 = (L_129->___minWidth_0);
			GUILayoutEntry_t907 * L_131 = V_17;
			NullCheck(L_131);
			float L_132 = (L_131->___maxWidth_1);
			float L_133 = V_13;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			float L_134 = Mathf_Lerp_m1395(NULL /*static, unused*/, L_130, L_132, L_133, /*hidden argument*/NULL);
			V_19 = L_134;
			float L_135 = V_19;
			float L_136 = V_14;
			GUILayoutEntry_t907 * L_137 = V_17;
			NullCheck(L_137);
			int32_t L_138 = (L_137->___stretchWidth_5);
			V_19 = ((float)((float)L_135+(float)((float)((float)L_136*(float)(((float)L_138))))));
			GUILayoutEntry_t907 * L_139 = V_17;
			NullCheck(L_139);
			GUIStyle_t342 * L_140 = GUILayoutEntry_get_style_m4753(L_139, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
			GUIStyle_t342 * L_141 = GUILayoutUtility_get_spaceStyle_m4746(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t342 *)L_140) == ((Object_t*)(GUIStyle_t342 *)L_141)))
			{
				goto IL_0371;
			}
		}

IL_032d:
		{
			GUILayoutEntry_t907 * L_142 = V_17;
			NullCheck(L_142);
			RectOffset_t666 * L_143 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_142);
			NullCheck(L_143);
			int32_t L_144 = RectOffset_get_left_m3762(L_143, /*hidden argument*/NULL);
			V_20 = L_144;
			bool L_145 = V_16;
			if (!L_145)
			{
				goto IL_0348;
			}
		}

IL_0342:
		{
			V_20 = 0;
			V_16 = 0;
		}

IL_0348:
		{
			int32_t L_146 = V_15;
			int32_t L_147 = V_20;
			if ((((int32_t)L_146) <= ((int32_t)L_147)))
			{
				goto IL_0358;
			}
		}

IL_0351:
		{
			int32_t L_148 = V_15;
			G_B39_0 = L_148;
			goto IL_035a;
		}

IL_0358:
		{
			int32_t L_149 = V_20;
			G_B39_0 = L_149;
		}

IL_035a:
		{
			V_21 = G_B39_0;
			float L_150 = ___x;
			int32_t L_151 = V_21;
			___x = ((float)((float)L_150+(float)(((float)L_151))));
			GUILayoutEntry_t907 * L_152 = V_17;
			NullCheck(L_152);
			RectOffset_t666 * L_153 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_152);
			NullCheck(L_153);
			int32_t L_154 = RectOffset_get_right_m4857(L_153, /*hidden argument*/NULL);
			V_15 = L_154;
		}

IL_0371:
		{
			GUILayoutEntry_t907 * L_155 = V_17;
			float L_156 = ___x;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			float L_157 = roundf(L_156);
			float L_158 = V_19;
			float L_159 = roundf(L_158);
			NullCheck(L_155);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_155, L_157, L_159);
			float L_160 = ___x;
			float L_161 = V_19;
			float L_162 = (__this->___spacing_13);
			___x = ((float)((float)L_160+(float)((float)((float)L_161+(float)L_162))));
		}

IL_0392:
		{
			bool L_163 = Enumerator_MoveNext_m5149((&V_18), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_163)
			{
				goto IL_02ec;
			}
		}

IL_039e:
		{
			IL2CPP_LEAVE(0x3B0, FINALLY_03a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_03a3;
	}

FINALLY_03a3:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_164 = V_18;
		Enumerator_t1023  L_165 = L_164;
		Object_t * L_166 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_165);
		NullCheck(L_166);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_166);
		IL2CPP_END_FINALLY(931)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(931)
	{
		IL2CPP_JUMP_TBL(0x3B0, IL_03b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_03b0:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::CalcHeight()
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1023_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m5147_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m5148_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m5149_MethodInfo_var;
extern "C" void GUILayoutGroup_CalcHeight_m4772 (GUILayoutGroup_t905 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		Enumerator_t1023_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		List_1_GetEnumerator_m5147_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484230);
		Enumerator_get_Current_m5148_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484231);
		Enumerator_MoveNext_m5149_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	GUILayoutEntry_t907 * V_4 = {0};
	Enumerator_t1023  V_5 = {0};
	RectOffset_t666 * V_6 = {0};
	int32_t V_7 = 0;
	bool V_8 = false;
	GUILayoutEntry_t907 * V_9 = {0};
	Enumerator_t1023  V_10 = {0};
	RectOffset_t666 * V_11 = {0};
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B36_0 = 0;
	int32_t G_B36_1 = 0;
	GUILayoutGroup_t905 * G_B36_2 = {0};
	int32_t G_B35_0 = 0;
	int32_t G_B35_1 = 0;
	GUILayoutGroup_t905 * G_B35_2 = {0};
	int32_t G_B37_0 = 0;
	int32_t G_B37_1 = 0;
	int32_t G_B37_2 = 0;
	GUILayoutGroup_t905 * G_B37_3 = {0};
	{
		List_1_t908 * L_0 = (__this->___entries_10);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		GUIStyle_t342 * L_2 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectOffset_t666 * L_3 = GUIStyle_get_padding_m4867(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_vertical_m3759(L_3, /*hidden argument*/NULL);
		float L_5 = (((float)L_4));
		V_14 = L_5;
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = L_5;
		float L_6 = V_14;
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = L_6;
		return;
	}

IL_0033:
	{
		V_0 = 0;
		V_1 = 0;
		__this->___m_ChildMinHeight_24 = (0.0f);
		__this->___m_ChildMaxHeight_25 = (0.0f);
		__this->___m_StretchableCountY_19 = 0;
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_01d4;
		}
	}
	{
		V_2 = 0;
		V_3 = 1;
		List_1_t908 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1023  L_9 = List_1_GetEnumerator_m5147(L_8, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_5 = L_9;
	}

IL_0070:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0159;
		}

IL_0075:
		{
			GUILayoutEntry_t907 * L_10 = Enumerator_get_Current_m5148((&V_5), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_4 = L_10;
			GUILayoutEntry_t907 * L_11 = V_4;
			NullCheck(L_11);
			VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutEntry::CalcHeight() */, L_11);
			GUILayoutEntry_t907 * L_12 = V_4;
			NullCheck(L_12);
			RectOffset_t666 * L_13 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_12);
			V_6 = L_13;
			GUILayoutEntry_t907 * L_14 = V_4;
			NullCheck(L_14);
			GUIStyle_t342 * L_15 = GUILayoutEntry_get_style_m4753(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
			GUIStyle_t342 * L_16 = GUILayoutUtility_get_spaceStyle_m4746(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t342 *)L_15) == ((Object_t*)(GUIStyle_t342 *)L_16)))
			{
				goto IL_011d;
			}
		}

IL_009f:
		{
			bool L_17 = V_3;
			if (L_17)
			{
				goto IL_00b9;
			}
		}

IL_00a5:
		{
			int32_t L_18 = V_2;
			RectOffset_t666 * L_19 = V_6;
			NullCheck(L_19);
			int32_t L_20 = RectOffset_get_top_m3763(L_19, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			int32_t L_21 = Mathf_Max_m3620(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
			V_7 = L_21;
			goto IL_00be;
		}

IL_00b9:
		{
			V_7 = 0;
			V_3 = 0;
		}

IL_00be:
		{
			float L_22 = (__this->___m_ChildMinHeight_24);
			GUILayoutEntry_t907 * L_23 = V_4;
			NullCheck(L_23);
			float L_24 = (L_23->___minHeight_2);
			float L_25 = (__this->___spacing_13);
			int32_t L_26 = V_7;
			__this->___m_ChildMinHeight_24 = ((float)((float)L_22+(float)((float)((float)((float)((float)L_24+(float)L_25))+(float)(((float)L_26))))));
			float L_27 = (__this->___m_ChildMaxHeight_25);
			GUILayoutEntry_t907 * L_28 = V_4;
			NullCheck(L_28);
			float L_29 = (L_28->___maxHeight_3);
			float L_30 = (__this->___spacing_13);
			int32_t L_31 = V_7;
			__this->___m_ChildMaxHeight_25 = ((float)((float)L_27+(float)((float)((float)((float)((float)L_29+(float)L_30))+(float)(((float)L_31))))));
			RectOffset_t666 * L_32 = V_6;
			NullCheck(L_32);
			int32_t L_33 = RectOffset_get_bottom_m4860(L_32, /*hidden argument*/NULL);
			V_2 = L_33;
			int32_t L_34 = (__this->___m_StretchableCountY_19);
			GUILayoutEntry_t907 * L_35 = V_4;
			NullCheck(L_35);
			int32_t L_36 = (L_35->___stretchHeight_6);
			__this->___m_StretchableCountY_19 = ((int32_t)((int32_t)L_34+(int32_t)L_36));
			goto IL_0159;
		}

IL_011d:
		{
			float L_37 = (__this->___m_ChildMinHeight_24);
			GUILayoutEntry_t907 * L_38 = V_4;
			NullCheck(L_38);
			float L_39 = (L_38->___minHeight_2);
			__this->___m_ChildMinHeight_24 = ((float)((float)L_37+(float)L_39));
			float L_40 = (__this->___m_ChildMaxHeight_25);
			GUILayoutEntry_t907 * L_41 = V_4;
			NullCheck(L_41);
			float L_42 = (L_41->___maxHeight_3);
			__this->___m_ChildMaxHeight_25 = ((float)((float)L_40+(float)L_42));
			int32_t L_43 = (__this->___m_StretchableCountY_19);
			GUILayoutEntry_t907 * L_44 = V_4;
			NullCheck(L_44);
			int32_t L_45 = (L_44->___stretchHeight_6);
			__this->___m_StretchableCountY_19 = ((int32_t)((int32_t)L_43+(int32_t)L_45));
		}

IL_0159:
		{
			bool L_46 = Enumerator_MoveNext_m5149((&V_5), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_46)
			{
				goto IL_0075;
			}
		}

IL_0165:
		{
			IL2CPP_LEAVE(0x177, FINALLY_016a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_016a;
	}

FINALLY_016a:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_47 = V_5;
		Enumerator_t1023  L_48 = L_47;
		Object_t * L_49 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_49);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_49);
		IL2CPP_END_FINALLY(362)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(362)
	{
		IL2CPP_JUMP_TBL(0x177, IL_0177)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0177:
	{
		float L_50 = (__this->___m_ChildMinHeight_24);
		float L_51 = (__this->___spacing_13);
		__this->___m_ChildMinHeight_24 = ((float)((float)L_50-(float)L_51));
		float L_52 = (__this->___m_ChildMaxHeight_25);
		float L_53 = (__this->___spacing_13);
		__this->___m_ChildMaxHeight_25 = ((float)((float)L_52-(float)L_53));
		List_1_t908 * L_54 = (__this->___entries_10);
		NullCheck(L_54);
		int32_t L_55 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_54);
		if (!L_55)
		{
			goto IL_01cb;
		}
	}
	{
		List_1_t908 * L_56 = (__this->___entries_10);
		NullCheck(L_56);
		GUILayoutEntry_t907 * L_57 = (GUILayoutEntry_t907 *)VirtFuncInvoker1< GUILayoutEntry_t907 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_56, 0);
		NullCheck(L_57);
		RectOffset_t666 * L_58 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_57);
		NullCheck(L_58);
		int32_t L_59 = RectOffset_get_top_m3763(L_58, /*hidden argument*/NULL);
		V_0 = L_59;
		int32_t L_60 = V_2;
		V_1 = L_60;
		goto IL_01cf;
	}

IL_01cb:
	{
		int32_t L_61 = 0;
		V_0 = L_61;
		V_1 = L_61;
	}

IL_01cf:
	{
		goto IL_02b0;
	}

IL_01d4:
	{
		V_8 = 1;
		List_1_t908 * L_62 = (__this->___entries_10);
		NullCheck(L_62);
		Enumerator_t1023  L_63 = List_1_GetEnumerator_m5147(L_62, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_10 = L_63;
	}

IL_01e4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0292;
		}

IL_01e9:
		{
			GUILayoutEntry_t907 * L_64 = Enumerator_get_Current_m5148((&V_10), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_9 = L_64;
			GUILayoutEntry_t907 * L_65 = V_9;
			NullCheck(L_65);
			VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutEntry::CalcHeight() */, L_65);
			GUILayoutEntry_t907 * L_66 = V_9;
			NullCheck(L_66);
			RectOffset_t666 * L_67 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_66);
			V_11 = L_67;
			GUILayoutEntry_t907 * L_68 = V_9;
			NullCheck(L_68);
			GUIStyle_t342 * L_69 = GUILayoutEntry_get_style_m4753(L_68, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
			GUIStyle_t342 * L_70 = GUILayoutUtility_get_spaceStyle_m4746(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t342 *)L_69) == ((Object_t*)(GUIStyle_t342 *)L_70)))
			{
				goto IL_027e;
			}
		}

IL_0213:
		{
			bool L_71 = V_8;
			if (L_71)
			{
				goto IL_023b;
			}
		}

IL_021a:
		{
			RectOffset_t666 * L_72 = V_11;
			NullCheck(L_72);
			int32_t L_73 = RectOffset_get_top_m3763(L_72, /*hidden argument*/NULL);
			int32_t L_74 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			int32_t L_75 = Mathf_Min_m1717(NULL /*static, unused*/, L_73, L_74, /*hidden argument*/NULL);
			V_0 = L_75;
			RectOffset_t666 * L_76 = V_11;
			NullCheck(L_76);
			int32_t L_77 = RectOffset_get_bottom_m4860(L_76, /*hidden argument*/NULL);
			int32_t L_78 = V_1;
			int32_t L_79 = Mathf_Min_m1717(NULL /*static, unused*/, L_77, L_78, /*hidden argument*/NULL);
			V_1 = L_79;
			goto IL_024e;
		}

IL_023b:
		{
			RectOffset_t666 * L_80 = V_11;
			NullCheck(L_80);
			int32_t L_81 = RectOffset_get_top_m3763(L_80, /*hidden argument*/NULL);
			V_0 = L_81;
			RectOffset_t666 * L_82 = V_11;
			NullCheck(L_82);
			int32_t L_83 = RectOffset_get_bottom_m4860(L_82, /*hidden argument*/NULL);
			V_1 = L_83;
			V_8 = 0;
		}

IL_024e:
		{
			GUILayoutEntry_t907 * L_84 = V_9;
			NullCheck(L_84);
			float L_85 = (L_84->___minHeight_2);
			float L_86 = (__this->___m_ChildMinHeight_24);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			float L_87 = Mathf_Max_m3711(NULL /*static, unused*/, L_85, L_86, /*hidden argument*/NULL);
			__this->___m_ChildMinHeight_24 = L_87;
			GUILayoutEntry_t907 * L_88 = V_9;
			NullCheck(L_88);
			float L_89 = (L_88->___maxHeight_3);
			float L_90 = (__this->___m_ChildMaxHeight_25);
			float L_91 = Mathf_Max_m3711(NULL /*static, unused*/, L_89, L_90, /*hidden argument*/NULL);
			__this->___m_ChildMaxHeight_25 = L_91;
		}

IL_027e:
		{
			int32_t L_92 = (__this->___m_StretchableCountY_19);
			GUILayoutEntry_t907 * L_93 = V_9;
			NullCheck(L_93);
			int32_t L_94 = (L_93->___stretchHeight_6);
			__this->___m_StretchableCountY_19 = ((int32_t)((int32_t)L_92+(int32_t)L_94));
		}

IL_0292:
		{
			bool L_95 = Enumerator_MoveNext_m5149((&V_10), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_95)
			{
				goto IL_01e9;
			}
		}

IL_029e:
		{
			IL2CPP_LEAVE(0x2B0, FINALLY_02a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_02a3;
	}

FINALLY_02a3:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_96 = V_10;
		Enumerator_t1023  L_97 = L_96;
		Object_t * L_98 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_97);
		NullCheck(L_98);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_98);
		IL2CPP_END_FINALLY(675)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(675)
	{
		IL2CPP_JUMP_TBL(0x2B0, IL_02b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_02b0:
	{
		V_12 = (0.0f);
		V_13 = (0.0f);
		GUIStyle_t342 * L_99 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_100 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t342 *)L_99) == ((Object_t*)(GUIStyle_t342 *)L_100))))
		{
			goto IL_02d9;
		}
	}
	{
		bool L_101 = (__this->___m_UserSpecifiedHeight_21);
		if (!L_101)
		{
			goto IL_0310;
		}
	}

IL_02d9:
	{
		GUIStyle_t342 * L_102 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_102);
		RectOffset_t666 * L_103 = GUIStyle_get_padding_m4867(L_102, /*hidden argument*/NULL);
		NullCheck(L_103);
		int32_t L_104 = RectOffset_get_top_m3763(L_103, /*hidden argument*/NULL);
		int32_t L_105 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_106 = Mathf_Max_m3620(NULL /*static, unused*/, L_104, L_105, /*hidden argument*/NULL);
		V_12 = (((float)L_106));
		GUIStyle_t342 * L_107 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_107);
		RectOffset_t666 * L_108 = GUIStyle_get_padding_m4867(L_107, /*hidden argument*/NULL);
		NullCheck(L_108);
		int32_t L_109 = RectOffset_get_bottom_m4860(L_108, /*hidden argument*/NULL);
		int32_t L_110 = V_1;
		int32_t L_111 = Mathf_Max_m3620(NULL /*static, unused*/, L_109, L_110, /*hidden argument*/NULL);
		V_13 = (((float)L_111));
		goto IL_0332;
	}

IL_0310:
	{
		RectOffset_t666 * L_112 = (__this->___m_Margin_26);
		int32_t L_113 = V_0;
		NullCheck(L_112);
		RectOffset_set_top_m4859(L_112, L_113, /*hidden argument*/NULL);
		RectOffset_t666 * L_114 = (__this->___m_Margin_26);
		int32_t L_115 = V_1;
		NullCheck(L_114);
		RectOffset_set_bottom_m4861(L_114, L_115, /*hidden argument*/NULL);
		float L_116 = (0.0f);
		V_13 = L_116;
		V_12 = L_116;
	}

IL_0332:
	{
		float L_117 = (((GUILayoutEntry_t907 *)__this)->___minHeight_2);
		float L_118 = (__this->___m_ChildMinHeight_24);
		float L_119 = V_12;
		float L_120 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_121 = Mathf_Max_m3711(NULL /*static, unused*/, L_117, ((float)((float)((float)((float)L_118+(float)L_119))+(float)L_120)), /*hidden argument*/NULL);
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = L_121;
		float L_122 = (((GUILayoutEntry_t907 *)__this)->___maxHeight_3);
		if ((!(((float)L_122) == ((float)(0.0f)))))
		{
			goto IL_03a1;
		}
	}
	{
		int32_t L_123 = (((GUILayoutEntry_t907 *)__this)->___stretchHeight_6);
		int32_t L_124 = (__this->___m_StretchableCountY_19);
		GUIStyle_t342 * L_125 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_125);
		bool L_126 = GUIStyle_get_stretchHeight_m4889(L_125, /*hidden argument*/NULL);
		G_B35_0 = L_124;
		G_B35_1 = L_123;
		G_B35_2 = __this;
		if (!L_126)
		{
			G_B36_0 = L_124;
			G_B36_1 = L_123;
			G_B36_2 = __this;
			goto IL_0382;
		}
	}
	{
		G_B37_0 = 1;
		G_B37_1 = G_B35_0;
		G_B37_2 = G_B35_1;
		G_B37_3 = G_B35_2;
		goto IL_0383;
	}

IL_0382:
	{
		G_B37_0 = 0;
		G_B37_1 = G_B36_0;
		G_B37_2 = G_B36_1;
		G_B37_3 = G_B36_2;
	}

IL_0383:
	{
		NullCheck(G_B37_3);
		((GUILayoutEntry_t907 *)G_B37_3)->___stretchHeight_6 = ((int32_t)((int32_t)G_B37_2+(int32_t)((int32_t)((int32_t)G_B37_1+(int32_t)G_B37_0))));
		float L_127 = (__this->___m_ChildMaxHeight_25);
		float L_128 = V_12;
		float L_129 = V_13;
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = ((float)((float)((float)((float)L_127+(float)L_128))+(float)L_129));
		goto IL_03a8;
	}

IL_03a1:
	{
		((GUILayoutEntry_t907 *)__this)->___stretchHeight_6 = 0;
	}

IL_03a8:
	{
		float L_130 = (((GUILayoutEntry_t907 *)__this)->___maxHeight_3);
		float L_131 = (((GUILayoutEntry_t907 *)__this)->___minHeight_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_132 = Mathf_Max_m3711(NULL /*static, unused*/, L_130, L_131, /*hidden argument*/NULL);
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = L_132;
		GUIStyle_t342 * L_133 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_133);
		float L_134 = GUIStyle_get_fixedHeight_m4886(L_133, /*hidden argument*/NULL);
		if ((((float)L_134) == ((float)(0.0f))))
		{
			goto IL_03f7;
		}
	}
	{
		GUIStyle_t342 * L_135 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_135);
		float L_136 = GUIStyle_get_fixedHeight_m4886(L_135, /*hidden argument*/NULL);
		float L_137 = L_136;
		V_14 = L_137;
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = L_137;
		float L_138 = V_14;
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = L_138;
		((GUILayoutEntry_t907 *)__this)->___stretchHeight_6 = 0;
	}

IL_03f7:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single)
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1023_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m5147_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m5148_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m5149_MethodInfo_var;
extern "C" void GUILayoutGroup_SetVertical_m4773 (GUILayoutGroup_t905 * __this, float ___y, float ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Enumerator_t1023_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		List_1_GetEnumerator_m5147_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484230);
		Enumerator_get_Current_m5148_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484231);
		Enumerator_MoveNext_m5149_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		s_Il2CppMethodIntialized = true;
	}
	RectOffset_t666 * V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	bool V_7 = false;
	GUILayoutEntry_t907 * V_8 = {0};
	Enumerator_t1023  V_9 = {0};
	float V_10 = 0.0f;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	GUILayoutEntry_t907 * V_13 = {0};
	Enumerator_t1023  V_14 = {0};
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	GUILayoutEntry_t907 * V_20 = {0};
	Enumerator_t1023  V_21 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B22_0 = 0;
	{
		float L_0 = ___y;
		float L_1 = ___height;
		GUILayoutEntry_SetVertical_m4759(__this, L_0, L_1, /*hidden argument*/NULL);
		List_1_t908 * L_2 = (__this->___entries_10);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_2);
		if (L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		GUIStyle_t342 * L_4 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		RectOffset_t666 * L_5 = GUIStyle_get_padding_m4867(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		bool L_6 = (__this->___resetCoords_12);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		___y = (0.0f);
	}

IL_0037:
	{
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_022f;
		}
	}
	{
		GUIStyle_t342 * L_8 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_9 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t342 *)L_8) == ((Object_t*)(GUIStyle_t342 *)L_9)))
		{
			goto IL_00c6;
		}
	}
	{
		RectOffset_t666 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_top_m3763(L_10, /*hidden argument*/NULL);
		V_1 = (((float)L_11));
		RectOffset_t666 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_bottom_m4860(L_12, /*hidden argument*/NULL);
		V_2 = (((float)L_13));
		List_1_t908 * L_14 = (__this->___entries_10);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_14);
		if (!L_15)
		{
			goto IL_00ba;
		}
	}
	{
		float L_16 = V_1;
		List_1_t908 * L_17 = (__this->___entries_10);
		NullCheck(L_17);
		GUILayoutEntry_t907 * L_18 = (GUILayoutEntry_t907 *)VirtFuncInvoker1< GUILayoutEntry_t907 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_17, 0);
		NullCheck(L_18);
		RectOffset_t666 * L_19 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_18);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_top_m3763(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_21 = Mathf_Max_m3711(NULL /*static, unused*/, L_16, (((float)L_20)), /*hidden argument*/NULL);
		V_1 = L_21;
		float L_22 = V_2;
		List_1_t908 * L_23 = (__this->___entries_10);
		List_1_t908 * L_24 = (__this->___entries_10);
		NullCheck(L_24);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_24);
		NullCheck(L_23);
		GUILayoutEntry_t907 * L_26 = (GUILayoutEntry_t907 *)VirtFuncInvoker1< GUILayoutEntry_t907 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_23, ((int32_t)((int32_t)L_25-(int32_t)1)));
		NullCheck(L_26);
		RectOffset_t666 * L_27 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_26);
		NullCheck(L_27);
		int32_t L_28 = RectOffset_get_bottom_m4860(L_27, /*hidden argument*/NULL);
		float L_29 = Mathf_Max_m3711(NULL /*static, unused*/, L_22, (((float)L_28)), /*hidden argument*/NULL);
		V_2 = L_29;
	}

IL_00ba:
	{
		float L_30 = ___y;
		float L_31 = V_1;
		___y = ((float)((float)L_30+(float)L_31));
		float L_32 = ___height;
		float L_33 = V_2;
		float L_34 = V_1;
		___height = ((float)((float)L_32-(float)((float)((float)L_33+(float)L_34))));
	}

IL_00c6:
	{
		float L_35 = ___height;
		float L_36 = (__this->___spacing_13);
		List_1_t908 * L_37 = (__this->___entries_10);
		NullCheck(L_37);
		int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_37);
		V_3 = ((float)((float)L_35-(float)((float)((float)L_36*(float)(((float)((int32_t)((int32_t)L_38-(int32_t)1))))))));
		V_4 = (0.0f);
		float L_39 = (__this->___m_ChildMinHeight_24);
		float L_40 = (__this->___m_ChildMaxHeight_25);
		if ((((float)L_39) == ((float)L_40)))
		{
			goto IL_011d;
		}
	}
	{
		float L_41 = V_3;
		float L_42 = (__this->___m_ChildMinHeight_24);
		float L_43 = (__this->___m_ChildMaxHeight_25);
		float L_44 = (__this->___m_ChildMinHeight_24);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_45 = Mathf_Clamp_m1358(NULL /*static, unused*/, ((float)((float)((float)((float)L_41-(float)L_42))/(float)((float)((float)L_43-(float)L_44)))), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_4 = L_45;
	}

IL_011d:
	{
		V_5 = (0.0f);
		float L_46 = V_3;
		float L_47 = (__this->___m_ChildMaxHeight_25);
		if ((!(((float)L_46) > ((float)L_47))))
		{
			goto IL_014e;
		}
	}
	{
		int32_t L_48 = (__this->___m_StretchableCountY_19);
		if ((((int32_t)L_48) <= ((int32_t)0)))
		{
			goto IL_014e;
		}
	}
	{
		float L_49 = V_3;
		float L_50 = (__this->___m_ChildMaxHeight_25);
		int32_t L_51 = (__this->___m_StretchableCountY_19);
		V_5 = ((float)((float)((float)((float)L_49-(float)L_50))/(float)(((float)L_51))));
	}

IL_014e:
	{
		V_6 = 0;
		V_7 = 1;
		List_1_t908 * L_52 = (__this->___entries_10);
		NullCheck(L_52);
		Enumerator_t1023  L_53 = List_1_GetEnumerator_m5147(L_52, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_9 = L_53;
	}

IL_0161:
	try
	{ // begin try (depth: 1)
		{
			goto IL_020c;
		}

IL_0166:
		{
			GUILayoutEntry_t907 * L_54 = Enumerator_get_Current_m5148((&V_9), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_8 = L_54;
			GUILayoutEntry_t907 * L_55 = V_8;
			NullCheck(L_55);
			float L_56 = (L_55->___minHeight_2);
			GUILayoutEntry_t907 * L_57 = V_8;
			NullCheck(L_57);
			float L_58 = (L_57->___maxHeight_3);
			float L_59 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			float L_60 = Mathf_Lerp_m1395(NULL /*static, unused*/, L_56, L_58, L_59, /*hidden argument*/NULL);
			V_10 = L_60;
			float L_61 = V_10;
			float L_62 = V_5;
			GUILayoutEntry_t907 * L_63 = V_8;
			NullCheck(L_63);
			int32_t L_64 = (L_63->___stretchHeight_6);
			V_10 = ((float)((float)L_61+(float)((float)((float)L_62*(float)(((float)L_64))))));
			GUILayoutEntry_t907 * L_65 = V_8;
			NullCheck(L_65);
			GUIStyle_t342 * L_66 = GUILayoutEntry_get_style_m4753(L_65, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
			GUIStyle_t342 * L_67 = GUILayoutUtility_get_spaceStyle_m4746(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t342 *)L_66) == ((Object_t*)(GUIStyle_t342 *)L_67)))
			{
				goto IL_01eb;
			}
		}

IL_01a7:
		{
			GUILayoutEntry_t907 * L_68 = V_8;
			NullCheck(L_68);
			RectOffset_t666 * L_69 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_68);
			NullCheck(L_69);
			int32_t L_70 = RectOffset_get_top_m3763(L_69, /*hidden argument*/NULL);
			V_11 = L_70;
			bool L_71 = V_7;
			if (!L_71)
			{
				goto IL_01c2;
			}
		}

IL_01bc:
		{
			V_11 = 0;
			V_7 = 0;
		}

IL_01c2:
		{
			int32_t L_72 = V_6;
			int32_t L_73 = V_11;
			if ((((int32_t)L_72) <= ((int32_t)L_73)))
			{
				goto IL_01d2;
			}
		}

IL_01cb:
		{
			int32_t L_74 = V_6;
			G_B22_0 = L_74;
			goto IL_01d4;
		}

IL_01d2:
		{
			int32_t L_75 = V_11;
			G_B22_0 = L_75;
		}

IL_01d4:
		{
			V_12 = G_B22_0;
			float L_76 = ___y;
			int32_t L_77 = V_12;
			___y = ((float)((float)L_76+(float)(((float)L_77))));
			GUILayoutEntry_t907 * L_78 = V_8;
			NullCheck(L_78);
			RectOffset_t666 * L_79 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_78);
			NullCheck(L_79);
			int32_t L_80 = RectOffset_get_bottom_m4860(L_79, /*hidden argument*/NULL);
			V_6 = L_80;
		}

IL_01eb:
		{
			GUILayoutEntry_t907 * L_81 = V_8;
			float L_82 = ___y;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			float L_83 = roundf(L_82);
			float L_84 = V_10;
			float L_85 = roundf(L_84);
			NullCheck(L_81);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_81, L_83, L_85);
			float L_86 = ___y;
			float L_87 = V_10;
			float L_88 = (__this->___spacing_13);
			___y = ((float)((float)L_86+(float)((float)((float)L_87+(float)L_88))));
		}

IL_020c:
		{
			bool L_89 = Enumerator_MoveNext_m5149((&V_9), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_89)
			{
				goto IL_0166;
			}
		}

IL_0218:
		{
			IL2CPP_LEAVE(0x22A, FINALLY_021d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_021d;
	}

FINALLY_021d:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_90 = V_9;
		Enumerator_t1023  L_91 = L_90;
		Object_t * L_92 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_91);
		NullCheck(L_92);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_92);
		IL2CPP_END_FINALLY(541)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(541)
	{
		IL2CPP_JUMP_TBL(0x22A, IL_022a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_022a:
	{
		goto IL_03c1;
	}

IL_022f:
	{
		GUIStyle_t342 * L_93 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_94 = GUIStyle_get_none_m4871(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t342 *)L_93) == ((Object_t*)(GUIStyle_t342 *)L_94)))
		{
			goto IL_02f6;
		}
	}
	{
		List_1_t908 * L_95 = (__this->___entries_10);
		NullCheck(L_95);
		Enumerator_t1023  L_96 = List_1_GetEnumerator_m5147(L_95, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_14 = L_96;
	}

IL_024c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02d3;
		}

IL_0251:
		{
			GUILayoutEntry_t907 * L_97 = Enumerator_get_Current_m5148((&V_14), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_13 = L_97;
			GUILayoutEntry_t907 * L_98 = V_13;
			NullCheck(L_98);
			RectOffset_t666 * L_99 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_98);
			NullCheck(L_99);
			int32_t L_100 = RectOffset_get_top_m3763(L_99, /*hidden argument*/NULL);
			RectOffset_t666 * L_101 = V_0;
			NullCheck(L_101);
			int32_t L_102 = RectOffset_get_top_m3763(L_101, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			int32_t L_103 = Mathf_Max_m3620(NULL /*static, unused*/, L_100, L_102, /*hidden argument*/NULL);
			V_15 = (((float)L_103));
			float L_104 = ___y;
			float L_105 = V_15;
			V_16 = ((float)((float)L_104+(float)L_105));
			float L_106 = ___height;
			GUILayoutEntry_t907 * L_107 = V_13;
			NullCheck(L_107);
			RectOffset_t666 * L_108 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_107);
			NullCheck(L_108);
			int32_t L_109 = RectOffset_get_bottom_m4860(L_108, /*hidden argument*/NULL);
			RectOffset_t666 * L_110 = V_0;
			NullCheck(L_110);
			int32_t L_111 = RectOffset_get_bottom_m4860(L_110, /*hidden argument*/NULL);
			int32_t L_112 = Mathf_Max_m3620(NULL /*static, unused*/, L_109, L_111, /*hidden argument*/NULL);
			float L_113 = V_15;
			V_17 = ((float)((float)((float)((float)L_106-(float)(((float)L_112))))-(float)L_113));
			GUILayoutEntry_t907 * L_114 = V_13;
			NullCheck(L_114);
			int32_t L_115 = (L_114->___stretchHeight_6);
			if (!L_115)
			{
				goto IL_02b5;
			}
		}

IL_02a5:
		{
			GUILayoutEntry_t907 * L_116 = V_13;
			float L_117 = V_16;
			float L_118 = V_17;
			NullCheck(L_116);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_116, L_117, L_118);
			goto IL_02d3;
		}

IL_02b5:
		{
			GUILayoutEntry_t907 * L_119 = V_13;
			float L_120 = V_16;
			float L_121 = V_17;
			GUILayoutEntry_t907 * L_122 = V_13;
			NullCheck(L_122);
			float L_123 = (L_122->___minHeight_2);
			GUILayoutEntry_t907 * L_124 = V_13;
			NullCheck(L_124);
			float L_125 = (L_124->___maxHeight_3);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			float L_126 = Mathf_Clamp_m1358(NULL /*static, unused*/, L_121, L_123, L_125, /*hidden argument*/NULL);
			NullCheck(L_119);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_119, L_120, L_126);
		}

IL_02d3:
		{
			bool L_127 = Enumerator_MoveNext_m5149((&V_14), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_127)
			{
				goto IL_0251;
			}
		}

IL_02df:
		{
			IL2CPP_LEAVE(0x2F1, FINALLY_02e4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_02e4;
	}

FINALLY_02e4:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_128 = V_14;
		Enumerator_t1023  L_129 = L_128;
		Object_t * L_130 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_129);
		NullCheck(L_130);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_130);
		IL2CPP_END_FINALLY(740)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(740)
	{
		IL2CPP_JUMP_TBL(0x2F1, IL_02f1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_02f1:
	{
		goto IL_03c1;
	}

IL_02f6:
	{
		float L_131 = ___y;
		RectOffset_t666 * L_132 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_132);
		int32_t L_133 = RectOffset_get_top_m3763(L_132, /*hidden argument*/NULL);
		V_18 = ((float)((float)L_131-(float)(((float)L_133))));
		float L_134 = ___height;
		RectOffset_t666 * L_135 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_135);
		int32_t L_136 = RectOffset_get_vertical_m3759(L_135, /*hidden argument*/NULL);
		V_19 = ((float)((float)L_134+(float)(((float)L_136))));
		List_1_t908 * L_137 = (__this->___entries_10);
		NullCheck(L_137);
		Enumerator_t1023  L_138 = List_1_GetEnumerator_m5147(L_137, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_21 = L_138;
	}

IL_0323:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03a3;
		}

IL_0328:
		{
			GUILayoutEntry_t907 * L_139 = Enumerator_get_Current_m5148((&V_21), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_20 = L_139;
			GUILayoutEntry_t907 * L_140 = V_20;
			NullCheck(L_140);
			int32_t L_141 = (L_140->___stretchHeight_6);
			if (!L_141)
			{
				goto IL_0369;
			}
		}

IL_033d:
		{
			GUILayoutEntry_t907 * L_142 = V_20;
			float L_143 = V_18;
			GUILayoutEntry_t907 * L_144 = V_20;
			NullCheck(L_144);
			RectOffset_t666 * L_145 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_144);
			NullCheck(L_145);
			int32_t L_146 = RectOffset_get_top_m3763(L_145, /*hidden argument*/NULL);
			float L_147 = V_19;
			GUILayoutEntry_t907 * L_148 = V_20;
			NullCheck(L_148);
			RectOffset_t666 * L_149 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_148);
			NullCheck(L_149);
			int32_t L_150 = RectOffset_get_vertical_m3759(L_149, /*hidden argument*/NULL);
			NullCheck(L_142);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_142, ((float)((float)L_143+(float)(((float)L_146)))), ((float)((float)L_147-(float)(((float)L_150)))));
			goto IL_03a3;
		}

IL_0369:
		{
			GUILayoutEntry_t907 * L_151 = V_20;
			float L_152 = V_18;
			GUILayoutEntry_t907 * L_153 = V_20;
			NullCheck(L_153);
			RectOffset_t666 * L_154 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_153);
			NullCheck(L_154);
			int32_t L_155 = RectOffset_get_top_m3763(L_154, /*hidden argument*/NULL);
			float L_156 = V_19;
			GUILayoutEntry_t907 * L_157 = V_20;
			NullCheck(L_157);
			RectOffset_t666 * L_158 = (RectOffset_t666 *)VirtFuncInvoker0< RectOffset_t666 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_157);
			NullCheck(L_158);
			int32_t L_159 = RectOffset_get_vertical_m3759(L_158, /*hidden argument*/NULL);
			GUILayoutEntry_t907 * L_160 = V_20;
			NullCheck(L_160);
			float L_161 = (L_160->___minHeight_2);
			GUILayoutEntry_t907 * L_162 = V_20;
			NullCheck(L_162);
			float L_163 = (L_162->___maxHeight_3);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
			float L_164 = Mathf_Clamp_m1358(NULL /*static, unused*/, ((float)((float)L_156-(float)(((float)L_159)))), L_161, L_163, /*hidden argument*/NULL);
			NullCheck(L_151);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_151, ((float)((float)L_152+(float)(((float)L_155)))), L_164);
		}

IL_03a3:
		{
			bool L_165 = Enumerator_MoveNext_m5149((&V_21), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_165)
			{
				goto IL_0328;
			}
		}

IL_03af:
		{
			IL2CPP_LEAVE(0x3C1, FINALLY_03b4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_03b4;
	}

FINALLY_03b4:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_166 = V_21;
		Enumerator_t1023  L_167 = L_166;
		Object_t * L_168 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_167);
		NullCheck(L_168);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_168);
		IL2CPP_END_FINALLY(948)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(948)
	{
		IL2CPP_JUMP_TBL(0x3C1, IL_03c1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_03c1:
	{
		return;
	}
}
// System.String UnityEngine.GUILayoutGroup::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t907_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t388_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1023_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m5147_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m5148_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m5149_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral643;
extern Il2CppCodeGenString* _stringLiteral644;
extern Il2CppCodeGenString* _stringLiteral645;
extern Il2CppCodeGenString* _stringLiteral646;
extern "C" String_t* GUILayoutGroup_ToString_m4774 (GUILayoutGroup_t905 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUILayoutEntry_t907_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(602);
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Single_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		Enumerator_t1023_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		List_1_GetEnumerator_m5147_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484230);
		Enumerator_get_Current_m5148_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484231);
		Enumerator_MoveNext_m5149_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		_stringLiteral44 = il2cpp_codegen_string_literal_from_index(44);
		_stringLiteral643 = il2cpp_codegen_string_literal_from_index(643);
		_stringLiteral644 = il2cpp_codegen_string_literal_from_index(644);
		_stringLiteral645 = il2cpp_codegen_string_literal_from_index(645);
		_stringLiteral646 = il2cpp_codegen_string_literal_from_index(646);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	int32_t V_2 = 0;
	GUILayoutEntry_t907 * V_3 = {0};
	Enumerator_t1023  V_4 = {0};
	String_t* V_5 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0023;
	}

IL_0013:
	{
		String_t* L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1319(NULL /*static, unused*/, L_2, _stringLiteral44, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_2;
		V_2 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t907_il2cpp_TypeInfo_var);
		int32_t L_6 = ((GUILayoutEntry_t907_StaticFields*)GUILayoutEntry_t907_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_7 = V_0;
		V_5 = L_7;
		ObjectU5BU5D_t356* L_8 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 5));
		String_t* L_9 = V_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t356* L_10 = L_8;
		String_t* L_11 = GUILayoutEntry_ToString_m4762(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 1, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t356* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral643);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral643;
		ObjectU5BU5D_t356* L_13 = L_12;
		float L_14 = (__this->___m_ChildMinHeight_24);
		float L_15 = L_14;
		Object_t * L_16 = Box(Single_t388_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 3, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t356* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 4);
		ArrayElementTypeCheck (L_17, _stringLiteral644);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral644;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m1316(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t907_il2cpp_TypeInfo_var);
		int32_t L_19 = ((GUILayoutEntry_t907_StaticFields*)GUILayoutEntry_t907_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		((GUILayoutEntry_t907_StaticFields*)GUILayoutEntry_t907_il2cpp_TypeInfo_var->static_fields)->___indent_9 = ((int32_t)((int32_t)L_19+(int32_t)4));
		List_1_t908 * L_20 = (__this->___entries_10);
		NullCheck(L_20);
		Enumerator_t1023  L_21 = List_1_GetEnumerator_m5147(L_20, /*hidden argument*/List_1_GetEnumerator_m5147_MethodInfo_var);
		V_4 = L_21;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a1;
		}

IL_0087:
		{
			GUILayoutEntry_t907 * L_22 = Enumerator_get_Current_m5148((&V_4), /*hidden argument*/Enumerator_get_Current_m5148_MethodInfo_var);
			V_3 = L_22;
			String_t* L_23 = V_0;
			GUILayoutEntry_t907 * L_24 = V_3;
			NullCheck(L_24);
			String_t* L_25 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.GUILayoutEntry::ToString() */, L_24);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_26 = String_Concat_m1575(NULL /*static, unused*/, L_23, L_25, _stringLiteral645, /*hidden argument*/NULL);
			V_0 = L_26;
		}

IL_00a1:
		{
			bool L_27 = Enumerator_MoveNext_m5149((&V_4), /*hidden argument*/Enumerator_MoveNext_m5149_MethodInfo_var);
			if (L_27)
			{
				goto IL_0087;
			}
		}

IL_00ad:
		{
			IL2CPP_LEAVE(0xBF, FINALLY_00b2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_00b2;
	}

FINALLY_00b2:
	{ // begin finally (depth: 1)
		Enumerator_t1023  L_28 = V_4;
		Enumerator_t1023  L_29 = L_28;
		Object_t * L_30 = Box(Enumerator_t1023_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_30);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_30);
		IL2CPP_END_FINALLY(178)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(178)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_00bf:
	{
		String_t* L_31 = V_0;
		String_t* L_32 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m1575(NULL /*static, unused*/, L_31, L_32, _stringLiteral646, /*hidden argument*/NULL);
		V_0 = L_33;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t907_il2cpp_TypeInfo_var);
		int32_t L_34 = ((GUILayoutEntry_t907_StaticFields*)GUILayoutEntry_t907_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		((GUILayoutEntry_t907_StaticFields*)GUILayoutEntry_t907_il2cpp_TypeInfo_var->static_fields)->___indent_9 = ((int32_t)((int32_t)L_34-(int32_t)4));
		String_t* L_35 = V_0;
		return L_35;
	}
}
// UnityEngine.GUIScrollGroup
#include "UnityEngine_UnityEngine_GUIScrollGroup.h"
// UnityEngine.GUIScrollGroup
#include "UnityEngine_UnityEngine_GUIScrollGroupMethodDeclarations.h"
// System.Void UnityEngine.GUIScrollGroup::.ctor()
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroupMethodDeclarations.h"
extern "C" void GUIScrollGroup__ctor_m4775 (GUIScrollGroup_t909 * __this, const MethodInfo* method)
{
	{
		__this->___allowHorizontalScroll_33 = 1;
		__this->___allowVerticalScroll_34 = 1;
		GUILayoutGroup__ctor_m4763(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::CalcWidth()
extern "C" void GUIScrollGroup_CalcWidth_m4776 (GUIScrollGroup_t909 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t907 *)__this)->___minWidth_0);
		V_0 = L_0;
		float L_1 = (((GUILayoutEntry_t907 *)__this)->___maxWidth_1);
		V_1 = L_1;
		bool L_2 = (__this->___allowHorizontalScroll_33);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		((GUILayoutEntry_t907 *)__this)->___minWidth_0 = (0.0f);
		((GUILayoutEntry_t907 *)__this)->___maxWidth_1 = (0.0f);
	}

IL_002f:
	{
		GUILayoutGroup_CalcWidth_m4770(__this, /*hidden argument*/NULL);
		float L_3 = (((GUILayoutEntry_t907 *)__this)->___minWidth_0);
		__this->___calcMinWidth_27 = L_3;
		float L_4 = (((GUILayoutEntry_t907 *)__this)->___maxWidth_1);
		__this->___calcMaxWidth_28 = L_4;
		bool L_5 = (__this->___allowHorizontalScroll_33);
		if (!L_5)
		{
			goto IL_009e;
		}
	}
	{
		float L_6 = (((GUILayoutEntry_t907 *)__this)->___minWidth_0);
		if ((!(((float)L_6) > ((float)(32.0f)))))
		{
			goto IL_0073;
		}
	}
	{
		((GUILayoutEntry_t907 *)__this)->___minWidth_0 = (32.0f);
	}

IL_0073:
	{
		float L_7 = V_0;
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_0085;
		}
	}
	{
		float L_8 = V_0;
		((GUILayoutEntry_t907 *)__this)->___minWidth_0 = L_8;
	}

IL_0085:
	{
		float L_9 = V_1;
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_009e;
		}
	}
	{
		float L_10 = V_1;
		((GUILayoutEntry_t907 *)__this)->___maxWidth_1 = L_10;
		((GUILayoutEntry_t907 *)__this)->___stretchWidth_5 = 0;
	}

IL_009e:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::SetHorizontal(System.Single,System.Single)
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
extern "C" void GUIScrollGroup_SetHorizontal_m4777 (GUIScrollGroup_t909 * __this, float ___x, float ___width, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (__this->___needsVerticalScrollbar_36);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		float L_1 = ___width;
		GUIStyle_t342 * L_2 = (__this->___verticalScrollbar_38);
		NullCheck(L_2);
		float L_3 = GUIStyle_get_fixedWidth_m4885(L_2, /*hidden argument*/NULL);
		GUIStyle_t342 * L_4 = (__this->___verticalScrollbar_38);
		NullCheck(L_4);
		RectOffset_t666 * L_5 = GUIStyle_get_margin_m4866(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = RectOffset_get_left_m3762(L_5, /*hidden argument*/NULL);
		G_B3_0 = ((float)((float)((float)((float)L_1-(float)L_3))-(float)(((float)L_6))));
		goto IL_0030;
	}

IL_002f:
	{
		float L_7 = ___width;
		G_B3_0 = L_7;
	}

IL_0030:
	{
		V_0 = G_B3_0;
		bool L_8 = (__this->___allowHorizontalScroll_33);
		if (!L_8)
		{
			goto IL_0091;
		}
	}
	{
		float L_9 = V_0;
		float L_10 = (__this->___calcMinWidth_27);
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0091;
		}
	}
	{
		__this->___needsHorizontalScrollbar_35 = 1;
		float L_11 = (__this->___calcMinWidth_27);
		((GUILayoutEntry_t907 *)__this)->___minWidth_0 = L_11;
		float L_12 = (__this->___calcMaxWidth_28);
		((GUILayoutEntry_t907 *)__this)->___maxWidth_1 = L_12;
		float L_13 = ___x;
		float L_14 = (__this->___calcMinWidth_27);
		GUILayoutGroup_SetHorizontal_m4771(__this, L_13, L_14, /*hidden argument*/NULL);
		Rect_t267 * L_15 = &(((GUILayoutEntry_t907 *)__this)->___rect_4);
		float L_16 = ___width;
		Rect_set_width_m1763(L_15, L_16, /*hidden argument*/NULL);
		float L_17 = (__this->___calcMinWidth_27);
		__this->___clientWidth_31 = L_17;
		goto IL_00d6;
	}

IL_0091:
	{
		__this->___needsHorizontalScrollbar_35 = 0;
		bool L_18 = (__this->___allowHorizontalScroll_33);
		if (!L_18)
		{
			goto IL_00bb;
		}
	}
	{
		float L_19 = (__this->___calcMinWidth_27);
		((GUILayoutEntry_t907 *)__this)->___minWidth_0 = L_19;
		float L_20 = (__this->___calcMaxWidth_28);
		((GUILayoutEntry_t907 *)__this)->___maxWidth_1 = L_20;
	}

IL_00bb:
	{
		float L_21 = ___x;
		float L_22 = V_0;
		GUILayoutGroup_SetHorizontal_m4771(__this, L_21, L_22, /*hidden argument*/NULL);
		Rect_t267 * L_23 = &(((GUILayoutEntry_t907 *)__this)->___rect_4);
		float L_24 = ___width;
		Rect_set_width_m1763(L_23, L_24, /*hidden argument*/NULL);
		float L_25 = V_0;
		__this->___clientWidth_31 = L_25;
	}

IL_00d6:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::CalcHeight()
extern "C" void GUIScrollGroup_CalcHeight_m4778 (GUIScrollGroup_t909 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t907 *)__this)->___minHeight_2);
		V_0 = L_0;
		float L_1 = (((GUILayoutEntry_t907 *)__this)->___maxHeight_3);
		V_1 = L_1;
		bool L_2 = (__this->___allowVerticalScroll_34);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = (0.0f);
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = (0.0f);
	}

IL_002f:
	{
		GUILayoutGroup_CalcHeight_m4772(__this, /*hidden argument*/NULL);
		float L_3 = (((GUILayoutEntry_t907 *)__this)->___minHeight_2);
		__this->___calcMinHeight_29 = L_3;
		float L_4 = (((GUILayoutEntry_t907 *)__this)->___maxHeight_3);
		__this->___calcMaxHeight_30 = L_4;
		bool L_5 = (__this->___needsHorizontalScrollbar_35);
		if (!L_5)
		{
			goto IL_0092;
		}
	}
	{
		GUIStyle_t342 * L_6 = (__this->___horizontalScrollbar_37);
		NullCheck(L_6);
		float L_7 = GUIStyle_get_fixedHeight_m4886(L_6, /*hidden argument*/NULL);
		GUIStyle_t342 * L_8 = (__this->___horizontalScrollbar_37);
		NullCheck(L_8);
		RectOffset_t666 * L_9 = GUIStyle_get_margin_m4866(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_top_m3763(L_9, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_7+(float)(((float)L_10))));
		float L_11 = (((GUILayoutEntry_t907 *)__this)->___minHeight_2);
		float L_12 = V_2;
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = ((float)((float)L_11+(float)L_12));
		float L_13 = (((GUILayoutEntry_t907 *)__this)->___maxHeight_3);
		float L_14 = V_2;
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = ((float)((float)L_13+(float)L_14));
	}

IL_0092:
	{
		bool L_15 = (__this->___allowVerticalScroll_34);
		if (!L_15)
		{
			goto IL_00e3;
		}
	}
	{
		float L_16 = (((GUILayoutEntry_t907 *)__this)->___minHeight_2);
		if ((!(((float)L_16) > ((float)(32.0f)))))
		{
			goto IL_00b8;
		}
	}
	{
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = (32.0f);
	}

IL_00b8:
	{
		float L_17 = V_0;
		if ((((float)L_17) == ((float)(0.0f))))
		{
			goto IL_00ca;
		}
	}
	{
		float L_18 = V_0;
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = L_18;
	}

IL_00ca:
	{
		float L_19 = V_1;
		if ((((float)L_19) == ((float)(0.0f))))
		{
			goto IL_00e3;
		}
	}
	{
		float L_20 = V_1;
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = L_20;
		((GUILayoutEntry_t907 *)__this)->___stretchHeight_6 = 0;
	}

IL_00e3:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::SetVertical(System.Single,System.Single)
// UnityEngine.GUIScrollGroup
#include "UnityEngine_UnityEngine_GUIScrollGroupMethodDeclarations.h"
extern "C" void GUIScrollGroup_SetVertical_m4779 (GUIScrollGroup_t909 * __this, float ___y, float ___height, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = ___height;
		V_0 = L_0;
		bool L_1 = (__this->___needsHorizontalScrollbar_35);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		float L_2 = V_0;
		GUIStyle_t342 * L_3 = (__this->___horizontalScrollbar_37);
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedHeight_m4886(L_3, /*hidden argument*/NULL);
		GUIStyle_t342 * L_5 = (__this->___horizontalScrollbar_37);
		NullCheck(L_5);
		RectOffset_t666 * L_6 = GUIStyle_get_margin_m4866(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_top_m3763(L_6, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_2-(float)((float)((float)L_4+(float)(((float)L_7))))));
	}

IL_002d:
	{
		bool L_8 = (__this->___allowVerticalScroll_34);
		if (!L_8)
		{
			goto IL_0139;
		}
	}
	{
		float L_9 = V_0;
		float L_10 = (__this->___calcMinHeight_29);
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0139;
		}
	}
	{
		bool L_11 = (__this->___needsHorizontalScrollbar_35);
		if (L_11)
		{
			goto IL_00db;
		}
	}
	{
		bool L_12 = (__this->___needsVerticalScrollbar_36);
		if (L_12)
		{
			goto IL_00db;
		}
	}
	{
		Rect_t267 * L_13 = &(((GUILayoutEntry_t907 *)__this)->___rect_4);
		float L_14 = Rect_get_width_m1762(L_13, /*hidden argument*/NULL);
		GUIStyle_t342 * L_15 = (__this->___verticalScrollbar_38);
		NullCheck(L_15);
		float L_16 = GUIStyle_get_fixedWidth_m4885(L_15, /*hidden argument*/NULL);
		GUIStyle_t342 * L_17 = (__this->___verticalScrollbar_38);
		NullCheck(L_17);
		RectOffset_t666 * L_18 = GUIStyle_get_margin_m4866(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = RectOffset_get_left_m3762(L_18, /*hidden argument*/NULL);
		__this->___clientWidth_31 = ((float)((float)((float)((float)L_14-(float)L_16))-(float)(((float)L_19))));
		float L_20 = (__this->___clientWidth_31);
		float L_21 = (__this->___calcMinWidth_27);
		if ((!(((float)L_20) < ((float)L_21))))
		{
			goto IL_00a6;
		}
	}
	{
		float L_22 = (__this->___calcMinWidth_27);
		__this->___clientWidth_31 = L_22;
	}

IL_00a6:
	{
		Rect_t267 * L_23 = &(((GUILayoutEntry_t907 *)__this)->___rect_4);
		float L_24 = Rect_get_width_m1762(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		Rect_t267 * L_25 = &(((GUILayoutEntry_t907 *)__this)->___rect_4);
		float L_26 = Rect_get_x_m1758(L_25, /*hidden argument*/NULL);
		float L_27 = (__this->___clientWidth_31);
		GUIScrollGroup_SetHorizontal_m4777(__this, L_26, L_27, /*hidden argument*/NULL);
		GUIScrollGroup_CalcHeight_m4778(__this, /*hidden argument*/NULL);
		Rect_t267 * L_28 = &(((GUILayoutEntry_t907 *)__this)->___rect_4);
		float L_29 = V_1;
		Rect_set_width_m1763(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00db:
	{
		float L_30 = (((GUILayoutEntry_t907 *)__this)->___minHeight_2);
		V_2 = L_30;
		float L_31 = (((GUILayoutEntry_t907 *)__this)->___maxHeight_3);
		V_3 = L_31;
		float L_32 = (__this->___calcMinHeight_29);
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = L_32;
		float L_33 = (__this->___calcMaxHeight_30);
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = L_33;
		float L_34 = ___y;
		float L_35 = (__this->___calcMinHeight_29);
		GUILayoutGroup_SetVertical_m4773(__this, L_34, L_35, /*hidden argument*/NULL);
		float L_36 = V_2;
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = L_36;
		float L_37 = V_3;
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = L_37;
		Rect_t267 * L_38 = &(((GUILayoutEntry_t907 *)__this)->___rect_4);
		float L_39 = ___height;
		Rect_set_height_m1765(L_38, L_39, /*hidden argument*/NULL);
		float L_40 = (__this->___calcMinHeight_29);
		__this->___clientHeight_32 = L_40;
		goto IL_0177;
	}

IL_0139:
	{
		bool L_41 = (__this->___allowVerticalScroll_34);
		if (!L_41)
		{
			goto IL_015c;
		}
	}
	{
		float L_42 = (__this->___calcMinHeight_29);
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = L_42;
		float L_43 = (__this->___calcMaxHeight_30);
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = L_43;
	}

IL_015c:
	{
		float L_44 = ___y;
		float L_45 = V_0;
		GUILayoutGroup_SetVertical_m4773(__this, L_44, L_45, /*hidden argument*/NULL);
		Rect_t267 * L_46 = &(((GUILayoutEntry_t907 *)__this)->___rect_4);
		float L_47 = ___height;
		Rect_set_height_m1765(L_46, L_47, /*hidden argument*/NULL);
		float L_48 = V_0;
		__this->___clientHeight_32 = L_48;
	}

IL_0177:
	{
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::.ctor(UnityEngine.GUIStyle,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntryMethodDeclarations.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
extern TypeInfo* GUILayoutEntry_t907_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern "C" void GUIWordWrapSizer__ctor_m4780 (GUIWordWrapSizer_t910 * __this, GUIStyle_t342 * ___style, GUIContent_t379 * ___content, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutEntry_t907_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(602);
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t342 * L_0 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t907_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m4750(__this, (0.0f), (0.0f), (0.0f), (0.0f), L_0, /*hidden argument*/NULL);
		GUIContent_t379 * L_1 = ___content;
		GUIContent_t379 * L_2 = (GUIContent_t379 *)il2cpp_codegen_object_new (GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent__ctor_m4722(L_2, L_1, /*hidden argument*/NULL);
		__this->___m_Content_10 = L_2;
		GUILayoutOptionU5BU5D_t441* L_3 = ___options;
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t441* >::Invoke(10 /* System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[]) */, __this, L_3);
		float L_4 = (((GUILayoutEntry_t907 *)__this)->___minHeight_2);
		__this->___m_ForcedMinHeight_11 = L_4;
		float L_5 = (((GUILayoutEntry_t907 *)__this)->___maxHeight_3);
		__this->___m_ForcedMaxHeight_12 = L_5;
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::CalcWidth()
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
extern "C" void GUIWordWrapSizer_CalcWidth_m4781 (GUIWordWrapSizer_t910 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t907 *)__this)->___minWidth_0);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = (((GUILayoutEntry_t907 *)__this)->___maxWidth_1);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_0063;
		}
	}

IL_0020:
	{
		GUIStyle_t342 * L_2 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		GUIContent_t379 * L_3 = (__this->___m_Content_10);
		NullCheck(L_2);
		GUIStyle_CalcMinMaxWidth_m4875(L_2, L_3, (&V_0), (&V_1), /*hidden argument*/NULL);
		float L_4 = (((GUILayoutEntry_t907 *)__this)->___minWidth_0);
		if ((!(((float)L_4) == ((float)(0.0f)))))
		{
			goto IL_004c;
		}
	}
	{
		float L_5 = V_0;
		((GUILayoutEntry_t907 *)__this)->___minWidth_0 = L_5;
	}

IL_004c:
	{
		float L_6 = (((GUILayoutEntry_t907 *)__this)->___maxWidth_1);
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0063;
		}
	}
	{
		float L_7 = V_1;
		((GUILayoutEntry_t907 *)__this)->___maxWidth_1 = L_7;
	}

IL_0063:
	{
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::CalcHeight()
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
extern "C" void GUIWordWrapSizer_CalcHeight_m4782 (GUIWordWrapSizer_t910 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (__this->___m_ForcedMinHeight_11);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = (__this->___m_ForcedMaxHeight_12);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_008d;
		}
	}

IL_0020:
	{
		GUIStyle_t342 * L_2 = GUILayoutEntry_get_style_m4753(__this, /*hidden argument*/NULL);
		GUIContent_t379 * L_3 = (__this->___m_Content_10);
		Rect_t267 * L_4 = &(((GUILayoutEntry_t907 *)__this)->___rect_4);
		float L_5 = Rect_get_width_m1762(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_6 = GUIStyle_CalcHeight_m4873(L_2, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = (__this->___m_ForcedMinHeight_11);
		if ((!(((float)L_7) == ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float L_8 = V_0;
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = L_8;
		goto IL_0065;
	}

IL_0059:
	{
		float L_9 = (__this->___m_ForcedMinHeight_11);
		((GUILayoutEntry_t907 *)__this)->___minHeight_2 = L_9;
	}

IL_0065:
	{
		float L_10 = (__this->___m_ForcedMaxHeight_12);
		if ((!(((float)L_10) == ((float)(0.0f)))))
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = V_0;
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = L_11;
		goto IL_008d;
	}

IL_0081:
	{
		float L_12 = (__this->___m_ForcedMaxHeight_12);
		((GUILayoutEntry_t907 *)__this)->___maxHeight_3 = L_12;
	}

IL_008d:
	{
		return;
	}
}
// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_TypeMethodDeclarations.h"
// System.Void UnityEngine.GUILayoutOption::.ctor(UnityEngine.GUILayoutOption/Type,System.Object)
// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_Type.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void GUILayoutOption__ctor_m4783 (GUILayoutOption_t912 * __this, int32_t ___type, Object_t * ___value, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type;
		__this->___type_0 = L_0;
		Object_t * L_1 = ___value;
		__this->___value_1 = L_1;
		return;
	}
}
// UnityEngine.GUISettings
#include "UnityEngine_UnityEngine_GUISettings.h"
// UnityEngine.GUISettings
#include "UnityEngine_UnityEngine_GUISettingsMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// System.Void UnityEngine.GUISettings::.ctor()
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void GUISettings__ctor_m4784 (GUISettings_t913 * __this, const MethodInfo* method)
{
	{
		__this->___m_DoubleClickSelectsWord_0 = 1;
		__this->___m_TripleClickSelectsLine_1 = 1;
		Color_t9  L_0 = Color_get_white_m1499(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_CursorColor_2 = L_0;
		__this->___m_CursorFlashSpeed_3 = (-1.0f);
		Color_t9  L_1 = {0};
		Color__ctor_m3412(&L_1, (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		__this->___m_SelectionColor_4 = L_1;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISkin/SkinChangedDelegate
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegate.h"
// UnityEngine.GUISkin/SkinChangedDelegate
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegateMethodDeclarations.h"
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void SkinChangedDelegate__ctor_m4785 (SkinChangedDelegate_t914 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::Invoke()
extern "C" void SkinChangedDelegate_Invoke_m4786 (SkinChangedDelegate_t914 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SkinChangedDelegate_Invoke_m4786((SkinChangedDelegate_t914 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t914(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.GUISkin/SkinChangedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * SkinChangedDelegate_BeginInvoke_m4787 (SkinChangedDelegate_t914 * __this, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void SkinChangedDelegate_EndInvoke_m4788 (SkinChangedDelegate_t914 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.StringComparer
#include "mscorlib_System_StringComparer.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_20.h"
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleState.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_1.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_2.h"
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
// System.StringComparer
#include "mscorlib_System_StringComparerMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_20MethodDeclarations.h"
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_1MethodDeclarations.h"
// System.Void UnityEngine.GUISkin::.ctor()
// UnityEngine.GUISettings
#include "UnityEngine_UnityEngine_GUISettingsMethodDeclarations.h"
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
extern TypeInfo* GUISettings_t913_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyleU5BU5D_t915_il2cpp_TypeInfo_var;
extern "C" void GUISkin__ctor_m4789 (GUISkin_t901 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISettings_t913_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(606);
		GUIStyleU5BU5D_t915_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISettings_t913 * L_0 = (GUISettings_t913 *)il2cpp_codegen_object_new (GUISettings_t913_il2cpp_TypeInfo_var);
		GUISettings__ctor_m4784(L_0, /*hidden argument*/NULL);
		__this->___m_Settings_24 = L_0;
		ScriptableObject__ctor_m3825(__this, /*hidden argument*/NULL);
		__this->___m_CustomStyles_23 = ((GUIStyleU5BU5D_t915*)SZArrayNew(GUIStyleU5BU5D_t915_il2cpp_TypeInfo_var, 1));
		return;
	}
}
// System.Void UnityEngine.GUISkin::OnEnable()
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
extern "C" void GUISkin_OnEnable_m4790 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Font UnityEngine.GUISkin::get_font()
extern "C" Font_t569 * GUISkin_get_font_m4791 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		Font_t569 * L_0 = (__this->___m_Font_2);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_font(UnityEngine.Font)
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
extern TypeInfo* GUISkin_t901_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" void GUISkin_set_font_m4792 (GUISkin_t901 * __this, Font_t569 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t569 * L_0 = ___value;
		__this->___m_Font_2 = L_0;
		GUISkin_t901 * L_1 = ((GUISkin_t901_StaticFields*)GUISkin_t901_il2cpp_TypeInfo_var->static_fields)->___current_28;
		bool L_2 = Object_op_Equality_m1413(NULL /*static, unused*/, L_1, __this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Font_t569 * L_3 = (__this->___m_Font_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_SetDefaultFont_m4894(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_box()
extern "C" GUIStyle_t342 * GUISkin_get_box_m4793 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_box_3);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_box(UnityEngine.GUIStyle)
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
extern "C" void GUISkin_set_box_m4794 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_box_3 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_label()
extern "C" GUIStyle_t342 * GUISkin_get_label_m4795 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_label_6);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_label(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_label_m4796 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_label_6 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textField()
extern "C" GUIStyle_t342 * GUISkin_get_textField_m4797 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_textField_7);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_textField(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_textField_m4798 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_textField_7 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textArea()
extern "C" GUIStyle_t342 * GUISkin_get_textArea_m4799 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_textArea_8);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_textArea(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_textArea_m4800 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_textArea_8 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_button()
extern "C" GUIStyle_t342 * GUISkin_get_button_m4801 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_button_4);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_button(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_button_m4802 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_button_4 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_toggle()
extern "C" GUIStyle_t342 * GUISkin_get_toggle_m4803 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_toggle_5);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_toggle(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_toggle_m4804 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_toggle_5 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_window()
extern "C" GUIStyle_t342 * GUISkin_get_window_m4805 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_window_9);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_window(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_window_m4806 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_window_9 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalSlider()
extern "C" GUIStyle_t342 * GUISkin_get_horizontalSlider_m4807 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_horizontalSlider_10);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalSlider(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalSlider_m4808 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_horizontalSlider_10 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalSliderThumb()
extern "C" GUIStyle_t342 * GUISkin_get_horizontalSliderThumb_m4809 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_horizontalSliderThumb_11);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalSliderThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalSliderThumb_m4810 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_horizontalSliderThumb_11 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalSlider()
extern "C" GUIStyle_t342 * GUISkin_get_verticalSlider_m4811 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_verticalSlider_12);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalSlider(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalSlider_m4812 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_verticalSlider_12 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalSliderThumb()
extern "C" GUIStyle_t342 * GUISkin_get_verticalSliderThumb_m4813 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_verticalSliderThumb_13);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalSliderThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalSliderThumb_m4814 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_verticalSliderThumb_13 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbar()
extern "C" GUIStyle_t342 * GUISkin_get_horizontalScrollbar_m4815 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_horizontalScrollbar_14);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbar(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbar_m4816 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_horizontalScrollbar_14 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarThumb()
extern "C" GUIStyle_t342 * GUISkin_get_horizontalScrollbarThumb_m4817 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_horizontalScrollbarThumb_15);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarThumb_m4818 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_horizontalScrollbarThumb_15 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarLeftButton()
extern "C" GUIStyle_t342 * GUISkin_get_horizontalScrollbarLeftButton_m4819 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_horizontalScrollbarLeftButton_16);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarLeftButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarLeftButton_m4820 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_horizontalScrollbarLeftButton_16 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarRightButton()
extern "C" GUIStyle_t342 * GUISkin_get_horizontalScrollbarRightButton_m4821 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_horizontalScrollbarRightButton_17);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarRightButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarRightButton_m4822 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_horizontalScrollbarRightButton_17 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbar()
extern "C" GUIStyle_t342 * GUISkin_get_verticalScrollbar_m4823 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_verticalScrollbar_18);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbar(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbar_m4824 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_verticalScrollbar_18 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarThumb()
extern "C" GUIStyle_t342 * GUISkin_get_verticalScrollbarThumb_m4825 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_verticalScrollbarThumb_19);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarThumb_m4826 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_verticalScrollbarThumb_19 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarUpButton()
extern "C" GUIStyle_t342 * GUISkin_get_verticalScrollbarUpButton_m4827 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_verticalScrollbarUpButton_20);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarUpButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarUpButton_m4828 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_verticalScrollbarUpButton_20 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarDownButton()
extern "C" GUIStyle_t342 * GUISkin_get_verticalScrollbarDownButton_m4829 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_verticalScrollbarDownButton_21);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarDownButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarDownButton_m4830 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_verticalScrollbarDownButton_21 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_scrollView()
extern "C" GUIStyle_t342 * GUISkin_get_scrollView_m4831 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = (__this->___m_ScrollView_22);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_scrollView(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_scrollView_m4832 (GUISkin_t901 * __this, GUIStyle_t342 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t342 * L_0 = ___value;
		__this->___m_ScrollView_22 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle[] UnityEngine.GUISkin::get_customStyles()
extern "C" GUIStyleU5BU5D_t915* GUISkin_get_customStyles_m4833 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUIStyleU5BU5D_t915* L_0 = (__this->___m_CustomStyles_23);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_customStyles(UnityEngine.GUIStyle[])
#include "UnityEngine_ArrayTypes.h"
extern "C" void GUISkin_set_customStyles_m4834 (GUISkin_t901 * __this, GUIStyleU5BU5D_t915* ___value, const MethodInfo* method)
{
	{
		GUIStyleU5BU5D_t915* L_0 = ___value;
		__this->___m_CustomStyles_23 = L_0;
		GUISkin_Apply_m4837(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISettings UnityEngine.GUISkin::get_settings()
extern "C" GUISettings_t913 * GUISkin_get_settings_m4835 (GUISkin_t901 * __this, const MethodInfo* method)
{
	{
		GUISettings_t913 * L_0 = (__this->___m_Settings_24);
		return L_0;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_error()
extern TypeInfo* GUISkin_t901_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t342 * GUISkin_get_error_m4836 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t342 * L_0 = ((GUISkin_t901_StaticFields*)GUISkin_t901_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t342 * L_1 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_1, /*hidden argument*/NULL);
		((GUISkin_t901_StaticFields*)GUISkin_t901_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25 = L_1;
	}

IL_0014:
	{
		GUIStyle_t342 * L_2 = ((GUISkin_t901_StaticFields*)GUISkin_t901_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25;
		return L_2;
	}
}
// System.Void UnityEngine.GUISkin::Apply()
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern Il2CppCodeGenString* _stringLiteral647;
extern "C" void GUISkin_Apply_m4837 (GUISkin_t901 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral647 = il2cpp_codegen_string_literal_from_index(647);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleU5BU5D_t915* L_0 = (__this->___m_CustomStyles_23);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_Log_m1411(NULL /*static, unused*/, _stringLiteral647, /*hidden argument*/NULL);
	}

IL_0015:
	{
		GUISkin_BuildStyleCache_m4838(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUISkin::BuildStyleCache()
// System.StringComparer
#include "mscorlib_System_StringComparerMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_20MethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern TypeInfo* StringComparer_t1024_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t916_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m5153_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral648;
extern Il2CppCodeGenString* _stringLiteral649;
extern Il2CppCodeGenString* _stringLiteral650;
extern Il2CppCodeGenString* _stringLiteral651;
extern Il2CppCodeGenString* _stringLiteral652;
extern Il2CppCodeGenString* _stringLiteral653;
extern Il2CppCodeGenString* _stringLiteral654;
extern Il2CppCodeGenString* _stringLiteral655;
extern Il2CppCodeGenString* _stringLiteral656;
extern Il2CppCodeGenString* _stringLiteral657;
extern Il2CppCodeGenString* _stringLiteral658;
extern Il2CppCodeGenString* _stringLiteral659;
extern Il2CppCodeGenString* _stringLiteral660;
extern Il2CppCodeGenString* _stringLiteral661;
extern Il2CppCodeGenString* _stringLiteral662;
extern Il2CppCodeGenString* _stringLiteral663;
extern Il2CppCodeGenString* _stringLiteral664;
extern Il2CppCodeGenString* _stringLiteral665;
extern Il2CppCodeGenString* _stringLiteral666;
extern Il2CppCodeGenString* _stringLiteral667;
extern "C" void GUISkin_BuildStyleCache_m4838 (GUISkin_t901 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		StringComparer_t1024_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(609);
		Dictionary_2_t916_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(610);
		Dictionary_2__ctor_m5153_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484234);
		_stringLiteral648 = il2cpp_codegen_string_literal_from_index(648);
		_stringLiteral649 = il2cpp_codegen_string_literal_from_index(649);
		_stringLiteral650 = il2cpp_codegen_string_literal_from_index(650);
		_stringLiteral651 = il2cpp_codegen_string_literal_from_index(651);
		_stringLiteral652 = il2cpp_codegen_string_literal_from_index(652);
		_stringLiteral653 = il2cpp_codegen_string_literal_from_index(653);
		_stringLiteral654 = il2cpp_codegen_string_literal_from_index(654);
		_stringLiteral655 = il2cpp_codegen_string_literal_from_index(655);
		_stringLiteral656 = il2cpp_codegen_string_literal_from_index(656);
		_stringLiteral657 = il2cpp_codegen_string_literal_from_index(657);
		_stringLiteral658 = il2cpp_codegen_string_literal_from_index(658);
		_stringLiteral659 = il2cpp_codegen_string_literal_from_index(659);
		_stringLiteral660 = il2cpp_codegen_string_literal_from_index(660);
		_stringLiteral661 = il2cpp_codegen_string_literal_from_index(661);
		_stringLiteral662 = il2cpp_codegen_string_literal_from_index(662);
		_stringLiteral663 = il2cpp_codegen_string_literal_from_index(663);
		_stringLiteral664 = il2cpp_codegen_string_literal_from_index(664);
		_stringLiteral665 = il2cpp_codegen_string_literal_from_index(665);
		_stringLiteral666 = il2cpp_codegen_string_literal_from_index(666);
		_stringLiteral667 = il2cpp_codegen_string_literal_from_index(667);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GUIStyle_t342 * L_0 = (__this->___m_box_3);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		GUIStyle_t342 * L_1 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_1, /*hidden argument*/NULL);
		__this->___m_box_3 = L_1;
	}

IL_0016:
	{
		GUIStyle_t342 * L_2 = (__this->___m_button_4);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		GUIStyle_t342 * L_3 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_3, /*hidden argument*/NULL);
		__this->___m_button_4 = L_3;
	}

IL_002c:
	{
		GUIStyle_t342 * L_4 = (__this->___m_toggle_5);
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		GUIStyle_t342 * L_5 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_5, /*hidden argument*/NULL);
		__this->___m_toggle_5 = L_5;
	}

IL_0042:
	{
		GUIStyle_t342 * L_6 = (__this->___m_label_6);
		if (L_6)
		{
			goto IL_0058;
		}
	}
	{
		GUIStyle_t342 * L_7 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_7, /*hidden argument*/NULL);
		__this->___m_label_6 = L_7;
	}

IL_0058:
	{
		GUIStyle_t342 * L_8 = (__this->___m_window_9);
		if (L_8)
		{
			goto IL_006e;
		}
	}
	{
		GUIStyle_t342 * L_9 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_9, /*hidden argument*/NULL);
		__this->___m_window_9 = L_9;
	}

IL_006e:
	{
		GUIStyle_t342 * L_10 = (__this->___m_textField_7);
		if (L_10)
		{
			goto IL_0084;
		}
	}
	{
		GUIStyle_t342 * L_11 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_11, /*hidden argument*/NULL);
		__this->___m_textField_7 = L_11;
	}

IL_0084:
	{
		GUIStyle_t342 * L_12 = (__this->___m_textArea_8);
		if (L_12)
		{
			goto IL_009a;
		}
	}
	{
		GUIStyle_t342 * L_13 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_13, /*hidden argument*/NULL);
		__this->___m_textArea_8 = L_13;
	}

IL_009a:
	{
		GUIStyle_t342 * L_14 = (__this->___m_horizontalSlider_10);
		if (L_14)
		{
			goto IL_00b0;
		}
	}
	{
		GUIStyle_t342 * L_15 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_15, /*hidden argument*/NULL);
		__this->___m_horizontalSlider_10 = L_15;
	}

IL_00b0:
	{
		GUIStyle_t342 * L_16 = (__this->___m_horizontalSliderThumb_11);
		if (L_16)
		{
			goto IL_00c6;
		}
	}
	{
		GUIStyle_t342 * L_17 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_17, /*hidden argument*/NULL);
		__this->___m_horizontalSliderThumb_11 = L_17;
	}

IL_00c6:
	{
		GUIStyle_t342 * L_18 = (__this->___m_verticalSlider_12);
		if (L_18)
		{
			goto IL_00dc;
		}
	}
	{
		GUIStyle_t342 * L_19 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_19, /*hidden argument*/NULL);
		__this->___m_verticalSlider_12 = L_19;
	}

IL_00dc:
	{
		GUIStyle_t342 * L_20 = (__this->___m_verticalSliderThumb_13);
		if (L_20)
		{
			goto IL_00f2;
		}
	}
	{
		GUIStyle_t342 * L_21 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_21, /*hidden argument*/NULL);
		__this->___m_verticalSliderThumb_13 = L_21;
	}

IL_00f2:
	{
		GUIStyle_t342 * L_22 = (__this->___m_horizontalScrollbar_14);
		if (L_22)
		{
			goto IL_0108;
		}
	}
	{
		GUIStyle_t342 * L_23 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_23, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbar_14 = L_23;
	}

IL_0108:
	{
		GUIStyle_t342 * L_24 = (__this->___m_horizontalScrollbarThumb_15);
		if (L_24)
		{
			goto IL_011e;
		}
	}
	{
		GUIStyle_t342 * L_25 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_25, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarThumb_15 = L_25;
	}

IL_011e:
	{
		GUIStyle_t342 * L_26 = (__this->___m_horizontalScrollbarLeftButton_16);
		if (L_26)
		{
			goto IL_0134;
		}
	}
	{
		GUIStyle_t342 * L_27 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_27, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarLeftButton_16 = L_27;
	}

IL_0134:
	{
		GUIStyle_t342 * L_28 = (__this->___m_horizontalScrollbarRightButton_17);
		if (L_28)
		{
			goto IL_014a;
		}
	}
	{
		GUIStyle_t342 * L_29 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_29, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarRightButton_17 = L_29;
	}

IL_014a:
	{
		GUIStyle_t342 * L_30 = (__this->___m_verticalScrollbar_18);
		if (L_30)
		{
			goto IL_0160;
		}
	}
	{
		GUIStyle_t342 * L_31 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_31, /*hidden argument*/NULL);
		__this->___m_verticalScrollbar_18 = L_31;
	}

IL_0160:
	{
		GUIStyle_t342 * L_32 = (__this->___m_verticalScrollbarThumb_19);
		if (L_32)
		{
			goto IL_0176;
		}
	}
	{
		GUIStyle_t342 * L_33 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_33, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarThumb_19 = L_33;
	}

IL_0176:
	{
		GUIStyle_t342 * L_34 = (__this->___m_verticalScrollbarUpButton_20);
		if (L_34)
		{
			goto IL_018c;
		}
	}
	{
		GUIStyle_t342 * L_35 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_35, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarUpButton_20 = L_35;
	}

IL_018c:
	{
		GUIStyle_t342 * L_36 = (__this->___m_verticalScrollbarDownButton_21);
		if (L_36)
		{
			goto IL_01a2;
		}
	}
	{
		GUIStyle_t342 * L_37 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_37, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarDownButton_21 = L_37;
	}

IL_01a2:
	{
		GUIStyle_t342 * L_38 = (__this->___m_ScrollView_22);
		if (L_38)
		{
			goto IL_01b8;
		}
	}
	{
		GUIStyle_t342 * L_39 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_39, /*hidden argument*/NULL);
		__this->___m_ScrollView_22 = L_39;
	}

IL_01b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t1024_il2cpp_TypeInfo_var);
		StringComparer_t1024 * L_40 = StringComparer_get_OrdinalIgnoreCase_m5152(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_41 = (Dictionary_2_t916 *)il2cpp_codegen_object_new (Dictionary_2_t916_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m5153(L_41, L_40, /*hidden argument*/Dictionary_2__ctor_m5153_MethodInfo_var);
		__this->___m_Styles_26 = L_41;
		Dictionary_2_t916 * L_42 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_43 = (__this->___m_box_3);
		NullCheck(L_42);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_42, _stringLiteral648, L_43);
		GUIStyle_t342 * L_44 = (__this->___m_box_3);
		NullCheck(L_44);
		GUIStyle_set_name_m4880(L_44, _stringLiteral648, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_45 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_46 = (__this->___m_button_4);
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_45, _stringLiteral649, L_46);
		GUIStyle_t342 * L_47 = (__this->___m_button_4);
		NullCheck(L_47);
		GUIStyle_set_name_m4880(L_47, _stringLiteral649, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_48 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_49 = (__this->___m_toggle_5);
		NullCheck(L_48);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_48, _stringLiteral650, L_49);
		GUIStyle_t342 * L_50 = (__this->___m_toggle_5);
		NullCheck(L_50);
		GUIStyle_set_name_m4880(L_50, _stringLiteral650, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_51 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_52 = (__this->___m_label_6);
		NullCheck(L_51);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_51, _stringLiteral651, L_52);
		GUIStyle_t342 * L_53 = (__this->___m_label_6);
		NullCheck(L_53);
		GUIStyle_set_name_m4880(L_53, _stringLiteral651, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_54 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_55 = (__this->___m_window_9);
		NullCheck(L_54);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_54, _stringLiteral652, L_55);
		GUIStyle_t342 * L_56 = (__this->___m_window_9);
		NullCheck(L_56);
		GUIStyle_set_name_m4880(L_56, _stringLiteral652, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_57 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_58 = (__this->___m_textField_7);
		NullCheck(L_57);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_57, _stringLiteral653, L_58);
		GUIStyle_t342 * L_59 = (__this->___m_textField_7);
		NullCheck(L_59);
		GUIStyle_set_name_m4880(L_59, _stringLiteral653, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_60 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_61 = (__this->___m_textArea_8);
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_60, _stringLiteral654, L_61);
		GUIStyle_t342 * L_62 = (__this->___m_textArea_8);
		NullCheck(L_62);
		GUIStyle_set_name_m4880(L_62, _stringLiteral654, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_63 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_64 = (__this->___m_horizontalSlider_10);
		NullCheck(L_63);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_63, _stringLiteral655, L_64);
		GUIStyle_t342 * L_65 = (__this->___m_horizontalSlider_10);
		NullCheck(L_65);
		GUIStyle_set_name_m4880(L_65, _stringLiteral655, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_66 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_67 = (__this->___m_horizontalSliderThumb_11);
		NullCheck(L_66);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_66, _stringLiteral656, L_67);
		GUIStyle_t342 * L_68 = (__this->___m_horizontalSliderThumb_11);
		NullCheck(L_68);
		GUIStyle_set_name_m4880(L_68, _stringLiteral656, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_69 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_70 = (__this->___m_verticalSlider_12);
		NullCheck(L_69);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_69, _stringLiteral657, L_70);
		GUIStyle_t342 * L_71 = (__this->___m_verticalSlider_12);
		NullCheck(L_71);
		GUIStyle_set_name_m4880(L_71, _stringLiteral657, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_72 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_73 = (__this->___m_verticalSliderThumb_13);
		NullCheck(L_72);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_72, _stringLiteral658, L_73);
		GUIStyle_t342 * L_74 = (__this->___m_verticalSliderThumb_13);
		NullCheck(L_74);
		GUIStyle_set_name_m4880(L_74, _stringLiteral658, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_75 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_76 = (__this->___m_horizontalScrollbar_14);
		NullCheck(L_75);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_75, _stringLiteral659, L_76);
		GUIStyle_t342 * L_77 = (__this->___m_horizontalScrollbar_14);
		NullCheck(L_77);
		GUIStyle_set_name_m4880(L_77, _stringLiteral659, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_78 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_79 = (__this->___m_horizontalScrollbarThumb_15);
		NullCheck(L_78);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_78, _stringLiteral660, L_79);
		GUIStyle_t342 * L_80 = (__this->___m_horizontalScrollbarThumb_15);
		NullCheck(L_80);
		GUIStyle_set_name_m4880(L_80, _stringLiteral660, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_81 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_82 = (__this->___m_horizontalScrollbarLeftButton_16);
		NullCheck(L_81);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_81, _stringLiteral661, L_82);
		GUIStyle_t342 * L_83 = (__this->___m_horizontalScrollbarLeftButton_16);
		NullCheck(L_83);
		GUIStyle_set_name_m4880(L_83, _stringLiteral661, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_84 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_85 = (__this->___m_horizontalScrollbarRightButton_17);
		NullCheck(L_84);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_84, _stringLiteral662, L_85);
		GUIStyle_t342 * L_86 = (__this->___m_horizontalScrollbarRightButton_17);
		NullCheck(L_86);
		GUIStyle_set_name_m4880(L_86, _stringLiteral662, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_87 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_88 = (__this->___m_verticalScrollbar_18);
		NullCheck(L_87);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_87, _stringLiteral663, L_88);
		GUIStyle_t342 * L_89 = (__this->___m_verticalScrollbar_18);
		NullCheck(L_89);
		GUIStyle_set_name_m4880(L_89, _stringLiteral663, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_90 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_91 = (__this->___m_verticalScrollbarThumb_19);
		NullCheck(L_90);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_90, _stringLiteral664, L_91);
		GUIStyle_t342 * L_92 = (__this->___m_verticalScrollbarThumb_19);
		NullCheck(L_92);
		GUIStyle_set_name_m4880(L_92, _stringLiteral664, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_93 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_94 = (__this->___m_verticalScrollbarUpButton_20);
		NullCheck(L_93);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_93, _stringLiteral665, L_94);
		GUIStyle_t342 * L_95 = (__this->___m_verticalScrollbarUpButton_20);
		NullCheck(L_95);
		GUIStyle_set_name_m4880(L_95, _stringLiteral665, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_96 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_97 = (__this->___m_verticalScrollbarDownButton_21);
		NullCheck(L_96);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_96, _stringLiteral666, L_97);
		GUIStyle_t342 * L_98 = (__this->___m_verticalScrollbarDownButton_21);
		NullCheck(L_98);
		GUIStyle_set_name_m4880(L_98, _stringLiteral666, /*hidden argument*/NULL);
		Dictionary_2_t916 * L_99 = (__this->___m_Styles_26);
		GUIStyle_t342 * L_100 = (__this->___m_ScrollView_22);
		NullCheck(L_99);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_99, _stringLiteral667, L_100);
		GUIStyle_t342 * L_101 = (__this->___m_ScrollView_22);
		NullCheck(L_101);
		GUIStyle_set_name_m4880(L_101, _stringLiteral667, /*hidden argument*/NULL);
		GUIStyleU5BU5D_t915* L_102 = (__this->___m_CustomStyles_23);
		if (!L_102)
		{
			goto IL_0516;
		}
	}
	{
		V_0 = 0;
		goto IL_0508;
	}

IL_04d2:
	{
		GUIStyleU5BU5D_t915* L_103 = (__this->___m_CustomStyles_23);
		int32_t L_104 = V_0;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, L_104);
		int32_t L_105 = L_104;
		if ((*(GUIStyle_t342 **)(GUIStyle_t342 **)SZArrayLdElema(L_103, L_105, sizeof(GUIStyle_t342 *))))
		{
			goto IL_04e4;
		}
	}
	{
		goto IL_0504;
	}

IL_04e4:
	{
		Dictionary_2_t916 * L_106 = (__this->___m_Styles_26);
		GUIStyleU5BU5D_t915* L_107 = (__this->___m_CustomStyles_23);
		int32_t L_108 = V_0;
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, L_108);
		int32_t L_109 = L_108;
		NullCheck((*(GUIStyle_t342 **)(GUIStyle_t342 **)SZArrayLdElema(L_107, L_109, sizeof(GUIStyle_t342 *))));
		String_t* L_110 = GUIStyle_get_name_m4879((*(GUIStyle_t342 **)(GUIStyle_t342 **)SZArrayLdElema(L_107, L_109, sizeof(GUIStyle_t342 *))), /*hidden argument*/NULL);
		GUIStyleU5BU5D_t915* L_111 = (__this->___m_CustomStyles_23);
		int32_t L_112 = V_0;
		NullCheck(L_111);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_111, L_112);
		int32_t L_113 = L_112;
		NullCheck(L_106);
		VirtActionInvoker2< String_t*, GUIStyle_t342 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_106, L_110, (*(GUIStyle_t342 **)(GUIStyle_t342 **)SZArrayLdElema(L_111, L_113, sizeof(GUIStyle_t342 *))));
	}

IL_0504:
	{
		int32_t L_114 = V_0;
		V_0 = ((int32_t)((int32_t)L_114+(int32_t)1));
	}

IL_0508:
	{
		int32_t L_115 = V_0;
		GUIStyleU5BU5D_t915* L_116 = (__this->___m_CustomStyles_23);
		NullCheck(L_116);
		if ((((int32_t)L_115) < ((int32_t)(((int32_t)(((Array_t *)L_116)->max_length))))))
		{
			goto IL_04d2;
		}
	}

IL_0516:
	{
		GUIStyle_t342 * L_117 = GUISkin_get_error_m4836(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_117);
		GUIStyle_set_stretchHeight_m4890(L_117, 1, /*hidden argument*/NULL);
		GUIStyle_t342 * L_118 = GUISkin_get_error_m4836(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_118);
		GUIStyleState_t917 * L_119 = GUIStyle_get_normal_m4865(L_118, /*hidden argument*/NULL);
		Color_t9  L_120 = Color_get_red_m4040(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_119);
		GUIStyleState_set_textColor_m4849(L_119, L_120, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::GetStyle(System.String)
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t898_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral668;
extern Il2CppCodeGenString* _stringLiteral669;
extern Il2CppCodeGenString* _stringLiteral670;
extern "C" GUIStyle_t342 * GUISkin_GetStyle_m4839 (GUISkin_t901 * __this, String_t* ___styleName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		EventType_t898_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(596);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral668 = il2cpp_codegen_string_literal_from_index(668);
		_stringLiteral669 = il2cpp_codegen_string_literal_from_index(669);
		_stringLiteral670 = il2cpp_codegen_string_literal_from_index(670);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t342 * V_0 = {0};
	{
		String_t* L_0 = ___styleName;
		GUIStyle_t342 * L_1 = GUISkin_FindStyle_m4840(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GUIStyle_t342 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		GUIStyle_t342 * L_3 = V_0;
		return L_3;
	}

IL_0010:
	{
		ObjectU5BU5D_t356* L_4 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 6));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, _stringLiteral668);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral668;
		ObjectU5BU5D_t356* L_5 = L_4;
		String_t* L_6 = ___styleName;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t356* L_7 = L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral669);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral669;
		ObjectU5BU5D_t356* L_8 = L_7;
		String_t* L_9 = Object_get_name_m1338(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t356* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral670);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral670;
		ObjectU5BU5D_t356* L_11 = L_10;
		Event_t381 * L_12 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = Event_get_type_m1405(L_12, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(EventType_t898_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 5, sizeof(Object_t *))) = (Object_t *)L_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1316(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Debug_LogWarning_m1399(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		GUIStyle_t342 * L_17 = GUISkin_get_error_m4836(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_17;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::FindStyle(System.String)
extern Il2CppCodeGenString* _stringLiteral671;
extern "C" GUIStyle_t342 * GUISkin_FindStyle_m4840 (GUISkin_t901 * __this, String_t* ___styleName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral671 = il2cpp_codegen_string_literal_from_index(671);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t342 * V_0 = {0};
	{
		bool L_0 = Object_op_Equality_m1413(NULL /*static, unused*/, __this, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Debug_LogError_m1317(NULL /*static, unused*/, _stringLiteral671, /*hidden argument*/NULL);
		return (GUIStyle_t342 *)NULL;
	}

IL_0018:
	{
		Dictionary_2_t916 * L_1 = (__this->___m_Styles_26);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		GUISkin_BuildStyleCache_m4838(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		Dictionary_2_t916 * L_2 = (__this->___m_Styles_26);
		String_t* L_3 = ___styleName;
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker2< bool, String_t*, GUIStyle_t342 ** >::Invoke(33 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::TryGetValue(!0,!1&) */, L_2, L_3, (&V_0));
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		GUIStyle_t342 * L_5 = V_0;
		return L_5;
	}

IL_003e:
	{
		return (GUIStyle_t342 *)NULL;
	}
}
// System.Void UnityEngine.GUISkin::MakeCurrent()
// UnityEngine.GUISkin/SkinChangedDelegate
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegateMethodDeclarations.h"
extern TypeInfo* GUISkin_t901_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" void GUISkin_MakeCurrent_m4841 (GUISkin_t901 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUISkin_t901_StaticFields*)GUISkin_t901_il2cpp_TypeInfo_var->static_fields)->___current_28 = __this;
		Font_t569 * L_0 = GUISkin_get_font_m4791(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_SetDefaultFont_m4894(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SkinChangedDelegate_t914 * L_1 = ((GUISkin_t901_StaticFields*)GUISkin_t901_il2cpp_TypeInfo_var->static_fields)->___m_SkinChanged_27;
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		SkinChangedDelegate_t914 * L_2 = ((GUISkin_t901_StaticFields*)GUISkin_t901_il2cpp_TypeInfo_var->static_fields)->___m_SkinChanged_27;
		NullCheck(L_2);
		SkinChangedDelegate_Invoke_m4786(L_2, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.GUISkin::GetEnumerator()
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_1MethodDeclarations.h"
extern TypeInfo* Enumerator_t1026_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m5154_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m5155_MethodInfo_var;
extern "C" Object_t * GUISkin_GetEnumerator_m4842 (GUISkin_t901 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1026_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(611);
		Dictionary_2_get_Values_m5154_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484235);
		ValueCollection_GetEnumerator_m5155_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484236);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t916 * L_0 = (__this->___m_Styles_26);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		GUISkin_BuildStyleCache_m4838(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		Dictionary_2_t916 * L_1 = (__this->___m_Styles_26);
		NullCheck(L_1);
		ValueCollection_t1025 * L_2 = Dictionary_2_get_Values_m5154(L_1, /*hidden argument*/Dictionary_2_get_Values_m5154_MethodInfo_var);
		NullCheck(L_2);
		Enumerator_t1026  L_3 = ValueCollection_GetEnumerator_m5155(L_2, /*hidden argument*/ValueCollection_GetEnumerator_m5155_MethodInfo_var);
		Enumerator_t1026  L_4 = L_3;
		Object_t * L_5 = Box(Enumerator_t1026_il2cpp_TypeInfo_var, &L_4);
		return (Object_t *)L_5;
	}
}
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// System.Void UnityEngine.GUIStyleState::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
extern "C" void GUIStyleState__ctor_m4843 (GUIStyleState_t917 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		GUIStyleState_Init_m4846(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::.ctor(UnityEngine.GUIStyle,System.IntPtr)
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void GUIStyleState__ctor_m4844 (GUIStyleState_t917 * __this, GUIStyle_t342 * ___sourceStyle, IntPtr_t ___source, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		GUIStyle_t342 * L_0 = ___sourceStyle;
		__this->___m_SourceStyle_1 = L_0;
		IntPtr_t L_1 = ___source;
		__this->___m_Ptr_0 = L_1;
		Texture2D_t355 * L_2 = GUIStyleState_GetBackgroundInternal_m4848(__this, /*hidden argument*/NULL);
		__this->___m_Background_2 = L_2;
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::Finalize()
extern "C" void GUIStyleState_Finalize_m4845 (GUIStyleState_t917 * __this, const MethodInfo* method)
{
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t342 * L_0 = (__this->___m_SourceStyle_1);
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			GUIStyleState_Cleanup_m4847(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m5102(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::Init()
extern "C" void GUIStyleState_Init_m4846 (GUIStyleState_t917 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyleState_Init_m4846_ftn) (GUIStyleState_t917 *);
	static GUIStyleState_Init_m4846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_Init_m4846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyleState::Cleanup()
extern "C" void GUIStyleState_Cleanup_m4847 (GUIStyleState_t917 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyleState_Cleanup_m4847_ftn) (GUIStyleState_t917 *);
	static GUIStyleState_Cleanup_m4847_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_Cleanup_m4847_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::Cleanup()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternal()
extern "C" Texture2D_t355 * GUIStyleState_GetBackgroundInternal_m4848 (GUIStyleState_t917 * __this, const MethodInfo* method)
{
	typedef Texture2D_t355 * (*GUIStyleState_GetBackgroundInternal_m4848_ftn) (GUIStyleState_t917 *);
	static GUIStyleState_GetBackgroundInternal_m4848_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_GetBackgroundInternal_m4848_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::GetBackgroundInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyleState::set_textColor(UnityEngine.Color)
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
extern "C" void GUIStyleState_set_textColor_m4849 (GUIStyleState_t917 * __this, Color_t9  ___value, const MethodInfo* method)
{
	{
		GUIStyleState_INTERNAL_set_textColor_m4850(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)
extern "C" void GUIStyleState_INTERNAL_set_textColor_m4850 (GUIStyleState_t917 * __this, Color_t9 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyleState_INTERNAL_set_textColor_m4850_ftn) (GUIStyleState_t917 *, Color_t9 *);
	static GUIStyleState_INTERNAL_set_textColor_m4850_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_INTERNAL_set_textColor_m4850_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectOffset::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
extern "C" void RectOffset__ctor_m3764 (RectOffset_t666 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		RectOffset_Init_m4854(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void RectOffset__ctor_m4851 (RectOffset_t666 * __this, GUIStyle_t342 * ___sourceStyle, IntPtr_t ___source, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		GUIStyle_t342 * L_0 = ___sourceStyle;
		__this->___m_SourceStyle_1 = L_0;
		IntPtr_t L_1 = ___source;
		__this->___m_Ptr_0 = L_1;
		return;
	}
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C" void RectOffset_Finalize_m4852 (RectOffset_t666 * __this, const MethodInfo* method)
{
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t342 * L_0 = (__this->___m_SourceStyle_1);
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			RectOffset_Cleanup_m4855(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m5102(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_001d:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral672;
extern "C" String_t* RectOffset_ToString_m4853 (RectOffset_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		_stringLiteral672 = il2cpp_codegen_string_literal_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t356* L_0 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 4));
		int32_t L_1 = RectOffset_get_left_m3762(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t356* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m4857(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t356* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m3763(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t356* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m4860(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m4240(NULL /*static, unused*/, _stringLiteral672, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C" void RectOffset_Init_m4854 (RectOffset_t666 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m4854_ftn) (RectOffset_t666 *);
	static RectOffset_Init_m4854_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m4854_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C" void RectOffset_Cleanup_m4855 (RectOffset_t666 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m4855_ftn) (RectOffset_t666 *);
	static RectOffset_Cleanup_m4855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m4855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C" int32_t RectOffset_get_left_m3762 (RectOffset_t666 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m3762_ftn) (RectOffset_t666 *);
	static RectOffset_get_left_m3762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m3762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void RectOffset_set_left_m4856 (RectOffset_t666 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m4856_ftn) (RectOffset_t666 *, int32_t);
	static RectOffset_set_left_m4856_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m4856_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C" int32_t RectOffset_get_right_m4857 (RectOffset_t666 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m4857_ftn) (RectOffset_t666 *);
	static RectOffset_get_right_m4857_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m4857_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C" void RectOffset_set_right_m4858 (RectOffset_t666 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m4858_ftn) (RectOffset_t666 *, int32_t);
	static RectOffset_set_right_m4858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m4858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C" int32_t RectOffset_get_top_m3763 (RectOffset_t666 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m3763_ftn) (RectOffset_t666 *);
	static RectOffset_get_top_m3763_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m3763_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C" void RectOffset_set_top_m4859 (RectOffset_t666 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m4859_ftn) (RectOffset_t666 *, int32_t);
	static RectOffset_set_top_m4859_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m4859_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C" int32_t RectOffset_get_bottom_m4860 (RectOffset_t666 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m4860_ftn) (RectOffset_t666 *);
	static RectOffset_get_bottom_m4860_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m4860_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C" void RectOffset_set_bottom_m4861 (RectOffset_t666 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m4861_ftn) (RectOffset_t666 *, int32_t);
	static RectOffset_set_bottom_m4861_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m4861_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C" int32_t RectOffset_get_horizontal_m3758 (RectOffset_t666 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m3758_ftn) (RectOffset_t666 *);
	static RectOffset_get_horizontal_m3758_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m3758_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C" int32_t RectOffset_get_vertical_m3759 (RectOffset_t666 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m3759_ftn) (RectOffset_t666 *);
	static RectOffset_get_vertical_m3759_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m3759_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyleMethodDeclarations.h"
// UnityEngine.ImagePosition
#include "UnityEngine_UnityEngine_ImagePosition.h"
// UnityEngine.ImagePosition
#include "UnityEngine_UnityEngine_ImagePositionMethodDeclarations.h"
// UnityEngine.Internal_DrawArguments
#include "UnityEngine_UnityEngine_Internal_DrawArguments.h"
// System.Void UnityEngine.GUIStyle::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
extern "C" void GUIStyle__ctor_m4862 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		GUIStyle_Init_m4877(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::.cctor()
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" void GUIStyle__cctor_m4863 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUIStyle_t342_StaticFields*)GUIStyle_t342_il2cpp_TypeInfo_var->static_fields)->___showKeyboardFocus_14 = 1;
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Finalize()
extern "C" void GUIStyle_Finalize_m4864 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GUIStyle_Cleanup_m4878(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m5102(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_normal()
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
extern TypeInfo* GUIStyleState_t917_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t917 * GUIStyle_get_normal_m4865 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t917_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(612);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t917 * L_0 = (__this->___m_Normal_1);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m4881(__this, 0, /*hidden argument*/NULL);
		GUIStyleState_t917 * L_2 = (GUIStyleState_t917 *)il2cpp_codegen_object_new (GUIStyleState_t917_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m4844(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Normal_1 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t917 * L_3 = (__this->___m_Normal_1);
		return L_3;
	}
}
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_margin()
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
extern TypeInfo* RectOffset_t666_il2cpp_TypeInfo_var;
extern "C" RectOffset_t666 * GUIStyle_get_margin_m4866 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t666_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(520);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t666 * L_0 = (__this->___m_Margin_11);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetRectOffsetPtr_m4882(__this, 1, /*hidden argument*/NULL);
		RectOffset_t666 * L_2 = (RectOffset_t666 *)il2cpp_codegen_object_new (RectOffset_t666_il2cpp_TypeInfo_var);
		RectOffset__ctor_m4851(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Margin_11 = L_2;
	}

IL_001e:
	{
		RectOffset_t666 * L_3 = (__this->___m_Margin_11);
		return L_3;
	}
}
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_padding()
extern TypeInfo* RectOffset_t666_il2cpp_TypeInfo_var;
extern "C" RectOffset_t666 * GUIStyle_get_padding_m4867 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t666_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(520);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t666 * L_0 = (__this->___m_Padding_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetRectOffsetPtr_m4882(__this, 2, /*hidden argument*/NULL);
		RectOffset_t666 * L_2 = (RectOffset_t666 *)il2cpp_codegen_object_new (RectOffset_t666_il2cpp_TypeInfo_var);
		RectOffset__ctor_m4851(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Padding_10 = L_2;
	}

IL_001e:
	{
		RectOffset_t666 * L_3 = (__this->___m_Padding_10);
		return L_3;
	}
}
// System.Void UnityEngine.GUIStyle::Internal_Draw(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo* Internal_DrawArguments_t922_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_Draw_m4868 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t267  ___position, GUIContent_t379 * ___content, bool ___isHover, bool ___isActive, bool ___on, bool ___hasKeyboardFocus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Internal_DrawArguments_t922_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	Internal_DrawArguments_t922  V_0 = {0};
	Internal_DrawArguments_t922 * G_B2_0 = {0};
	Internal_DrawArguments_t922 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Internal_DrawArguments_t922 * G_B3_1 = {0};
	Internal_DrawArguments_t922 * G_B5_0 = {0};
	Internal_DrawArguments_t922 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Internal_DrawArguments_t922 * G_B6_1 = {0};
	Internal_DrawArguments_t922 * G_B8_0 = {0};
	Internal_DrawArguments_t922 * G_B7_0 = {0};
	int32_t G_B9_0 = 0;
	Internal_DrawArguments_t922 * G_B9_1 = {0};
	Internal_DrawArguments_t922 * G_B11_0 = {0};
	Internal_DrawArguments_t922 * G_B10_0 = {0};
	int32_t G_B12_0 = 0;
	Internal_DrawArguments_t922 * G_B12_1 = {0};
	{
		Initobj (Internal_DrawArguments_t922_il2cpp_TypeInfo_var, (&V_0));
		IntPtr_t L_0 = ___target;
		(&V_0)->___target_0 = L_0;
		Rect_t267  L_1 = ___position;
		(&V_0)->___position_1 = L_1;
		bool L_2 = ___isHover;
		G_B1_0 = (&V_0);
		if (!L_2)
		{
			G_B2_0 = (&V_0);
			goto IL_0026;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_0027;
	}

IL_0026:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0027:
	{
		G_B3_1->___isHover_2 = G_B3_0;
		bool L_3 = ___isActive;
		G_B4_0 = (&V_0);
		if (!L_3)
		{
			G_B5_0 = (&V_0);
			goto IL_003b;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_003c;
	}

IL_003b:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_003c:
	{
		G_B6_1->___isActive_3 = G_B6_0;
		bool L_4 = ___on;
		G_B7_0 = (&V_0);
		if (!L_4)
		{
			G_B8_0 = (&V_0);
			goto IL_0050;
		}
	}
	{
		G_B9_0 = 1;
		G_B9_1 = G_B7_0;
		goto IL_0051;
	}

IL_0050:
	{
		G_B9_0 = 0;
		G_B9_1 = G_B8_0;
	}

IL_0051:
	{
		G_B9_1->___on_4 = G_B9_0;
		bool L_5 = ___hasKeyboardFocus;
		G_B10_0 = (&V_0);
		if (!L_5)
		{
			G_B11_0 = (&V_0);
			goto IL_0065;
		}
	}
	{
		G_B12_0 = 1;
		G_B12_1 = G_B10_0;
		goto IL_0066;
	}

IL_0065:
	{
		G_B12_0 = 0;
		G_B12_1 = G_B11_0;
	}

IL_0066:
	{
		G_B12_1->___hasKeyboardFocus_5 = G_B12_0;
		GUIContent_t379 * L_6 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw_m4891(NULL /*static, unused*/, L_6, (&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,System.String,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Draw_m1412 (GUIStyle_t342 * __this, Rect_t267  ___position, String_t* ___text, bool ___isHover, bool ___isActive, bool ___on, bool ___hasKeyboardFocus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		Rect_t267  L_1 = ___position;
		String_t* L_2 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent_t379 * L_3 = GUIContent_Temp_m4724(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_4 = ___isHover;
		bool L_5 = ___isActive;
		bool L_6 = ___on;
		bool L_7 = ___hasKeyboardFocus;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw_m4868(NULL /*static, unused*/, L_0, L_1, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,UnityEngine.GUIContent,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Draw_m1408 (GUIStyle_t342 * __this, Rect_t267  ___position, GUIContent_t379 * ___content, bool ___isHover, bool ___isActive, bool ___on, bool ___hasKeyboardFocus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		Rect_t267  L_1 = ___position;
		GUIContent_t379 * L_2 = ___content;
		bool L_3 = ___isHover;
		bool L_4 = ___isActive;
		bool L_5 = ___on;
		bool L_6 = ___hasKeyboardFocus;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw_m4868(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void GUIStyle_Draw_m4869 (GUIStyle_t342 * __this, Rect_t267  ___position, GUIContent_t379 * ___content, int32_t ___controlID, const MethodInfo* method)
{
	{
		Rect_t267  L_0 = ___position;
		GUIContent_t379 * L_1 = ___content;
		int32_t L_2 = ___controlID;
		GUIStyle_Draw_m4870(__this, L_0, L_1, L_2, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Boolean)
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral673;
extern "C" void GUIStyle_Draw_m4870 (GUIStyle_t342 * __this, Rect_t267  ___position, GUIContent_t379 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral673 = il2cpp_codegen_string_literal_from_index(673);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t379 * L_0 = ___content;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_1 = (__this->___m_Ptr_0);
		Rect_t267  L_2 = ___position;
		GUIContent_t379 * L_3 = ___content;
		int32_t L_4 = ___controlID;
		bool L_5 = ___on;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw2_m4892(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_001b:
	{
		Debug_LogError_m1317(NULL /*static, unused*/, _stringLiteral673, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUIStyle::get_none()
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t342 * GUIStyle_get_none_m4871 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_0 = ((GUIStyle_t342_StaticFields*)GUIStyle_t342_il2cpp_TypeInfo_var->static_fields)->___s_None_15;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t342 * L_1 = (GUIStyle_t342 *)il2cpp_codegen_object_new (GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4862(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		((GUIStyle_t342_StaticFields*)GUIStyle_t342_il2cpp_TypeInfo_var->static_fields)->___s_None_15 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_2 = ((GUIStyle_t342_StaticFields*)GUIStyle_t342_il2cpp_TypeInfo_var->static_fields)->___s_None_15;
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.GUIStyle::CalcSize(UnityEngine.GUIContent)
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" Vector2_t2  GUIStyle_CalcSize_m4872 (GUIStyle_t342 * __this, GUIContent_t379 * ___content, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2  V_0 = {0};
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t379 * L_1 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_Internal_CalcSize_m4895(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector2_t2  L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.GUIStyle::CalcHeight(UnityEngine.GUIContent,System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" float GUIStyle_CalcHeight_m4873 (GUIStyle_t342 * __this, GUIContent_t379 * ___content, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t379 * L_1 = ___content;
		float L_2 = ___width;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		float L_3 = GUIStyle_Internal_CalcHeight_m4896(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.GUIStyle::get_isHeightDependantOnWidth()
extern "C" bool GUIStyle_get_isHeightDependantOnWidth_m4874 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		float L_0 = GUIStyle_get_fixedHeight_m4886(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_002c;
		}
	}
	{
		bool L_1 = GUIStyle_get_wordWrap_m4884(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = GUIStyle_get_imagePosition_m4883(__this, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002a;
	}

IL_0029:
	{
		G_B4_0 = 0;
	}

IL_002a:
	{
		G_B6_0 = G_B4_0;
		goto IL_002d;
	}

IL_002c:
	{
		G_B6_0 = 0;
	}

IL_002d:
	{
		return G_B6_0;
	}
}
// System.Void UnityEngine.GUIStyle::CalcMinMaxWidth(UnityEngine.GUIContent,System.Single&,System.Single&)
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_CalcMinMaxWidth_m4875 (GUIStyle_t342 * __this, GUIContent_t379 * ___content, float* ___minWidth, float* ___maxWidth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t379 * L_1 = ___content;
		float* L_2 = ___minWidth;
		float* L_3 = ___maxWidth;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_Internal_CalcMinMaxWidth_m4897(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.GUIStyle::ToString()
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral674;
extern "C" String_t* GUIStyle_ToString_m4876 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral674 = il2cpp_codegen_string_literal_from_index(674);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t356* L_0 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 1));
		String_t* L_1 = GUIStyle_get_name_m4879(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		String_t* L_2 = UnityString_Format_m4240(NULL /*static, unused*/, _stringLiteral674, L_0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.GUIStyle::Init()
extern "C" void GUIStyle_Init_m4877 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyle_Init_m4877_ftn) (GUIStyle_t342 *);
	static GUIStyle_Init_m4877_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Init_m4877_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::Cleanup()
extern "C" void GUIStyle_Cleanup_m4878 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyle_Cleanup_m4878_ftn) (GUIStyle_t342 *);
	static GUIStyle_Cleanup_m4878_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Cleanup_m4878_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.String UnityEngine.GUIStyle::get_name()
extern "C" String_t* GUIStyle_get_name_m4879 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	typedef String_t* (*GUIStyle_get_name_m4879_ftn) (GUIStyle_t342 *);
	static GUIStyle_get_name_m4879_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_name_m4879_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_name(System.String)
extern "C" void GUIStyle_set_name_m4880 (GUIStyle_t342 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_name_m4880_ftn) (GUIStyle_t342 *, String_t*);
	static GUIStyle_set_name_m4880_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_name_m4880_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.IntPtr UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)
extern "C" IntPtr_t GUIStyle_GetStyleStatePtr_m4881 (GUIStyle_t342 * __this, int32_t ___idx, const MethodInfo* method)
{
	typedef IntPtr_t (*GUIStyle_GetStyleStatePtr_m4881_ftn) (GUIStyle_t342 *, int32_t);
	static GUIStyle_GetStyleStatePtr_m4881_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetStyleStatePtr_m4881_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)");
	return _il2cpp_icall_func(__this, ___idx);
}
// System.IntPtr UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)
extern "C" IntPtr_t GUIStyle_GetRectOffsetPtr_m4882 (GUIStyle_t342 * __this, int32_t ___idx, const MethodInfo* method)
{
	typedef IntPtr_t (*GUIStyle_GetRectOffsetPtr_m4882_ftn) (GUIStyle_t342 *, int32_t);
	static GUIStyle_GetRectOffsetPtr_m4882_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetRectOffsetPtr_m4882_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)");
	return _il2cpp_icall_func(__this, ___idx);
}
// UnityEngine.ImagePosition UnityEngine.GUIStyle::get_imagePosition()
extern "C" int32_t GUIStyle_get_imagePosition_m4883 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	typedef int32_t (*GUIStyle_get_imagePosition_m4883_ftn) (GUIStyle_t342 *);
	static GUIStyle_get_imagePosition_m4883_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_imagePosition_m4883_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_imagePosition()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.GUIStyle::get_wordWrap()
extern "C" bool GUIStyle_get_wordWrap_m4884 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_wordWrap_m4884_ftn) (GUIStyle_t342 *);
	static GUIStyle_get_wordWrap_m4884_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_wordWrap_m4884_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_wordWrap()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.GUIStyle::get_fixedWidth()
extern "C" float GUIStyle_get_fixedWidth_m4885 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	typedef float (*GUIStyle_get_fixedWidth_m4885_ftn) (GUIStyle_t342 *);
	static GUIStyle_get_fixedWidth_m4885_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_fixedWidth_m4885_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_fixedWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.GUIStyle::get_fixedHeight()
extern "C" float GUIStyle_get_fixedHeight_m4886 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	typedef float (*GUIStyle_get_fixedHeight_m4886_ftn) (GUIStyle_t342 *);
	static GUIStyle_get_fixedHeight_m4886_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_fixedHeight_m4886_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_fixedHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.GUIStyle::get_stretchWidth()
extern "C" bool GUIStyle_get_stretchWidth_m4887 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_stretchWidth_m4887_ftn) (GUIStyle_t342 *);
	static GUIStyle_get_stretchWidth_m4887_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_stretchWidth_m4887_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_stretchWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
extern "C" void GUIStyle_set_stretchWidth_m4888 (GUIStyle_t342 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_stretchWidth_m4888_ftn) (GUIStyle_t342 *, bool);
	static GUIStyle_set_stretchWidth_m4888_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_stretchWidth_m4888_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GUIStyle::get_stretchHeight()
extern "C" bool GUIStyle_get_stretchHeight_m4889 (GUIStyle_t342 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_stretchHeight_m4889_ftn) (GUIStyle_t342 *);
	static GUIStyle_get_stretchHeight_m4889_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_stretchHeight_m4889_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_stretchHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)
extern "C" void GUIStyle_set_stretchHeight_m4890 (GUIStyle_t342 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_stretchHeight_m4890_ftn) (GUIStyle_t342 *, bool);
	static GUIStyle_set_stretchHeight_m4890_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_stretchHeight_m4890_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUIStyle::Internal_Draw(UnityEngine.GUIContent,UnityEngine.Internal_DrawArguments&)
extern "C" void GUIStyle_Internal_Draw_m4891 (Object_t * __this /* static, unused */, GUIContent_t379 * ___content, Internal_DrawArguments_t922 * ___arguments, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_Draw_m4891_ftn) (GUIContent_t379 *, Internal_DrawArguments_t922 *);
	static GUIStyle_Internal_Draw_m4891_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_Draw_m4891_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_Draw(UnityEngine.GUIContent,UnityEngine.Internal_DrawArguments&)");
	_il2cpp_icall_func(___content, ___arguments);
}
// System.Void UnityEngine.GUIStyle::Internal_Draw2(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Boolean)
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_Draw2_m4892 (Object_t * __this /* static, unused */, IntPtr_t ___style, Rect_t267  ___position, GUIContent_t379 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___style;
		GUIContent_t379 * L_1 = ___content;
		int32_t L_2 = ___controlID;
		bool L_3 = ___on;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_INTERNAL_CALL_Internal_Draw2_m4893(NULL /*static, unused*/, L_0, (&___position), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)
extern "C" void GUIStyle_INTERNAL_CALL_Internal_Draw2_m4893 (Object_t * __this /* static, unused */, IntPtr_t ___style, Rect_t267 * ___position, GUIContent_t379 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_CALL_Internal_Draw2_m4893_ftn) (IntPtr_t, Rect_t267 *, GUIContent_t379 *, int32_t, bool);
	static GUIStyle_INTERNAL_CALL_Internal_Draw2_m4893_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_Draw2_m4893_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)");
	_il2cpp_icall_func(___style, ___position, ___content, ___controlID, ___on);
}
// System.Void UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
extern "C" void GUIStyle_SetDefaultFont_m4894 (Object_t * __this /* static, unused */, Font_t569 * ___font, const MethodInfo* method)
{
	typedef void (*GUIStyle_SetDefaultFont_m4894_ftn) (Font_t569 *);
	static GUIStyle_SetDefaultFont_m4894_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_SetDefaultFont_m4894_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)");
	_il2cpp_icall_func(___font);
}
// System.Void UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)
extern "C" void GUIStyle_Internal_CalcSize_m4895 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t379 * ___content, Vector2_t2 * ___ret, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_CalcSize_m4895_ftn) (IntPtr_t, GUIContent_t379 *, Vector2_t2 *);
	static GUIStyle_Internal_CalcSize_m4895_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcSize_m4895_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___target, ___content, ___ret);
}
// System.Single UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)
extern "C" float GUIStyle_Internal_CalcHeight_m4896 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t379 * ___content, float ___width, const MethodInfo* method)
{
	typedef float (*GUIStyle_Internal_CalcHeight_m4896_ftn) (IntPtr_t, GUIContent_t379 *, float);
	static GUIStyle_Internal_CalcHeight_m4896_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcHeight_m4896_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)");
	return _il2cpp_icall_func(___target, ___content, ___width);
}
// System.Void UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)
extern "C" void GUIStyle_Internal_CalcMinMaxWidth_m4897 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t379 * ___content, float* ___minWidth, float* ___maxWidth, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_CalcMinMaxWidth_m4897_ftn) (IntPtr_t, GUIContent_t379 *, float*, float*);
	static GUIStyle_Internal_CalcMinMaxWidth_m4897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcMinMaxWidth_m4897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)");
	_il2cpp_icall_func(___target, ___content, ___minWidth, ___maxWidth);
}
// UnityEngine.GUIStyle UnityEngine.GUIStyle::op_Implicit(System.String)
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
extern TypeInfo* GUISkin_t901_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral675;
extern "C" GUIStyle_t342 * GUIStyle_op_Implicit_m1400 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		_stringLiteral675 = il2cpp_codegen_string_literal_from_index(675);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISkin_t901 * L_0 = ((GUISkin_t901_StaticFields*)GUISkin_t901_il2cpp_TypeInfo_var->static_fields)->___current_28;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Debug_LogError_m1317(NULL /*static, unused*/, _stringLiteral675, /*hidden argument*/NULL);
		GUIStyle_t342 * L_2 = GUISkin_get_error_m4836(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0020:
	{
		GUISkin_t901 * L_3 = ((GUISkin_t901_StaticFields*)GUISkin_t901_il2cpp_TypeInfo_var->static_fields)->___current_28;
		String_t* L_4 = ___str;
		NullCheck(L_3);
		GUIStyle_t342 * L_5 = GUISkin_GetStyle_m4839(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.ExitGUIException
#include "UnityEngine_UnityEngine_ExitGUIException.h"
// UnityEngine.ExitGUIException
#include "UnityEngine_UnityEngine_ExitGUIExceptionMethodDeclarations.h"
// UnityEngine.FocusType
#include "UnityEngine_UnityEngine_FocusTypeMethodDeclarations.h"
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtility.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// System.Void UnityEngine.GUIUtility::.cctor()
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern "C" void GUIUtility__cctor_m4898 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t2  L_0 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		((GUIUtility_t750_StaticFields*)GUIUtility_t750_il2cpp_TypeInfo_var->static_fields)->___s_EditorScreenPointOffset_2 = L_0;
		((GUIUtility_t750_StaticFields*)GUIUtility_t750_il2cpp_TypeInfo_var->static_fields)->___s_HasKeyboardFocus_3 = 0;
		return;
	}
}
// System.Single UnityEngine.GUIUtility::get_pixelsPerPoint()
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern "C" float GUIUtility_get_pixelsPerPoint_m4899 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		float L_0 = GUIUtility_Internal_GetPixelsPerPoint_m4905(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GUISkin UnityEngine.GUIUtility::GetDefaultSkin()
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern "C" GUISkin_t901 * GUIUtility_GetDefaultSkin_m4900 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUIUtility_t750_StaticFields*)GUIUtility_t750_il2cpp_TypeInfo_var->static_fields)->___s_SkinMode_0;
		GUISkin_t901 * L_1 = GUIUtility_Internal_GetDefaultSkin_m4907(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GUIUtility::BeginGUI(System.Int32,System.Int32,System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t457_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_BeginGUI_m4901 (Object_t * __this /* static, unused */, int32_t ___skinMode, int32_t ___instanceID, int32_t ___useGUILayout, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		GUI_t457_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___skinMode;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		((GUIUtility_t750_StaticFields*)GUIUtility_t750_il2cpp_TypeInfo_var->static_fields)->___s_SkinMode_0 = L_0;
		int32_t L_1 = ___instanceID;
		((GUIUtility_t750_StaticFields*)GUIUtility_t750_il2cpp_TypeInfo_var->static_fields)->___s_OriginalID_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUI_set_skin_m4707(NULL /*static, unused*/, (GUISkin_t901 *)NULL, /*hidden argument*/NULL);
		int32_t L_2 = ___useGUILayout;
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_3 = ___instanceID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		GUILayoutUtility_SelectIDList_m4733(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		int32_t L_4 = ___instanceID;
		GUILayoutUtility_Begin_m4734(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t457_il2cpp_TypeInfo_var);
		GUI_set_changed_m4714(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIUtility::EndGUI(System.Int32)
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_EndGUI_m4902 (Object_t * __this /* static, unused */, int32_t ___layoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Event_t381 * L_0 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_0);
			int32_t L_1 = Event_get_type_m1405(L_0, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_1) == ((uint32_t)8))))
			{
				goto IL_0042;
			}
		}

IL_0010:
		{
			int32_t L_2 = ___layoutType;
			V_0 = L_2;
			int32_t L_3 = V_0;
			if (L_3 == 0)
			{
				goto IL_0029;
			}
			if (L_3 == 1)
			{
				goto IL_002e;
			}
			if (L_3 == 2)
			{
				goto IL_0038;
			}
		}

IL_0024:
		{
			goto IL_0042;
		}

IL_0029:
		{
			goto IL_0042;
		}

IL_002e:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
			GUILayoutUtility_Layout_m4737(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0042;
		}

IL_0038:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
			GUILayoutUtility_LayoutFromEditorWindow_m4738(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0042;
		}

IL_0042:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
			int32_t L_4 = ((GUIUtility_t750_StaticFields*)GUIUtility_t750_il2cpp_TypeInfo_var->static_fields)->___s_OriginalID_1;
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
			GUILayoutUtility_SelectIDList_m4733(NULL /*static, unused*/, L_4, 0, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t379_il2cpp_TypeInfo_var);
			GUIContent_ClearStaticCache_m4725(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x5E, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		GUIUtility_Internal_ExitGUI_m4908(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_005e:
	{
		return;
	}
}
// System.Boolean UnityEngine.GUIUtility::EndGUIFromException(System.Exception)
// System.Exception
#include "mscorlib_System_Exception.h"
extern TypeInfo* ExitGUIException_t920_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern "C" bool GUIUtility_EndGUIFromException_m4903 (Object_t * __this /* static, unused */, Exception_t359 * ___exception, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExitGUIException_t920_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(614);
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t359 * L_0 = ___exception;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Exception_t359 * L_1 = ___exception;
		if (((ExitGUIException_t920 *)IsInstSealed(L_1, ExitGUIException_t920_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		Exception_t359 * L_2 = ___exception;
		NullCheck(L_2);
		Exception_t359 * L_3 = (Exception_t359 *)VirtFuncInvoker0< Exception_t359 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_2);
		if (((ExitGUIException_t920 *)IsInstSealed(L_3, ExitGUIException_t920_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		return 0;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		GUIUtility_Internal_ExitGUI_m4908(NULL /*static, unused*/, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Void UnityEngine.GUIUtility::CheckOnGUI()
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern TypeInfo* GUIUtility_t750_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral676;
extern "C" void GUIUtility_CheckOnGUI_m4904 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t750_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(467);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral676 = il2cpp_codegen_string_literal_from_index(676);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t750_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_Internal_GetGUIDepth_m4909(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t764 * L_1 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_1, _stringLiteral676, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Single UnityEngine.GUIUtility::Internal_GetPixelsPerPoint()
extern "C" float GUIUtility_Internal_GetPixelsPerPoint_m4905 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*GUIUtility_Internal_GetPixelsPerPoint_m4905_ftn) ();
	static GUIUtility_Internal_GetPixelsPerPoint_m4905_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetPixelsPerPoint_m4905_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetPixelsPerPoint()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)
// UnityEngine.FocusType
#include "UnityEngine_UnityEngine_FocusType.h"
extern "C" int32_t GUIUtility_GetControlID_m4906 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___focus, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_GetControlID_m4906_ftn) (int32_t, int32_t);
	static GUIUtility_GetControlID_m4906_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_GetControlID_m4906_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)");
	return _il2cpp_icall_func(___hint, ___focus);
}
// System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
extern "C" String_t* GUIUtility_get_systemCopyBuffer_m3599 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GUIUtility_get_systemCopyBuffer_m3599_ftn) ();
	static GUIUtility_get_systemCopyBuffer_m3599_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_get_systemCopyBuffer_m3599_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::get_systemCopyBuffer()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" void GUIUtility_set_systemCopyBuffer_m3600 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_systemCopyBuffer_m3600_ftn) (String_t*);
	static GUIUtility_set_systemCopyBuffer_m3600_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_systemCopyBuffer_m3600_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.GUISkin UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)
extern "C" GUISkin_t901 * GUIUtility_Internal_GetDefaultSkin_m4907 (Object_t * __this /* static, unused */, int32_t ___skinMode, const MethodInfo* method)
{
	typedef GUISkin_t901 * (*GUIUtility_Internal_GetDefaultSkin_m4907_ftn) (int32_t);
	static GUIUtility_Internal_GetDefaultSkin_m4907_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetDefaultSkin_m4907_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)");
	return _il2cpp_icall_func(___skinMode);
}
// System.Void UnityEngine.GUIUtility::Internal_ExitGUI()
extern "C" void GUIUtility_Internal_ExitGUI_m4908 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GUIUtility_Internal_ExitGUI_m4908_ftn) ();
	static GUIUtility_Internal_ExitGUI_m4908_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_ExitGUI_m4908_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_ExitGUI()");
	_il2cpp_icall_func();
}
// System.Int32 UnityEngine.GUIUtility::Internal_GetGUIDepth()
extern "C" int32_t GUIUtility_Internal_GetGUIDepth_m4909 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_Internal_GetGUIDepth_m4909_ftn) ();
	static GUIUtility_Internal_GetGUIDepth_m4909_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetGUIDepth_m4909_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetGUIDepth()");
	return _il2cpp_icall_func();
}
// UnityEngine.Internal_DrawArguments
#include "UnityEngine_UnityEngine_Internal_DrawArgumentsMethodDeclarations.h"
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"
// System.Void UnityEngine.WrapperlessIcall::.ctor()
// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"
extern "C" void WrapperlessIcall__ctor_m4910 (WrapperlessIcall_t923 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m5125(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"
extern "C" void IL2CPPStructAlignmentAttribute__ctor_m4911 (IL2CPPStructAlignmentAttribute_t924 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m5125(__this, /*hidden argument*/NULL);
		__this->___Align_0 = 1;
		return;
	}
}
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// System.Collections.Generic.Stack`1<System.Type>
#include "System_System_Collections_Generic_Stack_1_gen_1.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// System.Collections.Generic.List`1<System.Type>
#include "mscorlib_System_Collections_Generic_List_1_gen_47.h"
// System.Collections.Generic.Stack`1<System.Type>
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
// System.Collections.Generic.List`1<System.Type>
#include "mscorlib_System_Collections_Generic_List_1_gen_47MethodDeclarations.h"
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern TypeInfo* DisallowMultipleComponentU5BU5D_t926_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeHelperEngine_t925_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditModeU5BU5D_t927_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponentU5BU5D_t928_il2cpp_TypeInfo_var;
extern "C" void AttributeHelperEngine__cctor_m4912 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisallowMultipleComponentU5BU5D_t926_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(615);
		AttributeHelperEngine_t925_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(617);
		ExecuteInEditModeU5BU5D_t927_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		RequireComponentU5BU5D_t928_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(620);
		s_Il2CppMethodIntialized = true;
	}
	{
		((AttributeHelperEngine_t925_StaticFields*)AttributeHelperEngine_t925_il2cpp_TypeInfo_var->static_fields)->____disallowMultipleComponentArray_0 = ((DisallowMultipleComponentU5BU5D_t926*)SZArrayNew(DisallowMultipleComponentU5BU5D_t926_il2cpp_TypeInfo_var, 1));
		((AttributeHelperEngine_t925_StaticFields*)AttributeHelperEngine_t925_il2cpp_TypeInfo_var->static_fields)->____executeInEditModeArray_1 = ((ExecuteInEditModeU5BU5D_t927*)SZArrayNew(ExecuteInEditModeU5BU5D_t927_il2cpp_TypeInfo_var, 1));
		((AttributeHelperEngine_t925_StaticFields*)AttributeHelperEngine_t925_il2cpp_TypeInfo_var->static_fields)->____requireComponentArray_2 = ((RequireComponentU5BU5D_t928*)SZArrayNew(RequireComponentU5BU5D_t928_il2cpp_TypeInfo_var, 1));
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
// System.Type
#include "mscorlib_System_Type.h"
// System.Collections.Generic.Stack`1<System.Type>
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
extern const Il2CppType* MonoBehaviour_t5_0_0_0_var;
extern const Il2CppType* DisallowMultipleComponent_t929_0_0_0_var;
extern TypeInfo* Stack_1_t1027_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m5156_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m5157_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m5158_MethodInfo_var;
extern "C" Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m4913 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviour_t5_0_0_0_var = il2cpp_codegen_type_from_index(18);
		DisallowMultipleComponent_t929_0_0_0_var = il2cpp_codegen_type_from_index(616);
		Stack_1_t1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		Stack_1__ctor_m5156_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484237);
		Stack_1_Push_m5157_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484238);
		Stack_1_Pop_m5158_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484239);
		s_Il2CppMethodIntialized = true;
	}
	Stack_1_t1027 * V_0 = {0};
	Type_t * V_1 = {0};
	ObjectU5BU5D_t356* V_2 = {0};
	int32_t V_3 = 0;
	{
		Stack_1_t1027 * L_0 = (Stack_1_t1027 *)il2cpp_codegen_object_new (Stack_1_t1027_il2cpp_TypeInfo_var);
		Stack_1__ctor_m5156(L_0, /*hidden argument*/Stack_1__ctor_m5156_MethodInfo_var);
		V_0 = L_0;
		goto IL_001a;
	}

IL_000b:
	{
		Stack_1_t1027 * L_1 = V_0;
		Type_t * L_2 = ___type;
		NullCheck(L_1);
		Stack_1_Push_m5157(L_1, L_2, /*hidden argument*/Stack_1_Push_m5157_MethodInfo_var);
		Type_t * L_3 = ___type;
		NullCheck(L_3);
		Type_t * L_4 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type = L_4;
	}

IL_001a:
	{
		Type_t * L_5 = ___type;
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_6 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t5_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_6) == ((Object_t*)(Type_t *)L_7))))
		{
			goto IL_000b;
		}
	}

IL_0030:
	{
		V_1 = (Type_t *)NULL;
		goto IL_005c;
	}

IL_0037:
	{
		Stack_1_t1027 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m5158(L_8, /*hidden argument*/Stack_1_Pop_m5158_MethodInfo_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t929_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t356* L_12 = (ObjectU5BU5D_t356*)VirtFuncInvoker2< ObjectU5BU5D_t356*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, 0);
		V_2 = L_12;
		ObjectU5BU5D_t356* L_13 = V_2;
		NullCheck(L_13);
		V_3 = (((int32_t)(((Array_t *)L_13)->max_length)));
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_005c;
		}
	}
	{
		Type_t * L_15 = V_1;
		return L_15;
	}

IL_005c:
	{
		Stack_1_t1027 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count() */, L_16);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		return (Type_t *)NULL;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
// System.Collections.Generic.List`1<System.Type>
#include "mscorlib_System_Collections_Generic_List_1_gen_47MethodDeclarations.h"
extern const Il2CppType* RequireComponent_t930_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t5_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponentU5BU5D_t928_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1005_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1028_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m5159_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m5160_MethodInfo_var;
extern "C" TypeU5BU5D_t1005* AttributeHelperEngine_GetRequiredComponents_m4914 (Object_t * __this /* static, unused */, Type_t * ___klass, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RequireComponent_t930_0_0_0_var = il2cpp_codegen_type_from_index(621);
		MonoBehaviour_t5_0_0_0_var = il2cpp_codegen_type_from_index(18);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		RequireComponentU5BU5D_t928_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(620);
		TypeU5BU5D_t1005_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(623);
		List_1_t1028_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(624);
		List_1__ctor_m5159_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484240);
		List_1_ToArray_m5160_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484241);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1028 * V_0 = {0};
	RequireComponentU5BU5D_t928* V_1 = {0};
	Type_t * V_2 = {0};
	RequireComponent_t930 * V_3 = {0};
	RequireComponentU5BU5D_t928* V_4 = {0};
	int32_t V_5 = 0;
	TypeU5BU5D_t1005* V_6 = {0};
	{
		V_0 = (List_1_t1028 *)NULL;
		goto IL_00e0;
	}

IL_0007:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t930_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t356* L_2 = (ObjectU5BU5D_t356*)VirtFuncInvoker2< ObjectU5BU5D_t356*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_1 = ((RequireComponentU5BU5D_t928*)Castclass(L_2, RequireComponentU5BU5D_t928_il2cpp_TypeInfo_var));
		Type_t * L_3 = ___klass;
		NullCheck(L_3);
		Type_t * L_4 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		V_2 = L_4;
		RequireComponentU5BU5D_t928* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_00d2;
	}

IL_0030:
	{
		RequireComponentU5BU5D_t928* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_3 = (*(RequireComponent_t930 **)(RequireComponent_t930 **)SZArrayLdElema(L_6, L_8, sizeof(RequireComponent_t930 *)));
		List_1_t1028 * L_9 = V_0;
		if (L_9)
		{
			goto IL_007b;
		}
	}
	{
		RequireComponentU5BU5D_t928* L_10 = V_1;
		NullCheck(L_10);
		if ((!(((uint32_t)(((int32_t)(((Array_t *)L_10)->max_length)))) == ((uint32_t)1))))
		{
			goto IL_007b;
		}
	}
	{
		Type_t * L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t5_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_11) == ((Object_t*)(Type_t *)L_12))))
		{
			goto IL_007b;
		}
	}
	{
		TypeU5BU5D_t1005* L_13 = ((TypeU5BU5D_t1005*)SZArrayNew(TypeU5BU5D_t1005_il2cpp_TypeInfo_var, 3));
		RequireComponent_t930 * L_14 = V_3;
		NullCheck(L_14);
		Type_t * L_15 = (L_14->___m_Type0_0);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		ArrayElementTypeCheck (L_13, L_15);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_13, 0, sizeof(Type_t *))) = (Type_t *)L_15;
		TypeU5BU5D_t1005* L_16 = L_13;
		RequireComponent_t930 * L_17 = V_3;
		NullCheck(L_17);
		Type_t * L_18 = (L_17->___m_Type1_1);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_18);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_16, 1, sizeof(Type_t *))) = (Type_t *)L_18;
		TypeU5BU5D_t1005* L_19 = L_16;
		RequireComponent_t930 * L_20 = V_3;
		NullCheck(L_20);
		Type_t * L_21 = (L_20->___m_Type2_2);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		ArrayElementTypeCheck (L_19, L_21);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_19, 2, sizeof(Type_t *))) = (Type_t *)L_21;
		V_6 = L_19;
		TypeU5BU5D_t1005* L_22 = V_6;
		return L_22;
	}

IL_007b:
	{
		List_1_t1028 * L_23 = V_0;
		if (L_23)
		{
			goto IL_0087;
		}
	}
	{
		List_1_t1028 * L_24 = (List_1_t1028 *)il2cpp_codegen_object_new (List_1_t1028_il2cpp_TypeInfo_var);
		List_1__ctor_m5159(L_24, /*hidden argument*/List_1__ctor_m5159_MethodInfo_var);
		V_0 = L_24;
	}

IL_0087:
	{
		RequireComponent_t930 * L_25 = V_3;
		NullCheck(L_25);
		Type_t * L_26 = (L_25->___m_Type0_0);
		if (!L_26)
		{
			goto IL_009e;
		}
	}
	{
		List_1_t1028 * L_27 = V_0;
		RequireComponent_t930 * L_28 = V_3;
		NullCheck(L_28);
		Type_t * L_29 = (L_28->___m_Type0_0);
		NullCheck(L_27);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_27, L_29);
	}

IL_009e:
	{
		RequireComponent_t930 * L_30 = V_3;
		NullCheck(L_30);
		Type_t * L_31 = (L_30->___m_Type1_1);
		if (!L_31)
		{
			goto IL_00b5;
		}
	}
	{
		List_1_t1028 * L_32 = V_0;
		RequireComponent_t930 * L_33 = V_3;
		NullCheck(L_33);
		Type_t * L_34 = (L_33->___m_Type1_1);
		NullCheck(L_32);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_32, L_34);
	}

IL_00b5:
	{
		RequireComponent_t930 * L_35 = V_3;
		NullCheck(L_35);
		Type_t * L_36 = (L_35->___m_Type2_2);
		if (!L_36)
		{
			goto IL_00cc;
		}
	}
	{
		List_1_t1028 * L_37 = V_0;
		RequireComponent_t930 * L_38 = V_3;
		NullCheck(L_38);
		Type_t * L_39 = (L_38->___m_Type2_2);
		NullCheck(L_37);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_37, L_39);
	}

IL_00cc:
	{
		int32_t L_40 = V_5;
		V_5 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00d2:
	{
		int32_t L_41 = V_5;
		RequireComponentU5BU5D_t928* L_42 = V_4;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)(((Array_t *)L_42)->max_length))))))
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_43 = V_2;
		___klass = L_43;
	}

IL_00e0:
	{
		Type_t * L_44 = ___klass;
		if (!L_44)
		{
			goto IL_00f6;
		}
	}
	{
		Type_t * L_45 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_46 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t5_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_45) == ((Object_t*)(Type_t *)L_46))))
		{
			goto IL_0007;
		}
	}

IL_00f6:
	{
		List_1_t1028 * L_47 = V_0;
		if (L_47)
		{
			goto IL_00fe;
		}
	}
	{
		return (TypeU5BU5D_t1005*)NULL;
	}

IL_00fe:
	{
		List_1_t1028 * L_48 = V_0;
		NullCheck(L_48);
		TypeU5BU5D_t1005* L_49 = List_1_ToArray_m5160(L_48, /*hidden argument*/List_1_ToArray_m5160_MethodInfo_var);
		return L_49;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const Il2CppType* ExecuteInEditMode_t932_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t5_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" bool AttributeHelperEngine_CheckIsEditorScript_m4915 (Object_t * __this /* static, unused */, Type_t * ___klass, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t932_0_0_0_var = il2cpp_codegen_type_from_index(619);
		MonoBehaviour_t5_0_0_0_var = il2cpp_codegen_type_from_index(18);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t356* V_0 = {0};
	int32_t V_1 = 0;
	{
		goto IL_002b;
	}

IL_0005:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t932_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t356* L_2 = (ObjectU5BU5D_t356*)VirtFuncInvoker2< ObjectU5BU5D_t356*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_0 = L_2;
		ObjectU5BU5D_t356* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)(((Array_t *)L_3)->max_length)));
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		return 1;
	}

IL_0023:
	{
		Type_t * L_5 = ___klass;
		NullCheck(L_5);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_5);
		___klass = L_6;
	}

IL_002b:
	{
		Type_t * L_7 = ___klass;
		if (!L_7)
		{
			goto IL_0041;
		}
	}
	{
		Type_t * L_8 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t5_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_8) == ((Object_t*)(Type_t *)L_9))))
		{
			goto IL_0005;
		}
	}

IL_0041:
	{
		return 0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
