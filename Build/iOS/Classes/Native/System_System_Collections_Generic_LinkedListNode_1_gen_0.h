﻿#pragma once
#include <stdint.h>
// SwarmItem
struct SwarmItem_t124;
// System.Collections.Generic.LinkedList`1<SwarmItem>
struct LinkedList_1_t152;
// System.Collections.Generic.LinkedListNode`1<SwarmItem>
struct LinkedListNode_1_t408;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.LinkedListNode`1<SwarmItem>
struct  LinkedListNode_1_t408  : public Object_t
{
	// T System.Collections.Generic.LinkedListNode`1<SwarmItem>::item
	SwarmItem_t124 * ___item_0;
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<SwarmItem>::container
	LinkedList_1_t152 * ___container_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<SwarmItem>::forward
	LinkedListNode_1_t408 * ___forward_2;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<SwarmItem>::back
	LinkedListNode_1_t408 * ___back_3;
};
