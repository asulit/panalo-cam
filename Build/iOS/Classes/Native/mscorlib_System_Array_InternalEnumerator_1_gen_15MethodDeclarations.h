﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15920(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2573 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14611_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15921(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2573 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14612_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::Dispose()
#define InternalEnumerator_1_Dispose_m15922(__this, method) (( void (*) (InternalEnumerator_1_t2573 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14613_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15923(__this, method) (( bool (*) (InternalEnumerator_1_t2573 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14614_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.MeshFilter>::get_Current()
#define InternalEnumerator_1_get_Current_m15924(__this, method) (( MeshFilter_t398 * (*) (InternalEnumerator_1_t2573 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14615_gshared)(__this, method)
