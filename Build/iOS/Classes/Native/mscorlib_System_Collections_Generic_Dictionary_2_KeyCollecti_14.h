﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t134;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.Object>
struct  KeyCollection_t2629  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.Object>::dictionary
	Dictionary_2_t134 * ___dictionary_0;
};
