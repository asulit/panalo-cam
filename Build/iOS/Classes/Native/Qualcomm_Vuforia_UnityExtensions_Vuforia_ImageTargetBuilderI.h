﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableSource
struct TrackableSource_t1112;
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder.h"
// Vuforia.ImageTargetBuilderImpl
struct  ImageTargetBuilderImpl_t1111  : public ImageTargetBuilder_t1101
{
	// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::mTrackableSource
	TrackableSource_t1112 * ___mTrackableSource_0;
};
