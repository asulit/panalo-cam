﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3476;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_63.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m28773_gshared (Enumerator_t3482 * __this, Dictionary_2_t3476 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m28773(__this, ___host, method) (( void (*) (Enumerator_t3482 *, Dictionary_2_t3476 *, const MethodInfo*))Enumerator__ctor_m28773_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m28774_gshared (Enumerator_t3482 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m28774(__this, method) (( Object_t * (*) (Enumerator_t3482 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m28774_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void Enumerator_Dispose_m28775_gshared (Enumerator_t3482 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m28775(__this, method) (( void (*) (Enumerator_t3482 *, const MethodInfo*))Enumerator_Dispose_m28775_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m28776_gshared (Enumerator_t3482 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m28776(__this, method) (( bool (*) (Enumerator_t3482 *, const MethodInfo*))Enumerator_MoveNext_m28776_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m28777_gshared (Enumerator_t3482 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m28777(__this, method) (( Object_t * (*) (Enumerator_t3482 *, const MethodInfo*))Enumerator_get_Current_m28777_gshared)(__this, method)
