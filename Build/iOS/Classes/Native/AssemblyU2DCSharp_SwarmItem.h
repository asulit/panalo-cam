﻿#pragma once
#include <stdint.h>
// SwarmItemManager
struct SwarmItemManager_t111;
// UnityEngine.Transform
struct Transform_t35;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SwarmItem/STATE
#include "AssemblyU2DCSharp_SwarmItem_STATE.h"
// SwarmItem
struct  SwarmItem_t124  : public MonoBehaviour_t5
{
	// SwarmItem/STATE SwarmItem::_state
	int32_t ____state_2;
	// SwarmItemManager SwarmItem::_swarmItemManager
	SwarmItemManager_t111 * ____swarmItemManager_3;
	// UnityEngine.Transform SwarmItem::_thisTransform
	Transform_t35 * ____thisTransform_4;
	// System.Int32 SwarmItem::_prefabIndex
	int32_t ____prefabIndex_5;
	// System.Single SwarmItem::_lifeSpanLeft
	float ____lifeSpanLeft_6;
	// System.Boolean SwarmItem::_debugEvents
	bool ____debugEvents_7;
	// System.Single SwarmItem::minimumLifeSpan
	float ___minimumLifeSpan_8;
	// System.Single SwarmItem::maximumLifeSpan
	float ___maximumLifeSpan_9;
};
