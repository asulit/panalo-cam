﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t320;

// System.Void Vuforia.VuforiaBehaviour::.ctor()
extern "C" void VuforiaBehaviour__ctor_m1270 (VuforiaBehaviour_t320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaBehaviour::.cctor()
extern "C" void VuforiaBehaviour__cctor_m1271 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaBehaviour::Awake()
extern "C" void VuforiaBehaviour_Awake_m1272 (VuforiaBehaviour_t320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::get_Instance()
extern "C" VuforiaBehaviour_t320 * VuforiaBehaviour_get_Instance_m1273 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
