﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"

// System.Void System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m17424_gshared (KeyValuePair_2_t2690 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m17424(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2690 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m17424_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m17425_gshared (KeyValuePair_2_t2690 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m17425(__this, method) (( int32_t (*) (KeyValuePair_2_t2690 *, const MethodInfo*))KeyValuePair_2_get_Key_m17425_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m17426_gshared (KeyValuePair_2_t2690 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m17426(__this, ___value, method) (( void (*) (KeyValuePair_2_t2690 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m17426_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m17427_gshared (KeyValuePair_2_t2690 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m17427(__this, method) (( Object_t * (*) (KeyValuePair_2_t2690 *, const MethodInfo*))KeyValuePair_2_get_Value_m17427_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m17428_gshared (KeyValuePair_2_t2690 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m17428(__this, ___value, method) (( void (*) (KeyValuePair_2_t2690 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m17428_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m17429_gshared (KeyValuePair_2_t2690 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m17429(__this, method) (( String_t* (*) (KeyValuePair_2_t2690 *, const MethodInfo*))KeyValuePair_2_ToString_m17429_gshared)(__this, method)
