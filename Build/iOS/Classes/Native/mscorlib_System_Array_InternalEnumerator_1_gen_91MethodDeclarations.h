﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_91.h"
// Vuforia.VuforiaManagerImpl/PropData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__10.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26085_gshared (InternalEnumerator_1_t3321 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26085(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3321 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26085_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26086_gshared (InternalEnumerator_1_t3321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26086(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3321 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26086_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26087_gshared (InternalEnumerator_1_t3321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26087(__this, method) (( void (*) (InternalEnumerator_1_t3321 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26087_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26088_gshared (InternalEnumerator_1_t3321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26088(__this, method) (( bool (*) (InternalEnumerator_1_t3321 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26088_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::get_Current()
extern "C" PropData_t1144  InternalEnumerator_1_get_Current_m26089_gshared (InternalEnumerator_1_t3321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26089(__this, method) (( PropData_t1144  (*) (InternalEnumerator_1_t3321 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26089_gshared)(__this, method)
