﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordBehaviour
struct WordBehaviour_t326;

// System.Void Vuforia.WordBehaviour::.ctor()
extern "C" void WordBehaviour__ctor_m1284 (WordBehaviour_t326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
