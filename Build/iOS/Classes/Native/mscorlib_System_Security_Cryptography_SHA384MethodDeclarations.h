﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SHA384
struct SHA384_t2226;

// System.Void System.Security.Cryptography.SHA384::.ctor()
extern "C" void SHA384__ctor_m13206 (SHA384_t2226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
