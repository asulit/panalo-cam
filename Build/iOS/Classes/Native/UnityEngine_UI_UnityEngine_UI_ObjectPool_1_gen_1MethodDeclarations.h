﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_0MethodDeclarations.h"
#define ObjectPool_1__ctor_m19089(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t2825 *, UnityAction_1_t2826 *, UnityAction_1_t2826 *, const MethodInfo*))ObjectPool_1__ctor_m18958_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::get_countAll()
#define ObjectPool_1_get_countAll_m19090(__this, method) (( int32_t (*) (ObjectPool_1_t2825 *, const MethodInfo*))ObjectPool_1_get_countAll_m18960_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m19091(__this, ___value, method) (( void (*) (ObjectPool_1_t2825 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m18962_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::get_countActive()
#define ObjectPool_1_get_countActive_m19092(__this, method) (( int32_t (*) (ObjectPool_1_t2825 *, const MethodInfo*))ObjectPool_1_get_countActive_m18964_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m19093(__this, method) (( int32_t (*) (ObjectPool_1_t2825 *, const MethodInfo*))ObjectPool_1_get_countInactive_m18966_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::Get()
#define ObjectPool_1_Get_m19094(__this, method) (( List_1_t344 * (*) (ObjectPool_1_t2825 *, const MethodInfo*))ObjectPool_1_Get_m18968_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>::Release(T)
#define ObjectPool_1_Release_m19095(__this, ___element, method) (( void (*) (ObjectPool_1_t2825 *, List_1_t344 *, const MethodInfo*))ObjectPool_1_Release_m18970_gshared)(__this, ___element, method)
