﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// NotificationHandlerComponent
struct NotificationHandlerComponent_t84;

// System.Void NotificationHandlerComponent::.ctor()
extern "C" void NotificationHandlerComponent__ctor_m283 (NotificationHandlerComponent_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
