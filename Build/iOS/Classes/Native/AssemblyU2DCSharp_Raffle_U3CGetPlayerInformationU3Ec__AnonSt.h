﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Raffle
struct Raffle_t202;
// System.Object
#include "mscorlib_System_Object.h"
// Raffle/<GetPlayerInformation>c__AnonStorey18
struct  U3CGetPlayerInformationU3Ec__AnonStorey18_t203  : public Object_t
{
	// System.String Raffle/<GetPlayerInformation>c__AnonStorey18::api
	String_t* ___api_0;
	// Raffle Raffle/<GetPlayerInformation>c__AnonStorey18::<>f__this
	Raffle_t202 * ___U3CU3Ef__this_1;
};
