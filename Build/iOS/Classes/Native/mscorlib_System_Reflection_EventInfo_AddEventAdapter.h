﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Delegate
struct Delegate_t466;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Reflection.EventInfo/AddEventAdapter
struct  AddEventAdapter_t2008  : public MulticastDelegate_t28
{
};
