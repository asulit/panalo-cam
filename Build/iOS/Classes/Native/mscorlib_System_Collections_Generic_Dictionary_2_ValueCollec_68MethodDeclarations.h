﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_66MethodDeclarations.h"
#define ValueCollection__ctor_m26503(__this, ___dictionary, method) (( void (*) (ValueCollection_t3354 *, Dictionary_2_t1175 *, const MethodInfo*))ValueCollection__ctor_m26431_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26504(__this, ___item, method) (( void (*) (ValueCollection_t3354 *, uint16_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26432_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26505(__this, method) (( void (*) (ValueCollection_t3354 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26433_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26506(__this, ___item, method) (( bool (*) (ValueCollection_t3354 *, uint16_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26434_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26507(__this, ___item, method) (( bool (*) (ValueCollection_t3354 *, uint16_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26435_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26508(__this, method) (( Object_t* (*) (ValueCollection_t3354 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26436_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m26509(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3354 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m26437_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26510(__this, method) (( Object_t * (*) (ValueCollection_t3354 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26438_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26511(__this, method) (( bool (*) (ValueCollection_t3354 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26439_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26512(__this, method) (( bool (*) (ValueCollection_t3354 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26440_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m26513(__this, method) (( Object_t * (*) (ValueCollection_t3354 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m26441_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m26514(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3354 *, UInt16U5BU5D_t1635*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m26442_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::GetEnumerator()
#define ValueCollection_GetEnumerator_m26515(__this, method) (( Enumerator_t3765  (*) (ValueCollection_t3354 *, const MethodInfo*))ValueCollection_GetEnumerator_m26443_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::get_Count()
#define ValueCollection_get_Count_m26516(__this, method) (( int32_t (*) (ValueCollection_t3354 *, const MethodInfo*))ValueCollection_get_Count_m26444_gshared)(__this, method)
