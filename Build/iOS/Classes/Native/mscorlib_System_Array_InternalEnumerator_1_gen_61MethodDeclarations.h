﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.UserProfile>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22996(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3103 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14611_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.UserProfile>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22997(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3103 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14612_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.UserProfile>::Dispose()
#define InternalEnumerator_1_Dispose_m22998(__this, method) (( void (*) (InternalEnumerator_1_t3103 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14613_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.UserProfile>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22999(__this, method) (( bool (*) (InternalEnumerator_1_t3103 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14614_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.UserProfile>::get_Current()
#define InternalEnumerator_1_get_Current_m23000(__this, method) (( UserProfile_t951 * (*) (InternalEnumerator_1_t3103 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14615_gshared)(__this, method)
