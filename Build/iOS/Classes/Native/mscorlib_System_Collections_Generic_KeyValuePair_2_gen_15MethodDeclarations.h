﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<EScreens,System.String>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17280(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2681 *, int32_t, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m17185_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<EScreens,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m17281(__this, method) (( int32_t (*) (KeyValuePair_2_t2681 *, const MethodInfo*))KeyValuePair_2_get_Key_m17186_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<EScreens,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17282(__this, ___value, method) (( void (*) (KeyValuePair_2_t2681 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m17187_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<EScreens,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m17283(__this, method) (( String_t* (*) (KeyValuePair_2_t2681 *, const MethodInfo*))KeyValuePair_2_get_Value_m17188_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<EScreens,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17284(__this, ___value, method) (( void (*) (KeyValuePair_2_t2681 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m17189_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<EScreens,System.String>::ToString()
#define KeyValuePair_2_ToString_m17285(__this, method) (( String_t* (*) (KeyValuePair_2_t2681 *, const MethodInfo*))KeyValuePair_2_ToString_m17190_gshared)(__this, method)
