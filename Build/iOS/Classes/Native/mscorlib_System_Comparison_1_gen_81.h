﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName
struct StrongName_t2246;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<System.Security.Policy.StrongName>
struct  Comparison_1_t3600  : public MulticastDelegate_t28
{
};
