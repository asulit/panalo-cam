﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2568;
// System.Object
struct Object_t;
// System.Collections.Generic.Queue`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C" void Enumerator__ctor_m15895_gshared (Enumerator_t2569 * __this, Queue_1_t2568 * ___q, const MethodInfo* method);
#define Enumerator__ctor_m15895(__this, ___q, method) (( void (*) (Enumerator_t2569 *, Queue_1_t2568 *, const MethodInfo*))Enumerator__ctor_m15895_gshared)(__this, ___q, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15896_gshared (Enumerator_t2569 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15896(__this, method) (( Object_t * (*) (Enumerator_t2569 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15896_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15897_gshared (Enumerator_t2569 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15897(__this, method) (( void (*) (Enumerator_t2569 *, const MethodInfo*))Enumerator_Dispose_m15897_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15898_gshared (Enumerator_t2569 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15898(__this, method) (( bool (*) (Enumerator_t2569 *, const MethodInfo*))Enumerator_MoveNext_m15898_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15899_gshared (Enumerator_t2569 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15899(__this, method) (( Object_t * (*) (Enumerator_t2569 *, const MethodInfo*))Enumerator_get_Current_m15899_gshared)(__this, method)
