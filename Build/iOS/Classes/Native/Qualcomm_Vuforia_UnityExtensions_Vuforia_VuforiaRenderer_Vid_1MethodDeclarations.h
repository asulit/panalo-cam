﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void VideoBGCfgData_t1156_marshal(const VideoBGCfgData_t1156& unmarshaled, VideoBGCfgData_t1156_marshaled& marshaled);
extern "C" void VideoBGCfgData_t1156_marshal_back(const VideoBGCfgData_t1156_marshaled& marshaled, VideoBGCfgData_t1156& unmarshaled);
extern "C" void VideoBGCfgData_t1156_marshal_cleanup(VideoBGCfgData_t1156_marshaled& marshaled);
