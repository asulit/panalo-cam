﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t3181;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__30.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_30.h"
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m24189_gshared (Enumerator_t3188 * __this, Dictionary_2_t3181 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m24189(__this, ___dictionary, method) (( void (*) (Enumerator_t3188 *, Dictionary_2_t3181 *, const MethodInfo*))Enumerator__ctor_m24189_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m24190_gshared (Enumerator_t3188 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m24190(__this, method) (( Object_t * (*) (Enumerator_t3188 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24190_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24191_gshared (Enumerator_t3188 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24191(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3188 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24191_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24192_gshared (Enumerator_t3188 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24192(__this, method) (( Object_t * (*) (Enumerator_t3188 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24192_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24193_gshared (Enumerator_t3188 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24193(__this, method) (( Object_t * (*) (Enumerator_t3188 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m24194_gshared (Enumerator_t3188 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m24194(__this, method) (( bool (*) (Enumerator_t3188 *, const MethodInfo*))Enumerator_MoveNext_m24194_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" KeyValuePair_2_t3183  Enumerator_get_Current_m24195_gshared (Enumerator_t3188 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m24195(__this, method) (( KeyValuePair_2_t3183  (*) (Enumerator_t3188 *, const MethodInfo*))Enumerator_get_Current_m24195_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m24196_gshared (Enumerator_t3188 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m24196(__this, method) (( Object_t * (*) (Enumerator_t3188 *, const MethodInfo*))Enumerator_get_CurrentKey_m24196_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m24197_gshared (Enumerator_t3188 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m24197(__this, method) (( int32_t (*) (Enumerator_t3188 *, const MethodInfo*))Enumerator_get_CurrentValue_m24197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyState()
extern "C" void Enumerator_VerifyState_m24198_gshared (Enumerator_t3188 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m24198(__this, method) (( void (*) (Enumerator_t3188 *, const MethodInfo*))Enumerator_VerifyState_m24198_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m24199_gshared (Enumerator_t3188 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m24199(__this, method) (( void (*) (Enumerator_t3188 *, const MethodInfo*))Enumerator_VerifyCurrent_m24199_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m24200_gshared (Enumerator_t3188 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m24200(__this, method) (( void (*) (Enumerator_t3188 *, const MethodInfo*))Enumerator_Dispose_m24200_gshared)(__this, method)
