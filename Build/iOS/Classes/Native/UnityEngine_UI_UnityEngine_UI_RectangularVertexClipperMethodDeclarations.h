﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.RectangularVertexClipper
struct RectangularVertexClipper_t614;
// UnityEngine.RectTransform
struct RectTransform_t556;
// UnityEngine.Canvas
struct Canvas_t574;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void UnityEngine.UI.RectangularVertexClipper::.ctor()
extern "C" void RectangularVertexClipper__ctor_m3044 (RectangularVertexClipper_t614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.UI.RectangularVertexClipper::GetCanvasRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C" Rect_t267  RectangularVertexClipper_GetCanvasRect_m3045 (RectangularVertexClipper_t614 * __this, RectTransform_t556 * ___t, Canvas_t574 * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
