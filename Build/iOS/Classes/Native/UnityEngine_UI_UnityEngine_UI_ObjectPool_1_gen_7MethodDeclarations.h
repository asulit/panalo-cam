﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_0MethodDeclarations.h"
#define ObjectPool_1__ctor_m22911(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t3090 *, UnityAction_1_t3091 *, UnityAction_1_t3091 *, const MethodInfo*))ObjectPool_1__ctor_m18958_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>::get_countAll()
#define ObjectPool_1_get_countAll_m22912(__this, method) (( int32_t (*) (ObjectPool_1_t3090 *, const MethodInfo*))ObjectPool_1_get_countAll_m18960_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m22913(__this, ___value, method) (( void (*) (ObjectPool_1_t3090 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m18962_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>::get_countActive()
#define ObjectPool_1_get_countActive_m22914(__this, method) (( int32_t (*) (ObjectPool_1_t3090 *, const MethodInfo*))ObjectPool_1_get_countActive_m18964_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m22915(__this, method) (( int32_t (*) (ObjectPool_1_t3090 *, const MethodInfo*))ObjectPool_1_get_countInactive_m18966_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>::Get()
#define ObjectPool_1_Get_m22916(__this, method) (( List_1_t678 * (*) (ObjectPool_1_t3090 *, const MethodInfo*))ObjectPool_1_Get_m18968_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>::Release(T)
#define ObjectPool_1_Release_m22917(__this, ___element, method) (( void (*) (ObjectPool_1_t3090 *, List_1_t678 *, const MethodInfo*))ObjectPool_1_Release_m18970_gshared)(__this, ___element, method)
