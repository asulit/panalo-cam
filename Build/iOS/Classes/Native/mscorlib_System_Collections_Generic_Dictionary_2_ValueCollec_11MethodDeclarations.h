﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_65MethodDeclarations.h"
#define Enumerator__ctor_m26095(__this, ___host, method) (( void (*) (Enumerator_t1316 *, Dictionary_2_t1103 *, const MethodInfo*))Enumerator__ctor_m25159_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26096(__this, method) (( Object_t * (*) (Enumerator_t1316 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25160_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Dispose()
#define Enumerator_Dispose_m7297(__this, method) (( void (*) (Enumerator_t1316 *, const MethodInfo*))Enumerator_Dispose_m25161_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::MoveNext()
#define Enumerator_MoveNext_m7296(__this, method) (( bool (*) (Enumerator_t1316 *, const MethodInfo*))Enumerator_MoveNext_m25162_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Current()
#define Enumerator_get_Current_m7295(__this, method) (( Image_t1109 * (*) (Enumerator_t1316 *, const MethodInfo*))Enumerator_get_Current_m25163_gshared)(__this, method)
