﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t3034;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Predicate`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m22245_gshared (Predicate_1_t3034 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m22245(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3034 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m22245_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m22246_gshared (Predicate_1_t3034 * __this, Vector3_t36  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m22246(__this, ___obj, method) (( bool (*) (Predicate_1_t3034 *, Vector3_t36 , const MethodInfo*))Predicate_1_Invoke_m22246_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m22247_gshared (Predicate_1_t3034 * __this, Vector3_t36  ___obj, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m22247(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3034 *, Vector3_t36 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m22247_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m22248_gshared (Predicate_1_t3034 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m22248(__this, ___result, method) (( bool (*) (Predicate_1_t3034 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m22248_gshared)(__this, ___result, method)
