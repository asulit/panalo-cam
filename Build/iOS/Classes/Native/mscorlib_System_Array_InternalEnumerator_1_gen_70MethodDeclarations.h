﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_70.h"
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23330_gshared (InternalEnumerator_1_t3129 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m23330(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3129 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m23330_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23331_gshared (InternalEnumerator_1_t3129 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23331(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3129 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23331_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23332_gshared (InternalEnumerator_1_t3129 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23332(__this, method) (( void (*) (InternalEnumerator_1_t3129 *, const MethodInfo*))InternalEnumerator_1_Dispose_m23332_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23333_gshared (InternalEnumerator_1_t3129 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23333(__this, method) (( bool (*) (InternalEnumerator_1_t3129 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m23333_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::get_Current()
extern "C" WebCamDevice_t876  InternalEnumerator_1_get_Current_m23334_gshared (InternalEnumerator_1_t3129 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23334(__this, method) (( WebCamDevice_t876  (*) (InternalEnumerator_1_t3129 *, const MethodInfo*))InternalEnumerator_1_get_Current_m23334_gshared)(__this, method)
