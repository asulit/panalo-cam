﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_94.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_38.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26380_gshared (InternalEnumerator_1_t3338 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26380(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3338 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26380_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26381_gshared (InternalEnumerator_1_t3338 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26381(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3338 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26381_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26382_gshared (InternalEnumerator_1_t3338 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26382(__this, method) (( void (*) (InternalEnumerator_1_t3338 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26382_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26383_gshared (InternalEnumerator_1_t3338 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26383(__this, method) (( bool (*) (InternalEnumerator_1_t3338 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26383_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::get_Current()
extern "C" KeyValuePair_2_t3337  InternalEnumerator_1_get_Current_m26384_gshared (InternalEnumerator_1_t3338 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26384(__this, method) (( KeyValuePair_2_t3337  (*) (InternalEnumerator_1_t3338 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26384_gshared)(__this, method)
