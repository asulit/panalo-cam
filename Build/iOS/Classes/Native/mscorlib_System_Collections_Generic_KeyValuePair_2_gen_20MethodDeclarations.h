﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.String>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m18147(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2760 *, InputField_t226 *, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m15105_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m18148(__this, method) (( InputField_t226 * (*) (KeyValuePair_2_t2760 *, const MethodInfo*))KeyValuePair_2_get_Key_m15106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18149(__this, ___value, method) (( void (*) (KeyValuePair_2_t2760 *, InputField_t226 *, const MethodInfo*))KeyValuePair_2_set_Key_m15107_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m18150(__this, method) (( String_t* (*) (KeyValuePair_2_t2760 *, const MethodInfo*))KeyValuePair_2_get_Value_m15108_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18151(__this, ___value, method) (( void (*) (KeyValuePair_2_t2760 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m15109_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.String>::ToString()
#define KeyValuePair_2_ToString_m18152(__this, method) (( String_t* (*) (KeyValuePair_2_t2760 *, const MethodInfo*))KeyValuePair_2_ToString_m15110_gshared)(__this, method)
