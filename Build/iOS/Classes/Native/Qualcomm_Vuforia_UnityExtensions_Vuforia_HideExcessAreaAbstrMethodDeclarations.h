﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t284;
// UnityEngine.GameObject
struct GameObject_t155;
// System.String
struct String_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::CreateQuad(UnityEngine.GameObject,System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Int32)
extern "C" GameObject_t155 * HideExcessAreaAbstractBehaviour_CreateQuad_m5401 (HideExcessAreaAbstractBehaviour_t284 * __this, GameObject_t155 * ___parent, String_t* ___name, Vector3_t36  ___position, Quaternion_t41  ___rotation, Vector3_t36  ___scale, int32_t ___layer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesActive(System.Boolean)
extern "C" void HideExcessAreaAbstractBehaviour_SetPlanesActive_m5402 (HideExcessAreaAbstractBehaviour_t284 * __this, bool ___activeflag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesRenderingActive(System.Boolean)
extern "C" void HideExcessAreaAbstractBehaviour_SetPlanesRenderingActive_m5403 (HideExcessAreaAbstractBehaviour_t284 * __this, bool ___activeflag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::isPlanesRenderingActive()
extern "C" bool HideExcessAreaAbstractBehaviour_isPlanesRenderingActive_m5404 (HideExcessAreaAbstractBehaviour_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::ForceUpdateHideBehaviours()
extern "C" void HideExcessAreaAbstractBehaviour_ForceUpdateHideBehaviours_m5405 (HideExcessAreaAbstractBehaviour_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnVuforiaStarted()
extern "C" void HideExcessAreaAbstractBehaviour_OnVuforiaStarted_m5406 (HideExcessAreaAbstractBehaviour_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::HasCalculationDataChanged()
extern "C" bool HideExcessAreaAbstractBehaviour_HasCalculationDataChanged_m5407 (HideExcessAreaAbstractBehaviour_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnPreCull()
extern "C" void HideExcessAreaAbstractBehaviour_OnPreCull_m5408 (HideExcessAreaAbstractBehaviour_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnPostRender()
extern "C" void HideExcessAreaAbstractBehaviour_OnPostRender_m5409 (HideExcessAreaAbstractBehaviour_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Start()
extern "C" void HideExcessAreaAbstractBehaviour_Start_m5410 (HideExcessAreaAbstractBehaviour_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDisable()
extern "C" void HideExcessAreaAbstractBehaviour_OnDisable_m5411 (HideExcessAreaAbstractBehaviour_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDestroy()
extern "C" void HideExcessAreaAbstractBehaviour_OnDestroy_m5412 (HideExcessAreaAbstractBehaviour_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Update()
extern "C" void HideExcessAreaAbstractBehaviour_Update_m5413 (HideExcessAreaAbstractBehaviour_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::.ctor()
extern "C" void HideExcessAreaAbstractBehaviour__ctor_m1831 (HideExcessAreaAbstractBehaviour_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
