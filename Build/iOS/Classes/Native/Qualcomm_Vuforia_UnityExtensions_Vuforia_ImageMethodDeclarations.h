﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.Image
struct Image_t1109;

// System.Void Vuforia.Image::.ctor()
extern "C" void Image__ctor_m5638 (Image_t1109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
