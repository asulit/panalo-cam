﻿#pragma once
#include <stdint.h>
// Vuforia.Marker
struct Marker_t1231;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>
struct  KeyValuePair_2_t3308 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::value
	Object_t * ___value_1;
};
