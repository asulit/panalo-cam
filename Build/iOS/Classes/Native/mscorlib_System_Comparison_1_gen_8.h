﻿#pragma once
#include <stdint.h>
// UnityEngine.Collider
struct Collider_t369;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Collider>
struct  Comparison_1_t2536  : public MulticastDelegate_t28
{
};
