﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Sprite
struct Sprite_t553;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"

// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
extern "C" Sprite_t553 * SpriteState_get_highlightedSprite_m2947 (SpriteState_t630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_highlightedSprite_m2948 (SpriteState_t630 * __this, Sprite_t553 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
extern "C" Sprite_t553 * SpriteState_get_pressedSprite_m2949 (SpriteState_t630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_pressedSprite_m2950 (SpriteState_t630 * __this, Sprite_t553 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
extern "C" Sprite_t553 * SpriteState_get_disabledSprite_m2951 (SpriteState_t630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_disabledSprite_m2952 (SpriteState_t630 * __this, Sprite_t553 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
