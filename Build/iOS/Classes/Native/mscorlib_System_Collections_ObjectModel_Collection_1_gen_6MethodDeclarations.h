﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>
struct Collection_1_t3052;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t262;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t3717;
// System.Collections.Generic.IList`1<UnityEngine.Vector2>
struct IList_1_t3051;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::.ctor()
extern "C" void Collection_1__ctor_m22496_gshared (Collection_1_t3052 * __this, const MethodInfo* method);
#define Collection_1__ctor_m22496(__this, method) (( void (*) (Collection_1_t3052 *, const MethodInfo*))Collection_1__ctor_m22496_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22497_gshared (Collection_1_t3052 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22497(__this, method) (( bool (*) (Collection_1_t3052 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22497_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m22498_gshared (Collection_1_t3052 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m22498(__this, ___array, ___index, method) (( void (*) (Collection_1_t3052 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m22498_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m22499_gshared (Collection_1_t3052 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m22499(__this, method) (( Object_t * (*) (Collection_1_t3052 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m22499_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m22500_gshared (Collection_1_t3052 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m22500(__this, ___value, method) (( int32_t (*) (Collection_1_t3052 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m22500_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m22501_gshared (Collection_1_t3052 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m22501(__this, ___value, method) (( bool (*) (Collection_1_t3052 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m22501_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m22502_gshared (Collection_1_t3052 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m22502(__this, ___value, method) (( int32_t (*) (Collection_1_t3052 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m22502_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m22503_gshared (Collection_1_t3052 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m22503(__this, ___index, ___value, method) (( void (*) (Collection_1_t3052 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m22503_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m22504_gshared (Collection_1_t3052 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m22504(__this, ___value, method) (( void (*) (Collection_1_t3052 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m22504_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m22505_gshared (Collection_1_t3052 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m22505(__this, method) (( bool (*) (Collection_1_t3052 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m22505_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m22506_gshared (Collection_1_t3052 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m22506(__this, method) (( Object_t * (*) (Collection_1_t3052 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m22506_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m22507_gshared (Collection_1_t3052 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m22507(__this, method) (( bool (*) (Collection_1_t3052 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m22507_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m22508_gshared (Collection_1_t3052 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m22508(__this, method) (( bool (*) (Collection_1_t3052 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m22508_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m22509_gshared (Collection_1_t3052 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m22509(__this, ___index, method) (( Object_t * (*) (Collection_1_t3052 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m22509_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m22510_gshared (Collection_1_t3052 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m22510(__this, ___index, ___value, method) (( void (*) (Collection_1_t3052 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m22510_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Add(T)
extern "C" void Collection_1_Add_m22511_gshared (Collection_1_t3052 * __this, Vector2_t2  ___item, const MethodInfo* method);
#define Collection_1_Add_m22511(__this, ___item, method) (( void (*) (Collection_1_t3052 *, Vector2_t2 , const MethodInfo*))Collection_1_Add_m22511_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Clear()
extern "C" void Collection_1_Clear_m22512_gshared (Collection_1_t3052 * __this, const MethodInfo* method);
#define Collection_1_Clear_m22512(__this, method) (( void (*) (Collection_1_t3052 *, const MethodInfo*))Collection_1_Clear_m22512_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::ClearItems()
extern "C" void Collection_1_ClearItems_m22513_gshared (Collection_1_t3052 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m22513(__this, method) (( void (*) (Collection_1_t3052 *, const MethodInfo*))Collection_1_ClearItems_m22513_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Contains(T)
extern "C" bool Collection_1_Contains_m22514_gshared (Collection_1_t3052 * __this, Vector2_t2  ___item, const MethodInfo* method);
#define Collection_1_Contains_m22514(__this, ___item, method) (( bool (*) (Collection_1_t3052 *, Vector2_t2 , const MethodInfo*))Collection_1_Contains_m22514_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m22515_gshared (Collection_1_t3052 * __this, Vector2U5BU5D_t262* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m22515(__this, ___array, ___index, method) (( void (*) (Collection_1_t3052 *, Vector2U5BU5D_t262*, int32_t, const MethodInfo*))Collection_1_CopyTo_m22515_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m22516_gshared (Collection_1_t3052 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m22516(__this, method) (( Object_t* (*) (Collection_1_t3052 *, const MethodInfo*))Collection_1_GetEnumerator_m22516_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m22517_gshared (Collection_1_t3052 * __this, Vector2_t2  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m22517(__this, ___item, method) (( int32_t (*) (Collection_1_t3052 *, Vector2_t2 , const MethodInfo*))Collection_1_IndexOf_m22517_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m22518_gshared (Collection_1_t3052 * __this, int32_t ___index, Vector2_t2  ___item, const MethodInfo* method);
#define Collection_1_Insert_m22518(__this, ___index, ___item, method) (( void (*) (Collection_1_t3052 *, int32_t, Vector2_t2 , const MethodInfo*))Collection_1_Insert_m22518_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m22519_gshared (Collection_1_t3052 * __this, int32_t ___index, Vector2_t2  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m22519(__this, ___index, ___item, method) (( void (*) (Collection_1_t3052 *, int32_t, Vector2_t2 , const MethodInfo*))Collection_1_InsertItem_m22519_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::Remove(T)
extern "C" bool Collection_1_Remove_m22520_gshared (Collection_1_t3052 * __this, Vector2_t2  ___item, const MethodInfo* method);
#define Collection_1_Remove_m22520(__this, ___item, method) (( bool (*) (Collection_1_t3052 *, Vector2_t2 , const MethodInfo*))Collection_1_Remove_m22520_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m22521_gshared (Collection_1_t3052 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m22521(__this, ___index, method) (( void (*) (Collection_1_t3052 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m22521_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m22522_gshared (Collection_1_t3052 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m22522(__this, ___index, method) (( void (*) (Collection_1_t3052 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m22522_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::get_Count()
extern "C" int32_t Collection_1_get_Count_m22523_gshared (Collection_1_t3052 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m22523(__this, method) (( int32_t (*) (Collection_1_t3052 *, const MethodInfo*))Collection_1_get_Count_m22523_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C" Vector2_t2  Collection_1_get_Item_m22524_gshared (Collection_1_t3052 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m22524(__this, ___index, method) (( Vector2_t2  (*) (Collection_1_t3052 *, int32_t, const MethodInfo*))Collection_1_get_Item_m22524_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m22525_gshared (Collection_1_t3052 * __this, int32_t ___index, Vector2_t2  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m22525(__this, ___index, ___value, method) (( void (*) (Collection_1_t3052 *, int32_t, Vector2_t2 , const MethodInfo*))Collection_1_set_Item_m22525_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m22526_gshared (Collection_1_t3052 * __this, int32_t ___index, Vector2_t2  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m22526(__this, ___index, ___item, method) (( void (*) (Collection_1_t3052 *, int32_t, Vector2_t2 , const MethodInfo*))Collection_1_SetItem_m22526_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m22527_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m22527(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m22527_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::ConvertItem(System.Object)
extern "C" Vector2_t2  Collection_1_ConvertItem_m22528_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m22528(__this /* static, unused */, ___item, method) (( Vector2_t2  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m22528_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m22529_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m22529(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m22529_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m22530_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m22530(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m22530_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector2>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m22531_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m22531(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m22531_gshared)(__this /* static, unused */, ___list, method)
