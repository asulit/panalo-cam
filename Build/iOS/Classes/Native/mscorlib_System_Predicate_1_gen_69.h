﻿#pragma once
#include <stdint.h>
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t1271;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.ISmartTerrainEventHandler>
struct  Predicate_1_t3393  : public MulticastDelegate_t28
{
};
