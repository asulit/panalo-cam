﻿#pragma once
#include <stdint.h>
// Vuforia.MultiTarget
struct MultiTarget_t1232;
// Vuforia.DataSetTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTrackableBeh.h"
// Vuforia.MultiTargetAbstractBehaviour
struct  MultiTargetAbstractBehaviour_t297  : public DataSetTrackableBehaviour_t1061
{
	// Vuforia.MultiTarget Vuforia.MultiTargetAbstractBehaviour::mMultiTarget
	Object_t * ___mMultiTarget_20;
};
