﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
struct ReceiveRecordAsyncResult_t1511;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t139;
// System.IO.Stream
struct Stream_t1512;
// System.Exception
struct Exception_t359;
// System.Threading.WaitHandle
struct WaitHandle_t351;

// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::.ctor(System.AsyncCallback,System.Object,System.Byte[],System.IO.Stream)
extern "C" void ReceiveRecordAsyncResult__ctor_m8129 (ReceiveRecordAsyncResult_t1511 * __this, AsyncCallback_t31 * ___userCallback, Object_t * ___userState, ByteU5BU5D_t139* ___initialBuffer, Stream_t1512 * ___record, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_Record()
extern "C" Stream_t1512 * ReceiveRecordAsyncResult_get_Record_m8130 (ReceiveRecordAsyncResult_t1511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_ResultingBuffer()
extern "C" ByteU5BU5D_t139* ReceiveRecordAsyncResult_get_ResultingBuffer_m8131 (ReceiveRecordAsyncResult_t1511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_InitialBuffer()
extern "C" ByteU5BU5D_t139* ReceiveRecordAsyncResult_get_InitialBuffer_m8132 (ReceiveRecordAsyncResult_t1511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncState()
extern "C" Object_t * ReceiveRecordAsyncResult_get_AsyncState_m8133 (ReceiveRecordAsyncResult_t1511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncException()
extern "C" Exception_t359 * ReceiveRecordAsyncResult_get_AsyncException_m8134 (ReceiveRecordAsyncResult_t1511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_CompletedWithError()
extern "C" bool ReceiveRecordAsyncResult_get_CompletedWithError_m8135 (ReceiveRecordAsyncResult_t1511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncWaitHandle()
extern "C" WaitHandle_t351 * ReceiveRecordAsyncResult_get_AsyncWaitHandle_m8136 (ReceiveRecordAsyncResult_t1511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_IsCompleted()
extern "C" bool ReceiveRecordAsyncResult_get_IsCompleted_m8137 (ReceiveRecordAsyncResult_t1511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception,System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m8138 (ReceiveRecordAsyncResult_t1511 * __this, Exception_t359 * ___ex, ByteU5BU5D_t139* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception)
extern "C" void ReceiveRecordAsyncResult_SetComplete_m8139 (ReceiveRecordAsyncResult_t1511 * __this, Exception_t359 * ___ex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m8140 (ReceiveRecordAsyncResult_t1511 * __this, ByteU5BU5D_t139* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
