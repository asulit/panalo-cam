﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction
struct UnityAction_t575;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall
struct  InvokableCall_t982  : public BaseInvokableCall_t981
{
	// UnityEngine.Events.UnityAction UnityEngine.Events.InvokableCall::Delegate
	UnityAction_t575 * ___Delegate_0;
};
