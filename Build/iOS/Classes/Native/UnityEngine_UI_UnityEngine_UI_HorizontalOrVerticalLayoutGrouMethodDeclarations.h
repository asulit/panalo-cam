﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct HorizontalOrVerticalLayoutGroup_t664;

// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
extern "C" void HorizontalOrVerticalLayoutGroup__ctor_m3128 (HorizontalOrVerticalLayoutGroup_t664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern "C" float HorizontalOrVerticalLayoutGroup_get_spacing_m3129 (HorizontalOrVerticalLayoutGroup_t664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
extern "C" void HorizontalOrVerticalLayoutGroup_set_spacing_m3130 (HorizontalOrVerticalLayoutGroup_t664 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m3131 (HorizontalOrVerticalLayoutGroup_t664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m3132 (HorizontalOrVerticalLayoutGroup_t664 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m3133 (HorizontalOrVerticalLayoutGroup_t664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m3134 (HorizontalOrVerticalLayoutGroup_t664 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m3135 (HorizontalOrVerticalLayoutGroup_t664 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m3136 (HorizontalOrVerticalLayoutGroup_t664 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method) IL2CPP_METHOD_ATTR;
