﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Utils.SerializedKeyValue`2<System.Object,System.Object>
struct SerializedKeyValue_2_t2634;
// System.Object
struct Object_t;

// System.Void Common.Utils.SerializedKeyValue`2<System.Object,System.Object>::.ctor()
extern "C" void SerializedKeyValue_2__ctor_m16663_gshared (SerializedKeyValue_2_t2634 * __this, const MethodInfo* method);
#define SerializedKeyValue_2__ctor_m16663(__this, method) (( void (*) (SerializedKeyValue_2_t2634 *, const MethodInfo*))SerializedKeyValue_2__ctor_m16663_gshared)(__this, method)
// K Common.Utils.SerializedKeyValue`2<System.Object,System.Object>::get_Key()
extern "C" Object_t * SerializedKeyValue_2_get_Key_m16664_gshared (SerializedKeyValue_2_t2634 * __this, const MethodInfo* method);
#define SerializedKeyValue_2_get_Key_m16664(__this, method) (( Object_t * (*) (SerializedKeyValue_2_t2634 *, const MethodInfo*))SerializedKeyValue_2_get_Key_m16664_gshared)(__this, method)
// V Common.Utils.SerializedKeyValue`2<System.Object,System.Object>::get_Value()
extern "C" Object_t * SerializedKeyValue_2_get_Value_m16665_gshared (SerializedKeyValue_2_t2634 * __this, const MethodInfo* method);
#define SerializedKeyValue_2_get_Value_m16665(__this, method) (( Object_t * (*) (SerializedKeyValue_2_t2634 *, const MethodInfo*))SerializedKeyValue_2_get_Value_m16665_gshared)(__this, method)
