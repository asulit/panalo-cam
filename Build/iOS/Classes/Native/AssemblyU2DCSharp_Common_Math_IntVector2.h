﻿#pragma once
#include <stdint.h>
// Common.Math.IntVector2
struct IntVector2_t79;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Math.IntVector2
struct  IntVector2_t79  : public Object_t
{
	// System.Int32 Common.Math.IntVector2::x
	int32_t ___x_1;
	// System.Int32 Common.Math.IntVector2::y
	int32_t ___y_2;
};
struct IntVector2_t79_StaticFields{
	// Common.Math.IntVector2 Common.Math.IntVector2::ZERO
	IntVector2_t79 * ___ZERO_0;
};
