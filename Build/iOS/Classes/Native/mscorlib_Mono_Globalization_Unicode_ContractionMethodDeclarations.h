﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.Contraction
struct Contraction_t1806;
// System.Char[]
struct CharU5BU5D_t385;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t139;

// System.Void Mono.Globalization.Unicode.Contraction::.ctor(System.Char[],System.String,System.Byte[])
extern "C" void Contraction__ctor_m10411 (Contraction_t1806 * __this, CharU5BU5D_t385* ___source, String_t* ___replacement, ByteU5BU5D_t139* ___sortkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
