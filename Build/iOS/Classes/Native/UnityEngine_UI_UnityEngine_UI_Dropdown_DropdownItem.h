﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t196;
// UnityEngine.UI.Image
struct Image_t213;
// UnityEngine.RectTransform
struct RectTransform_t556;
// UnityEngine.UI.Toggle
struct Toggle_t557;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.UI.Dropdown/DropdownItem
struct  DropdownItem_t555  : public MonoBehaviour_t5
{
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown/DropdownItem::m_Text
	Text_t196 * ___m_Text_2;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown/DropdownItem::m_Image
	Image_t213 * ___m_Image_3;
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown/DropdownItem::m_RectTransform
	RectTransform_t556 * ___m_RectTransform_4;
	// UnityEngine.UI.Toggle UnityEngine.UI.Dropdown/DropdownItem::m_Toggle
	Toggle_t557 * ___m_Toggle_5;
};
