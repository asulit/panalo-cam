﻿#pragma once
#include <stdint.h>
// UnityEngine.Sprite
struct Sprite_t553;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.UI.DefaultControls/Resources
struct  Resources_t552 
{
	// UnityEngine.Sprite UnityEngine.UI.DefaultControls/Resources::standard
	Sprite_t553 * ___standard_0;
	// UnityEngine.Sprite UnityEngine.UI.DefaultControls/Resources::background
	Sprite_t553 * ___background_1;
	// UnityEngine.Sprite UnityEngine.UI.DefaultControls/Resources::inputField
	Sprite_t553 * ___inputField_2;
	// UnityEngine.Sprite UnityEngine.UI.DefaultControls/Resources::knob
	Sprite_t553 * ___knob_3;
	// UnityEngine.Sprite UnityEngine.UI.DefaultControls/Resources::checkmark
	Sprite_t553 * ___checkmark_4;
	// UnityEngine.Sprite UnityEngine.UI.DefaultControls/Resources::dropdown
	Sprite_t553 * ___dropdown_5;
	// UnityEngine.Sprite UnityEngine.UI.DefaultControls/Resources::mask
	Sprite_t553 * ___mask_6;
};
