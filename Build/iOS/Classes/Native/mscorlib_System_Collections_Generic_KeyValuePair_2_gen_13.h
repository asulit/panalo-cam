﻿#pragma once
#include <stdint.h>
// System.Type
struct Type_t;
// System.Object
struct Object_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Object>
struct  KeyValuePair_2_t2628 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.Object>::key
	Type_t * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.Object>::value
	Object_t * ___value_1;
};
