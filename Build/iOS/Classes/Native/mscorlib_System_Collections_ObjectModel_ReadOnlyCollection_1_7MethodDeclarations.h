﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m16902(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2645 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m14724_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16903(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2645 *, TaskBase_t163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14725_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16904(__this, method) (( void (*) (ReadOnlyCollection_1_t2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14726_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16905(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2645 *, int32_t, TaskBase_t163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14727_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16906(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2645 *, TaskBase_t163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14728_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16907(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2645 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14729_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16908(__this, ___index, method) (( TaskBase_t163 * (*) (ReadOnlyCollection_1_t2645 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14730_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16909(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2645 *, int32_t, TaskBase_t163 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14731_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16910(__this, method) (( bool (*) (ReadOnlyCollection_1_t2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14732_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16911(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2645 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14733_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16912(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14734_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m16913(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2645 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m14735_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m16914(__this, method) (( void (*) (ReadOnlyCollection_1_t2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m14736_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m16915(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2645 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m14737_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16916(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2645 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14738_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m16917(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2645 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m14739_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m16918(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2645 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m14740_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16919(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2645 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14741_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16920(__this, method) (( bool (*) (ReadOnlyCollection_1_t2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14742_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16921(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14743_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16922(__this, method) (( bool (*) (ReadOnlyCollection_1_t2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14744_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16923(__this, method) (( bool (*) (ReadOnlyCollection_1_t2645 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14745_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m16924(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2645 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m14746_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m16925(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2645 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m14747_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::Contains(T)
#define ReadOnlyCollection_1_Contains_m16926(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2645 *, TaskBase_t163 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m14748_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m16927(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2645 *, TaskBaseU5BU5D_t2644*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m14749_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m16928(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2645 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m14750_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m16929(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2645 *, TaskBase_t163 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m14751_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::get_Count()
#define ReadOnlyCollection_1_get_Count_m16930(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2645 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m14752_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.TaskBase>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m16931(__this, ___index, method) (( TaskBase_t163 * (*) (ReadOnlyCollection_1_t2645 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m14753_gshared)(__this, ___index, method)
