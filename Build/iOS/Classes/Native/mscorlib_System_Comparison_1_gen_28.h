﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t558;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.Dropdown/OptionData>
struct  Comparison_1_t2903  : public MulticastDelegate_t28
{
};
