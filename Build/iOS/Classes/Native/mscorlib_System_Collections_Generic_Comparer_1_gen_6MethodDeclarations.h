﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.Vector2>
struct Comparer_1_t3056;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.ctor()
extern "C" void Comparer_1__ctor_m22544_gshared (Comparer_1_t3056 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m22544(__this, method) (( void (*) (Comparer_1_t3056 *, const MethodInfo*))Comparer_1__ctor_m22544_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.cctor()
extern "C" void Comparer_1__cctor_m22545_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m22545(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m22545_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m22546_gshared (Comparer_1_t3056 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m22546(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3056 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m22546_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::get_Default()
extern "C" Comparer_1_t3056 * Comparer_1_get_Default_m22547_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m22547(__this /* static, unused */, method) (( Comparer_1_t3056 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m22547_gshared)(__this /* static, unused */, method)
