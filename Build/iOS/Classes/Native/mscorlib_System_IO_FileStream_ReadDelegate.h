﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t139;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.IO.FileStream/ReadDelegate
struct  ReadDelegate_t1932  : public MulticastDelegate_t28
{
};
