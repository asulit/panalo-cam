﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"
#define List_1__ctor_m7485(__this, method) (( void (*) (List_1_t1239 *, const MethodInfo*))List_1__ctor_m1429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m29075(__this, ___collection, method) (( void (*) (List_1_t1239 *, Object_t*, const MethodInfo*))List_1__ctor_m14617_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m29076(__this, ___capacity, method) (( void (*) (List_1_t1239 *, int32_t, const MethodInfo*))List_1__ctor_m14619_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.cctor()
#define List_1__cctor_m29077(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14621_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29078(__this, method) (( Object_t* (*) (List_1_t1239 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14623_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m29079(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1239 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m14625_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m29080(__this, method) (( Object_t * (*) (List_1_t1239 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m14627_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m29081(__this, ___item, method) (( int32_t (*) (List_1_t1239 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m14629_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m29082(__this, ___item, method) (( bool (*) (List_1_t1239 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m14631_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m29083(__this, ___item, method) (( int32_t (*) (List_1_t1239 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m14633_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m29084(__this, ___index, ___item, method) (( void (*) (List_1_t1239 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m14635_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m29085(__this, ___item, method) (( void (*) (List_1_t1239 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m14637_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29086(__this, method) (( bool (*) (List_1_t1239 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14639_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m29087(__this, method) (( bool (*) (List_1_t1239 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m14641_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m29088(__this, method) (( Object_t * (*) (List_1_t1239 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m14643_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m29089(__this, method) (( bool (*) (List_1_t1239 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m14645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m29090(__this, method) (( bool (*) (List_1_t1239 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m14647_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m29091(__this, ___index, method) (( Object_t * (*) (List_1_t1239 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m14649_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m29092(__this, ___index, ___value, method) (( void (*) (List_1_t1239 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m14651_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Add(T)
#define List_1_Add_m29093(__this, ___item, method) (( void (*) (List_1_t1239 *, Object_t *, const MethodInfo*))List_1_Add_m14653_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m29094(__this, ___newCount, method) (( void (*) (List_1_t1239 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m14655_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m29095(__this, ___idx, ___count, method) (( void (*) (List_1_t1239 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m14657_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m29096(__this, ___collection, method) (( void (*) (List_1_t1239 *, Object_t*, const MethodInfo*))List_1_AddCollection_m14659_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m29097(__this, ___enumerable, method) (( void (*) (List_1_t1239 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m14661_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m29098(__this, ___collection, method) (( void (*) (List_1_t1239 *, Object_t*, const MethodInfo*))List_1_AddRange_m14663_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m29099(__this, method) (( ReadOnlyCollection_1_t3506 * (*) (List_1_t1239 *, const MethodInfo*))List_1_AsReadOnly_m14665_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Clear()
#define List_1_Clear_m29100(__this, method) (( void (*) (List_1_t1239 *, const MethodInfo*))List_1_Clear_m14667_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Contains(T)
#define List_1_Contains_m29101(__this, ___item, method) (( bool (*) (List_1_t1239 *, Object_t *, const MethodInfo*))List_1_Contains_m14669_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m29102(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1239 *, IVideoBackgroundEventHandlerU5BU5D_t3505*, int32_t, const MethodInfo*))List_1_CopyTo_m14671_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m29103(__this, ___match, method) (( Object_t * (*) (List_1_t1239 *, Predicate_1_t3508 *, const MethodInfo*))List_1_Find_m14673_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m29104(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3508 *, const MethodInfo*))List_1_CheckMatch_m14675_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m29105(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1239 *, int32_t, int32_t, Predicate_1_t3508 *, const MethodInfo*))List_1_GetIndex_m14677_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m7480(__this, method) (( Enumerator_t1387  (*) (List_1_t1239 *, const MethodInfo*))List_1_GetEnumerator_m14679_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::IndexOf(T)
#define List_1_IndexOf_m29106(__this, ___item, method) (( int32_t (*) (List_1_t1239 *, Object_t *, const MethodInfo*))List_1_IndexOf_m14681_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m29107(__this, ___start, ___delta, method) (( void (*) (List_1_t1239 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m14683_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m29108(__this, ___index, method) (( void (*) (List_1_t1239 *, int32_t, const MethodInfo*))List_1_CheckIndex_m14685_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m29109(__this, ___index, ___item, method) (( void (*) (List_1_t1239 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m14687_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m29110(__this, ___collection, method) (( void (*) (List_1_t1239 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m14689_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Remove(T)
#define List_1_Remove_m29111(__this, ___item, method) (( bool (*) (List_1_t1239 *, Object_t *, const MethodInfo*))List_1_Remove_m14691_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m29112(__this, ___match, method) (( int32_t (*) (List_1_t1239 *, Predicate_1_t3508 *, const MethodInfo*))List_1_RemoveAll_m14693_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m29113(__this, ___index, method) (( void (*) (List_1_t1239 *, int32_t, const MethodInfo*))List_1_RemoveAt_m14695_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m29114(__this, ___index, ___count, method) (( void (*) (List_1_t1239 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m14697_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Reverse()
#define List_1_Reverse_m29115(__this, method) (( void (*) (List_1_t1239 *, const MethodInfo*))List_1_Reverse_m14699_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Sort()
#define List_1_Sort_m29116(__this, method) (( void (*) (List_1_t1239 *, const MethodInfo*))List_1_Sort_m14701_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m29117(__this, ___comparison, method) (( void (*) (List_1_t1239 *, Comparison_1_t3509 *, const MethodInfo*))List_1_Sort_m14703_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::ToArray()
#define List_1_ToArray_m29118(__this, method) (( IVideoBackgroundEventHandlerU5BU5D_t3505* (*) (List_1_t1239 *, const MethodInfo*))List_1_ToArray_m14705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::TrimExcess()
#define List_1_TrimExcess_m29119(__this, method) (( void (*) (List_1_t1239 *, const MethodInfo*))List_1_TrimExcess_m14707_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::get_Capacity()
#define List_1_get_Capacity_m29120(__this, method) (( int32_t (*) (List_1_t1239 *, const MethodInfo*))List_1_get_Capacity_m14709_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m29121(__this, ___value, method) (( void (*) (List_1_t1239 *, int32_t, const MethodInfo*))List_1_set_Capacity_m14711_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::get_Count()
#define List_1_get_Count_m29122(__this, method) (( int32_t (*) (List_1_t1239 *, const MethodInfo*))List_1_get_Count_m14713_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m29123(__this, ___index, method) (( Object_t * (*) (List_1_t1239 *, int32_t, const MethodInfo*))List_1_get_Item_m14715_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m29124(__this, ___index, ___value, method) (( void (*) (List_1_t1239 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m14717_gshared)(__this, ___index, ___value, method)
