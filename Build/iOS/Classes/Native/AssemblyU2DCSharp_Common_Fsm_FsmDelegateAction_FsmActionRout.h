﻿#pragma once
#include <stdint.h>
// Common.Fsm.FsmState
struct FsmState_t29;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// Common.Fsm.FsmDelegateAction/FsmActionRoutine
struct  FsmActionRoutine_t27  : public MulticastDelegate_t28
{
};
