﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_82.h"
// Vuforia.Eyewear/EyewearCalibrationReading
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_EyewearCali.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24528_gshared (InternalEnumerator_1_t3223 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24528(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3223 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24528_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24529_gshared (InternalEnumerator_1_t3223 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24529(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3223 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24529_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24530_gshared (InternalEnumerator_1_t3223 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24530(__this, method) (( void (*) (InternalEnumerator_1_t3223 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24530_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24531_gshared (InternalEnumerator_1_t3223 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24531(__this, method) (( bool (*) (InternalEnumerator_1_t3223 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24531_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.Eyewear/EyewearCalibrationReading>::get_Current()
extern "C" EyewearCalibrationReading_t1080  InternalEnumerator_1_get_Current_m24532_gshared (InternalEnumerator_1_t3223 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24532(__this, method) (( EyewearCalibrationReading_t1080  (*) (InternalEnumerator_1_t3223 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24532_gshared)(__this, method)
