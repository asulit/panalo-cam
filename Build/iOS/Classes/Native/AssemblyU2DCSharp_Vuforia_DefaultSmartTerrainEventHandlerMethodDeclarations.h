﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DefaultSmartTerrainEventHandler
struct DefaultSmartTerrainEventHandler_t277;
// Vuforia.Prop
struct Prop_t357;
// Vuforia.Surface
struct Surface_t358;

// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern "C" void DefaultSmartTerrainEventHandler__ctor_m1190 (DefaultSmartTerrainEventHandler_t277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern "C" void DefaultSmartTerrainEventHandler_Start_m1191 (DefaultSmartTerrainEventHandler_t277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern "C" void DefaultSmartTerrainEventHandler_OnDestroy_m1192 (DefaultSmartTerrainEventHandler_t277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern "C" void DefaultSmartTerrainEventHandler_OnPropCreated_m1193 (DefaultSmartTerrainEventHandler_t277 * __this, Object_t * ___prop, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern "C" void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m1194 (DefaultSmartTerrainEventHandler_t277 * __this, Object_t * ___surface, const MethodInfo* method) IL2CPP_METHOD_ATTR;
