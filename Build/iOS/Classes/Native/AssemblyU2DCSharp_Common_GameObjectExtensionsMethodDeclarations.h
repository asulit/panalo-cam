﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GameObject
struct GameObject_t155;

// System.Void Common.GameObjectExtensions::Deactivate(UnityEngine.GameObject)
extern "C" void GameObjectExtensions_Deactivate_m438 (Object_t * __this /* static, unused */, GameObject_t155 * ___self, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.GameObjectExtensions::Activate(UnityEngine.GameObject)
extern "C" void GameObjectExtensions_Activate_m439 (Object_t * __this /* static, unused */, GameObject_t155 * ___self, const MethodInfo* method) IL2CPP_METHOD_ATTR;
