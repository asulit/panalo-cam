﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreadHelper
struct UnityThreadHelper_t181;
// UnityThreading.Dispatcher
struct Dispatcher_t162;
// UnityThreading.TaskDistributor
struct TaskDistributor_t166;
// UnityThreading.ActionThread
struct ActionThread_t171;
// System.Action`1<UnityThreading.ActionThread>
struct Action_1_t172;
// System.Action
struct Action_t16;
// UnityThreading.ThreadBase
struct ThreadBase_t169;
// System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>
struct Func_2_t174;
// System.Func`1<System.Collections.IEnumerator>
struct Func_1_t179;

// System.Void UnityThreadHelper::.ctor()
extern "C" void UnityThreadHelper__ctor_m611 (UnityThreadHelper_t181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreadHelper::.cctor()
extern "C" void UnityThreadHelper__cctor_m612 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreadHelper::EnsureHelper()
extern "C" void UnityThreadHelper_EnsureHelper_m613 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreadHelper UnityThreadHelper::get_Instance()
extern "C" UnityThreadHelper_t181 * UnityThreadHelper_get_Instance_m614 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.Dispatcher UnityThreadHelper::get_Dispatcher()
extern "C" Dispatcher_t162 * UnityThreadHelper_get_Dispatcher_m615 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.TaskDistributor UnityThreadHelper::get_TaskDistributor()
extern "C" TaskDistributor_t166 * UnityThreadHelper_get_TaskDistributor_m616 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.Dispatcher UnityThreadHelper::get_CurrentDispatcher()
extern "C" Dispatcher_t162 * UnityThreadHelper_get_CurrentDispatcher_m617 (UnityThreadHelper_t181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.TaskDistributor UnityThreadHelper::get_CurrentTaskDistributor()
extern "C" TaskDistributor_t166 * UnityThreadHelper_get_CurrentTaskDistributor_m618 (UnityThreadHelper_t181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreadHelper::EnsureHelperInstance()
extern "C" void UnityThreadHelper_EnsureHelperInstance_m619 (UnityThreadHelper_t181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.ActionThread UnityThreadHelper::CreateThread(System.Action`1<UnityThreading.ActionThread>,System.Boolean)
extern "C" ActionThread_t171 * UnityThreadHelper_CreateThread_m620 (Object_t * __this /* static, unused */, Action_1_t172 * ___action, bool ___autoStartThread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.ActionThread UnityThreadHelper::CreateThread(System.Action`1<UnityThreading.ActionThread>)
extern "C" ActionThread_t171 * UnityThreadHelper_CreateThread_m621 (Object_t * __this /* static, unused */, Action_1_t172 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.ActionThread UnityThreadHelper::CreateThread(System.Action,System.Boolean)
extern "C" ActionThread_t171 * UnityThreadHelper_CreateThread_m622 (Object_t * __this /* static, unused */, Action_t16 * ___action, bool ___autoStartThread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.ActionThread UnityThreadHelper::CreateThread(System.Action)
extern "C" ActionThread_t171 * UnityThreadHelper_CreateThread_m623 (Object_t * __this /* static, unused */, Action_t16 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.ThreadBase UnityThreadHelper::CreateThread(System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>,System.Boolean)
extern "C" ThreadBase_t169 * UnityThreadHelper_CreateThread_m624 (Object_t * __this /* static, unused */, Func_2_t174 * ___action, bool ___autoStartThread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.ThreadBase UnityThreadHelper::CreateThread(System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>)
extern "C" ThreadBase_t169 * UnityThreadHelper_CreateThread_m625 (Object_t * __this /* static, unused */, Func_2_t174 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.ThreadBase UnityThreadHelper::CreateThread(System.Func`1<System.Collections.IEnumerator>,System.Boolean)
extern "C" ThreadBase_t169 * UnityThreadHelper_CreateThread_m626 (Object_t * __this /* static, unused */, Func_1_t179 * ___action, bool ___autoStartThread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.ThreadBase UnityThreadHelper::CreateThread(System.Func`1<System.Collections.IEnumerator>)
extern "C" ThreadBase_t169 * UnityThreadHelper_CreateThread_m627 (Object_t * __this /* static, unused */, Func_1_t179 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreadHelper::RegisterThread(UnityThreading.ThreadBase)
extern "C" void UnityThreadHelper_RegisterThread_m628 (UnityThreadHelper_t181 * __this, ThreadBase_t169 * ___thread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreadHelper::OnDestroy()
extern "C" void UnityThreadHelper_OnDestroy_m629 (UnityThreadHelper_t181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreadHelper::Update()
extern "C" void UnityThreadHelper_Update_m630 (UnityThreadHelper_t181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityThreadHelper::<Update>m__B(UnityThreading.ThreadBase)
extern "C" bool UnityThreadHelper_U3CUpdateU3Em__B_m631 (Object_t * __this /* static, unused */, ThreadBase_t169 * ___thread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
