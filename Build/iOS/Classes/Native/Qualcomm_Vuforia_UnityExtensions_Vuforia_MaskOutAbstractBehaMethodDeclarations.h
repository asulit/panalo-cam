﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t295;

// System.Void Vuforia.MaskOutAbstractBehaviour::.ctor()
extern "C" void MaskOutAbstractBehaviour__ctor_m1860 (MaskOutAbstractBehaviour_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
