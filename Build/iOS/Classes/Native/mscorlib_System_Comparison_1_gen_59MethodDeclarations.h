﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparison_1_t3281;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m25372_gshared (Comparison_1_t3281 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m25372(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3281 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m25372_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m25373_gshared (Comparison_1_t3281 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m25373(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3281 *, int32_t, int32_t, const MethodInfo*))Comparison_1_Invoke_m25373_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m25374_gshared (Comparison_1_t3281 * __this, int32_t ___x, int32_t ___y, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m25374(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3281 *, int32_t, int32_t, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m25374_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m25375_gshared (Comparison_1_t3281 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m25375(__this, ___result, method) (( int32_t (*) (Comparison_1_t3281 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m25375_gshared)(__this, ___result, method)
