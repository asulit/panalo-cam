﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIWordWrapSizer
struct GUIWordWrapSizer_t910;
// UnityEngine.GUIStyle
struct GUIStyle_t342;
// UnityEngine.GUIContent
struct GUIContent_t379;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t441;

// System.Void UnityEngine.GUIWordWrapSizer::.ctor(UnityEngine.GUIStyle,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C" void GUIWordWrapSizer__ctor_m4780 (GUIWordWrapSizer_t910 * __this, GUIStyle_t342 * ___style, GUIContent_t379 * ___content, GUILayoutOptionU5BU5D_t441* ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIWordWrapSizer::CalcWidth()
extern "C" void GUIWordWrapSizer_CalcWidth_m4781 (GUIWordWrapSizer_t910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIWordWrapSizer::CalcHeight()
extern "C" void GUIWordWrapSizer_CalcHeight_m4782 (GUIWordWrapSizer_t910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
