﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t631;
// UnityEngine.CanvasGroup
struct CanvasGroup_t733;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>
struct  Enumerator_t2998 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::l
	List_1_t631 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::current
	CanvasGroup_t733 * ___current_3;
};
