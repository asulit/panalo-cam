﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
#include "mscorlib_System_Array.h"

// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteStateMethodDeclarations.h"
// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_Sprite.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
extern "C" Sprite_t553 * SpriteState_get_highlightedSprite_m2947 (SpriteState_t630 * __this, const MethodInfo* method)
{
	{
		Sprite_t553 * L_0 = (__this->___m_HighlightedSprite_0);
		return L_0;
	}
}
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_Sprite.h"
extern "C" void SpriteState_set_highlightedSprite_m2948 (SpriteState_t630 * __this, Sprite_t553 * ___value, const MethodInfo* method)
{
	{
		Sprite_t553 * L_0 = ___value;
		__this->___m_HighlightedSprite_0 = L_0;
		return;
	}
}
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
extern "C" Sprite_t553 * SpriteState_get_pressedSprite_m2949 (SpriteState_t630 * __this, const MethodInfo* method)
{
	{
		Sprite_t553 * L_0 = (__this->___m_PressedSprite_1);
		return L_0;
	}
}
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_pressedSprite_m2950 (SpriteState_t630 * __this, Sprite_t553 * ___value, const MethodInfo* method)
{
	{
		Sprite_t553 * L_0 = ___value;
		__this->___m_PressedSprite_1 = L_0;
		return;
	}
}
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
extern "C" Sprite_t553 * SpriteState_get_disabledSprite_m2951 (SpriteState_t630 * __this, const MethodInfo* method)
{
	{
		Sprite_t553 * L_0 = (__this->___m_DisabledSprite_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_disabledSprite_m2952 (SpriteState_t630 * __this, Sprite_t553 * ___value, const MethodInfo* method)
{
	{
		Sprite_t553 * L_0 = ___value;
		__this->___m_DisabledSprite_2 = L_0;
		return;
	}
}
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntry.h"
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntryMethodDeclarations.h"
// UnityEngine.Rendering.CompareFunction
#include "UnityEngine_UnityEngine_Rendering_CompareFunction.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void MatEntry__ctor_m2953 (MatEntry_t638 * __this, const MethodInfo* method)
{
	{
		__this->___compareFunction_5 = 8;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial.h"
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterialMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
#include "mscorlib_System_Collections_Generic_List_1_gen_33.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Rendering.StencilOp
#include "UnityEngine_UnityEngine_Rendering_StencilOp.h"
// UnityEngine.Rendering.ColorWriteMask
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
#include "mscorlib_ArrayTypes.h"
// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
#include "mscorlib_System_Collections_Generic_List_1_gen_33MethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// UnityEngine.UI.Misc
#include "UnityEngine_UI_UnityEngine_UI_MiscMethodDeclarations.h"
// System.Void UnityEngine.UI.StencilMaterial::.cctor()
// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
#include "mscorlib_System_Collections_Generic_List_1_gen_33MethodDeclarations.h"
extern TypeInfo* List_1_t640_il2cpp_TypeInfo_var;
extern TypeInfo* StencilMaterial_t639_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3720_MethodInfo_var;
extern "C" void StencilMaterial__cctor_m2954 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t640_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(499);
		StencilMaterial_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(476);
		List_1__ctor_m3720_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484151);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t640 * L_0 = (List_1_t640 *)il2cpp_codegen_object_new (List_1_t640_il2cpp_TypeInfo_var);
		List_1__ctor_m3720(L_0, /*hidden argument*/List_1__ctor_m3720_MethodInfo_var);
		((StencilMaterial_t639_StaticFields*)StencilMaterial_t639_il2cpp_TypeInfo_var->static_fields)->___m_List_0 = L_0;
		return;
	}
}
// UnityEngine.Material UnityEngine.UI.StencilMaterial::Add(UnityEngine.Material,System.Int32)
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" Material_t82 * StencilMaterial_Add_m2955 (Object_t * __this /* static, unused */, Material_t82 * ___baseMat, int32_t ___stencilID, const MethodInfo* method)
{
	{
		return (Material_t82 *)NULL;
	}
}
// UnityEngine.Material UnityEngine.UI.StencilMaterial::Add(UnityEngine.Material,System.Int32,UnityEngine.Rendering.StencilOp,UnityEngine.Rendering.CompareFunction,UnityEngine.Rendering.ColorWriteMask)
// UnityEngine.Rendering.StencilOp
#include "UnityEngine_UnityEngine_Rendering_StencilOp.h"
// UnityEngine.Rendering.CompareFunction
#include "UnityEngine_UnityEngine_Rendering_CompareFunction.h"
// UnityEngine.Rendering.ColorWriteMask
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask.h"
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterialMethodDeclarations.h"
extern TypeInfo* StencilMaterial_t639_il2cpp_TypeInfo_var;
extern "C" Material_t82 * StencilMaterial_Add_m2956 (Object_t * __this /* static, unused */, Material_t82 * ___baseMat, int32_t ___stencilID, int32_t ___operation, int32_t ___compareFunction, int32_t ___colorWriteMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StencilMaterial_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(476);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t82 * L_0 = ___baseMat;
		int32_t L_1 = ___stencilID;
		int32_t L_2 = ___operation;
		int32_t L_3 = ___compareFunction;
		int32_t L_4 = ___colorWriteMask;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t639_il2cpp_TypeInfo_var);
		Material_t82 * L_5 = StencilMaterial_Add_m2957(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, ((int32_t)255), ((int32_t)255), /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Material UnityEngine.UI.StencilMaterial::Add(UnityEngine.Material,System.Int32,UnityEngine.Rendering.StencilOp,UnityEngine.Rendering.CompareFunction,UnityEngine.Rendering.ColorWriteMask,System.Int32,System.Int32)
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntryMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StencilMaterial_t639_il2cpp_TypeInfo_var;
extern TypeInfo* MatEntry_t638_il2cpp_TypeInfo_var;
extern TypeInfo* Material_t82_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* StencilOp_t759_il2cpp_TypeInfo_var;
extern TypeInfo* CompareFunction_t760_il2cpp_TypeInfo_var;
extern TypeInfo* ColorWriteMask_t761_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t384_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral484;
extern Il2CppCodeGenString* _stringLiteral485;
extern Il2CppCodeGenString* _stringLiteral486;
extern Il2CppCodeGenString* _stringLiteral487;
extern Il2CppCodeGenString* _stringLiteral488;
extern Il2CppCodeGenString* _stringLiteral489;
extern Il2CppCodeGenString* _stringLiteral490;
extern Il2CppCodeGenString* _stringLiteral491;
extern Il2CppCodeGenString* _stringLiteral492;
extern Il2CppCodeGenString* _stringLiteral493;
extern Il2CppCodeGenString* _stringLiteral494;
extern Il2CppCodeGenString* _stringLiteral495;
extern Il2CppCodeGenString* _stringLiteral496;
extern Il2CppCodeGenString* _stringLiteral497;
extern Il2CppCodeGenString* _stringLiteral498;
extern "C" Material_t82 * StencilMaterial_Add_m2957 (Object_t * __this /* static, unused */, Material_t82 * ___baseMat, int32_t ___stencilID, int32_t ___operation, int32_t ___compareFunction, int32_t ___colorWriteMask, int32_t ___readMask, int32_t ___writeMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		StencilMaterial_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(476);
		MatEntry_t638_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(498);
		Material_t82_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		StencilOp_t759_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		CompareFunction_t760_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		ColorWriteMask_t761_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		Boolean_t384_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		_stringLiteral484 = il2cpp_codegen_string_literal_from_index(484);
		_stringLiteral485 = il2cpp_codegen_string_literal_from_index(485);
		_stringLiteral486 = il2cpp_codegen_string_literal_from_index(486);
		_stringLiteral487 = il2cpp_codegen_string_literal_from_index(487);
		_stringLiteral488 = il2cpp_codegen_string_literal_from_index(488);
		_stringLiteral489 = il2cpp_codegen_string_literal_from_index(489);
		_stringLiteral490 = il2cpp_codegen_string_literal_from_index(490);
		_stringLiteral491 = il2cpp_codegen_string_literal_from_index(491);
		_stringLiteral492 = il2cpp_codegen_string_literal_from_index(492);
		_stringLiteral493 = il2cpp_codegen_string_literal_from_index(493);
		_stringLiteral494 = il2cpp_codegen_string_literal_from_index(494);
		_stringLiteral495 = il2cpp_codegen_string_literal_from_index(495);
		_stringLiteral496 = il2cpp_codegen_string_literal_from_index(496);
		_stringLiteral497 = il2cpp_codegen_string_literal_from_index(497);
		_stringLiteral498 = il2cpp_codegen_string_literal_from_index(498);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	MatEntry_t638 * V_1 = {0};
	MatEntry_t638 * V_2 = {0};
	MatEntry_t638 * G_B29_0 = {0};
	MatEntry_t638 * G_B28_0 = {0};
	int32_t G_B30_0 = 0;
	MatEntry_t638 * G_B30_1 = {0};
	String_t* G_B32_0 = {0};
	Material_t82 * G_B32_1 = {0};
	String_t* G_B31_0 = {0};
	Material_t82 * G_B31_1 = {0};
	int32_t G_B33_0 = 0;
	String_t* G_B33_1 = {0};
	Material_t82 * G_B33_2 = {0};
	{
		int32_t L_0 = ___stencilID;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_1 = ___colorWriteMask;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)15))))
		{
			goto IL_001c;
		}
	}

IL_0010:
	{
		Material_t82 * L_2 = ___baseMat;
		bool L_3 = Object_op_Equality_m1413(NULL /*static, unused*/, L_2, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}

IL_001c:
	{
		Material_t82 * L_4 = ___baseMat;
		return L_4;
	}

IL_001e:
	{
		Material_t82 * L_5 = ___baseMat;
		NullCheck(L_5);
		bool L_6 = Material_HasProperty_m3721(L_5, _stringLiteral484, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004b;
		}
	}
	{
		Material_t82 * L_7 = ___baseMat;
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m1338(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1575(NULL /*static, unused*/, _stringLiteral485, L_8, _stringLiteral486, /*hidden argument*/NULL);
		Material_t82 * L_10 = ___baseMat;
		Debug_LogWarning_m3722(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Material_t82 * L_11 = ___baseMat;
		return L_11;
	}

IL_004b:
	{
		Material_t82 * L_12 = ___baseMat;
		NullCheck(L_12);
		bool L_13 = Material_HasProperty_m3721(L_12, _stringLiteral487, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0078;
		}
	}
	{
		Material_t82 * L_14 = ___baseMat;
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m1338(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1575(NULL /*static, unused*/, _stringLiteral485, L_15, _stringLiteral488, /*hidden argument*/NULL);
		Material_t82 * L_17 = ___baseMat;
		Debug_LogWarning_m3722(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		Material_t82 * L_18 = ___baseMat;
		return L_18;
	}

IL_0078:
	{
		Material_t82 * L_19 = ___baseMat;
		NullCheck(L_19);
		bool L_20 = Material_HasProperty_m3721(L_19, _stringLiteral489, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00a5;
		}
	}
	{
		Material_t82 * L_21 = ___baseMat;
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m1338(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m1575(NULL /*static, unused*/, _stringLiteral485, L_22, _stringLiteral490, /*hidden argument*/NULL);
		Material_t82 * L_24 = ___baseMat;
		Debug_LogWarning_m3722(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		Material_t82 * L_25 = ___baseMat;
		return L_25;
	}

IL_00a5:
	{
		Material_t82 * L_26 = ___baseMat;
		NullCheck(L_26);
		bool L_27 = Material_HasProperty_m3721(L_26, _stringLiteral491, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00d2;
		}
	}
	{
		Material_t82 * L_28 = ___baseMat;
		NullCheck(L_28);
		String_t* L_29 = Object_get_name_m1338(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m1575(NULL /*static, unused*/, _stringLiteral485, L_29, _stringLiteral492, /*hidden argument*/NULL);
		Material_t82 * L_31 = ___baseMat;
		Debug_LogWarning_m3722(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		Material_t82 * L_32 = ___baseMat;
		return L_32;
	}

IL_00d2:
	{
		Material_t82 * L_33 = ___baseMat;
		NullCheck(L_33);
		bool L_34 = Material_HasProperty_m3721(L_33, _stringLiteral491, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_00ff;
		}
	}
	{
		Material_t82 * L_35 = ___baseMat;
		NullCheck(L_35);
		String_t* L_36 = Object_get_name_m1338(L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Concat_m1575(NULL /*static, unused*/, _stringLiteral485, L_36, _stringLiteral493, /*hidden argument*/NULL);
		Material_t82 * L_38 = ___baseMat;
		Debug_LogWarning_m3722(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		Material_t82 * L_39 = ___baseMat;
		return L_39;
	}

IL_00ff:
	{
		Material_t82 * L_40 = ___baseMat;
		NullCheck(L_40);
		bool L_41 = Material_HasProperty_m3721(L_40, _stringLiteral494, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_012c;
		}
	}
	{
		Material_t82 * L_42 = ___baseMat;
		NullCheck(L_42);
		String_t* L_43 = Object_get_name_m1338(L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m1575(NULL /*static, unused*/, _stringLiteral485, L_43, _stringLiteral495, /*hidden argument*/NULL);
		Material_t82 * L_45 = ___baseMat;
		Debug_LogWarning_m3722(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		Material_t82 * L_46 = ___baseMat;
		return L_46;
	}

IL_012c:
	{
		V_0 = 0;
		goto IL_01b4;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t639_il2cpp_TypeInfo_var);
		List_1_t640 * L_47 = ((StencilMaterial_t639_StaticFields*)StencilMaterial_t639_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		int32_t L_48 = V_0;
		NullCheck(L_47);
		MatEntry_t638 * L_49 = (MatEntry_t638 *)VirtFuncInvoker1< MatEntry_t638 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Item(System.Int32) */, L_47, L_48);
		V_1 = L_49;
		MatEntry_t638 * L_50 = V_1;
		NullCheck(L_50);
		Material_t82 * L_51 = (L_50->___baseMat_0);
		Material_t82 * L_52 = ___baseMat;
		bool L_53 = Object_op_Equality_m1413(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t638 * L_54 = V_1;
		NullCheck(L_54);
		int32_t L_55 = (L_54->___stencilId_3);
		int32_t L_56 = ___stencilID;
		if ((!(((uint32_t)L_55) == ((uint32_t)L_56))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t638 * L_57 = V_1;
		NullCheck(L_57);
		int32_t L_58 = (L_57->___operation_4);
		int32_t L_59 = ___operation;
		if ((!(((uint32_t)L_58) == ((uint32_t)L_59))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t638 * L_60 = V_1;
		NullCheck(L_60);
		int32_t L_61 = (L_60->___compareFunction_5);
		int32_t L_62 = ___compareFunction;
		if ((!(((uint32_t)L_61) == ((uint32_t)L_62))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t638 * L_63 = V_1;
		NullCheck(L_63);
		int32_t L_64 = (L_63->___readMask_6);
		int32_t L_65 = ___readMask;
		if ((!(((uint32_t)L_64) == ((uint32_t)L_65))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t638 * L_66 = V_1;
		NullCheck(L_66);
		int32_t L_67 = (L_66->___writeMask_7);
		int32_t L_68 = ___writeMask;
		if ((!(((uint32_t)L_67) == ((uint32_t)L_68))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t638 * L_69 = V_1;
		NullCheck(L_69);
		int32_t L_70 = (L_69->___colorMask_9);
		int32_t L_71 = ___colorWriteMask;
		if ((!(((uint32_t)L_70) == ((uint32_t)L_71))))
		{
			goto IL_01b0;
		}
	}
	{
		MatEntry_t638 * L_72 = V_1;
		MatEntry_t638 * L_73 = L_72;
		NullCheck(L_73);
		int32_t L_74 = (L_73->___count_2);
		NullCheck(L_73);
		L_73->___count_2 = ((int32_t)((int32_t)L_74+(int32_t)1));
		MatEntry_t638 * L_75 = V_1;
		NullCheck(L_75);
		Material_t82 * L_76 = (L_75->___customMat_1);
		return L_76;
	}

IL_01b0:
	{
		int32_t L_77 = V_0;
		V_0 = ((int32_t)((int32_t)L_77+(int32_t)1));
	}

IL_01b4:
	{
		int32_t L_78 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t639_il2cpp_TypeInfo_var);
		List_1_t640 * L_79 = ((StencilMaterial_t639_StaticFields*)StencilMaterial_t639_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		NullCheck(L_79);
		int32_t L_80 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Count() */, L_79);
		if ((((int32_t)L_78) < ((int32_t)L_80)))
		{
			goto IL_0133;
		}
	}
	{
		MatEntry_t638 * L_81 = (MatEntry_t638 *)il2cpp_codegen_object_new (MatEntry_t638_il2cpp_TypeInfo_var);
		MatEntry__ctor_m2953(L_81, /*hidden argument*/NULL);
		V_2 = L_81;
		MatEntry_t638 * L_82 = V_2;
		NullCheck(L_82);
		L_82->___count_2 = 1;
		MatEntry_t638 * L_83 = V_2;
		Material_t82 * L_84 = ___baseMat;
		NullCheck(L_83);
		L_83->___baseMat_0 = L_84;
		MatEntry_t638 * L_85 = V_2;
		Material_t82 * L_86 = ___baseMat;
		Material_t82 * L_87 = (Material_t82 *)il2cpp_codegen_object_new (Material_t82_il2cpp_TypeInfo_var);
		Material__ctor_m3723(L_87, L_86, /*hidden argument*/NULL);
		NullCheck(L_85);
		L_85->___customMat_1 = L_87;
		MatEntry_t638 * L_88 = V_2;
		NullCheck(L_88);
		Material_t82 * L_89 = (L_88->___customMat_1);
		NullCheck(L_89);
		Object_set_hideFlags_m1619(L_89, ((int32_t)61), /*hidden argument*/NULL);
		MatEntry_t638 * L_90 = V_2;
		int32_t L_91 = ___stencilID;
		NullCheck(L_90);
		L_90->___stencilId_3 = L_91;
		MatEntry_t638 * L_92 = V_2;
		int32_t L_93 = ___operation;
		NullCheck(L_92);
		L_92->___operation_4 = L_93;
		MatEntry_t638 * L_94 = V_2;
		int32_t L_95 = ___compareFunction;
		NullCheck(L_94);
		L_94->___compareFunction_5 = L_95;
		MatEntry_t638 * L_96 = V_2;
		int32_t L_97 = ___readMask;
		NullCheck(L_96);
		L_96->___readMask_6 = L_97;
		MatEntry_t638 * L_98 = V_2;
		int32_t L_99 = ___writeMask;
		NullCheck(L_98);
		L_98->___writeMask_7 = L_99;
		MatEntry_t638 * L_100 = V_2;
		int32_t L_101 = ___colorWriteMask;
		NullCheck(L_100);
		L_100->___colorMask_9 = L_101;
		MatEntry_t638 * L_102 = V_2;
		int32_t L_103 = ___operation;
		G_B28_0 = L_102;
		if (!L_103)
		{
			G_B29_0 = L_102;
			goto IL_022c;
		}
	}
	{
		int32_t L_104 = ___writeMask;
		G_B30_0 = ((((int32_t)L_104) > ((int32_t)0))? 1 : 0);
		G_B30_1 = G_B28_0;
		goto IL_022d;
	}

IL_022c:
	{
		G_B30_0 = 0;
		G_B30_1 = G_B29_0;
	}

IL_022d:
	{
		NullCheck(G_B30_1);
		G_B30_1->___useAlphaClip_8 = G_B30_0;
		MatEntry_t638 * L_105 = V_2;
		NullCheck(L_105);
		Material_t82 * L_106 = (L_105->___customMat_1);
		ObjectU5BU5D_t356* L_107 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 8));
		int32_t L_108 = ___stencilID;
		int32_t L_109 = L_108;
		Object_t * L_110 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_109);
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, 0);
		ArrayElementTypeCheck (L_107, L_110);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_107, 0, sizeof(Object_t *))) = (Object_t *)L_110;
		ObjectU5BU5D_t356* L_111 = L_107;
		int32_t L_112 = ___operation;
		int32_t L_113 = L_112;
		Object_t * L_114 = Box(StencilOp_t759_il2cpp_TypeInfo_var, &L_113);
		NullCheck(L_111);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_111, 1);
		ArrayElementTypeCheck (L_111, L_114);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_111, 1, sizeof(Object_t *))) = (Object_t *)L_114;
		ObjectU5BU5D_t356* L_115 = L_111;
		int32_t L_116 = ___compareFunction;
		int32_t L_117 = L_116;
		Object_t * L_118 = Box(CompareFunction_t760_il2cpp_TypeInfo_var, &L_117);
		NullCheck(L_115);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_115, 2);
		ArrayElementTypeCheck (L_115, L_118);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_115, 2, sizeof(Object_t *))) = (Object_t *)L_118;
		ObjectU5BU5D_t356* L_119 = L_115;
		int32_t L_120 = ___writeMask;
		int32_t L_121 = L_120;
		Object_t * L_122 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_121);
		NullCheck(L_119);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_119, 3);
		ArrayElementTypeCheck (L_119, L_122);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_119, 3, sizeof(Object_t *))) = (Object_t *)L_122;
		ObjectU5BU5D_t356* L_123 = L_119;
		int32_t L_124 = ___readMask;
		int32_t L_125 = L_124;
		Object_t * L_126 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_125);
		NullCheck(L_123);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_123, 4);
		ArrayElementTypeCheck (L_123, L_126);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_123, 4, sizeof(Object_t *))) = (Object_t *)L_126;
		ObjectU5BU5D_t356* L_127 = L_123;
		int32_t L_128 = ___colorWriteMask;
		int32_t L_129 = L_128;
		Object_t * L_130 = Box(ColorWriteMask_t761_il2cpp_TypeInfo_var, &L_129);
		NullCheck(L_127);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_127, 5);
		ArrayElementTypeCheck (L_127, L_130);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_127, 5, sizeof(Object_t *))) = (Object_t *)L_130;
		ObjectU5BU5D_t356* L_131 = L_127;
		MatEntry_t638 * L_132 = V_2;
		NullCheck(L_132);
		bool L_133 = (L_132->___useAlphaClip_8);
		bool L_134 = L_133;
		Object_t * L_135 = Box(Boolean_t384_il2cpp_TypeInfo_var, &L_134);
		NullCheck(L_131);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_131, 6);
		ArrayElementTypeCheck (L_131, L_135);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_131, 6, sizeof(Object_t *))) = (Object_t *)L_135;
		ObjectU5BU5D_t356* L_136 = L_131;
		Material_t82 * L_137 = ___baseMat;
		NullCheck(L_137);
		String_t* L_138 = Object_get_name_m1338(L_137, /*hidden argument*/NULL);
		NullCheck(L_136);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_136, 7);
		ArrayElementTypeCheck (L_136, L_138);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_136, 7, sizeof(Object_t *))) = (Object_t *)L_138;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_139 = String_Format_m3724(NULL /*static, unused*/, _stringLiteral496, L_136, /*hidden argument*/NULL);
		NullCheck(L_106);
		Object_set_name_m1476(L_106, L_139, /*hidden argument*/NULL);
		MatEntry_t638 * L_140 = V_2;
		NullCheck(L_140);
		Material_t82 * L_141 = (L_140->___customMat_1);
		int32_t L_142 = ___stencilID;
		NullCheck(L_141);
		Material_SetInt_m3725(L_141, _stringLiteral484, L_142, /*hidden argument*/NULL);
		MatEntry_t638 * L_143 = V_2;
		NullCheck(L_143);
		Material_t82 * L_144 = (L_143->___customMat_1);
		int32_t L_145 = ___operation;
		NullCheck(L_144);
		Material_SetInt_m3725(L_144, _stringLiteral487, L_145, /*hidden argument*/NULL);
		MatEntry_t638 * L_146 = V_2;
		NullCheck(L_146);
		Material_t82 * L_147 = (L_146->___customMat_1);
		int32_t L_148 = ___compareFunction;
		NullCheck(L_147);
		Material_SetInt_m3725(L_147, _stringLiteral489, L_148, /*hidden argument*/NULL);
		MatEntry_t638 * L_149 = V_2;
		NullCheck(L_149);
		Material_t82 * L_150 = (L_149->___customMat_1);
		int32_t L_151 = ___readMask;
		NullCheck(L_150);
		Material_SetInt_m3725(L_150, _stringLiteral491, L_151, /*hidden argument*/NULL);
		MatEntry_t638 * L_152 = V_2;
		NullCheck(L_152);
		Material_t82 * L_153 = (L_152->___customMat_1);
		int32_t L_154 = ___writeMask;
		NullCheck(L_153);
		Material_SetInt_m3725(L_153, _stringLiteral497, L_154, /*hidden argument*/NULL);
		MatEntry_t638 * L_155 = V_2;
		NullCheck(L_155);
		Material_t82 * L_156 = (L_155->___customMat_1);
		int32_t L_157 = ___colorWriteMask;
		NullCheck(L_156);
		Material_SetInt_m3725(L_156, _stringLiteral494, L_157, /*hidden argument*/NULL);
		MatEntry_t638 * L_158 = V_2;
		NullCheck(L_158);
		Material_t82 * L_159 = (L_158->___customMat_1);
		MatEntry_t638 * L_160 = V_2;
		NullCheck(L_160);
		bool L_161 = (L_160->___useAlphaClip_8);
		G_B31_0 = _stringLiteral498;
		G_B31_1 = L_159;
		if (!L_161)
		{
			G_B32_0 = _stringLiteral498;
			G_B32_1 = L_159;
			goto IL_0322;
		}
	}
	{
		G_B33_0 = 1;
		G_B33_1 = G_B31_0;
		G_B33_2 = G_B31_1;
		goto IL_0323;
	}

IL_0322:
	{
		G_B33_0 = 0;
		G_B33_1 = G_B32_0;
		G_B33_2 = G_B32_1;
	}

IL_0323:
	{
		NullCheck(G_B33_2);
		Material_SetInt_m3725(G_B33_2, G_B33_1, G_B33_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t639_il2cpp_TypeInfo_var);
		List_1_t640 * L_162 = ((StencilMaterial_t639_StaticFields*)StencilMaterial_t639_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		MatEntry_t638 * L_163 = V_2;
		NullCheck(L_162);
		VirtActionInvoker1< MatEntry_t638 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Add(!0) */, L_162, L_163);
		MatEntry_t638 * L_164 = V_2;
		NullCheck(L_164);
		Material_t82 * L_165 = (L_164->___customMat_1);
		return L_165;
	}
}
// System.Void UnityEngine.UI.StencilMaterial::Remove(UnityEngine.Material)
// UnityEngine.UI.Misc
#include "UnityEngine_UI_UnityEngine_UI_MiscMethodDeclarations.h"
extern TypeInfo* StencilMaterial_t639_il2cpp_TypeInfo_var;
extern "C" void StencilMaterial_Remove_m2958 (Object_t * __this /* static, unused */, Material_t82 * ___customMat, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StencilMaterial_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(476);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	MatEntry_t638 * V_1 = {0};
	int32_t V_2 = 0;
	{
		Material_t82 * L_0 = ___customMat;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		V_0 = 0;
		goto IL_006e;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t639_il2cpp_TypeInfo_var);
		List_1_t640 * L_2 = ((StencilMaterial_t639_StaticFields*)StencilMaterial_t639_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		MatEntry_t638 * L_4 = (MatEntry_t638 *)VirtFuncInvoker1< MatEntry_t638 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Item(System.Int32) */, L_2, L_3);
		V_1 = L_4;
		MatEntry_t638 * L_5 = V_1;
		NullCheck(L_5);
		Material_t82 * L_6 = (L_5->___customMat_1);
		Material_t82 * L_7 = ___customMat;
		bool L_8 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_006a;
	}

IL_0036:
	{
		MatEntry_t638 * L_9 = V_1;
		MatEntry_t638 * L_10 = L_9;
		NullCheck(L_10);
		int32_t L_11 = (L_10->___count_2);
		int32_t L_12 = ((int32_t)((int32_t)L_11-(int32_t)1));
		V_2 = L_12;
		NullCheck(L_10);
		L_10->___count_2 = L_12;
		int32_t L_13 = V_2;
		if (L_13)
		{
			goto IL_0069;
		}
	}
	{
		MatEntry_t638 * L_14 = V_1;
		NullCheck(L_14);
		Material_t82 * L_15 = (L_14->___customMat_1);
		Misc_DestroyImmediate_m2685(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MatEntry_t638 * L_16 = V_1;
		NullCheck(L_16);
		L_16->___baseMat_0 = (Material_t82 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t639_il2cpp_TypeInfo_var);
		List_1_t640 * L_17 = ((StencilMaterial_t639_StaticFields*)StencilMaterial_t639_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		int32_t L_18 = V_0;
		NullCheck(L_17);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::RemoveAt(System.Int32) */, L_17, L_18);
	}

IL_0069:
	{
		return;
	}

IL_006a:
	{
		int32_t L_19 = V_0;
		V_0 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_006e:
	{
		int32_t L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t639_il2cpp_TypeInfo_var);
		List_1_t640 * L_21 = ((StencilMaterial_t639_StaticFields*)StencilMaterial_t639_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		NullCheck(L_21);
		int32_t L_22 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Count() */, L_21);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.StencilMaterial::ClearAll()
extern TypeInfo* StencilMaterial_t639_il2cpp_TypeInfo_var;
extern "C" void StencilMaterial_ClearAll_m2959 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StencilMaterial_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(476);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	MatEntry_t638 * V_1 = {0};
	{
		V_0 = 0;
		goto IL_0029;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t639_il2cpp_TypeInfo_var);
		List_1_t640 * L_0 = ((StencilMaterial_t639_StaticFields*)StencilMaterial_t639_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		MatEntry_t638 * L_2 = (MatEntry_t638 *)VirtFuncInvoker1< MatEntry_t638 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Item(System.Int32) */, L_0, L_1);
		V_1 = L_2;
		MatEntry_t638 * L_3 = V_1;
		NullCheck(L_3);
		Material_t82 * L_4 = (L_3->___customMat_1);
		Misc_DestroyImmediate_m2685(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		MatEntry_t638 * L_5 = V_1;
		NullCheck(L_5);
		L_5->___baseMat_0 = (Material_t82 *)NULL;
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t639_il2cpp_TypeInfo_var);
		List_1_t640 * L_8 = ((StencilMaterial_t639_StaticFields*)StencilMaterial_t639_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Count() */, L_8);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0007;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StencilMaterial_t639_il2cpp_TypeInfo_var);
		List_1_t640 * L_10 = ((StencilMaterial_t639_StaticFields*)StencilMaterial_t639_il2cpp_TypeInfo_var->static_fields)->___m_List_0;
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Clear() */, L_10);
		return;
	}
}
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
// UnityEngine.UI.FontData
#include "UnityEngine_UI_UnityEngine_UI_FontData.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGenerator.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
// UnityEngine.UI.Graphic
#include "UnityEngine_UI_UnityEngine_UI_Graphic.h"
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
// UnityEngine.UI.MaskableGraphic
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic.h"
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.UI.VertexHelper
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.UI.FontData
#include "UnityEngine_UI_UnityEngine_UI_FontDataMethodDeclarations.h"
// UnityEngine.UI.MaskableGraphic
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphicMethodDeclarations.h"
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"
// UnityEngine.Font
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
// UnityEngine.UI.Graphic
#include "UnityEngine_UI_UnityEngine_UI_GraphicMethodDeclarations.h"
// UnityEngine.UI.FontUpdateTracker
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTrackerMethodDeclarations.h"
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
// UnityEngine.UI.CanvasUpdateRegistry
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistryMethodDeclarations.h"
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.UI.VertexHelper
#include "UnityEngine_UI_UnityEngine_UI_VertexHelperMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// System.Void UnityEngine.UI.Text::.ctor()
// UnityEngine.UI.FontData
#include "UnityEngine_UI_UnityEngine_UI_FontDataMethodDeclarations.h"
// UnityEngine.UI.MaskableGraphic
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphicMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* UIVertexU5BU5D_t602_il2cpp_TypeInfo_var;
extern "C" void Text__ctor_m2960 (Text_t196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		UIVertexU5BU5D_t602_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(475);
		s_Il2CppMethodIntialized = true;
	}
	{
		FontData_t568 * L_0 = FontData_get_defaultFontData_m2360(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_FontData_26 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_27 = L_1;
		__this->___m_TempVerts_32 = ((UIVertexU5BU5D_t602*)SZArrayNew(UIVertexU5BU5D_t602_il2cpp_TypeInfo_var, 4));
		MaskableGraphic__ctor_m2659(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Text::.cctor()
extern "C" void Text__cctor_m2961 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGenerator()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"
extern TypeInfo* TextGenerator_t603_il2cpp_TypeInfo_var;
extern "C" TextGenerator_t603 * Text_get_cachedTextGenerator_m2962 (Text_t196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextGenerator_t603_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(458);
		s_Il2CppMethodIntialized = true;
	}
	TextGenerator_t603 * V_0 = {0};
	TextGenerator_t603 * G_B5_0 = {0};
	TextGenerator_t603 * G_B1_0 = {0};
	Text_t196 * G_B3_0 = {0};
	Text_t196 * G_B2_0 = {0};
	TextGenerator_t603 * G_B4_0 = {0};
	Text_t196 * G_B4_1 = {0};
	{
		TextGenerator_t603 * L_0 = (__this->___m_TextCache_28);
		TextGenerator_t603 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B5_0 = L_1;
			goto IL_0040;
		}
	}
	{
		String_t* L_2 = (__this->___m_Text_27);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1336(L_2, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if (!L_3)
		{
			G_B3_0 = __this;
			goto IL_0033;
		}
	}
	{
		String_t* L_4 = (__this->___m_Text_27);
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1336(L_4, /*hidden argument*/NULL);
		TextGenerator_t603 * L_6 = (TextGenerator_t603 *)il2cpp_codegen_object_new (TextGenerator_t603_il2cpp_TypeInfo_var);
		TextGenerator__ctor_m3726(L_6, L_5, /*hidden argument*/NULL);
		G_B4_0 = L_6;
		G_B4_1 = G_B2_0;
		goto IL_0038;
	}

IL_0033:
	{
		TextGenerator_t603 * L_7 = (TextGenerator_t603 *)il2cpp_codegen_object_new (TextGenerator_t603_il2cpp_TypeInfo_var);
		TextGenerator__ctor_m3583(L_7, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B3_0;
	}

IL_0038:
	{
		TextGenerator_t603 * L_8 = G_B4_0;
		V_0 = L_8;
		NullCheck(G_B4_1);
		G_B4_1->___m_TextCache_28 = L_8;
		TextGenerator_t603 * L_9 = V_0;
		G_B5_0 = L_9;
	}

IL_0040:
	{
		return G_B5_0;
	}
}
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGeneratorForLayout()
extern TypeInfo* TextGenerator_t603_il2cpp_TypeInfo_var;
extern "C" TextGenerator_t603 * Text_get_cachedTextGeneratorForLayout_m2963 (Text_t196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextGenerator_t603_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(458);
		s_Il2CppMethodIntialized = true;
	}
	TextGenerator_t603 * V_0 = {0};
	TextGenerator_t603 * G_B2_0 = {0};
	TextGenerator_t603 * G_B1_0 = {0};
	{
		TextGenerator_t603 * L_0 = (__this->___m_TextCacheForLayout_29);
		TextGenerator_t603 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		TextGenerator_t603 * L_2 = (TextGenerator_t603 *)il2cpp_codegen_object_new (TextGenerator_t603_il2cpp_TypeInfo_var);
		TextGenerator__ctor_m3583(L_2, /*hidden argument*/NULL);
		TextGenerator_t603 * L_3 = L_2;
		V_0 = L_3;
		__this->___m_TextCacheForLayout_29 = L_3;
		TextGenerator_t603 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// UnityEngine.Texture UnityEngine.UI.Text::get_mainTexture()
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Font
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.UI.Graphic
#include "UnityEngine_UI_UnityEngine_UI_GraphicMethodDeclarations.h"
extern "C" Texture_t103 * Text_get_mainTexture_m2964 (Text_t196 * __this, const MethodInfo* method)
{
	{
		Font_t569 * L_0 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		Font_t569 * L_2 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_t82 * L_3 = Font_get_material_m3727(L_2, /*hidden argument*/NULL);
		bool L_4 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_3, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0053;
		}
	}
	{
		Font_t569 * L_5 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_t82 * L_6 = Font_get_material_m3727(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Texture_t103 * L_7 = Material_get_mainTexture_m3728(L_6, /*hidden argument*/NULL);
		bool L_8 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_7, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0053;
		}
	}
	{
		Font_t569 * L_9 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_t82 * L_10 = Font_get_material_m3727(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Texture_t103 * L_11 = Material_get_mainTexture_m3728(L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0053:
	{
		Material_t82 * L_12 = (((Graphic_t572 *)__this)->___m_Material_4);
		bool L_13 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_12, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0070;
		}
	}
	{
		Material_t82 * L_14 = (((Graphic_t572 *)__this)->___m_Material_4);
		NullCheck(L_14);
		Texture_t103 * L_15 = Material_get_mainTexture_m3728(L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_0070:
	{
		Texture_t103 * L_16 = Graphic_get_mainTexture_m2410(__this, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void UnityEngine.UI.Text::FontTextureChanged()
// UnityEngine.UI.FontUpdateTracker
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTrackerMethodDeclarations.h"
// UnityEngine.UI.CanvasUpdateRegistry
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistryMethodDeclarations.h"
extern TypeInfo* FontUpdateTracker_t570_il2cpp_TypeInfo_var;
extern TypeInfo* CanvasUpdateRegistry_t547_il2cpp_TypeInfo_var;
extern "C" void Text_FontTextureChanged_m2965 (Text_t196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontUpdateTracker_t570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(428);
		CanvasUpdateRegistry_t547_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(391);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Object_op_Implicit_m1320(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FontUpdateTracker_t570_il2cpp_TypeInfo_var);
		FontUpdateTracker_UntrackText_m2386(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		bool L_1 = (__this->___m_DisableFontTextureRebuiltCallback_31);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		TextGenerator_t603 * L_2 = Text_get_cachedTextGenerator_m2962(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		TextGenerator_Invalidate_m3729(L_2, /*hidden argument*/NULL);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_3)
		{
			goto IL_0035;
		}
	}
	{
		return;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CanvasUpdateRegistry_t547_il2cpp_TypeInfo_var);
		bool L_4 = CanvasUpdateRegistry_IsRebuildingGraphics_m2255(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CanvasUpdateRegistry_t547_il2cpp_TypeInfo_var);
		bool L_5 = CanvasUpdateRegistry_IsRebuildingLayout_m2254(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0054;
		}
	}

IL_0049:
	{
		VirtActionInvoker0::Invoke(30 /* System.Void UnityEngine.UI.Text::UpdateGeometry() */, __this);
		goto IL_005a;
	}

IL_0054:
	{
		VirtActionInvoker0::Invoke(19 /* System.Void UnityEngine.UI.Graphic::SetAllDirty() */, __this);
	}

IL_005a:
	{
		return;
	}
}
// UnityEngine.Font UnityEngine.UI.Text::get_font()
extern "C" Font_t569 * Text_get_font_m2966 (Text_t196 * __this, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		Font_t569 * L_1 = FontData_get_font_m2361(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_font(UnityEngine.Font)
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
extern TypeInfo* FontUpdateTracker_t570_il2cpp_TypeInfo_var;
extern "C" void Text_set_font_m2967 (Text_t196 * __this, Font_t569 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontUpdateTracker_t570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(428);
		s_Il2CppMethodIntialized = true;
	}
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		Font_t569 * L_1 = FontData_get_font_m2361(L_0, /*hidden argument*/NULL);
		Font_t569 * L_2 = ___value;
		bool L_3 = Object_op_Equality_m1413(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FontUpdateTracker_t570_il2cpp_TypeInfo_var);
		FontUpdateTracker_UntrackText_m2386(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		FontData_t568 * L_4 = (__this->___m_FontData_26);
		Font_t569 * L_5 = ___value;
		NullCheck(L_4);
		FontData_set_font_m2362(L_4, L_5, /*hidden argument*/NULL);
		FontUpdateTracker_TrackText_m2384(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(19 /* System.Void UnityEngine.UI.Graphic::SetAllDirty() */, __this);
		return;
	}
}
// System.String UnityEngine.UI.Text::get_text()
extern "C" String_t* Text_get_text_m2968 (Text_t196 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Text_27);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Text::set_text(System.String)
// System.String
#include "mscorlib_System_String.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Text_set_text_m2969 (Text_t196 * __this, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_2 = (__this->___m_Text_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_27 = L_4;
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		goto IL_0056;
	}

IL_0032:
	{
		String_t* L_5 = (__this->___m_Text_27);
		String_t* L_6 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Inequality_m1805(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		String_t* L_8 = ___value;
		__this->___m_Text_27 = L_8;
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
	}

IL_0056:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.Text::get_supportRichText()
extern "C" bool Text_get_supportRichText_m2970 (Text_t196 * __this, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		bool L_1 = FontData_get_richText_m2375(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_supportRichText(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void Text_set_supportRichText_m2971 (Text_t196 * __this, bool ___value, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		bool L_1 = FontData_get_richText_m2375(L_0, /*hidden argument*/NULL);
		bool L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t568 * L_3 = (__this->___m_FontData_26);
		bool L_4 = ___value;
		NullCheck(L_3);
		FontData_set_richText_m2376(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Boolean UnityEngine.UI.Text::get_resizeTextForBestFit()
extern "C" bool Text_get_resizeTextForBestFit_m2972 (Text_t196 * __this, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		bool L_1 = FontData_get_bestFit_m2367(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_resizeTextForBestFit(System.Boolean)
extern "C" void Text_set_resizeTextForBestFit_m2973 (Text_t196 * __this, bool ___value, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		bool L_1 = FontData_get_bestFit_m2367(L_0, /*hidden argument*/NULL);
		bool L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t568 * L_3 = (__this->___m_FontData_26);
		bool L_4 = ___value;
		NullCheck(L_3);
		FontData_set_bestFit_m2368(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Int32 UnityEngine.UI.Text::get_resizeTextMinSize()
extern "C" int32_t Text_get_resizeTextMinSize_m2974 (Text_t196 * __this, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_minSize_m2369(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_resizeTextMinSize(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void Text_set_resizeTextMinSize_m2975 (Text_t196 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_minSize_m2369(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t568 * L_3 = (__this->___m_FontData_26);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_minSize_m2370(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Int32 UnityEngine.UI.Text::get_resizeTextMaxSize()
extern "C" int32_t Text_get_resizeTextMaxSize_m2976 (Text_t196 * __this, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_maxSize_m2371(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_resizeTextMaxSize(System.Int32)
extern "C" void Text_set_resizeTextMaxSize_m2977 (Text_t196 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_maxSize_m2371(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t568 * L_3 = (__this->___m_FontData_26);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_maxSize_m2372(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// UnityEngine.TextAnchor UnityEngine.UI.Text::get_alignment()
extern "C" int32_t Text_get_alignment_m2978 (Text_t196 * __this, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_alignment_m2373(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_alignment(UnityEngine.TextAnchor)
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
extern "C" void Text_set_alignment_m2979 (Text_t196 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_alignment_m2373(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t568 * L_3 = (__this->___m_FontData_26);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_alignment_m2374(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Int32 UnityEngine.UI.Text::get_fontSize()
extern "C" int32_t Text_get_fontSize_m2980 (Text_t196 * __this, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_fontSize_m2363(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_fontSize(System.Int32)
extern "C" void Text_set_fontSize_m2981 (Text_t196 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_fontSize_m2363(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t568 * L_3 = (__this->___m_FontData_26);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_fontSize_m2364(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// UnityEngine.HorizontalWrapMode UnityEngine.UI.Text::get_horizontalOverflow()
extern "C" int32_t Text_get_horizontalOverflow_m2982 (Text_t196 * __this, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_horizontalOverflow_m2377(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_horizontalOverflow(UnityEngine.HorizontalWrapMode)
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
extern "C" void Text_set_horizontalOverflow_m2983 (Text_t196 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_horizontalOverflow_m2377(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t568 * L_3 = (__this->___m_FontData_26);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_horizontalOverflow_m2378(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// UnityEngine.VerticalWrapMode UnityEngine.UI.Text::get_verticalOverflow()
extern "C" int32_t Text_get_verticalOverflow_m2984 (Text_t196 * __this, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_verticalOverflow_m2379(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_verticalOverflow(UnityEngine.VerticalWrapMode)
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
extern "C" void Text_set_verticalOverflow_m2985 (Text_t196 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_verticalOverflow_m2379(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t568 * L_3 = (__this->___m_FontData_26);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_verticalOverflow_m2380(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Single UnityEngine.UI.Text::get_lineSpacing()
extern "C" float Text_get_lineSpacing_m2986 (Text_t196 * __this, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		float L_1 = FontData_get_lineSpacing_m2381(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_lineSpacing(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void Text_set_lineSpacing_m2987 (Text_t196 * __this, float ___value, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		float L_1 = FontData_get_lineSpacing_m2381(L_0, /*hidden argument*/NULL);
		float L_2 = ___value;
		if ((!(((float)L_1) == ((float)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t568 * L_3 = (__this->___m_FontData_26);
		float L_4 = ___value;
		NullCheck(L_3);
		FontData_set_lineSpacing_m2382(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// UnityEngine.FontStyle UnityEngine.UI.Text::get_fontStyle()
extern "C" int32_t Text_get_fontStyle_m2988 (Text_t196 * __this, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_fontStyle_m2365(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.Text::set_fontStyle(UnityEngine.FontStyle)
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
extern "C" void Text_set_fontStyle_m2989 (Text_t196 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		FontData_t568 * L_0 = (__this->___m_FontData_26);
		NullCheck(L_0);
		int32_t L_1 = FontData_get_fontStyle_m2365(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___value;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		FontData_t568 * L_3 = (__this->___m_FontData_26);
		int32_t L_4 = ___value;
		NullCheck(L_3);
		FontData_set_fontStyle_m2366(L_3, L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, __this);
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.Graphic::SetLayoutDirty() */, __this);
		return;
	}
}
// System.Single UnityEngine.UI.Text::get_pixelsPerUnit()
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
extern "C" float Text_get_pixelsPerUnit_m2990 (Text_t196 * __this, const MethodInfo* method)
{
	Canvas_t574 * V_0 = {0};
	{
		Canvas_t574 * L_0 = Graphic_get_canvas_m2403(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Canvas_t574 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m1320(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		return (1.0f);
	}

IL_0018:
	{
		Font_t569 * L_3 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		bool L_4 = Object_op_Implicit_m1320(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		Font_t569 * L_5 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Font_get_dynamic_m3730(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}

IL_0038:
	{
		Canvas_t574 * L_7 = V_0;
		NullCheck(L_7);
		float L_8 = Canvas_get_scaleFactor_m3731(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_003f:
	{
		FontData_t568 * L_9 = (__this->___m_FontData_26);
		NullCheck(L_9);
		int32_t L_10 = FontData_get_fontSize_m2363(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		Font_t569 * L_11 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = Font_get_fontSize_m3732(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_12) > ((int32_t)0)))
		{
			goto IL_0067;
		}
	}

IL_0061:
	{
		return (1.0f);
	}

IL_0067:
	{
		Font_t569 * L_13 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = Font_get_fontSize_m3732(L_13, /*hidden argument*/NULL);
		FontData_t568 * L_15 = (__this->___m_FontData_26);
		NullCheck(L_15);
		int32_t L_16 = FontData_get_fontSize_m2363(L_15, /*hidden argument*/NULL);
		return ((float)((float)(((float)L_14))/(float)(((float)L_16))));
	}
}
// System.Void UnityEngine.UI.Text::OnEnable()
extern TypeInfo* FontUpdateTracker_t570_il2cpp_TypeInfo_var;
extern "C" void Text_OnEnable_m2991 (Text_t196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontUpdateTracker_t570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(428);
		s_Il2CppMethodIntialized = true;
	}
	{
		MaskableGraphic_OnEnable_m2667(__this, /*hidden argument*/NULL);
		TextGenerator_t603 * L_0 = Text_get_cachedTextGenerator_m2962(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		TextGenerator_Invalidate_m3729(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FontUpdateTracker_t570_il2cpp_TypeInfo_var);
		FontUpdateTracker_TrackText_m2384(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Text::OnDisable()
extern TypeInfo* FontUpdateTracker_t570_il2cpp_TypeInfo_var;
extern "C" void Text_OnDisable_m2992 (Text_t196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontUpdateTracker_t570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(428);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FontUpdateTracker_t570_il2cpp_TypeInfo_var);
		FontUpdateTracker_UntrackText_m2386(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		MaskableGraphic_OnDisable_m2668(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Text::UpdateGeometry()
extern "C" void Text_UpdateGeometry_m2993 (Text_t196 * __this, const MethodInfo* method)
{
	{
		Font_t569 * L_0 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		Graphic_UpdateGeometry_m2416(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.UI.Text::GetGenerationSettings(UnityEngine.Vector2)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
extern TypeInfo* TextGenerationSettings_t715_il2cpp_TypeInfo_var;
extern "C" TextGenerationSettings_t715  Text_GetGenerationSettings_m2994 (Text_t196 * __this, Vector2_t2  ___extents, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextGenerationSettings_t715_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	TextGenerationSettings_t715  V_0 = {0};
	{
		Initobj (TextGenerationSettings_t715_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2  L_0 = ___extents;
		(&V_0)->___generationExtents_14 = L_0;
		Font_t569 * L_1 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_1, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0067;
		}
	}
	{
		Font_t569 * L_3 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = Font_get_dynamic_m3730(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0067;
		}
	}
	{
		FontData_t568 * L_5 = (__this->___m_FontData_26);
		NullCheck(L_5);
		int32_t L_6 = FontData_get_fontSize_m2363(L_5, /*hidden argument*/NULL);
		(&V_0)->___fontSize_2 = L_6;
		FontData_t568 * L_7 = (__this->___m_FontData_26);
		NullCheck(L_7);
		int32_t L_8 = FontData_get_minSize_m2369(L_7, /*hidden argument*/NULL);
		(&V_0)->___resizeTextMinSize_9 = L_8;
		FontData_t568 * L_9 = (__this->___m_FontData_26);
		NullCheck(L_9);
		int32_t L_10 = FontData_get_maxSize_m2371(L_9, /*hidden argument*/NULL);
		(&V_0)->___resizeTextMaxSize_10 = L_10;
	}

IL_0067:
	{
		FontData_t568 * L_11 = (__this->___m_FontData_26);
		NullCheck(L_11);
		int32_t L_12 = FontData_get_alignment_m2373(L_11, /*hidden argument*/NULL);
		(&V_0)->___textAnchor_7 = L_12;
		float L_13 = Text_get_pixelsPerUnit_m2990(__this, /*hidden argument*/NULL);
		(&V_0)->___scaleFactor_5 = L_13;
		Color_t9  L_14 = Graphic_get_color_m2390(__this, /*hidden argument*/NULL);
		(&V_0)->___color_1 = L_14;
		Font_t569 * L_15 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		(&V_0)->___font_0 = L_15;
		RectTransform_t556 * L_16 = Graphic_get_rectTransform_m2402(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector2_t2  L_17 = RectTransform_get_pivot_m3476(L_16, /*hidden argument*/NULL);
		(&V_0)->___pivot_15 = L_17;
		FontData_t568 * L_18 = (__this->___m_FontData_26);
		NullCheck(L_18);
		bool L_19 = FontData_get_richText_m2375(L_18, /*hidden argument*/NULL);
		(&V_0)->___richText_4 = L_19;
		FontData_t568 * L_20 = (__this->___m_FontData_26);
		NullCheck(L_20);
		float L_21 = FontData_get_lineSpacing_m2381(L_20, /*hidden argument*/NULL);
		(&V_0)->___lineSpacing_3 = L_21;
		FontData_t568 * L_22 = (__this->___m_FontData_26);
		NullCheck(L_22);
		int32_t L_23 = FontData_get_fontStyle_m2365(L_22, /*hidden argument*/NULL);
		(&V_0)->___fontStyle_6 = L_23;
		FontData_t568 * L_24 = (__this->___m_FontData_26);
		NullCheck(L_24);
		bool L_25 = FontData_get_bestFit_m2367(L_24, /*hidden argument*/NULL);
		(&V_0)->___resizeTextForBestFit_8 = L_25;
		(&V_0)->___updateBounds_11 = 0;
		FontData_t568 * L_26 = (__this->___m_FontData_26);
		NullCheck(L_26);
		int32_t L_27 = FontData_get_horizontalOverflow_m2377(L_26, /*hidden argument*/NULL);
		(&V_0)->___horizontalOverflow_13 = L_27;
		FontData_t568 * L_28 = (__this->___m_FontData_26);
		NullCheck(L_28);
		int32_t L_29 = FontData_get_verticalOverflow_m2379(L_28, /*hidden argument*/NULL);
		(&V_0)->___verticalOverflow_12 = L_29;
		TextGenerationSettings_t715  L_30 = V_0;
		return L_30;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.Text::GetTextAnchorPivot(UnityEngine.TextAnchor)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
extern "C" Vector2_t2  Text_GetTextAnchorPivot_m2995 (Object_t * __this /* static, unused */, int32_t ___anchor, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___anchor;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0091;
		}
		if (L_1 == 1)
		{
			goto IL_00a1;
		}
		if (L_1 == 2)
		{
			goto IL_00b1;
		}
		if (L_1 == 3)
		{
			goto IL_0061;
		}
		if (L_1 == 4)
		{
			goto IL_0071;
		}
		if (L_1 == 5)
		{
			goto IL_0081;
		}
		if (L_1 == 6)
		{
			goto IL_0031;
		}
		if (L_1 == 7)
		{
			goto IL_0041;
		}
		if (L_1 == 8)
		{
			goto IL_0051;
		}
	}
	{
		goto IL_00c1;
	}

IL_0031:
	{
		Vector2_t2  L_2 = {0};
		Vector2__ctor_m1309(&L_2, (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_2;
	}

IL_0041:
	{
		Vector2_t2  L_3 = {0};
		Vector2__ctor_m1309(&L_3, (0.5f), (0.0f), /*hidden argument*/NULL);
		return L_3;
	}

IL_0051:
	{
		Vector2_t2  L_4 = {0};
		Vector2__ctor_m1309(&L_4, (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_4;
	}

IL_0061:
	{
		Vector2_t2  L_5 = {0};
		Vector2__ctor_m1309(&L_5, (0.0f), (0.5f), /*hidden argument*/NULL);
		return L_5;
	}

IL_0071:
	{
		Vector2_t2  L_6 = {0};
		Vector2__ctor_m1309(&L_6, (0.5f), (0.5f), /*hidden argument*/NULL);
		return L_6;
	}

IL_0081:
	{
		Vector2_t2  L_7 = {0};
		Vector2__ctor_m1309(&L_7, (1.0f), (0.5f), /*hidden argument*/NULL);
		return L_7;
	}

IL_0091:
	{
		Vector2_t2  L_8 = {0};
		Vector2__ctor_m1309(&L_8, (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_8;
	}

IL_00a1:
	{
		Vector2_t2  L_9 = {0};
		Vector2__ctor_m1309(&L_9, (0.5f), (1.0f), /*hidden argument*/NULL);
		return L_9;
	}

IL_00b1:
	{
		Vector2_t2  L_10 = {0};
		Vector2__ctor_m1309(&L_10, (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_10;
	}

IL_00c1:
	{
		Vector2_t2  L_11 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void UnityEngine.UI.Text::OnPopulateMesh(UnityEngine.Mesh)
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.UI.VertexHelper
#include "UnityEngine_UI_UnityEngine_UI_VertexHelperMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
extern TypeInfo* Text_t196_il2cpp_TypeInfo_var;
extern TypeInfo* ICollection_1_t762_il2cpp_TypeInfo_var;
extern TypeInfo* VertexHelper_t674_il2cpp_TypeInfo_var;
extern TypeInfo* IList_1_t763_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void Text_OnPopulateMesh_m2996 (Text_t196 * __this, Mesh_t104 * ___toFill, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Text_t196_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(398);
		ICollection_1_t762_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		VertexHelper_t674_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(440);
		IList_1_t763_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(505);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2  V_0 = {0};
	TextGenerationSettings_t715  V_1 = {0};
	Rect_t267  V_2 = {0};
	Vector2_t2  V_3 = {0};
	Vector2_t2  V_4 = {0};
	Vector2_t2  V_5 = {0};
	Object_t* V_6 = {0};
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	VertexHelper_t674 * V_9 = {0};
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	Rect_t267  V_14 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Vector2_t2 * G_B4_0 = {0};
	Vector2_t2 * G_B3_0 = {0};
	float G_B5_0 = 0.0f;
	Vector2_t2 * G_B5_1 = {0};
	Vector2_t2 * G_B7_0 = {0};
	Vector2_t2 * G_B6_0 = {0};
	float G_B8_0 = 0.0f;
	Vector2_t2 * G_B8_1 = {0};
	{
		Font_t569 * L_0 = Text_get_font_m2966(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		__this->___m_DisableFontTextureRebuiltCallback_31 = 1;
		RectTransform_t556 * L_2 = Graphic_get_rectTransform_m2402(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rect_t267  L_3 = RectTransform_get_rect_m3463(L_2, /*hidden argument*/NULL);
		V_14 = L_3;
		Vector2_t2  L_4 = Rect_get_size_m3466((&V_14), /*hidden argument*/NULL);
		V_0 = L_4;
		Vector2_t2  L_5 = V_0;
		TextGenerationSettings_t715  L_6 = Text_GetGenerationSettings_m2994(__this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		TextGenerator_t603 * L_7 = Text_get_cachedTextGenerator_m2962(__this, /*hidden argument*/NULL);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(59 /* System.String UnityEngine.UI.Text::get_text() */, __this);
		TextGenerationSettings_t715  L_9 = V_1;
		NullCheck(L_7);
		TextGenerator_Populate_m3627(L_7, L_8, L_9, /*hidden argument*/NULL);
		RectTransform_t556 * L_10 = Graphic_get_rectTransform_m2402(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Rect_t267  L_11 = RectTransform_get_rect_m3463(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		FontData_t568 * L_12 = (__this->___m_FontData_26);
		NullCheck(L_12);
		int32_t L_13 = FontData_get_alignment_m2373(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Text_t196_il2cpp_TypeInfo_var);
		Vector2_t2  L_14 = Text_GetTextAnchorPivot_m2995(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		Vector2_t2  L_15 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = ((&V_3)->___x_1);
		G_B3_0 = (&V_4);
		if ((!(((float)L_16) == ((float)(1.0f)))))
		{
			G_B4_0 = (&V_4);
			goto IL_008c;
		}
	}
	{
		float L_17 = Rect_get_xMax_m1310((&V_2), /*hidden argument*/NULL);
		G_B5_0 = L_17;
		G_B5_1 = G_B3_0;
		goto IL_0093;
	}

IL_008c:
	{
		float L_18 = Rect_get_xMin_m1307((&V_2), /*hidden argument*/NULL);
		G_B5_0 = L_18;
		G_B5_1 = G_B4_0;
	}

IL_0093:
	{
		G_B5_1->___x_1 = G_B5_0;
		float L_19 = ((&V_3)->___y_2);
		G_B6_0 = (&V_4);
		if ((!(((float)L_19) == ((float)(0.0f)))))
		{
			G_B7_0 = (&V_4);
			goto IL_00b7;
		}
	}
	{
		float L_20 = Rect_get_yMin_m1308((&V_2), /*hidden argument*/NULL);
		G_B8_0 = L_20;
		G_B8_1 = G_B6_0;
		goto IL_00be;
	}

IL_00b7:
	{
		float L_21 = Rect_get_yMax_m1311((&V_2), /*hidden argument*/NULL);
		G_B8_0 = L_21;
		G_B8_1 = G_B7_0;
	}

IL_00be:
	{
		G_B8_1->___y_2 = G_B8_0;
		Vector2_t2  L_22 = V_4;
		Vector2_t2  L_23 = Graphic_PixelAdjustPoint_m2423(__this, L_22, /*hidden argument*/NULL);
		Vector2_t2  L_24 = V_4;
		Vector2_t2  L_25 = Vector2_op_Subtraction_m1314(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		TextGenerator_t603 * L_26 = Text_get_cachedTextGenerator_m2962(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Object_t* L_27 = TextGenerator_get_verts_m3733(L_26, /*hidden argument*/NULL);
		V_6 = L_27;
		float L_28 = Text_get_pixelsPerUnit_m2990(__this, /*hidden argument*/NULL);
		V_7 = ((float)((float)(1.0f)/(float)L_28));
		Object_t* L_29 = V_6;
		NullCheck(L_29);
		int32_t L_30 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>::get_Count() */, ICollection_1_t762_il2cpp_TypeInfo_var, L_29);
		V_8 = ((int32_t)((int32_t)L_30-(int32_t)4));
		VertexHelper_t674 * L_31 = (VertexHelper_t674 *)il2cpp_codegen_object_new (VertexHelper_t674_il2cpp_TypeInfo_var);
		VertexHelper__ctor_m3235(L_31, /*hidden argument*/NULL);
		V_9 = L_31;
	}

IL_0101:
	try
	{ // begin try (depth: 1)
		{
			Vector2_t2  L_32 = V_5;
			Vector2_t2  L_33 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
			bool L_34 = Vector2_op_Inequality_m3636(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
			if (!L_34)
			{
				goto IL_01cd;
			}
		}

IL_0112:
		{
			V_10 = 0;
			goto IL_01bf;
		}

IL_011a:
		{
			int32_t L_35 = V_10;
			V_11 = ((int32_t)((int32_t)L_35&(int32_t)3));
			UIVertexU5BU5D_t602* L_36 = (__this->___m_TempVerts_32);
			int32_t L_37 = V_11;
			NullCheck(L_36);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
			Object_t* L_38 = V_6;
			int32_t L_39 = V_10;
			NullCheck(L_38);
			UIVertex_t605  L_40 = (UIVertex_t605 )InterfaceFuncInvoker1< UIVertex_t605 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, IList_1_t763_il2cpp_TypeInfo_var, L_38, L_39);
			*((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_36, L_37, sizeof(UIVertex_t605 ))) = L_40;
			UIVertexU5BU5D_t602* L_41 = (__this->___m_TempVerts_32);
			int32_t L_42 = V_11;
			NullCheck(L_41);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
			UIVertex_t605 * L_43 = ((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_41, L_42, sizeof(UIVertex_t605 )));
			Vector3_t36  L_44 = (L_43->___position_0);
			float L_45 = V_7;
			Vector3_t36  L_46 = Vector3_op_Multiply_m1387(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
			L_43->___position_0 = L_46;
			UIVertexU5BU5D_t602* L_47 = (__this->___m_TempVerts_32);
			int32_t L_48 = V_11;
			NullCheck(L_47);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
			Vector3_t36 * L_49 = &(((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_47, L_48, sizeof(UIVertex_t605 )))->___position_0);
			Vector3_t36 * L_50 = L_49;
			float L_51 = (L_50->___x_1);
			float L_52 = ((&V_5)->___x_1);
			L_50->___x_1 = ((float)((float)L_51+(float)L_52));
			UIVertexU5BU5D_t602* L_53 = (__this->___m_TempVerts_32);
			int32_t L_54 = V_11;
			NullCheck(L_53);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
			Vector3_t36 * L_55 = &(((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_53, L_54, sizeof(UIVertex_t605 )))->___position_0);
			Vector3_t36 * L_56 = L_55;
			float L_57 = (L_56->___y_2);
			float L_58 = ((&V_5)->___y_2);
			L_56->___y_2 = ((float)((float)L_57+(float)L_58));
			int32_t L_59 = V_11;
			if ((!(((uint32_t)L_59) == ((uint32_t)3))))
			{
				goto IL_01b9;
			}
		}

IL_01ac:
		{
			VertexHelper_t674 * L_60 = V_9;
			UIVertexU5BU5D_t602* L_61 = (__this->___m_TempVerts_32);
			NullCheck(L_60);
			VertexHelper_AddUIVertexQuad_m3246(L_60, L_61, /*hidden argument*/NULL);
		}

IL_01b9:
		{
			int32_t L_62 = V_10;
			V_10 = ((int32_t)((int32_t)L_62+(int32_t)1));
		}

IL_01bf:
		{
			int32_t L_63 = V_10;
			int32_t L_64 = V_8;
			if ((((int32_t)L_63) < ((int32_t)L_64)))
			{
				goto IL_011a;
			}
		}

IL_01c8:
		{
			goto IL_0239;
		}

IL_01cd:
		{
			V_12 = 0;
			goto IL_0230;
		}

IL_01d5:
		{
			int32_t L_65 = V_12;
			V_13 = ((int32_t)((int32_t)L_65&(int32_t)3));
			UIVertexU5BU5D_t602* L_66 = (__this->___m_TempVerts_32);
			int32_t L_67 = V_13;
			NullCheck(L_66);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
			Object_t* L_68 = V_6;
			int32_t L_69 = V_12;
			NullCheck(L_68);
			UIVertex_t605  L_70 = (UIVertex_t605 )InterfaceFuncInvoker1< UIVertex_t605 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, IList_1_t763_il2cpp_TypeInfo_var, L_68, L_69);
			*((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_66, L_67, sizeof(UIVertex_t605 ))) = L_70;
			UIVertexU5BU5D_t602* L_71 = (__this->___m_TempVerts_32);
			int32_t L_72 = V_13;
			NullCheck(L_71);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_71, L_72);
			UIVertex_t605 * L_73 = ((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_71, L_72, sizeof(UIVertex_t605 )));
			Vector3_t36  L_74 = (L_73->___position_0);
			float L_75 = V_7;
			Vector3_t36  L_76 = Vector3_op_Multiply_m1387(NULL /*static, unused*/, L_74, L_75, /*hidden argument*/NULL);
			L_73->___position_0 = L_76;
			int32_t L_77 = V_13;
			if ((!(((uint32_t)L_77) == ((uint32_t)3))))
			{
				goto IL_022a;
			}
		}

IL_021d:
		{
			VertexHelper_t674 * L_78 = V_9;
			UIVertexU5BU5D_t602* L_79 = (__this->___m_TempVerts_32);
			NullCheck(L_78);
			VertexHelper_AddUIVertexQuad_m3246(L_78, L_79, /*hidden argument*/NULL);
		}

IL_022a:
		{
			int32_t L_80 = V_12;
			V_12 = ((int32_t)((int32_t)L_80+(int32_t)1));
		}

IL_0230:
		{
			int32_t L_81 = V_12;
			int32_t L_82 = V_8;
			if ((((int32_t)L_81) < ((int32_t)L_82)))
			{
				goto IL_01d5;
			}
		}

IL_0239:
		{
			VertexHelper_t674 * L_83 = V_9;
			Mesh_t104 * L_84 = ___toFill;
			NullCheck(L_83);
			VertexHelper_FillMesh_m3240(L_83, L_84, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x255, FINALLY_0246);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0246;
	}

FINALLY_0246:
	{ // begin finally (depth: 1)
		{
			VertexHelper_t674 * L_85 = V_9;
			if (!L_85)
			{
				goto IL_0254;
			}
		}

IL_024d:
		{
			VertexHelper_t674 * L_86 = V_9;
			NullCheck(L_86);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_86);
		}

IL_0254:
		{
			IL2CPP_END_FINALLY(582)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(582)
	{
		IL2CPP_JUMP_TBL(0x255, IL_0255)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0255:
	{
		__this->___m_DisableFontTextureRebuiltCallback_31 = 0;
		return;
	}
}
// System.Void UnityEngine.UI.Text::CalculateLayoutInputHorizontal()
extern "C" void Text_CalculateLayoutInputHorizontal_m2997 (Text_t196 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.Text::CalculateLayoutInputVertical()
extern "C" void Text_CalculateLayoutInputVertical_m2998 (Text_t196 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Single UnityEngine.UI.Text::get_minWidth()
extern "C" float Text_get_minWidth_m2999 (Text_t196 * __this, const MethodInfo* method)
{
	{
		return (0.0f);
	}
}
// System.Single UnityEngine.UI.Text::get_preferredWidth()
extern "C" float Text_get_preferredWidth_m3000 (Text_t196 * __this, const MethodInfo* method)
{
	TextGenerationSettings_t715  V_0 = {0};
	{
		Vector2_t2  L_0 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		TextGenerationSettings_t715  L_1 = Text_GetGenerationSettings_m2994(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		TextGenerator_t603 * L_2 = Text_get_cachedTextGeneratorForLayout_m2963(__this, /*hidden argument*/NULL);
		String_t* L_3 = (__this->___m_Text_27);
		TextGenerationSettings_t715  L_4 = V_0;
		NullCheck(L_2);
		float L_5 = TextGenerator_GetPreferredWidth_m3734(L_2, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Text_get_pixelsPerUnit_m2990(__this, /*hidden argument*/NULL);
		return ((float)((float)L_5/(float)L_6));
	}
}
// System.Single UnityEngine.UI.Text::get_flexibleWidth()
extern "C" float Text_get_flexibleWidth_m3001 (Text_t196 * __this, const MethodInfo* method)
{
	{
		return (-1.0f);
	}
}
// System.Single UnityEngine.UI.Text::get_minHeight()
extern "C" float Text_get_minHeight_m3002 (Text_t196 * __this, const MethodInfo* method)
{
	{
		return (0.0f);
	}
}
// System.Single UnityEngine.UI.Text::get_preferredHeight()
extern "C" float Text_get_preferredHeight_m3003 (Text_t196 * __this, const MethodInfo* method)
{
	TextGenerationSettings_t715  V_0 = {0};
	Rect_t267  V_1 = {0};
	Vector2_t2  V_2 = {0};
	{
		RectTransform_t556 * L_0 = Graphic_get_rectTransform_m2402(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rect_t267  L_1 = RectTransform_get_rect_m3463(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector2_t2  L_2 = Rect_get_size_m3466((&V_1), /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = ((&V_2)->___x_1);
		Vector2_t2  L_4 = {0};
		Vector2__ctor_m1309(&L_4, L_3, (0.0f), /*hidden argument*/NULL);
		TextGenerationSettings_t715  L_5 = Text_GetGenerationSettings_m2994(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		TextGenerator_t603 * L_6 = Text_get_cachedTextGeneratorForLayout_m2963(__this, /*hidden argument*/NULL);
		String_t* L_7 = (__this->___m_Text_27);
		TextGenerationSettings_t715  L_8 = V_0;
		NullCheck(L_6);
		float L_9 = TextGenerator_GetPreferredHeight_m3735(L_6, L_7, L_8, /*hidden argument*/NULL);
		float L_10 = Text_get_pixelsPerUnit_m2990(__this, /*hidden argument*/NULL);
		return ((float)((float)L_9/(float)L_10));
	}
}
// System.Single UnityEngine.UI.Text::get_flexibleHeight()
extern "C" float Text_get_flexibleHeight_m3004 (Text_t196 * __this, const MethodInfo* method)
{
	{
		return (-1.0f);
	}
}
// System.Int32 UnityEngine.UI.Text::get_layoutPriority()
extern "C" int32_t Text_get_layoutPriority_m3005 (Text_t196 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransition.h"
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransitionMethodDeclarations.h"
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent.h"
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEventMethodDeclarations.h"
// UnityEngine.Events.UnityEvent`1<System.Boolean>
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_4MethodDeclarations.h"
// System.Void UnityEngine.UI.Toggle/ToggleEvent::.ctor()
// UnityEngine.Events.UnityEvent`1<System.Boolean>
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_4MethodDeclarations.h"
extern const MethodInfo* UnityEvent_1__ctor_m3654_MethodInfo_var;
extern "C" void ToggleEvent__ctor_m3006 (ToggleEvent_t642 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityEvent_1__ctor_m3654_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484126);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityEvent_1__ctor_m3654(__this, /*hidden argument*/UnityEvent_1__ctor_m3654_MethodInfo_var);
		return;
	}
}
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup.h"
// UnityEngine.UI.CanvasUpdate
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate.h"
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
// UnityEngine.EventSystems.PointerEventData/InputButton
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
// UnityEngine.EventSystems.BaseEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventData.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_SelectableMethodDeclarations.h"
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroupMethodDeclarations.h"
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventDataMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// System.Void UnityEngine.UI.Toggle::.ctor()
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEventMethodDeclarations.h"
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_SelectableMethodDeclarations.h"
extern TypeInfo* ToggleEvent_t642_il2cpp_TypeInfo_var;
extern TypeInfo* Selectable_t545_il2cpp_TypeInfo_var;
extern "C" void Toggle__ctor_m3007 (Toggle_t557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ToggleEvent_t642_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		Selectable_t545_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(385);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___toggleTransition_16 = 1;
		ToggleEvent_t642 * L_0 = (ToggleEvent_t642 *)il2cpp_codegen_object_new (ToggleEvent_t642_il2cpp_TypeInfo_var);
		ToggleEvent__ctor_m3006(L_0, /*hidden argument*/NULL);
		__this->___onValueChanged_19 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Selectable_t545_il2cpp_TypeInfo_var);
		Selectable__ctor_m2840(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::get_group()
extern "C" ToggleGroup_t643 * Toggle_get_group_m3008 (Toggle_t557 * __this, const MethodInfo* method)
{
	{
		ToggleGroup_t643 * L_0 = (__this->___m_Group_18);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Toggle::set_group(UnityEngine.UI.ToggleGroup)
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup.h"
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
extern "C" void Toggle_set_group_m3009 (Toggle_t557 * __this, ToggleGroup_t643 * ___value, const MethodInfo* method)
{
	{
		ToggleGroup_t643 * L_0 = ___value;
		__this->___m_Group_18 = L_0;
		ToggleGroup_t643 * L_1 = (__this->___m_Group_18);
		Toggle_SetToggleGroup_m3013(__this, L_1, 1, /*hidden argument*/NULL);
		Toggle_PlayEffect_m3018(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::Rebuild(UnityEngine.UI.CanvasUpdate)
// UnityEngine.UI.CanvasUpdate
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate.h"
extern "C" void Toggle_Rebuild_m3010 (Toggle_t557 * __this, int32_t ___executing, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::OnEnable()
extern "C" void Toggle_OnEnable_m3011 (Toggle_t557 * __this, const MethodInfo* method)
{
	{
		Selectable_OnEnable_m2870(__this, /*hidden argument*/NULL);
		ToggleGroup_t643 * L_0 = (__this->___m_Group_18);
		Toggle_SetToggleGroup_m3013(__this, L_0, 0, /*hidden argument*/NULL);
		Toggle_PlayEffect_m3018(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::OnDisable()
extern "C" void Toggle_OnDisable_m3012 (Toggle_t557 * __this, const MethodInfo* method)
{
	{
		Toggle_SetToggleGroup_m3013(__this, (ToggleGroup_t643 *)NULL, 0, /*hidden argument*/NULL);
		Selectable_OnDisable_m2872(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::SetToggleGroup(UnityEngine.UI.ToggleGroup,System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroupMethodDeclarations.h"
extern "C" void Toggle_SetToggleGroup_m3013 (Toggle_t557 * __this, ToggleGroup_t643 * ___newGroup, bool ___setMemberValue, const MethodInfo* method)
{
	ToggleGroup_t643 * V_0 = {0};
	{
		ToggleGroup_t643 * L_0 = (__this->___m_Group_18);
		V_0 = L_0;
		ToggleGroup_t643 * L_1 = (__this->___m_Group_18);
		bool L_2 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_1, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		ToggleGroup_t643 * L_3 = (__this->___m_Group_18);
		NullCheck(L_3);
		ToggleGroup_UnregisterToggle_m3030(L_3, __this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		bool L_4 = ___setMemberValue;
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		ToggleGroup_t643 * L_5 = ___newGroup;
		__this->___m_Group_18 = L_5;
	}

IL_0031:
	{
		ToggleGroup_t643 * L_6 = (__this->___m_Group_18);
		bool L_7 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_6, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0059;
		}
	}
	{
		bool L_8 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (!L_8)
		{
			goto IL_0059;
		}
	}
	{
		ToggleGroup_t643 * L_9 = (__this->___m_Group_18);
		NullCheck(L_9);
		ToggleGroup_RegisterToggle_m3031(L_9, __this, /*hidden argument*/NULL);
	}

IL_0059:
	{
		ToggleGroup_t643 * L_10 = ___newGroup;
		bool L_11 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_10, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0093;
		}
	}
	{
		ToggleGroup_t643 * L_12 = ___newGroup;
		ToggleGroup_t643 * L_13 = V_0;
		bool L_14 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0093;
		}
	}
	{
		bool L_15 = Toggle_get_isOn_m3014(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0093;
		}
	}
	{
		bool L_16 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (!L_16)
		{
			goto IL_0093;
		}
	}
	{
		ToggleGroup_t643 * L_17 = (__this->___m_Group_18);
		NullCheck(L_17);
		ToggleGroup_NotifyToggleOn_m3029(L_17, __this, /*hidden argument*/NULL);
	}

IL_0093:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.Toggle::get_isOn()
extern "C" bool Toggle_get_isOn_m3014 (Toggle_t557 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_IsOn_20);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Toggle::set_isOn(System.Boolean)
extern "C" void Toggle_set_isOn_m3015 (Toggle_t557 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		Toggle_Set_m3016(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean)
extern "C" void Toggle_Set_m3016 (Toggle_t557 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		Toggle_Set_m3017(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean,System.Boolean)
// UnityEngine.Events.UnityEvent`1<System.Boolean>
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_4MethodDeclarations.h"
extern const MethodInfo* UnityEvent_1_Invoke_m3659_MethodInfo_var;
extern "C" void Toggle_Set_m3017 (Toggle_t557 * __this, bool ___value, bool ___sendCallback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityEvent_1_Invoke_m3659_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484128);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_IsOn_20);
		bool L_1 = ___value;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___value;
		__this->___m_IsOn_20 = L_2;
		ToggleGroup_t643 * L_3 = (__this->___m_Group_18);
		bool L_4 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_3, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006e;
		}
	}
	{
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (!L_5)
		{
			goto IL_006e;
		}
	}
	{
		bool L_6 = (__this->___m_IsOn_20);
		if (L_6)
		{
			goto IL_005b;
		}
	}
	{
		ToggleGroup_t643 * L_7 = (__this->___m_Group_18);
		NullCheck(L_7);
		bool L_8 = ToggleGroup_AnyTogglesOn_m3032(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_006e;
		}
	}
	{
		ToggleGroup_t643 * L_9 = (__this->___m_Group_18);
		NullCheck(L_9);
		bool L_10 = ToggleGroup_get_allowSwitchOff_m3026(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_006e;
		}
	}

IL_005b:
	{
		__this->___m_IsOn_20 = 1;
		ToggleGroup_t643 * L_11 = (__this->___m_Group_18);
		NullCheck(L_11);
		ToggleGroup_NotifyToggleOn_m3029(L_11, __this, /*hidden argument*/NULL);
	}

IL_006e:
	{
		int32_t L_12 = (__this->___toggleTransition_16);
		Toggle_PlayEffect_m3018(__this, ((((int32_t)L_12) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		bool L_13 = ___sendCallback;
		if (!L_13)
		{
			goto IL_0094;
		}
	}
	{
		ToggleEvent_t642 * L_14 = (__this->___onValueChanged_19);
		bool L_15 = (__this->___m_IsOn_20);
		NullCheck(L_14);
		UnityEvent_1_Invoke_m3659(L_14, L_15, /*hidden argument*/UnityEvent_1_Invoke_m3659_MethodInfo_var);
	}

IL_0094:
	{
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::PlayEffect(System.Boolean)
// UnityEngine.UI.Graphic
#include "UnityEngine_UI_UnityEngine_UI_GraphicMethodDeclarations.h"
extern "C" void Toggle_PlayEffect_m3018 (Toggle_t557 * __this, bool ___instant, const MethodInfo* method)
{
	Graphic_t572 * G_B4_0 = {0};
	Graphic_t572 * G_B3_0 = {0};
	float G_B5_0 = 0.0f;
	Graphic_t572 * G_B5_1 = {0};
	float G_B7_0 = 0.0f;
	Graphic_t572 * G_B7_1 = {0};
	float G_B6_0 = 0.0f;
	Graphic_t572 * G_B6_1 = {0};
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	Graphic_t572 * G_B8_2 = {0};
	{
		Graphic_t572 * L_0 = (__this->___graphic_17);
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Graphic_t572 * L_2 = (__this->___graphic_17);
		bool L_3 = (__this->___m_IsOn_20);
		G_B3_0 = L_2;
		if (!L_3)
		{
			G_B4_0 = L_2;
			goto IL_002d;
		}
	}
	{
		G_B5_0 = (1.0f);
		G_B5_1 = G_B3_0;
		goto IL_0032;
	}

IL_002d:
	{
		G_B5_0 = (0.0f);
		G_B5_1 = G_B4_0;
	}

IL_0032:
	{
		bool L_4 = ___instant;
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
		if (!L_4)
		{
			G_B7_0 = G_B5_0;
			G_B7_1 = G_B5_1;
			goto IL_0042;
		}
	}
	{
		G_B8_0 = (0.0f);
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		goto IL_0047;
	}

IL_0042:
	{
		G_B8_0 = (0.1f);
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
	}

IL_0047:
	{
		NullCheck(G_B8_2);
		Graphic_CrossFadeAlpha_m2428(G_B8_2, G_B8_1, G_B8_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::Start()
extern "C" void Toggle_Start_m3019 (Toggle_t557 * __this, const MethodInfo* method)
{
	{
		Toggle_PlayEffect_m3018(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::InternalToggle()
extern "C" void Toggle_InternalToggle_m3020 (Toggle_t557 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(23 /* System.Boolean UnityEngine.UI.Selectable::IsInteractable() */, __this);
		if (L_1)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		bool L_2 = Toggle_get_isOn_m3014(__this, /*hidden argument*/NULL);
		Toggle_set_isOn_m3015(__this, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventDataMethodDeclarations.h"
extern "C" void Toggle_OnPointerClick_m3021 (Toggle_t557 * __this, PointerEventData_t517 * ___eventData, const MethodInfo* method)
{
	{
		PointerEventData_t517 * L_0 = ___eventData;
		NullCheck(L_0);
		int32_t L_1 = PointerEventData_get_button_m2069(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Toggle_InternalToggle_m3020(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Toggle::OnSubmit(UnityEngine.EventSystems.BaseEventData)
// UnityEngine.EventSystems.BaseEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventData.h"
extern "C" void Toggle_OnSubmit_m3022 (Toggle_t557 * __this, BaseEventData_t480 * ___eventData, const MethodInfo* method)
{
	{
		Toggle_InternalToggle_m3020(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.IsDestroyed()
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
extern "C" bool Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m3023 (Toggle_t557 * __this, const MethodInfo* method)
{
	{
		bool L_0 = UIBehaviour_IsDestroyed_m2019(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.get_transform()
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern "C" Transform_t35 * Toggle_UnityEngine_UI_ICanvasElement_get_transform_m3024 (Toggle_t557 * __this, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = Component_get_transform_m1417(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
#include "mscorlib_System_Collections_Generic_List_1_gen_34.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.Predicate`1<UnityEngine.UI.Toggle>
#include "mscorlib_System_Predicate_1_gen_0.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
#include "System_Core_System_Func_2_gen_2.h"
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
#include "mscorlib_System_Collections_Generic_List_1_gen_34MethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.Predicate`1<UnityEngine.UI.Toggle>
#include "mscorlib_System_Predicate_1_gen_0MethodDeclarations.h"
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
#include "System_Core_System_Func_2_gen_2MethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
struct IEnumerable_1_t716;
struct Func_2_t646;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct IEnumerable_1_t376;
struct Func_2_t432;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C" Object_t* Enumerable_Where_TisObject_t_m1630_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t432 * p1, const MethodInfo* method);
#define Enumerable_Where_TisObject_t_m1630(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t432 *, const MethodInfo*))Enumerable_Where_TisObject_t_m1630_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.UI.Toggle>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.UI.Toggle>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisToggle_t557_m3741(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t646 *, const MethodInfo*))Enumerable_Where_TisObject_t_m1630_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void UnityEngine.UI.ToggleGroup::.ctor()
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
#include "mscorlib_System_Collections_Generic_List_1_gen_34MethodDeclarations.h"
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
extern TypeInfo* List_1_t644_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3736_MethodInfo_var;
extern "C" void ToggleGroup__ctor_m3025 (ToggleGroup_t643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t644_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(507);
		List_1__ctor_m3736_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484152);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t644 * L_0 = (List_1_t644 *)il2cpp_codegen_object_new (List_1_t644_il2cpp_TypeInfo_var);
		List_1__ctor_m3736(L_0, /*hidden argument*/List_1__ctor_m3736_MethodInfo_var);
		__this->___m_Toggles_3 = L_0;
		UIBehaviour__ctor_m2006(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.ToggleGroup::get_allowSwitchOff()
extern "C" bool ToggleGroup_get_allowSwitchOff_m3026 (ToggleGroup_t643 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_AllowSwitchOff_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::set_allowSwitchOff(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void ToggleGroup_set_allowSwitchOff_m3027 (ToggleGroup_t643 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_AllowSwitchOff_2 = L_0;
		return;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::ValidateToggleIsInGroup(UnityEngine.UI.Toggle)
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral499;
extern "C" void ToggleGroup_ValidateToggleIsInGroup_m3028 (ToggleGroup_t643 * __this, Toggle_t557 * ___toggle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral499 = il2cpp_codegen_string_literal_from_index(499);
		s_Il2CppMethodIntialized = true;
	}
	{
		Toggle_t557 * L_0 = ___toggle;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		List_1_t644 * L_2 = (__this->___m_Toggles_3);
		Toggle_t557 * L_3 = ___toggle;
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker1< bool, Toggle_t557 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Contains(!0) */, L_2, L_3);
		if (L_4)
		{
			goto IL_003b;
		}
	}

IL_001d:
	{
		ObjectU5BU5D_t356* L_5 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 2));
		Toggle_t557 * L_6 = ___toggle;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 0, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t356* L_7 = L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, __this);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 1, sizeof(Object_t *))) = (Object_t *)__this;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m3724(NULL /*static, unused*/, _stringLiteral499, L_7, /*hidden argument*/NULL);
		ArgumentException_t764 * L_9 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_9, L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::NotifyToggleOn(UnityEngine.UI.Toggle)
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroupMethodDeclarations.h"
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
extern "C" void ToggleGroup_NotifyToggleOn_m3029 (ToggleGroup_t643 * __this, Toggle_t557 * ___toggle, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Toggle_t557 * L_0 = ___toggle;
		ToggleGroup_ValidateToggleIsInGroup_m3028(__this, L_0, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0040;
	}

IL_000e:
	{
		List_1_t644 * L_1 = (__this->___m_Toggles_3);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Toggle_t557 * L_3 = (Toggle_t557 *)VirtFuncInvoker1< Toggle_t557 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Item(System.Int32) */, L_1, L_2);
		Toggle_t557 * L_4 = ___toggle;
		bool L_5 = Object_op_Equality_m1413(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		goto IL_003c;
	}

IL_002a:
	{
		List_1_t644 * L_6 = (__this->___m_Toggles_3);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Toggle_t557 * L_8 = (Toggle_t557 *)VirtFuncInvoker1< Toggle_t557 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Item(System.Int32) */, L_6, L_7);
		NullCheck(L_8);
		Toggle_set_isOn_m3015(L_8, 0, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_10 = V_0;
		List_1_t644 * L_11 = (__this->___m_Toggles_3);
		NullCheck(L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::UnregisterToggle(UnityEngine.UI.Toggle)
extern "C" void ToggleGroup_UnregisterToggle_m3030 (ToggleGroup_t643 * __this, Toggle_t557 * ___toggle, const MethodInfo* method)
{
	{
		List_1_t644 * L_0 = (__this->___m_Toggles_3);
		Toggle_t557 * L_1 = ___toggle;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Toggle_t557 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Contains(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t644 * L_3 = (__this->___m_Toggles_3);
		Toggle_t557 * L_4 = ___toggle;
		NullCheck(L_3);
		VirtFuncInvoker1< bool, Toggle_t557 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Remove(!0) */, L_3, L_4);
	}

IL_001e:
	{
		return;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::RegisterToggle(UnityEngine.UI.Toggle)
extern "C" void ToggleGroup_RegisterToggle_m3031 (ToggleGroup_t643 * __this, Toggle_t557 * ___toggle, const MethodInfo* method)
{
	{
		List_1_t644 * L_0 = (__this->___m_Toggles_3);
		Toggle_t557 * L_1 = ___toggle;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Toggle_t557 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Contains(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		List_1_t644 * L_3 = (__this->___m_Toggles_3);
		Toggle_t557 * L_4 = ___toggle;
		NullCheck(L_3);
		VirtActionInvoker1< Toggle_t557 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Add(!0) */, L_3, L_4);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.ToggleGroup::AnyTogglesOn()
// System.Predicate`1<UnityEngine.UI.Toggle>
#include "mscorlib_System_Predicate_1_gen_0MethodDeclarations.h"
extern TypeInfo* ToggleGroup_t643_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t645_il2cpp_TypeInfo_var;
extern const MethodInfo* ToggleGroup_U3CAnyTogglesOnU3Em__6_m3035_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m3738_MethodInfo_var;
extern const MethodInfo* List_1_Find_m3739_MethodInfo_var;
extern "C" bool ToggleGroup_AnyTogglesOn_m3032 (ToggleGroup_t643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ToggleGroup_t643_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(509);
		Predicate_1_t645_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(510);
		ToggleGroup_U3CAnyTogglesOnU3Em__6_m3035_MethodInfo_var = il2cpp_codegen_method_info_from_index(505);
		Predicate_1__ctor_m3738_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484154);
		List_1_Find_m3739_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484155);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t644 * G_B2_0 = {0};
	List_1_t644 * G_B1_0 = {0};
	{
		List_1_t644 * L_0 = (__this->___m_Toggles_3);
		Predicate_1_t645 * L_1 = ((ToggleGroup_t643_StaticFields*)ToggleGroup_t643_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_4;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2 = { (void*)ToggleGroup_U3CAnyTogglesOnU3Em__6_m3035_MethodInfo_var };
		Predicate_1_t645 * L_3 = (Predicate_1_t645 *)il2cpp_codegen_object_new (Predicate_1_t645_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m3738(L_3, NULL, L_2, /*hidden argument*/Predicate_1__ctor_m3738_MethodInfo_var);
		((ToggleGroup_t643_StaticFields*)ToggleGroup_t643_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_4 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Predicate_1_t645 * L_4 = ((ToggleGroup_t643_StaticFields*)ToggleGroup_t643_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_4;
		NullCheck(G_B2_0);
		Toggle_t557 * L_5 = List_1_Find_m3739(G_B2_0, L_4, /*hidden argument*/List_1_Find_m3739_MethodInfo_var);
		bool L_6 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_5, (Object_t335 *)NULL, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::ActiveToggles()
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
#include "System_Core_System_Func_2_gen_2MethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
extern TypeInfo* ToggleGroup_t643_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t646_il2cpp_TypeInfo_var;
extern const MethodInfo* ToggleGroup_U3CActiveTogglesU3Em__7_m3036_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3740_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisToggle_t557_m3741_MethodInfo_var;
extern "C" Object_t* ToggleGroup_ActiveToggles_m3033 (ToggleGroup_t643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ToggleGroup_t643_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(509);
		Func_2_t646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		ToggleGroup_U3CActiveTogglesU3Em__7_m3036_MethodInfo_var = il2cpp_codegen_method_info_from_index(508);
		Func_2__ctor_m3740_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484157);
		Enumerable_Where_TisToggle_t557_m3741_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484158);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t644 * G_B2_0 = {0};
	List_1_t644 * G_B1_0 = {0};
	{
		List_1_t644 * L_0 = (__this->___m_Toggles_3);
		Func_2_t646 * L_1 = ((ToggleGroup_t643_StaticFields*)ToggleGroup_t643_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_5;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2 = { (void*)ToggleGroup_U3CActiveTogglesU3Em__7_m3036_MethodInfo_var };
		Func_2_t646 * L_3 = (Func_2_t646 *)il2cpp_codegen_object_new (Func_2_t646_il2cpp_TypeInfo_var);
		Func_2__ctor_m3740(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3740_MethodInfo_var);
		((ToggleGroup_t643_StaticFields*)ToggleGroup_t643_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_5 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Func_2_t646 * L_4 = ((ToggleGroup_t643_StaticFields*)ToggleGroup_t643_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_5;
		Object_t* L_5 = Enumerable_Where_TisToggle_t557_m3741(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_Where_TisToggle_t557_m3741_MethodInfo_var);
		return L_5;
	}
}
// System.Void UnityEngine.UI.ToggleGroup::SetAllTogglesOff()
extern "C" void ToggleGroup_SetAllTogglesOff_m3034 (ToggleGroup_t643 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		bool L_0 = (__this->___m_AllowSwitchOff_2);
		V_0 = L_0;
		__this->___m_AllowSwitchOff_2 = 1;
		V_1 = 0;
		goto IL_002b;
	}

IL_0015:
	{
		List_1_t644 * L_1 = (__this->___m_Toggles_3);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Toggle_t557 * L_3 = (Toggle_t557 *)VirtFuncInvoker1< Toggle_t557 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Item(System.Int32) */, L_1, L_2);
		NullCheck(L_3);
		Toggle_set_isOn_m3015(L_3, 0, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_5 = V_1;
		List_1_t644 * L_6 = (__this->___m_Toggles_3);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Count() */, L_6);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0015;
		}
	}
	{
		bool L_8 = V_0;
		__this->___m_AllowSwitchOff_2 = L_8;
		return;
	}
}
// System.Boolean UnityEngine.UI.ToggleGroup::<AnyTogglesOn>m__6(UnityEngine.UI.Toggle)
extern "C" bool ToggleGroup_U3CAnyTogglesOnU3Em__6_m3035 (Object_t * __this /* static, unused */, Toggle_t557 * ___x, const MethodInfo* method)
{
	{
		Toggle_t557 * L_0 = ___x;
		NullCheck(L_0);
		bool L_1 = Toggle_get_isOn_m3014(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.UI.ToggleGroup::<ActiveToggles>m__7(UnityEngine.UI.Toggle)
extern "C" bool ToggleGroup_U3CActiveTogglesU3Em__7_m3036 (Object_t * __this /* static, unused */, Toggle_t557 * ___x, const MethodInfo* method)
{
	{
		Toggle_t557 * L_0 = ___x;
		NullCheck(L_0);
		bool L_1 = Toggle_get_isOn_m3014(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.UI.ClipperRegistry
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry.h"
// UnityEngine.UI.ClipperRegistry
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistryMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_18.h"
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1.h"
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
// System.Void UnityEngine.UI.ClipperRegistry::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern TypeInfo* IndexedSet_1_t648_il2cpp_TypeInfo_var;
extern const MethodInfo* IndexedSet_1__ctor_m3742_MethodInfo_var;
extern "C" void ClipperRegistry__ctor_m3037 (ClipperRegistry_t647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexedSet_1_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		IndexedSet_1__ctor_m3742_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484159);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t765 * V_0 = {0};
	{
		IndexedSet_1_t648 * L_0 = (IndexedSet_1_t648 *)il2cpp_codegen_object_new (IndexedSet_1_t648_il2cpp_TypeInfo_var);
		IndexedSet_1__ctor_m3742(L_0, /*hidden argument*/IndexedSet_1__ctor_m3742_MethodInfo_var);
		__this->___m_Clippers_1 = L_0;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.ClipperRegistry UnityEngine.UI.ClipperRegistry::get_instance()
// UnityEngine.UI.ClipperRegistry
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistryMethodDeclarations.h"
extern TypeInfo* ClipperRegistry_t647_il2cpp_TypeInfo_var;
extern "C" ClipperRegistry_t647 * ClipperRegistry_get_instance_m3038 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClipperRegistry_t647_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(514);
		s_Il2CppMethodIntialized = true;
	}
	{
		ClipperRegistry_t647 * L_0 = ((ClipperRegistry_t647_StaticFields*)ClipperRegistry_t647_il2cpp_TypeInfo_var->static_fields)->___s_Instance_0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ClipperRegistry_t647 * L_1 = (ClipperRegistry_t647 *)il2cpp_codegen_object_new (ClipperRegistry_t647_il2cpp_TypeInfo_var);
		ClipperRegistry__ctor_m3037(L_1, /*hidden argument*/NULL);
		((ClipperRegistry_t647_StaticFields*)ClipperRegistry_t647_il2cpp_TypeInfo_var->static_fields)->___s_Instance_0 = L_1;
	}

IL_0014:
	{
		ClipperRegistry_t647 * L_2 = ((ClipperRegistry_t647_StaticFields*)ClipperRegistry_t647_il2cpp_TypeInfo_var->static_fields)->___s_Instance_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ClipperRegistry::Cull()
extern TypeInfo* IClipper_t717_il2cpp_TypeInfo_var;
extern "C" void ClipperRegistry_Cull_m3039 (ClipperRegistry_t647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IClipper_t717_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(512);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001c;
	}

IL_0007:
	{
		IndexedSet_1_t648 * L_0 = (__this->___m_Clippers_1);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(7 /* T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::get_Item(System.Int32) */, L_0, L_1);
		NullCheck(L_2);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.UI.IClipper::PerformClipping() */, IClipper_t717_il2cpp_TypeInfo_var, L_2);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001c:
	{
		int32_t L_4 = V_0;
		IndexedSet_1_t648 * L_5 = (__this->___m_Clippers_1);
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::get_Count() */, L_5);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.ClipperRegistry::Register(UnityEngine.UI.IClipper)
extern "C" void ClipperRegistry_Register_m3040 (Object_t * __this /* static, unused */, Object_t * ___c, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___c;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		ClipperRegistry_t647 * L_1 = ClipperRegistry_get_instance_m3038(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		IndexedSet_1_t648 * L_2 = (L_1->___m_Clippers_1);
		Object_t * L_3 = ___c;
		NullCheck(L_2);
		VirtActionInvoker1< Object_t * >::Invoke(12 /* System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Add(T) */, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.UI.ClipperRegistry::Unregister(UnityEngine.UI.IClipper)
extern "C" void ClipperRegistry_Unregister_m3041 (Object_t * __this /* static, unused */, Object_t * ___c, const MethodInfo* method)
{
	{
		ClipperRegistry_t647 * L_0 = ClipperRegistry_get_instance_m3038(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		IndexedSet_1_t648 * L_1 = (L_0->___m_Clippers_1);
		Object_t * L_2 = ___c;
		NullCheck(L_1);
		VirtFuncInvoker1< bool, Object_t * >::Invoke(16 /* System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Remove(T) */, L_1, L_2);
		return;
	}
}
// UnityEngine.UI.Clipping
#include "UnityEngine_UI_UnityEngine_UI_Clipping.h"
// UnityEngine.UI.Clipping
#include "UnityEngine_UI_UnityEngine_UI_ClippingMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_29.h"
// UnityEngine.UI.RectMask2D
#include "UnityEngine_UI_UnityEngine_UI_RectMask2D.h"
// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_29MethodDeclarations.h"
// UnityEngine.UI.RectMask2D
#include "UnityEngine_UI_UnityEngine_UI_RectMask2DMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// UnityEngine.Rect UnityEngine.UI.Clipping::FindCullAndClipWorldRect(System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>,System.Boolean&)
// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_29.h"
// UnityEngine.UI.RectMask2D
#include "UnityEngine_UI_UnityEngine_UI_RectMask2DMethodDeclarations.h"
// UnityEngine.UI.Clipping
#include "UnityEngine_UI_UnityEngine_UI_ClippingMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
extern TypeInfo* Rect_t267_il2cpp_TypeInfo_var;
extern "C" Rect_t267  Clipping_FindCullAndClipWorldRect_m3042 (Object_t * __this /* static, unused */, List_1_t616 * ___rectMaskParents, bool* ___validRect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Rect_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(251);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t267  V_0 = {0};
	int32_t V_1 = 0;
	bool V_2 = false;
	Vector3_t36  V_3 = {0};
	Vector3_t36  V_4 = {0};
	Rect_t267  V_5 = {0};
	Rect_t267  V_6 = {0};
	int32_t G_B8_0 = 0;
	{
		List_1_t616 * L_0 = ___rectMaskParents;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		bool* L_2 = ___validRect;
		*((int8_t*)(L_2)) = (int8_t)0;
		Initobj (Rect_t267_il2cpp_TypeInfo_var, (&V_5));
		Rect_t267  L_3 = V_5;
		return L_3;
	}

IL_0019:
	{
		List_1_t616 * L_4 = ___rectMaskParents;
		NullCheck(L_4);
		RectMask2D_t609 * L_5 = (RectMask2D_t609 *)VirtFuncInvoker1< RectMask2D_t609 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::get_Item(System.Int32) */, L_4, 0);
		NullCheck(L_5);
		Rect_t267  L_6 = RectMask2D_get_canvasRect_m2705(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		V_1 = 0;
		goto IL_0044;
	}

IL_002d:
	{
		Rect_t267  L_7 = V_0;
		List_1_t616 * L_8 = ___rectMaskParents;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		RectMask2D_t609 * L_10 = (RectMask2D_t609 *)VirtFuncInvoker1< RectMask2D_t609 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::get_Item(System.Int32) */, L_8, L_9);
		NullCheck(L_10);
		Rect_t267  L_11 = RectMask2D_get_canvasRect_m2705(L_10, /*hidden argument*/NULL);
		Rect_t267  L_12 = Clipping_RectIntersect_m3043(NULL /*static, unused*/, L_7, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0044:
	{
		int32_t L_14 = V_1;
		List_1_t616 * L_15 = ___rectMaskParents;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>::get_Count() */, L_15);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_002d;
		}
	}
	{
		float L_17 = Rect_get_width_m1762((&V_0), /*hidden argument*/NULL);
		if ((((float)L_17) <= ((float)(0.0f))))
		{
			goto IL_0074;
		}
	}
	{
		float L_18 = Rect_get_height_m1764((&V_0), /*hidden argument*/NULL);
		G_B8_0 = ((((int32_t)((!(((float)L_18) <= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0075;
	}

IL_0074:
	{
		G_B8_0 = 1;
	}

IL_0075:
	{
		V_2 = G_B8_0;
		bool L_19 = V_2;
		if (!L_19)
		{
			goto IL_008a;
		}
	}
	{
		bool* L_20 = ___validRect;
		*((int8_t*)(L_20)) = (int8_t)0;
		Initobj (Rect_t267_il2cpp_TypeInfo_var, (&V_6));
		Rect_t267  L_21 = V_6;
		return L_21;
	}

IL_008a:
	{
		float L_22 = Rect_get_x_m1758((&V_0), /*hidden argument*/NULL);
		float L_23 = Rect_get_y_m1760((&V_0), /*hidden argument*/NULL);
		Vector3__ctor_m1500((&V_3), L_22, L_23, (0.0f), /*hidden argument*/NULL);
		float L_24 = Rect_get_x_m1758((&V_0), /*hidden argument*/NULL);
		float L_25 = Rect_get_width_m1762((&V_0), /*hidden argument*/NULL);
		float L_26 = Rect_get_y_m1760((&V_0), /*hidden argument*/NULL);
		float L_27 = Rect_get_height_m1764((&V_0), /*hidden argument*/NULL);
		Vector3__ctor_m1500((&V_4), ((float)((float)L_24+(float)L_25)), ((float)((float)L_26+(float)L_27)), (0.0f), /*hidden argument*/NULL);
		bool* L_28 = ___validRect;
		*((int8_t*)(L_28)) = (int8_t)1;
		float L_29 = ((&V_3)->___x_1);
		float L_30 = ((&V_3)->___y_2);
		float L_31 = ((&V_4)->___x_1);
		float L_32 = ((&V_3)->___x_1);
		float L_33 = ((&V_4)->___y_2);
		float L_34 = ((&V_3)->___y_2);
		Rect_t267  L_35 = {0};
		Rect__ctor_m1775(&L_35, L_29, L_30, ((float)((float)L_31-(float)L_32)), ((float)((float)L_33-(float)L_34)), /*hidden argument*/NULL);
		return L_35;
	}
}
// UnityEngine.Rect UnityEngine.UI.Clipping::RectIntersect(UnityEngine.Rect,UnityEngine.Rect)
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" Rect_t267  Clipping_RectIntersect_m3043 (Object_t * __this /* static, unused */, Rect_t267  ___a, Rect_t267  ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = Rect_get_x_m1758((&___a), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m1758((&___b), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m3711(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m1758((&___a), /*hidden argument*/NULL);
		float L_4 = Rect_get_width_m1762((&___a), /*hidden argument*/NULL);
		float L_5 = Rect_get_x_m1758((&___b), /*hidden argument*/NULL);
		float L_6 = Rect_get_width_m1762((&___b), /*hidden argument*/NULL);
		float L_7 = Mathf_Min_m1767(NULL /*static, unused*/, ((float)((float)L_3+(float)L_4)), ((float)((float)L_5+(float)L_6)), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_y_m1760((&___a), /*hidden argument*/NULL);
		float L_9 = Rect_get_y_m1760((&___b), /*hidden argument*/NULL);
		float L_10 = Mathf_Max_m3711(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = Rect_get_y_m1760((&___a), /*hidden argument*/NULL);
		float L_12 = Rect_get_height_m1764((&___a), /*hidden argument*/NULL);
		float L_13 = Rect_get_y_m1760((&___b), /*hidden argument*/NULL);
		float L_14 = Rect_get_height_m1764((&___b), /*hidden argument*/NULL);
		float L_15 = Mathf_Min_m1767(NULL /*static, unused*/, ((float)((float)L_11+(float)L_12)), ((float)((float)L_13+(float)L_14)), /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = V_1;
		float L_17 = V_0;
		if ((!(((float)L_16) >= ((float)L_17))))
		{
			goto IL_008c;
		}
	}
	{
		float L_18 = V_3;
		float L_19 = V_2;
		if ((!(((float)L_18) >= ((float)L_19))))
		{
			goto IL_008c;
		}
	}
	{
		float L_20 = V_0;
		float L_21 = V_2;
		float L_22 = V_1;
		float L_23 = V_0;
		float L_24 = V_3;
		float L_25 = V_2;
		Rect_t267  L_26 = {0};
		Rect__ctor_m1775(&L_26, L_20, L_21, ((float)((float)L_22-(float)L_23)), ((float)((float)L_24-(float)L_25)), /*hidden argument*/NULL);
		return L_26;
	}

IL_008c:
	{
		Rect_t267  L_27 = {0};
		Rect__ctor_m1775(&L_27, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_27;
	}
}
// UnityEngine.UI.RectangularVertexClipper
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexClipper.h"
// UnityEngine.UI.RectangularVertexClipper
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexClipperMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
struct Transform_t35;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m1326_gshared (Component_t365 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m1326(__this, method) (( Object_t * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
#define Component_GetComponent_TisTransform_t35_m3743(__this, method) (( Transform_t35 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void UnityEngine.UI.RectangularVertexClipper::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern TypeInfo* Vector3U5BU5D_t254_il2cpp_TypeInfo_var;
extern "C" void RectangularVertexClipper__ctor_m3044 (RectangularVertexClipper_t614 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(91);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_WorldCorners_0 = ((Vector3U5BU5D_t254*)SZArrayNew(Vector3U5BU5D_t254_il2cpp_TypeInfo_var, 4));
		__this->___m_CanvasCorners_1 = ((Vector3U5BU5D_t254*)SZArrayNew(Vector3U5BU5D_t254_il2cpp_TypeInfo_var, 4));
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect UnityEngine.UI.RectangularVertexClipper::GetCanvasRect(UnityEngine.RectTransform,UnityEngine.Canvas)
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisTransform_t35_m3743_MethodInfo_var;
extern "C" Rect_t267  RectangularVertexClipper_GetCanvasRect_m3045 (RectangularVertexClipper_t614 * __this, RectTransform_t556 * ___t, Canvas_t574 * ___c, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisTransform_t35_m3743_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484160);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t35 * V_0 = {0};
	int32_t V_1 = 0;
	{
		RectTransform_t556 * L_0 = ___t;
		Vector3U5BU5D_t254* L_1 = (__this->___m_WorldCorners_0);
		NullCheck(L_0);
		RectTransform_GetWorldCorners_m3469(L_0, L_1, /*hidden argument*/NULL);
		Canvas_t574 * L_2 = ___c;
		NullCheck(L_2);
		Transform_t35 * L_3 = Component_GetComponent_TisTransform_t35_m3743(L_2, /*hidden argument*/Component_GetComponent_TisTransform_t35_m3743_MethodInfo_var);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0046;
	}

IL_001a:
	{
		Vector3U5BU5D_t254* L_4 = (__this->___m_CanvasCorners_1);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Transform_t35 * L_6 = V_0;
		Vector3U5BU5D_t254* L_7 = (__this->___m_WorldCorners_0);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		NullCheck(L_6);
		Vector3_t36  L_9 = Transform_InverseTransformPoint_m3470(L_6, (*(Vector3_t36 *)((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_7, L_8, sizeof(Vector3_t36 )))), /*hidden argument*/NULL);
		*((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_4, L_5, sizeof(Vector3_t36 ))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_001a;
		}
	}
	{
		Vector3U5BU5D_t254* L_12 = (__this->___m_CanvasCorners_1);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		float L_13 = (((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_12, 0, sizeof(Vector3_t36 )))->___x_1);
		Vector3U5BU5D_t254* L_14 = (__this->___m_CanvasCorners_1);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		float L_15 = (((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_14, 0, sizeof(Vector3_t36 )))->___y_2);
		Vector3U5BU5D_t254* L_16 = (__this->___m_CanvasCorners_1);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		float L_17 = (((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_16, 2, sizeof(Vector3_t36 )))->___x_1);
		Vector3U5BU5D_t254* L_18 = (__this->___m_CanvasCorners_1);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		float L_19 = (((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_18, 0, sizeof(Vector3_t36 )))->___x_1);
		Vector3U5BU5D_t254* L_20 = (__this->___m_CanvasCorners_1);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		float L_21 = (((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_20, 2, sizeof(Vector3_t36 )))->___y_2);
		Vector3U5BU5D_t254* L_22 = (__this->___m_CanvasCorners_1);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		float L_23 = (((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_22, 0, sizeof(Vector3_t36 )))->___y_2);
		Rect_t267  L_24 = {0};
		Rect__ctor_m1775(&L_24, L_13, L_15, ((float)((float)L_17-(float)L_19)), ((float)((float)L_21-(float)L_23)), /*hidden argument*/NULL);
		return L_24;
	}
}
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectModeMethodDeclarations.h"
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter.h"
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitterMethodDeclarations.h"
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker.h"
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"
// UnityEngine.RectTransform/Axis
#include "UnityEngine_UnityEngine_RectTransform_Axis.h"
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTrackerMethodDeclarations.h"
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility.h"
// Declaration System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<UnityEngine.UI.AspectRatioFitter/AspectMode>(!!0&,!!0)
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<UnityEngine.UI.AspectRatioFitter/AspectMode>(!!0&,!!0)
extern "C" bool SetPropertyUtility_SetStruct_TisAspectMode_t650_m3744_gshared (Object_t * __this /* static, unused */, int32_t* p0, int32_t p1, const MethodInfo* method);
#define SetPropertyUtility_SetStruct_TisAspectMode_t650_m3744(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, int32_t*, int32_t, const MethodInfo*))SetPropertyUtility_SetStruct_TisAspectMode_t650_m3744_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Single>(!!0&,!!0)
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Single>(!!0&,!!0)
extern "C" bool SetPropertyUtility_SetStruct_TisSingle_t388_m3555_gshared (Object_t * __this /* static, unused */, float* p0, float p1, const MethodInfo* method);
#define SetPropertyUtility_SetStruct_TisSingle_t388_m3555(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, float*, float, const MethodInfo*))SetPropertyUtility_SetStruct_TisSingle_t388_m3555_gshared)(__this /* static, unused */, p0, p1, method)
struct RectTransform_t556;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t556_m3505(__this, method) (( RectTransform_t556 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void UnityEngine.UI.AspectRatioFitter::.ctor()
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
extern "C" void AspectRatioFitter__ctor_m3046 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	{
		__this->___m_AspectRatio_3 = (1.0f);
		UIBehaviour__ctor_m2006(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::get_aspectMode()
extern "C" int32_t AspectRatioFitter_get_aspectMode_m3047 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_AspectMode_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectMode(UnityEngine.UI.AspectRatioFitter/AspectMode)
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitterMethodDeclarations.h"
extern const MethodInfo* SetPropertyUtility_SetStruct_TisAspectMode_t650_m3744_MethodInfo_var;
extern "C" void AspectRatioFitter_set_aspectMode_m3048 (AspectRatioFitter_t651 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisAspectMode_t650_m3744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484161);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_AspectMode_2);
		int32_t L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisAspectMode_t650_m3744(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisAspectMode_t650_m3744_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		AspectRatioFitter_SetDirty_m3060(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.AspectRatioFitter::get_aspectRatio()
extern "C" float AspectRatioFitter_get_aspectRatio_m3049 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_AspectRatio_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectRatio(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var;
extern "C" void AspectRatioFitter_set_aspectRatio_m3050 (AspectRatioFitter_t651 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_AspectRatio_3);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t388_m3555(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		AspectRatioFitter_SetDirty_m3060(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::get_rectTransform()
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisRectTransform_t556_m3505_MethodInfo_var;
extern "C" RectTransform_t556 * AspectRatioFitter_get_rectTransform_m3051 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRectTransform_t556_m3505_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484087);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t556 * L_0 = (__this->___m_Rect_4);
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t556 * L_2 = Component_GetComponent_TisRectTransform_t556_m3505(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t556_m3505_MethodInfo_var);
		__this->___m_Rect_4 = L_2;
	}

IL_001d:
	{
		RectTransform_t556 * L_3 = (__this->___m_Rect_4);
		return L_3;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::OnEnable()
extern "C" void AspectRatioFitter_OnEnable_m3052 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m2008(__this, /*hidden argument*/NULL);
		AspectRatioFitter_SetDirty_m3060(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::OnDisable()
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTrackerMethodDeclarations.h"
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern "C" void AspectRatioFitter_OnDisable_m3053 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		s_Il2CppMethodIntialized = true;
	}
	{
		DrivenRectTransformTracker_t622 * L_0 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m3674(L_0, /*hidden argument*/NULL);
		RectTransform_t556 * L_1 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m3199(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m2010(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::OnRectTransformDimensionsChange()
extern "C" void AspectRatioFitter_OnRectTransformDimensionsChange_m3054 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	{
		AspectRatioFitter_UpdateRect_m3055(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::UpdateRect()
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
extern "C" void AspectRatioFitter_UpdateRect_m3055 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	Vector2_t2  V_0 = {0};
	Vector2_t2  V_1 = {0};
	int32_t V_2 = {0};
	Rect_t267  V_3 = {0};
	Rect_t267  V_4 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		DrivenRectTransformTracker_t622 * L_1 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m3674(L_1, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_AspectMode_2);
		V_2 = L_2;
		int32_t L_3 = V_2;
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 0)
		{
			goto IL_007d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 1)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 2)
		{
			goto IL_00c0;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 3)
		{
			goto IL_00c0;
		}
	}
	{
		goto IL_0188;
	}

IL_003b:
	{
		DrivenRectTransformTracker_t622 * L_4 = &(__this->___m_Tracker_5);
		RectTransform_t556 * L_5 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		DrivenRectTransformTracker_Add_m3675(L_4, __this, L_5, ((int32_t)4096), /*hidden argument*/NULL);
		RectTransform_t556 * L_6 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		RectTransform_t556 * L_7 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Rect_t267  L_8 = RectTransform_get_rect_m3463(L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = Rect_get_height_m1764((&V_3), /*hidden argument*/NULL);
		float L_10 = (__this->___m_AspectRatio_3);
		NullCheck(L_6);
		RectTransform_SetSizeWithCurrentAnchors_m3745(L_6, 0, ((float)((float)L_9*(float)L_10)), /*hidden argument*/NULL);
		goto IL_0188;
	}

IL_007d:
	{
		DrivenRectTransformTracker_t622 * L_11 = &(__this->___m_Tracker_5);
		RectTransform_t556 * L_12 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		DrivenRectTransformTracker_Add_m3675(L_11, __this, L_12, ((int32_t)8192), /*hidden argument*/NULL);
		RectTransform_t556 * L_13 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		RectTransform_t556 * L_14 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Rect_t267  L_15 = RectTransform_get_rect_m3463(L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = Rect_get_width_m1762((&V_4), /*hidden argument*/NULL);
		float L_17 = (__this->___m_AspectRatio_3);
		NullCheck(L_13);
		RectTransform_SetSizeWithCurrentAnchors_m3745(L_13, 1, ((float)((float)L_16/(float)L_17)), /*hidden argument*/NULL);
		goto IL_0188;
	}

IL_00c0:
	{
		DrivenRectTransformTracker_t622 * L_18 = &(__this->___m_Tracker_5);
		RectTransform_t556 * L_19 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		DrivenRectTransformTracker_Add_m3675(L_18, __this, L_19, ((int32_t)16134), /*hidden argument*/NULL);
		RectTransform_t556 * L_20 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		Vector2_t2  L_21 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		RectTransform_set_anchorMin_m3418(L_20, L_21, /*hidden argument*/NULL);
		RectTransform_t556 * L_22 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		Vector2_t2  L_23 = Vector2_get_one_m3419(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchorMax_m3420(L_22, L_23, /*hidden argument*/NULL);
		RectTransform_t556 * L_24 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		Vector2_t2  L_25 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		RectTransform_set_anchoredPosition_m3421(L_24, L_25, /*hidden argument*/NULL);
		Vector2_t2  L_26 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_26;
		Vector2_t2  L_27 = AspectRatioFitter_GetParentSize_m3057(__this, /*hidden argument*/NULL);
		V_1 = L_27;
		float L_28 = ((&V_1)->___y_2);
		float L_29 = AspectRatioFitter_get_aspectRatio_m3049(__this, /*hidden argument*/NULL);
		float L_30 = ((&V_1)->___x_1);
		int32_t L_31 = (__this->___m_AspectMode_2);
		if (!((int32_t)((int32_t)((((float)((float)((float)L_28*(float)L_29))) < ((float)L_30))? 1 : 0)^(int32_t)((((int32_t)L_31) == ((int32_t)3))? 1 : 0))))
		{
			goto IL_015b;
		}
	}
	{
		float L_32 = ((&V_1)->___x_1);
		float L_33 = AspectRatioFitter_get_aspectRatio_m3049(__this, /*hidden argument*/NULL);
		float L_34 = AspectRatioFitter_GetSizeDeltaToProduceSize_m3056(__this, ((float)((float)L_32/(float)L_33)), 1, /*hidden argument*/NULL);
		(&V_0)->___y_2 = L_34;
		goto IL_0177;
	}

IL_015b:
	{
		float L_35 = ((&V_1)->___y_2);
		float L_36 = AspectRatioFitter_get_aspectRatio_m3049(__this, /*hidden argument*/NULL);
		float L_37 = AspectRatioFitter_GetSizeDeltaToProduceSize_m3056(__this, ((float)((float)L_35*(float)L_36)), 0, /*hidden argument*/NULL);
		(&V_0)->___x_1 = L_37;
	}

IL_0177:
	{
		RectTransform_t556 * L_38 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		Vector2_t2  L_39 = V_0;
		NullCheck(L_38);
		RectTransform_set_sizeDelta_m3411(L_38, L_39, /*hidden argument*/NULL);
		goto IL_0188;
	}

IL_0188:
	{
		return;
	}
}
// System.Single UnityEngine.UI.AspectRatioFitter::GetSizeDeltaToProduceSize(System.Single,System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" float AspectRatioFitter_GetSizeDeltaToProduceSize_m3056 (AspectRatioFitter_t651 * __this, float ___size, int32_t ___axis, const MethodInfo* method)
{
	Vector2_t2  V_0 = {0};
	Vector2_t2  V_1 = {0};
	Vector2_t2  V_2 = {0};
	{
		float L_0 = ___size;
		Vector2_t2  L_1 = AspectRatioFitter_GetParentSize_m3057(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___axis;
		float L_3 = Vector2_get_Item_m3571((&V_0), L_2, /*hidden argument*/NULL);
		RectTransform_t556 * L_4 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector2_t2  L_5 = RectTransform_get_anchorMax_m3474(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = ___axis;
		float L_7 = Vector2_get_Item_m3571((&V_1), L_6, /*hidden argument*/NULL);
		RectTransform_t556 * L_8 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector2_t2  L_9 = RectTransform_get_anchorMin_m3473(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		int32_t L_10 = ___axis;
		float L_11 = Vector2_get_Item_m3571((&V_2), L_10, /*hidden argument*/NULL);
		return ((float)((float)L_0-(float)((float)((float)L_3*(float)((float)((float)L_7-(float)L_11))))));
	}
}
// UnityEngine.Vector2 UnityEngine.UI.AspectRatioFitter::GetParentSize()
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern TypeInfo* RectTransform_t556_il2cpp_TypeInfo_var;
extern "C" Vector2_t2  AspectRatioFitter_GetParentSize_m3057 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t556 * V_0 = {0};
	Rect_t267  V_1 = {0};
	{
		RectTransform_t556 * L_0 = AspectRatioFitter_get_rectTransform_m3051(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t35 * L_1 = Transform_get_parent_m1342(L_0, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t556 *)IsInstSealed(L_1, RectTransform_t556_il2cpp_TypeInfo_var));
		RectTransform_t556 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m1320(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0022;
		}
	}
	{
		Vector2_t2  L_4 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_4;
	}

IL_0022:
	{
		RectTransform_t556 * L_5 = V_0;
		NullCheck(L_5);
		Rect_t267  L_6 = RectTransform_get_rect_m3463(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector2_t2  L_7 = Rect_get_size_m3466((&V_1), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutHorizontal()
extern "C" void AspectRatioFitter_SetLayoutHorizontal_m3058 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutVertical()
extern "C" void AspectRatioFitter_SetLayoutVertical_m3059 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.AspectRatioFitter::SetDirty()
extern "C" void AspectRatioFitter_SetDirty_m3060 (AspectRatioFitter_t651 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		AspectRatioFitter_UpdateRect_m3055(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleModeMethodDeclarations.h"
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchModeMethodDeclarations.h"
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_UnitMethodDeclarations.h"
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler.h"
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScalerMethodDeclarations.h"
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
struct Canvas_t574;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Canvas>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Canvas>()
#define Component_GetComponent_TisCanvas_t574_m3535(__this, method) (( Canvas_t574 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void UnityEngine.UI.CanvasScaler::.ctor()
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
extern "C" void CanvasScaler__ctor_m3061 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		__this->___m_ReferencePixelsPerUnit_4 = (100.0f);
		__this->___m_ScaleFactor_5 = (1.0f);
		Vector2_t2  L_0 = {0};
		Vector2__ctor_m1309(&L_0, (800.0f), (600.0f), /*hidden argument*/NULL);
		__this->___m_ReferenceResolution_6 = L_0;
		__this->___m_PhysicalUnit_9 = 3;
		__this->___m_FallbackScreenDPI_10 = (96.0f);
		__this->___m_DefaultSpriteDPI_11 = (96.0f);
		__this->___m_DynamicPixelsPerUnit_12 = (1.0f);
		__this->___m_PrevScaleFactor_14 = (1.0f);
		__this->___m_PrevReferencePixelsPerUnit_15 = (100.0f);
		UIBehaviour__ctor_m2006(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::get_uiScaleMode()
extern "C" int32_t CanvasScaler_get_uiScaleMode_m3062 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_UiScaleMode_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_uiScaleMode(UnityEngine.UI.CanvasScaler/ScaleMode)
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
extern "C" void CanvasScaler_set_uiScaleMode_m3063 (CanvasScaler_t655 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_UiScaleMode_3 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_referencePixelsPerUnit()
extern "C" float CanvasScaler_get_referencePixelsPerUnit_m3064 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_ReferencePixelsPerUnit_4);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_referencePixelsPerUnit(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void CanvasScaler_set_referencePixelsPerUnit_m3065 (CanvasScaler_t655 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_ReferencePixelsPerUnit_4 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_scaleFactor()
extern "C" float CanvasScaler_get_scaleFactor_m3066 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_ScaleFactor_5);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_scaleFactor(System.Single)
extern "C" void CanvasScaler_set_scaleFactor_m3067 (CanvasScaler_t655 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_ScaleFactor_5 = L_0;
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::get_referenceResolution()
extern "C" Vector2_t2  CanvasScaler_get_referenceResolution_m3068 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___m_ReferenceResolution_6);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_referenceResolution(UnityEngine.Vector2)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
extern "C" void CanvasScaler_set_referenceResolution_m3069 (CanvasScaler_t655 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = ___value;
		__this->___m_ReferenceResolution_6 = L_0;
		return;
	}
}
// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::get_screenMatchMode()
extern "C" int32_t CanvasScaler_get_screenMatchMode_m3070 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_ScreenMatchMode_7);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_screenMatchMode(UnityEngine.UI.CanvasScaler/ScreenMatchMode)
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
extern "C" void CanvasScaler_set_screenMatchMode_m3071 (CanvasScaler_t655 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_ScreenMatchMode_7 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_matchWidthOrHeight()
extern "C" float CanvasScaler_get_matchWidthOrHeight_m3072 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_MatchWidthOrHeight_8);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_matchWidthOrHeight(System.Single)
extern "C" void CanvasScaler_set_matchWidthOrHeight_m3073 (CanvasScaler_t655 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_MatchWidthOrHeight_8 = L_0;
		return;
	}
}
// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::get_physicalUnit()
extern "C" int32_t CanvasScaler_get_physicalUnit_m3074 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_PhysicalUnit_9);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_physicalUnit(UnityEngine.UI.CanvasScaler/Unit)
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
extern "C" void CanvasScaler_set_physicalUnit_m3075 (CanvasScaler_t655 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_PhysicalUnit_9 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_fallbackScreenDPI()
extern "C" float CanvasScaler_get_fallbackScreenDPI_m3076 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FallbackScreenDPI_10);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_fallbackScreenDPI(System.Single)
extern "C" void CanvasScaler_set_fallbackScreenDPI_m3077 (CanvasScaler_t655 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_FallbackScreenDPI_10 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_defaultSpriteDPI()
extern "C" float CanvasScaler_get_defaultSpriteDPI_m3078 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_DefaultSpriteDPI_11);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_defaultSpriteDPI(System.Single)
extern "C" void CanvasScaler_set_defaultSpriteDPI_m3079 (CanvasScaler_t655 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_DefaultSpriteDPI_11 = L_0;
		return;
	}
}
// System.Single UnityEngine.UI.CanvasScaler::get_dynamicPixelsPerUnit()
extern "C" float CanvasScaler_get_dynamicPixelsPerUnit_m3080 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_DynamicPixelsPerUnit_12);
		return L_0;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::set_dynamicPixelsPerUnit(System.Single)
extern "C" void CanvasScaler_set_dynamicPixelsPerUnit_m3081 (CanvasScaler_t655 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_DynamicPixelsPerUnit_12 = L_0;
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::OnEnable()
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisCanvas_t574_m3535_MethodInfo_var;
extern "C" void CanvasScaler_OnEnable_m3082 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCanvas_t574_m3535_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484096);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIBehaviour_OnEnable_m2008(__this, /*hidden argument*/NULL);
		Canvas_t574 * L_0 = Component_GetComponent_TisCanvas_t574_m3535(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t574_m3535_MethodInfo_var);
		__this->___m_Canvas_13 = L_0;
		VirtActionInvoker0::Invoke(17 /* System.Void UnityEngine.UI.CanvasScaler::Handle() */, __this);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::OnDisable()
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScalerMethodDeclarations.h"
extern "C" void CanvasScaler_OnDisable_m3083 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		CanvasScaler_SetScaleFactor_m3090(__this, (1.0f), /*hidden argument*/NULL);
		CanvasScaler_SetReferencePixelsPerUnit_m3091(__this, (100.0f), /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m2010(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::Update()
extern "C" void CanvasScaler_Update_m3084 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(17 /* System.Void UnityEngine.UI.CanvasScaler::Handle() */, __this);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::Handle()
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
extern "C" void CanvasScaler_Handle_m3085 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		Canvas_t574 * L_0 = (__this->___m_Canvas_13);
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		Canvas_t574 * L_2 = (__this->___m_Canvas_13);
		NullCheck(L_2);
		bool L_3 = Canvas_get_isRootCanvas_m3746(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0022;
		}
	}

IL_0021:
	{
		return;
	}

IL_0022:
	{
		Canvas_t574 * L_4 = (__this->___m_Canvas_13);
		NullCheck(L_4);
		int32_t L_5 = Canvas_get_renderMode_m3533(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_003a;
		}
	}
	{
		VirtActionInvoker0::Invoke(18 /* System.Void UnityEngine.UI.CanvasScaler::HandleWorldCanvas() */, __this);
		return;
	}

IL_003a:
	{
		int32_t L_6 = (__this->___m_UiScaleMode_3);
		V_0 = L_6;
		int32_t L_7 = V_0;
		if (L_7 == 0)
		{
			goto IL_0058;
		}
		if (L_7 == 1)
		{
			goto IL_0063;
		}
		if (L_7 == 2)
		{
			goto IL_006e;
		}
	}
	{
		goto IL_0079;
	}

IL_0058:
	{
		VirtActionInvoker0::Invoke(19 /* System.Void UnityEngine.UI.CanvasScaler::HandleConstantPixelSize() */, __this);
		goto IL_0079;
	}

IL_0063:
	{
		VirtActionInvoker0::Invoke(20 /* System.Void UnityEngine.UI.CanvasScaler::HandleScaleWithScreenSize() */, __this);
		goto IL_0079;
	}

IL_006e:
	{
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.CanvasScaler::HandleConstantPhysicalSize() */, __this);
		goto IL_0079;
	}

IL_0079:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::HandleWorldCanvas()
extern "C" void CanvasScaler_HandleWorldCanvas_m3086 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_DynamicPixelsPerUnit_12);
		CanvasScaler_SetScaleFactor_m3090(__this, L_0, /*hidden argument*/NULL);
		float L_1 = (__this->___m_ReferencePixelsPerUnit_4);
		CanvasScaler_SetReferencePixelsPerUnit_m3091(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPixelSize()
extern "C" void CanvasScaler_HandleConstantPixelSize_m3087 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_ScaleFactor_5);
		CanvasScaler_SetScaleFactor_m3090(__this, L_0, /*hidden argument*/NULL);
		float L_1 = (__this->___m_ReferencePixelsPerUnit_4);
		CanvasScaler_SetReferencePixelsPerUnit_m3091(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::HandleScaleWithScreenSize()
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" void CanvasScaler_HandleScaleWithScreenSize_m3088 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = {0};
	{
		int32_t L_0 = Screen_get_width_m1779(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1780(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2__ctor_m1309((&V_0), (((float)L_0)), (((float)L_1)), /*hidden argument*/NULL);
		V_1 = (0.0f);
		int32_t L_2 = (__this->___m_ScreenMatchMode_7);
		V_5 = L_2;
		int32_t L_3 = V_5;
		if (L_3 == 0)
		{
			goto IL_0039;
		}
		if (L_3 == 1)
		{
			goto IL_0096;
		}
		if (L_3 == 2)
		{
			goto IL_00c7;
		}
	}
	{
		goto IL_00f8;
	}

IL_0039:
	{
		float L_4 = ((&V_0)->___x_1);
		Vector2_t2 * L_5 = &(__this->___m_ReferenceResolution_6);
		float L_6 = (L_5->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Log_m3747(NULL /*static, unused*/, ((float)((float)L_4/(float)L_6)), (2.0f), /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = ((&V_0)->___y_2);
		Vector2_t2 * L_9 = &(__this->___m_ReferenceResolution_6);
		float L_10 = (L_9->___y_2);
		float L_11 = Mathf_Log_m3747(NULL /*static, unused*/, ((float)((float)L_8/(float)L_10)), (2.0f), /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = V_2;
		float L_13 = V_3;
		float L_14 = (__this->___m_MatchWidthOrHeight_8);
		float L_15 = Mathf_Lerp_m1395(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = V_4;
		float L_17 = powf((2.0f), L_16);
		V_1 = L_17;
		goto IL_00f8;
	}

IL_0096:
	{
		float L_18 = ((&V_0)->___x_1);
		Vector2_t2 * L_19 = &(__this->___m_ReferenceResolution_6);
		float L_20 = (L_19->___x_1);
		float L_21 = ((&V_0)->___y_2);
		Vector2_t2 * L_22 = &(__this->___m_ReferenceResolution_6);
		float L_23 = (L_22->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Min_m1767(NULL /*static, unused*/, ((float)((float)L_18/(float)L_20)), ((float)((float)L_21/(float)L_23)), /*hidden argument*/NULL);
		V_1 = L_24;
		goto IL_00f8;
	}

IL_00c7:
	{
		float L_25 = ((&V_0)->___x_1);
		Vector2_t2 * L_26 = &(__this->___m_ReferenceResolution_6);
		float L_27 = (L_26->___x_1);
		float L_28 = ((&V_0)->___y_2);
		Vector2_t2 * L_29 = &(__this->___m_ReferenceResolution_6);
		float L_30 = (L_29->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_31 = Mathf_Max_m3711(NULL /*static, unused*/, ((float)((float)L_25/(float)L_27)), ((float)((float)L_28/(float)L_30)), /*hidden argument*/NULL);
		V_1 = L_31;
		goto IL_00f8;
	}

IL_00f8:
	{
		float L_32 = V_1;
		CanvasScaler_SetScaleFactor_m3090(__this, L_32, /*hidden argument*/NULL);
		float L_33 = (__this->___m_ReferencePixelsPerUnit_4);
		CanvasScaler_SetReferencePixelsPerUnit_m3091(__this, L_33, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPhysicalSize()
extern "C" void CanvasScaler_HandleConstantPhysicalSize_m3089 (CanvasScaler_t655 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = {0};
	float G_B3_0 = 0.0f;
	{
		float L_0 = Screen_get_dpi_m3748(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_001c;
		}
	}
	{
		float L_2 = (__this->___m_FallbackScreenDPI_10);
		G_B3_0 = L_2;
		goto IL_001d;
	}

IL_001c:
	{
		float L_3 = V_0;
		G_B3_0 = L_3;
	}

IL_001d:
	{
		V_1 = G_B3_0;
		V_2 = (1.0f);
		int32_t L_4 = (__this->___m_PhysicalUnit_9);
		V_3 = L_4;
		int32_t L_5 = V_3;
		if (L_5 == 0)
		{
			goto IL_004a;
		}
		if (L_5 == 1)
		{
			goto IL_0055;
		}
		if (L_5 == 2)
		{
			goto IL_0060;
		}
		if (L_5 == 3)
		{
			goto IL_006b;
		}
		if (L_5 == 4)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_0081;
	}

IL_004a:
	{
		V_2 = (2.54f);
		goto IL_0081;
	}

IL_0055:
	{
		V_2 = (25.4f);
		goto IL_0081;
	}

IL_0060:
	{
		V_2 = (1.0f);
		goto IL_0081;
	}

IL_006b:
	{
		V_2 = (72.0f);
		goto IL_0081;
	}

IL_0076:
	{
		V_2 = (6.0f);
		goto IL_0081;
	}

IL_0081:
	{
		float L_6 = V_1;
		float L_7 = V_2;
		CanvasScaler_SetScaleFactor_m3090(__this, ((float)((float)L_6/(float)L_7)), /*hidden argument*/NULL);
		float L_8 = (__this->___m_ReferencePixelsPerUnit_4);
		float L_9 = V_2;
		float L_10 = (__this->___m_DefaultSpriteDPI_11);
		CanvasScaler_SetReferencePixelsPerUnit_m3091(__this, ((float)((float)((float)((float)L_8*(float)L_9))/(float)L_10)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::SetScaleFactor(System.Single)
extern "C" void CanvasScaler_SetScaleFactor_m3090 (CanvasScaler_t655 * __this, float ___scaleFactor, const MethodInfo* method)
{
	{
		float L_0 = ___scaleFactor;
		float L_1 = (__this->___m_PrevScaleFactor_14);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Canvas_t574 * L_2 = (__this->___m_Canvas_13);
		float L_3 = ___scaleFactor;
		NullCheck(L_2);
		Canvas_set_scaleFactor_m3749(L_2, L_3, /*hidden argument*/NULL);
		float L_4 = ___scaleFactor;
		__this->___m_PrevScaleFactor_14 = L_4;
		return;
	}
}
// System.Void UnityEngine.UI.CanvasScaler::SetReferencePixelsPerUnit(System.Single)
extern "C" void CanvasScaler_SetReferencePixelsPerUnit_m3091 (CanvasScaler_t655 * __this, float ___referencePixelsPerUnit, const MethodInfo* method)
{
	{
		float L_0 = ___referencePixelsPerUnit;
		float L_1 = (__this->___m_PrevReferencePixelsPerUnit_15);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Canvas_t574 * L_2 = (__this->___m_Canvas_13);
		float L_3 = ___referencePixelsPerUnit;
		NullCheck(L_2);
		Canvas_set_referencePixelsPerUnit_m3750(L_2, L_3, /*hidden argument*/NULL);
		float L_4 = ___referencePixelsPerUnit;
		__this->___m_PrevReferencePixelsPerUnit_15 = L_4;
		return;
	}
}
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitModeMethodDeclarations.h"
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter.h"
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitterMethodDeclarations.h"
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
// Declaration System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<UnityEngine.UI.ContentSizeFitter/FitMode>(!!0&,!!0)
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<UnityEngine.UI.ContentSizeFitter/FitMode>(!!0&,!!0)
extern "C" bool SetPropertyUtility_SetStruct_TisFitMode_t656_m3751_gshared (Object_t * __this /* static, unused */, int32_t* p0, int32_t p1, const MethodInfo* method);
#define SetPropertyUtility_SetStruct_TisFitMode_t656_m3751(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, int32_t*, int32_t, const MethodInfo*))SetPropertyUtility_SetStruct_TisFitMode_t656_m3751_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void UnityEngine.UI.ContentSizeFitter::.ctor()
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
extern "C" void ContentSizeFitter__ctor_m3092 (ContentSizeFitter_t657 * __this, const MethodInfo* method)
{
	{
		UIBehaviour__ctor_m2006(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_horizontalFit()
extern "C" int32_t ContentSizeFitter_get_horizontalFit_m3093 (ContentSizeFitter_t657 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_HorizontalFit_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::set_horizontalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitterMethodDeclarations.h"
extern const MethodInfo* SetPropertyUtility_SetStruct_TisFitMode_t656_m3751_MethodInfo_var;
extern "C" void ContentSizeFitter_set_horizontalFit_m3094 (ContentSizeFitter_t657 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisFitMode_t656_m3751_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484162);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_HorizontalFit_2);
		int32_t L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisFitMode_t656_m3751(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisFitMode_t656_m3751_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		ContentSizeFitter_SetDirty_m3104(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_verticalFit()
extern "C" int32_t ContentSizeFitter_get_verticalFit_m3095 (ContentSizeFitter_t657 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_VerticalFit_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::set_verticalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisFitMode_t656_m3751_MethodInfo_var;
extern "C" void ContentSizeFitter_set_verticalFit_m3096 (ContentSizeFitter_t657 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisFitMode_t656_m3751_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484162);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_VerticalFit_3);
		int32_t L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisFitMode_t656_m3751(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisFitMode_t656_m3751_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		ContentSizeFitter_SetDirty_m3104(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::get_rectTransform()
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisRectTransform_t556_m3505_MethodInfo_var;
extern "C" RectTransform_t556 * ContentSizeFitter_get_rectTransform_m3097 (ContentSizeFitter_t657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRectTransform_t556_m3505_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484087);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t556 * L_0 = (__this->___m_Rect_4);
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t556 * L_2 = Component_GetComponent_TisRectTransform_t556_m3505(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t556_m3505_MethodInfo_var);
		__this->___m_Rect_4 = L_2;
	}

IL_001d:
	{
		RectTransform_t556 * L_3 = (__this->___m_Rect_4);
		return L_3;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::OnEnable()
extern "C" void ContentSizeFitter_OnEnable_m3098 (ContentSizeFitter_t657 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m2008(__this, /*hidden argument*/NULL);
		ContentSizeFitter_SetDirty_m3104(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::OnDisable()
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTrackerMethodDeclarations.h"
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern "C" void ContentSizeFitter_OnDisable_m3099 (ContentSizeFitter_t657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		s_Il2CppMethodIntialized = true;
	}
	{
		DrivenRectTransformTracker_t622 * L_0 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m3674(L_0, /*hidden argument*/NULL);
		RectTransform_t556 * L_1 = ContentSizeFitter_get_rectTransform_m3097(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m3199(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m2010(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::OnRectTransformDimensionsChange()
extern "C" void ContentSizeFitter_OnRectTransformDimensionsChange_m3100 (ContentSizeFitter_t657 * __this, const MethodInfo* method)
{
	{
		ContentSizeFitter_SetDirty_m3104(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::HandleSelfFittingAlongAxis(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
extern "C" void ContentSizeFitter_HandleSelfFittingAlongAxis_m3101 (ContentSizeFitter_t657 * __this, int32_t ___axis, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B3_0 = {0};
	RectTransform_t556 * G_B7_0 = {0};
	ContentSizeFitter_t657 * G_B7_1 = {0};
	DrivenRectTransformTracker_t622 * G_B7_2 = {0};
	RectTransform_t556 * G_B6_0 = {0};
	ContentSizeFitter_t657 * G_B6_1 = {0};
	DrivenRectTransformTracker_t622 * G_B6_2 = {0};
	int32_t G_B8_0 = 0;
	RectTransform_t556 * G_B8_1 = {0};
	ContentSizeFitter_t657 * G_B8_2 = {0};
	DrivenRectTransformTracker_t622 * G_B8_3 = {0};
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ContentSizeFitter_get_horizontalFit_m3093(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0011:
	{
		int32_t L_2 = ContentSizeFitter_get_verticalFit_m3095(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		int32_t L_3 = V_0;
		if (L_3)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		DrivenRectTransformTracker_t622 * L_4 = &(__this->___m_Tracker_5);
		RectTransform_t556 * L_5 = ContentSizeFitter_get_rectTransform_m3097(__this, /*hidden argument*/NULL);
		int32_t L_6 = ___axis;
		G_B6_0 = L_5;
		G_B6_1 = __this;
		G_B6_2 = L_4;
		if (L_6)
		{
			G_B7_0 = L_5;
			G_B7_1 = __this;
			G_B7_2 = L_4;
			goto IL_003c;
		}
	}
	{
		G_B8_0 = ((int32_t)4096);
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		goto IL_0041;
	}

IL_003c:
	{
		G_B8_0 = ((int32_t)8192);
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
	}

IL_0041:
	{
		DrivenRectTransformTracker_Add_m3675(G_B8_3, G_B8_2, G_B8_1, G_B8_0, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_006a;
		}
	}
	{
		RectTransform_t556 * L_8 = ContentSizeFitter_get_rectTransform_m3097(__this, /*hidden argument*/NULL);
		int32_t L_9 = ___axis;
		RectTransform_t556 * L_10 = (__this->___m_Rect_4);
		int32_t L_11 = ___axis;
		float L_12 = LayoutUtility_GetMinSize_m3211(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		RectTransform_SetSizeWithCurrentAnchors_m3745(L_8, L_9, L_12, /*hidden argument*/NULL);
		goto IL_0082;
	}

IL_006a:
	{
		RectTransform_t556 * L_13 = ContentSizeFitter_get_rectTransform_m3097(__this, /*hidden argument*/NULL);
		int32_t L_14 = ___axis;
		RectTransform_t556 * L_15 = (__this->___m_Rect_4);
		int32_t L_16 = ___axis;
		float L_17 = LayoutUtility_GetPreferredSize_m3212(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_SetSizeWithCurrentAnchors_m3745(L_13, L_14, L_17, /*hidden argument*/NULL);
	}

IL_0082:
	{
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutHorizontal()
extern "C" void ContentSizeFitter_SetLayoutHorizontal_m3102 (ContentSizeFitter_t657 * __this, const MethodInfo* method)
{
	{
		DrivenRectTransformTracker_t622 * L_0 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m3674(L_0, /*hidden argument*/NULL);
		ContentSizeFitter_HandleSelfFittingAlongAxis_m3101(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutVertical()
extern "C" void ContentSizeFitter_SetLayoutVertical_m3103 (ContentSizeFitter_t657 * __this, const MethodInfo* method)
{
	{
		ContentSizeFitter_HandleSelfFittingAlongAxis_m3101(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.ContentSizeFitter::SetDirty()
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern "C" void ContentSizeFitter_SetDirty_m3104 (ContentSizeFitter_t657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		RectTransform_t556 * L_1 = ContentSizeFitter_get_rectTransform_m3097(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m3199(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_CornerMethodDeclarations.h"
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_AxisMethodDeclarations.h"
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_ConstraintMethodDeclarations.h"
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup.h"
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroupMethodDeclarations.h"
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup.h"
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
#include "mscorlib_System_Collections_Generic_List_1_gen_35.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
#include "mscorlib_System_Collections_Generic_List_1_gen_35MethodDeclarations.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Corner>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Corner>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisCorner_t658_m3752_gshared (LayoutGroup_t662 * __this, int32_t* p0, int32_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisCorner_t658_m3752(__this, p0, p1, method) (( void (*) (LayoutGroup_t662 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisCorner_t658_m3752_gshared)(__this, p0, p1, method)
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Axis>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Axis>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisAxis_t659_m3753_gshared (LayoutGroup_t662 * __this, int32_t* p0, int32_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisAxis_t659_m3753(__this, p0, p1, method) (( void (*) (LayoutGroup_t662 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisAxis_t659_m3753_gshared)(__this, p0, p1, method)
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.Vector2>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.Vector2>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisVector2_t2_m3754_gshared (LayoutGroup_t662 * __this, Vector2_t2 * p0, Vector2_t2  p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisVector2_t2_m3754(__this, p0, p1, method) (( void (*) (LayoutGroup_t662 *, Vector2_t2 *, Vector2_t2 , const MethodInfo*))LayoutGroup_SetProperty_TisVector2_t2_m3754_gshared)(__this, p0, p1, method)
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Constraint>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.UI.GridLayoutGroup/Constraint>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisConstraint_t660_m3755_gshared (LayoutGroup_t662 * __this, int32_t* p0, int32_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisConstraint_t660_m3755(__this, p0, p1, method) (( void (*) (LayoutGroup_t662 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisConstraint_t660_m3755_gshared)(__this, p0, p1, method)
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Int32>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Int32>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisInt32_t372_m3756_gshared (LayoutGroup_t662 * __this, int32_t* p0, int32_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisInt32_t372_m3756(__this, p0, p1, method) (( void (*) (LayoutGroup_t662 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisInt32_t372_m3756_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.UI.GridLayoutGroup::.ctor()
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
extern "C" void GridLayoutGroup__ctor_m3105 (GridLayoutGroup_t661 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = {0};
		Vector2__ctor_m1309(&L_0, (100.0f), (100.0f), /*hidden argument*/NULL);
		__this->___m_CellSize_12 = L_0;
		Vector2_t2  L_1 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Spacing_13 = L_1;
		__this->___m_ConstraintCount_15 = 2;
		LayoutGroup__ctor_m3161(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::get_startCorner()
extern "C" int32_t GridLayoutGroup_get_startCorner_m3106 (GridLayoutGroup_t661 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_StartCorner_10);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_startCorner(UnityEngine.UI.GridLayoutGroup/Corner)
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
extern const MethodInfo* LayoutGroup_SetProperty_TisCorner_t658_m3752_MethodInfo_var;
extern "C" void GridLayoutGroup_set_startCorner_m3107 (GridLayoutGroup_t661 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisCorner_t658_m3752_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484163);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_StartCorner_10);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisCorner_t658_m3752(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisCorner_t658_m3752_MethodInfo_var);
		return;
	}
}
// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::get_startAxis()
extern "C" int32_t GridLayoutGroup_get_startAxis_m3108 (GridLayoutGroup_t661 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_StartAxis_11);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_startAxis(UnityEngine.UI.GridLayoutGroup/Axis)
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
extern const MethodInfo* LayoutGroup_SetProperty_TisAxis_t659_m3753_MethodInfo_var;
extern "C" void GridLayoutGroup_set_startAxis_m3109 (GridLayoutGroup_t661 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisAxis_t659_m3753_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484164);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_StartAxis_11);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisAxis_t659_m3753(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisAxis_t659_m3753_MethodInfo_var);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_cellSize()
extern "C" Vector2_t2  GridLayoutGroup_get_cellSize_m3110 (GridLayoutGroup_t661 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___m_CellSize_12);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_cellSize(UnityEngine.Vector2)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
extern const MethodInfo* LayoutGroup_SetProperty_TisVector2_t2_m3754_MethodInfo_var;
extern "C" void GridLayoutGroup_set_cellSize_m3111 (GridLayoutGroup_t661 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisVector2_t2_m3754_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484165);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t2 * L_0 = &(__this->___m_CellSize_12);
		Vector2_t2  L_1 = ___value;
		LayoutGroup_SetProperty_TisVector2_t2_m3754(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisVector2_t2_m3754_MethodInfo_var);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_spacing()
extern "C" Vector2_t2  GridLayoutGroup_get_spacing_m3112 (GridLayoutGroup_t661 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___m_Spacing_13);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_spacing(UnityEngine.Vector2)
extern const MethodInfo* LayoutGroup_SetProperty_TisVector2_t2_m3754_MethodInfo_var;
extern "C" void GridLayoutGroup_set_spacing_m3113 (GridLayoutGroup_t661 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisVector2_t2_m3754_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484165);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t2 * L_0 = &(__this->___m_Spacing_13);
		Vector2_t2  L_1 = ___value;
		LayoutGroup_SetProperty_TisVector2_t2_m3754(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisVector2_t2_m3754_MethodInfo_var);
		return;
	}
}
// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::get_constraint()
extern "C" int32_t GridLayoutGroup_get_constraint_m3114 (GridLayoutGroup_t661 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Constraint_14);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraint(UnityEngine.UI.GridLayoutGroup/Constraint)
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
extern const MethodInfo* LayoutGroup_SetProperty_TisConstraint_t660_m3755_MethodInfo_var;
extern "C" void GridLayoutGroup_set_constraint_m3115 (GridLayoutGroup_t661 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisConstraint_t660_m3755_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484166);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_Constraint_14);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisConstraint_t660_m3755(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisConstraint_t660_m3755_MethodInfo_var);
		return;
	}
}
// System.Int32 UnityEngine.UI.GridLayoutGroup::get_constraintCount()
extern "C" int32_t GridLayoutGroup_get_constraintCount_m3116 (GridLayoutGroup_t661 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_ConstraintCount_15);
		return L_0;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraintCount(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutGroup_SetProperty_TisInt32_t372_m3756_MethodInfo_var;
extern "C" void GridLayoutGroup_set_constraintCount_m3117 (GridLayoutGroup_t661 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		LayoutGroup_SetProperty_TisInt32_t372_m3756_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484167);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_ConstraintCount_15);
		int32_t L_1 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m3620(NULL /*static, unused*/, 1, L_1, /*hidden argument*/NULL);
		LayoutGroup_SetProperty_TisInt32_t372_m3756(__this, L_0, L_2, /*hidden argument*/LayoutGroup_SetProperty_TisInt32_t372_m3756_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputHorizontal()
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroupMethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" void GridLayoutGroup_CalculateLayoutInputHorizontal_m3118 (GridLayoutGroup_t661 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector2_t2  V_2 = {0};
	Vector2_t2  V_3 = {0};
	Vector2_t2  V_4 = {0};
	Vector2_t2  V_5 = {0};
	Vector2_t2  V_6 = {0};
	Vector2_t2  V_7 = {0};
	{
		LayoutGroup_CalculateLayoutInputHorizontal_m3168(__this, /*hidden argument*/NULL);
		V_0 = 0;
		V_1 = 0;
		int32_t L_0 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_1 = (__this->___m_ConstraintCount_15);
		int32_t L_2 = L_1;
		V_1 = L_2;
		V_0 = L_2;
		goto IL_0070;
	}

IL_0024:
	{
		int32_t L_3 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0057;
		}
	}
	{
		List_1_t667 * L_4 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_4);
		int32_t L_6 = (__this->___m_ConstraintCount_15);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_CeilToInt_m3757(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)L_5))/(float)(((float)L_6))))-(float)(0.001f))), /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		V_1 = L_8;
		V_0 = L_8;
		goto IL_0070;
	}

IL_0057:
	{
		V_0 = 1;
		List_1_t667 * L_9 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_11 = sqrtf((((float)L_10)));
		int32_t L_12 = Mathf_CeilToInt_m3757(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
	}

IL_0070:
	{
		RectOffset_t666 * L_13 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_horizontal_m3758(L_13, /*hidden argument*/NULL);
		Vector2_t2  L_15 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_2 = L_15;
		float L_16 = ((&V_2)->___x_1);
		Vector2_t2  L_17 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_3 = L_17;
		float L_18 = ((&V_3)->___x_1);
		int32_t L_19 = V_0;
		Vector2_t2  L_20 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_4 = L_20;
		float L_21 = ((&V_4)->___x_1);
		RectOffset_t666 * L_22 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = RectOffset_get_horizontal_m3758(L_22, /*hidden argument*/NULL);
		Vector2_t2  L_24 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_5 = L_24;
		float L_25 = ((&V_5)->___x_1);
		Vector2_t2  L_26 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_6 = L_26;
		float L_27 = ((&V_6)->___x_1);
		int32_t L_28 = V_1;
		Vector2_t2  L_29 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_7 = L_29;
		float L_30 = ((&V_7)->___x_1);
		LayoutGroup_SetLayoutInputForAxis_m3183(__this, ((float)((float)((float)((float)(((float)L_14))+(float)((float)((float)((float)((float)L_16+(float)L_18))*(float)(((float)L_19))))))-(float)L_21)), ((float)((float)((float)((float)(((float)L_23))+(float)((float)((float)((float)((float)L_25+(float)L_27))*(float)(((float)L_28))))))-(float)L_30)), (-1.0f), 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputVertical()
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" void GridLayoutGroup_CalculateLayoutInputVertical_m3119 (GridLayoutGroup_t661 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	Rect_t267  V_4 = {0};
	Vector2_t2  V_5 = {0};
	Vector2_t2  V_6 = {0};
	Vector2_t2  V_7 = {0};
	Vector2_t2  V_8 = {0};
	Vector2_t2  V_9 = {0};
	Vector2_t2  V_10 = {0};
	Vector2_t2  V_11 = {0};
	{
		V_0 = 0;
		int32_t L_0 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0033;
		}
	}
	{
		List_1_t667 * L_1 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_1);
		int32_t L_3 = (__this->___m_ConstraintCount_15);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_CeilToInt_m3757(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)L_2))/(float)(((float)L_3))))-(float)(0.001f))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_00ce;
	}

IL_0033:
	{
		int32_t L_5 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_6 = (__this->___m_ConstraintCount_15);
		V_0 = L_6;
		goto IL_00ce;
	}

IL_004b:
	{
		RectTransform_t556 * L_7 = LayoutGroup_get_rectTransform_m3166(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Rect_t267  L_8 = RectTransform_get_rect_m3463(L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		Vector2_t2  L_9 = Rect_get_size_m3466((&V_4), /*hidden argument*/NULL);
		V_5 = L_9;
		float L_10 = ((&V_5)->___x_1);
		V_1 = L_10;
		float L_11 = V_1;
		RectOffset_t666 * L_12 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_horizontal_m3758(L_12, /*hidden argument*/NULL);
		Vector2_t2  L_14 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_6 = L_14;
		float L_15 = ((&V_6)->___x_1);
		Vector2_t2  L_16 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_7 = L_16;
		float L_17 = ((&V_7)->___x_1);
		Vector2_t2  L_18 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_8 = L_18;
		float L_19 = ((&V_8)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_20 = Mathf_FloorToInt_m1587(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)((float)((float)L_11-(float)(((float)L_13))))+(float)L_15))+(float)(0.001f)))/(float)((float)((float)L_17+(float)L_19)))), /*hidden argument*/NULL);
		int32_t L_21 = Mathf_Max_m3620(NULL /*static, unused*/, 1, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		List_1_t667 * L_22 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_22);
		int32_t L_24 = V_2;
		int32_t L_25 = Mathf_CeilToInt_m3757(NULL /*static, unused*/, ((float)((float)(((float)L_23))/(float)(((float)L_24)))), /*hidden argument*/NULL);
		V_0 = L_25;
	}

IL_00ce:
	{
		RectOffset_t666 * L_26 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = RectOffset_get_vertical_m3759(L_26, /*hidden argument*/NULL);
		Vector2_t2  L_28 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_9 = L_28;
		float L_29 = ((&V_9)->___y_2);
		Vector2_t2  L_30 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_10 = L_30;
		float L_31 = ((&V_10)->___y_2);
		int32_t L_32 = V_0;
		Vector2_t2  L_33 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_11 = L_33;
		float L_34 = ((&V_11)->___y_2);
		V_3 = ((float)((float)((float)((float)(((float)L_27))+(float)((float)((float)((float)((float)L_29+(float)L_31))*(float)(((float)L_32))))))-(float)L_34));
		float L_35 = V_3;
		float L_36 = V_3;
		LayoutGroup_SetLayoutInputForAxis_m3183(__this, L_35, L_36, (-1.0f), 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutHorizontal()
extern "C" void GridLayoutGroup_SetLayoutHorizontal_m3120 (GridLayoutGroup_t661 * __this, const MethodInfo* method)
{
	{
		GridLayoutGroup_SetCellsAlongAxis_m3122(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutVertical()
extern "C" void GridLayoutGroup_SetLayoutVertical_m3121 (GridLayoutGroup_t661 * __this, const MethodInfo* method)
{
	{
		GridLayoutGroup_SetCellsAlongAxis_m3122(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.GridLayoutGroup::SetCellsAlongAxis(System.Int32)
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTrackerMethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" void GridLayoutGroup_SetCellsAlongAxis_m3122 (GridLayoutGroup_t661 * __this, int32_t ___axis, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t556 * V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Vector2_t2  V_11 = {0};
	Vector2_t2  V_12 = {0};
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	Rect_t267  V_16 = {0};
	Vector2_t2  V_17 = {0};
	Rect_t267  V_18 = {0};
	Vector2_t2  V_19 = {0};
	Vector2_t2  V_20 = {0};
	Vector2_t2  V_21 = {0};
	Vector2_t2  V_22 = {0};
	Vector2_t2  V_23 = {0};
	Vector2_t2  V_24 = {0};
	Vector2_t2  V_25 = {0};
	Vector2_t2  V_26 = {0};
	Vector2_t2  V_27 = {0};
	Vector2_t2  V_28 = {0};
	Vector2_t2  V_29 = {0};
	Vector2_t2  V_30 = {0};
	Vector2_t2  V_31 = {0};
	Vector2_t2  V_32 = {0};
	Vector2_t2  V_33 = {0};
	Vector2_t2  V_34 = {0};
	Vector2_t2  V_35 = {0};
	Vector2_t2  V_36 = {0};
	Vector2_t2  V_37 = {0};
	Vector2_t2  V_38 = {0};
	Vector2_t2  V_39 = {0};
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_0064;
		}
	}
	{
		V_0 = 0;
		goto IL_0052;
	}

IL_000d:
	{
		List_1_t667 * L_1 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		RectTransform_t556 * L_3 = (RectTransform_t556 *)VirtFuncInvoker1< RectTransform_t556 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_1, L_2);
		V_1 = L_3;
		DrivenRectTransformTracker_t622 * L_4 = &(((LayoutGroup_t662 *)__this)->___m_Tracker_5);
		RectTransform_t556 * L_5 = V_1;
		DrivenRectTransformTracker_Add_m3675(L_4, __this, L_5, ((int32_t)16134), /*hidden argument*/NULL);
		RectTransform_t556 * L_6 = V_1;
		Vector2_t2  L_7 = Vector2_get_up_m3439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectTransform_set_anchorMin_m3418(L_6, L_7, /*hidden argument*/NULL);
		RectTransform_t556 * L_8 = V_1;
		Vector2_t2  L_9 = Vector2_get_up_m3439(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		RectTransform_set_anchorMax_m3420(L_8, L_9, /*hidden argument*/NULL);
		RectTransform_t556 * L_10 = V_1;
		Vector2_t2  L_11 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		RectTransform_set_sizeDelta_m3411(L_10, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0052:
	{
		int32_t L_13 = V_0;
		List_1_t667 * L_14 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_14);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_0064:
	{
		RectTransform_t556 * L_16 = LayoutGroup_get_rectTransform_m3166(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Rect_t267  L_17 = RectTransform_get_rect_m3463(L_16, /*hidden argument*/NULL);
		V_16 = L_17;
		Vector2_t2  L_18 = Rect_get_size_m3466((&V_16), /*hidden argument*/NULL);
		V_17 = L_18;
		float L_19 = ((&V_17)->___x_1);
		V_2 = L_19;
		RectTransform_t556 * L_20 = LayoutGroup_get_rectTransform_m3166(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Rect_t267  L_21 = RectTransform_get_rect_m3463(L_20, /*hidden argument*/NULL);
		V_18 = L_21;
		Vector2_t2  L_22 = Rect_get_size_m3466((&V_18), /*hidden argument*/NULL);
		V_19 = L_22;
		float L_23 = ((&V_19)->___y_2);
		V_3 = L_23;
		V_4 = 1;
		V_5 = 1;
		int32_t L_24 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_24) == ((uint32_t)1))))
		{
			goto IL_00dc;
		}
	}
	{
		int32_t L_25 = (__this->___m_ConstraintCount_15);
		V_4 = L_25;
		List_1_t667 * L_26 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_26);
		int32_t L_28 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_29 = Mathf_CeilToInt_m3757(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)L_27))/(float)(((float)L_28))))-(float)(0.001f))), /*hidden argument*/NULL);
		V_5 = L_29;
		goto IL_021e;
	}

IL_00dc:
	{
		int32_t L_30 = (__this->___m_Constraint_14);
		if ((!(((uint32_t)L_30) == ((uint32_t)2))))
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_31 = (__this->___m_ConstraintCount_15);
		V_5 = L_31;
		List_1_t667 * L_32 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_32);
		int32_t L_34 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_35 = Mathf_CeilToInt_m3757(NULL /*static, unused*/, ((float)((float)((float)((float)(((float)L_33))/(float)(((float)L_34))))-(float)(0.001f))), /*hidden argument*/NULL);
		V_4 = L_35;
		goto IL_021e;
	}

IL_0112:
	{
		Vector2_t2  L_36 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_20 = L_36;
		float L_37 = ((&V_20)->___x_1);
		Vector2_t2  L_38 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_21 = L_38;
		float L_39 = ((&V_21)->___x_1);
		if ((!(((float)((float)((float)L_37+(float)L_39))) <= ((float)(0.0f)))))
		{
			goto IL_0147;
		}
	}
	{
		V_4 = ((int32_t)2147483647);
		goto IL_0198;
	}

IL_0147:
	{
		float L_40 = V_2;
		RectOffset_t666 * L_41 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		int32_t L_42 = RectOffset_get_horizontal_m3758(L_41, /*hidden argument*/NULL);
		Vector2_t2  L_43 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_22 = L_43;
		float L_44 = ((&V_22)->___x_1);
		Vector2_t2  L_45 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_23 = L_45;
		float L_46 = ((&V_23)->___x_1);
		Vector2_t2  L_47 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_24 = L_47;
		float L_48 = ((&V_24)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_49 = Mathf_FloorToInt_m1587(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)((float)((float)L_40-(float)(((float)L_42))))+(float)L_44))+(float)(0.001f)))/(float)((float)((float)L_46+(float)L_48)))), /*hidden argument*/NULL);
		int32_t L_50 = Mathf_Max_m3620(NULL /*static, unused*/, 1, L_49, /*hidden argument*/NULL);
		V_4 = L_50;
	}

IL_0198:
	{
		Vector2_t2  L_51 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_25 = L_51;
		float L_52 = ((&V_25)->___y_2);
		Vector2_t2  L_53 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_26 = L_53;
		float L_54 = ((&V_26)->___y_2);
		if ((!(((float)((float)((float)L_52+(float)L_54))) <= ((float)(0.0f)))))
		{
			goto IL_01cd;
		}
	}
	{
		V_5 = ((int32_t)2147483647);
		goto IL_021e;
	}

IL_01cd:
	{
		float L_55 = V_3;
		RectOffset_t666 * L_56 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		int32_t L_57 = RectOffset_get_vertical_m3759(L_56, /*hidden argument*/NULL);
		Vector2_t2  L_58 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_27 = L_58;
		float L_59 = ((&V_27)->___y_2);
		Vector2_t2  L_60 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_28 = L_60;
		float L_61 = ((&V_28)->___y_2);
		Vector2_t2  L_62 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_29 = L_62;
		float L_63 = ((&V_29)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_64 = Mathf_FloorToInt_m1587(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)((float)((float)L_55-(float)(((float)L_57))))+(float)L_59))+(float)(0.001f)))/(float)((float)((float)L_61+(float)L_63)))), /*hidden argument*/NULL);
		int32_t L_65 = Mathf_Max_m3620(NULL /*static, unused*/, 1, L_64, /*hidden argument*/NULL);
		V_5 = L_65;
	}

IL_021e:
	{
		int32_t L_66 = GridLayoutGroup_get_startCorner_m3106(__this, /*hidden argument*/NULL);
		V_6 = ((int32_t)((int32_t)L_66%(int32_t)2));
		int32_t L_67 = GridLayoutGroup_get_startCorner_m3106(__this, /*hidden argument*/NULL);
		V_7 = ((int32_t)((int32_t)L_67/(int32_t)2));
		int32_t L_68 = GridLayoutGroup_get_startAxis_m3108(__this, /*hidden argument*/NULL);
		if (L_68)
		{
			goto IL_027a;
		}
	}
	{
		int32_t L_69 = V_4;
		V_8 = L_69;
		int32_t L_70 = V_4;
		List_1_t667 * L_71 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_71);
		int32_t L_72 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_71);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_73 = Mathf_Clamp_m3446(NULL /*static, unused*/, L_70, 1, L_72, /*hidden argument*/NULL);
		V_9 = L_73;
		int32_t L_74 = V_5;
		List_1_t667 * L_75 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_75);
		int32_t L_76 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_75);
		int32_t L_77 = V_8;
		int32_t L_78 = Mathf_CeilToInt_m3757(NULL /*static, unused*/, ((float)((float)(((float)L_76))/(float)(((float)L_77)))), /*hidden argument*/NULL);
		int32_t L_79 = Mathf_Clamp_m3446(NULL /*static, unused*/, L_74, 1, L_78, /*hidden argument*/NULL);
		V_10 = L_79;
		goto IL_02b2;
	}

IL_027a:
	{
		int32_t L_80 = V_5;
		V_8 = L_80;
		int32_t L_81 = V_5;
		List_1_t667 * L_82 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_82);
		int32_t L_83 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_82);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_84 = Mathf_Clamp_m3446(NULL /*static, unused*/, L_81, 1, L_83, /*hidden argument*/NULL);
		V_10 = L_84;
		int32_t L_85 = V_4;
		List_1_t667 * L_86 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_86);
		int32_t L_87 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_86);
		int32_t L_88 = V_8;
		int32_t L_89 = Mathf_CeilToInt_m3757(NULL /*static, unused*/, ((float)((float)(((float)L_87))/(float)(((float)L_88)))), /*hidden argument*/NULL);
		int32_t L_90 = Mathf_Clamp_m3446(NULL /*static, unused*/, L_85, 1, L_89, /*hidden argument*/NULL);
		V_9 = L_90;
	}

IL_02b2:
	{
		int32_t L_91 = V_9;
		Vector2_t2  L_92 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_30 = L_92;
		float L_93 = ((&V_30)->___x_1);
		int32_t L_94 = V_9;
		Vector2_t2  L_95 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_31 = L_95;
		float L_96 = ((&V_31)->___x_1);
		int32_t L_97 = V_10;
		Vector2_t2  L_98 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_32 = L_98;
		float L_99 = ((&V_32)->___y_2);
		int32_t L_100 = V_10;
		Vector2_t2  L_101 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_33 = L_101;
		float L_102 = ((&V_33)->___y_2);
		Vector2__ctor_m1309((&V_11), ((float)((float)((float)((float)(((float)L_91))*(float)L_93))+(float)((float)((float)(((float)((int32_t)((int32_t)L_94-(int32_t)1))))*(float)L_96)))), ((float)((float)((float)((float)(((float)L_97))*(float)L_99))+(float)((float)((float)(((float)((int32_t)((int32_t)L_100-(int32_t)1))))*(float)L_102)))), /*hidden argument*/NULL);
		float L_103 = ((&V_11)->___x_1);
		float L_104 = LayoutGroup_GetStartOffset_m3182(__this, 0, L_103, /*hidden argument*/NULL);
		float L_105 = ((&V_11)->___y_2);
		float L_106 = LayoutGroup_GetStartOffset_m3182(__this, 1, L_105, /*hidden argument*/NULL);
		Vector2__ctor_m1309((&V_12), L_104, L_106, /*hidden argument*/NULL);
		V_13 = 0;
		goto IL_042c;
	}

IL_0336:
	{
		int32_t L_107 = GridLayoutGroup_get_startAxis_m3108(__this, /*hidden argument*/NULL);
		if (L_107)
		{
			goto IL_0354;
		}
	}
	{
		int32_t L_108 = V_13;
		int32_t L_109 = V_8;
		V_14 = ((int32_t)((int32_t)L_108%(int32_t)L_109));
		int32_t L_110 = V_13;
		int32_t L_111 = V_8;
		V_15 = ((int32_t)((int32_t)L_110/(int32_t)L_111));
		goto IL_0362;
	}

IL_0354:
	{
		int32_t L_112 = V_13;
		int32_t L_113 = V_8;
		V_14 = ((int32_t)((int32_t)L_112/(int32_t)L_113));
		int32_t L_114 = V_13;
		int32_t L_115 = V_8;
		V_15 = ((int32_t)((int32_t)L_114%(int32_t)L_115));
	}

IL_0362:
	{
		int32_t L_116 = V_6;
		if ((!(((uint32_t)L_116) == ((uint32_t)1))))
		{
			goto IL_0373;
		}
	}
	{
		int32_t L_117 = V_9;
		int32_t L_118 = V_14;
		V_14 = ((int32_t)((int32_t)((int32_t)((int32_t)L_117-(int32_t)1))-(int32_t)L_118));
	}

IL_0373:
	{
		int32_t L_119 = V_7;
		if ((!(((uint32_t)L_119) == ((uint32_t)1))))
		{
			goto IL_0384;
		}
	}
	{
		int32_t L_120 = V_10;
		int32_t L_121 = V_15;
		V_15 = ((int32_t)((int32_t)((int32_t)((int32_t)L_120-(int32_t)1))-(int32_t)L_121));
	}

IL_0384:
	{
		List_1_t667 * L_122 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		int32_t L_123 = V_13;
		NullCheck(L_122);
		RectTransform_t556 * L_124 = (RectTransform_t556 *)VirtFuncInvoker1< RectTransform_t556 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_122, L_123);
		float L_125 = ((&V_12)->___x_1);
		Vector2_t2  L_126 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_34 = L_126;
		float L_127 = Vector2_get_Item_m3571((&V_34), 0, /*hidden argument*/NULL);
		Vector2_t2  L_128 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_35 = L_128;
		float L_129 = Vector2_get_Item_m3571((&V_35), 0, /*hidden argument*/NULL);
		int32_t L_130 = V_14;
		Vector2_t2  L_131 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_36 = L_131;
		float L_132 = Vector2_get_Item_m3571((&V_36), 0, /*hidden argument*/NULL);
		LayoutGroup_SetChildAlongAxis_m3184(__this, L_124, 0, ((float)((float)L_125+(float)((float)((float)((float)((float)L_127+(float)L_129))*(float)(((float)L_130)))))), L_132, /*hidden argument*/NULL);
		List_1_t667 * L_133 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		int32_t L_134 = V_13;
		NullCheck(L_133);
		RectTransform_t556 * L_135 = (RectTransform_t556 *)VirtFuncInvoker1< RectTransform_t556 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_133, L_134);
		float L_136 = ((&V_12)->___y_2);
		Vector2_t2  L_137 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_37 = L_137;
		float L_138 = Vector2_get_Item_m3571((&V_37), 1, /*hidden argument*/NULL);
		Vector2_t2  L_139 = GridLayoutGroup_get_spacing_m3112(__this, /*hidden argument*/NULL);
		V_38 = L_139;
		float L_140 = Vector2_get_Item_m3571((&V_38), 1, /*hidden argument*/NULL);
		int32_t L_141 = V_15;
		Vector2_t2  L_142 = GridLayoutGroup_get_cellSize_m3110(__this, /*hidden argument*/NULL);
		V_39 = L_142;
		float L_143 = Vector2_get_Item_m3571((&V_39), 1, /*hidden argument*/NULL);
		LayoutGroup_SetChildAlongAxis_m3184(__this, L_135, 1, ((float)((float)L_136+(float)((float)((float)((float)((float)L_138+(float)L_140))*(float)(((float)L_141)))))), L_143, /*hidden argument*/NULL);
		int32_t L_144 = V_13;
		V_13 = ((int32_t)((int32_t)L_144+(int32_t)1));
	}

IL_042c:
	{
		int32_t L_145 = V_13;
		List_1_t667 * L_146 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_146);
		int32_t L_147 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_146);
		if ((((int32_t)L_145) < ((int32_t)L_147)))
		{
			goto IL_0336;
		}
	}
	{
		return;
	}
}
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroup.h"
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroupMethodDeclarations.h"
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"
// System.Void UnityEngine.UI.HorizontalLayoutGroup::.ctor()
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"
extern "C" void HorizontalLayoutGroup__ctor_m3123 (HorizontalLayoutGroup_t663 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup__ctor_m3128(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputHorizontal()
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m3124 (HorizontalLayoutGroup_t663 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_CalculateLayoutInputHorizontal_m3168(__this, /*hidden argument*/NULL);
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m3135(__this, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputVertical()
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputVertical_m3125 (HorizontalLayoutGroup_t663 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m3135(__this, 1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutHorizontal()
extern "C" void HorizontalLayoutGroup_SetLayoutHorizontal_m3126 (HorizontalLayoutGroup_t663 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m3136(__this, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutVertical()
extern "C" void HorizontalLayoutGroup_SetLayoutVertical_m3127 (HorizontalLayoutGroup_t663 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m3136(__this, 1, 0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrou.h"
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Single>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Single>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisSingle_t388_m3760_gshared (LayoutGroup_t662 * __this, float* p0, float p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisSingle_t388_m3760(__this, p0, p1, method) (( void (*) (LayoutGroup_t662 *, float*, float, const MethodInfo*))LayoutGroup_SetProperty_TisSingle_t388_m3760_gshared)(__this, p0, p1, method)
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Boolean>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Boolean>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisBoolean_t384_m3761_gshared (LayoutGroup_t662 * __this, bool* p0, bool p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisBoolean_t384_m3761(__this, p0, p1, method) (( void (*) (LayoutGroup_t662 *, bool*, bool, const MethodInfo*))LayoutGroup_SetProperty_TisBoolean_t384_m3761_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
extern "C" void HorizontalOrVerticalLayoutGroup__ctor_m3128 (HorizontalOrVerticalLayoutGroup_t664 * __this, const MethodInfo* method)
{
	{
		__this->___m_ChildForceExpandWidth_11 = 1;
		__this->___m_ChildForceExpandHeight_12 = 1;
		LayoutGroup__ctor_m3161(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern "C" float HorizontalOrVerticalLayoutGroup_get_spacing_m3129 (HorizontalOrVerticalLayoutGroup_t664 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Spacing_10);
		return L_0;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern const MethodInfo* LayoutGroup_SetProperty_TisSingle_t388_m3760_MethodInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_set_spacing_m3130 (HorizontalOrVerticalLayoutGroup_t664 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisSingle_t388_m3760_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484168);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_Spacing_10);
		float L_1 = ___value;
		LayoutGroup_SetProperty_TisSingle_t388_m3760(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisSingle_t388_m3760_MethodInfo_var);
		return;
	}
}
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m3131 (HorizontalOrVerticalLayoutGroup_t664 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_ChildForceExpandWidth_11);
		return L_0;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern const MethodInfo* LayoutGroup_SetProperty_TisBoolean_t384_m3761_MethodInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m3132 (HorizontalOrVerticalLayoutGroup_t664 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisBoolean_t384_m3761_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484169);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool* L_0 = &(__this->___m_ChildForceExpandWidth_11);
		bool L_1 = ___value;
		LayoutGroup_SetProperty_TisBoolean_t384_m3761(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisBoolean_t384_m3761_MethodInfo_var);
		return;
	}
}
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m3133 (HorizontalOrVerticalLayoutGroup_t664 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_ChildForceExpandHeight_12);
		return L_0;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern const MethodInfo* LayoutGroup_SetProperty_TisBoolean_t384_m3761_MethodInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m3134 (HorizontalOrVerticalLayoutGroup_t664 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisBoolean_t384_m3761_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484169);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool* L_0 = &(__this->___m_ChildForceExpandHeight_12);
		bool L_1 = ___value;
		LayoutGroup_SetProperty_TisBoolean_t384_m3761(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisBoolean_t384_m3761_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m3135 (HorizontalOrVerticalLayoutGroup_t664 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	RectTransform_t556 * V_6 = {0};
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	int32_t G_B3_0 = 0;
	bool G_B7_0 = false;
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		RectOffset_t666 * L_1 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = RectOffset_get_horizontal_m3758(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0021;
	}

IL_0016:
	{
		RectOffset_t666 * L_3 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_vertical_m3759(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0021:
	{
		V_0 = (((float)G_B3_0));
		float L_5 = V_0;
		V_1 = L_5;
		float L_6 = V_0;
		V_2 = L_6;
		V_3 = (0.0f);
		bool L_7 = ___isVertical;
		int32_t L_8 = ___axis;
		V_4 = ((int32_t)((int32_t)L_7^(int32_t)((((int32_t)L_8) == ((int32_t)1))? 1 : 0)));
		V_5 = 0;
		goto IL_00e2;
	}

IL_003d:
	{
		List_1_t667 * L_9 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_5;
		NullCheck(L_9);
		RectTransform_t556 * L_11 = (RectTransform_t556 *)VirtFuncInvoker1< RectTransform_t556 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_9, L_10);
		V_6 = L_11;
		RectTransform_t556 * L_12 = V_6;
		int32_t L_13 = ___axis;
		float L_14 = LayoutUtility_GetMinSize_m3211(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		V_7 = L_14;
		RectTransform_t556 * L_15 = V_6;
		int32_t L_16 = ___axis;
		float L_17 = LayoutUtility_GetPreferredSize_m3212(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_8 = L_17;
		RectTransform_t556 * L_18 = V_6;
		int32_t L_19 = ___axis;
		float L_20 = LayoutUtility_GetFlexibleSize_m3213(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_9 = L_20;
		int32_t L_21 = ___axis;
		if (L_21)
		{
			goto IL_007b;
		}
	}
	{
		bool L_22 = HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m3131(__this, /*hidden argument*/NULL);
		G_B7_0 = L_22;
		goto IL_0081;
	}

IL_007b:
	{
		bool L_23 = HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m3133(__this, /*hidden argument*/NULL);
		G_B7_0 = L_23;
	}

IL_0081:
	{
		if (!G_B7_0)
		{
			goto IL_0094;
		}
	}
	{
		float L_24 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Max_m3711(NULL /*static, unused*/, L_24, (1.0f), /*hidden argument*/NULL);
		V_9 = L_25;
	}

IL_0094:
	{
		bool L_26 = V_4;
		if (!L_26)
		{
			goto IL_00bf;
		}
	}
	{
		float L_27 = V_7;
		float L_28 = V_0;
		float L_29 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_30 = Mathf_Max_m3711(NULL /*static, unused*/, ((float)((float)L_27+(float)L_28)), L_29, /*hidden argument*/NULL);
		V_1 = L_30;
		float L_31 = V_8;
		float L_32 = V_0;
		float L_33 = V_2;
		float L_34 = Mathf_Max_m3711(NULL /*static, unused*/, ((float)((float)L_31+(float)L_32)), L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		float L_35 = V_9;
		float L_36 = V_3;
		float L_37 = Mathf_Max_m3711(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_3 = L_37;
		goto IL_00dc;
	}

IL_00bf:
	{
		float L_38 = V_1;
		float L_39 = V_7;
		float L_40 = HorizontalOrVerticalLayoutGroup_get_spacing_m3129(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_38+(float)((float)((float)L_39+(float)L_40))));
		float L_41 = V_2;
		float L_42 = V_8;
		float L_43 = HorizontalOrVerticalLayoutGroup_get_spacing_m3129(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_41+(float)((float)((float)L_42+(float)L_43))));
		float L_44 = V_3;
		float L_45 = V_9;
		V_3 = ((float)((float)L_44+(float)L_45));
	}

IL_00dc:
	{
		int32_t L_46 = V_5;
		V_5 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_00e2:
	{
		int32_t L_47 = V_5;
		List_1_t667 * L_48 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_48);
		if ((((int32_t)L_47) < ((int32_t)L_49)))
		{
			goto IL_003d;
		}
	}
	{
		bool L_50 = V_4;
		if (L_50)
		{
			goto IL_011e;
		}
	}
	{
		List_1_t667 * L_51 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_51);
		int32_t L_52 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_51);
		if ((((int32_t)L_52) <= ((int32_t)0)))
		{
			goto IL_011e;
		}
	}
	{
		float L_53 = V_1;
		float L_54 = HorizontalOrVerticalLayoutGroup_get_spacing_m3129(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_53-(float)L_54));
		float L_55 = V_2;
		float L_56 = HorizontalOrVerticalLayoutGroup_get_spacing_m3129(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_55-(float)L_56));
	}

IL_011e:
	{
		float L_57 = V_1;
		float L_58 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_59 = Mathf_Max_m3711(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		V_2 = L_59;
		float L_60 = V_1;
		float L_61 = V_2;
		float L_62 = V_3;
		int32_t L_63 = ___axis;
		LayoutGroup_SetLayoutInputForAxis_m3183(__this, L_60, L_61, L_62, L_63, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m3136 (HorizontalOrVerticalLayoutGroup_t664 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	RectTransform_t556 * V_4 = {0};
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	int32_t V_13 = 0;
	RectTransform_t556 * V_14 = {0};
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	Rect_t267  V_19 = {0};
	Vector2_t2  V_20 = {0};
	float G_B3_0 = 0.0f;
	float G_B2_0 = 0.0f;
	int32_t G_B4_0 = 0;
	float G_B4_1 = 0.0f;
	bool G_B8_0 = false;
	float G_B12_0 = 0.0f;
	float G_B12_1 = 0.0f;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	float G_B13_0 = 0.0f;
	float G_B13_1 = 0.0f;
	float G_B13_2 = 0.0f;
	int32_t G_B19_0 = 0;
	float G_B23_0 = 0.0f;
	int32_t G_B23_1 = 0;
	HorizontalOrVerticalLayoutGroup_t664 * G_B23_2 = {0};
	float G_B22_0 = 0.0f;
	int32_t G_B22_1 = 0;
	HorizontalOrVerticalLayoutGroup_t664 * G_B22_2 = {0};
	int32_t G_B24_0 = 0;
	float G_B24_1 = 0.0f;
	int32_t G_B24_2 = 0;
	HorizontalOrVerticalLayoutGroup_t664 * G_B24_3 = {0};
	bool G_B34_0 = false;
	{
		RectTransform_t556 * L_0 = LayoutGroup_get_rectTransform_m3166(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rect_t267  L_1 = RectTransform_get_rect_m3463(L_0, /*hidden argument*/NULL);
		V_19 = L_1;
		Vector2_t2  L_2 = Rect_get_size_m3466((&V_19), /*hidden argument*/NULL);
		V_20 = L_2;
		int32_t L_3 = ___axis;
		float L_4 = Vector2_get_Item_m3571((&V_20), L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = ___isVertical;
		int32_t L_6 = ___axis;
		V_1 = ((int32_t)((int32_t)L_5^(int32_t)((((int32_t)L_6) == ((int32_t)1))? 1 : 0)));
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_00fe;
		}
	}
	{
		float L_8 = V_0;
		int32_t L_9 = ___axis;
		G_B2_0 = L_8;
		if (L_9)
		{
			G_B3_0 = L_8;
			goto IL_0043;
		}
	}
	{
		RectOffset_t666 * L_10 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_horizontal_m3758(L_10, /*hidden argument*/NULL);
		G_B4_0 = L_11;
		G_B4_1 = G_B2_0;
		goto IL_004e;
	}

IL_0043:
	{
		RectOffset_t666 * L_12 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_vertical_m3759(L_12, /*hidden argument*/NULL);
		G_B4_0 = L_13;
		G_B4_1 = G_B3_0;
	}

IL_004e:
	{
		V_2 = ((float)((float)G_B4_1-(float)(((float)G_B4_0))));
		V_3 = 0;
		goto IL_00e8;
	}

IL_0058:
	{
		List_1_t667 * L_14 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		int32_t L_15 = V_3;
		NullCheck(L_14);
		RectTransform_t556 * L_16 = (RectTransform_t556 *)VirtFuncInvoker1< RectTransform_t556 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_14, L_15);
		V_4 = L_16;
		RectTransform_t556 * L_17 = V_4;
		int32_t L_18 = ___axis;
		float L_19 = LayoutUtility_GetMinSize_m3211(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		RectTransform_t556 * L_20 = V_4;
		int32_t L_21 = ___axis;
		float L_22 = LayoutUtility_GetPreferredSize_m3212(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		V_6 = L_22;
		RectTransform_t556 * L_23 = V_4;
		int32_t L_24 = ___axis;
		float L_25 = LayoutUtility_GetFlexibleSize_m3213(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_7 = L_25;
		int32_t L_26 = ___axis;
		if (L_26)
		{
			goto IL_0095;
		}
	}
	{
		bool L_27 = HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m3131(__this, /*hidden argument*/NULL);
		G_B8_0 = L_27;
		goto IL_009b;
	}

IL_0095:
	{
		bool L_28 = HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m3133(__this, /*hidden argument*/NULL);
		G_B8_0 = L_28;
	}

IL_009b:
	{
		if (!G_B8_0)
		{
			goto IL_00ae;
		}
	}
	{
		float L_29 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_30 = Mathf_Max_m3711(NULL /*static, unused*/, L_29, (1.0f), /*hidden argument*/NULL);
		V_7 = L_30;
	}

IL_00ae:
	{
		float L_31 = V_2;
		float L_32 = V_5;
		float L_33 = V_7;
		G_B11_0 = L_32;
		G_B11_1 = L_31;
		if ((!(((float)L_33) > ((float)(0.0f)))))
		{
			G_B12_0 = L_32;
			G_B12_1 = L_31;
			goto IL_00c3;
		}
	}
	{
		float L_34 = V_0;
		G_B13_0 = L_34;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c5;
	}

IL_00c3:
	{
		float L_35 = V_6;
		G_B13_0 = L_35;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_36 = Mathf_Clamp_m1358(NULL /*static, unused*/, G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		V_8 = L_36;
		int32_t L_37 = ___axis;
		float L_38 = V_8;
		float L_39 = LayoutGroup_GetStartOffset_m3182(__this, L_37, L_38, /*hidden argument*/NULL);
		V_9 = L_39;
		RectTransform_t556 * L_40 = V_4;
		int32_t L_41 = ___axis;
		float L_42 = V_9;
		float L_43 = V_8;
		LayoutGroup_SetChildAlongAxis_m3184(__this, L_40, L_41, L_42, L_43, /*hidden argument*/NULL);
		int32_t L_44 = V_3;
		V_3 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_00e8:
	{
		int32_t L_45 = V_3;
		List_1_t667 * L_46 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		int32_t L_47 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_46);
		if ((((int32_t)L_45) < ((int32_t)L_47)))
		{
			goto IL_0058;
		}
	}
	{
		goto IL_028e;
	}

IL_00fe:
	{
		int32_t L_48 = ___axis;
		if (L_48)
		{
			goto IL_0114;
		}
	}
	{
		RectOffset_t666 * L_49 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		int32_t L_50 = RectOffset_get_left_m3762(L_49, /*hidden argument*/NULL);
		G_B19_0 = L_50;
		goto IL_011f;
	}

IL_0114:
	{
		RectOffset_t666 * L_51 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_51);
		int32_t L_52 = RectOffset_get_top_m3763(L_51, /*hidden argument*/NULL);
		G_B19_0 = L_52;
	}

IL_011f:
	{
		V_10 = (((float)G_B19_0));
		int32_t L_53 = ___axis;
		float L_54 = LayoutGroup_GetTotalFlexibleSize_m3181(__this, L_53, /*hidden argument*/NULL);
		if ((!(((float)L_54) == ((float)(0.0f)))))
		{
			goto IL_0173;
		}
	}
	{
		int32_t L_55 = ___axis;
		float L_56 = LayoutGroup_GetTotalPreferredSize_m3180(__this, L_55, /*hidden argument*/NULL);
		float L_57 = V_0;
		if ((!(((float)L_56) < ((float)L_57))))
		{
			goto IL_0173;
		}
	}
	{
		int32_t L_58 = ___axis;
		int32_t L_59 = ___axis;
		float L_60 = LayoutGroup_GetTotalPreferredSize_m3180(__this, L_59, /*hidden argument*/NULL);
		int32_t L_61 = ___axis;
		G_B22_0 = L_60;
		G_B22_1 = L_58;
		G_B22_2 = __this;
		if (L_61)
		{
			G_B23_0 = L_60;
			G_B23_1 = L_58;
			G_B23_2 = __this;
			goto IL_015f;
		}
	}
	{
		RectOffset_t666 * L_62 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = RectOffset_get_horizontal_m3758(L_62, /*hidden argument*/NULL);
		G_B24_0 = L_63;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		goto IL_016a;
	}

IL_015f:
	{
		RectOffset_t666 * L_64 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_64);
		int32_t L_65 = RectOffset_get_vertical_m3759(L_64, /*hidden argument*/NULL);
		G_B24_0 = L_65;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
	}

IL_016a:
	{
		NullCheck(G_B24_3);
		float L_66 = LayoutGroup_GetStartOffset_m3182(G_B24_3, G_B24_2, ((float)((float)G_B24_1-(float)(((float)G_B24_0)))), /*hidden argument*/NULL);
		V_10 = L_66;
	}

IL_0173:
	{
		V_11 = (0.0f);
		int32_t L_67 = ___axis;
		float L_68 = LayoutGroup_GetTotalMinSize_m3179(__this, L_67, /*hidden argument*/NULL);
		int32_t L_69 = ___axis;
		float L_70 = LayoutGroup_GetTotalPreferredSize_m3180(__this, L_69, /*hidden argument*/NULL);
		if ((((float)L_68) == ((float)L_70)))
		{
			goto IL_01ad;
		}
	}
	{
		float L_71 = V_0;
		int32_t L_72 = ___axis;
		float L_73 = LayoutGroup_GetTotalMinSize_m3179(__this, L_72, /*hidden argument*/NULL);
		int32_t L_74 = ___axis;
		float L_75 = LayoutGroup_GetTotalPreferredSize_m3180(__this, L_74, /*hidden argument*/NULL);
		int32_t L_76 = ___axis;
		float L_77 = LayoutGroup_GetTotalMinSize_m3179(__this, L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_78 = Mathf_Clamp01_m1576(NULL /*static, unused*/, ((float)((float)((float)((float)L_71-(float)L_73))/(float)((float)((float)L_75-(float)L_77)))), /*hidden argument*/NULL);
		V_11 = L_78;
	}

IL_01ad:
	{
		V_12 = (0.0f);
		float L_79 = V_0;
		int32_t L_80 = ___axis;
		float L_81 = LayoutGroup_GetTotalPreferredSize_m3180(__this, L_80, /*hidden argument*/NULL);
		if ((!(((float)L_79) > ((float)L_81))))
		{
			goto IL_01e5;
		}
	}
	{
		int32_t L_82 = ___axis;
		float L_83 = LayoutGroup_GetTotalFlexibleSize_m3181(__this, L_82, /*hidden argument*/NULL);
		if ((!(((float)L_83) > ((float)(0.0f)))))
		{
			goto IL_01e5;
		}
	}
	{
		float L_84 = V_0;
		int32_t L_85 = ___axis;
		float L_86 = LayoutGroup_GetTotalPreferredSize_m3180(__this, L_85, /*hidden argument*/NULL);
		int32_t L_87 = ___axis;
		float L_88 = LayoutGroup_GetTotalFlexibleSize_m3181(__this, L_87, /*hidden argument*/NULL);
		V_12 = ((float)((float)((float)((float)L_84-(float)L_86))/(float)L_88));
	}

IL_01e5:
	{
		V_13 = 0;
		goto IL_027c;
	}

IL_01ed:
	{
		List_1_t667 * L_89 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		int32_t L_90 = V_13;
		NullCheck(L_89);
		RectTransform_t556 * L_91 = (RectTransform_t556 *)VirtFuncInvoker1< RectTransform_t556 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Item(System.Int32) */, L_89, L_90);
		V_14 = L_91;
		RectTransform_t556 * L_92 = V_14;
		int32_t L_93 = ___axis;
		float L_94 = LayoutUtility_GetMinSize_m3211(NULL /*static, unused*/, L_92, L_93, /*hidden argument*/NULL);
		V_15 = L_94;
		RectTransform_t556 * L_95 = V_14;
		int32_t L_96 = ___axis;
		float L_97 = LayoutUtility_GetPreferredSize_m3212(NULL /*static, unused*/, L_95, L_96, /*hidden argument*/NULL);
		V_16 = L_97;
		RectTransform_t556 * L_98 = V_14;
		int32_t L_99 = ___axis;
		float L_100 = LayoutUtility_GetFlexibleSize_m3213(NULL /*static, unused*/, L_98, L_99, /*hidden argument*/NULL);
		V_17 = L_100;
		int32_t L_101 = ___axis;
		if (L_101)
		{
			goto IL_022b;
		}
	}
	{
		bool L_102 = HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m3131(__this, /*hidden argument*/NULL);
		G_B34_0 = L_102;
		goto IL_0231;
	}

IL_022b:
	{
		bool L_103 = HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m3133(__this, /*hidden argument*/NULL);
		G_B34_0 = L_103;
	}

IL_0231:
	{
		if (!G_B34_0)
		{
			goto IL_0244;
		}
	}
	{
		float L_104 = V_17;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_105 = Mathf_Max_m3711(NULL /*static, unused*/, L_104, (1.0f), /*hidden argument*/NULL);
		V_17 = L_105;
	}

IL_0244:
	{
		float L_106 = V_15;
		float L_107 = V_16;
		float L_108 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_109 = Mathf_Lerp_m1395(NULL /*static, unused*/, L_106, L_107, L_108, /*hidden argument*/NULL);
		V_18 = L_109;
		float L_110 = V_18;
		float L_111 = V_17;
		float L_112 = V_12;
		V_18 = ((float)((float)L_110+(float)((float)((float)L_111*(float)L_112))));
		RectTransform_t556 * L_113 = V_14;
		int32_t L_114 = ___axis;
		float L_115 = V_10;
		float L_116 = V_18;
		LayoutGroup_SetChildAlongAxis_m3184(__this, L_113, L_114, L_115, L_116, /*hidden argument*/NULL);
		float L_117 = V_10;
		float L_118 = V_18;
		float L_119 = HorizontalOrVerticalLayoutGroup_get_spacing_m3129(__this, /*hidden argument*/NULL);
		V_10 = ((float)((float)L_117+(float)((float)((float)L_118+(float)L_119))));
		int32_t L_120 = V_13;
		V_13 = ((int32_t)((int32_t)L_120+(int32_t)1));
	}

IL_027c:
	{
		int32_t L_121 = V_13;
		List_1_t667 * L_122 = LayoutGroup_get_rectChildren_m3167(__this, /*hidden argument*/NULL);
		NullCheck(L_122);
		int32_t L_123 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.RectTransform>::get_Count() */, L_122);
		if ((((int32_t)L_121) < ((int32_t)L_123)))
		{
			goto IL_01ed;
		}
	}

IL_028e:
	{
		return;
	}
}
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement.h"
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElementMethodDeclarations.h"
// Declaration System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Boolean>(!!0&,!!0)
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct<System.Boolean>(!!0&,!!0)
extern "C" bool SetPropertyUtility_SetStruct_TisBoolean_t384_m3553_gshared (Object_t * __this /* static, unused */, bool* p0, bool p1, const MethodInfo* method);
#define SetPropertyUtility_SetStruct_TisBoolean_t384_m3553(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, bool*, bool, const MethodInfo*))SetPropertyUtility_SetStruct_TisBoolean_t384_m3553_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void UnityEngine.UI.LayoutElement::.ctor()
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
extern "C" void LayoutElement__ctor_m3137 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		__this->___m_MinWidth_3 = (-1.0f);
		__this->___m_MinHeight_4 = (-1.0f);
		__this->___m_PreferredWidth_5 = (-1.0f);
		__this->___m_PreferredHeight_6 = (-1.0f);
		__this->___m_FlexibleWidth_7 = (-1.0f);
		__this->___m_FlexibleHeight_8 = (-1.0f);
		UIBehaviour__ctor_m2006(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutElement::get_ignoreLayout()
extern "C" bool LayoutElement_get_ignoreLayout_m3138 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_IgnoreLayout_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_ignoreLayout(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElementMethodDeclarations.h"
extern const MethodInfo* SetPropertyUtility_SetStruct_TisBoolean_t384_m3553_MethodInfo_var;
extern "C" void LayoutElement_set_ignoreLayout_m3139 (LayoutElement_t665 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisBoolean_t384_m3553_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484104);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool* L_0 = &(__this->___m_IgnoreLayout_2);
		bool L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisBoolean_t384_m3553(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisBoolean_t384_m3553_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputHorizontal()
extern "C" void LayoutElement_CalculateLayoutInputHorizontal_m3140 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputVertical()
extern "C" void LayoutElement_CalculateLayoutInputVertical_m3141 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_minWidth()
extern "C" float LayoutElement_get_minWidth_m3142 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_MinWidth_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var;
extern "C" void LayoutElement_set_minWidth_m3143 (LayoutElement_t665 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_MinWidth_3);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t388_m3555(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_minHeight()
extern "C" float LayoutElement_get_minHeight_m3144 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_MinHeight_4);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var;
extern "C" void LayoutElement_set_minHeight_m3145 (LayoutElement_t665 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_MinHeight_4);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t388_m3555(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_preferredWidth()
extern "C" float LayoutElement_get_preferredWidth_m3146 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_PreferredWidth_5);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var;
extern "C" void LayoutElement_set_preferredWidth_m3147 (LayoutElement_t665 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_PreferredWidth_5);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t388_m3555(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_preferredHeight()
extern "C" float LayoutElement_get_preferredHeight_m3148 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_PreferredHeight_6);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var;
extern "C" void LayoutElement_set_preferredHeight_m3149 (LayoutElement_t665 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_PreferredHeight_6);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t388_m3555(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth()
extern "C" float LayoutElement_get_flexibleWidth_m3150 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FlexibleWidth_7);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var;
extern "C" void LayoutElement_set_flexibleWidth_m3151 (LayoutElement_t665 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_FlexibleWidth_7);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t388_m3555(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight()
extern "C" float LayoutElement_get_flexibleHeight_m3152 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FlexibleHeight_8);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single)
extern const MethodInfo* SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var;
extern "C" void LayoutElement_set_flexibleHeight_m3153 (LayoutElement_t665 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		s_Il2CppMethodIntialized = true;
	}
	{
		float* L_0 = &(__this->___m_FlexibleHeight_8);
		float L_1 = ___value;
		bool L_2 = SetPropertyUtility_SetStruct_TisSingle_t388_m3555(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/SetPropertyUtility_SetStruct_TisSingle_t388_m3555_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Int32 UnityEngine.UI.LayoutElement::get_layoutPriority()
extern "C" int32_t LayoutElement_get_layoutPriority_m3154 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnEnable()
extern "C" void LayoutElement_OnEnable_m3155 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m2008(__this, /*hidden argument*/NULL);
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnTransformParentChanged()
extern "C" void LayoutElement_OnTransformParentChanged_m3156 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnDisable()
extern "C" void LayoutElement_OnDisable_m3157 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m2010(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnDidApplyAnimationProperties()
extern "C" void LayoutElement_OnDidApplyAnimationProperties_m3158 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::OnBeforeTransformParentChanged()
extern "C" void LayoutElement_OnBeforeTransformParentChanged_m3159 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	{
		LayoutElement_SetDirty_m3160(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutElement::SetDirty()
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
extern TypeInfo* RectTransform_t556_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern "C" void LayoutElement_SetDirty_m3160 (LayoutElement_t665 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Transform_t35 * L_1 = Component_get_transform_m1417(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m3199(NULL /*static, unused*/, ((RectTransform_t556 *)IsInstSealed(L_1, RectTransform_t556_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.RectTransform/Edge
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
struct RectOffset_t666;
struct Object_t;
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Object>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<System.Object>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisObject_t_m3770_gshared (LayoutGroup_t662 * __this, Object_t ** p0, Object_t * p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisObject_t_m3770(__this, p0, p1, method) (( void (*) (LayoutGroup_t662 *, Object_t **, Object_t *, const MethodInfo*))LayoutGroup_SetProperty_TisObject_t_m3770_gshared)(__this, p0, p1, method)
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.RectOffset>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.RectOffset>(!!0&,!!0)
#define LayoutGroup_SetProperty_TisRectOffset_t666_m3766(__this, p0, p1, method) (( void (*) (LayoutGroup_t662 *, RectOffset_t666 **, RectOffset_t666 *, const MethodInfo*))LayoutGroup_SetProperty_TisObject_t_m3770_gshared)(__this, p0, p1, method)
// Declaration System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.TextAnchor>(!!0&,!!0)
// System.Void UnityEngine.UI.LayoutGroup::SetProperty<UnityEngine.TextAnchor>(!!0&,!!0)
extern "C" void LayoutGroup_SetProperty_TisTextAnchor_t766_m3767_gshared (LayoutGroup_t662 * __this, int32_t* p0, int32_t p1, const MethodInfo* method);
#define LayoutGroup_SetProperty_TisTextAnchor_t766_m3767(__this, p0, p1, method) (( void (*) (LayoutGroup_t662 *, int32_t*, int32_t, const MethodInfo*))LayoutGroup_SetProperty_TisTextAnchor_t766_m3767_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.UI.LayoutGroup::.ctor()
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
#include "mscorlib_System_Collections_Generic_List_1_gen_35MethodDeclarations.h"
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
extern TypeInfo* RectOffset_t666_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t667_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3765_MethodInfo_var;
extern "C" void LayoutGroup__ctor_m3161 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t666_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(520);
		List_1_t667_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(521);
		List_1__ctor_m3765_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484170);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t666 * L_0 = (RectOffset_t666 *)il2cpp_codegen_object_new (RectOffset_t666_il2cpp_TypeInfo_var);
		RectOffset__ctor_m3764(L_0, /*hidden argument*/NULL);
		__this->___m_Padding_2 = L_0;
		Vector2_t2  L_1 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_TotalMinSize_6 = L_1;
		Vector2_t2  L_2 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_TotalPreferredSize_7 = L_2;
		Vector2_t2  L_3 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_TotalFlexibleSize_8 = L_3;
		List_1_t667 * L_4 = (List_1_t667 *)il2cpp_codegen_object_new (List_1_t667_il2cpp_TypeInfo_var);
		List_1__ctor_m3765(L_4, /*hidden argument*/List_1__ctor_m3765_MethodInfo_var);
		__this->___m_RectChildren_9 = L_4;
		UIBehaviour__ctor_m2006(__this, /*hidden argument*/NULL);
		RectOffset_t666 * L_5 = (__this->___m_Padding_2);
		if (L_5)
		{
			goto IL_0053;
		}
	}
	{
		RectOffset_t666 * L_6 = (RectOffset_t666 *)il2cpp_codegen_object_new (RectOffset_t666_il2cpp_TypeInfo_var);
		RectOffset__ctor_m3764(L_6, /*hidden argument*/NULL);
		__this->___m_Padding_2 = L_6;
	}

IL_0053:
	{
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::get_padding()
extern "C" RectOffset_t666 * LayoutGroup_get_padding_m3162 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		RectOffset_t666 * L_0 = (__this->___m_Padding_2);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::set_padding(UnityEngine.RectOffset)
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
extern const MethodInfo* LayoutGroup_SetProperty_TisRectOffset_t666_m3766_MethodInfo_var;
extern "C" void LayoutGroup_set_padding_m3163 (LayoutGroup_t662 * __this, RectOffset_t666 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisRectOffset_t666_m3766_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484171);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t666 ** L_0 = &(__this->___m_Padding_2);
		RectOffset_t666 * L_1 = ___value;
		LayoutGroup_SetProperty_TisRectOffset_t666_m3766(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisRectOffset_t666_m3766_MethodInfo_var);
		return;
	}
}
// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::get_childAlignment()
extern "C" int32_t LayoutGroup_get_childAlignment_m3164 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_ChildAlignment_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::set_childAlignment(UnityEngine.TextAnchor)
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
extern const MethodInfo* LayoutGroup_SetProperty_TisTextAnchor_t766_m3767_MethodInfo_var;
extern "C" void LayoutGroup_set_childAlignment_m3165 (LayoutGroup_t662 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutGroup_SetProperty_TisTextAnchor_t766_m3767_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484172);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = &(__this->___m_ChildAlignment_3);
		int32_t L_1 = ___value;
		LayoutGroup_SetProperty_TisTextAnchor_t766_m3767(__this, L_0, L_1, /*hidden argument*/LayoutGroup_SetProperty_TisTextAnchor_t766_m3767_MethodInfo_var);
		return;
	}
}
// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::get_rectTransform()
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisRectTransform_t556_m3505_MethodInfo_var;
extern "C" RectTransform_t556 * LayoutGroup_get_rectTransform_m3166 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRectTransform_t556_m3505_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484087);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t556 * L_0 = (__this->___m_Rect_4);
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t556 * L_2 = Component_GetComponent_TisRectTransform_t556_m3505(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t556_m3505_MethodInfo_var);
		__this->___m_Rect_4 = L_2;
	}

IL_001d:
	{
		RectTransform_t556 * L_3 = (__this->___m_Rect_4);
		return L_3;
	}
}
// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::get_rectChildren()
extern "C" List_1_t667 * LayoutGroup_get_rectChildren_m3167 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		List_1_t667 * L_0 = (__this->___m_RectChildren_9);
		return L_0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputHorizontal()
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTrackerMethodDeclarations.h"
extern const Il2CppType* ILayoutIgnorer_t767_0_0_0_var;
extern TypeInfo* RectTransform_t556_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ILayoutIgnorer_t767_il2cpp_TypeInfo_var;
extern "C" void LayoutGroup_CalculateLayoutInputHorizontal_m3168 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutIgnorer_t767_0_0_0_var = il2cpp_codegen_type_from_index(523);
		RectTransform_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		ILayoutIgnorer_t767_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(523);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t556 * V_1 = {0};
	Object_t * V_2 = {0};
	{
		List_1_t667 * L_0 = (__this->___m_RectChildren_9);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.RectTransform>::Clear() */, L_0);
		V_0 = 0;
		goto IL_007c;
	}

IL_0012:
	{
		RectTransform_t556 * L_1 = LayoutGroup_get_rectTransform_m3166(__this, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Transform_t35 * L_3 = Transform_GetChild_m3415(L_1, L_2, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t556 *)IsInstSealed(L_3, RectTransform_t556_il2cpp_TypeInfo_var));
		RectTransform_t556 * L_4 = V_1;
		bool L_5 = Object_op_Equality_m1413(NULL /*static, unused*/, L_4, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		goto IL_0078;
	}

IL_0035:
	{
		RectTransform_t556 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(ILayoutIgnorer_t767_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		Component_t365 * L_8 = Component_GetComponent_m3768(L_6, L_7, /*hidden argument*/NULL);
		V_2 = ((Object_t *)IsInst(L_8, ILayoutIgnorer_t767_il2cpp_TypeInfo_var));
		RectTransform_t556 * L_9 = V_1;
		NullCheck(L_9);
		GameObject_t155 * L_10 = Component_get_gameObject_m1337(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = GameObject_get_activeInHierarchy_m3317(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0078;
		}
	}
	{
		Object_t * L_12 = V_2;
		if (!L_12)
		{
			goto IL_006c;
		}
	}
	{
		Object_t * L_13 = V_2;
		NullCheck(L_13);
		bool L_14 = (bool)InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.UI.ILayoutIgnorer::get_ignoreLayout() */, ILayoutIgnorer_t767_il2cpp_TypeInfo_var, L_13);
		if (L_14)
		{
			goto IL_0078;
		}
	}

IL_006c:
	{
		List_1_t667 * L_15 = (__this->___m_RectChildren_9);
		RectTransform_t556 * L_16 = V_1;
		NullCheck(L_15);
		VirtActionInvoker1< RectTransform_t556 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.RectTransform>::Add(!0) */, L_15, L_16);
	}

IL_0078:
	{
		int32_t L_17 = V_0;
		V_0 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_007c:
	{
		int32_t L_18 = V_0;
		RectTransform_t556 * L_19 = LayoutGroup_get_rectTransform_m3166(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = Transform_get_childCount_m3416(L_19, /*hidden argument*/NULL);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_0012;
		}
	}
	{
		DrivenRectTransformTracker_t622 * L_21 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m3674(L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_minWidth()
extern "C" float LayoutGroup_get_minWidth_m3169 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalMinSize_m3179(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_preferredWidth()
extern "C" float LayoutGroup_get_preferredWidth_m3170 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalPreferredSize_m3180(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleWidth()
extern "C" float LayoutGroup_get_flexibleWidth_m3171 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalFlexibleSize_m3181(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_minHeight()
extern "C" float LayoutGroup_get_minHeight_m3172 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalMinSize_m3179(__this, 1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_preferredHeight()
extern "C" float LayoutGroup_get_preferredHeight_m3173 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalPreferredSize_m3180(__this, 1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleHeight()
extern "C" float LayoutGroup_get_flexibleHeight_m3174 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		float L_0 = LayoutGroup_GetTotalFlexibleSize_m3181(__this, 1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.UI.LayoutGroup::get_layoutPriority()
extern "C" int32_t LayoutGroup_get_layoutPriority_m3175 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnEnable()
extern "C" void LayoutGroup_OnEnable_m3176 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m2008(__this, /*hidden argument*/NULL);
		LayoutGroup_SetDirty_m3188(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnDisable()
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern "C" void LayoutGroup_OnDisable_m3177 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		s_Il2CppMethodIntialized = true;
	}
	{
		DrivenRectTransformTracker_t622 * L_0 = &(__this->___m_Tracker_5);
		DrivenRectTransformTracker_Clear_m3674(L_0, /*hidden argument*/NULL);
		RectTransform_t556 * L_1 = LayoutGroup_get_rectTransform_m3166(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m3199(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		UIBehaviour_OnDisable_m2010(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnDidApplyAnimationProperties()
extern "C" void LayoutGroup_OnDidApplyAnimationProperties_m3178 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_SetDirty_m3188(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetTotalMinSize(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" float LayoutGroup_GetTotalMinSize_m3179 (LayoutGroup_t662 * __this, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t2 * L_0 = &(__this->___m_TotalMinSize_6);
		int32_t L_1 = ___axis;
		float L_2 = Vector2_get_Item_m3571(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetTotalPreferredSize(System.Int32)
extern "C" float LayoutGroup_GetTotalPreferredSize_m3180 (LayoutGroup_t662 * __this, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t2 * L_0 = &(__this->___m_TotalPreferredSize_7);
		int32_t L_1 = ___axis;
		float L_2 = Vector2_get_Item_m3571(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetTotalFlexibleSize(System.Int32)
extern "C" float LayoutGroup_GetTotalFlexibleSize_m3181 (LayoutGroup_t662 * __this, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t2 * L_0 = &(__this->___m_TotalFlexibleSize_8);
		int32_t L_1 = ___axis;
		float L_2 = Vector2_get_Item_m3571(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.UI.LayoutGroup::GetStartOffset(System.Int32,System.Single)
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
extern "C" float LayoutGroup_GetStartOffset_m3182 (LayoutGroup_t662 * __this, int32_t ___axis, float ___requiredSpaceWithoutPadding, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Rect_t267  V_4 = {0};
	Vector2_t2  V_5 = {0};
	float G_B2_0 = 0.0f;
	float G_B1_0 = 0.0f;
	int32_t G_B3_0 = 0;
	float G_B3_1 = 0.0f;
	int32_t G_B9_0 = 0;
	{
		float L_0 = ___requiredSpaceWithoutPadding;
		int32_t L_1 = ___axis;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0017;
		}
	}
	{
		RectOffset_t666 * L_2 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = RectOffset_get_horizontal_m3758(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_0022;
	}

IL_0017:
	{
		RectOffset_t666 * L_4 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_vertical_m3759(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_0022:
	{
		V_0 = ((float)((float)G_B3_1+(float)(((float)G_B3_0))));
		RectTransform_t556 * L_6 = LayoutGroup_get_rectTransform_m3166(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Rect_t267  L_7 = RectTransform_get_rect_m3463(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		Vector2_t2  L_8 = Rect_get_size_m3466((&V_4), /*hidden argument*/NULL);
		V_5 = L_8;
		int32_t L_9 = ___axis;
		float L_10 = Vector2_get_Item_m3571((&V_5), L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = V_1;
		float L_12 = V_0;
		V_2 = ((float)((float)L_11-(float)L_12));
		V_3 = (0.0f);
		int32_t L_13 = ___axis;
		if (L_13)
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_14 = LayoutGroup_get_childAlignment_m3164(__this, /*hidden argument*/NULL);
		V_3 = ((float)((float)(((float)((int32_t)((int32_t)L_14%(int32_t)3))))*(float)(0.5f)));
		goto IL_0079;
	}

IL_0069:
	{
		int32_t L_15 = LayoutGroup_get_childAlignment_m3164(__this, /*hidden argument*/NULL);
		V_3 = ((float)((float)(((float)((int32_t)((int32_t)L_15/(int32_t)3))))*(float)(0.5f)));
	}

IL_0079:
	{
		int32_t L_16 = ___axis;
		if (L_16)
		{
			goto IL_008f;
		}
	}
	{
		RectOffset_t666 * L_17 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		int32_t L_18 = RectOffset_get_left_m3762(L_17, /*hidden argument*/NULL);
		G_B9_0 = L_18;
		goto IL_009a;
	}

IL_008f:
	{
		RectOffset_t666 * L_19 = LayoutGroup_get_padding_m3162(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_top_m3763(L_19, /*hidden argument*/NULL);
		G_B9_0 = L_20;
	}

IL_009a:
	{
		float L_21 = V_2;
		float L_22 = V_3;
		return ((float)((float)(((float)G_B9_0))+(float)((float)((float)L_21*(float)L_22))));
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutInputForAxis(System.Single,System.Single,System.Single,System.Int32)
extern "C" void LayoutGroup_SetLayoutInputForAxis_m3183 (LayoutGroup_t662 * __this, float ___totalMin, float ___totalPreferred, float ___totalFlexible, int32_t ___axis, const MethodInfo* method)
{
	{
		Vector2_t2 * L_0 = &(__this->___m_TotalMinSize_6);
		int32_t L_1 = ___axis;
		float L_2 = ___totalMin;
		Vector2_set_Item_m3577(L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t2 * L_3 = &(__this->___m_TotalPreferredSize_7);
		int32_t L_4 = ___axis;
		float L_5 = ___totalPreferred;
		Vector2_set_Item_m3577(L_3, L_4, L_5, /*hidden argument*/NULL);
		Vector2_t2 * L_6 = &(__this->___m_TotalFlexibleSize_8);
		int32_t L_7 = ___axis;
		float L_8 = ___totalFlexible;
		Vector2_set_Item_m3577(L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetChildAlongAxis(UnityEngine.RectTransform,System.Int32,System.Single,System.Single)
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
extern "C" void LayoutGroup_SetChildAlongAxis_m3184 (LayoutGroup_t662 * __this, RectTransform_t556 * ___rect, int32_t ___axis, float ___pos, float ___size, const MethodInfo* method)
{
	RectTransform_t556 * G_B4_0 = {0};
	RectTransform_t556 * G_B3_0 = {0};
	int32_t G_B5_0 = 0;
	RectTransform_t556 * G_B5_1 = {0};
	{
		RectTransform_t556 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		DrivenRectTransformTracker_t622 * L_2 = &(__this->___m_Tracker_5);
		RectTransform_t556 * L_3 = ___rect;
		DrivenRectTransformTracker_Add_m3675(L_2, __this, L_3, ((int32_t)16134), /*hidden argument*/NULL);
		RectTransform_t556 * L_4 = ___rect;
		int32_t L_5 = ___axis;
		G_B3_0 = L_4;
		if (L_5)
		{
			G_B4_0 = L_4;
			goto IL_002c;
		}
	}
	{
		G_B5_0 = 0;
		G_B5_1 = G_B3_0;
		goto IL_002d;
	}

IL_002c:
	{
		G_B5_0 = 2;
		G_B5_1 = G_B4_0;
	}

IL_002d:
	{
		float L_6 = ___pos;
		float L_7 = ___size;
		NullCheck(G_B5_1);
		RectTransform_SetInsetAndSizeFromParentEdge_m3769(G_B5_1, G_B5_0, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutGroup::get_isRootLayoutGroup()
extern const Il2CppType* ILayoutGroup_t768_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" bool LayoutGroup_get_isRootLayoutGroup_m3185 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutGroup_t768_0_0_0_var = il2cpp_codegen_type_from_index(524);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t35 * V_0 = {0};
	{
		Transform_t35 * L_0 = Component_get_transform_m1417(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t35 * L_1 = Transform_get_parent_m1342(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t35 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m1413(NULL /*static, unused*/, L_2, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		return 1;
	}

IL_001a:
	{
		Transform_t35 * L_4 = Component_get_transform_m1417(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t35 * L_5 = Transform_get_parent_m1342(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(ILayoutGroup_t768_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		Component_t365 * L_7 = Component_GetComponent_m3768(L_5, L_6, /*hidden argument*/NULL);
		bool L_8 = Object_op_Equality_m1413(NULL /*static, unused*/, L_7, (Object_t335 *)NULL, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnRectTransformDimensionsChange()
extern "C" void LayoutGroup_OnRectTransformDimensionsChange_m3186 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnRectTransformDimensionsChange_m2013(__this, /*hidden argument*/NULL);
		bool L_0 = LayoutGroup_get_isRootLayoutGroup_m3185(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		LayoutGroup_SetDirty_m3188(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::OnTransformChildrenChanged()
extern "C" void LayoutGroup_OnTransformChildrenChanged_m3187 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_SetDirty_m3188(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutGroup::SetDirty()
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern "C" void LayoutGroup_SetDirty_m3188 (LayoutGroup_t662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		RectTransform_t556 * L_1 = LayoutGroup_get_rectTransform_m3166(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m3199(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertie.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3.h"
// System.Collections.Generic.List`1<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_gen_27.h"
// System.Predicate`1<UnityEngine.Component>
#include "mscorlib_System_Predicate_1_gen_1.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertieMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3MethodDeclarations.h"
// System.Predicate`1<UnityEngine.Component>
#include "mscorlib_System_Predicate_1_gen_1MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_gen_27MethodDeclarations.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Component>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_0MethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// System.Void UnityEngine.UI.LayoutRebuilder::.ctor(UnityEngine.RectTransform)
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
extern "C" void LayoutRebuilder__ctor_m3189 (LayoutRebuilder_t668 * __this, RectTransform_t556 * ___controller, const MethodInfo* method)
{
	{
		RectTransform_t556 * L_0 = ___controller;
		__this->___m_ToRebuild_0 = L_0;
		RectTransform_t556 * L_1 = (__this->___m_ToRebuild_0);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 UnityEngine.Object::GetHashCode() */, L_1);
		__this->___m_CachedHashFromTransform_1 = L_2;
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::.cctor()
// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertieMethodDeclarations.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
extern TypeInfo* ReapplyDrivenProperties_t769_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutRebuilder_ReapplyDrivenProperties_m3192_MethodInfo_var;
extern "C" void LayoutRebuilder__cctor_m3190 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReapplyDrivenProperties_t769_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(525);
		LayoutRebuilder_ReapplyDrivenProperties_m3192_MethodInfo_var = il2cpp_codegen_method_info_from_index(525);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { (void*)LayoutRebuilder_ReapplyDrivenProperties_m3192_MethodInfo_var };
		ReapplyDrivenProperties_t769 * L_1 = (ReapplyDrivenProperties_t769 *)il2cpp_codegen_object_new (ReapplyDrivenProperties_t769_il2cpp_TypeInfo_var);
		ReapplyDrivenProperties__ctor_m3771(L_1, NULL, L_0, /*hidden argument*/NULL);
		RectTransform_add_reapplyDrivenProperties_m3772(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::UnityEngine.UI.ICanvasElement.Rebuild(UnityEngine.UI.CanvasUpdate)
// UnityEngine.UI.CanvasUpdate
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3MethodDeclarations.h"
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAction_1_t669_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__8_m3206_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3773_MethodInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__9_m3207_MethodInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__A_m3208_MethodInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CRebuildU3Em__B_m3209_MethodInfo_var;
extern "C" void LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m3191 (LayoutRebuilder_t668 * __this, int32_t ___executing, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		UnityAction_1_t669_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(526);
		LayoutRebuilder_U3CRebuildU3Em__8_m3206_MethodInfo_var = il2cpp_codegen_method_info_from_index(526);
		UnityAction_1__ctor_m3773_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484175);
		LayoutRebuilder_U3CRebuildU3Em__9_m3207_MethodInfo_var = il2cpp_codegen_method_info_from_index(528);
		LayoutRebuilder_U3CRebuildU3Em__A_m3208_MethodInfo_var = il2cpp_codegen_method_info_from_index(529);
		LayoutRebuilder_U3CRebuildU3Em__B_m3209_MethodInfo_var = il2cpp_codegen_method_info_from_index(530);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	RectTransform_t556 * G_B4_0 = {0};
	LayoutRebuilder_t668 * G_B4_1 = {0};
	RectTransform_t556 * G_B3_0 = {0};
	LayoutRebuilder_t668 * G_B3_1 = {0};
	RectTransform_t556 * G_B6_0 = {0};
	LayoutRebuilder_t668 * G_B6_1 = {0};
	RectTransform_t556 * G_B5_0 = {0};
	LayoutRebuilder_t668 * G_B5_1 = {0};
	RectTransform_t556 * G_B8_0 = {0};
	LayoutRebuilder_t668 * G_B8_1 = {0};
	RectTransform_t556 * G_B7_0 = {0};
	LayoutRebuilder_t668 * G_B7_1 = {0};
	RectTransform_t556 * G_B10_0 = {0};
	LayoutRebuilder_t668 * G_B10_1 = {0};
	RectTransform_t556 * G_B9_0 = {0};
	LayoutRebuilder_t668 * G_B9_1 = {0};
	{
		int32_t L_0 = ___executing;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		goto IL_00b7;
	}

IL_000e:
	{
		RectTransform_t556 * L_2 = (__this->___m_ToRebuild_0);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		UnityAction_1_t669 * L_3 = ((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2;
		G_B3_0 = L_2;
		G_B3_1 = __this;
		if (L_3)
		{
			G_B4_0 = L_2;
			G_B4_1 = __this;
			goto IL_002d;
		}
	}
	{
		IntPtr_t L_4 = { (void*)LayoutRebuilder_U3CRebuildU3Em__8_m3206_MethodInfo_var };
		UnityAction_1_t669 * L_5 = (UnityAction_1_t669 *)il2cpp_codegen_object_new (UnityAction_1_t669_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3773(L_5, NULL, L_4, /*hidden argument*/UnityAction_1__ctor_m3773_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2 = L_5;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		UnityAction_1_t669 * L_6 = ((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2;
		LayoutRebuilder_PerformLayoutCalculation_m3198(G_B4_1, G_B4_0, L_6, /*hidden argument*/NULL);
		RectTransform_t556 * L_7 = (__this->___m_ToRebuild_0);
		UnityAction_1_t669 * L_8 = ((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		G_B5_0 = L_7;
		G_B5_1 = __this;
		if (L_8)
		{
			G_B6_0 = L_7;
			G_B6_1 = __this;
			goto IL_0056;
		}
	}
	{
		IntPtr_t L_9 = { (void*)LayoutRebuilder_U3CRebuildU3Em__9_m3207_MethodInfo_var };
		UnityAction_1_t669 * L_10 = (UnityAction_1_t669 *)il2cpp_codegen_object_new (UnityAction_1_t669_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3773(L_10, NULL, L_9, /*hidden argument*/UnityAction_1__ctor_m3773_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3 = L_10;
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		UnityAction_1_t669 * L_11 = ((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		LayoutRebuilder_PerformLayoutControl_m3197(G_B6_1, G_B6_0, L_11, /*hidden argument*/NULL);
		RectTransform_t556 * L_12 = (__this->___m_ToRebuild_0);
		UnityAction_1_t669 * L_13 = ((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		G_B7_0 = L_12;
		G_B7_1 = __this;
		if (L_13)
		{
			G_B8_0 = L_12;
			G_B8_1 = __this;
			goto IL_007f;
		}
	}
	{
		IntPtr_t L_14 = { (void*)LayoutRebuilder_U3CRebuildU3Em__A_m3208_MethodInfo_var };
		UnityAction_1_t669 * L_15 = (UnityAction_1_t669 *)il2cpp_codegen_object_new (UnityAction_1_t669_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3773(L_15, NULL, L_14, /*hidden argument*/UnityAction_1__ctor_m3773_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4 = L_15;
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
	}

IL_007f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		UnityAction_1_t669 * L_16 = ((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		LayoutRebuilder_PerformLayoutCalculation_m3198(G_B8_1, G_B8_0, L_16, /*hidden argument*/NULL);
		RectTransform_t556 * L_17 = (__this->___m_ToRebuild_0);
		UnityAction_1_t669 * L_18 = ((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		G_B9_0 = L_17;
		G_B9_1 = __this;
		if (L_18)
		{
			G_B10_0 = L_17;
			G_B10_1 = __this;
			goto IL_00a8;
		}
	}
	{
		IntPtr_t L_19 = { (void*)LayoutRebuilder_U3CRebuildU3Em__B_m3209_MethodInfo_var };
		UnityAction_1_t669 * L_20 = (UnityAction_1_t669 *)il2cpp_codegen_object_new (UnityAction_1_t669_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3773(L_20, NULL, L_19, /*hidden argument*/UnityAction_1__ctor_m3773_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5 = L_20;
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		UnityAction_1_t669 * L_21 = ((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		LayoutRebuilder_PerformLayoutControl_m3197(G_B10_1, G_B10_0, L_21, /*hidden argument*/NULL);
		goto IL_00b7;
	}

IL_00b7:
	{
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::ReapplyDrivenProperties(UnityEngine.RectTransform)
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_ReapplyDrivenProperties_m3192 (Object_t * __this /* static, unused */, RectTransform_t556 * ___driven, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t556 * L_0 = ___driven;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m3199(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.UI.LayoutRebuilder::get_transform()
extern "C" Transform_t35 * LayoutRebuilder_get_transform_m3193 (LayoutRebuilder_t668 * __this, const MethodInfo* method)
{
	{
		RectTransform_t556 * L_0 = (__this->___m_ToRebuild_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::IsDestroyed()
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern "C" bool LayoutRebuilder_IsDestroyed_m3194 (LayoutRebuilder_t668 * __this, const MethodInfo* method)
{
	{
		RectTransform_t556 * L_0 = (__this->___m_ToRebuild_0);
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::StripDisabledBehavioursFromList(System.Collections.Generic.List`1<UnityEngine.Component>)
// System.Collections.Generic.List`1<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_gen_27.h"
// System.Predicate`1<UnityEngine.Component>
#include "mscorlib_System_Predicate_1_gen_1MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_gen_27MethodDeclarations.h"
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t670_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__C_m3210_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m3774_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAll_m3775_MethodInfo_var;
extern "C" void LayoutRebuilder_StripDisabledBehavioursFromList_m3195 (Object_t * __this /* static, unused */, List_1_t718 * ___components, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		Predicate_1_t670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(527);
		LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__C_m3210_MethodInfo_var = il2cpp_codegen_method_info_from_index(531);
		Predicate_1__ctor_m3774_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484180);
		List_1_RemoveAll_m3775_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484181);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t718 * G_B2_0 = {0};
	List_1_t718 * G_B1_0 = {0};
	{
		List_1_t718 * L_0 = ___components;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		Predicate_1_t670 * L_1 = ((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__C_m3210_MethodInfo_var };
		Predicate_1_t670 * L_3 = (Predicate_1_t670 *)il2cpp_codegen_object_new (Predicate_1_t670_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m3774(L_3, NULL, L_2, /*hidden argument*/Predicate_1__ctor_m3774_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		Predicate_1_t670 * L_4 = ((LayoutRebuilder_t668_StaticFields*)LayoutRebuilder_t668_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		NullCheck(G_B2_0);
		List_1_RemoveAll_m3775(G_B2_0, L_4, /*hidden argument*/List_1_RemoveAll_m3775_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::ForceRebuildLayoutImmediate(UnityEngine.RectTransform)
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern TypeInfo* ICanvasElement_t708_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_ForceRebuildLayoutImmediate_m3196 (Object_t * __this /* static, unused */, RectTransform_t556 * ___layoutRoot, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		ICanvasElement_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(387);
		s_Il2CppMethodIntialized = true;
	}
	LayoutRebuilder_t668  V_0 = {0};
	{
		RectTransform_t556 * L_0 = ___layoutRoot;
		LayoutRebuilder__ctor_m3189((&V_0), L_0, /*hidden argument*/NULL);
		LayoutRebuilder_t668  L_1 = V_0;
		LayoutRebuilder_t668  L_2 = L_1;
		Object_t * L_3 = Box(LayoutRebuilder_t668_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_3);
		InterfaceActionInvoker1< int32_t >::Invoke(0 /* System.Void UnityEngine.UI.ICanvasElement::Rebuild(UnityEngine.UI.CanvasUpdate) */, ICanvasElement_t708_il2cpp_TypeInfo_var, L_3, 1);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutControl(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Component>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_0MethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern const Il2CppType* ILayoutController_t770_0_0_0_var;
extern TypeInfo* ListPool_1_t740_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern TypeInfo* ILayoutSelfController_t771_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransform_t556_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m3507_MethodInfo_var;
extern const MethodInfo* UnityAction_1_Invoke_m3776_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m3509_MethodInfo_var;
extern "C" void LayoutRebuilder_PerformLayoutControl_m3197 (LayoutRebuilder_t668 * __this, RectTransform_t556 * ___rect, UnityAction_1_t669 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t770_0_0_0_var = il2cpp_codegen_type_from_index(528);
		ListPool_1_t740_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(438);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		ILayoutSelfController_t771_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(529);
		RectTransform_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		ListPool_1_Get_m3507_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484089);
		UnityAction_1_Invoke_m3776_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484182);
		ListPool_1_Release_m3509_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484090);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t718 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		RectTransform_t556 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t740_il2cpp_TypeInfo_var);
		List_1_t718 * L_2 = ListPool_1_Get_m3507(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3507_MethodInfo_var);
		V_0 = L_2;
		RectTransform_t556 * L_3 = ___rect;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(ILayoutController_t770_0_0_0_var), /*hidden argument*/NULL);
		List_1_t718 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m3508(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t718 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m3195(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t718 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_00ca;
		}
	}
	{
		V_1 = 0;
		goto IL_005f;
	}

IL_003d:
	{
		List_1_t718 * L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Component_t365 * L_11 = (Component_t365 *)VirtFuncInvoker1< Component_t365 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_9, L_10);
		if (!((Object_t *)IsInst(L_11, ILayoutSelfController_t771_il2cpp_TypeInfo_var)))
		{
			goto IL_005b;
		}
	}
	{
		UnityAction_1_t669 * L_12 = ___action;
		List_1_t718 * L_13 = V_0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		Component_t365 * L_15 = (Component_t365 *)VirtFuncInvoker1< Component_t365 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_13, L_14);
		NullCheck(L_12);
		UnityAction_1_Invoke_m3776(L_12, L_15, /*hidden argument*/UnityAction_1_Invoke_m3776_MethodInfo_var);
	}

IL_005b:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005f:
	{
		int32_t L_17 = V_1;
		List_1_t718 * L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_18);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_003d;
		}
	}
	{
		V_2 = 0;
		goto IL_0094;
	}

IL_0072:
	{
		List_1_t718 * L_20 = V_0;
		int32_t L_21 = V_2;
		NullCheck(L_20);
		Component_t365 * L_22 = (Component_t365 *)VirtFuncInvoker1< Component_t365 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_20, L_21);
		if (((Object_t *)IsInst(L_22, ILayoutSelfController_t771_il2cpp_TypeInfo_var)))
		{
			goto IL_0090;
		}
	}
	{
		UnityAction_1_t669 * L_23 = ___action;
		List_1_t718 * L_24 = V_0;
		int32_t L_25 = V_2;
		NullCheck(L_24);
		Component_t365 * L_26 = (Component_t365 *)VirtFuncInvoker1< Component_t365 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_24, L_25);
		NullCheck(L_23);
		UnityAction_1_Invoke_m3776(L_23, L_26, /*hidden argument*/UnityAction_1_Invoke_m3776_MethodInfo_var);
	}

IL_0090:
	{
		int32_t L_27 = V_2;
		V_2 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0094:
	{
		int32_t L_28 = V_2;
		List_1_t718 * L_29 = V_0;
		NullCheck(L_29);
		int32_t L_30 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_29);
		if ((((int32_t)L_28) < ((int32_t)L_30)))
		{
			goto IL_0072;
		}
	}
	{
		V_3 = 0;
		goto IL_00be;
	}

IL_00a7:
	{
		RectTransform_t556 * L_31 = ___rect;
		int32_t L_32 = V_3;
		NullCheck(L_31);
		Transform_t35 * L_33 = Transform_GetChild_m3415(L_31, L_32, /*hidden argument*/NULL);
		UnityAction_1_t669 * L_34 = ___action;
		LayoutRebuilder_PerformLayoutControl_m3197(__this, ((RectTransform_t556 *)IsInstSealed(L_33, RectTransform_t556_il2cpp_TypeInfo_var)), L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_3;
		V_3 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00be:
	{
		int32_t L_36 = V_3;
		RectTransform_t556 * L_37 = ___rect;
		NullCheck(L_37);
		int32_t L_38 = Transform_get_childCount_m3416(L_37, /*hidden argument*/NULL);
		if ((((int32_t)L_36) < ((int32_t)L_38)))
		{
			goto IL_00a7;
		}
	}

IL_00ca:
	{
		List_1_t718 * L_39 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t740_il2cpp_TypeInfo_var);
		ListPool_1_Release_m3509(NULL /*static, unused*/, L_39, /*hidden argument*/ListPool_1_Release_m3509_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutCalculation(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const Il2CppType* ILayoutElement_t719_0_0_0_var;
extern TypeInfo* ListPool_1_t740_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransform_t556_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m3507_MethodInfo_var;
extern const MethodInfo* UnityAction_1_Invoke_m3776_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m3509_MethodInfo_var;
extern "C" void LayoutRebuilder_PerformLayoutCalculation_m3198 (LayoutRebuilder_t668 * __this, RectTransform_t556 * ___rect, UnityAction_1_t669 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_0_0_0_var = il2cpp_codegen_type_from_index(530);
		ListPool_1_t740_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(438);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		RectTransform_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		ListPool_1_Get_m3507_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484089);
		UnityAction_1_Invoke_m3776_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484182);
		ListPool_1_Release_m3509_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484090);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t718 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		RectTransform_t556 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t740_il2cpp_TypeInfo_var);
		List_1_t718 * L_2 = ListPool_1_Get_m3507(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3507_MethodInfo_var);
		V_0 = L_2;
		RectTransform_t556 * L_3 = ___rect;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(ILayoutElement_t719_0_0_0_var), /*hidden argument*/NULL);
		List_1_t718 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m3508(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t718 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m3195(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t718 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0084;
		}
	}
	{
		V_1 = 0;
		goto IL_0054;
	}

IL_003d:
	{
		RectTransform_t556 * L_9 = ___rect;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Transform_t35 * L_11 = Transform_GetChild_m3415(L_9, L_10, /*hidden argument*/NULL);
		UnityAction_1_t669 * L_12 = ___action;
		LayoutRebuilder_PerformLayoutCalculation_m3198(__this, ((RectTransform_t556 *)IsInstSealed(L_11, RectTransform_t556_il2cpp_TypeInfo_var)), L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_14 = V_1;
		RectTransform_t556 * L_15 = ___rect;
		NullCheck(L_15);
		int32_t L_16 = Transform_get_childCount_m3416(L_15, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_003d;
		}
	}
	{
		V_2 = 0;
		goto IL_0078;
	}

IL_0067:
	{
		UnityAction_1_t669 * L_17 = ___action;
		List_1_t718 * L_18 = V_0;
		int32_t L_19 = V_2;
		NullCheck(L_18);
		Component_t365 * L_20 = (Component_t365 *)VirtFuncInvoker1< Component_t365 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_18, L_19);
		NullCheck(L_17);
		UnityAction_1_Invoke_m3776(L_17, L_20, /*hidden argument*/UnityAction_1_Invoke_m3776_MethodInfo_var);
		int32_t L_21 = V_2;
		V_2 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_22 = V_2;
		List_1_t718 * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_23);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_0067;
		}
	}

IL_0084:
	{
		List_1_t718 * L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t740_il2cpp_TypeInfo_var);
		ListPool_1_Release_m3509(NULL /*static, unused*/, L_25, /*hidden argument*/ListPool_1_Release_m3509_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutForRebuild(UnityEngine.RectTransform)
extern TypeInfo* RectTransform_t556_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_MarkLayoutForRebuild_m3199 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t556 * V_0 = {0};
	RectTransform_t556 * V_1 = {0};
	{
		RectTransform_t556 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		RectTransform_t556 * L_2 = ___rect;
		V_0 = L_2;
	}

IL_000f:
	{
		RectTransform_t556 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t35 * L_4 = Transform_get_parent_m1342(L_3, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t556 *)IsInstSealed(L_4, RectTransform_t556_il2cpp_TypeInfo_var));
		RectTransform_t556 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		bool L_6 = LayoutRebuilder_ValidLayoutGroup_m3200(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_0032;
	}

IL_002b:
	{
		RectTransform_t556 * L_7 = V_1;
		V_0 = L_7;
		goto IL_000f;
	}

IL_0032:
	{
		RectTransform_t556 * L_8 = V_0;
		RectTransform_t556 * L_9 = ___rect;
		bool L_10 = Object_op_Equality_m1413(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004a;
		}
	}
	{
		RectTransform_t556 * L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		bool L_12 = LayoutRebuilder_ValidController_m3201(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_004a;
		}
	}
	{
		return;
	}

IL_004a:
	{
		RectTransform_t556 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutRootForRebuild_m3202(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidLayoutGroup(UnityEngine.RectTransform)
extern const Il2CppType* ILayoutGroup_t768_0_0_0_var;
extern TypeInfo* ListPool_1_t740_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m3507_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m3509_MethodInfo_var;
extern "C" bool LayoutRebuilder_ValidLayoutGroup_m3200 (Object_t * __this /* static, unused */, RectTransform_t556 * ___parent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutGroup_t768_0_0_0_var = il2cpp_codegen_type_from_index(524);
		ListPool_1_t740_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(438);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		ListPool_1_Get_m3507_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484089);
		ListPool_1_Release_m3509_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484090);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t718 * V_0 = {0};
	bool V_1 = false;
	{
		RectTransform_t556 * L_0 = ___parent;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t740_il2cpp_TypeInfo_var);
		List_1_t718 * L_2 = ListPool_1_Get_m3507(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3507_MethodInfo_var);
		V_0 = L_2;
		RectTransform_t556 * L_3 = ___parent;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(ILayoutGroup_t768_0_0_0_var), /*hidden argument*/NULL);
		List_1_t718 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m3508(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t718 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m3195(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t718 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		V_1 = ((((int32_t)L_8) > ((int32_t)0))? 1 : 0);
		List_1_t718 * L_9 = V_0;
		ListPool_1_Release_m3509(NULL /*static, unused*/, L_9, /*hidden argument*/ListPool_1_Release_m3509_MethodInfo_var);
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidController(UnityEngine.RectTransform)
extern const Il2CppType* ILayoutController_t770_0_0_0_var;
extern TypeInfo* ListPool_1_t740_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m3507_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m3509_MethodInfo_var;
extern "C" bool LayoutRebuilder_ValidController_m3201 (Object_t * __this /* static, unused */, RectTransform_t556 * ___layoutRoot, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t770_0_0_0_var = il2cpp_codegen_type_from_index(528);
		ListPool_1_t740_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(438);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		ListPool_1_Get_m3507_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484089);
		ListPool_1_Release_m3509_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484090);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t718 * V_0 = {0};
	bool V_1 = false;
	{
		RectTransform_t556 * L_0 = ___layoutRoot;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t740_il2cpp_TypeInfo_var);
		List_1_t718 * L_2 = ListPool_1_Get_m3507(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3507_MethodInfo_var);
		V_0 = L_2;
		RectTransform_t556 * L_3 = ___layoutRoot;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(ILayoutController_t770_0_0_0_var), /*hidden argument*/NULL);
		List_1_t718 * L_5 = V_0;
		NullCheck(L_3);
		Component_GetComponents_m3508(L_3, L_4, L_5, /*hidden argument*/NULL);
		List_1_t718 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_t668_il2cpp_TypeInfo_var);
		LayoutRebuilder_StripDisabledBehavioursFromList_m3195(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t718 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_7);
		V_1 = ((((int32_t)L_8) > ((int32_t)0))? 1 : 0);
		List_1_t718 * L_9 = V_0;
		ListPool_1_Release_m3509(NULL /*static, unused*/, L_9, /*hidden argument*/ListPool_1_Release_m3509_MethodInfo_var);
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutRootForRebuild(UnityEngine.RectTransform)
// UnityEngine.UI.CanvasUpdateRegistry
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistryMethodDeclarations.h"
extern TypeInfo* LayoutRebuilder_t668_il2cpp_TypeInfo_var;
extern TypeInfo* CanvasUpdateRegistry_t547_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_MarkLayoutRootForRebuild_m3202 (Object_t * __this /* static, unused */, RectTransform_t556 * ___controller, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutRebuilder_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		CanvasUpdateRegistry_t547_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(391);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t556 * L_0 = ___controller;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		RectTransform_t556 * L_2 = ___controller;
		LayoutRebuilder_t668  L_3 = {0};
		LayoutRebuilder__ctor_m3189(&L_3, L_2, /*hidden argument*/NULL);
		LayoutRebuilder_t668  L_4 = L_3;
		Object_t * L_5 = Box(LayoutRebuilder_t668_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(CanvasUpdateRegistry_t547_il2cpp_TypeInfo_var);
		CanvasUpdateRegistry_RegisterCanvasElementForLayoutRebuild_m2247(NULL /*static, unused*/, (Object_t *)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::Equals(UnityEngine.UI.LayoutRebuilder)
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
extern "C" bool LayoutRebuilder_Equals_m3203 (LayoutRebuilder_t668 * __this, LayoutRebuilder_t668  ___other, const MethodInfo* method)
{
	{
		RectTransform_t556 * L_0 = (__this->___m_ToRebuild_0);
		RectTransform_t556 * L_1 = ((&___other)->___m_ToRebuild_0);
		bool L_2 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.UI.LayoutRebuilder::GetHashCode()
extern "C" int32_t LayoutRebuilder_GetHashCode_m3204 (LayoutRebuilder_t668 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_CachedHashFromTransform_1);
		return L_0;
	}
}
// System.String UnityEngine.UI.LayoutRebuilder::ToString()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral500;
extern "C" String_t* LayoutRebuilder_ToString_m3205 (LayoutRebuilder_t668 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral500 = il2cpp_codegen_string_literal_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t556 * L_0 = (__this->___m_ToRebuild_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1410(NULL /*static, unused*/, _stringLiteral500, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__8(UnityEngine.Component)
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
extern TypeInfo* ILayoutElement_t719_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__8_m3206 (Object_t * __this /* static, unused */, Component_t365 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(530);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t365 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutElement_t719_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputHorizontal() */, ILayoutElement_t719_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutElement_t719_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__9(UnityEngine.Component)
extern TypeInfo* ILayoutController_t770_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__9_m3207 (Object_t * __this /* static, unused */, Component_t365 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t770_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(528);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t365 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutController_t770_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.UI.ILayoutController::SetLayoutHorizontal() */, ILayoutController_t770_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutController_t770_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__A(UnityEngine.Component)
extern TypeInfo* ILayoutElement_t719_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__A_m3208 (Object_t * __this /* static, unused */, Component_t365 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(530);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t365 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutElement_t719_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(1 /* System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputVertical() */, ILayoutElement_t719_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutElement_t719_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__B(UnityEngine.Component)
extern TypeInfo* ILayoutController_t770_il2cpp_TypeInfo_var;
extern "C" void LayoutRebuilder_U3CRebuildU3Em__B_m3209 (Object_t * __this /* static, unused */, Component_t365 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutController_t770_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(528);
		s_Il2CppMethodIntialized = true;
	}
	{
		Component_t365 * L_0 = ___e;
		NullCheck(((Object_t *)IsInst(L_0, ILayoutController_t770_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker0::Invoke(1 /* System.Void UnityEngine.UI.ILayoutController::SetLayoutVertical() */, ILayoutController_t770_il2cpp_TypeInfo_var, ((Object_t *)IsInst(L_0, ILayoutController_t770_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Boolean UnityEngine.UI.LayoutRebuilder::<StripDisabledBehavioursFromList>m__C(UnityEngine.Component)
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
extern TypeInfo* Behaviour_t772_il2cpp_TypeInfo_var;
extern "C" bool LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__C_m3210 (Object_t * __this /* static, unused */, Component_t365 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Behaviour_t772_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(531);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Component_t365 * L_0 = ___e;
		if (!((Behaviour_t772 *)IsInstClass(L_0, Behaviour_t772_il2cpp_TypeInfo_var)))
		{
			goto IL_001b;
		}
	}
	{
		Component_t365 * L_1 = ___e;
		NullCheck(((Behaviour_t772 *)CastclassClass(L_1, Behaviour_t772_il2cpp_TypeInfo_var)));
		bool L_2 = Behaviour_get_isActiveAndEnabled_m3310(((Behaviour_t772 *)CastclassClass(L_1, Behaviour_t772_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility.h"
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
#include "System_Core_System_Func_2_gen_3.h"
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
#include "System_Core_System_Func_2_gen_3MethodDeclarations.h"
// System.Single UnityEngine.UI.LayoutUtility::GetMinSize(UnityEngine.RectTransform,System.Int32)
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
extern "C" float LayoutUtility_GetMinSize_m3211 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, int32_t ___axis, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		RectTransform_t556 * L_1 = ___rect;
		float L_2 = LayoutUtility_GetMinWidth_m3214(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000d:
	{
		RectTransform_t556 * L_3 = ___rect;
		float L_4 = LayoutUtility_GetMinHeight_m3217(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredSize(UnityEngine.RectTransform,System.Int32)
extern "C" float LayoutUtility_GetPreferredSize_m3212 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, int32_t ___axis, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		RectTransform_t556 * L_1 = ___rect;
		float L_2 = LayoutUtility_GetPreferredWidth_m3215(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000d:
	{
		RectTransform_t556 * L_3 = ___rect;
		float L_4 = LayoutUtility_GetPreferredHeight_m3218(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleSize(UnityEngine.RectTransform,System.Int32)
extern "C" float LayoutUtility_GetFlexibleSize_m3213 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, int32_t ___axis, const MethodInfo* method)
{
	{
		int32_t L_0 = ___axis;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		RectTransform_t556 * L_1 = ___rect;
		float L_2 = LayoutUtility_GetFlexibleWidth_m3216(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_000d:
	{
		RectTransform_t556 * L_3 = ___rect;
		float L_4 = LayoutUtility_GetFlexibleHeight_m3219(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetMinWidth(UnityEngine.RectTransform)
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
#include "System_Core_System_Func_2_gen_3MethodDeclarations.h"
extern TypeInfo* LayoutUtility_t671_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t672_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetMinWidthU3Em__D_m3222_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3777_MethodInfo_var;
extern "C" float LayoutUtility_GetMinWidth_m3214 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t671_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(532);
		Func_2_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(533);
		LayoutUtility_U3CGetMinWidthU3Em__D_m3222_MethodInfo_var = il2cpp_codegen_method_info_from_index(535);
		Func_2__ctor_m3777_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484184);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t556 * G_B2_0 = {0};
	RectTransform_t556 * G_B1_0 = {0};
	{
		RectTransform_t556 * L_0 = ___rect;
		Func_2_t672 * L_1 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_0;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetMinWidthU3Em__D_m3222_MethodInfo_var };
		Func_2_t672 * L_3 = (Func_2_t672 *)il2cpp_codegen_object_new (Func_2_t672_il2cpp_TypeInfo_var);
		Func_2__ctor_m3777(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3777_MethodInfo_var);
		((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_0 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t672 * L_4 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache0_0;
		float L_5 = LayoutUtility_GetLayoutProperty_m3220(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredWidth(UnityEngine.RectTransform)
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* LayoutUtility_t671_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t672_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredWidthU3Em__E_m3223_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3777_MethodInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredWidthU3Em__F_m3224_MethodInfo_var;
extern "C" float LayoutUtility_GetPreferredWidth_m3215 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t671_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(532);
		Func_2_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(533);
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		LayoutUtility_U3CGetPreferredWidthU3Em__E_m3223_MethodInfo_var = il2cpp_codegen_method_info_from_index(537);
		Func_2__ctor_m3777_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484184);
		LayoutUtility_U3CGetPreferredWidthU3Em__F_m3224_MethodInfo_var = il2cpp_codegen_method_info_from_index(538);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t556 * G_B2_0 = {0};
	RectTransform_t556 * G_B1_0 = {0};
	RectTransform_t556 * G_B4_0 = {0};
	float G_B4_1 = 0.0f;
	RectTransform_t556 * G_B3_0 = {0};
	float G_B3_1 = 0.0f;
	{
		RectTransform_t556 * L_0 = ___rect;
		Func_2_t672 * L_1 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetPreferredWidthU3Em__E_m3223_MethodInfo_var };
		Func_2_t672 * L_3 = (Func_2_t672 *)il2cpp_codegen_object_new (Func_2_t672_il2cpp_TypeInfo_var);
		Func_2__ctor_m3777(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3777_MethodInfo_var);
		((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t672 * L_4 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		float L_5 = LayoutUtility_GetLayoutProperty_m3220(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		RectTransform_t556 * L_6 = ___rect;
		Func_2_t672 * L_7 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2;
		G_B3_0 = L_6;
		G_B3_1 = L_5;
		if (L_7)
		{
			G_B4_0 = L_6;
			G_B4_1 = L_5;
			goto IL_0041;
		}
	}
	{
		IntPtr_t L_8 = { (void*)LayoutUtility_U3CGetPreferredWidthU3Em__F_m3224_MethodInfo_var };
		Func_2_t672 * L_9 = (Func_2_t672 *)il2cpp_codegen_object_new (Func_2_t672_il2cpp_TypeInfo_var);
		Func_2__ctor_m3777(L_9, NULL, L_8, /*hidden argument*/Func_2__ctor_m3777_MethodInfo_var);
		((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2 = L_9;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0041:
	{
		Func_2_t672 * L_10 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_2;
		float L_11 = LayoutUtility_GetLayoutProperty_m3220(NULL /*static, unused*/, G_B4_0, L_10, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Max_m3711(NULL /*static, unused*/, G_B4_1, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleWidth(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t671_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t672_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetFlexibleWidthU3Em__10_m3225_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3777_MethodInfo_var;
extern "C" float LayoutUtility_GetFlexibleWidth_m3216 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t671_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(532);
		Func_2_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(533);
		LayoutUtility_U3CGetFlexibleWidthU3Em__10_m3225_MethodInfo_var = il2cpp_codegen_method_info_from_index(539);
		Func_2__ctor_m3777_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484184);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t556 * G_B2_0 = {0};
	RectTransform_t556 * G_B1_0 = {0};
	{
		RectTransform_t556 * L_0 = ___rect;
		Func_2_t672 * L_1 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetFlexibleWidthU3Em__10_m3225_MethodInfo_var };
		Func_2_t672 * L_3 = (Func_2_t672 *)il2cpp_codegen_object_new (Func_2_t672_il2cpp_TypeInfo_var);
		Func_2__ctor_m3777(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3777_MethodInfo_var);
		((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t672 * L_4 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		float L_5 = LayoutUtility_GetLayoutProperty_m3220(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetMinHeight(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t671_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t672_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetMinHeightU3Em__11_m3226_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3777_MethodInfo_var;
extern "C" float LayoutUtility_GetMinHeight_m3217 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t671_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(532);
		Func_2_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(533);
		LayoutUtility_U3CGetMinHeightU3Em__11_m3226_MethodInfo_var = il2cpp_codegen_method_info_from_index(540);
		Func_2__ctor_m3777_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484184);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t556 * G_B2_0 = {0};
	RectTransform_t556 * G_B1_0 = {0};
	{
		RectTransform_t556 * L_0 = ___rect;
		Func_2_t672 * L_1 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetMinHeightU3Em__11_m3226_MethodInfo_var };
		Func_2_t672 * L_3 = (Func_2_t672 *)il2cpp_codegen_object_new (Func_2_t672_il2cpp_TypeInfo_var);
		Func_2__ctor_m3777(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3777_MethodInfo_var);
		((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t672 * L_4 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		float L_5 = LayoutUtility_GetLayoutProperty_m3220(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredHeight(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t671_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t672_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredHeightU3Em__12_m3227_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3777_MethodInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetPreferredHeightU3Em__13_m3228_MethodInfo_var;
extern "C" float LayoutUtility_GetPreferredHeight_m3218 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t671_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(532);
		Func_2_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(533);
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		LayoutUtility_U3CGetPreferredHeightU3Em__12_m3227_MethodInfo_var = il2cpp_codegen_method_info_from_index(541);
		Func_2__ctor_m3777_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484184);
		LayoutUtility_U3CGetPreferredHeightU3Em__13_m3228_MethodInfo_var = il2cpp_codegen_method_info_from_index(542);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t556 * G_B2_0 = {0};
	RectTransform_t556 * G_B1_0 = {0};
	RectTransform_t556 * G_B4_0 = {0};
	float G_B4_1 = 0.0f;
	RectTransform_t556 * G_B3_0 = {0};
	float G_B3_1 = 0.0f;
	{
		RectTransform_t556 * L_0 = ___rect;
		Func_2_t672 * L_1 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetPreferredHeightU3Em__12_m3227_MethodInfo_var };
		Func_2_t672 * L_3 = (Func_2_t672 *)il2cpp_codegen_object_new (Func_2_t672_il2cpp_TypeInfo_var);
		Func_2__ctor_m3777(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3777_MethodInfo_var);
		((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t672 * L_4 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		float L_5 = LayoutUtility_GetLayoutProperty_m3220(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		RectTransform_t556 * L_6 = ___rect;
		Func_2_t672 * L_7 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		G_B3_0 = L_6;
		G_B3_1 = L_5;
		if (L_7)
		{
			G_B4_0 = L_6;
			G_B4_1 = L_5;
			goto IL_0041;
		}
	}
	{
		IntPtr_t L_8 = { (void*)LayoutUtility_U3CGetPreferredHeightU3Em__13_m3228_MethodInfo_var };
		Func_2_t672 * L_9 = (Func_2_t672 *)il2cpp_codegen_object_new (Func_2_t672_il2cpp_TypeInfo_var);
		Func_2__ctor_m3777(L_9, NULL, L_8, /*hidden argument*/Func_2__ctor_m3777_MethodInfo_var);
		((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6 = L_9;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0041:
	{
		Func_2_t672 * L_10 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache6_6;
		float L_11 = LayoutUtility_GetLayoutProperty_m3220(NULL /*static, unused*/, G_B4_0, L_10, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Max_m3711(NULL /*static, unused*/, G_B4_1, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleHeight(UnityEngine.RectTransform)
extern TypeInfo* LayoutUtility_t671_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t672_il2cpp_TypeInfo_var;
extern const MethodInfo* LayoutUtility_U3CGetFlexibleHeightU3Em__14_m3229_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3777_MethodInfo_var;
extern "C" float LayoutUtility_GetFlexibleHeight_m3219 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LayoutUtility_t671_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(532);
		Func_2_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(533);
		LayoutUtility_U3CGetFlexibleHeightU3Em__14_m3229_MethodInfo_var = il2cpp_codegen_method_info_from_index(543);
		Func_2__ctor_m3777_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484184);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t556 * G_B2_0 = {0};
	RectTransform_t556 * G_B1_0 = {0};
	{
		RectTransform_t556 * L_0 = ___rect;
		Func_2_t672 * L_1 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_2 = { (void*)LayoutUtility_U3CGetFlexibleHeightU3Em__14_m3229_MethodInfo_var };
		Func_2_t672 * L_3 = (Func_2_t672 *)il2cpp_codegen_object_new (Func_2_t672_il2cpp_TypeInfo_var);
		Func_2__ctor_m3777(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3777_MethodInfo_var);
		((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Func_2_t672 * L_4 = ((LayoutUtility_t671_StaticFields*)LayoutUtility_t671_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache7_7;
		float L_5 = LayoutUtility_GetLayoutProperty_m3220(NULL /*static, unused*/, G_B2_0, L_4, (0.0f), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single)
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
#include "System_Core_System_Func_2_gen_3.h"
// System.Single
#include "mscorlib_System_Single.h"
extern "C" float LayoutUtility_GetLayoutProperty_m3220 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, Func_2_t672 * ___property, float ___defaultValue, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		RectTransform_t556 * L_0 = ___rect;
		Func_2_t672 * L_1 = ___property;
		float L_2 = ___defaultValue;
		float L_3 = LayoutUtility_GetLayoutProperty_m3221(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single,UnityEngine.UI.ILayoutElement&)
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Component>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_0MethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
extern const Il2CppType* ILayoutElement_t719_0_0_0_var;
extern TypeInfo* ListPool_1_t740_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ILayoutElement_t719_il2cpp_TypeInfo_var;
extern TypeInfo* Behaviour_t772_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m3507_MethodInfo_var;
extern const MethodInfo* Func_2_Invoke_m3778_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m3509_MethodInfo_var;
extern "C" float LayoutUtility_GetLayoutProperty_m3221 (Object_t * __this /* static, unused */, RectTransform_t556 * ___rect, Func_2_t672 * ___property, float ___defaultValue, Object_t ** ___source, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_0_0_0_var = il2cpp_codegen_type_from_index(530);
		ListPool_1_t740_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(438);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		ILayoutElement_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(530);
		Behaviour_t772_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(531);
		ListPool_1_Get_m3507_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484089);
		Func_2_Invoke_m3778_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484192);
		ListPool_1_Release_m3509_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484090);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	List_1_t718 * V_2 = {0};
	int32_t V_3 = 0;
	Object_t * V_4 = {0};
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	{
		Object_t ** L_0 = ___source;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		RectTransform_t556 * L_1 = ___rect;
		bool L_2 = Object_op_Equality_m1413(NULL /*static, unused*/, L_1, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		return (0.0f);
	}

IL_0015:
	{
		float L_3 = ___defaultValue;
		V_0 = L_3;
		V_1 = ((int32_t)-2147483648);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t740_il2cpp_TypeInfo_var);
		List_1_t718 * L_4 = ListPool_1_Get_m3507(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3507_MethodInfo_var);
		V_2 = L_4;
		RectTransform_t556 * L_5 = ___rect;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(ILayoutElement_t719_0_0_0_var), /*hidden argument*/NULL);
		List_1_t718 * L_7 = V_2;
		NullCheck(L_5);
		Component_GetComponents_m3508(L_5, L_6, L_7, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_00c6;
	}

IL_003b:
	{
		List_1_t718 * L_8 = V_2;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		Component_t365 * L_10 = (Component_t365 *)VirtFuncInvoker1< Component_t365 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32) */, L_8, L_9);
		V_4 = ((Object_t *)IsInst(L_10, ILayoutElement_t719_il2cpp_TypeInfo_var));
		Object_t * L_11 = V_4;
		if (!((Behaviour_t772 *)IsInstClass(L_11, Behaviour_t772_il2cpp_TypeInfo_var)))
		{
			goto IL_006b;
		}
	}
	{
		Object_t * L_12 = V_4;
		NullCheck(((Behaviour_t772 *)CastclassClass(L_12, Behaviour_t772_il2cpp_TypeInfo_var)));
		bool L_13 = Behaviour_get_isActiveAndEnabled_m3310(((Behaviour_t772 *)CastclassClass(L_12, Behaviour_t772_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_006b;
		}
	}
	{
		goto IL_00c2;
	}

IL_006b:
	{
		Object_t * L_14 = V_4;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 UnityEngine.UI.ILayoutElement::get_layoutPriority() */, ILayoutElement_t719_il2cpp_TypeInfo_var, L_14);
		V_5 = L_15;
		int32_t L_16 = V_5;
		int32_t L_17 = V_1;
		if ((((int32_t)L_16) >= ((int32_t)L_17)))
		{
			goto IL_0081;
		}
	}
	{
		goto IL_00c2;
	}

IL_0081:
	{
		Func_2_t672 * L_18 = ___property;
		Object_t * L_19 = V_4;
		NullCheck(L_18);
		float L_20 = Func_2_Invoke_m3778(L_18, L_19, /*hidden argument*/Func_2_Invoke_m3778_MethodInfo_var);
		V_6 = L_20;
		float L_21 = V_6;
		if ((!(((float)L_21) < ((float)(0.0f)))))
		{
			goto IL_009c;
		}
	}
	{
		goto IL_00c2;
	}

IL_009c:
	{
		int32_t L_22 = V_5;
		int32_t L_23 = V_1;
		if ((((int32_t)L_22) <= ((int32_t)L_23)))
		{
			goto IL_00b3;
		}
	}
	{
		float L_24 = V_6;
		V_0 = L_24;
		int32_t L_25 = V_5;
		V_1 = L_25;
		Object_t ** L_26 = ___source;
		Object_t * L_27 = V_4;
		*((Object_t **)(L_26)) = (Object_t *)L_27;
		goto IL_00c2;
	}

IL_00b3:
	{
		float L_28 = V_6;
		float L_29 = V_0;
		if ((!(((float)L_28) > ((float)L_29))))
		{
			goto IL_00c2;
		}
	}
	{
		float L_30 = V_6;
		V_0 = L_30;
		Object_t ** L_31 = ___source;
		Object_t * L_32 = V_4;
		*((Object_t **)(L_31)) = (Object_t *)L_32;
	}

IL_00c2:
	{
		int32_t L_33 = V_3;
		V_3 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00c6:
	{
		int32_t L_34 = V_3;
		List_1_t718 * L_35 = V_2;
		NullCheck(L_35);
		int32_t L_36 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count() */, L_35);
		if ((((int32_t)L_34) < ((int32_t)L_36)))
		{
			goto IL_003b;
		}
	}
	{
		List_1_t718 * L_37 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t740_il2cpp_TypeInfo_var);
		ListPool_1_Release_m3509(NULL /*static, unused*/, L_37, /*hidden argument*/ListPool_1_Release_m3509_MethodInfo_var);
		float L_38 = V_0;
		return L_38;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetMinWidth>m__D(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t719_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetMinWidthU3Em__D_m3222 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(530);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(2 /* System.Single UnityEngine.UI.ILayoutElement::get_minWidth() */, ILayoutElement_t719_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__E(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t719_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredWidthU3Em__E_m3223 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(530);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(2 /* System.Single UnityEngine.UI.ILayoutElement::get_minWidth() */, ILayoutElement_t719_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__F(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t719_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredWidthU3Em__F_m3224 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(530);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(3 /* System.Single UnityEngine.UI.ILayoutElement::get_preferredWidth() */, ILayoutElement_t719_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleWidth>m__10(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t719_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetFlexibleWidthU3Em__10_m3225 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(530);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(4 /* System.Single UnityEngine.UI.ILayoutElement::get_flexibleWidth() */, ILayoutElement_t719_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetMinHeight>m__11(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t719_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetMinHeightU3Em__11_m3226 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(530);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(5 /* System.Single UnityEngine.UI.ILayoutElement::get_minHeight() */, ILayoutElement_t719_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__12(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t719_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredHeightU3Em__12_m3227 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(530);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(5 /* System.Single UnityEngine.UI.ILayoutElement::get_minHeight() */, ILayoutElement_t719_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__13(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t719_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetPreferredHeightU3Em__13_m3228 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(530);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(6 /* System.Single UnityEngine.UI.ILayoutElement::get_preferredHeight() */, ILayoutElement_t719_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleHeight>m__14(UnityEngine.UI.ILayoutElement)
extern TypeInfo* ILayoutElement_t719_il2cpp_TypeInfo_var;
extern "C" float LayoutUtility_U3CGetFlexibleHeightU3Em__14_m3229 (Object_t * __this /* static, unused */, Object_t * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILayoutElement_t719_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(530);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___e;
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(7 /* System.Single UnityEngine.UI.ILayoutElement::get_flexibleHeight() */, ILayoutElement_t719_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup.h"
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroupMethodDeclarations.h"
// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"
extern "C" void VerticalLayoutGroup__ctor_m3230 (VerticalLayoutGroup_t673 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup__ctor_m3128(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
extern "C" void VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3231 (VerticalLayoutGroup_t673 * __this, const MethodInfo* method)
{
	{
		LayoutGroup_CalculateLayoutInputHorizontal_m3168(__this, /*hidden argument*/NULL);
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m3135(__this, 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
extern "C" void VerticalLayoutGroup_CalculateLayoutInputVertical_m3232 (VerticalLayoutGroup_t673 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m3135(__this, 1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
extern "C" void VerticalLayoutGroup_SetLayoutHorizontal_m3233 (VerticalLayoutGroup_t673 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m3136(__this, 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
extern "C" void VerticalLayoutGroup_SetLayoutVertical_m3234 (VerticalLayoutGroup_t673 * __this, const MethodInfo* method)
{
	{
		HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m3136(__this, 1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Vector3>
#include "mscorlib_System_Collections_Generic_List_1_gen_36.h"
// System.Collections.Generic.List`1<UnityEngine.Color32>
#include "mscorlib_System_Collections_Generic_List_1_gen_37.h"
// System.Collections.Generic.List`1<UnityEngine.Vector2>
#include "mscorlib_System_Collections_Generic_List_1_gen_38.h"
// System.Collections.Generic.List`1<UnityEngine.Vector4>
#include "mscorlib_System_Collections_Generic_List_1_gen_39.h"
// System.Collections.Generic.List`1<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_gen_40.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_26.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Vector3>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_1MethodDeclarations.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Color32>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_2MethodDeclarations.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Vector2>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_3MethodDeclarations.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Vector4>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_4MethodDeclarations.h"
// UnityEngine.UI.ListPool`1<System.Int32>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_5MethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Vector3>
#include "mscorlib_System_Collections_Generic_List_1_gen_36MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Color32>
#include "mscorlib_System_Collections_Generic_List_1_gen_37MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Vector2>
#include "mscorlib_System_Collections_Generic_List_1_gen_38MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Vector4>
#include "mscorlib_System_Collections_Generic_List_1_gen_39MethodDeclarations.h"
// System.Collections.Generic.List`1<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_gen_40MethodDeclarations.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_26MethodDeclarations.h"
// System.Void UnityEngine.UI.VertexHelper::.ctor()
// UnityEngine.UI.ListPool`1<UnityEngine.Vector3>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_1MethodDeclarations.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Color32>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_2MethodDeclarations.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Vector2>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_3MethodDeclarations.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Vector4>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_4MethodDeclarations.h"
// UnityEngine.UI.ListPool`1<System.Int32>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_5MethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern TypeInfo* ListPool_1_t773_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t774_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t775_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t776_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t777_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m3779_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m3780_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m3781_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m3782_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m3783_MethodInfo_var;
extern "C" void VertexHelper__ctor_m3235 (VertexHelper_t674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ListPool_1_t773_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(536);
		ListPool_1_t774_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(537);
		ListPool_1_t775_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(538);
		ListPool_1_t776_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(539);
		ListPool_1_t777_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(540);
		ListPool_1_Get_m3779_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484193);
		ListPool_1_Get_m3780_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484194);
		ListPool_1_Get_m3781_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484195);
		ListPool_1_Get_m3782_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484196);
		ListPool_1_Get_m3783_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484197);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t773_il2cpp_TypeInfo_var);
		List_1_t675 * L_0 = ListPool_1_Get_m3779(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3779_MethodInfo_var);
		__this->___m_Positions_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t774_il2cpp_TypeInfo_var);
		List_1_t676 * L_1 = ListPool_1_Get_m3780(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3780_MethodInfo_var);
		__this->___m_Colors_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t775_il2cpp_TypeInfo_var);
		List_1_t677 * L_2 = ListPool_1_Get_m3781(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3781_MethodInfo_var);
		__this->___m_Uv0S_2 = L_2;
		List_1_t677 * L_3 = ListPool_1_Get_m3781(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3781_MethodInfo_var);
		__this->___m_Uv1S_3 = L_3;
		List_1_t675 * L_4 = ListPool_1_Get_m3779(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3779_MethodInfo_var);
		__this->___m_Normals_4 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t776_il2cpp_TypeInfo_var);
		List_1_t678 * L_5 = ListPool_1_Get_m3782(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3782_MethodInfo_var);
		__this->___m_Tangents_5 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t777_il2cpp_TypeInfo_var);
		List_1_t679 * L_6 = ListPool_1_Get_m3783(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3783_MethodInfo_var);
		__this->___m_Indicies_6 = L_6;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::.ctor(UnityEngine.Mesh)
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Vector3>
#include "mscorlib_System_Collections_Generic_List_1_gen_36MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Color32>
#include "mscorlib_System_Collections_Generic_List_1_gen_37MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Vector2>
#include "mscorlib_System_Collections_Generic_List_1_gen_38MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.Vector4>
#include "mscorlib_System_Collections_Generic_List_1_gen_39MethodDeclarations.h"
// System.Collections.Generic.List`1<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_gen_40MethodDeclarations.h"
extern TypeInfo* ListPool_1_t773_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t774_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t775_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t776_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t777_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Get_m3779_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m3780_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m3781_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m3782_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m3783_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m3784_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m3786_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m3787_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m3790_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m3792_MethodInfo_var;
extern "C" void VertexHelper__ctor_m3236 (VertexHelper_t674 * __this, Mesh_t104 * ___m, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ListPool_1_t773_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(536);
		ListPool_1_t774_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(537);
		ListPool_1_t775_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(538);
		ListPool_1_t776_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(539);
		ListPool_1_t777_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(540);
		ListPool_1_Get_m3779_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484193);
		ListPool_1_Get_m3780_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484194);
		ListPool_1_Get_m3781_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484195);
		ListPool_1_Get_m3782_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484196);
		ListPool_1_Get_m3783_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484197);
		List_1_AddRange_m3784_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484198);
		List_1_AddRange_m3786_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484199);
		List_1_AddRange_m3787_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484200);
		List_1_AddRange_m3790_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484201);
		List_1_AddRange_m3792_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484202);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t773_il2cpp_TypeInfo_var);
		List_1_t675 * L_0 = ListPool_1_Get_m3779(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3779_MethodInfo_var);
		__this->___m_Positions_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t774_il2cpp_TypeInfo_var);
		List_1_t676 * L_1 = ListPool_1_Get_m3780(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3780_MethodInfo_var);
		__this->___m_Colors_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t775_il2cpp_TypeInfo_var);
		List_1_t677 * L_2 = ListPool_1_Get_m3781(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3781_MethodInfo_var);
		__this->___m_Uv0S_2 = L_2;
		List_1_t677 * L_3 = ListPool_1_Get_m3781(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3781_MethodInfo_var);
		__this->___m_Uv1S_3 = L_3;
		List_1_t675 * L_4 = ListPool_1_Get_m3779(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3779_MethodInfo_var);
		__this->___m_Normals_4 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t776_il2cpp_TypeInfo_var);
		List_1_t678 * L_5 = ListPool_1_Get_m3782(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3782_MethodInfo_var);
		__this->___m_Tangents_5 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t777_il2cpp_TypeInfo_var);
		List_1_t679 * L_6 = ListPool_1_Get_m3783(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3783_MethodInfo_var);
		__this->___m_Indicies_6 = L_6;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		List_1_t675 * L_7 = (__this->___m_Positions_0);
		Mesh_t104 * L_8 = ___m;
		NullCheck(L_8);
		Vector3U5BU5D_t254* L_9 = Mesh_get_vertices_m1465(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		List_1_AddRange_m3784(L_7, (Object_t*)(Object_t*)L_9, /*hidden argument*/List_1_AddRange_m3784_MethodInfo_var);
		List_1_t676 * L_10 = (__this->___m_Colors_1);
		Mesh_t104 * L_11 = ___m;
		NullCheck(L_11);
		Color32U5BU5D_t778* L_12 = Mesh_get_colors32_m3785(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		List_1_AddRange_m3786(L_10, (Object_t*)(Object_t*)L_12, /*hidden argument*/List_1_AddRange_m3786_MethodInfo_var);
		List_1_t677 * L_13 = (__this->___m_Uv0S_2);
		Mesh_t104 * L_14 = ___m;
		NullCheck(L_14);
		Vector2U5BU5D_t262* L_15 = Mesh_get_uv_m1468(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		List_1_AddRange_m3787(L_13, (Object_t*)(Object_t*)L_15, /*hidden argument*/List_1_AddRange_m3787_MethodInfo_var);
		List_1_t677 * L_16 = (__this->___m_Uv1S_3);
		Mesh_t104 * L_17 = ___m;
		NullCheck(L_17);
		Vector2U5BU5D_t262* L_18 = Mesh_get_uv2_m3788(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		List_1_AddRange_m3787(L_16, (Object_t*)(Object_t*)L_18, /*hidden argument*/List_1_AddRange_m3787_MethodInfo_var);
		List_1_t675 * L_19 = (__this->___m_Normals_4);
		Mesh_t104 * L_20 = ___m;
		NullCheck(L_20);
		Vector3U5BU5D_t254* L_21 = Mesh_get_normals_m1466(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		List_1_AddRange_m3784(L_19, (Object_t*)(Object_t*)L_21, /*hidden argument*/List_1_AddRange_m3784_MethodInfo_var);
		List_1_t678 * L_22 = (__this->___m_Tangents_5);
		Mesh_t104 * L_23 = ___m;
		NullCheck(L_23);
		Vector4U5BU5D_t779* L_24 = Mesh_get_tangents_m3789(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		List_1_AddRange_m3790(L_22, (Object_t*)(Object_t*)L_24, /*hidden argument*/List_1_AddRange_m3790_MethodInfo_var);
		List_1_t679 * L_25 = (__this->___m_Indicies_6);
		Mesh_t104 * L_26 = ___m;
		NullCheck(L_26);
		Int32U5BU5D_t401* L_27 = Mesh_GetIndices_m3791(L_26, 0, /*hidden argument*/NULL);
		NullCheck(L_25);
		List_1_AddRange_m3792(L_25, (Object_t*)(Object_t*)L_27, /*hidden argument*/List_1_AddRange_m3792_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::.cctor()
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
extern TypeInfo* VertexHelper_t674_il2cpp_TypeInfo_var;
extern "C" void VertexHelper__cctor_m3237 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VertexHelper_t674_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(440);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector4_t680  L_0 = {0};
		Vector4__ctor_m3517(&L_0, (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((VertexHelper_t674_StaticFields*)VertexHelper_t674_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7 = L_0;
		Vector3_t36  L_1 = Vector3_get_back_m3793(NULL /*static, unused*/, /*hidden argument*/NULL);
		((VertexHelper_t674_StaticFields*)VertexHelper_t674_il2cpp_TypeInfo_var->static_fields)->___s_DefaultNormal_8 = L_1;
		return;
	}
}
// System.Int32 UnityEngine.UI.VertexHelper::get_currentVertCount()
extern "C" int32_t VertexHelper_get_currentVertCount_m3238 (VertexHelper_t674 * __this, const MethodInfo* method)
{
	{
		List_1_t675 * L_0 = (__this->___m_Positions_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void UnityEngine.UI.VertexHelper::PopulateUIVertex(UnityEngine.UIVertex&,System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void VertexHelper_PopulateUIVertex_m3239 (VertexHelper_t674 * __this, UIVertex_t605 * ___vertex, int32_t ___i, const MethodInfo* method)
{
	{
		UIVertex_t605 * L_0 = ___vertex;
		List_1_t675 * L_1 = (__this->___m_Positions_0);
		int32_t L_2 = ___i;
		NullCheck(L_1);
		Vector3_t36  L_3 = (Vector3_t36 )VirtFuncInvoker1< Vector3_t36 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_1, L_2);
		L_0->___position_0 = L_3;
		UIVertex_t605 * L_4 = ___vertex;
		List_1_t676 * L_5 = (__this->___m_Colors_1);
		int32_t L_6 = ___i;
		NullCheck(L_5);
		Color32_t711  L_7 = (Color32_t711 )VirtFuncInvoker1< Color32_t711 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Color32>::get_Item(System.Int32) */, L_5, L_6);
		L_4->___color_2 = L_7;
		UIVertex_t605 * L_8 = ___vertex;
		List_1_t677 * L_9 = (__this->___m_Uv0S_2);
		int32_t L_10 = ___i;
		NullCheck(L_9);
		Vector2_t2  L_11 = (Vector2_t2 )VirtFuncInvoker1< Vector2_t2 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32) */, L_9, L_10);
		L_8->___uv0_3 = L_11;
		UIVertex_t605 * L_12 = ___vertex;
		List_1_t677 * L_13 = (__this->___m_Uv1S_3);
		int32_t L_14 = ___i;
		NullCheck(L_13);
		Vector2_t2  L_15 = (Vector2_t2 )VirtFuncInvoker1< Vector2_t2 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector2>::get_Item(System.Int32) */, L_13, L_14);
		L_12->___uv1_4 = L_15;
		UIVertex_t605 * L_16 = ___vertex;
		List_1_t675 * L_17 = (__this->___m_Normals_4);
		int32_t L_18 = ___i;
		NullCheck(L_17);
		Vector3_t36  L_19 = (Vector3_t36 )VirtFuncInvoker1< Vector3_t36 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_17, L_18);
		L_16->___normal_1 = L_19;
		UIVertex_t605 * L_20 = ___vertex;
		List_1_t678 * L_21 = (__this->___m_Tangents_5);
		int32_t L_22 = ___i;
		NullCheck(L_21);
		Vector4_t680  L_23 = (Vector4_t680 )VirtFuncInvoker1< Vector4_t680 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Item(System.Int32) */, L_21, L_22);
		L_20->___tangent_5 = L_23;
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::FillMesh(UnityEngine.Mesh)
extern "C" void VertexHelper_FillMesh_m3240 (VertexHelper_t674 * __this, Mesh_t104 * ___mesh, const MethodInfo* method)
{
	{
		Mesh_t104 * L_0 = ___mesh;
		NullCheck(L_0);
		Mesh_Clear_m3794(L_0, /*hidden argument*/NULL);
		Mesh_t104 * L_1 = ___mesh;
		List_1_t675 * L_2 = (__this->___m_Positions_0);
		NullCheck(L_1);
		Mesh_SetVertices_m3795(L_1, L_2, /*hidden argument*/NULL);
		Mesh_t104 * L_3 = ___mesh;
		List_1_t676 * L_4 = (__this->___m_Colors_1);
		NullCheck(L_3);
		Mesh_SetColors_m3796(L_3, L_4, /*hidden argument*/NULL);
		Mesh_t104 * L_5 = ___mesh;
		List_1_t677 * L_6 = (__this->___m_Uv0S_2);
		NullCheck(L_5);
		Mesh_SetUVs_m3797(L_5, 0, L_6, /*hidden argument*/NULL);
		Mesh_t104 * L_7 = ___mesh;
		List_1_t677 * L_8 = (__this->___m_Uv1S_3);
		NullCheck(L_7);
		Mesh_SetUVs_m3797(L_7, 1, L_8, /*hidden argument*/NULL);
		Mesh_t104 * L_9 = ___mesh;
		List_1_t675 * L_10 = (__this->___m_Normals_4);
		NullCheck(L_9);
		Mesh_SetNormals_m3798(L_9, L_10, /*hidden argument*/NULL);
		Mesh_t104 * L_11 = ___mesh;
		List_1_t678 * L_12 = (__this->___m_Tangents_5);
		NullCheck(L_11);
		Mesh_SetTangents_m3799(L_11, L_12, /*hidden argument*/NULL);
		Mesh_t104 * L_13 = ___mesh;
		List_1_t679 * L_14 = (__this->___m_Indicies_6);
		NullCheck(L_13);
		Mesh_SetTriangles_m3800(L_13, L_14, 0, /*hidden argument*/NULL);
		Mesh_t104 * L_15 = ___mesh;
		NullCheck(L_15);
		Mesh_RecalculateBounds_m3801(L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::Dispose()
extern TypeInfo* ListPool_1_t773_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t774_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t775_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t776_il2cpp_TypeInfo_var;
extern TypeInfo* ListPool_1_t777_il2cpp_TypeInfo_var;
extern const MethodInfo* ListPool_1_Release_m3802_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m3803_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m3804_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m3805_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m3806_MethodInfo_var;
extern "C" void VertexHelper_Dispose_m3241 (VertexHelper_t674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ListPool_1_t773_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(536);
		ListPool_1_t774_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(537);
		ListPool_1_t775_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(538);
		ListPool_1_t776_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(539);
		ListPool_1_t777_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(540);
		ListPool_1_Release_m3802_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484203);
		ListPool_1_Release_m3803_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484204);
		ListPool_1_Release_m3804_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484205);
		ListPool_1_Release_m3805_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484206);
		ListPool_1_Release_m3806_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484207);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t675 * L_0 = (__this->___m_Positions_0);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t773_il2cpp_TypeInfo_var);
		ListPool_1_Release_m3802(NULL /*static, unused*/, L_0, /*hidden argument*/ListPool_1_Release_m3802_MethodInfo_var);
		List_1_t676 * L_1 = (__this->___m_Colors_1);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t774_il2cpp_TypeInfo_var);
		ListPool_1_Release_m3803(NULL /*static, unused*/, L_1, /*hidden argument*/ListPool_1_Release_m3803_MethodInfo_var);
		List_1_t677 * L_2 = (__this->___m_Uv0S_2);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t775_il2cpp_TypeInfo_var);
		ListPool_1_Release_m3804(NULL /*static, unused*/, L_2, /*hidden argument*/ListPool_1_Release_m3804_MethodInfo_var);
		List_1_t677 * L_3 = (__this->___m_Uv1S_3);
		ListPool_1_Release_m3804(NULL /*static, unused*/, L_3, /*hidden argument*/ListPool_1_Release_m3804_MethodInfo_var);
		List_1_t675 * L_4 = (__this->___m_Normals_4);
		ListPool_1_Release_m3802(NULL /*static, unused*/, L_4, /*hidden argument*/ListPool_1_Release_m3802_MethodInfo_var);
		List_1_t678 * L_5 = (__this->___m_Tangents_5);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t776_il2cpp_TypeInfo_var);
		ListPool_1_Release_m3805(NULL /*static, unused*/, L_5, /*hidden argument*/ListPool_1_Release_m3805_MethodInfo_var);
		List_1_t679 * L_6 = (__this->___m_Indicies_6);
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t777_il2cpp_TypeInfo_var);
		ListPool_1_Release_m3806(NULL /*static, unused*/, L_6, /*hidden argument*/ListPool_1_Release_m3806_MethodInfo_var);
		__this->___m_Positions_0 = (List_1_t675 *)NULL;
		__this->___m_Colors_1 = (List_1_t676 *)NULL;
		__this->___m_Uv0S_2 = (List_1_t677 *)NULL;
		__this->___m_Uv1S_3 = (List_1_t677 *)NULL;
		__this->___m_Normals_4 = (List_1_t675 *)NULL;
		__this->___m_Tangents_5 = (List_1_t678 *)NULL;
		__this->___m_Indicies_6 = (List_1_t679 *)NULL;
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddVert(UnityEngine.Vector3,UnityEngine.Color32,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector3,UnityEngine.Vector4)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
extern "C" void VertexHelper_AddVert_m3242 (VertexHelper_t674 * __this, Vector3_t36  ___position, Color32_t711  ___color, Vector2_t2  ___uv0, Vector2_t2  ___uv1, Vector3_t36  ___normal, Vector4_t680  ___tangent, const MethodInfo* method)
{
	{
		List_1_t675 * L_0 = (__this->___m_Positions_0);
		Vector3_t36  L_1 = ___position;
		NullCheck(L_0);
		VirtActionInvoker1< Vector3_t36  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_0, L_1);
		List_1_t676 * L_2 = (__this->___m_Colors_1);
		Color32_t711  L_3 = ___color;
		NullCheck(L_2);
		VirtActionInvoker1< Color32_t711  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Add(!0) */, L_2, L_3);
		List_1_t677 * L_4 = (__this->___m_Uv0S_2);
		Vector2_t2  L_5 = ___uv0;
		NullCheck(L_4);
		VirtActionInvoker1< Vector2_t2  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0) */, L_4, L_5);
		List_1_t677 * L_6 = (__this->___m_Uv1S_3);
		Vector2_t2  L_7 = ___uv1;
		NullCheck(L_6);
		VirtActionInvoker1< Vector2_t2  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0) */, L_6, L_7);
		List_1_t675 * L_8 = (__this->___m_Normals_4);
		Vector3_t36  L_9 = ___normal;
		NullCheck(L_8);
		VirtActionInvoker1< Vector3_t36  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_8, L_9);
		List_1_t678 * L_10 = (__this->___m_Tangents_5);
		Vector4_t680  L_11 = ___tangent;
		NullCheck(L_10);
		VirtActionInvoker1< Vector4_t680  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Add(!0) */, L_10, L_11);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddVert(UnityEngine.Vector3,UnityEngine.Color32,UnityEngine.Vector2)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.UI.VertexHelper
#include "UnityEngine_UI_UnityEngine_UI_VertexHelperMethodDeclarations.h"
extern TypeInfo* VertexHelper_t674_il2cpp_TypeInfo_var;
extern "C" void VertexHelper_AddVert_m3243 (VertexHelper_t674 * __this, Vector3_t36  ___position, Color32_t711  ___color, Vector2_t2  ___uv0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VertexHelper_t674_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(440);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t36  L_0 = ___position;
		Color32_t711  L_1 = ___color;
		Vector2_t2  L_2 = ___uv0;
		Vector2_t2  L_3 = Vector2_get_zero_m3309(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VertexHelper_t674_il2cpp_TypeInfo_var);
		Vector3_t36  L_4 = ((VertexHelper_t674_StaticFields*)VertexHelper_t674_il2cpp_TypeInfo_var->static_fields)->___s_DefaultNormal_8;
		Vector4_t680  L_5 = ((VertexHelper_t674_StaticFields*)VertexHelper_t674_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7;
		VertexHelper_AddVert_m3242(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddVert(UnityEngine.UIVertex)
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
extern "C" void VertexHelper_AddVert_m3244 (VertexHelper_t674 * __this, UIVertex_t605  ___v, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = ((&___v)->___position_0);
		Color32_t711  L_1 = ((&___v)->___color_2);
		Vector2_t2  L_2 = ((&___v)->___uv0_3);
		Vector2_t2  L_3 = ((&___v)->___uv1_4);
		Vector3_t36  L_4 = ((&___v)->___normal_1);
		Vector4_t680  L_5 = ((&___v)->___tangent_5);
		VertexHelper_AddVert_m3242(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddTriangle(System.Int32,System.Int32,System.Int32)
extern "C" void VertexHelper_AddTriangle_m3245 (VertexHelper_t674 * __this, int32_t ___idx0, int32_t ___idx1, int32_t ___idx2, const MethodInfo* method)
{
	{
		List_1_t679 * L_0 = (__this->___m_Indicies_6);
		int32_t L_1 = ___idx0;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_0, L_1);
		List_1_t679 * L_2 = (__this->___m_Indicies_6);
		int32_t L_3 = ___idx1;
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_2, L_3);
		List_1_t679 * L_4 = (__this->___m_Indicies_6);
		int32_t L_5 = ___idx2;
		NullCheck(L_4);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_4, L_5);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddUIVertexQuad(UnityEngine.UIVertex[])
#include "UnityEngine_ArrayTypes.h"
extern "C" void VertexHelper_AddUIVertexQuad_m3246 (VertexHelper_t674 * __this, UIVertexU5BU5D_t602* ___verts, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VertexHelper_get_currentVertCount_m3238(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0060;
	}

IL_000e:
	{
		UIVertexU5BU5D_t602* L_1 = ___verts;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector3_t36  L_3 = (((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_1, L_2, sizeof(UIVertex_t605 )))->___position_0);
		UIVertexU5BU5D_t602* L_4 = ___verts;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Color32_t711  L_6 = (((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_4, L_5, sizeof(UIVertex_t605 )))->___color_2);
		UIVertexU5BU5D_t602* L_7 = ___verts;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Vector2_t2  L_9 = (((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_7, L_8, sizeof(UIVertex_t605 )))->___uv0_3);
		UIVertexU5BU5D_t602* L_10 = ___verts;
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		Vector2_t2  L_12 = (((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_10, L_11, sizeof(UIVertex_t605 )))->___uv1_4);
		UIVertexU5BU5D_t602* L_13 = ___verts;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		Vector3_t36  L_15 = (((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_13, L_14, sizeof(UIVertex_t605 )))->___normal_1);
		UIVertexU5BU5D_t602* L_16 = ___verts;
		int32_t L_17 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		Vector4_t680  L_18 = (((UIVertex_t605 *)(UIVertex_t605 *)SZArrayLdElema(L_16, L_17, sizeof(UIVertex_t605 )))->___tangent_5);
		VertexHelper_AddVert_m3242(__this, L_3, L_6, L_9, L_12, L_15, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0060:
	{
		int32_t L_20 = V_1;
		if ((((int32_t)L_20) < ((int32_t)4)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_21 = V_0;
		int32_t L_22 = V_0;
		int32_t L_23 = V_0;
		VertexHelper_AddTriangle_m3245(__this, L_21, ((int32_t)((int32_t)L_22+(int32_t)1)), ((int32_t)((int32_t)L_23+(int32_t)2)), /*hidden argument*/NULL);
		int32_t L_24 = V_0;
		int32_t L_25 = V_0;
		int32_t L_26 = V_0;
		VertexHelper_AddTriangle_m3245(__this, ((int32_t)((int32_t)L_24+(int32_t)2)), ((int32_t)((int32_t)L_25+(int32_t)3)), L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::AddUIVertexTriangleStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_26.h"
extern "C" void VertexHelper_AddUIVertexTriangleStream_m3247 (VertexHelper_t674 * __this, List_1_t709 * ___verts, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	UIVertex_t605  V_3 = {0};
	UIVertex_t605  V_4 = {0};
	UIVertex_t605  V_5 = {0};
	UIVertex_t605  V_6 = {0};
	UIVertex_t605  V_7 = {0};
	UIVertex_t605  V_8 = {0};
	{
		V_0 = 0;
		goto IL_00a2;
	}

IL_0007:
	{
		int32_t L_0 = VertexHelper_get_currentVertCount_m3238(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		V_2 = 0;
		goto IL_008a;
	}

IL_0015:
	{
		List_1_t709 * L_1 = ___verts;
		int32_t L_2 = V_0;
		int32_t L_3 = V_2;
		NullCheck(L_1);
		UIVertex_t605  L_4 = (UIVertex_t605 )VirtFuncInvoker1< UIVertex_t605 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_1, ((int32_t)((int32_t)L_2+(int32_t)L_3)));
		V_3 = L_4;
		Vector3_t36  L_5 = ((&V_3)->___position_0);
		List_1_t709 * L_6 = ___verts;
		int32_t L_7 = V_0;
		int32_t L_8 = V_2;
		NullCheck(L_6);
		UIVertex_t605  L_9 = (UIVertex_t605 )VirtFuncInvoker1< UIVertex_t605 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_6, ((int32_t)((int32_t)L_7+(int32_t)L_8)));
		V_4 = L_9;
		Color32_t711  L_10 = ((&V_4)->___color_2);
		List_1_t709 * L_11 = ___verts;
		int32_t L_12 = V_0;
		int32_t L_13 = V_2;
		NullCheck(L_11);
		UIVertex_t605  L_14 = (UIVertex_t605 )VirtFuncInvoker1< UIVertex_t605 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_11, ((int32_t)((int32_t)L_12+(int32_t)L_13)));
		V_5 = L_14;
		Vector2_t2  L_15 = ((&V_5)->___uv0_3);
		List_1_t709 * L_16 = ___verts;
		int32_t L_17 = V_0;
		int32_t L_18 = V_2;
		NullCheck(L_16);
		UIVertex_t605  L_19 = (UIVertex_t605 )VirtFuncInvoker1< UIVertex_t605 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_16, ((int32_t)((int32_t)L_17+(int32_t)L_18)));
		V_6 = L_19;
		Vector2_t2  L_20 = ((&V_6)->___uv1_4);
		List_1_t709 * L_21 = ___verts;
		int32_t L_22 = V_0;
		int32_t L_23 = V_2;
		NullCheck(L_21);
		UIVertex_t605  L_24 = (UIVertex_t605 )VirtFuncInvoker1< UIVertex_t605 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_21, ((int32_t)((int32_t)L_22+(int32_t)L_23)));
		V_7 = L_24;
		Vector3_t36  L_25 = ((&V_7)->___normal_1);
		List_1_t709 * L_26 = ___verts;
		int32_t L_27 = V_0;
		int32_t L_28 = V_2;
		NullCheck(L_26);
		UIVertex_t605  L_29 = (UIVertex_t605 )VirtFuncInvoker1< UIVertex_t605 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_26, ((int32_t)((int32_t)L_27+(int32_t)L_28)));
		V_8 = L_29;
		Vector4_t680  L_30 = ((&V_8)->___tangent_5);
		VertexHelper_AddVert_m3242(__this, L_5, L_10, L_15, L_20, L_25, L_30, /*hidden argument*/NULL);
		int32_t L_31 = V_2;
		V_2 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_008a:
	{
		int32_t L_32 = V_2;
		if ((((int32_t)L_32) < ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_33 = V_1;
		int32_t L_34 = V_1;
		int32_t L_35 = V_1;
		VertexHelper_AddTriangle_m3245(__this, L_33, ((int32_t)((int32_t)L_34+(int32_t)1)), ((int32_t)((int32_t)L_35+(int32_t)2)), /*hidden argument*/NULL);
		int32_t L_36 = V_0;
		V_0 = ((int32_t)((int32_t)L_36+(int32_t)3));
	}

IL_00a2:
	{
		int32_t L_37 = V_0;
		List_1_t709 * L_38 = ___verts;
		NullCheck(L_38);
		int32_t L_39 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_38);
		if ((((int32_t)L_37) < ((int32_t)L_39)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.VertexHelper::GetUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern TypeInfo* UIVertex_t605_il2cpp_TypeInfo_var;
extern "C" void VertexHelper_GetUIVertexStream_m3248 (VertexHelper_t674 * __this, List_1_t709 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIVertex_t605_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(453);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t605  V_0 = {0};
	int32_t V_1 = 0;
	{
		List_1_t709 * L_0 = ___stream;
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear() */, L_0);
		Initobj (UIVertex_t605_il2cpp_TypeInfo_var, (&V_0));
		V_1 = 0;
		goto IL_0034;
	}

IL_0015:
	{
		List_1_t679 * L_1 = (__this->___m_Indicies_6);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = (int32_t)VirtFuncInvoker1< int32_t, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32) */, L_1, L_2);
		VertexHelper_PopulateUIVertex_m3239(__this, (&V_0), L_3, /*hidden argument*/NULL);
		List_1_t709 * L_4 = ___stream;
		UIVertex_t605  L_5 = V_0;
		NullCheck(L_4);
		VirtActionInvoker1< UIVertex_t605  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(!0) */, L_4, L_5);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_7 = V_1;
		List_1_t679 * L_8 = (__this->___m_Indicies_6);
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count() */, L_8);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0015;
		}
	}
	{
		return;
	}
}
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffectMethodDeclarations.h"
// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void BaseVertexEffect__ctor_m3249 (BaseVertexEffect_t681 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.BaseMeshEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect.h"
// UnityEngine.UI.BaseMeshEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffectMethodDeclarations.h"
struct Graphic_t572;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Graphic>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Graphic>()
#define Component_GetComponent_TisGraphic_t572_m3650(__this, method) (( Graphic_t572 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void UnityEngine.UI.BaseMeshEffect::.ctor()
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
extern "C" void BaseMeshEffect__ctor_m3250 (BaseMeshEffect_t682 * __this, const MethodInfo* method)
{
	{
		UIBehaviour__ctor_m2006(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::get_graphic()
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisGraphic_t572_m3650_MethodInfo_var;
extern "C" Graphic_t572 * BaseMeshEffect_get_graphic_m3251 (BaseMeshEffect_t682 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisGraphic_t572_m3650_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484125);
		s_Il2CppMethodIntialized = true;
	}
	{
		Graphic_t572 * L_0 = (__this->___m_Graphic_2);
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Graphic_t572 * L_2 = Component_GetComponent_TisGraphic_t572_m3650(__this, /*hidden argument*/Component_GetComponent_TisGraphic_t572_m3650_MethodInfo_var);
		__this->___m_Graphic_2 = L_2;
	}

IL_001d:
	{
		Graphic_t572 * L_3 = (__this->___m_Graphic_2);
		return L_3;
	}
}
// System.Void UnityEngine.UI.BaseMeshEffect::OnEnable()
// UnityEngine.UI.BaseMeshEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffectMethodDeclarations.h"
extern "C" void BaseMeshEffect_OnEnable_m3252 (BaseMeshEffect_t682 * __this, const MethodInfo* method)
{
	{
		UIBehaviour_OnEnable_m2008(__this, /*hidden argument*/NULL);
		Graphic_t572 * L_0 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Graphic_t572 * L_2 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_2);
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.UI.BaseMeshEffect::OnDisable()
extern "C" void BaseMeshEffect_OnDisable_m3253 (BaseMeshEffect_t682 * __this, const MethodInfo* method)
{
	{
		Graphic_t572 * L_0 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Graphic_t572 * L_2 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_2);
	}

IL_001c:
	{
		UIBehaviour_OnDisable_m2010(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.BaseMeshEffect::OnDidApplyAnimationProperties()
extern "C" void BaseMeshEffect_OnDidApplyAnimationProperties_m3254 (BaseMeshEffect_t682 * __this, const MethodInfo* method)
{
	{
		Graphic_t572 * L_0 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Graphic_t572 * L_2 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_2);
	}

IL_001c:
	{
		UIBehaviour_OnDidApplyAnimationProperties_m2016(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_Outline.h"
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_OutlineMethodDeclarations.h"
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_ShadowMethodDeclarations.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
// System.Void UnityEngine.UI.Outline::.ctor()
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_ShadowMethodDeclarations.h"
extern "C" void Outline__ctor_m3255 (Outline_t683 * __this, const MethodInfo* method)
{
	{
		Shadow__ctor_m3259(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Outline::ModifyMesh(UnityEngine.Mesh)
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_26MethodDeclarations.h"
// UnityEngine.UI.VertexHelper
#include "UnityEngine_UI_UnityEngine_UI_VertexHelperMethodDeclarations.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
extern TypeInfo* List_1_t709_il2cpp_TypeInfo_var;
extern TypeInfo* VertexHelper_t674_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3807_MethodInfo_var;
extern const MethodInfo* List_1_get_Capacity_m3808_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m3809_MethodInfo_var;
extern "C" void Outline_ModifyMesh_m3256 (Outline_t683 * __this, Mesh_t104 * ___mesh, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(541);
		VertexHelper_t674_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(440);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		List_1__ctor_m3807_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484208);
		List_1_get_Capacity_m3808_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484209);
		List_1_set_Capacity_m3809_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484210);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t709 * V_0 = {0};
	VertexHelper_t674 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	VertexHelper_t674 * V_5 = {0};
	Vector2_t2  V_6 = {0};
	Vector2_t2  V_7 = {0};
	Vector2_t2  V_8 = {0};
	Vector2_t2  V_9 = {0};
	Vector2_t2  V_10 = {0};
	Vector2_t2  V_11 = {0};
	Vector2_t2  V_12 = {0};
	Vector2_t2  V_13 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t709 * L_1 = (List_1_t709 *)il2cpp_codegen_object_new (List_1_t709_il2cpp_TypeInfo_var);
		List_1__ctor_m3807(L_1, /*hidden argument*/List_1__ctor_m3807_MethodInfo_var);
		V_0 = L_1;
		Mesh_t104 * L_2 = ___mesh;
		VertexHelper_t674 * L_3 = (VertexHelper_t674 *)il2cpp_codegen_object_new (VertexHelper_t674_il2cpp_TypeInfo_var);
		VertexHelper__ctor_m3236(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		VertexHelper_t674 * L_4 = V_1;
		List_1_t709 * L_5 = V_0;
		NullCheck(L_4);
		VertexHelper_GetUIVertexStream_m3248(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x32, FINALLY_0025);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		{
			VertexHelper_t674 * L_6 = V_1;
			if (!L_6)
			{
				goto IL_0031;
			}
		}

IL_002b:
		{
			VertexHelper_t674 * L_7 = V_1;
			NullCheck(L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_7);
		}

IL_0031:
		{
			IL2CPP_END_FINALLY(37)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0032:
	{
		List_1_t709 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_8);
		V_2 = ((int32_t)((int32_t)L_9*(int32_t)5));
		List_1_t709 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Capacity_m3808(L_10, /*hidden argument*/List_1_get_Capacity_m3808_MethodInfo_var);
		int32_t L_12 = V_2;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_004e;
		}
	}
	{
		List_1_t709 * L_13 = V_0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		List_1_set_Capacity_m3809(L_13, L_14, /*hidden argument*/List_1_set_Capacity_m3809_MethodInfo_var);
	}

IL_004e:
	{
		V_3 = 0;
		List_1_t709 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_15);
		V_4 = L_16;
		List_1_t709 * L_17 = V_0;
		Color_t9  L_18 = Shadow_get_effectColor_m3260(__this, /*hidden argument*/NULL);
		Color32_t711  L_19 = Color32_op_Implicit_m3518(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_3;
		List_1_t709 * L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_21);
		Vector2_t2  L_23 = Shadow_get_effectDistance_m3262(__this, /*hidden argument*/NULL);
		V_6 = L_23;
		float L_24 = ((&V_6)->___x_1);
		Vector2_t2  L_25 = Shadow_get_effectDistance_m3262(__this, /*hidden argument*/NULL);
		V_7 = L_25;
		float L_26 = ((&V_7)->___y_2);
		Shadow_ApplyShadowZeroAlloc_m3266(__this, L_17, L_19, L_20, L_22, L_24, L_26, /*hidden argument*/NULL);
		int32_t L_27 = V_4;
		V_3 = L_27;
		List_1_t709 * L_28 = V_0;
		NullCheck(L_28);
		int32_t L_29 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_28);
		V_4 = L_29;
		List_1_t709 * L_30 = V_0;
		Color_t9  L_31 = Shadow_get_effectColor_m3260(__this, /*hidden argument*/NULL);
		Color32_t711  L_32 = Color32_op_Implicit_m3518(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		int32_t L_33 = V_3;
		List_1_t709 * L_34 = V_0;
		NullCheck(L_34);
		int32_t L_35 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_34);
		Vector2_t2  L_36 = Shadow_get_effectDistance_m3262(__this, /*hidden argument*/NULL);
		V_8 = L_36;
		float L_37 = ((&V_8)->___x_1);
		Vector2_t2  L_38 = Shadow_get_effectDistance_m3262(__this, /*hidden argument*/NULL);
		V_9 = L_38;
		float L_39 = ((&V_9)->___y_2);
		Shadow_ApplyShadowZeroAlloc_m3266(__this, L_30, L_32, L_33, L_35, L_37, ((-L_39)), /*hidden argument*/NULL);
		int32_t L_40 = V_4;
		V_3 = L_40;
		List_1_t709 * L_41 = V_0;
		NullCheck(L_41);
		int32_t L_42 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_41);
		V_4 = L_42;
		List_1_t709 * L_43 = V_0;
		Color_t9  L_44 = Shadow_get_effectColor_m3260(__this, /*hidden argument*/NULL);
		Color32_t711  L_45 = Color32_op_Implicit_m3518(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		int32_t L_46 = V_3;
		List_1_t709 * L_47 = V_0;
		NullCheck(L_47);
		int32_t L_48 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_47);
		Vector2_t2  L_49 = Shadow_get_effectDistance_m3262(__this, /*hidden argument*/NULL);
		V_10 = L_49;
		float L_50 = ((&V_10)->___x_1);
		Vector2_t2  L_51 = Shadow_get_effectDistance_m3262(__this, /*hidden argument*/NULL);
		V_11 = L_51;
		float L_52 = ((&V_11)->___y_2);
		Shadow_ApplyShadowZeroAlloc_m3266(__this, L_43, L_45, L_46, L_48, ((-L_50)), L_52, /*hidden argument*/NULL);
		int32_t L_53 = V_4;
		V_3 = L_53;
		List_1_t709 * L_54 = V_0;
		NullCheck(L_54);
		int32_t L_55 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_54);
		V_4 = L_55;
		List_1_t709 * L_56 = V_0;
		Color_t9  L_57 = Shadow_get_effectColor_m3260(__this, /*hidden argument*/NULL);
		Color32_t711  L_58 = Color32_op_Implicit_m3518(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		int32_t L_59 = V_3;
		List_1_t709 * L_60 = V_0;
		NullCheck(L_60);
		int32_t L_61 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_60);
		Vector2_t2  L_62 = Shadow_get_effectDistance_m3262(__this, /*hidden argument*/NULL);
		V_12 = L_62;
		float L_63 = ((&V_12)->___x_1);
		Vector2_t2  L_64 = Shadow_get_effectDistance_m3262(__this, /*hidden argument*/NULL);
		V_13 = L_64;
		float L_65 = ((&V_13)->___y_2);
		Shadow_ApplyShadowZeroAlloc_m3266(__this, L_56, L_58, L_59, L_61, ((-L_63)), ((-L_65)), /*hidden argument*/NULL);
		VertexHelper_t674 * L_66 = (VertexHelper_t674 *)il2cpp_codegen_object_new (VertexHelper_t674_il2cpp_TypeInfo_var);
		VertexHelper__ctor_m3235(L_66, /*hidden argument*/NULL);
		V_5 = L_66;
	}

IL_0160:
	try
	{ // begin try (depth: 1)
		VertexHelper_t674 * L_67 = V_5;
		List_1_t709 * L_68 = V_0;
		NullCheck(L_67);
		VertexHelper_AddUIVertexTriangleStream_m3247(L_67, L_68, /*hidden argument*/NULL);
		VertexHelper_t674 * L_69 = V_5;
		Mesh_t104 * L_70 = ___mesh;
		NullCheck(L_69);
		VertexHelper_FillMesh_m3240(L_69, L_70, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x184, FINALLY_0175);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0175;
	}

FINALLY_0175:
	{ // begin finally (depth: 1)
		{
			VertexHelper_t674 * L_71 = V_5;
			if (!L_71)
			{
				goto IL_0183;
			}
		}

IL_017c:
		{
			VertexHelper_t674 * L_72 = V_5;
			NullCheck(L_72);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_72);
		}

IL_0183:
		{
			IL2CPP_END_FINALLY(373)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(373)
	{
		IL2CPP_JUMP_TBL(0x184, IL_0184)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0184:
	{
		return;
	}
}
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1.h"
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1MethodDeclarations.h"
struct List_1_t675;
struct IEnumerable_1_t780;
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" List_1_t675 * Enumerable_ToList_TisVector3_t36_m3810_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_ToList_TisVector3_t36_m3810(__this /* static, unused */, p0, method) (( List_1_t675 * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToList_TisVector3_t36_m3810_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
// UnityEngine.UI.BaseMeshEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffectMethodDeclarations.h"
extern "C" void PositionAsUV1__ctor_m3257 (PositionAsUV1_t685 * __this, const MethodInfo* method)
{
	{
		BaseMeshEffect__ctor_m3250(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.PositionAsUV1::ModifyMesh(UnityEngine.Mesh)
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Vector2>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_3MethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
extern TypeInfo* ListPool_1_t775_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToList_TisVector3_t36_m3810_MethodInfo_var;
extern const MethodInfo* ListPool_1_Get_m3781_MethodInfo_var;
extern const MethodInfo* ListPool_1_Release_m3804_MethodInfo_var;
extern "C" void PositionAsUV1_ModifyMesh_m3258 (PositionAsUV1_t685 * __this, Mesh_t104 * ___mesh, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ListPool_1_t775_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(538);
		Enumerable_ToList_TisVector3_t36_m3810_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484211);
		ListPool_1_Get_m3781_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484195);
		ListPool_1_Release_m3804_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484205);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t675 * V_0 = {0};
	List_1_t677 * V_1 = {0};
	int32_t V_2 = 0;
	Vector3_t36  V_3 = {0};
	Vector3_t36  V_4 = {0};
	Vector3_t36  V_5 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Mesh_t104 * L_1 = ___mesh;
		NullCheck(L_1);
		Vector3U5BU5D_t254* L_2 = Mesh_get_vertices_m1465(L_1, /*hidden argument*/NULL);
		List_1_t675 * L_3 = Enumerable_ToList_TisVector3_t36_m3810(NULL /*static, unused*/, (Object_t*)(Object_t*)L_2, /*hidden argument*/Enumerable_ToList_TisVector3_t36_m3810_MethodInfo_var);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t775_il2cpp_TypeInfo_var);
		List_1_t677 * L_4 = ListPool_1_Get_m3781(NULL /*static, unused*/, /*hidden argument*/ListPool_1_Get_m3781_MethodInfo_var);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0064;
	}

IL_0025:
	{
		List_1_t675 * L_5 = V_0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		Vector3_t36  L_7 = (Vector3_t36 )VirtFuncInvoker1< Vector3_t36 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_5, L_6);
		V_3 = L_7;
		List_1_t677 * L_8 = V_1;
		List_1_t675 * L_9 = V_0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		Vector3_t36  L_11 = (Vector3_t36 )VirtFuncInvoker1< Vector3_t36 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_9, L_10);
		V_4 = L_11;
		float L_12 = ((&V_4)->___x_1);
		List_1_t675 * L_13 = V_0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		Vector3_t36  L_15 = (Vector3_t36 )VirtFuncInvoker1< Vector3_t36 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_13, L_14);
		V_5 = L_15;
		float L_16 = ((&V_5)->___y_2);
		Vector2_t2  L_17 = {0};
		Vector2__ctor_m1309(&L_17, L_12, L_16, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< Vector2_t2  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0) */, L_8, L_17);
		List_1_t675 * L_18 = V_0;
		int32_t L_19 = V_2;
		Vector3_t36  L_20 = V_3;
		NullCheck(L_18);
		VirtActionInvoker2< int32_t, Vector3_t36  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,!0) */, L_18, L_19, L_20);
		int32_t L_21 = V_2;
		V_2 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0064:
	{
		int32_t L_22 = V_2;
		List_1_t675 * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count() */, L_23);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_0025;
		}
	}
	{
		Mesh_t104 * L_25 = ___mesh;
		List_1_t677 * L_26 = V_1;
		NullCheck(L_25);
		Mesh_SetUVs_m3797(L_25, 1, L_26, /*hidden argument*/NULL);
		List_1_t677 * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(ListPool_1_t775_il2cpp_TypeInfo_var);
		ListPool_1_Release_m3804(NULL /*static, unused*/, L_27, /*hidden argument*/ListPool_1_Release_m3804_MethodInfo_var);
		return;
	}
}
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_Shadow.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// System.Void UnityEngine.UI.Shadow::.ctor()
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.UI.BaseMeshEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffectMethodDeclarations.h"
extern "C" void Shadow__ctor_m3259 (Shadow_t684 * __this, const MethodInfo* method)
{
	{
		Color_t9  L_0 = {0};
		Color__ctor_m1327(&L_0, (0.0f), (0.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		__this->___m_EffectColor_3 = L_0;
		Vector2_t2  L_1 = {0};
		Vector2__ctor_m1309(&L_1, (1.0f), (-1.0f), /*hidden argument*/NULL);
		__this->___m_EffectDistance_4 = L_1;
		__this->___m_UseGraphicAlpha_5 = 1;
		BaseMeshEffect__ctor_m3250(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color UnityEngine.UI.Shadow::get_effectColor()
extern "C" Color_t9  Shadow_get_effectColor_m3260 (Shadow_t684 * __this, const MethodInfo* method)
{
	{
		Color_t9  L_0 = (__this->___m_EffectColor_3);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Shadow::set_effectColor(UnityEngine.Color)
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern "C" void Shadow_set_effectColor_m3261 (Shadow_t684 * __this, Color_t9  ___value, const MethodInfo* method)
{
	{
		Color_t9  L_0 = ___value;
		__this->___m_EffectColor_3 = L_0;
		Graphic_t572 * L_1 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_1, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Graphic_t572 * L_3 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_3);
	}

IL_0023:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.UI.Shadow::get_effectDistance()
extern "C" Vector2_t2  Shadow_get_effectDistance_m3262 (Shadow_t684 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___m_EffectDistance_4);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Shadow::set_effectDistance(UnityEngine.Vector2)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
extern "C" void Shadow_set_effectDistance_m3263 (Shadow_t684 * __this, Vector2_t2  ___value, const MethodInfo* method)
{
	{
		float L_0 = ((&___value)->___x_1);
		if ((!(((float)L_0) > ((float)(600.0f)))))
		{
			goto IL_001d;
		}
	}
	{
		(&___value)->___x_1 = (600.0f);
	}

IL_001d:
	{
		float L_1 = ((&___value)->___x_1);
		if ((!(((float)L_1) < ((float)(-600.0f)))))
		{
			goto IL_003a;
		}
	}
	{
		(&___value)->___x_1 = (-600.0f);
	}

IL_003a:
	{
		float L_2 = ((&___value)->___y_2);
		if ((!(((float)L_2) > ((float)(600.0f)))))
		{
			goto IL_0057;
		}
	}
	{
		(&___value)->___y_2 = (600.0f);
	}

IL_0057:
	{
		float L_3 = ((&___value)->___y_2);
		if ((!(((float)L_3) < ((float)(-600.0f)))))
		{
			goto IL_0074;
		}
	}
	{
		(&___value)->___y_2 = (-600.0f);
	}

IL_0074:
	{
		Vector2_t2  L_4 = (__this->___m_EffectDistance_4);
		Vector2_t2  L_5 = ___value;
		bool L_6 = Vector2_op_Equality_m3811(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0086;
		}
	}
	{
		return;
	}

IL_0086:
	{
		Vector2_t2  L_7 = ___value;
		__this->___m_EffectDistance_4 = L_7;
		Graphic_t572 * L_8 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		bool L_9 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_8, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00a9;
		}
	}
	{
		Graphic_t572 * L_10 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_10);
	}

IL_00a9:
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.Shadow::get_useGraphicAlpha()
extern "C" bool Shadow_get_useGraphicAlpha_m3264 (Shadow_t684 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_UseGraphicAlpha_5);
		return L_0;
	}
}
// System.Void UnityEngine.UI.Shadow::set_useGraphicAlpha(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void Shadow_set_useGraphicAlpha_m3265 (Shadow_t684 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_UseGraphicAlpha_5 = L_0;
		Graphic_t572 * L_1 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_1, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Graphic_t572 * L_3 = BaseMeshEffect_get_graphic_m3251(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(21 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_3);
	}

IL_0023:
	{
		return;
	}
}
// System.Void UnityEngine.UI.Shadow::ApplyShadowZeroAlloc(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color32,System.Int32,System.Int32,System.Single,System.Single)
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_26.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Single
#include "mscorlib_System_Single.h"
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_26MethodDeclarations.h"
extern const MethodInfo* List_1_get_Capacity_m3808_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m3809_MethodInfo_var;
extern "C" void Shadow_ApplyShadowZeroAlloc_m3266 (Shadow_t684 * __this, List_1_t709 * ___verts, Color32_t711  ___color, int32_t ___start, int32_t ___end, float ___x, float ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_get_Capacity_m3808_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484209);
		List_1_set_Capacity_m3809_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484210);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t605  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t36  V_3 = {0};
	Color32_t711  V_4 = {0};
	UIVertex_t605  V_5 = {0};
	{
		List_1_t709 * L_0 = ___verts;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_0);
		V_1 = ((int32_t)((int32_t)L_1*(int32_t)2));
		List_1_t709 * L_2 = ___verts;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Capacity_m3808(L_2, /*hidden argument*/List_1_get_Capacity_m3808_MethodInfo_var);
		int32_t L_4 = V_1;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_001c;
		}
	}
	{
		List_1_t709 * L_5 = ___verts;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		List_1_set_Capacity_m3809(L_5, L_6, /*hidden argument*/List_1_set_Capacity_m3809_MethodInfo_var);
	}

IL_001c:
	{
		int32_t L_7 = ___start;
		V_2 = L_7;
		goto IL_00b0;
	}

IL_0023:
	{
		List_1_t709 * L_8 = ___verts;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		UIVertex_t605  L_10 = (UIVertex_t605 )VirtFuncInvoker1< UIVertex_t605 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_8, L_9);
		V_0 = L_10;
		List_1_t709 * L_11 = ___verts;
		UIVertex_t605  L_12 = V_0;
		NullCheck(L_11);
		VirtActionInvoker1< UIVertex_t605  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(!0) */, L_11, L_12);
		Vector3_t36  L_13 = ((&V_0)->___position_0);
		V_3 = L_13;
		Vector3_t36 * L_14 = (&V_3);
		float L_15 = (L_14->___x_1);
		float L_16 = ___x;
		L_14->___x_1 = ((float)((float)L_15+(float)L_16));
		Vector3_t36 * L_17 = (&V_3);
		float L_18 = (L_17->___y_2);
		float L_19 = ___y;
		L_17->___y_2 = ((float)((float)L_18+(float)L_19));
		Vector3_t36  L_20 = V_3;
		(&V_0)->___position_0 = L_20;
		Color32_t711  L_21 = ___color;
		V_4 = L_21;
		bool L_22 = (__this->___m_UseGraphicAlpha_5);
		if (!L_22)
		{
			goto IL_009b;
		}
	}
	{
		uint8_t L_23 = ((&V_4)->___a_3);
		List_1_t709 * L_24 = ___verts;
		int32_t L_25 = V_2;
		NullCheck(L_24);
		UIVertex_t605  L_26 = (UIVertex_t605 )VirtFuncInvoker1< UIVertex_t605 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32) */, L_24, L_25);
		V_5 = L_26;
		Color32_t711 * L_27 = &((&V_5)->___color_2);
		uint8_t L_28 = (L_27->___a_3);
		(&V_4)->___a_3 = (((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_23*(int32_t)L_28))/(int32_t)((int32_t)255)))));
	}

IL_009b:
	{
		Color32_t711  L_29 = V_4;
		(&V_0)->___color_2 = L_29;
		List_1_t709 * L_30 = ___verts;
		int32_t L_31 = V_2;
		UIVertex_t605  L_32 = V_0;
		NullCheck(L_30);
		VirtActionInvoker2< int32_t, UIVertex_t605  >::Invoke(32 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,!0) */, L_30, L_31, L_32);
		int32_t L_33 = V_2;
		V_2 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b0:
	{
		int32_t L_34 = V_2;
		int32_t L_35 = ___end;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_0023;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.Shadow::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color32,System.Int32,System.Int32,System.Single,System.Single)
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_ShadowMethodDeclarations.h"
extern const MethodInfo* List_1_get_Capacity_m3808_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m3809_MethodInfo_var;
extern "C" void Shadow_ApplyShadow_m3267 (Shadow_t684 * __this, List_1_t709 * ___verts, Color32_t711  ___color, int32_t ___start, int32_t ___end, float ___x, float ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_get_Capacity_m3808_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484209);
		List_1_set_Capacity_m3809_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484210);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t709 * L_0 = ___verts;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_0);
		V_0 = ((int32_t)((int32_t)L_1*(int32_t)2));
		List_1_t709 * L_2 = ___verts;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Capacity_m3808(L_2, /*hidden argument*/List_1_get_Capacity_m3808_MethodInfo_var);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_001c;
		}
	}
	{
		List_1_t709 * L_5 = ___verts;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		List_1_set_Capacity_m3809(L_5, L_6, /*hidden argument*/List_1_set_Capacity_m3809_MethodInfo_var);
	}

IL_001c:
	{
		List_1_t709 * L_7 = ___verts;
		Color32_t711  L_8 = ___color;
		int32_t L_9 = ___start;
		int32_t L_10 = ___end;
		float L_11 = ___x;
		float L_12 = ___y;
		Shadow_ApplyShadowZeroAlloc_m3266(__this, L_7, L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Shadow::ModifyMesh(UnityEngine.Mesh)
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityEngine.UI.VertexHelper
#include "UnityEngine_UI_UnityEngine_UI_VertexHelperMethodDeclarations.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
extern TypeInfo* List_1_t709_il2cpp_TypeInfo_var;
extern TypeInfo* VertexHelper_t674_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3807_MethodInfo_var;
extern "C" void Shadow_ModifyMesh_m3268 (Shadow_t684 * __this, Mesh_t104 * ___mesh, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(541);
		VertexHelper_t674_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(440);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		List_1__ctor_m3807_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484208);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t709 * V_0 = {0};
	VertexHelper_t674 * V_1 = {0};
	VertexHelper_t674 * V_2 = {0};
	Vector2_t2  V_3 = {0};
	Vector2_t2  V_4 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive() */, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t709 * L_1 = (List_1_t709 *)il2cpp_codegen_object_new (List_1_t709_il2cpp_TypeInfo_var);
		List_1__ctor_m3807(L_1, /*hidden argument*/List_1__ctor_m3807_MethodInfo_var);
		V_0 = L_1;
		Mesh_t104 * L_2 = ___mesh;
		VertexHelper_t674 * L_3 = (VertexHelper_t674 *)il2cpp_codegen_object_new (VertexHelper_t674_il2cpp_TypeInfo_var);
		VertexHelper__ctor_m3236(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		VertexHelper_t674 * L_4 = V_1;
		List_1_t709 * L_5 = V_0;
		NullCheck(L_4);
		VertexHelper_GetUIVertexStream_m3248(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x32, FINALLY_0025);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		{
			VertexHelper_t674 * L_6 = V_1;
			if (!L_6)
			{
				goto IL_0031;
			}
		}

IL_002b:
		{
			VertexHelper_t674 * L_7 = V_1;
			NullCheck(L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_7);
		}

IL_0031:
		{
			IL2CPP_END_FINALLY(37)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0032:
	{
		List_1_t709 * L_8 = V_0;
		Color_t9  L_9 = Shadow_get_effectColor_m3260(__this, /*hidden argument*/NULL);
		Color32_t711  L_10 = Color32_op_Implicit_m3518(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		List_1_t709 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_11);
		Vector2_t2  L_13 = Shadow_get_effectDistance_m3262(__this, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = ((&V_3)->___x_1);
		Vector2_t2  L_15 = Shadow_get_effectDistance_m3262(__this, /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = ((&V_4)->___y_2);
		Shadow_ApplyShadow_m3267(__this, L_8, L_10, 0, L_12, L_14, L_16, /*hidden argument*/NULL);
		VertexHelper_t674 * L_17 = (VertexHelper_t674 *)il2cpp_codegen_object_new (VertexHelper_t674_il2cpp_TypeInfo_var);
		VertexHelper__ctor_m3235(L_17, /*hidden argument*/NULL);
		V_2 = L_17;
	}

IL_006e:
	try
	{ // begin try (depth: 1)
		VertexHelper_t674 * L_18 = V_2;
		List_1_t709 * L_19 = V_0;
		NullCheck(L_18);
		VertexHelper_AddUIVertexTriangleStream_m3247(L_18, L_19, /*hidden argument*/NULL);
		VertexHelper_t674 * L_20 = V_2;
		Mesh_t104 * L_21 = ___mesh;
		NullCheck(L_20);
		VertexHelper_FillMesh_m3240(L_20, L_21, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x8E, FINALLY_0081);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0081;
	}

FINALLY_0081:
	{ // begin finally (depth: 1)
		{
			VertexHelper_t674 * L_22 = V_2;
			if (!L_22)
			{
				goto IL_008d;
			}
		}

IL_0087:
		{
			VertexHelper_t674 * L_23 = V_2;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_23);
		}

IL_008d:
		{
			IL2CPP_END_FINALLY(129)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(129)
	{
		IL2CPP_JUMP_TBL(0x8E, IL_008e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_008e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
