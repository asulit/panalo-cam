﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.RectangleData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_96.h"
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26529_gshared (InternalEnumerator_1_t3356 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26529(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3356 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26529_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.RectangleData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26530_gshared (InternalEnumerator_1_t3356 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26530(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3356 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26530_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26531_gshared (InternalEnumerator_1_t3356 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26531(__this, method) (( void (*) (InternalEnumerator_1_t3356 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26531_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.RectangleData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26532_gshared (InternalEnumerator_1_t3356 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26532(__this, method) (( bool (*) (InternalEnumerator_1_t3356 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26532_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.RectangleData>::get_Current()
extern "C" RectangleData_t1089  InternalEnumerator_1_get_Current_m26533_gshared (InternalEnumerator_1_t3356 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26533(__this, method) (( RectangleData_t1089  (*) (InternalEnumerator_1_t3356 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26533_gshared)(__this, method)
