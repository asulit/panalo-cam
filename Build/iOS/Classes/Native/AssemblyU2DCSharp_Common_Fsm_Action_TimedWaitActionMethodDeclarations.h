﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.Action.TimedWaitAction
struct TimedWaitAction_t45;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.String
struct String_t;

// System.Void Common.Fsm.Action.TimedWaitAction::.ctor(Common.Fsm.FsmState,System.String,System.String)
extern "C" void TimedWaitAction__ctor_m145 (TimedWaitAction_t45 * __this, Object_t * ___owner, String_t* ___timeReferenceName, String_t* ___finishEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.TimedWaitAction::Init(System.Single)
extern "C" void TimedWaitAction_Init_m146 (TimedWaitAction_t45 * __this, float ___waitTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.TimedWaitAction::OnEnter()
extern "C" void TimedWaitAction_OnEnter_m147 (TimedWaitAction_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.TimedWaitAction::OnUpdate()
extern "C" void TimedWaitAction_OnUpdate_m148 (TimedWaitAction_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.TimedWaitAction::Finish()
extern "C" void TimedWaitAction_Finish_m149 (TimedWaitAction_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Common.Fsm.Action.TimedWaitAction::GetRatio()
extern "C" float TimedWaitAction_GetRatio_m150 (TimedWaitAction_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
