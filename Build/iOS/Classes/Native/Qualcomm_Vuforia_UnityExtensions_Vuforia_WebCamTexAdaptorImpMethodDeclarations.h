﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamTexAdaptorImpl
struct WebCamTexAdaptorImpl_t1176;
// UnityEngine.Texture
struct Texture_t103;
// System.String
struct String_t;
// Vuforia.VuforiaRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec.h"

// System.Boolean Vuforia.WebCamTexAdaptorImpl::get_DidUpdateThisFrame()
extern "C" bool WebCamTexAdaptorImpl_get_DidUpdateThisFrame_m5846 (WebCamTexAdaptorImpl_t1176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamTexAdaptorImpl::get_IsPlaying()
extern "C" bool WebCamTexAdaptorImpl_get_IsPlaying_m5847 (WebCamTexAdaptorImpl_t1176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture Vuforia.WebCamTexAdaptorImpl::get_Texture()
extern "C" Texture_t103 * WebCamTexAdaptorImpl_get_Texture_m5848 (WebCamTexAdaptorImpl_t1176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::.ctor(System.String,System.Int32,Vuforia.VuforiaRenderer/Vec2I)
extern "C" void WebCamTexAdaptorImpl__ctor_m5849 (WebCamTexAdaptorImpl_t1176 * __this, String_t* ___deviceName, int32_t ___requestedFPS, Vec2I_t1157  ___requestedTextureSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::Play()
extern "C" void WebCamTexAdaptorImpl_Play_m5850 (WebCamTexAdaptorImpl_t1176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::Stop()
extern "C" void WebCamTexAdaptorImpl_Stop_m5851 (WebCamTexAdaptorImpl_t1176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
