﻿#pragma once
#include <stdint.h>
// UnityEngine.CanvasGroup
struct CanvasGroup_t733;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.CanvasGroup>
struct  Comparison_1_t2999  : public MulticastDelegate_t28
{
};
