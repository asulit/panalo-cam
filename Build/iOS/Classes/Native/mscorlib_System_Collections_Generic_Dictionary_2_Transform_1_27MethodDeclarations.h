﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ERaffleResult,System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>
struct Transform_1_t2719;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ERaffleResult,System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m17684_gshared (Transform_1_t2719 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m17684(__this, ___object, ___method, method) (( void (*) (Transform_1_t2719 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m17684_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ERaffleResult,System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t352  Transform_1_Invoke_m17685_gshared (Transform_1_t2719 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Transform_1_Invoke_m17685(__this, ___key, ___value, method) (( KeyValuePair_2_t352  (*) (Transform_1_t2719 *, int32_t, int32_t, const MethodInfo*))Transform_1_Invoke_m17685_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ERaffleResult,System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m17686_gshared (Transform_1_t2719 * __this, int32_t ___key, int32_t ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m17686(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2719 *, int32_t, int32_t, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m17686_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ERaffleResult,System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t352  Transform_1_EndInvoke_m17687_gshared (Transform_1_t2719 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m17687(__this, ___result, method) (( KeyValuePair_2_t352  (*) (Transform_1_t2719 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m17687_gshared)(__this, ___result, method)
