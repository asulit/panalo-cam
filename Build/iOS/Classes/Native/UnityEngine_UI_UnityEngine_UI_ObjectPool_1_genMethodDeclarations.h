﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_0MethodDeclarations.h"
#define ObjectPool_1__ctor_m3303(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t506 *, UnityAction_1_t508 *, UnityAction_1_t508 *, const MethodInfo*))ObjectPool_1__ctor_m18958_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countAll()
#define ObjectPool_1_get_countAll_m18959(__this, method) (( int32_t (*) (ObjectPool_1_t506 *, const MethodInfo*))ObjectPool_1_get_countAll_m18960_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m18961(__this, ___value, method) (( void (*) (ObjectPool_1_t506 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m18962_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countActive()
#define ObjectPool_1_get_countActive_m18963(__this, method) (( int32_t (*) (ObjectPool_1_t506 *, const MethodInfo*))ObjectPool_1_get_countActive_m18964_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m18965(__this, method) (( int32_t (*) (ObjectPool_1_t506 *, const MethodInfo*))ObjectPool_1_get_countInactive_m18966_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Get()
#define ObjectPool_1_Get_m18967(__this, method) (( List_1_t686 * (*) (ObjectPool_1_t506 *, const MethodInfo*))ObjectPool_1_Get_m18968_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Release(T)
#define ObjectPool_1_Release_m18969(__this, ___element, method) (( void (*) (ObjectPool_1_t506 *, List_1_t686 *, const MethodInfo*))ObjectPool_1_Release_m18970_gshared)(__this, ___element, method)
