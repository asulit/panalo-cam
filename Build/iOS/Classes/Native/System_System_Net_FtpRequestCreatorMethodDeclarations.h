﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FtpRequestCreator
struct FtpRequestCreator_t1628;
// System.Net.WebRequest
struct WebRequest_t1588;
// System.Uri
struct Uri_t1583;

// System.Void System.Net.FtpRequestCreator::.ctor()
extern "C" void FtpRequestCreator__ctor_m8683 (FtpRequestCreator_t1628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FtpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t1588 * FtpRequestCreator_Create_m8684 (FtpRequestCreator_t1628 * __this, Uri_t1583 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
