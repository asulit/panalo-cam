﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<AnimatorFrame>
struct Comparer_1_t2778;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<AnimatorFrame>
struct  Comparer_1_t2778  : public Object_t
{
};
struct Comparer_1_t2778_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<AnimatorFrame>::_default
	Comparer_1_t2778 * ____default_0;
};
