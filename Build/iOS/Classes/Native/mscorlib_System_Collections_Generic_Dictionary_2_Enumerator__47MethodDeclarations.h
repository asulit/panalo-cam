﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22MethodDeclarations.h"
#define Enumerator__ctor_m28635(__this, ___dictionary, method) (( void (*) (Enumerator_t3473 *, Dictionary_2_t1217 *, const MethodInfo*))Enumerator__ctor_m19836_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m28636(__this, method) (( Object_t * (*) (Enumerator_t3473 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19837_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m28637(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3473 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19838_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m28638(__this, method) (( Object_t * (*) (Enumerator_t3473 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19839_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m28639(__this, method) (( Object_t * (*) (Enumerator_t3473 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19840_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::MoveNext()
#define Enumerator_MoveNext_m28640(__this, method) (( bool (*) (Enumerator_t3473 *, const MethodInfo*))Enumerator_MoveNext_m19841_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::get_Current()
#define Enumerator_get_Current_m28641(__this, method) (( KeyValuePair_2_t3471  (*) (Enumerator_t3473 *, const MethodInfo*))Enumerator_get_Current_m19842_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m28642(__this, method) (( int32_t (*) (Enumerator_t3473 *, const MethodInfo*))Enumerator_get_CurrentKey_m19843_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m28643(__this, method) (( Object_t * (*) (Enumerator_t3473 *, const MethodInfo*))Enumerator_get_CurrentValue_m19844_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::VerifyState()
#define Enumerator_VerifyState_m28644(__this, method) (( void (*) (Enumerator_t3473 *, const MethodInfo*))Enumerator_VerifyState_m19845_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m28645(__this, method) (( void (*) (Enumerator_t3473 *, const MethodInfo*))Enumerator_VerifyCurrent_m19846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::Dispose()
#define Enumerator_Dispose_m28646(__this, method) (( void (*) (Enumerator_t3473 *, const MethodInfo*))Enumerator_Dispose_m19847_gshared)(__this, method)
