﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::.ctor()
// System.Collections.Generic.Queue`1<System.Object>
#include "System_System_Collections_Generic_Queue_1_gen_1MethodDeclarations.h"
#define Queue_1__ctor_m1450(__this, method) (( void (*) (Queue_1_t77 *, const MethodInfo*))Queue_1__ctor_m15872_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m15905(__this, ___array, ___idx, method) (( void (*) (Queue_1_t77 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m15874_gshared)(__this, ___array, ___idx, method)
// System.Boolean System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m15906(__this, method) (( bool (*) (Queue_1_t77 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m15876_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m15907(__this, method) (( Object_t * (*) (Queue_1_t77 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m15878_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15908(__this, method) (( Object_t* (*) (Queue_1_t77 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15880_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m15909(__this, method) (( Object_t * (*) (Queue_1_t77 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m15882_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m15910(__this, ___array, ___idx, method) (( void (*) (Queue_1_t77 *, Queue_1U5BU5D_t2571*, int32_t, const MethodInfo*))Queue_1_CopyTo_m15884_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::Dequeue()
#define Queue_1_Dequeue_m1459(__this, method) (( Queue_1_t76 * (*) (Queue_1_t77 *, const MethodInfo*))Queue_1_Dequeue_m15885_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::Peek()
#define Queue_1_Peek_m15911(__this, method) (( Queue_1_t76 * (*) (Queue_1_t77 *, const MethodInfo*))Queue_1_Peek_m15887_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::Enqueue(T)
#define Queue_1_Enqueue_m1455(__this, ___item, method) (( void (*) (Queue_1_t77 *, Queue_1_t76 *, const MethodInfo*))Queue_1_Enqueue_m15888_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m15912(__this, ___new_size, method) (( void (*) (Queue_1_t77 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m15890_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::get_Count()
#define Queue_1_get_Count_m15913(__this, method) (( int32_t (*) (Queue_1_t77 *, const MethodInfo*))Queue_1_get_Count_m15892_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::GetEnumerator()
#define Queue_1_GetEnumerator_m15914(__this, method) (( Enumerator_t2572  (*) (Queue_1_t77 *, const MethodInfo*))Queue_1_GetEnumerator_m15894_gshared)(__this, method)
