﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t635;

// System.Void UnityEngine.UI.Slider/SliderEvent::.ctor()
extern "C" void SliderEvent__ctor_m2901 (SliderEvent_t635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
