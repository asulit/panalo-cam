﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SecurityCriticalAttribute
struct SecurityCriticalAttribute_t2252;

// System.Void System.Security.SecurityCriticalAttribute::.ctor()
extern "C" void SecurityCriticalAttribute__ctor_m13337 (SecurityCriticalAttribute_t2252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
