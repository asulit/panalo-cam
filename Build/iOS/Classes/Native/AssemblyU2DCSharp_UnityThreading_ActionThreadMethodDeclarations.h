﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.ActionThread
struct ActionThread_t171;
// System.Action`1<UnityThreading.ActionThread>
struct Action_1_t172;
// System.Collections.IEnumerator
struct IEnumerator_t337;

// System.Void UnityThreading.ActionThread::.ctor(System.Action`1<UnityThreading.ActionThread>)
extern "C" void ActionThread__ctor_m595 (ActionThread_t171 * __this, Action_1_t172 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.ActionThread::.ctor(System.Action`1<UnityThreading.ActionThread>,System.Boolean)
extern "C" void ActionThread__ctor_m596 (ActionThread_t171 * __this, Action_1_t172 * ___action, bool ___autoStartThread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityThreading.ActionThread::Do()
extern "C" Object_t * ActionThread_Do_m597 (ActionThread_t171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
