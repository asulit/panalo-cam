﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t2881;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2869;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m19883_gshared (ShimEnumerator_t2881 * __this, Dictionary_2_t2869 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m19883(__this, ___host, method) (( void (*) (ShimEnumerator_t2881 *, Dictionary_2_t2869 *, const MethodInfo*))ShimEnumerator__ctor_m19883_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m19884_gshared (ShimEnumerator_t2881 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m19884(__this, method) (( bool (*) (ShimEnumerator_t2881 *, const MethodInfo*))ShimEnumerator_MoveNext_m19884_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m19885_gshared (ShimEnumerator_t2881 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m19885(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t2881 *, const MethodInfo*))ShimEnumerator_get_Entry_m19885_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m19886_gshared (ShimEnumerator_t2881 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m19886(__this, method) (( Object_t * (*) (ShimEnumerator_t2881 *, const MethodInfo*))ShimEnumerator_get_Key_m19886_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m19887_gshared (ShimEnumerator_t2881 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m19887(__this, method) (( Object_t * (*) (ShimEnumerator_t2881 *, const MethodInfo*))ShimEnumerator_get_Value_m19887_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m19888_gshared (ShimEnumerator_t2881 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m19888(__this, method) (( Object_t * (*) (ShimEnumerator_t2881 *, const MethodInfo*))ShimEnumerator_get_Current_m19888_gshared)(__this, method)
