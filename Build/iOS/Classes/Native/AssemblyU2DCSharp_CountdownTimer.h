﻿#pragma once
#include <stdint.h>
// Common.Time.TimeReference
struct TimeReference_t14;
// System.Object
#include "mscorlib_System_Object.h"
// CountdownTimer
struct  CountdownTimer_t13  : public Object_t
{
	// System.Single CountdownTimer::polledTime
	float ___polledTime_0;
	// System.Single CountdownTimer::countdownTime
	float ___countdownTime_1;
	// Common.Time.TimeReference CountdownTimer::timeReference
	TimeReference_t14 * ___timeReference_2;
};
