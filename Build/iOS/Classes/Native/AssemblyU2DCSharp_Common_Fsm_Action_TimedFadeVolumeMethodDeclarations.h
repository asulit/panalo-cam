﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.Action.TimedFadeVolume
struct TimedFadeVolume_t43;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t44;

// System.Void Common.Fsm.Action.TimedFadeVolume::.ctor(Common.Fsm.FsmState,System.String)
extern "C" void TimedFadeVolume__ctor_m140 (TimedFadeVolume_t43 * __this, Object_t * ___owner, String_t* ___timeReferenceName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.TimedFadeVolume::Init(UnityEngine.AudioSource,System.Single,System.Single,System.Single,System.String)
extern "C" void TimedFadeVolume_Init_m141 (TimedFadeVolume_t43 * __this, AudioSource_t44 * ___audioSource, float ___volumeFrom, float ___volumeTo, float ___duration, String_t* ___finishEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.TimedFadeVolume::OnEnter()
extern "C" void TimedFadeVolume_OnEnter_m142 (TimedFadeVolume_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.TimedFadeVolume::OnUpdate()
extern "C" void TimedFadeVolume_OnUpdate_m143 (TimedFadeVolume_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.TimedFadeVolume::Finish()
extern "C" void TimedFadeVolume_Finish_m144 (TimedFadeVolume_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
