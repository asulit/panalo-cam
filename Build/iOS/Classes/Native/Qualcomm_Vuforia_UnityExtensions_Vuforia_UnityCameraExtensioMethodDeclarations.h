﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Camera
struct Camera_t6;

// System.Int32 Vuforia.UnityCameraExtensions::GetPixelHeightInt(UnityEngine.Camera)
extern "C" int32_t UnityCameraExtensions_GetPixelHeightInt_m5287 (Object_t * __this /* static, unused */, Camera_t6 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.UnityCameraExtensions::GetPixelWidthInt(UnityEngine.Camera)
extern "C" int32_t UnityCameraExtensions_GetPixelWidthInt_m5288 (Object_t * __this /* static, unused */, Camera_t6 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.UnityCameraExtensions::GetMaxDepthForVideoBackground(UnityEngine.Camera)
extern "C" float UnityCameraExtensions_GetMaxDepthForVideoBackground_m5289 (Object_t * __this /* static, unused */, Camera_t6 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.UnityCameraExtensions::GetMinDepthForVideoBackground(UnityEngine.Camera)
extern "C" float UnityCameraExtensions_GetMinDepthForVideoBackground_m5290 (Object_t * __this /* static, unused */, Camera_t6 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
