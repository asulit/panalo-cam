﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t955;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"

// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern "C" Score_t955 * GcScoreData_ToScore_m4931 (GcScoreData_t940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void GcScoreData_t940_marshal(const GcScoreData_t940& unmarshaled, GcScoreData_t940_marshaled& marshaled);
extern "C" void GcScoreData_t940_marshal_back(const GcScoreData_t940_marshaled& marshaled, GcScoreData_t940& unmarshaled);
extern "C" void GcScoreData_t940_marshal_cleanup(GcScoreData_t940_marshaled& marshaled);
