﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.Contexts.IDynamicProperty
struct IDynamicProperty_t2079;
// System.Runtime.Remoting.Contexts.IDynamicMessageSink
struct IDynamicMessageSink_t2080;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Contexts.DynamicPropertyCollection/DynamicPropertyReg
struct  DynamicPropertyReg_t2078  : public Object_t
{
	// System.Runtime.Remoting.Contexts.IDynamicProperty System.Runtime.Remoting.Contexts.DynamicPropertyCollection/DynamicPropertyReg::Property
	Object_t * ___Property_0;
	// System.Runtime.Remoting.Contexts.IDynamicMessageSink System.Runtime.Remoting.Contexts.DynamicPropertyCollection/DynamicPropertyReg::Sink
	Object_t * ___Sink_1;
};
