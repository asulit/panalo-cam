﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t2556;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2544;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m15714_gshared (ShimEnumerator_t2556 * __this, Dictionary_2_t2544 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m15714(__this, ___host, method) (( void (*) (ShimEnumerator_t2556 *, Dictionary_2_t2544 *, const MethodInfo*))ShimEnumerator__ctor_m15714_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m15715_gshared (ShimEnumerator_t2556 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m15715(__this, method) (( bool (*) (ShimEnumerator_t2556 *, const MethodInfo*))ShimEnumerator_MoveNext_m15715_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m15716_gshared (ShimEnumerator_t2556 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m15716(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t2556 *, const MethodInfo*))ShimEnumerator_get_Entry_m15716_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m15717_gshared (ShimEnumerator_t2556 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m15717(__this, method) (( Object_t * (*) (ShimEnumerator_t2556 *, const MethodInfo*))ShimEnumerator_get_Key_m15717_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m15718_gshared (ShimEnumerator_t2556 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m15718(__this, method) (( Object_t * (*) (ShimEnumerator_t2556 *, const MethodInfo*))ShimEnumerator_get_Value_m15718_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m15719_gshared (ShimEnumerator_t2556 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m15719(__this, method) (( Object_t * (*) (ShimEnumerator_t2556 *, const MethodInfo*))ShimEnumerator_get_Current_m15719_gshared)(__this, method)
