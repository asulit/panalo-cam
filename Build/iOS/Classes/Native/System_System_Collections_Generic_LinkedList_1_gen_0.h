﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<SwarmItem>
struct LinkedListNode_1_t408;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.LinkedList`1<SwarmItem>
struct  LinkedList_1_t152  : public Object_t
{
	// System.UInt32 System.Collections.Generic.LinkedList`1<SwarmItem>::count
	uint32_t ___count_2;
	// System.UInt32 System.Collections.Generic.LinkedList`1<SwarmItem>::version
	uint32_t ___version_3;
	// System.Object System.Collections.Generic.LinkedList`1<SwarmItem>::syncRoot
	Object_t * ___syncRoot_4;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<SwarmItem>::first
	LinkedListNode_1_t408 * ___first_5;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1<SwarmItem>::si
	SerializationInfo_t1012 * ___si_6;
};
