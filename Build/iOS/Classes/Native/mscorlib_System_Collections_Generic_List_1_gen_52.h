﻿#pragma once
#include <stdint.h>
// Vuforia.HideExcessAreaAbstractBehaviour[]
struct HideExcessAreaAbstractBehaviourU5BU5D_t1069;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct  List_1_t1070  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour>::_items
	HideExcessAreaAbstractBehaviourU5BU5D_t1069* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour>::_version
	int32_t ____version_3;
};
struct List_1_t1070_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour>::EmptyArray
	HideExcessAreaAbstractBehaviourU5BU5D_t1069* ___EmptyArray_4;
};
