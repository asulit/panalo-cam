﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Events.UnityAction`1<System.Int32>
struct  UnityAction_1_t2904  : public MulticastDelegate_t28
{
};
