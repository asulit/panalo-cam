﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Func`1<System.Collections.IEnumerator>::.ctor(System.Object,System.IntPtr)
// System.Func`1<System.Object>
#include "System_Core_System_Func_1_gen_0MethodDeclarations.h"
#define Func_1__ctor_m17077(__this, ___object, ___method, method) (( void (*) (Func_1_t179 *, Object_t *, IntPtr_t, const MethodInfo*))Func_1__ctor_m16948_gshared)(__this, ___object, ___method, method)
// TResult System.Func`1<System.Collections.IEnumerator>::Invoke()
#define Func_1_Invoke_m1616(__this, method) (( Object_t * (*) (Func_1_t179 *, const MethodInfo*))Func_1_Invoke_m16949_gshared)(__this, method)
// System.IAsyncResult System.Func`1<System.Collections.IEnumerator>::BeginInvoke(System.AsyncCallback,System.Object)
#define Func_1_BeginInvoke_m17078(__this, ___callback, ___object, method) (( Object_t * (*) (Func_1_t179 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Func_1_BeginInvoke_m16950_gshared)(__this, ___callback, ___object, method)
// TResult System.Func`1<System.Collections.IEnumerator>::EndInvoke(System.IAsyncResult)
#define Func_1_EndInvoke_m17079(__this, ___result, method) (( Object_t * (*) (Func_1_t179 *, Object_t *, const MethodInfo*))Func_1_EndInvoke_m16951_gshared)(__this, ___result, method)
