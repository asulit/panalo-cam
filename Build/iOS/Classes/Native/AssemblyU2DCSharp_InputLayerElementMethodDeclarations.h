﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InputLayerElement
struct InputLayerElement_t58;
// UnityEngine.Camera
struct Camera_t6;
// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_t343;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void InputLayerElement::.ctor()
extern "C" void InputLayerElement__ctor_m206 (InputLayerElement_t58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputLayerElement::RespondsToTouchPosition(UnityEngine.Vector3)
extern "C" bool InputLayerElement_RespondsToTouchPosition_m207 (InputLayerElement_t58 * __this, Vector3_t36  ___touchPos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera InputLayerElement::get_ReferenceCamera()
extern "C" Camera_t6 * InputLayerElement_get_ReferenceCamera_m208 (InputLayerElement_t58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputLayerElement::SetColliders(System.Collections.Generic.List`1<UnityEngine.Collider>)
extern "C" void InputLayerElement_SetColliders_m209 (InputLayerElement_t58 * __this, List_1_t343 * ___colliderList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
