﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t261;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.SoapServices
struct  SoapServices_t2156  : public Object_t
{
};
struct SoapServices_t2156_StaticFields{
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices::_xmlTypes
	Hashtable_t261 * ____xmlTypes_0;
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices::_xmlElements
	Hashtable_t261 * ____xmlElements_1;
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices::_soapActions
	Hashtable_t261 * ____soapActions_2;
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices::_soapActionsMethods
	Hashtable_t261 * ____soapActionsMethods_3;
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices::_typeInfos
	Hashtable_t261 * ____typeInfos_4;
};
