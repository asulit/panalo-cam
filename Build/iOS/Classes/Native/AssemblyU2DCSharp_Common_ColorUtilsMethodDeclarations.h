﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void Common.ColorUtils::.cctor()
extern "C" void ColorUtils__cctor_m34 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Common.ColorUtils::NewColor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" Color_t9  ColorUtils_NewColor_m35 (Object_t * __this /* static, unused */, int32_t ___r, int32_t ___g, int32_t ___b, int32_t ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
