﻿#pragma once
#include <stdint.h>
// SelfManagingSwarmItemManager
struct SelfManagingSwarmItemManager_t99;
// PrefabManager/PruneData[]
struct PruneDataU5BU5D_t98;
// PrefabManager/PreloadData[]
struct PreloadDataU5BU5D_t100;
// System.Collections.Generic.IDictionary`2<System.String,System.Int32>
struct IDictionary_2_t101;
// CountdownTimer
struct CountdownTimer_t13;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// PrefabManager
struct  PrefabManager_t96  : public MonoBehaviour_t5
{
	// SelfManagingSwarmItemManager PrefabManager::itemManager
	SelfManagingSwarmItemManager_t99 * ___itemManager_2;
	// PrefabManager/PruneData[] PrefabManager::pruneDataList
	PruneDataU5BU5D_t98* ___pruneDataList_3;
	// PrefabManager/PreloadData[] PrefabManager::preloadDataList
	PreloadDataU5BU5D_t100* ___preloadDataList_4;
	// System.Single PrefabManager::pruneIntervalTime
	float ___pruneIntervalTime_5;
	// System.Collections.Generic.IDictionary`2<System.String,System.Int32> PrefabManager::nameToIndexMapping
	Object_t* ___nameToIndexMapping_6;
	// CountdownTimer PrefabManager::pruneTimer
	CountdownTimer_t13 * ___pruneTimer_7;
};
