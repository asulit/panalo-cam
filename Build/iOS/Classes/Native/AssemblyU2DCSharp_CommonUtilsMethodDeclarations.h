﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Transform
struct Transform_t35;
// UnityEngine.GameObject
struct GameObject_t155;
// System.String
struct String_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void CommonUtils::SetAsParent(UnityEngine.Transform,UnityEngine.Transform)
extern "C" void CommonUtils_SetAsParent_m40 (Object_t * __this /* static, unused */, Transform_t35 * ___parentTransform, Transform_t35 * ___childTransform, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommonUtils::SeedRandomizer()
extern "C" void CommonUtils_SeedRandomizer_m41 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CommonUtils::RandomBoolean()
extern "C" bool CommonUtils_RandomBoolean_m42 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommonUtils::HideObject(UnityEngine.GameObject,System.Boolean)
extern "C" void CommonUtils_HideObject_m43 (Object_t * __this /* static, unused */, GameObject_t155 * ___gameObject, bool ___recurseChildren, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommonUtils::ShowObject(UnityEngine.GameObject,System.Boolean)
extern "C" void CommonUtils_ShowObject_m44 (Object_t * __this /* static, unused */, GameObject_t155 * ___gameObject, bool ___recurseChildren, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CommonUtils::Clamp(System.Single,System.Single,System.Single)
extern "C" float CommonUtils_Clamp_m45 (Object_t * __this /* static, unused */, float ___aValue, float ___min, float ___max, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CommonUtils::Clamp(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CommonUtils_Clamp_m46 (Object_t * __this /* static, unused */, int32_t ___aValue, int32_t ___min, int32_t ___max, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CommonUtils::IsEmpty(System.String)
extern "C" bool CommonUtils_IsEmpty_m47 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CommonUtils::ContainsObjectWithName(UnityEngine.Transform,System.String)
extern "C" bool CommonUtils_ContainsObjectWithName_m48 (Object_t * __this /* static, unused */, Transform_t35 * ___transform, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform CommonUtils::FindTransformByName(UnityEngine.Transform,System.String)
extern "C" Transform_t35 * CommonUtils_FindTransformByName_m49 (Object_t * __this /* static, unused */, Transform_t35 * ___transformRoot, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CommonUtils::ComputeTravelTime(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" float CommonUtils_ComputeTravelTime_m50 (Object_t * __this /* static, unused */, Vector3_t36  ___start, Vector3_t36  ___destination, float ___velocity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CommonUtils::TolerantEquals(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool CommonUtils_TolerantEquals_m51 (Object_t * __this /* static, unused */, Vector3_t36  ___a, Vector3_t36  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CommonUtils::RandomSign()
extern "C" float CommonUtils_RandomSign_m52 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommonUtils::SetAllScriptsEnabled(UnityEngine.GameObject,System.Boolean,System.Boolean)
extern "C" void CommonUtils_SetAllScriptsEnabled_m53 (Object_t * __this /* static, unused */, GameObject_t155 * ___gameObject, bool ___enabled, bool ___traverseChildren, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommonUtils::SetLayer(UnityEngine.GameObject,System.String,System.Boolean)
extern "C" void CommonUtils_SetLayer_m54 (Object_t * __this /* static, unused */, GameObject_t155 * ___go, String_t* ___layerName, bool ___recurseToChildren, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommonUtils::SetLayer(UnityEngine.GameObject,System.Int32,System.Boolean)
extern "C" void CommonUtils_SetLayer_m55 (Object_t * __this /* static, unused */, GameObject_t155 * ___go, int32_t ___layer, bool ___recurseToChildren, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CommonUtils::ReadTextFile(System.String)
extern "C" String_t* CommonUtils_ReadTextFile_m56 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CommonUtils::SetChildrenActiveRecursively(UnityEngine.GameObject,System.Boolean)
extern "C" void CommonUtils_SetChildrenActiveRecursively_m57 (Object_t * __this /* static, unused */, GameObject_t155 * ___gameObject, bool ___active, const MethodInfo* method) IL2CPP_METHOD_ATTR;
