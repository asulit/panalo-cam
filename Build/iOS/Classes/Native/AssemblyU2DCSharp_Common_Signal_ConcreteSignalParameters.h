﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t86;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Signal.ConcreteSignalParameters
struct  ConcreteSignalParameters_t113  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Common.Signal.ConcreteSignalParameters::parameterMap
	Dictionary_2_t86 * ___parameterMap_0;
};
