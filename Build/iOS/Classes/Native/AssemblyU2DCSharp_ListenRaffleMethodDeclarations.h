﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ListenRaffle
struct ListenRaffle_t331;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void ListenRaffle::.ctor(System.Object,System.IntPtr)
extern "C" void ListenRaffle__ctor_m1289 (ListenRaffle_t331 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ListenRaffle::Invoke(System.Int32)
extern "C" void ListenRaffle_Invoke_m1290 (ListenRaffle_t331 * __this, int32_t ___raffleId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ListenRaffle_t331(Il2CppObject* delegate, int32_t ___raffleId);
// System.IAsyncResult ListenRaffle::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C" Object_t * ListenRaffle_BeginInvoke_m1291 (ListenRaffle_t331 * __this, int32_t ___raffleId, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ListenRaffle::EndInvoke(System.IAsyncResult)
extern "C" void ListenRaffle_EndInvoke_m1292 (ListenRaffle_t331 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
