﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>
struct ReadOnlyCollection_1_t2726;
// System.Collections.Generic.IList`1<Vuforia.CameraDevice/FocusMode>
struct IList_1_t2727;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// Vuforia.CameraDevice/FocusMode[]
struct FocusModeU5BU5D_t2723;
// System.Collections.Generic.IEnumerator`1<Vuforia.CameraDevice/FocusMode>
struct IEnumerator_1_t3664;
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m17764_gshared (ReadOnlyCollection_1_t2726 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m17764(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2726 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m17764_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17765_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17765(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2726 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17765_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17766_gshared (ReadOnlyCollection_1_t2726 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17766(__this, method) (( void (*) (ReadOnlyCollection_1_t2726 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m17766_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17767_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17767(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2726 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m17767_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17768_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17768(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2726 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m17768_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17769_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17769(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2726 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m17769_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17770_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17770(__this, ___index, method) (( int32_t (*) (ReadOnlyCollection_1_t2726 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m17770_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17771_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17771(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2726 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m17771_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17772_gshared (ReadOnlyCollection_1_t2726 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17772(__this, method) (( bool (*) (ReadOnlyCollection_1_t2726 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17772_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17773_gshared (ReadOnlyCollection_1_t2726 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17773(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2726 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17773_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17774_gshared (ReadOnlyCollection_1_t2726 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17774(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2726 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m17774_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m17775_gshared (ReadOnlyCollection_1_t2726 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m17775(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2726 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m17775_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m17776_gshared (ReadOnlyCollection_1_t2726 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m17776(__this, method) (( void (*) (ReadOnlyCollection_1_t2726 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m17776_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m17777_gshared (ReadOnlyCollection_1_t2726 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m17777(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2726 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m17777_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17778_gshared (ReadOnlyCollection_1_t2726 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17778(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2726 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m17778_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m17779_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m17779(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2726 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m17779_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m17780_gshared (ReadOnlyCollection_1_t2726 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m17780(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2726 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m17780_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17781_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17781(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2726 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m17781_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17782_gshared (ReadOnlyCollection_1_t2726 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17782(__this, method) (( bool (*) (ReadOnlyCollection_1_t2726 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m17782_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17783_gshared (ReadOnlyCollection_1_t2726 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17783(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2726 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m17783_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17784_gshared (ReadOnlyCollection_1_t2726 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17784(__this, method) (( bool (*) (ReadOnlyCollection_1_t2726 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m17784_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17785_gshared (ReadOnlyCollection_1_t2726 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17785(__this, method) (( bool (*) (ReadOnlyCollection_1_t2726 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m17785_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m17786_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m17786(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2726 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m17786_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m17787_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m17787(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2726 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m17787_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m17788_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m17788(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2726 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m17788_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m17789_gshared (ReadOnlyCollection_1_t2726 * __this, FocusModeU5BU5D_t2723* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m17789(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2726 *, FocusModeU5BU5D_t2723*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m17789_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m17790_gshared (ReadOnlyCollection_1_t2726 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m17790(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2726 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m17790_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m17791_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m17791(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2726 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m17791_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m17792_gshared (ReadOnlyCollection_1_t2726 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m17792(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2726 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m17792_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::get_Item(System.Int32)
extern "C" int32_t ReadOnlyCollection_1_get_Item_m17793_gshared (ReadOnlyCollection_1_t2726 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m17793(__this, ___index, method) (( int32_t (*) (ReadOnlyCollection_1_t2726 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m17793_gshared)(__this, ___index, method)
