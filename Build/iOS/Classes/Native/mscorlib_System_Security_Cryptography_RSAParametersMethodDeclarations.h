﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void RSAParameters_t1533_marshal(const RSAParameters_t1533& unmarshaled, RSAParameters_t1533_marshaled& marshaled);
extern "C" void RSAParameters_t1533_marshal_back(const RSAParameters_t1533_marshaled& marshaled, RSAParameters_t1533& unmarshaled);
extern "C" void RSAParameters_t1533_marshal_cleanup(RSAParameters_t1533_marshaled& marshaled);
