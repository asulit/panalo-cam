﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.UI.InputField[]
// UnityEngine.UI.InputField[]
struct  InputFieldU5BU5D_t2735  : public Array_t
{
};
struct InputFieldU5BU5D_t2735_StaticFields{
};
// UnityEngine.UI.Image[]
// UnityEngine.UI.Image[]
struct  ImageU5BU5D_t2764  : public Array_t
{
};
struct ImageU5BU5D_t2764_StaticFields{
};
// UnityEngine.EventSystems.BaseInputModule[]
// UnityEngine.EventSystems.BaseInputModule[]
struct  BaseInputModuleU5BU5D_t2801  : public Array_t
{
};
// UnityEngine.EventSystems.IEventSystemHandler[]
// UnityEngine.EventSystems.IEventSystemHandler[]
struct  IEventSystemHandlerU5BU5D_t2807  : public Array_t
{
};
// UnityEngine.EventSystems.RaycastResult[]
// UnityEngine.EventSystems.RaycastResult[]
struct  RaycastResultU5BU5D_t2831  : public Array_t
{
};
// UnityEngine.EventSystems.BaseRaycaster[]
// UnityEngine.EventSystems.BaseRaycaster[]
struct  BaseRaycasterU5BU5D_t2842  : public Array_t
{
};
// UnityEngine.EventSystems.EventTrigger/Entry[]
// UnityEngine.EventSystems.EventTrigger/Entry[]
struct  EntryU5BU5D_t2848  : public Array_t
{
};
// UnityEngine.EventSystems.PointerEventData[]
// UnityEngine.EventSystems.PointerEventData[]
struct  PointerEventDataU5BU5D_t2867  : public Array_t
{
};
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
struct  ButtonStateU5BU5D_t2883  : public Array_t
{
};
// UnityEngine.UI.ICanvasElement[]
// UnityEngine.UI.ICanvasElement[]
struct  ICanvasElementU5BU5D_t2895  : public Array_t
{
};
// UnityEngine.UI.Dropdown/OptionData[]
// UnityEngine.UI.Dropdown/OptionData[]
struct  OptionDataU5BU5D_t2898  : public Array_t
{
};
// UnityEngine.UI.Dropdown/DropdownItem[]
// UnityEngine.UI.Dropdown/DropdownItem[]
struct  DropdownItemU5BU5D_t2906  : public Array_t
{
};
// UnityEngine.UI.Text[]
// UnityEngine.UI.Text[]
struct  TextU5BU5D_t2928  : public Array_t
{
};
struct TextU5BU5D_t2928_StaticFields{
};
// UnityEngine.UI.Graphic[]
// UnityEngine.UI.Graphic[]
struct  GraphicU5BU5D_t2949  : public Array_t
{
};
struct GraphicU5BU5D_t2949_StaticFields{
};
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
struct  IndexedSet_1U5BU5D_t2953  : public Array_t
{
};
// UnityEngine.UI.InputField/ContentType[]
// UnityEngine.UI.InputField/ContentType[]
struct  ContentTypeU5BU5D_t712  : public Array_t
{
};
// UnityEngine.UI.RectMask2D[]
// UnityEngine.UI.RectMask2D[]
struct  RectMask2DU5BU5D_t2974  : public Array_t
{
};
// UnityEngine.UI.IClippable[]
// UnityEngine.UI.IClippable[]
struct  IClippableU5BU5D_t2980  : public Array_t
{
};
// UnityEngine.UI.Selectable[]
// UnityEngine.UI.Selectable[]
struct  SelectableU5BU5D_t2988  : public Array_t
{
};
struct SelectableU5BU5D_t2988_StaticFields{
};
// UnityEngine.UI.StencilMaterial/MatEntry[]
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct  MatEntryU5BU5D_t3000  : public Array_t
{
};
// UnityEngine.UI.Toggle[]
// UnityEngine.UI.Toggle[]
struct  ToggleU5BU5D_t3006  : public Array_t
{
};
// UnityEngine.UI.IClipper[]
// UnityEngine.UI.IClipper[]
struct  IClipperU5BU5D_t3012  : public Array_t
{
};
