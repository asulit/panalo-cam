﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RotationInRadians
struct RotationInRadians_t128;

// System.Void RotationInRadians::.ctor()
extern "C" void RotationInRadians__ctor_m436 (RotationInRadians_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
