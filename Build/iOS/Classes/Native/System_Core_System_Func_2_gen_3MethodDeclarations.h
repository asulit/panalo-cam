﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Single>
#include "System_Core_System_Func_2_gen_6MethodDeclarations.h"
#define Func_2__ctor_m3777(__this, ___object, ___method, method) (( void (*) (Func_2_t672 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m22108_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::Invoke(T)
#define Func_2_Invoke_m3778(__this, ___arg1, method) (( float (*) (Func_2_t672 *, Object_t *, const MethodInfo*))Func_2_Invoke_m22109_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m22110(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t672 *, Object_t *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m22111_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m22112(__this, ___result, method) (( float (*) (Func_2_t672 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m22113_gshared)(__this, ___result, method)
