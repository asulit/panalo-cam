﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.UI.IClipper>
struct List_1_t3011;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>
struct Dictionary_2_t765;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>
struct  IndexedSet_1_t648  : public Object_t
{
	// System.Collections.Generic.List`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::m_List
	List_1_t3011 * ___m_List_0;
	// System.Collections.Generic.Dictionary`2<T,System.Int32> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::m_Dictionary
	Dictionary_2_t765 * ___m_Dictionary_1;
};
