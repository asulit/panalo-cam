﻿#pragma once
#include <stdint.h>
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.CLSCompliantAttribute
struct  CLSCompliantAttribute_t1767  : public Attribute_t463
{
	// System.Boolean System.CLSCompliantAttribute::is_compliant
	bool ___is_compliant_0;
};
