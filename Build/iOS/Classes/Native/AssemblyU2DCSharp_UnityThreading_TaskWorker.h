﻿#pragma once
#include <stdint.h>
// UnityThreading.Dispatcher
struct Dispatcher_t162;
// UnityThreading.TaskDistributor
struct TaskDistributor_t166;
// UnityThreading.ThreadBase
#include "AssemblyU2DCSharp_UnityThreading_ThreadBase.h"
// UnityThreading.TaskWorker
struct  TaskWorker_t168  : public ThreadBase_t169
{
	// UnityThreading.Dispatcher UnityThreading.TaskWorker::Dispatcher
	Dispatcher_t162 * ___Dispatcher_4;
	// UnityThreading.TaskDistributor UnityThreading.TaskWorker::TaskDistributor
	TaskDistributor_t166 * ___TaskDistributor_5;
};
