﻿#pragma once
#include <stdint.h>
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_t1765;
// System.Object
#include "mscorlib_System_Object.h"
// System.MonoCustomAttrs/AttributeInfo
struct  AttributeInfo_t2354  : public Object_t
{
	// System.AttributeUsageAttribute System.MonoCustomAttrs/AttributeInfo::_usage
	AttributeUsageAttribute_t1765 * ____usage_0;
	// System.Int32 System.MonoCustomAttrs/AttributeInfo::_inheritanceLevel
	int32_t ____inheritanceLevel_1;
};
