﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3335;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_50.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m26410_gshared (Enumerator_t3341 * __this, Dictionary_2_t3335 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m26410(__this, ___host, method) (( void (*) (Enumerator_t3341 *, Dictionary_2_t3335 *, const MethodInfo*))Enumerator__ctor_m26410_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26411_gshared (Enumerator_t3341 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26411(__this, method) (( Object_t * (*) (Enumerator_t3341 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26411_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m26412_gshared (Enumerator_t3341 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m26412(__this, method) (( void (*) (Enumerator_t3341 *, const MethodInfo*))Enumerator_Dispose_m26412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26413_gshared (Enumerator_t3341 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26413(__this, method) (( bool (*) (Enumerator_t3341 *, const MethodInfo*))Enumerator_MoveNext_m26413_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m26414_gshared (Enumerator_t3341 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26414(__this, method) (( Object_t * (*) (Enumerator_t3341 *, const MethodInfo*))Enumerator_get_Current_m26414_gshared)(__this, method)
