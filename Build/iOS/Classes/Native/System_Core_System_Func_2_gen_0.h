﻿#pragma once
#include <stdint.h>
// UnityThreading.ThreadBase
struct ThreadBase_t169;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityThreading.ThreadBase,System.Boolean>
struct  Func_2_t183  : public MulticastDelegate_t28
{
};
