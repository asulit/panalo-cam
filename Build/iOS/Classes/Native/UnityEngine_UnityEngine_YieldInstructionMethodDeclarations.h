﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.YieldInstruction
struct YieldInstruction_t791;
struct YieldInstruction_t791_marshaled;

// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m4396 (YieldInstruction_t791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void YieldInstruction_t791_marshal(const YieldInstruction_t791& unmarshaled, YieldInstruction_t791_marshaled& marshaled);
extern "C" void YieldInstruction_t791_marshal_back(const YieldInstruction_t791_marshaled& marshaled, YieldInstruction_t791& unmarshaled);
extern "C" void YieldInstruction_t791_marshal_cleanup(YieldInstruction_t791_marshaled& marshaled);
