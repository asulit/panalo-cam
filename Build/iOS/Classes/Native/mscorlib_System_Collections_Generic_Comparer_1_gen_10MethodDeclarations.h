﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct Comparer_1_t3147;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Comparer_1__ctor_m23625_gshared (Comparer_1_t3147 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m23625(__this, method) (( void (*) (Comparer_1_t3147 *, const MethodInfo*))Comparer_1__ctor_m23625_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void Comparer_1__cctor_m23626_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m23626(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m23626_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m23627_gshared (Comparer_1_t3147 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m23627(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3147 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m23627_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::get_Default()
extern "C" Comparer_1_t3147 * Comparer_1_get_Default_m23628_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m23628(__this /* static, unused */, method) (( Comparer_1_t3147 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m23628_gshared)(__this /* static, unused */, method)
