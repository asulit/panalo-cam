﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ESubScreens>
struct DefaultComparer_t2703;
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ESubScreens>::.ctor()
extern "C" void DefaultComparer__ctor_m17512_gshared (DefaultComparer_t2703 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m17512(__this, method) (( void (*) (DefaultComparer_t2703 *, const MethodInfo*))DefaultComparer__ctor_m17512_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ESubScreens>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m17513_gshared (DefaultComparer_t2703 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m17513(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2703 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m17513_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ESubScreens>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m17514_gshared (DefaultComparer_t2703 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m17514(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2703 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m17514_gshared)(__this, ___x, ___y, method)
