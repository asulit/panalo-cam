﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU248_t2396_marshal(const U24ArrayTypeU248_t2396& unmarshaled, U24ArrayTypeU248_t2396_marshaled& marshaled);
extern "C" void U24ArrayTypeU248_t2396_marshal_back(const U24ArrayTypeU248_t2396_marshaled& marshaled, U24ArrayTypeU248_t2396& unmarshaled);
extern "C" void U24ArrayTypeU248_t2396_marshal_cleanup(U24ArrayTypeU248_t2396_marshaled& marshaled);
