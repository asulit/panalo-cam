﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Int32>
struct Collection_1_t3072;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Int32[]
struct Int32U5BU5D_t401;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3626;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t3071;

// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::.ctor()
extern "C" void Collection_1__ctor_m22785_gshared (Collection_1_t3072 * __this, const MethodInfo* method);
#define Collection_1__ctor_m22785(__this, method) (( void (*) (Collection_1_t3072 *, const MethodInfo*))Collection_1__ctor_m22785_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22786_gshared (Collection_1_t3072 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22786(__this, method) (( bool (*) (Collection_1_t3072 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22786_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m22787_gshared (Collection_1_t3072 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m22787(__this, ___array, ___index, method) (( void (*) (Collection_1_t3072 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m22787_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m22788_gshared (Collection_1_t3072 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m22788(__this, method) (( Object_t * (*) (Collection_1_t3072 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m22788_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m22789_gshared (Collection_1_t3072 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m22789(__this, ___value, method) (( int32_t (*) (Collection_1_t3072 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m22789_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m22790_gshared (Collection_1_t3072 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m22790(__this, ___value, method) (( bool (*) (Collection_1_t3072 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m22790_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m22791_gshared (Collection_1_t3072 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m22791(__this, ___value, method) (( int32_t (*) (Collection_1_t3072 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m22791_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m22792_gshared (Collection_1_t3072 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m22792(__this, ___index, ___value, method) (( void (*) (Collection_1_t3072 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m22792_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m22793_gshared (Collection_1_t3072 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m22793(__this, ___value, method) (( void (*) (Collection_1_t3072 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m22793_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m22794_gshared (Collection_1_t3072 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m22794(__this, method) (( bool (*) (Collection_1_t3072 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m22794_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m22795_gshared (Collection_1_t3072 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m22795(__this, method) (( Object_t * (*) (Collection_1_t3072 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m22795_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m22796_gshared (Collection_1_t3072 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m22796(__this, method) (( bool (*) (Collection_1_t3072 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m22796_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m22797_gshared (Collection_1_t3072 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m22797(__this, method) (( bool (*) (Collection_1_t3072 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m22797_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m22798_gshared (Collection_1_t3072 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m22798(__this, ___index, method) (( Object_t * (*) (Collection_1_t3072 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m22798_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m22799_gshared (Collection_1_t3072 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m22799(__this, ___index, ___value, method) (( void (*) (Collection_1_t3072 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m22799_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Add(T)
extern "C" void Collection_1_Add_m22800_gshared (Collection_1_t3072 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m22800(__this, ___item, method) (( void (*) (Collection_1_t3072 *, int32_t, const MethodInfo*))Collection_1_Add_m22800_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Clear()
extern "C" void Collection_1_Clear_m22801_gshared (Collection_1_t3072 * __this, const MethodInfo* method);
#define Collection_1_Clear_m22801(__this, method) (( void (*) (Collection_1_t3072 *, const MethodInfo*))Collection_1_Clear_m22801_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::ClearItems()
extern "C" void Collection_1_ClearItems_m22802_gshared (Collection_1_t3072 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m22802(__this, method) (( void (*) (Collection_1_t3072 *, const MethodInfo*))Collection_1_ClearItems_m22802_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Contains(T)
extern "C" bool Collection_1_Contains_m22803_gshared (Collection_1_t3072 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m22803(__this, ___item, method) (( bool (*) (Collection_1_t3072 *, int32_t, const MethodInfo*))Collection_1_Contains_m22803_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m22804_gshared (Collection_1_t3072 * __this, Int32U5BU5D_t401* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m22804(__this, ___array, ___index, method) (( void (*) (Collection_1_t3072 *, Int32U5BU5D_t401*, int32_t, const MethodInfo*))Collection_1_CopyTo_m22804_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Int32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m22805_gshared (Collection_1_t3072 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m22805(__this, method) (( Object_t* (*) (Collection_1_t3072 *, const MethodInfo*))Collection_1_GetEnumerator_m22805_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m22806_gshared (Collection_1_t3072 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m22806(__this, ___item, method) (( int32_t (*) (Collection_1_t3072 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m22806_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m22807_gshared (Collection_1_t3072 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m22807(__this, ___index, ___item, method) (( void (*) (Collection_1_t3072 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m22807_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m22808_gshared (Collection_1_t3072 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m22808(__this, ___index, ___item, method) (( void (*) (Collection_1_t3072 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m22808_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Remove(T)
extern "C" bool Collection_1_Remove_m22809_gshared (Collection_1_t3072 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m22809(__this, ___item, method) (( bool (*) (Collection_1_t3072 *, int32_t, const MethodInfo*))Collection_1_Remove_m22809_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m22810_gshared (Collection_1_t3072 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m22810(__this, ___index, method) (( void (*) (Collection_1_t3072 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m22810_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m22811_gshared (Collection_1_t3072 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m22811(__this, ___index, method) (( void (*) (Collection_1_t3072 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m22811_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m22812_gshared (Collection_1_t3072 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m22812(__this, method) (( int32_t (*) (Collection_1_t3072 *, const MethodInfo*))Collection_1_get_Count_m22812_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m22813_gshared (Collection_1_t3072 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m22813(__this, ___index, method) (( int32_t (*) (Collection_1_t3072 *, int32_t, const MethodInfo*))Collection_1_get_Item_m22813_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m22814_gshared (Collection_1_t3072 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m22814(__this, ___index, ___value, method) (( void (*) (Collection_1_t3072 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m22814_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m22815_gshared (Collection_1_t3072 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m22815(__this, ___index, ___item, method) (( void (*) (Collection_1_t3072 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m22815_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m22816_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m22816(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m22816_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m22817_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m22817(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m22817_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m22818_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m22818(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m22818_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m22819_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m22819(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m22819_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m22820_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m22820(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m22820_gshared)(__this /* static, unused */, ___list, method)
