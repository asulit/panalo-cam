﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Utils.SimpleList`1/Visitor<System.Object>
struct Visitor_t2600;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Common.Utils.SimpleList`1/Visitor<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Visitor__ctor_m16195_gshared (Visitor_t2600 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Visitor__ctor_m16195(__this, ___object, ___method, method) (( void (*) (Visitor_t2600 *, Object_t *, IntPtr_t, const MethodInfo*))Visitor__ctor_m16195_gshared)(__this, ___object, ___method, method)
// System.Void Common.Utils.SimpleList`1/Visitor<System.Object>::Invoke(T)
extern "C" void Visitor_Invoke_m16196_gshared (Visitor_t2600 * __this, Object_t * ___item, const MethodInfo* method);
#define Visitor_Invoke_m16196(__this, ___item, method) (( void (*) (Visitor_t2600 *, Object_t *, const MethodInfo*))Visitor_Invoke_m16196_gshared)(__this, ___item, method)
// System.IAsyncResult Common.Utils.SimpleList`1/Visitor<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Visitor_BeginInvoke_m16197_gshared (Visitor_t2600 * __this, Object_t * ___item, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Visitor_BeginInvoke_m16197(__this, ___item, ___callback, ___object, method) (( Object_t * (*) (Visitor_t2600 *, Object_t *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Visitor_BeginInvoke_m16197_gshared)(__this, ___item, ___callback, ___object, method)
// System.Void Common.Utils.SimpleList`1/Visitor<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void Visitor_EndInvoke_m16198_gshared (Visitor_t2600 * __this, Object_t * ___result, const MethodInfo* method);
#define Visitor_EndInvoke_m16198(__this, ___result, method) (( void (*) (Visitor_t2600 *, Object_t *, const MethodInfo*))Visitor_EndInvoke_m16198_gshared)(__this, ___result, method)
