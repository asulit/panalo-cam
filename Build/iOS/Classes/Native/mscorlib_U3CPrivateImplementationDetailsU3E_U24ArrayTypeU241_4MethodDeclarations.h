﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU241024_t2402_marshal(const U24ArrayTypeU241024_t2402& unmarshaled, U24ArrayTypeU241024_t2402_marshaled& marshaled);
extern "C" void U24ArrayTypeU241024_t2402_marshal_back(const U24ArrayTypeU241024_t2402_marshaled& marshaled, U24ArrayTypeU241024_t2402& unmarshaled);
extern "C" void U24ArrayTypeU241024_t2402_marshal_cleanup(U24ArrayTypeU241024_t2402_marshaled& marshaled);
