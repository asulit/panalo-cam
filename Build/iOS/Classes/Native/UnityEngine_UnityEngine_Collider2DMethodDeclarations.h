﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Collider2D
struct Collider2D_t730;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t868;

// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C" Rigidbody2D_t868 * Collider2D_get_attachedRigidbody_m4459 (Collider2D_t730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
