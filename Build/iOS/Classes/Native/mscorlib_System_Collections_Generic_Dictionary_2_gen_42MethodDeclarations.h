﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2544;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2506;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.ICollection
struct ICollection_t1751;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3624;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t3625;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1606;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>
struct KeyCollection_t2548;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int32>
struct ValueCollection_t2552;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__8.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m15545_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m15545(__this, method) (( void (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2__ctor_m15545_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m15547_gshared (Dictionary_2_t2544 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m15547(__this, ___comparer, method) (( void (*) (Dictionary_2_t2544 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15547_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m15548_gshared (Dictionary_2_t2544 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m15548(__this, ___capacity, method) (( void (*) (Dictionary_2_t2544 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m15548_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m15550_gshared (Dictionary_2_t2544 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m15550(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2544 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m15550_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m15552_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m15552(__this, method) (( Object_t * (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m15552_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m15554_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m15554(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2544 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m15554_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m15556_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m15556(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2544 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m15556_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m15558_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m15558(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2544 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m15558_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m15560_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m15560(__this, ___key, method) (( bool (*) (Dictionary_2_t2544 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m15560_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m15562_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m15562(__this, ___key, method) (( void (*) (Dictionary_2_t2544 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m15562_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15564_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15564(__this, method) (( bool (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15564_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15566_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15566(__this, method) (( Object_t * (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15566_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15568_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15568(__this, method) (( bool (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15568_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15570_gshared (Dictionary_2_t2544 * __this, KeyValuePair_2_t2546  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15570(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2544 *, KeyValuePair_2_t2546 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15570_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15572_gshared (Dictionary_2_t2544 * __this, KeyValuePair_2_t2546  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15572(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2544 *, KeyValuePair_2_t2546 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15572_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15574_gshared (Dictionary_2_t2544 * __this, KeyValuePair_2U5BU5D_t3624* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15574(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2544 *, KeyValuePair_2U5BU5D_t3624*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15574_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15576_gshared (Dictionary_2_t2544 * __this, KeyValuePair_2_t2546  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15576(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2544 *, KeyValuePair_2_t2546 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15576_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m15578_gshared (Dictionary_2_t2544 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m15578(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2544 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m15578_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15580_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15580(__this, method) (( Object_t * (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15580_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15582_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15582(__this, method) (( Object_t* (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15582_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15584_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15584(__this, method) (( Object_t * (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15584_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m15586_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m15586(__this, method) (( int32_t (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_get_Count_m15586_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m15588_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m15588(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2544 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m15588_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m15590_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m15590(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2544 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m15590_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m15592_gshared (Dictionary_2_t2544 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m15592(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2544 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m15592_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m15594_gshared (Dictionary_2_t2544 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m15594(__this, ___size, method) (( void (*) (Dictionary_2_t2544 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m15594_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m15596_gshared (Dictionary_2_t2544 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m15596(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2544 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m15596_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2546  Dictionary_2_make_pair_m15598_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m15598(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2546  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m15598_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m15600_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m15600(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m15600_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m15602_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m15602(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m15602_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m15604_gshared (Dictionary_2_t2544 * __this, KeyValuePair_2U5BU5D_t3624* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m15604(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2544 *, KeyValuePair_2U5BU5D_t3624*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m15604_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m15606_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m15606(__this, method) (( void (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_Resize_m15606_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m15608_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m15608(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2544 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m15608_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m15610_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m15610(__this, method) (( void (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_Clear_m15610_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m15612_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m15612(__this, ___key, method) (( bool (*) (Dictionary_2_t2544 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m15612_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m15614_gshared (Dictionary_2_t2544 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m15614(__this, ___value, method) (( bool (*) (Dictionary_2_t2544 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m15614_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m15616_gshared (Dictionary_2_t2544 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m15616(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2544 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m15616_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m15618_gshared (Dictionary_2_t2544 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m15618(__this, ___sender, method) (( void (*) (Dictionary_2_t2544 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m15618_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m15620_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m15620(__this, ___key, method) (( bool (*) (Dictionary_2_t2544 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m15620_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m15622_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m15622(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2544 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m15622_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Keys()
extern "C" KeyCollection_t2548 * Dictionary_2_get_Keys_m15624_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m15624(__this, method) (( KeyCollection_t2548 * (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_get_Keys_m15624_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Values()
extern "C" ValueCollection_t2552 * Dictionary_2_get_Values_m15626_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m15626(__this, method) (( ValueCollection_t2552 * (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_get_Values_m15626_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m15628_gshared (Dictionary_2_t2544 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m15628(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2544 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m15628_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m15630_gshared (Dictionary_2_t2544 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m15630(__this, ___value, method) (( int32_t (*) (Dictionary_2_t2544 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m15630_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m15632_gshared (Dictionary_2_t2544 * __this, KeyValuePair_2_t2546  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m15632(__this, ___pair, method) (( bool (*) (Dictionary_2_t2544 *, KeyValuePair_2_t2546 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m15632_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetEnumerator()
extern "C" Enumerator_t2550  Dictionary_2_GetEnumerator_m15634_gshared (Dictionary_2_t2544 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m15634(__this, method) (( Enumerator_t2550  (*) (Dictionary_2_t2544 *, const MethodInfo*))Dictionary_2_GetEnumerator_m15634_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m15636_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m15636(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m15636_gshared)(__this /* static, unused */, ___key, ___value, method)
