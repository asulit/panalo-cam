﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WritableAttribute
struct WritableAttribute_t935;

// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C" void WritableAttribute__ctor_m4925 (WritableAttribute_t935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
