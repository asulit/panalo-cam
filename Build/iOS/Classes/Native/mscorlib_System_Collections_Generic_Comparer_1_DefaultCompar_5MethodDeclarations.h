﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>
struct DefaultComparer_t3047;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>::.ctor()
extern "C" void DefaultComparer__ctor_m22403_gshared (DefaultComparer_t3047 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22403(__this, method) (( void (*) (DefaultComparer_t3047 *, const MethodInfo*))DefaultComparer__ctor_m22403_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color32>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m22404_gshared (DefaultComparer_t3047 * __this, Color32_t711  ___x, Color32_t711  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m22404(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3047 *, Color32_t711 , Color32_t711 , const MethodInfo*))DefaultComparer_Compare_m22404_gshared)(__this, ___x, ___y, method)
