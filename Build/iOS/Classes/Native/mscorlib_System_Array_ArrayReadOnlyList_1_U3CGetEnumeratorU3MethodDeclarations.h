﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t3557;
// System.Object
struct Object_t;

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m29691_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3557 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m29691(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3557 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m29691_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29692_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3557 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29692(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3557 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m29692_gshared)(__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29693_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3557 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29693(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3557 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m29693_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29694_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3557 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29694(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t3557 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m29694_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29695_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3557 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29695(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3557 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m29695_gshared)(__this, method)
