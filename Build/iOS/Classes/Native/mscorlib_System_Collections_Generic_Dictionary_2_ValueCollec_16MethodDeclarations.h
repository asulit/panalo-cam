﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_53MethodDeclarations.h"
#define ValueCollection__ctor_m26602(__this, ___dictionary, method) (( void (*) (ValueCollection_t1321 *, Dictionary_2_t1182 *, const MethodInfo*))ValueCollection__ctor_m19852_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26603(__this, ___item, method) (( void (*) (ValueCollection_t1321 *, WordResult_t1188 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19853_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26604(__this, method) (( void (*) (ValueCollection_t1321 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19854_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26605(__this, ___item, method) (( bool (*) (ValueCollection_t1321 *, WordResult_t1188 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19855_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26606(__this, ___item, method) (( bool (*) (ValueCollection_t1321 *, WordResult_t1188 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19856_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26607(__this, method) (( Object_t* (*) (ValueCollection_t1321 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19857_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m26608(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1321 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m19858_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26609(__this, method) (( Object_t * (*) (ValueCollection_t1321 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19859_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26610(__this, method) (( bool (*) (ValueCollection_t1321 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19860_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26611(__this, method) (( bool (*) (ValueCollection_t1321 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19861_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m26612(__this, method) (( Object_t * (*) (ValueCollection_t1321 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m19862_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m26613(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1321 *, WordResultU5BU5D_t3357*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m19863_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::GetEnumerator()
#define ValueCollection_GetEnumerator_m26614(__this, method) (( Enumerator_t3768  (*) (ValueCollection_t1321 *, const MethodInfo*))ValueCollection_GetEnumerator_m19864_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordResult>::get_Count()
#define ValueCollection_get_Count_m26615(__this, method) (( int32_t (*) (ValueCollection_t1321 *, const MethodInfo*))ValueCollection_get_Count_m19865_gshared)(__this, method)
