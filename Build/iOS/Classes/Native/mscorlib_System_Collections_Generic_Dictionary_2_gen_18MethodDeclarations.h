﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_42MethodDeclarations.h"
#define Dictionary_2__ctor_m21906(__this, method) (( void (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2__ctor_m15545_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m21907(__this, ___comparer, method) (( void (*) (Dictionary_2_t765 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15547_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::.ctor(System.Int32)
#define Dictionary_2__ctor_m21908(__this, ___capacity, method) (( void (*) (Dictionary_2_t765 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m15548_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m21909(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t765 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m15550_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m21910(__this, method) (( Object_t * (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m15552_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m21911(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t765 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m15554_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m21912(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t765 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m15556_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m21913(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t765 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m15558_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m21914(__this, ___key, method) (( bool (*) (Dictionary_2_t765 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m15560_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m21915(__this, ___key, method) (( void (*) (Dictionary_2_t765 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m15562_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21916(__this, method) (( bool (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15564_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21917(__this, method) (( Object_t * (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15566_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21918(__this, method) (( bool (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15568_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21919(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t765 *, KeyValuePair_2_t3017 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15570_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21920(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t765 *, KeyValuePair_2_t3017 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15572_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21921(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t765 *, KeyValuePair_2U5BU5D_t3708*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15574_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21922(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t765 *, KeyValuePair_2_t3017 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15576_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m21923(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t765 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m15578_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21924(__this, method) (( Object_t * (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15580_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21925(__this, method) (( Object_t* (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15582_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21926(__this, method) (( Object_t * (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15584_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::get_Count()
#define Dictionary_2_get_Count_m21927(__this, method) (( int32_t (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_get_Count_m15586_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::get_Item(TKey)
#define Dictionary_2_get_Item_m21928(__this, ___key, method) (( int32_t (*) (Dictionary_2_t765 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m15588_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m21929(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t765 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m15590_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m21930(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t765 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m15592_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m21931(__this, ___size, method) (( void (*) (Dictionary_2_t765 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m15594_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m21932(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t765 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m15596_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m21933(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3017  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m15598_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m21934(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m15600_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m21935(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m15602_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m21936(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t765 *, KeyValuePair_2U5BU5D_t3708*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m15604_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::Resize()
#define Dictionary_2_Resize_m21937(__this, method) (( void (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_Resize_m15606_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::Add(TKey,TValue)
#define Dictionary_2_Add_m21938(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t765 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m15608_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::Clear()
#define Dictionary_2_Clear_m21939(__this, method) (( void (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_Clear_m15610_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m21940(__this, ___key, method) (( bool (*) (Dictionary_2_t765 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m15612_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m21941(__this, ___value, method) (( bool (*) (Dictionary_2_t765 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m15614_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m21942(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t765 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m15616_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m21943(__this, ___sender, method) (( void (*) (Dictionary_2_t765 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m15618_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::Remove(TKey)
#define Dictionary_2_Remove_m21944(__this, ___key, method) (( bool (*) (Dictionary_2_t765 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m15620_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m21945(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t765 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m15622_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::get_Keys()
#define Dictionary_2_get_Keys_m21946(__this, method) (( KeyCollection_t3018 * (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_get_Keys_m15624_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::get_Values()
#define Dictionary_2_get_Values_m21947(__this, method) (( ValueCollection_t3019 * (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_get_Values_m15626_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m21948(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t765 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m15628_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m21949(__this, ___value, method) (( int32_t (*) (Dictionary_2_t765 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m15630_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m21950(__this, ___pair, method) (( bool (*) (Dictionary_2_t765 *, KeyValuePair_2_t3017 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m15632_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m21951(__this, method) (( Enumerator_t3020  (*) (Dictionary_2_t765 *, const MethodInfo*))Dictionary_2_GetEnumerator_m15634_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m21952(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m15636_gshared)(__this /* static, unused */, ___key, ___value, method)
