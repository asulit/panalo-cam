﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>
struct DefaultComparer_t3468;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void DefaultComparer__ctor_m28547_gshared (DefaultComparer_t3468 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m28547(__this, method) (( void (*) (DefaultComparer_t3468 *, const MethodInfo*))DefaultComparer__ctor_m28547_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m28548_gshared (DefaultComparer_t3468 * __this, TargetSearchResult_t1212  ___x, TargetSearchResult_t1212  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m28548(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3468 *, TargetSearchResult_t1212 , TargetSearchResult_t1212 , const MethodInfo*))DefaultComparer_Compare_m28548_gshared)(__this, ___x, ___y, method)
