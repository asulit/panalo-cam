﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// NotificationSystemComponent
struct NotificationSystemComponent_t91;
// Common.Notification.NotificationHandler
struct NotificationHandler_t347;
// System.String
struct String_t;
// Common.Notification.NotificationInstance
struct NotificationInstance_t85;
// Common.Notification.NotificationSystem
struct NotificationSystem_t89;

// System.Void NotificationSystemComponent::.ctor()
extern "C" void NotificationSystemComponent__ctor_m312 (NotificationSystemComponent_t91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationSystemComponent::Awake()
extern "C" void NotificationSystemComponent_Awake_m313 (NotificationSystemComponent_t91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationSystemComponent::AddHandler(Common.Notification.NotificationHandler)
extern "C" void NotificationSystemComponent_AddHandler_m314 (NotificationSystemComponent_t91 * __this, Object_t * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationSystemComponent::RemoveHandler(Common.Notification.NotificationHandler)
extern "C" void NotificationSystemComponent_RemoveHandler_m315 (NotificationSystemComponent_t91 * __this, Object_t * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationSystemComponent::ClearHandlers()
extern "C" void NotificationSystemComponent_ClearHandlers_m316 (NotificationSystemComponent_t91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationSystemComponent::Post(System.String)
extern "C" void NotificationSystemComponent_Post_m317 (NotificationSystemComponent_t91 * __this, String_t* ___notificationId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationSystemComponent::Post(Common.Notification.NotificationInstance)
extern "C" void NotificationSystemComponent_Post_m318 (NotificationSystemComponent_t91 * __this, NotificationInstance_t85 * ___notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Notification.NotificationSystem NotificationSystemComponent::get_System()
extern "C" NotificationSystem_t89 * NotificationSystemComponent_get_System_m319 (NotificationSystemComponent_t91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
