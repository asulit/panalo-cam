﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU24256_t1551_marshal(const U24ArrayTypeU24256_t1551& unmarshaled, U24ArrayTypeU24256_t1551_marshaled& marshaled);
extern "C" void U24ArrayTypeU24256_t1551_marshal_back(const U24ArrayTypeU24256_t1551_marshaled& marshaled, U24ArrayTypeU24256_t1551& unmarshaled);
extern "C" void U24ArrayTypeU24256_t1551_marshal_cleanup(U24ArrayTypeU24256_t1551_marshaled& marshaled);
