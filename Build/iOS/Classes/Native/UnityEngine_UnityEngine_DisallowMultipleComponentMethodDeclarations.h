﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t929;

// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern "C" void DisallowMultipleComponent__ctor_m4916 (DisallowMultipleComponent_t929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
