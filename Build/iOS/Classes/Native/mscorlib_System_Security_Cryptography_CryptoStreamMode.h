﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// System.Security.Cryptography.CryptoStreamMode
#include "mscorlib_System_Security_Cryptography_CryptoStreamMode.h"
// System.Security.Cryptography.CryptoStreamMode
struct  CryptoStreamMode_t2199 
{
	// System.Int32 System.Security.Cryptography.CryptoStreamMode::value__
	int32_t ___value___1;
};
