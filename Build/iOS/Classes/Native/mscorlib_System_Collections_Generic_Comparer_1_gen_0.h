﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.CameraDevice/FocusMode>
struct Comparer_1_t2732;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.CameraDevice/FocusMode>
struct  Comparer_1_t2732  : public Object_t
{
};
struct Comparer_1_t2732_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.CameraDevice/FocusMode>::_default
	Comparer_1_t2732 * ____default_0;
};
