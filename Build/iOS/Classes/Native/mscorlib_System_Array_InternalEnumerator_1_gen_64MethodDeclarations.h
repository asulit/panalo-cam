﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_64.h"
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23103_gshared (InternalEnumerator_1_t3111 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m23103(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3111 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m23103_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23104_gshared (InternalEnumerator_1_t3111 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23104(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3111 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23104_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23105_gshared (InternalEnumerator_1_t3111 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23105(__this, method) (( void (*) (InternalEnumerator_1_t3111 *, const MethodInfo*))InternalEnumerator_1_Dispose_m23105_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23106_gshared (InternalEnumerator_1_t3111 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23106(__this, method) (( bool (*) (InternalEnumerator_1_t3111 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m23106_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern "C" GcScoreData_t940  InternalEnumerator_1_get_Current_m23107_gshared (InternalEnumerator_1_t3111 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23107(__this, method) (( GcScoreData_t940  (*) (InternalEnumerator_1_t3111 *, const MethodInfo*))InternalEnumerator_1_get_Current_m23107_gshared)(__this, method)
