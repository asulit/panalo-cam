﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>
struct Dictionary_2_t1054;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>
struct  ValueCollection_t1286  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::dictionary
	Dictionary_2_t1054 * ___dictionary_0;
};
