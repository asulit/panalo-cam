﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t2472;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2471;

// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor()
extern "C" void Collection_1__ctor_m14754_gshared (Collection_1_t2472 * __this, const MethodInfo* method);
#define Collection_1__ctor_m14754(__this, method) (( void (*) (Collection_1_t2472 *, const MethodInfo*))Collection_1__ctor_m14754_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14755_gshared (Collection_1_t2472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14755(__this, method) (( bool (*) (Collection_1_t2472 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14755_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m14756_gshared (Collection_1_t2472 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m14756(__this, ___array, ___index, method) (( void (*) (Collection_1_t2472 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m14756_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m14757_gshared (Collection_1_t2472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m14757(__this, method) (( Object_t * (*) (Collection_1_t2472 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m14757_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m14758_gshared (Collection_1_t2472 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m14758(__this, ___value, method) (( int32_t (*) (Collection_1_t2472 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m14758_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m14759_gshared (Collection_1_t2472 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m14759(__this, ___value, method) (( bool (*) (Collection_1_t2472 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m14759_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m14760_gshared (Collection_1_t2472 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m14760(__this, ___value, method) (( int32_t (*) (Collection_1_t2472 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m14760_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m14761_gshared (Collection_1_t2472 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m14761(__this, ___index, ___value, method) (( void (*) (Collection_1_t2472 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m14761_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m14762_gshared (Collection_1_t2472 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m14762(__this, ___value, method) (( void (*) (Collection_1_t2472 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m14762_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m14763_gshared (Collection_1_t2472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m14763(__this, method) (( bool (*) (Collection_1_t2472 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m14763_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m14764_gshared (Collection_1_t2472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m14764(__this, method) (( Object_t * (*) (Collection_1_t2472 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m14764_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m14765_gshared (Collection_1_t2472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m14765(__this, method) (( bool (*) (Collection_1_t2472 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m14765_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m14766_gshared (Collection_1_t2472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m14766(__this, method) (( bool (*) (Collection_1_t2472 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m14766_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m14767_gshared (Collection_1_t2472 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m14767(__this, ___index, method) (( Object_t * (*) (Collection_1_t2472 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m14767_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m14768_gshared (Collection_1_t2472 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m14768(__this, ___index, ___value, method) (( void (*) (Collection_1_t2472 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m14768_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(T)
extern "C" void Collection_1_Add_m14769_gshared (Collection_1_t2472 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Add_m14769(__this, ___item, method) (( void (*) (Collection_1_t2472 *, Object_t *, const MethodInfo*))Collection_1_Add_m14769_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear()
extern "C" void Collection_1_Clear_m14770_gshared (Collection_1_t2472 * __this, const MethodInfo* method);
#define Collection_1_Clear_m14770(__this, method) (( void (*) (Collection_1_t2472 *, const MethodInfo*))Collection_1_Clear_m14770_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems()
extern "C" void Collection_1_ClearItems_m14771_gshared (Collection_1_t2472 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m14771(__this, method) (( void (*) (Collection_1_t2472 *, const MethodInfo*))Collection_1_ClearItems_m14771_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(T)
extern "C" bool Collection_1_Contains_m14772_gshared (Collection_1_t2472 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Contains_m14772(__this, ___item, method) (( bool (*) (Collection_1_t2472 *, Object_t *, const MethodInfo*))Collection_1_Contains_m14772_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m14773_gshared (Collection_1_t2472 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m14773(__this, ___array, ___index, method) (( void (*) (Collection_1_t2472 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))Collection_1_CopyTo_m14773_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m14774_gshared (Collection_1_t2472 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m14774(__this, method) (( Object_t* (*) (Collection_1_t2472 *, const MethodInfo*))Collection_1_GetEnumerator_m14774_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m14775_gshared (Collection_1_t2472 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m14775(__this, ___item, method) (( int32_t (*) (Collection_1_t2472 *, Object_t *, const MethodInfo*))Collection_1_IndexOf_m14775_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m14776_gshared (Collection_1_t2472 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Insert_m14776(__this, ___index, ___item, method) (( void (*) (Collection_1_t2472 *, int32_t, Object_t *, const MethodInfo*))Collection_1_Insert_m14776_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m14777_gshared (Collection_1_t2472 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m14777(__this, ___index, ___item, method) (( void (*) (Collection_1_t2472 *, int32_t, Object_t *, const MethodInfo*))Collection_1_InsertItem_m14777_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(T)
extern "C" bool Collection_1_Remove_m14778_gshared (Collection_1_t2472 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Remove_m14778(__this, ___item, method) (( bool (*) (Collection_1_t2472 *, Object_t *, const MethodInfo*))Collection_1_Remove_m14778_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m14779_gshared (Collection_1_t2472 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m14779(__this, ___index, method) (( void (*) (Collection_1_t2472 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m14779_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m14780_gshared (Collection_1_t2472 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m14780(__this, ___index, method) (( void (*) (Collection_1_t2472 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m14780_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count()
extern "C" int32_t Collection_1_get_Count_m14781_gshared (Collection_1_t2472 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m14781(__this, method) (( int32_t (*) (Collection_1_t2472 *, const MethodInfo*))Collection_1_get_Count_m14781_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * Collection_1_get_Item_m14782_gshared (Collection_1_t2472 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m14782(__this, ___index, method) (( Object_t * (*) (Collection_1_t2472 *, int32_t, const MethodInfo*))Collection_1_get_Item_m14782_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m14783_gshared (Collection_1_t2472 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_set_Item_m14783(__this, ___index, ___value, method) (( void (*) (Collection_1_t2472 *, int32_t, Object_t *, const MethodInfo*))Collection_1_set_Item_m14783_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m14784_gshared (Collection_1_t2472 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_SetItem_m14784(__this, ___index, ___item, method) (( void (*) (Collection_1_t2472 *, int32_t, Object_t *, const MethodInfo*))Collection_1_SetItem_m14784_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m14785_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m14785(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m14785_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::ConvertItem(System.Object)
extern "C" Object_t * Collection_1_ConvertItem_m14786_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m14786(__this /* static, unused */, ___item, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m14786_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m14787_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m14787(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m14787_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m14788_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m14788(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m14788_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m14789_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m14789(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m14789_gshared)(__this /* static, unused */, ___list, method)
