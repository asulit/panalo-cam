﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Cryptography.MACAlgorithm
struct MACAlgorithm_t1837;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t1402;
// System.Byte[]
struct ByteU5BU5D_t139;

// System.Void Mono.Security.Cryptography.MACAlgorithm::.ctor(System.Security.Cryptography.SymmetricAlgorithm)
extern "C" void MACAlgorithm__ctor_m10664 (MACAlgorithm_t1837 * __this, SymmetricAlgorithm_t1402 * ___algorithm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.MACAlgorithm::Initialize(System.Byte[])
extern "C" void MACAlgorithm_Initialize_m10665 (MACAlgorithm_t1837 * __this, ByteU5BU5D_t139* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.MACAlgorithm::Core(System.Byte[],System.Int32,System.Int32)
extern "C" void MACAlgorithm_Core_m10666 (MACAlgorithm_t1837 * __this, ByteU5BU5D_t139* ___rgb, int32_t ___ib, int32_t ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.MACAlgorithm::Final()
extern "C" ByteU5BU5D_t139* MACAlgorithm_Final_m10667 (MACAlgorithm_t1837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
