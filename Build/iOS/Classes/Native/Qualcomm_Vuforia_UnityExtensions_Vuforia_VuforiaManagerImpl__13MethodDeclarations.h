﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaManagerImpl/<>c__DisplayClass3
struct U3CU3Ec__DisplayClass3_t1147;
// Vuforia.VuforiaManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void Vuforia.VuforiaManagerImpl/<>c__DisplayClass3::.ctor()
extern "C" void U3CU3Ec__DisplayClass3__ctor_m5736 (U3CU3Ec__DisplayClass3_t1147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaManagerImpl/<>c__DisplayClass3::<UpdateTrackers>b__1(Vuforia.VuforiaManagerImpl/TrackableResultData)
extern "C" bool U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m5737 (U3CU3Ec__DisplayClass3_t1147 * __this, TrackableResultData_t1134  ___tr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
