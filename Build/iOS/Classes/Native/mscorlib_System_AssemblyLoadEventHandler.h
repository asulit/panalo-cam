﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.AssemblyLoadEventArgs
struct AssemblyLoadEventArgs_t2316;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.AssemblyLoadEventHandler
struct  AssemblyLoadEventHandler_t2309  : public MulticastDelegate_t28
{
};
