﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.Action.MoveAlongDirectionByPolledTime
struct MoveAlongDirectionByPolledTime_t38;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t35;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Common.Fsm.Action.MoveAlongDirectionByPolledTime::.ctor(Common.Fsm.FsmState,System.String)
extern "C" void MoveAlongDirectionByPolledTime__ctor_m117 (MoveAlongDirectionByPolledTime_t38 * __this, Object_t * ___owner, String_t* ___timeReferenceName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAlongDirectionByPolledTime::Init(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern "C" void MoveAlongDirectionByPolledTime_Init_m118 (MoveAlongDirectionByPolledTime_t38 * __this, Transform_t35 * ___actor, Vector3_t36  ___direction, float ___velocity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Common.Fsm.Action.MoveAlongDirectionByPolledTime::get_StartPosition()
extern "C" Vector3_t36  MoveAlongDirectionByPolledTime_get_StartPosition_m119 (MoveAlongDirectionByPolledTime_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAlongDirectionByPolledTime::set_StartPosition(UnityEngine.Vector3)
extern "C" void MoveAlongDirectionByPolledTime_set_StartPosition_m120 (MoveAlongDirectionByPolledTime_t38 * __this, Vector3_t36  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAlongDirectionByPolledTime::OnEnter()
extern "C" void MoveAlongDirectionByPolledTime_OnEnter_m121 (MoveAlongDirectionByPolledTime_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAlongDirectionByPolledTime::OnUpdate()
extern "C" void MoveAlongDirectionByPolledTime_OnUpdate_m122 (MoveAlongDirectionByPolledTime_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
