﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.ConcreteFsmState
struct ConcreteFsmState_t46;
// System.String
struct String_t;
// Common.Fsm.Fsm
struct Fsm_t47;
// Common.Fsm.FsmState
struct FsmState_t29;
// Common.Fsm.FsmAction
struct FsmAction_t51;
// System.Collections.Generic.IEnumerable`1<Common.Fsm.FsmAction>
struct IEnumerable_1_t340;

// System.Void Common.Fsm.ConcreteFsmState::.ctor(System.String,Common.Fsm.Fsm)
extern "C" void ConcreteFsmState__ctor_m151 (ConcreteFsmState_t46 * __this, String_t* ___name, Fsm_t47 * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Fsm.ConcreteFsmState::GetName()
extern "C" String_t* ConcreteFsmState_GetName_m152 (ConcreteFsmState_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.ConcreteFsmState::AddTransition(System.String,Common.Fsm.FsmState)
extern "C" void ConcreteFsmState_AddTransition_m153 (ConcreteFsmState_t46 * __this, String_t* ___eventId, Object_t * ___destinationState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Fsm.FsmState Common.Fsm.ConcreteFsmState::GetTransition(System.String)
extern "C" Object_t * ConcreteFsmState_GetTransition_m154 (ConcreteFsmState_t46 * __this, String_t* ___eventId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.ConcreteFsmState::AddAction(Common.Fsm.FsmAction)
extern "C" void ConcreteFsmState_AddAction_m155 (ConcreteFsmState_t46 * __this, Object_t * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Common.Fsm.FsmAction> Common.Fsm.ConcreteFsmState::GetActions()
extern "C" Object_t* ConcreteFsmState_GetActions_m156 (ConcreteFsmState_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.ConcreteFsmState::SendEvent(System.String)
extern "C" void ConcreteFsmState_SendEvent_m157 (ConcreteFsmState_t46 * __this, String_t* ___eventId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
