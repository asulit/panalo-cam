﻿#pragma once
#include <stdint.h>
// System.UInt32[]
struct UInt32U5BU5D_t1409;
// System.UInt64[]
struct UInt64U5BU5D_t2228;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.SHAConstants
struct  SHAConstants_t2231  : public Object_t
{
};
struct SHAConstants_t2231_StaticFields{
	// System.UInt32[] System.Security.Cryptography.SHAConstants::K1
	UInt32U5BU5D_t1409* ___K1_0;
	// System.UInt64[] System.Security.Cryptography.SHAConstants::K2
	UInt64U5BU5D_t2228* ___K2_1;
};
