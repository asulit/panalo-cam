﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void VectorUtils::.cctor()
extern "C" void VectorUtils__cctor_m463 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VectorUtils::Equals(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool VectorUtils_Equals_m464 (Object_t * __this /* static, unused */, Vector3_t36  ___a, Vector3_t36  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
