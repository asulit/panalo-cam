﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_29MethodDeclarations.h"
#define Enumerator__ctor_m24627(__this, ___host, method) (( void (*) (Enumerator_t1285 *, Dictionary_2_t1054 *, const MethodInfo*))Enumerator__ctor_m15170_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24628(__this, method) (( Object_t * (*) (Enumerator_t1285 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15171_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m7195(__this, method) (( void (*) (Enumerator_t1285 *, const MethodInfo*))Enumerator_Dispose_m15172_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m7194(__this, method) (( bool (*) (Enumerator_t1285 *, const MethodInfo*))Enumerator_MoveNext_m15173_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m7193(__this, method) (( VideoBackgroundAbstractBehaviour_t315 * (*) (Enumerator_t1285 *, const MethodInfo*))Enumerator_get_Current_m15174_gshared)(__this, method)
