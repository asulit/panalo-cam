﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
// System.Collections.Generic.LinkedListNode`1<System.Object>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_2MethodDeclarations.h"
#define LinkedListNode_1__ctor_m16033(__this, ___list, ___value, method) (( void (*) (LinkedListNode_1_t406 *, LinkedList_1_t90 *, Object_t *, const MethodInfo*))LinkedListNode_1__ctor_m16022_gshared)(__this, ___list, ___value, method)
// System.Void System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m16034(__this, ___list, ___value, ___previousNode, ___nextNode, method) (( void (*) (LinkedListNode_1_t406 *, LinkedList_1_t90 *, Object_t *, LinkedListNode_1_t406 *, LinkedListNode_1_t406 *, const MethodInfo*))LinkedListNode_1__ctor_m16023_gshared)(__this, ___list, ___value, ___previousNode, ___nextNode, method)
// System.Void System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>::Detach()
#define LinkedListNode_1_Detach_m16035(__this, method) (( void (*) (LinkedListNode_1_t406 *, const MethodInfo*))LinkedListNode_1_Detach_m16024_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>::get_List()
#define LinkedListNode_1_get_List_m16036(__this, method) (( LinkedList_1_t90 * (*) (LinkedListNode_1_t406 *, const MethodInfo*))LinkedListNode_1_get_List_m16025_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>::get_Next()
#define LinkedListNode_1_get_Next_m1496(__this, method) (( LinkedListNode_1_t406 * (*) (LinkedListNode_1_t406 *, const MethodInfo*))LinkedListNode_1_get_Next_m16026_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>::get_Value()
#define LinkedListNode_1_get_Value_m1495(__this, method) (( Object_t * (*) (LinkedListNode_1_t406 *, const MethodInfo*))LinkedListNode_1_get_Value_m16027_gshared)(__this, method)
