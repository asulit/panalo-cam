﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t718;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct  UnityAction_1_t2823  : public MulticastDelegate_t28
{
};
