﻿#pragma once
#include <stdint.h>
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t1279;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.ITrackerEventHandler>
struct  Comparison_1_t3504  : public MulticastDelegate_t28
{
};
