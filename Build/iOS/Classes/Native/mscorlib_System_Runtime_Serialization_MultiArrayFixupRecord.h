﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t401;
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecord.h"
// System.Runtime.Serialization.MultiArrayFixupRecord
struct  MultiArrayFixupRecord_t2181  : public BaseFixupRecord_t2179
{
	// System.Int32[] System.Runtime.Serialization.MultiArrayFixupRecord::_indices
	Int32U5BU5D_t401* ____indices_4;
};
