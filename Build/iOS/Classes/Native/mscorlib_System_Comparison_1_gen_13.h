﻿#pragma once
#include <stdint.h>
// Common.Xml.SimpleXmlNode
struct SimpleXmlNode_t142;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Common.Xml.SimpleXmlNode>
struct  Comparison_1_t2640  : public MulticastDelegate_t28
{
};
