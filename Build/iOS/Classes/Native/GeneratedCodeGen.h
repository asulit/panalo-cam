﻿#pragma once
struct Object_t;
typedef Object_t Il2CppCodeGenObject;
// System.Array
#include "mscorlib_System_Array.h"
typedef Array_t Il2CppCodeGenArray;
struct String_t;
typedef String_t Il2CppCodeGenString;
struct Type_t;
typedef Type_t Il2CppCodeGenType;
struct Exception_t359;
typedef Exception_t359 Il2CppCodeGenException;
struct Exception_t359;
typedef Exception_t359 Il2CppCodeGenException;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
typedef RuntimeTypeHandle_t1772 Il2CppCodeGenRuntimeTypeHandle;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
typedef RuntimeFieldHandle_t1774 Il2CppCodeGenRuntimeFieldHandle;
// System.RuntimeArgumentHandle
#include "mscorlib_System_RuntimeArgumentHandle.h"
typedef RuntimeArgumentHandle_t1791 Il2CppCodeGenRuntimeArgumentHandle;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
typedef RuntimeMethodHandle_t2367 Il2CppCodeGenRuntimeMethodHandle;
struct StringBuilder_t70;
typedef StringBuilder_t70 Il2CppCodeGenStringBuilder;
struct MulticastDelegate_t28;
typedef MulticastDelegate_t28 Il2CppCodeGenMulticastDelegate;
struct MethodBase_t1033;
typedef MethodBase_t1033 Il2CppCodeGenMethodBase;
