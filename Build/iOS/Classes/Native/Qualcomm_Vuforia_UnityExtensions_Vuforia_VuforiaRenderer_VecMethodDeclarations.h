﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec.h"

// System.Void Vuforia.VuforiaRenderer/Vec2I::.ctor(System.Int32,System.Int32)
extern "C" void Vec2I__ctor_m5768 (Vec2I_t1157 * __this, int32_t ___v1, int32_t ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
