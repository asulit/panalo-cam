﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t557;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct  Func_2_t646  : public MulticastDelegate_t28
{
};
