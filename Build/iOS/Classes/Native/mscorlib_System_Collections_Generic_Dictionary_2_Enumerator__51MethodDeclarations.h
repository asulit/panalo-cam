﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__18MethodDeclarations.h"
#define Enumerator__ctor_m29609(__this, ___dictionary, method) (( void (*) (Enumerator_t3544 *, Dictionary_2_t1639 *, const MethodInfo*))Enumerator__ctor_m17983_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m29610(__this, method) (( Object_t * (*) (Enumerator_t3544 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17984_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m29611(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3544 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17985_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m29612(__this, method) (( Object_t * (*) (Enumerator_t3544 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17986_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m29613(__this, method) (( Object_t * (*) (Enumerator_t3544 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17987_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m29614(__this, method) (( bool (*) (Enumerator_t3544 *, const MethodInfo*))Enumerator_MoveNext_m17988_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m29615(__this, method) (( KeyValuePair_2_t3541  (*) (Enumerator_t3544 *, const MethodInfo*))Enumerator_get_Current_m17989_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m29616(__this, method) (( String_t* (*) (Enumerator_t3544 *, const MethodInfo*))Enumerator_get_CurrentKey_m17990_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m29617(__this, method) (( bool (*) (Enumerator_t3544 *, const MethodInfo*))Enumerator_get_CurrentValue_m17991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m29618(__this, method) (( void (*) (Enumerator_t3544 *, const MethodInfo*))Enumerator_VerifyState_m17992_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m29619(__this, method) (( void (*) (Enumerator_t3544 *, const MethodInfo*))Enumerator_VerifyCurrent_m17993_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m29620(__this, method) (( void (*) (Enumerator_t3544 *, const MethodInfo*))Enumerator_Dispose_m17994_gshared)(__this, method)
