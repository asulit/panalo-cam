﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// VectorUtils
struct  VectorUtils_t140  : public Object_t
{
};
struct VectorUtils_t140_StaticFields{
	// UnityEngine.Vector3 VectorUtils::ZERO
	Vector3_t36  ___ZERO_0;
	// UnityEngine.Vector3 VectorUtils::ONE
	Vector3_t36  ___ONE_1;
	// UnityEngine.Vector3 VectorUtils::UP
	Vector3_t36  ___UP_2;
	// UnityEngine.Vector3 VectorUtils::RIGHT
	Vector3_t36  ___RIGHT_3;
};
