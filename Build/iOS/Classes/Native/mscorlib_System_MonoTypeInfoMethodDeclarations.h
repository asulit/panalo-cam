﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTypeInfo
struct MonoTypeInfo_t2357;

// System.Void System.MonoTypeInfo::.ctor()
extern "C" void MonoTypeInfo__ctor_m14248 (MonoTypeInfo_t2357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
