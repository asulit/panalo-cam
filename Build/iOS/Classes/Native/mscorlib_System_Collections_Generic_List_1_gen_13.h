﻿#pragma once
#include <stdint.h>
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t464;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct  List_1_t461  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Reflection.MethodInfo>::_items
	MethodInfoU5BU5D_t464* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Reflection.MethodInfo>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Reflection.MethodInfo>::_version
	int32_t ____version_3;
};
struct List_1_t461_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Reflection.MethodInfo>::EmptyArray
	MethodInfoU5BU5D_t464* ___EmptyArray_4;
};
