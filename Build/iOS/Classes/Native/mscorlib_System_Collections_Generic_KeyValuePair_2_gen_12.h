﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Time.TimeReference
struct TimeReference_t14;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Time.TimeReference>
struct  KeyValuePair_2_t2617 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,Common.Time.TimeReference>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,Common.Time.TimeReference>::value
	TimeReference_t14 * ___value_1;
};
