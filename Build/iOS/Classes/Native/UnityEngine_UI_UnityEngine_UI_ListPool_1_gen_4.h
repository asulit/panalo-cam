﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct ObjectPool_1_t3090;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct UnityAction_1_t3091;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Vector4>
struct  ListPool_1_t776  : public Object_t
{
};
struct ListPool_1_t776_StaticFields{
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::s_ListPool
	ObjectPool_1_t3090 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::<>f__am$cache1
	UnityAction_1_t3091 * ___U3CU3Ef__amU24cache1_1;
};
