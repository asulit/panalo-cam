﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1531;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t1587;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrors.h"
// System.Net.Security.RemoteCertificateValidationCallback
struct  RemoteCertificateValidationCallback_t1585  : public MulticastDelegate_t28
{
};
