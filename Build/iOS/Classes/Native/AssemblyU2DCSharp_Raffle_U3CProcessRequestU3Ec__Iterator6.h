﻿#pragma once
#include <stdint.h>
// UnityEngine.WWW
struct WWW_t199;
// RequestHandler
struct RequestHandler_t200;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// Raffle/<ProcessRequest>c__Iterator6
struct  U3CProcessRequestU3Ec__Iterator6_t198  : public Object_t
{
	// UnityEngine.WWW Raffle/<ProcessRequest>c__Iterator6::request
	WWW_t199 * ___request_0;
	// RequestHandler Raffle/<ProcessRequest>c__Iterator6::handler
	RequestHandler_t200 * ___handler_1;
	// System.Int32 Raffle/<ProcessRequest>c__Iterator6::$PC
	int32_t ___U24PC_2;
	// System.Object Raffle/<ProcessRequest>c__Iterator6::$current
	Object_t * ___U24current_3;
	// UnityEngine.WWW Raffle/<ProcessRequest>c__Iterator6::<$>request
	WWW_t199 * ___U3CU24U3Erequest_4;
	// RequestHandler Raffle/<ProcessRequest>c__Iterator6::<$>handler
	RequestHandler_t200 * ___U3CU24U3Ehandler_5;
};
