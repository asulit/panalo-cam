﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BackgroundPlaneAbstractBehaviour
struct BackgroundPlaneAbstractBehaviour_t269;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::get_NumDivisions()
extern "C" int32_t BackgroundPlaneAbstractBehaviour_get_NumDivisions_m5209 (BackgroundPlaneAbstractBehaviour_t269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::set_NumDivisions(System.Int32)
extern "C" void BackgroundPlaneAbstractBehaviour_set_NumDivisions_m5210 (BackgroundPlaneAbstractBehaviour_t269 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetEditorValues(System.Int32)
extern "C" void BackgroundPlaneAbstractBehaviour_SetEditorValues_m5211 (BackgroundPlaneAbstractBehaviour_t269 * __this, int32_t ___numDivisions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::CheckNumDivisions()
extern "C" bool BackgroundPlaneAbstractBehaviour_CheckNumDivisions_m5212 (BackgroundPlaneAbstractBehaviour_t269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetStereoDepth(System.Single)
extern "C" void BackgroundPlaneAbstractBehaviour_SetStereoDepth_m5213 (BackgroundPlaneAbstractBehaviour_t269 * __this, float ___depth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Start()
extern "C" void BackgroundPlaneAbstractBehaviour_Start_m5214 (BackgroundPlaneAbstractBehaviour_t269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Update()
extern "C" void BackgroundPlaneAbstractBehaviour_Update_m5215 (BackgroundPlaneAbstractBehaviour_t269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Vuforia.BackgroundPlaneAbstractBehaviour::get_DefaultRotationTowardsCamera()
extern "C" Quaternion_t41  BackgroundPlaneAbstractBehaviour_get_DefaultRotationTowardsCamera_m5216 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::CreateAndSetVideoMesh()
extern "C" void BackgroundPlaneAbstractBehaviour_CreateAndSetVideoMesh_m5217 (BackgroundPlaneAbstractBehaviour_t269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::PositionVideoMesh()
extern "C" void BackgroundPlaneAbstractBehaviour_PositionVideoMesh_m5218 (BackgroundPlaneAbstractBehaviour_t269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::ShouldFitWidth()
extern "C" bool BackgroundPlaneAbstractBehaviour_ShouldFitWidth_m5219 (BackgroundPlaneAbstractBehaviour_t269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::OnBackgroundTextureChanged()
extern "C" void BackgroundPlaneAbstractBehaviour_OnBackgroundTextureChanged_m5220 (BackgroundPlaneAbstractBehaviour_t269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C" void BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m5221 (BackgroundPlaneAbstractBehaviour_t269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.ctor()
extern "C" void BackgroundPlaneAbstractBehaviour__ctor_m1807 (BackgroundPlaneAbstractBehaviour_t269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.cctor()
extern "C" void BackgroundPlaneAbstractBehaviour__cctor_m5222 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
