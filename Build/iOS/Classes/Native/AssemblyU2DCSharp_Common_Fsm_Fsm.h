﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>
struct Dictionary_2_t48;
// Common.Fsm.Fsm/StateActionProcessor
struct StateActionProcessor_t50;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Fsm.Fsm
struct  Fsm_t47  : public Object_t
{
	// System.String Common.Fsm.Fsm::name
	String_t* ___name_0;
	// Common.Fsm.FsmState Common.Fsm.Fsm::currentState
	Object_t * ___currentState_1;
	// System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState> Common.Fsm.Fsm::stateMap
	Dictionary_2_t48 * ___stateMap_2;
	// System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState> Common.Fsm.Fsm::globalTransitionMap
	Dictionary_2_t48 * ___globalTransitionMap_3;
};
struct Fsm_t47_StaticFields{
	// Common.Fsm.Fsm/StateActionProcessor Common.Fsm.Fsm::<>f__am$cache4
	StateActionProcessor_t50 * ___U3CU3Ef__amU24cache4_4;
	// Common.Fsm.Fsm/StateActionProcessor Common.Fsm.Fsm::<>f__am$cache5
	StateActionProcessor_t50 * ___U3CU3Ef__amU24cache5_5;
};
