﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t953;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"

// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern "C" Achievement_t953 * GcAchievementData_ToAchievement_m4930 (GcAchievementData_t939 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void GcAchievementData_t939_marshal(const GcAchievementData_t939& unmarshaled, GcAchievementData_t939_marshaled& marshaled);
extern "C" void GcAchievementData_t939_marshal_back(const GcAchievementData_t939_marshaled& marshaled, GcAchievementData_t939& unmarshaled);
extern "C" void GcAchievementData_t939_marshal_cleanup(GcAchievementData_t939_marshaled& marshaled);
