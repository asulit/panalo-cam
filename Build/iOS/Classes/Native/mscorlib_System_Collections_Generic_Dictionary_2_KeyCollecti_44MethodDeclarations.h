﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t3255;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_44.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25124_gshared (Enumerator_t3261 * __this, Dictionary_2_t3255 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m25124(__this, ___host, method) (( void (*) (Enumerator_t3261 *, Dictionary_2_t3255 *, const MethodInfo*))Enumerator__ctor_m25124_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25125_gshared (Enumerator_t3261 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25125(__this, method) (( Object_t * (*) (Enumerator_t3261 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m25126_gshared (Enumerator_t3261 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25126(__this, method) (( void (*) (Enumerator_t3261 *, const MethodInfo*))Enumerator_Dispose_m25126_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25127_gshared (Enumerator_t3261 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25127(__this, method) (( bool (*) (Enumerator_t3261 *, const MethodInfo*))Enumerator_MoveNext_m25127_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m25128_gshared (Enumerator_t3261 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25128(__this, method) (( int32_t (*) (Enumerator_t3261 *, const MethodInfo*))Enumerator_get_Current_m25128_gshared)(__this, method)
