﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t137;
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// System.Runtime.Remoting.Messaging.MethodCallDictionary
struct  MethodCallDictionary_t2110  : public MethodDictionary_t2105
{
};
struct MethodCallDictionary_t2110_StaticFields{
	// System.String[] System.Runtime.Remoting.Messaging.MethodCallDictionary::InternalKeys
	StringU5BU5D_t137* ___InternalKeys_6;
};
