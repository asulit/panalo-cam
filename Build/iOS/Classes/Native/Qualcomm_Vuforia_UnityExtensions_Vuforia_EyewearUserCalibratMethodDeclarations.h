﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.EyewearUserCalibrator
struct EyewearUserCalibrator_t1050;

// System.Void Vuforia.EyewearUserCalibrator::.ctor()
extern "C" void EyewearUserCalibrator__ctor_m5223 (EyewearUserCalibrator_t1050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
