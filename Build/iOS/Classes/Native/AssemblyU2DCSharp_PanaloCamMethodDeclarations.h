﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PanaloCam
struct PanaloCam_t194;
// Common.Signal.ISignalParameters
struct ISignalParameters_t115;
// Common.Fsm.FsmState
struct FsmState_t29;

// System.Void PanaloCam::.ctor()
extern "C" void PanaloCam__ctor_m673 (PanaloCam_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::Awake()
extern "C" void PanaloCam_Awake_m674 (PanaloCam_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::Start()
extern "C" void PanaloCam_Start_m675 (PanaloCam_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::Update()
extern "C" void PanaloCam_Update_m676 (PanaloCam_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::OnDestroy()
extern "C" void PanaloCam_OnDestroy_m677 (PanaloCam_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::PrepareFsm()
extern "C" void PanaloCam_PrepareFsm_m678 (PanaloCam_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::OnTrackingDetected(Common.Signal.ISignalParameters)
extern "C" void PanaloCam_OnTrackingDetected_m679 (PanaloCam_t194 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::OnTrackingLost(Common.Signal.ISignalParameters)
extern "C" void PanaloCam_OnTrackingLost_m680 (PanaloCam_t194 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::OnLoadOldScanner(Common.Signal.ISignalParameters)
extern "C" void PanaloCam_OnLoadOldScanner_m681 (PanaloCam_t194 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::OnVideoPlayerDone(Common.Signal.ISignalParameters)
extern "C" void PanaloCam_OnVideoPlayerDone_m682 (PanaloCam_t194 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::<PrepareFsm>m__C(Common.Fsm.FsmState)
extern "C" void PanaloCam_U3CPrepareFsmU3Em__C_m683 (Object_t * __this /* static, unused */, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::<PrepareFsm>m__D(Common.Fsm.FsmState)
extern "C" void PanaloCam_U3CPrepareFsmU3Em__D_m684 (Object_t * __this /* static, unused */, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::<PrepareFsm>m__E(Common.Fsm.FsmState)
extern "C" void PanaloCam_U3CPrepareFsmU3Em__E_m685 (Object_t * __this /* static, unused */, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::<PrepareFsm>m__F(Common.Fsm.FsmState)
extern "C" void PanaloCam_U3CPrepareFsmU3Em__F_m686 (Object_t * __this /* static, unused */, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCam::<PrepareFsm>m__10(Common.Fsm.FsmState)
extern "C" void PanaloCam_U3CPrepareFsmU3Em__10_m687 (Object_t * __this /* static, unused */, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
