﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t480;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>
struct  UnityAction_1_t2856  : public MulticastDelegate_t28
{
};
