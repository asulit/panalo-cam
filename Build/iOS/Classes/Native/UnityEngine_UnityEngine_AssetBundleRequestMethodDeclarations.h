﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t784;
// UnityEngine.Object
struct Object_t335;
struct Object_t335_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t997;

// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m3815 (AssetBundleRequest_t784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t335 * AssetBundleRequest_get_asset_m3816 (AssetBundleRequest_t784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t997* AssetBundleRequest_get_allAssets_m3817 (AssetBundleRequest_t784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
