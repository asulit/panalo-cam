﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Void
#include "mscorlib_System_Void.h"
void* RuntimeInvoker_Void_t1771 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

struct Object_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
void* RuntimeInvoker_Void_t1771_Vector2_t2_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
void* RuntimeInvoker_Void_t1771_Rect_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t267 *)args[0]), method);
	return NULL;
}

struct Object_t;
// System.Boolean
#include "mscorlib_System_Boolean.h"
void* RuntimeInvoker_Boolean_t384 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t2  (*Func)(void* obj, const MethodInfo* method);
	Vector2_t2  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Single
#include "mscorlib_System_Single.h"
void* RuntimeInvoker_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t2  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t2 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t2  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

struct Object_t;
// System.SByte
#include "mscorlib_System_SByte.h"
void* RuntimeInvoker_Void_t1771_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.Int32
#include "mscorlib_System_Int32.h"
void* RuntimeInvoker_Color_t9_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t9  (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Color_t9  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
void* RuntimeInvoker_Single_t388_Vector3_t36_Vector3_t36_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector3_t36_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Single_t388_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
void* RuntimeInvoker_Void_t1771_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Vector3_t36_Vector3_t36_Single_t388_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t36  p2, Vector3_t36  p3, float p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t36 *)args[1]), *((Vector3_t36 *)args[2]), *((float*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t36 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Vector3_t36_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t36  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t36 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Quaternion_t41_Quaternion_t41_Single_t388_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Quaternion_t41  p2, Quaternion_t41  p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Quaternion_t41 *)args[1]), *((Quaternion_t41 *)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Vector3_t36_Vector3_t36_Single_t388_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t36  p2, Vector3_t36  p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t36 *)args[1]), *((Vector3_t36 *)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Single_t388_Single_t388_Single_t388_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector3_t36_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t36  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t36  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Single_t388_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Single_t388_Single_t388_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int16
#include "mscorlib_System_Int16.h"
void* RuntimeInvoker_Boolean_t384_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

struct Object_t;
// System.Char
#include "mscorlib_System_Char.h"
void* RuntimeInvoker_Char_t383 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// MiniJSON.Json/Parser/TOKEN
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser_TOKEN.h"
void* RuntimeInvoker_TOKEN_t65 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"
void* RuntimeInvoker_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector3_t36_Quaternion_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t36  p2, Quaternion_t41  p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t36 *)args[1]), *((Quaternion_t41 *)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t9  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t9 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Quaternion_t41_Quaternion_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t41  p1, Quaternion_t41  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t41 *)args[0]), *((Quaternion_t41 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

struct Object_t;
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
void* RuntimeInvoker_Boolean_t384_Bounds_t349_Bounds_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t349  p1, Bounds_t349  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t349 *)args[0]), *((Bounds_t349 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct String_t;
// System.String
#include "mscorlib_System_String.h"
void* RuntimeInvoker_Boolean_t384_Object_t_StringU26_t4077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t388_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// SwarmItem/STATE
#include "AssemblyU2DCSharp_SwarmItem_STATE.h"
void* RuntimeInvoker_STATE_t150 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
void* RuntimeInvoker_ERaffleResult_t207_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t352 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t352  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t352 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Color_t9_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t9  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t9 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Rect_t267_Rect_t267_Rect_t267_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, Rect_t267  p1, Rect_t267  p2, float p3, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, *((Rect_t267 *)args[0]), *((Rect_t267 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Vector3_t36_Vector3_t36_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, float p3, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t2_Vector2_t2_Vector2_t2_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t2  (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, float p3, const MethodInfo* method);
	Vector2_t2  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t9  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t9 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t36_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t9  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t9 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Color_t9_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t9  p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t9 *)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, float p1, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t388_Single_t388_Single_t388_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
// Vuforia.VuforiaUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"
struct Object_t;
void* RuntimeInvoker_InitError_t1233_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
void* RuntimeInvoker_Int32_t372_RaycastResult_t512_RaycastResult_t512 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t512  p1, RaycastResult_t512  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t512 *)args[0]), *((RaycastResult_t512 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// UnityEngine.EventSystems.MoveDirection
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
void* RuntimeInvoker_MoveDirection_t509 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastResult_t512 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t512  (*Func)(void* obj, const MethodInfo* method);
	RaycastResult_t512  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_RaycastResult_t512 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResult_t512  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastResult_t512 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.EventSystems.PointerEventData/InputButton
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
void* RuntimeInvoker_InputButton_t515 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_RaycastResult_t512_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t512  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RaycastResult_t512  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MoveDirection_t509_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MoveDirection_t509_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

struct Object_t;
struct PointerEventData_t517;
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
void* RuntimeInvoker_Boolean_t384_Int32_t372_PointerEventDataU26_t4078_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, PointerEventData_t517 ** p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (PointerEventData_t517 **)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
void* RuntimeInvoker_Object_t_Touch_t705_BooleanU26_t4079_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Touch_t705  p1, bool* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Touch_t705 *)args[0]), (bool*)args[1], (bool*)args[2], method);
	return ret;
}

struct Object_t;
// UnityEngine.EventSystems.PointerEventData/FramePressState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
void* RuntimeInvoker_FramePressState_t516_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector2_t2_Vector2_t2_Single_t388_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, float p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), *((float*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
// UnityEngine.EventSystems.StandaloneInputModule/InputMode
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
void* RuntimeInvoker_InputMode_t526 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
void* RuntimeInvoker_LayerMask_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t531  (*Func)(void* obj, const MethodInfo* method);
	LayerMask_t531  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_LayerMask_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LayerMask_t531  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LayerMask_t531 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
void* RuntimeInvoker_Int32_t372_RaycastHit_t60_RaycastHit_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t60  p1, RaycastHit_t60  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t60 *)args[0]), *((RaycastHit_t60 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t9  (*Func)(void* obj, const MethodInfo* method);
	Color_t9  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
void* RuntimeInvoker_ColorTweenMode_t533 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
void* RuntimeInvoker_ColorBlock_t551 (const MethodInfo* method, void* obj, void** args)
{
	typedef ColorBlock_t551  (*Func)(void* obj, const MethodInfo* method);
	ColorBlock_t551  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector2_t2  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t2 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
// UnityEngine.UI.DefaultControls/Resources
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls_Resources.h"
void* RuntimeInvoker_Object_t_Resources_t552 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Resources_t552  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Resources_t552 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t390_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
void* RuntimeInvoker_FontStyle_t918 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
void* RuntimeInvoker_TextAnchor_t766 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
void* RuntimeInvoker_HorizontalWrapMode_t890 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
void* RuntimeInvoker_VerticalWrapMode_t891 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector2_t2_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t2  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t2_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t2  (*Func)(void* obj, Vector2_t2  p1, const MethodInfo* method);
	Vector2_t2  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Color_t9_Single_t388_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t9  p1, float p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t9 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Color_t9_Single_t388_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t9  p1, float p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t9 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Color_t9_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t9  (*Func)(void* obj, float p1, const MethodInfo* method);
	Color_t9  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Single_t388_Single_t388_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.GraphicRaycaster/BlockingObjects
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
void* RuntimeInvoker_BlockingObjects_t577 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Vector2_t2_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t2  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector2_t2 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.Image/Type
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
void* RuntimeInvoker_Type_t583 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Image/FillMethod
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
void* RuntimeInvoker_FillMethod_t584 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
void* RuntimeInvoker_Vector4_t680_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t680  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Vector4_t680  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Color32_t711_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Color32_t711  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color32_t711 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Vector2_t2_Vector2_t2_Color32_t711_Vector2_t2_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t2  p2, Vector2_t2  p3, Color32_t711  p4, Vector2_t2  p5, Vector2_t2  p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t2 *)args[1]), *((Vector2_t2 *)args[2]), *((Color32_t711 *)args[3]), *((Vector2_t2 *)args[4]), *((Vector2_t2 *)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector4_t680_Vector4_t680_Rect_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t680  (*Func)(void* obj, Vector4_t680  p1, Rect_t267  p2, const MethodInfo* method);
	Vector4_t680  ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), *((Rect_t267 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Single_t388_SByte_t390_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Single_t388_Single_t388_SByte_t390_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector2_t2_Vector2_t2_Rect_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t2  (*Func)(void* obj, Vector2_t2  p1, Rect_t267  p2, const MethodInfo* method);
	Vector2_t2  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Rect_t267 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
void* RuntimeInvoker_ContentType_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/LineType
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
void* RuntimeInvoker_LineType_t594 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/InputType
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
void* RuntimeInvoker_InputType_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
void* RuntimeInvoker_TouchScreenKeyboardType_t749 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/CharacterValidation
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
void* RuntimeInvoker_CharacterValidation_t593 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Vector2_t2_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t2  p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t2  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/EditState
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
struct Object_t;
void* RuntimeInvoker_EditState_t598_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t2  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t2 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t388_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t383_Object_t_Int32_t372_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Int16_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t383_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Rect_t267_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t267 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
void* RuntimeInvoker_Mode_t612 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
void* RuntimeInvoker_Navigation_t613 (const MethodInfo* method, void* obj, void** args)
{
	typedef Navigation_t613  (*Func)(void* obj, const MethodInfo* method);
	Navigation_t613  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Scrollbar/Direction
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
void* RuntimeInvoker_Direction_t617 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Single_t388_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.Scrollbar/Axis
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
void* RuntimeInvoker_Axis_t619 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ScrollRect/MovementType
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
void* RuntimeInvoker_MovementType_t623 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ScrollRect/ScrollbarVisibility
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarVisibility.h"
void* RuntimeInvoker_ScrollbarVisibility_t624 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Single_t388_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Bounds_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef Bounds_t349  (*Func)(void* obj, const MethodInfo* method);
	Bounds_t349  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Navigation_t613 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Navigation_t613  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Navigation_t613 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
void* RuntimeInvoker_Transition_t628 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_ColorBlock_t551 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorBlock_t551  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorBlock_t551 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
void* RuntimeInvoker_SpriteState_t630 (const MethodInfo* method, void* obj, void** args)
{
	typedef SpriteState_t630  (*Func)(void* obj, const MethodInfo* method);
	SpriteState_t630  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SpriteState_t630 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpriteState_t630  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SpriteState_t630 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
void* RuntimeInvoker_SelectionState_t629 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t36  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t36_Object_t_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Object_t * p1, Vector2_t2  p2, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t2 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Color_t9_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t9  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t9 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_ColorU26_t4081_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t9 * p1, Color_t9  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Color_t9 *)args[0], *((Color_t9 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
void* RuntimeInvoker_Direction_t634 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
void* RuntimeInvoker_Axis_t636 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return ret;
}

struct Object_t;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
void* RuntimeInvoker_TextGenerationSettings_t715_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t715  (*Func)(void* obj, Vector2_t2  p1, const MethodInfo* method);
	TextGenerationSettings_t715  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t2_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t2  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector2_t2  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t267_Object_t_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t267_Rect_t267_Rect_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, Rect_t267  p1, Rect_t267  p2, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, *((Rect_t267 *)args[0]), *((Rect_t267 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t267_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
void* RuntimeInvoker_AspectMode_t650 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Single_t388_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
void* RuntimeInvoker_ScaleMode_t652 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
void* RuntimeInvoker_ScreenMatchMode_t653 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
void* RuntimeInvoker_Unit_t654 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
void* RuntimeInvoker_FitMode_t656 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
void* RuntimeInvoker_Corner_t658 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
void* RuntimeInvoker_Axis_t659 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
void* RuntimeInvoker_Constraint_t660 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Int32_t372_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Single_t388_Single_t388_Single_t388_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
void* RuntimeInvoker_Boolean_t384_LayoutRebuilder_t668 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LayoutRebuilder_t668  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LayoutRebuilder_t668 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t388_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t388_Object_t_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ILayoutElement_t719;
void* RuntimeInvoker_Single_t388_Object_t_Object_t_Single_t388_ILayoutElementU26_t4082 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, Object_t ** p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), (Object_t **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
void* RuntimeInvoker_Void_t1771_UIVertexU26_t4083_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t605 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertex_t605 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3_t36_Color32_t711_Vector2_t2_Vector2_t2_Vector3_t36_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36  p1, Color32_t711  p2, Vector2_t2  p3, Vector2_t2  p4, Vector3_t36  p5, Vector4_t680  p6, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Color32_t711 *)args[1]), *((Vector2_t2 *)args[2]), *((Vector2_t2 *)args[3]), *((Vector3_t36 *)args[4]), *((Vector4_t680 *)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3_t36_Color32_t711_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36  p1, Color32_t711  p2, Vector2_t2  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Color32_t711 *)args[1]), *((Vector2_t2 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_UIVertex_t605 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t605  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t605 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Color32_t711_Int32_t372_Int32_t372_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color32_t711  p2, int32_t p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t711 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.Double
#include "mscorlib_System_Double.h"
void* RuntimeInvoker_Void_t1771_Object_t_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Int64
#include "mscorlib_System_Int64.h"
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int64_t386_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
void* RuntimeInvoker_Void_t1771_GcAchievementDescriptionData_t938_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t938  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t938 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
void* RuntimeInvoker_Void_t1771_GcUserProfileData_t937_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t937  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t937 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Double_t387_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int64_t386_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct UserProfileU5BU5D_t802;
#include "UnityEngine_ArrayTypes.h"
struct Object_t;
void* RuntimeInvoker_Void_t1771_UserProfileU5BU5DU26_t4084_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t802** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t802**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UserProfileU5BU5D_t802;
void* RuntimeInvoker_Void_t1771_UserProfileU5BU5DU26_t4084_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t802** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t802**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
void* RuntimeInvoker_Void_t1771_GcScoreData_t940 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t940  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t940 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_BoundsU26_t4085 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t349 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Bounds_t349 *)args[0], method);
	return NULL;
}

struct Object_t;
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
void* RuntimeInvoker_Boolean_t384_BoneWeight_t405_BoneWeight_t405 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t405  p1, BoneWeight_t405  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t405 *)args[0]), *((BoneWeight_t405 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
void* RuntimeInvoker_ScreenOrientation_t944 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t36 *)args[0], method);
	return NULL;
}

struct Object_t;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
void* RuntimeInvoker_Void_t1771_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t404  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Matrix4x4U26_t4087 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t404 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4_t404 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t9  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t9 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_Color_t9_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t9  p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t9 *)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_ColorU26_t4081_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t9 * p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Color_t9 *)args[2], *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_ColorU26_t4081 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t9 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t9 *)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t36 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t36 *)args[1], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
void* RuntimeInvoker_TextureFormat_t947 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t9_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t9  (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	Color_t9  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Rect_t267_Int32_t372_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267  p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t267 *)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_RectU26_t4088_Int32_t372_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Rect_t267 * p2, int32_t p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Rect_t267 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_IntPtr_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.CullingGroupEvent
#include "UnityEngine_UnityEngine_CullingGroupEvent.h"
void* RuntimeInvoker_Void_t1771_CullingGroupEvent_t813 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CullingGroupEvent_t813  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CullingGroupEvent_t813 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_CullingGroupEvent_t813_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CullingGroupEvent_t813  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CullingGroupEvent_t813 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Color_t9_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t9  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t9 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Color_t9_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Color_t9  p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Color_t9 *)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_ColorU26_t4081_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Color_t9 * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Color_t9 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_SByte_t390_SByte_t390_SByte_t390_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t4089_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TouchScreenKeyboard_InternalConstructorHelperArguments_t819 * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (TouchScreenKeyboard_InternalConstructorHelperArguments_t819 *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_SByte_t390_SByte_t390_SByte_t390_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3_t36_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3U26_t4086_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36 * p1, Vector3_t36 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t36 *)args[0], (Vector3_t36 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_LayerMask_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t531  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t531 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LayerMask_t531_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t531  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t531  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t2_Vector2_t2_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t2  (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, const MethodInfo* method);
	Vector2_t2  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Vector2_t2_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t2  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t2_Vector2_t2_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t2  (*Func)(void* obj, Vector2_t2  p1, float p2, const MethodInfo* method);
	Vector2_t2  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector2_t2_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t2_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t2  (*Func)(void* obj, Vector3_t36  p1, const MethodInfo* method);
	Vector2_t2  ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Vector2_t2  p1, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Vector3_t36_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Vector3_t36  p1, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Vector3_t36_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t36  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Vector3_t36_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Vector3_t36  p1, float p2, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Single_t388_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, float p1, Vector3_t36  p2, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t36 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Single_t388_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Color_t9_Color_t9_Color_t9_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t9  (*Func)(void* obj, Color_t9  p1, Color_t9  p2, float p3, const MethodInfo* method);
	Color_t9  ret = ((Func)method->method)(obj, *((Color_t9 *)args[0]), *((Color_t9 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t9_Color_t9_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t9  (*Func)(void* obj, Color_t9  p1, float p2, const MethodInfo* method);
	Color_t9  ret = ((Func)method->method)(obj, *((Color_t9 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t680_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t680  (*Func)(void* obj, Color_t9  p1, const MethodInfo* method);
	Vector4_t680  ret = ((Func)method->method)(obj, *((Color_t9 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Color32_t711_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t711  (*Func)(void* obj, Color_t9  p1, const MethodInfo* method);
	Color32_t711  ret = ((Func)method->method)(obj, *((Color_t9 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t9_Color32_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t9  (*Func)(void* obj, Color32_t711  p1, const MethodInfo* method);
	Color_t9  ret = ((Func)method->method)(obj, *((Color32_t711 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Quaternion_t41_Quaternion_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Quaternion_t41  p1, Quaternion_t41  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Quaternion_t41 *)args[0]), *((Quaternion_t41 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_Single_t388_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, float p1, Vector3_t36  p2, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t36 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_Single_t388_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, float p1, Vector3_t36 * p2, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, *((float*)args[0]), (Vector3_t36 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SingleU26_t4090_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, Vector3_t36 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], (Vector3_t36 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_Vector3_t36_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_Vector3U26_t4086_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, Vector3_t36 * p1, Vector3_t36 * p2, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, (Vector3_t36 *)args[0], (Vector3_t36 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_Quaternion_t41_Quaternion_t41_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, Quaternion_t41  p1, Quaternion_t41  p2, float p3, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, *((Quaternion_t41 *)args[0]), *((Quaternion_t41 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_QuaternionU26_t4091_QuaternionU26_t4091_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, Quaternion_t41 * p1, Quaternion_t41 * p2, float p3, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, (Quaternion_t41 *)args[0], (Quaternion_t41 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_Quaternion_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, Quaternion_t41  p1, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, *((Quaternion_t41 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_QuaternionU26_t4091 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, Quaternion_t41 * p1, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, (Quaternion_t41 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, Vector3_t36  p1, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Quaternion_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Quaternion_t41  p1, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((Quaternion_t41 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_QuaternionU26_t4091 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Quaternion_t41 * p1, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, (Quaternion_t41 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, Vector3_t36 * p1, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, (Vector3_t36 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Quaternion_t41_Vector3U26_t4086_SingleU26_t4090 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t41  p1, Vector3_t36 * p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t41 *)args[0]), (Vector3_t36 *)args[1], (float*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_QuaternionU26_t4091_Vector3U26_t4086_SingleU26_t4090 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t41 * p1, Vector3_t36 * p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t41 *)args[0], (Vector3_t36 *)args[1], (float*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_Quaternion_t41_Quaternion_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, Quaternion_t41  p1, Quaternion_t41  p2, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, *((Quaternion_t41 *)args[0]), *((Quaternion_t41 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Quaternion_t41_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Quaternion_t41  p1, Vector3_t36  p2, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((Quaternion_t41 *)args[0]), *((Vector3_t36 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Rect_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t267  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t267 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Rect_t267_Rect_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t267  p1, Rect_t267  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t267 *)args[0]), *((Rect_t267 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, Matrix4x4_t404  p1, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Matrix4x4U26_t4087 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, Matrix4x4_t404 * p1, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, (Matrix4x4_t404 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Matrix4x4_t404_Matrix4x4U26_t4087 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t404  p1, Matrix4x4_t404 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), (Matrix4x4_t404 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Matrix4x4U26_t4087_Matrix4x4U26_t4087 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t404 * p1, Matrix4x4_t404 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t404 *)args[0], (Matrix4x4_t404 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t680_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t680  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t680  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t680  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t680 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, Vector3_t36  p1, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3_t36_Quaternion_t41_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36  p1, Quaternion_t41  p2, Vector3_t36  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Quaternion_t41 *)args[1]), *((Vector3_t36 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Vector3_t36_Quaternion_t41_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, Vector3_t36  p1, Quaternion_t41  p2, Vector3_t36  p3, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Quaternion_t41 *)args[1]), *((Vector3_t36 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Vector3U26_t4086_QuaternionU26_t4091_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, Vector3_t36 * p1, Quaternion_t41 * p2, Vector3_t36 * p3, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, (Vector3_t36 *)args[0], (Quaternion_t41 *)args[1], (Vector3_t36 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Single_t388_Single_t388_Single_t388_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Single_t388_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Matrix4x4_t404_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, Matrix4x4_t404  p1, Matrix4x4_t404  p2, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), *((Matrix4x4_t404 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t680_Matrix4x4_t404_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t680  (*Func)(void* obj, Matrix4x4_t404  p1, Vector4_t680  p2, const MethodInfo* method);
	Vector4_t680  ret = ((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), *((Vector4_t680 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Matrix4x4_t404_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t404  p1, Matrix4x4_t404  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), *((Matrix4x4_t404 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Bounds_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t349  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t349 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Bounds_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t349  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t349 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Bounds_t349_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t349  p1, Vector3_t36  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t349 *)args[0]), *((Vector3_t36 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_BoundsU26_t4085_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t349 * p1, Vector3_t36 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t349 *)args[0], (Vector3_t36 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Bounds_t349_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t349  p1, Vector3_t36  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t349 *)args[0]), *((Vector3_t36 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_BoundsU26_t4085_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t349 * p1, Vector3_t36 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t349 *)args[0], (Vector3_t36 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
void* RuntimeInvoker_Boolean_t384_RayU26_t4092_BoundsU26_t4085_SingleU26_t4090 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t382 * p1, Bounds_t349 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t382 *)args[0], (Bounds_t349 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Ray_t382 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t382  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t382 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Ray_t382_SingleU26_t4090 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t382  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t382 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_BoundsU26_t4085_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Bounds_t349 * p1, Vector3_t36 * p2, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, (Bounds_t349 *)args[0], (Vector3_t36 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Vector4_t680_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t680  p1, Vector4_t680  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), *((Vector4_t680 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t680  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t680  (*Func)(void* obj, const MethodInfo* method);
	Vector4_t680  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t680_Vector4_t680_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t680  (*Func)(void* obj, Vector4_t680  p1, Vector4_t680  p2, const MethodInfo* method);
	Vector4_t680  ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), *((Vector4_t680 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t680_Vector4_t680_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t680  (*Func)(void* obj, Vector4_t680  p1, float p2, const MethodInfo* method);
	Vector4_t680  ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector4_t680_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t680  p1, Vector4_t680  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), *((Vector4_t680 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Single_t388_Single_t388_SingleU26_t4090_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Single_t388_Single_t388_SingleU26_t4090_Single_t388_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_RectU26_t4088 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t267 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector2U26_t4093 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t2 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t2 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color_t9  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color_t9 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_ColorU26_t4081 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Color_t9 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Color_t9 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Color_t9_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t9  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Color_t9  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t9_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t9  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Color_t9  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
void* RuntimeInvoker_Void_t1771_SphericalHarmonicsL2U26_t4094 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t833 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t833 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Color_t9_SphericalHarmonicsL2U26_t4094 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t9  p1, SphericalHarmonicsL2_t833 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t9 *)args[0]), (SphericalHarmonicsL2_t833 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_ColorU26_t4081_SphericalHarmonicsL2U26_t4094 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t9 * p1, SphericalHarmonicsL2_t833 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t9 *)args[0], (SphericalHarmonicsL2_t833 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3_t36_Color_t9_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36  p1, Color_t9  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Color_t9 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3_t36_Color_t9_SphericalHarmonicsL2U26_t4094 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36  p1, Color_t9  p2, SphericalHarmonicsL2_t833 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Color_t9 *)args[1]), (SphericalHarmonicsL2_t833 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3U26_t4086_ColorU26_t4081_SphericalHarmonicsL2U26_t4094 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36 * p1, Color_t9 * p2, SphericalHarmonicsL2_t833 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t36 *)args[0], (Color_t9 *)args[1], (SphericalHarmonicsL2_t833 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t833_SphericalHarmonicsL2_t833_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t833  (*Func)(void* obj, SphericalHarmonicsL2_t833  p1, float p2, const MethodInfo* method);
	SphericalHarmonicsL2_t833  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t833 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t833_Single_t388_SphericalHarmonicsL2_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t833  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t833  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t833  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t833 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t833_SphericalHarmonicsL2_t833_SphericalHarmonicsL2_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t833  (*Func)(void* obj, SphericalHarmonicsL2_t833  p1, SphericalHarmonicsL2_t833  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t833  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t833 *)args[0]), *((SphericalHarmonicsL2_t833 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_SphericalHarmonicsL2_t833_SphericalHarmonicsL2_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t833  p1, SphericalHarmonicsL2_t833  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t833 *)args[0]), *((SphericalHarmonicsL2_t833 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector4U26_t4095 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t680 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4_t680 *)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector4_t680_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t680  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector4_t680  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t2_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t2  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector2_t2  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Vector2U26_t4093 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t2 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t2 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t390_SByte_t390_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
void* RuntimeInvoker_RuntimePlatform_t788 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
void* RuntimeInvoker_CameraClearFlags_t943 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t36_Object_t_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Object_t * p1, Vector3_t36 * p2, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t36 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Ray_t382_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t382  (*Func)(void* obj, Vector3_t36  p1, const MethodInfo* method);
	Ray_t382  ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Ray_t382_Object_t_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t382  (*Func)(void* obj, Object_t * p1, Vector3_t36 * p2, const MethodInfo* method);
	Ray_t382  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t36 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Ray_t382_Single_t388_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t382  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t382 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_RayU26_t4092_Single_t388_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t382 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t382 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_RayU26_t4092_Single_t388_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t382 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t382 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
void* RuntimeInvoker_RenderBuffer_t942 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t942  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t942  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Int32U26_t4080_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_RenderBufferU26_t4096_RenderBufferU26_t4096 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t942 * p2, RenderBuffer_t942 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t942 *)args[1], (RenderBuffer_t942 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Int32U26_t4080_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
void* RuntimeInvoker_TouchPhase_t847 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Touch_t705_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t705  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Touch_t705  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t390_SByte_t390_SByte_t390_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Quaternion_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t41  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t41 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_QuaternionU26_t4091 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t41 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t41 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3_t36_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t36  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t36 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Vector3U26_t4086_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t36 * p2, Vector3_t36 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t36 *)args[1], (Vector3_t36 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_SByte_t390_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector3_t36_Vector3_t36_RaycastHitU26_t4097_Single_t388_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, RaycastHit_t60 * p3, float p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), (RaycastHit_t60 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Ray_t382_RaycastHitU26_t4097_Single_t388_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t382  p1, RaycastHit_t60 * p2, float p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t382 *)args[0]), (RaycastHit_t60 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Ray_t382_RaycastHitU26_t4097_Single_t388_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t382  p1, RaycastHit_t60 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t382 *)args[0]), (RaycastHit_t60 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Ray_t382_Single_t388_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t382  p1, float p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t382 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t36_Vector3_t36_Single_t388_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3U26_t4086_Vector3U26_t4086_Single_t388_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t36 * p1, Vector3_t36 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t36 *)args[0], (Vector3_t36 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector3U26_t4086_Vector3U26_t4086_RaycastHitU26_t4097_Single_t388_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t36 * p1, Vector3_t36 * p2, RaycastHit_t60 * p3, float p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t36 *)args[0], (Vector3_t36 *)args[1], (RaycastHit_t60 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t36 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t36 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_QuaternionU26_t4091 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Quaternion_t41 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Quaternion_t41 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Ray_t382_RaycastHitU26_t4097_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Ray_t382  p2, RaycastHit_t60 * p3, float p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Ray_t382 *)args[1]), (RaycastHit_t60 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_RayU26_t4092_RaycastHitU26_t4097_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Ray_t382 * p2, RaycastHit_t60 * p3, float p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t382 *)args[1], (RaycastHit_t60 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Ray_t382_RaycastHitU26_t4097_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t382  p1, RaycastHit_t60 * p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t382 *)args[0]), (RaycastHit_t60 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
void* RuntimeInvoker_Void_t1771_Vector2_t2_Vector2_t2_Single_t388_Int32_t372_Single_t388_Single_t388_RaycastHit2DU26_t4098 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t729 * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t729 *)args[6], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector2U26_t4093_Vector2U26_t4093_Single_t388_Int32_t372_Single_t388_Single_t388_RaycastHit2DU26_t4098 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t2 * p1, Vector2_t2 * p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t729 * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t2 *)args[0], (Vector2_t2 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t729 *)args[6], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t729_Vector2_t2_Vector2_t2_Single_t388_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t729  (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, float p3, int32_t p4, const MethodInfo* method);
	RaycastHit2D_t729  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t729_Vector2_t2_Vector2_t2_Single_t388_Int32_t372_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t729  (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	RaycastHit2D_t729  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t2_Vector2_t2_Single_t388_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2U26_t4093_Vector2U26_t4093_Single_t388_Int32_t372_Single_t388_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t2 * p1, Vector2_t2 * p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector2_t2 *)args[0], (Vector2_t2 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t390_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
void* RuntimeInvoker_SendMessageOptions_t786 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
void* RuntimeInvoker_AnimatorStateInfo_t881 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t881  (*Func)(void* obj, const MethodInfo* method);
	AnimatorStateInfo_t881  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
void* RuntimeInvoker_AnimatorClipInfo_t882 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t882  (*Func)(void* obj, const MethodInfo* method);
	AnimatorClipInfo_t882  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfo.h"
void* RuntimeInvoker_Boolean_t384_Int16_t392_CharacterInfoU26_t4099_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t892 * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t892 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int16_t392_CharacterInfoU26_t4099_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t892 * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t892 *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int16_t392_CharacterInfoU26_t4099 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t892 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t892 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Color_t9_Int32_t372_Single_t388_Single_t388_Int32_t372_SByte_t390_SByte_t390_Int32_t372_Int32_t372_Int32_t372_Int32_t372_SByte_t390_Int32_t372_Vector2_t2_Vector2_t2_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t9  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, Vector2_t2  p16, Vector2_t2  p17, int8_t p18, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t9 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((Vector2_t2 *)args[15]), *((Vector2_t2 *)args[16]), *((int8_t*)args[17]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Color_t9_Int32_t372_Single_t388_Single_t388_Int32_t372_SByte_t390_SByte_t390_Int32_t372_Int32_t372_Int32_t372_Int32_t372_SByte_t390_Int32_t372_Single_t388_Single_t388_Single_t388_Single_t388_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t9  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t9 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_ColorU26_t4081_Int32_t372_Single_t388_Single_t388_Int32_t372_SByte_t390_SByte_t390_Int32_t372_Int32_t372_Int32_t372_Int32_t372_SByte_t390_Int32_t372_Single_t388_Single_t388_Single_t388_Single_t388_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t9 * p4, int32_t p5, float p6, float p7, int32_t p8, int8_t p9, int8_t p10, int32_t p11, int32_t p12, int32_t p13, int32_t p14, int8_t p15, int32_t p16, float p17, float p18, float p19, float p20, int8_t p21, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t9 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((float*)args[6]), *((int32_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int32_t*)args[13]), *((int8_t*)args[14]), *((int32_t*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((float*)args[19]), *((int8_t*)args[20]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextGenerationSettings_t715_TextGenerationSettings_t715 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t715  (*Func)(void* obj, TextGenerationSettings_t715  p1, const MethodInfo* method);
	TextGenerationSettings_t715  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t715 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t388_Object_t_TextGenerationSettings_t715 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t715  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t715 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_TextGenerationSettings_t715 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t715  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t715 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
void* RuntimeInvoker_RenderMode_t896 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_ColorU26_t4081 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t9 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Color_t9 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_RectU26_t4088 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Rect_t267 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Rect_t267 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Vector2_t2_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t2  p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t2 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Vector2U26_t4093_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t2 * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t2 *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t2_Vector2_t2_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t2  (*Func)(void* obj, Vector2_t2  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Vector2_t2  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector2_t2_Object_t_Object_t_Vector2U26_t4093 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t2  p1, Object_t * p2, Object_t * p3, Vector2_t2 * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t2 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Vector2_t2 *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector2U26_t4093_Object_t_Object_t_Vector2U26_t4093 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t2 * p1, Object_t * p2, Object_t * p3, Vector2_t2 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t2 *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Vector2_t2 *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Vector2_t2_Object_t_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t2  p2, Object_t * p3, Vector3_t36 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t2 *)args[1]), (Object_t *)args[2], (Vector3_t36 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Vector2_t2_Object_t_Vector2U26_t4093 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t2  p2, Object_t * p3, Vector2_t2 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t2 *)args[1]), (Object_t *)args[2], (Vector2_t2 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Ray_t382_Object_t_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t382  (*Func)(void* obj, Object_t * p1, Vector2_t2  p2, const MethodInfo* method);
	Ray_t382  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t2 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Ray_t382 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t382  (*Func)(void* obj, const MethodInfo* method);
	Ray_t382  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Ray_t382 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Ray_t382  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Ray_t382 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
void* RuntimeInvoker_EventType_t898 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_EventType_t898_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
void* RuntimeInvoker_EventModifiers_t899 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
void* RuntimeInvoker_KeyCode_t897 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t74  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t74 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Rect_t267_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t267 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Rect_t267_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t267 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Rect_t267_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t267  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t267 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Rect_t267_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t267  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t267 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t267_Int32_t372_Rect_t267_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, int32_t p1, Rect_t267  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t267 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Object_t_Int32_t372_Single_t388_Single_t388_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Rect_t267_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t267 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_RectU26_t4088_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t267 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Rect_t267_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t267  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t267 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_RectU26_t4088_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t267 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t267 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t267_Int32_t372_Rect_t267_Object_t_Object_t_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, int32_t p1, Rect_t267  p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t267 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t267_Int32_t372_RectU26_t4088_Object_t_Object_t_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, int32_t p1, Rect_t267 * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t267 *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t267_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t267_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Rect_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t267  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t267 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_RectU26_t4088 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t267 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t267 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Single_t388_Single_t388_Single_t388_Single_t388_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Single_t388_Single_t388_Single_t388_Single_t388_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Rect_t267_Object_t_SByte_t390_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t267  p2, Object_t * p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t267 *)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Rect_t267_Object_t_SByte_t390_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267  p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t267 *)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Rect_t267_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t267 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Rect_t267_Object_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267  p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t267 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t388_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_SingleU26_t4090_SingleU26_t4090 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float* p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (float*)args[1], (float*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.ImagePosition
#include "UnityEngine_UnityEngine_ImagePosition.h"
void* RuntimeInvoker_ImagePosition_t919 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// UnityEngine.Internal_DrawArguments
#include "UnityEngine_UnityEngine_Internal_DrawArguments.h"
void* RuntimeInvoker_Void_t1771_Object_t_Internal_DrawArgumentsU26_t4100 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Internal_DrawArguments_t922 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Internal_DrawArguments_t922 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Rect_t267_Object_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t267  p2, Object_t * p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t267 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_RectU26_t4088_Object_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t267 * p2, Object_t * p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t267 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Object_t_Vector2U26_t4093 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Vector2_t2 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Vector2_t2 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t388_IntPtr_t_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Object_t_SingleU26_t4090_SingleU26_t4090 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float* p3, float* p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (float*)args[2], (float*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
void* RuntimeInvoker_UserState_t962 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Double_t387_SByte_t390_SByte_t390_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t74  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t74 *)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t390_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int64_t386_Object_t_DateTime_t74_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t74  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t74 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
void* RuntimeInvoker_UserScope_t963 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
void* RuntimeInvoker_Range_t957 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t957  (*Func)(void* obj, const MethodInfo* method);
	Range_t957  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Range_t957 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t957  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t957 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
void* RuntimeInvoker_TimeScope_t964 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
void* RuntimeInvoker_Void_t1771_Int32_t372_HitInfo_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t959  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t959 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_HitInfo_t959_HitInfo_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t959  p1, HitInfo_t959  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t959 *)args[0]), *((HitInfo_t959 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_HitInfo_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t959  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t959 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct String_t;
struct String_t;
void* RuntimeInvoker_Void_t1771_Object_t_StringU26_t4077_StringU26_t4077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
void* RuntimeInvoker_Void_t1771_Object_t_StreamingContext_t1013 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t1013  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1013 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Color_t9_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t9  p1, Color_t9  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t9 *)args[0]), *((Color_t9 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_TextGenerationSettings_t715 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t715  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t715 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
void* RuntimeInvoker_PersistentListenerMode_t979 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
void* RuntimeInvoker_CameraDirection_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.VuforiaRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid_0.h"
void* RuntimeInvoker_VideoBackgroundReflection_t1155 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, Matrix4x4_t404  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((Matrix4x4_t404 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Matrix4x4_t404  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Matrix4x4_t404 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Matrix4x4_t404_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t404  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Matrix4x4_t404  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Matrix4x4_t404 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390_Single_t388_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, float p4, int32_t p5, Object_t * p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((float*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Matrix4x4_t404_SingleU26_t4090_SingleU26_t4090 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t404  p1, float* p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), (float*)args[1], (float*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Matrix4x4_t404  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Vector4_t680  p1, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
void* RuntimeInvoker_Status_t1058 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
void* RuntimeInvoker_VideoModeData_t1065 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoModeData_t1065  (*Func)(void* obj, const MethodInfo* method);
	VideoModeData_t1065  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_VideoModeData_t1065_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoModeData_t1065  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	VideoModeData_t1065  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"
void* RuntimeInvoker_Boolean_t384_CameraDeviceModeU26_t4101 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_RectU26_t4088 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t267 * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t267 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t36_Quaternion_t41_Vector3_t36_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector3_t36  p3, Quaternion_t41  p4, Vector3_t36  p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector3_t36 *)args[2]), *((Quaternion_t41 *)args[3]), *((Vector3_t36 *)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Vector3_t36_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector3_t36  p2, Vector3_t36  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t36 *)args[1]), *((Vector3_t36 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Vector3_t36_Vector3_t36_Vector3_t36_Quaternion_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector3_t36  p2, Vector3_t36  p3, Vector3_t36  p4, Quaternion_t41  p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t36 *)args[1]), *((Vector3_t36 *)args[2]), *((Vector3_t36 *)args[3]), *((Quaternion_t41 *)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3U26_t4086_Vector3U26_t4086 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t36 * p1, Vector3_t36 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t36 *)args[0], (Vector3_t36 *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3U26_t4086_Vector3U26_t4086_Vector3U26_t4086_QuaternionU26_t4091 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t36 * p1, Vector3_t36 * p2, Vector3_t36 * p3, Quaternion_t41 * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t36 *)args[0], (Vector3_t36 *)args[1], (Vector3_t36 *)args[2], (Quaternion_t41 *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_IntPtr_t_Object_t_Vector3_t36_Vector3_t36_Vector3_t36_Quaternion_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Vector3_t36  p3, Vector3_t36  p4, Vector3_t36  p5, Quaternion_t41  p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((Vector3_t36 *)args[2]), *((Vector3_t36 *)args[3]), *((Vector3_t36 *)args[4]), *((Quaternion_t41 *)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.VuforiaUnity/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Storag.h"
void* RuntimeInvoker_StorageType_t1235 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector2_t2_Vector2_t2_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3_t36_Vector3_t36_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
void* RuntimeInvoker_Void_t1771_TargetSearchResult_t1212 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TargetSearchResult_t1212  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TargetSearchResult_t1212 *)args[0]), method);
	return NULL;
}

struct Object_t;
// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"
void* RuntimeInvoker_ImageTargetType_t1098 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"
void* RuntimeInvoker_Object_t_Object_t_RectangleData_t1089 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, RectangleData_t1089  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RectangleData_t1089 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Vector3_t36  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((Vector3_t36 *)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"
void* RuntimeInvoker_FrameQuality_t1100 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct VirtualButtonAbstractBehaviour_t319;
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
void* RuntimeInvoker_Boolean_t384_Int32_t372_VirtualButtonAbstractBehaviourU26_t4102 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, VirtualButtonAbstractBehaviour_t319 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (VirtualButtonAbstractBehaviour_t319 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterMode.h"
void* RuntimeInvoker_WordFilterMode_t1248 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.WordPrefabCreationMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefabCreationM.h"
void* RuntimeInvoker_WordPrefabCreationMode_t1179 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.VirtualButton/Sensitivity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"
void* RuntimeInvoker_Sensitivity_t1222 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t404  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"
void* RuntimeInvoker_WordTemplateMode_t1107 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
void* RuntimeInvoker_PIXEL_FORMAT_t1108 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextureFormat_t947_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Single_t388_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.VuforiaRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec.h"
void* RuntimeInvoker_Void_t1771_Int32_t372_Vec2I_t1157 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vec2I_t1157  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vec2I_t1157 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Vuforia.WebCamProfile/ProfileCollection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"
struct Object_t;
void* RuntimeInvoker_ProfileCollection_t1227_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileCollection_t1227  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	ProfileCollection_t1227  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.VuforiaAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeha_0.h"
void* RuntimeInvoker_WorldCenterMode_t1237 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.VuforiaManagerImpl/FrameState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__11.h"
void* RuntimeInvoker_Void_t1771_FrameState_t1145 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FrameState_t1145  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((FrameState_t1145 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_FrameState_t1145_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FrameState_t1145  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((FrameState_t1145 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// Vuforia.VuforiaManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"
void* RuntimeInvoker_Boolean_t384_TrackableResultData_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TrackableResultData_t1134  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TrackableResultData_t1134 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.VuforiaRenderer/VideoBGCfgData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid_1.h"
void* RuntimeInvoker_VideoBGCfgData_t1156 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoBGCfgData_t1156  (*Func)(void* obj, const MethodInfo* method);
	VideoBGCfgData_t1156  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_VideoBGCfgData_t1156 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, VideoBGCfgData_t1156  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((VideoBGCfgData_t1156 *)args[0]), method);
	return NULL;
}

struct Object_t;
// Vuforia.VuforiaRenderer/VideoTextureInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid.h"
void* RuntimeInvoker_VideoTextureInfo_t1049 (const MethodInfo* method, void* obj, void** args)
{
	typedef VideoTextureInfo_t1049  (*Func)(void* obj, const MethodInfo* method);
	VideoTextureInfo_t1049  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Single_t388_Single_t388_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, float p1, float p2, int32_t p3, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.VuforiaManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_.h"
void* RuntimeInvoker_Void_t1771_PoseData_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PoseData_t1133  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((PoseData_t1133 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
// Vuforia.OrientedBoundingBox3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0.h"
void* RuntimeInvoker_OrientedBoundingBox3D_t1092 (const MethodInfo* method, void* obj, void** args)
{
	typedef OrientedBoundingBox3D_t1092  (*Func)(void* obj, const MethodInfo* method);
	OrientedBoundingBox3D_t1092  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_OrientedBoundingBox3D_t1092 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OrientedBoundingBox3D_t1092  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OrientedBoundingBox3D_t1092 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_RectU26_t4088_RectU26_t4088 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t267 * p1, Rect_t267 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t267 *)args[0], (Rect_t267 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.RectangleIntData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleIntData.h"
void* RuntimeInvoker_Rect_t267_RectangleIntData_t1090_Rect_t267_SByte_t390_VideoModeData_t1065 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, RectangleIntData_t1090  p1, Rect_t267  p2, int8_t p3, VideoModeData_t1065  p4, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, *((RectangleIntData_t1090 *)args[0]), *((Rect_t267 *)args[1]), *((int8_t*)args[2]), *((VideoModeData_t1065 *)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.TextTrackerImpl/UpDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_UpD.h"
void* RuntimeInvoker_UpDirection_t1171 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt16
#include "mscorlib_System_UInt16.h"
struct Object_t;
void* RuntimeInvoker_UInt16_t393_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Vec2I_t1157 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Vec2I_t1157  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((Vec2I_t1157 *)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Vector2_t2  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((Vector2_t2 *)args[2]), method);
	return NULL;
}

struct Object_t;
// Vuforia.VuforiaManagerImpl/ImageHeaderData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__6.h"
struct Object_t;
void* RuntimeInvoker_Void_t1771_ImageHeaderData_t1140_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ImageHeaderData_t1140  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((ImageHeaderData_t1140 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct WordAbstractBehaviour_t327;
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
void* RuntimeInvoker_Boolean_t384_Object_t_WordAbstractBehaviourU26_t4103 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, WordAbstractBehaviour_t327 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (WordAbstractBehaviour_t327 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"
void* RuntimeInvoker_OrientedBoundingBox_t1091 (const MethodInfo* method, void* obj, void** args)
{
	typedef OrientedBoundingBox_t1091  (*Func)(void* obj, const MethodInfo* method);
	OrientedBoundingBox_t1091  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector3_t36_Quaternion_t41 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t36  p1, Quaternion_t41  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Quaternion_t41 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_OrientedBoundingBox_t1091 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OrientedBoundingBox_t1091  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OrientedBoundingBox_t1091 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, IntPtr_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_IntPtr_t_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, IntPtr_t p2, int32_t p3, IntPtr_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), *((int32_t*)args[2]), *((IntPtr_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_IntPtr_t_Object_t_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, Object_t * p3, int32_t p4, IntPtr_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((IntPtr_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Object_t * p3, IntPtr_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((IntPtr_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_IntPtr_t_Int32_t372_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, int32_t p3, IntPtr_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), *((int32_t*)args[2]), *((IntPtr_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Int32_t372_IntPtr_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Single_t388_Single_t388_IntPtr_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_IntPtr_t_IntPtr_t_Int32_t372_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, int32_t p3, IntPtr_t p4, IntPtr_t p5, IntPtr_t p6, IntPtr_t p7, float p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), *((int32_t*)args[2]), *((IntPtr_t*)args[3]), *((IntPtr_t*)args[4]), *((IntPtr_t*)args[5]), *((IntPtr_t*)args[6]), *((float*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_IntPtr_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, IntPtr_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_IntPtr_t_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, IntPtr_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.SmartTerrainInitializationInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitial.h"
void* RuntimeInvoker_Void_t1771_SmartTerrainInitializationInfo_t1083 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SmartTerrainInitializationInfo_t1083  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SmartTerrainInitializationInfo_t1083 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct PropAbstractBehaviour_t300;
// Vuforia.PropAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavio.h"
void* RuntimeInvoker_Boolean_t384_Object_t_PropAbstractBehaviourU26_t4104 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, PropAbstractBehaviour_t300 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (PropAbstractBehaviour_t300 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct SurfaceAbstractBehaviour_t306;
// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"
void* RuntimeInvoker_Boolean_t384_Object_t_SurfaceAbstractBehaviourU26_t4105 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, SurfaceAbstractBehaviour_t306 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (SurfaceAbstractBehaviour_t306 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// Vuforia.VuforiaManagerImpl/MeshData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__7.h"
struct Object_t;
void* RuntimeInvoker_Object_t_MeshData_t1141_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, MeshData_t1141  p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((MeshData_t1141 *)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct LinkedList_1_t1152;
// System.Collections.Generic.LinkedList`1<System.Int32>
#include "System_System_Collections_Generic_LinkedList_1_gen_1.h"
void* RuntimeInvoker_Void_t1771_LinkedList_1U26_t4106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LinkedList_1_t1152 ** p1, const MethodInfo* method);
	((Func)method->method)(obj, (LinkedList_1_t1152 **)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_PoseData_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, PoseData_t1133  p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((PoseData_t1133 *)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Matrix4x4_t404  p1, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t41_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t41  (*Func)(void* obj, Matrix4x4_t404  p1, const MethodInfo* method);
	Quaternion_t41  ret = ((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_PoseData_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, PoseData_t1133  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((PoseData_t1133 *)args[2]), method);
	return NULL;
}

struct Object_t;
// Vuforia.TargetFinder/InitState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"
void* RuntimeInvoker_InitState_t1209 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.TargetFinder/UpdateState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"
void* RuntimeInvoker_UpdateState_t1210 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UpdateState_t1210_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_TargetSearchResult_t1212_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, TargetSearchResult_t1212  p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((TargetSearchResult_t1212 *)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_RectangleData_t1089 (const MethodInfo* method, void* obj, void** args)
{
	typedef RectangleData_t1089  (*Func)(void* obj, const MethodInfo* method);
	RectangleData_t1089  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_RectangleData_t1089 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RectangleData_t1089  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RectangleData_t1089 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_RectangleData_t1089_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, RectangleData_t1089  p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((RectangleData_t1089 *)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vec2I_t1157 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vec2I_t1157  (*Func)(void* obj, const MethodInfo* method);
	Vec2I_t1157  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
void* RuntimeInvoker_ProfileData_t1226 (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t1226  (*Func)(void* obj, const MethodInfo* method);
	ProfileData_t1226  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_ProfileData_t1226_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t1226  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	ProfileData_t1226  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_ProfileData_t1226_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ProfileData_t1226  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((ProfileData_t1226 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector2_t2_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector2_t2  p2, Vector2_t2  p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t2 *)args[1]), *((Vector2_t2 *)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector2_t2_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector2_t2  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t2 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_CameraDeviceMode_t1063 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vec2I_t1157_Vector2_t2_Rect_t267_SByte_t390_VideoModeData_t1065 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vec2I_t1157  (*Func)(void* obj, Vector2_t2  p1, Rect_t267  p2, int8_t p3, VideoModeData_t1065  p4, const MethodInfo* method);
	Vec2I_t1157  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Rect_t267 *)args[1]), *((int8_t*)args[2]), *((VideoModeData_t1065 *)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t2_Vector2_t2_Rect_t267_SByte_t390_VideoModeData_t1065 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t2  (*Func)(void* obj, Vector2_t2  p1, Rect_t267  p2, int8_t p3, VideoModeData_t1065  p4, const MethodInfo* method);
	Vector2_t2  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Rect_t267 *)args[1]), *((int8_t*)args[2]), *((VideoModeData_t1065 *)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_OrientedBoundingBox_t1091_OrientedBoundingBox_t1091_Rect_t267_SByte_t390_VideoModeData_t1065 (const MethodInfo* method, void* obj, void** args)
{
	typedef OrientedBoundingBox_t1091  (*Func)(void* obj, OrientedBoundingBox_t1091  p1, Rect_t267  p2, int8_t p3, VideoModeData_t1065  p4, const MethodInfo* method);
	OrientedBoundingBox_t1091  ret = ((Func)method->method)(obj, *((OrientedBoundingBox_t1091 *)args[0]), *((Rect_t267 *)args[1]), *((int8_t*)args[2]), *((VideoModeData_t1065 *)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Rect_t267_SByte_t390_Vector2U26_t4093_Vector2U26_t4093 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t267  p1, int8_t p2, Vector2_t2 * p3, Vector2_t2 * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t267 *)args[0]), *((int8_t*)args[1]), (Vector2_t2 *)args[2], (Vector2_t2 *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Rect_t267_Vector2_t2_Vector2_t2_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t267  (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, int8_t p3, const MethodInfo* method);
	Rect_t267  ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_SingleU26_t4090_SingleU26_t4090_SingleU26_t4090_SingleU26_t4090_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, float* p2, float* p3, float* p4, float* p5, bool* p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (float*)args[1], (float*)args[2], (float*)args[3], (float*)args[4], (bool*)args[5], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector2U26_t4093_Vector2U26_t4093 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t2 * p1, Vector2_t2 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector2_t2 *)args[0], (Vector2_t2 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector2_t2_Vector2_t2_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_SByte_t390_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
// System.UInt32
#include "mscorlib_System_UInt32.h"
void* RuntimeInvoker_UInt32_t389_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t389_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t1426_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t1430 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Byte
#include "mscorlib_System_Byte.h"
void* RuntimeInvoker_Byte_t391 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32U26_t4080_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t139;
#include "mscorlib_ArrayTypes.h"
void* RuntimeInvoker_Void_t1771_Object_t_Int32U26_t4080_ByteU26_t4107_Int32U26_t4080_ByteU5BU5DU26_t4108 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t139** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t139**)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t74_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t1561 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t1561  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t1561 *)args[1]), method);
	return ret;
}

struct Object_t;
// System.Security.Cryptography.RSAParameters
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
void* RuntimeInvoker_RSAParameters_t1533_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1533  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t1533  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_RSAParameters_t1533 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t1533  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t1533 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t1561_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1561  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t1561  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t390_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t74  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.X509.X509ChainStatusFlags
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
void* RuntimeInvoker_X509ChainStatusFlags_t1466 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Byte_t391 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Byte_t391_Byte_t391 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertLevel
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
void* RuntimeInvoker_AlertLevel_t1485 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
void* RuntimeInvoker_AlertDescription_t1486 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t391 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int16_t392_Object_t_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390_SByte_t390_SByte_t390_Int16_t392_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
void* RuntimeInvoker_CipherAlgorithmType_t1488 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HashAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
void* RuntimeInvoker_HashAlgorithmType_t1506 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
void* RuntimeInvoker_ExchangeAlgorithmType_t1504 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
void* RuntimeInvoker_CipherMode_t1417 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t139;
struct ByteU5BU5D_t139;
void* RuntimeInvoker_Void_t1771_Object_t_ByteU5BU5DU26_t4108_ByteU5BU5DU26_t4108 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t139** p2, ByteU5BU5D_t139** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t139**)args[1], (ByteU5BU5D_t139**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t391_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t392_Object_t_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390_SByte_t390_SByte_t390_Int16_t392_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
void* RuntimeInvoker_SecurityProtocolType_t1520 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityCompressionType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
void* RuntimeInvoker_SecurityCompressionType_t1519 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
void* RuntimeInvoker_HandshakeType_t1536 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
void* RuntimeInvoker_HandshakeState_t1505 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt64
#include "mscorlib_System_UInt64.h"
void* RuntimeInvoker_UInt64_t394 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SecurityProtocolType_t1520_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t391_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t391_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Byte_t391_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t391_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t386_Int64_t386_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Int32_t372_Int32_t372_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Byte_t391_Byte_t391_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_RSAParameters_t1533 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1533  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t1533  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Byte_t391 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Byte_t391_Byte_t391 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Byte_t391_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.ContentType
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
void* RuntimeInvoker_ContentType_t1499 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DictionaryNode_t1603;
// System.Collections.Specialized.ListDictionary/DictionaryNode
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t4109 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t1603 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t1603 **)args[1], method);
	return ret;
}

struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
void* RuntimeInvoker_DictionaryEntry_t451 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"
void* RuntimeInvoker_EditorBrowsableState_t1617 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t392_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct IPAddress_t1634;
// System.Net.IPAddress
#include "System_System_Net_IPAddress.h"
void* RuntimeInvoker_Boolean_t384_Object_t_IPAddressU26_t4110 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t1634 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t1634 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamily.h"
void* RuntimeInvoker_AddressFamily_t1622 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct IPv6Address_t1636;
// System.Net.IPv6Address
#include "System_System_Net_IPv6Address.h"
void* RuntimeInvoker_Boolean_t384_Object_t_IPv6AddressU26_t4111 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t1636 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t1636 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t393_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolType.h"
void* RuntimeInvoker_SecurityProtocolType_t1637 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Security.Cryptography.AsnDecodeStatus
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
struct Object_t;
void* RuntimeInvoker_AsnDecodeStatus_t1677_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1665_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1665_Object_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1665_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1665 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32U26_t4080_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationFlag
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
void* RuntimeInvoker_X509RevocationFlag_t1672 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationMode
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
void* RuntimeInvoker_X509RevocationMode_t1673 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509VerificationFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
void* RuntimeInvoker_X509VerificationFlags_t1676 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
void* RuntimeInvoker_X509KeyUsageFlags_t1670 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509KeyUsageFlags_t1670_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t391_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t391_Int16_t392_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t372_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
// System.Text.RegularExpressions.RegexOptions
#include "System_System_Text_RegularExpressions_RegexOptions.h"
void* RuntimeInvoker_RegexOptions_t1689 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
struct Object_t;
void* RuntimeInvoker_Category_t1696_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_UInt16_t393_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int16_t392_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_UInt16_t393_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int16_t392_Int16_t392_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int16_t392_Object_t_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_UInt16_t393 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt16_t393_UInt16_t393_UInt16_t393 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.OpFlags
#include "System_System_Text_RegularExpressions_OpFlags.h"
void* RuntimeInvoker_OpFlags_t1691_SByte_t390_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_UInt16_t393_UInt16_t393 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Int32U26_t4080_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Int32U26_t4080_Int32U26_t4080_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32U26_t4080_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_UInt16_t393_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_SByte_t390_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32U26_t4080_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_SByte_t390_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
void* RuntimeInvoker_Interval_t1711 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1711  (*Func)(void* obj, const MethodInfo* method);
	Interval_t1711  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Interval_t1711 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t1711  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t1711 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Interval_t1711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t1711  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t1711 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Interval_t1711_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1711  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t1711  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Double_t387_Interval_t1711 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t1711  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t1711 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Interval_t1711_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t1711  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t1711 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t387_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32U26_t4080_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32U26_t4080_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RegexOptionsU26_t4112 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_RegexOptionsU26_t4112_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32U26_t4080_Int32U26_t4080_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Category_t1696 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int16_t392_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t383_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32U26_t4080_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32U26_t4080_Int32U26_t4080_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_UInt16_t393_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int16_t392_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_UInt16_t393 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Text.RegularExpressions.Position
#include "System_System_Text_RegularExpressions_Position.h"
void* RuntimeInvoker_Position_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
struct Object_t;
void* RuntimeInvoker_UriHostNameType_t1743_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct String_t;
void* RuntimeInvoker_Void_t1771_StringU26_t4077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t383_Object_t_Int32U26_t4080_CharU26_t4113 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct UriFormatException_t1742;
// System.UriFormatException
#include "System_System_UriFormatException.h"
void* RuntimeInvoker_Void_t1771_Object_t_UriFormatExceptionU26_t4114 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t1742 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t1742 **)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t356;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_ObjectU5BU5DU26_t4115 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t356** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t356**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t356;
void* RuntimeInvoker_Int32_t372_Object_t_ObjectU5BU5DU26_t4115 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t356** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t356**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t391_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Decimal
#include "mscorlib_System_Decimal.h"
struct Object_t;
void* RuntimeInvoker_Decimal_t395_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t386_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t389_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t394_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t359;
// System.Exception
#include "mscorlib_System_Exception.h"
void* RuntimeInvoker_Boolean_t384_SByte_t390_Object_t_Int32_t372_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t359 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t359 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_SByte_t390_Int32U26_t4080_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t359 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t359 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Int32_t372_SByte_t390_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t359 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t359 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Int32U26_t4080_Object_t_SByte_t390_SByte_t390_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t359 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t359 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32U26_t4080_Object_t_Object_t_BooleanU26_t4079_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32U26_t4080_Object_t_Object_t_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Int32U26_t4080_Object_t_Int32U26_t4080_SByte_t390_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t359 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t359 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32U26_t4080_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int16_t392_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_Int32U26_t4080_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t359 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t359 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
void* RuntimeInvoker_TypeCode_t2375 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_SByte_t390_Int64U26_t4117_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t359 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t359 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t386_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_Int64U26_t4117_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t359 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t359 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t386_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int64U26_t4117 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_Int64U26_t4117 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_SByte_t390_UInt32U26_t4118_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t359 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t359 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_UInt32U26_t4118_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t359 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t359 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t389_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t389_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_UInt32U26_t4118 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_UInt32U26_t4118 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t394_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_UInt64U26_t4119_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t359 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t359 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t394_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_UInt64U26_t4119 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t391_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t391_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_ByteU26_t4107 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_ByteU26_t4107 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_SByte_t390_SByteU26_t4120_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t359 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t359 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t390_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t390_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_SByteU26_t4120 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_SByte_t390_Int16U26_t4121_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t359 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t359 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t392_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int16U26_t4121 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t393_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t393_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_UInt16U26_t4122 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_UInt16U26_t4122 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_ByteU2AU26_t4123_ByteU2AU26_t4123_DoubleU2AU26_t4124_UInt16U2AU26_t4125_UInt16U2AU26_t4125_UInt16U2AU26_t4125_UInt16U2AU26_t4125 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

struct Object_t;
// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategory.h"
void* RuntimeInvoker_UnicodeCategory_t1917_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t383_Int16_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int16_t392_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Char_t383_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Object_t_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Int32_t372_Int32_t372_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int16_t392_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t392_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32U26_t4080_Int32U26_t4080_Int32U26_t4080_BooleanU26_t4079_StringU26_t4077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t392_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t388_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t387_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t387_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_DoubleU26_t4126_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t359 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t359 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_DoubleU26_t4126 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_DoubleU26_t4126 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, double* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (double*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Decimal_t395_Decimal_t395_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, Decimal_t395  p1, Decimal_t395  p2, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), *((Decimal_t395 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t394_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t386_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Decimal_t395_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t395  p1, Decimal_t395  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), *((Decimal_t395 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t395_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Decimal_t395_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t395  p1, Decimal_t395  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), *((Decimal_t395 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t395_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Int32U26_t4080_BooleanU26_t4079_BooleanU26_t4079_Int32U26_t4080_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t395_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_DecimalU26_t4127_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t395 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t395 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_DecimalU26_t4127_UInt64U26_t4119 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t395 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t395 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_DecimalU26_t4127_Int64U26_t4117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t395 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t395 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_DecimalU26_t4127_DecimalU26_t4127 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t395 * p1, Decimal_t395 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t395 *)args[0], (Decimal_t395 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_DecimalU26_t4127_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t395 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t395 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_DecimalU26_t4127_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t395 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t395 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t387_DecimalU26_t4127 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t395 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t395 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_DecimalU26_t4127_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t395 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t395 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_DecimalU26_t4127_DecimalU26_t4127_DecimalU26_t4127 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t395 * p1, Decimal_t395 * p2, Decimal_t395 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t395 *)args[0], (Decimal_t395 *)args[1], (Decimal_t395 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t391_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t390_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t392_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t393_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t389_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t395_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t395_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t395_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t395_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t395_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t395_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t387_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t389 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t394_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t389_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
void* RuntimeInvoker_UIntPtr_t_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct MulticastDelegate_t28;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t4128 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t28 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t28 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t372_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t394_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int64_t386_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t386_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t386_Int64_t386_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int64_t386_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int64_t386_Int64_t386_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int64_t386_Object_t_Int64_t386_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
void* RuntimeInvoker_TypeAttributes_t2031 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"
void* RuntimeInvoker_MemberTypes_t2011 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
void* RuntimeInvoker_RuntimeTypeHandle_t1772 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1772  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t1772  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeCode_t2375_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t1772 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t1772  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t1772 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_RuntimeTypeHandle_t1772_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1772  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t1772  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Object_t_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
void* RuntimeInvoker_Void_t1771_Object_t_RuntimeFieldHandle_t1774 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t1774  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t1774 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ContractionU5BU5D_t1819;
struct Level2MapU5BU5D_t1820;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_ContractionU5BU5DU26_t4129_Level2MapU5BU5DU26_t4130 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t1819** p3, Level2MapU5BU5D_t1820** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t1819**)args[2], (Level2MapU5BU5D_t1820**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct CodePointIndexer_t1803;
// Mono.Globalization.Unicode.CodePointIndexer
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
struct CodePointIndexer_t1803;
void* RuntimeInvoker_Void_t1771_Object_t_CodePointIndexerU26_t4131_ByteU2AU26_t4123_ByteU2AU26_t4123_CodePointIndexerU26_t4131_ByteU2AU26_t4123 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t1803 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t1803 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t1803 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t1803 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t391_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t391_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
void* RuntimeInvoker_ExtenderType_t1816_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372_BooleanU26_t4079_BooleanU26_t4079_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/Context
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372_BooleanU26_t4079_BooleanU26_t4079_SByte_t390_SByte_t390_ContextU26_t4132 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t1813 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t1813 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Int32_t372_Int32_t372_SByte_t390_ContextU26_t4132 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t1813 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t1813 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Object_t_Int32_t372_Int32_t372_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int16_t392_Int32_t372_SByte_t390_ContextU26_t4132 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t1813 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1813 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Object_t_Int32_t372_Int32_t372_Object_t_ContextU26_t4132 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t1813 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t1813 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372_Object_t_Int32_t372_SByte_t390_ContextU26_t4132 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t1813 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1813 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32U26_t4080_Int32_t372_Int32_t372_Object_t_SByte_t390_ContextU26_t4132 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t1813 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t1813 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t1806;
// Mono.Globalization.Unicode.Contraction
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
void* RuntimeInvoker_Boolean_t384_Object_t_Int32U26_t4080_Int32_t372_Int32_t372_Object_t_SByte_t390_Int32_t372_ContractionU26_t4133_ContextU26_t4132 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t1806 ** p8, Context_t1813 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t1806 **)args[7], (Context_t1813 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32U26_t4080_Int32_t372_Int32_t372_Int32_t372_Object_t_SByte_t390_ContextU26_t4132 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t1813 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t1813 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t1806;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32U26_t4080_Int32_t372_Int32_t372_Int32_t372_Object_t_SByte_t390_Int32_t372_ContractionU26_t4133_ContextU26_t4132 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t1806 ** p9, Context_t1813 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t1806 **)args[8], (Context_t1813 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct ByteU5BU5D_t139;
void* RuntimeInvoker_Void_t1771_SByte_t390_ByteU5BU5DU26_t4108_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t139** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t139**)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t1825 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t1827_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t1561_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1561  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t1561  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_DSAParameters_t1561 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t1561  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t1561 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t392_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t387_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t392_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Single_t388_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Single_t388_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Single_t388_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct MethodBase_t1033;
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
struct String_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_SByte_t390_MethodBaseU26_t4134_Int32U26_t4080_Int32U26_t4080_StringU26_t4077_Int32U26_t4080_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t1033 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t1033 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t74  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"
void* RuntimeInvoker_DayOfWeek_t2325_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t74  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32U26_t4080_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t2325_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32U26_t4080_Int32U26_t4080_Int32U26_t4080_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
void* RuntimeInvoker_Void_t1771_DateTime_t74_DateTime_t74_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t74  p1, DateTime_t74  p2, TimeSpan_t427  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t74 *)args[0]), *((DateTime_t74 *)args[1]), *((TimeSpan_t427 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t427  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t427  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t395  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t395  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t393 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_SByte_t390_Int32_t372_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_SByte_t390_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"
void* RuntimeInvoker_Boolean_t384_Object_t_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t372_Int32_t372_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"
struct Object_t;
void* RuntimeInvoker_FileAttributes_t1927_Object_t_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileType.h"
void* RuntimeInvoker_MonoFileType_t1936_IntPtr_t_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOStat
#include "mscorlib_System_IO_MonoIOStat.h"
void* RuntimeInvoker_Boolean_t384_Object_t_MonoIOStatU26_t4136_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t1935 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t1935 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_IntPtr_t_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Int32_t372_Int32_t372_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t386_IntPtr_t_Int64_t386_Int32_t372_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t386_IntPtr_t_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_IntPtr_t_Int64_t386_MonoIOErrorU26_t4135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_SByte_t390_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"
void* RuntimeInvoker_CallingConventions_t2006 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
void* RuntimeInvoker_RuntimeMethodHandle_t2367 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t2367  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t2367  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributes.h"
void* RuntimeInvoker_MethodAttributes_t2012 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.Emit.MethodToken
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
void* RuntimeInvoker_MethodToken_t1973 (const MethodInfo* method, void* obj, void** args)
{
	typedef MethodToken_t1973  (*Func)(void* obj, const MethodInfo* method);
	MethodToken_t1973  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"
void* RuntimeInvoker_FieldAttributes_t2009 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RuntimeFieldHandle_t1774 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t1774  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t1774  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.OpCode
#include "mscorlib_System_Reflection_Emit_OpCode.h"
void* RuntimeInvoker_Void_t1771_OpCode_t1977 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1977  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1977 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_OpCode_t1977_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1977  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1977 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.StackBehaviour
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
void* RuntimeInvoker_StackBehaviour_t1981 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Module_t1970;
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t4080_ModuleU26_t4137 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t1970 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t1970 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
void* RuntimeInvoker_AssemblyNameFlags_t2000 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t356;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Object_t_ObjectU5BU5DU26_t4115_Object_t_Object_t_Object_t_ObjectU26_t4138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t356** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t356**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

struct Object_t;
struct ObjectU5BU5D_t356;
struct Object_t;
void* RuntimeInvoker_Void_t1771_ObjectU5BU5DU26_t4115_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t356** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t356**)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t356;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_ObjectU5BU5DU26_t4115_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t356** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t356**)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Object_t_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributes.h"
void* RuntimeInvoker_EventAttributes_t2007 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t1774 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t1774  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t1774 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t1013 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1013  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1013 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t2367 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t2367  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t2367 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"
void* RuntimeInvoker_Void_t1771_Object_t_MonoEventInfoU26_t4139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t2016 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t2016 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_MonoEventInfo_t2016_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t2016  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t2016  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
void* RuntimeInvoker_Void_t1771_IntPtr_t_MonoMethodInfoU26_t4140 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t2019 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t2019 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_MonoMethodInfo_t2019_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t2019  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t2019  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MethodAttributes_t2012_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_CallingConventions_t2006_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t359 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t359 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
void* RuntimeInvoker_Void_t1771_Object_t_MonoPropertyInfoU26_t4141_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t2020 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t2020 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"
void* RuntimeInvoker_PropertyAttributes_t2027 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributes.h"
void* RuntimeInvoker_ParameterAttributes_t2023 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
struct Object_t;
void* RuntimeInvoker_GCHandle_t1300_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t1300  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t1300  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int32_t372_IntPtr_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, IntPtr_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_IntPtr_t_Object_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_IntPtr_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t4077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t4077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, String_t** p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (String_t**)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_Object_t_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TimeSpan_t427_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t427  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TimeSpan_t427  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t427  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t427 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1013_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1013  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1013 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ISurrogateSelector_t2117;
void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1013_ISurrogateSelectorU26_t4142 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t1013  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1013 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_StringU26_t4077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (String_t**)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t4138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct String_t;
struct String_t;
void* RuntimeInvoker_Boolean_t384_Object_t_StringU26_t4077_StringU26_t4077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
void* RuntimeInvoker_WellKnownObjectMode_t2159 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_StreamingContext_t1013 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t1013  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t1013  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
void* RuntimeInvoker_TypeFilterLevel_t2175 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t391_Object_t_SByte_t390_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t391_Object_t_SByte_t390_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t2383;
void* RuntimeInvoker_Void_t1771_Object_t_SByte_t390_ObjectU26_t4138_HeaderU5BU5DU26_t4143 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t2383** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t2383**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t2383;
void* RuntimeInvoker_Void_t1771_Byte_t391_Object_t_SByte_t390_ObjectU26_t4138_HeaderU5BU5DU26_t4143 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t2383** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t2383**)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Byte_t391_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1012;
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
void* RuntimeInvoker_Void_t1771_Byte_t391_Object_t_Int64U26_t4117_ObjectU26_t4138_SerializationInfoU26_t4144 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t1012 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t1012 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1012;
void* RuntimeInvoker_Void_t1771_Object_t_SByte_t390_SByte_t390_Int64U26_t4117_ObjectU26_t4138_SerializationInfoU26_t4144 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t1012 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t1012 **)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1012;
void* RuntimeInvoker_Void_t1771_Object_t_Int64U26_t4117_ObjectU26_t4138_SerializationInfoU26_t4144 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t1012 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t1012 **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1012;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Int64_t386_ObjectU26_t4138_SerializationInfoU26_t4144 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t1012 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t1012 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int64_t386_Object_t_Object_t_Int64_t386_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int64U26_t4117_ObjectU26_t4138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Int64U26_t4117_ObjectU26_t4138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Int64_t386_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int64_t386_Int64_t386_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t386_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Byte_t391 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int64_t386_Int32_t372_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int64_t386_Object_t_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int64_t386_Object_t_Int64_t386_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_SByte_t390_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_StreamingContext_t1013 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1013  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1013 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_StreamingContext_t1013 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1013  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1013 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_StreamingContext_t1013 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t1013  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t1013 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t1013_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1013  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1013 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t74  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t74 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
void* RuntimeInvoker_SerializationEntry_t2192 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t2192  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t2192  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
void* RuntimeInvoker_StreamingContextStates_t2195 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
void* RuntimeInvoker_CspProviderFlags_t2200 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t389_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int64_t386_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t389_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_UInt32U26_t4118_Int32_t372_UInt32U26_t4118_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int64_t386_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt64_t394_Int64_t386_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t394_Int64_t386_Int64_t386_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t394_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
void* RuntimeInvoker_PaddingMode_t1421 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct StringBuilder_t70;
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
void* RuntimeInvoker_Void_t1771_StringBuilderU26_t4145_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t70 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t70 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct EncoderFallbackBuffer_t2274;
// System.Text.EncoderFallbackBuffer
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
struct CharU5BU5D_t385;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_EncoderFallbackBufferU26_t4146_CharU5BU5DU26_t4147 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t2274 ** p6, CharU5BU5D_t385** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t2274 **)args[5], (CharU5BU5D_t385**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2265;
// System.Text.DecoderFallbackBuffer
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_DecoderFallbackBufferU26_t4148 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t2265 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t2265 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int16_t392_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int16_t392_Int16_t392_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int16_t392_Int16_t392_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_SByte_t390_Int32_t372_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_SByte_t390_Int32U26_t4080_BooleanU26_t4079_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_CharU26_t4113_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_CharU26_t4113_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_CharU26_t4113_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Int32_t372_CharU26_t4113_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2265;
struct ByteU5BU5D_t139;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t2265 ** p7, ByteU5BU5D_t139** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t2265 **)args[6], (ByteU5BU5D_t139**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2265;
struct ByteU5BU5D_t139;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t2265 ** p6, ByteU5BU5D_t139** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t2265 **)args[5], (ByteU5BU5D_t139**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2265;
struct ByteU5BU5D_t139;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_Object_t_Int64_t386_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t2265 ** p2, ByteU5BU5D_t139** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t2265 **)args[1], (ByteU5BU5D_t139**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2265;
struct ByteU5BU5D_t139;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_Object_t_Int64_t386_Int32_t372_Object_t_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t2265 ** p2, ByteU5BU5D_t139** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t2265 **)args[1], (ByteU5BU5D_t139**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2265;
struct ByteU5BU5D_t139;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_UInt32U26_t4118_UInt32U26_t4118_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t2265 ** p9, ByteU5BU5D_t139** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t2265 **)args[8], (ByteU5BU5D_t139**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2265;
struct ByteU5BU5D_t139;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Int32_t372_UInt32U26_t4118_UInt32U26_t4118_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t2265 ** p8, ByteU5BU5D_t139** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t2265 **)args[7], (ByteU5BU5D_t139**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t390_Object_t_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t390_SByte_t390_Object_t_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Threading.ThreadState
#include "mscorlib_System_Threading_ThreadState.h"
void* RuntimeInvoker_ThreadState_t2299 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_TimeSpan_t427_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t427  p1, TimeSpan_t427  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t427 *)args[0]), *((TimeSpan_t427 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int64_t386_Int64_t386_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_IntPtr_t_Int32_t372_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t427  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t427 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_TimeSpan_t427_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t427  p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t427 *)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t386_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t386_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t393_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Byte_t391_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t391_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t391_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t391_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t383_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t383_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t383_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t383_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t74_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t74_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t74_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t74_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t74_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t74_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t387_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t387_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t387_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t387_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t387_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t387_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t392_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t392_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t392_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t392_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t392_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t386_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t386_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t386_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t386_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t390_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t390_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t390_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t390_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t390_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t388_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t393_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t393_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t393_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t393_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t393_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t389_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t389_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t389_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t389_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t389_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t394_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t394_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t394_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t394_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t394_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SByte_t390_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t427  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t427 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int64_t386_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t2325 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
void* RuntimeInvoker_DateTimeKind_t2322 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t74_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, TimeSpan_t427  p1, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, *((TimeSpan_t427 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t74_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_DateTime_t74_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t74  p1, DateTime_t74  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), *((DateTime_t74 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t74_DateTime_t74_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, DateTime_t74  p1, int32_t p2, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t74_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Int32_t372_DateTimeU26_t4149_DateTimeOffsetU26_t4150_SByte_t390_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t74 * p4, DateTimeOffset_t2323 * p5, int8_t p6, Exception_t359 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t74 *)args[3], (DateTimeOffset_t2323 *)args[4], *((int8_t*)args[5]), (Exception_t359 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Object_t_Object_t_SByte_t390_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t359 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t359 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Object_t_SByte_t390_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Int32_t372_Object_t_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Int32_t372_Object_t_SByte_t390_Int32U26_t4080_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_SByte_t390_DateTimeU26_t4149_DateTimeOffsetU26_t4150_Object_t_Int32_t372_SByte_t390_BooleanU26_t4079_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t74 * p5, DateTimeOffset_t2323 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t74 *)args[4], (DateTimeOffset_t2323 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t74_Object_t_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t359;
void* RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_Int32_t372_DateTimeU26_t4149_SByte_t390_BooleanU26_t4079_SByte_t390_ExceptionU26_t4116 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t74 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t359 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t74 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t359 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t74_DateTime_t74_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, DateTime_t74  p1, TimeSpan_t427  p2, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), *((TimeSpan_t427 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_DateTime_t74_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t74  p1, DateTime_t74  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), *((DateTime_t74 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t427_DateTime_t74_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t427  (*Func)(void* obj, DateTime_t74  p1, DateTime_t74  p2, const MethodInfo* method);
	TimeSpan_t427  ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), *((DateTime_t74 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_DateTime_t74_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t74  p1, TimeSpan_t427  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t74 *)args[0]), *((TimeSpan_t427 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int64_t386_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t427  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t427 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_DateTimeOffset_t2323 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t2323  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t2323 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_DateTimeOffset_t2323 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t2323  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t2323 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t392_Object_t_BooleanU26_t4079_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t392_Object_t_BooleanU26_t4079_BooleanU26_t4079_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t74_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t74  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen_0.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t74_Nullable_1_t2426_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t74  p1, Nullable_1_t2426  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), *((Nullable_1_t2426 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.MonoEnumInfo
#include "mscorlib_System_MonoEnumInfo.h"
void* RuntimeInvoker_Void_t1771_MonoEnumInfo_t2336 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t2336  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t2336 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_MonoEnumInfoU26_t4151 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t2336 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t2336 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int16_t392_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int64_t386_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
void* RuntimeInvoker_PlatformID_t2364 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int16_t392_Int16_t392_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

struct Object_t;
// System.Guid
#include "mscorlib_System_Guid.h"
void* RuntimeInvoker_Int32_t372_Guid_t452 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t452  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t452 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Guid_t452 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t452  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t452 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Guid_t452 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t452  (*Func)(void* obj, const MethodInfo* method);
	Guid_t452  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t390_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Double_t387_Double_t387_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeAttributes_t2031_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t390_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_UInt64U2AU26_t4152_Int32U2AU26_t4153_CharU2AU26_t4154_CharU2AU26_t4154_Int64U2AU26_t4155_Int32U2AU26_t4153 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Double_t387_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t395  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t395 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t392_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t386_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Single_t388_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Double_t387_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Decimal_t395_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t395  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t395 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t388_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t387_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_BooleanU26_t4079_SByte_t390_Int32U26_t4080_Int32U26_t4080 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Object_t_SByte_t390_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int64_t386_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t427_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t427  (*Func)(void* obj, TimeSpan_t427  p1, const MethodInfo* method);
	TimeSpan_t427  ret = ((Func)method->method)(obj, *((TimeSpan_t427 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_TimeSpan_t427_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t427  p1, TimeSpan_t427  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t427 *)args[0]), *((TimeSpan_t427 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t427  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t427 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t427_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t427  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t427  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t427_Double_t387_Int64_t386 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t427  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t427  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t427_TimeSpan_t427_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t427  (*Func)(void* obj, TimeSpan_t427  p1, TimeSpan_t427  p2, const MethodInfo* method);
	TimeSpan_t427  ret = ((Func)method->method)(obj, *((TimeSpan_t427 *)args[0]), *((TimeSpan_t427 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t427_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t427  (*Func)(void* obj, DateTime_t74  p1, const MethodInfo* method);
	TimeSpan_t427  ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_DateTime_t74_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t74  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t74_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t74  (*Func)(void* obj, DateTime_t74  p1, const MethodInfo* method);
	DateTime_t74  ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t427_DateTime_t74_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t427  (*Func)(void* obj, DateTime_t74  p1, TimeSpan_t427  p2, const MethodInfo* method);
	TimeSpan_t427  ret = ((Func)method->method)(obj, *((DateTime_t74 *)args[0]), *((TimeSpan_t427 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Int64U5BU5D_t2410;
struct StringU5BU5D_t137;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Int64U5BU5DU26_t4156_StringU5BU5DU26_t4157 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t2410** p2, StringU5BU5D_t137** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t2410**)args[1], (StringU5BU5D_t137**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_ObjectU26_t4138_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_ObjectU26_t4138_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"
void* RuntimeInvoker_Enumerator_t3528 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3528  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3528  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"
void* RuntimeInvoker_Enumerator_t2585 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2585  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2585  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Queue`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"
void* RuntimeInvoker_Enumerator_t2569 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2569  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2569  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
void* RuntimeInvoker_Enumerator_t2581 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2581  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2581  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_ObjectU26_t4138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

struct Object_t;
struct ObjectU5BU5D_t356;
void* RuntimeInvoker_Void_t1771_ObjectU5BU5DU26_t4115_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t356** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t356**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct ObjectU5BU5D_t356;
void* RuntimeInvoker_Void_t1771_ObjectU5BU5DU26_t4115_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t356** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t356**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t2507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2507  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2507 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2507 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2507  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2507 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2507_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2507  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2507  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_ObjectU26_t4138 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6.h"
void* RuntimeInvoker_Enumerator_t2513 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2513  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2513  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t451_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2507 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2507  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2507  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_5.h"
void* RuntimeInvoker_Enumerator_t2512 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2512  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2512  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_29.h"
void* RuntimeInvoker_Enumerator_t2516 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2516  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2516  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30.h"
void* RuntimeInvoker_Enumerator_t2469 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2469  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2469  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_ERaffleResult_t207_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
void* RuntimeInvoker_Enumerator_t436 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t436  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t436  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t352 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t352  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t352  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ERaffleResult_t207 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t352_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t352  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t352  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<AnimatorFrame>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
void* RuntimeInvoker_Enumerator_t234 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t234  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t234  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"
void* RuntimeInvoker_AnimatorFrame_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorFrame_t235  (*Func)(void* obj, const MethodInfo* method);
	AnimatorFrame_t235  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_54.h"
void* RuntimeInvoker_Enumerator_t2878 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2878  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2878  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
void* RuntimeInvoker_Enumerator_t2875 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2875  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2875  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"
void* RuntimeInvoker_KeyValuePair_2_t2871 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2871  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2871  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CoroutineTween.FloatTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatTween.h"
void* RuntimeInvoker_Void_t1771_FloatTween_t539 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FloatTween_t539  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((FloatTween_t539 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
void* RuntimeInvoker_Void_t1771_ColorTween_t536 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorTween_t536  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorTween_t536 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_TypeU26_t4158_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_BooleanU26_t4079_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_FillMethodU26_t4159_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_SingleU26_t4090_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_ContentTypeU26_t4160_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_LineTypeU26_t4161_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_InputTypeU26_t4162_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_TouchScreenKeyboardTypeU26_t4163_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_CharacterValidationU26_t4164_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_CharU26_t4113_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t* p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint16_t*)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_DirectionU26_t4165_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_NavigationU26_t4166_Navigation_t613 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Navigation_t613 * p1, Navigation_t613  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Navigation_t613 *)args[0], *((Navigation_t613 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_TransitionU26_t4167_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_ColorBlockU26_t4168_ColorBlock_t551 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ColorBlock_t551 * p1, ColorBlock_t551  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (ColorBlock_t551 *)args[0], *((ColorBlock_t551 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_SpriteStateU26_t4169_SpriteState_t630 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SpriteState_t630 * p1, SpriteState_t630  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (SpriteState_t630 *)args[0], *((SpriteState_t630 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_DirectionU26_t4170_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_AspectModeU26_t4171_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_FitModeU26_t4172_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_CornerU26_t4173_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_AxisU26_t4174_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector2U26_t4093_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t2 * p1, Vector2_t2  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t2 *)args[0], *((Vector2_t2 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_ConstraintU26_t4175_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32U26_t4080_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SingleU26_t4090_Single_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_BooleanU26_t4079_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_TextAnchorU26_t4176_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"
void* RuntimeInvoker_Enumerator_t1311 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1311  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1311  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_65.h"
void* RuntimeInvoker_Enumerator_t3265 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3265  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3265  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Double_t387 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2507_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2507  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2507  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t2507 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2507  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2507 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2507  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2507 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_Link.h"
void* RuntimeInvoker_Link_t1869_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1869  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t1869  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Link_t1869 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t1869  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t1869 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Link_t1869 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t1869  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t1869 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Link_t1869 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t1869  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t1869 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Link_t1869 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t1869  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t1869 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t451_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_DictionaryEntry_t451 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t451  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t451 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_DictionaryEntry_t451 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t451  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t451 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_DictionaryEntry_t451 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t451  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t451 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_DictionaryEntry_t451 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t451  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t451 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"
void* RuntimeInvoker_KeyValuePair_2_t2546_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2546  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2546  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t2546 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2546  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2546 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2546 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2546  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2546 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t2546 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2546  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2546 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2546 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2546  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2546 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector3_t36_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t36  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector3_t36  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector3_t36 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t404_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t404  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Matrix4x4_t404  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Matrix4x4_t404  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Matrix4x4_t404 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Matrix4x4_t404 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Matrix4x4_t404  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Matrix4x4_t404 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_BoneWeight_t405_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef BoneWeight_t405  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	BoneWeight_t405  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_BoneWeight_t405 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, BoneWeight_t405  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((BoneWeight_t405 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_BoneWeight_t405 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t405  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t405 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_BoneWeight_t405 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, BoneWeight_t405  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((BoneWeight_t405 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_BoneWeight_t405 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, BoneWeight_t405  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((BoneWeight_t405 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector2_t2  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector2_t2 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
void* RuntimeInvoker_KeyValuePair_2_t2667_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2667  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2667  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t2667 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2667  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2667 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2667 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2667  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2667 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t2667 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2667  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2667 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2667 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2667  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2667 *)args[1]), method);
	return NULL;
}

struct Object_t;
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
void* RuntimeInvoker_EScreens_t188_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
void* RuntimeInvoker_KeyValuePair_2_t2690_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2690  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2690  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t2690 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2690  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2690 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2690 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2690  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2690 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t2690 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2690  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2690 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2690 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2690  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2690 *)args[1]), method);
	return NULL;
}

struct Object_t;
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
void* RuntimeInvoker_ESubScreens_t189_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t352_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t352  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t352  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t352 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t352  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t352 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t352 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t352  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t352 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t352 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t352  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t352 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"
void* RuntimeInvoker_FocusMode_t438_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct FocusModeU5BU5D_t2723;
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"
void* RuntimeInvoker_Void_t1771_FocusModeU5BU5DU26_t4177_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FocusModeU5BU5D_t2723** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (FocusModeU5BU5D_t2723**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct FocusModeU5BU5D_t2723;
void* RuntimeInvoker_Void_t1771_FocusModeU5BU5DU26_t4177_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FocusModeU5BU5D_t2723** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (FocusModeU5BU5D_t2723**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t352_Object_t_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t352  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	KeyValuePair_2_t352  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
void* RuntimeInvoker_KeyValuePair_2_t2740_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2740  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2740  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t2740 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2740  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2740 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2740 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2740  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2740 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t2740 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2740  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2740 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2740 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2740  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2740 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_AnimatorFrame_t235_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorFrame_t235  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	AnimatorFrame_t235  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_AnimatorFrame_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, AnimatorFrame_t235  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((AnimatorFrame_t235 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_AnimatorFrame_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, AnimatorFrame_t235  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((AnimatorFrame_t235 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_AnimatorFrame_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, AnimatorFrame_t235  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((AnimatorFrame_t235 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_AnimatorFrame_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, AnimatorFrame_t235  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((AnimatorFrame_t235 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct AnimatorFrameU5BU5D_t2770;
#include "Assembly-CSharp_ArrayTypes.h"
void* RuntimeInvoker_Void_t1771_AnimatorFrameU5BU5DU26_t4178_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, AnimatorFrameU5BU5D_t2770** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (AnimatorFrameU5BU5D_t2770**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct AnimatorFrameU5BU5D_t2770;
void* RuntimeInvoker_Void_t1771_AnimatorFrameU5BU5DU26_t4178_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, AnimatorFrameU5BU5D_t2770** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (AnimatorFrameU5BU5D_t2770**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_AnimatorFrame_t235_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, AnimatorFrame_t235  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((AnimatorFrame_t235 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_AnimatorFrame_t235_AnimatorFrame_t235_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, AnimatorFrame_t235  p1, AnimatorFrame_t235  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((AnimatorFrame_t235 *)args[0]), *((AnimatorFrame_t235 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t9  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t9 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Color_t9 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color_t9  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color_t9 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Rect_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Rect_t267  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Rect_t267 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastResult_t512_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t512  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastResult_t512  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_RaycastResult_t512 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t512  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t512 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_RaycastResult_t512 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t512  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t512 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_RaycastResult_t512 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastResult_t512  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastResult_t512 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct RaycastResultU5BU5D_t2831;
#include "UnityEngine.UI_ArrayTypes.h"
void* RuntimeInvoker_Void_t1771_RaycastResultU5BU5DU26_t4179_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2831** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2831**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct RaycastResultU5BU5D_t2831;
void* RuntimeInvoker_Void_t1771_RaycastResultU5BU5DU26_t4179_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2831** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2831**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_RaycastResult_t512_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, RaycastResult_t512  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RaycastResult_t512 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_RaycastResult_t512_RaycastResult_t512_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t512  p1, RaycastResult_t512  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t512 *)args[0]), *((RaycastResult_t512 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2871_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2871  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2871  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t2871 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2871  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2871 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2871 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2871  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2871 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t2871 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2871  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2871 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2871 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2871  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2871 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t729_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t729  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit2D_t729  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_RaycastHit2D_t729 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit2D_t729  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit2D_t729 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_RaycastHit2D_t729 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit2D_t729  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit2D_t729 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_RaycastHit2D_t729 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit2D_t729  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit2D_t729 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_RaycastHit2D_t729 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit2D_t729  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit2D_t729 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_RaycastHit_t60_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t60  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit_t60  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_RaycastHit_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit_t60  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit_t60 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_RaycastHit_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit_t60  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit_t60 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_RaycastHit_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t60  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t60 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_RaycastHit_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit_t60  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit_t60 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UIVertex_t605_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t605  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIVertex_t605  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_UIVertex_t605 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t605  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t605 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_UIVertex_t605 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t605  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t605 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_UIVertex_t605 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t605  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t605 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t602;
void* RuntimeInvoker_Void_t1771_UIVertexU5BU5DU26_t4180_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t602** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t602**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t602;
void* RuntimeInvoker_Void_t1771_UIVertexU5BU5DU26_t4180_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t602** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t602**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_UIVertex_t605_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t605  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t605 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_UIVertex_t605_UIVertex_t605_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t605  p1, UIVertex_t605  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t605 *)args[0]), *((UIVertex_t605 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ContentType_t591_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
void* RuntimeInvoker_UILineInfo_t752_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t752  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UILineInfo_t752  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_UILineInfo_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t752  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t752 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_UILineInfo_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t752  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t752 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_UILineInfo_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t752  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t752 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_UILineInfo_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t752  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t752 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
void* RuntimeInvoker_UICharInfo_t754_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t754  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UICharInfo_t754  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_UICharInfo_t754 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t754  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t754 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_UICharInfo_t754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t754  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t754 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_UICharInfo_t754 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t754  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t754 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_UICharInfo_t754 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t754  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t754 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Vector3U5BU5D_t254;
void* RuntimeInvoker_Void_t1771_Vector3U5BU5DU26_t4181_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t254** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t254**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Vector3U5BU5D_t254;
void* RuntimeInvoker_Void_t1771_Vector3U5BU5DU26_t4181_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t254** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t254**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Vector3_t36_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector3_t36  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t36 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Vector3_t36_Vector3_t36_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color32_t711_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t711  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Color32_t711  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Color32_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32_t711  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color32_t711 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Color32_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color32_t711  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color32_t711 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Color32_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t711  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t711 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Color32_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color32_t711  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color32_t711 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Color32U5BU5D_t778;
void* RuntimeInvoker_Void_t1771_Color32U5BU5DU26_t4182_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32U5BU5D_t778** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color32U5BU5D_t778**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Color32U5BU5D_t778;
void* RuntimeInvoker_Void_t1771_Color32U5BU5DU26_t4182_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32U5BU5D_t778** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Color32U5BU5D_t778**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Color32_t711_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Color32_t711  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t711 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Color32_t711_Color32_t711_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t711  p1, Color32_t711  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t711 *)args[0]), *((Color32_t711 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Vector2U5BU5D_t262;
void* RuntimeInvoker_Void_t1771_Vector2U5BU5DU26_t4183_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2U5BU5D_t262** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2U5BU5D_t262**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Vector2U5BU5D_t262;
void* RuntimeInvoker_Void_t1771_Vector2U5BU5DU26_t4183_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2U5BU5D_t262** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2U5BU5D_t262**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Vector2_t2_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector2_t2  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t2 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Vector2_t2_Vector2_t2_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t680  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector4_t680 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t680  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t680  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Vector4U5BU5D_t779;
void* RuntimeInvoker_Void_t1771_Vector4U5BU5DU26_t4184_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4U5BU5D_t779** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4U5BU5D_t779**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Vector4U5BU5D_t779;
void* RuntimeInvoker_Void_t1771_Vector4U5BU5DU26_t4184_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4U5BU5D_t779** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4U5BU5D_t779**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Vector4_t680_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector4_t680  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector4_t680 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Vector4_t680_Vector4_t680_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t680  p1, Vector4_t680  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), *((Vector4_t680 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Int32U5BU5D_t401;
void* RuntimeInvoker_Void_t1771_Int32U5BU5DU26_t4185_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t401** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t401**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Int32U5BU5D_t401;
void* RuntimeInvoker_Void_t1771_Int32U5BU5DU26_t4185_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t401** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t401**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
void* RuntimeInvoker_GcAchievementData_t939_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t939  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t939  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_GcAchievementData_t939 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t939  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t939 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_GcAchievementData_t939 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t939  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t939 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_GcAchievementData_t939 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t939  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t939 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_GcAchievementData_t939 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t939  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t939 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_GcScoreData_t940_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t940  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t940  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_GcScoreData_t940 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t940  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t940 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_GcScoreData_t940 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t940  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t940 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_GcScoreData_t940 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t940  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t940 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPoint.h"
void* RuntimeInvoker_ContactPoint_t863_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t863  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint_t863  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_ContactPoint_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint_t863  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint_t863 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_ContactPoint_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint_t863  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint_t863 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_ContactPoint_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint_t863  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint_t863 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_ContactPoint_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint_t863  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint_t863 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.ContactPoint2D
#include "UnityEngine_UnityEngine_ContactPoint2D.h"
void* RuntimeInvoker_ContactPoint2D_t869_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint2D_t869  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint2D_t869  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_ContactPoint2D_t869 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint2D_t869  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint2D_t869 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_ContactPoint2D_t869 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint2D_t869  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint2D_t869 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_ContactPoint2D_t869 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint2D_t869  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint2D_t869 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_ContactPoint2D_t869 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint2D_t869  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint2D_t869 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"
void* RuntimeInvoker_WebCamDevice_t876_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef WebCamDevice_t876  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	WebCamDevice_t876  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_WebCamDevice_t876 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, WebCamDevice_t876  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((WebCamDevice_t876 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_WebCamDevice_t876 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, WebCamDevice_t876  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((WebCamDevice_t876 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_WebCamDevice_t876 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, WebCamDevice_t876  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((WebCamDevice_t876 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_WebCamDevice_t876 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, WebCamDevice_t876  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((WebCamDevice_t876 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
void* RuntimeInvoker_Keyframe_t883_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t883  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t883  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Keyframe_t883 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t883  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t883 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Keyframe_t883 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t883  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t883 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Keyframe_t883 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t883  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t883 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Keyframe_t883 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t883  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t883 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_CharacterInfo_t892_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef CharacterInfo_t892  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CharacterInfo_t892  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_CharacterInfo_t892 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CharacterInfo_t892  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CharacterInfo_t892 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_CharacterInfo_t892 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CharacterInfo_t892  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CharacterInfo_t892 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_CharacterInfo_t892 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CharacterInfo_t892  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CharacterInfo_t892 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_CharacterInfo_t892 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CharacterInfo_t892  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CharacterInfo_t892 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t1009;
void* RuntimeInvoker_Void_t1771_UICharInfoU5BU5DU26_t4186_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t1009** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t1009**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t1009;
void* RuntimeInvoker_Void_t1771_UICharInfoU5BU5DU26_t4186_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t1009** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t1009**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_UICharInfo_t754_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t754  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t754 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_UICharInfo_t754_UICharInfo_t754_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t754  p1, UICharInfo_t754  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t754 *)args[0]), *((UICharInfo_t754 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct UILineInfoU5BU5D_t1010;
void* RuntimeInvoker_Void_t1771_UILineInfoU5BU5DU26_t4187_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t1010** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t1010**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UILineInfoU5BU5D_t1010;
void* RuntimeInvoker_Void_t1771_UILineInfoU5BU5DU26_t4187_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t1010** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t1010**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_UILineInfo_t752_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t752  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t752 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_UILineInfo_t752_UILineInfo_t752_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t752  p1, UILineInfo_t752  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t752 *)args[0]), *((UILineInfo_t752 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
void* RuntimeInvoker_ParameterModifier_t2024_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t2024  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t2024  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_ParameterModifier_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t2024  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t2024 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_ParameterModifier_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t2024  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t2024 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_ParameterModifier_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t2024  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t2024 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_ParameterModifier_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t2024  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t2024 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_HitInfo_t959_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t959  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t959  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_HitInfo_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t959  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t959 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_HitInfo_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t959  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t959 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_30.h"
void* RuntimeInvoker_KeyValuePair_2_t3183_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3183  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3183  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t3183 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3183  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3183 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3183 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3183  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3183 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t3183 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3183  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3183 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3183 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3183  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3183 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
void* RuntimeInvoker_TextEditOp_t976_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Vuforia.Eyewear/EyewearCalibrationReading
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_EyewearCali.h"
void* RuntimeInvoker_EyewearCalibrationReading_t1080_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef EyewearCalibrationReading_t1080  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	EyewearCalibrationReading_t1080  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_EyewearCalibrationReading_t1080 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, EyewearCalibrationReading_t1080  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((EyewearCalibrationReading_t1080 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_EyewearCalibrationReading_t1080 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, EyewearCalibrationReading_t1080  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((EyewearCalibrationReading_t1080 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_EyewearCalibrationReading_t1080 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, EyewearCalibrationReading_t1080  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((EyewearCalibrationReading_t1080 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_EyewearCalibrationReading_t1080 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, EyewearCalibrationReading_t1080  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((EyewearCalibrationReading_t1080 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_33.h"
void* RuntimeInvoker_KeyValuePair_2_t3257_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3257  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3257  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t3257 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3257  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3257 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3257 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3257  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3257 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t3257 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3257  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3257 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3257 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3257  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3257 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_PIXEL_FORMAT_t1108_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct PIXEL_FORMATU5BU5D_t3251;
void* RuntimeInvoker_Void_t1771_PIXEL_FORMATU5BU5DU26_t4188_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PIXEL_FORMATU5BU5D_t3251** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (PIXEL_FORMATU5BU5D_t3251**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct PIXEL_FORMATU5BU5D_t3251;
void* RuntimeInvoker_Void_t1771_PIXEL_FORMATU5BU5DU26_t4188_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PIXEL_FORMATU5BU5D_t3251** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (PIXEL_FORMATU5BU5D_t3251**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_TrackableResultData_t1134_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t1134  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TrackableResultData_t1134  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_TrackableResultData_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TrackableResultData_t1134  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TrackableResultData_t1134 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_TrackableResultData_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TrackableResultData_t1134  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TrackableResultData_t1134 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_TrackableResultData_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TrackableResultData_t1134  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t1134 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Vuforia.VuforiaManagerImpl/WordData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__5.h"
void* RuntimeInvoker_WordData_t1139_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordData_t1139  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	WordData_t1139  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_WordData_t1139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, WordData_t1139  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((WordData_t1139 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_WordData_t1139 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, WordData_t1139  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((WordData_t1139 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_WordData_t1139 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, WordData_t1139  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((WordData_t1139 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_WordData_t1139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, WordData_t1139  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((WordData_t1139 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Vuforia.VuforiaManagerImpl/WordResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__4.h"
void* RuntimeInvoker_WordResultData_t1138_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordResultData_t1138  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	WordResultData_t1138  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_WordResultData_t1138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, WordResultData_t1138  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((WordResultData_t1138 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_WordResultData_t1138 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, WordResultData_t1138  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((WordResultData_t1138 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_WordResultData_t1138 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, WordResultData_t1138  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((WordResultData_t1138 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_WordResultData_t1138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, WordResultData_t1138  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((WordResultData_t1138 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__8.h"
void* RuntimeInvoker_SmartTerrainRevisionData_t1142_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef SmartTerrainRevisionData_t1142  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	SmartTerrainRevisionData_t1142  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SmartTerrainRevisionData_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SmartTerrainRevisionData_t1142  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SmartTerrainRevisionData_t1142 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_SmartTerrainRevisionData_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SmartTerrainRevisionData_t1142  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SmartTerrainRevisionData_t1142 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_SmartTerrainRevisionData_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, SmartTerrainRevisionData_t1142  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((SmartTerrainRevisionData_t1142 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_SmartTerrainRevisionData_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, SmartTerrainRevisionData_t1142  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((SmartTerrainRevisionData_t1142 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Vuforia.VuforiaManagerImpl/SurfaceData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__9.h"
void* RuntimeInvoker_SurfaceData_t1143_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef SurfaceData_t1143  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	SurfaceData_t1143  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_SurfaceData_t1143 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SurfaceData_t1143  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SurfaceData_t1143 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_SurfaceData_t1143 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SurfaceData_t1143  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SurfaceData_t1143 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_SurfaceData_t1143 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, SurfaceData_t1143  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((SurfaceData_t1143 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_SurfaceData_t1143 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, SurfaceData_t1143  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((SurfaceData_t1143 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Vuforia.VuforiaManagerImpl/PropData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__10.h"
void* RuntimeInvoker_PropData_t1144_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef PropData_t1144  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	PropData_t1144  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_PropData_t1144 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PropData_t1144  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((PropData_t1144 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_PropData_t1144 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, PropData_t1144  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((PropData_t1144 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_PropData_t1144 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, PropData_t1144  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((PropData_t1144 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_PropData_t1144 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, PropData_t1144  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((PropData_t1144 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_38.h"
void* RuntimeInvoker_KeyValuePair_2_t3337_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3337  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3337  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t3337 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3337  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3337 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3337 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3337  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3337 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t3337 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3337  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3337 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3337 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3337  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3337 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_RectangleData_t1089_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef RectangleData_t1089  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RectangleData_t1089  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_RectangleData_t1089 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RectangleData_t1089  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RectangleData_t1089 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_RectangleData_t1089 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RectangleData_t1089  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RectangleData_t1089 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_RectangleData_t1089 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RectangleData_t1089  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RectangleData_t1089 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45.h"
void* RuntimeInvoker_KeyValuePair_2_t3425_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3425  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3425  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t3425 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3425  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3425 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3425 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3425  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3425 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t3425 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3425  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3425 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3425 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3425  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3425 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_46.h"
void* RuntimeInvoker_KeyValuePair_2_t3440_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3440  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3440  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t3440 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3440  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3440 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3440 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3440  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3440 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t3440 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3440  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3440 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3440 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3440  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3440 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Vuforia.VuforiaManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"
void* RuntimeInvoker_VirtualButtonData_t1135_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t1135  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	VirtualButtonData_t1135  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_VirtualButtonData_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, VirtualButtonData_t1135  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((VirtualButtonData_t1135 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_VirtualButtonData_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, VirtualButtonData_t1135  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((VirtualButtonData_t1135 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_VirtualButtonData_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, VirtualButtonData_t1135  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((VirtualButtonData_t1135 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_VirtualButtonData_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, VirtualButtonData_t1135  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t1135 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_TargetSearchResult_t1212_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef TargetSearchResult_t1212  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TargetSearchResult_t1212  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_TargetSearchResult_t1212 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TargetSearchResult_t1212  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TargetSearchResult_t1212 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_TargetSearchResult_t1212 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TargetSearchResult_t1212  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TargetSearchResult_t1212 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_TargetSearchResult_t1212 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TargetSearchResult_t1212  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TargetSearchResult_t1212 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct TargetSearchResultU5BU5D_t3458;
void* RuntimeInvoker_Void_t1771_TargetSearchResultU5BU5DU26_t4189_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TargetSearchResultU5BU5D_t3458** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (TargetSearchResultU5BU5D_t3458**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct TargetSearchResultU5BU5D_t3458;
void* RuntimeInvoker_Void_t1771_TargetSearchResultU5BU5DU26_t4189_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TargetSearchResultU5BU5D_t3458** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (TargetSearchResultU5BU5D_t3458**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_TargetSearchResult_t1212_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, TargetSearchResult_t1212  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TargetSearchResult_t1212 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_TargetSearchResult_t1212_TargetSearchResult_t1212_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TargetSearchResult_t1212  p1, TargetSearchResult_t1212  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TargetSearchResult_t1212 *)args[0]), *((TargetSearchResult_t1212 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_48.h"
void* RuntimeInvoker_KeyValuePair_2_t3478_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3478  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t3478  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_KeyValuePair_2_t3478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3478  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3478 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3478  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3478 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_KeyValuePair_2_t3478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3478  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3478 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3478  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3478 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_ProfileData_t1226_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t1226  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ProfileData_t1226  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_ProfileData_t1226 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ProfileData_t1226  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ProfileData_t1226 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_ProfileData_t1226 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ProfileData_t1226  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ProfileData_t1226 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_ProfileData_t1226 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ProfileData_t1226  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ProfileData_t1226 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_ProfileData_t1226 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ProfileData_t1226  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ProfileData_t1226 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.HashSet`1/Link<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"
void* RuntimeInvoker_Link_t3526_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t3526  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t3526  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Link_t3526 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t3526  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t3526 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Link_t3526 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t3526  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t3526 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Link_t3526 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t3526  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t3526 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Link_t3526 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t3526  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t3526 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
void* RuntimeInvoker_ClientCertificateType_t1535_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
void* RuntimeInvoker_X509ChainStatus_t1662_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1662  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t1662  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_X509ChainStatus_t1662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t1662  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t1662 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_X509ChainStatus_t1662 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t1662  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t1662 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_X509ChainStatus_t1662 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t1662  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t1662 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_X509ChainStatus_t1662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t1662  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t1662 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
void* RuntimeInvoker_Mark_t1704_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1704  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t1704  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Mark_t1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t1704  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t1704 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Mark_t1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t1704  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t1704 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Mark_t1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t1704  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t1704 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Mark_t1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t1704  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t1704 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
void* RuntimeInvoker_UriScheme_t1740_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1740  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t1740  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_UriScheme_t1740 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t1740  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t1740 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_UriScheme_t1740 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t1740  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t1740 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_UriScheme_t1740 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t1740  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t1740 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_UriScheme_t1740 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t1740  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t1740 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
void* RuntimeInvoker_TableRange_t1802_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1802  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t1802  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_TableRange_t1802 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t1802  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t1802 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_TableRange_t1802 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t1802  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t1802 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_TableRange_t1802 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t1802  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t1802 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_TableRange_t1802 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t1802  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t1802 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"
void* RuntimeInvoker_Slot_t1879_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1879  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1879  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Slot_t1879 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1879  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1879 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Slot_t1879 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1879  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1879 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Slot_t1879 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1879  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1879 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Slot_t1879 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1879  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1879 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"
void* RuntimeInvoker_Slot_t1888_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1888  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1888  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Slot_t1888 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1888  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1888 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Slot_t1888 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1888  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1888 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Slot_t1888 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1888  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1888 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Slot_t1888 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1888  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1888 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.ILTokenInfo
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
void* RuntimeInvoker_ILTokenInfo_t1964_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1964  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ILTokenInfo_t1964  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_ILTokenInfo_t1964 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t1964  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ILTokenInfo_t1964 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_ILTokenInfo_t1964 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t1964  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ILTokenInfo_t1964 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_ILTokenInfo_t1964 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t1964  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ILTokenInfo_t1964 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_ILTokenInfo_t1964 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t1964  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t1964 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.ILGenerator/LabelData
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
void* RuntimeInvoker_LabelData_t1966_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1966  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelData_t1966  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_LabelData_t1966 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t1966  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelData_t1966 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_LabelData_t1966 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t1966  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelData_t1966 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_LabelData_t1966 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t1966  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelData_t1966 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_LabelData_t1966 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t1966  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelData_t1966 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
void* RuntimeInvoker_LabelFixup_t1965_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1965  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelFixup_t1965  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_LabelFixup_t1965 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t1965  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelFixup_t1965 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_LabelFixup_t1965 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t1965  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelFixup_t1965 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_LabelFixup_t1965 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t1965  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelFixup_t1965 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_LabelFixup_t1965 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t1965  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelFixup_t1965 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_DateTime_t74 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t74  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t74 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t395  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t395 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Decimal_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t395  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t395 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t427_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t427  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t427  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_TimeSpan_t427 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t427  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t427 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
void* RuntimeInvoker_TypeTag_t2163_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Byte_t391 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Byte_t391 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1771_Int32_t372_Byte_t391 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Link_t1869 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1869  (*Func)(void* obj, const MethodInfo* method);
	Link_t1869  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t451_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2507_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2507  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2507  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2546_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2546  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2546  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__8.h"
void* RuntimeInvoker_Enumerator_t2550 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2550  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2550  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t451_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2546 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2546  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2546  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_8.h"
void* RuntimeInvoker_Enumerator_t2549 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2549  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2549  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32.h"
void* RuntimeInvoker_Enumerator_t2553 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2553  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2553  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2546_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2546  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2546  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_BoneWeight_t405 (const MethodInfo* method, void* obj, void** args)
{
	typedef BoneWeight_t405  (*Func)(void* obj, const MethodInfo* method);
	BoneWeight_t405  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2667_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2667  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2667  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_EScreens_t188_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_ObjectU26_t4138 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_EScreens_t188_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
void* RuntimeInvoker_Enumerator_t2672 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2672  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2672  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t451_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2667 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2667  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2667  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_EScreens_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_17.h"
void* RuntimeInvoker_Enumerator_t2671 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2671  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2671  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_41.h"
void* RuntimeInvoker_Enumerator_t2675 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2675  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2675  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2667_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2667  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2667  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2690_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2690  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2690  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_ESubScreens_t189_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_ESubScreens_t189_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
void* RuntimeInvoker_Enumerator_t2695 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2695  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2695  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2690 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2690  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2690  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ESubScreens_t189 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_20.h"
void* RuntimeInvoker_Enumerator_t2694 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2694  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2694  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_44.h"
void* RuntimeInvoker_Enumerator_t2698 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2698  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2698  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2690_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2690  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2690  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t352_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t352  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t352  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ERaffleResult_t207_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_ERaffleResultU26_t4190 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t451_Int32_t372_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_23.h"
void* RuntimeInvoker_Enumerator_t2714 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2714  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2714  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_47.h"
void* RuntimeInvoker_Enumerator_t2717 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2717  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2717  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t352_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t352  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t352  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_FocusMode_t438_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_38.h"
void* RuntimeInvoker_Enumerator_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2725  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2725  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_FocusMode_t438 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_KeyValuePair_2_t352_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, KeyValuePair_2_t352  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((KeyValuePair_2_t352 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2740_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2740  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t2740  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_BooleanU26_t4079 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__18.h"
void* RuntimeInvoker_Enumerator_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2745  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2745  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t451_Object_t_SByte_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2740 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2740  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2740  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_25.h"
void* RuntimeInvoker_Enumerator_t2744 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2744  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2744  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_49.h"
void* RuntimeInvoker_Enumerator_t2748 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2748  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2748  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2740_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2740  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2740  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_AnimatorFrame_t235_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorFrame_t235  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	AnimatorFrame_t235  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_AnimatorFrame_t235_AnimatorFrame_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, AnimatorFrame_t235  p1, AnimatorFrame_t235  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((AnimatorFrame_t235 *)args[0]), *((AnimatorFrame_t235 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_AnimatorFrame_t235_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, AnimatorFrame_t235  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((AnimatorFrame_t235 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_AnimatorFrame_t235_AnimatorFrame_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, AnimatorFrame_t235  p1, AnimatorFrame_t235  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((AnimatorFrame_t235 *)args[0]), *((AnimatorFrame_t235 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_AnimatorFrame_t235_AnimatorFrame_t235_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, AnimatorFrame_t235  p1, AnimatorFrame_t235  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((AnimatorFrame_t235 *)args[0]), *((AnimatorFrame_t235 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Nullable`1<UnityEngine.Vector3>
#include "mscorlib_System_Nullable_1_gen.h"
void* RuntimeInvoker_Boolean_t384_Nullable_1_t445 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t445  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastResult_t512_RaycastResult_t512_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t512  p1, RaycastResult_t512  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t512 *)args[0]), *((RaycastResult_t512 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_42.h"
void* RuntimeInvoker_Enumerator_t2833 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2833  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2833  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_RaycastResult_t512_RaycastResult_t512 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t512  p1, RaycastResult_t512  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t512 *)args[0]), *((RaycastResult_t512 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastResult_t512_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t512  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t512 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2871_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2871  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2871  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_30.h"
void* RuntimeInvoker_Enumerator_t2874 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2874  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2874  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2871_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2871  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2871  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t729 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t729  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit2D_t729  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastHit_t60_RaycastHit_t60_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastHit_t60  p1, RaycastHit_t60  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastHit_t60 *)args[0]), *((RaycastHit_t60 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_RaycastHit_t60 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t60  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit_t60  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Color_t9_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t9  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t9 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t388_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_FloatTween_t539 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, FloatTween_t539  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((FloatTween_t539 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_ColorTween_t536 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ColorTween_t536  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ColorTween_t536 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIVertex_t605_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t605  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIVertex_t605  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_52.h"
void* RuntimeInvoker_Enumerator_t2940 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2940  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2940  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIVertex_t605 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t605  (*Func)(void* obj, const MethodInfo* method);
	UIVertex_t605  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_UIVertex_t605_UIVertex_t605 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t605  p1, UIVertex_t605  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t605 *)args[0]), *((UIVertex_t605 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UIVertex_t605_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t605  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t605 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_UIVertex_t605_UIVertex_t605 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t605  p1, UIVertex_t605  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t605 *)args[0]), *((UIVertex_t605 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UIVertex_t605_UIVertex_t605_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t605  p1, UIVertex_t605  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t605 *)args[0]), *((UIVertex_t605 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_UILineInfo_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t752  (*Func)(void* obj, const MethodInfo* method);
	UILineInfo_t752  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UICharInfo_t754 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t754  (*Func)(void* obj, const MethodInfo* method);
	UICharInfo_t754  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t2_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t2  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t36_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t36  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector3_t36  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_61.h"
void* RuntimeInvoker_Enumerator_t3028 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3028  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3028  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t36  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Vector3_t36_Vector3_t36 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t36_Vector3_t36_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t36  p1, Vector3_t36  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t36 *)args[0]), *((Vector3_t36 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Color32_t711_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t711  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Color32_t711  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_62.h"
void* RuntimeInvoker_Enumerator_t3039 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3039  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3039  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color32_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t711  (*Func)(void* obj, const MethodInfo* method);
	Color32_t711  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Color32_t711_Color32_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color32_t711  p1, Color32_t711  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color32_t711 *)args[0]), *((Color32_t711 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Color32_t711_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color32_t711  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color32_t711 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Color32_t711_Color32_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t711  p1, Color32_t711  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t711 *)args[0]), *((Color32_t711 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Color32_t711_Color32_t711_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color32_t711  p1, Color32_t711  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color32_t711 *)args[0]), *((Color32_t711 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_63.h"
void* RuntimeInvoker_Enumerator_t3049 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3049  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3049  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Vector2_t2_Vector2_t2 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t2_Vector2_t2_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t2  p1, Vector2_t2  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t2 *)args[0]), *((Vector2_t2 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_64.h"
void* RuntimeInvoker_Enumerator_t3060 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3060  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3060  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector4_t680_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector4_t680  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Vector4_t680_Vector4_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t680  p1, Vector4_t680  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), *((Vector4_t680 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector4_t680_Vector4_t680_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector4_t680  p1, Vector4_t680  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector4_t680 *)args[0]), *((Vector4_t680 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_GcAchievementData_t939 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t939  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t939  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_GcScoreData_t940 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t940  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t940  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ContactPoint_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t863  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint_t863  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ContactPoint2D_t869 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint2D_t869  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint2D_t869  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_WebCamDevice_t876 (const MethodInfo* method, void* obj, void** args)
{
	typedef WebCamDevice_t876  (*Func)(void* obj, const MethodInfo* method);
	WebCamDevice_t876  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Keyframe_t883 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t883  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t883  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_CharacterInfo_t892 (const MethodInfo* method, void* obj, void** args)
{
	typedef CharacterInfo_t892  (*Func)(void* obj, const MethodInfo* method);
	CharacterInfo_t892  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UICharInfo_t754_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t754  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UICharInfo_t754  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_67.h"
void* RuntimeInvoker_Enumerator_t3132 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3132  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3132  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_UICharInfo_t754_UICharInfo_t754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t754  p1, UICharInfo_t754  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t754 *)args[0]), *((UICharInfo_t754 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UICharInfo_t754_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t754  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t754 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_UICharInfo_t754_UICharInfo_t754 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t754  p1, UICharInfo_t754  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t754 *)args[0]), *((UICharInfo_t754 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UICharInfo_t754_UICharInfo_t754_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t754  p1, UICharInfo_t754  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t754 *)args[0]), *((UICharInfo_t754 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UILineInfo_t752_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t752  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UILineInfo_t752  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_68.h"
void* RuntimeInvoker_Enumerator_t3141 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3141  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3141  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_UILineInfo_t752_UILineInfo_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t752  p1, UILineInfo_t752  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t752 *)args[0]), *((UILineInfo_t752 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UILineInfo_t752_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t752  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t752 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_UILineInfo_t752_UILineInfo_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t752  p1, UILineInfo_t752  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t752 *)args[0]), *((UILineInfo_t752 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UILineInfo_t752_UILineInfo_t752_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t752  p1, UILineInfo_t752  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t752 *)args[0]), *((UILineInfo_t752 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_ParameterModifier_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t2024  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t2024  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_HitInfo_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t959  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t959  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TextEditOp_t976_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3183_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3183  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t3183  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TextEditOp_t976_Object_t_Int32_t372 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_TextEditOpU26_t4191 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__30.h"
void* RuntimeInvoker_Enumerator_t3188 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3188  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3188  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3183 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3183  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3183  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextEditOp_t976 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_40.h"
void* RuntimeInvoker_Enumerator_t3187 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3187  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3187  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_62.h"
void* RuntimeInvoker_Enumerator_t3191 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3191  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3191  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3183_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3183  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3183  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_EyewearCalibrationReading_t1080 (const MethodInfo* method, void* obj, void** args)
{
	typedef EyewearCalibrationReading_t1080  (*Func)(void* obj, const MethodInfo* method);
	EyewearCalibrationReading_t1080  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3257_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3257  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t3257  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_PIXEL_FORMAT_t1108_Int32_t372_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_PIXEL_FORMAT_t1108_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__33.h"
void* RuntimeInvoker_Enumerator_t3262 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3262  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3262  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3257 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3257  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3257  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_44.h"
void* RuntimeInvoker_Enumerator_t3261 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3261  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3261  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3257_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3257  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3257  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_71.h"
void* RuntimeInvoker_Enumerator_t3274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3274  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3274  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TrackableResultData_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t1134  (*Func)(void* obj, const MethodInfo* method);
	TrackableResultData_t1134  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_WordData_t1139 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordData_t1139  (*Func)(void* obj, const MethodInfo* method);
	WordData_t1139  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_WordResultData_t1138 (const MethodInfo* method, void* obj, void** args)
{
	typedef WordResultData_t1138  (*Func)(void* obj, const MethodInfo* method);
	WordResultData_t1138  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_2.h"
void* RuntimeInvoker_Enumerator_t3318 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3318  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3318  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_TrackableResultData_t1134_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, TrackableResultData_t1134  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((TrackableResultData_t1134 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_SmartTerrainRevisionData_t1142 (const MethodInfo* method, void* obj, void** args)
{
	typedef SmartTerrainRevisionData_t1142  (*Func)(void* obj, const MethodInfo* method);
	SmartTerrainRevisionData_t1142  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SurfaceData_t1143 (const MethodInfo* method, void* obj, void** args)
{
	typedef SurfaceData_t1143  (*Func)(void* obj, const MethodInfo* method);
	SurfaceData_t1143  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_PropData_t1144 (const MethodInfo* method, void* obj, void** args)
{
	typedef PropData_t1144  (*Func)(void* obj, const MethodInfo* method);
	PropData_t1144  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3337_Object_t_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3337  (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	KeyValuePair_2_t3337  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t393_Object_t_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__38.h"
void* RuntimeInvoker_Enumerator_t3342 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3342  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3342  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t451_Object_t_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3337 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3337  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3337  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_50.h"
void* RuntimeInvoker_Enumerator_t3341 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3341  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3341  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t392_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_67.h"
void* RuntimeInvoker_Enumerator_t3345 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3345  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3345  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3337_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3337  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3337  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int16_t392_Int16_t392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SmartTerrainInitializationInfo_t1083_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, SmartTerrainInitializationInfo_t1083  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((SmartTerrainInitializationInfo_t1083 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3425_Int32_t372_TrackableResultData_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3425  (*Func)(void* obj, int32_t p1, TrackableResultData_t1134  p2, const MethodInfo* method);
	KeyValuePair_2_t3425  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t1134 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_TrackableResultData_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, TrackableResultData_t1134  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t1134 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TrackableResultData_t1134_Int32_t372_TrackableResultData_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t1134  (*Func)(void* obj, int32_t p1, TrackableResultData_t1134  p2, const MethodInfo* method);
	TrackableResultData_t1134  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t1134 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_TrackableResultDataU26_t4192 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, TrackableResultData_t1134 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (TrackableResultData_t1134 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TrackableResultData_t1134_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TrackableResultData_t1134  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TrackableResultData_t1134  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__45.h"
void* RuntimeInvoker_Enumerator_t3429 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3429  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3429  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t451_Int32_t372_TrackableResultData_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, int32_t p1, TrackableResultData_t1134  p2, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t1134 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3425 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3425  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3425  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_58.h"
void* RuntimeInvoker_Enumerator_t3428 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3428  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3428  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_TrackableResultData_t1134_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, TrackableResultData_t1134  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((TrackableResultData_t1134 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_70.h"
void* RuntimeInvoker_Enumerator_t3432 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3432  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3432  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3425_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3425  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3425  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_TrackableResultData_t1134_TrackableResultData_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TrackableResultData_t1134  p1, TrackableResultData_t1134  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TrackableResultData_t1134 *)args[0]), *((TrackableResultData_t1134 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3440_Int32_t372_VirtualButtonData_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3440  (*Func)(void* obj, int32_t p1, VirtualButtonData_t1135  p2, const MethodInfo* method);
	KeyValuePair_2_t3440  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t1135 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Int32_t372_VirtualButtonData_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, VirtualButtonData_t1135  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t1135 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_VirtualButtonData_t1135_Int32_t372_VirtualButtonData_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t1135  (*Func)(void* obj, int32_t p1, VirtualButtonData_t1135  p2, const MethodInfo* method);
	VirtualButtonData_t1135  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t1135 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Int32_t372_VirtualButtonDataU26_t4193 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, VirtualButtonData_t1135 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (VirtualButtonData_t1135 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_VirtualButtonData_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t1135  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	VirtualButtonData_t1135  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__46.h"
void* RuntimeInvoker_Enumerator_t3445 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3445  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3445  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t451_Int32_t372_VirtualButtonData_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, int32_t p1, VirtualButtonData_t1135  p2, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t1135 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3440 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3440  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3440  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_VirtualButtonData_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef VirtualButtonData_t1135  (*Func)(void* obj, const MethodInfo* method);
	VirtualButtonData_t1135  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_60.h"
void* RuntimeInvoker_Enumerator_t3444 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3444  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3444  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t372_VirtualButtonData_t1135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, VirtualButtonData_t1135  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((VirtualButtonData_t1135 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_72.h"
void* RuntimeInvoker_Enumerator_t3448 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3448  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3448  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3440_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3440  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3440  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_VirtualButtonData_t1135_VirtualButtonData_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, VirtualButtonData_t1135  p1, VirtualButtonData_t1135  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((VirtualButtonData_t1135 *)args[0]), *((VirtualButtonData_t1135 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TargetSearchResult_t1212_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TargetSearchResult_t1212  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TargetSearchResult_t1212  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_75.h"
void* RuntimeInvoker_Enumerator_t3460 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3460  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3460  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TargetSearchResult_t1212 (const MethodInfo* method, void* obj, void** args)
{
	typedef TargetSearchResult_t1212  (*Func)(void* obj, const MethodInfo* method);
	TargetSearchResult_t1212  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_TargetSearchResult_t1212_TargetSearchResult_t1212 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TargetSearchResult_t1212  p1, TargetSearchResult_t1212  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TargetSearchResult_t1212 *)args[0]), *((TargetSearchResult_t1212 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_TargetSearchResult_t1212_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, TargetSearchResult_t1212  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((TargetSearchResult_t1212 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_TargetSearchResult_t1212_TargetSearchResult_t1212 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TargetSearchResult_t1212  p1, TargetSearchResult_t1212  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TargetSearchResult_t1212 *)args[0]), *((TargetSearchResult_t1212 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_TargetSearchResult_t1212_TargetSearchResult_t1212_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, TargetSearchResult_t1212  p1, TargetSearchResult_t1212  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((TargetSearchResult_t1212 *)args[0]), *((TargetSearchResult_t1212 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1771_Object_t_ProfileData_t1226 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ProfileData_t1226  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1226 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3478_Object_t_ProfileData_t1226 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3478  (*Func)(void* obj, Object_t * p1, ProfileData_t1226  p2, const MethodInfo* method);
	KeyValuePair_2_t3478  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1226 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_ProfileData_t1226 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, ProfileData_t1226  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1226 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_ProfileData_t1226_Object_t_ProfileData_t1226 (const MethodInfo* method, void* obj, void** args)
{
	typedef ProfileData_t1226  (*Func)(void* obj, Object_t * p1, ProfileData_t1226  p2, const MethodInfo* method);
	ProfileData_t1226  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1226 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t384_Object_t_ProfileDataU26_t4194 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, ProfileData_t1226 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (ProfileData_t1226 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__48.h"
void* RuntimeInvoker_Enumerator_t3483 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3483  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3483  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t451_Object_t_ProfileData_t1226 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t451  (*Func)(void* obj, Object_t * p1, ProfileData_t1226  p2, const MethodInfo* method);
	DictionaryEntry_t451  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1226 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3478  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t3478  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_63.h"
void* RuntimeInvoker_Enumerator_t3482 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3482  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3482  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_ProfileData_t1226_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, ProfileData_t1226  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((ProfileData_t1226 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_74.h"
void* RuntimeInvoker_Enumerator_t3486 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3486  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t3486  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3478  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t3478  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_ProfileData_t1226_ProfileData_t1226 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ProfileData_t1226  p1, ProfileData_t1226  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ProfileData_t1226 *)args[0]), *((ProfileData_t1226 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Link_t3526 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t3526  (*Func)(void* obj, const MethodInfo* method);
	Link_t3526  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ClientCertificateType_t1535 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509ChainStatus_t1662 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1662  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t1662  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Mark_t1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1704  (*Func)(void* obj, const MethodInfo* method);
	Mark_t1704  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UriScheme_t1740 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1740  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t1740  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TableRange_t1802 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1802  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t1802  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t1879 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1879  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1879  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t1888 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1888  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1888  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ILTokenInfo_t1964 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1964  (*Func)(void* obj, const MethodInfo* method);
	ILTokenInfo_t1964  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LabelData_t1966 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1966  (*Func)(void* obj, const MethodInfo* method);
	LabelData_t1966  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LabelFixup_t1965 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1965  (*Func)(void* obj, const MethodInfo* method);
	LabelFixup_t1965  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TypeTag_t2163 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_DateTimeOffset_t2323_DateTimeOffset_t2323 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t2323  p1, DateTimeOffset_t2323  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t2323 *)args[0]), *((DateTimeOffset_t2323 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_DateTimeOffset_t2323_DateTimeOffset_t2323 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t2323  p1, DateTimeOffset_t2323  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t2323 *)args[0]), *((DateTimeOffset_t2323 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Nullable_1_t2426 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t2426  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t2426 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t372_Guid_t452_Guid_t452 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t452  p1, Guid_t452  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t452 *)args[0]), *((Guid_t452 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t384_Guid_t452_Guid_t452 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t452  p1, Guid_t452  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t452 *)args[0]), *((Guid_t452 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

extern const InvokerMethod g_Il2CppInvokerPointers[1950] = 
{
	RuntimeInvoker_Void_t1771,
	RuntimeInvoker_Void_t1771_Vector2_t2_Vector2_t2,
	RuntimeInvoker_Void_t1771_Rect_t267,
	RuntimeInvoker_Boolean_t384,
	RuntimeInvoker_Vector2_t2,
	RuntimeInvoker_Single_t388,
	RuntimeInvoker_Void_t1771_Vector2_t2,
	RuntimeInvoker_Boolean_t384_Vector2_t2,
	RuntimeInvoker_Boolean_t384_Object_t,
	RuntimeInvoker_Object_t,
	RuntimeInvoker_Void_t1771_SByte_t390,
	RuntimeInvoker_Void_t1771_SByte_t390_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t,
	RuntimeInvoker_Void_t1771_Single_t388,
	RuntimeInvoker_Color_t9_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_SByte_t390,
	RuntimeInvoker_Single_t388_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t,
	RuntimeInvoker_Single_t388_Vector3_t36_Vector3_t36_Single_t388,
	RuntimeInvoker_Boolean_t384_Vector3_t36_Vector3_t36,
	RuntimeInvoker_Void_t1771_Object_t_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_SByte_t390,
	RuntimeInvoker_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Single_t388_Single_t388,
	RuntimeInvoker_Void_t1771_Single_t388_Object_t,
	RuntimeInvoker_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Vector3_t36_Vector3_t36_Single_t388_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Vector3_t36,
	RuntimeInvoker_Void_t1771_Object_t_Vector3_t36_Single_t388,
	RuntimeInvoker_Vector3_t36,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Quaternion_t41_Quaternion_t41_Single_t388_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Vector3_t36_Vector3_t36_Single_t388_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Single_t388_Single_t388_Single_t388_Object_t,
	RuntimeInvoker_Int32_t372_Int32_t372_Object_t,
	RuntimeInvoker_Int32_t372_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_Int32_t372_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Int32_t372,
	RuntimeInvoker_Object_t_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Vector3_t36_Object_t,
	RuntimeInvoker_Boolean_t384_Vector3_t36,
	RuntimeInvoker_Single_t388_Single_t388,
	RuntimeInvoker_Single_t388_Single_t388_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Single_t388_Single_t388_Single_t388_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Boolean_t384_Int16_t392,
	RuntimeInvoker_Object_t_Int32_t372,
	RuntimeInvoker_Char_t383,
	RuntimeInvoker_TOKEN_t65,
	RuntimeInvoker_DateTime_t74,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_Vector3_t36_Quaternion_t41,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Color_t9,
	RuntimeInvoker_Boolean_t384_Quaternion_t41_Quaternion_t41,
	RuntimeInvoker_Object_t_Single_t388,
	RuntimeInvoker_Boolean_t384_Bounds_t349_Bounds_t349,
	RuntimeInvoker_Void_t1771_Object_t_Single_t388,
	RuntimeInvoker_Boolean_t384_Object_t_StringU26_t4077,
	RuntimeInvoker_Int32_t372_Object_t,
	RuntimeInvoker_Single_t388_Object_t,
	RuntimeInvoker_STATE_t150,
	RuntimeInvoker_Void_t1771_Int32_t372_Single_t388,
	RuntimeInvoker_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Int32_t372_SByte_t390,
	RuntimeInvoker_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_ERaffleResult_t207_Int32_t372,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t352,
	RuntimeInvoker_Void_t1771_Single_t388_Single_t388,
	RuntimeInvoker_Void_t1771_Object_t_Single_t388_Single_t388,
	RuntimeInvoker_Void_t1771_Object_t_Color_t9_Single_t388,
	RuntimeInvoker_Void_t1771_Object_t_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Single_t388,
	RuntimeInvoker_Rect_t267_Rect_t267_Rect_t267_Single_t388,
	RuntimeInvoker_Vector3_t36_Vector3_t36_Vector3_t36_Single_t388,
	RuntimeInvoker_Vector2_t2_Vector2_t2_Vector2_t2_Single_t388,
	RuntimeInvoker_Object_t_Color_t9,
	RuntimeInvoker_Vector3_t36_Object_t_Single_t388,
	RuntimeInvoker_Void_t1771_Object_t_Color_t9,
	RuntimeInvoker_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Color_t9_Object_t,
	RuntimeInvoker_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Vector3_t36_Single_t388,
	RuntimeInvoker_Object_t_Single_t388_Single_t388_Single_t388_Object_t_Object_t,
	RuntimeInvoker_InitError_t1233_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_RaycastResult_t512_RaycastResult_t512,
	RuntimeInvoker_Boolean_t384_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Object_t,
	RuntimeInvoker_MoveDirection_t509,
	RuntimeInvoker_RaycastResult_t512,
	RuntimeInvoker_Void_t1771_RaycastResult_t512,
	RuntimeInvoker_InputButton_t515,
	RuntimeInvoker_RaycastResult_t512_Object_t,
	RuntimeInvoker_MoveDirection_t509_Single_t388_Single_t388,
	RuntimeInvoker_MoveDirection_t509_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Object_t_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Boolean_t384_Int32_t372_PointerEventDataU26_t4078_SByte_t390,
	RuntimeInvoker_Object_t_Touch_t705_BooleanU26_t4079_BooleanU26_t4079,
	RuntimeInvoker_FramePressState_t516_Int32_t372,
	RuntimeInvoker_Boolean_t384_Vector2_t2_Vector2_t2_Single_t388_SByte_t390,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Object_t,
	RuntimeInvoker_InputMode_t526,
	RuntimeInvoker_LayerMask_t531,
	RuntimeInvoker_Void_t1771_LayerMask_t531,
	RuntimeInvoker_Int32_t372_RaycastHit_t60_RaycastHit_t60,
	RuntimeInvoker_Color_t9,
	RuntimeInvoker_ColorTweenMode_t533,
	RuntimeInvoker_ColorBlock_t551,
	RuntimeInvoker_Object_t_Object_t_Vector2_t2,
	RuntimeInvoker_Object_t_Resources_t552,
	RuntimeInvoker_Object_t_Object_t_SByte_t390_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_FontStyle_t918,
	RuntimeInvoker_TextAnchor_t766,
	RuntimeInvoker_HorizontalWrapMode_t890,
	RuntimeInvoker_VerticalWrapMode_t891,
	RuntimeInvoker_Boolean_t384_Vector2_t2_Object_t,
	RuntimeInvoker_Vector2_t2_Vector2_t2,
	RuntimeInvoker_Rect_t267,
	RuntimeInvoker_Void_t1771_Color_t9_Single_t388_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_Color_t9_Single_t388_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Color_t9_Single_t388,
	RuntimeInvoker_Void_t1771_Single_t388_Single_t388_SByte_t390,
	RuntimeInvoker_BlockingObjects_t577,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Vector2_t2_Object_t,
	RuntimeInvoker_Type_t583,
	RuntimeInvoker_FillMethod_t584,
	RuntimeInvoker_Vector4_t680_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Color32_t711_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Vector2_t2_Vector2_t2_Color32_t711_Vector2_t2_Vector2_t2,
	RuntimeInvoker_Vector4_t680_Vector4_t680_Rect_t267,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Single_t388_SByte_t390_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Single_t388_Single_t388_SByte_t390_Int32_t372,
	RuntimeInvoker_Vector2_t2_Vector2_t2_Rect_t267,
	RuntimeInvoker_ContentType_t591,
	RuntimeInvoker_LineType_t594,
	RuntimeInvoker_InputType_t592,
	RuntimeInvoker_TouchScreenKeyboardType_t749,
	RuntimeInvoker_CharacterValidation_t593,
	RuntimeInvoker_Void_t1771_Int16_t392,
	RuntimeInvoker_Void_t1771_Int32U26_t4080,
	RuntimeInvoker_Int32_t372_Vector2_t2_Object_t,
	RuntimeInvoker_Int32_t372_Vector2_t2,
	RuntimeInvoker_EditState_t598_Object_t,
	RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390,
	RuntimeInvoker_Int32_t372_Int32_t372_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Vector2_t2,
	RuntimeInvoker_Single_t388_Int32_t372_Object_t,
	RuntimeInvoker_Char_t383_Object_t_Int32_t372_Int16_t392,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Int16_t392_Object_t_Object_t,
	RuntimeInvoker_Char_t383_Object_t,
	RuntimeInvoker_Void_t1771_Rect_t267_SByte_t390,
	RuntimeInvoker_Mode_t612,
	RuntimeInvoker_Navigation_t613,
	RuntimeInvoker_Direction_t617,
	RuntimeInvoker_Void_t1771_Single_t388_SByte_t390,
	RuntimeInvoker_Axis_t619,
	RuntimeInvoker_MovementType_t623,
	RuntimeInvoker_ScrollbarVisibility_t624,
	RuntimeInvoker_Void_t1771_Single_t388_Int32_t372,
	RuntimeInvoker_Bounds_t349,
	RuntimeInvoker_Void_t1771_Navigation_t613,
	RuntimeInvoker_Transition_t628,
	RuntimeInvoker_Void_t1771_ColorBlock_t551,
	RuntimeInvoker_SpriteState_t630,
	RuntimeInvoker_Void_t1771_SpriteState_t630,
	RuntimeInvoker_SelectionState_t629,
	RuntimeInvoker_Object_t_Vector3_t36,
	RuntimeInvoker_Vector3_t36_Object_t_Vector2_t2,
	RuntimeInvoker_Void_t1771_Color_t9_SByte_t390,
	RuntimeInvoker_Boolean_t384_ColorU26_t4081_Color_t9,
	RuntimeInvoker_Direction_t634,
	RuntimeInvoker_Axis_t636,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_TextGenerationSettings_t715_Vector2_t2,
	RuntimeInvoker_Vector2_t2_Int32_t372,
	RuntimeInvoker_Rect_t267_Object_t_BooleanU26_t4079,
	RuntimeInvoker_Rect_t267_Rect_t267_Rect_t267,
	RuntimeInvoker_Rect_t267_Object_t_Object_t,
	RuntimeInvoker_AspectMode_t650,
	RuntimeInvoker_Single_t388_Single_t388_Int32_t372,
	RuntimeInvoker_ScaleMode_t652,
	RuntimeInvoker_ScreenMatchMode_t653,
	RuntimeInvoker_Unit_t654,
	RuntimeInvoker_FitMode_t656,
	RuntimeInvoker_Corner_t658,
	RuntimeInvoker_Axis_t659,
	RuntimeInvoker_Constraint_t660,
	RuntimeInvoker_Single_t388_Int32_t372,
	RuntimeInvoker_Single_t388_Int32_t372_Single_t388,
	RuntimeInvoker_Void_t1771_Single_t388_Single_t388_Single_t388_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Single_t388_Single_t388,
	RuntimeInvoker_Boolean_t384_LayoutRebuilder_t668,
	RuntimeInvoker_Single_t388_Object_t_Int32_t372,
	RuntimeInvoker_Single_t388_Object_t_Object_t_Single_t388,
	RuntimeInvoker_Single_t388_Object_t_Object_t_Single_t388_ILayoutElementU26_t4082,
	RuntimeInvoker_Void_t1771_UIVertexU26_t4083_Int32_t372,
	RuntimeInvoker_Void_t1771_Vector3_t36_Color32_t711_Vector2_t2_Vector2_t2_Vector3_t36_Vector4_t680,
	RuntimeInvoker_Void_t1771_Vector3_t36_Color32_t711_Vector2_t2,
	RuntimeInvoker_Void_t1771_UIVertex_t605,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Color32_t711_Int32_t372_Int32_t372_Single_t388_Single_t388,
	RuntimeInvoker_Void_t1771_Object_t_Double_t387,
	RuntimeInvoker_Void_t1771_Int64_t386_Object_t,
	RuntimeInvoker_Void_t1771_GcAchievementDescriptionData_t938_Int32_t372,
	RuntimeInvoker_Void_t1771_GcUserProfileData_t937_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Double_t387_Object_t,
	RuntimeInvoker_Void_t1771_Int64_t386_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_UserProfileU5BU5DU26_t4084_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_UserProfileU5BU5DU26_t4084_Int32_t372,
	RuntimeInvoker_Void_t1771_GcScoreData_t940,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_BoundsU26_t4085,
	RuntimeInvoker_Boolean_t384_BoneWeight_t405_BoneWeight_t405,
	RuntimeInvoker_ScreenOrientation_t944,
	RuntimeInvoker_Void_t1771_Vector3U26_t4086,
	RuntimeInvoker_Void_t1771_Matrix4x4_t404,
	RuntimeInvoker_Void_t1771_Matrix4x4U26_t4087,
	RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_Color_t9,
	RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_Color_t9_Single_t388,
	RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_ColorU26_t4081_Single_t388,
	RuntimeInvoker_Void_t1771_ColorU26_t4081,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t4086,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390_IntPtr_t,
	RuntimeInvoker_TextureFormat_t947,
	RuntimeInvoker_Color_t9_Single_t388_Single_t388,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_Int32_t372_SByte_t390,
	RuntimeInvoker_Void_t1771_Rect_t267_Int32_t372_Int32_t372_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_RectU26_t4088_Int32_t372_Int32_t372_SByte_t390,
	RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_IntPtr_t_Int32_t372,
	RuntimeInvoker_Void_t1771_CullingGroupEvent_t813,
	RuntimeInvoker_Object_t_CullingGroupEvent_t813_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Color_t9_Single_t388,
	RuntimeInvoker_Boolean_t384_Object_t_Color_t9_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_ColorU26_t4081_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_SByte_t390_SByte_t390_SByte_t390_SByte_t390_Object_t,
	RuntimeInvoker_Void_t1771_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t4089_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_SByte_t390_SByte_t390,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_SByte_t390_SByte_t390_SByte_t390_SByte_t390_Object_t,
	RuntimeInvoker_Void_t1771_Vector3_t36_Vector3_t36,
	RuntimeInvoker_Void_t1771_Vector3U26_t4086_Vector3U26_t4086,
	RuntimeInvoker_Int32_t372_LayerMask_t531,
	RuntimeInvoker_LayerMask_t531_Int32_t372,
	RuntimeInvoker_Vector2_t2_Vector2_t2_Vector2_t2,
	RuntimeInvoker_Single_t388_Vector2_t2_Vector2_t2,
	RuntimeInvoker_Single_t388_Vector2_t2,
	RuntimeInvoker_Vector2_t2_Vector2_t2_Single_t388,
	RuntimeInvoker_Boolean_t384_Vector2_t2_Vector2_t2,
	RuntimeInvoker_Vector2_t2_Vector3_t36,
	RuntimeInvoker_Vector3_t36_Vector2_t2,
	RuntimeInvoker_Vector3_t36_Vector3_t36_Vector3_t36,
	RuntimeInvoker_Vector3_t36_Vector3_t36,
	RuntimeInvoker_Single_t388_Vector3_t36_Vector3_t36,
	RuntimeInvoker_Single_t388_Vector3_t36,
	RuntimeInvoker_Vector3_t36_Vector3_t36_Single_t388,
	RuntimeInvoker_Vector3_t36_Single_t388_Vector3_t36,
	RuntimeInvoker_Void_t1771_Single_t388_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Color_t9_Color_t9_Color_t9_Single_t388,
	RuntimeInvoker_Color_t9_Color_t9_Single_t388,
	RuntimeInvoker_Vector4_t680_Color_t9,
	RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Color32_t711_Color_t9,
	RuntimeInvoker_Color_t9_Color32_t711,
	RuntimeInvoker_Quaternion_t41,
	RuntimeInvoker_Single_t388_Quaternion_t41_Quaternion_t41,
	RuntimeInvoker_Quaternion_t41_Single_t388_Vector3_t36,
	RuntimeInvoker_Quaternion_t41_Single_t388_Vector3U26_t4086,
	RuntimeInvoker_Void_t1771_SingleU26_t4090_Vector3U26_t4086,
	RuntimeInvoker_Quaternion_t41_Vector3_t36_Vector3_t36,
	RuntimeInvoker_Quaternion_t41_Vector3U26_t4086_Vector3U26_t4086,
	RuntimeInvoker_Quaternion_t41_Quaternion_t41_Quaternion_t41_Single_t388,
	RuntimeInvoker_Quaternion_t41_QuaternionU26_t4091_QuaternionU26_t4091_Single_t388,
	RuntimeInvoker_Quaternion_t41_Quaternion_t41,
	RuntimeInvoker_Quaternion_t41_QuaternionU26_t4091,
	RuntimeInvoker_Quaternion_t41_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Quaternion_t41_Vector3_t36,
	RuntimeInvoker_Vector3_t36_Quaternion_t41,
	RuntimeInvoker_Vector3_t36_QuaternionU26_t4091,
	RuntimeInvoker_Quaternion_t41_Vector3U26_t4086,
	RuntimeInvoker_Void_t1771_Quaternion_t41_Vector3U26_t4086_SingleU26_t4090,
	RuntimeInvoker_Void_t1771_QuaternionU26_t4091_Vector3U26_t4086_SingleU26_t4090,
	RuntimeInvoker_Quaternion_t41_Quaternion_t41_Quaternion_t41,
	RuntimeInvoker_Vector3_t36_Quaternion_t41_Vector3_t36,
	RuntimeInvoker_Boolean_t384_Rect_t267,
	RuntimeInvoker_Boolean_t384_Rect_t267_Rect_t267,
	RuntimeInvoker_Single_t388_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Single_t388,
	RuntimeInvoker_Matrix4x4_t404_Matrix4x4_t404,
	RuntimeInvoker_Matrix4x4_t404_Matrix4x4U26_t4087,
	RuntimeInvoker_Boolean_t384_Matrix4x4_t404_Matrix4x4U26_t4087,
	RuntimeInvoker_Boolean_t384_Matrix4x4U26_t4087_Matrix4x4U26_t4087,
	RuntimeInvoker_Matrix4x4_t404,
	RuntimeInvoker_Vector4_t680_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Vector4_t680,
	RuntimeInvoker_Matrix4x4_t404_Vector3_t36,
	RuntimeInvoker_Void_t1771_Vector3_t36_Quaternion_t41_Vector3_t36,
	RuntimeInvoker_Matrix4x4_t404_Vector3_t36_Quaternion_t41_Vector3_t36,
	RuntimeInvoker_Matrix4x4_t404_Vector3U26_t4086_QuaternionU26_t4091_Vector3U26_t4086,
	RuntimeInvoker_Matrix4x4_t404_Single_t388_Single_t388_Single_t388_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Matrix4x4_t404_Single_t388_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Matrix4x4_t404_Matrix4x4_t404_Matrix4x4_t404,
	RuntimeInvoker_Vector4_t680_Matrix4x4_t404_Vector4_t680,
	RuntimeInvoker_Boolean_t384_Matrix4x4_t404_Matrix4x4_t404,
	RuntimeInvoker_Void_t1771_Bounds_t349,
	RuntimeInvoker_Boolean_t384_Bounds_t349,
	RuntimeInvoker_Boolean_t384_Bounds_t349_Vector3_t36,
	RuntimeInvoker_Boolean_t384_BoundsU26_t4085_Vector3U26_t4086,
	RuntimeInvoker_Single_t388_Bounds_t349_Vector3_t36,
	RuntimeInvoker_Single_t388_BoundsU26_t4085_Vector3U26_t4086,
	RuntimeInvoker_Boolean_t384_RayU26_t4092_BoundsU26_t4085_SingleU26_t4090,
	RuntimeInvoker_Boolean_t384_Ray_t382,
	RuntimeInvoker_Boolean_t384_Ray_t382_SingleU26_t4090,
	RuntimeInvoker_Vector3_t36_BoundsU26_t4085_Vector3U26_t4086,
	RuntimeInvoker_Single_t388_Vector4_t680_Vector4_t680,
	RuntimeInvoker_Single_t388_Vector4_t680,
	RuntimeInvoker_Vector4_t680,
	RuntimeInvoker_Vector4_t680_Vector4_t680_Vector4_t680,
	RuntimeInvoker_Vector4_t680_Vector4_t680_Single_t388,
	RuntimeInvoker_Boolean_t384_Vector4_t680_Vector4_t680,
	RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Single_t388,
	RuntimeInvoker_Single_t388_Single_t388_Single_t388_SingleU26_t4090_Single_t388,
	RuntimeInvoker_Single_t388_Single_t388_Single_t388_SingleU26_t4090_Single_t388_Single_t388_Single_t388,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_RectU26_t4088,
	RuntimeInvoker_Void_t1771_Vector2U26_t4093,
	RuntimeInvoker_Void_t1771_Int32_t372_Single_t388_Single_t388,
	RuntimeInvoker_Void_t1771_Int32_t372_Color_t9,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_ColorU26_t4081,
	RuntimeInvoker_Color_t9_Object_t,
	RuntimeInvoker_Color_t9_Int32_t372,
	RuntimeInvoker_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_SphericalHarmonicsL2U26_t4094,
	RuntimeInvoker_Void_t1771_Color_t9_SphericalHarmonicsL2U26_t4094,
	RuntimeInvoker_Void_t1771_ColorU26_t4081_SphericalHarmonicsL2U26_t4094,
	RuntimeInvoker_Void_t1771_Vector3_t36_Color_t9_Single_t388,
	RuntimeInvoker_Void_t1771_Vector3_t36_Color_t9_SphericalHarmonicsL2U26_t4094,
	RuntimeInvoker_Void_t1771_Vector3U26_t4086_ColorU26_t4081_SphericalHarmonicsL2U26_t4094,
	RuntimeInvoker_SphericalHarmonicsL2_t833_SphericalHarmonicsL2_t833_Single_t388,
	RuntimeInvoker_SphericalHarmonicsL2_t833_Single_t388_SphericalHarmonicsL2_t833,
	RuntimeInvoker_SphericalHarmonicsL2_t833_SphericalHarmonicsL2_t833_SphericalHarmonicsL2_t833,
	RuntimeInvoker_Boolean_t384_SphericalHarmonicsL2_t833_SphericalHarmonicsL2_t833,
	RuntimeInvoker_Void_t1771_Vector4U26_t4095,
	RuntimeInvoker_Vector4_t680_Object_t,
	RuntimeInvoker_Vector2_t2_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Vector2U26_t4093,
	RuntimeInvoker_Object_t_SByte_t390_Object_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t390_SByte_t390_Object_t_SByte_t390,
	RuntimeInvoker_Boolean_t384_Object_t_SByte_t390,
	RuntimeInvoker_RuntimePlatform_t788,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_SByte_t390,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_CameraClearFlags_t943,
	RuntimeInvoker_Vector3_t36_Object_t_Vector3U26_t4086,
	RuntimeInvoker_Ray_t382_Vector3_t36,
	RuntimeInvoker_Ray_t382_Object_t_Vector3U26_t4086,
	RuntimeInvoker_Object_t_Ray_t382_Single_t388_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_RayU26_t4092_Single_t388_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_RayU26_t4092_Single_t388_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_IntPtr_t,
	RuntimeInvoker_RenderBuffer_t942,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_IntPtr_t_Int32U26_t4080_Int32U26_t4080,
	RuntimeInvoker_Void_t1771_IntPtr_t_RenderBufferU26_t4096_RenderBufferU26_t4096,
	RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Int32U26_t4080_Int32U26_t4080,
	RuntimeInvoker_TouchPhase_t847,
	RuntimeInvoker_Touch_t705_Int32_t372,
	RuntimeInvoker_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t390_SByte_t390_SByte_t390_SByte_t390_Object_t,
	RuntimeInvoker_Void_t1771_Quaternion_t41,
	RuntimeInvoker_Void_t1771_QuaternionU26_t4091,
	RuntimeInvoker_Void_t1771_Vector3_t36_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Vector3_t36,
	RuntimeInvoker_Void_t1771_Object_t_Vector3U26_t4086_Vector3U26_t4086,
	RuntimeInvoker_Void_t1771_Object_t_SByte_t390_SByte_t390_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Vector3_t36_Vector3_t36_RaycastHitU26_t4097_Single_t388_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Ray_t382_RaycastHitU26_t4097_Single_t388_Int32_t372,
	RuntimeInvoker_Boolean_t384_Ray_t382_RaycastHitU26_t4097_Single_t388_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Ray_t382_Single_t388_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Vector3_t36_Vector3_t36_Single_t388_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Vector3U26_t4086_Vector3U26_t4086_Single_t388_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Vector3U26_t4086_Vector3U26_t4086_RaycastHitU26_t4097_Single_t388_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Vector3U26_t4086,
	RuntimeInvoker_Void_t1771_Object_t_QuaternionU26_t4091,
	RuntimeInvoker_Boolean_t384_Object_t_Ray_t382_RaycastHitU26_t4097_Single_t388,
	RuntimeInvoker_Boolean_t384_Object_t_RayU26_t4092_RaycastHitU26_t4097_Single_t388,
	RuntimeInvoker_Boolean_t384_Ray_t382_RaycastHitU26_t4097_Single_t388,
	RuntimeInvoker_Void_t1771_Vector2_t2_Vector2_t2_Single_t388_Int32_t372_Single_t388_Single_t388_RaycastHit2DU26_t4098,
	RuntimeInvoker_Void_t1771_Vector2U26_t4093_Vector2U26_t4093_Single_t388_Int32_t372_Single_t388_Single_t388_RaycastHit2DU26_t4098,
	RuntimeInvoker_RaycastHit2D_t729_Vector2_t2_Vector2_t2_Single_t388_Int32_t372,
	RuntimeInvoker_RaycastHit2D_t729_Vector2_t2_Vector2_t2_Single_t388_Int32_t372_Single_t388_Single_t388,
	RuntimeInvoker_Object_t_Vector2_t2_Vector2_t2_Single_t388_Int32_t372,
	RuntimeInvoker_Object_t_Vector2U26_t4093_Vector2U26_t4093_Single_t388_Int32_t372_Single_t388_Single_t388,
	RuntimeInvoker_Object_t_SByte_t390_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_SendMessageOptions_t786,
	RuntimeInvoker_AnimatorStateInfo_t881,
	RuntimeInvoker_AnimatorClipInfo_t882,
	RuntimeInvoker_Boolean_t384_Int16_t392_CharacterInfoU26_t4099_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Int16_t392_CharacterInfoU26_t4099_Int32_t372,
	RuntimeInvoker_Boolean_t384_Int16_t392_CharacterInfoU26_t4099,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Color_t9_Int32_t372_Single_t388_Single_t388_Int32_t372_SByte_t390_SByte_t390_Int32_t372_Int32_t372_Int32_t372_Int32_t372_SByte_t390_Int32_t372_Vector2_t2_Vector2_t2_SByte_t390,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Color_t9_Int32_t372_Single_t388_Single_t388_Int32_t372_SByte_t390_SByte_t390_Int32_t372_Int32_t372_Int32_t372_Int32_t372_SByte_t390_Int32_t372_Single_t388_Single_t388_Single_t388_Single_t388_SByte_t390,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_ColorU26_t4081_Int32_t372_Single_t388_Single_t388_Int32_t372_SByte_t390_SByte_t390_Int32_t372_Int32_t372_Int32_t372_Int32_t372_SByte_t390_Int32_t372_Single_t388_Single_t388_Single_t388_Single_t388_SByte_t390,
	RuntimeInvoker_TextGenerationSettings_t715_TextGenerationSettings_t715,
	RuntimeInvoker_Single_t388_Object_t_TextGenerationSettings_t715,
	RuntimeInvoker_Boolean_t384_Object_t_TextGenerationSettings_t715,
	RuntimeInvoker_RenderMode_t896,
	RuntimeInvoker_Void_t1771_Object_t_ColorU26_t4081,
	RuntimeInvoker_Void_t1771_Object_t_RectU26_t4088,
	RuntimeInvoker_Boolean_t384_Object_t_Vector2_t2_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Vector2U26_t4093_Object_t,
	RuntimeInvoker_Vector2_t2_Vector2_t2_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Vector2_t2_Object_t_Object_t_Vector2U26_t4093,
	RuntimeInvoker_Void_t1771_Vector2U26_t4093_Object_t_Object_t_Vector2U26_t4093,
	RuntimeInvoker_Boolean_t384_Object_t_Vector2_t2_Object_t_Vector3U26_t4086,
	RuntimeInvoker_Boolean_t384_Object_t_Vector2_t2_Object_t_Vector2U26_t4093,
	RuntimeInvoker_Ray_t382_Object_t_Vector2_t2,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_SByte_t390_SByte_t390,
	RuntimeInvoker_Ray_t382,
	RuntimeInvoker_Void_t1771_Ray_t382,
	RuntimeInvoker_EventType_t898,
	RuntimeInvoker_EventType_t898_Int32_t372,
	RuntimeInvoker_EventModifiers_t899,
	RuntimeInvoker_KeyCode_t897,
	RuntimeInvoker_Void_t1771_DateTime_t74,
	RuntimeInvoker_Void_t1771_Rect_t267_Object_t,
	RuntimeInvoker_Void_t1771_Rect_t267_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Rect_t267_Object_t,
	RuntimeInvoker_Boolean_t384_Rect_t267_Object_t_Object_t,
	RuntimeInvoker_Rect_t267_Int32_t372_Rect_t267_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Object_t_Int32_t372_Single_t388_Single_t388_Object_t,
	RuntimeInvoker_Void_t1771_Rect_t267_Object_t_IntPtr_t,
	RuntimeInvoker_Void_t1771_RectU26_t4088_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t384_Rect_t267_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t384_RectU26_t4088_Object_t_IntPtr_t,
	RuntimeInvoker_Rect_t267_Int32_t372_Rect_t267_Object_t_Object_t_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_Rect_t267_Int32_t372_RectU26_t4088_Object_t_Object_t_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_Object_t_Int32_t372_SByte_t390,
	RuntimeInvoker_Rect_t267_Object_t_Object_t_Object_t,
	RuntimeInvoker_Rect_t267_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Rect_t267,
	RuntimeInvoker_Void_t1771_Int32_t372_RectU26_t4088,
	RuntimeInvoker_Void_t1771_Single_t388_Single_t388_Single_t388_Single_t388_Object_t,
	RuntimeInvoker_Void_t1771_Single_t388_Single_t388_Single_t388_Single_t388_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_IntPtr_t_Rect_t267_Object_t_SByte_t390_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_Rect_t267_Object_t_SByte_t390_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_Rect_t267_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Rect_t267_Object_t_Int32_t372_SByte_t390,
	RuntimeInvoker_Single_t388_Object_t_Single_t388,
	RuntimeInvoker_Void_t1771_Object_t_SingleU26_t4090_SingleU26_t4090,
	RuntimeInvoker_IntPtr_t_Int32_t372,
	RuntimeInvoker_ImagePosition_t919,
	RuntimeInvoker_Void_t1771_Object_t_Internal_DrawArgumentsU26_t4100,
	RuntimeInvoker_Void_t1771_IntPtr_t_Rect_t267_Object_t_Int32_t372_SByte_t390,
	RuntimeInvoker_Void_t1771_IntPtr_t_RectU26_t4088_Object_t_Int32_t372_SByte_t390,
	RuntimeInvoker_Void_t1771_IntPtr_t_Object_t_Vector2U26_t4093,
	RuntimeInvoker_Single_t388_IntPtr_t_Object_t_Single_t388,
	RuntimeInvoker_Void_t1771_IntPtr_t_Object_t_SingleU26_t4090_SingleU26_t4090,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390_Int32_t372_Object_t,
	RuntimeInvoker_UserState_t962,
	RuntimeInvoker_Void_t1771_Object_t_Double_t387_SByte_t390_SByte_t390_DateTime_t74,
	RuntimeInvoker_Double_t387,
	RuntimeInvoker_Void_t1771_Double_t387,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t390_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Int64_t386,
	RuntimeInvoker_Void_t1771_Object_t_Int64_t386_Object_t_DateTime_t74_Object_t_Int32_t372,
	RuntimeInvoker_Int64_t386,
	RuntimeInvoker_Void_t1771_Int64_t386,
	RuntimeInvoker_UserScope_t963,
	RuntimeInvoker_Range_t957,
	RuntimeInvoker_Void_t1771_Range_t957,
	RuntimeInvoker_TimeScope_t964,
	RuntimeInvoker_Void_t1771_Int32_t372_HitInfo_t959,
	RuntimeInvoker_Boolean_t384_HitInfo_t959_HitInfo_t959,
	RuntimeInvoker_Boolean_t384_HitInfo_t959,
	RuntimeInvoker_Void_t1771_Object_t_StringU26_t4077_StringU26_t4077,
	RuntimeInvoker_Void_t1771_Object_t_StreamingContext_t1013,
	RuntimeInvoker_Boolean_t384_Color_t9_Color_t9,
	RuntimeInvoker_Boolean_t384_TextGenerationSettings_t715,
	RuntimeInvoker_PersistentListenerMode_t979,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_CameraDirection_t1064,
	RuntimeInvoker_VideoBackgroundReflection_t1155,
	RuntimeInvoker_Matrix4x4_t404_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_Matrix4x4_t404,
	RuntimeInvoker_Boolean_t384_Int32_t372_Object_t,
	RuntimeInvoker_Boolean_t384_SByte_t390,
	RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_Matrix4x4_t404,
	RuntimeInvoker_Void_t1771_Matrix4x4_t404_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Matrix4x4_t404,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390_Single_t388_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Matrix4x4_t404_SingleU26_t4090_SingleU26_t4090,
	RuntimeInvoker_Single_t388_Matrix4x4_t404,
	RuntimeInvoker_Vector3_t36_Vector4_t680,
	RuntimeInvoker_Status_t1058,
	RuntimeInvoker_VideoModeData_t1065,
	RuntimeInvoker_VideoModeData_t1065_Int32_t372,
	RuntimeInvoker_Boolean_t384_CameraDeviceModeU26_t4101,
	RuntimeInvoker_Boolean_t384_Int32_t372_SByte_t390,
	RuntimeInvoker_Boolean_t384_RectU26_t4088,
	RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t36_Quaternion_t41_Vector3_t36_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_Vector3_t36_Vector3_t36,
	RuntimeInvoker_Boolean_t384_Object_t_Vector3_t36_Vector3_t36_Vector3_t36_Quaternion_t41,
	RuntimeInvoker_Object_t_Vector3U26_t4086_Vector3U26_t4086,
	RuntimeInvoker_Object_t_Vector3U26_t4086_Vector3U26_t4086_Vector3U26_t4086_QuaternionU26_t4091,
	RuntimeInvoker_Boolean_t384_IntPtr_t_Object_t_Vector3_t36_Vector3_t36_Vector3_t36_Quaternion_t41,
	RuntimeInvoker_Matrix4x4_t404_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Single_t388,
	RuntimeInvoker_StorageType_t1235,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Vector2_t2_Vector2_t2_Single_t388,
	RuntimeInvoker_Void_t1771_Vector3_t36_Vector3_t36_Single_t388,
	RuntimeInvoker_Void_t1771_TargetSearchResult_t1212,
	RuntimeInvoker_ImageTargetType_t1098,
	RuntimeInvoker_Object_t_Object_t_RectangleData_t1089,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Vector3_t36,
	RuntimeInvoker_Boolean_t384_Object_t_Single_t388,
	RuntimeInvoker_FrameQuality_t1100,
	RuntimeInvoker_Boolean_t384_Int32_t372_VirtualButtonAbstractBehaviourU26_t4102,
	RuntimeInvoker_WordFilterMode_t1248,
	RuntimeInvoker_WordPrefabCreationMode_t1179,
	RuntimeInvoker_Sensitivity_t1222,
	RuntimeInvoker_Boolean_t384_Matrix4x4_t404,
	RuntimeInvoker_WordTemplateMode_t1107,
	RuntimeInvoker_PIXEL_FORMAT_t1108,
	RuntimeInvoker_TextureFormat_t947_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Single_t388_Int32_t372,
	RuntimeInvoker_Object_t_Int32_t372_Object_t_Single_t388,
	RuntimeInvoker_Int32_t372_Int32_t372_Object_t_Single_t388,
	RuntimeInvoker_Void_t1771_Int32_t372_Vec2I_t1157,
	RuntimeInvoker_ProfileCollection_t1227_Object_t,
	RuntimeInvoker_WorldCenterMode_t1237,
	RuntimeInvoker_Void_t1771_FrameState_t1145,
	RuntimeInvoker_Void_t1771_FrameState_t1145_Object_t,
	RuntimeInvoker_Boolean_t384_TrackableResultData_t1134,
	RuntimeInvoker_VideoBGCfgData_t1156,
	RuntimeInvoker_Void_t1771_VideoBGCfgData_t1156,
	RuntimeInvoker_VideoTextureInfo_t1049,
	RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372,
	RuntimeInvoker_Matrix4x4_t404_Single_t388_Single_t388_Int32_t372,
	RuntimeInvoker_Void_t1771_PoseData_t1133,
	RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Object_t_Object_t,
	RuntimeInvoker_OrientedBoundingBox3D_t1092,
	RuntimeInvoker_Void_t1771_OrientedBoundingBox3D_t1092,
	RuntimeInvoker_Boolean_t384_RectU26_t4088_RectU26_t4088,
	RuntimeInvoker_Rect_t267_RectangleIntData_t1090_Rect_t267_SByte_t390_VideoModeData_t1065,
	RuntimeInvoker_UpDirection_t1171,
	RuntimeInvoker_UInt16_t393_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Vec2I_t1157,
	RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Vector2_t2,
	RuntimeInvoker_Void_t1771_ImageHeaderData_t1140_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_WordAbstractBehaviourU26_t4103,
	RuntimeInvoker_OrientedBoundingBox_t1091,
	RuntimeInvoker_Void_t1771_Vector3_t36_Quaternion_t41,
	RuntimeInvoker_Void_t1771_OrientedBoundingBox_t1091,
	RuntimeInvoker_Void_t1771_Int32_t372_IntPtr_t,
	RuntimeInvoker_Boolean_t384_IntPtr_t,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_IntPtr_t,
	RuntimeInvoker_Int32_t372_Int32_t372_IntPtr_t,
	RuntimeInvoker_Int32_t372_Int32_t372_IntPtr_t_Int32_t372_IntPtr_t,
	RuntimeInvoker_Int32_t372_IntPtr_t_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Int32_t372_IntPtr_t_IntPtr_t_Object_t_Int32_t372_IntPtr_t,
	RuntimeInvoker_Int32_t372_IntPtr_t_Int32_t372,
	RuntimeInvoker_Int32_t372_IntPtr_t,
	RuntimeInvoker_Int32_t372_Object_t_Single_t388,
	RuntimeInvoker_Void_t1771_IntPtr_t_SByte_t390,
	RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Object_t_IntPtr_t,
	RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t372_IntPtr_t_IntPtr_t_Int32_t372_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_IntPtr_t,
	RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Single_t388,
	RuntimeInvoker_Int32_t372_Int32_t372_Single_t388,
	RuntimeInvoker_Int32_t372_IntPtr_t_Int32_t372_IntPtr_t_Int32_t372,
	RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Single_t388_Single_t388_IntPtr_t_Int32_t372,
	RuntimeInvoker_Boolean_t384_IntPtr_t_IntPtr_t_Int32_t372_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t_Single_t388,
	RuntimeInvoker_Boolean_t384_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Void_t1771_IntPtr_t_Single_t388,
	RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372,
	RuntimeInvoker_Int32_t372_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_IntPtr_t_Int32_t372,
	RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_IntPtr_t,
	RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_IntPtr_t,
	RuntimeInvoker_Boolean_t384_Int32_t372_IntPtr_t,
	RuntimeInvoker_Boolean_t384_IntPtr_t_Int32_t372_IntPtr_t,
	RuntimeInvoker_Int32_t372_SByte_t390,
	RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_IntPtr_t_Int32_t372_IntPtr_t,
	RuntimeInvoker_Void_t1771_SmartTerrainInitializationInfo_t1083,
	RuntimeInvoker_Boolean_t384_Object_t_PropAbstractBehaviourU26_t4104,
	RuntimeInvoker_Boolean_t384_Object_t_SurfaceAbstractBehaviourU26_t4105,
	RuntimeInvoker_Object_t_MeshData_t1141_Object_t_SByte_t390,
	RuntimeInvoker_Object_t_Int32_t372_IntPtr_t,
	RuntimeInvoker_Void_t1771_LinkedList_1U26_t4106,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_PoseData_t1133,
	RuntimeInvoker_Vector3_t36_Matrix4x4_t404,
	RuntimeInvoker_Quaternion_t41_Matrix4x4_t404,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_PoseData_t1133,
	RuntimeInvoker_InitState_t1209,
	RuntimeInvoker_UpdateState_t1210,
	RuntimeInvoker_UpdateState_t1210_Int32_t372,
	RuntimeInvoker_Object_t_TargetSearchResult_t1212_Object_t,
	RuntimeInvoker_RectangleData_t1089,
	RuntimeInvoker_Boolean_t384_RectangleData_t1089,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_RectangleData_t1089_Object_t_Object_t,
	RuntimeInvoker_Vec2I_t1157,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Object_t_SByte_t390,
	RuntimeInvoker_ProfileData_t1226,
	RuntimeInvoker_ProfileData_t1226_Object_t,
	RuntimeInvoker_Void_t1771_ProfileData_t1226_Object_t,
	RuntimeInvoker_Object_t_Object_t_Vector2_t2_Vector2_t2,
	RuntimeInvoker_Object_t_Object_t_Vector2_t2_Object_t,
	RuntimeInvoker_CameraDeviceMode_t1063,
	RuntimeInvoker_Boolean_t384_SByte_t390_SByte_t390,
	RuntimeInvoker_Vec2I_t1157_Vector2_t2_Rect_t267_SByte_t390_VideoModeData_t1065,
	RuntimeInvoker_Vector2_t2_Vector2_t2_Rect_t267_SByte_t390_VideoModeData_t1065,
	RuntimeInvoker_OrientedBoundingBox_t1091_OrientedBoundingBox_t1091_Rect_t267_SByte_t390_VideoModeData_t1065,
	RuntimeInvoker_Void_t1771_Rect_t267_SByte_t390_Vector2U26_t4093_Vector2U26_t4093,
	RuntimeInvoker_Rect_t267_Vector2_t2_Vector2_t2_SByte_t390,
	RuntimeInvoker_Void_t1771_SByte_t390_SingleU26_t4090_SingleU26_t4090_SingleU26_t4090_SingleU26_t4090_BooleanU26_t4079,
	RuntimeInvoker_Boolean_t384_Vector2U26_t4093_Vector2U26_t4093,
	RuntimeInvoker_Boolean_t384_Vector2_t2_Vector2_t2_Single_t388,
	RuntimeInvoker_Void_t1771_Object_t_SByte_t390_Object_t,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_SByte_t390_Object_t_Object_t,
	RuntimeInvoker_UInt32_t389_Int32_t372,
	RuntimeInvoker_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_UInt32_t389_Object_t_Int32_t372,
	RuntimeInvoker_Sign_t1426_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_ConfidenceFactor_t1430,
	RuntimeInvoker_Byte_t391,
	RuntimeInvoker_Void_t1771_Object_t_Int32U26_t4080_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Int32U26_t4080_ByteU26_t4107_Int32U26_t4080_ByteU5BU5DU26_t4108,
	RuntimeInvoker_DateTime_t74_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_DSAParameters_t1561,
	RuntimeInvoker_RSAParameters_t1533_SByte_t390,
	RuntimeInvoker_Void_t1771_RSAParameters_t1533,
	RuntimeInvoker_Object_t_SByte_t390,
	RuntimeInvoker_DSAParameters_t1561_BooleanU26_t4079,
	RuntimeInvoker_Object_t_Object_t_SByte_t390_Object_t_SByte_t390,
	RuntimeInvoker_Boolean_t384_DateTime_t74,
	RuntimeInvoker_X509ChainStatusFlags_t1466,
	RuntimeInvoker_Void_t1771_Byte_t391,
	RuntimeInvoker_Void_t1771_Byte_t391_Byte_t391,
	RuntimeInvoker_AlertLevel_t1485,
	RuntimeInvoker_AlertDescription_t1486,
	RuntimeInvoker_Object_t_Byte_t391,
	RuntimeInvoker_Void_t1771_Int16_t392_Object_t_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390_SByte_t390_SByte_t390_Int16_t392_SByte_t390_SByte_t390,
	RuntimeInvoker_CipherAlgorithmType_t1488,
	RuntimeInvoker_HashAlgorithmType_t1506,
	RuntimeInvoker_ExchangeAlgorithmType_t1504,
	RuntimeInvoker_CipherMode_t1417,
	RuntimeInvoker_Int16_t392,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int16_t392,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int64_t386,
	RuntimeInvoker_Void_t1771_Object_t_ByteU5BU5DU26_t4108_ByteU5BU5DU26_t4108,
	RuntimeInvoker_Object_t_Byte_t391_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Object_t_Int16_t392,
	RuntimeInvoker_Int32_t372_Int16_t392,
	RuntimeInvoker_Object_t_Int16_t392_Object_t_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390_SByte_t390_SByte_t390_Int16_t392_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_SecurityProtocolType_t1520,
	RuntimeInvoker_SecurityCompressionType_t1519,
	RuntimeInvoker_HandshakeType_t1536,
	RuntimeInvoker_HandshakeState_t1505,
	RuntimeInvoker_UInt64_t394,
	RuntimeInvoker_SecurityProtocolType_t1520_Int16_t392,
	RuntimeInvoker_Object_t_Byte_t391_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t391_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Byte_t391_Object_t,
	RuntimeInvoker_Object_t_Byte_t391_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Int64_t386_Int64_t386_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Int32_t372_Int32_t372_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_Byte_t391_Byte_t391_Object_t,
	RuntimeInvoker_RSAParameters_t1533,
	RuntimeInvoker_Void_t1771_Object_t_Byte_t391,
	RuntimeInvoker_Void_t1771_Object_t_Byte_t391_Byte_t391,
	RuntimeInvoker_Void_t1771_Object_t_Byte_t391_Object_t,
	RuntimeInvoker_ContentType_t1499,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t4109,
	RuntimeInvoker_DictionaryEntry_t451,
	RuntimeInvoker_EditorBrowsableState_t1617,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Int16_t392_Int16_t392,
	RuntimeInvoker_Boolean_t384_Object_t_IPAddressU26_t4110,
	RuntimeInvoker_AddressFamily_t1622,
	RuntimeInvoker_Object_t_Int64_t386,
	RuntimeInvoker_Boolean_t384_Object_t_Int32U26_t4080,
	RuntimeInvoker_Boolean_t384_Object_t_IPv6AddressU26_t4111,
	RuntimeInvoker_UInt16_t393_Int16_t392,
	RuntimeInvoker_SecurityProtocolType_t1637,
	RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_Int32_t372_SByte_t390,
	RuntimeInvoker_AsnDecodeStatus_t1677_Object_t,
	RuntimeInvoker_Object_t_Int32_t372_Object_t_SByte_t390,
	RuntimeInvoker_X509ChainStatusFlags_t1665_Object_t,
	RuntimeInvoker_X509ChainStatusFlags_t1665_Object_t_Int32_t372_SByte_t390,
	RuntimeInvoker_X509ChainStatusFlags_t1665_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_X509ChainStatusFlags_t1665,
	RuntimeInvoker_Void_t1771_Object_t_Int32U26_t4080_Int32_t372_Int32_t372,
	RuntimeInvoker_X509RevocationFlag_t1672,
	RuntimeInvoker_X509RevocationMode_t1673,
	RuntimeInvoker_X509VerificationFlags_t1676,
	RuntimeInvoker_X509KeyUsageFlags_t1670,
	RuntimeInvoker_X509KeyUsageFlags_t1670_Int32_t372,
	RuntimeInvoker_Byte_t391_Int16_t392,
	RuntimeInvoker_Byte_t391_Int16_t392_Int16_t392,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t372_Int32_t372_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_RegexOptions_t1689,
	RuntimeInvoker_Category_t1696_Object_t,
	RuntimeInvoker_Boolean_t384_UInt16_t393_Int16_t392,
	RuntimeInvoker_Boolean_t384_Int32_t372_Int16_t392,
	RuntimeInvoker_Void_t1771_Int16_t392_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_UInt16_t393_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_Int16_t392_Int16_t392_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_Int16_t392_Object_t_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_UInt16_t393,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_SByte_t390_Object_t,
	RuntimeInvoker_Void_t1771_Int32_t372_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_SByte_t390_Int32_t372_Object_t,
	RuntimeInvoker_UInt16_t393_UInt16_t393_UInt16_t393,
	RuntimeInvoker_OpFlags_t1691_SByte_t390_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_UInt16_t393_UInt16_t393,
	RuntimeInvoker_Boolean_t384_Int32_t372_Int32U26_t4080_Int32_t372,
	RuntimeInvoker_Boolean_t384_Int32_t372_Int32U26_t4080_Int32U26_t4080_SByte_t390,
	RuntimeInvoker_Boolean_t384_Int32U26_t4080_Int32_t372,
	RuntimeInvoker_Boolean_t384_UInt16_t393_Int32_t372,
	RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_SByte_t390_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32U26_t4080_Int32U26_t4080,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_SByte_t390_Int32_t372,
	RuntimeInvoker_Interval_t1711,
	RuntimeInvoker_Boolean_t384_Interval_t1711,
	RuntimeInvoker_Void_t1771_Interval_t1711,
	RuntimeInvoker_Interval_t1711_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Double_t387_Interval_t1711,
	RuntimeInvoker_Object_t_Interval_t1711_Object_t_Object_t,
	RuntimeInvoker_Double_t387_Object_t,
	RuntimeInvoker_Int32_t372_Object_t_Int32U26_t4080,
	RuntimeInvoker_Int32_t372_Object_t_Int32U26_t4080_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Int32U26_t4080_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t4080,
	RuntimeInvoker_Object_t_RegexOptionsU26_t4112,
	RuntimeInvoker_Void_t1771_RegexOptionsU26_t4112_SByte_t390,
	RuntimeInvoker_Boolean_t384_Int32U26_t4080_Int32U26_t4080_Int32_t372,
	RuntimeInvoker_Category_t1696,
	RuntimeInvoker_Int32_t372_Int16_t392_Int32_t372_Int32_t372,
	RuntimeInvoker_Char_t383_Int16_t392,
	RuntimeInvoker_Int32_t372_Int32U26_t4080,
	RuntimeInvoker_Void_t1771_Int32U26_t4080_Int32U26_t4080,
	RuntimeInvoker_Void_t1771_Int32U26_t4080_Int32U26_t4080_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_UInt16_t393_SByte_t390,
	RuntimeInvoker_Void_t1771_Int16_t392_Int16_t392,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_UInt16_t393,
	RuntimeInvoker_Position_t1692,
	RuntimeInvoker_UriHostNameType_t1743_Object_t,
	RuntimeInvoker_Void_t1771_StringU26_t4077,
	RuntimeInvoker_Object_t_Object_t_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Char_t383_Object_t_Int32U26_t4080_CharU26_t4113,
	RuntimeInvoker_Void_t1771_Object_t_UriFormatExceptionU26_t4114,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_ObjectU5BU5DU26_t4115,
	RuntimeInvoker_Int32_t372_Object_t_ObjectU5BU5DU26_t4115,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_Byte_t391_Object_t,
	RuntimeInvoker_Decimal_t395_Object_t,
	RuntimeInvoker_Int16_t392_Object_t,
	RuntimeInvoker_Int64_t386_Object_t,
	RuntimeInvoker_SByte_t390_Object_t,
	RuntimeInvoker_UInt32_t389_Object_t,
	RuntimeInvoker_UInt64_t394_Object_t,
	RuntimeInvoker_Boolean_t384_SByte_t390_Object_t_Int32_t372_ExceptionU26_t4116,
	RuntimeInvoker_Boolean_t384_Object_t_SByte_t390_Int32U26_t4080_ExceptionU26_t4116,
	RuntimeInvoker_Boolean_t384_Int32_t372_SByte_t390_ExceptionU26_t4116,
	RuntimeInvoker_Boolean_t384_Int32U26_t4080_Object_t_SByte_t390_SByte_t390_ExceptionU26_t4116,
	RuntimeInvoker_Void_t1771_Int32U26_t4080_Object_t_Object_t_BooleanU26_t4079_BooleanU26_t4079,
	RuntimeInvoker_Void_t1771_Int32U26_t4080_Object_t_Object_t_BooleanU26_t4079,
	RuntimeInvoker_Boolean_t384_Int32U26_t4080_Object_t_Int32U26_t4080_SByte_t390_ExceptionU26_t4116,
	RuntimeInvoker_Boolean_t384_Int32U26_t4080_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Int16_t392_SByte_t390,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_Int32U26_t4080_ExceptionU26_t4116,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_Int32U26_t4080,
	RuntimeInvoker_TypeCode_t2375,
	RuntimeInvoker_Int32_t372_Int64_t386,
	RuntimeInvoker_Boolean_t384_Int64_t386,
	RuntimeInvoker_Boolean_t384_Object_t_SByte_t390_Int64U26_t4117_ExceptionU26_t4116,
	RuntimeInvoker_Int64_t386_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_Int64U26_t4117_ExceptionU26_t4116,
	RuntimeInvoker_Int64_t386_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Int64U26_t4117,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_Int64U26_t4117,
	RuntimeInvoker_Boolean_t384_Object_t_SByte_t390_UInt32U26_t4118_ExceptionU26_t4116,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_UInt32U26_t4118_ExceptionU26_t4116,
	RuntimeInvoker_UInt32_t389_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_UInt32_t389_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_UInt32U26_t4118,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_UInt32U26_t4118,
	RuntimeInvoker_UInt64_t394_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_UInt64U26_t4119_ExceptionU26_t4116,
	RuntimeInvoker_UInt64_t394_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_UInt64U26_t4119,
	RuntimeInvoker_Byte_t391_Object_t_Object_t,
	RuntimeInvoker_Byte_t391_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_ByteU26_t4107,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_ByteU26_t4107,
	RuntimeInvoker_Boolean_t384_Object_t_SByte_t390_SByteU26_t4120_ExceptionU26_t4116,
	RuntimeInvoker_SByte_t390_Object_t_Object_t,
	RuntimeInvoker_SByte_t390_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_SByteU26_t4120,
	RuntimeInvoker_Boolean_t384_Object_t_SByte_t390_Int16U26_t4121_ExceptionU26_t4116,
	RuntimeInvoker_Int16_t392_Object_t_Object_t,
	RuntimeInvoker_Int16_t392_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Int16U26_t4121,
	RuntimeInvoker_UInt16_t393_Object_t_Object_t,
	RuntimeInvoker_UInt16_t393_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_UInt16U26_t4122,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_UInt16U26_t4122,
	RuntimeInvoker_Void_t1771_ByteU2AU26_t4123_ByteU2AU26_t4123_DoubleU2AU26_t4124_UInt16U2AU26_t4125_UInt16U2AU26_t4125_UInt16U2AU26_t4125_UInt16U2AU26_t4125,
	RuntimeInvoker_UnicodeCategory_t1917_Int16_t392,
	RuntimeInvoker_Char_t383_Int16_t392_Object_t,
	RuntimeInvoker_Void_t1771_Int16_t392_Int32_t372,
	RuntimeInvoker_Char_t383_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Object_t,
	RuntimeInvoker_Int32_t372_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Object_t_SByte_t390_Object_t,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Int32_t372_Int32_t372_SByte_t390_Object_t,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Int16_t392_Int32_t372,
	RuntimeInvoker_Object_t_Int32_t372_Int16_t392,
	RuntimeInvoker_Object_t_Int16_t392_Int16_t392,
	RuntimeInvoker_Void_t1771_Object_t_Int32U26_t4080_Int32U26_t4080_Int32U26_t4080_BooleanU26_t4079_StringU26_t4077,
	RuntimeInvoker_Void_t1771_Int32_t372_Int16_t392,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Object_t,
	RuntimeInvoker_Object_t_Int16_t392_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Single_t388_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_Double_t387,
	RuntimeInvoker_Boolean_t384_Double_t387,
	RuntimeInvoker_Double_t387_Object_t_Object_t,
	RuntimeInvoker_Double_t387_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_DoubleU26_t4126_ExceptionU26_t4116,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_DoubleU26_t4126,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_DoubleU26_t4126,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390,
	RuntimeInvoker_Object_t_Decimal_t395,
	RuntimeInvoker_Decimal_t395_Decimal_t395_Decimal_t395,
	RuntimeInvoker_UInt64_t394_Decimal_t395,
	RuntimeInvoker_Int64_t386_Decimal_t395,
	RuntimeInvoker_Boolean_t384_Decimal_t395_Decimal_t395,
	RuntimeInvoker_Decimal_t395_Decimal_t395,
	RuntimeInvoker_Int32_t372_Decimal_t395_Decimal_t395,
	RuntimeInvoker_Int32_t372_Decimal_t395,
	RuntimeInvoker_Boolean_t384_Decimal_t395,
	RuntimeInvoker_Decimal_t395_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Int32U26_t4080_BooleanU26_t4079_BooleanU26_t4079_Int32U26_t4080_SByte_t390,
	RuntimeInvoker_Decimal_t395_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_DecimalU26_t4127_SByte_t390,
	RuntimeInvoker_Int32_t372_DecimalU26_t4127_UInt64U26_t4119,
	RuntimeInvoker_Int32_t372_DecimalU26_t4127_Int64U26_t4117,
	RuntimeInvoker_Int32_t372_DecimalU26_t4127_DecimalU26_t4127,
	RuntimeInvoker_Int32_t372_DecimalU26_t4127_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_DecimalU26_t4127_Int32_t372,
	RuntimeInvoker_Double_t387_DecimalU26_t4127,
	RuntimeInvoker_Void_t1771_DecimalU26_t4127_Int32_t372,
	RuntimeInvoker_Int32_t372_DecimalU26_t4127_DecimalU26_t4127_DecimalU26_t4127,
	RuntimeInvoker_Byte_t391_Decimal_t395,
	RuntimeInvoker_SByte_t390_Decimal_t395,
	RuntimeInvoker_Int16_t392_Decimal_t395,
	RuntimeInvoker_UInt16_t393_Decimal_t395,
	RuntimeInvoker_UInt32_t389_Decimal_t395,
	RuntimeInvoker_Decimal_t395_SByte_t390,
	RuntimeInvoker_Decimal_t395_Int16_t392,
	RuntimeInvoker_Decimal_t395_Int32_t372,
	RuntimeInvoker_Decimal_t395_Int64_t386,
	RuntimeInvoker_Decimal_t395_Single_t388,
	RuntimeInvoker_Decimal_t395_Double_t387,
	RuntimeInvoker_Single_t388_Decimal_t395,
	RuntimeInvoker_Double_t387_Decimal_t395,
	RuntimeInvoker_IntPtr_t_Int64_t386,
	RuntimeInvoker_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_IntPtr_t,
	RuntimeInvoker_UInt32_t389,
	RuntimeInvoker_UInt64_t394_IntPtr_t,
	RuntimeInvoker_UInt32_t389_IntPtr_t,
	RuntimeInvoker_UIntPtr_t_Int64_t386,
	RuntimeInvoker_UIntPtr_t_Object_t,
	RuntimeInvoker_UIntPtr_t_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t4128,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t372_SByte_t390_SByte_t390,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t390_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_UInt64_t394_Object_t_Int32_t372,
	RuntimeInvoker_Object_t_Object_t_Int16_t392,
	RuntimeInvoker_Object_t_Object_t_Int64_t386,
	RuntimeInvoker_Int64_t386_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Object_t_Int64_t386_Int64_t386,
	RuntimeInvoker_Object_t_Int64_t386_Int64_t386_Int64_t386,
	RuntimeInvoker_Void_t1771_Object_t_Int64_t386_Int64_t386,
	RuntimeInvoker_Void_t1771_Object_t_Int64_t386_Int64_t386_Int64_t386,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Int64_t386_Object_t_Int64_t386_Int64_t386,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Int64_t386,
	RuntimeInvoker_Int32_t372_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_Int32_t372_Object_t,
	RuntimeInvoker_TypeAttributes_t2031,
	RuntimeInvoker_MemberTypes_t2011,
	RuntimeInvoker_RuntimeTypeHandle_t1772,
	RuntimeInvoker_Object_t_Object_t_SByte_t390_SByte_t390,
	RuntimeInvoker_TypeCode_t2375_Object_t,
	RuntimeInvoker_Object_t_RuntimeTypeHandle_t1772,
	RuntimeInvoker_RuntimeTypeHandle_t1772_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t372_Object_t_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t372_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_RuntimeFieldHandle_t1774,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_ContractionU5BU5DU26_t4129_Level2MapU5BU5DU26_t4130,
	RuntimeInvoker_Void_t1771_Object_t_CodePointIndexerU26_t4131_ByteU2AU26_t4123_ByteU2AU26_t4123_CodePointIndexerU26_t4131_ByteU2AU26_t4123,
	RuntimeInvoker_Byte_t391_Int32_t372,
	RuntimeInvoker_Byte_t391_Int32_t372_Int32_t372,
	RuntimeInvoker_ExtenderType_t1816_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372_BooleanU26_t4079_BooleanU26_t4079_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32_t372_BooleanU26_t4079_BooleanU26_t4079_SByte_t390_SByte_t390_ContextU26_t4132,
	RuntimeInvoker_Int32_t372_SByte_t390_SByte_t390,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Int32_t372_Int32_t372_SByte_t390_ContextU26_t4132,
	RuntimeInvoker_Int32_t372_Object_t_Object_t_Int32_t372_Int32_t372_BooleanU26_t4079,
	RuntimeInvoker_Int32_t372_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int16_t392_Int32_t372_SByte_t390_ContextU26_t4132,
	RuntimeInvoker_Int32_t372_Object_t_Object_t_Int32_t372_Int32_t372_Object_t_ContextU26_t4132,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372_Object_t_Int32_t372_SByte_t390_ContextU26_t4132,
	RuntimeInvoker_Boolean_t384_Object_t_Int32U26_t4080_Int32_t372_Int32_t372_Object_t_SByte_t390_ContextU26_t4132,
	RuntimeInvoker_Boolean_t384_Object_t_Int32U26_t4080_Int32_t372_Int32_t372_Object_t_SByte_t390_Int32_t372_ContractionU26_t4133_ContextU26_t4132,
	RuntimeInvoker_Boolean_t384_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_SByte_t390,
	RuntimeInvoker_Boolean_t384_Object_t_Int32U26_t4080_Int32_t372_Int32_t372_Int32_t372_Object_t_SByte_t390_ContextU26_t4132,
	RuntimeInvoker_Boolean_t384_Object_t_Int32U26_t4080_Int32_t372_Int32_t372_Int32_t372_Object_t_SByte_t390_Int32_t372_ContractionU26_t4133_ContextU26_t4132,
	RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Object_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Object_t_SByte_t390,
	RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_SByte_t390_ByteU5BU5DU26_t4108_Int32U26_t4080,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_SByte_t390,
	RuntimeInvoker_ConfidenceFactor_t1825,
	RuntimeInvoker_Sign_t1827_Object_t_Object_t,
	RuntimeInvoker_DSAParameters_t1561_SByte_t390,
	RuntimeInvoker_Void_t1771_DSAParameters_t1561,
	RuntimeInvoker_Int16_t392_Object_t_Int32_t372,
	RuntimeInvoker_Double_t387_Object_t_Int32_t372,
	RuntimeInvoker_Object_t_Int16_t392_SByte_t390,
	RuntimeInvoker_Void_t1771_Int32_t372_Single_t388_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Single_t388_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Int32_t372_Single_t388_Object_t,
	RuntimeInvoker_Boolean_t384_Int32_t372_SByte_t390_MethodBaseU26_t4134_Int32U26_t4080_Int32U26_t4080_StringU26_t4077_Int32U26_t4080_Int32U26_t4080,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_SByte_t390,
	RuntimeInvoker_Int32_t372_DateTime_t74,
	RuntimeInvoker_DayOfWeek_t2325_DateTime_t74,
	RuntimeInvoker_Int32_t372_Int32U26_t4080_Int32_t372_Int32_t372,
	RuntimeInvoker_DayOfWeek_t2325_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32U26_t4080_Int32U26_t4080_Int32U26_t4080_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_SByte_t390,
	RuntimeInvoker_Void_t1771_DateTime_t74_DateTime_t74_TimeSpan_t427,
	RuntimeInvoker_TimeSpan_t427,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32U26_t4080,
	RuntimeInvoker_Decimal_t395,
	RuntimeInvoker_SByte_t390,
	RuntimeInvoker_UInt16_t393,
	RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_SByte_t390_Int32_t372_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_SByte_t390_Int32_t372,
	RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_SByte_t390_SByte_t390,
	RuntimeInvoker_Boolean_t384_Object_t_MonoIOErrorU26_t4135,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t372_Int32_t372_MonoIOErrorU26_t4135,
	RuntimeInvoker_Object_t_MonoIOErrorU26_t4135,
	RuntimeInvoker_FileAttributes_t1927_Object_t_MonoIOErrorU26_t4135,
	RuntimeInvoker_MonoFileType_t1936_IntPtr_t_MonoIOErrorU26_t4135,
	RuntimeInvoker_Boolean_t384_Object_t_MonoIOStatU26_t4136_MonoIOErrorU26_t4135,
	RuntimeInvoker_IntPtr_t_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_MonoIOErrorU26_t4135,
	RuntimeInvoker_Boolean_t384_IntPtr_t_MonoIOErrorU26_t4135,
	RuntimeInvoker_Int32_t372_IntPtr_t_Object_t_Int32_t372_Int32_t372_MonoIOErrorU26_t4135,
	RuntimeInvoker_Int64_t386_IntPtr_t_Int64_t386_Int32_t372_MonoIOErrorU26_t4135,
	RuntimeInvoker_Int64_t386_IntPtr_t_MonoIOErrorU26_t4135,
	RuntimeInvoker_Boolean_t384_IntPtr_t_Int64_t386_MonoIOErrorU26_t4135,
	RuntimeInvoker_Void_t1771_Object_t_SByte_t390_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_Int32_t372_Object_t_Object_t_Object_t,
	RuntimeInvoker_CallingConventions_t2006,
	RuntimeInvoker_RuntimeMethodHandle_t2367,
	RuntimeInvoker_MethodAttributes_t2012,
	RuntimeInvoker_MethodToken_t1973,
	RuntimeInvoker_FieldAttributes_t2009,
	RuntimeInvoker_RuntimeFieldHandle_t1774,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_OpCode_t1977,
	RuntimeInvoker_Void_t1771_OpCode_t1977_Object_t,
	RuntimeInvoker_StackBehaviour_t1981,
	RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Object_t,
	RuntimeInvoker_Object_t_Int32_t372_Int32_t372_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_SByte_t390_Object_t,
	RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t4080_ModuleU26_t4137,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t390_SByte_t390,
	RuntimeInvoker_AssemblyNameFlags_t2000,
	RuntimeInvoker_Object_t_Int32_t372_Object_t_ObjectU5BU5DU26_t4115_Object_t_Object_t_Object_t_ObjectU26_t4138,
	RuntimeInvoker_Void_t1771_ObjectU5BU5DU26_t4115_Object_t,
	RuntimeInvoker_Object_t_Int32_t372_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_ObjectU5BU5DU26_t4115_Object_t,
	RuntimeInvoker_Object_t_Int32_t372_Object_t_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_EventAttributes_t2007,
	RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Object_t_RuntimeFieldHandle_t1774,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Object_t_StreamingContext_t1013,
	RuntimeInvoker_Object_t_RuntimeMethodHandle_t2367,
	RuntimeInvoker_Void_t1771_Object_t_MonoEventInfoU26_t4139,
	RuntimeInvoker_MonoEventInfo_t2016_Object_t,
	RuntimeInvoker_Void_t1771_IntPtr_t_MonoMethodInfoU26_t4140,
	RuntimeInvoker_MonoMethodInfo_t2019_IntPtr_t,
	RuntimeInvoker_MethodAttributes_t2012_IntPtr_t,
	RuntimeInvoker_CallingConventions_t2006_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t4116,
	RuntimeInvoker_Void_t1771_Object_t_MonoPropertyInfoU26_t4141_Int32_t372,
	RuntimeInvoker_PropertyAttributes_t2027,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Int32_t372_Object_t_Object_t_Object_t,
	RuntimeInvoker_ParameterAttributes_t2023,
	RuntimeInvoker_GCHandle_t1300_Object_t_Int32_t372,
	RuntimeInvoker_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Void_t1771_Object_t_Int32_t372_IntPtr_t_Int32_t372,
	RuntimeInvoker_Void_t1771_IntPtr_t_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_IntPtr_t_Object_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_IntPtr_t_SByte_t390,
	RuntimeInvoker_Void_t1771_BooleanU26_t4079,
	RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t4077,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t4077,
	RuntimeInvoker_Void_t1771_SByte_t390_Object_t_SByte_t390_SByte_t390,
	RuntimeInvoker_TimeSpan_t427_Object_t,
	RuntimeInvoker_Void_t1771_TimeSpan_t427,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_SByte_t390_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1013_Object_t,
	RuntimeInvoker_Object_t_Object_t_StreamingContext_t1013_ISurrogateSelectorU26_t4142,
	RuntimeInvoker_Void_t1771_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_StringU26_t4077,
	RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t4138,
	RuntimeInvoker_Boolean_t384_Object_t_StringU26_t4077_StringU26_t4077,
	RuntimeInvoker_WellKnownObjectMode_t2159,
	RuntimeInvoker_StreamingContext_t1013,
	RuntimeInvoker_TypeFilterLevel_t2175,
	RuntimeInvoker_Void_t1771_Object_t_BooleanU26_t4079,
	RuntimeInvoker_Object_t_Byte_t391_Object_t_SByte_t390_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t391_Object_t_SByte_t390_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_SByte_t390_ObjectU26_t4138_HeaderU5BU5DU26_t4143,
	RuntimeInvoker_Void_t1771_Byte_t391_Object_t_SByte_t390_ObjectU26_t4138_HeaderU5BU5DU26_t4143,
	RuntimeInvoker_Boolean_t384_Byte_t391_Object_t,
	RuntimeInvoker_Void_t1771_Byte_t391_Object_t_Int64U26_t4117_ObjectU26_t4138_SerializationInfoU26_t4144,
	RuntimeInvoker_Void_t1771_Object_t_SByte_t390_SByte_t390_Int64U26_t4117_ObjectU26_t4138_SerializationInfoU26_t4144,
	RuntimeInvoker_Void_t1771_Object_t_Int64U26_t4117_ObjectU26_t4138_SerializationInfoU26_t4144,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Int64_t386_ObjectU26_t4138_SerializationInfoU26_t4144,
	RuntimeInvoker_Void_t1771_Int64_t386_Object_t_Object_t_Int64_t386_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Int64U26_t4117_ObjectU26_t4138,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Int64U26_t4117_ObjectU26_t4138,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Int64_t386_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Int64_t386_Int64_t386_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int64_t386_Object_t,
	RuntimeInvoker_Object_t_Object_t_Byte_t391,
	RuntimeInvoker_Void_t1771_Int64_t386_Int32_t372_Int64_t386,
	RuntimeInvoker_Void_t1771_Int64_t386_Object_t_Int64_t386,
	RuntimeInvoker_Void_t1771_Object_t_Int64_t386_Object_t_Int64_t386_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_SByte_t390_Object_t_SByte_t390,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_StreamingContext_t1013,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_StreamingContext_t1013,
	RuntimeInvoker_Void_t1771_StreamingContext_t1013,
	RuntimeInvoker_Object_t_StreamingContext_t1013_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_Int16_t392,
	RuntimeInvoker_Void_t1771_Object_t_DateTime_t74,
	RuntimeInvoker_SerializationEntry_t2192,
	RuntimeInvoker_StreamingContextStates_t2195,
	RuntimeInvoker_CspProviderFlags_t2200,
	RuntimeInvoker_UInt32_t389_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Object_t_Object_t_SByte_t390,
	RuntimeInvoker_Void_t1771_Int64_t386_Object_t_Int32_t372,
	RuntimeInvoker_UInt32_t389_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_UInt32U26_t4118_Int32_t372_UInt32U26_t4118_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t,
	RuntimeInvoker_Void_t1771_Int64_t386_Int64_t386,
	RuntimeInvoker_UInt64_t394_Int64_t386_Int32_t372,
	RuntimeInvoker_UInt64_t394_Int64_t386_Int64_t386_Int64_t386,
	RuntimeInvoker_UInt64_t394_Int64_t386,
	RuntimeInvoker_PaddingMode_t1421,
	RuntimeInvoker_Void_t1771_StringBuilderU26_t4145_Int32_t372,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_EncoderFallbackBufferU26_t4146_CharU5BU5DU26_t4147,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_DecoderFallbackBufferU26_t4148,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Boolean_t384_Int16_t392_Int32_t372,
	RuntimeInvoker_Boolean_t384_Int16_t392_Int16_t392_Int32_t372,
	RuntimeInvoker_Void_t1771_Int16_t392_Int16_t392_Int32_t372,
	RuntimeInvoker_Object_t_Int32U26_t4080,
	RuntimeInvoker_Object_t_Int32_t372_Object_t_Int32_t372,
	RuntimeInvoker_Void_t1771_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_SByte_t390_Int32_t372_SByte_t390_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_SByte_t390_Int32U26_t4080_BooleanU26_t4079_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_Int32U26_t4080,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_CharU26_t4113_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_CharU26_t4113_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_CharU26_t4113_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Int32_t372_CharU26_t4113_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_Object_t_Int64_t386_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_Object_t_Int64_t386_Int32_t372_Object_t_Int32U26_t4080,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Object_t_Int32_t372_UInt32U26_t4118_UInt32U26_t4118_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_SByte_t390,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Int32_t372_UInt32U26_t4118_UInt32U26_t4118_Object_t_DecoderFallbackBufferU26_t4148_ByteU5BU5DU26_t4108_SByte_t390,
	RuntimeInvoker_Void_t1771_SByte_t390_Int32_t372,
	RuntimeInvoker_IntPtr_t_SByte_t390_Object_t_BooleanU26_t4079,
	RuntimeInvoker_IntPtr_t_SByte_t390_SByte_t390_Object_t_BooleanU26_t4079,
	RuntimeInvoker_ThreadState_t2299,
	RuntimeInvoker_Boolean_t384_TimeSpan_t427_TimeSpan_t427,
	RuntimeInvoker_Boolean_t384_Int64_t386_Int64_t386_SByte_t390,
	RuntimeInvoker_Boolean_t384_IntPtr_t_Int32_t372_SByte_t390,
	RuntimeInvoker_Boolean_t384_TimeSpan_t427,
	RuntimeInvoker_Boolean_t384_TimeSpan_t427_SByte_t390,
	RuntimeInvoker_Int64_t386_Double_t387,
	RuntimeInvoker_Object_t_Double_t387,
	RuntimeInvoker_Int64_t386_Object_t_Int32_t372,
	RuntimeInvoker_UInt16_t393_Object_t_Int32_t372,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t372_Int32_t372,
	RuntimeInvoker_Byte_t391_SByte_t390,
	RuntimeInvoker_Byte_t391_Double_t387,
	RuntimeInvoker_Byte_t391_Single_t388,
	RuntimeInvoker_Byte_t391_Int64_t386,
	RuntimeInvoker_Char_t383_SByte_t390,
	RuntimeInvoker_Char_t383_Int64_t386,
	RuntimeInvoker_Char_t383_Single_t388,
	RuntimeInvoker_Char_t383_Object_t_Object_t,
	RuntimeInvoker_DateTime_t74_Object_t_Object_t,
	RuntimeInvoker_DateTime_t74_Int16_t392,
	RuntimeInvoker_DateTime_t74_Int32_t372,
	RuntimeInvoker_DateTime_t74_Int64_t386,
	RuntimeInvoker_DateTime_t74_Single_t388,
	RuntimeInvoker_DateTime_t74_SByte_t390,
	RuntimeInvoker_Double_t387_SByte_t390,
	RuntimeInvoker_Double_t387_Double_t387,
	RuntimeInvoker_Double_t387_Single_t388,
	RuntimeInvoker_Double_t387_Int32_t372,
	RuntimeInvoker_Double_t387_Int64_t386,
	RuntimeInvoker_Double_t387_Int16_t392,
	RuntimeInvoker_Int16_t392_SByte_t390,
	RuntimeInvoker_Int16_t392_Double_t387,
	RuntimeInvoker_Int16_t392_Single_t388,
	RuntimeInvoker_Int16_t392_Int32_t372,
	RuntimeInvoker_Int16_t392_Int64_t386,
	RuntimeInvoker_Int64_t386_SByte_t390,
	RuntimeInvoker_Int64_t386_Int16_t392,
	RuntimeInvoker_Int64_t386_Single_t388,
	RuntimeInvoker_Int64_t386_Int64_t386,
	RuntimeInvoker_SByte_t390_SByte_t390,
	RuntimeInvoker_SByte_t390_Int16_t392,
	RuntimeInvoker_SByte_t390_Double_t387,
	RuntimeInvoker_SByte_t390_Single_t388,
	RuntimeInvoker_SByte_t390_Int32_t372,
	RuntimeInvoker_SByte_t390_Int64_t386,
	RuntimeInvoker_Single_t388_SByte_t390,
	RuntimeInvoker_Single_t388_Double_t387,
	RuntimeInvoker_Single_t388_Int64_t386,
	RuntimeInvoker_Single_t388_Int16_t392,
	RuntimeInvoker_UInt16_t393_SByte_t390,
	RuntimeInvoker_UInt16_t393_Double_t387,
	RuntimeInvoker_UInt16_t393_Single_t388,
	RuntimeInvoker_UInt16_t393_Int32_t372,
	RuntimeInvoker_UInt16_t393_Int64_t386,
	RuntimeInvoker_UInt32_t389_SByte_t390,
	RuntimeInvoker_UInt32_t389_Int16_t392,
	RuntimeInvoker_UInt32_t389_Double_t387,
	RuntimeInvoker_UInt32_t389_Single_t388,
	RuntimeInvoker_UInt32_t389_Int64_t386,
	RuntimeInvoker_UInt64_t394_SByte_t390,
	RuntimeInvoker_UInt64_t394_Int16_t392,
	RuntimeInvoker_UInt64_t394_Double_t387,
	RuntimeInvoker_UInt64_t394_Single_t388,
	RuntimeInvoker_UInt64_t394_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_SByte_t390_TimeSpan_t427,
	RuntimeInvoker_Void_t1771_Int64_t386_Int32_t372,
	RuntimeInvoker_DayOfWeek_t2325,
	RuntimeInvoker_DateTimeKind_t2322,
	RuntimeInvoker_DateTime_t74_TimeSpan_t427,
	RuntimeInvoker_DateTime_t74_Double_t387,
	RuntimeInvoker_Int32_t372_DateTime_t74_DateTime_t74,
	RuntimeInvoker_DateTime_t74_DateTime_t74_Int32_t372,
	RuntimeInvoker_DateTime_t74_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Int32_t372_DateTimeU26_t4149_DateTimeOffsetU26_t4150_SByte_t390_ExceptionU26_t4116,
	RuntimeInvoker_Object_t_Object_t_SByte_t390_ExceptionU26_t4116,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372_SByte_t390_SByte_t390_Int32U26_t4080,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Object_t_Object_t_SByte_t390_Int32U26_t4080,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Int32_t372_Object_t_Int32U26_t4080,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Int32_t372_Object_t_SByte_t390_Int32U26_t4080_Int32U26_t4080,
	RuntimeInvoker_Boolean_t384_Object_t_Int32_t372_Object_t_SByte_t390_Int32U26_t4080,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_SByte_t390_DateTimeU26_t4149_DateTimeOffsetU26_t4150_Object_t_Int32_t372_SByte_t390_BooleanU26_t4079_BooleanU26_t4079,
	RuntimeInvoker_DateTime_t74_Object_t_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_Object_t_Object_t_Int32_t372_DateTimeU26_t4149_SByte_t390_BooleanU26_t4079_SByte_t390_ExceptionU26_t4116,
	RuntimeInvoker_DateTime_t74_DateTime_t74_TimeSpan_t427,
	RuntimeInvoker_Boolean_t384_DateTime_t74_DateTime_t74,
	RuntimeInvoker_TimeSpan_t427_DateTime_t74_DateTime_t74,
	RuntimeInvoker_Void_t1771_DateTime_t74_TimeSpan_t427,
	RuntimeInvoker_Void_t1771_Int64_t386_TimeSpan_t427,
	RuntimeInvoker_Int32_t372_DateTimeOffset_t2323,
	RuntimeInvoker_Boolean_t384_DateTimeOffset_t2323,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int16_t392,
	RuntimeInvoker_Object_t_Int16_t392_Object_t_BooleanU26_t4079_BooleanU26_t4079,
	RuntimeInvoker_Object_t_Int16_t392_Object_t_BooleanU26_t4079_BooleanU26_t4079_SByte_t390,
	RuntimeInvoker_Object_t_DateTime_t74_Object_t_Object_t,
	RuntimeInvoker_Object_t_DateTime_t74_Nullable_1_t2426_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_MonoEnumInfo_t2336,
	RuntimeInvoker_Void_t1771_Object_t_MonoEnumInfoU26_t4151,
	RuntimeInvoker_Int32_t372_Int16_t392_Int16_t392,
	RuntimeInvoker_Int32_t372_Int64_t386_Int64_t386,
	RuntimeInvoker_PlatformID_t2364,
	RuntimeInvoker_Void_t1771_Int32_t372_Int16_t392_Int16_t392_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Int32_t372_Guid_t452,
	RuntimeInvoker_Boolean_t384_Guid_t452,
	RuntimeInvoker_Guid_t452,
	RuntimeInvoker_Object_t_SByte_t390_SByte_t390_SByte_t390,
	RuntimeInvoker_Double_t387_Double_t387_Double_t387,
	RuntimeInvoker_TypeAttributes_t2031_Object_t,
	RuntimeInvoker_Object_t_SByte_t390_SByte_t390,
	RuntimeInvoker_Void_t1771_UInt64U2AU26_t4152_Int32U2AU26_t4153_CharU2AU26_t4154_CharU2AU26_t4154_Int64U2AU26_t4155_Int32U2AU26_t4153,
	RuntimeInvoker_Void_t1771_Int32_t372_Int64_t386,
	RuntimeInvoker_Void_t1771_Object_t_Double_t387_Int32_t372,
	RuntimeInvoker_Void_t1771_Object_t_Decimal_t395,
	RuntimeInvoker_Object_t_Object_t_SByte_t390_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t392_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int64_t386_Object_t,
	RuntimeInvoker_Object_t_Object_t_Single_t388_Object_t,
	RuntimeInvoker_Object_t_Object_t_Double_t387_Object_t,
	RuntimeInvoker_Object_t_Object_t_Decimal_t395_Object_t,
	RuntimeInvoker_Object_t_Single_t388_Object_t,
	RuntimeInvoker_Object_t_Double_t387_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_BooleanU26_t4079_SByte_t390_Int32U26_t4080_Int32U26_t4080,
	RuntimeInvoker_Object_t_Object_t_Int32_t372_Int32_t372_Object_t_SByte_t390_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_Int64_t386_Int32_t372_Int32_t372_Int32_t372_Int32_t372_Int32_t372,
	RuntimeInvoker_TimeSpan_t427_TimeSpan_t427,
	RuntimeInvoker_Int32_t372_TimeSpan_t427_TimeSpan_t427,
	RuntimeInvoker_Int32_t372_TimeSpan_t427,
	RuntimeInvoker_TimeSpan_t427_Double_t387,
	RuntimeInvoker_TimeSpan_t427_Double_t387_Int64_t386,
	RuntimeInvoker_TimeSpan_t427_TimeSpan_t427_TimeSpan_t427,
	RuntimeInvoker_TimeSpan_t427_DateTime_t74,
	RuntimeInvoker_Boolean_t384_DateTime_t74_Object_t,
	RuntimeInvoker_DateTime_t74_DateTime_t74,
	RuntimeInvoker_TimeSpan_t427_DateTime_t74_TimeSpan_t427,
	RuntimeInvoker_Boolean_t384_Int32_t372_Int64U5BU5DU26_t4156_StringU5BU5DU26_t4157,
	RuntimeInvoker_Object_t_Object_t_Single_t388,
	RuntimeInvoker_Boolean_t384_ObjectU26_t4138_Object_t,
	RuntimeInvoker_Void_t1771_ObjectU26_t4138_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Int32_t372_Int32_t372_Object_t,
	RuntimeInvoker_Enumerator_t3528,
	RuntimeInvoker_Enumerator_t2585,
	RuntimeInvoker_Enumerator_t2569,
	RuntimeInvoker_Enumerator_t2581,
	RuntimeInvoker_Void_t1771_Int32_t372_ObjectU26_t4138,
	RuntimeInvoker_Void_t1771_ObjectU5BU5DU26_t4115_Int32_t372,
	RuntimeInvoker_Void_t1771_ObjectU5BU5DU26_t4115_Int32_t372_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t2507,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2507,
	RuntimeInvoker_KeyValuePair_2_t2507_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Object_t_ObjectU26_t4138,
	RuntimeInvoker_Enumerator_t2513,
	RuntimeInvoker_DictionaryEntry_t451_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2507,
	RuntimeInvoker_Enumerator_t2512,
	RuntimeInvoker_Enumerator_t2516,
	RuntimeInvoker_Int32_t372_Int32_t372_Int32_t372_Object_t,
	RuntimeInvoker_Enumerator_t2469,
	RuntimeInvoker_ERaffleResult_t207_Object_t,
	RuntimeInvoker_Enumerator_t436,
	RuntimeInvoker_KeyValuePair_2_t352,
	RuntimeInvoker_ERaffleResult_t207,
	RuntimeInvoker_KeyValuePair_2_t352_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t234,
	RuntimeInvoker_AnimatorFrame_t235,
	RuntimeInvoker_Enumerator_t2878,
	RuntimeInvoker_Enumerator_t2875,
	RuntimeInvoker_KeyValuePair_2_t2871,
	RuntimeInvoker_Void_t1771_FloatTween_t539,
	RuntimeInvoker_Void_t1771_ColorTween_t536,
	RuntimeInvoker_Boolean_t384_TypeU26_t4158_Int32_t372,
	RuntimeInvoker_Boolean_t384_BooleanU26_t4079_SByte_t390,
	RuntimeInvoker_Boolean_t384_FillMethodU26_t4159_Int32_t372,
	RuntimeInvoker_Boolean_t384_SingleU26_t4090_Single_t388,
	RuntimeInvoker_Boolean_t384_ContentTypeU26_t4160_Int32_t372,
	RuntimeInvoker_Boolean_t384_LineTypeU26_t4161_Int32_t372,
	RuntimeInvoker_Boolean_t384_InputTypeU26_t4162_Int32_t372,
	RuntimeInvoker_Boolean_t384_TouchScreenKeyboardTypeU26_t4163_Int32_t372,
	RuntimeInvoker_Boolean_t384_CharacterValidationU26_t4164_Int32_t372,
	RuntimeInvoker_Boolean_t384_CharU26_t4113_Int16_t392,
	RuntimeInvoker_Boolean_t384_DirectionU26_t4165_Int32_t372,
	RuntimeInvoker_Boolean_t384_NavigationU26_t4166_Navigation_t613,
	RuntimeInvoker_Boolean_t384_TransitionU26_t4167_Int32_t372,
	RuntimeInvoker_Boolean_t384_ColorBlockU26_t4168_ColorBlock_t551,
	RuntimeInvoker_Boolean_t384_SpriteStateU26_t4169_SpriteState_t630,
	RuntimeInvoker_Boolean_t384_DirectionU26_t4170_Int32_t372,
	RuntimeInvoker_Boolean_t384_AspectModeU26_t4171_Int32_t372,
	RuntimeInvoker_Boolean_t384_FitModeU26_t4172_Int32_t372,
	RuntimeInvoker_Void_t1771_CornerU26_t4173_Int32_t372,
	RuntimeInvoker_Void_t1771_AxisU26_t4174_Int32_t372,
	RuntimeInvoker_Void_t1771_Vector2U26_t4093_Vector2_t2,
	RuntimeInvoker_Void_t1771_ConstraintU26_t4175_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32U26_t4080_Int32_t372,
	RuntimeInvoker_Void_t1771_SingleU26_t4090_Single_t388,
	RuntimeInvoker_Void_t1771_BooleanU26_t4079_SByte_t390,
	RuntimeInvoker_Void_t1771_TextAnchorU26_t4176_Int32_t372,
	RuntimeInvoker_Enumerator_t1311,
	RuntimeInvoker_Enumerator_t3265,
	RuntimeInvoker_Void_t1771_Int32_t372_Double_t387,
	RuntimeInvoker_KeyValuePair_2_t2507_Int32_t372,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t2507,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2507,
	RuntimeInvoker_Link_t1869_Int32_t372,
	RuntimeInvoker_Void_t1771_Link_t1869,
	RuntimeInvoker_Boolean_t384_Link_t1869,
	RuntimeInvoker_Int32_t372_Link_t1869,
	RuntimeInvoker_Void_t1771_Int32_t372_Link_t1869,
	RuntimeInvoker_DictionaryEntry_t451_Int32_t372,
	RuntimeInvoker_Void_t1771_DictionaryEntry_t451,
	RuntimeInvoker_Boolean_t384_DictionaryEntry_t451,
	RuntimeInvoker_Int32_t372_DictionaryEntry_t451,
	RuntimeInvoker_Void_t1771_Int32_t372_DictionaryEntry_t451,
	RuntimeInvoker_KeyValuePair_2_t2546_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t2546,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2546,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t2546,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2546,
	RuntimeInvoker_Vector3_t36_Int32_t372,
	RuntimeInvoker_Int32_t372_Vector3_t36,
	RuntimeInvoker_Void_t1771_Int32_t372_Vector3_t36,
	RuntimeInvoker_Matrix4x4_t404_Int32_t372,
	RuntimeInvoker_Int32_t372_Matrix4x4_t404,
	RuntimeInvoker_Void_t1771_Int32_t372_Matrix4x4_t404,
	RuntimeInvoker_BoneWeight_t405_Int32_t372,
	RuntimeInvoker_Void_t1771_BoneWeight_t405,
	RuntimeInvoker_Boolean_t384_BoneWeight_t405,
	RuntimeInvoker_Int32_t372_BoneWeight_t405,
	RuntimeInvoker_Void_t1771_Int32_t372_BoneWeight_t405,
	RuntimeInvoker_Void_t1771_Int32_t372_Vector2_t2,
	RuntimeInvoker_KeyValuePair_2_t2667_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t2667,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2667,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t2667,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2667,
	RuntimeInvoker_EScreens_t188_Int32_t372,
	RuntimeInvoker_KeyValuePair_2_t2690_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t2690,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2690,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t2690,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2690,
	RuntimeInvoker_ESubScreens_t189_Int32_t372,
	RuntimeInvoker_KeyValuePair_2_t352_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t352,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t352,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t352,
	RuntimeInvoker_FocusMode_t438_Int32_t372,
	RuntimeInvoker_Void_t1771_FocusModeU5BU5DU26_t4177_Int32_t372,
	RuntimeInvoker_Void_t1771_FocusModeU5BU5DU26_t4177_Int32_t372_Int32_t372,
	RuntimeInvoker_KeyValuePair_2_t352_Object_t_Object_t_Int32_t372,
	RuntimeInvoker_KeyValuePair_2_t2740_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t2740,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2740,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t2740,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2740,
	RuntimeInvoker_AnimatorFrame_t235_Int32_t372,
	RuntimeInvoker_Void_t1771_AnimatorFrame_t235,
	RuntimeInvoker_Boolean_t384_AnimatorFrame_t235,
	RuntimeInvoker_Int32_t372_AnimatorFrame_t235,
	RuntimeInvoker_Void_t1771_Int32_t372_AnimatorFrame_t235,
	RuntimeInvoker_Void_t1771_AnimatorFrameU5BU5DU26_t4178_Int32_t372,
	RuntimeInvoker_Void_t1771_AnimatorFrameU5BU5DU26_t4178_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_AnimatorFrame_t235_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_AnimatorFrame_t235_AnimatorFrame_t235_Object_t,
	RuntimeInvoker_Boolean_t384_Color_t9,
	RuntimeInvoker_Int32_t372_Color_t9,
	RuntimeInvoker_Int32_t372_Rect_t267,
	RuntimeInvoker_RaycastResult_t512_Int32_t372,
	RuntimeInvoker_Boolean_t384_RaycastResult_t512,
	RuntimeInvoker_Int32_t372_RaycastResult_t512,
	RuntimeInvoker_Void_t1771_Int32_t372_RaycastResult_t512,
	RuntimeInvoker_Void_t1771_RaycastResultU5BU5DU26_t4179_Int32_t372,
	RuntimeInvoker_Void_t1771_RaycastResultU5BU5DU26_t4179_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_RaycastResult_t512_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_RaycastResult_t512_RaycastResult_t512_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2871_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t2871,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t2871,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t2871,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t2871,
	RuntimeInvoker_RaycastHit2D_t729_Int32_t372,
	RuntimeInvoker_Void_t1771_RaycastHit2D_t729,
	RuntimeInvoker_Boolean_t384_RaycastHit2D_t729,
	RuntimeInvoker_Int32_t372_RaycastHit2D_t729,
	RuntimeInvoker_Void_t1771_Int32_t372_RaycastHit2D_t729,
	RuntimeInvoker_RaycastHit_t60_Int32_t372,
	RuntimeInvoker_Void_t1771_RaycastHit_t60,
	RuntimeInvoker_Boolean_t384_RaycastHit_t60,
	RuntimeInvoker_Int32_t372_RaycastHit_t60,
	RuntimeInvoker_Void_t1771_Int32_t372_RaycastHit_t60,
	RuntimeInvoker_UIVertex_t605_Int32_t372,
	RuntimeInvoker_Boolean_t384_UIVertex_t605,
	RuntimeInvoker_Int32_t372_UIVertex_t605,
	RuntimeInvoker_Void_t1771_Int32_t372_UIVertex_t605,
	RuntimeInvoker_Void_t1771_UIVertexU5BU5DU26_t4180_Int32_t372,
	RuntimeInvoker_Void_t1771_UIVertexU5BU5DU26_t4180_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_UIVertex_t605_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_UIVertex_t605_UIVertex_t605_Object_t,
	RuntimeInvoker_ContentType_t591_Int32_t372,
	RuntimeInvoker_UILineInfo_t752_Int32_t372,
	RuntimeInvoker_Void_t1771_UILineInfo_t752,
	RuntimeInvoker_Boolean_t384_UILineInfo_t752,
	RuntimeInvoker_Int32_t372_UILineInfo_t752,
	RuntimeInvoker_Void_t1771_Int32_t372_UILineInfo_t752,
	RuntimeInvoker_UICharInfo_t754_Int32_t372,
	RuntimeInvoker_Void_t1771_UICharInfo_t754,
	RuntimeInvoker_Boolean_t384_UICharInfo_t754,
	RuntimeInvoker_Int32_t372_UICharInfo_t754,
	RuntimeInvoker_Void_t1771_Int32_t372_UICharInfo_t754,
	RuntimeInvoker_Void_t1771_Vector3U5BU5DU26_t4181_Int32_t372,
	RuntimeInvoker_Void_t1771_Vector3U5BU5DU26_t4181_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Vector3_t36_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Vector3_t36_Vector3_t36_Object_t,
	RuntimeInvoker_Color32_t711_Int32_t372,
	RuntimeInvoker_Void_t1771_Color32_t711,
	RuntimeInvoker_Boolean_t384_Color32_t711,
	RuntimeInvoker_Int32_t372_Color32_t711,
	RuntimeInvoker_Void_t1771_Int32_t372_Color32_t711,
	RuntimeInvoker_Void_t1771_Color32U5BU5DU26_t4182_Int32_t372,
	RuntimeInvoker_Void_t1771_Color32U5BU5DU26_t4182_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Color32_t711_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Color32_t711_Color32_t711_Object_t,
	RuntimeInvoker_Void_t1771_Vector2U5BU5DU26_t4183_Int32_t372,
	RuntimeInvoker_Void_t1771_Vector2U5BU5DU26_t4183_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Vector2_t2_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Vector2_t2_Vector2_t2_Object_t,
	RuntimeInvoker_Void_t1771_Vector4_t680,
	RuntimeInvoker_Boolean_t384_Vector4_t680,
	RuntimeInvoker_Int32_t372_Vector4_t680,
	RuntimeInvoker_Void_t1771_Vector4U5BU5DU26_t4184_Int32_t372,
	RuntimeInvoker_Void_t1771_Vector4U5BU5DU26_t4184_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_Vector4_t680_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Vector4_t680_Vector4_t680_Object_t,
	RuntimeInvoker_Void_t1771_Int32U5BU5DU26_t4185_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32U5BU5DU26_t4185_Int32_t372_Int32_t372,
	RuntimeInvoker_GcAchievementData_t939_Int32_t372,
	RuntimeInvoker_Void_t1771_GcAchievementData_t939,
	RuntimeInvoker_Boolean_t384_GcAchievementData_t939,
	RuntimeInvoker_Int32_t372_GcAchievementData_t939,
	RuntimeInvoker_Void_t1771_Int32_t372_GcAchievementData_t939,
	RuntimeInvoker_GcScoreData_t940_Int32_t372,
	RuntimeInvoker_Boolean_t384_GcScoreData_t940,
	RuntimeInvoker_Int32_t372_GcScoreData_t940,
	RuntimeInvoker_Void_t1771_Int32_t372_GcScoreData_t940,
	RuntimeInvoker_ContactPoint_t863_Int32_t372,
	RuntimeInvoker_Void_t1771_ContactPoint_t863,
	RuntimeInvoker_Boolean_t384_ContactPoint_t863,
	RuntimeInvoker_Int32_t372_ContactPoint_t863,
	RuntimeInvoker_Void_t1771_Int32_t372_ContactPoint_t863,
	RuntimeInvoker_ContactPoint2D_t869_Int32_t372,
	RuntimeInvoker_Void_t1771_ContactPoint2D_t869,
	RuntimeInvoker_Boolean_t384_ContactPoint2D_t869,
	RuntimeInvoker_Int32_t372_ContactPoint2D_t869,
	RuntimeInvoker_Void_t1771_Int32_t372_ContactPoint2D_t869,
	RuntimeInvoker_WebCamDevice_t876_Int32_t372,
	RuntimeInvoker_Void_t1771_WebCamDevice_t876,
	RuntimeInvoker_Boolean_t384_WebCamDevice_t876,
	RuntimeInvoker_Int32_t372_WebCamDevice_t876,
	RuntimeInvoker_Void_t1771_Int32_t372_WebCamDevice_t876,
	RuntimeInvoker_Keyframe_t883_Int32_t372,
	RuntimeInvoker_Void_t1771_Keyframe_t883,
	RuntimeInvoker_Boolean_t384_Keyframe_t883,
	RuntimeInvoker_Int32_t372_Keyframe_t883,
	RuntimeInvoker_Void_t1771_Int32_t372_Keyframe_t883,
	RuntimeInvoker_CharacterInfo_t892_Int32_t372,
	RuntimeInvoker_Void_t1771_CharacterInfo_t892,
	RuntimeInvoker_Boolean_t384_CharacterInfo_t892,
	RuntimeInvoker_Int32_t372_CharacterInfo_t892,
	RuntimeInvoker_Void_t1771_Int32_t372_CharacterInfo_t892,
	RuntimeInvoker_Void_t1771_UICharInfoU5BU5DU26_t4186_Int32_t372,
	RuntimeInvoker_Void_t1771_UICharInfoU5BU5DU26_t4186_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_UICharInfo_t754_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_UICharInfo_t754_UICharInfo_t754_Object_t,
	RuntimeInvoker_Void_t1771_UILineInfoU5BU5DU26_t4187_Int32_t372,
	RuntimeInvoker_Void_t1771_UILineInfoU5BU5DU26_t4187_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_UILineInfo_t752_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_UILineInfo_t752_UILineInfo_t752_Object_t,
	RuntimeInvoker_ParameterModifier_t2024_Int32_t372,
	RuntimeInvoker_Void_t1771_ParameterModifier_t2024,
	RuntimeInvoker_Boolean_t384_ParameterModifier_t2024,
	RuntimeInvoker_Int32_t372_ParameterModifier_t2024,
	RuntimeInvoker_Void_t1771_Int32_t372_ParameterModifier_t2024,
	RuntimeInvoker_HitInfo_t959_Int32_t372,
	RuntimeInvoker_Void_t1771_HitInfo_t959,
	RuntimeInvoker_Int32_t372_HitInfo_t959,
	RuntimeInvoker_KeyValuePair_2_t3183_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t3183,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3183,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t3183,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3183,
	RuntimeInvoker_TextEditOp_t976_Int32_t372,
	RuntimeInvoker_EyewearCalibrationReading_t1080_Int32_t372,
	RuntimeInvoker_Void_t1771_EyewearCalibrationReading_t1080,
	RuntimeInvoker_Boolean_t384_EyewearCalibrationReading_t1080,
	RuntimeInvoker_Int32_t372_EyewearCalibrationReading_t1080,
	RuntimeInvoker_Void_t1771_Int32_t372_EyewearCalibrationReading_t1080,
	RuntimeInvoker_KeyValuePair_2_t3257_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t3257,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3257,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t3257,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3257,
	RuntimeInvoker_PIXEL_FORMAT_t1108_Int32_t372,
	RuntimeInvoker_Void_t1771_PIXEL_FORMATU5BU5DU26_t4188_Int32_t372,
	RuntimeInvoker_Void_t1771_PIXEL_FORMATU5BU5DU26_t4188_Int32_t372_Int32_t372,
	RuntimeInvoker_TrackableResultData_t1134_Int32_t372,
	RuntimeInvoker_Void_t1771_TrackableResultData_t1134,
	RuntimeInvoker_Int32_t372_TrackableResultData_t1134,
	RuntimeInvoker_Void_t1771_Int32_t372_TrackableResultData_t1134,
	RuntimeInvoker_WordData_t1139_Int32_t372,
	RuntimeInvoker_Void_t1771_WordData_t1139,
	RuntimeInvoker_Boolean_t384_WordData_t1139,
	RuntimeInvoker_Int32_t372_WordData_t1139,
	RuntimeInvoker_Void_t1771_Int32_t372_WordData_t1139,
	RuntimeInvoker_WordResultData_t1138_Int32_t372,
	RuntimeInvoker_Void_t1771_WordResultData_t1138,
	RuntimeInvoker_Boolean_t384_WordResultData_t1138,
	RuntimeInvoker_Int32_t372_WordResultData_t1138,
	RuntimeInvoker_Void_t1771_Int32_t372_WordResultData_t1138,
	RuntimeInvoker_SmartTerrainRevisionData_t1142_Int32_t372,
	RuntimeInvoker_Void_t1771_SmartTerrainRevisionData_t1142,
	RuntimeInvoker_Boolean_t384_SmartTerrainRevisionData_t1142,
	RuntimeInvoker_Int32_t372_SmartTerrainRevisionData_t1142,
	RuntimeInvoker_Void_t1771_Int32_t372_SmartTerrainRevisionData_t1142,
	RuntimeInvoker_SurfaceData_t1143_Int32_t372,
	RuntimeInvoker_Void_t1771_SurfaceData_t1143,
	RuntimeInvoker_Boolean_t384_SurfaceData_t1143,
	RuntimeInvoker_Int32_t372_SurfaceData_t1143,
	RuntimeInvoker_Void_t1771_Int32_t372_SurfaceData_t1143,
	RuntimeInvoker_PropData_t1144_Int32_t372,
	RuntimeInvoker_Void_t1771_PropData_t1144,
	RuntimeInvoker_Boolean_t384_PropData_t1144,
	RuntimeInvoker_Int32_t372_PropData_t1144,
	RuntimeInvoker_Void_t1771_Int32_t372_PropData_t1144,
	RuntimeInvoker_KeyValuePair_2_t3337_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t3337,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3337,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t3337,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3337,
	RuntimeInvoker_RectangleData_t1089_Int32_t372,
	RuntimeInvoker_Void_t1771_RectangleData_t1089,
	RuntimeInvoker_Int32_t372_RectangleData_t1089,
	RuntimeInvoker_Void_t1771_Int32_t372_RectangleData_t1089,
	RuntimeInvoker_KeyValuePair_2_t3425_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t3425,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3425,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t3425,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3425,
	RuntimeInvoker_KeyValuePair_2_t3440_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t3440,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3440,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t3440,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3440,
	RuntimeInvoker_VirtualButtonData_t1135_Int32_t372,
	RuntimeInvoker_Void_t1771_VirtualButtonData_t1135,
	RuntimeInvoker_Boolean_t384_VirtualButtonData_t1135,
	RuntimeInvoker_Int32_t372_VirtualButtonData_t1135,
	RuntimeInvoker_Void_t1771_Int32_t372_VirtualButtonData_t1135,
	RuntimeInvoker_TargetSearchResult_t1212_Int32_t372,
	RuntimeInvoker_Boolean_t384_TargetSearchResult_t1212,
	RuntimeInvoker_Int32_t372_TargetSearchResult_t1212,
	RuntimeInvoker_Void_t1771_Int32_t372_TargetSearchResult_t1212,
	RuntimeInvoker_Void_t1771_TargetSearchResultU5BU5DU26_t4189_Int32_t372,
	RuntimeInvoker_Void_t1771_TargetSearchResultU5BU5DU26_t4189_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_Object_t_TargetSearchResult_t1212_Int32_t372_Int32_t372,
	RuntimeInvoker_Int32_t372_TargetSearchResult_t1212_TargetSearchResult_t1212_Object_t,
	RuntimeInvoker_KeyValuePair_2_t3478_Int32_t372,
	RuntimeInvoker_Void_t1771_KeyValuePair_2_t3478,
	RuntimeInvoker_Boolean_t384_KeyValuePair_2_t3478,
	RuntimeInvoker_Int32_t372_KeyValuePair_2_t3478,
	RuntimeInvoker_Void_t1771_Int32_t372_KeyValuePair_2_t3478,
	RuntimeInvoker_ProfileData_t1226_Int32_t372,
	RuntimeInvoker_Void_t1771_ProfileData_t1226,
	RuntimeInvoker_Boolean_t384_ProfileData_t1226,
	RuntimeInvoker_Int32_t372_ProfileData_t1226,
	RuntimeInvoker_Void_t1771_Int32_t372_ProfileData_t1226,
	RuntimeInvoker_Link_t3526_Int32_t372,
	RuntimeInvoker_Void_t1771_Link_t3526,
	RuntimeInvoker_Boolean_t384_Link_t3526,
	RuntimeInvoker_Int32_t372_Link_t3526,
	RuntimeInvoker_Void_t1771_Int32_t372_Link_t3526,
	RuntimeInvoker_ClientCertificateType_t1535_Int32_t372,
	RuntimeInvoker_X509ChainStatus_t1662_Int32_t372,
	RuntimeInvoker_Void_t1771_X509ChainStatus_t1662,
	RuntimeInvoker_Boolean_t384_X509ChainStatus_t1662,
	RuntimeInvoker_Int32_t372_X509ChainStatus_t1662,
	RuntimeInvoker_Void_t1771_Int32_t372_X509ChainStatus_t1662,
	RuntimeInvoker_Int32_t372_Object_t_Int32_t372_Int32_t372_Int32_t372_Object_t,
	RuntimeInvoker_Mark_t1704_Int32_t372,
	RuntimeInvoker_Void_t1771_Mark_t1704,
	RuntimeInvoker_Boolean_t384_Mark_t1704,
	RuntimeInvoker_Int32_t372_Mark_t1704,
	RuntimeInvoker_Void_t1771_Int32_t372_Mark_t1704,
	RuntimeInvoker_UriScheme_t1740_Int32_t372,
	RuntimeInvoker_Void_t1771_UriScheme_t1740,
	RuntimeInvoker_Boolean_t384_UriScheme_t1740,
	RuntimeInvoker_Int32_t372_UriScheme_t1740,
	RuntimeInvoker_Void_t1771_Int32_t372_UriScheme_t1740,
	RuntimeInvoker_TableRange_t1802_Int32_t372,
	RuntimeInvoker_Void_t1771_TableRange_t1802,
	RuntimeInvoker_Boolean_t384_TableRange_t1802,
	RuntimeInvoker_Int32_t372_TableRange_t1802,
	RuntimeInvoker_Void_t1771_Int32_t372_TableRange_t1802,
	RuntimeInvoker_Slot_t1879_Int32_t372,
	RuntimeInvoker_Void_t1771_Slot_t1879,
	RuntimeInvoker_Boolean_t384_Slot_t1879,
	RuntimeInvoker_Int32_t372_Slot_t1879,
	RuntimeInvoker_Void_t1771_Int32_t372_Slot_t1879,
	RuntimeInvoker_Slot_t1888_Int32_t372,
	RuntimeInvoker_Void_t1771_Slot_t1888,
	RuntimeInvoker_Boolean_t384_Slot_t1888,
	RuntimeInvoker_Int32_t372_Slot_t1888,
	RuntimeInvoker_Void_t1771_Int32_t372_Slot_t1888,
	RuntimeInvoker_ILTokenInfo_t1964_Int32_t372,
	RuntimeInvoker_Void_t1771_ILTokenInfo_t1964,
	RuntimeInvoker_Boolean_t384_ILTokenInfo_t1964,
	RuntimeInvoker_Int32_t372_ILTokenInfo_t1964,
	RuntimeInvoker_Void_t1771_Int32_t372_ILTokenInfo_t1964,
	RuntimeInvoker_LabelData_t1966_Int32_t372,
	RuntimeInvoker_Void_t1771_LabelData_t1966,
	RuntimeInvoker_Boolean_t384_LabelData_t1966,
	RuntimeInvoker_Int32_t372_LabelData_t1966,
	RuntimeInvoker_Void_t1771_Int32_t372_LabelData_t1966,
	RuntimeInvoker_LabelFixup_t1965_Int32_t372,
	RuntimeInvoker_Void_t1771_LabelFixup_t1965,
	RuntimeInvoker_Boolean_t384_LabelFixup_t1965,
	RuntimeInvoker_Int32_t372_LabelFixup_t1965,
	RuntimeInvoker_Void_t1771_Int32_t372_LabelFixup_t1965,
	RuntimeInvoker_Void_t1771_Int32_t372_DateTime_t74,
	RuntimeInvoker_Void_t1771_Decimal_t395,
	RuntimeInvoker_Void_t1771_Int32_t372_Decimal_t395,
	RuntimeInvoker_TimeSpan_t427_Int32_t372,
	RuntimeInvoker_Void_t1771_Int32_t372_TimeSpan_t427,
	RuntimeInvoker_TypeTag_t2163_Int32_t372,
	RuntimeInvoker_Boolean_t384_Byte_t391,
	RuntimeInvoker_Int32_t372_Byte_t391,
	RuntimeInvoker_Void_t1771_Int32_t372_Byte_t391,
	RuntimeInvoker_Link_t1869,
	RuntimeInvoker_DictionaryEntry_t451_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2507_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2546_Object_t_Int32_t372,
	RuntimeInvoker_Enumerator_t2550,
	RuntimeInvoker_DictionaryEntry_t451_Object_t_Int32_t372,
	RuntimeInvoker_KeyValuePair_2_t2546,
	RuntimeInvoker_Enumerator_t2549,
	RuntimeInvoker_Enumerator_t2553,
	RuntimeInvoker_KeyValuePair_2_t2546_Object_t,
	RuntimeInvoker_BoneWeight_t405,
	RuntimeInvoker_KeyValuePair_2_t2667_Int32_t372_Object_t,
	RuntimeInvoker_EScreens_t188_Int32_t372_Object_t,
	RuntimeInvoker_Boolean_t384_Int32_t372_ObjectU26_t4138,
	RuntimeInvoker_EScreens_t188_Object_t,
	RuntimeInvoker_Enumerator_t2672,
	RuntimeInvoker_DictionaryEntry_t451_Int32_t372_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2667,
	RuntimeInvoker_EScreens_t188,
	RuntimeInvoker_Enumerator_t2671,
	RuntimeInvoker_Enumerator_t2675,
	RuntimeInvoker_KeyValuePair_2_t2667_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2690_Int32_t372_Object_t,
	RuntimeInvoker_ESubScreens_t189_Int32_t372_Object_t,
	RuntimeInvoker_ESubScreens_t189_Object_t,
	RuntimeInvoker_Enumerator_t2695,
	RuntimeInvoker_KeyValuePair_2_t2690,
	RuntimeInvoker_ESubScreens_t189,
	RuntimeInvoker_Enumerator_t2694,
	RuntimeInvoker_Enumerator_t2698,
	RuntimeInvoker_KeyValuePair_2_t2690_Object_t,
	RuntimeInvoker_KeyValuePair_2_t352_Int32_t372_Int32_t372,
	RuntimeInvoker_ERaffleResult_t207_Int32_t372_Int32_t372,
	RuntimeInvoker_Boolean_t384_Int32_t372_ERaffleResultU26_t4190,
	RuntimeInvoker_DictionaryEntry_t451_Int32_t372_Int32_t372,
	RuntimeInvoker_Enumerator_t2714,
	RuntimeInvoker_Enumerator_t2717,
	RuntimeInvoker_KeyValuePair_2_t352_Object_t,
	RuntimeInvoker_FocusMode_t438_Object_t,
	RuntimeInvoker_Enumerator_t2725,
	RuntimeInvoker_FocusMode_t438,
	RuntimeInvoker_Object_t_KeyValuePair_2_t352_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2740_Object_t_SByte_t390,
	RuntimeInvoker_Boolean_t384_Object_t_BooleanU26_t4079,
	RuntimeInvoker_Enumerator_t2745,
	RuntimeInvoker_DictionaryEntry_t451_Object_t_SByte_t390,
	RuntimeInvoker_KeyValuePair_2_t2740,
	RuntimeInvoker_Enumerator_t2744,
	RuntimeInvoker_Enumerator_t2748,
	RuntimeInvoker_KeyValuePair_2_t2740_Object_t,
	RuntimeInvoker_AnimatorFrame_t235_Object_t,
	RuntimeInvoker_Boolean_t384_AnimatorFrame_t235_AnimatorFrame_t235,
	RuntimeInvoker_Object_t_AnimatorFrame_t235_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_AnimatorFrame_t235_AnimatorFrame_t235,
	RuntimeInvoker_Object_t_AnimatorFrame_t235_AnimatorFrame_t235_Object_t_Object_t,
	RuntimeInvoker_Boolean_t384_Nullable_1_t445,
	RuntimeInvoker_Object_t_RaycastResult_t512_RaycastResult_t512_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2833,
	RuntimeInvoker_Boolean_t384_RaycastResult_t512_RaycastResult_t512,
	RuntimeInvoker_Object_t_RaycastResult_t512_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2871_Int32_t372_Object_t,
	RuntimeInvoker_Enumerator_t2874,
	RuntimeInvoker_KeyValuePair_2_t2871_Object_t,
	RuntimeInvoker_RaycastHit2D_t729,
	RuntimeInvoker_Object_t_RaycastHit_t60_RaycastHit_t60_Object_t_Object_t,
	RuntimeInvoker_RaycastHit_t60,
	RuntimeInvoker_Object_t_Color_t9_Object_t_Object_t,
	RuntimeInvoker_Object_t_Single_t388_Object_t_Object_t,
	RuntimeInvoker_Object_t_FloatTween_t539,
	RuntimeInvoker_Object_t_ColorTween_t536,
	RuntimeInvoker_UIVertex_t605_Object_t,
	RuntimeInvoker_Enumerator_t2940,
	RuntimeInvoker_UIVertex_t605,
	RuntimeInvoker_Boolean_t384_UIVertex_t605_UIVertex_t605,
	RuntimeInvoker_Object_t_UIVertex_t605_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_UIVertex_t605_UIVertex_t605,
	RuntimeInvoker_Object_t_UIVertex_t605_UIVertex_t605_Object_t_Object_t,
	RuntimeInvoker_UILineInfo_t752,
	RuntimeInvoker_UICharInfo_t754,
	RuntimeInvoker_Object_t_Vector2_t2_Object_t_Object_t,
	RuntimeInvoker_Vector3_t36_Object_t,
	RuntimeInvoker_Enumerator_t3028,
	RuntimeInvoker_Object_t_Vector3_t36_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_Vector3_t36_Vector3_t36,
	RuntimeInvoker_Object_t_Vector3_t36_Vector3_t36_Object_t_Object_t,
	RuntimeInvoker_Color32_t711_Object_t,
	RuntimeInvoker_Enumerator_t3039,
	RuntimeInvoker_Color32_t711,
	RuntimeInvoker_Boolean_t384_Color32_t711_Color32_t711,
	RuntimeInvoker_Object_t_Color32_t711_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_Color32_t711_Color32_t711,
	RuntimeInvoker_Object_t_Color32_t711_Color32_t711_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t3049,
	RuntimeInvoker_Int32_t372_Vector2_t2_Vector2_t2,
	RuntimeInvoker_Object_t_Vector2_t2_Vector2_t2_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t3060,
	RuntimeInvoker_Object_t_Vector4_t680_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_Vector4_t680_Vector4_t680,
	RuntimeInvoker_Object_t_Vector4_t680_Vector4_t680_Object_t_Object_t,
	RuntimeInvoker_GcAchievementData_t939,
	RuntimeInvoker_GcScoreData_t940,
	RuntimeInvoker_ContactPoint_t863,
	RuntimeInvoker_ContactPoint2D_t869,
	RuntimeInvoker_WebCamDevice_t876,
	RuntimeInvoker_Keyframe_t883,
	RuntimeInvoker_CharacterInfo_t892,
	RuntimeInvoker_UICharInfo_t754_Object_t,
	RuntimeInvoker_Enumerator_t3132,
	RuntimeInvoker_Boolean_t384_UICharInfo_t754_UICharInfo_t754,
	RuntimeInvoker_Object_t_UICharInfo_t754_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_UICharInfo_t754_UICharInfo_t754,
	RuntimeInvoker_Object_t_UICharInfo_t754_UICharInfo_t754_Object_t_Object_t,
	RuntimeInvoker_UILineInfo_t752_Object_t,
	RuntimeInvoker_Enumerator_t3141,
	RuntimeInvoker_Boolean_t384_UILineInfo_t752_UILineInfo_t752,
	RuntimeInvoker_Object_t_UILineInfo_t752_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_UILineInfo_t752_UILineInfo_t752,
	RuntimeInvoker_Object_t_UILineInfo_t752_UILineInfo_t752_Object_t_Object_t,
	RuntimeInvoker_ParameterModifier_t2024,
	RuntimeInvoker_HitInfo_t959,
	RuntimeInvoker_TextEditOp_t976_Object_t,
	RuntimeInvoker_KeyValuePair_2_t3183_Object_t_Int32_t372,
	RuntimeInvoker_TextEditOp_t976_Object_t_Int32_t372,
	RuntimeInvoker_Boolean_t384_Object_t_TextEditOpU26_t4191,
	RuntimeInvoker_Enumerator_t3188,
	RuntimeInvoker_KeyValuePair_2_t3183,
	RuntimeInvoker_TextEditOp_t976,
	RuntimeInvoker_Enumerator_t3187,
	RuntimeInvoker_Enumerator_t3191,
	RuntimeInvoker_KeyValuePair_2_t3183_Object_t,
	RuntimeInvoker_EyewearCalibrationReading_t1080,
	RuntimeInvoker_KeyValuePair_2_t3257_Int32_t372_Object_t,
	RuntimeInvoker_PIXEL_FORMAT_t1108_Int32_t372_Object_t,
	RuntimeInvoker_PIXEL_FORMAT_t1108_Object_t,
	RuntimeInvoker_Enumerator_t3262,
	RuntimeInvoker_KeyValuePair_2_t3257,
	RuntimeInvoker_Enumerator_t3261,
	RuntimeInvoker_KeyValuePair_2_t3257_Object_t,
	RuntimeInvoker_Enumerator_t3274,
	RuntimeInvoker_TrackableResultData_t1134,
	RuntimeInvoker_WordData_t1139,
	RuntimeInvoker_WordResultData_t1138,
	RuntimeInvoker_Enumerator_t3318,
	RuntimeInvoker_Object_t_TrackableResultData_t1134_Object_t_Object_t,
	RuntimeInvoker_SmartTerrainRevisionData_t1142,
	RuntimeInvoker_SurfaceData_t1143,
	RuntimeInvoker_PropData_t1144,
	RuntimeInvoker_KeyValuePair_2_t3337_Object_t_Int16_t392,
	RuntimeInvoker_UInt16_t393_Object_t_Int16_t392,
	RuntimeInvoker_Enumerator_t3342,
	RuntimeInvoker_DictionaryEntry_t451_Object_t_Int16_t392,
	RuntimeInvoker_KeyValuePair_2_t3337,
	RuntimeInvoker_Enumerator_t3341,
	RuntimeInvoker_Object_t_Object_t_Int16_t392_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t3345,
	RuntimeInvoker_KeyValuePair_2_t3337_Object_t,
	RuntimeInvoker_Boolean_t384_Int16_t392_Int16_t392,
	RuntimeInvoker_Object_t_SmartTerrainInitializationInfo_t1083_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t3425_Int32_t372_TrackableResultData_t1134,
	RuntimeInvoker_Int32_t372_Int32_t372_TrackableResultData_t1134,
	RuntimeInvoker_TrackableResultData_t1134_Int32_t372_TrackableResultData_t1134,
	RuntimeInvoker_Boolean_t384_Int32_t372_TrackableResultDataU26_t4192,
	RuntimeInvoker_TrackableResultData_t1134_Object_t,
	RuntimeInvoker_Enumerator_t3429,
	RuntimeInvoker_DictionaryEntry_t451_Int32_t372_TrackableResultData_t1134,
	RuntimeInvoker_KeyValuePair_2_t3425,
	RuntimeInvoker_Enumerator_t3428,
	RuntimeInvoker_Object_t_Int32_t372_TrackableResultData_t1134_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t3432,
	RuntimeInvoker_KeyValuePair_2_t3425_Object_t,
	RuntimeInvoker_Boolean_t384_TrackableResultData_t1134_TrackableResultData_t1134,
	RuntimeInvoker_KeyValuePair_2_t3440_Int32_t372_VirtualButtonData_t1135,
	RuntimeInvoker_Int32_t372_Int32_t372_VirtualButtonData_t1135,
	RuntimeInvoker_VirtualButtonData_t1135_Int32_t372_VirtualButtonData_t1135,
	RuntimeInvoker_Boolean_t384_Int32_t372_VirtualButtonDataU26_t4193,
	RuntimeInvoker_VirtualButtonData_t1135_Object_t,
	RuntimeInvoker_Enumerator_t3445,
	RuntimeInvoker_DictionaryEntry_t451_Int32_t372_VirtualButtonData_t1135,
	RuntimeInvoker_KeyValuePair_2_t3440,
	RuntimeInvoker_VirtualButtonData_t1135,
	RuntimeInvoker_Enumerator_t3444,
	RuntimeInvoker_Object_t_Int32_t372_VirtualButtonData_t1135_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t3448,
	RuntimeInvoker_KeyValuePair_2_t3440_Object_t,
	RuntimeInvoker_Boolean_t384_VirtualButtonData_t1135_VirtualButtonData_t1135,
	RuntimeInvoker_TargetSearchResult_t1212_Object_t,
	RuntimeInvoker_Enumerator_t3460,
	RuntimeInvoker_TargetSearchResult_t1212,
	RuntimeInvoker_Boolean_t384_TargetSearchResult_t1212_TargetSearchResult_t1212,
	RuntimeInvoker_Object_t_TargetSearchResult_t1212_Object_t_Object_t,
	RuntimeInvoker_Int32_t372_TargetSearchResult_t1212_TargetSearchResult_t1212,
	RuntimeInvoker_Object_t_TargetSearchResult_t1212_TargetSearchResult_t1212_Object_t_Object_t,
	RuntimeInvoker_Void_t1771_Object_t_ProfileData_t1226,
	RuntimeInvoker_KeyValuePair_2_t3478_Object_t_ProfileData_t1226,
	RuntimeInvoker_Object_t_Object_t_ProfileData_t1226,
	RuntimeInvoker_ProfileData_t1226_Object_t_ProfileData_t1226,
	RuntimeInvoker_Boolean_t384_Object_t_ProfileDataU26_t4194,
	RuntimeInvoker_Enumerator_t3483,
	RuntimeInvoker_DictionaryEntry_t451_Object_t_ProfileData_t1226,
	RuntimeInvoker_KeyValuePair_2_t3478,
	RuntimeInvoker_Enumerator_t3482,
	RuntimeInvoker_Object_t_Object_t_ProfileData_t1226_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t3486,
	RuntimeInvoker_KeyValuePair_2_t3478_Object_t,
	RuntimeInvoker_Boolean_t384_ProfileData_t1226_ProfileData_t1226,
	RuntimeInvoker_Link_t3526,
	RuntimeInvoker_ClientCertificateType_t1535,
	RuntimeInvoker_X509ChainStatus_t1662,
	RuntimeInvoker_Mark_t1704,
	RuntimeInvoker_UriScheme_t1740,
	RuntimeInvoker_TableRange_t1802,
	RuntimeInvoker_Slot_t1879,
	RuntimeInvoker_Slot_t1888,
	RuntimeInvoker_ILTokenInfo_t1964,
	RuntimeInvoker_LabelData_t1966,
	RuntimeInvoker_LabelFixup_t1965,
	RuntimeInvoker_TypeTag_t2163,
	RuntimeInvoker_Int32_t372_DateTimeOffset_t2323_DateTimeOffset_t2323,
	RuntimeInvoker_Boolean_t384_DateTimeOffset_t2323_DateTimeOffset_t2323,
	RuntimeInvoker_Boolean_t384_Nullable_1_t2426,
	RuntimeInvoker_Int32_t372_Guid_t452_Guid_t452,
	RuntimeInvoker_Boolean_t384_Guid_t452_Guid_t452,
};
