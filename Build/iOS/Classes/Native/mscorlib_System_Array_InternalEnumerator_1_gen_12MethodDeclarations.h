﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Array/InternalEnumerator`1<InputLayerElement>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15344(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2531 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14611_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<InputLayerElement>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15345(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2531 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14612_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<InputLayerElement>::Dispose()
#define InternalEnumerator_1_Dispose_m15346(__this, method) (( void (*) (InternalEnumerator_1_t2531 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14613_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<InputLayerElement>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15347(__this, method) (( bool (*) (InternalEnumerator_1_t2531 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14614_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<InputLayerElement>::get_Current()
#define InternalEnumerator_1_get_Current_m15348(__this, method) (( InputLayerElement_t58 * (*) (InternalEnumerator_1_t2531 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14615_gshared)(__this, method)
