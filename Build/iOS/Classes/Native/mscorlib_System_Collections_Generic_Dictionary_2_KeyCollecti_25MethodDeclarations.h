﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2738;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_25.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17978_gshared (Enumerator_t2744 * __this, Dictionary_2_t2738 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m17978(__this, ___host, method) (( void (*) (Enumerator_t2744 *, Dictionary_2_t2738 *, const MethodInfo*))Enumerator__ctor_m17978_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17979_gshared (Enumerator_t2744 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17979(__this, method) (( Object_t * (*) (Enumerator_t2744 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17979_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m17980_gshared (Enumerator_t2744 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17980(__this, method) (( void (*) (Enumerator_t2744 *, const MethodInfo*))Enumerator_Dispose_m17980_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17981_gshared (Enumerator_t2744 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17981(__this, method) (( bool (*) (Enumerator_t2744 *, const MethodInfo*))Enumerator_MoveNext_m17981_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m17982_gshared (Enumerator_t2744 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17982(__this, method) (( Object_t * (*) (Enumerator_t2744 *, const MethodInfo*))Enumerator_get_Current_m17982_gshared)(__this, method)
