﻿#pragma once
#include <stdint.h>
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t1085;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.SmartTerrainTrackable>
struct  Predicate_1_t3326  : public MulticastDelegate_t28
{
};
