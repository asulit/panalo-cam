﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>::.ctor(System.Object,System.IntPtr)
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_16MethodDeclarations.h"
#define EventFunction_1__ctor_m3294(__this, ___object, ___method, method) (( void (*) (EventFunction_1_t498 *, Object_t *, IntPtr_t, const MethodInfo*))EventFunction_1__ctor_m18855_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
#define EventFunction_1_Invoke_m19507(__this, ___handler, ___eventData, method) (( void (*) (EventFunction_1_t498 *, Object_t *, BaseEventData_t480 *, const MethodInfo*))EventFunction_1_Invoke_m18857_gshared)(__this, ___handler, ___eventData, method)
// System.IAsyncResult UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>::BeginInvoke(T1,UnityEngine.EventSystems.BaseEventData,System.AsyncCallback,System.Object)
#define EventFunction_1_BeginInvoke_m19508(__this, ___handler, ___eventData, ___callback, ___object, method) (( Object_t * (*) (EventFunction_1_t498 *, Object_t *, BaseEventData_t480 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))EventFunction_1_BeginInvoke_m18859_gshared)(__this, ___handler, ___eventData, ___callback, ___object, method)
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>::EndInvoke(System.IAsyncResult)
#define EventFunction_1_EndInvoke_m19509(__this, ___result, method) (( void (*) (EventFunction_1_t498 *, Object_t *, const MethodInfo*))EventFunction_1_EndInvoke_m18861_gshared)(__this, ___result, method)
