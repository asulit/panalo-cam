﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.Contexts.Context
struct Context_t2074;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Messaging.ClientContextTerminatorSink
struct  ClientContextTerminatorSink_t2101  : public Object_t
{
	// System.Runtime.Remoting.Contexts.Context System.Runtime.Remoting.Messaging.ClientContextTerminatorSink::_context
	Context_t2074 * ____context_0;
};
