﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.Action.ScaleAction
struct ScaleAction_t42;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t35;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Common.Fsm.Action.ScaleAction::.ctor(Common.Fsm.FsmState,System.String)
extern "C" void ScaleAction__ctor_m135 (ScaleAction_t42 * __this, Object_t * ___owner, String_t* ___timeReference, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.ScaleAction::Init(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.String)
extern "C" void ScaleAction_Init_m136 (ScaleAction_t42 * __this, Transform_t35 * ___transform, Vector3_t36  ___scaleFrom, Vector3_t36  ___scaleTo, float ___duration, String_t* ___finishEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.ScaleAction::OnEnter()
extern "C" void ScaleAction_OnEnter_m137 (ScaleAction_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.ScaleAction::OnUpdate()
extern "C" void ScaleAction_OnUpdate_m138 (ScaleAction_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.ScaleAction::Finish()
extern "C" void ScaleAction_Finish_m139 (ScaleAction_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
