﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionFromTargetImpl
struct ReconstructionFromTargetImpl_t1076;
// Vuforia.CylinderTarget
struct CylinderTarget_t1086;
// Vuforia.ImageTarget
struct ImageTarget_t1225;
// Vuforia.MultiTarget
struct MultiTarget_t1232;
// Vuforia.Trackable
struct Trackable_t1060;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void Vuforia.ReconstructionFromTargetImpl::.ctor(System.IntPtr)
extern "C" void ReconstructionFromTargetImpl__ctor_m5445 (ReconstructionFromTargetImpl_t1076 * __this, IntPtr_t ___nativeReconstructionPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m5446 (ReconstructionFromTargetImpl_t1076 * __this, Object_t * ___cylinderTarget, Vector3_t36  ___occluderMin, Vector3_t36  ___occluderMax, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m5447 (ReconstructionFromTargetImpl_t1076 * __this, Object_t * ___cylinderTarget, Vector3_t36  ___occluderMin, Vector3_t36  ___occluderMax, Vector3_t36  ___offsetToOccluderOrigin, Quaternion_t41  ___rotationToOccluderOrigin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.ImageTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m5448 (ReconstructionFromTargetImpl_t1076 * __this, Object_t * ___imageTarget, Vector3_t36  ___occluderMin, Vector3_t36  ___occluderMax, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.ImageTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m5449 (ReconstructionFromTargetImpl_t1076 * __this, Object_t * ___imageTarget, Vector3_t36  ___occluderMin, Vector3_t36  ___occluderMax, Vector3_t36  ___offsetToOccluderOrigin, Quaternion_t41  ___rotationToOccluderOrigin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.MultiTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m5450 (ReconstructionFromTargetImpl_t1076 * __this, Object_t * ___multiTarget, Vector3_t36  ___occluderMin, Vector3_t36  ___occluderMax, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.MultiTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m5451 (ReconstructionFromTargetImpl_t1076 * __this, Object_t * ___multiTarget, Vector3_t36  ___occluderMin, Vector3_t36  ___occluderMax, Vector3_t36  ___offsetToOccluderOrigin, Quaternion_t41  ___rotationToOccluderOrigin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" Object_t * ReconstructionFromTargetImpl_GetInitializationTarget_m5452 (ReconstructionFromTargetImpl_t1076 * __this, Vector3_t36 * ___occluderMin, Vector3_t36 * ___occluderMax, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C" Object_t * ReconstructionFromTargetImpl_GetInitializationTarget_m5453 (ReconstructionFromTargetImpl_t1076 * __this, Vector3_t36 * ___occluderMin, Vector3_t36 * ___occluderMax, Vector3_t36 * ___offsetToOccluderOrigin, Quaternion_t41 * ___rotationToOccluderOrigin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::Reset()
extern "C" bool ReconstructionFromTargetImpl_Reset_m5454 (ReconstructionFromTargetImpl_t1076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::Start()
extern "C" bool ReconstructionFromTargetImpl_Start_m5455 (ReconstructionFromTargetImpl_t1076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(System.IntPtr,Vuforia.Trackable,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m5456 (ReconstructionFromTargetImpl_t1076 * __this, IntPtr_t ___datasetPtr, Object_t * ___trackable, Vector3_t36  ___occluderMin, Vector3_t36  ___occluderMax, Vector3_t36  ___offsetToOccluderOrigin, Quaternion_t41  ___rotationToOccluderOrigin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionFromTargetImpl::get_CanAutoSetInitializationTarget()
extern "C" bool ReconstructionFromTargetImpl_get_CanAutoSetInitializationTarget_m5457 (ReconstructionFromTargetImpl_t1076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetImpl::set_CanAutoSetInitializationTarget(System.Boolean)
extern "C" void ReconstructionFromTargetImpl_set_CanAutoSetInitializationTarget_m5458 (ReconstructionFromTargetImpl_t1076 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
