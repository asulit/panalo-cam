﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// FrameRate
struct  FrameRate_t24  : public Object_t
{
	// System.Int32 FrameRate::frameRate
	int32_t ___frameRate_0;
	// System.Int32 FrameRate::numFrames
	int32_t ___numFrames_1;
	// System.Single FrameRate::polledTime
	float ___polledTime_2;
};
