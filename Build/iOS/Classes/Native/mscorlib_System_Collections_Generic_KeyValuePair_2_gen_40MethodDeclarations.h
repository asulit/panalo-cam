﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22MethodDeclarations.h"
#define KeyValuePair_2__ctor_m26582(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3359 *, int32_t, WordResult_t1188 *, const MethodInfo*))KeyValuePair_2__ctor_m19811_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::get_Key()
#define KeyValuePair_2_get_Key_m26583(__this, method) (( int32_t (*) (KeyValuePair_2_t3359 *, const MethodInfo*))KeyValuePair_2_get_Key_m19812_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m26584(__this, ___value, method) (( void (*) (KeyValuePair_2_t3359 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m19813_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::get_Value()
#define KeyValuePair_2_get_Value_m26585(__this, method) (( WordResult_t1188 * (*) (KeyValuePair_2_t3359 *, const MethodInfo*))KeyValuePair_2_get_Value_m19814_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m26586(__this, ___value, method) (( void (*) (KeyValuePair_2_t3359 *, WordResult_t1188 *, const MethodInfo*))KeyValuePair_2_set_Value_m19815_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::ToString()
#define KeyValuePair_2_ToString_m26587(__this, method) (( String_t* (*) (KeyValuePair_2_t3359 *, const MethodInfo*))KeyValuePair_2_ToString_m19816_gshared)(__this, method)
