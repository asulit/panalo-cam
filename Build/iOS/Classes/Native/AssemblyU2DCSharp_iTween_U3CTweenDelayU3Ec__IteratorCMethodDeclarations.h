﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// iTween/<TweenDelay>c__IteratorC
struct U3CTweenDelayU3Ec__IteratorC_t257;
// System.Object
struct Object_t;

// System.Void iTween/<TweenDelay>c__IteratorC::.ctor()
extern "C" void U3CTweenDelayU3Ec__IteratorC__ctor_m905 (U3CTweenDelayU3Ec__IteratorC_t257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenDelay>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CTweenDelayU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m906 (U3CTweenDelayU3Ec__IteratorC_t257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenDelay>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CTweenDelayU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m907 (U3CTweenDelayU3Ec__IteratorC_t257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<TweenDelay>c__IteratorC::MoveNext()
extern "C" bool U3CTweenDelayU3Ec__IteratorC_MoveNext_m908 (U3CTweenDelayU3Ec__IteratorC_t257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenDelay>c__IteratorC::Dispose()
extern "C" void U3CTweenDelayU3Ec__IteratorC_Dispose_m909 (U3CTweenDelayU3Ec__IteratorC_t257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenDelay>c__IteratorC::Reset()
extern "C" void U3CTweenDelayU3Ec__IteratorC_Reset_m910 (U3CTweenDelayU3Ec__IteratorC_t257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
