﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>::.ctor(System.Object,System.IntPtr)
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_16MethodDeclarations.h"
#define EventFunction_1__ctor_m3293(__this, ___object, ___method, method) (( void (*) (EventFunction_1_t497 *, Object_t *, IntPtr_t, const MethodInfo*))EventFunction_1__ctor_m18855_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
#define EventFunction_1_Invoke_m19504(__this, ___handler, ___eventData, method) (( void (*) (EventFunction_1_t497 *, Object_t *, BaseEventData_t480 *, const MethodInfo*))EventFunction_1_Invoke_m18857_gshared)(__this, ___handler, ___eventData, method)
// System.IAsyncResult UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>::BeginInvoke(T1,UnityEngine.EventSystems.BaseEventData,System.AsyncCallback,System.Object)
#define EventFunction_1_BeginInvoke_m19505(__this, ___handler, ___eventData, ___callback, ___object, method) (( Object_t * (*) (EventFunction_1_t497 *, Object_t *, BaseEventData_t480 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))EventFunction_1_BeginInvoke_m18859_gshared)(__this, ___handler, ___eventData, ___callback, ___object, method)
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>::EndInvoke(System.IAsyncResult)
#define EventFunction_1_EndInvoke_m19506(__this, ___result, method) (( void (*) (EventFunction_1_t497 *, Object_t *, const MethodInfo*))EventFunction_1_EndInvoke_m18861_gshared)(__this, ___result, method)
