﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t196;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Text>
struct  Predicate_1_t2931  : public MulticastDelegate_t28
{
};
