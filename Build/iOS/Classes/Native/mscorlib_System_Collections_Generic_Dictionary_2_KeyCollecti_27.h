﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,System.String>
struct Dictionary_2_t228;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>
struct  KeyCollection_t2761  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::dictionary
	Dictionary_2_t228 * ___dictionary_0;
};
