﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PrefabManager/<PruneItems>c__Iterator2
struct U3CPruneItemsU3Ec__Iterator2_t97;
// System.Object
struct Object_t;

// System.Void PrefabManager/<PruneItems>c__Iterator2::.ctor()
extern "C" void U3CPruneItemsU3Ec__Iterator2__ctor_m334 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PrefabManager/<PruneItems>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CPruneItemsU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m335 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PrefabManager/<PruneItems>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CPruneItemsU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m336 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PrefabManager/<PruneItems>c__Iterator2::MoveNext()
extern "C" bool U3CPruneItemsU3Ec__Iterator2_MoveNext_m337 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager/<PruneItems>c__Iterator2::Dispose()
extern "C" void U3CPruneItemsU3Ec__Iterator2_Dispose_m338 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager/<PruneItems>c__Iterator2::Reset()
extern "C" void U3CPruneItemsU3Ec__Iterator2_Reset_m339 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
