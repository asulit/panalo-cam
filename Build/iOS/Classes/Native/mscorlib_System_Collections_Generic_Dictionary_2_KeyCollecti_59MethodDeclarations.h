﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct KeyCollection_t3443;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1372;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3626;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Int32[]
struct Int32U5BU5D_t401;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_60.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void KeyCollection__ctor_m28231_gshared (KeyCollection_t3443 * __this, Dictionary_2_t1372 * ___dictionary, const MethodInfo* method);
#define KeyCollection__ctor_m28231(__this, ___dictionary, method) (( void (*) (KeyCollection_t3443 *, Dictionary_2_t1372 *, const MethodInfo*))KeyCollection__ctor_m28231_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m28232_gshared (KeyCollection_t3443 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m28232(__this, ___item, method) (( void (*) (KeyCollection_t3443 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m28232_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m28233_gshared (KeyCollection_t3443 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m28233(__this, method) (( void (*) (KeyCollection_t3443 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m28233_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m28234_gshared (KeyCollection_t3443 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m28234(__this, ___item, method) (( bool (*) (KeyCollection_t3443 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m28234_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m28235_gshared (KeyCollection_t3443 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m28235(__this, ___item, method) (( bool (*) (KeyCollection_t3443 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m28235_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m28236_gshared (KeyCollection_t3443 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m28236(__this, method) (( Object_t* (*) (KeyCollection_t3443 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m28236_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m28237_gshared (KeyCollection_t3443 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m28237(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3443 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m28237_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m28238_gshared (KeyCollection_t3443 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m28238(__this, method) (( Object_t * (*) (KeyCollection_t3443 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m28238_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m28239_gshared (KeyCollection_t3443 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m28239(__this, method) (( bool (*) (KeyCollection_t3443 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m28239_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m28240_gshared (KeyCollection_t3443 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m28240(__this, method) (( bool (*) (KeyCollection_t3443 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m28240_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m28241_gshared (KeyCollection_t3443 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m28241(__this, method) (( Object_t * (*) (KeyCollection_t3443 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m28241_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m28242_gshared (KeyCollection_t3443 * __this, Int32U5BU5D_t401* ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_CopyTo_m28242(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3443 *, Int32U5BU5D_t401*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m28242_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::GetEnumerator()
extern "C" Enumerator_t3444  KeyCollection_GetEnumerator_m28243_gshared (KeyCollection_t3443 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m28243(__this, method) (( Enumerator_t3444  (*) (KeyCollection_t3443 *, const MethodInfo*))KeyCollection_GetEnumerator_m28243_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m28244_gshared (KeyCollection_t3443 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m28244(__this, method) (( int32_t (*) (KeyCollection_t3443 *, const MethodInfo*))KeyCollection_get_Count_m28244_gshared)(__this, method)
