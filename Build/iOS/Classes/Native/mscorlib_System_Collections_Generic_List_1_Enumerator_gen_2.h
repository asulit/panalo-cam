﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct List_1_t266;
// System.Collections.Hashtable
struct Hashtable_t261;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<System.Collections.Hashtable>
struct  Enumerator_t453 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<System.Collections.Hashtable>::l
	List_1_t266 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Collections.Hashtable>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Collections.Hashtable>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<System.Collections.Hashtable>::current
	Hashtable_t261 * ___current_3;
};
