﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// System.Predicate`1<UnityEngine.Color32>
struct  Predicate_1_t3045  : public MulticastDelegate_t28
{
};
