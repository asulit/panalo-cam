﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamBehaviour
struct WebCamBehaviour_t322;

// System.Void Vuforia.WebCamBehaviour::.ctor()
extern "C" void WebCamBehaviour__ctor_m1274 (WebCamBehaviour_t322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
