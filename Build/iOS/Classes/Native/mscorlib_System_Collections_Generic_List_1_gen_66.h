﻿#pragma once
#include <stdint.h>
// Vuforia.ISmartTerrainEventHandler[]
struct ISmartTerrainEventHandlerU5BU5D_t3390;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>
struct  List_1_t1198  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::_items
	ISmartTerrainEventHandlerU5BU5D_t3390* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t1198_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::EmptyArray
	ISmartTerrainEventHandlerU5BU5D_t3390* ___EmptyArray_4;
};
