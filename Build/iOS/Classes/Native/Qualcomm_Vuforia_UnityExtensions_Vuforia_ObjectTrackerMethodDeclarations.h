﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ObjectTracker
struct ObjectTracker_t1066;

// System.Void Vuforia.ObjectTracker::.ctor()
extern "C" void ObjectTracker__ctor_m5680 (ObjectTracker_t1066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
