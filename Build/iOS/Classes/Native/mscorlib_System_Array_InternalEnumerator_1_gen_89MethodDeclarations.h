﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_89.h"
// Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__8.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26075_gshared (InternalEnumerator_1_t3319 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26075(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3319 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26075_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26076_gshared (InternalEnumerator_1_t3319 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26076(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3319 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26076_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26077_gshared (InternalEnumerator_1_t3319 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26077(__this, method) (( void (*) (InternalEnumerator_1_t3319 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26077_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26078_gshared (InternalEnumerator_1_t3319 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26078(__this, method) (( bool (*) (InternalEnumerator_1_t3319 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26078_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::get_Current()
extern "C" SmartTerrainRevisionData_t1142  InternalEnumerator_1_get_Current_m26079_gshared (InternalEnumerator_1_t3319 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26079(__this, method) (( SmartTerrainRevisionData_t1142  (*) (InternalEnumerator_1_t3319 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26079_gshared)(__this, method)
