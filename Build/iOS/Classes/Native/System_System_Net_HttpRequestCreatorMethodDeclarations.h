﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.HttpRequestCreator
struct HttpRequestCreator_t1631;
// System.Net.WebRequest
struct WebRequest_t1588;
// System.Uri
struct Uri_t1583;

// System.Void System.Net.HttpRequestCreator::.ctor()
extern "C" void HttpRequestCreator__ctor_m8689 (HttpRequestCreator_t1631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.HttpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t1588 * HttpRequestCreator_Create_m8690 (HttpRequestCreator_t1631 * __this, Uri_t1583 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
