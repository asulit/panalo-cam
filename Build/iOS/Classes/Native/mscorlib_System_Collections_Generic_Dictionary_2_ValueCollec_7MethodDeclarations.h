﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_54MethodDeclarations.h"
#define Enumerator__ctor_m25655(__this, ___host, method) (( void (*) (Enumerator_t1301 *, Dictionary_2_t1114 *, const MethodInfo*))Enumerator__ctor_m19866_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25656(__this, method) (( Object_t * (*) (Enumerator_t1301 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19867_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::Dispose()
#define Enumerator_Dispose_m7257(__this, method) (( void (*) (Enumerator_t1301 *, const MethodInfo*))Enumerator_Dispose_m19868_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::MoveNext()
#define Enumerator_MoveNext_m7256(__this, method) (( bool (*) (Enumerator_t1301 *, const MethodInfo*))Enumerator_MoveNext_m19869_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>::get_Current()
#define Enumerator_get_Current_m7255(__this, method) (( VirtualButton_t1223 * (*) (Enumerator_t1301 *, const MethodInfo*))Enumerator_get_Current_m19870_gshared)(__this, method)
