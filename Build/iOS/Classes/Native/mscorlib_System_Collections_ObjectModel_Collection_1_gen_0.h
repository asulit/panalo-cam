﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.CameraDevice/FocusMode>
struct IList_1_t2727;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>
struct  Collection_1_t2728  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::syncRoot
	Object_t * ___syncRoot_1;
};
