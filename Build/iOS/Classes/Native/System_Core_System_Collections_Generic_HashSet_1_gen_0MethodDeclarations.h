﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3524;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2506;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"

// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C" void HashSet_1__ctor_m29348_gshared (HashSet_1_t3524 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m29348(__this, method) (( void (*) (HashSet_1_t3524 *, const MethodInfo*))HashSet_1__ctor_m29348_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1__ctor_m29350_gshared (HashSet_1_t3524 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define HashSet_1__ctor_m29350(__this, ___info, ___context, method) (( void (*) (HashSet_1_t3524 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))HashSet_1__ctor_m29350_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29352_gshared (HashSet_1_t3524 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29352(__this, method) (( Object_t* (*) (HashSet_1_t3524 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29352_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29354_gshared (HashSet_1_t3524 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29354(__this, method) (( bool (*) (HashSet_1_t3524 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29354_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m29356_gshared (HashSet_1_t3524 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m29356(__this, ___array, ___index, method) (( void (*) (HashSet_1_t3524 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m29356_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29358_gshared (HashSet_1_t3524 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29358(__this, ___item, method) (( void (*) (HashSet_1_t3524 *, Object_t *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29358_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m29360_gshared (HashSet_1_t3524 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m29360(__this, method) (( Object_t * (*) (HashSet_1_t3524 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m29360_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
extern "C" int32_t HashSet_1_get_Count_m29362_gshared (HashSet_1_t3524 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m29362(__this, method) (( int32_t (*) (HashSet_1_t3524 *, const MethodInfo*))HashSet_1_get_Count_m29362_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C" void HashSet_1_Init_m29364_gshared (HashSet_1_t3524 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define HashSet_1_Init_m29364(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t3524 *, int32_t, Object_t*, const MethodInfo*))HashSet_1_Init_m29364_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::InitArrays(System.Int32)
extern "C" void HashSet_1_InitArrays_m29366_gshared (HashSet_1_t3524 * __this, int32_t ___size, const MethodInfo* method);
#define HashSet_1_InitArrays_m29366(__this, ___size, method) (( void (*) (HashSet_1_t3524 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m29366_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C" bool HashSet_1_SlotsContainsAt_m29368_gshared (HashSet_1_t3524 * __this, int32_t ___index, int32_t ___hash, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m29368(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t3524 *, int32_t, int32_t, Object_t *, const MethodInfo*))HashSet_1_SlotsContainsAt_m29368_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void HashSet_1_CopyTo_m29370_gshared (HashSet_1_t3524 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_CopyTo_m29370(__this, ___array, ___index, method) (( void (*) (HashSet_1_t3524 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m29370_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32,System.Int32)
extern "C" void HashSet_1_CopyTo_m29372_gshared (HashSet_1_t3524 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define HashSet_1_CopyTo_m29372(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t3524 *, ObjectU5BU5D_t356*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m29372_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Resize()
extern "C" void HashSet_1_Resize_m29374_gshared (HashSet_1_t3524 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m29374(__this, method) (( void (*) (HashSet_1_t3524 *, const MethodInfo*))HashSet_1_Resize_m29374_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetLinkHashCode(System.Int32)
extern "C" int32_t HashSet_1_GetLinkHashCode_m29376_gshared (HashSet_1_t3524 * __this, int32_t ___index, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m29376(__this, ___index, method) (( int32_t (*) (HashSet_1_t3524 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m29376_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetItemHashCode(T)
extern "C" int32_t HashSet_1_GetItemHashCode_m29378_gshared (HashSet_1_t3524 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m29378(__this, ___item, method) (( int32_t (*) (HashSet_1_t3524 *, Object_t *, const MethodInfo*))HashSet_1_GetItemHashCode_m29378_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
extern "C" bool HashSet_1_Add_m29379_gshared (HashSet_1_t3524 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Add_m29379(__this, ___item, method) (( bool (*) (HashSet_1_t3524 *, Object_t *, const MethodInfo*))HashSet_1_Add_m29379_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
extern "C" void HashSet_1_Clear_m29381_gshared (HashSet_1_t3524 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m29381(__this, method) (( void (*) (HashSet_1_t3524 *, const MethodInfo*))HashSet_1_Clear_m29381_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(T)
extern "C" bool HashSet_1_Contains_m29383_gshared (HashSet_1_t3524 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Contains_m29383(__this, ___item, method) (( bool (*) (HashSet_1_t3524 *, Object_t *, const MethodInfo*))HashSet_1_Contains_m29383_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(T)
extern "C" bool HashSet_1_Remove_m29385_gshared (HashSet_1_t3524 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Remove_m29385(__this, ___item, method) (( bool (*) (HashSet_1_t3524 *, Object_t *, const MethodInfo*))HashSet_1_Remove_m29385_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1_GetObjectData_m29387_gshared (HashSet_1_t3524 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define HashSet_1_GetObjectData_m29387(__this, ___info, ___context, method) (( void (*) (HashSet_1_t3524 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))HashSet_1_GetObjectData_m29387_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::OnDeserialization(System.Object)
extern "C" void HashSet_1_OnDeserialization_m29389_gshared (HashSet_1_t3524 * __this, Object_t * ___sender, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m29389(__this, ___sender, method) (( void (*) (HashSet_1_t3524 *, Object_t *, const MethodInfo*))HashSet_1_OnDeserialization_m29389_gshared)(__this, ___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3528  HashSet_1_GetEnumerator_m29390_gshared (HashSet_1_t3524 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m29390(__this, method) (( Enumerator_t3528  (*) (HashSet_1_t3524 *, const MethodInfo*))HashSet_1_GetEnumerator_m29390_gshared)(__this, method)
