﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_33MethodDeclarations.h"
#define KeyValuePair_2__ctor_m25194(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3271 *, int32_t, Image_t1109 *, const MethodInfo*))KeyValuePair_2__ctor_m25099_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Key()
#define KeyValuePair_2_get_Key_m25195(__this, method) (( int32_t (*) (KeyValuePair_2_t3271 *, const MethodInfo*))KeyValuePair_2_get_Key_m25100_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m25196(__this, ___value, method) (( void (*) (KeyValuePair_2_t3271 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m25101_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Value()
#define KeyValuePair_2_get_Value_m25197(__this, method) (( Image_t1109 * (*) (KeyValuePair_2_t3271 *, const MethodInfo*))KeyValuePair_2_get_Value_m25102_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m25198(__this, ___value, method) (( void (*) (KeyValuePair_2_t3271 *, Image_t1109 *, const MethodInfo*))KeyValuePair_2_set_Value_m25103_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ToString()
#define KeyValuePair_2_ToString_m25199(__this, method) (( String_t* (*) (KeyValuePair_2_t3271 *, const MethodInfo*))KeyValuePair_2_ToString_m25104_gshared)(__this, method)
