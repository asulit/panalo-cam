﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CameraDevice
struct CameraDevice_t437;

// Vuforia.CameraDevice Vuforia.CameraDevice::get_Instance()
extern "C" CameraDevice_t437 * CameraDevice_get_Instance_m1681 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CameraDevice::.ctor()
extern "C" void CameraDevice__ctor_m5370 (CameraDevice_t437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CameraDevice::.cctor()
extern "C" void CameraDevice__cctor_m5371 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
