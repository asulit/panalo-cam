﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// BridgeHandler
struct BridgeHandler_t185;
// System.Object
#include "mscorlib_System_Object.h"
// BridgeHandler/<OnStartAudioRecord>c__Iterator5
struct  U3COnStartAudioRecordU3Ec__Iterator5_t184  : public Object_t
{
	// System.Int32 BridgeHandler/<OnStartAudioRecord>c__Iterator5::$PC
	int32_t ___U24PC_0;
	// System.Object BridgeHandler/<OnStartAudioRecord>c__Iterator5::$current
	Object_t * ___U24current_1;
	// BridgeHandler BridgeHandler/<OnStartAudioRecord>c__Iterator5::<>f__this
	BridgeHandler_t185 * ___U3CU3Ef__this_2;
};
