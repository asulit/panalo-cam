﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Object
#include "mscorlib_System_Object.h"
// System.Array/ArrayReadOnlyList`1<System.Object>
struct  ArrayReadOnlyList_1_t3556  : public Object_t
{
	// T[] System.Array/ArrayReadOnlyList`1<System.Object>::array
	ObjectU5BU5D_t356* ___array_0;
};
