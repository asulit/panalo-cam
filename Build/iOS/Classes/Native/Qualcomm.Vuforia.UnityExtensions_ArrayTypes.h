﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Vuforia.CameraDevice/FocusMode[]
// Vuforia.CameraDevice/FocusMode[]
struct  FocusModeU5BU5D_t2723  : public Array_t
{
};
// Vuforia.Eyewear/EyewearCalibrationReading[]
// Vuforia.Eyewear/EyewearCalibrationReading[]
struct  EyewearCalibrationReadingU5BU5D_t1251  : public Array_t
{
};
// Vuforia.VideoBackgroundAbstractBehaviour[]
// Vuforia.VideoBackgroundAbstractBehaviour[]
struct  VideoBackgroundAbstractBehaviourU5BU5D_t3224  : public Array_t
{
};
// Vuforia.ITrackableEventHandler[]
// Vuforia.ITrackableEventHandler[]
struct  ITrackableEventHandlerU5BU5D_t3230  : public Array_t
{
};
// Vuforia.ICloudRecoEventHandler[]
// Vuforia.ICloudRecoEventHandler[]
struct  ICloudRecoEventHandlerU5BU5D_t3235  : public Array_t
{
};
// Vuforia.HideExcessAreaAbstractBehaviour[]
// Vuforia.HideExcessAreaAbstractBehaviour[]
struct  HideExcessAreaAbstractBehaviourU5BU5D_t1069  : public Array_t
{
};
// Vuforia.VirtualButton[]
// Vuforia.VirtualButton[]
struct  VirtualButtonU5BU5D_t3245  : public Array_t
{
};
// Vuforia.Image/PIXEL_FORMAT[]
// Vuforia.Image/PIXEL_FORMAT[]
struct  PIXEL_FORMATU5BU5D_t3251  : public Array_t
{
};
// Vuforia.Image[]
// Vuforia.Image[]
struct  ImageU5BU5D_t3252  : public Array_t
{
};
// Vuforia.Trackable[]
// Vuforia.Trackable[]
struct  TrackableU5BU5D_t3282  : public Array_t
{
};
// Vuforia.DataSetImpl[]
// Vuforia.DataSetImpl[]
struct  DataSetImplU5BU5D_t3295  : public Array_t
{
};
// Vuforia.DataSet[]
// Vuforia.DataSet[]
struct  DataSetU5BU5D_t3300  : public Array_t
{
};
// Vuforia.Marker[]
// Vuforia.Marker[]
struct  MarkerU5BU5D_t3306  : public Array_t
{
};
// Vuforia.VuforiaManagerImpl/TrackableResultData[]
// Vuforia.VuforiaManagerImpl/TrackableResultData[]
struct  TrackableResultDataU5BU5D_t1149  : public Array_t
{
};
// Vuforia.VuforiaManagerImpl/WordData[]
// Vuforia.VuforiaManagerImpl/WordData[]
#pragma pack(push, tp, 1)
struct  WordDataU5BU5D_t1150  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.VuforiaManagerImpl/WordResultData[]
// Vuforia.VuforiaManagerImpl/WordResultData[]
struct  WordResultDataU5BU5D_t1151  : public Array_t
{
};
// Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData[]
// Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData[]
#pragma pack(push, tp, 1)
struct  SmartTerrainRevisionDataU5BU5D_t1259  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.VuforiaManagerImpl/SurfaceData[]
// Vuforia.VuforiaManagerImpl/SurfaceData[]
#pragma pack(push, tp, 1)
struct  SurfaceDataU5BU5D_t1260  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.VuforiaManagerImpl/PropData[]
// Vuforia.VuforiaManagerImpl/PropData[]
#pragma pack(push, tp, 1)
struct  PropDataU5BU5D_t1261  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.TrackableBehaviour[]
// Vuforia.TrackableBehaviour[]
struct  TrackableBehaviourU5BU5D_t1313  : public Array_t
{
};
// Vuforia.IEditorTrackableBehaviour[]
// Vuforia.IEditorTrackableBehaviour[]
struct  IEditorTrackableBehaviourU5BU5D_t3871  : public Array_t
{
};
// Vuforia.SmartTerrainTrackable[]
// Vuforia.SmartTerrainTrackable[]
struct  SmartTerrainTrackableU5BU5D_t3323  : public Array_t
{
};
// Vuforia.ReconstructionAbstractBehaviour[]
// Vuforia.ReconstructionAbstractBehaviour[]
struct  ReconstructionAbstractBehaviourU5BU5D_t1318  : public Array_t
{
};
// Vuforia.SmartTerrainTrackableBehaviour[]
// Vuforia.SmartTerrainTrackableBehaviour[]
struct  SmartTerrainTrackableBehaviourU5BU5D_t1319  : public Array_t
{
};
// Vuforia.RectangleData[]
// Vuforia.RectangleData[]
#pragma pack(push, tp, 1)
struct  RectangleDataU5BU5D_t1178  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.WordResult[]
// Vuforia.WordResult[]
struct  WordResultU5BU5D_t3357  : public Array_t
{
};
// Vuforia.Word[]
// Vuforia.Word[]
struct  WordU5BU5D_t3365  : public Array_t
{
};
// Vuforia.WordAbstractBehaviour[]
// Vuforia.WordAbstractBehaviour[]
struct  WordAbstractBehaviourU5BU5D_t1325  : public Array_t
{
};
// Vuforia.ILoadLevelEventHandler[]
// Vuforia.ILoadLevelEventHandler[]
struct  ILoadLevelEventHandlerU5BU5D_t3380  : public Array_t
{
};
// Vuforia.ISmartTerrainEventHandler[]
// Vuforia.ISmartTerrainEventHandler[]
struct  ISmartTerrainEventHandlerU5BU5D_t3390  : public Array_t
{
};
// Vuforia.Surface[]
// Vuforia.Surface[]
struct  SurfaceU5BU5D_t3395  : public Array_t
{
};
// Vuforia.SurfaceAbstractBehaviour[]
// Vuforia.SurfaceAbstractBehaviour[]
struct  SurfaceAbstractBehaviourU5BU5D_t3397  : public Array_t
{
};
// Vuforia.Prop[]
// Vuforia.Prop[]
struct  PropU5BU5D_t3402  : public Array_t
{
};
// Vuforia.PropAbstractBehaviour[]
// Vuforia.PropAbstractBehaviour[]
struct  PropAbstractBehaviourU5BU5D_t1361  : public Array_t
{
};
// Vuforia.MarkerAbstractBehaviour[]
// Vuforia.MarkerAbstractBehaviour[]
struct  MarkerAbstractBehaviourU5BU5D_t1363  : public Array_t
{
};
// Vuforia.IEditorMarkerBehaviour[]
// Vuforia.IEditorMarkerBehaviour[]
struct  IEditorMarkerBehaviourU5BU5D_t3872  : public Array_t
{
};
// Vuforia.WorldCenterTrackableBehaviour[]
// Vuforia.WorldCenterTrackableBehaviour[]
struct  WorldCenterTrackableBehaviourU5BU5D_t3873  : public Array_t
{
};
// Vuforia.DataSetTrackableBehaviour[]
// Vuforia.DataSetTrackableBehaviour[]
struct  DataSetTrackableBehaviourU5BU5D_t1365  : public Array_t
{
};
// Vuforia.IEditorDataSetTrackableBehaviour[]
// Vuforia.IEditorDataSetTrackableBehaviour[]
struct  IEditorDataSetTrackableBehaviourU5BU5D_t3874  : public Array_t
{
};
// Vuforia.VirtualButtonAbstractBehaviour[]
// Vuforia.VirtualButtonAbstractBehaviour[]
struct  VirtualButtonAbstractBehaviourU5BU5D_t1274  : public Array_t
{
};
// Vuforia.IEditorVirtualButtonBehaviour[]
// Vuforia.IEditorVirtualButtonBehaviour[]
struct  IEditorVirtualButtonBehaviourU5BU5D_t3875  : public Array_t
{
};
// Vuforia.VuforiaManagerImpl/VirtualButtonData[]
// Vuforia.VuforiaManagerImpl/VirtualButtonData[]
#pragma pack(push, tp, 1)
struct  VirtualButtonDataU5BU5D_t3438  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.TargetFinder/TargetSearchResult[]
// Vuforia.TargetFinder/TargetSearchResult[]
struct  TargetSearchResultU5BU5D_t3458  : public Array_t
{
};
// Vuforia.ImageTarget[]
// Vuforia.ImageTarget[]
struct  ImageTargetU5BU5D_t1377  : public Array_t
{
};
// Vuforia.WebCamProfile/ProfileData[]
// Vuforia.WebCamProfile/ProfileData[]
struct  ProfileDataU5BU5D_t3474  : public Array_t
{
};
// Vuforia.ITrackerEventHandler[]
// Vuforia.ITrackerEventHandler[]
struct  ITrackerEventHandlerU5BU5D_t3500  : public Array_t
{
};
// Vuforia.IVideoBackgroundEventHandler[]
// Vuforia.IVideoBackgroundEventHandler[]
struct  IVideoBackgroundEventHandlerU5BU5D_t3505  : public Array_t
{
};
// Vuforia.BackgroundPlaneAbstractBehaviour[]
// Vuforia.BackgroundPlaneAbstractBehaviour[]
struct  BackgroundPlaneAbstractBehaviourU5BU5D_t1384  : public Array_t
{
};
struct BackgroundPlaneAbstractBehaviourU5BU5D_t1384_StaticFields{
};
// Vuforia.ITextRecoEventHandler[]
// Vuforia.ITextRecoEventHandler[]
struct  ITextRecoEventHandlerU5BU5D_t3510  : public Array_t
{
};
// Vuforia.IUserDefinedTargetEventHandler[]
// Vuforia.IUserDefinedTargetEventHandler[]
struct  IUserDefinedTargetEventHandlerU5BU5D_t3515  : public Array_t
{
};
// Vuforia.IVirtualButtonEventHandler[]
// Vuforia.IVirtualButtonEventHandler[]
struct  IVirtualButtonEventHandlerU5BU5D_t3530  : public Array_t
{
};
