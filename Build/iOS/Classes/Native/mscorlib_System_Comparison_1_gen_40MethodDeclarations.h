﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Comparison`1<UnityEngine.RectTransform>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_4MethodDeclarations.h"
#define Comparison_1__ctor_m22102(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3026 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m14828_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.RectTransform>::Invoke(T,T)
#define Comparison_1_Invoke_m22103(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3026 *, RectTransform_t556 *, RectTransform_t556 *, const MethodInfo*))Comparison_1_Invoke_m14829_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.RectTransform>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m22104(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3026 *, RectTransform_t556 *, RectTransform_t556 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m14830_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.RectTransform>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m22105(__this, ___result, method) (( int32_t (*) (Comparison_1_t3026 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m14831_gshared)(__this, ___result, method)
