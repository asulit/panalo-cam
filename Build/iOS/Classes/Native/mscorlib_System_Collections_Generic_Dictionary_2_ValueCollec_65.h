﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__33.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct  Enumerator_t3265 
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::host_enumerator
	Enumerator_t3262  ___host_enumerator_0;
};
