﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_10MethodDeclarations.h"
#define Action_1__ctor_m22959(__this, ___object, ___method, method) (( void (*) (Action_1_t797 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m16966_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>::Invoke(T)
#define Action_1_Invoke_m5108(__this, ___obj, method) (( void (*) (Action_1_t797 *, IAchievementDescriptionU5BU5D_t1017*, const MethodInfo*))Action_1_Invoke_m16967_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m22960(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t797 *, IAchievementDescriptionU5BU5D_t1017*, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m16969_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m22961(__this, ___result, method) (( void (*) (Action_1_t797 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m16971_gshared)(__this, ___result, method)
