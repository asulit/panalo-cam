﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ClipperRegistry
struct ClipperRegistry_t647;
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>
struct IndexedSet_1_t648;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.ClipperRegistry
struct  ClipperRegistry_t647  : public Object_t
{
	// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper> UnityEngine.UI.ClipperRegistry::m_Clippers
	IndexedSet_1_t648 * ___m_Clippers_1;
};
struct ClipperRegistry_t647_StaticFields{
	// UnityEngine.UI.ClipperRegistry UnityEngine.UI.ClipperRegistry::s_Instance
	ClipperRegistry_t647 * ___s_Instance_0;
};
