﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Signal.Signal
struct Signal_t116;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Signal.Signal>
struct  KeyValuePair_2_t2611 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,Common.Signal.Signal>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,Common.Signal.Signal>::value
	Signal_t116 * ___value_1;
};
