﻿#pragma once
#include <stdint.h>
// Vuforia.IPlayModeEditorUtility
struct IPlayModeEditorUtility_t1129;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.PlayModeEditorUtility
struct  PlayModeEditorUtility_t1128  : public Object_t
{
};
struct PlayModeEditorUtility_t1128_StaticFields{
	// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::sInstance
	Object_t * ___sInstance_0;
};
