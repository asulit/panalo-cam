﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2448_t2392_marshal(const U24ArrayTypeU2448_t2392& unmarshaled, U24ArrayTypeU2448_t2392_marshaled& marshaled);
extern "C" void U24ArrayTypeU2448_t2392_marshal_back(const U24ArrayTypeU2448_t2392_marshaled& marshaled, U24ArrayTypeU2448_t2392& unmarshaled);
extern "C" void U24ArrayTypeU2448_t2392_marshal_cleanup(U24ArrayTypeU2448_t2392_marshaled& marshaled);
