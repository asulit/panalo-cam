﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.TargetFinder/FilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Filter.h"
// Vuforia.TargetFinder/FilterMode
struct  FilterMode_t1211 
{
	// System.Int32 Vuforia.TargetFinder/FilterMode::value__
	int32_t ___value___1;
};
