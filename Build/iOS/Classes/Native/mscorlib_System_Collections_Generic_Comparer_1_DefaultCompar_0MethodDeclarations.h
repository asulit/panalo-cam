﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.CameraDevice/FocusMode>
struct DefaultComparer_t2733;
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.CameraDevice/FocusMode>::.ctor()
extern "C" void DefaultComparer__ctor_m17846_gshared (DefaultComparer_t2733 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m17846(__this, method) (( void (*) (DefaultComparer_t2733 *, const MethodInfo*))DefaultComparer__ctor_m17846_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.CameraDevice/FocusMode>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m17847_gshared (DefaultComparer_t2733 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m17847(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2733 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m17847_gshared)(__this, ___x, ___y, method)
