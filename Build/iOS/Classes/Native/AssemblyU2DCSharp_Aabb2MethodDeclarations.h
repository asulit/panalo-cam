﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Aabb2
struct Aabb2_t1;
// System.String
struct String_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void Aabb2::.ctor()
extern "C" void Aabb2__ctor_m0 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Aabb2::.ctor(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" void Aabb2__ctor_m1 (Aabb2_t1 * __this, Vector2_t2  ___v1, Vector2_t2  ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Aabb2::.ctor(UnityEngine.Rect)
extern "C" void Aabb2__ctor_m2 (Aabb2_t1 * __this, Rect_t267  ___rect, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Aabb2::Empty()
extern "C" void Aabb2_Empty_m3 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Aabb2::IsEmpty()
extern "C" bool Aabb2_IsEmpty_m4 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Aabb2::GetMinimum()
extern "C" Vector2_t2  Aabb2_GetMinimum_m5 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Aabb2::GetMaximum()
extern "C" Vector2_t2  Aabb2_GetMaximum_m6 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Aabb2::GetCenter()
extern "C" Vector2_t2  Aabb2_GetCenter_m7 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Aabb2::GetSize()
extern "C" Vector2_t2  Aabb2_GetSize_m8 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Aabb2::GetRadiusVector()
extern "C" Vector2_t2  Aabb2_GetRadiusVector_m9 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Aabb2::GetRadius()
extern "C" float Aabb2_GetRadius_m10 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Aabb2::AddToContain(UnityEngine.Vector2)
extern "C" void Aabb2_AddToContain_m11 (Aabb2_t1 * __this, Vector2_t2  ___v, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Aabb2::Contains(UnityEngine.Vector2)
extern "C" bool Aabb2_Contains_m12 (Aabb2_t1 * __this, Vector2_t2  ___v, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Aabb2::IsOverlapping(Aabb2)
extern "C" bool Aabb2_IsOverlapping_m13 (Aabb2_t1 * __this, Aabb2_t1 * ___otherBox, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Aabb2::Translate(UnityEngine.Vector2)
extern "C" void Aabb2_Translate_m14 (Aabb2_t1 * __this, Vector2_t2  ___translation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Aabb2 Aabb2::GetAabbInLocalSpace()
extern "C" Aabb2_t1 * Aabb2_GetAabbInLocalSpace_m15 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Aabb2::GetTopLeft()
extern "C" Vector2_t2  Aabb2_GetTopLeft_m16 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Aabb2::GetBottomLeft()
extern "C" Vector2_t2  Aabb2_GetBottomLeft_m17 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Aabb2::GetTopRight()
extern "C" Vector2_t2  Aabb2_GetTopRight_m18 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Aabb2::GetBottomRight()
extern "C" Vector2_t2  Aabb2_GetBottomRight_m19 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Aabb2::ToString()
extern "C" String_t* Aabb2_ToString_m20 (Aabb2_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
