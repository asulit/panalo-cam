﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_85.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25105_gshared (InternalEnumerator_1_t3259 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m25105(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3259 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m25105_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25106_gshared (InternalEnumerator_1_t3259 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25106(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3259 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25106_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25107_gshared (InternalEnumerator_1_t3259 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25107(__this, method) (( void (*) (InternalEnumerator_1_t3259 *, const MethodInfo*))InternalEnumerator_1_Dispose_m25107_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25108_gshared (InternalEnumerator_1_t3259 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25108(__this, method) (( bool (*) (InternalEnumerator_1_t3259 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m25108_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m25109_gshared (InternalEnumerator_1_t3259 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25109(__this, method) (( int32_t (*) (InternalEnumerator_1_t3259 *, const MethodInfo*))InternalEnumerator_1_get_Current_m25109_gshared)(__this, method)
