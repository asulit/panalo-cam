﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>
struct Dictionary_2_t208;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17643_gshared (Enumerator_t436 * __this, Dictionary_2_t208 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m17643(__this, ___dictionary, method) (( void (*) (Enumerator_t436 *, Dictionary_2_t208 *, const MethodInfo*))Enumerator__ctor_m17643_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17644_gshared (Enumerator_t436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17644(__this, method) (( Object_t * (*) (Enumerator_t436 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17644_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17645_gshared (Enumerator_t436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17645(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t436 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17645_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17646_gshared (Enumerator_t436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17646(__this, method) (( Object_t * (*) (Enumerator_t436 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17646_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17647_gshared (Enumerator_t436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17647(__this, method) (( Object_t * (*) (Enumerator_t436 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17647_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1677_gshared (Enumerator_t436 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1677(__this, method) (( bool (*) (Enumerator_t436 *, const MethodInfo*))Enumerator_MoveNext_m1677_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::get_Current()
extern "C" KeyValuePair_2_t352  Enumerator_get_Current_m1674_gshared (Enumerator_t436 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1674(__this, method) (( KeyValuePair_2_t352  (*) (Enumerator_t436 *, const MethodInfo*))Enumerator_get_Current_m1674_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m17648_gshared (Enumerator_t436 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m17648(__this, method) (( int32_t (*) (Enumerator_t436 *, const MethodInfo*))Enumerator_get_CurrentKey_m17648_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m17649_gshared (Enumerator_t436 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m17649(__this, method) (( int32_t (*) (Enumerator_t436 *, const MethodInfo*))Enumerator_get_CurrentValue_m17649_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m17650_gshared (Enumerator_t436 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17650(__this, method) (( void (*) (Enumerator_t436 *, const MethodInfo*))Enumerator_VerifyState_m17650_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m17651_gshared (Enumerator_t436 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m17651(__this, method) (( void (*) (Enumerator_t436 *, const MethodInfo*))Enumerator_VerifyCurrent_m17651_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>::Dispose()
extern "C" void Enumerator_Dispose_m17652_gshared (Enumerator_t436 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17652(__this, method) (( void (*) (Enumerator_t436 *, const MethodInfo*))Enumerator_Dispose_m17652_gshared)(__this, method)
