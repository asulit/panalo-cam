﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<EScreens,System.String>
struct Dictionary_2_t191;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<EScreens,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"
// System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>
struct  Enumerator_t2684 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::dictionary
	Dictionary_2_t191 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.String>::current
	KeyValuePair_2_t2681  ___current_3;
};
