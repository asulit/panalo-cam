﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Query.QuerySystemImplementation
struct QuerySystemImplementation_t109;
// System.String
struct String_t;
// Common.Query.QueryResultResolver
struct QueryResultResolver_t328;
// Common.Query.IQueryRequest
struct IQueryRequest_t329;

// System.Void Common.Query.QuerySystemImplementation::.ctor()
extern "C" void QuerySystemImplementation__ctor_m375 (QuerySystemImplementation_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Query.QuerySystemImplementation::RegisterResolver(System.String,Common.Query.QueryResultResolver)
extern "C" void QuerySystemImplementation_RegisterResolver_m376 (QuerySystemImplementation_t109 * __this, String_t* ___queryId, QueryResultResolver_t328 * ___resolver, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Query.QuerySystemImplementation::RemoveResolver(System.String)
extern "C" void QuerySystemImplementation_RemoveResolver_m377 (QuerySystemImplementation_t109 * __this, String_t* ___queryId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Query.IQueryRequest Common.Query.QuerySystemImplementation::Start(System.String)
extern "C" Object_t * QuerySystemImplementation_Start_m378 (QuerySystemImplementation_t109 * __this, String_t* ___queryId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Query.QuerySystemImplementation::HasResolver(System.String)
extern "C" bool QuerySystemImplementation_HasResolver_m379 (QuerySystemImplementation_t109 * __this, String_t* ___queryId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
