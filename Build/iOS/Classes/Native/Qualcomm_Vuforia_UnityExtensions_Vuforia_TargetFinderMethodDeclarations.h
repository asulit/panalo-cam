﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinder
struct TargetFinder_t1119;

// System.Void Vuforia.TargetFinder::.ctor()
extern "C" void TargetFinder__ctor_m6837 (TargetFinder_t1119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
