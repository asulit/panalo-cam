﻿#pragma once
#include <stdint.h>
// Vuforia.Word
struct Word_t1190;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Word>
struct  Comparison_1_t3369  : public MulticastDelegate_t28
{
};
