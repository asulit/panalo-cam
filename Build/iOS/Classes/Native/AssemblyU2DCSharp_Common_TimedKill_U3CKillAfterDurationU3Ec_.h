﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// Common.TimedKill
struct TimedKill_t123;
// System.Object
#include "mscorlib_System_Object.h"
// Common.TimedKill/<KillAfterDuration>c__Iterator3
struct  U3CKillAfterDurationU3Ec__Iterator3_t122  : public Object_t
{
	// System.Single Common.TimedKill/<KillAfterDuration>c__Iterator3::duration
	float ___duration_0;
	// System.Int32 Common.TimedKill/<KillAfterDuration>c__Iterator3::$PC
	int32_t ___U24PC_1;
	// System.Object Common.TimedKill/<KillAfterDuration>c__Iterator3::$current
	Object_t * ___U24current_2;
	// System.Single Common.TimedKill/<KillAfterDuration>c__Iterator3::<$>duration
	float ___U3CU24U3Eduration_3;
	// Common.TimedKill Common.TimedKill/<KillAfterDuration>c__Iterator3::<>f__this
	TimedKill_t123 * ___U3CU3Ef__this_4;
};
