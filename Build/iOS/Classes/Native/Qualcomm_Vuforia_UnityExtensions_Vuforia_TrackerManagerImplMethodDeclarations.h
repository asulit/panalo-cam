﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackerManagerImpl
struct TrackerManagerImpl_t1221;
// Vuforia.StateManager
struct StateManager_t1205;

// Vuforia.StateManager Vuforia.TrackerManagerImpl::GetStateManager()
extern "C" StateManager_t1205 * TrackerManagerImpl_GetStateManager_m6867 (TrackerManagerImpl_t1221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManagerImpl::.ctor()
extern "C" void TrackerManagerImpl__ctor_m6868 (TrackerManagerImpl_t1221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
