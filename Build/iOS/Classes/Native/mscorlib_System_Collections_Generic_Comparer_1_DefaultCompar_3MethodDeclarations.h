﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t2947;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m20872_gshared (DefaultComparer_t2947 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m20872(__this, method) (( void (*) (DefaultComparer_t2947 *, const MethodInfo*))DefaultComparer__ctor_m20872_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m20873_gshared (DefaultComparer_t2947 * __this, UIVertex_t605  ___x, UIVertex_t605  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m20873(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2947 *, UIVertex_t605 , UIVertex_t605 , const MethodInfo*))DefaultComparer_Compare_m20873_gshared)(__this, ___x, ___y, method)
