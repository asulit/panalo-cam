﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// UnityEngine.Color UnityEngine.UI.ColorBlock::get_normalColor()
extern "C" Color_t9  ColorBlock_get_normalColor_m2258 (ColorBlock_t551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_normalColor(UnityEngine.Color)
extern "C" void ColorBlock_set_normalColor_m2259 (ColorBlock_t551 * __this, Color_t9  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_highlightedColor()
extern "C" Color_t9  ColorBlock_get_highlightedColor_m2260 (ColorBlock_t551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_highlightedColor(UnityEngine.Color)
extern "C" void ColorBlock_set_highlightedColor_m2261 (ColorBlock_t551 * __this, Color_t9  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_pressedColor()
extern "C" Color_t9  ColorBlock_get_pressedColor_m2262 (ColorBlock_t551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_pressedColor(UnityEngine.Color)
extern "C" void ColorBlock_set_pressedColor_m2263 (ColorBlock_t551 * __this, Color_t9  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_disabledColor()
extern "C" Color_t9  ColorBlock_get_disabledColor_m2264 (ColorBlock_t551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_disabledColor(UnityEngine.Color)
extern "C" void ColorBlock_set_disabledColor_m2265 (ColorBlock_t551 * __this, Color_t9  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_colorMultiplier()
extern "C" float ColorBlock_get_colorMultiplier_m2266 (ColorBlock_t551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_colorMultiplier(System.Single)
extern "C" void ColorBlock_set_colorMultiplier_m2267 (ColorBlock_t551 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_fadeDuration()
extern "C" float ColorBlock_get_fadeDuration_m2268 (ColorBlock_t551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_fadeDuration(System.Single)
extern "C" void ColorBlock_set_fadeDuration_m2269 (ColorBlock_t551 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::get_defaultColorBlock()
extern "C" ColorBlock_t551  ColorBlock_get_defaultColorBlock_m2270 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
