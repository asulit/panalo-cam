﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.DispatcherBase
struct DispatcherBase_t158;
// UnityThreading.Task
struct Task_t165;
// System.Action
struct Action_t16;
// UnityThreading.TaskBase
struct TaskBase_t163;
// System.Collections.Generic.IEnumerable`1<UnityThreading.TaskBase>
struct IEnumerable_1_t350;

// System.Void UnityThreading.DispatcherBase::.ctor()
extern "C" void DispatcherBase__ctor_m524 (DispatcherBase_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityThreading.DispatcherBase::get_TaskCount()
extern "C" int32_t DispatcherBase_get_TaskCount_m525 (DispatcherBase_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.Task UnityThreading.DispatcherBase::Dispatch(System.Action)
extern "C" Task_t165 * DispatcherBase_Dispatch_m526 (DispatcherBase_t158 * __this, Action_t16 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.TaskBase UnityThreading.DispatcherBase::Dispatch(UnityThreading.TaskBase)
extern "C" TaskBase_t163 * DispatcherBase_Dispatch_m527 (DispatcherBase_t158 * __this, TaskBase_t163 * ___task, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.DispatcherBase::AddTask(UnityThreading.TaskBase)
extern "C" void DispatcherBase_AddTask_m528 (DispatcherBase_t158 * __this, TaskBase_t163 * ___task, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.DispatcherBase::AddTasks(System.Collections.Generic.IEnumerable`1<UnityThreading.TaskBase>)
extern "C" void DispatcherBase_AddTasks_m529 (DispatcherBase_t158 * __this, Object_t* ___tasks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.DispatcherBase::ReorderTasks()
extern "C" void DispatcherBase_ReorderTasks_m530 (DispatcherBase_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<UnityThreading.TaskBase> UnityThreading.DispatcherBase::SplitTasks(System.Int32)
extern "C" Object_t* DispatcherBase_SplitTasks_m531 (DispatcherBase_t158 * __this, int32_t ___divisor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<UnityThreading.TaskBase> UnityThreading.DispatcherBase::IsolateTasks(System.Int32)
extern "C" Object_t* DispatcherBase_IsolateTasks_m532 (DispatcherBase_t158 * __this, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.DispatcherBase::Dispose()
extern "C" void DispatcherBase_Dispose_m533 (DispatcherBase_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityThreading.DispatcherBase::<ReorderTasks>m__3(UnityThreading.TaskBase,UnityThreading.TaskBase)
extern "C" int32_t DispatcherBase_U3CReorderTasksU3Em__3_m534 (Object_t * __this /* static, unused */, TaskBase_t163 * ___a, TaskBase_t163 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
