﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ShimEnumerator_t3194;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t3181;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m24236_gshared (ShimEnumerator_t3194 * __this, Dictionary_2_t3181 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m24236(__this, ___host, method) (( void (*) (ShimEnumerator_t3194 *, Dictionary_2_t3181 *, const MethodInfo*))ShimEnumerator__ctor_m24236_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m24237_gshared (ShimEnumerator_t3194 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m24237(__this, method) (( bool (*) (ShimEnumerator_t3194 *, const MethodInfo*))ShimEnumerator_MoveNext_m24237_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m24238_gshared (ShimEnumerator_t3194 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m24238(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t3194 *, const MethodInfo*))ShimEnumerator_get_Entry_m24238_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m24239_gshared (ShimEnumerator_t3194 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m24239(__this, method) (( Object_t * (*) (ShimEnumerator_t3194 *, const MethodInfo*))ShimEnumerator_get_Key_m24239_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m24240_gshared (ShimEnumerator_t3194 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m24240(__this, method) (( Object_t * (*) (ShimEnumerator_t3194 *, const MethodInfo*))ShimEnumerator_get_Value_m24240_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m24241_gshared (ShimEnumerator_t3194 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m24241(__this, method) (( Object_t * (*) (ShimEnumerator_t3194 *, const MethodInfo*))ShimEnumerator_get_Current_m24241_gshared)(__this, method)
