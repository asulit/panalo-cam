﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.StateManager
struct StateManager_t1205;

// System.Void Vuforia.StateManager::.ctor()
extern "C" void StateManager__ctor_m6803 (StateManager_t1205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
