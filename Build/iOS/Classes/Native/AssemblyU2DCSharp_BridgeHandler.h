﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// INativeCalls
struct INativeCalls_t186;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// BridgeHandler
struct  BridgeHandler_t185  : public MonoBehaviour_t5
{
	// INativeCalls BridgeHandler::nativeCalls
	Object_t * ___nativeCalls_10;
};
struct BridgeHandler_t185_StaticFields{
	// System.String BridgeHandler::BRIDGE
	String_t* ___BRIDGE_2;
	// System.String BridgeHandler::SHARE
	String_t* ___SHARE_3;
	// System.String BridgeHandler::SMS
	String_t* ___SMS_4;
	// System.String BridgeHandler::START_RECORD
	String_t* ___START_RECORD_5;
	// System.String BridgeHandler::STOP_RECORD
	String_t* ___STOP_RECORD_6;
	// System.String BridgeHandler::CANCEL_RECORD
	String_t* ___CANCEL_RECORD_7;
	// System.String BridgeHandler::RECEIVER
	String_t* ___RECEIVER_8;
	// System.String BridgeHandler::MESSAGE
	String_t* ___MESSAGE_9;
};
