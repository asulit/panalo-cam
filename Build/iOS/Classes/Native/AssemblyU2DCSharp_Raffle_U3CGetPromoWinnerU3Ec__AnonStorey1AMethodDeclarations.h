﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Raffle/<GetPromoWinner>c__AnonStorey1A
struct U3CGetPromoWinnerU3Ec__AnonStorey1A_t205;
// UnityEngine.WWW
struct WWW_t199;

// System.Void Raffle/<GetPromoWinner>c__AnonStorey1A::.ctor()
extern "C" void U3CGetPromoWinnerU3Ec__AnonStorey1A__ctor_m710 (U3CGetPromoWinnerU3Ec__AnonStorey1A_t205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle/<GetPromoWinner>c__AnonStorey1A::<>m__14(UnityEngine.WWW)
extern "C" void U3CGetPromoWinnerU3Ec__AnonStorey1A_U3CU3Em__14_m711 (U3CGetPromoWinnerU3Ec__AnonStorey1A_t205 * __this, WWW_t199 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
