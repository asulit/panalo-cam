﻿#pragma once
#include <stdint.h>
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t254;
// System.Object
#include "mscorlib_System_Object.h"
// iTween/CRSpline
struct  CRSpline_t253  : public Object_t
{
	// UnityEngine.Vector3[] iTween/CRSpline::pts
	Vector3U5BU5D_t254* ___pts_0;
};
