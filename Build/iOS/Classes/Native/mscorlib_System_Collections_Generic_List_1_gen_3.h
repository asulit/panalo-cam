﻿#pragma once
#include <stdint.h>
// UnityEngine.Collider[]
struct ColliderU5BU5D_t59;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Collider>
struct  List_1_t343  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Collider>::_items
	ColliderU5BU5D_t59* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Collider>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Collider>::_version
	int32_t ____version_3;
};
struct List_1_t343_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Collider>::EmptyArray
	ColliderU5BU5D_t59* ___EmptyArray_4;
};
