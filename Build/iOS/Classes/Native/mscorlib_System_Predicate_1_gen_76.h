﻿#pragma once
#include <stdint.h>
// Vuforia.ITextRecoEventHandler
struct ITextRecoEventHandler_t1281;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.ITextRecoEventHandler>
struct  Predicate_1_t3513  : public MulticastDelegate_t28
{
};
