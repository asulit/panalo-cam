﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_54MethodDeclarations.h"
#define Enumerator__ctor_m28647(__this, ___host, method) (( void (*) (Enumerator_t1379 *, Dictionary_2_t1217 *, const MethodInfo*))Enumerator__ctor_m19866_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m28648(__this, method) (( Object_t * (*) (Enumerator_t1379 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19867_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::Dispose()
#define Enumerator_Dispose_m7452(__this, method) (( void (*) (Enumerator_t1379 *, const MethodInfo*))Enumerator_Dispose_m19868_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::MoveNext()
#define Enumerator_MoveNext_m7451(__this, method) (( bool (*) (Enumerator_t1379 *, const MethodInfo*))Enumerator_MoveNext_m19869_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::get_Current()
#define Enumerator_get_Current_m7450(__this, method) (( Object_t * (*) (Enumerator_t1379 *, const MethodInfo*))Enumerator_get_Current_m19870_gshared)(__this, method)
