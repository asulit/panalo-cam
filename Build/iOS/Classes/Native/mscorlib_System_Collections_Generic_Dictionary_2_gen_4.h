﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t401;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t2500;
// System.String[]
struct StringU5BU5D_t137;
// Common.Time.TimeReference[]
struct TimeReferenceU5BU5D_t2615;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t2503;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,Common.Time.TimeReference,System.Collections.DictionaryEntry>
struct Transform_1_t2616;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>
struct  Dictionary_2_t409  : public Object_t
{
	// System.Int32[] System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::table
	Int32U5BU5D_t401* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::linkSlots
	LinkU5BU5D_t2500* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::keySlots
	StringU5BU5D_t137* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::valueSlots
	TimeReferenceU5BU5D_t2615* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::hcp
	Object_t* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::serialization_info
	SerializationInfo_t1012 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::generation
	int32_t ___generation_14;
};
struct Dictionary_2_t409_StaticFields{
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>::<>f__am$cacheB
	Transform_1_t2616 * ___U3CU3Ef__amU24cacheB_15;
};
