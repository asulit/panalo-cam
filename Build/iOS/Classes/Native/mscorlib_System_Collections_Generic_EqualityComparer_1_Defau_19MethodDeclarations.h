﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct DefaultComparer_t3453;
// Vuforia.VuforiaManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor()
extern "C" void DefaultComparer__ctor_m28308_gshared (DefaultComparer_t3453 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m28308(__this, method) (( void (*) (DefaultComparer_t3453 *, const MethodInfo*))DefaultComparer__ctor_m28308_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m28309_gshared (DefaultComparer_t3453 * __this, VirtualButtonData_t1135  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m28309(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3453 *, VirtualButtonData_t1135 , const MethodInfo*))DefaultComparer_GetHashCode_m28309_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/VirtualButtonData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m28310_gshared (DefaultComparer_t3453 * __this, VirtualButtonData_t1135  ___x, VirtualButtonData_t1135  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m28310(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3453 *, VirtualButtonData_t1135 , VirtualButtonData_t1135 , const MethodInfo*))DefaultComparer_Equals_m28310_gshared)(__this, ___x, ___y, method)
