﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.Tracker
struct Tracker_t1115;

// System.Boolean Vuforia.Tracker::get_IsActive()
extern "C" bool Tracker_get_IsActive_m5677 (Tracker_t1115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Tracker::set_IsActive(System.Boolean)
extern "C" void Tracker_set_IsActive_m5678 (Tracker_t1115 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Tracker::.ctor()
extern "C" void Tracker__ctor_m5679 (Tracker_t1115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
