﻿#pragma once
#include <stdint.h>
// System.Text.StringBuilder
struct StringBuilder_t70;
// System.Object
#include "mscorlib_System_Object.h"
// MiniJSON.Json/Serializer
struct  Serializer_t69  : public Object_t
{
	// System.Text.StringBuilder MiniJSON.Json/Serializer::builder
	StringBuilder_t70 * ___builder_0;
};
