﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t706;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C" void UnityAction_1__ctor_m3528_gshared (UnityAction_1_t706 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define UnityAction_1__ctor_m3528(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t706 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m3528_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
extern "C" void UnityAction_1_Invoke_m20048_gshared (UnityAction_1_t706 * __this, Color_t9  ___arg0, const MethodInfo* method);
#define UnityAction_1_Invoke_m20048(__this, ___arg0, method) (( void (*) (UnityAction_1_t706 *, Color_t9 , const MethodInfo*))UnityAction_1_Invoke_m20048_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Color>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C" Object_t * UnityAction_1_BeginInvoke_m20049_gshared (UnityAction_1_t706 * __this, Color_t9  ___arg0, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define UnityAction_1_BeginInvoke_m20049(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t706 *, Color_t9 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m20049_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C" void UnityAction_1_EndInvoke_m20050_gshared (UnityAction_1_t706 * __this, Object_t * ___result, const MethodInfo* method);
#define UnityAction_1_EndInvoke_m20050(__this, ___result, method) (( void (*) (UnityAction_1_t706 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m20050_gshared)(__this, ___result, method)
