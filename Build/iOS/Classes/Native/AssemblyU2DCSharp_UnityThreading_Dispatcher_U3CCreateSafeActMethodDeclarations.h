﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.Dispatcher/<CreateSafeAction>c__AnonStorey11`1<System.Object>
struct U3CCreateSafeActionU3Ec__AnonStorey11_1_t2652;

// System.Void UnityThreading.Dispatcher/<CreateSafeAction>c__AnonStorey11`1<System.Object>::.ctor()
extern "C" void U3CCreateSafeActionU3Ec__AnonStorey11_1__ctor_m16954_gshared (U3CCreateSafeActionU3Ec__AnonStorey11_1_t2652 * __this, const MethodInfo* method);
#define U3CCreateSafeActionU3Ec__AnonStorey11_1__ctor_m16954(__this, method) (( void (*) (U3CCreateSafeActionU3Ec__AnonStorey11_1_t2652 *, const MethodInfo*))U3CCreateSafeActionU3Ec__AnonStorey11_1__ctor_m16954_gshared)(__this, method)
// System.Void UnityThreading.Dispatcher/<CreateSafeAction>c__AnonStorey11`1<System.Object>::<>m__5()
extern "C" void U3CCreateSafeActionU3Ec__AnonStorey11_1_U3CU3Em__5_m16955_gshared (U3CCreateSafeActionU3Ec__AnonStorey11_1_t2652 * __this, const MethodInfo* method);
#define U3CCreateSafeActionU3Ec__AnonStorey11_1_U3CU3Em__5_m16955(__this, method) (( void (*) (U3CCreateSafeActionU3Ec__AnonStorey11_1_t2652 *, const MethodInfo*))U3CCreateSafeActionU3Ec__AnonStorey11_1_U3CU3Em__5_m16955_gshared)(__this, method)
