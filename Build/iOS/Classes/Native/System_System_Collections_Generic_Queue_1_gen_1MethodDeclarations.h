﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2568;
// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Collections.Generic.Queue`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
extern "C" void Queue_1__ctor_m15872_gshared (Queue_1_t2568 * __this, const MethodInfo* method);
#define Queue_1__ctor_m15872(__this, method) (( void (*) (Queue_1_t2568 *, const MethodInfo*))Queue_1__ctor_m15872_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Queue_1_System_Collections_ICollection_CopyTo_m15874_gshared (Queue_1_t2568 * __this, Array_t * ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m15874(__this, ___array, ___idx, method) (( void (*) (Queue_1_t2568 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m15874_gshared)(__this, ___array, ___idx, method)
// System.Boolean System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m15876_gshared (Queue_1_t2568 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m15876(__this, method) (( bool (*) (Queue_1_t2568 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m15876_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Queue_1_System_Collections_ICollection_get_SyncRoot_m15878_gshared (Queue_1_t2568 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m15878(__this, method) (( Object_t * (*) (Queue_1_t2568 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m15878_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15880_gshared (Queue_1_t2568 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15880(__this, method) (( Object_t* (*) (Queue_1_t2568 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15880_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Queue_1_System_Collections_IEnumerable_GetEnumerator_m15882_gshared (Queue_1_t2568 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m15882(__this, method) (( Object_t * (*) (Queue_1_t2568 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m15882_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Queue_1_CopyTo_m15884_gshared (Queue_1_t2568 * __this, ObjectU5BU5D_t356* ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_CopyTo_m15884(__this, ___array, ___idx, method) (( void (*) (Queue_1_t2568 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))Queue_1_CopyTo_m15884_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<System.Object>::Dequeue()
extern "C" Object_t * Queue_1_Dequeue_m15885_gshared (Queue_1_t2568 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m15885(__this, method) (( Object_t * (*) (Queue_1_t2568 *, const MethodInfo*))Queue_1_Dequeue_m15885_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.Object>::Peek()
extern "C" Object_t * Queue_1_Peek_m15887_gshared (Queue_1_t2568 * __this, const MethodInfo* method);
#define Queue_1_Peek_m15887(__this, method) (( Object_t * (*) (Queue_1_t2568 *, const MethodInfo*))Queue_1_Peek_m15887_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::Enqueue(T)
extern "C" void Queue_1_Enqueue_m15888_gshared (Queue_1_t2568 * __this, Object_t * ___item, const MethodInfo* method);
#define Queue_1_Enqueue_m15888(__this, ___item, method) (( void (*) (Queue_1_t2568 *, Object_t *, const MethodInfo*))Queue_1_Enqueue_m15888_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::SetCapacity(System.Int32)
extern "C" void Queue_1_SetCapacity_m15890_gshared (Queue_1_t2568 * __this, int32_t ___new_size, const MethodInfo* method);
#define Queue_1_SetCapacity_m15890(__this, ___new_size, method) (( void (*) (Queue_1_t2568 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m15890_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
extern "C" int32_t Queue_1_get_Count_m15892_gshared (Queue_1_t2568 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m15892(__this, method) (( int32_t (*) (Queue_1_t2568 *, const MethodInfo*))Queue_1_get_Count_m15892_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2569  Queue_1_GetEnumerator_m15894_gshared (Queue_1_t2568 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m15894(__this, method) (( Enumerator_t2569  (*) (Queue_1_t2568 *, const MethodInfo*))Queue_1_GetEnumerator_m15894_gshared)(__this, method)
