﻿#pragma once
#include <stdint.h>
// Vuforia.Prop
struct Prop_t357;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.Prop>
struct  Predicate_1_t3410  : public MulticastDelegate_t28
{
};
