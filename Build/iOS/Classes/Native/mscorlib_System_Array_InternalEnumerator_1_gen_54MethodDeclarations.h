﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.Color32>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_54.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22310_gshared (InternalEnumerator_1_t3038 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22310(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3038 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22310_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22311_gshared (InternalEnumerator_1_t3038 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22311(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3038 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22311_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22312_gshared (InternalEnumerator_1_t3038 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22312(__this, method) (( void (*) (InternalEnumerator_1_t3038 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22312_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22313_gshared (InternalEnumerator_1_t3038 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22313(__this, method) (( bool (*) (InternalEnumerator_1_t3038 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22313_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
extern "C" Color32_t711  InternalEnumerator_1_get_Current_m22314_gshared (InternalEnumerator_1_t3038 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22314(__this, method) (( Color32_t711  (*) (InternalEnumerator_1_t3038 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22314_gshared)(__this, method)
