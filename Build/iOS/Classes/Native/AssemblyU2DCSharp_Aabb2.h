﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Aabb2
struct  Aabb2_t1  : public Object_t
{
	// UnityEngine.Vector2 Aabb2::min
	Vector2_t2  ___min_1;
	// UnityEngine.Vector2 Aabb2::max
	Vector2_t2  ___max_2;
};
