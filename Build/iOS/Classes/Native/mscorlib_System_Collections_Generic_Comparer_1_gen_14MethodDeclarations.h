﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct Comparer_1_t3605;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<System.DateTimeOffset>::.ctor()
extern "C" void Comparer_1__ctor_m29982_gshared (Comparer_1_t3605 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m29982(__this, method) (( void (*) (Comparer_1_t3605 *, const MethodInfo*))Comparer_1__ctor_m29982_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.DateTimeOffset>::.cctor()
extern "C" void Comparer_1__cctor_m29983_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m29983(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m29983_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.DateTimeOffset>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m29984_gshared (Comparer_1_t3605 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m29984(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3605 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m29984_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.DateTimeOffset>::get_Default()
extern "C" Comparer_1_t3605 * Comparer_1_get_Default_m29985_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m29985(__this /* static, unused */, method) (( Comparer_1_t3605 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m29985_gshared)(__this /* static, unused */, method)
