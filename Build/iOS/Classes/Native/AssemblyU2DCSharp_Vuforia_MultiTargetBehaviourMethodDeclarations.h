﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MultiTargetBehaviour
struct MultiTargetBehaviour_t296;

// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern "C" void MultiTargetBehaviour__ctor_m1254 (MultiTargetBehaviour_t296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
