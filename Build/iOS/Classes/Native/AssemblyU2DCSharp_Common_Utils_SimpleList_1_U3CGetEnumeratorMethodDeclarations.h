﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator4_t2599;
// System.Object
struct Object_t;

// System.Void Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator4__ctor_m16189_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator4__ctor_m16189(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator4_t2599 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator4__ctor_m16189_gshared)(__this, method)
// T Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m16190_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m16190(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator4_t2599 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m16190_gshared)(__this, method)
// System.Object Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m16191_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m16191(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator4_t2599 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m16191_gshared)(__this, method)
// System.Boolean Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator4_MoveNext_m16192_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator4_MoveNext_m16192(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator4_t2599 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator4_MoveNext_m16192_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator4_Dispose_m16193_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator4_Dispose_m16193(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator4_t2599 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator4_Dispose_m16193_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::Reset()
extern "C" void U3CGetEnumeratorU3Ec__Iterator4_Reset_m16194_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator4_Reset_m16194(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator4_t2599 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator4_Reset_m16194_gshared)(__this, method)
