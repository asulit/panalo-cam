﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct IList_1_t3242;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct  ReadOnlyCollection_1_t3241  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::list
	Object_t* ___list_0;
};
