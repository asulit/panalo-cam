﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Raffle/<RegisterUser>c__AnonStorey17
struct U3CRegisterUserU3Ec__AnonStorey17_t201;
// UnityEngine.WWW
struct WWW_t199;

// System.Void Raffle/<RegisterUser>c__AnonStorey17::.ctor()
extern "C" void U3CRegisterUserU3Ec__AnonStorey17__ctor_m704 (U3CRegisterUserU3Ec__AnonStorey17_t201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle/<RegisterUser>c__AnonStorey17::<>m__11(UnityEngine.WWW)
extern "C" void U3CRegisterUserU3Ec__AnonStorey17_U3CU3Em__11_m705 (U3CRegisterUserU3Ec__AnonStorey17_t201 * __this, WWW_t199 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
