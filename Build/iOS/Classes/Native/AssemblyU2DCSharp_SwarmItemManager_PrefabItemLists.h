﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.LinkedList`1<SwarmItem>
struct LinkedList_1_t152;
// System.Collections.Generic.Stack`1<SwarmItem>
struct Stack_1_t153;
// System.Object
#include "mscorlib_System_Object.h"
// SwarmItemManager/PrefabItemLists
struct  PrefabItemLists_t151  : public Object_t
{
	// System.Collections.Generic.LinkedList`1<SwarmItem> SwarmItemManager/PrefabItemLists::activeItems
	LinkedList_1_t152 * ___activeItems_0;
	// System.Collections.Generic.Stack`1<SwarmItem> SwarmItemManager/PrefabItemLists::inactiveItems
	Stack_1_t153 * ___inactiveItems_1;
	// System.Single SwarmItemManager/PrefabItemLists::inactivePruneTimeLeft
	float ___inactivePruneTimeLeft_2;
};
