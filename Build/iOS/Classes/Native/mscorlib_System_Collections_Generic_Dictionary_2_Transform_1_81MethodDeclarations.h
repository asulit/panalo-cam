﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>
struct Transform_1_t3450;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_46.h"
// Vuforia.VuforiaManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m28293_gshared (Transform_1_t3450 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m28293(__this, ___object, ___method, method) (( void (*) (Transform_1_t3450 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m28293_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t3440  Transform_1_Invoke_m28294_gshared (Transform_1_t3450 * __this, int32_t ___key, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m28294(__this, ___key, ___value, method) (( KeyValuePair_2_t3440  (*) (Transform_1_t3450 *, int32_t, VirtualButtonData_t1135 , const MethodInfo*))Transform_1_Invoke_m28294_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m28295_gshared (Transform_1_t3450 * __this, int32_t ___key, VirtualButtonData_t1135  ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m28295(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3450 *, int32_t, VirtualButtonData_t1135 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m28295_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t3440  Transform_1_EndInvoke_m28296_gshared (Transform_1_t3450 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m28296(__this, ___result, method) (( KeyValuePair_2_t3440  (*) (Transform_1_t3450 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m28296_gshared)(__this, ___result, method)
