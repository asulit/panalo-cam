﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.HttpWebRequest
struct HttpWebRequest_t1508;
// System.Uri
struct Uri_t1583;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Net.ServicePoint
struct ServicePoint_t1584;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Net.HttpWebRequest::.ctor(System.Uri)
extern "C" void HttpWebRequest__ctor_m8692 (HttpWebRequest_t1508 * __this, Uri_t1583 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpWebRequest::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HttpWebRequest__ctor_m8693 (HttpWebRequest_t1508 * __this, SerializationInfo_t1012 * ___serializationInfo, StreamingContext_t1013  ___streamingContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpWebRequest::.cctor()
extern "C" void HttpWebRequest__cctor_m8694 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpWebRequest::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m8695 (HttpWebRequest_t1508 * __this, SerializationInfo_t1012 * ___serializationInfo, StreamingContext_t1013  ___streamingContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.HttpWebRequest::get_Address()
extern "C" Uri_t1583 * HttpWebRequest_get_Address_m8537 (HttpWebRequest_t1508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ServicePoint System.Net.HttpWebRequest::get_ServicePoint()
extern "C" ServicePoint_t1584 * HttpWebRequest_get_ServicePoint_m8541 (HttpWebRequest_t1508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ServicePoint System.Net.HttpWebRequest::GetServicePoint()
extern "C" ServicePoint_t1584 * HttpWebRequest_GetServicePoint_m8696 (HttpWebRequest_t1508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpWebRequest::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HttpWebRequest_GetObjectData_m8697 (HttpWebRequest_t1508 * __this, SerializationInfo_t1012 * ___serializationInfo, StreamingContext_t1013  ___streamingContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
