﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.EyewearCalibrationProfileManager
struct EyewearCalibrationProfileManager_t1047;

// System.Void Vuforia.EyewearCalibrationProfileManager::.ctor()
extern "C" void EyewearCalibrationProfileManager__ctor_m5197 (EyewearCalibrationProfileManager_t1047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
