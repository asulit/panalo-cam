﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__8MethodDeclarations.h"
#define Enumerator__ctor_m21168(__this, ___dictionary, method) (( void (*) (Enumerator_t2965 *, Dictionary_2_t745 *, const MethodInfo*))Enumerator__ctor_m15667_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21169(__this, method) (( Object_t * (*) (Enumerator_t2965 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15668_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21170(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t2965 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15669_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21171(__this, method) (( Object_t * (*) (Enumerator_t2965 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15670_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21172(__this, method) (( Object_t * (*) (Enumerator_t2965 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m21173(__this, method) (( bool (*) (Enumerator_t2965 *, const MethodInfo*))Enumerator_MoveNext_m15672_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::get_Current()
#define Enumerator_get_Current_m21174(__this, method) (( KeyValuePair_2_t2962  (*) (Enumerator_t2965 *, const MethodInfo*))Enumerator_get_Current_m15673_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m21175(__this, method) (( Graphic_t572 * (*) (Enumerator_t2965 *, const MethodInfo*))Enumerator_get_CurrentKey_m15674_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m21176(__this, method) (( int32_t (*) (Enumerator_t2965 *, const MethodInfo*))Enumerator_get_CurrentValue_m15675_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m21177(__this, method) (( void (*) (Enumerator_t2965 *, const MethodInfo*))Enumerator_VerifyState_m15676_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m21178(__this, method) (( void (*) (Enumerator_t2965 *, const MethodInfo*))Enumerator_VerifyCurrent_m15677_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::Dispose()
#define Enumerator_Dispose_m21179(__this, method) (( void (*) (Enumerator_t2965 *, const MethodInfo*))Enumerator_Dispose_m15678_gshared)(__this, method)
