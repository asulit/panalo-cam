﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t401;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t2500;
// UnityEngine.UI.InputField[]
struct InputFieldU5BU5D_t2735;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2764;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.UI.InputField>
struct IEqualityComparer_1_t2737;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.InputField,UnityEngine.UI.Image,System.Collections.DictionaryEntry>
struct Transform_1_t2765;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>
struct  Dictionary_2_t231  : public Object_t
{
	// System.Int32[] System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::table
	Int32U5BU5D_t401* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::linkSlots
	LinkU5BU5D_t2500* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::keySlots
	InputFieldU5BU5D_t2735* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::valueSlots
	ImageU5BU5D_t2764* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::hcp
	Object_t* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::serialization_info
	SerializationInfo_t1012 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::generation
	int32_t ___generation_14;
};
struct Dictionary_2_t231_StaticFields{
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::<>f__am$cacheB
	Transform_1_t2765 * ___U3CU3Ef__amU24cacheB_15;
};
