﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_4MethodDeclarations.h"
#define KeyCollection__ctor_m18153(__this, ___dictionary, method) (( void (*) (KeyCollection_t2761 *, Dictionary_2_t228 *, const MethodInfo*))KeyCollection__ctor_m15121_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18154(__this, ___item, method) (( void (*) (KeyCollection_t2761 *, InputField_t226 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15122_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18155(__this, method) (( void (*) (KeyCollection_t2761 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18156(__this, ___item, method) (( bool (*) (KeyCollection_t2761 *, InputField_t226 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15124_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18157(__this, ___item, method) (( bool (*) (KeyCollection_t2761 *, InputField_t226 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15125_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18158(__this, method) (( Object_t* (*) (KeyCollection_t2761 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15126_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m18159(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2761 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m15127_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18160(__this, method) (( Object_t * (*) (KeyCollection_t2761 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15128_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18161(__this, method) (( bool (*) (KeyCollection_t2761 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15129_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18162(__this, method) (( bool (*) (KeyCollection_t2761 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15130_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m18163(__this, method) (( Object_t * (*) (KeyCollection_t2761 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m15131_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m18164(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2761 *, InputFieldU5BU5D_t2735*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m15132_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::GetEnumerator()
#define KeyCollection_GetEnumerator_m18165(__this, method) (( Enumerator_t3673  (*) (KeyCollection_t2761 *, const MethodInfo*))KeyCollection_GetEnumerator_m15133_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.String>::get_Count()
#define KeyCollection_get_Count_m18166(__this, method) (( int32_t (*) (KeyCollection_t2761 *, const MethodInfo*))KeyCollection_get_Count_m15134_gshared)(__this, method)
