﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.String
struct String_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.FullScreenMovieControlMode
#include "UnityEngine_UnityEngine_FullScreenMovieControlMode.h"
// UnityEngine.FullScreenMovieScalingMode
#include "UnityEngine_UnityEngine_FullScreenMovieScalingMode.h"
// UnityEngine.iOS.ActivityIndicatorStyle
#include "UnityEngine_UnityEngine_iOS_ActivityIndicatorStyle.h"

// System.Boolean UnityEngine.Handheld::PlayFullScreenMovie(System.String,UnityEngine.Color,UnityEngine.FullScreenMovieControlMode)
extern "C" bool Handheld_PlayFullScreenMovie_m1707 (Object_t * __this /* static, unused */, String_t* ___path, Color_t9  ___bgColor, int32_t ___controlMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Handheld::INTERNAL_CALL_PlayFullScreenMovie(System.String,UnityEngine.Color&,UnityEngine.FullScreenMovieControlMode,UnityEngine.FullScreenMovieScalingMode)
extern "C" bool Handheld_INTERNAL_CALL_PlayFullScreenMovie_m4006 (Object_t * __this /* static, unused */, String_t* ___path, Color_t9 * ___bgColor, int32_t ___controlMode, int32_t ___scalingMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Handheld::SetActivityIndicatorStyleImpl(System.Int32)
extern "C" void Handheld_SetActivityIndicatorStyleImpl_m4007 (Object_t * __this /* static, unused */, int32_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Handheld::SetActivityIndicatorStyle(UnityEngine.iOS.ActivityIndicatorStyle)
extern "C" void Handheld_SetActivityIndicatorStyle_m1708 (Object_t * __this /* static, unused */, int32_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Handheld::StartActivityIndicator()
extern "C" void Handheld_StartActivityIndicator_m1709 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Handheld::ClearShaderCache()
extern "C" void Handheld_ClearShaderCache_m1705 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
