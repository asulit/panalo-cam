﻿#pragma once
#include <stdint.h>
// System.MarshalByRefObject
struct MarshalByRefObject_t1643;
// System.Runtime.Remoting.Proxies.RealProxy
struct RealProxy_t2122;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Messaging.StackBuilderSink
struct  StackBuilderSink_t2121  : public Object_t
{
	// System.MarshalByRefObject System.Runtime.Remoting.Messaging.StackBuilderSink::_target
	MarshalByRefObject_t1643 * ____target_0;
	// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.Messaging.StackBuilderSink::_rp
	RealProxy_t2122 * ____rp_1;
};
