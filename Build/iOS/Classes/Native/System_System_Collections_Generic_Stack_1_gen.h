﻿#pragma once
#include <stdint.h>
// Common.Notification.NotificationInstance[]
struct NotificationInstanceU5BU5D_t2579;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>
struct  Stack_1_t87  : public Object_t
{
	// T[] System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::_array
	NotificationInstanceU5BU5D_t2579* ____array_1;
	// System.Int32 System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::_version
	int32_t ____version_3;
};
