﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2504;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2506;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.ICollection
struct ICollection_t1751;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3619;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3620;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1606;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t2511;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t2515;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m15007_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m15007(__this, method) (( void (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2__ctor_m15007_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m15009_gshared (Dictionary_2_t2504 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m15009(__this, ___comparer, method) (( void (*) (Dictionary_2_t2504 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15009_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m15011_gshared (Dictionary_2_t2504 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m15011(__this, ___capacity, method) (( void (*) (Dictionary_2_t2504 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m15011_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m15013_gshared (Dictionary_2_t2504 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m15013(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2504 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m15013_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m15015_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m15015(__this, method) (( Object_t * (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m15015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m15017_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m15017(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2504 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m15017_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m15019_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m15019(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2504 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m15019_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m15021_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m15021(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2504 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m15021_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m15023_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m15023(__this, ___key, method) (( bool (*) (Dictionary_2_t2504 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m15023_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m15025_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m15025(__this, ___key, method) (( void (*) (Dictionary_2_t2504 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m15025_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15027_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15027(__this, method) (( bool (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15027_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15029_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15029(__this, method) (( Object_t * (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15029_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15031_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15031(__this, method) (( bool (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15031_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15033_gshared (Dictionary_2_t2504 * __this, KeyValuePair_2_t2507  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15033(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2504 *, KeyValuePair_2_t2507 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15033_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15035_gshared (Dictionary_2_t2504 * __this, KeyValuePair_2_t2507  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15035(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2504 *, KeyValuePair_2_t2507 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15035_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15037_gshared (Dictionary_2_t2504 * __this, KeyValuePair_2U5BU5D_t3619* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15037(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2504 *, KeyValuePair_2U5BU5D_t3619*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15037_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15039_gshared (Dictionary_2_t2504 * __this, KeyValuePair_2_t2507  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15039(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2504 *, KeyValuePair_2_t2507 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15039_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m15041_gshared (Dictionary_2_t2504 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m15041(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2504 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m15041_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15043_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15043(__this, method) (( Object_t * (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15043_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15045_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15045(__this, method) (( Object_t* (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15045_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15047_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15047(__this, method) (( Object_t * (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15047_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m15049_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m15049(__this, method) (( int32_t (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_get_Count_m15049_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m15051_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m15051(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2504 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m15051_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m15053_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m15053(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2504 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m15053_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m15055_gshared (Dictionary_2_t2504 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m15055(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2504 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m15055_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m15057_gshared (Dictionary_2_t2504 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m15057(__this, ___size, method) (( void (*) (Dictionary_2_t2504 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m15057_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m15059_gshared (Dictionary_2_t2504 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m15059(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2504 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m15059_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2507  Dictionary_2_make_pair_m15061_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m15061(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2507  (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m15061_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Object>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m15063_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m15063(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m15063_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m15065_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m15065(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m15065_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m15067_gshared (Dictionary_2_t2504 * __this, KeyValuePair_2U5BU5D_t3619* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m15067(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2504 *, KeyValuePair_2U5BU5D_t3619*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m15067_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m15069_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m15069(__this, method) (( void (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_Resize_m15069_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m15071_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m15071(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2504 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_Add_m15071_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m15073_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m15073(__this, method) (( void (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_Clear_m15073_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m15075_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m15075(__this, ___key, method) (( bool (*) (Dictionary_2_t2504 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m15075_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m15077_gshared (Dictionary_2_t2504 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m15077(__this, ___value, method) (( bool (*) (Dictionary_2_t2504 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m15077_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m15079_gshared (Dictionary_2_t2504 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m15079(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2504 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m15079_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m15081_gshared (Dictionary_2_t2504 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m15081(__this, ___sender, method) (( void (*) (Dictionary_2_t2504 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m15081_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m15083_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m15083(__this, ___key, method) (( bool (*) (Dictionary_2_t2504 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m15083_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m15085_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m15085(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2504 *, Object_t *, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m15085_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Keys()
extern "C" KeyCollection_t2511 * Dictionary_2_get_Keys_m15087_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m15087(__this, method) (( KeyCollection_t2511 * (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_get_Keys_m15087_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
extern "C" ValueCollection_t2515 * Dictionary_2_get_Values_m15089_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m15089(__this, method) (( ValueCollection_t2515 * (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_get_Values_m15089_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m15091_gshared (Dictionary_2_t2504 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m15091(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2504 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m15091_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m15093_gshared (Dictionary_2_t2504 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m15093(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t2504 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m15093_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m15095_gshared (Dictionary_2_t2504 * __this, KeyValuePair_2_t2507  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m15095(__this, ___pair, method) (( bool (*) (Dictionary_2_t2504 *, KeyValuePair_2_t2507 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m15095_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" Enumerator_t2513  Dictionary_2_GetEnumerator_m15097_gshared (Dictionary_2_t2504 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m15097(__this, method) (( Enumerator_t2513  (*) (Dictionary_2_t2504 *, const MethodInfo*))Dictionary_2_GetEnumerator_m15097_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m15099_gshared (Object_t * __this /* static, unused */, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m15099(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m15099_gshared)(__this /* static, unused */, ___key, ___value, method)
