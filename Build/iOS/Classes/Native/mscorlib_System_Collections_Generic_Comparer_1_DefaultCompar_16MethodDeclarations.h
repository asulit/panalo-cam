﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t3614;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C" void DefaultComparer__ctor_m30024_gshared (DefaultComparer_t3614 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m30024(__this, method) (( void (*) (DefaultComparer_t3614 *, const MethodInfo*))DefaultComparer__ctor_m30024_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m30025_gshared (DefaultComparer_t3614 * __this, TimeSpan_t427  ___x, TimeSpan_t427  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m30025(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3614 *, TimeSpan_t427 , TimeSpan_t427 , const MethodInfo*))DefaultComparer_Compare_m30025_gshared)(__this, ___x, ___y, method)
