﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.StackBuilderSink
struct StackBuilderSink_t2121;
// System.MarshalByRefObject
struct MarshalByRefObject_t1643;

// System.Void System.Runtime.Remoting.Messaging.StackBuilderSink::.ctor(System.MarshalByRefObject,System.Boolean)
extern "C" void StackBuilderSink__ctor_m12574 (StackBuilderSink_t2121 * __this, MarshalByRefObject_t1643 * ___obj, bool ___forceInternalExecute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
