﻿#pragma once
#include <stdint.h>
// System.WeakReference
struct WeakReference_t2139;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// System.Runtime.Remoting.ClientIdentity
struct  ClientIdentity_t2138  : public Identity_t2130
{
	// System.WeakReference System.Runtime.Remoting.ClientIdentity::_proxyReference
	WeakReference_t2139 * ____proxyReference_7;
};
