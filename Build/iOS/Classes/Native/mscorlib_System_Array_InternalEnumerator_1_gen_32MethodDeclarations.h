﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<ESubScreens>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_32.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"

// System.Void System.Array/InternalEnumerator`1<ESubScreens>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17430_gshared (InternalEnumerator_1_t2692 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17430(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2692 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17430_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<ESubScreens>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17431_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17431(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2692 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17431_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ESubScreens>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17432_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17432(__this, method) (( void (*) (InternalEnumerator_1_t2692 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17432_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ESubScreens>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17433_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17433(__this, method) (( bool (*) (InternalEnumerator_1_t2692 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17433_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ESubScreens>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m17434_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17434(__this, method) (( int32_t (*) (InternalEnumerator_1_t2692 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17434_gshared)(__this, method)
