﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Predicate`1<System.Int32>
struct  Predicate_1_t3073  : public MulticastDelegate_t28
{
};
