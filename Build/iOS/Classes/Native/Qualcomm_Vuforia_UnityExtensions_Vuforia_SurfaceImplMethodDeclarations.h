﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceImpl
struct SurfaceImpl_t1164;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t1085;
// UnityEngine.Mesh
struct Mesh_t104;
// System.Int32[]
struct Int32U5BU5D_t401;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void Vuforia.SurfaceImpl::.ctor(System.Int32,Vuforia.SmartTerrainTrackable)
extern "C" void SurfaceImpl__ctor_m5808 (SurfaceImpl_t1164 * __this, int32_t ___id, Object_t * ___parent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetID(System.Int32)
extern "C" void SurfaceImpl_SetID_m5809 (SurfaceImpl_t1164 * __this, int32_t ___trackableID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetMesh(System.Int32,UnityEngine.Mesh,UnityEngine.Mesh,System.Int32[])
extern "C" void SurfaceImpl_SetMesh_m5810 (SurfaceImpl_t1164 * __this, int32_t ___meshRev, Mesh_t104 * ___mesh, Mesh_t104 * ___navMesh, Int32U5BU5D_t401* ___meshBoundaries, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetBoundingBox(UnityEngine.Rect)
extern "C" void SurfaceImpl_SetBoundingBox_m5811 (SurfaceImpl_t1164 * __this, Rect_t267  ___boundingBox, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh Vuforia.SurfaceImpl::GetNavMesh()
extern "C" Mesh_t104 * SurfaceImpl_GetNavMesh_m5812 (SurfaceImpl_t1164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Vuforia.SurfaceImpl::GetMeshBoundaries()
extern "C" Int32U5BU5D_t401* SurfaceImpl_GetMeshBoundaries_m5813 (SurfaceImpl_t1164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.SurfaceImpl::get_BoundingBox()
extern "C" Rect_t267  SurfaceImpl_get_BoundingBox_m5814 (SurfaceImpl_t1164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.SurfaceImpl::GetArea()
extern "C" float SurfaceImpl_GetArea_m5815 (SurfaceImpl_t1164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
