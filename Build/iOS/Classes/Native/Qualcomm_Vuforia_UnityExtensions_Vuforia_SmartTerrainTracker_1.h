﻿#pragma once
#include <stdint.h>
// Vuforia.SmartTerrainBuilderImpl
struct SmartTerrainBuilderImpl_t1165;
// Vuforia.SmartTerrainTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_0.h"
// Vuforia.SmartTerrainTrackerImpl
struct  SmartTerrainTrackerImpl_t1169  : public SmartTerrainTracker_t1168
{
	// System.Single Vuforia.SmartTerrainTrackerImpl::mScaleToMillimeter
	float ___mScaleToMillimeter_1;
	// Vuforia.SmartTerrainBuilderImpl Vuforia.SmartTerrainTrackerImpl::mSmartTerrainBuilder
	SmartTerrainBuilderImpl_t1165 * ___mSmartTerrainBuilder_2;
};
