﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RijndaelManagedTransform
struct RijndaelManagedTransform_t2221;
// System.Security.Cryptography.Rijndael
struct Rijndael_t417;
// System.Byte[]
struct ByteU5BU5D_t139;

// System.Void System.Security.Cryptography.RijndaelManagedTransform::.ctor(System.Security.Cryptography.Rijndael,System.Boolean,System.Byte[],System.Byte[])
extern "C" void RijndaelManagedTransform__ctor_m13168 (RijndaelManagedTransform_t2221 * __this, Rijndael_t417 * ___algo, bool ___encryption, ByteU5BU5D_t139* ___key, ByteU5BU5D_t139* ___iv, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelManagedTransform::System.IDisposable.Dispose()
extern "C" void RijndaelManagedTransform_System_IDisposable_Dispose_m13169 (RijndaelManagedTransform_t2221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RijndaelManagedTransform::get_CanTransformMultipleBlocks()
extern "C" bool RijndaelManagedTransform_get_CanTransformMultipleBlocks_m13170 (RijndaelManagedTransform_t2221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RijndaelManagedTransform::get_CanReuseTransform()
extern "C" bool RijndaelManagedTransform_get_CanReuseTransform_m13171 (RijndaelManagedTransform_t2221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::get_InputBlockSize()
extern "C" int32_t RijndaelManagedTransform_get_InputBlockSize_m13172 (RijndaelManagedTransform_t2221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::get_OutputBlockSize()
extern "C" int32_t RijndaelManagedTransform_get_OutputBlockSize_m13173 (RijndaelManagedTransform_t2221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t RijndaelManagedTransform_TransformBlock_m13174 (RijndaelManagedTransform_t2221 * __this, ByteU5BU5D_t139* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, ByteU5BU5D_t139* ___outputBuffer, int32_t ___outputOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RijndaelManagedTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t139* RijndaelManagedTransform_TransformFinalBlock_m13175 (RijndaelManagedTransform_t2221 * __this, ByteU5BU5D_t139* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
