﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CullingGroup
struct CullingGroup_t815;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.CullingGroup::Finalize()
extern "C" void CullingGroup_Finalize_m3996 (CullingGroup_t815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup::Dispose()
extern "C" void CullingGroup_Dispose_m3997 (CullingGroup_t815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern "C" void CullingGroup_SendEvents_m3998 (Object_t * __this /* static, unused */, CullingGroup_t815 * ___cullingGroup, IntPtr_t ___eventsPtr, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C" void CullingGroup_FinalizerFailure_m3999 (CullingGroup_t815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
