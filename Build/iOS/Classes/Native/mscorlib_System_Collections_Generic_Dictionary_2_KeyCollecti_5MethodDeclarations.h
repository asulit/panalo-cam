﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2504;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_5.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15135_gshared (Enumerator_t2512 * __this, Dictionary_2_t2504 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15135(__this, ___host, method) (( void (*) (Enumerator_t2512 *, Dictionary_2_t2504 *, const MethodInfo*))Enumerator__ctor_m15135_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15136_gshared (Enumerator_t2512 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15136(__this, method) (( Object_t * (*) (Enumerator_t2512 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15136_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15137_gshared (Enumerator_t2512 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15137(__this, method) (( void (*) (Enumerator_t2512 *, const MethodInfo*))Enumerator_Dispose_m15137_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15138_gshared (Enumerator_t2512 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15138(__this, method) (( bool (*) (Enumerator_t2512 *, const MethodInfo*))Enumerator_MoveNext_m15138_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15139_gshared (Enumerator_t2512 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15139(__this, method) (( Object_t * (*) (Enumerator_t2512 *, const MethodInfo*))Enumerator_get_Current_m15139_gshared)(__this, method)
