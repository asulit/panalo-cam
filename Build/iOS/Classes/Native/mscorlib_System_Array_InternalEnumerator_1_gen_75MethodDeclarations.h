﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m23932(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3167 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14611_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23933(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3167 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14612_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>::Dispose()
#define InternalEnumerator_1_Dispose_m23934(__this, method) (( void (*) (InternalEnumerator_1_t3167 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14613_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>::MoveNext()
#define InternalEnumerator_1_MoveNext_m23935(__this, method) (( bool (*) (InternalEnumerator_1_t3167 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14614_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.ExecuteInEditMode>::get_Current()
#define InternalEnumerator_1_get_Current_m23936(__this, method) (( ExecuteInEditMode_t932 * (*) (InternalEnumerator_1_t3167 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14615_gshared)(__this, method)
