﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>
struct ValueCollection_t2716;
// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>
struct Dictionary_2_t208;
// System.Collections.Generic.IEnumerator`1<ERaffleResult>
struct IEnumerator_1_t3662;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// ERaffleResult[]
struct ERaffleResultU5BU5D_t2708;
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_47.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m17657_gshared (ValueCollection_t2716 * __this, Dictionary_2_t208 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m17657(__this, ___dictionary, method) (( void (*) (ValueCollection_t2716 *, Dictionary_2_t208 *, const MethodInfo*))ValueCollection__ctor_m17657_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17658_gshared (ValueCollection_t2716 * __this, int32_t ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17658(__this, ___item, method) (( void (*) (ValueCollection_t2716 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17658_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17659_gshared (ValueCollection_t2716 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17659(__this, method) (( void (*) (ValueCollection_t2716 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17659_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17660_gshared (ValueCollection_t2716 * __this, int32_t ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17660(__this, ___item, method) (( bool (*) (ValueCollection_t2716 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17660_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17661_gshared (ValueCollection_t2716 * __this, int32_t ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17661(__this, ___item, method) (( bool (*) (ValueCollection_t2716 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17661_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17662_gshared (ValueCollection_t2716 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17662(__this, method) (( Object_t* (*) (ValueCollection_t2716 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17662_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m17663_gshared (ValueCollection_t2716 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m17663(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2716 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m17663_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17664_gshared (ValueCollection_t2716 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17664(__this, method) (( Object_t * (*) (ValueCollection_t2716 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17664_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17665_gshared (ValueCollection_t2716 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17665(__this, method) (( bool (*) (ValueCollection_t2716 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17665_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17666_gshared (ValueCollection_t2716 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17666(__this, method) (( bool (*) (ValueCollection_t2716 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17666_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m17667_gshared (ValueCollection_t2716 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m17667(__this, method) (( Object_t * (*) (ValueCollection_t2716 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m17667_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m17668_gshared (ValueCollection_t2716 * __this, ERaffleResultU5BU5D_t2708* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m17668(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2716 *, ERaffleResultU5BU5D_t2708*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m17668_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::GetEnumerator()
extern "C" Enumerator_t2717  ValueCollection_GetEnumerator_m17669_gshared (ValueCollection_t2716 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m17669(__this, method) (( Enumerator_t2717  (*) (ValueCollection_t2716 *, const MethodInfo*))ValueCollection_GetEnumerator_m17669_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m17670_gshared (ValueCollection_t2716 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m17670(__this, method) (( int32_t (*) (ValueCollection_t2716 *, const MethodInfo*))ValueCollection_get_Count_m17670_gshared)(__this, method)
