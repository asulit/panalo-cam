﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Component>::.cctor()
// UnityEngine.UI.ListPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_6MethodDeclarations.h"
#define ListPool_1__cctor_m19083(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m19084_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Component>::Get()
#define ListPool_1_Get_m3507(__this /* static, unused */, method) (( List_1_t718 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m19085_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Component>::Release(System.Collections.Generic.List`1<T>)
#define ListPool_1_Release_m3509(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t718 *, const MethodInfo*))ListPool_1_Release_m19086_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Component>::<s_ListPool>m__15(System.Collections.Generic.List`1<T>)
#define ListPool_1_U3Cs_ListPoolU3Em__15_m19087(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t718 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__15_m19088_gshared)(__this /* static, unused */, ___l, method)
