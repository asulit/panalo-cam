﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.Task`1<System.Object>
struct Task_1_t2649;
// System.Func`1<System.Object>
struct Func_1_t2650;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;

// System.Void UnityThreading.Task`1<System.Object>::.ctor(System.Func`1<T>)
extern "C" void Task_1__ctor_m16945_gshared (Task_1_t2649 * __this, Func_1_t2650 * ___function, const MethodInfo* method);
#define Task_1__ctor_m16945(__this, ___function, method) (( void (*) (Task_1_t2649 *, Func_1_t2650 *, const MethodInfo*))Task_1__ctor_m16945_gshared)(__this, ___function, method)
// System.Collections.IEnumerator UnityThreading.Task`1<System.Object>::Do()
extern "C" Object_t * Task_1_Do_m16946_gshared (Task_1_t2649 * __this, const MethodInfo* method);
#define Task_1_Do_m16946(__this, method) (( Object_t * (*) (Task_1_t2649 *, const MethodInfo*))Task_1_Do_m16946_gshared)(__this, method)
// T UnityThreading.Task`1<System.Object>::get_Result()
extern "C" Object_t * Task_1_get_Result_m16947_gshared (Task_1_t2649 * __this, const MethodInfo* method);
#define Task_1_get_Result_m16947(__this, method) (( Object_t * (*) (Task_1_t2649 *, const MethodInfo*))Task_1_get_Result_m16947_gshared)(__this, method)
