﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Queue`1/Enumerator<Common.Logger.Log>::.ctor(System.Collections.Generic.Queue`1<T>)
// System.Collections.Generic.Queue`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Queue_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m15900(__this, ___q, method) (( void (*) (Enumerator_t2570 *, Queue_1_t76 *, const MethodInfo*))Enumerator__ctor_m15895_gshared)(__this, ___q, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<Common.Logger.Log>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15901(__this, method) (( Object_t * (*) (Enumerator_t2570 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15896_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<Common.Logger.Log>::Dispose()
#define Enumerator_Dispose_m15902(__this, method) (( void (*) (Enumerator_t2570 *, const MethodInfo*))Enumerator_Dispose_m15897_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<Common.Logger.Log>::MoveNext()
#define Enumerator_MoveNext_m15903(__this, method) (( bool (*) (Enumerator_t2570 *, const MethodInfo*))Enumerator_MoveNext_m15898_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<Common.Logger.Log>::get_Current()
#define Enumerator_get_Current_m15904(__this, method) (( Log_t72 * (*) (Enumerator_t2570 *, const MethodInfo*))Enumerator_get_Current_m15899_gshared)(__this, method)
