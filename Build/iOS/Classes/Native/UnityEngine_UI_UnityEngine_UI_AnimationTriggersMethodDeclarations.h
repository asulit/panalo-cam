﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t540;
// System.String
struct String_t;

// System.Void UnityEngine.UI.AnimationTriggers::.ctor()
extern "C" void AnimationTriggers__ctor_m2217 (AnimationTriggers_t540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_normalTrigger()
extern "C" String_t* AnimationTriggers_get_normalTrigger_m2218 (AnimationTriggers_t540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_normalTrigger(System.String)
extern "C" void AnimationTriggers_set_normalTrigger_m2219 (AnimationTriggers_t540 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_highlightedTrigger()
extern "C" String_t* AnimationTriggers_get_highlightedTrigger_m2220 (AnimationTriggers_t540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_highlightedTrigger(System.String)
extern "C" void AnimationTriggers_set_highlightedTrigger_m2221 (AnimationTriggers_t540 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_pressedTrigger()
extern "C" String_t* AnimationTriggers_get_pressedTrigger_m2222 (AnimationTriggers_t540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_pressedTrigger(System.String)
extern "C" void AnimationTriggers_set_pressedTrigger_m2223 (AnimationTriggers_t540 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_disabledTrigger()
extern "C" String_t* AnimationTriggers_get_disabledTrigger_m2224 (AnimationTriggers_t540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_disabledTrigger(System.String)
extern "C" void AnimationTriggers_set_disabledTrigger_m2225 (AnimationTriggers_t540 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
