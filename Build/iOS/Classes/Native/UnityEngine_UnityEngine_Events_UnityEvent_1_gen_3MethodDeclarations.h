﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.Events.UnityEvent`1<System.String>::.ctor()
// UnityEngine.Events.UnityEvent`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_6MethodDeclarations.h"
#define UnityEvent_1__ctor_m3581(__this, method) (( void (*) (UnityEvent_1_t596 *, const MethodInfo*))UnityEvent_1__ctor_m19460_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m21245(__this, ___call, method) (( void (*) (UnityEvent_1_t596 *, UnityAction_1_t2973 *, const MethodInfo*))UnityEvent_1_AddListener_m19462_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_RemoveListener_m21246(__this, ___call, method) (( void (*) (UnityEvent_1_t596 *, UnityAction_1_t2973 *, const MethodInfo*))UnityEvent_1_RemoveListener_m19464_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.String>::FindMethod_Impl(System.String,System.Object)
#define UnityEvent_1_FindMethod_Impl_m21247(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t596 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m19466_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.String>::GetDelegate(System.Object,System.Reflection.MethodInfo)
#define UnityEvent_1_GetDelegate_m21248(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t981 * (*) (UnityEvent_1_t596 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m19468_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.String>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_GetDelegate_m21249(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t981 * (*) (Object_t * /* static, unused */, UnityAction_1_t2973 *, const MethodInfo*))UnityEvent_1_GetDelegate_m19470_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::Invoke(T0)
#define UnityEvent_1_Invoke_m3625(__this, ___arg0, method) (( void (*) (UnityEvent_1_t596 *, String_t*, const MethodInfo*))UnityEvent_1_Invoke_m19471_gshared)(__this, ___arg0, method)
