﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::.ctor(System.Collections.Generic.HashSet`1<T>)
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0MethodDeclarations.h"
#define Enumerator__ctor_m29406(__this, ___hashset, method) (( void (*) (Enumerator_t1393 *, HashSet_1_t1246 *, const MethodInfo*))Enumerator__ctor_m29396_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m29407(__this, method) (( Object_t * (*) (Enumerator_t1393 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m29397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::MoveNext()
#define Enumerator_MoveNext_m7501(__this, method) (( bool (*) (Enumerator_t1393 *, const MethodInfo*))Enumerator_MoveNext_m29398_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::get_Current()
#define Enumerator_get_Current_m7500(__this, method) (( MeshRenderer_t402 * (*) (Enumerator_t1393 *, const MethodInfo*))Enumerator_get_Current_m29399_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::Dispose()
#define Enumerator_Dispose_m7502(__this, method) (( void (*) (Enumerator_t1393 *, const MethodInfo*))Enumerator_Dispose_m29400_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::CheckState()
#define Enumerator_CheckState_m29408(__this, method) (( void (*) (Enumerator_t1393 *, const MethodInfo*))Enumerator_CheckState_m29401_gshared)(__this, method)
