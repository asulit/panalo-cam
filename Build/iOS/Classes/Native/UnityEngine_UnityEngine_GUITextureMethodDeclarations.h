﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUITexture
struct GUITexture_t243;
// UnityEngine.Texture
struct Texture_t103;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// UnityEngine.Color UnityEngine.GUITexture::get_color()
extern "C" Color_t9  GUITexture_get_color_m1723 (GUITexture_t243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_color(UnityEngine.Color)
extern "C" void GUITexture_set_color_m1729 (GUITexture_t243 * __this, Color_t9  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void GUITexture_INTERNAL_get_color_m3955 (GUITexture_t243 * __this, Color_t9 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void GUITexture_INTERNAL_set_color_m3956 (GUITexture_t243 * __this, Color_t9 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_texture(UnityEngine.Texture)
extern "C" void GUITexture_set_texture_m1784 (GUITexture_t243 * __this, Texture_t103 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
