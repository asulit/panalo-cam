﻿#pragma once
#include <stdint.h>
// Common.Logger.LogLevel
struct LogLevel_t73;
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// Common.Logger.Log
struct  Log_t72  : public Object_t
{
	// Common.Logger.LogLevel Common.Logger.Log::level
	LogLevel_t73 * ___level_0;
	// System.String Common.Logger.Log::message
	String_t* ___message_1;
	// System.DateTime Common.Logger.Log::timestamp
	DateTime_t74  ___timestamp_2;
};
