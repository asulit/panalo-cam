﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>
struct KeyCollection_t2670;
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
struct Dictionary_2_t2665;
// System.Collections.Generic.IEnumerator`1<EScreens>
struct IEnumerator_1_t3651;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// EScreens[]
struct EScreensU5BU5D_t2662;
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_17.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void KeyCollection__ctor_m17196_gshared (KeyCollection_t2670 * __this, Dictionary_2_t2665 * ___dictionary, const MethodInfo* method);
#define KeyCollection__ctor_m17196(__this, ___dictionary, method) (( void (*) (KeyCollection_t2670 *, Dictionary_2_t2665 *, const MethodInfo*))KeyCollection__ctor_m17196_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17197_gshared (KeyCollection_t2670 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17197(__this, ___item, method) (( void (*) (KeyCollection_t2670 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17197_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17198_gshared (KeyCollection_t2670 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17198(__this, method) (( void (*) (KeyCollection_t2670 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17199_gshared (KeyCollection_t2670 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17199(__this, ___item, method) (( bool (*) (KeyCollection_t2670 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17199_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17200_gshared (KeyCollection_t2670 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17200(__this, ___item, method) (( bool (*) (KeyCollection_t2670 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17200_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17201_gshared (KeyCollection_t2670 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17201(__this, method) (( Object_t* (*) (KeyCollection_t2670 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17201_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m17202_gshared (KeyCollection_t2670 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m17202(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2670 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m17202_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17203_gshared (KeyCollection_t2670 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17203(__this, method) (( Object_t * (*) (KeyCollection_t2670 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17203_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17204_gshared (KeyCollection_t2670 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17204(__this, method) (( bool (*) (KeyCollection_t2670 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17205_gshared (KeyCollection_t2670 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17205(__this, method) (( bool (*) (KeyCollection_t2670 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17205_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m17206_gshared (KeyCollection_t2670 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m17206(__this, method) (( Object_t * (*) (KeyCollection_t2670 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m17206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m17207_gshared (KeyCollection_t2670 * __this, EScreensU5BU5D_t2662* ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_CopyTo_m17207(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2670 *, EScreensU5BU5D_t2662*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m17207_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::GetEnumerator()
extern "C" Enumerator_t2671  KeyCollection_GetEnumerator_m17208_gshared (KeyCollection_t2670 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m17208(__this, method) (( Enumerator_t2671  (*) (KeyCollection_t2670 *, const MethodInfo*))KeyCollection_GetEnumerator_m17208_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m17209_gshared (KeyCollection_t2670 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m17209(__this, method) (( int32_t (*) (KeyCollection_t2670 *, const MethodInfo*))KeyCollection_get_Count_m17209_gshared)(__this, method)
