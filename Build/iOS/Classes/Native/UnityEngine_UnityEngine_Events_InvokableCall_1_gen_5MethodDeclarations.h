﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.Events.InvokableCall`1<System.String>::.ctor(System.Object,System.Reflection.MethodInfo)
// UnityEngine.Events.InvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_genMethodDeclarations.h"
#define InvokableCall_1__ctor_m24326(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t3208 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m19472_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.String>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
#define InvokableCall_1__ctor_m24327(__this, ___action, method) (( void (*) (InvokableCall_1_t3208 *, UnityAction_1_t2973 *, const MethodInfo*))InvokableCall_1__ctor_m19473_gshared)(__this, ___action, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.String>::Invoke(System.Object[])
#define InvokableCall_1_Invoke_m24328(__this, ___args, method) (( void (*) (InvokableCall_1_t3208 *, ObjectU5BU5D_t356*, const MethodInfo*))InvokableCall_1_Invoke_m19474_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.String>::Find(System.Object,System.Reflection.MethodInfo)
#define InvokableCall_1_Find_m24329(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t3208 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m19475_gshared)(__this, ___targetObj, ___method, method)
