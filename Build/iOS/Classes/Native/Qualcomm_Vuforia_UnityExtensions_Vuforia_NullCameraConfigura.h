﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// Vuforia.NullCameraConfiguration
struct  NullCameraConfiguration_t1056  : public Object_t
{
	// UnityEngine.ScreenOrientation Vuforia.NullCameraConfiguration::mProjectionOrientation
	int32_t ___mProjectionOrientation_0;
};
