﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.IClippable
struct IClippable_t713;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.IClippable>
struct  Comparison_1_t2985  : public MulticastDelegate_t28
{
};
