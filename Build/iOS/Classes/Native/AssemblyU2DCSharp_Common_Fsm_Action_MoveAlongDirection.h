﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t35;
// Common.Time.TimeReference
struct TimeReference_t14;
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapter.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Common.Fsm.Action.MoveAlongDirection
struct  MoveAlongDirection_t37  : public FsmActionAdapter_t33
{
	// UnityEngine.Transform Common.Fsm.Action.MoveAlongDirection::actor
	Transform_t35 * ___actor_1;
	// UnityEngine.Vector3 Common.Fsm.Action.MoveAlongDirection::direction
	Vector3_t36  ___direction_2;
	// System.Single Common.Fsm.Action.MoveAlongDirection::velocity
	float ___velocity_3;
	// Common.Time.TimeReference Common.Fsm.Action.MoveAlongDirection::timeReference
	TimeReference_t14 * ___timeReference_4;
};
