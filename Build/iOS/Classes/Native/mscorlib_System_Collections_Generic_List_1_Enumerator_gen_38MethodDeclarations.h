﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>
struct List_1_t214;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_38.h"
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m17758_gshared (Enumerator_t2725 * __this, List_1_t214 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m17758(__this, ___l, method) (( void (*) (Enumerator_t2725 *, List_1_t214 *, const MethodInfo*))Enumerator__ctor_m17758_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17759_gshared (Enumerator_t2725 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17759(__this, method) (( Object_t * (*) (Enumerator_t2725 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17759_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>::Dispose()
extern "C" void Enumerator_Dispose_m17760_gshared (Enumerator_t2725 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17760(__this, method) (( void (*) (Enumerator_t2725 *, const MethodInfo*))Enumerator_Dispose_m17760_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>::VerifyState()
extern "C" void Enumerator_VerifyState_m17761_gshared (Enumerator_t2725 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17761(__this, method) (( void (*) (Enumerator_t2725 *, const MethodInfo*))Enumerator_VerifyState_m17761_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17762_gshared (Enumerator_t2725 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17762(__this, method) (( bool (*) (Enumerator_t2725 *, const MethodInfo*))Enumerator_MoveNext_m17762_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>::get_Current()
extern "C" int32_t Enumerator_get_Current_m17763_gshared (Enumerator_t2725 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17763(__this, method) (( int32_t (*) (Enumerator_t2725 *, const MethodInfo*))Enumerator_get_Current_m17763_gshared)(__this, method)
