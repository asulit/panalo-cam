﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreadHelper/<CreateThread>c__AnonStorey14
struct U3CCreateThreadU3Ec__AnonStorey14_t177;
// UnityThreading.ActionThread
struct ActionThread_t171;

// System.Void UnityThreadHelper/<CreateThread>c__AnonStorey14::.ctor()
extern "C" void U3CCreateThreadU3Ec__AnonStorey14__ctor_m605 (U3CCreateThreadU3Ec__AnonStorey14_t177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreadHelper/<CreateThread>c__AnonStorey14::<>m__8(UnityThreading.ActionThread)
extern "C" void U3CCreateThreadU3Ec__AnonStorey14_U3CU3Em__8_m606 (U3CCreateThreadU3Ec__AnonStorey14_t177 * __this, ActionThread_t171 * ___thread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
