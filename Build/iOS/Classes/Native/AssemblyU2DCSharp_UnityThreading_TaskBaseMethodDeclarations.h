﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.TaskBase
struct TaskBase_t163;

// System.Void UnityThreading.TaskBase::.ctor()
extern "C" void TaskBase__ctor_m550 (TaskBase_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityThreading.TaskBase::get_ShouldAbort()
extern "C" bool TaskBase_get_ShouldAbort_m551 (TaskBase_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityThreading.TaskBase::get_HasEnded()
extern "C" bool TaskBase_get_HasEnded_m552 (TaskBase_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityThreading.TaskBase::get_IsSucceeded()
extern "C" bool TaskBase_get_IsSucceeded_m553 (TaskBase_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityThreading.TaskBase::get_IsFailed()
extern "C" bool TaskBase_get_IsFailed_m554 (TaskBase_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskBase::Abort()
extern "C" void TaskBase_Abort_m555 (TaskBase_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskBase::AbortWait()
extern "C" void TaskBase_AbortWait_m556 (TaskBase_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskBase::AbortWaitForSeconds(System.Single)
extern "C" void TaskBase_AbortWaitForSeconds_m557 (TaskBase_t163 * __this, float ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskBase::Wait()
extern "C" void TaskBase_Wait_m558 (TaskBase_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskBase::WaitForSeconds(System.Single)
extern "C" void TaskBase_WaitForSeconds_m559 (TaskBase_t163 * __this, float ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskBase::DoInternal()
extern "C" void TaskBase_DoInternal_m560 (TaskBase_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskBase::Dispose()
extern "C" void TaskBase_Dispose_m561 (TaskBase_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
