﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimatorFrame>
struct DefaultComparer_t2776;
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimatorFrame>::.ctor()
extern "C" void DefaultComparer__ctor_m18420_gshared (DefaultComparer_t2776 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m18420(__this, method) (( void (*) (DefaultComparer_t2776 *, const MethodInfo*))DefaultComparer__ctor_m18420_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimatorFrame>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m18421_gshared (DefaultComparer_t2776 * __this, AnimatorFrame_t235  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m18421(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2776 *, AnimatorFrame_t235 , const MethodInfo*))DefaultComparer_GetHashCode_m18421_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<AnimatorFrame>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m18422_gshared (DefaultComparer_t2776 * __this, AnimatorFrame_t235  ___x, AnimatorFrame_t235  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m18422(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2776 *, AnimatorFrame_t235 , AnimatorFrame_t235 , const MethodInfo*))DefaultComparer_Equals_m18422_gshared)(__this, ___x, ___y, method)
