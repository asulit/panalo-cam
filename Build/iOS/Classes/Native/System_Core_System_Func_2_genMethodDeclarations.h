﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Object>
#include "System_Core_System_Func_2_gen_4MethodDeclarations.h"
#define Func_2__ctor_m1622(__this, ___object, ___method, method) (( void (*) (Func_2_t174 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m16972_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>::Invoke(T)
#define Func_2_Invoke_m1615(__this, ___arg1, method) (( Object_t * (*) (Func_2_t174 *, ThreadBase_t169 *, const MethodInfo*))Func_2_Invoke_m16973_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m16974(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t174 *, ThreadBase_t169 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m16975_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m16976(__this, ___result, method) (( Object_t * (*) (Func_2_t174 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m16977_gshared)(__this, ___result, method)
