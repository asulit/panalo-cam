﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::.ctor(System.Object,System.IntPtr)
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.Object,System.Boolean>
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate_2_ge_0MethodDeclarations.h"
#define UnityAdsDelegate_2__ctor_m23218(__this, ___object, ___method, method) (( void (*) (UnityAdsDelegate_2_t859 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAdsDelegate_2__ctor_m23219_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::Invoke(T1,T2)
#define UnityAdsDelegate_2_Invoke_m5137(__this, ___p1, ___p2, method) (( void (*) (UnityAdsDelegate_2_t859 *, String_t*, bool, const MethodInfo*))UnityAdsDelegate_2_Invoke_m23220_gshared)(__this, ___p1, ___p2, method)
// System.IAsyncResult UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define UnityAdsDelegate_2_BeginInvoke_m23221(__this, ___p1, ___p2, ___callback, ___object, method) (( Object_t * (*) (UnityAdsDelegate_2_t859 *, String_t*, bool, AsyncCallback_t31 *, Object_t *, const MethodInfo*))UnityAdsDelegate_2_BeginInvoke_m23222_gshared)(__this, ___p1, ___p2, ___callback, ___object, method)
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::EndInvoke(System.IAsyncResult)
#define UnityAdsDelegate_2_EndInvoke_m23223(__this, ___result, method) (( void (*) (UnityAdsDelegate_2_t859 *, Object_t *, const MethodInfo*))UnityAdsDelegate_2_EndInvoke_m23224_gshared)(__this, ___result, method)
