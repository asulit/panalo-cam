﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<AnimatorFrame>
struct EqualityComparer_1_t2775;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<AnimatorFrame>::.ctor()
extern "C" void EqualityComparer_1__ctor_m18415_gshared (EqualityComparer_1_t2775 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m18415(__this, method) (( void (*) (EqualityComparer_1_t2775 *, const MethodInfo*))EqualityComparer_1__ctor_m18415_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<AnimatorFrame>::.cctor()
extern "C" void EqualityComparer_1__cctor_m18416_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m18416(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m18416_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<AnimatorFrame>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18417_gshared (EqualityComparer_1_t2775 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18417(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t2775 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18417_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<AnimatorFrame>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18418_gshared (EqualityComparer_1_t2775 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18418(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t2775 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18418_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<AnimatorFrame>::get_Default()
extern "C" EqualityComparer_1_t2775 * EqualityComparer_1_get_Default_m18419_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m18419(__this /* static, unused */, method) (( EqualityComparer_1_t2775 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m18419_gshared)(__this /* static, unused */, method)
