﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Vector4>
struct Comparer_1_t3067;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Vector4>
struct  Comparer_1_t3067  : public Object_t
{
};
struct Comparer_1_t3067_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::_default
	Comparer_1_t3067 * ____default_0;
};
