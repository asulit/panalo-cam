﻿#pragma once
#include <stdint.h>
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t907;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.GUILayoutEntry>
struct  Predicate_1_t3159  : public MulticastDelegate_t28
{
};
