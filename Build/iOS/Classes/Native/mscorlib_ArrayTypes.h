﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// System.Object[]
// System.Object[]
struct  ObjectU5BU5D_t356  : public Array_t
{
};
// System.Type[]
// System.Type[]
struct  TypeU5BU5D_t1005  : public Array_t
{
};
struct TypeU5BU5D_t1005_StaticFields{
};
// System.Reflection.IReflect[]
// System.Reflection.IReflect[]
struct  IReflectU5BU5D_t3804  : public Array_t
{
};
// System.Runtime.InteropServices._Type[]
// System.Runtime.InteropServices._Type[]
struct  _TypeU5BU5D_t3805  : public Array_t
{
};
// System.Reflection.MemberInfo[]
// System.Reflection.MemberInfo[]
struct  MemberInfoU5BU5D_t2169  : public Array_t
{
};
// System.Reflection.ICustomAttributeProvider[]
// System.Reflection.ICustomAttributeProvider[]
struct  ICustomAttributeProviderU5BU5D_t3806  : public Array_t
{
};
// System.Runtime.InteropServices._MemberInfo[]
// System.Runtime.InteropServices._MemberInfo[]
struct  _MemberInfoU5BU5D_t3807  : public Array_t
{
};
// System.Int32[]
// System.Int32[]
struct  Int32U5BU5D_t401  : public Array_t
{
};
// System.IFormattable[]
// System.IFormattable[]
struct  IFormattableU5BU5D_t3808  : public Array_t
{
};
// System.IConvertible[]
// System.IConvertible[]
struct  IConvertibleU5BU5D_t3809  : public Array_t
{
};
// System.IComparable[]
// System.IComparable[]
struct  IComparableU5BU5D_t3810  : public Array_t
{
};
// System.IComparable`1<System.Int32>[]
// System.IComparable`1<System.Int32>[]
struct  IComparable_1U5BU5D_t3811  : public Array_t
{
};
// System.IEquatable`1<System.Int32>[]
// System.IEquatable`1<System.Int32>[]
struct  IEquatable_1U5BU5D_t3812  : public Array_t
{
};
// System.ValueType[]
// System.ValueType[]
struct  ValueTypeU5BU5D_t3813  : public Array_t
{
};
// System.Double[]
// System.Double[]
struct  DoubleU5BU5D_t2411  : public Array_t
{
};
// System.IComparable`1<System.Double>[]
// System.IComparable`1<System.Double>[]
struct  IComparable_1U5BU5D_t3814  : public Array_t
{
};
// System.IEquatable`1<System.Double>[]
// System.IEquatable`1<System.Double>[]
struct  IEquatable_1U5BU5D_t3815  : public Array_t
{
};
// System.Char[]
// System.Char[]
struct  CharU5BU5D_t385  : public Array_t
{
};
struct CharU5BU5D_t385_StaticFields{
};
// System.IComparable`1<System.Char>[]
// System.IComparable`1<System.Char>[]
struct  IComparable_1U5BU5D_t3816  : public Array_t
{
};
// System.IEquatable`1<System.Char>[]
// System.IEquatable`1<System.Char>[]
struct  IEquatable_1U5BU5D_t3817  : public Array_t
{
};
// System.Enum[]
// System.Enum[]
struct  EnumU5BU5D_t338  : public Array_t
{
};
struct EnumU5BU5D_t338_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3619  : public Array_t
{
};
// System.String[]
// System.String[]
struct  StringU5BU5D_t137  : public Array_t
{
};
struct StringU5BU5D_t137_StaticFields{
};
// System.Collections.IEnumerable[]
// System.Collections.IEnumerable[]
struct  IEnumerableU5BU5D_t3818  : public Array_t
{
};
// System.ICloneable[]
// System.ICloneable[]
struct  ICloneableU5BU5D_t3819  : public Array_t
{
};
// System.IComparable`1<System.String>[]
// System.IComparable`1<System.String>[]
struct  IComparable_1U5BU5D_t3820  : public Array_t
{
};
// System.IEquatable`1<System.String>[]
// System.IEquatable`1<System.String>[]
struct  IEquatable_1U5BU5D_t3821  : public Array_t
{
};
// System.Collections.Generic.Link[]
// System.Collections.Generic.Link[]
struct  LinkU5BU5D_t2500  : public Array_t
{
};
// System.Collections.DictionaryEntry[]
// System.Collections.DictionaryEntry[]
struct  DictionaryEntryU5BU5D_t3822  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>[]
struct  KeyValuePair_2U5BU5D_t3618  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t3624  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t3623  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3629  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Query.QueryResultResolver>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Query.QueryResultResolver>[]
struct  KeyValuePair_2U5BU5D_t3633  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Signal.Signal>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Signal.Signal>[]
struct  KeyValuePair_2U5BU5D_t3636  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Time.TimeReference>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Time.TimeReference>[]
struct  KeyValuePair_2U5BU5D_t3639  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3642  : public Array_t
{
};
// System.Byte[]
// System.Byte[]
struct  ByteU5BU5D_t139  : public Array_t
{
};
// System.IComparable`1<System.Byte>[]
// System.IComparable`1<System.Byte>[]
struct  IComparable_1U5BU5D_t3823  : public Array_t
{
};
// System.IEquatable`1<System.Byte>[]
// System.IEquatable`1<System.Byte>[]
struct  IEquatable_1U5BU5D_t3824  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct  KeyValuePair_2U5BU5D_t3645  : public Array_t
{
};
// System.Threading.WaitHandle[]
// System.Threading.WaitHandle[]
struct  WaitHandleU5BU5D_t426  : public Array_t
{
};
struct WaitHandleU5BU5D_t426_StaticFields{
};
// System.IDisposable[]
// System.IDisposable[]
struct  IDisposableU5BU5D_t3825  : public Array_t
{
};
// System.MarshalByRefObject[]
// System.MarshalByRefObject[]
struct  MarshalByRefObjectU5BU5D_t3826  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3649  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<EScreens,System.String>[]
// System.Collections.Generic.KeyValuePair`2<EScreens,System.String>[]
struct  KeyValuePair_2U5BU5D_t3648  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3655  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>[]
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>[]
struct  KeyValuePair_2U5BU5D_t3654  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>[]
struct  KeyValuePair_2U5BU5D_t3660  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct  KeyValuePair_2U5BU5D_t3667  : public Array_t
{
};
// System.Boolean[]
// System.Boolean[]
struct  BooleanU5BU5D_t1640  : public Array_t
{
};
struct BooleanU5BU5D_t1640_StaticFields{
};
// System.IComparable`1<System.Boolean>[]
// System.IComparable`1<System.Boolean>[]
struct  IComparable_1U5BU5D_t3827  : public Array_t
{
};
// System.IEquatable`1<System.Boolean>[]
// System.IEquatable`1<System.Boolean>[]
struct  IEquatable_1U5BU5D_t3828  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.Boolean>[]
struct  KeyValuePair_2U5BU5D_t3666  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.String>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.String>[]
struct  KeyValuePair_2U5BU5D_t3672  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>[]
struct  KeyValuePair_2U5BU5D_t3675  : public Array_t
{
};
// System.Collections.Hashtable[]
// System.Collections.Hashtable[]
struct  HashtableU5BU5D_t2786  : public Array_t
{
};
struct HashtableU5BU5D_t2786_StaticFields{
};
// System.Single[]
// System.Single[]
struct  SingleU5BU5D_t264  : public Array_t
{
};
// System.IComparable`1<System.Single>[]
// System.IComparable`1<System.Single>[]
struct  IComparable_1U5BU5D_t3829  : public Array_t
{
};
// System.IEquatable`1<System.Single>[]
// System.IEquatable`1<System.Single>[]
struct  IEquatable_1U5BU5D_t3830  : public Array_t
{
};
// System.Reflection.MethodInfo[]
// System.Reflection.MethodInfo[]
struct  MethodInfoU5BU5D_t464  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
struct  List_1U5BU5D_t2817  : public Array_t
{
};
struct List_1U5BU5D_t2817_StaticFields{
};
// System.Collections.Generic.List`1<System.Object>[]
// System.Collections.Generic.List`1<System.Object>[]
struct  List_1U5BU5D_t2828  : public Array_t
{
};
struct List_1U5BU5D_t2828_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Component>[]
// System.Collections.Generic.List`1<UnityEngine.Component>[]
struct  List_1U5BU5D_t2830  : public Array_t
{
};
struct List_1U5BU5D_t2830_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3687  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
struct  KeyValuePair_2U5BU5D_t3686  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
struct  List_1U5BU5D_t2922  : public Array_t
{
};
struct List_1U5BU5D_t2922_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
struct  List_1U5BU5D_t2925  : public Array_t
{
};
struct List_1U5BU5D_t2925_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
struct  KeyValuePair_2U5BU5D_t3693  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
struct  KeyValuePair_2U5BU5D_t3698  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t3699  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t3692  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.IClipper,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t3708  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.Vector3>[]
// System.Collections.Generic.List`1<UnityEngine.Vector3>[]
struct  List_1U5BU5D_t3081  : public Array_t
{
};
struct List_1U5BU5D_t3081_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Color32>[]
// System.Collections.Generic.List`1<UnityEngine.Color32>[]
struct  List_1U5BU5D_t3085  : public Array_t
{
};
struct List_1U5BU5D_t3085_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Vector2>[]
// System.Collections.Generic.List`1<UnityEngine.Vector2>[]
struct  List_1U5BU5D_t3089  : public Array_t
{
};
struct List_1U5BU5D_t3089_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Vector4>[]
// System.Collections.Generic.List`1<UnityEngine.Vector4>[]
struct  List_1U5BU5D_t3093  : public Array_t
{
};
struct List_1U5BU5D_t3093_StaticFields{
};
// System.Collections.Generic.List`1<System.Int32>[]
// System.Collections.Generic.List`1<System.Int32>[]
struct  List_1U5BU5D_t3097  : public Array_t
{
};
struct List_1U5BU5D_t3097_StaticFields{
};
// System.Byte[][]
// System.Byte[][]
struct  ByteU5BU5DU5BU5D_t1578  : public Array_t
{
};
// System.IntPtr[]
// System.IntPtr[]
struct  IntPtrU5BU5D_t1004  : public Array_t
{
};
struct IntPtrU5BU5D_t1004_StaticFields{
};
// System.Runtime.Serialization.ISerializable[]
// System.Runtime.Serialization.ISerializable[]
struct  ISerializableU5BU5D_t3831  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct  KeyValuePair_2U5BU5D_t3733  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct  KeyValuePair_2U5BU5D_t3736  : public Array_t
{
};
// System.Attribute[]
// System.Attribute[]
struct  AttributeU5BU5D_t3832  : public Array_t
{
};
// System.Runtime.InteropServices._Attribute[]
// System.Runtime.InteropServices._Attribute[]
struct  _AttributeU5BU5D_t3833  : public Array_t
{
};
// System.Reflection.ParameterModifier[]
// System.Reflection.ParameterModifier[]
struct  ParameterModifierU5BU5D_t1030  : public Array_t
{
};
// System.Reflection.ParameterInfo[]
// System.Reflection.ParameterInfo[]
struct  ParameterInfoU5BU5D_t1034  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterInfo[]
// System.Runtime.InteropServices._ParameterInfo[]
struct  _ParameterInfoU5BU5D_t3834  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct  KeyValuePair_2U5BU5D_t3739  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct  KeyValuePair_2U5BU5D_t3738  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t3744  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3747  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
struct  KeyValuePair_2U5BU5D_t3746  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
struct  KeyValuePair_2U5BU5D_t3753  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
struct  KeyValuePair_2U5BU5D_t3756  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
struct  KeyValuePair_2U5BU5D_t3758  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct  KeyValuePair_2U5BU5D_t3761  : public Array_t
{
};
// System.UInt16[]
// System.UInt16[]
struct  UInt16U5BU5D_t1635  : public Array_t
{
};
// System.IComparable`1<System.UInt16>[]
// System.IComparable`1<System.UInt16>[]
struct  IComparable_1U5BU5D_t3835  : public Array_t
{
};
// System.IEquatable`1<System.UInt16>[]
// System.IEquatable`1<System.UInt16>[]
struct  IEquatable_1U5BU5D_t3836  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
struct  KeyValuePair_2U5BU5D_t3760  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
struct  KeyValuePair_2U5BU5D_t3766  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t3769  : public Array_t
{
};
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
struct  List_1U5BU5D_t3376  : public Array_t
{
};
struct List_1U5BU5D_t3376_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
struct  KeyValuePair_2U5BU5D_t3771  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
struct  KeyValuePair_2U5BU5D_t3773  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t3775  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
struct  KeyValuePair_2U5BU5D_t3777  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t3779  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
struct  KeyValuePair_2U5BU5D_t3781  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>[]
struct  KeyValuePair_2U5BU5D_t3783  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>[]
struct  KeyValuePair_2U5BU5D_t3786  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
struct  KeyValuePair_2U5BU5D_t3790  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct  KeyValuePair_2U5BU5D_t3793  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
struct  KeyValuePair_2U5BU5D_t3792  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t3798  : public Array_t
{
};
// System.Security.Cryptography.KeySizes[]
// System.Security.Cryptography.KeySizes[]
struct  KeySizesU5BU5D_t1422  : public Array_t
{
};
// System.UInt32[]
// System.UInt32[]
struct  UInt32U5BU5D_t1409  : public Array_t
{
};
// System.IComparable`1<System.UInt32>[]
// System.IComparable`1<System.UInt32>[]
struct  IComparable_1U5BU5D_t3837  : public Array_t
{
};
// System.IEquatable`1<System.UInt32>[]
// System.IEquatable`1<System.UInt32>[]
struct  IEquatable_1U5BU5D_t3838  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509Certificate[]
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct  X509CertificateU5BU5D_t1590  : public Array_t
{
};
// System.Runtime.Serialization.IDeserializationCallback[]
// System.Runtime.Serialization.IDeserializationCallback[]
struct  IDeserializationCallbackU5BU5D_t3839  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct  KeyValuePair_2U5BU5D_t3801  : public Array_t
{
};
// System.Single[,]
// System.Single[,]
struct  SingleU5BU2CU5D_t3840  : public Array_t
{
};
// System.Delegate[]
// System.Delegate[]
struct  DelegateU5BU5D_t2409  : public Array_t
{
};
// System.UInt64[]
// System.UInt64[]
struct  UInt64U5BU5D_t2228  : public Array_t
{
};
// System.IComparable`1<System.UInt64>[]
// System.IComparable`1<System.UInt64>[]
struct  IComparable_1U5BU5D_t3841  : public Array_t
{
};
// System.IEquatable`1<System.UInt64>[]
// System.IEquatable`1<System.UInt64>[]
struct  IEquatable_1U5BU5D_t3842  : public Array_t
{
};
// System.Int16[]
// System.Int16[]
struct  Int16U5BU5D_t2430  : public Array_t
{
};
// System.IComparable`1<System.Int16>[]
// System.IComparable`1<System.Int16>[]
struct  IComparable_1U5BU5D_t3843  : public Array_t
{
};
// System.IEquatable`1<System.Int16>[]
// System.IEquatable`1<System.Int16>[]
struct  IEquatable_1U5BU5D_t3844  : public Array_t
{
};
// System.SByte[]
// System.SByte[]
struct  SByteU5BU5D_t2284  : public Array_t
{
};
// System.IComparable`1<System.SByte>[]
// System.IComparable`1<System.SByte>[]
struct  IComparable_1U5BU5D_t3845  : public Array_t
{
};
// System.IEquatable`1<System.SByte>[]
// System.IEquatable`1<System.SByte>[]
struct  IEquatable_1U5BU5D_t3846  : public Array_t
{
};
// System.Int64[]
// System.Int64[]
struct  Int64U5BU5D_t2410  : public Array_t
{
};
// System.IComparable`1<System.Int64>[]
// System.IComparable`1<System.Int64>[]
struct  IComparable_1U5BU5D_t3847  : public Array_t
{
};
// System.IEquatable`1<System.Int64>[]
// System.IEquatable`1<System.Int64>[]
struct  IEquatable_1U5BU5D_t3848  : public Array_t
{
};
// System.Reflection.FieldInfo[]
// System.Reflection.FieldInfo[]
struct  FieldInfoU5BU5D_t2415  : public Array_t
{
};
// System.Runtime.InteropServices._FieldInfo[]
// System.Runtime.InteropServices._FieldInfo[]
struct  _FieldInfoU5BU5D_t3849  : public Array_t
{
};
// System.Reflection.ConstructorInfo[]
// System.Reflection.ConstructorInfo[]
struct  ConstructorInfoU5BU5D_t2414  : public Array_t
{
};
struct ConstructorInfoU5BU5D_t2414_StaticFields{
};
// System.Runtime.InteropServices._ConstructorInfo[]
// System.Runtime.InteropServices._ConstructorInfo[]
struct  _ConstructorInfoU5BU5D_t3850  : public Array_t
{
};
// System.Reflection.MethodBase[]
// System.Reflection.MethodBase[]
struct  MethodBaseU5BU5D_t2416  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBase[]
// System.Runtime.InteropServices._MethodBase[]
struct  _MethodBaseU5BU5D_t3851  : public Array_t
{
};
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct  TableRangeU5BU5D_t1804  : public Array_t
{
};
// Mono.Globalization.Unicode.TailoringInfo[]
// Mono.Globalization.Unicode.TailoringInfo[]
struct  TailoringInfoU5BU5D_t1811  : public Array_t
{
};
// Mono.Globalization.Unicode.Contraction[]
// Mono.Globalization.Unicode.Contraction[]
struct  ContractionU5BU5D_t1819  : public Array_t
{
};
// Mono.Globalization.Unicode.Level2Map[]
// Mono.Globalization.Unicode.Level2Map[]
struct  Level2MapU5BU5D_t1820  : public Array_t
{
};
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct  BigIntegerU5BU5D_t2412  : public Array_t
{
};
struct BigIntegerU5BU5D_t2412_StaticFields{
};
// System.Collections.Hashtable/Slot[]
// System.Collections.Hashtable/Slot[]
struct  SlotU5BU5D_t1885  : public Array_t
{
};
// System.Collections.SortedList/Slot[]
// System.Collections.SortedList/Slot[]
struct  SlotU5BU5D_t1892  : public Array_t
{
};
// System.Diagnostics.StackFrame[]
// System.Diagnostics.StackFrame[]
struct  StackFrameU5BU5D_t1901  : public Array_t
{
};
// System.Globalization.Calendar[]
// System.Globalization.Calendar[]
struct  CalendarU5BU5D_t1909  : public Array_t
{
};
// System.Reflection.Emit.ModuleBuilder[]
// System.Reflection.Emit.ModuleBuilder[]
struct  ModuleBuilderU5BU5D_t1952  : public Array_t
{
};
struct ModuleBuilderU5BU5D_t1952_StaticFields{
};
// System.Runtime.InteropServices._ModuleBuilder[]
// System.Runtime.InteropServices._ModuleBuilder[]
struct  _ModuleBuilderU5BU5D_t3852  : public Array_t
{
};
// System.Reflection.Module[]
// System.Reflection.Module[]
struct  ModuleU5BU5D_t1953  : public Array_t
{
};
struct ModuleU5BU5D_t1953_StaticFields{
};
// System.Runtime.InteropServices._Module[]
// System.Runtime.InteropServices._Module[]
struct  _ModuleU5BU5D_t3853  : public Array_t
{
};
// System.Reflection.Emit.ParameterBuilder[]
// System.Reflection.Emit.ParameterBuilder[]
struct  ParameterBuilderU5BU5D_t1957  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterBuilder[]
// System.Runtime.InteropServices._ParameterBuilder[]
struct  _ParameterBuilderU5BU5D_t3854  : public Array_t
{
};
// System.Type[][]
// System.Type[][]
struct  TypeU5BU5DU5BU5D_t1958  : public Array_t
{
};
struct TypeU5BU5DU5BU5D_t1958_StaticFields{
};
// System.Array[]
// System.Array[]
struct  ArrayU5BU5D_t3855  : public Array_t
{
};
// System.Collections.ICollection[]
// System.Collections.ICollection[]
struct  ICollectionU5BU5D_t3856  : public Array_t
{
};
// System.Collections.IList[]
// System.Collections.IList[]
struct  IListU5BU5D_t3857  : public Array_t
{
};
// System.Reflection.Emit.ILTokenInfo[]
// System.Reflection.Emit.ILTokenInfo[]
struct  ILTokenInfoU5BU5D_t1967  : public Array_t
{
};
// System.Reflection.Emit.ILGenerator/LabelData[]
// System.Reflection.Emit.ILGenerator/LabelData[]
struct  LabelDataU5BU5D_t1968  : public Array_t
{
};
// System.Reflection.Emit.ILGenerator/LabelFixup[]
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct  LabelFixupU5BU5D_t1969  : public Array_t
{
};
// System.Reflection.Emit.GenericTypeParameterBuilder[]
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct  GenericTypeParameterBuilderU5BU5D_t1972  : public Array_t
{
};
// System.Reflection.Emit.TypeBuilder[]
// System.Reflection.Emit.TypeBuilder[]
struct  TypeBuilderU5BU5D_t1975  : public Array_t
{
};
// System.Runtime.InteropServices._TypeBuilder[]
// System.Runtime.InteropServices._TypeBuilder[]
struct  _TypeBuilderU5BU5D_t3858  : public Array_t
{
};
// System.Reflection.Emit.MethodBuilder[]
// System.Reflection.Emit.MethodBuilder[]
struct  MethodBuilderU5BU5D_t1982  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBuilder[]
// System.Runtime.InteropServices._MethodBuilder[]
struct  _MethodBuilderU5BU5D_t3859  : public Array_t
{
};
// System.Runtime.InteropServices._MethodInfo[]
// System.Runtime.InteropServices._MethodInfo[]
struct  _MethodInfoU5BU5D_t3860  : public Array_t
{
};
// System.Reflection.Emit.ConstructorBuilder[]
// System.Reflection.Emit.ConstructorBuilder[]
struct  ConstructorBuilderU5BU5D_t1983  : public Array_t
{
};
// System.Runtime.InteropServices._ConstructorBuilder[]
// System.Runtime.InteropServices._ConstructorBuilder[]
struct  _ConstructorBuilderU5BU5D_t3861  : public Array_t
{
};
// System.Reflection.Emit.FieldBuilder[]
// System.Reflection.Emit.FieldBuilder[]
struct  FieldBuilderU5BU5D_t1984  : public Array_t
{
};
// System.Runtime.InteropServices._FieldBuilder[]
// System.Runtime.InteropServices._FieldBuilder[]
struct  _FieldBuilderU5BU5D_t3862  : public Array_t
{
};
// System.Reflection.PropertyInfo[]
// System.Reflection.PropertyInfo[]
struct  PropertyInfoU5BU5D_t2417  : public Array_t
{
};
// System.Runtime.InteropServices._PropertyInfo[]
// System.Runtime.InteropServices._PropertyInfo[]
struct  _PropertyInfoU5BU5D_t3863  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.IContextProperty[]
// System.Runtime.Remoting.Contexts.IContextProperty[]
struct  IContextPropertyU5BU5D_t2421  : public Array_t
{
};
// System.Runtime.Remoting.Messaging.Header[]
// System.Runtime.Remoting.Messaging.Header[]
struct  HeaderU5BU5D_t2383  : public Array_t
{
};
// System.Runtime.Remoting.Services.ITrackingHandler[]
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct  ITrackingHandlerU5BU5D_t2448  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.IContextAttribute[]
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct  IContextAttributeU5BU5D_t2425  : public Array_t
{
};
// System.DateTime[]
// System.DateTime[]
struct  DateTimeU5BU5D_t2450  : public Array_t
{
};
struct DateTimeU5BU5D_t2450_StaticFields{
};
// System.IComparable`1<System.DateTime>[]
// System.IComparable`1<System.DateTime>[]
struct  IComparable_1U5BU5D_t3864  : public Array_t
{
};
// System.IEquatable`1<System.DateTime>[]
// System.IEquatable`1<System.DateTime>[]
struct  IEquatable_1U5BU5D_t3865  : public Array_t
{
};
// System.Decimal[]
// System.Decimal[]
struct  DecimalU5BU5D_t2451  : public Array_t
{
};
struct DecimalU5BU5D_t2451_StaticFields{
};
// System.IComparable`1<System.Decimal>[]
// System.IComparable`1<System.Decimal>[]
struct  IComparable_1U5BU5D_t3866  : public Array_t
{
};
// System.IEquatable`1<System.Decimal>[]
// System.IEquatable`1<System.Decimal>[]
struct  IEquatable_1U5BU5D_t3867  : public Array_t
{
};
// System.TimeSpan[]
// System.TimeSpan[]
struct  TimeSpanU5BU5D_t2452  : public Array_t
{
};
struct TimeSpanU5BU5D_t2452_StaticFields{
};
// System.IComparable`1<System.TimeSpan>[]
// System.IComparable`1<System.TimeSpan>[]
struct  IComparable_1U5BU5D_t3868  : public Array_t
{
};
// System.IEquatable`1<System.TimeSpan>[]
// System.IEquatable`1<System.TimeSpan>[]
struct  IEquatable_1U5BU5D_t3869  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct  TypeTagU5BU5D_t2453  : public Array_t
{
};
// System.MonoType[]
// System.MonoType[]
struct  MonoTypeU5BU5D_t2456  : public Array_t
{
};
// System.Byte[,]
// System.Byte[,]
struct  ByteU5BU2CU5D_t2201  : public Array_t
{
};
// System.Security.Policy.StrongName[]
// System.Security.Policy.StrongName[]
struct  StrongNameU5BU5D_t3596  : public Array_t
{
};
