﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t561;

// System.Void UnityEngine.UI.Dropdown/DropdownEvent::.ctor()
extern "C" void DropdownEvent__ctor_m2311 (DropdownEvent_t561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
