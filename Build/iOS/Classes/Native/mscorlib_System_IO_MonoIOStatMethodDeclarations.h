﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void MonoIOStat_t1935_marshal(const MonoIOStat_t1935& unmarshaled, MonoIOStat_t1935_marshaled& marshaled);
extern "C" void MonoIOStat_t1935_marshal_back(const MonoIOStat_t1935_marshaled& marshaled, MonoIOStat_t1935& unmarshaled);
extern "C" void MonoIOStat_t1935_marshal_cleanup(MonoIOStat_t1935_marshaled& marshaled);
