﻿#pragma once
#include <stdint.h>
// System.Enum[]
struct EnumU5BU5D_t338;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Enum>
struct  List_1_t377  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Enum>::_items
	EnumU5BU5D_t338* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Enum>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Enum>::_version
	int32_t ____version_3;
};
struct List_1_t377_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Enum>::EmptyArray
	EnumU5BU5D_t338* ___EmptyArray_4;
};
