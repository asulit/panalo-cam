﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t981;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
struct  Predicate_1_t1045  : public MulticastDelegate_t28
{
};
