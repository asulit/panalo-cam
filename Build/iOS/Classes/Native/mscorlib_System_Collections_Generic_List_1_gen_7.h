﻿#pragma once
#include <stdint.h>
// UnityThreading.TaskBase[]
struct TaskBaseU5BU5D_t2644;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityThreading.TaskBase>
struct  List_1_t159  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityThreading.TaskBase>::_items
	TaskBaseU5BU5D_t2644* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityThreading.TaskBase>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityThreading.TaskBase>::_version
	int32_t ____version_3;
};
struct List_1_t159_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityThreading.TaskBase>::EmptyArray
	TaskBaseU5BU5D_t2644* ___EmptyArray_4;
};
