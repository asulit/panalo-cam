﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__8MethodDeclarations.h"
#define Enumerator__ctor_m21999(__this, ___dictionary, method) (( void (*) (Enumerator_t3020 *, Dictionary_2_t765 *, const MethodInfo*))Enumerator__ctor_m15667_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22000(__this, method) (( Object_t * (*) (Enumerator_t3020 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15668_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22001(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3020 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15669_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22002(__this, method) (( Object_t * (*) (Enumerator_t3020 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15670_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22003(__this, method) (( Object_t * (*) (Enumerator_t3020 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m22004(__this, method) (( bool (*) (Enumerator_t3020 *, const MethodInfo*))Enumerator_MoveNext_m15672_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::get_Current()
#define Enumerator_get_Current_m22005(__this, method) (( KeyValuePair_2_t3017  (*) (Enumerator_t3020 *, const MethodInfo*))Enumerator_get_Current_m15673_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m22006(__this, method) (( Object_t * (*) (Enumerator_t3020 *, const MethodInfo*))Enumerator_get_CurrentKey_m15674_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m22007(__this, method) (( int32_t (*) (Enumerator_t3020 *, const MethodInfo*))Enumerator_get_CurrentValue_m15675_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m22008(__this, method) (( void (*) (Enumerator_t3020 *, const MethodInfo*))Enumerator_VerifyState_m15676_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m22009(__this, method) (( void (*) (Enumerator_t3020 *, const MethodInfo*))Enumerator_VerifyCurrent_m15677_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.IClipper,System.Int32>::Dispose()
#define Enumerator_Dispose_m22010(__this, method) (( void (*) (Enumerator_t3020 *, const MethodInfo*))Enumerator_Dispose_m15678_gshared)(__this, method)
