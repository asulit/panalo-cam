﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoEnumInfo/IntComparer
struct IntComparer_t2334;
// System.Object
struct Object_t;

// System.Void System.MonoEnumInfo/IntComparer::.ctor()
extern "C" void IntComparer__ctor_m14135 (IntComparer_t2334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MonoEnumInfo/IntComparer::Compare(System.Object,System.Object)
extern "C" int32_t IntComparer_Compare_m14136 (IntComparer_t2334 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MonoEnumInfo/IntComparer::Compare(System.Int32,System.Int32)
extern "C" int32_t IntComparer_Compare_m14137 (IntComparer_t2334 * __this, int32_t ___ix, int32_t ___iy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
