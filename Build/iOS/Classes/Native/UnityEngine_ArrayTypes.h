﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.Renderer[]
// UnityEngine.Renderer[]
struct  RendererU5BU5D_t366  : public Array_t
{
};
// UnityEngine.Component[]
// UnityEngine.Component[]
struct  ComponentU5BU5D_t397  : public Array_t
{
};
// UnityEngine.Object[]
// UnityEngine.Object[]
struct  ObjectU5BU5D_t997  : public Array_t
{
};
// UnityEngine.MonoBehaviour[]
// UnityEngine.MonoBehaviour[]
struct  MonoBehaviourU5BU5D_t368  : public Array_t
{
};
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct  BehaviourU5BU5D_t3870  : public Array_t
{
};
// UnityEngine.Collider[]
// UnityEngine.Collider[]
struct  ColliderU5BU5D_t59  : public Array_t
{
};
// UnityEngine.GUIContent[]
// UnityEngine.GUIContent[]
struct  GUIContentU5BU5D_t341  : public Array_t
{
};
struct GUIContentU5BU5D_t341_StaticFields{
};
// UnityEngine.MeshFilter[]
// UnityEngine.MeshFilter[]
struct  MeshFilterU5BU5D_t81  : public Array_t
{
};
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct  Vector3U5BU5D_t254  : public Array_t
{
};
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct  TransformU5BU5D_t354  : public Array_t
{
};
// UnityEngine.Matrix4x4[]
// UnityEngine.Matrix4x4[]
struct  Matrix4x4U5BU5D_t399  : public Array_t
{
};
// UnityEngine.BoneWeight[]
// UnityEngine.BoneWeight[]
struct  BoneWeightU5BU5D_t400  : public Array_t
{
};
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct  Vector2U5BU5D_t262  : public Array_t
{
};
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct  GUILayoutOptionU5BU5D_t441  : public Array_t
{
};
// UnityEngine.Color[,]
// UnityEngine.Color[,]
struct  ColorU5BU2CU5D_t263  : public Array_t
{
};
// UnityEngine.Color[]
// UnityEngine.Color[]
struct  ColorU5BU5D_t448  : public Array_t
{
};
// UnityEngine.Rect[]
// UnityEngine.Rect[]
struct  RectU5BU5D_t265  : public Array_t
{
};
// UnityEngine.Material[]
// UnityEngine.Material[]
struct  MaterialU5BU5D_t446  : public Array_t
{
};
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct  CameraU5BU5D_t471  : public Array_t
{
};
struct CameraU5BU5D_t471_StaticFields{
};
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct  GameObjectU5BU5D_t2861  : public Array_t
{
};
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct  RaycastHit2DU5BU5D_t726  : public Array_t
{
};
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct  RaycastHitU5BU5D_t731  : public Array_t
{
};
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct  CanvasU5BU5D_t2913  : public Array_t
{
};
struct CanvasU5BU5D_t2913_StaticFields{
};
// UnityEngine.Font[]
// UnityEngine.Font[]
struct  FontU5BU5D_t2924  : public Array_t
{
};
struct FontU5BU5D_t2924_StaticFields{
};
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct  UIVertexU5BU5D_t602  : public Array_t
{
};
struct UIVertexU5BU5D_t602_StaticFields{
};
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct  UILineInfoU5BU5D_t1010  : public Array_t
{
};
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct  UICharInfoU5BU5D_t1009  : public Array_t
{
};
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct  CanvasGroupU5BU5D_t2994  : public Array_t
{
};
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct  RectTransformU5BU5D_t3021  : public Array_t
{
};
struct RectTransformU5BU5D_t3021_StaticFields{
};
// UnityEngine.Color32[]
// UnityEngine.Color32[]
struct ALIGN_TYPE(4) Color32U5BU5D_t778  : public Array_t
{
};
// UnityEngine.Vector4[]
// UnityEngine.Vector4[]
struct  Vector4U5BU5D_t779  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct  IAchievementDescriptionU5BU5D_t1017  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct  IAchievementU5BU5D_t1019  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct  IScoreU5BU5D_t956  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct  IUserProfileU5BU5D_t952  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct  AchievementDescriptionU5BU5D_t801  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct  UserProfileU5BU5D_t802  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct  GcLeaderboardU5BU5D_t3104  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct  GcAchievementDataU5BU5D_t999  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct  AchievementU5BU5D_t1018  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct  GcScoreDataU5BU5D_t1000  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct  ScoreU5BU5D_t1020  : public Array_t
{
};
// UnityEngine.Display[]
// UnityEngine.Display[]
struct  DisplayU5BU5D_t846  : public Array_t
{
};
struct DisplayU5BU5D_t846_StaticFields{
};
// UnityEngine.ContactPoint[]
// UnityEngine.ContactPoint[]
struct  ContactPointU5BU5D_t862  : public Array_t
{
};
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct  Rigidbody2DU5BU5D_t3122  : public Array_t
{
};
// UnityEngine.ContactPoint2D[]
// UnityEngine.ContactPoint2D[]
struct  ContactPoint2DU5BU5D_t871  : public Array_t
{
};
// UnityEngine.WebCamDevice[]
// UnityEngine.WebCamDevice[]
struct  WebCamDeviceU5BU5D_t1006  : public Array_t
{
};
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct  KeyframeU5BU5D_t1007  : public Array_t
{
};
// UnityEngine.CharacterInfo[]
// UnityEngine.CharacterInfo[]
struct  CharacterInfoU5BU5D_t1008  : public Array_t
{
};
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct  LayoutCacheU5BU5D_t3150  : public Array_t
{
};
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct  GUILayoutEntryU5BU5D_t3156  : public Array_t
{
};
struct GUILayoutEntryU5BU5D_t3156_StaticFields{
};
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct  GUIStyleU5BU5D_t915  : public Array_t
{
};
struct GUIStyleU5BU5D_t915_StaticFields{
};
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct  DisallowMultipleComponentU5BU5D_t926  : public Array_t
{
};
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct  ExecuteInEditModeU5BU5D_t927  : public Array_t
{
};
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct  RequireComponentU5BU5D_t928  : public Array_t
{
};
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct  HitInfoU5BU5D_t961  : public Array_t
{
};
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct  TextEditOpU5BU5D_t3178  : public Array_t
{
};
// UnityEngine.Event[]
// UnityEngine.Event[]
struct  EventU5BU5D_t3177  : public Array_t
{
};
struct EventU5BU5D_t3177_StaticFields{
};
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct  PersistentCallU5BU5D_t3209  : public Array_t
{
};
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct  BaseInvokableCallU5BU5D_t3214  : public Array_t
{
};
// UnityEngine.MeshRenderer[]
// UnityEngine.MeshRenderer[]
struct  MeshRendererU5BU5D_t3521  : public Array_t
{
};
