﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Query.ConcreteQueryResult
struct ConcreteQueryResult_t107;
// System.Object
struct Object_t;

// System.Void Common.Query.ConcreteQueryResult::.ctor()
extern "C" void ConcreteQueryResult__ctor_m367 (ConcreteQueryResult_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Query.ConcreteQueryResult::Clear()
extern "C" void ConcreteQueryResult_Clear_m368 (ConcreteQueryResult_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Query.ConcreteQueryResult::Set(System.Object)
extern "C" void ConcreteQueryResult_Set_m369 (ConcreteQueryResult_t107 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
