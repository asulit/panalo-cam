﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionFromTargetBehaviour
struct ReconstructionFromTargetBehaviour_t302;

// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
extern "C" void ReconstructionFromTargetBehaviour__ctor_m1258 (ReconstructionFromTargetBehaviour_t302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
