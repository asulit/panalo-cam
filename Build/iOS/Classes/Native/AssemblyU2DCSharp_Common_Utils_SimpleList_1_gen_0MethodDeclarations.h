﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::.ctor()
// Common.Utils.SimpleList`1<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_gen_1MethodDeclarations.h"
#define SimpleList_1__ctor_m1519(__this, method) (( void (*) (SimpleList_1_t117 *, const MethodInfo*))SimpleList_1__ctor_m16158_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::GetEnumerator()
#define SimpleList_1_GetEnumerator_m16254(__this, method) (( Object_t* (*) (SimpleList_1_t117 *, const MethodInfo*))SimpleList_1_GetEnumerator_m16160_gshared)(__this, method)
// T Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::get_Item(System.Int32)
#define SimpleList_1_get_Item_m1524(__this, ___i, method) (( SignalListener_t114 * (*) (SimpleList_1_t117 *, int32_t, const MethodInfo*))SimpleList_1_get_Item_m16161_gshared)(__this, ___i, method)
// System.Void Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::set_Item(System.Int32,T)
#define SimpleList_1_set_Item_m16255(__this, ___i, ___value, method) (( void (*) (SimpleList_1_t117 *, int32_t, SignalListener_t114 *, const MethodInfo*))SimpleList_1_set_Item_m16163_gshared)(__this, ___i, ___value, method)
// System.Void Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::AllocateMore()
#define SimpleList_1_AllocateMore_m16256(__this, method) (( void (*) (SimpleList_1_t117 *, const MethodInfo*))SimpleList_1_AllocateMore_m16165_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::Trim()
#define SimpleList_1_Trim_m16257(__this, method) (( void (*) (SimpleList_1_t117 *, const MethodInfo*))SimpleList_1_Trim_m16167_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::Clear()
#define SimpleList_1_Clear_m16258(__this, method) (( void (*) (SimpleList_1_t117 *, const MethodInfo*))SimpleList_1_Clear_m16168_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::Release()
#define SimpleList_1_Release_m16259(__this, method) (( void (*) (SimpleList_1_t117 *, const MethodInfo*))SimpleList_1_Release_m16170_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::Add(T)
#define SimpleList_1_Add_m1520(__this, ___item, method) (( void (*) (SimpleList_1_t117 *, SignalListener_t114 *, const MethodInfo*))SimpleList_1_Add_m16171_gshared)(__this, ___item, method)
// System.Void Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::Insert(System.Int32,T)
#define SimpleList_1_Insert_m16260(__this, ___index, ___item, method) (( void (*) (SimpleList_1_t117 *, int32_t, SignalListener_t114 *, const MethodInfo*))SimpleList_1_Insert_m16173_gshared)(__this, ___index, ___item, method)
// System.Boolean Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::Contains(T)
#define SimpleList_1_Contains_m1523(__this, ___item, method) (( bool (*) (SimpleList_1_t117 *, SignalListener_t114 *, const MethodInfo*))SimpleList_1_Contains_m16175_gshared)(__this, ___item, method)
// System.Boolean Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::Remove(T)
#define SimpleList_1_Remove_m1521(__this, ___item, method) (( bool (*) (SimpleList_1_t117 *, SignalListener_t114 *, const MethodInfo*))SimpleList_1_Remove_m16177_gshared)(__this, ___item, method)
// System.Void Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::RemoveAt(System.Int32)
#define SimpleList_1_RemoveAt_m16261(__this, ___index, method) (( void (*) (SimpleList_1_t117 *, int32_t, const MethodInfo*))SimpleList_1_RemoveAt_m16179_gshared)(__this, ___index, method)
// T[] Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::ToArray()
#define SimpleList_1_ToArray_m16262(__this, method) (( SignalListenerU5BU5D_t2606* (*) (SimpleList_1_t117 *, const MethodInfo*))SimpleList_1_ToArray_m16181_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::Sort(System.Comparison`1<T>)
#define SimpleList_1_Sort_m16263(__this, ___comparer, method) (( void (*) (SimpleList_1_t117 *, Comparison_1_t2607 *, const MethodInfo*))SimpleList_1_Sort_m16183_gshared)(__this, ___comparer, method)
// System.Int32 Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::get_Count()
#define SimpleList_1_get_Count_m1522(__this, method) (( int32_t (*) (SimpleList_1_t117 *, const MethodInfo*))SimpleList_1_get_Count_m16184_gshared)(__this, method)
// System.Boolean Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::IsEmpty()
#define SimpleList_1_IsEmpty_m16264(__this, method) (( bool (*) (SimpleList_1_t117 *, const MethodInfo*))SimpleList_1_IsEmpty_m16186_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>::TraverseItems(Common.Utils.SimpleList`1/Visitor<T>)
#define SimpleList_1_TraverseItems_m16265(__this, ___visitor, method) (( void (*) (SimpleList_1_t117 *, Visitor_t2608 *, const MethodInfo*))SimpleList_1_TraverseItems_m16188_gshared)(__this, ___visitor, method)
