﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1372;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_72.h"
// Vuforia.VuforiaManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m28280_gshared (Enumerator_t3448 * __this, Dictionary_2_t1372 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m28280(__this, ___host, method) (( void (*) (Enumerator_t3448 *, Dictionary_2_t1372 *, const MethodInfo*))Enumerator__ctor_m28280_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m28281_gshared (Enumerator_t3448 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m28281(__this, method) (( Object_t * (*) (Enumerator_t3448 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m28281_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C" void Enumerator_Dispose_m28282_gshared (Enumerator_t3448 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m28282(__this, method) (( void (*) (Enumerator_t3448 *, const MethodInfo*))Enumerator_Dispose_m28282_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m28283_gshared (Enumerator_t3448 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m28283(__this, method) (( bool (*) (Enumerator_t3448 *, const MethodInfo*))Enumerator_MoveNext_m28283_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C" VirtualButtonData_t1135  Enumerator_get_Current_m28284_gshared (Enumerator_t3448 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m28284(__this, method) (( VirtualButtonData_t1135  (*) (Enumerator_t3448 *, const MethodInfo*))Enumerator_get_Current_m28284_gshared)(__this, method)
