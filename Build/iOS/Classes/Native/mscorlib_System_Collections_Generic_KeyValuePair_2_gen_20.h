﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.InputField
struct InputField_t226;
// System.String
struct String_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.String>
struct  KeyValuePair_2_t2760 
{
	// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.String>::key
	InputField_t226 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.String>::value
	String_t* ___value_1;
};
