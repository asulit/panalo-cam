﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_38MethodDeclarations.h"
#define KeyValuePair_2__ctor_m26483(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3352 *, Type_t *, uint16_t, const MethodInfo*))KeyValuePair_2__ctor_m26385_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Key()
#define KeyValuePair_2_get_Key_m26484(__this, method) (( Type_t * (*) (KeyValuePair_2_t3352 *, const MethodInfo*))KeyValuePair_2_get_Key_m26386_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m26485(__this, ___value, method) (( void (*) (KeyValuePair_2_t3352 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m26387_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Value()
#define KeyValuePair_2_get_Value_m26486(__this, method) (( uint16_t (*) (KeyValuePair_2_t3352 *, const MethodInfo*))KeyValuePair_2_get_Value_m26388_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m26487(__this, ___value, method) (( void (*) (KeyValuePair_2_t3352 *, uint16_t, const MethodInfo*))KeyValuePair_2_set_Value_m26389_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::ToString()
#define KeyValuePair_2_ToString_m26488(__this, method) (( String_t* (*) (KeyValuePair_2_t3352 *, const MethodInfo*))KeyValuePair_2_ToString_m26390_gshared)(__this, method)
