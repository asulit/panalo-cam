﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1371;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__45.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45.h"
// Vuforia.VuforiaManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m28108_gshared (Enumerator_t3429 * __this, Dictionary_2_t1371 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m28108(__this, ___dictionary, method) (( void (*) (Enumerator_t3429 *, Dictionary_2_t1371 *, const MethodInfo*))Enumerator__ctor_m28108_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m28109_gshared (Enumerator_t3429 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m28109(__this, method) (( Object_t * (*) (Enumerator_t3429 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m28109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m28110_gshared (Enumerator_t3429 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m28110(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3429 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m28110_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m28111_gshared (Enumerator_t3429 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m28111(__this, method) (( Object_t * (*) (Enumerator_t3429 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m28111_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m28112_gshared (Enumerator_t3429 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m28112(__this, method) (( Object_t * (*) (Enumerator_t3429 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m28112_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m28113_gshared (Enumerator_t3429 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m28113(__this, method) (( bool (*) (Enumerator_t3429 *, const MethodInfo*))Enumerator_MoveNext_m28113_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" KeyValuePair_2_t3425  Enumerator_get_Current_m28114_gshared (Enumerator_t3429 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m28114(__this, method) (( KeyValuePair_2_t3425  (*) (Enumerator_t3429 *, const MethodInfo*))Enumerator_get_Current_m28114_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m28115_gshared (Enumerator_t3429 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m28115(__this, method) (( int32_t (*) (Enumerator_t3429 *, const MethodInfo*))Enumerator_get_CurrentKey_m28115_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_CurrentValue()
extern "C" TrackableResultData_t1134  Enumerator_get_CurrentValue_m28116_gshared (Enumerator_t3429 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m28116(__this, method) (( TrackableResultData_t1134  (*) (Enumerator_t3429 *, const MethodInfo*))Enumerator_get_CurrentValue_m28116_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::VerifyState()
extern "C" void Enumerator_VerifyState_m28117_gshared (Enumerator_t3429 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m28117(__this, method) (( void (*) (Enumerator_t3429 *, const MethodInfo*))Enumerator_VerifyState_m28117_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m28118_gshared (Enumerator_t3429 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m28118(__this, method) (( void (*) (Enumerator_t3429 *, const MethodInfo*))Enumerator_VerifyCurrent_m28118_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m28119_gshared (Enumerator_t3429 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m28119(__this, method) (( void (*) (Enumerator_t3429 *, const MethodInfo*))Enumerator_Dispose_m28119_gshared)(__this, method)
