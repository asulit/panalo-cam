﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t450;
// System.Object
#include "mscorlib_System_Object.h"
// System.Threading.CompressedStack
struct  CompressedStack_t2251  : public Object_t
{
	// System.Collections.ArrayList System.Threading.CompressedStack::_list
	ArrayList_t450 * ____list_0;
};
