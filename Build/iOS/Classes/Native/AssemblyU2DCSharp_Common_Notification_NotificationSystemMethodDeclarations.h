﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Notification.NotificationSystem
struct NotificationSystem_t89;
// Common.Notification.NotificationHandler
struct NotificationHandler_t347;
// System.String
struct String_t;
// Common.Notification.NotificationSystem/NotificationInitializer
struct NotificationInitializer_t88;
// Common.Notification.NotificationInstance
struct NotificationInstance_t85;

// System.Void Common.Notification.NotificationSystem::.ctor()
extern "C" void NotificationSystem__ctor_m302 (NotificationSystem_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationSystem::AddHandler(Common.Notification.NotificationHandler)
extern "C" void NotificationSystem_AddHandler_m303 (NotificationSystem_t89 * __this, Object_t * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationSystem::RemoveHandler(Common.Notification.NotificationHandler)
extern "C" void NotificationSystem_RemoveHandler_m304 (NotificationSystem_t89 * __this, Object_t * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationSystem::ClearHandlers()
extern "C" void NotificationSystem_ClearHandlers_m305 (NotificationSystem_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationSystem::Post(System.String)
extern "C" void NotificationSystem_Post_m306 (NotificationSystem_t89 * __this, String_t* ___notificationId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationSystem::Post(System.String,Common.Notification.NotificationSystem/NotificationInitializer)
extern "C" void NotificationSystem_Post_m307 (NotificationSystem_t89 * __this, String_t* ___notificationId, NotificationInitializer_t88 * ___initializer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Notification.NotificationInstance Common.Notification.NotificationSystem::Begin(System.String)
extern "C" NotificationInstance_t85 * NotificationSystem_Begin_m308 (NotificationSystem_t89 * __this, String_t* ___notificationId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Notification.NotificationInstance Common.Notification.NotificationSystem::Start(System.String)
extern "C" NotificationInstance_t85 * NotificationSystem_Start_m309 (NotificationSystem_t89 * __this, String_t* ___notificationId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationSystem::Post(Common.Notification.NotificationInstance)
extern "C" void NotificationSystem_Post_m310 (NotificationSystem_t89 * __this, NotificationInstance_t85 * ___notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Notification.NotificationSystem Common.Notification.NotificationSystem::GetInstance()
extern "C" NotificationSystem_t89 * NotificationSystem_GetInstance_m311 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
