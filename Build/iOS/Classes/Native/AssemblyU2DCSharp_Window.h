﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t196;
// UnityEngine.GameObject
struct GameObject_t155;
// System.String
struct String_t;
// Handle
struct Handle_t221;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Window
struct  Window_t223  : public MonoBehaviour_t5
{
	// UnityEngine.UI.Text Window::message
	Text_t196 * ___message_2;
	// UnityEngine.GameObject Window::button
	GameObject_t155 * ___button_3;
	// System.String Window::body
	String_t* ___body_4;
	// Handle Window::handler
	Handle_t221 * ___handler_5;
};
