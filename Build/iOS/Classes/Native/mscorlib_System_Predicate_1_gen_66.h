﻿#pragma once
#include <stdint.h>
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t327;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.WordAbstractBehaviour>
struct  Predicate_1_t3374  : public MulticastDelegate_t28
{
};
