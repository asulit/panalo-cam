﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t707;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<System.Single>
struct  InvokableCall_1_t2892  : public BaseInvokableCall_t981
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.Single>::Delegate
	UnityAction_1_t707 * ___Delegate_0;
};
