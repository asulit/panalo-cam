﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>::.ctor(System.Object,System.IntPtr)
// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_4MethodDeclarations.h"
#define UnityAction_1__ctor_m22953(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t3095 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m18971_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>::Invoke(T0)
#define UnityAction_1_Invoke_m22954(__this, ___arg0, method) (( void (*) (UnityAction_1_t3095 *, List_1_t679 *, const MethodInfo*))UnityAction_1_Invoke_m18972_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m22955(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t3095 *, List_1_t679 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m18973_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m22956(__this, ___result, method) (( void (*) (UnityAction_1_t3095 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m18974_gshared)(__this, ___result, method)
