﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Logger.Logger
struct Logger_t75;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// LoggerComponent
struct  LoggerComponent_t78  : public MonoBehaviour_t5
{
	// System.String LoggerComponent::name
	String_t* ___name_2;
	// Common.Logger.Logger LoggerComponent::logger
	Logger_t75 * ___logger_3;
};
