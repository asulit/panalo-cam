﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
// System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.DictionaryEntry>
struct  Transform_1_t2689  : public MulticastDelegate_t28
{
};
