﻿#pragma once
#include <stdint.h>
// System.Text.Encoding
struct Encoding_t420;
// System.Text.Decoder
#include "mscorlib_System_Text_Decoder.h"
// System.Text.Encoding/ForwardingDecoder
struct  ForwardingDecoder_t2278  : public Decoder_t1920
{
	// System.Text.Encoding System.Text.Encoding/ForwardingDecoder::encoding
	Encoding_t420 * ___encoding_2;
};
