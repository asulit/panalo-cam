﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t519;
// System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerable_1_t3683;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t3684;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.RaycastResult>
struct ICollection_1_t3685;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
struct ReadOnlyCollection_1_t2834;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2831;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2839;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t481;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_42.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void List_1__ctor_m3313_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1__ctor_m3313(__this, method) (( void (*) (List_1_t519 *, const MethodInfo*))List_1__ctor_m3313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m19125_gshared (List_1_t519 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m19125(__this, ___collection, method) (( void (*) (List_1_t519 *, Object_t*, const MethodInfo*))List_1__ctor_m19125_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m19126_gshared (List_1_t519 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m19126(__this, ___capacity, method) (( void (*) (List_1_t519 *, int32_t, const MethodInfo*))List_1__ctor_m19126_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern "C" void List_1__cctor_m19127_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m19127(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m19127_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19128_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19128(__this, method) (( Object_t* (*) (List_1_t519 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19128_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m19129_gshared (List_1_t519 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m19129(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t519 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m19129_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m19130_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19130(__this, method) (( Object_t * (*) (List_1_t519 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m19130_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m19131_gshared (List_1_t519 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m19131(__this, ___item, method) (( int32_t (*) (List_1_t519 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m19131_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m19132_gshared (List_1_t519 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m19132(__this, ___item, method) (( bool (*) (List_1_t519 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m19132_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m19133_gshared (List_1_t519 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m19133(__this, ___item, method) (( int32_t (*) (List_1_t519 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m19133_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m19134_gshared (List_1_t519 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m19134(__this, ___index, ___item, method) (( void (*) (List_1_t519 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m19134_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m19135_gshared (List_1_t519 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m19135(__this, ___item, method) (( void (*) (List_1_t519 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m19135_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19136_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19136(__this, method) (( bool (*) (List_1_t519 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19136_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m19137_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19137(__this, method) (( bool (*) (List_1_t519 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m19137_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m19138_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m19138(__this, method) (( Object_t * (*) (List_1_t519 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m19138_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m19139_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m19139(__this, method) (( bool (*) (List_1_t519 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m19139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m19140_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m19140(__this, method) (( bool (*) (List_1_t519 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m19140_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m19141_gshared (List_1_t519 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m19141(__this, ___index, method) (( Object_t * (*) (List_1_t519 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m19141_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m19142_gshared (List_1_t519 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m19142(__this, ___index, ___value, method) (( void (*) (List_1_t519 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m19142_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void List_1_Add_m19143_gshared (List_1_t519 * __this, RaycastResult_t512  ___item, const MethodInfo* method);
#define List_1_Add_m19143(__this, ___item, method) (( void (*) (List_1_t519 *, RaycastResult_t512 , const MethodInfo*))List_1_Add_m19143_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m19144_gshared (List_1_t519 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m19144(__this, ___newCount, method) (( void (*) (List_1_t519 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m19144_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m19145_gshared (List_1_t519 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m19145(__this, ___idx, ___count, method) (( void (*) (List_1_t519 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m19145_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m19146_gshared (List_1_t519 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m19146(__this, ___collection, method) (( void (*) (List_1_t519 *, Object_t*, const MethodInfo*))List_1_AddCollection_m19146_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m19147_gshared (List_1_t519 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m19147(__this, ___enumerable, method) (( void (*) (List_1_t519 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m19147_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m19148_gshared (List_1_t519 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m19148(__this, ___collection, method) (( void (*) (List_1_t519 *, Object_t*, const MethodInfo*))List_1_AddRange_m19148_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2834 * List_1_AsReadOnly_m19149_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m19149(__this, method) (( ReadOnlyCollection_1_t2834 * (*) (List_1_t519 *, const MethodInfo*))List_1_AsReadOnly_m19149_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void List_1_Clear_m19150_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_Clear_m19150(__this, method) (( void (*) (List_1_t519 *, const MethodInfo*))List_1_Clear_m19150_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool List_1_Contains_m19151_gshared (List_1_t519 * __this, RaycastResult_t512  ___item, const MethodInfo* method);
#define List_1_Contains_m19151(__this, ___item, method) (( bool (*) (List_1_t519 *, RaycastResult_t512 , const MethodInfo*))List_1_Contains_m19151_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m19152_gshared (List_1_t519 * __this, RaycastResultU5BU5D_t2831* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m19152(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t519 *, RaycastResultU5BU5D_t2831*, int32_t, const MethodInfo*))List_1_CopyTo_m19152_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Find(System.Predicate`1<T>)
extern "C" RaycastResult_t512  List_1_Find_m19153_gshared (List_1_t519 * __this, Predicate_1_t2839 * ___match, const MethodInfo* method);
#define List_1_Find_m19153(__this, ___match, method) (( RaycastResult_t512  (*) (List_1_t519 *, Predicate_1_t2839 *, const MethodInfo*))List_1_Find_m19153_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m19154_gshared (Object_t * __this /* static, unused */, Predicate_1_t2839 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m19154(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2839 *, const MethodInfo*))List_1_CheckMatch_m19154_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m19155_gshared (List_1_t519 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2839 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m19155(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t519 *, int32_t, int32_t, Predicate_1_t2839 *, const MethodInfo*))List_1_GetIndex_m19155_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Enumerator_t2833  List_1_GetEnumerator_m19156_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m19156(__this, method) (( Enumerator_t2833  (*) (List_1_t519 *, const MethodInfo*))List_1_GetEnumerator_m19156_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m19157_gshared (List_1_t519 * __this, RaycastResult_t512  ___item, const MethodInfo* method);
#define List_1_IndexOf_m19157(__this, ___item, method) (( int32_t (*) (List_1_t519 *, RaycastResult_t512 , const MethodInfo*))List_1_IndexOf_m19157_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m19158_gshared (List_1_t519 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m19158(__this, ___start, ___delta, method) (( void (*) (List_1_t519 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m19158_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m19159_gshared (List_1_t519 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m19159(__this, ___index, method) (( void (*) (List_1_t519 *, int32_t, const MethodInfo*))List_1_CheckIndex_m19159_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m19160_gshared (List_1_t519 * __this, int32_t ___index, RaycastResult_t512  ___item, const MethodInfo* method);
#define List_1_Insert_m19160(__this, ___index, ___item, method) (( void (*) (List_1_t519 *, int32_t, RaycastResult_t512 , const MethodInfo*))List_1_Insert_m19160_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m19161_gshared (List_1_t519 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m19161(__this, ___collection, method) (( void (*) (List_1_t519 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m19161_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool List_1_Remove_m19162_gshared (List_1_t519 * __this, RaycastResult_t512  ___item, const MethodInfo* method);
#define List_1_Remove_m19162(__this, ___item, method) (( bool (*) (List_1_t519 *, RaycastResult_t512 , const MethodInfo*))List_1_Remove_m19162_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m19163_gshared (List_1_t519 * __this, Predicate_1_t2839 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m19163(__this, ___match, method) (( int32_t (*) (List_1_t519 *, Predicate_1_t2839 *, const MethodInfo*))List_1_RemoveAll_m19163_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m19164_gshared (List_1_t519 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m19164(__this, ___index, method) (( void (*) (List_1_t519 *, int32_t, const MethodInfo*))List_1_RemoveAt_m19164_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m19165_gshared (List_1_t519 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m19165(__this, ___index, ___count, method) (( void (*) (List_1_t519 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m19165_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Reverse()
extern "C" void List_1_Reverse_m19166_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_Reverse_m19166(__this, method) (( void (*) (List_1_t519 *, const MethodInfo*))List_1_Reverse_m19166_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort()
extern "C" void List_1_Sort_m19167_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_Sort_m19167(__this, method) (( void (*) (List_1_t519 *, const MethodInfo*))List_1_Sort_m19167_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m3277_gshared (List_1_t519 * __this, Comparison_1_t481 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m3277(__this, ___comparison, method) (( void (*) (List_1_t519 *, Comparison_1_t481 *, const MethodInfo*))List_1_Sort_m3277_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::ToArray()
extern "C" RaycastResultU5BU5D_t2831* List_1_ToArray_m19168_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_ToArray_m19168(__this, method) (( RaycastResultU5BU5D_t2831* (*) (List_1_t519 *, const MethodInfo*))List_1_ToArray_m19168_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m19169_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m19169(__this, method) (( void (*) (List_1_t519 *, const MethodInfo*))List_1_TrimExcess_m19169_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m19170_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m19170(__this, method) (( int32_t (*) (List_1_t519 *, const MethodInfo*))List_1_get_Capacity_m19170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m19171_gshared (List_1_t519 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m19171(__this, ___value, method) (( void (*) (List_1_t519 *, int32_t, const MethodInfo*))List_1_set_Capacity_m19171_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t List_1_get_Count_m19172_gshared (List_1_t519 * __this, const MethodInfo* method);
#define List_1_get_Count_m19172(__this, method) (( int32_t (*) (List_1_t519 *, const MethodInfo*))List_1_get_Count_m19172_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t512  List_1_get_Item_m19173_gshared (List_1_t519 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m19173(__this, ___index, method) (( RaycastResult_t512  (*) (List_1_t519 *, int32_t, const MethodInfo*))List_1_get_Item_m19173_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m19174_gshared (List_1_t519 * __this, int32_t ___index, RaycastResult_t512  ___value, const MethodInfo* method);
#define List_1_set_Item_m19174(__this, ___index, ___value, method) (( void (*) (List_1_t519 *, int32_t, RaycastResult_t512 , const MethodInfo*))List_1_set_Item_m19174_gshared)(__this, ___index, ___value, method)
