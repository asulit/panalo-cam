﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ScannerHandler
struct ScannerHandler_t209;
// Common.Signal.ISignalParameters
struct ISignalParameters_t115;
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"

// System.Void ScannerHandler::.ctor()
extern "C" void ScannerHandler__ctor_m738 (ScannerHandler_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHandler::Awake()
extern "C" void ScannerHandler_Awake_m739 (ScannerHandler_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHandler::Start()
extern "C" void ScannerHandler_Start_m740 (ScannerHandler_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHandler::OnDestroy()
extern "C" void ScannerHandler_OnDestroy_m741 (ScannerHandler_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C" void ScannerHandler_OnTrackableStateChanged_m742 (ScannerHandler_t209 * __this, int32_t ___previousStatus, int32_t ___newStatus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHandler::OnStartScanning(Common.Signal.ISignalParameters)
extern "C" void ScannerHandler_OnStartScanning_m743 (ScannerHandler_t209 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHandler::OnExitScanning(Common.Signal.ISignalParameters)
extern "C" void ScannerHandler_OnExitScanning_m744 (ScannerHandler_t209 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHandler::OnUpdateCameraFocus(Common.Signal.ISignalParameters)
extern "C" void ScannerHandler_OnUpdateCameraFocus_m745 (ScannerHandler_t209 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHandler::OnUpdateCameraFlash(Common.Signal.ISignalParameters)
extern "C" void ScannerHandler_OnUpdateCameraFlash_m746 (ScannerHandler_t209 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
