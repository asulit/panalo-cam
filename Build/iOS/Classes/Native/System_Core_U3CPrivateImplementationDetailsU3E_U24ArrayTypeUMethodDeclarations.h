﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU24136_t1410_marshal(const U24ArrayTypeU24136_t1410& unmarshaled, U24ArrayTypeU24136_t1410_marshaled& marshaled);
extern "C" void U24ArrayTypeU24136_t1410_marshal_back(const U24ArrayTypeU24136_t1410_marshaled& marshaled, U24ArrayTypeU24136_t1410& unmarshaled);
extern "C" void U24ArrayTypeU24136_t1410_marshal_cleanup(U24ArrayTypeU24136_t1410_marshaled& marshaled);
