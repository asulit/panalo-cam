﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSource
struct AudioSource_t44;
// System.String
struct String_t;
// CountdownTimer
struct CountdownTimer_t13;
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapter.h"
// Common.Fsm.Action.TimedFadeVolume
struct  TimedFadeVolume_t43  : public FsmActionAdapter_t33
{
	// UnityEngine.AudioSource Common.Fsm.Action.TimedFadeVolume::audioSource
	AudioSource_t44 * ___audioSource_1;
	// System.Single Common.Fsm.Action.TimedFadeVolume::volumeFrom
	float ___volumeFrom_2;
	// System.Single Common.Fsm.Action.TimedFadeVolume::volumeTo
	float ___volumeTo_3;
	// System.Single Common.Fsm.Action.TimedFadeVolume::duration
	float ___duration_4;
	// System.String Common.Fsm.Action.TimedFadeVolume::finishEvent
	String_t* ___finishEvent_5;
	// CountdownTimer Common.Fsm.Action.TimedFadeVolume::timer
	CountdownTimer_t13 * ___timer_6;
};
