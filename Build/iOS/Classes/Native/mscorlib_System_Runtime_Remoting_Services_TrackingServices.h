﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t450;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Services.TrackingServices
struct  TrackingServices_t2132  : public Object_t
{
};
struct TrackingServices_t2132_StaticFields{
	// System.Collections.ArrayList System.Runtime.Remoting.Services.TrackingServices::_handlers
	ArrayList_t450 * ____handlers_0;
};
