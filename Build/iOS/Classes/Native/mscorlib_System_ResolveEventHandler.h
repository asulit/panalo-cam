﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t1758;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t2366;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.ResolveEventHandler
struct  ResolveEventHandler_t2310  : public MulticastDelegate_t28
{
};
