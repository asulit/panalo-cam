﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.Action.MoveAlongDirection
struct MoveAlongDirection_t37;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t35;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Common.Fsm.Action.MoveAlongDirection::.ctor(Common.Fsm.FsmState,System.String)
extern "C" void MoveAlongDirection__ctor_m113 (MoveAlongDirection_t37 * __this, Object_t * ___owner, String_t* ___timeReferenceName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAlongDirection::Init(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern "C" void MoveAlongDirection_Init_m114 (MoveAlongDirection_t37 * __this, Transform_t35 * ___actor, Vector3_t36  ___direction, float ___velocity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAlongDirection::OnEnter()
extern "C" void MoveAlongDirection_OnEnter_m115 (MoveAlongDirection_t37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAlongDirection::OnUpdate()
extern "C" void MoveAlongDirection_OnUpdate_m116 (MoveAlongDirection_t37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
