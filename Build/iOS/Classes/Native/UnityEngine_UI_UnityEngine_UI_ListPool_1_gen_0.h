﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct ObjectPool_1_t2822;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct UnityAction_1_t2823;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Component>
struct  ListPool_1_t740  : public Object_t
{
};
struct ListPool_1_t740_StaticFields{
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Component>::s_ListPool
	ObjectPool_1_t2822 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Component>::<>f__am$cache1
	UnityAction_1_t2823 * ___U3CU3Ef__amU24cache1_1;
};
