﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22MethodDeclarations.h"
#define Enumerator__ctor_m27443(__this, ___dictionary, method) (( void (*) (Enumerator_t1351 *, Dictionary_2_t1200 *, const MethodInfo*))Enumerator__ctor_m19836_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27444(__this, method) (( Object_t * (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19837_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27445(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19838_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27446(__this, method) (( Object_t * (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19839_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27447(__this, method) (( Object_t * (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19840_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::MoveNext()
#define Enumerator_MoveNext_m7394(__this, method) (( bool (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_MoveNext_m19841_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::get_Current()
#define Enumerator_get_Current_m7392(__this, method) (( KeyValuePair_2_t1349  (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_get_Current_m19842_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m27448(__this, method) (( int32_t (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_get_CurrentKey_m19843_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m27449(__this, method) (( Object_t * (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_get_CurrentValue_m19844_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::VerifyState()
#define Enumerator_VerifyState_m27450(__this, method) (( void (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_VerifyState_m19845_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m27451(__this, method) (( void (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_VerifyCurrent_m19846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Surface>::Dispose()
#define Enumerator_Dispose_m7395(__this, method) (( void (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_Dispose_m19847_gshared)(__this, method)
