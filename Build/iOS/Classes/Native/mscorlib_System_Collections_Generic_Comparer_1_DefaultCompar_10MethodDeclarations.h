﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t3148;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m23629_gshared (DefaultComparer_t3148 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m23629(__this, method) (( void (*) (DefaultComparer_t3148 *, const MethodInfo*))DefaultComparer__ctor_m23629_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m23630_gshared (DefaultComparer_t3148 * __this, UILineInfo_t752  ___x, UILineInfo_t752  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m23630(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3148 *, UILineInfo_t752 , UILineInfo_t752 , const MethodInfo*))DefaultComparer_Compare_m23630_gshared)(__this, ___x, ___y, method)
