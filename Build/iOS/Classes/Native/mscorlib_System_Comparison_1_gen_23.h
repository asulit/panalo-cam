﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t513;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>
struct  Comparison_1_t2847  : public MulticastDelegate_t28
{
};
