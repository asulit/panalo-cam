﻿#pragma once
#include <stdint.h>
// System.Version
struct Version_t1633;
// System.Object
#include "mscorlib_System_Object.h"
// System.Net.HttpVersion
struct  HttpVersion_t1632  : public Object_t
{
};
struct HttpVersion_t1632_StaticFields{
	// System.Version System.Net.HttpVersion::Version10
	Version_t1633 * ___Version10_0;
	// System.Version System.Net.HttpVersion::Version11
	Version_t1633 * ___Version11_1;
};
