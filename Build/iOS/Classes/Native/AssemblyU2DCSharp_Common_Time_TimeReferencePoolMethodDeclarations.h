﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Time.TimeReferencePool
struct TimeReferencePool_t120;
// Common.Time.TimeReference
struct TimeReference_t14;
// System.String
struct String_t;

// System.Void Common.Time.TimeReferencePool::.ctor()
extern "C" void TimeReferencePool__ctor_m416 (TimeReferencePool_t120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Time.TimeReferencePool::.cctor()
extern "C" void TimeReferencePool__cctor_m417 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Time.TimeReferencePool Common.Time.TimeReferencePool::GetInstance()
extern "C" TimeReferencePool_t120 * TimeReferencePool_GetInstance_m418 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Time.TimeReference Common.Time.TimeReferencePool::Add(System.String)
extern "C" TimeReference_t14 * TimeReferencePool_Add_m419 (TimeReferencePool_t120 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Time.TimeReference Common.Time.TimeReferencePool::Get(System.String)
extern "C" TimeReference_t14 * TimeReferencePool_Get_m420 (TimeReferencePool_t120 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Time.TimeReference Common.Time.TimeReferencePool::GetDefault()
extern "C" TimeReference_t14 * TimeReferencePool_GetDefault_m421 (TimeReferencePool_t120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
