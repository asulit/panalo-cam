﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t607;

// System.Void UnityEngine.UI.MaskableGraphic/CullStateChangedEvent::.ctor()
extern "C" void CullStateChangedEvent__ctor_m2658 (CullStateChangedEvent_t607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
