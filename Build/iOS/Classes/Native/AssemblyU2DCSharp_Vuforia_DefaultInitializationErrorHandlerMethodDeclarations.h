﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t276;
// Vuforia.VuforiaUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"

// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern "C" void DefaultInitializationErrorHandler__ctor_m1182 (DefaultInitializationErrorHandler_t276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern "C" void DefaultInitializationErrorHandler_Awake_m1183 (DefaultInitializationErrorHandler_t276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern "C" void DefaultInitializationErrorHandler_OnGUI_m1184 (DefaultInitializationErrorHandler_t276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern "C" void DefaultInitializationErrorHandler_OnDestroy_m1185 (DefaultInitializationErrorHandler_t276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern "C" void DefaultInitializationErrorHandler_DrawWindowContent_m1186 (DefaultInitializationErrorHandler_t276 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_SetErrorCode_m1187 (DefaultInitializationErrorHandler_t276 * __this, int32_t ___errorCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C" void DefaultInitializationErrorHandler_SetErrorOccurred_m1188 (DefaultInitializationErrorHandler_t276 * __this, bool ___errorOccurred, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnVuforiaInitializationError(Vuforia.VuforiaUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_OnVuforiaInitializationError_m1189 (DefaultInitializationErrorHandler_t276 * __this, int32_t ___initError, const MethodInfo* method) IL2CPP_METHOD_ATTR;
