﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Stack`1<SwarmItem>
struct Stack_1_t153;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.Stack`1/Enumerator<SwarmItem>
struct  Enumerator_t2605 
{
	// System.Collections.Generic.Stack`1<T> System.Collections.Generic.Stack`1/Enumerator<SwarmItem>::parent
	Stack_1_t153 * ___parent_0;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator<SwarmItem>::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator<SwarmItem>::_version
	int32_t ____version_2;
};
