﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Utils.SimpleList`1<System.Object>
struct SimpleList_1_t2598;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Comparison`1<System.Object>
struct Comparison_1_t2482;
// Common.Utils.SimpleList`1/Visitor<System.Object>
struct Visitor_t2600;

// System.Void Common.Utils.SimpleList`1<System.Object>::.ctor()
extern "C" void SimpleList_1__ctor_m16158_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method);
#define SimpleList_1__ctor_m16158(__this, method) (( void (*) (SimpleList_1_t2598 *, const MethodInfo*))SimpleList_1__ctor_m16158_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Common.Utils.SimpleList`1<System.Object>::GetEnumerator()
extern "C" Object_t* SimpleList_1_GetEnumerator_m16160_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method);
#define SimpleList_1_GetEnumerator_m16160(__this, method) (( Object_t* (*) (SimpleList_1_t2598 *, const MethodInfo*))SimpleList_1_GetEnumerator_m16160_gshared)(__this, method)
// T Common.Utils.SimpleList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * SimpleList_1_get_Item_m16161_gshared (SimpleList_1_t2598 * __this, int32_t ___i, const MethodInfo* method);
#define SimpleList_1_get_Item_m16161(__this, ___i, method) (( Object_t * (*) (SimpleList_1_t2598 *, int32_t, const MethodInfo*))SimpleList_1_get_Item_m16161_gshared)(__this, ___i, method)
// System.Void Common.Utils.SimpleList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void SimpleList_1_set_Item_m16163_gshared (SimpleList_1_t2598 * __this, int32_t ___i, Object_t * ___value, const MethodInfo* method);
#define SimpleList_1_set_Item_m16163(__this, ___i, ___value, method) (( void (*) (SimpleList_1_t2598 *, int32_t, Object_t *, const MethodInfo*))SimpleList_1_set_Item_m16163_gshared)(__this, ___i, ___value, method)
// System.Void Common.Utils.SimpleList`1<System.Object>::AllocateMore()
extern "C" void SimpleList_1_AllocateMore_m16165_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method);
#define SimpleList_1_AllocateMore_m16165(__this, method) (( void (*) (SimpleList_1_t2598 *, const MethodInfo*))SimpleList_1_AllocateMore_m16165_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<System.Object>::Trim()
extern "C" void SimpleList_1_Trim_m16167_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method);
#define SimpleList_1_Trim_m16167(__this, method) (( void (*) (SimpleList_1_t2598 *, const MethodInfo*))SimpleList_1_Trim_m16167_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<System.Object>::Clear()
extern "C" void SimpleList_1_Clear_m16168_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method);
#define SimpleList_1_Clear_m16168(__this, method) (( void (*) (SimpleList_1_t2598 *, const MethodInfo*))SimpleList_1_Clear_m16168_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<System.Object>::Release()
extern "C" void SimpleList_1_Release_m16170_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method);
#define SimpleList_1_Release_m16170(__this, method) (( void (*) (SimpleList_1_t2598 *, const MethodInfo*))SimpleList_1_Release_m16170_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<System.Object>::Add(T)
extern "C" void SimpleList_1_Add_m16171_gshared (SimpleList_1_t2598 * __this, Object_t * ___item, const MethodInfo* method);
#define SimpleList_1_Add_m16171(__this, ___item, method) (( void (*) (SimpleList_1_t2598 *, Object_t *, const MethodInfo*))SimpleList_1_Add_m16171_gshared)(__this, ___item, method)
// System.Void Common.Utils.SimpleList`1<System.Object>::Insert(System.Int32,T)
extern "C" void SimpleList_1_Insert_m16173_gshared (SimpleList_1_t2598 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define SimpleList_1_Insert_m16173(__this, ___index, ___item, method) (( void (*) (SimpleList_1_t2598 *, int32_t, Object_t *, const MethodInfo*))SimpleList_1_Insert_m16173_gshared)(__this, ___index, ___item, method)
// System.Boolean Common.Utils.SimpleList`1<System.Object>::Contains(T)
extern "C" bool SimpleList_1_Contains_m16175_gshared (SimpleList_1_t2598 * __this, Object_t * ___item, const MethodInfo* method);
#define SimpleList_1_Contains_m16175(__this, ___item, method) (( bool (*) (SimpleList_1_t2598 *, Object_t *, const MethodInfo*))SimpleList_1_Contains_m16175_gshared)(__this, ___item, method)
// System.Boolean Common.Utils.SimpleList`1<System.Object>::Remove(T)
extern "C" bool SimpleList_1_Remove_m16177_gshared (SimpleList_1_t2598 * __this, Object_t * ___item, const MethodInfo* method);
#define SimpleList_1_Remove_m16177(__this, ___item, method) (( bool (*) (SimpleList_1_t2598 *, Object_t *, const MethodInfo*))SimpleList_1_Remove_m16177_gshared)(__this, ___item, method)
// System.Void Common.Utils.SimpleList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void SimpleList_1_RemoveAt_m16179_gshared (SimpleList_1_t2598 * __this, int32_t ___index, const MethodInfo* method);
#define SimpleList_1_RemoveAt_m16179(__this, ___index, method) (( void (*) (SimpleList_1_t2598 *, int32_t, const MethodInfo*))SimpleList_1_RemoveAt_m16179_gshared)(__this, ___index, method)
// T[] Common.Utils.SimpleList`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t356* SimpleList_1_ToArray_m16181_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method);
#define SimpleList_1_ToArray_m16181(__this, method) (( ObjectU5BU5D_t356* (*) (SimpleList_1_t2598 *, const MethodInfo*))SimpleList_1_ToArray_m16181_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void SimpleList_1_Sort_m16183_gshared (SimpleList_1_t2598 * __this, Comparison_1_t2482 * ___comparer, const MethodInfo* method);
#define SimpleList_1_Sort_m16183(__this, ___comparer, method) (( void (*) (SimpleList_1_t2598 *, Comparison_1_t2482 *, const MethodInfo*))SimpleList_1_Sort_m16183_gshared)(__this, ___comparer, method)
// System.Int32 Common.Utils.SimpleList`1<System.Object>::get_Count()
extern "C" int32_t SimpleList_1_get_Count_m16184_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method);
#define SimpleList_1_get_Count_m16184(__this, method) (( int32_t (*) (SimpleList_1_t2598 *, const MethodInfo*))SimpleList_1_get_Count_m16184_gshared)(__this, method)
// System.Boolean Common.Utils.SimpleList`1<System.Object>::IsEmpty()
extern "C" bool SimpleList_1_IsEmpty_m16186_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method);
#define SimpleList_1_IsEmpty_m16186(__this, method) (( bool (*) (SimpleList_1_t2598 *, const MethodInfo*))SimpleList_1_IsEmpty_m16186_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<System.Object>::TraverseItems(Common.Utils.SimpleList`1/Visitor<T>)
extern "C" void SimpleList_1_TraverseItems_m16188_gshared (SimpleList_1_t2598 * __this, Visitor_t2600 * ___visitor, const MethodInfo* method);
#define SimpleList_1_TraverseItems_m16188(__this, ___visitor, method) (( void (*) (SimpleList_1_t2598 *, Visitor_t2600 *, const MethodInfo*))SimpleList_1_TraverseItems_m16188_gshared)(__this, ___visitor, method)
