﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RegisterSceneRoot
struct RegisterSceneRoot_t225;
// Common.Signal.ISignalParameters
struct ISignalParameters_t115;
// System.String
struct String_t;
// UnityEngine.UI.InputField
struct InputField_t226;

// System.Void RegisterSceneRoot::.ctor()
extern "C" void RegisterSceneRoot__ctor_m798 (RegisterSceneRoot_t225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::Awake()
extern "C" void RegisterSceneRoot_Awake_m799 (RegisterSceneRoot_t225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::Start()
extern "C" void RegisterSceneRoot_Start_m800 (RegisterSceneRoot_t225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::OnEnable()
extern "C" void RegisterSceneRoot_OnEnable_m801 (RegisterSceneRoot_t225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::OnDisable()
extern "C" void RegisterSceneRoot_OnDisable_m802 (RegisterSceneRoot_t225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::Update()
extern "C" void RegisterSceneRoot_Update_m803 (RegisterSceneRoot_t225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::OnClickedPlay()
extern "C" void RegisterSceneRoot_OnClickedPlay_m804 (RegisterSceneRoot_t225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::OnClickedSignUp()
extern "C" void RegisterSceneRoot_OnClickedSignUp_m805 (RegisterSceneRoot_t225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::OnRegisterSuccessful(Common.Signal.ISignalParameters)
extern "C" void RegisterSceneRoot_OnRegisterSuccessful_m806 (RegisterSceneRoot_t225 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::OnFirstnameEndEdit(System.String)
extern "C" void RegisterSceneRoot_OnFirstnameEndEdit_m807 (RegisterSceneRoot_t225 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::OnLastnameEndEdit(System.String)
extern "C" void RegisterSceneRoot_OnLastnameEndEdit_m808 (RegisterSceneRoot_t225 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::OnMobileEndEdit(System.String)
extern "C" void RegisterSceneRoot_OnMobileEndEdit_m809 (RegisterSceneRoot_t225 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::OnCompanyEndEdit(System.String)
extern "C" void RegisterSceneRoot_OnCompanyEndEdit_m810 (RegisterSceneRoot_t225 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::OnClickedRegister()
extern "C" void RegisterSceneRoot_OnClickedRegister_m811 (RegisterSceneRoot_t225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::ValidateText(UnityEngine.UI.InputField,System.String)
extern "C" void RegisterSceneRoot_ValidateText_m812 (RegisterSceneRoot_t225 * __this, InputField_t226 * ___field, String_t* ___defaultText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegisterSceneRoot::ClearText(UnityEngine.UI.InputField)
extern "C" void RegisterSceneRoot_ClearText_m813 (RegisterSceneRoot_t225 * __this, InputField_t226 * ___field, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RegisterSceneRoot::HasValidFields()
extern "C" bool RegisterSceneRoot_HasValidFields_m814 (RegisterSceneRoot_t225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RegisterSceneRoot::HasValidField(UnityEngine.UI.InputField)
extern "C" bool RegisterSceneRoot_HasValidField_m815 (RegisterSceneRoot_t225 * __this, InputField_t226 * ___input, const MethodInfo* method) IL2CPP_METHOD_ATTR;
