﻿#pragma once
#include <stdint.h>
// InputLayer[]
struct InputLayerU5BU5D_t62;
// InputLayer
struct InputLayer_t56;
// System.Collections.Generic.List`1<InputLayer>
struct List_1_t63;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// InputLayerStack
struct  InputLayerStack_t61  : public MonoBehaviour_t5
{
	// InputLayer[] InputLayerStack::initialLayers
	InputLayerU5BU5D_t62* ___initialLayers_2;
	// InputLayer InputLayerStack::topLayer
	InputLayer_t56 * ___topLayer_3;
	// System.Collections.Generic.List`1<InputLayer> InputLayerStack::layerStack
	List_1_t63 * ___layerStack_4;
};
