﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ScannerRoot
struct ScannerRoot_t215;

// System.Void ScannerRoot::.ctor()
extern "C" void ScannerRoot__ctor_m753 (ScannerRoot_t215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerRoot::Awake()
extern "C" void ScannerRoot_Awake_m754 (ScannerRoot_t215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
