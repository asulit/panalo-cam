﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Object
struct Object_t335;
struct Object_t335_marshaled;

// System.Void Assertion::Assert(System.Boolean)
extern "C" void Assertion_Assert_m21 (Object_t * __this /* static, unused */, bool ___expression, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assertion::Assert(System.Boolean,System.String)
extern "C" void Assertion_Assert_m22 (Object_t * __this /* static, unused */, bool ___expression, String_t* ___assertErrorMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assertion::AssertNotNull(System.Object,System.String)
extern "C" void Assertion_AssertNotNull_m23 (Object_t * __this /* static, unused */, Object_t * ___pointer, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assertion::AssertNotNull(System.Object)
extern "C" void Assertion_AssertNotNull_m24 (Object_t * __this /* static, unused */, Object_t * ___pointer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assertion::AssertNotNull(UnityEngine.Object,System.String)
extern "C" void Assertion_AssertNotNull_m25 (Object_t * __this /* static, unused */, Object_t335 * ___pointer, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assertion::AssertNotNull(UnityEngine.Object)
extern "C" void Assertion_AssertNotNull_m26 (Object_t * __this /* static, unused */, Object_t335 * ___pointer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assertion::AssertNotEmpty(System.String,System.String)
extern "C" void Assertion_AssertNotEmpty_m27 (Object_t * __this /* static, unused */, String_t* ___s, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Assertion::AssertNotEmpty(System.String)
extern "C" void Assertion_AssertNotEmpty_m28 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
