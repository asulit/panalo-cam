﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable[]
struct HashtableU5BU5D_t2786;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct  List_1_t266  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Collections.Hashtable>::_items
	HashtableU5BU5D_t2786* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Collections.Hashtable>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Collections.Hashtable>::_version
	int32_t ____version_3;
};
struct List_1_t266_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Collections.Hashtable>::EmptyArray
	HashtableU5BU5D_t2786* ___EmptyArray_4;
};
