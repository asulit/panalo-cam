﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IDictionary`2<System.String,Common.Time.TimeReference>
struct IDictionary_2_t121;
// Common.Time.TimeReferencePool
struct TimeReferencePool_t120;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Time.TimeReferencePool
struct  TimeReferencePool_t120  : public Object_t
{
	// System.Collections.Generic.IDictionary`2<System.String,Common.Time.TimeReference> Common.Time.TimeReferencePool::instanceMap
	Object_t* ___instanceMap_0;
};
struct TimeReferencePool_t120_StaticFields{
	// Common.Time.TimeReferencePool Common.Time.TimeReferencePool::ONLY_INSTANCE
	TimeReferencePool_t120 * ___ONLY_INSTANCE_1;
};
