﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Logger.Log
struct Log_t72;
// Common.Logger.LogLevel
struct LogLevel_t73;
// System.String
struct String_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void Common.Logger.Log::.ctor(Common.Logger.LogLevel,System.String)
extern "C" void Log__ctor_m249 (Log_t72 * __this, LogLevel_t73 * ___level, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Logger.LogLevel Common.Logger.Log::get_Level()
extern "C" LogLevel_t73 * Log_get_Level_m250 (Log_t72 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Logger.Log::get_Message()
extern "C" String_t* Log_get_Message_m251 (Log_t72 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Common.Logger.Log::get_Timestamp()
extern "C" DateTime_t74  Log_get_Timestamp_m252 (Log_t72 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
