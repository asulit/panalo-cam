﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Channels.SinkProviderData
struct SinkProviderData_t2073;
// System.String
struct String_t;
// System.Collections.IList
struct IList_t346;
// System.Collections.IDictionary
struct IDictionary_t345;

// System.Void System.Runtime.Remoting.Channels.SinkProviderData::.ctor(System.String)
extern "C" void SinkProviderData__ctor_m12361 (SinkProviderData_t2073 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Runtime.Remoting.Channels.SinkProviderData::get_Children()
extern "C" Object_t * SinkProviderData_get_Children_m12362 (SinkProviderData_t2073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Channels.SinkProviderData::get_Properties()
extern "C" Object_t * SinkProviderData_get_Properties_m12363 (SinkProviderData_t2073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
