﻿#pragma once
#include <stdint.h>
// System.Reflection.Binder
struct Binder_t1029;
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.Binder
struct  Binder_t1029  : public Object_t
{
};
struct Binder_t1029_StaticFields{
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t1029 * ___default_binder_0;
};
