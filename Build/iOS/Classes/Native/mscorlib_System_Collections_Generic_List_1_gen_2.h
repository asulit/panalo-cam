﻿#pragma once
#include <stdint.h>
// Common.Fsm.FsmAction[]
struct FsmActionU5BU5D_t2524;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Common.Fsm.FsmAction>
struct  List_1_t49  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Common.Fsm.FsmAction>::_items
	FsmActionU5BU5D_t2524* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Common.Fsm.FsmAction>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Common.Fsm.FsmAction>::_version
	int32_t ____version_3;
};
struct List_1_t49_StaticFields{
	// T[] System.Collections.Generic.List`1<Common.Fsm.FsmAction>::EmptyArray
	FsmActionU5BU5D_t2524* ___EmptyArray_4;
};
