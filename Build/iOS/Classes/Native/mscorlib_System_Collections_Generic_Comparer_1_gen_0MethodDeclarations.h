﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<Vuforia.CameraDevice/FocusMode>
struct Comparer_1_t2732;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<Vuforia.CameraDevice/FocusMode>::.ctor()
extern "C" void Comparer_1__ctor_m17842_gshared (Comparer_1_t2732 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m17842(__this, method) (( void (*) (Comparer_1_t2732 *, const MethodInfo*))Comparer_1__ctor_m17842_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Vuforia.CameraDevice/FocusMode>::.cctor()
extern "C" void Comparer_1__cctor_m17843_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m17843(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m17843_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m17844_gshared (Comparer_1_t2732 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m17844(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2732 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m17844_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.CameraDevice/FocusMode>::get_Default()
extern "C" Comparer_1_t2732 * Comparer_1_get_Default_m17845_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m17845(__this /* static, unused */, method) (( Comparer_1_t2732 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m17845_gshared)(__this /* static, unused */, method)
