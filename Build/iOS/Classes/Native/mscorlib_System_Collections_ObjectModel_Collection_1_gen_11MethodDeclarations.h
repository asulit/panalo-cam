﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>
struct Collection_1_t3277;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3251;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t3749;
// System.Collections.Generic.IList`1<Vuforia.Image/PIXEL_FORMAT>
struct IList_1_t3276;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C" void Collection_1__ctor_m25326_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1__ctor_m25326(__this, method) (( void (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1__ctor_m25326_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25327_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25327(__this, method) (( bool (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25327_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m25328_gshared (Collection_1_t3277 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m25328(__this, ___array, ___index, method) (( void (*) (Collection_1_t3277 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m25328_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m25329_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m25329(__this, method) (( Object_t * (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m25329_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m25330_gshared (Collection_1_t3277 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m25330(__this, ___value, method) (( int32_t (*) (Collection_1_t3277 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m25330_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m25331_gshared (Collection_1_t3277 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m25331(__this, ___value, method) (( bool (*) (Collection_1_t3277 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m25331_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m25332_gshared (Collection_1_t3277 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m25332(__this, ___value, method) (( int32_t (*) (Collection_1_t3277 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m25332_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m25333_gshared (Collection_1_t3277 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m25333(__this, ___index, ___value, method) (( void (*) (Collection_1_t3277 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m25333_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m25334_gshared (Collection_1_t3277 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m25334(__this, ___value, method) (( void (*) (Collection_1_t3277 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m25334_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m25335_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m25335(__this, method) (( bool (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m25335_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m25336_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m25336(__this, method) (( Object_t * (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m25336_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m25337_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m25337(__this, method) (( bool (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m25337_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m25338_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m25338(__this, method) (( bool (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m25338_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m25339_gshared (Collection_1_t3277 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m25339(__this, ___index, method) (( Object_t * (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m25339_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m25340_gshared (Collection_1_t3277 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m25340(__this, ___index, ___value, method) (( void (*) (Collection_1_t3277 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m25340_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Add(T)
extern "C" void Collection_1_Add_m25341_gshared (Collection_1_t3277 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m25341(__this, ___item, method) (( void (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_Add_m25341_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Clear()
extern "C" void Collection_1_Clear_m25342_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_Clear_m25342(__this, method) (( void (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_Clear_m25342_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::ClearItems()
extern "C" void Collection_1_ClearItems_m25343_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m25343(__this, method) (( void (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_ClearItems_m25343_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
extern "C" bool Collection_1_Contains_m25344_gshared (Collection_1_t3277 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m25344(__this, ___item, method) (( bool (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_Contains_m25344_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m25345_gshared (Collection_1_t3277 * __this, PIXEL_FORMATU5BU5D_t3251* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m25345(__this, ___array, ___index, method) (( void (*) (Collection_1_t3277 *, PIXEL_FORMATU5BU5D_t3251*, int32_t, const MethodInfo*))Collection_1_CopyTo_m25345_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m25346_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m25346(__this, method) (( Object_t* (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_GetEnumerator_m25346_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m25347_gshared (Collection_1_t3277 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m25347(__this, ___item, method) (( int32_t (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m25347_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m25348_gshared (Collection_1_t3277 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m25348(__this, ___index, ___item, method) (( void (*) (Collection_1_t3277 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m25348_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m25349_gshared (Collection_1_t3277 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m25349(__this, ___index, ___item, method) (( void (*) (Collection_1_t3277 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m25349_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T)
extern "C" bool Collection_1_Remove_m25350_gshared (Collection_1_t3277 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m25350(__this, ___item, method) (( bool (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_Remove_m25350_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m25351_gshared (Collection_1_t3277 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m25351(__this, ___index, method) (( void (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m25351_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m25352_gshared (Collection_1_t3277 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m25352(__this, ___index, method) (( void (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m25352_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
extern "C" int32_t Collection_1_get_Count_m25353_gshared (Collection_1_t3277 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m25353(__this, method) (( int32_t (*) (Collection_1_t3277 *, const MethodInfo*))Collection_1_get_Count_m25353_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m25354_gshared (Collection_1_t3277 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m25354(__this, ___index, method) (( int32_t (*) (Collection_1_t3277 *, int32_t, const MethodInfo*))Collection_1_get_Item_m25354_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m25355_gshared (Collection_1_t3277 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m25355(__this, ___index, ___value, method) (( void (*) (Collection_1_t3277 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m25355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m25356_gshared (Collection_1_t3277 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m25356(__this, ___index, ___item, method) (( void (*) (Collection_1_t3277 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m25356_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m25357_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m25357(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m25357_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m25358_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m25358(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m25358_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m25359_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m25359(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m25359_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m25360_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m25360(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m25360_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.Image/PIXEL_FORMAT>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m25361_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m25361(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m25361_gshared)(__this /* static, unused */, ___list, method)
