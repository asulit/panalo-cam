﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t155;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.GameObject>
struct  Predicate_1_t2864  : public MulticastDelegate_t28
{
};
