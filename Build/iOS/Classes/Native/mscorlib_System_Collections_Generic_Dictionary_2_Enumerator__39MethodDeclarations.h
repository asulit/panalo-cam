﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__38MethodDeclarations.h"
#define Enumerator__ctor_m26517(__this, ___dictionary, method) (( void (*) (Enumerator_t3355 *, Dictionary_2_t1175 *, const MethodInfo*))Enumerator__ctor_m26415_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26518(__this, method) (( Object_t * (*) (Enumerator_t3355 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26416_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26519(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3355 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26417_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26520(__this, method) (( Object_t * (*) (Enumerator_t3355 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26418_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26521(__this, method) (( Object_t * (*) (Enumerator_t3355 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26419_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::MoveNext()
#define Enumerator_MoveNext_m26522(__this, method) (( bool (*) (Enumerator_t3355 *, const MethodInfo*))Enumerator_MoveNext_m26420_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_Current()
#define Enumerator_get_Current_m26523(__this, method) (( KeyValuePair_2_t3352  (*) (Enumerator_t3355 *, const MethodInfo*))Enumerator_get_Current_m26421_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m26524(__this, method) (( Type_t * (*) (Enumerator_t3355 *, const MethodInfo*))Enumerator_get_CurrentKey_m26422_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m26525(__this, method) (( uint16_t (*) (Enumerator_t3355 *, const MethodInfo*))Enumerator_get_CurrentValue_m26423_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyState()
#define Enumerator_VerifyState_m26526(__this, method) (( void (*) (Enumerator_t3355 *, const MethodInfo*))Enumerator_VerifyState_m26424_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m26527(__this, method) (( void (*) (Enumerator_t3355 *, const MethodInfo*))Enumerator_VerifyCurrent_m26425_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::Dispose()
#define Enumerator_Dispose_m26528(__this, method) (( void (*) (Enumerator_t3355 *, const MethodInfo*))Enumerator_Dispose_m26426_gshared)(__this, method)
