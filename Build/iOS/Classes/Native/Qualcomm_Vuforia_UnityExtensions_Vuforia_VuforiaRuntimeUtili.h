﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.VuforiaRuntimeUtilities/WebCamUsed
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtili_0.h"
// Vuforia.VuforiaRuntimeUtilities
struct  VuforiaRuntimeUtilities_t468  : public Object_t
{
};
struct VuforiaRuntimeUtilities_t468_StaticFields{
	// Vuforia.VuforiaRuntimeUtilities/WebCamUsed Vuforia.VuforiaRuntimeUtilities::sWebCamUsed
	int32_t ___sWebCamUsed_0;
};
