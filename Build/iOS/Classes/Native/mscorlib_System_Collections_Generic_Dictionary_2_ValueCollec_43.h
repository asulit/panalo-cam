﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
struct Dictionary_2_t2688;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>
struct  ValueCollection_t2697  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::dictionary
	Dictionary_2_t2688 * ___dictionary_0;
};
