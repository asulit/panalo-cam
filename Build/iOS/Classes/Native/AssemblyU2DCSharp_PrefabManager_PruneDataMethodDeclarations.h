﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PrefabManager/PruneData
struct PruneData_t93;
// System.String
struct String_t;

// System.Void PrefabManager/PruneData::.ctor()
extern "C" void PruneData__ctor_m320 (PruneData_t93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager/PruneData::Set(System.String,System.Int32)
extern "C" void PruneData_Set_m321 (PruneData_t93 * __this, String_t* ___prefabName, int32_t ___maxInactiveCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrefabManager/PruneData::get_PrefabName()
extern "C" String_t* PruneData_get_PrefabName_m322 (PruneData_t93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PrefabManager/PruneData::get_MaxInactiveCount()
extern "C" int32_t PruneData_get_MaxInactiveCount_m323 (PruneData_t93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
