﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DatabaseLoadBehaviour
struct DatabaseLoadBehaviour_t274;

// System.Void Vuforia.DatabaseLoadBehaviour::.ctor()
extern "C" void DatabaseLoadBehaviour__ctor_m1180 (DatabaseLoadBehaviour_t274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DatabaseLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m1181 (DatabaseLoadBehaviour_t274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
