﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoDocumentationNoteAttribute
struct MonoDocumentationNoteAttribute_t1798;
// System.String
struct String_t;

// System.Void System.MonoDocumentationNoteAttribute::.ctor(System.String)
extern "C" void MonoDocumentationNoteAttribute__ctor_m10402 (MonoDocumentationNoteAttribute_t1798 * __this, String_t* ___comment, const MethodInfo* method) IL2CPP_METHOD_ATTR;
