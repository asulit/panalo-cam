﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// iTween
struct iTween_t258;
// System.Object
#include "mscorlib_System_Object.h"
// iTween/<TweenRestart>c__IteratorD
struct  U3CTweenRestartU3Ec__IteratorD_t259  : public Object_t
{
	// System.Int32 iTween/<TweenRestart>c__IteratorD::$PC
	int32_t ___U24PC_0;
	// System.Object iTween/<TweenRestart>c__IteratorD::$current
	Object_t * ___U24current_1;
	// iTween iTween/<TweenRestart>c__IteratorD::<>f__this
	iTween_t258 * ___U3CU3Ef__this_2;
};
