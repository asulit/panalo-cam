﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
struct ArrayNullFiller_t2170;

// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller::.ctor(System.Int32)
extern "C" void ArrayNullFiller__ctor_m12791 (ArrayNullFiller_t2170 * __this, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
