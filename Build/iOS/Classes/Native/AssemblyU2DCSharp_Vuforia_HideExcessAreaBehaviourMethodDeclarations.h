﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.HideExcessAreaBehaviour
struct HideExcessAreaBehaviour_t283;

// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern "C" void HideExcessAreaBehaviour__ctor_m1205 (HideExcessAreaBehaviour_t283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
