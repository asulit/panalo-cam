﻿#pragma once
#include <stdint.h>
// Common.Fsm.FsmState
struct FsmState_t29;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Fsm.FsmActionAdapter
struct  FsmActionAdapter_t33  : public Object_t
{
	// Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::owner
	Object_t * ___owner_0;
};
