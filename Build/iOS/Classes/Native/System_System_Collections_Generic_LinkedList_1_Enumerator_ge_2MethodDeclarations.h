﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t1152;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_2.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C" void Enumerator__ctor_m26067_gshared (Enumerator_t3318 * __this, LinkedList_1_t1152 * ___parent, const MethodInfo* method);
#define Enumerator__ctor_m26067(__this, ___parent, method) (( void (*) (Enumerator_t3318 *, LinkedList_1_t1152 *, const MethodInfo*))Enumerator__ctor_m26067_gshared)(__this, ___parent, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26068_gshared (Enumerator_t3318 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26068(__this, method) (( Object_t * (*) (Enumerator_t3318 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26068_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m26069_gshared (Enumerator_t3318 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26069(__this, method) (( int32_t (*) (Enumerator_t3318 *, const MethodInfo*))Enumerator_get_Current_m26069_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26070_gshared (Enumerator_t3318 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26070(__this, method) (( bool (*) (Enumerator_t3318 *, const MethodInfo*))Enumerator_MoveNext_m26070_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m26071_gshared (Enumerator_t3318 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m26071(__this, method) (( void (*) (Enumerator_t3318 *, const MethodInfo*))Enumerator_Dispose_m26071_gshared)(__this, method)
