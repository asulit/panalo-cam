﻿#pragma once
#include <stdint.h>
// Common.Fsm.FsmDelegateAction/FsmActionRoutine
struct FsmActionRoutine_t27;
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapter.h"
// Common.Fsm.FsmDelegateAction
struct  FsmDelegateAction_t32  : public FsmActionAdapter_t33
{
	// Common.Fsm.FsmDelegateAction/FsmActionRoutine Common.Fsm.FsmDelegateAction::onEnterRoutine
	FsmActionRoutine_t27 * ___onEnterRoutine_1;
	// Common.Fsm.FsmDelegateAction/FsmActionRoutine Common.Fsm.FsmDelegateAction::onUpdateRoutine
	FsmActionRoutine_t27 * ___onUpdateRoutine_2;
	// Common.Fsm.FsmDelegateAction/FsmActionRoutine Common.Fsm.FsmDelegateAction::onExitRoutine
	FsmActionRoutine_t27 * ___onExitRoutine_3;
};
