﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2412_t1556_marshal(const U24ArrayTypeU2412_t1556& unmarshaled, U24ArrayTypeU2412_t1556_marshaled& marshaled);
extern "C" void U24ArrayTypeU2412_t1556_marshal_back(const U24ArrayTypeU2412_t1556_marshaled& marshaled, U24ArrayTypeU2412_t1556& unmarshaled);
extern "C" void U24ArrayTypeU2412_t1556_marshal_cleanup(U24ArrayTypeU2412_t1556_marshaled& marshaled);
