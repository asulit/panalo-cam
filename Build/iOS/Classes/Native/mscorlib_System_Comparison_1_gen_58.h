﻿#pragma once
#include <stdint.h>
// Vuforia.VirtualButton
struct VirtualButton_t1223;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.VirtualButton>
struct  Comparison_1_t3250  : public MulticastDelegate_t28
{
};
