﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaRenderer
struct VuforiaRenderer_t1158;
// Vuforia.VuforiaRendererImpl
struct VuforiaRendererImpl_t1160;

// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::get_Instance()
extern "C" VuforiaRenderer_t1158 * VuforiaRenderer_get_Instance_m5769 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRendererImpl Vuforia.VuforiaRenderer::get_InternalInstance()
extern "C" VuforiaRendererImpl_t1160 * VuforiaRenderer_get_InternalInstance_m5770 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRenderer::.ctor()
extern "C" void VuforiaRenderer__ctor_m5771 (VuforiaRenderer_t1158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRenderer::.cctor()
extern "C" void VuforiaRenderer__cctor_m5772 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
