﻿#pragma once
#include <stdint.h>
// Common.Xml.SimpleXmlNode
struct SimpleXmlNode_t142;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Common.Xml.SimpleXmlNode>
struct  Predicate_1_t2639  : public MulticastDelegate_t28
{
};
