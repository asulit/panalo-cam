﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Predicate_1_t1310;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.VuforiaManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m7286_gshared (Predicate_1_t1310 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m7286(__this, ___object, ___method, method) (( void (*) (Predicate_1_t1310 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m7286_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m26072_gshared (Predicate_1_t1310 * __this, TrackableResultData_t1134  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m26072(__this, ___obj, method) (( bool (*) (Predicate_1_t1310 *, TrackableResultData_t1134 , const MethodInfo*))Predicate_1_Invoke_m26072_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m26073_gshared (Predicate_1_t1310 * __this, TrackableResultData_t1134  ___obj, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m26073(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t1310 *, TrackableResultData_t1134 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m26073_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m26074_gshared (Predicate_1_t1310 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m26074(__this, ___result, method) (( bool (*) (Predicate_1_t1310 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m26074_gshared)(__this, ___result, method)
