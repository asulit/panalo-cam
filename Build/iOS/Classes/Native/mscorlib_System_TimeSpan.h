﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.TimeSpan
struct  TimeSpan_t427 
{
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;
};
struct TimeSpan_t427_StaticFields{
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t427  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t427  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t427  ___Zero_7;
};
