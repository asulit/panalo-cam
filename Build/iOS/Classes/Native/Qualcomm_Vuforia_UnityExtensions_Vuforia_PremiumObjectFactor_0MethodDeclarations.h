﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.IPremiumObjectFactory
struct IPremiumObjectFactory_t1132;
// Vuforia.PremiumObjectFactory
struct PremiumObjectFactory_t1131;

// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::get_Instance()
extern "C" Object_t * PremiumObjectFactory_get_Instance_m5731 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PremiumObjectFactory::set_Instance(Vuforia.IPremiumObjectFactory)
extern "C" void PremiumObjectFactory_set_Instance_m5732 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PremiumObjectFactory::.ctor()
extern "C" void PremiumObjectFactory__ctor_m5733 (PremiumObjectFactory_t1131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
