﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
struct List_1_t1067;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t1253;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>
struct  Enumerator_t1291 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::l
	List_1_t1067 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::current
	Object_t * ___current_3;
};
