﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"
#define Enumerator__ctor_m18181(__this, ___dictionary, method) (( void (*) (Enumerator_t2763 *, Dictionary_2_t228 *, const MethodInfo*))Enumerator__ctor_m15140_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18182(__this, method) (( Object_t * (*) (Enumerator_t2763 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15141_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18183(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t2763 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15142_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18184(__this, method) (( Object_t * (*) (Enumerator_t2763 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15143_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18185(__this, method) (( Object_t * (*) (Enumerator_t2763 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::MoveNext()
#define Enumerator_MoveNext_m18186(__this, method) (( bool (*) (Enumerator_t2763 *, const MethodInfo*))Enumerator_MoveNext_m15145_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::get_Current()
#define Enumerator_get_Current_m18187(__this, method) (( KeyValuePair_2_t2760  (*) (Enumerator_t2763 *, const MethodInfo*))Enumerator_get_Current_m15146_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18188(__this, method) (( InputField_t226 * (*) (Enumerator_t2763 *, const MethodInfo*))Enumerator_get_CurrentKey_m15147_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18189(__this, method) (( String_t* (*) (Enumerator_t2763 *, const MethodInfo*))Enumerator_get_CurrentValue_m15148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::VerifyState()
#define Enumerator_VerifyState_m18190(__this, method) (( void (*) (Enumerator_t2763 *, const MethodInfo*))Enumerator_VerifyState_m15149_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18191(__this, method) (( void (*) (Enumerator_t2763 *, const MethodInfo*))Enumerator_VerifyCurrent_m15150_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,System.String>::Dispose()
#define Enumerator_Dispose_m18192(__this, method) (( void (*) (Enumerator_t2763 *, const MethodInfo*))Enumerator_Dispose_m15151_gshared)(__this, method)
