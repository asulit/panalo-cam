﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.DelegateCommand
struct DelegateCommand_t15;
// System.Action
struct Action_t16;

// System.Void Common.DelegateCommand::.ctor(System.Action)
extern "C" void DelegateCommand__ctor_m75 (DelegateCommand_t15 * __this, Action_t16 * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.DelegateCommand::Execute()
extern "C" void DelegateCommand_Execute_m76 (DelegateCommand_t15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
