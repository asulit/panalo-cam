﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Query.QueryResultResolver
struct QueryResultResolver_t328;
// System.Object
struct Object_t;
// Common.Query.IQueryRequest
struct IQueryRequest_t329;
// Common.Query.IMutableQueryResult
struct IMutableQueryResult_t330;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Common.Query.QueryResultResolver::.ctor(System.Object,System.IntPtr)
extern "C" void QueryResultResolver__ctor_m1285 (QueryResultResolver_t328 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Query.QueryResultResolver::Invoke(Common.Query.IQueryRequest,Common.Query.IMutableQueryResult)
extern "C" void QueryResultResolver_Invoke_m1286 (QueryResultResolver_t328 * __this, Object_t * ___request, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_QueryResultResolver_t328(Il2CppObject* delegate, Object_t * ___request, Object_t * ___result);
// System.IAsyncResult Common.Query.QueryResultResolver::BeginInvoke(Common.Query.IQueryRequest,Common.Query.IMutableQueryResult,System.AsyncCallback,System.Object)
extern "C" Object_t * QueryResultResolver_BeginInvoke_m1287 (QueryResultResolver_t328 * __this, Object_t * ___request, Object_t * ___result, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Query.QueryResultResolver::EndInvoke(System.IAsyncResult)
extern "C" void QueryResultResolver_EndInvoke_m1288 (QueryResultResolver_t328 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
