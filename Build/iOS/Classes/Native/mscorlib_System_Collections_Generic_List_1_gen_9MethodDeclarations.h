﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>
struct List_1_t214;
// System.Collections.Generic.IEnumerable`1<Vuforia.CameraDevice/FocusMode>
struct IEnumerable_1_t3663;
// System.Collections.Generic.IEnumerator`1<Vuforia.CameraDevice/FocusMode>
struct IEnumerator_1_t3664;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<Vuforia.CameraDevice/FocusMode>
struct ICollection_1_t3665;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>
struct ReadOnlyCollection_1_t2726;
// Vuforia.CameraDevice/FocusMode[]
struct FocusModeU5BU5D_t2723;
// System.Predicate`1<Vuforia.CameraDevice/FocusMode>
struct Predicate_1_t2731;
// System.Comparison`1<Vuforia.CameraDevice/FocusMode>
struct Comparison_1_t2734;
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_38.h"

// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::.ctor()
extern "C" void List_1__ctor_m1682_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1__ctor_m1682(__this, method) (( void (*) (List_1_t214 *, const MethodInfo*))List_1__ctor_m1682_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m17702_gshared (List_1_t214 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m17702(__this, ___collection, method) (( void (*) (List_1_t214 *, Object_t*, const MethodInfo*))List_1__ctor_m17702_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::.ctor(System.Int32)
extern "C" void List_1__ctor_m17703_gshared (List_1_t214 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m17703(__this, ___capacity, method) (( void (*) (List_1_t214 *, int32_t, const MethodInfo*))List_1__ctor_m17703_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::.cctor()
extern "C" void List_1__cctor_m17704_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17704(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17704_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17705_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17705(__this, method) (( Object_t* (*) (List_1_t214 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17706_gshared (List_1_t214 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17706(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t214 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17706_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17707_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17707(__this, method) (( Object_t * (*) (List_1_t214 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17707_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17708_gshared (List_1_t214 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17708(__this, ___item, method) (( int32_t (*) (List_1_t214 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17708_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17709_gshared (List_1_t214 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17709(__this, ___item, method) (( bool (*) (List_1_t214 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17709_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17710_gshared (List_1_t214 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17710(__this, ___item, method) (( int32_t (*) (List_1_t214 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17710_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17711_gshared (List_1_t214 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17711(__this, ___index, ___item, method) (( void (*) (List_1_t214 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17711_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17712_gshared (List_1_t214 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17712(__this, ___item, method) (( void (*) (List_1_t214 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17712_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17713_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17713(__this, method) (( bool (*) (List_1_t214 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17713_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m17714_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17714(__this, method) (( bool (*) (List_1_t214 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m17714_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m17715_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m17715(__this, method) (( Object_t * (*) (List_1_t214 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m17715_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17716_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17716(__this, method) (( bool (*) (List_1_t214 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17716_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m17717_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m17717(__this, method) (( bool (*) (List_1_t214 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m17717_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17718_gshared (List_1_t214 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17718(__this, ___index, method) (( Object_t * (*) (List_1_t214 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17718_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17719_gshared (List_1_t214 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17719(__this, ___index, ___value, method) (( void (*) (List_1_t214 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17719_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::Add(T)
extern "C" void List_1_Add_m17720_gshared (List_1_t214 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m17720(__this, ___item, method) (( void (*) (List_1_t214 *, int32_t, const MethodInfo*))List_1_Add_m17720_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m17721_gshared (List_1_t214 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m17721(__this, ___newCount, method) (( void (*) (List_1_t214 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m17721_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m17722_gshared (List_1_t214 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m17722(__this, ___idx, ___count, method) (( void (*) (List_1_t214 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m17722_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m17723_gshared (List_1_t214 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m17723(__this, ___collection, method) (( void (*) (List_1_t214 *, Object_t*, const MethodInfo*))List_1_AddCollection_m17723_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m17724_gshared (List_1_t214 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m17724(__this, ___enumerable, method) (( void (*) (List_1_t214 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m17724_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m17725_gshared (List_1_t214 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m17725(__this, ___collection, method) (( void (*) (List_1_t214 *, Object_t*, const MethodInfo*))List_1_AddRange_m17725_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2726 * List_1_AsReadOnly_m17726_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m17726(__this, method) (( ReadOnlyCollection_1_t2726 * (*) (List_1_t214 *, const MethodInfo*))List_1_AsReadOnly_m17726_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::Clear()
extern "C" void List_1_Clear_m17727_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_Clear_m17727(__this, method) (( void (*) (List_1_t214 *, const MethodInfo*))List_1_Clear_m17727_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::Contains(T)
extern "C" bool List_1_Contains_m17728_gshared (List_1_t214 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m17728(__this, ___item, method) (( bool (*) (List_1_t214 *, int32_t, const MethodInfo*))List_1_Contains_m17728_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17729_gshared (List_1_t214 * __this, FocusModeU5BU5D_t2723* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m17729(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t214 *, FocusModeU5BU5D_t2723*, int32_t, const MethodInfo*))List_1_CopyTo_m17729_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m17730_gshared (List_1_t214 * __this, Predicate_1_t2731 * ___match, const MethodInfo* method);
#define List_1_Find_m17730(__this, ___match, method) (( int32_t (*) (List_1_t214 *, Predicate_1_t2731 *, const MethodInfo*))List_1_Find_m17730_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m17731_gshared (Object_t * __this /* static, unused */, Predicate_1_t2731 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m17731(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2731 *, const MethodInfo*))List_1_CheckMatch_m17731_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m17732_gshared (List_1_t214 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2731 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m17732(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t214 *, int32_t, int32_t, Predicate_1_t2731 *, const MethodInfo*))List_1_GetIndex_m17732_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::GetEnumerator()
extern "C" Enumerator_t2725  List_1_GetEnumerator_m17733_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m17733(__this, method) (( Enumerator_t2725  (*) (List_1_t214 *, const MethodInfo*))List_1_GetEnumerator_m17733_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17734_gshared (List_1_t214 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m17734(__this, ___item, method) (( int32_t (*) (List_1_t214 *, int32_t, const MethodInfo*))List_1_IndexOf_m17734_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m17735_gshared (List_1_t214 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m17735(__this, ___start, ___delta, method) (( void (*) (List_1_t214 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m17735_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m17736_gshared (List_1_t214 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17736(__this, ___index, method) (( void (*) (List_1_t214 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17736_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m17737_gshared (List_1_t214 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m17737(__this, ___index, ___item, method) (( void (*) (List_1_t214 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m17737_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m17738_gshared (List_1_t214 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m17738(__this, ___collection, method) (( void (*) (List_1_t214 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m17738_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::Remove(T)
extern "C" bool List_1_Remove_m17739_gshared (List_1_t214 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m17739(__this, ___item, method) (( bool (*) (List_1_t214 *, int32_t, const MethodInfo*))List_1_Remove_m17739_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m17740_gshared (List_1_t214 * __this, Predicate_1_t2731 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m17740(__this, ___match, method) (( int32_t (*) (List_1_t214 *, Predicate_1_t2731 *, const MethodInfo*))List_1_RemoveAll_m17740_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m17741_gshared (List_1_t214 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17741(__this, ___index, method) (( void (*) (List_1_t214 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17741_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m17742_gshared (List_1_t214 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m17742(__this, ___index, ___count, method) (( void (*) (List_1_t214 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m17742_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::Reverse()
extern "C" void List_1_Reverse_m17743_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_Reverse_m17743(__this, method) (( void (*) (List_1_t214 *, const MethodInfo*))List_1_Reverse_m17743_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::Sort()
extern "C" void List_1_Sort_m17744_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_Sort_m17744(__this, method) (( void (*) (List_1_t214 *, const MethodInfo*))List_1_Sort_m17744_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m17745_gshared (List_1_t214 * __this, Comparison_1_t2734 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m17745(__this, ___comparison, method) (( void (*) (List_1_t214 *, Comparison_1_t2734 *, const MethodInfo*))List_1_Sort_m17745_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::ToArray()
extern "C" FocusModeU5BU5D_t2723* List_1_ToArray_m17746_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_ToArray_m17746(__this, method) (( FocusModeU5BU5D_t2723* (*) (List_1_t214 *, const MethodInfo*))List_1_ToArray_m17746_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::TrimExcess()
extern "C" void List_1_TrimExcess_m17747_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m17747(__this, method) (( void (*) (List_1_t214 *, const MethodInfo*))List_1_TrimExcess_m17747_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m17748_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m17748(__this, method) (( int32_t (*) (List_1_t214 *, const MethodInfo*))List_1_get_Capacity_m17748_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m17749_gshared (List_1_t214 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m17749(__this, ___value, method) (( void (*) (List_1_t214 *, int32_t, const MethodInfo*))List_1_set_Capacity_m17749_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::get_Count()
extern "C" int32_t List_1_get_Count_m17750_gshared (List_1_t214 * __this, const MethodInfo* method);
#define List_1_get_Count_m17750(__this, method) (( int32_t (*) (List_1_t214 *, const MethodInfo*))List_1_get_Count_m17750_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m17751_gshared (List_1_t214 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17751(__this, ___index, method) (( int32_t (*) (List_1_t214 *, int32_t, const MethodInfo*))List_1_get_Item_m17751_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17752_gshared (List_1_t214 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m17752(__this, ___index, ___value, method) (( void (*) (List_1_t214 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m17752_gshared)(__this, ___index, ___value, method)
