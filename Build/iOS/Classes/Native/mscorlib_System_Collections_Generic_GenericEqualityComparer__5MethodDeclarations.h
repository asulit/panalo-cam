﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>
struct GenericEqualityComparer_1_t3350;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m26473_gshared (GenericEqualityComparer_1_t3350 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m26473(__this, method) (( void (*) (GenericEqualityComparer_1_t3350 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m26473_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m26474_gshared (GenericEqualityComparer_1_t3350 * __this, uint16_t ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m26474(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t3350 *, uint16_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m26474_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m26475_gshared (GenericEqualityComparer_1_t3350 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m26475(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t3350 *, uint16_t, uint16_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m26475_gshared)(__this, ___x, ___y, method)
