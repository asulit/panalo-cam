﻿#pragma once
#include <stdint.h>
// Common.Fsm.FsmState
struct FsmState_t29;
// Common.Fsm.Fsm
struct Fsm_t47;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Fsm.Fsm/<ExitState>c__AnonStoreyF
struct  U3CExitStateU3Ec__AnonStoreyF_t52  : public Object_t
{
	// Common.Fsm.FsmState Common.Fsm.Fsm/<ExitState>c__AnonStoreyF::currentStateOnInvoke
	Object_t * ___currentStateOnInvoke_0;
	// Common.Fsm.Fsm Common.Fsm.Fsm/<ExitState>c__AnonStoreyF::<>f__this
	Fsm_t47 * ___U3CU3Ef__this_1;
};
