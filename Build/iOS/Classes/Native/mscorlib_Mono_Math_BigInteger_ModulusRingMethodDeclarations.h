﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t1828;
// Mono.Math.BigInteger
struct BigInteger_t1829;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
extern "C" void ModulusRing__ctor_m10527 (ModulusRing_t1828 * __this, BigInteger_t1829 * ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
extern "C" void ModulusRing_BarrettReduction_m10528 (ModulusRing_t1828 * __this, BigInteger_t1829 * ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1829 * ModulusRing_Multiply_m10529 (ModulusRing_t1828 * __this, BigInteger_t1829 * ___a, BigInteger_t1829 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1829 * ModulusRing_Difference_m10530 (ModulusRing_t1828 * __this, BigInteger_t1829 * ___a, BigInteger_t1829 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1829 * ModulusRing_Pow_m10531 (ModulusRing_t1828 * __this, BigInteger_t1829 * ___a, BigInteger_t1829 * ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
extern "C" BigInteger_t1829 * ModulusRing_Pow_m10532 (ModulusRing_t1828 * __this, uint32_t ___b, BigInteger_t1829 * ___exp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
