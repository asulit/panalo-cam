﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t2459;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
extern "C" void GenericComparer_1__ctor_m14600_gshared (GenericComparer_1_t2459 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m14600(__this, method) (( void (*) (GenericComparer_1_t2459 *, const MethodInfo*))GenericComparer_1__ctor_m14600_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m29964_gshared (GenericComparer_1_t2459 * __this, DateTime_t74  ___x, DateTime_t74  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m29964(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2459 *, DateTime_t74 , DateTime_t74 , const MethodInfo*))GenericComparer_1_Compare_m29964_gshared)(__this, ___x, ___y, method)
