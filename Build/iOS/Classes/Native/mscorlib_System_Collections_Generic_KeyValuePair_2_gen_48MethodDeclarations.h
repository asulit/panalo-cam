﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_48.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m28748_gshared (KeyValuePair_2_t3478 * __this, Object_t * ___key, ProfileData_t1226  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m28748(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3478 *, Object_t *, ProfileData_t1226 , const MethodInfo*))KeyValuePair_2__ctor_m28748_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m28749_gshared (KeyValuePair_2_t3478 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m28749(__this, method) (( Object_t * (*) (KeyValuePair_2_t3478 *, const MethodInfo*))KeyValuePair_2_get_Key_m28749_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m28750_gshared (KeyValuePair_2_t3478 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m28750(__this, ___value, method) (( void (*) (KeyValuePair_2_t3478 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m28750_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C" ProfileData_t1226  KeyValuePair_2_get_Value_m28751_gshared (KeyValuePair_2_t3478 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m28751(__this, method) (( ProfileData_t1226  (*) (KeyValuePair_2_t3478 *, const MethodInfo*))KeyValuePair_2_get_Value_m28751_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m28752_gshared (KeyValuePair_2_t3478 * __this, ProfileData_t1226  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m28752(__this, ___value, method) (( void (*) (KeyValuePair_2_t3478 *, ProfileData_t1226 , const MethodInfo*))KeyValuePair_2_set_Value_m28752_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m28753_gshared (KeyValuePair_2_t3478 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m28753(__this, method) (( String_t* (*) (KeyValuePair_2_t3478 *, const MethodInfo*))KeyValuePair_2_ToString_m28753_gshared)(__this, method)
