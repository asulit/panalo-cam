﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PanaloVideoPlayer
struct PanaloVideoPlayer_t242;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.String
struct String_t;

// System.Void PanaloVideoPlayer::.ctor()
extern "C" void PanaloVideoPlayer__ctor_m882 (PanaloVideoPlayer_t242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloVideoPlayer::Calculate()
extern "C" void PanaloVideoPlayer_Calculate_m883 (PanaloVideoPlayer_t242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloVideoPlayer::Update()
extern "C" void PanaloVideoPlayer_Update_m884 (PanaloVideoPlayer_t242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PanaloVideoPlayer::Play(System.String)
extern "C" Object_t * PanaloVideoPlayer_Play_m885 (PanaloVideoPlayer_t242 * __this, String_t* ___movieTexturePath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloVideoPlayer::OnVideoPlayerDone()
extern "C" void PanaloVideoPlayer_OnVideoPlayerDone_m886 (PanaloVideoPlayer_t242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PanaloVideoPlayer::get_IsPlaying()
extern "C" bool PanaloVideoPlayer_get_IsPlaying_m887 (PanaloVideoPlayer_t242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
