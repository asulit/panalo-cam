﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t1419  : public ArgumentException_t764
{
	// System.Object System.ArgumentOutOfRangeException::actual_value
	Object_t * ___actual_value_13;
};
