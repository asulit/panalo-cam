﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Params
struct Params_t197;

// System.Void Params::.ctor()
extern "C" void Params__ctor_m696 (Params_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Params::.cctor()
extern "C" void Params__cctor_m697 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
