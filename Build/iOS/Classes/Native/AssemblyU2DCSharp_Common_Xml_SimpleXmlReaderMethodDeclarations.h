﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Xml.SimpleXmlReader
struct SimpleXmlReader_t146;
// Common.Xml.SimpleXmlNode
struct SimpleXmlNode_t142;
// System.String
struct String_t;

// System.Void Common.Xml.SimpleXmlReader::.ctor()
extern "C" void SimpleXmlReader__ctor_m482 (SimpleXmlReader_t146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Xml.SimpleXmlReader::.cctor()
extern "C" void SimpleXmlReader__cctor_m483 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlReader::Read(System.String)
extern "C" SimpleXmlNode_t142 * SimpleXmlReader_Read_m484 (SimpleXmlReader_t146 * __this, String_t* ___xml, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlReader::ParseTag(System.String)
extern "C" SimpleXmlNode_t142 * SimpleXmlReader_ParseTag_m485 (SimpleXmlReader_t146 * __this, String_t* ___xmlTag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlReader::ParseAttributes(System.String,Common.Xml.SimpleXmlNode)
extern "C" SimpleXmlNode_t142 * SimpleXmlReader_ParseAttributes_m486 (SimpleXmlReader_t146 * __this, String_t* ___xmlTag, SimpleXmlNode_t142 * ___node, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Xml.SimpleXmlReader::PrintXML(Common.Xml.SimpleXmlNode,System.Int32)
extern "C" void SimpleXmlReader_PrintXML_m487 (SimpleXmlReader_t146 * __this, SimpleXmlNode_t142 * ___node, int32_t ___indent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
