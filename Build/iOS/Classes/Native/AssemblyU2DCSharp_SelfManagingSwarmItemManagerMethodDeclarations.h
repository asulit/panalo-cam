﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SelfManagingSwarmItemManager
struct SelfManagingSwarmItemManager_t99;

// System.Void SelfManagingSwarmItemManager::.ctor()
extern "C" void SelfManagingSwarmItemManager__ctor_m380 (SelfManagingSwarmItemManager_t99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelfManagingSwarmItemManager::Awake()
extern "C" void SelfManagingSwarmItemManager_Awake_m381 (SelfManagingSwarmItemManager_t99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelfManagingSwarmItemManager::Update()
extern "C" void SelfManagingSwarmItemManager_Update_m382 (SelfManagingSwarmItemManager_t99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelfManagingSwarmItemManager::KillAllActiveItems()
extern "C" void SelfManagingSwarmItemManager_KillAllActiveItems_m383 (SelfManagingSwarmItemManager_t99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelfManagingSwarmItemManager::PruneInactiveList(System.Int32,System.Int32)
extern "C" void SelfManagingSwarmItemManager_PruneInactiveList_m384 (SelfManagingSwarmItemManager_t99 * __this, int32_t ___itemPrefabIndex, int32_t ___maxCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelfManagingSwarmItemManager::Preload(System.Int32,System.Int32)
extern "C" void SelfManagingSwarmItemManager_Preload_m385 (SelfManagingSwarmItemManager_t99 * __this, int32_t ___itemPrefabIndex, int32_t ___preloadCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
