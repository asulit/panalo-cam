﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_0MethodDeclarations.h"
#define ObjectPool_1__ctor_m20509(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t2919 *, UnityAction_1_t2920 *, UnityAction_1_t2920 *, const MethodInfo*))ObjectPool_1__ctor_m18958_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countAll()
#define ObjectPool_1_get_countAll_m20510(__this, method) (( int32_t (*) (ObjectPool_1_t2919 *, const MethodInfo*))ObjectPool_1_get_countAll_m18960_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m20511(__this, ___value, method) (( void (*) (ObjectPool_1_t2919 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m18962_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countActive()
#define ObjectPool_1_get_countActive_m20512(__this, method) (( int32_t (*) (ObjectPool_1_t2919 *, const MethodInfo*))ObjectPool_1_get_countActive_m18964_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m20513(__this, method) (( int32_t (*) (ObjectPool_1_t2919 *, const MethodInfo*))ObjectPool_1_get_countInactive_m18966_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Get()
#define ObjectPool_1_Get_m20514(__this, method) (( List_1_t734 * (*) (ObjectPool_1_t2919 *, const MethodInfo*))ObjectPool_1_Get_m18968_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Release(T)
#define ObjectPool_1_Release_m20515(__this, ___element, method) (( void (*) (ObjectPool_1_t2919 *, List_1_t734 *, const MethodInfo*))ObjectPool_1_Release_m18970_gshared)(__this, ___element, method)
