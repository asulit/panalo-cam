﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>
struct EqualityComparer_1_t3135;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void EqualityComparer_1__ctor_m23468_gshared (EqualityComparer_1_t3135 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m23468(__this, method) (( void (*) (EqualityComparer_1_t3135 *, const MethodInfo*))EqualityComparer_1__ctor_m23468_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void EqualityComparer_1__cctor_m23469_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m23469(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m23469_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m23470_gshared (EqualityComparer_1_t3135 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m23470(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3135 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m23470_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m23471_gshared (EqualityComparer_1_t3135 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m23471(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3135 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m23471_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::get_Default()
extern "C" EqualityComparer_1_t3135 * EqualityComparer_1_get_Default_m23472_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m23472(__this /* static, unused */, method) (( EqualityComparer_1_t3135 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m23472_gshared)(__this /* static, unused */, method)
