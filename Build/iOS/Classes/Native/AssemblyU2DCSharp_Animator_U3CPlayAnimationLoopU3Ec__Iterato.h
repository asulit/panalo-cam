﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// Animator
struct Animator_t236;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1/Enumerator<AnimatorFrame>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"
// Animator/<PlayAnimationLoop>c__Iterator9
struct  U3CPlayAnimationLoopU3Ec__Iterator9_t233  : public Object_t
{
	// System.Single Animator/<PlayAnimationLoop>c__Iterator9::delay
	float ___delay_0;
	// System.Collections.Generic.List`1/Enumerator<AnimatorFrame> Animator/<PlayAnimationLoop>c__Iterator9::<$s_68>__0
	Enumerator_t234  ___U3CU24s_68U3E__0_1;
	// AnimatorFrame Animator/<PlayAnimationLoop>c__Iterator9::<frame>__1
	AnimatorFrame_t235  ___U3CframeU3E__1_2;
	// System.Int32 Animator/<PlayAnimationLoop>c__Iterator9::$PC
	int32_t ___U24PC_3;
	// System.Object Animator/<PlayAnimationLoop>c__Iterator9::$current
	Object_t * ___U24current_4;
	// System.Single Animator/<PlayAnimationLoop>c__Iterator9::<$>delay
	float ___U3CU24U3Edelay_5;
	// Animator Animator/<PlayAnimationLoop>c__Iterator9::<>f__this
	Animator_t236 * ___U3CU3Ef__this_6;
};
