﻿#pragma once
#include <stdint.h>
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t1268;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.ILoadLevelEventHandler>
struct  Comparison_1_t3384  : public MulticastDelegate_t28
{
};
