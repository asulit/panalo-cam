﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m17615_gshared (KeyValuePair_2_t352 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m17615(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t352 *, int32_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m17615_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m1676_gshared (KeyValuePair_2_t352 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1676(__this, method) (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))KeyValuePair_2_get_Key_m1676_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m17616_gshared (KeyValuePair_2_t352 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m17616(__this, ___value, method) (( void (*) (KeyValuePair_2_t352 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m17616_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m1675_gshared (KeyValuePair_2_t352 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1675(__this, method) (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))KeyValuePair_2_get_Value_m1675_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m17617_gshared (KeyValuePair_2_t352 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m17617(__this, ___value, method) (( void (*) (KeyValuePair_2_t352 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m17617_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m17618_gshared (KeyValuePair_2_t352 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m17618(__this, method) (( String_t* (*) (KeyValuePair_2_t352 *, const MethodInfo*))KeyValuePair_2_ToString_m17618_gshared)(__this, method)
