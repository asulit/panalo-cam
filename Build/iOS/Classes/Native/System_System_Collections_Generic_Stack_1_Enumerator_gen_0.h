﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>
struct Stack_1_t87;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.Stack`1/Enumerator<Common.Notification.NotificationInstance>
struct  Enumerator_t2582 
{
	// System.Collections.Generic.Stack`1<T> System.Collections.Generic.Stack`1/Enumerator<Common.Notification.NotificationInstance>::parent
	Stack_1_t87 * ___parent_0;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator<Common.Notification.NotificationInstance>::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Stack`1/Enumerator<Common.Notification.NotificationInstance>::_version
	int32_t ____version_2;
};
