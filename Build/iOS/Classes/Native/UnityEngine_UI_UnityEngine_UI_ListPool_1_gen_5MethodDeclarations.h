﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t679;

// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
extern "C" void ListPool_1__cctor_m22933_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m22933(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m22933_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
extern "C" List_1_t679 * ListPool_1_Get_m3783_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m3783(__this /* static, unused */, method) (( List_1_t679 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m3783_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m3806_gshared (Object_t * __this /* static, unused */, List_1_t679 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m3806(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t679 *, const MethodInfo*))ListPool_1_Release_m3806_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::<s_ListPool>m__15(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__15_m22934_gshared (Object_t * __this /* static, unused */, List_1_t679 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__15_m22934(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t679 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__15_m22934_gshared)(__this /* static, unused */, ___l, method)
