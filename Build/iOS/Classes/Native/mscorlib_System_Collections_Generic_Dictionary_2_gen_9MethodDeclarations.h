﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>
struct Dictionary_2_t208;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2710;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.ICollection
struct ICollection_t1751;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>[]
struct KeyValuePair_2U5BU5D_t3660;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>
struct IEnumerator_1_t3661;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1606;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>
struct KeyCollection_t2713;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>
struct ValueCollection_t2716;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::.ctor()
extern "C" void Dictionary_2__ctor_m1658_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1658(__this, method) (( void (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2__ctor_m1658_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17565_gshared (Dictionary_2_t208 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17565(__this, ___comparer, method) (( void (*) (Dictionary_2_t208 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17565_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m17566_gshared (Dictionary_2_t208 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m17566(__this, ___capacity, method) (( void (*) (Dictionary_2_t208 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m17566_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m17567_gshared (Dictionary_2_t208 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m17567(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t208 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m17567_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m17568_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m17568(__this, method) (( Object_t * (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m17568_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17569_gshared (Dictionary_2_t208 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17569(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t208 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m17569_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17570_gshared (Dictionary_2_t208 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17570(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t208 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m17570_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17571_gshared (Dictionary_2_t208 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m17571(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t208 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m17571_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17572_gshared (Dictionary_2_t208 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m17572(__this, ___key, method) (( bool (*) (Dictionary_2_t208 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m17572_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17573_gshared (Dictionary_2_t208 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m17573(__this, ___key, method) (( void (*) (Dictionary_2_t208 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m17573_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17574_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17574(__this, method) (( bool (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17574_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17575_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17575(__this, method) (( Object_t * (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17575_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17576_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17576(__this, method) (( bool (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17576_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17577_gshared (Dictionary_2_t208 * __this, KeyValuePair_2_t352  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17577(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t208 *, KeyValuePair_2_t352 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17577_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17578_gshared (Dictionary_2_t208 * __this, KeyValuePair_2_t352  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17578(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t208 *, KeyValuePair_2_t352 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17578_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17579_gshared (Dictionary_2_t208 * __this, KeyValuePair_2U5BU5D_t3660* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17579(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t208 *, KeyValuePair_2U5BU5D_t3660*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17579_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17580_gshared (Dictionary_2_t208 * __this, KeyValuePair_2_t352  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17580(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t208 *, KeyValuePair_2_t352 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17580_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17581_gshared (Dictionary_2_t208 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17581(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t208 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m17581_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17582_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17582(__this, method) (( Object_t * (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17582_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17583_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17583(__this, method) (( Object_t* (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17583_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17584_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17584(__this, method) (( Object_t * (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17584_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17585_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m17585(__this, method) (( int32_t (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_get_Count_m17585_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m17586_gshared (Dictionary_2_t208 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m17586(__this, ___key, method) (( int32_t (*) (Dictionary_2_t208 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m17586_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m17587_gshared (Dictionary_2_t208 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m17587(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t208 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m17587_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m17588_gshared (Dictionary_2_t208 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m17588(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t208 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m17588_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m17589_gshared (Dictionary_2_t208 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m17589(__this, ___size, method) (( void (*) (Dictionary_2_t208 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m17589_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m17590_gshared (Dictionary_2_t208 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m17590(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t208 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m17590_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t352  Dictionary_2_make_pair_m17591_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m17591(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t352  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m17591_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m17592_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m17592(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m17592_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m17593_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m17593(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m17593_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17594_gshared (Dictionary_2_t208 * __this, KeyValuePair_2U5BU5D_t3660* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m17594(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t208 *, KeyValuePair_2U5BU5D_t3660*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m17594_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Resize()
extern "C" void Dictionary_2_Resize_m17595_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m17595(__this, method) (( void (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_Resize_m17595_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m17596_gshared (Dictionary_2_t208 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m17596(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t208 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m17596_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Clear()
extern "C" void Dictionary_2_Clear_m17597_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m17597(__this, method) (( void (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_Clear_m17597_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m17598_gshared (Dictionary_2_t208 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m17598(__this, ___key, method) (( bool (*) (Dictionary_2_t208 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m17598_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17599_gshared (Dictionary_2_t208 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m17599(__this, ___value, method) (( bool (*) (Dictionary_2_t208 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m17599_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m17600_gshared (Dictionary_2_t208 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m17600(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t208 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m17600_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m17601_gshared (Dictionary_2_t208 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m17601(__this, ___sender, method) (( void (*) (Dictionary_2_t208 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m17601_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m17602_gshared (Dictionary_2_t208 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m17602(__this, ___key, method) (( bool (*) (Dictionary_2_t208 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m17602_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m17603_gshared (Dictionary_2_t208 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m17603(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t208 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m17603_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::get_Keys()
extern "C" KeyCollection_t2713 * Dictionary_2_get_Keys_m17604_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m17604(__this, method) (( KeyCollection_t2713 * (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_get_Keys_m17604_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::get_Values()
extern "C" ValueCollection_t2716 * Dictionary_2_get_Values_m17605_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m17605(__this, method) (( ValueCollection_t2716 * (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_get_Values_m17605_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m17606_gshared (Dictionary_2_t208 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m17606(__this, ___key, method) (( int32_t (*) (Dictionary_2_t208 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m17606_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m17607_gshared (Dictionary_2_t208 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m17607(__this, ___value, method) (( int32_t (*) (Dictionary_2_t208 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m17607_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17608_gshared (Dictionary_2_t208 * __this, KeyValuePair_2_t352  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m17608(__this, ___pair, method) (( bool (*) (Dictionary_2_t208 *, KeyValuePair_2_t352 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m17608_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::GetEnumerator()
extern "C" Enumerator_t436  Dictionary_2_GetEnumerator_m1673_gshared (Dictionary_2_t208 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1673(__this, method) (( Enumerator_t436  (*) (Dictionary_2_t208 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1673_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m17609_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m17609(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m17609_gshared)(__this /* static, unused */, ___key, ___value, method)
