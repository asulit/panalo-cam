﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Predicate`1<Vuforia.SmartTerrainTrackable>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m26178(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3326 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m14803_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.SmartTerrainTrackable>::Invoke(T)
#define Predicate_1_Invoke_m26179(__this, ___obj, method) (( bool (*) (Predicate_1_t3326 *, Object_t *, const MethodInfo*))Predicate_1_Invoke_m14804_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.SmartTerrainTrackable>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m26180(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3326 *, Object_t *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m14805_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.SmartTerrainTrackable>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m26181(__this, ___result, method) (( bool (*) (Predicate_1_t3326 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m14806_gshared)(__this, ___result, method)
