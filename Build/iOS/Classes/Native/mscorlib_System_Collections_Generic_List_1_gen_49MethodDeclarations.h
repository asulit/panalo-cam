﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"
#define List_1__ctor_m5188(__this, method) (( void (*) (List_1_t988 *, const MethodInfo*))List_1__ctor_m1429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m24423(__this, ___collection, method) (( void (*) (List_1_t988 *, Object_t*, const MethodInfo*))List_1__ctor_m14617_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Int32)
#define List_1__ctor_m24424(__this, ___capacity, method) (( void (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1__ctor_m14619_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.cctor()
#define List_1__cctor_m24425(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14621_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24426(__this, method) (( Object_t* (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14623_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m24427(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t988 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m14625_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m24428(__this, method) (( Object_t * (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m14627_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m24429(__this, ___item, method) (( int32_t (*) (List_1_t988 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m14629_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m24430(__this, ___item, method) (( bool (*) (List_1_t988 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m14631_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m24431(__this, ___item, method) (( int32_t (*) (List_1_t988 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m14633_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m24432(__this, ___index, ___item, method) (( void (*) (List_1_t988 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m14635_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m24433(__this, ___item, method) (( void (*) (List_1_t988 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m14637_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24434(__this, method) (( bool (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14639_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m24435(__this, method) (( bool (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m14641_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m24436(__this, method) (( Object_t * (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m14643_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m24437(__this, method) (( bool (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m14645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m24438(__this, method) (( bool (*) (List_1_t988 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m14647_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m24439(__this, ___index, method) (( Object_t * (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m14649_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m24440(__this, ___index, ___value, method) (( void (*) (List_1_t988 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m14651_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(T)
#define List_1_Add_m24441(__this, ___item, method) (( void (*) (List_1_t988 *, BaseInvokableCall_t981 *, const MethodInfo*))List_1_Add_m14653_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m24442(__this, ___newCount, method) (( void (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m14655_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m24443(__this, ___idx, ___count, method) (( void (*) (List_1_t988 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m14657_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m24444(__this, ___collection, method) (( void (*) (List_1_t988 *, Object_t*, const MethodInfo*))List_1_AddCollection_m14659_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m24445(__this, ___enumerable, method) (( void (*) (List_1_t988 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m14661_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m5191(__this, ___collection, method) (( void (*) (List_1_t988 *, Object_t*, const MethodInfo*))List_1_AddRange_m14663_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AsReadOnly()
#define List_1_AsReadOnly_m24446(__this, method) (( ReadOnlyCollection_1_t3215 * (*) (List_1_t988 *, const MethodInfo*))List_1_AsReadOnly_m14665_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear()
#define List_1_Clear_m24447(__this, method) (( void (*) (List_1_t988 *, const MethodInfo*))List_1_Clear_m14667_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Contains(T)
#define List_1_Contains_m24448(__this, ___item, method) (( bool (*) (List_1_t988 *, BaseInvokableCall_t981 *, const MethodInfo*))List_1_Contains_m14669_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m24449(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t988 *, BaseInvokableCallU5BU5D_t3214*, int32_t, const MethodInfo*))List_1_CopyTo_m14671_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Find(System.Predicate`1<T>)
#define List_1_Find_m24450(__this, ___match, method) (( BaseInvokableCall_t981 * (*) (List_1_t988 *, Predicate_1_t1045 *, const MethodInfo*))List_1_Find_m14673_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m24451(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1045 *, const MethodInfo*))List_1_CheckMatch_m14675_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m24452(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t988 *, int32_t, int32_t, Predicate_1_t1045 *, const MethodInfo*))List_1_GetIndex_m14677_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GetEnumerator()
#define List_1_GetEnumerator_m24453(__this, method) (( Enumerator_t3217  (*) (List_1_t988 *, const MethodInfo*))List_1_GetEnumerator_m14679_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::IndexOf(T)
#define List_1_IndexOf_m24454(__this, ___item, method) (( int32_t (*) (List_1_t988 *, BaseInvokableCall_t981 *, const MethodInfo*))List_1_IndexOf_m14681_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m24455(__this, ___start, ___delta, method) (( void (*) (List_1_t988 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m14683_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m24456(__this, ___index, method) (( void (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_CheckIndex_m14685_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Insert(System.Int32,T)
#define List_1_Insert_m24457(__this, ___index, ___item, method) (( void (*) (List_1_t988 *, int32_t, BaseInvokableCall_t981 *, const MethodInfo*))List_1_Insert_m14687_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m24458(__this, ___collection, method) (( void (*) (List_1_t988 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m14689_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Remove(T)
#define List_1_Remove_m24459(__this, ___item, method) (( bool (*) (List_1_t988 *, BaseInvokableCall_t981 *, const MethodInfo*))List_1_Remove_m14691_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m5190(__this, ___match, method) (( int32_t (*) (List_1_t988 *, Predicate_1_t1045 *, const MethodInfo*))List_1_RemoveAll_m14693_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m24460(__this, ___index, method) (( void (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_RemoveAt_m14695_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m24461(__this, ___index, ___count, method) (( void (*) (List_1_t988 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m14697_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Reverse()
#define List_1_Reverse_m24462(__this, method) (( void (*) (List_1_t988 *, const MethodInfo*))List_1_Reverse_m14699_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Sort()
#define List_1_Sort_m24463(__this, method) (( void (*) (List_1_t988 *, const MethodInfo*))List_1_Sort_m14701_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m24464(__this, ___comparison, method) (( void (*) (List_1_t988 *, Comparison_1_t3218 *, const MethodInfo*))List_1_Sort_m14703_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::ToArray()
#define List_1_ToArray_m24465(__this, method) (( BaseInvokableCallU5BU5D_t3214* (*) (List_1_t988 *, const MethodInfo*))List_1_ToArray_m14705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::TrimExcess()
#define List_1_TrimExcess_m24466(__this, method) (( void (*) (List_1_t988 *, const MethodInfo*))List_1_TrimExcess_m14707_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Capacity()
#define List_1_get_Capacity_m24467(__this, method) (( int32_t (*) (List_1_t988 *, const MethodInfo*))List_1_get_Capacity_m14709_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m24468(__this, ___value, method) (( void (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_set_Capacity_m14711_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count()
#define List_1_get_Count_m24469(__this, method) (( int32_t (*) (List_1_t988 *, const MethodInfo*))List_1_get_Count_m14713_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32)
#define List_1_get_Item_m24470(__this, ___index, method) (( BaseInvokableCall_t981 * (*) (List_1_t988 *, int32_t, const MethodInfo*))List_1_get_Item_m14715_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::set_Item(System.Int32,T)
#define List_1_set_Item_m24471(__this, ___index, ___value, method) (( void (*) (List_1_t988 *, int32_t, BaseInvokableCall_t981 *, const MethodInfo*))List_1_set_Item_m14717_gshared)(__this, ___index, ___value, method)
