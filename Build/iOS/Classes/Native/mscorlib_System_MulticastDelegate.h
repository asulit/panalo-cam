﻿#pragma once
#include <stdint.h>
// System.MulticastDelegate
struct MulticastDelegate_t28;
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.MulticastDelegate
struct  MulticastDelegate_t28  : public Delegate_t466
{
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t28 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t28 * ___kpm_next_10;
};
