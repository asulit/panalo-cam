﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.String
struct String_t;
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"

// System.Void Screens::.cctor()
extern "C" void Screens__cctor_m666 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Screens::GetScreen(EScreens)
extern "C" String_t* Screens_GetScreen_m667 (Object_t * __this /* static, unused */, int32_t ___screen, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Screens::GetSubScreen(ESubScreens)
extern "C" String_t* Screens_GetSubScreen_m668 (Object_t * __this /* static, unused */, int32_t ___screen, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screens::Load(EScreens)
extern "C" void Screens_Load_m669 (Object_t * __this /* static, unused */, int32_t ___screen, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Screens::LoadAdditive(ESubScreens)
extern "C" void Screens_LoadAdditive_m670 (Object_t * __this /* static, unused */, int32_t ___screen, const MethodInfo* method) IL2CPP_METHOD_ATTR;
