﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t638;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct  Comparison_1_t3005  : public MulticastDelegate_t28
{
};
