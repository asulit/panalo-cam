﻿#pragma once
#include <stdint.h>
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t926;
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t927;
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t928;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.AttributeHelperEngine
struct  AttributeHelperEngine_t925  : public Object_t
{
};
struct AttributeHelperEngine_t925_StaticFields{
	// UnityEngine.DisallowMultipleComponent[] UnityEngine.AttributeHelperEngine::_disallowMultipleComponentArray
	DisallowMultipleComponentU5BU5D_t926* ____disallowMultipleComponentArray_0;
	// UnityEngine.ExecuteInEditMode[] UnityEngine.AttributeHelperEngine::_executeInEditModeArray
	ExecuteInEditModeU5BU5D_t927* ____executeInEditModeArray_1;
	// UnityEngine.RequireComponent[] UnityEngine.AttributeHelperEngine::_requireComponentArray
	RequireComponentU5BU5D_t928* ____requireComponentArray_2;
};
