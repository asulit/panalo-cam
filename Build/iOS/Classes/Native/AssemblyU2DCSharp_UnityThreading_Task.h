﻿#pragma once
#include <stdint.h>
// System.Action
struct Action_t16;
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBase.h"
// UnityThreading.Task
struct  Task_t165  : public TaskBase_t163
{
	// System.Action UnityThreading.Task::action
	Action_t16 * ___action_4;
};
