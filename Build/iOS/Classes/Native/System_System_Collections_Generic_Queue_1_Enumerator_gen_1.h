﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>
struct Queue_1_t77;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.Queue`1/Enumerator<System.Collections.Generic.Queue`1<Common.Logger.Log>>
struct  Enumerator_t2572 
{
	// System.Collections.Generic.Queue`1<T> System.Collections.Generic.Queue`1/Enumerator<System.Collections.Generic.Queue`1<Common.Logger.Log>>::q
	Queue_1_t77 * ___q_0;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator<System.Collections.Generic.Queue`1<Common.Logger.Log>>::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator<System.Collections.Generic.Queue`1<Common.Logger.Log>>::ver
	int32_t ___ver_2;
};
