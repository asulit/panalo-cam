﻿#pragma once
#include <stdint.h>
// UnityEngine.TextAsset
struct TextAsset_t148;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// XmlTest
struct  XmlTest_t147  : public MonoBehaviour_t5
{
	// UnityEngine.TextAsset XmlTest::sampleXml
	TextAsset_t148 * ___sampleXml_2;
};
