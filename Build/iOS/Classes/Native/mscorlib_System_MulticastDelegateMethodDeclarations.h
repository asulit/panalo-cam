﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MulticastDelegate
struct MulticastDelegate_t28;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Object
struct Object_t;
// System.Delegate[]
struct DelegateU5BU5D_t2409;
// System.Delegate
struct Delegate_t466;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.MulticastDelegate::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MulticastDelegate_GetObjectData_m10107 (MulticastDelegate_t28 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::Equals(System.Object)
extern "C" bool MulticastDelegate_Equals_m10108 (MulticastDelegate_t28 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MulticastDelegate::GetHashCode()
extern "C" int32_t MulticastDelegate_GetHashCode_m10109 (MulticastDelegate_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate[] System.MulticastDelegate::GetInvocationList()
extern "C" DelegateU5BU5D_t2409* MulticastDelegate_GetInvocationList_m10110 (MulticastDelegate_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::CombineImpl(System.Delegate)
extern "C" Delegate_t466 * MulticastDelegate_CombineImpl_m10111 (MulticastDelegate_t28 * __this, Delegate_t466 * ___follow, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::BaseEquals(System.MulticastDelegate)
extern "C" bool MulticastDelegate_BaseEquals_m10112 (MulticastDelegate_t28 * __this, MulticastDelegate_t28 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MulticastDelegate System.MulticastDelegate::KPM(System.MulticastDelegate,System.MulticastDelegate,System.MulticastDelegate&)
extern "C" MulticastDelegate_t28 * MulticastDelegate_KPM_m10113 (Object_t * __this /* static, unused */, MulticastDelegate_t28 * ___needle, MulticastDelegate_t28 * ___haystack, MulticastDelegate_t28 ** ___tail, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::RemoveImpl(System.Delegate)
extern "C" Delegate_t466 * MulticastDelegate_RemoveImpl_m10114 (MulticastDelegate_t28 * __this, Delegate_t466 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
