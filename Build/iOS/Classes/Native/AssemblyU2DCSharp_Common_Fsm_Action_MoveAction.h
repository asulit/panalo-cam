﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t35;
// System.String
struct String_t;
// CountdownTimer
struct CountdownTimer_t13;
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapter.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
// Common.Fsm.Action.MoveAction
struct  MoveAction_t34  : public FsmActionAdapter_t33
{
	// UnityEngine.Transform Common.Fsm.Action.MoveAction::transform
	Transform_t35 * ___transform_1;
	// UnityEngine.Vector3 Common.Fsm.Action.MoveAction::positionFrom
	Vector3_t36  ___positionFrom_2;
	// UnityEngine.Vector3 Common.Fsm.Action.MoveAction::positionTo
	Vector3_t36  ___positionTo_3;
	// System.Single Common.Fsm.Action.MoveAction::duration
	float ___duration_4;
	// System.String Common.Fsm.Action.MoveAction::timeReference
	String_t* ___timeReference_5;
	// System.String Common.Fsm.Action.MoveAction::finishEvent
	String_t* ___finishEvent_6;
	// UnityEngine.Space Common.Fsm.Action.MoveAction::space
	int32_t ___space_7;
	// CountdownTimer Common.Fsm.Action.MoveAction::timer
	CountdownTimer_t13 * ___timer_8;
};
