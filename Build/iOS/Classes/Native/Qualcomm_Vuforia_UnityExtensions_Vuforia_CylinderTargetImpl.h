﻿#pragma once
#include <stdint.h>
// Vuforia.ObjectTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetImpl.h"
// Vuforia.CylinderTargetImpl
struct  CylinderTargetImpl_t1097  : public ObjectTargetImpl_t1072
{
	// System.Single Vuforia.CylinderTargetImpl::mSideLength
	float ___mSideLength_4;
	// System.Single Vuforia.CylinderTargetImpl::mTopDiameter
	float ___mTopDiameter_5;
	// System.Single Vuforia.CylinderTargetImpl::mBottomDiameter
	float ___mBottomDiameter_6;
};
