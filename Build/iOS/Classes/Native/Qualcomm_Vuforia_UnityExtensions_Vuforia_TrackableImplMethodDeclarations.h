﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackableImpl
struct TrackableImpl_t1071;
// System.String
struct String_t;

// System.Void Vuforia.TrackableImpl::.ctor(System.String,System.Int32)
extern "C" void TrackableImpl__ctor_m5414 (TrackableImpl_t1071 * __this, String_t* ___name, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.TrackableImpl::get_Name()
extern "C" String_t* TrackableImpl_get_Name_m5415 (TrackableImpl_t1071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableImpl::set_Name(System.String)
extern "C" void TrackableImpl_set_Name_m5416 (TrackableImpl_t1071 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.TrackableImpl::get_ID()
extern "C" int32_t TrackableImpl_get_ID_m5417 (TrackableImpl_t1071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableImpl::set_ID(System.Int32)
extern "C" void TrackableImpl_set_ID_m5418 (TrackableImpl_t1071 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
