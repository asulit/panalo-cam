﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_54MethodDeclarations.h"
#define Enumerator__ctor_m27082(__this, ___host, method) (( void (*) (Enumerator_t1327 *, Dictionary_2_t1185 *, const MethodInfo*))Enumerator__ctor_m19866_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27083(__this, method) (( Object_t * (*) (Enumerator_t1327 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19867_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m7330(__this, method) (( void (*) (Enumerator_t1327 *, const MethodInfo*))Enumerator_Dispose_m19868_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m7329(__this, method) (( bool (*) (Enumerator_t1327 *, const MethodInfo*))Enumerator_MoveNext_m19869_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m7328(__this, method) (( WordAbstractBehaviour_t327 * (*) (Enumerator_t1327 *, const MethodInfo*))Enumerator_get_Current_m19870_gshared)(__this, method)
