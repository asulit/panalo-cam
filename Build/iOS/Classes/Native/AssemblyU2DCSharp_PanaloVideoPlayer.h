﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// PanaloVideoPlayer
struct  PanaloVideoPlayer_t242  : public Object_t
{
	// System.String PanaloVideoPlayer::EXT
	String_t* ___EXT_0;
	// System.String PanaloVideoPlayer::movieTexturePath
	String_t* ___movieTexturePath_1;
	// System.Boolean PanaloVideoPlayer::isPlaying
	bool ___isPlaying_2;
};
