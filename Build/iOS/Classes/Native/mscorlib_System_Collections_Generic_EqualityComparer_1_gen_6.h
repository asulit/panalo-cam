﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<AnimatorFrame>
struct EqualityComparer_1_t2775;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<AnimatorFrame>
struct  EqualityComparer_1_t2775  : public Object_t
{
};
struct EqualityComparer_1_t2775_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<AnimatorFrame>::_default
	EqualityComparer_1_t2775 * ____default_0;
};
