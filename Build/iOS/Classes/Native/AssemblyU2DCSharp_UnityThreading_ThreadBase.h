﻿#pragma once
#include <stdint.h>
// UnityThreading.Dispatcher
struct Dispatcher_t162;
// System.Threading.Thread
struct Thread_t170;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t160;
// UnityThreading.ThreadBase
struct ThreadBase_t169;
// System.Object
#include "mscorlib_System_Object.h"
// UnityThreading.ThreadBase
struct  ThreadBase_t169  : public Object_t
{
	// UnityThreading.Dispatcher UnityThreading.ThreadBase::targetDispatcher
	Dispatcher_t162 * ___targetDispatcher_0;
	// System.Threading.Thread UnityThreading.ThreadBase::thread
	Thread_t170 * ___thread_1;
	// System.Threading.ManualResetEvent UnityThreading.ThreadBase::exitEvent
	ManualResetEvent_t160 * ___exitEvent_2;
};
struct ThreadBase_t169_ThreadStaticFields{
	// UnityThreading.ThreadBase UnityThreading.ThreadBase::currentThread
	ThreadBase_t169 * ___currentThread_3;
};
