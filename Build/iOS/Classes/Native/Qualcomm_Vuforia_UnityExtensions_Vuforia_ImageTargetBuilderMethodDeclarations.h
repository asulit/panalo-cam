﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t1101;

// System.Void Vuforia.ImageTargetBuilder::.ctor()
extern "C" void ImageTargetBuilder__ctor_m5593 (ImageTargetBuilder_t1101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
