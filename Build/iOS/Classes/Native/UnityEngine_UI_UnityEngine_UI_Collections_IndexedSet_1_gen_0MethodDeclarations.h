﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_2MethodDeclarations.h"
#define IndexedSet_1__ctor_m3550(__this, method) (( void (*) (IndexedSet_1_t747 *, const MethodInfo*))IndexedSet_1__ctor_m20065_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m21068(__this, method) (( Object_t * (*) (IndexedSet_1_t747 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20067_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m21069(__this, ___item, method) (( void (*) (IndexedSet_1_t747 *, Graphic_t572 *, const MethodInfo*))IndexedSet_1_Add_m20069_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m21070(__this, ___item, method) (( bool (*) (IndexedSet_1_t747 *, Graphic_t572 *, const MethodInfo*))IndexedSet_1_Remove_m20071_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m21071(__this, method) (( Object_t* (*) (IndexedSet_1_t747 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m20073_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m21072(__this, method) (( void (*) (IndexedSet_1_t747 *, const MethodInfo*))IndexedSet_1_Clear_m20075_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m21073(__this, ___item, method) (( bool (*) (IndexedSet_1_t747 *, Graphic_t572 *, const MethodInfo*))IndexedSet_1_Contains_m20077_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m21074(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t747 *, GraphicU5BU5D_t2949*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m20079_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m21075(__this, method) (( int32_t (*) (IndexedSet_1_t747 *, const MethodInfo*))IndexedSet_1_get_Count_m20081_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m21076(__this, method) (( bool (*) (IndexedSet_1_t747 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m20083_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m21077(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t747 *, Graphic_t572 *, const MethodInfo*))IndexedSet_1_IndexOf_m20085_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m21078(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t747 *, int32_t, Graphic_t572 *, const MethodInfo*))IndexedSet_1_Insert_m20087_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m21079(__this, ___index, method) (( void (*) (IndexedSet_1_t747 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m20089_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m21080(__this, ___index, method) (( Graphic_t572 * (*) (IndexedSet_1_t747 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m20091_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m21081(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t747 *, int32_t, Graphic_t572 *, const MethodInfo*))IndexedSet_1_set_Item_m20093_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m21082(__this, ___match, method) (( void (*) (IndexedSet_1_t747 *, Predicate_1_t2951 *, const MethodInfo*))IndexedSet_1_RemoveAll_m20094_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m21083(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t747 *, Comparison_1_t580 *, const MethodInfo*))IndexedSet_1_Sort_m20095_gshared)(__this, ___sortLayoutFunction, method)
