﻿#pragma once
#include <stdint.h>
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t1282;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.IUserDefinedTargetEventHandler>
struct  Predicate_1_t3518  : public MulticastDelegate_t28
{
};
