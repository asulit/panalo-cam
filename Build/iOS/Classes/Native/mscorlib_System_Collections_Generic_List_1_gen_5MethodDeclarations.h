﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t344;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t376;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3617;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2470;
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Predicate`1<System.Object>
struct Predicate_1_t2476;
// System.Comparison`1<System.Object>
struct Comparison_1_t2482;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m1429_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1__ctor_m1429(__this, method) (( void (*) (List_1_t344 *, const MethodInfo*))List_1__ctor_m1429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m14617_gshared (List_1_t344 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m14617(__this, ___collection, method) (( void (*) (List_1_t344 *, Object_t*, const MethodInfo*))List_1__ctor_m14617_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m14619_gshared (List_1_t344 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m14619(__this, ___capacity, method) (( void (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1__ctor_m14619_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m14621_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m14621(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14621_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14623_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14623(__this, method) (( Object_t* (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14623_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m14625_gshared (List_1_t344 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m14625(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t344 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m14625_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m14627_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m14627(__this, method) (( Object_t * (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m14627_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m14629_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m14629(__this, ___item, method) (( int32_t (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m14629_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m14631_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m14631(__this, ___item, method) (( bool (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m14631_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m14633_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m14633(__this, ___item, method) (( int32_t (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m14633_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m14635_gshared (List_1_t344 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m14635(__this, ___index, ___item, method) (( void (*) (List_1_t344 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m14635_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m14637_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m14637(__this, ___item, method) (( void (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m14637_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14639_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14639(__this, method) (( bool (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14639_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m14641_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14641(__this, method) (( bool (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m14641_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m14643_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m14643(__this, method) (( Object_t * (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m14643_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m14645_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m14645(__this, method) (( bool (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m14645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m14647_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m14647(__this, method) (( bool (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m14647_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m14649_gshared (List_1_t344 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m14649(__this, ___index, method) (( Object_t * (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m14649_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m14651_gshared (List_1_t344 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m14651(__this, ___index, ___value, method) (( void (*) (List_1_t344 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m14651_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m14653_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m14653(__this, ___item, method) (( void (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_Add_m14653_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m14655_gshared (List_1_t344 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m14655(__this, ___newCount, method) (( void (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m14655_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m14657_gshared (List_1_t344 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m14657(__this, ___idx, ___count, method) (( void (*) (List_1_t344 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m14657_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m14659_gshared (List_1_t344 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m14659(__this, ___collection, method) (( void (*) (List_1_t344 *, Object_t*, const MethodInfo*))List_1_AddCollection_m14659_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m14661_gshared (List_1_t344 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m14661(__this, ___enumerable, method) (( void (*) (List_1_t344 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m14661_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m14663_gshared (List_1_t344 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m14663(__this, ___collection, method) (( void (*) (List_1_t344 *, Object_t*, const MethodInfo*))List_1_AddRange_m14663_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2470 * List_1_AsReadOnly_m14665_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m14665(__this, method) (( ReadOnlyCollection_1_t2470 * (*) (List_1_t344 *, const MethodInfo*))List_1_AsReadOnly_m14665_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m14667_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_Clear_m14667(__this, method) (( void (*) (List_1_t344 *, const MethodInfo*))List_1_Clear_m14667_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m14669_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m14669(__this, ___item, method) (( bool (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_Contains_m14669_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m14671_gshared (List_1_t344 * __this, ObjectU5BU5D_t356* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m14671(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t344 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))List_1_CopyTo_m14671_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern "C" Object_t * List_1_Find_m14673_gshared (List_1_t344 * __this, Predicate_1_t2476 * ___match, const MethodInfo* method);
#define List_1_Find_m14673(__this, ___match, method) (( Object_t * (*) (List_1_t344 *, Predicate_1_t2476 *, const MethodInfo*))List_1_Find_m14673_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m14675_gshared (Object_t * __this /* static, unused */, Predicate_1_t2476 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m14675(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2476 *, const MethodInfo*))List_1_CheckMatch_m14675_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m14677_gshared (List_1_t344 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2476 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m14677(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t344 *, int32_t, int32_t, Predicate_1_t2476 *, const MethodInfo*))List_1_GetIndex_m14677_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2469  List_1_GetEnumerator_m14679_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m14679(__this, method) (( Enumerator_t2469  (*) (List_1_t344 *, const MethodInfo*))List_1_GetEnumerator_m14679_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m14681_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m14681(__this, ___item, method) (( int32_t (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_IndexOf_m14681_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m14683_gshared (List_1_t344 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m14683(__this, ___start, ___delta, method) (( void (*) (List_1_t344 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m14683_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m14685_gshared (List_1_t344 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m14685(__this, ___index, method) (( void (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_CheckIndex_m14685_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m14687_gshared (List_1_t344 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m14687(__this, ___index, ___item, method) (( void (*) (List_1_t344 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m14687_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m14689_gshared (List_1_t344 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m14689(__this, ___collection, method) (( void (*) (List_1_t344 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m14689_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m14691_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m14691(__this, ___item, method) (( bool (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_Remove_m14691_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m14693_gshared (List_1_t344 * __this, Predicate_1_t2476 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m14693(__this, ___match, method) (( int32_t (*) (List_1_t344 *, Predicate_1_t2476 *, const MethodInfo*))List_1_RemoveAll_m14693_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m14695_gshared (List_1_t344 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m14695(__this, ___index, method) (( void (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_RemoveAt_m14695_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m14697_gshared (List_1_t344 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m14697(__this, ___index, ___count, method) (( void (*) (List_1_t344 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m14697_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C" void List_1_Reverse_m14699_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_Reverse_m14699(__this, method) (( void (*) (List_1_t344 *, const MethodInfo*))List_1_Reverse_m14699_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C" void List_1_Sort_m14701_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_Sort_m14701(__this, method) (( void (*) (List_1_t344 *, const MethodInfo*))List_1_Sort_m14701_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m14703_gshared (List_1_t344 * __this, Comparison_1_t2482 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m14703(__this, ___comparison, method) (( void (*) (List_1_t344 *, Comparison_1_t2482 *, const MethodInfo*))List_1_Sort_m14703_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t356* List_1_ToArray_m14705_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_ToArray_m14705(__this, method) (( ObjectU5BU5D_t356* (*) (List_1_t344 *, const MethodInfo*))List_1_ToArray_m14705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C" void List_1_TrimExcess_m14707_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m14707(__this, method) (( void (*) (List_1_t344 *, const MethodInfo*))List_1_TrimExcess_m14707_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m14709_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m14709(__this, method) (( int32_t (*) (List_1_t344 *, const MethodInfo*))List_1_get_Capacity_m14709_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m14711_gshared (List_1_t344 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m14711(__this, ___value, method) (( void (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_set_Capacity_m14711_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m14713_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_get_Count_m14713(__this, method) (( int32_t (*) (List_1_t344 *, const MethodInfo*))List_1_get_Count_m14713_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m14715_gshared (List_1_t344 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m14715(__this, ___index, method) (( Object_t * (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_get_Item_m14715_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m14717_gshared (List_1_t344 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m14717(__this, ___index, ___value, method) (( void (*) (List_1_t344 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m14717_gshared)(__this, ___index, ___value, method)
