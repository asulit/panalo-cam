﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MultiTargetImpl
struct MultiTargetImpl_t1124;
// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t1088;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.MultiTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
extern "C" void MultiTargetImpl__ctor_m5713 (MultiTargetImpl_t1124 * __this, String_t* ___name, int32_t ___id, DataSet_t1088 * ___dataSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.MultiTargetImpl::GetSize()
extern "C" Vector3_t36  MultiTargetImpl_GetSize_m5714 (MultiTargetImpl_t1124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetImpl::SetSize(UnityEngine.Vector3)
extern "C" void MultiTargetImpl_SetSize_m5715 (MultiTargetImpl_t1124 * __this, Vector3_t36  ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
