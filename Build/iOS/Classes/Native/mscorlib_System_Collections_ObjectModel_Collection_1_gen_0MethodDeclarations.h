﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>
struct Collection_1_t2728;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// Vuforia.CameraDevice/FocusMode[]
struct FocusModeU5BU5D_t2723;
// System.Collections.Generic.IEnumerator`1<Vuforia.CameraDevice/FocusMode>
struct IEnumerator_1_t3664;
// System.Collections.Generic.IList`1<Vuforia.CameraDevice/FocusMode>
struct IList_1_t2727;
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::.ctor()
extern "C" void Collection_1__ctor_m17794_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1__ctor_m17794(__this, method) (( void (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1__ctor_m17794_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17795_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17795(__this, method) (( bool (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17795_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17796_gshared (Collection_1_t2728 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m17796(__this, ___array, ___index, method) (( void (*) (Collection_1_t2728 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m17796_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m17797_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m17797(__this, method) (( Object_t * (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m17797_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m17798_gshared (Collection_1_t2728 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m17798(__this, ___value, method) (( int32_t (*) (Collection_1_t2728 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m17798_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m17799_gshared (Collection_1_t2728 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m17799(__this, ___value, method) (( bool (*) (Collection_1_t2728 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m17799_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m17800_gshared (Collection_1_t2728 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m17800(__this, ___value, method) (( int32_t (*) (Collection_1_t2728 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m17800_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m17801_gshared (Collection_1_t2728 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m17801(__this, ___index, ___value, method) (( void (*) (Collection_1_t2728 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m17801_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m17802_gshared (Collection_1_t2728 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m17802(__this, ___value, method) (( void (*) (Collection_1_t2728 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m17802_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m17803_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m17803(__this, method) (( bool (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m17803_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m17804_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m17804(__this, method) (( Object_t * (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m17804_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m17805_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m17805(__this, method) (( bool (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m17805_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m17806_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m17806(__this, method) (( bool (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m17806_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m17807_gshared (Collection_1_t2728 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m17807(__this, ___index, method) (( Object_t * (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m17807_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m17808_gshared (Collection_1_t2728 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m17808(__this, ___index, ___value, method) (( void (*) (Collection_1_t2728 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m17808_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::Add(T)
extern "C" void Collection_1_Add_m17809_gshared (Collection_1_t2728 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m17809(__this, ___item, method) (( void (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_Add_m17809_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::Clear()
extern "C" void Collection_1_Clear_m17810_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_Clear_m17810(__this, method) (( void (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_Clear_m17810_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::ClearItems()
extern "C" void Collection_1_ClearItems_m17811_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m17811(__this, method) (( void (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_ClearItems_m17811_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::Contains(T)
extern "C" bool Collection_1_Contains_m17812_gshared (Collection_1_t2728 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m17812(__this, ___item, method) (( bool (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_Contains_m17812_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m17813_gshared (Collection_1_t2728 * __this, FocusModeU5BU5D_t2723* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m17813(__this, ___array, ___index, method) (( void (*) (Collection_1_t2728 *, FocusModeU5BU5D_t2723*, int32_t, const MethodInfo*))Collection_1_CopyTo_m17813_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m17814_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m17814(__this, method) (( Object_t* (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_GetEnumerator_m17814_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m17815_gshared (Collection_1_t2728 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m17815(__this, ___item, method) (( int32_t (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m17815_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m17816_gshared (Collection_1_t2728 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m17816(__this, ___index, ___item, method) (( void (*) (Collection_1_t2728 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m17816_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m17817_gshared (Collection_1_t2728 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m17817(__this, ___index, ___item, method) (( void (*) (Collection_1_t2728 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m17817_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::Remove(T)
extern "C" bool Collection_1_Remove_m17818_gshared (Collection_1_t2728 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m17818(__this, ___item, method) (( bool (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_Remove_m17818_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m17819_gshared (Collection_1_t2728 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m17819(__this, ___index, method) (( void (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m17819_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m17820_gshared (Collection_1_t2728 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m17820(__this, ___index, method) (( void (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m17820_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::get_Count()
extern "C" int32_t Collection_1_get_Count_m17821_gshared (Collection_1_t2728 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m17821(__this, method) (( int32_t (*) (Collection_1_t2728 *, const MethodInfo*))Collection_1_get_Count_m17821_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m17822_gshared (Collection_1_t2728 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m17822(__this, ___index, method) (( int32_t (*) (Collection_1_t2728 *, int32_t, const MethodInfo*))Collection_1_get_Item_m17822_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m17823_gshared (Collection_1_t2728 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m17823(__this, ___index, ___value, method) (( void (*) (Collection_1_t2728 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m17823_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m17824_gshared (Collection_1_t2728 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m17824(__this, ___index, ___item, method) (( void (*) (Collection_1_t2728 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m17824_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m17825_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m17825(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m17825_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m17826_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m17826(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m17826_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m17827_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m17827(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m17827_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m17828_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m17828(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m17828_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.CameraDevice/FocusMode>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m17829_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m17829(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m17829_gshared)(__this /* static, unused */, ___list, method)
