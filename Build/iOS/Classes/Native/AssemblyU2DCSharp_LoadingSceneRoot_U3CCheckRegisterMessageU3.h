﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
struct Object_t;
// LoadingSceneRoot
struct LoadingSceneRoot_t218;
// Handle
struct Handle_t221;
// System.Object
#include "mscorlib_System_Object.h"
// LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8
struct  U3CCheckRegisterMessageU3Ec__Iterator8_t220  : public Object_t
{
	// System.String LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::<message>__0
	String_t* ___U3CmessageU3E__0_0;
	// System.Int32 LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::$PC
	int32_t ___U24PC_1;
	// System.Object LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::$current
	Object_t * ___U24current_2;
	// LoadingSceneRoot LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::<>f__this
	LoadingSceneRoot_t218 * ___U3CU3Ef__this_3;
};
struct U3CCheckRegisterMessageU3Ec__Iterator8_t220_StaticFields{
	// Handle LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::<>f__am$cache4
	Handle_t221 * ___U3CU3Ef__amU24cache4_4;
};
