﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>
struct  KeyValuePair_2_t2667 
{
	// TKey System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::value
	Object_t * ___value_1;
};
