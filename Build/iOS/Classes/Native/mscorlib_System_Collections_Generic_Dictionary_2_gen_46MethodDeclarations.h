﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2869;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2710;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.ICollection
struct ICollection_t1751;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t3687;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t3688;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1606;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t2873;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
struct ValueCollection_t2877;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m19715_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m19715(__this, method) (( void (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2__ctor_m19715_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m19717_gshared (Dictionary_2_t2869 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m19717(__this, ___comparer, method) (( void (*) (Dictionary_2_t2869 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m19717_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m19719_gshared (Dictionary_2_t2869 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m19719(__this, ___capacity, method) (( void (*) (Dictionary_2_t2869 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m19719_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m19721_gshared (Dictionary_2_t2869 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m19721(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2869 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m19721_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m19723_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m19723(__this, method) (( Object_t * (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m19723_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m19725_gshared (Dictionary_2_t2869 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m19725(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2869 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m19725_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m19727_gshared (Dictionary_2_t2869 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m19727(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2869 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m19727_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m19729_gshared (Dictionary_2_t2869 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m19729(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2869 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m19729_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m19731_gshared (Dictionary_2_t2869 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m19731(__this, ___key, method) (( bool (*) (Dictionary_2_t2869 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m19731_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m19733_gshared (Dictionary_2_t2869 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m19733(__this, ___key, method) (( void (*) (Dictionary_2_t2869 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m19733_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19735_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19735(__this, method) (( bool (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19735_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19737_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19737(__this, method) (( Object_t * (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19737_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19739_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19739(__this, method) (( bool (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19739_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19741_gshared (Dictionary_2_t2869 * __this, KeyValuePair_2_t2871  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19741(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2869 *, KeyValuePair_2_t2871 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19741_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19743_gshared (Dictionary_2_t2869 * __this, KeyValuePair_2_t2871  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19743(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2869 *, KeyValuePair_2_t2871 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19743_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19745_gshared (Dictionary_2_t2869 * __this, KeyValuePair_2U5BU5D_t3687* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19745(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2869 *, KeyValuePair_2U5BU5D_t3687*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19745_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19747_gshared (Dictionary_2_t2869 * __this, KeyValuePair_2_t2871  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19747(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2869 *, KeyValuePair_2_t2871 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19747_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m19749_gshared (Dictionary_2_t2869 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m19749(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2869 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m19749_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19751_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19751(__this, method) (( Object_t * (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19751_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19753_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19753(__this, method) (( Object_t* (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19753_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19755_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19755(__this, method) (( Object_t * (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19755_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m19757_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m19757(__this, method) (( int32_t (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_get_Count_m19757_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m19759_gshared (Dictionary_2_t2869 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m19759(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2869 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m19759_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m19761_gshared (Dictionary_2_t2869 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m19761(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2869 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m19761_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m19763_gshared (Dictionary_2_t2869 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m19763(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2869 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m19763_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m19765_gshared (Dictionary_2_t2869 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m19765(__this, ___size, method) (( void (*) (Dictionary_2_t2869 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m19765_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m19767_gshared (Dictionary_2_t2869 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m19767(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2869 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m19767_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2871  Dictionary_2_make_pair_m19769_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m19769(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2871  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m19769_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m19771_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m19771(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m19771_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m19773_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m19773(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m19773_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m19775_gshared (Dictionary_2_t2869 * __this, KeyValuePair_2U5BU5D_t3687* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m19775(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2869 *, KeyValuePair_2U5BU5D_t3687*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m19775_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m19777_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m19777(__this, method) (( void (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_Resize_m19777_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m19779_gshared (Dictionary_2_t2869 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m19779(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2869 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m19779_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m19781_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m19781(__this, method) (( void (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_Clear_m19781_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m19783_gshared (Dictionary_2_t2869 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m19783(__this, ___key, method) (( bool (*) (Dictionary_2_t2869 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m19783_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m19785_gshared (Dictionary_2_t2869 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m19785(__this, ___value, method) (( bool (*) (Dictionary_2_t2869 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m19785_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m19787_gshared (Dictionary_2_t2869 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m19787(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2869 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m19787_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m19789_gshared (Dictionary_2_t2869 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m19789(__this, ___sender, method) (( void (*) (Dictionary_2_t2869 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m19789_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m19791_gshared (Dictionary_2_t2869 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m19791(__this, ___key, method) (( bool (*) (Dictionary_2_t2869 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m19791_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m19793_gshared (Dictionary_2_t2869 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m19793(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2869 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m19793_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Keys()
extern "C" KeyCollection_t2873 * Dictionary_2_get_Keys_m19795_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m19795(__this, method) (( KeyCollection_t2873 * (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_get_Keys_m19795_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Values()
extern "C" ValueCollection_t2877 * Dictionary_2_get_Values_m19796_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m19796(__this, method) (( ValueCollection_t2877 * (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_get_Values_m19796_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m19798_gshared (Dictionary_2_t2869 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m19798(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2869 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m19798_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m19800_gshared (Dictionary_2_t2869 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m19800(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t2869 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m19800_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m19802_gshared (Dictionary_2_t2869 * __this, KeyValuePair_2_t2871  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m19802(__this, ___pair, method) (( bool (*) (Dictionary_2_t2869 *, KeyValuePair_2_t2871 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m19802_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C" Enumerator_t2875  Dictionary_2_GetEnumerator_m19803_gshared (Dictionary_2_t2869 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m19803(__this, method) (( Enumerator_t2875  (*) (Dictionary_2_t2869 *, const MethodInfo*))Dictionary_2_GetEnumerator_m19803_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m19805_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m19805(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m19805_gshared)(__this /* static, unused */, ___key, ___value, method)
