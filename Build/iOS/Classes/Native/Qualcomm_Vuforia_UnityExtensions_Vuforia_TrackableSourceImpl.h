﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableSource
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableSource.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.TrackableSourceImpl
struct  TrackableSourceImpl_t1218  : public TrackableSource_t1112
{
	// System.IntPtr Vuforia.TrackableSourceImpl::<TrackableSourcePtr>k__BackingField
	IntPtr_t ___U3CTrackableSourcePtrU3Ek__BackingField_0;
};
