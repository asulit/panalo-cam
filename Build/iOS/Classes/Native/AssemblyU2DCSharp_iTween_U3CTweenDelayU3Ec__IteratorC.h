﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// iTween
struct iTween_t258;
// System.Object
#include "mscorlib_System_Object.h"
// iTween/<TweenDelay>c__IteratorC
struct  U3CTweenDelayU3Ec__IteratorC_t257  : public Object_t
{
	// System.Int32 iTween/<TweenDelay>c__IteratorC::$PC
	int32_t ___U24PC_0;
	// System.Object iTween/<TweenDelay>c__IteratorC::$current
	Object_t * ___U24current_1;
	// iTween iTween/<TweenDelay>c__IteratorC::<>f__this
	iTween_t258 * ___U3CU3Ef__this_2;
};
