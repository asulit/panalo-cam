﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<AnimatorFrame>
struct List_1_t238;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"
// System.Collections.Generic.List`1/Enumerator<AnimatorFrame>
struct  Enumerator_t234 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<AnimatorFrame>::l
	List_1_t238 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<AnimatorFrame>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<AnimatorFrame>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<AnimatorFrame>::current
	AnimatorFrame_t235  ___current_3;
};
