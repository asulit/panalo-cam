﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2472_t2397_marshal(const U24ArrayTypeU2472_t2397& unmarshaled, U24ArrayTypeU2472_t2397_marshaled& marshaled);
extern "C" void U24ArrayTypeU2472_t2397_marshal_back(const U24ArrayTypeU2472_t2397_marshaled& marshaled, U24ArrayTypeU2472_t2397& unmarshaled);
extern "C" void U24ArrayTypeU2472_t2397_marshal_cleanup(U24ArrayTypeU2472_t2397_marshaled& marshaled);
