﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// NativeCallsIOS
struct NativeCallsIOS_t187;
// System.String
struct String_t;

// System.Void NativeCallsIOS::.ctor()
extern "C" void NativeCallsIOS__ctor_m655 (NativeCallsIOS_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeCallsIOS::SendSMS()
extern "C" void NativeCallsIOS_SendSMS_m656 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeCallsIOS::SendSMSWithParameters(System.String,System.String)
extern "C" void NativeCallsIOS_SendSMSWithParameters_m657 (Object_t * __this /* static, unused */, String_t* ___number, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeCallsIOS::Start()
extern "C" void NativeCallsIOS_Start_m658 (NativeCallsIOS_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeCallsIOS::Share(System.String,System.String)
extern "C" void NativeCallsIOS_Share_m659 (NativeCallsIOS_t187 * __this, String_t* ___header, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeCallsIOS::SendSMS(System.String,System.String)
extern "C" void NativeCallsIOS_SendSMS_m660 (NativeCallsIOS_t187 * __this, String_t* ___number, String_t* ___messages, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeCallsIOS::StartRecord()
extern "C" void NativeCallsIOS_StartRecord_m661 (NativeCallsIOS_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeCallsIOS::StopRecord()
extern "C" void NativeCallsIOS_StopRecord_m662 (NativeCallsIOS_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeCallsIOS::CancelRecord()
extern "C" void NativeCallsIOS_CancelRecord_m663 (NativeCallsIOS_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeCallsIOS::GetUserData()
extern "C" void NativeCallsIOS_GetUserData_m664 (NativeCallsIOS_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NativeCallsIOS::OnDestroy()
extern "C" void NativeCallsIOS_OnDestroy_m665 (NativeCallsIOS_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
