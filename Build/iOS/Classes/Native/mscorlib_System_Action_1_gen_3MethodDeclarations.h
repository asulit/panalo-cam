﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Action`1<UnityEngine.Font>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_10MethodDeclarations.h"
#define Action_1__ctor_m3498(__this, ___object, ___method, method) (( void (*) (Action_1_t739 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m16966_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.Font>::Invoke(T)
#define Action_1_Invoke_m5140(__this, ___obj, method) (( void (*) (Action_1_t739 *, Font_t569 *, const MethodInfo*))Action_1_Invoke_m16967_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.Font>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m20722(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t739 *, Font_t569 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m16969_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.Font>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m20723(__this, ___result, method) (( void (*) (Action_1_t739 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m16971_gshared)(__this, ___result, method)
