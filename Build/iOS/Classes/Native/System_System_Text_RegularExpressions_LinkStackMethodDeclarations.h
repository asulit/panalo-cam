﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.LinkStack
struct LinkStack_t1702;

// System.Void System.Text.RegularExpressions.LinkStack::.ctor()
extern "C" void LinkStack__ctor_m9162 (LinkStack_t1702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.LinkStack::Push()
extern "C" void LinkStack_Push_m9163 (LinkStack_t1702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.LinkStack::Pop()
extern "C" bool LinkStack_Pop_m9164 (LinkStack_t1702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
