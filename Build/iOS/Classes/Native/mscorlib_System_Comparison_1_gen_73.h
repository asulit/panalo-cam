﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t358;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Surface>
struct  Comparison_1_t3415  : public MulticastDelegate_t28
{
};
