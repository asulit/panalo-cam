﻿#pragma once
#include <stdint.h>
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t210;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t211;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ScannerHandler
struct  ScannerHandler_t209  : public MonoBehaviour_t5
{
	// Vuforia.ImageTargetBehaviour ScannerHandler::targetBehaviour
	ImageTargetBehaviour_t210 * ___targetBehaviour_2;
	// Vuforia.TrackableBehaviour ScannerHandler::trackBehaviour
	TrackableBehaviour_t211 * ___trackBehaviour_3;
	// System.Boolean ScannerHandler::isScanning
	bool ___isScanning_4;
	// System.Boolean ScannerHandler::flashFlag
	bool ___flashFlag_5;
};
