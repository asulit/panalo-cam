﻿#pragma once
#include <stdint.h>
// Vuforia.WordList
struct WordList_t1173;
// Vuforia.TextTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTracker.h"
// Vuforia.TextTrackerImpl
struct  TextTrackerImpl_t1172  : public TextTracker_t1170
{
	// Vuforia.WordList Vuforia.TextTrackerImpl::mWordList
	WordList_t1173 * ___mWordList_1;
};
