﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1371;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_70.h"
// Vuforia.VuforiaManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m28138_gshared (Enumerator_t3432 * __this, Dictionary_2_t1371 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m28138(__this, ___host, method) (( void (*) (Enumerator_t3432 *, Dictionary_2_t1371 *, const MethodInfo*))Enumerator__ctor_m28138_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m28139_gshared (Enumerator_t3432 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m28139(__this, method) (( Object_t * (*) (Enumerator_t3432 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m28139_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m28140_gshared (Enumerator_t3432 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m28140(__this, method) (( void (*) (Enumerator_t3432 *, const MethodInfo*))Enumerator_Dispose_m28140_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m28141_gshared (Enumerator_t3432 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m28141(__this, method) (( bool (*) (Enumerator_t3432 *, const MethodInfo*))Enumerator_MoveNext_m28141_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" TrackableResultData_t1134  Enumerator_get_Current_m28142_gshared (Enumerator_t3432 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m28142(__this, method) (( TrackableResultData_t1134  (*) (Enumerator_t3432 *, const MethodInfo*))Enumerator_get_Current_m28142_gshared)(__this, method)
