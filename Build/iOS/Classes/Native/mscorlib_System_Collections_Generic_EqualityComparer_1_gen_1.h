﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<EScreens>
struct EqualityComparer_1_t2679;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<EScreens>
struct  EqualityComparer_1_t2679  : public Object_t
{
};
struct EqualityComparer_1_t2679_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<EScreens>::_default
	EqualityComparer_1_t2679 * ____default_0;
};
