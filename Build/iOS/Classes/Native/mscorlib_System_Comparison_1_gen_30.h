﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas
struct Canvas_t574;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Canvas>
struct  Comparison_1_t2918  : public MulticastDelegate_t28
{
};
