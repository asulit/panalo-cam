﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void Vuforia.OrientedBoundingBox::.ctor(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C" void OrientedBoundingBox__ctor_m5547 (OrientedBoundingBox_t1091 * __this, Vector2_t2  ___center, Vector2_t2  ___halfExtents, float ___rotation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::get_Center()
extern "C" Vector2_t2  OrientedBoundingBox_get_Center_m5548 (OrientedBoundingBox_t1091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox::set_Center(UnityEngine.Vector2)
extern "C" void OrientedBoundingBox_set_Center_m5549 (OrientedBoundingBox_t1091 * __this, Vector2_t2  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::get_HalfExtents()
extern "C" Vector2_t2  OrientedBoundingBox_get_HalfExtents_m5550 (OrientedBoundingBox_t1091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox::set_HalfExtents(UnityEngine.Vector2)
extern "C" void OrientedBoundingBox_set_HalfExtents_m5551 (OrientedBoundingBox_t1091 * __this, Vector2_t2  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.OrientedBoundingBox::get_Rotation()
extern "C" float OrientedBoundingBox_get_Rotation_m5552 (OrientedBoundingBox_t1091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox::set_Rotation(System.Single)
extern "C" void OrientedBoundingBox_set_Rotation_m5553 (OrientedBoundingBox_t1091 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
