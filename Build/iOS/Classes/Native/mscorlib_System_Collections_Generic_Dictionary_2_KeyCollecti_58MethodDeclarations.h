﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1371;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_58.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m28103_gshared (Enumerator_t3428 * __this, Dictionary_2_t1371 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m28103(__this, ___host, method) (( void (*) (Enumerator_t3428 *, Dictionary_2_t1371 *, const MethodInfo*))Enumerator__ctor_m28103_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m28104_gshared (Enumerator_t3428 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m28104(__this, method) (( Object_t * (*) (Enumerator_t3428 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m28104_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m28105_gshared (Enumerator_t3428 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m28105(__this, method) (( void (*) (Enumerator_t3428 *, const MethodInfo*))Enumerator_Dispose_m28105_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m28106_gshared (Enumerator_t3428 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m28106(__this, method) (( bool (*) (Enumerator_t3428 *, const MethodInfo*))Enumerator_MoveNext_m28106_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" int32_t Enumerator_get_Current_m28107_gshared (Enumerator_t3428 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m28107(__this, method) (( int32_t (*) (Enumerator_t3428 *, const MethodInfo*))Enumerator_get_Current_m28107_gshared)(__this, method)
