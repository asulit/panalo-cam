﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t356;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct  UnityEvent_3_t3220  : public UnityEventBase_t989
{
	// System.Object[] UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::m_InvokeArray
	ObjectU5BU5D_t356* ___m_InvokeArray_4;
};
