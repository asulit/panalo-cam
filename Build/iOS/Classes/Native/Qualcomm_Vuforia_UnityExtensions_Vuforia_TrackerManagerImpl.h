﻿#pragma once
#include <stdint.h>
// Vuforia.ObjectTracker
struct ObjectTracker_t1066;
// Vuforia.MarkerTracker
struct MarkerTracker_t1121;
// Vuforia.TextTracker
struct TextTracker_t1170;
// Vuforia.SmartTerrainTracker
struct SmartTerrainTracker_t1168;
// Vuforia.StateManager
struct StateManager_t1205;
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManager.h"
// Vuforia.TrackerManagerImpl
struct  TrackerManagerImpl_t1221  : public TrackerManager_t1220
{
	// Vuforia.ObjectTracker Vuforia.TrackerManagerImpl::mObjectTracker
	ObjectTracker_t1066 * ___mObjectTracker_1;
	// Vuforia.MarkerTracker Vuforia.TrackerManagerImpl::mMarkerTracker
	MarkerTracker_t1121 * ___mMarkerTracker_2;
	// Vuforia.TextTracker Vuforia.TrackerManagerImpl::mTextTracker
	TextTracker_t1170 * ___mTextTracker_3;
	// Vuforia.SmartTerrainTracker Vuforia.TrackerManagerImpl::mSmartTerrainTracker
	SmartTerrainTracker_t1168 * ___mSmartTerrainTracker_4;
	// Vuforia.StateManager Vuforia.TrackerManagerImpl::mStateManager
	StateManager_t1205 * ___mStateManager_5;
};
