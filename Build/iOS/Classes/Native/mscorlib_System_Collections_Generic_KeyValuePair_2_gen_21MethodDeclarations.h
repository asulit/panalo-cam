﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m18243(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2766 *, InputField_t226 *, Image_t213 *, const MethodInfo*))KeyValuePair_2__ctor_m15105_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::get_Key()
#define KeyValuePair_2_get_Key_m18244(__this, method) (( InputField_t226 * (*) (KeyValuePair_2_t2766 *, const MethodInfo*))KeyValuePair_2_get_Key_m15106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18245(__this, ___value, method) (( void (*) (KeyValuePair_2_t2766 *, InputField_t226 *, const MethodInfo*))KeyValuePair_2_set_Key_m15107_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::get_Value()
#define KeyValuePair_2_get_Value_m18246(__this, method) (( Image_t213 * (*) (KeyValuePair_2_t2766 *, const MethodInfo*))KeyValuePair_2_get_Value_m15108_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18247(__this, ___value, method) (( void (*) (KeyValuePair_2_t2766 *, Image_t213 *, const MethodInfo*))KeyValuePair_2_set_Value_m15109_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::ToString()
#define KeyValuePair_2_ToString_m18248(__this, method) (( String_t* (*) (KeyValuePair_2_t2766 *, const MethodInfo*))KeyValuePair_2_ToString_m15110_gshared)(__this, method)
