﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
// System.Collections.Generic.KeyValuePair`2<EScreens,System.String>
struct  KeyValuePair_2_t2681 
{
	// TKey System.Collections.Generic.KeyValuePair`2<EScreens,System.String>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<EScreens,System.String>::value
	String_t* ___value_1;
};
