﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU24128_t2404_marshal(const U24ArrayTypeU24128_t2404& unmarshaled, U24ArrayTypeU24128_t2404_marshaled& marshaled);
extern "C" void U24ArrayTypeU24128_t2404_marshal_back(const U24ArrayTypeU24128_t2404_marshaled& marshaled, U24ArrayTypeU24128_t2404& unmarshaled);
extern "C" void U24ArrayTypeU24128_t2404_marshal_cleanup(U24ArrayTypeU24128_t2404_marshaled& marshaled);
