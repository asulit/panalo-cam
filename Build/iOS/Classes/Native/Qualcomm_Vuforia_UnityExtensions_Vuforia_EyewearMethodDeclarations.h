﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.Eyewear
struct Eyewear_t1081;
// Vuforia.EyewearCalibrationProfileManager
struct EyewearCalibrationProfileManager_t1047;
// Vuforia.EyewearUserCalibrator
struct EyewearUserCalibrator_t1050;

// Vuforia.Eyewear Vuforia.Eyewear::get_Instance()
extern "C" Eyewear_t1081 * Eyewear_get_Instance_m5466 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.EyewearCalibrationProfileManager Vuforia.Eyewear::getProfileManager()
extern "C" EyewearCalibrationProfileManager_t1047 * Eyewear_getProfileManager_m5467 (Eyewear_t1081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.EyewearUserCalibrator Vuforia.Eyewear::getCalibrator()
extern "C" EyewearUserCalibrator_t1050 * Eyewear_getCalibrator_m5468 (Eyewear_t1081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Eyewear::.ctor()
extern "C" void Eyewear__ctor_m5469 (Eyewear_t1081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Eyewear::.cctor()
extern "C" void Eyewear__cctor_m5470 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
