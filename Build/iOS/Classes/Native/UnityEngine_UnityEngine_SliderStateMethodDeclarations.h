﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SliderState
struct SliderState_t971;

// System.Void UnityEngine.SliderState::.ctor()
extern "C" void SliderState__ctor_m5012 (SliderState_t971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
