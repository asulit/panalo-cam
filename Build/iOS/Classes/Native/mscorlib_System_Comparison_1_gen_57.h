﻿#pragma once
#include <stdint.h>
// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t284;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct  Comparison_1_t3244  : public MulticastDelegate_t28
{
};
