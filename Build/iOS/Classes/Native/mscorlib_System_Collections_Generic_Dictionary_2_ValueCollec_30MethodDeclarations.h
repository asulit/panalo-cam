﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28MethodDeclarations.h"
#define ValueCollection__ctor_m15218(__this, ___dictionary, method) (( void (*) (ValueCollection_t2522 *, Dictionary_2_t48 *, const MethodInfo*))ValueCollection__ctor_m15156_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15219(__this, ___item, method) (( void (*) (ValueCollection_t2522 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15157_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15220(__this, method) (( void (*) (ValueCollection_t2522 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15158_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15221(__this, ___item, method) (( bool (*) (ValueCollection_t2522 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15159_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15222(__this, ___item, method) (( bool (*) (ValueCollection_t2522 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15160_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15223(__this, method) (( Object_t* (*) (ValueCollection_t2522 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15161_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m15224(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2522 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m15162_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15225(__this, method) (( Object_t * (*) (ValueCollection_t2522 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15163_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15226(__this, method) (( bool (*) (ValueCollection_t2522 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15164_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15227(__this, method) (( bool (*) (ValueCollection_t2522 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15165_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m15228(__this, method) (( Object_t * (*) (ValueCollection_t2522 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m15166_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m15229(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2522 *, FsmStateU5BU5D_t2501*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m15167_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::GetEnumerator()
#define ValueCollection_GetEnumerator_m15230(__this, method) (( Enumerator_t3622  (*) (ValueCollection_t2522 *, const MethodInfo*))ValueCollection_GetEnumerator_m15168_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::get_Count()
#define ValueCollection_get_Count_m15231(__this, method) (( int32_t (*) (ValueCollection_t2522 *, const MethodInfo*))ValueCollection_get_Count_m15169_gshared)(__this, method)
