﻿#pragma once
#include <stdint.h>
// InputLayer[]
struct InputLayerU5BU5D_t62;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<InputLayer>
struct  List_1_t63  : public Object_t
{
	// T[] System.Collections.Generic.List`1<InputLayer>::_items
	InputLayerU5BU5D_t62* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<InputLayer>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<InputLayer>::_version
	int32_t ____version_3;
};
struct List_1_t63_StaticFields{
	// T[] System.Collections.Generic.List`1<InputLayer>::EmptyArray
	InputLayerU5BU5D_t62* ___EmptyArray_4;
};
