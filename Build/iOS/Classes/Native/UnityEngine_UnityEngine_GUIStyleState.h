﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t342;
// UnityEngine.Texture2D
struct Texture2D_t355;
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.GUIStyleState
struct  GUIStyleState_t917  : public Object_t
{
	// System.IntPtr UnityEngine.GUIStyleState::m_Ptr
	IntPtr_t ___m_Ptr_0;
	// UnityEngine.GUIStyle UnityEngine.GUIStyleState::m_SourceStyle
	GUIStyle_t342 * ___m_SourceStyle_1;
	// UnityEngine.Texture2D UnityEngine.GUIStyleState::m_Background
	Texture2D_t355 * ___m_Background_2;
};
