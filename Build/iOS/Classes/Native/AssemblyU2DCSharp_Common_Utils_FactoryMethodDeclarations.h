﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Utils.Factory
struct Factory_t133;

// System.Void Common.Utils.Factory::.ctor()
extern "C" void Factory__ctor_m442 (Factory_t133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Utils.Factory::.cctor()
extern "C" void Factory__cctor_m443 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Utils.Factory::Clean()
extern "C" void Factory_Clean_m444 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
