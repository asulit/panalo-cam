﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIText
struct GUIText_t443;
// UnityEngine.Material
struct Material_t82;

// UnityEngine.Material UnityEngine.GUIText::get_material()
extern "C" Material_t82 * GUIText_get_material_m1725 (GUIText_t443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
