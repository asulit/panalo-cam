﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>
struct LinkedList_1_t90;
// Common.Notification.NotificationSystem
struct NotificationSystem_t89;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Notification.NotificationSystem
struct  NotificationSystem_t89  : public Object_t
{
	// System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler> Common.Notification.NotificationSystem::handlerList
	LinkedList_1_t90 * ___handlerList_0;
};
struct NotificationSystem_t89_StaticFields{
	// Common.Notification.NotificationSystem Common.Notification.NotificationSystem::ONLY_INSTANCE
	NotificationSystem_t89 * ___ONLY_INSTANCE_1;
};
