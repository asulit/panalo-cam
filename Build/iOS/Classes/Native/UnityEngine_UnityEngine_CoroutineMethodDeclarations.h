﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Coroutine
struct Coroutine_t442;
struct Coroutine_t442_marshaled;

// System.Void UnityEngine.Coroutine::.ctor()
extern "C" void Coroutine__ctor_m3822 (Coroutine_t442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C" void Coroutine_ReleaseCoroutine_m3823 (Coroutine_t442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::Finalize()
extern "C" void Coroutine_Finalize_m3824 (Coroutine_t442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void Coroutine_t442_marshal(const Coroutine_t442& unmarshaled, Coroutine_t442_marshaled& marshaled);
extern "C" void Coroutine_t442_marshal_back(const Coroutine_t442_marshaled& marshaled, Coroutine_t442& unmarshaled);
extern "C" void Coroutine_t442_marshal_cleanup(Coroutine_t442_marshaled& marshaled);
