﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>
struct Transform_1_t3343;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m26427_gshared (Transform_1_t3343 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m26427(__this, ___object, ___method, method) (( void (*) (Transform_1_t3343 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m26427_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::Invoke(TKey,TValue)
extern "C" Object_t * Transform_1_Invoke_m26428_gshared (Transform_1_t3343 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Transform_1_Invoke_m26428(__this, ___key, ___value, method) (( Object_t * (*) (Transform_1_t3343 *, Object_t *, uint16_t, const MethodInfo*))Transform_1_Invoke_m26428_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m26429_gshared (Transform_1_t3343 * __this, Object_t * ___key, uint16_t ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m26429(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3343 *, Object_t *, uint16_t, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m26429_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Transform_1_EndInvoke_m26430_gshared (Transform_1_t3343 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m26430(__this, ___result, method) (( Object_t * (*) (Transform_1_t3343 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m26430_gshared)(__this, ___result, method)
