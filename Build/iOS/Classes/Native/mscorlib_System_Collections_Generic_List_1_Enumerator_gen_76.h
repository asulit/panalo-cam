﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.Security.Policy.StrongName>
struct List_1_t2458;
// System.Security.Policy.StrongName
struct StrongName_t2246;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>
struct  Enumerator_t3599 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::l
	List_1_t2458 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::current
	StrongName_t2246 * ___current_3;
};
