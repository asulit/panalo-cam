﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_30MethodDeclarations.h"
#define KeyValuePair_2__ctor_m24254(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3197 *, Event_t381 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m24159_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m24255(__this, method) (( Event_t381 * (*) (KeyValuePair_2_t3197 *, const MethodInfo*))KeyValuePair_2_get_Key_m24160_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m24256(__this, ___value, method) (( void (*) (KeyValuePair_2_t3197 *, Event_t381 *, const MethodInfo*))KeyValuePair_2_set_Key_m24161_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m24257(__this, method) (( int32_t (*) (KeyValuePair_2_t3197 *, const MethodInfo*))KeyValuePair_2_get_Value_m24162_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m24258(__this, ___value, method) (( void (*) (KeyValuePair_2_t3197 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m24163_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m24259(__this, method) (( String_t* (*) (KeyValuePair_2_t3197 *, const MethodInfo*))KeyValuePair_2_ToString_m24164_gshared)(__this, method)
