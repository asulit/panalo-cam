﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InputLayerStack
struct InputLayerStack_t61;
// InputLayer
struct InputLayer_t56;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void InputLayerStack::.ctor()
extern "C" void InputLayerStack__ctor_m210 (InputLayerStack_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputLayerStack::Awake()
extern "C" void InputLayerStack_Awake_m211 (InputLayerStack_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputLayerStack::ResetStack()
extern "C" void InputLayerStack_ResetStack_m212 (InputLayerStack_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputLayerStack::Push(InputLayer)
extern "C" void InputLayerStack_Push_m213 (InputLayerStack_t61 * __this, InputLayer_t56 * ___layer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputLayerStack::Pop()
extern "C" void InputLayerStack_Pop_m214 (InputLayerStack_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputLayerStack::EnsureModality()
extern "C" void InputLayerStack_EnsureModality_m215 (InputLayerStack_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InputLayer InputLayerStack::Top()
extern "C" InputLayer_t56 * InputLayerStack_Top_m216 (InputLayerStack_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputLayerStack::IsEmpty()
extern "C" bool InputLayerStack_IsEmpty_m217 (InputLayerStack_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputLayerStack::Clear()
extern "C" void InputLayerStack_Clear_m218 (InputLayerStack_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputLayerStack::RespondsToTouchPosition(UnityEngine.Vector3,InputLayer)
extern "C" bool InputLayerStack_RespondsToTouchPosition_m219 (InputLayerStack_t61 * __this, Vector3_t36  ___touchPos, InputLayer_t56 * ___requesterLayer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
