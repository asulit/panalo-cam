﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2496_t2399_marshal(const U24ArrayTypeU2496_t2399& unmarshaled, U24ArrayTypeU2496_t2399_marshaled& marshaled);
extern "C" void U24ArrayTypeU2496_t2399_marshal_back(const U24ArrayTypeU2496_t2399_marshaled& marshaled, U24ArrayTypeU2496_t2399& unmarshaled);
extern "C" void U24ArrayTypeU2496_t2399_marshal_cleanup(U24ArrayTypeU2496_t2399_marshaled& marshaled);
