﻿#pragma once
#include <stdint.h>
// UnityThreading.ThreadBase[]
struct ThreadBaseU5BU5D_t430;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityThreading.ThreadBase>
struct  List_1_t182  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityThreading.ThreadBase>::_items
	ThreadBaseU5BU5D_t430* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityThreading.ThreadBase>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityThreading.ThreadBase>::_version
	int32_t ____version_3;
};
struct List_1_t182_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityThreading.ThreadBase>::EmptyArray
	ThreadBaseU5BU5D_t430* ___EmptyArray_4;
};
