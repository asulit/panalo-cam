﻿#pragma once
#include <stdint.h>
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstruction.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Coroutine
struct  Coroutine_t442  : public YieldInstruction_t791
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	IntPtr_t ___m_Ptr_0;
};
// Native definition for marshalling of: UnityEngine.Coroutine
struct Coroutine_t442_marshaled
{
	intptr_t ___m_Ptr_0;
};
