﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackerManager
struct TrackerManager_t1220;

// Vuforia.TrackerManager Vuforia.TrackerManager::get_Instance()
extern "C" TrackerManager_t1220 * TrackerManager_get_Instance_m6864 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManager::.ctor()
extern "C" void TrackerManager__ctor_m6865 (TrackerManager_t1220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManager::.cctor()
extern "C" void TrackerManager__cctor_m6866 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
