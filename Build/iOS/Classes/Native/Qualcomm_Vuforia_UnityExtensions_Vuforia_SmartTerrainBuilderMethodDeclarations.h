﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t1078;

// System.Void Vuforia.SmartTerrainBuilder::.ctor()
extern "C" void SmartTerrainBuilder__ctor_m5465 (SmartTerrainBuilder_t1078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
