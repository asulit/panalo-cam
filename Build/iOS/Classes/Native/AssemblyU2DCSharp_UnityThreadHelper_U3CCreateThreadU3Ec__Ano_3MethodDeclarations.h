﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreadHelper/<CreateThread>c__AnonStorey16
struct U3CCreateThreadU3Ec__AnonStorey16_t180;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// UnityThreading.ThreadBase
struct ThreadBase_t169;

// System.Void UnityThreadHelper/<CreateThread>c__AnonStorey16::.ctor()
extern "C" void U3CCreateThreadU3Ec__AnonStorey16__ctor_m609 (U3CCreateThreadU3Ec__AnonStorey16_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityThreadHelper/<CreateThread>c__AnonStorey16::<>m__A(UnityThreading.ThreadBase)
extern "C" Object_t * U3CCreateThreadU3Ec__AnonStorey16_U3CU3Em__A_m610 (U3CCreateThreadU3Ec__AnonStorey16_t180 * __this, ThreadBase_t169 * ___thread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
