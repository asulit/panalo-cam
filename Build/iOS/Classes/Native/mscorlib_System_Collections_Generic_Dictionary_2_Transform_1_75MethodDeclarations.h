﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Transform_1_t3433;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.VuforiaManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m28143_gshared (Transform_1_t3433 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m28143(__this, ___object, ___method, method) (( void (*) (Transform_1_t3433 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m28143_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::Invoke(TKey,TValue)
extern "C" TrackableResultData_t1134  Transform_1_Invoke_m28144_gshared (Transform_1_t3433 * __this, int32_t ___key, TrackableResultData_t1134  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m28144(__this, ___key, ___value, method) (( TrackableResultData_t1134  (*) (Transform_1_t3433 *, int32_t, TrackableResultData_t1134 , const MethodInfo*))Transform_1_Invoke_m28144_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m28145_gshared (Transform_1_t3433 * __this, int32_t ___key, TrackableResultData_t1134  ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m28145(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3433 *, int32_t, TrackableResultData_t1134 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m28145_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,Vuforia.VuforiaManagerImpl/TrackableResultData>::EndInvoke(System.IAsyncResult)
extern "C" TrackableResultData_t1134  Transform_1_EndInvoke_m28146_gshared (Transform_1_t3433 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m28146(__this, ___result, method) (( TrackableResultData_t1134  (*) (Transform_1_t3433 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m28146_gshared)(__this, ___result, method)
