﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t675;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_61.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m22165_gshared (Enumerator_t3028 * __this, List_1_t675 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m22165(__this, ___l, method) (( void (*) (Enumerator_t3028 *, List_1_t675 *, const MethodInfo*))Enumerator__ctor_m22165_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22166_gshared (Enumerator_t3028 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22166(__this, method) (( Object_t * (*) (Enumerator_t3028 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22166_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C" void Enumerator_Dispose_m22167_gshared (Enumerator_t3028 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22167(__this, method) (( void (*) (Enumerator_t3028 *, const MethodInfo*))Enumerator_Dispose_m22167_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::VerifyState()
extern "C" void Enumerator_VerifyState_m22168_gshared (Enumerator_t3028 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22168(__this, method) (( void (*) (Enumerator_t3028 *, const MethodInfo*))Enumerator_VerifyState_m22168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22169_gshared (Enumerator_t3028 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22169(__this, method) (( bool (*) (Enumerator_t3028 *, const MethodInfo*))Enumerator_MoveNext_m22169_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C" Vector3_t36  Enumerator_get_Current_m22170_gshared (Enumerator_t3028 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22170(__this, method) (( Vector3_t36  (*) (Enumerator_t3028 *, const MethodInfo*))Enumerator_get_Current_m22170_gshared)(__this, method)
