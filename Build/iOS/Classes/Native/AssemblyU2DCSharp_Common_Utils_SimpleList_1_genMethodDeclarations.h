﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void Common.Utils.SimpleList`1<SwarmItem>::.ctor()
// Common.Utils.SimpleList`1<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_gen_1MethodDeclarations.h"
#define SimpleList_1__ctor_m1514(__this, method) (( void (*) (SimpleList_1_t112 *, const MethodInfo*))SimpleList_1__ctor_m16158_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Common.Utils.SimpleList`1<SwarmItem>::GetEnumerator()
#define SimpleList_1_GetEnumerator_m16159(__this, method) (( Object_t* (*) (SimpleList_1_t112 *, const MethodInfo*))SimpleList_1_GetEnumerator_m16160_gshared)(__this, method)
// T Common.Utils.SimpleList`1<SwarmItem>::get_Item(System.Int32)
#define SimpleList_1_get_Item_m1517(__this, ___i, method) (( SwarmItem_t124 * (*) (SimpleList_1_t112 *, int32_t, const MethodInfo*))SimpleList_1_get_Item_m16161_gshared)(__this, ___i, method)
// System.Void Common.Utils.SimpleList`1<SwarmItem>::set_Item(System.Int32,T)
#define SimpleList_1_set_Item_m16162(__this, ___i, ___value, method) (( void (*) (SimpleList_1_t112 *, int32_t, SwarmItem_t124 *, const MethodInfo*))SimpleList_1_set_Item_m16163_gshared)(__this, ___i, ___value, method)
// System.Void Common.Utils.SimpleList`1<SwarmItem>::AllocateMore()
#define SimpleList_1_AllocateMore_m16164(__this, method) (( void (*) (SimpleList_1_t112 *, const MethodInfo*))SimpleList_1_AllocateMore_m16165_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<SwarmItem>::Trim()
#define SimpleList_1_Trim_m16166(__this, method) (( void (*) (SimpleList_1_t112 *, const MethodInfo*))SimpleList_1_Trim_m16167_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<SwarmItem>::Clear()
#define SimpleList_1_Clear_m1515(__this, method) (( void (*) (SimpleList_1_t112 *, const MethodInfo*))SimpleList_1_Clear_m16168_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<SwarmItem>::Release()
#define SimpleList_1_Release_m16169(__this, method) (( void (*) (SimpleList_1_t112 *, const MethodInfo*))SimpleList_1_Release_m16170_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<SwarmItem>::Add(T)
#define SimpleList_1_Add_m1516(__this, ___item, method) (( void (*) (SimpleList_1_t112 *, SwarmItem_t124 *, const MethodInfo*))SimpleList_1_Add_m16171_gshared)(__this, ___item, method)
// System.Void Common.Utils.SimpleList`1<SwarmItem>::Insert(System.Int32,T)
#define SimpleList_1_Insert_m16172(__this, ___index, ___item, method) (( void (*) (SimpleList_1_t112 *, int32_t, SwarmItem_t124 *, const MethodInfo*))SimpleList_1_Insert_m16173_gshared)(__this, ___index, ___item, method)
// System.Boolean Common.Utils.SimpleList`1<SwarmItem>::Contains(T)
#define SimpleList_1_Contains_m16174(__this, ___item, method) (( bool (*) (SimpleList_1_t112 *, SwarmItem_t124 *, const MethodInfo*))SimpleList_1_Contains_m16175_gshared)(__this, ___item, method)
// System.Boolean Common.Utils.SimpleList`1<SwarmItem>::Remove(T)
#define SimpleList_1_Remove_m16176(__this, ___item, method) (( bool (*) (SimpleList_1_t112 *, SwarmItem_t124 *, const MethodInfo*))SimpleList_1_Remove_m16177_gshared)(__this, ___item, method)
// System.Void Common.Utils.SimpleList`1<SwarmItem>::RemoveAt(System.Int32)
#define SimpleList_1_RemoveAt_m16178(__this, ___index, method) (( void (*) (SimpleList_1_t112 *, int32_t, const MethodInfo*))SimpleList_1_RemoveAt_m16179_gshared)(__this, ___index, method)
// T[] Common.Utils.SimpleList`1<SwarmItem>::ToArray()
#define SimpleList_1_ToArray_m16180(__this, method) (( SwarmItemU5BU5D_t2597* (*) (SimpleList_1_t112 *, const MethodInfo*))SimpleList_1_ToArray_m16181_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<SwarmItem>::Sort(System.Comparison`1<T>)
#define SimpleList_1_Sort_m16182(__this, ___comparer, method) (( void (*) (SimpleList_1_t112 *, Comparison_1_t2601 *, const MethodInfo*))SimpleList_1_Sort_m16183_gshared)(__this, ___comparer, method)
// System.Int32 Common.Utils.SimpleList`1<SwarmItem>::get_Count()
#define SimpleList_1_get_Count_m1518(__this, method) (( int32_t (*) (SimpleList_1_t112 *, const MethodInfo*))SimpleList_1_get_Count_m16184_gshared)(__this, method)
// System.Boolean Common.Utils.SimpleList`1<SwarmItem>::IsEmpty()
#define SimpleList_1_IsEmpty_m16185(__this, method) (( bool (*) (SimpleList_1_t112 *, const MethodInfo*))SimpleList_1_IsEmpty_m16186_gshared)(__this, method)
// System.Void Common.Utils.SimpleList`1<SwarmItem>::TraverseItems(Common.Utils.SimpleList`1/Visitor<T>)
#define SimpleList_1_TraverseItems_m16187(__this, ___visitor, method) (( void (*) (SimpleList_1_t112 *, Visitor_t2602 *, const MethodInfo*))SimpleList_1_TraverseItems_m16188_gshared)(__this, ___visitor, method)
