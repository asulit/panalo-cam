﻿#pragma once
#include <stdint.h>
// Vuforia.Marker
struct Marker_t1231;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Marker>
struct  Comparison_1_t3314  : public MulticastDelegate_t28
{
};
