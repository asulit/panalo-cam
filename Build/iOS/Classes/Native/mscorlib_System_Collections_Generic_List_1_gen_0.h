﻿#pragma once
#include <stdint.h>
// OrthographicCameraObserver[]
struct OrthographicCameraObserverU5BU5D_t2468;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<OrthographicCameraObserver>
struct  List_1_t360  : public Object_t
{
	// T[] System.Collections.Generic.List`1<OrthographicCameraObserver>::_items
	OrthographicCameraObserverU5BU5D_t2468* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<OrthographicCameraObserver>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<OrthographicCameraObserver>::_version
	int32_t ____version_3;
};
struct List_1_t360_StaticFields{
	// T[] System.Collections.Generic.List`1<OrthographicCameraObserver>::EmptyArray
	OrthographicCameraObserverU5BU5D_t2468* ___EmptyArray_4;
};
