﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t103;
// UnityEngine.Material
struct Material_t82;
// UnityEngine.Mesh
struct Mesh_t104;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// ProceduralTexturedQuad
struct  ProceduralTexturedQuad_t102  : public MonoBehaviour_t5
{
	// UnityEngine.Vector2 ProceduralTexturedQuad::topLeftUv
	Vector2_t2  ___topLeftUv_2;
	// UnityEngine.Vector2 ProceduralTexturedQuad::topRightUv
	Vector2_t2  ___topRightUv_3;
	// UnityEngine.Vector2 ProceduralTexturedQuad::bottomRightUv
	Vector2_t2  ___bottomRightUv_4;
	// UnityEngine.Vector2 ProceduralTexturedQuad::bottomLeftUv
	Vector2_t2  ___bottomLeftUv_5;
	// UnityEngine.Texture ProceduralTexturedQuad::texture
	Texture_t103 * ___texture_6;
	// UnityEngine.Color ProceduralTexturedQuad::modulationColor
	Color_t9  ___modulationColor_7;
	// System.Single ProceduralTexturedQuad::alphaCutoff
	float ___alphaCutoff_8;
	// UnityEngine.Material ProceduralTexturedQuad::material
	Material_t82 * ___material_9;
	// UnityEngine.Mesh ProceduralTexturedQuad::mesh
	Mesh_t104 * ___mesh_10;
};
