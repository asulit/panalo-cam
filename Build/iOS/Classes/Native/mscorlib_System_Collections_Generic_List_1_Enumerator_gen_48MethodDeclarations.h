﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Dropdown/OptionData>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30MethodDeclarations.h"
#define Enumerator__ctor_m20286(__this, ___l, method) (( void (*) (Enumerator_t2902 *, List_1_t560 *, const MethodInfo*))Enumerator__ctor_m14718_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Dropdown/OptionData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20287(__this, method) (( Object_t * (*) (Enumerator_t2902 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14719_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Dropdown/OptionData>::Dispose()
#define Enumerator_Dispose_m20288(__this, method) (( void (*) (Enumerator_t2902 *, const MethodInfo*))Enumerator_Dispose_m14720_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Dropdown/OptionData>::VerifyState()
#define Enumerator_VerifyState_m20289(__this, method) (( void (*) (Enumerator_t2902 *, const MethodInfo*))Enumerator_VerifyState_m14721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Dropdown/OptionData>::MoveNext()
#define Enumerator_MoveNext_m20290(__this, method) (( bool (*) (Enumerator_t2902 *, const MethodInfo*))Enumerator_MoveNext_m14722_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Dropdown/OptionData>::get_Current()
#define Enumerator_get_Current_m20291(__this, method) (( OptionData_t558 * (*) (Enumerator_t2902 *, const MethodInfo*))Enumerator_get_Current_m14723_gshared)(__this, method)
