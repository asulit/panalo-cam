﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_46MethodDeclarations.h"
#define Dictionary_2__ctor_m7276(__this, method) (( void (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2__ctor_m19715_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m25842(__this, ___comparer, method) (( void (*) (Dictionary_2_t1123 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m19717_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::.ctor(System.Int32)
#define Dictionary_2__ctor_m25843(__this, ___capacity, method) (( void (*) (Dictionary_2_t1123 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m19719_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m25844(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1123 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m19721_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m25845(__this, method) (( Object_t * (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m19723_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m25846(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1123 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m19725_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m25847(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1123 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m19727_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m25848(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1123 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m19729_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m25849(__this, ___key, method) (( bool (*) (Dictionary_2_t1123 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m19731_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m25850(__this, ___key, method) (( void (*) (Dictionary_2_t1123 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m19733_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25851(__this, method) (( bool (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m19735_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25852(__this, method) (( Object_t * (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m19737_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25853(__this, method) (( bool (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m19739_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25854(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1123 *, KeyValuePair_2_t3308 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m19741_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25855(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1123 *, KeyValuePair_2_t3308 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m19743_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25856(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1123 *, KeyValuePair_2U5BU5D_t3758*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m19745_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25857(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1123 *, KeyValuePair_2_t3308 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m19747_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m25858(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1123 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m19749_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25859(__this, method) (( Object_t * (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19751_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25860(__this, method) (( Object_t* (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m19753_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25861(__this, method) (( Object_t * (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m19755_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::get_Count()
#define Dictionary_2_get_Count_m25862(__this, method) (( int32_t (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_get_Count_m19757_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::get_Item(TKey)
#define Dictionary_2_get_Item_m25863(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1123 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m19759_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m25864(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1123 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m19761_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m25865(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1123 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m19763_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m25866(__this, ___size, method) (( void (*) (Dictionary_2_t1123 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m19765_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m25867(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1123 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m19767_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m25868(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3308  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m19769_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m25869(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m19771_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m25870(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m19773_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m25871(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1123 *, KeyValuePair_2U5BU5D_t3758*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m19775_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::Resize()
#define Dictionary_2_Resize_m25872(__this, method) (( void (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_Resize_m19777_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::Add(TKey,TValue)
#define Dictionary_2_Add_m25873(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1123 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m19779_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::Clear()
#define Dictionary_2_Clear_m25874(__this, method) (( void (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_Clear_m19781_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m25875(__this, ___key, method) (( bool (*) (Dictionary_2_t1123 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m19783_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m25876(__this, ___value, method) (( bool (*) (Dictionary_2_t1123 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m19785_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m25877(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1123 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m19787_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m25878(__this, ___sender, method) (( void (*) (Dictionary_2_t1123 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m19789_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::Remove(TKey)
#define Dictionary_2_Remove_m25879(__this, ___key, method) (( bool (*) (Dictionary_2_t1123 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m19791_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m25880(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1123 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m19793_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::get_Keys()
#define Dictionary_2_get_Keys_m25881(__this, method) (( KeyCollection_t3309 * (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_get_Keys_m19795_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::get_Values()
#define Dictionary_2_get_Values_m7266(__this, method) (( ValueCollection_t1307 * (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_get_Values_m19796_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m25882(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1123 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m19798_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m25883(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t1123 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m19800_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m25884(__this, ___pair, method) (( bool (*) (Dictionary_2_t1123 *, KeyValuePair_2_t3308 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m19802_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m25885(__this, method) (( Enumerator_t3310  (*) (Dictionary_2_t1123 *, const MethodInfo*))Dictionary_2_GetEnumerator_m19803_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m25886(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m19805_gshared)(__this /* static, unused */, ___key, ___value, method)
