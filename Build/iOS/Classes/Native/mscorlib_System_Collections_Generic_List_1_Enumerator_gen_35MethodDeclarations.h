﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1/Enumerator<InputLayer>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30MethodDeclarations.h"
#define Enumerator__ctor_m15535(__this, ___l, method) (( void (*) (Enumerator_t2541 *, List_1_t63 *, const MethodInfo*))Enumerator__ctor_m14718_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<InputLayer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15536(__this, method) (( Object_t * (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14719_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<InputLayer>::Dispose()
#define Enumerator_Dispose_m15537(__this, method) (( void (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_Dispose_m14720_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<InputLayer>::VerifyState()
#define Enumerator_VerifyState_m15538(__this, method) (( void (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_VerifyState_m14721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<InputLayer>::MoveNext()
#define Enumerator_MoveNext_m15539(__this, method) (( bool (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_MoveNext_m14722_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<InputLayer>::get_Current()
#define Enumerator_get_Current_m15540(__this, method) (( InputLayer_t56 * (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_get_Current_m14723_gshared)(__this, method)
