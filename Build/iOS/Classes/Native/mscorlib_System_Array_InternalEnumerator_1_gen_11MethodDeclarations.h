﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIContent>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15339(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2530 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14611_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GUIContent>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15340(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2530 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14612_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIContent>::Dispose()
#define InternalEnumerator_1_Dispose_m15341(__this, method) (( void (*) (InternalEnumerator_1_t2530 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14613_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GUIContent>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15342(__this, method) (( bool (*) (InternalEnumerator_1_t2530 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14614_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.GUIContent>::get_Current()
#define InternalEnumerator_1_get_Current_m15343(__this, method) (( GUIContent_t379 * (*) (InternalEnumerator_1_t2530 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14615_gshared)(__this, method)
