﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.Rect>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_42.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18634_gshared (InternalEnumerator_1_t2793 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18634(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2793 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18634_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rect>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18635_gshared (InternalEnumerator_1_t2793 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18635(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2793 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18635_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rect>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18636_gshared (InternalEnumerator_1_t2793 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18636(__this, method) (( void (*) (InternalEnumerator_1_t2793 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18636_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rect>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18637_gshared (InternalEnumerator_1_t2793 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18637(__this, method) (( bool (*) (InternalEnumerator_1_t2793 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18637_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Rect>::get_Current()
extern "C" Rect_t267  InternalEnumerator_1_get_Current_m18638_gshared (InternalEnumerator_1_t2793 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18638(__this, method) (( Rect_t267  (*) (InternalEnumerator_1_t2793 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18638_gshared)(__this, method)
