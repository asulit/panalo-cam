﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct ObjectPool_1_t3078;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct UnityAction_1_t3079;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Vector3>
struct  ListPool_1_t773  : public Object_t
{
};
struct ListPool_1_t773_StaticFields{
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::s_ListPool
	ObjectPool_1_t3078 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::<>f__am$cache1
	UnityAction_1_t3079 * ___U3CU3Ef__amU24cache1_1;
};
