﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.CryptoStream
struct CryptoStream_t418;
// System.IO.Stream
struct Stream_t1512;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t1415;
// System.Byte[]
struct ByteU5BU5D_t139;
// System.Security.Cryptography.CryptoStreamMode
#include "mscorlib_System_Security_Cryptography_CryptoStreamMode.h"
// System.IO.SeekOrigin
#include "mscorlib_System_IO_SeekOrigin.h"

// System.Void System.Security.Cryptography.CryptoStream::.ctor(System.IO.Stream,System.Security.Cryptography.ICryptoTransform,System.Security.Cryptography.CryptoStreamMode)
extern "C" void CryptoStream__ctor_m1546 (CryptoStream_t418 * __this, Stream_t1512 * ___stream, Object_t * ___transform, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoStream::Finalize()
extern "C" void CryptoStream_Finalize_m12929 (CryptoStream_t418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CryptoStream::get_CanRead()
extern "C" bool CryptoStream_get_CanRead_m12930 (CryptoStream_t418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CryptoStream::get_CanSeek()
extern "C" bool CryptoStream_get_CanSeek_m12931 (CryptoStream_t418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.CryptoStream::get_CanWrite()
extern "C" bool CryptoStream_get_CanWrite_m12932 (CryptoStream_t418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Security.Cryptography.CryptoStream::get_Length()
extern "C" int64_t CryptoStream_get_Length_m12933 (CryptoStream_t418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Security.Cryptography.CryptoStream::get_Position()
extern "C" int64_t CryptoStream_get_Position_m12934 (CryptoStream_t418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoStream::set_Position(System.Int64)
extern "C" void CryptoStream_set_Position_m12935 (CryptoStream_t418 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoStream::Close()
extern "C" void CryptoStream_Close_m12936 (CryptoStream_t418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.CryptoStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C" int32_t CryptoStream_Read_m12937 (CryptoStream_t418 * __this, ByteU5BU5D_t139* ___buffer, int32_t ___offset, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C" void CryptoStream_Write_m12938 (CryptoStream_t418 * __this, ByteU5BU5D_t139* ___buffer, int32_t ___offset, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoStream::Flush()
extern "C" void CryptoStream_Flush_m12939 (CryptoStream_t418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoStream::FlushFinalBlock()
extern "C" void CryptoStream_FlushFinalBlock_m12940 (CryptoStream_t418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Security.Cryptography.CryptoStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C" int64_t CryptoStream_Seek_m12941 (CryptoStream_t418 * __this, int64_t ___offset, int32_t ___origin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoStream::SetLength(System.Int64)
extern "C" void CryptoStream_SetLength_m12942 (CryptoStream_t418 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoStream::Dispose(System.Boolean)
extern "C" void CryptoStream_Dispose_m12943 (CryptoStream_t418 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
