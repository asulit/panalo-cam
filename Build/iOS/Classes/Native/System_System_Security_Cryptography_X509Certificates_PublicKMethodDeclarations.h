﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t1645;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1460;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t1646;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1549;
// System.Security.Cryptography.Oid
struct Oid_t1647;
// System.Byte[]
struct ByteU5BU5D_t139;
// System.Security.Cryptography.DSA
struct DSA_t1461;
// System.Security.Cryptography.RSA
struct RSA_t1454;

// System.Void System.Security.Cryptography.X509Certificates.PublicKey::.ctor(Mono.Security.X509.X509Certificate)
extern "C" void PublicKey__ctor_m8792 (PublicKey_t1645 * __this, X509Certificate_t1460 * ___certificate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedKeyValue()
extern "C" AsnEncodedData_t1646 * PublicKey_get_EncodedKeyValue_m8793 (PublicKey_t1645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedParameters()
extern "C" AsnEncodedData_t1646 * PublicKey_get_EncodedParameters_m8794 (PublicKey_t1645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::get_Key()
extern "C" AsymmetricAlgorithm_t1549 * PublicKey_get_Key_m8795 (PublicKey_t1645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::get_Oid()
extern "C" Oid_t1647 * PublicKey_get_Oid_m8796 (PublicKey_t1645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::GetUnsignedBigInteger(System.Byte[])
extern "C" ByteU5BU5D_t139* PublicKey_GetUnsignedBigInteger_m8797 (Object_t * __this /* static, unused */, ByteU5BU5D_t139* ___integer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeDSA(System.Byte[],System.Byte[])
extern "C" DSA_t1461 * PublicKey_DecodeDSA_m8798 (Object_t * __this /* static, unused */, ByteU5BU5D_t139* ___rawPublicKey, ByteU5BU5D_t139* ___rawParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeRSA(System.Byte[])
extern "C" RSA_t1454 * PublicKey_DecodeRSA_m8799 (Object_t * __this /* static, unused */, ByteU5BU5D_t139* ___rawPublicKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
