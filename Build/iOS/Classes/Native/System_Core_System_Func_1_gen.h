﻿#pragma once
#include <stdint.h>
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`1<System.Collections.IEnumerator>
struct  Func_1_t179  : public MulticastDelegate_t28
{
};
