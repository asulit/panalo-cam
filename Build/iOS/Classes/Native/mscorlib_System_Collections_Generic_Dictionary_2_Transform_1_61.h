﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.UInt16
#include "mscorlib_System_UInt16.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt16,System.Object>
struct  Transform_1_t3343  : public MulticastDelegate_t28
{
};
