﻿#pragma once
#include <stdint.h>
// UnityThreading.ThreadBase
struct ThreadBase_t169;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityThreading.ThreadBase>
struct  Comparison_1_t2660  : public MulticastDelegate_t28
{
};
