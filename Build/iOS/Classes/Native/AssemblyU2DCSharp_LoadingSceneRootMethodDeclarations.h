﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LoadingSceneRoot
struct LoadingSceneRoot_t218;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// System.Void LoadingSceneRoot::.ctor()
extern "C" void LoadingSceneRoot__ctor_m772 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::Awake()
extern "C" void LoadingSceneRoot_Awake_m773 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::Start()
extern "C" void LoadingSceneRoot_Start_m774 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::OnEnable()
extern "C" void LoadingSceneRoot_OnEnable_m775 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::OnDisable()
extern "C" void LoadingSceneRoot_OnDisable_m776 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::Update()
extern "C" void LoadingSceneRoot_Update_m777 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::OnDestroy()
extern "C" void LoadingSceneRoot_OnDestroy_m778 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::PrepareFsm()
extern "C" void LoadingSceneRoot_PrepareFsm_m779 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoadingSceneRoot::CheckRaffleResults()
extern "C" Object_t * LoadingSceneRoot_CheckRaffleResults_m780 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoadingSceneRoot::CheckRegisterMessage()
extern "C" Object_t * LoadingSceneRoot_CheckRegisterMessage_m781 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::OnClickedRegister()
extern "C" void LoadingSceneRoot_OnClickedRegister_m782 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::OnClickedGetPlayerInformation()
extern "C" void LoadingSceneRoot_OnClickedGetPlayerInformation_m783 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::OnClickedRegisterPlayerToPromo()
extern "C" void LoadingSceneRoot_OnClickedRegisterPlayerToPromo_m784 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::OnClickedGetPromoWinner()
extern "C" void LoadingSceneRoot_OnClickedGetPromoWinner_m785 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::OnClickedRaffle()
extern "C" void LoadingSceneRoot_OnClickedRaffle_m786 (LoadingSceneRoot_t218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::<PrepareFsm>m__15(Common.Fsm.FsmState)
extern "C" void LoadingSceneRoot_U3CPrepareFsmU3Em__15_m787 (Object_t * __this /* static, unused */, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::<PrepareFsm>m__16(Common.Fsm.FsmState)
extern "C" void LoadingSceneRoot_U3CPrepareFsmU3Em__16_m788 (Object_t * __this /* static, unused */, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot::<PrepareFsm>m__17(Common.Fsm.FsmState)
extern "C" void LoadingSceneRoot_U3CPrepareFsmU3Em__17_m789 (LoadingSceneRoot_t218 * __this, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingSceneRoot::<PrepareFsm>m__18(System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>)
extern "C" bool LoadingSceneRoot_U3CPrepareFsmU3Em__18_m790 (Object_t * __this /* static, unused */, KeyValuePair_2_t352  ___r, const MethodInfo* method) IL2CPP_METHOD_ATTR;
