﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.PKCS7/EncryptedData
struct EncryptedData_t1438;
// Mono.Security.ASN1
struct ASN1_t1434;
// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t1437;
// System.Byte[]
struct ByteU5BU5D_t139;

// System.Void Mono.Security.PKCS7/EncryptedData::.ctor()
extern "C" void EncryptedData__ctor_m7694 (EncryptedData_t1438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/EncryptedData::.ctor(Mono.Security.ASN1)
extern "C" void EncryptedData__ctor_m7695 (EncryptedData_t1438 * __this, ASN1_t1434 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EncryptedData::get_EncryptionAlgorithm()
extern "C" ContentInfo_t1437 * EncryptedData_get_EncryptionAlgorithm_m7696 (EncryptedData_t1438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/EncryptedData::get_EncryptedContent()
extern "C" ByteU5BU5D_t139* EncryptedData_get_EncryptedContent_m7697 (EncryptedData_t1438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
