﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"

// System.Void System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m17185_gshared (KeyValuePair_2_t2667 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m17185(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2667 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m17185_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m17186_gshared (KeyValuePair_2_t2667 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m17186(__this, method) (( int32_t (*) (KeyValuePair_2_t2667 *, const MethodInfo*))KeyValuePair_2_get_Key_m17186_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m17187_gshared (KeyValuePair_2_t2667 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m17187(__this, ___value, method) (( void (*) (KeyValuePair_2_t2667 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m17187_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m17188_gshared (KeyValuePair_2_t2667 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m17188(__this, method) (( Object_t * (*) (KeyValuePair_2_t2667 *, const MethodInfo*))KeyValuePair_2_get_Value_m17188_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m17189_gshared (KeyValuePair_2_t2667 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m17189(__this, ___value, method) (( void (*) (KeyValuePair_2_t2667 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m17189_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m17190_gshared (KeyValuePair_2_t2667 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m17190(__this, method) (( String_t* (*) (KeyValuePair_2_t2667 *, const MethodInfo*))KeyValuePair_2_ToString_m17190_gshared)(__this, method)
