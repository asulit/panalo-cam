﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t2584;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t2583;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C" void LinkedListNode_1__ctor_m16022_gshared (LinkedListNode_1_t2584 * __this, LinkedList_1_t2583 * ___list, Object_t * ___value, const MethodInfo* method);
#define LinkedListNode_1__ctor_m16022(__this, ___list, ___value, method) (( void (*) (LinkedListNode_1_t2584 *, LinkedList_1_t2583 *, Object_t *, const MethodInfo*))LinkedListNode_1__ctor_m16022_gshared)(__this, ___list, ___value, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedListNode_1__ctor_m16023_gshared (LinkedListNode_1_t2584 * __this, LinkedList_1_t2583 * ___list, Object_t * ___value, LinkedListNode_1_t2584 * ___previousNode, LinkedListNode_1_t2584 * ___nextNode, const MethodInfo* method);
#define LinkedListNode_1__ctor_m16023(__this, ___list, ___value, ___previousNode, ___nextNode, method) (( void (*) (LinkedListNode_1_t2584 *, LinkedList_1_t2583 *, Object_t *, LinkedListNode_1_t2584 *, LinkedListNode_1_t2584 *, const MethodInfo*))LinkedListNode_1__ctor_m16023_gshared)(__this, ___list, ___value, ___previousNode, ___nextNode, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C" void LinkedListNode_1_Detach_m16024_gshared (LinkedListNode_1_t2584 * __this, const MethodInfo* method);
#define LinkedListNode_1_Detach_m16024(__this, method) (( void (*) (LinkedListNode_1_t2584 *, const MethodInfo*))LinkedListNode_1_Detach_m16024_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C" LinkedList_1_t2583 * LinkedListNode_1_get_List_m16025_gshared (LinkedListNode_1_t2584 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_List_m16025(__this, method) (( LinkedList_1_t2583 * (*) (LinkedListNode_1_t2584 *, const MethodInfo*))LinkedListNode_1_get_List_m16025_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
extern "C" LinkedListNode_1_t2584 * LinkedListNode_1_get_Next_m16026_gshared (LinkedListNode_1_t2584 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Next_m16026(__this, method) (( LinkedListNode_1_t2584 * (*) (LinkedListNode_1_t2584 *, const MethodInfo*))LinkedListNode_1_get_Next_m16026_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C" Object_t * LinkedListNode_1_get_Value_m16027_gshared (LinkedListNode_1_t2584 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Value_m16027(__this, method) (( Object_t * (*) (LinkedListNode_1_t2584 *, const MethodInfo*))LinkedListNode_1_get_Value_m16027_gshared)(__this, method)
