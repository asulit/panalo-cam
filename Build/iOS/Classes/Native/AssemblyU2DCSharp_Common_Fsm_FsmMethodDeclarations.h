﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.Fsm
struct Fsm_t47;
// System.String
struct String_t;
// Common.Fsm.FsmState
struct FsmState_t29;
// Common.Fsm.Fsm/StateActionProcessor
struct StateActionProcessor_t50;
// Common.Fsm.FsmAction
struct FsmAction_t51;

// System.Void Common.Fsm.Fsm::.ctor(System.String)
extern "C" void Fsm__ctor_m164 (Fsm_t47 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Fsm.Fsm::get_Name()
extern "C" String_t* Fsm_get_Name_m165 (Fsm_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Fsm.FsmState Common.Fsm.Fsm::AddState(System.String)
extern "C" Object_t * Fsm_AddState_m166 (Fsm_t47 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm::ProcessStateActions(Common.Fsm.FsmState,Common.Fsm.Fsm/StateActionProcessor)
extern "C" void Fsm_ProcessStateActions_m167 (Fsm_t47 * __this, Object_t * ___state, StateActionProcessor_t50 * ___actionProcessor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm::Start(System.String)
extern "C" void Fsm_Start_m168 (Fsm_t47 * __this, String_t* ___stateName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm::ChangeToState(Common.Fsm.FsmState)
extern "C" void Fsm_ChangeToState_m169 (Fsm_t47 * __this, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm::EnterState(Common.Fsm.FsmState)
extern "C" void Fsm_EnterState_m170 (Fsm_t47 * __this, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm::ExitState(Common.Fsm.FsmState)
extern "C" void Fsm_ExitState_m171 (Fsm_t47 * __this, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm::Update()
extern "C" void Fsm_Update_m172 (Fsm_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Fsm.FsmState Common.Fsm.Fsm::GetCurrentState()
extern "C" Object_t * Fsm_GetCurrentState_m173 (Fsm_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm::SendEvent(System.String)
extern "C" void Fsm_SendEvent_m174 (Fsm_t47 * __this, String_t* ___eventId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Fsm.FsmState Common.Fsm.Fsm::ResolveTransition(System.String)
extern "C" Object_t * Fsm_ResolveTransition_m175 (Fsm_t47 * __this, String_t* ___eventId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm::AddGlobalTransition(System.String,Common.Fsm.FsmState)
extern "C" void Fsm_AddGlobalTransition_m176 (Fsm_t47 * __this, String_t* ___eventId, Object_t * ___destinationState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm::<EnterState>m__0(Common.Fsm.FsmAction)
extern "C" void Fsm_U3CEnterStateU3Em__0_m177 (Object_t * __this /* static, unused */, Object_t * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm::<Update>m__2(Common.Fsm.FsmAction)
extern "C" void Fsm_U3CUpdateU3Em__2_m178 (Object_t * __this /* static, unused */, Object_t * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
