﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>
struct ShimEnumerator_t2678;
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
struct Dictionary_2_t2665;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m17262_gshared (ShimEnumerator_t2678 * __this, Dictionary_2_t2665 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m17262(__this, ___host, method) (( void (*) (ShimEnumerator_t2678 *, Dictionary_2_t2665 *, const MethodInfo*))ShimEnumerator__ctor_m17262_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17263_gshared (ShimEnumerator_t2678 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m17263(__this, method) (( bool (*) (ShimEnumerator_t2678 *, const MethodInfo*))ShimEnumerator_MoveNext_m17263_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m17264_gshared (ShimEnumerator_t2678 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m17264(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t2678 *, const MethodInfo*))ShimEnumerator_get_Entry_m17264_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17265_gshared (ShimEnumerator_t2678 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m17265(__this, method) (( Object_t * (*) (ShimEnumerator_t2678 *, const MethodInfo*))ShimEnumerator_get_Key_m17265_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17266_gshared (ShimEnumerator_t2678 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m17266(__this, method) (( Object_t * (*) (ShimEnumerator_t2678 *, const MethodInfo*))ShimEnumerator_get_Value_m17266_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m17267_gshared (ShimEnumerator_t2678 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m17267(__this, method) (( Object_t * (*) (ShimEnumerator_t2678 *, const MethodInfo*))ShimEnumerator_get_Current_m17267_gshared)(__this, method)
