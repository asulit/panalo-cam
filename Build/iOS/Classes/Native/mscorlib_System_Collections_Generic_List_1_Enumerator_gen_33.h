﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Common.Fsm.FsmAction>
struct List_1_t49;
// Common.Fsm.FsmAction
struct FsmAction_t51;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Common.Fsm.FsmAction>
struct  Enumerator_t2528 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Common.Fsm.FsmAction>::l
	List_1_t49 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Common.Fsm.FsmAction>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Common.Fsm.FsmAction>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Common.Fsm.FsmAction>::current
	Object_t * ___current_3;
};
