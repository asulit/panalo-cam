﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"
#define Enumerator__ctor_m15862(__this, ___dictionary, method) (( void (*) (Enumerator_t435 *, Dictionary_2_t86 *, const MethodInfo*))Enumerator__ctor_m15140_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15863(__this, method) (( Object_t * (*) (Enumerator_t435 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15141_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15864(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t435 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15142_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15865(__this, method) (( Object_t * (*) (Enumerator_t435 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15143_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15866(__this, method) (( Object_t * (*) (Enumerator_t435 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::MoveNext()
#define Enumerator_MoveNext_m1669(__this, method) (( bool (*) (Enumerator_t435 *, const MethodInfo*))Enumerator_MoveNext_m15145_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::get_Current()
#define Enumerator_get_Current_m1664(__this, method) (( KeyValuePair_2_t434  (*) (Enumerator_t435 *, const MethodInfo*))Enumerator_get_Current_m15146_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15867(__this, method) (( String_t* (*) (Enumerator_t435 *, const MethodInfo*))Enumerator_get_CurrentKey_m15147_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15868(__this, method) (( Object_t * (*) (Enumerator_t435 *, const MethodInfo*))Enumerator_get_CurrentValue_m15148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::VerifyState()
#define Enumerator_VerifyState_m15869(__this, method) (( void (*) (Enumerator_t435 *, const MethodInfo*))Enumerator_VerifyState_m15149_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15870(__this, method) (( void (*) (Enumerator_t435 *, const MethodInfo*))Enumerator_VerifyCurrent_m15150_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::Dispose()
#define Enumerator_Dispose_m15871(__this, method) (( void (*) (Enumerator_t435 *, const MethodInfo*))Enumerator_Dispose_m15151_gshared)(__this, method)
