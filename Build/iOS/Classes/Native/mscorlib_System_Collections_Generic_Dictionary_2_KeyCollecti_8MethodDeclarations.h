﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2544;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_8.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15662_gshared (Enumerator_t2549 * __this, Dictionary_2_t2544 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15662(__this, ___host, method) (( void (*) (Enumerator_t2549 *, Dictionary_2_t2544 *, const MethodInfo*))Enumerator__ctor_m15662_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15663_gshared (Enumerator_t2549 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15663(__this, method) (( Object_t * (*) (Enumerator_t2549 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15663_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m15664_gshared (Enumerator_t2549 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15664(__this, method) (( void (*) (Enumerator_t2549 *, const MethodInfo*))Enumerator_Dispose_m15664_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15665_gshared (Enumerator_t2549 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15665(__this, method) (( bool (*) (Enumerator_t2549 *, const MethodInfo*))Enumerator_MoveNext_m15665_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15666_gshared (Enumerator_t2549 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15666(__this, method) (( Object_t * (*) (Enumerator_t2549 *, const MethodInfo*))Enumerator_get_Current_m15666_gshared)(__this, method)
