﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8MethodDeclarations.h"
#define KeyValuePair_2__ctor_m21184(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2966 *, Object_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m15642_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m21185(__this, method) (( Object_t * (*) (KeyValuePair_2_t2966 *, const MethodInfo*))KeyValuePair_2_get_Key_m15643_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21186(__this, ___value, method) (( void (*) (KeyValuePair_2_t2966 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m15644_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m21187(__this, method) (( int32_t (*) (KeyValuePair_2_t2966 *, const MethodInfo*))KeyValuePair_2_get_Value_m15645_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21188(__this, ___value, method) (( void (*) (KeyValuePair_2_t2966 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m15646_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m21189(__this, method) (( String_t* (*) (KeyValuePair_2_t2966 *, const MethodInfo*))KeyValuePair_2_ToString_m15647_gshared)(__this, method)
