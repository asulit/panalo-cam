﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"
#define List_1__ctor_m7236(__this, method) (( void (*) (List_1_t1296 *, const MethodInfo*))List_1__ctor_m1429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m24907(__this, ___collection, method) (( void (*) (List_1_t1296 *, Object_t*, const MethodInfo*))List_1__ctor_m14617_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.ctor(System.Int32)
#define List_1__ctor_m24908(__this, ___capacity, method) (( void (*) (List_1_t1296 *, int32_t, const MethodInfo*))List_1__ctor_m14619_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::.cctor()
#define List_1__cctor_m24909(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14621_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24910(__this, method) (( Object_t* (*) (List_1_t1296 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14623_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m24911(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1296 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m14625_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m24912(__this, method) (( Object_t * (*) (List_1_t1296 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m14627_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m24913(__this, ___item, method) (( int32_t (*) (List_1_t1296 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m14629_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m24914(__this, ___item, method) (( bool (*) (List_1_t1296 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m14631_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m24915(__this, ___item, method) (( int32_t (*) (List_1_t1296 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m14633_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m24916(__this, ___index, ___item, method) (( void (*) (List_1_t1296 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m14635_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m24917(__this, ___item, method) (( void (*) (List_1_t1296 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m14637_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24918(__this, method) (( bool (*) (List_1_t1296 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14639_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m24919(__this, method) (( bool (*) (List_1_t1296 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m14641_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m24920(__this, method) (( Object_t * (*) (List_1_t1296 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m14643_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m24921(__this, method) (( bool (*) (List_1_t1296 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m14645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m24922(__this, method) (( bool (*) (List_1_t1296 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m14647_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m24923(__this, ___index, method) (( Object_t * (*) (List_1_t1296 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m14649_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m24924(__this, ___index, ___value, method) (( void (*) (List_1_t1296 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m14651_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Add(T)
#define List_1_Add_m24925(__this, ___item, method) (( void (*) (List_1_t1296 *, VirtualButton_t1223 *, const MethodInfo*))List_1_Add_m14653_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m24926(__this, ___newCount, method) (( void (*) (List_1_t1296 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m14655_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m24927(__this, ___idx, ___count, method) (( void (*) (List_1_t1296 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m14657_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m24928(__this, ___collection, method) (( void (*) (List_1_t1296 *, Object_t*, const MethodInfo*))List_1_AddCollection_m14659_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m24929(__this, ___enumerable, method) (( void (*) (List_1_t1296 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m14661_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m24930(__this, ___collection, method) (( void (*) (List_1_t1296 *, Object_t*, const MethodInfo*))List_1_AddRange_m14663_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::AsReadOnly()
#define List_1_AsReadOnly_m24931(__this, method) (( ReadOnlyCollection_1_t3246 * (*) (List_1_t1296 *, const MethodInfo*))List_1_AsReadOnly_m14665_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Clear()
#define List_1_Clear_m24932(__this, method) (( void (*) (List_1_t1296 *, const MethodInfo*))List_1_Clear_m14667_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::Contains(T)
#define List_1_Contains_m24933(__this, ___item, method) (( bool (*) (List_1_t1296 *, VirtualButton_t1223 *, const MethodInfo*))List_1_Contains_m14669_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m24934(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1296 *, VirtualButtonU5BU5D_t3245*, int32_t, const MethodInfo*))List_1_CopyTo_m14671_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButton>::Find(System.Predicate`1<T>)
#define List_1_Find_m24935(__this, ___match, method) (( VirtualButton_t1223 * (*) (List_1_t1296 *, Predicate_1_t3248 *, const MethodInfo*))List_1_Find_m14673_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m24936(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3248 *, const MethodInfo*))List_1_CheckMatch_m14675_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m24937(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1296 *, int32_t, int32_t, Predicate_1_t3248 *, const MethodInfo*))List_1_GetIndex_m14677_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.VirtualButton>::GetEnumerator()
#define List_1_GetEnumerator_m24938(__this, method) (( Enumerator_t3249  (*) (List_1_t1296 *, const MethodInfo*))List_1_GetEnumerator_m14679_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::IndexOf(T)
#define List_1_IndexOf_m24939(__this, ___item, method) (( int32_t (*) (List_1_t1296 *, VirtualButton_t1223 *, const MethodInfo*))List_1_IndexOf_m14681_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m24940(__this, ___start, ___delta, method) (( void (*) (List_1_t1296 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m14683_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m24941(__this, ___index, method) (( void (*) (List_1_t1296 *, int32_t, const MethodInfo*))List_1_CheckIndex_m14685_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Insert(System.Int32,T)
#define List_1_Insert_m24942(__this, ___index, ___item, method) (( void (*) (List_1_t1296 *, int32_t, VirtualButton_t1223 *, const MethodInfo*))List_1_Insert_m14687_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m24943(__this, ___collection, method) (( void (*) (List_1_t1296 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m14689_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButton>::Remove(T)
#define List_1_Remove_m24944(__this, ___item, method) (( bool (*) (List_1_t1296 *, VirtualButton_t1223 *, const MethodInfo*))List_1_Remove_m14691_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m24945(__this, ___match, method) (( int32_t (*) (List_1_t1296 *, Predicate_1_t3248 *, const MethodInfo*))List_1_RemoveAll_m14693_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m24946(__this, ___index, method) (( void (*) (List_1_t1296 *, int32_t, const MethodInfo*))List_1_RemoveAt_m14695_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m24947(__this, ___index, ___count, method) (( void (*) (List_1_t1296 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m14697_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Reverse()
#define List_1_Reverse_m24948(__this, method) (( void (*) (List_1_t1296 *, const MethodInfo*))List_1_Reverse_m14699_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Sort()
#define List_1_Sort_m24949(__this, method) (( void (*) (List_1_t1296 *, const MethodInfo*))List_1_Sort_m14701_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m24950(__this, ___comparison, method) (( void (*) (List_1_t1296 *, Comparison_1_t3250 *, const MethodInfo*))List_1_Sort_m14703_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.VirtualButton>::ToArray()
#define List_1_ToArray_m24951(__this, method) (( VirtualButtonU5BU5D_t3245* (*) (List_1_t1296 *, const MethodInfo*))List_1_ToArray_m14705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::TrimExcess()
#define List_1_TrimExcess_m24952(__this, method) (( void (*) (List_1_t1296 *, const MethodInfo*))List_1_TrimExcess_m14707_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Capacity()
#define List_1_get_Capacity_m24953(__this, method) (( int32_t (*) (List_1_t1296 *, const MethodInfo*))List_1_get_Capacity_m14709_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m24954(__this, ___value, method) (( void (*) (List_1_t1296 *, int32_t, const MethodInfo*))List_1_set_Capacity_m14711_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Count()
#define List_1_get_Count_m24955(__this, method) (( int32_t (*) (List_1_t1296 *, const MethodInfo*))List_1_get_Count_m14713_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButton>::get_Item(System.Int32)
#define List_1_get_Item_m24956(__this, ___index, method) (( VirtualButton_t1223 * (*) (List_1_t1296 *, int32_t, const MethodInfo*))List_1_get_Item_m14715_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButton>::set_Item(System.Int32,T)
#define List_1_set_Item_m24957(__this, ___index, ___value, method) (( void (*) (List_1_t1296 *, int32_t, VirtualButton_t1223 *, const MethodInfo*))List_1_set_Item_m14717_gshared)(__this, ___index, ___value, method)
