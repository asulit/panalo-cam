﻿#pragma once
#include <stdint.h>
// Vuforia.Trackable
struct Trackable_t1060;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Trackable>
struct  Comparison_1_t3290  : public MulticastDelegate_t28
{
};
