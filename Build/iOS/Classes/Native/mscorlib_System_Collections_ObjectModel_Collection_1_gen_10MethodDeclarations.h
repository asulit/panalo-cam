﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t3143;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1010;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t3732;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t753;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m23577_gshared (Collection_1_t3143 * __this, const MethodInfo* method);
#define Collection_1__ctor_m23577(__this, method) (( void (*) (Collection_1_t3143 *, const MethodInfo*))Collection_1__ctor_m23577_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23578_gshared (Collection_1_t3143 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23578(__this, method) (( bool (*) (Collection_1_t3143 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23578_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23579_gshared (Collection_1_t3143 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m23579(__this, ___array, ___index, method) (( void (*) (Collection_1_t3143 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m23579_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m23580_gshared (Collection_1_t3143 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m23580(__this, method) (( Object_t * (*) (Collection_1_t3143 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m23580_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m23581_gshared (Collection_1_t3143 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m23581(__this, ___value, method) (( int32_t (*) (Collection_1_t3143 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m23581_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m23582_gshared (Collection_1_t3143 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m23582(__this, ___value, method) (( bool (*) (Collection_1_t3143 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m23582_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m23583_gshared (Collection_1_t3143 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m23583(__this, ___value, method) (( int32_t (*) (Collection_1_t3143 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m23583_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m23584_gshared (Collection_1_t3143 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m23584(__this, ___index, ___value, method) (( void (*) (Collection_1_t3143 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m23584_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m23585_gshared (Collection_1_t3143 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m23585(__this, ___value, method) (( void (*) (Collection_1_t3143 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m23585_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m23586_gshared (Collection_1_t3143 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m23586(__this, method) (( bool (*) (Collection_1_t3143 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m23586_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m23587_gshared (Collection_1_t3143 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m23587(__this, method) (( Object_t * (*) (Collection_1_t3143 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m23587_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m23588_gshared (Collection_1_t3143 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m23588(__this, method) (( bool (*) (Collection_1_t3143 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m23588_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m23589_gshared (Collection_1_t3143 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m23589(__this, method) (( bool (*) (Collection_1_t3143 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m23589_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m23590_gshared (Collection_1_t3143 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m23590(__this, ___index, method) (( Object_t * (*) (Collection_1_t3143 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m23590_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m23591_gshared (Collection_1_t3143 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m23591(__this, ___index, ___value, method) (( void (*) (Collection_1_t3143 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m23591_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m23592_gshared (Collection_1_t3143 * __this, UILineInfo_t752  ___item, const MethodInfo* method);
#define Collection_1_Add_m23592(__this, ___item, method) (( void (*) (Collection_1_t3143 *, UILineInfo_t752 , const MethodInfo*))Collection_1_Add_m23592_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m23593_gshared (Collection_1_t3143 * __this, const MethodInfo* method);
#define Collection_1_Clear_m23593(__this, method) (( void (*) (Collection_1_t3143 *, const MethodInfo*))Collection_1_Clear_m23593_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m23594_gshared (Collection_1_t3143 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m23594(__this, method) (( void (*) (Collection_1_t3143 *, const MethodInfo*))Collection_1_ClearItems_m23594_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m23595_gshared (Collection_1_t3143 * __this, UILineInfo_t752  ___item, const MethodInfo* method);
#define Collection_1_Contains_m23595(__this, ___item, method) (( bool (*) (Collection_1_t3143 *, UILineInfo_t752 , const MethodInfo*))Collection_1_Contains_m23595_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m23596_gshared (Collection_1_t3143 * __this, UILineInfoU5BU5D_t1010* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m23596(__this, ___array, ___index, method) (( void (*) (Collection_1_t3143 *, UILineInfoU5BU5D_t1010*, int32_t, const MethodInfo*))Collection_1_CopyTo_m23596_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m23597_gshared (Collection_1_t3143 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m23597(__this, method) (( Object_t* (*) (Collection_1_t3143 *, const MethodInfo*))Collection_1_GetEnumerator_m23597_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m23598_gshared (Collection_1_t3143 * __this, UILineInfo_t752  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m23598(__this, ___item, method) (( int32_t (*) (Collection_1_t3143 *, UILineInfo_t752 , const MethodInfo*))Collection_1_IndexOf_m23598_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m23599_gshared (Collection_1_t3143 * __this, int32_t ___index, UILineInfo_t752  ___item, const MethodInfo* method);
#define Collection_1_Insert_m23599(__this, ___index, ___item, method) (( void (*) (Collection_1_t3143 *, int32_t, UILineInfo_t752 , const MethodInfo*))Collection_1_Insert_m23599_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m23600_gshared (Collection_1_t3143 * __this, int32_t ___index, UILineInfo_t752  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m23600(__this, ___index, ___item, method) (( void (*) (Collection_1_t3143 *, int32_t, UILineInfo_t752 , const MethodInfo*))Collection_1_InsertItem_m23600_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m23601_gshared (Collection_1_t3143 * __this, UILineInfo_t752  ___item, const MethodInfo* method);
#define Collection_1_Remove_m23601(__this, ___item, method) (( bool (*) (Collection_1_t3143 *, UILineInfo_t752 , const MethodInfo*))Collection_1_Remove_m23601_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m23602_gshared (Collection_1_t3143 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m23602(__this, ___index, method) (( void (*) (Collection_1_t3143 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m23602_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m23603_gshared (Collection_1_t3143 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m23603(__this, ___index, method) (( void (*) (Collection_1_t3143 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m23603_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m23604_gshared (Collection_1_t3143 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m23604(__this, method) (( int32_t (*) (Collection_1_t3143 *, const MethodInfo*))Collection_1_get_Count_m23604_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t752  Collection_1_get_Item_m23605_gshared (Collection_1_t3143 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m23605(__this, ___index, method) (( UILineInfo_t752  (*) (Collection_1_t3143 *, int32_t, const MethodInfo*))Collection_1_get_Item_m23605_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m23606_gshared (Collection_1_t3143 * __this, int32_t ___index, UILineInfo_t752  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m23606(__this, ___index, ___value, method) (( void (*) (Collection_1_t3143 *, int32_t, UILineInfo_t752 , const MethodInfo*))Collection_1_set_Item_m23606_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m23607_gshared (Collection_1_t3143 * __this, int32_t ___index, UILineInfo_t752  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m23607(__this, ___index, ___item, method) (( void (*) (Collection_1_t3143 *, int32_t, UILineInfo_t752 , const MethodInfo*))Collection_1_SetItem_m23607_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m23608_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m23608(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m23608_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t752  Collection_1_ConvertItem_m23609_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m23609(__this /* static, unused */, ___item, method) (( UILineInfo_t752  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m23609_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m23610_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m23610(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m23610_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m23611_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m23611(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m23611_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m23612_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m23612(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m23612_gshared)(__this /* static, unused */, ___list, method)
