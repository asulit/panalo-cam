﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.TaskDistributor
struct TaskDistributor_t166;
// System.Threading.WaitHandle
struct WaitHandle_t351;
// UnityThreading.Dispatcher
struct Dispatcher_t162;

// System.Void UnityThreading.TaskDistributor::.ctor()
extern "C" void TaskDistributor__ctor_m564 (TaskDistributor_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskDistributor::.ctor(System.Int32)
extern "C" void TaskDistributor__ctor_m565 (TaskDistributor_t166 * __this, int32_t ___workerThreadCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskDistributor::.ctor(System.Int32,System.Boolean)
extern "C" void TaskDistributor__ctor_m566 (TaskDistributor_t166 * __this, int32_t ___workerThreadCount, bool ___autoStart, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle UnityThreading.TaskDistributor::get_NewDataWaitHandle()
extern "C" WaitHandle_t351 * TaskDistributor_get_NewDataWaitHandle_m567 (TaskDistributor_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.TaskDistributor UnityThreading.TaskDistributor::get_Main()
extern "C" TaskDistributor_t166 * TaskDistributor_get_Main_m568 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskDistributor::Start()
extern "C" void TaskDistributor_Start_m569 (TaskDistributor_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskDistributor::FillTasks(UnityThreading.Dispatcher)
extern "C" void TaskDistributor_FillTasks_m570 (TaskDistributor_t166 * __this, Dispatcher_t162 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskDistributor::CheckAccessLimitation()
extern "C" void TaskDistributor_CheckAccessLimitation_m571 (TaskDistributor_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskDistributor::Dispose()
extern "C" void TaskDistributor_Dispose_m572 (TaskDistributor_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
