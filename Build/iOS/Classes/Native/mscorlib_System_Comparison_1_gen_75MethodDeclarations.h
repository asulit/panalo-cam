﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>
struct Comparison_1_t3469;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m28549_gshared (Comparison_1_t3469 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m28549(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3469 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m28549_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m28550_gshared (Comparison_1_t3469 * __this, TargetSearchResult_t1212  ___x, TargetSearchResult_t1212  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m28550(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3469 *, TargetSearchResult_t1212 , TargetSearchResult_t1212 , const MethodInfo*))Comparison_1_Invoke_m28550_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m28551_gshared (Comparison_1_t3469 * __this, TargetSearchResult_t1212  ___x, TargetSearchResult_t1212  ___y, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m28551(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3469 *, TargetSearchResult_t1212 , TargetSearchResult_t1212 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m28551_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<Vuforia.TargetFinder/TargetSearchResult>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m28552_gshared (Comparison_1_t3469 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m28552(__this, ___result, method) (( int32_t (*) (Comparison_1_t3469 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m28552_gshared)(__this, ___result, method)
