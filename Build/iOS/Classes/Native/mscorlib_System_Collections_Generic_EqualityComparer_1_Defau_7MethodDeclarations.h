﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t2838;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m19257_gshared (DefaultComparer_t2838 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m19257(__this, method) (( void (*) (DefaultComparer_t2838 *, const MethodInfo*))DefaultComparer__ctor_m19257_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m19258_gshared (DefaultComparer_t2838 * __this, RaycastResult_t512  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m19258(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2838 *, RaycastResult_t512 , const MethodInfo*))DefaultComparer_GetHashCode_m19258_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m19259_gshared (DefaultComparer_t2838 * __this, RaycastResult_t512  ___x, RaycastResult_t512  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m19259(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2838 *, RaycastResult_t512 , RaycastResult_t512 , const MethodInfo*))DefaultComparer_Equals_m19259_gshared)(__this, ___x, ___y, method)
