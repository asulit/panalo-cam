﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t303;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t301;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t1077;

// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionBehaviour()
extern "C" ReconstructionAbstractBehaviour_t301 * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m5459 (ReconstructionFromTargetAbstractBehaviour_t303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionFromTarget()
extern "C" Object_t * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m5460 (ReconstructionFromTargetAbstractBehaviour_t303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Awake()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Awake_m5461 (ReconstructionFromTargetAbstractBehaviour_t303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnDestroy()
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnDestroy_m5462 (ReconstructionFromTargetAbstractBehaviour_t303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Initialize()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Initialize_m5463 (ReconstructionFromTargetAbstractBehaviour_t303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnTrackerStarted()
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m5464 (ReconstructionFromTargetAbstractBehaviour_t303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::.ctor()
extern "C" void ReconstructionFromTargetAbstractBehaviour__ctor_m1868 (ReconstructionFromTargetAbstractBehaviour_t303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
