﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,ERaffleResult>
struct ShimEnumerator_t2720;
// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>
struct Dictionary_2_t208;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,ERaffleResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m17688_gshared (ShimEnumerator_t2720 * __this, Dictionary_2_t208 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m17688(__this, ___host, method) (( void (*) (ShimEnumerator_t2720 *, Dictionary_2_t208 *, const MethodInfo*))ShimEnumerator__ctor_m17688_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,ERaffleResult>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17689_gshared (ShimEnumerator_t2720 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m17689(__this, method) (( bool (*) (ShimEnumerator_t2720 *, const MethodInfo*))ShimEnumerator_MoveNext_m17689_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,ERaffleResult>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m17690_gshared (ShimEnumerator_t2720 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m17690(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t2720 *, const MethodInfo*))ShimEnumerator_get_Entry_m17690_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,ERaffleResult>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17691_gshared (ShimEnumerator_t2720 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m17691(__this, method) (( Object_t * (*) (ShimEnumerator_t2720 *, const MethodInfo*))ShimEnumerator_get_Key_m17691_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,ERaffleResult>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17692_gshared (ShimEnumerator_t2720 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m17692(__this, method) (( Object_t * (*) (ShimEnumerator_t2720 *, const MethodInfo*))ShimEnumerator_get_Value_m17692_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,ERaffleResult>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m17693_gshared (ShimEnumerator_t2720 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m17693(__this, method) (( Object_t * (*) (ShimEnumerator_t2720 *, const MethodInfo*))ShimEnumerator_get_Current_m17693_gshared)(__this, method)
