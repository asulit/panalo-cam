﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct ObjectPool_1_t3082;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct UnityAction_1_t3083;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.ListPool`1<UnityEngine.Color32>
struct  ListPool_1_t774  : public Object_t
{
};
struct ListPool_1_t774_StaticFields{
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::s_ListPool
	ObjectPool_1_t3082 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::<>f__am$cache1
	UnityAction_1_t3083 * ___U3CU3Ef__amU24cache1_1;
};
