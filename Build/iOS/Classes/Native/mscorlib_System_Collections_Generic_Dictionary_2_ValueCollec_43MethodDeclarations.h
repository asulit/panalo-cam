﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>
struct ValueCollection_t2697;
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
struct Dictionary_2_t2688;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_44.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m17470_gshared (ValueCollection_t2697 * __this, Dictionary_2_t2688 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m17470(__this, ___dictionary, method) (( void (*) (ValueCollection_t2697 *, Dictionary_2_t2688 *, const MethodInfo*))ValueCollection__ctor_m17470_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17471_gshared (ValueCollection_t2697 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17471(__this, ___item, method) (( void (*) (ValueCollection_t2697 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17471_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17472_gshared (ValueCollection_t2697 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17472(__this, method) (( void (*) (ValueCollection_t2697 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17472_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17473_gshared (ValueCollection_t2697 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17473(__this, ___item, method) (( bool (*) (ValueCollection_t2697 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17473_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17474_gshared (ValueCollection_t2697 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17474(__this, ___item, method) (( bool (*) (ValueCollection_t2697 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17474_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17475_gshared (ValueCollection_t2697 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17475(__this, method) (( Object_t* (*) (ValueCollection_t2697 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17475_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m17476_gshared (ValueCollection_t2697 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m17476(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2697 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m17476_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17477_gshared (ValueCollection_t2697 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17477(__this, method) (( Object_t * (*) (ValueCollection_t2697 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17477_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17478_gshared (ValueCollection_t2697 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17478(__this, method) (( bool (*) (ValueCollection_t2697 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17478_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17479_gshared (ValueCollection_t2697 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17479(__this, method) (( bool (*) (ValueCollection_t2697 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17479_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m17480_gshared (ValueCollection_t2697 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m17480(__this, method) (( Object_t * (*) (ValueCollection_t2697 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m17480_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m17481_gshared (ValueCollection_t2697 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m17481(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2697 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m17481_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::GetEnumerator()
extern "C" Enumerator_t2698  ValueCollection_GetEnumerator_m17482_gshared (ValueCollection_t2697 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m17482(__this, method) (( Enumerator_t2698  (*) (ValueCollection_t2697 *, const MethodInfo*))ValueCollection_GetEnumerator_m17482_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m17483_gshared (ValueCollection_t2697 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m17483(__this, method) (( int32_t (*) (ValueCollection_t2697 *, const MethodInfo*))ValueCollection_get_Count_m17483_gshared)(__this, method)
