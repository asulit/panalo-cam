﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t3556;
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;
// System.Exception
struct Exception_t359;

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m29675_gshared (ArrayReadOnlyList_1_t3556 * __this, ObjectU5BU5D_t356* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m29675(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t3556 *, ObjectU5BU5D_t356*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m29675_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29676_gshared (ArrayReadOnlyList_1_t3556 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29676(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3556 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m29676_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m29677_gshared (ArrayReadOnlyList_1_t3556 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m29677(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3556 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m29677_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m29678_gshared (ArrayReadOnlyList_1_t3556 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m29678(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t3556 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m29678_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m29679_gshared (ArrayReadOnlyList_1_t3556 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m29679(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t3556 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m29679_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m29680_gshared (ArrayReadOnlyList_1_t3556 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m29680(__this, method) (( bool (*) (ArrayReadOnlyList_1_t3556 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m29680_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m29681_gshared (ArrayReadOnlyList_1_t3556 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m29681(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3556 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m29681_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m29682_gshared (ArrayReadOnlyList_1_t3556 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m29682(__this, method) (( void (*) (ArrayReadOnlyList_1_t3556 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m29682_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m29683_gshared (ArrayReadOnlyList_1_t3556 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m29683(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3556 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m29683_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m29684_gshared (ArrayReadOnlyList_1_t3556 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m29684(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3556 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m29684_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m29685_gshared (ArrayReadOnlyList_1_t3556 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m29685(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t3556 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m29685_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m29686_gshared (ArrayReadOnlyList_1_t3556 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m29686(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t3556 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m29686_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m29687_gshared (ArrayReadOnlyList_1_t3556 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m29687(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3556 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m29687_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m29688_gshared (ArrayReadOnlyList_1_t3556 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m29688(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3556 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m29688_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m29689_gshared (ArrayReadOnlyList_1_t3556 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m29689(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3556 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m29689_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t359 * ArrayReadOnlyList_1_ReadOnlyError_m29690_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m29690(__this /* static, unused */, method) (( Exception_t359 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m29690_gshared)(__this /* static, unused */, method)
