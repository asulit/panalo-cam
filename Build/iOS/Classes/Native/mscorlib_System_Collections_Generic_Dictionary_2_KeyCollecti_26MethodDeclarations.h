﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_24MethodDeclarations.h"
#define KeyCollection__ctor_m18057(__this, ___dictionary, method) (( void (*) (KeyCollection_t2756 *, Dictionary_2_t227 *, const MethodInfo*))KeyCollection__ctor_m17964_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18058(__this, ___item, method) (( void (*) (KeyCollection_t2756 *, InputField_t226 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17965_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18059(__this, method) (( void (*) (KeyCollection_t2756 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17966_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18060(__this, ___item, method) (( bool (*) (KeyCollection_t2756 *, InputField_t226 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17967_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18061(__this, ___item, method) (( bool (*) (KeyCollection_t2756 *, InputField_t226 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17968_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18062(__this, method) (( Object_t* (*) (KeyCollection_t2756 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17969_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m18063(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2756 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m17970_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18064(__this, method) (( Object_t * (*) (KeyCollection_t2756 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18065(__this, method) (( bool (*) (KeyCollection_t2756 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17972_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18066(__this, method) (( bool (*) (KeyCollection_t2756 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17973_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m18067(__this, method) (( Object_t * (*) (KeyCollection_t2756 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m17974_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m18068(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2756 *, InputFieldU5BU5D_t2735*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m17975_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::GetEnumerator()
#define KeyCollection_GetEnumerator_m18069(__this, method) (( Enumerator_t3670  (*) (KeyCollection_t2756 *, const MethodInfo*))KeyCollection_GetEnumerator_m17976_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::get_Count()
#define KeyCollection_get_Count_m18070(__this, method) (( int32_t (*) (KeyCollection_t2756 *, const MethodInfo*))KeyCollection_get_Count_m17977_gshared)(__this, method)
