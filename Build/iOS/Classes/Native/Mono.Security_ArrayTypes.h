﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct  BigIntegerU5BU5D_t1560  : public Array_t
{
};
struct BigIntegerU5BU5D_t1560_StaticFields{
};
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
struct  ClientCertificateTypeU5BU5D_t1534  : public Array_t
{
};
