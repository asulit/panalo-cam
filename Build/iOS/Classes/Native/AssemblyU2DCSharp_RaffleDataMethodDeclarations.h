﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RaffleData
struct RaffleData_t206;
// Common.Signal.ISignalParameters
struct ISignalParameters_t115;
// ListenRaffle
struct ListenRaffle_t331;
// ListenRaffleResult
struct ListenRaffleResult_t332;
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"

// System.Void RaffleData::.ctor()
extern "C" void RaffleData__ctor_m722 (RaffleData_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::Awake()
extern "C" void RaffleData_Awake_m723 (RaffleData_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::Start()
extern "C" void RaffleData_Start_m724 (RaffleData_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::OnEnable()
extern "C" void RaffleData_OnEnable_m725 (RaffleData_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::OnDisable()
extern "C" void RaffleData_OnDisable_m726 (RaffleData_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::OnRaffleWinners(Common.Signal.ISignalParameters)
extern "C" void RaffleData_OnRaffleWinners_m727 (RaffleData_t206 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::LoadCache()
extern "C" void RaffleData_LoadCache_m728 (RaffleData_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::CachePlayerData()
extern "C" void RaffleData_CachePlayerData_m729 (RaffleData_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::CacheRaffles()
extern "C" void RaffleData_CacheRaffles_m730 (RaffleData_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::RecordRaffleIdsToListen(System.Int32)
extern "C" void RaffleData_RecordRaffleIdsToListen_m731 (RaffleData_t206 * __this, int32_t ___raffle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::RaffleDone(System.Int32)
extern "C" void RaffleData_RaffleDone_m732 (RaffleData_t206 * __this, int32_t ___raffleId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ERaffleResult RaffleData::RaffleStatus(System.Int32)
extern "C" int32_t RaffleData_RaffleStatus_m733 (RaffleData_t206 * __this, int32_t ___raffleId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::ListenToRaffle(ListenRaffle)
extern "C" void RaffleData_ListenToRaffle_m734 (RaffleData_t206 * __this, ListenRaffle_t331 * ___listen, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::GetRaffleResult(ListenRaffleResult)
extern "C" void RaffleData_GetRaffleResult_m735 (RaffleData_t206 * __this, ListenRaffleResult_t332 * ___listen, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RaffleData::get_HasData()
extern "C" bool RaffleData_get_HasData_m736 (RaffleData_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RaffleData::set_HasData(System.Boolean)
extern "C" void RaffleData_set_HasData_m737 (RaffleData_t206 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
