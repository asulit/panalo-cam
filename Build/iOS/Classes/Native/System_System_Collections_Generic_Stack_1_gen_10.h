﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Vector4>[]
struct List_1U5BU5D_t3093;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct  Stack_1_t3092  : public Object_t
{
	// T[] System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>::_array
	List_1U5BU5D_t3093* ____array_1;
	// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>::_version
	int32_t ____version_3;
};
