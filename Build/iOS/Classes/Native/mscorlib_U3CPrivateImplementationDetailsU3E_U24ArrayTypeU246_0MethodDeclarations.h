﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU24640_t2403_marshal(const U24ArrayTypeU24640_t2403& unmarshaled, U24ArrayTypeU24640_t2403_marshaled& marshaled);
extern "C" void U24ArrayTypeU24640_t2403_marshal_back(const U24ArrayTypeU24640_t2403_marshaled& marshaled, U24ArrayTypeU24640_t2403& unmarshaled);
extern "C" void U24ArrayTypeU24640_t2403_marshal_cleanup(U24ArrayTypeU24640_t2403_marshaled& marshaled);
