﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.Action.RotateAction
struct RotateAction_t40;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t35;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void Common.Fsm.Action.RotateAction::.ctor(Common.Fsm.FsmState)
extern "C" void RotateAction__ctor_m129 (RotateAction_t40 * __this, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.RotateAction::.ctor(Common.Fsm.FsmState,System.String)
extern "C" void RotateAction__ctor_m130 (RotateAction_t40 * __this, Object_t * ___owner, String_t* ___timeReferenceName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.RotateAction::Init(UnityEngine.Transform,UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single,System.String)
extern "C" void RotateAction_Init_m131 (RotateAction_t40 * __this, Transform_t35 * ___transform, Quaternion_t41  ___quatFrom, Quaternion_t41  ___quatTo, float ___duration, String_t* ___finishEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.RotateAction::OnEnter()
extern "C" void RotateAction_OnEnter_m132 (RotateAction_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.RotateAction::OnUpdate()
extern "C" void RotateAction_OnUpdate_m133 (RotateAction_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.RotateAction::Finish()
extern "C" void RotateAction_Finish_m134 (RotateAction_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
