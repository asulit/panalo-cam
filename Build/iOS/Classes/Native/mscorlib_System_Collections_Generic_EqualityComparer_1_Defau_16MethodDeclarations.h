﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>
struct DefaultComparer_t3270;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C" void DefaultComparer__ctor_m25187_gshared (DefaultComparer_t3270 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m25187(__this, method) (( void (*) (DefaultComparer_t3270 *, const MethodInfo*))DefaultComparer__ctor_m25187_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m25188_gshared (DefaultComparer_t3270 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m25188(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3270 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m25188_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m25189_gshared (DefaultComparer_t3270 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m25189(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3270 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m25189_gshared)(__this, ___x, ___y, method)
