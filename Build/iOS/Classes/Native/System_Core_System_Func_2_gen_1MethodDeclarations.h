﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>,System.Boolean>
struct Func_2_t219;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C" void Func_2__ctor_m1685_gshared (Func_2_t219 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Func_2__ctor_m1685(__this, ___object, ___method, method) (( void (*) (Func_2_t219 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1685_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>,System.Boolean>::Invoke(T)
extern "C" bool Func_2_Invoke_m17852_gshared (Func_2_t219 * __this, KeyValuePair_2_t352  ___arg1, const MethodInfo* method);
#define Func_2_Invoke_m17852(__this, ___arg1, method) (( bool (*) (Func_2_t219 *, KeyValuePair_2_t352 , const MethodInfo*))Func_2_Invoke_m17852_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Func_2_BeginInvoke_m17853_gshared (Func_2_t219 * __this, KeyValuePair_2_t352  ___arg1, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Func_2_BeginInvoke_m17853(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t219 *, KeyValuePair_2_t352 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m17853_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C" bool Func_2_EndInvoke_m17854_gshared (Func_2_t219 * __this, Object_t * ___result, const MethodInfo* method);
#define Func_2_EndInvoke_m17854(__this, ___result, method) (( bool (*) (Func_2_t219 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m17854_gshared)(__this, ___result, method)
