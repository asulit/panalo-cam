﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t998;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.UnhandledExceptionEventHandler
struct  UnhandledExceptionEventHandler_t1016  : public MulticastDelegate_t28
{
};
