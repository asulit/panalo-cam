﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_90.h"
// Vuforia.VuforiaManagerImpl/SurfaceData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__9.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26080_gshared (InternalEnumerator_1_t3320 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26080(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3320 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26080_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26081_gshared (InternalEnumerator_1_t3320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26081(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3320 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26081_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26082_gshared (InternalEnumerator_1_t3320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26082(__this, method) (( void (*) (InternalEnumerator_1_t3320 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26082_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26083_gshared (InternalEnumerator_1_t3320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26083(__this, method) (( bool (*) (InternalEnumerator_1_t3320 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26083_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::get_Current()
extern "C" SurfaceData_t1143  InternalEnumerator_1_get_Current_m26084_gshared (InternalEnumerator_1_t3320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26084(__this, method) (( SurfaceData_t1143  (*) (InternalEnumerator_1_t3320 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26084_gshared)(__this, method)
