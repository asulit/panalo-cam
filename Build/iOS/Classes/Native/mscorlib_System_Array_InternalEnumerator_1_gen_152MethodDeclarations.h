﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_152.h"
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29859_gshared (InternalEnumerator_1_t3594 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29859(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3594 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29859_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860_gshared (InternalEnumerator_1_t3594 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3594 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29861_gshared (InternalEnumerator_1_t3594 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29861(__this, method) (( void (*) (InternalEnumerator_1_t3594 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29861_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29862_gshared (InternalEnumerator_1_t3594 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29862(__this, method) (( bool (*) (InternalEnumerator_1_t3594 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29862_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
extern "C" uint8_t InternalEnumerator_1_get_Current_m29863_gshared (InternalEnumerator_1_t3594 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29863(__this, method) (( uint8_t (*) (InternalEnumerator_1_t3594 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29863_gshared)(__this, method)
