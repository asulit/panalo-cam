﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Query.QueryResultResolver
struct QueryResultResolver_t328;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,Common.Query.QueryResultResolver,System.Collections.DictionaryEntry>
struct  Transform_1_t2592  : public MulticastDelegate_t28
{
};
