﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_30.h"
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m24159_gshared (KeyValuePair_2_t3183 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m24159(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3183 *, Object_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m24159_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m24160_gshared (KeyValuePair_2_t3183 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m24160(__this, method) (( Object_t * (*) (KeyValuePair_2_t3183 *, const MethodInfo*))KeyValuePair_2_get_Key_m24160_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m24161_gshared (KeyValuePair_2_t3183 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m24161(__this, ___value, method) (( void (*) (KeyValuePair_2_t3183 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m24161_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m24162_gshared (KeyValuePair_2_t3183 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m24162(__this, method) (( int32_t (*) (KeyValuePair_2_t3183 *, const MethodInfo*))KeyValuePair_2_get_Value_m24162_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m24163_gshared (KeyValuePair_2_t3183 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m24163(__this, ___value, method) (( void (*) (KeyValuePair_2_t3183 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m24163_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m24164_gshared (KeyValuePair_2_t3183 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m24164(__this, method) (( String_t* (*) (KeyValuePair_2_t3183 *, const MethodInfo*))KeyValuePair_2_ToString_m24164_gshared)(__this, method)
