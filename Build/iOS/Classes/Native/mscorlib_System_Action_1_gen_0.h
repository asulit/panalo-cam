﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// Vuforia.VuforiaUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"
// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct  Action_1_t455  : public MulticastDelegate_t28
{
};
