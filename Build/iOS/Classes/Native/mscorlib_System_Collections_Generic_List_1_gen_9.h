﻿#pragma once
#include <stdint.h>
// Vuforia.CameraDevice/FocusMode[]
struct FocusModeU5BU5D_t2723;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>
struct  List_1_t214  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::_items
	FocusModeU5BU5D_t2723* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::_version
	int32_t ____version_3;
};
struct List_1_t214_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>::EmptyArray
	FocusModeU5BU5D_t2723* ___EmptyArray_4;
};
