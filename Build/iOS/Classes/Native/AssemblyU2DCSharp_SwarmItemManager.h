﻿#pragma once
#include <stdint.h>
// SwarmItem
struct SwarmItem_t124;
// UnityEngine.GameObject
struct GameObject_t155;
// UnityEngine.Transform
struct Transform_t35;
// SwarmItemManager/PrefabItemLists[]
struct PrefabItemListsU5BU5D_t156;
// SwarmItemManager/PrefabItem[]
struct PrefabItemU5BU5D_t157;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SwarmItemManager
struct  SwarmItemManager_t111  : public MonoBehaviour_t5
{
	// SwarmItem SwarmItemManager::_item
	SwarmItem_t124 * ____item_2;
	// UnityEngine.GameObject SwarmItemManager::_go
	GameObject_t155 * ____go_3;
	// UnityEngine.Transform SwarmItemManager::_transform
	Transform_t35 * ____transform_4;
	// UnityEngine.Transform SwarmItemManager::_activeParentTransform
	Transform_t35 * ____activeParentTransform_5;
	// UnityEngine.Transform SwarmItemManager::_inactiveParentTransform
	Transform_t35 * ____inactiveParentTransform_6;
	// System.Int32 SwarmItemManager::_itemCount
	int32_t ____itemCount_7;
	// SwarmItemManager/PrefabItemLists[] SwarmItemManager::_prefabItemLists
	PrefabItemListsU5BU5D_t156* ____prefabItemLists_8;
	// System.Boolean SwarmItemManager::debugEvents
	bool ___debugEvents_9;
	// SwarmItemManager/PrefabItem[] SwarmItemManager::itemPrefabs
	PrefabItemU5BU5D_t157* ___itemPrefabs_10;
};
