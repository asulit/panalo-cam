﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.MeshCollider
struct MeshCollider_t866;
// UnityEngine.Mesh
struct Mesh_t104;

// System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
extern "C" void MeshCollider_set_sharedMesh_m4452 (MeshCollider_t866 * __this, Mesh_t104 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
