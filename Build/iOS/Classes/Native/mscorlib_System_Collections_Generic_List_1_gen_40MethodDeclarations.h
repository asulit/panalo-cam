﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t679;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1337;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3626;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3722;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t3070;
// System.Int32[]
struct Int32U5BU5D_t401;
// System.Predicate`1<System.Int32>
struct Predicate_1_t3073;
// System.Comparison`1<System.Int32>
struct Comparison_1_t3077;
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"

// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C" void List_1__ctor_m7331_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1__ctor_m7331(__this, method) (( void (*) (List_1_t679 *, const MethodInfo*))List_1__ctor_m7331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m7283_gshared (List_1_t679 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m7283(__this, ___collection, method) (( void (*) (List_1_t679 *, Object_t*, const MethodInfo*))List_1__ctor_m7283_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern "C" void List_1__ctor_m22704_gshared (List_1_t679 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m22704(__this, ___capacity, method) (( void (*) (List_1_t679 *, int32_t, const MethodInfo*))List_1__ctor_m22704_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C" void List_1__cctor_m22705_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m22705(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m22705_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22706_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22706(__this, method) (( Object_t* (*) (List_1_t679 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22706_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22707_gshared (List_1_t679 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m22707(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t679 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m22707_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m22708_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22708(__this, method) (( Object_t * (*) (List_1_t679 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m22708_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m22709_gshared (List_1_t679 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m22709(__this, ___item, method) (( int32_t (*) (List_1_t679 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m22709_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m22710_gshared (List_1_t679 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m22710(__this, ___item, method) (( bool (*) (List_1_t679 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m22710_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m22711_gshared (List_1_t679 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m22711(__this, ___item, method) (( int32_t (*) (List_1_t679 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m22711_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m22712_gshared (List_1_t679 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m22712(__this, ___index, ___item, method) (( void (*) (List_1_t679 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m22712_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m22713_gshared (List_1_t679 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m22713(__this, ___item, method) (( void (*) (List_1_t679 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m22713_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22714_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22714(__this, method) (( bool (*) (List_1_t679 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22714_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m22715_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22715(__this, method) (( bool (*) (List_1_t679 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m22715_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m22716_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m22716(__this, method) (( Object_t * (*) (List_1_t679 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m22716_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m22717_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m22717(__this, method) (( bool (*) (List_1_t679 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m22717_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m22718_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m22718(__this, method) (( bool (*) (List_1_t679 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m22718_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m22719_gshared (List_1_t679 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m22719(__this, ___index, method) (( Object_t * (*) (List_1_t679 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m22719_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m22720_gshared (List_1_t679 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m22720(__this, ___index, ___value, method) (( void (*) (List_1_t679 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m22720_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C" void List_1_Add_m22721_gshared (List_1_t679 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m22721(__this, ___item, method) (( void (*) (List_1_t679 *, int32_t, const MethodInfo*))List_1_Add_m22721_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m22722_gshared (List_1_t679 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m22722(__this, ___newCount, method) (( void (*) (List_1_t679 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m22722_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m22723_gshared (List_1_t679 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m22723(__this, ___idx, ___count, method) (( void (*) (List_1_t679 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m22723_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m22724_gshared (List_1_t679 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m22724(__this, ___collection, method) (( void (*) (List_1_t679 *, Object_t*, const MethodInfo*))List_1_AddCollection_m22724_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m22725_gshared (List_1_t679 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m22725(__this, ___enumerable, method) (( void (*) (List_1_t679 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m22725_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m3792_gshared (List_1_t679 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m3792(__this, ___collection, method) (( void (*) (List_1_t679 *, Object_t*, const MethodInfo*))List_1_AddRange_m3792_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Int32>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3070 * List_1_AsReadOnly_m22726_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m22726(__this, method) (( ReadOnlyCollection_1_t3070 * (*) (List_1_t679 *, const MethodInfo*))List_1_AsReadOnly_m22726_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C" void List_1_Clear_m22727_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_Clear_m22727(__this, method) (( void (*) (List_1_t679 *, const MethodInfo*))List_1_Clear_m22727_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C" bool List_1_Contains_m22728_gshared (List_1_t679 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m22728(__this, ___item, method) (( bool (*) (List_1_t679 *, int32_t, const MethodInfo*))List_1_Contains_m22728_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m22729_gshared (List_1_t679 * __this, Int32U5BU5D_t401* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m22729(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t679 *, Int32U5BU5D_t401*, int32_t, const MethodInfo*))List_1_CopyTo_m22729_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Int32>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m22730_gshared (List_1_t679 * __this, Predicate_1_t3073 * ___match, const MethodInfo* method);
#define List_1_Find_m22730(__this, ___match, method) (( int32_t (*) (List_1_t679 *, Predicate_1_t3073 *, const MethodInfo*))List_1_Find_m22730_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m22731_gshared (Object_t * __this /* static, unused */, Predicate_1_t3073 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m22731(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3073 *, const MethodInfo*))List_1_CheckMatch_m22731_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m22732_gshared (List_1_t679 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3073 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m22732(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t679 *, int32_t, int32_t, Predicate_1_t3073 *, const MethodInfo*))List_1_GetIndex_m22732_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t1311  List_1_GetEnumerator_m7284_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m7284(__this, method) (( Enumerator_t1311  (*) (List_1_t679 *, const MethodInfo*))List_1_GetEnumerator_m7284_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m22733_gshared (List_1_t679 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m22733(__this, ___item, method) (( int32_t (*) (List_1_t679 *, int32_t, const MethodInfo*))List_1_IndexOf_m22733_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m22734_gshared (List_1_t679 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m22734(__this, ___start, ___delta, method) (( void (*) (List_1_t679 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m22734_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m22735_gshared (List_1_t679 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m22735(__this, ___index, method) (( void (*) (List_1_t679 *, int32_t, const MethodInfo*))List_1_CheckIndex_m22735_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m22736_gshared (List_1_t679 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m22736(__this, ___index, ___item, method) (( void (*) (List_1_t679 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m22736_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m22737_gshared (List_1_t679 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m22737(__this, ___collection, method) (( void (*) (List_1_t679 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m22737_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C" bool List_1_Remove_m22738_gshared (List_1_t679 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m22738(__this, ___item, method) (( bool (*) (List_1_t679 *, int32_t, const MethodInfo*))List_1_Remove_m22738_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m22739_gshared (List_1_t679 * __this, Predicate_1_t3073 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m22739(__this, ___match, method) (( int32_t (*) (List_1_t679 *, Predicate_1_t3073 *, const MethodInfo*))List_1_RemoveAll_m22739_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m22740_gshared (List_1_t679 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m22740(__this, ___index, method) (( void (*) (List_1_t679 *, int32_t, const MethodInfo*))List_1_RemoveAt_m22740_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m22741_gshared (List_1_t679 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m22741(__this, ___index, ___count, method) (( void (*) (List_1_t679 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m22741_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Reverse()
extern "C" void List_1_Reverse_m22742_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_Reverse_m22742(__this, method) (( void (*) (List_1_t679 *, const MethodInfo*))List_1_Reverse_m22742_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort()
extern "C" void List_1_Sort_m22743_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_Sort_m22743(__this, method) (( void (*) (List_1_t679 *, const MethodInfo*))List_1_Sort_m22743_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m22744_gshared (List_1_t679 * __this, Comparison_1_t3077 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m22744(__this, ___comparison, method) (( void (*) (List_1_t679 *, Comparison_1_t3077 *, const MethodInfo*))List_1_Sort_m22744_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C" Int32U5BU5D_t401* List_1_ToArray_m22745_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_ToArray_m22745(__this, method) (( Int32U5BU5D_t401* (*) (List_1_t679 *, const MethodInfo*))List_1_ToArray_m22745_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::TrimExcess()
extern "C" void List_1_TrimExcess_m22746_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m22746(__this, method) (( void (*) (List_1_t679 *, const MethodInfo*))List_1_TrimExcess_m22746_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m22747_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m22747(__this, method) (( int32_t (*) (List_1_t679 *, const MethodInfo*))List_1_get_Capacity_m22747_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m22748_gshared (List_1_t679 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m22748(__this, ___value, method) (( void (*) (List_1_t679 *, int32_t, const MethodInfo*))List_1_set_Capacity_m22748_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C" int32_t List_1_get_Count_m22749_gshared (List_1_t679 * __this, const MethodInfo* method);
#define List_1_get_Count_m22749(__this, method) (( int32_t (*) (List_1_t679 *, const MethodInfo*))List_1_get_Count_m22749_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m22750_gshared (List_1_t679 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m22750(__this, ___index, method) (( int32_t (*) (List_1_t679 *, int32_t, const MethodInfo*))List_1_get_Item_m22750_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m22751_gshared (List_1_t679 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m22751(__this, ___index, ___value, method) (( void (*) (List_1_t679 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m22751_gshared)(__this, ___index, ___value, method)
