﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ShimEnumerator_t3435;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1371;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m28155_gshared (ShimEnumerator_t3435 * __this, Dictionary_2_t1371 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m28155(__this, ___host, method) (( void (*) (ShimEnumerator_t3435 *, Dictionary_2_t1371 *, const MethodInfo*))ShimEnumerator__ctor_m28155_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m28156_gshared (ShimEnumerator_t3435 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m28156(__this, method) (( bool (*) (ShimEnumerator_t3435 *, const MethodInfo*))ShimEnumerator_MoveNext_m28156_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m28157_gshared (ShimEnumerator_t3435 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m28157(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t3435 *, const MethodInfo*))ShimEnumerator_get_Entry_m28157_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m28158_gshared (ShimEnumerator_t3435 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m28158(__this, method) (( Object_t * (*) (ShimEnumerator_t3435 *, const MethodInfo*))ShimEnumerator_get_Key_m28158_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m28159_gshared (ShimEnumerator_t3435 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m28159(__this, method) (( Object_t * (*) (ShimEnumerator_t3435 *, const MethodInfo*))ShimEnumerator_get_Value_m28159_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m28160_gshared (ShimEnumerator_t3435 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m28160(__this, method) (( Object_t * (*) (ShimEnumerator_t3435 *, const MethodInfo*))ShimEnumerator_get_Current_m28160_gshared)(__this, method)
