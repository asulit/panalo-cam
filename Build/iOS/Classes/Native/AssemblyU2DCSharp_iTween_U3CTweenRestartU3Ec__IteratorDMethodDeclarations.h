﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// iTween/<TweenRestart>c__IteratorD
struct U3CTweenRestartU3Ec__IteratorD_t259;
// System.Object
struct Object_t;

// System.Void iTween/<TweenRestart>c__IteratorD::.ctor()
extern "C" void U3CTweenRestartU3Ec__IteratorD__ctor_m911 (U3CTweenRestartU3Ec__IteratorD_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenRestart>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CTweenRestartU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m912 (U3CTweenRestartU3Ec__IteratorD_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenRestart>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CTweenRestartU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m913 (U3CTweenRestartU3Ec__IteratorD_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<TweenRestart>c__IteratorD::MoveNext()
extern "C" bool U3CTweenRestartU3Ec__IteratorD_MoveNext_m914 (U3CTweenRestartU3Ec__IteratorD_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenRestart>c__IteratorD::Dispose()
extern "C" void U3CTweenRestartU3Ec__IteratorD_Dispose_m915 (U3CTweenRestartU3Ec__IteratorD_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenRestart>c__IteratorD::Reset()
extern "C" void U3CTweenRestartU3Ec__IteratorD_Reset_m916 (U3CTweenRestartU3Ec__IteratorD_t259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
