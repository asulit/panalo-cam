﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t3134;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1009;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t3729;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t755;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m23432_gshared (Collection_1_t3134 * __this, const MethodInfo* method);
#define Collection_1__ctor_m23432(__this, method) (( void (*) (Collection_1_t3134 *, const MethodInfo*))Collection_1__ctor_m23432_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23433_gshared (Collection_1_t3134 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23433(__this, method) (( bool (*) (Collection_1_t3134 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23433_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23434_gshared (Collection_1_t3134 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m23434(__this, ___array, ___index, method) (( void (*) (Collection_1_t3134 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m23434_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m23435_gshared (Collection_1_t3134 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m23435(__this, method) (( Object_t * (*) (Collection_1_t3134 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m23435_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m23436_gshared (Collection_1_t3134 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m23436(__this, ___value, method) (( int32_t (*) (Collection_1_t3134 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m23436_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m23437_gshared (Collection_1_t3134 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m23437(__this, ___value, method) (( bool (*) (Collection_1_t3134 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m23437_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m23438_gshared (Collection_1_t3134 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m23438(__this, ___value, method) (( int32_t (*) (Collection_1_t3134 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m23438_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m23439_gshared (Collection_1_t3134 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m23439(__this, ___index, ___value, method) (( void (*) (Collection_1_t3134 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m23439_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m23440_gshared (Collection_1_t3134 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m23440(__this, ___value, method) (( void (*) (Collection_1_t3134 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m23440_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m23441_gshared (Collection_1_t3134 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m23441(__this, method) (( bool (*) (Collection_1_t3134 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m23441_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m23442_gshared (Collection_1_t3134 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m23442(__this, method) (( Object_t * (*) (Collection_1_t3134 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m23442_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m23443_gshared (Collection_1_t3134 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m23443(__this, method) (( bool (*) (Collection_1_t3134 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m23443_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m23444_gshared (Collection_1_t3134 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m23444(__this, method) (( bool (*) (Collection_1_t3134 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m23444_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m23445_gshared (Collection_1_t3134 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m23445(__this, ___index, method) (( Object_t * (*) (Collection_1_t3134 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m23445_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m23446_gshared (Collection_1_t3134 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m23446(__this, ___index, ___value, method) (( void (*) (Collection_1_t3134 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m23446_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m23447_gshared (Collection_1_t3134 * __this, UICharInfo_t754  ___item, const MethodInfo* method);
#define Collection_1_Add_m23447(__this, ___item, method) (( void (*) (Collection_1_t3134 *, UICharInfo_t754 , const MethodInfo*))Collection_1_Add_m23447_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m23448_gshared (Collection_1_t3134 * __this, const MethodInfo* method);
#define Collection_1_Clear_m23448(__this, method) (( void (*) (Collection_1_t3134 *, const MethodInfo*))Collection_1_Clear_m23448_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m23449_gshared (Collection_1_t3134 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m23449(__this, method) (( void (*) (Collection_1_t3134 *, const MethodInfo*))Collection_1_ClearItems_m23449_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m23450_gshared (Collection_1_t3134 * __this, UICharInfo_t754  ___item, const MethodInfo* method);
#define Collection_1_Contains_m23450(__this, ___item, method) (( bool (*) (Collection_1_t3134 *, UICharInfo_t754 , const MethodInfo*))Collection_1_Contains_m23450_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m23451_gshared (Collection_1_t3134 * __this, UICharInfoU5BU5D_t1009* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m23451(__this, ___array, ___index, method) (( void (*) (Collection_1_t3134 *, UICharInfoU5BU5D_t1009*, int32_t, const MethodInfo*))Collection_1_CopyTo_m23451_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m23452_gshared (Collection_1_t3134 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m23452(__this, method) (( Object_t* (*) (Collection_1_t3134 *, const MethodInfo*))Collection_1_GetEnumerator_m23452_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m23453_gshared (Collection_1_t3134 * __this, UICharInfo_t754  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m23453(__this, ___item, method) (( int32_t (*) (Collection_1_t3134 *, UICharInfo_t754 , const MethodInfo*))Collection_1_IndexOf_m23453_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m23454_gshared (Collection_1_t3134 * __this, int32_t ___index, UICharInfo_t754  ___item, const MethodInfo* method);
#define Collection_1_Insert_m23454(__this, ___index, ___item, method) (( void (*) (Collection_1_t3134 *, int32_t, UICharInfo_t754 , const MethodInfo*))Collection_1_Insert_m23454_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m23455_gshared (Collection_1_t3134 * __this, int32_t ___index, UICharInfo_t754  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m23455(__this, ___index, ___item, method) (( void (*) (Collection_1_t3134 *, int32_t, UICharInfo_t754 , const MethodInfo*))Collection_1_InsertItem_m23455_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m23456_gshared (Collection_1_t3134 * __this, UICharInfo_t754  ___item, const MethodInfo* method);
#define Collection_1_Remove_m23456(__this, ___item, method) (( bool (*) (Collection_1_t3134 *, UICharInfo_t754 , const MethodInfo*))Collection_1_Remove_m23456_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m23457_gshared (Collection_1_t3134 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m23457(__this, ___index, method) (( void (*) (Collection_1_t3134 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m23457_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m23458_gshared (Collection_1_t3134 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m23458(__this, ___index, method) (( void (*) (Collection_1_t3134 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m23458_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m23459_gshared (Collection_1_t3134 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m23459(__this, method) (( int32_t (*) (Collection_1_t3134 *, const MethodInfo*))Collection_1_get_Count_m23459_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t754  Collection_1_get_Item_m23460_gshared (Collection_1_t3134 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m23460(__this, ___index, method) (( UICharInfo_t754  (*) (Collection_1_t3134 *, int32_t, const MethodInfo*))Collection_1_get_Item_m23460_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m23461_gshared (Collection_1_t3134 * __this, int32_t ___index, UICharInfo_t754  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m23461(__this, ___index, ___value, method) (( void (*) (Collection_1_t3134 *, int32_t, UICharInfo_t754 , const MethodInfo*))Collection_1_set_Item_m23461_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m23462_gshared (Collection_1_t3134 * __this, int32_t ___index, UICharInfo_t754  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m23462(__this, ___index, ___item, method) (( void (*) (Collection_1_t3134 *, int32_t, UICharInfo_t754 , const MethodInfo*))Collection_1_SetItem_m23462_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m23463_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m23463(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m23463_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t754  Collection_1_ConvertItem_m23464_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m23464(__this /* static, unused */, ___item, method) (( UICharInfo_t754  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m23464_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m23465_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m23465(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m23465_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m23466_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m23466(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m23466_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m23467_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m23467(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m23467_gshared)(__this /* static, unused */, ___list, method)
