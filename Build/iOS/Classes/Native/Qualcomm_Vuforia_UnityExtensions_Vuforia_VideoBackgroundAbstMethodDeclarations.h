﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t315;

// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
extern "C" void VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m7101 (VideoBackgroundAbstractBehaviour_t315 * __this, bool ___disable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::SetStereoDepth(System.Single)
extern "C" void VideoBackgroundAbstractBehaviour_SetStereoDepth_m7102 (VideoBackgroundAbstractBehaviour_t315 * __this, float ___depth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ApplyStereoDepthToMatrices()
extern "C" void VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m7103 (VideoBackgroundAbstractBehaviour_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::RenderOnUpdate()
extern "C" void VideoBackgroundAbstractBehaviour_RenderOnUpdate_m7104 (VideoBackgroundAbstractBehaviour_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Awake()
extern "C" void VideoBackgroundAbstractBehaviour_Awake_m7105 (VideoBackgroundAbstractBehaviour_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Start()
extern "C" void VideoBackgroundAbstractBehaviour_Start_m7106 (VideoBackgroundAbstractBehaviour_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPreRender()
extern "C" void VideoBackgroundAbstractBehaviour_OnPreRender_m7107 (VideoBackgroundAbstractBehaviour_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPostRender()
extern "C" void VideoBackgroundAbstractBehaviour_OnPostRender_m7108 (VideoBackgroundAbstractBehaviour_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnDestroy()
extern "C" void VideoBackgroundAbstractBehaviour_OnDestroy_m7109 (VideoBackgroundAbstractBehaviour_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
extern "C" void VideoBackgroundAbstractBehaviour__ctor_m1875 (VideoBackgroundAbstractBehaviour_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
