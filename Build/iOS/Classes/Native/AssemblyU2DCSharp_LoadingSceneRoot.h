﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Window
struct Window_t223;
// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>
struct Dictionary_2_t208;
// Common.Fsm.Fsm
struct Fsm_t47;
// Common.Fsm.FsmDelegateAction/FsmActionRoutine
struct FsmActionRoutine_t27;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>,System.Boolean>
struct Func_2_t219;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// LoadingSceneRoot
struct  LoadingSceneRoot_t218  : public MonoBehaviour_t5
{
	// Window LoadingSceneRoot::window
	Window_t223 * ___window_6;
	// Window LoadingSceneRoot::errorWindow
	Window_t223 * ___errorWindow_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult> LoadingSceneRoot::results
	Dictionary_2_t208 * ___results_8;
	// Common.Fsm.Fsm LoadingSceneRoot::fsm
	Fsm_t47 * ___fsm_9;
};
struct LoadingSceneRoot_t218_StaticFields{
	// Common.Fsm.FsmDelegateAction/FsmActionRoutine LoadingSceneRoot::<>f__am$cache4
	FsmActionRoutine_t27 * ___U3CU3Ef__amU24cache4_10;
	// Common.Fsm.FsmDelegateAction/FsmActionRoutine LoadingSceneRoot::<>f__am$cache5
	FsmActionRoutine_t27 * ___U3CU3Ef__amU24cache5_11;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>,System.Boolean> LoadingSceneRoot::<>f__am$cache6
	Func_2_t219 * ___U3CU3Ef__amU24cache6_12;
};
