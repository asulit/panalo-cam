﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_47.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19806_gshared (InternalEnumerator_1_t2872 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19806(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2872 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19806_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19807_gshared (InternalEnumerator_1_t2872 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19807(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2872 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19807_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19808_gshared (InternalEnumerator_1_t2872 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19808(__this, method) (( void (*) (InternalEnumerator_1_t2872 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19808_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19809_gshared (InternalEnumerator_1_t2872 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19809(__this, method) (( bool (*) (InternalEnumerator_1_t2872 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19809_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t2871  InternalEnumerator_1_get_Current_m19810_gshared (InternalEnumerator_1_t2872 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19810(__this, method) (( KeyValuePair_2_t2871  (*) (InternalEnumerator_1_t2872 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19810_gshared)(__this, method)
