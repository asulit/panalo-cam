﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.String,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_21MethodDeclarations.h"
#define Transform_1__ctor_m17515(__this, ___object, ___method, method) (( void (*) (Transform_1_t2686 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m17493_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.String,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m17516(__this, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Transform_1_t2686 *, int32_t, String_t*, const MethodInfo*))Transform_1_Invoke_m17494_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.String,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m17517(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2686 *, int32_t, String_t*, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m17495_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.String,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m17518(__this, ___result, method) (( DictionaryEntry_t451  (*) (Transform_1_t2686 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m17496_gshared)(__this, ___result, method)
