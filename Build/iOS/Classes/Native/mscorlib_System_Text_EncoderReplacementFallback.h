﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Text.EncoderFallback
#include "mscorlib_System_Text_EncoderFallback.h"
// System.Text.EncoderReplacementFallback
struct  EncoderReplacementFallback_t2276  : public EncoderFallback_t2272
{
	// System.String System.Text.EncoderReplacementFallback::replacement
	String_t* ___replacement_3;
};
