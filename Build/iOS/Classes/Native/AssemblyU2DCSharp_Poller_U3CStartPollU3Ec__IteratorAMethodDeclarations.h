﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Poller/<StartPoll>c__IteratorA
struct U3CStartPollU3Ec__IteratorA_t239;
// System.Object
struct Object_t;

// System.Void Poller/<StartPoll>c__IteratorA::.ctor()
extern "C" void U3CStartPollU3Ec__IteratorA__ctor_m851 (U3CStartPollU3Ec__IteratorA_t239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Poller/<StartPoll>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartPollU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m852 (U3CStartPollU3Ec__IteratorA_t239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Poller/<StartPoll>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartPollU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m853 (U3CStartPollU3Ec__IteratorA_t239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Poller/<StartPoll>c__IteratorA::MoveNext()
extern "C" bool U3CStartPollU3Ec__IteratorA_MoveNext_m854 (U3CStartPollU3Ec__IteratorA_t239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Poller/<StartPoll>c__IteratorA::Dispose()
extern "C" void U3CStartPollU3Ec__IteratorA_Dispose_m855 (U3CStartPollU3Ec__IteratorA_t239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Poller/<StartPoll>c__IteratorA::Reset()
extern "C" void U3CStartPollU3Ec__IteratorA_Reset_m856 (U3CStartPollU3Ec__IteratorA_t239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Poller/<StartPoll>c__IteratorA::<>m__1D(System.Int32)
extern "C" void U3CStartPollU3Ec__IteratorA_U3CU3Em__1D_m857 (U3CStartPollU3Ec__IteratorA_t239 * __this, int32_t ___raffleId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
