﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Collections.Generic.Queue`1<Common.Logger.Log>
struct Queue_1_t76;
// System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>
struct Queue_1_t77;
// CountdownTimer
struct CountdownTimer_t13;
// Common.Logger.Logger
struct Logger_t75;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Logger.Logger
struct  Logger_t75  : public Object_t
{
	// System.String Common.Logger.Logger::name
	String_t* ___name_0;
	// System.String Common.Logger.Logger::logFilePath
	String_t* ___logFilePath_1;
	// System.Collections.Generic.Queue`1<Common.Logger.Log> Common.Logger.Logger::bufferQueue
	Queue_1_t76 * ___bufferQueue_2;
	// System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>> Common.Logger.Logger::writeQueue
	Queue_1_t77 * ___writeQueue_3;
	// System.Boolean Common.Logger.Logger::currentlyWriting
	bool ___currentlyWriting_4;
	// CountdownTimer Common.Logger.Logger::writeTimer
	CountdownTimer_t13 * ___writeTimer_6;
};
struct Logger_t75_StaticFields{
	// System.Int32 Common.Logger.Logger::WRITE_TIME_INTERVAL
	int32_t ___WRITE_TIME_INTERVAL_5;
	// Common.Logger.Logger Common.Logger.Logger::ONLY_INSTANCE
	Logger_t75 * ___ONLY_INSTANCE_7;
};
