﻿#pragma once
#include <stdint.h>
// System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>
struct Func_2_t174;
// UnityThreading.ThreadBase
#include "AssemblyU2DCSharp_UnityThreading_ThreadBase.h"
// UnityThreading.EnumeratableActionThread
struct  EnumeratableActionThread_t173  : public ThreadBase_t169
{
	// System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator> UnityThreading.EnumeratableActionThread::enumeratableAction
	Func_2_t174 * ___enumeratableAction_4;
};
