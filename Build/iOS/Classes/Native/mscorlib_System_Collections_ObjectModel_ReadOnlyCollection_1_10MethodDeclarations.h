﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>
struct ReadOnlyCollection_1_t2772;
// System.Collections.Generic.IList`1<AnimatorFrame>
struct IList_1_t2773;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// AnimatorFrame[]
struct AnimatorFrameU5BU5D_t2770;
// System.Collections.Generic.IEnumerator`1<AnimatorFrame>
struct IEnumerator_1_t3679;
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m18349_gshared (ReadOnlyCollection_1_t2772 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m18349(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2772 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m18349_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m18350_gshared (ReadOnlyCollection_1_t2772 * __this, AnimatorFrame_t235  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m18350(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2772 *, AnimatorFrame_t235 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m18350_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m18351_gshared (ReadOnlyCollection_1_t2772 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m18351(__this, method) (( void (*) (ReadOnlyCollection_1_t2772 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m18351_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m18352_gshared (ReadOnlyCollection_1_t2772 * __this, int32_t ___index, AnimatorFrame_t235  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m18352(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2772 *, int32_t, AnimatorFrame_t235 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m18352_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m18353_gshared (ReadOnlyCollection_1_t2772 * __this, AnimatorFrame_t235  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m18353(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2772 *, AnimatorFrame_t235 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m18353_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m18354_gshared (ReadOnlyCollection_1_t2772 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m18354(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2772 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m18354_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" AnimatorFrame_t235  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m18355_gshared (ReadOnlyCollection_1_t2772 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m18355(__this, ___index, method) (( AnimatorFrame_t235  (*) (ReadOnlyCollection_1_t2772 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m18355_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m18356_gshared (ReadOnlyCollection_1_t2772 * __this, int32_t ___index, AnimatorFrame_t235  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m18356(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2772 *, int32_t, AnimatorFrame_t235 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m18356_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18357_gshared (ReadOnlyCollection_1_t2772 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18357(__this, method) (( bool (*) (ReadOnlyCollection_1_t2772 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18357_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m18358_gshared (ReadOnlyCollection_1_t2772 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m18358(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2772 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m18358_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m18359_gshared (ReadOnlyCollection_1_t2772 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m18359(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2772 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m18359_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m18360_gshared (ReadOnlyCollection_1_t2772 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m18360(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2772 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m18360_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m18361_gshared (ReadOnlyCollection_1_t2772 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m18361(__this, method) (( void (*) (ReadOnlyCollection_1_t2772 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m18361_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m18362_gshared (ReadOnlyCollection_1_t2772 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m18362(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2772 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m18362_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m18363_gshared (ReadOnlyCollection_1_t2772 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m18363(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2772 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m18363_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m18364_gshared (ReadOnlyCollection_1_t2772 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m18364(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2772 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m18364_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m18365_gshared (ReadOnlyCollection_1_t2772 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m18365(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2772 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m18365_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m18366_gshared (ReadOnlyCollection_1_t2772 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m18366(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2772 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m18366_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m18367_gshared (ReadOnlyCollection_1_t2772 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m18367(__this, method) (( bool (*) (ReadOnlyCollection_1_t2772 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m18367_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m18368_gshared (ReadOnlyCollection_1_t2772 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m18368(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2772 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m18368_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m18369_gshared (ReadOnlyCollection_1_t2772 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m18369(__this, method) (( bool (*) (ReadOnlyCollection_1_t2772 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m18369_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m18370_gshared (ReadOnlyCollection_1_t2772 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m18370(__this, method) (( bool (*) (ReadOnlyCollection_1_t2772 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m18370_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m18371_gshared (ReadOnlyCollection_1_t2772 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m18371(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2772 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m18371_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m18372_gshared (ReadOnlyCollection_1_t2772 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m18372(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2772 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m18372_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m18373_gshared (ReadOnlyCollection_1_t2772 * __this, AnimatorFrame_t235  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m18373(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2772 *, AnimatorFrame_t235 , const MethodInfo*))ReadOnlyCollection_1_Contains_m18373_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m18374_gshared (ReadOnlyCollection_1_t2772 * __this, AnimatorFrameU5BU5D_t2770* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m18374(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2772 *, AnimatorFrameU5BU5D_t2770*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m18374_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m18375_gshared (ReadOnlyCollection_1_t2772 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m18375(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2772 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m18375_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m18376_gshared (ReadOnlyCollection_1_t2772 * __this, AnimatorFrame_t235  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m18376(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2772 *, AnimatorFrame_t235 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m18376_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m18377_gshared (ReadOnlyCollection_1_t2772 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m18377(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2772 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m18377_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::get_Item(System.Int32)
extern "C" AnimatorFrame_t235  ReadOnlyCollection_1_get_Item_m18378_gshared (ReadOnlyCollection_1_t2772 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m18378(__this, ___index, method) (( AnimatorFrame_t235  (*) (ReadOnlyCollection_1_t2772 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m18378_gshared)(__this, ___index, method)
