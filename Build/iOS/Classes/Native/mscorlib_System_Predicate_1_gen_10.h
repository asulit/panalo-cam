﻿#pragma once
#include <stdint.h>
// Common.Command
struct Command_t348;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Common.Command>
struct  Predicate_1_t2623  : public MulticastDelegate_t28
{
};
