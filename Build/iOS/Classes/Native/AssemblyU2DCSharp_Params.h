﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// Params
struct  Params_t197  : public Object_t
{
};
struct Params_t197_StaticFields{
	// System.String Params::SCANNED_ID
	String_t* ___SCANNED_ID_0;
	// System.String Params::CAMERA_FOCUS_SETTINGS
	String_t* ___CAMERA_FOCUS_SETTINGS_1;
	// System.String Params::SMS_NUMBER
	String_t* ___SMS_NUMBER_2;
	// System.String Params::MESSAGE
	String_t* ___MESSAGE_3;
	// System.String Params::SHARE_SUBJECT
	String_t* ___SHARE_SUBJECT_4;
	// System.String Params::SHARE_BODY
	String_t* ___SHARE_BODY_5;
	// System.String Params::PROMO_ID
	String_t* ___PROMO_ID_6;
	// System.String Params::RAFFLE_RESULT
	String_t* ___RAFFLE_RESULT_7;
};
