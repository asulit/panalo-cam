﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<EScreens>
struct EqualityComparer_1_t2679;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<EScreens>::.ctor()
extern "C" void EqualityComparer_1__ctor_m17268_gshared (EqualityComparer_1_t2679 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m17268(__this, method) (( void (*) (EqualityComparer_1_t2679 *, const MethodInfo*))EqualityComparer_1__ctor_m17268_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<EScreens>::.cctor()
extern "C" void EqualityComparer_1__cctor_m17269_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m17269(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m17269_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<EScreens>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17270_gshared (EqualityComparer_1_t2679 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17270(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t2679 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17270_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<EScreens>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17271_gshared (EqualityComparer_1_t2679 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17271(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t2679 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17271_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<EScreens>::get_Default()
extern "C" EqualityComparer_1_t2679 * EqualityComparer_1_get_Default_m17272_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m17272(__this /* static, unused */, method) (( EqualityComparer_1_t2679 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m17272_gshared)(__this /* static, unused */, method)
