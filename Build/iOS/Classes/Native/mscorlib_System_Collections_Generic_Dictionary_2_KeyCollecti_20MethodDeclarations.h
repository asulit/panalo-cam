﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
struct Dictionary_2_t2688;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_20.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17449_gshared (Enumerator_t2694 * __this, Dictionary_2_t2688 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m17449(__this, ___host, method) (( void (*) (Enumerator_t2694 *, Dictionary_2_t2688 *, const MethodInfo*))Enumerator__ctor_m17449_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17450_gshared (Enumerator_t2694 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17450(__this, method) (( Object_t * (*) (Enumerator_t2694 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17450_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m17451_gshared (Enumerator_t2694 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17451(__this, method) (( void (*) (Enumerator_t2694 *, const MethodInfo*))Enumerator_Dispose_m17451_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17452_gshared (Enumerator_t2694 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17452(__this, method) (( bool (*) (Enumerator_t2694 *, const MethodInfo*))Enumerator_MoveNext_m17452_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m17453_gshared (Enumerator_t2694 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17453(__this, method) (( int32_t (*) (Enumerator_t2694 *, const MethodInfo*))Enumerator_get_Current_m17453_gshared)(__this, method)
