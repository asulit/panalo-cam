﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"
#define List_1__ctor_m3444(__this, method) (( void (*) (List_1_t566 *, const MethodInfo*))List_1__ctor_m1429_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20309(__this, ___collection, method) (( void (*) (List_1_t566 *, Object_t*, const MethodInfo*))List_1__ctor_m14617_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::.ctor(System.Int32)
#define List_1__ctor_m20310(__this, ___capacity, method) (( void (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1__ctor_m14619_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::.cctor()
#define List_1__cctor_m20311(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14621_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20312(__this, method) (( Object_t* (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14623_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20313(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t566 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m14625_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20314(__this, method) (( Object_t * (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m14627_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20315(__this, ___item, method) (( int32_t (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m14629_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20316(__this, ___item, method) (( bool (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m14631_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20317(__this, ___item, method) (( int32_t (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m14633_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20318(__this, ___index, ___item, method) (( void (*) (List_1_t566 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m14635_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20319(__this, ___item, method) (( void (*) (List_1_t566 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m14637_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20320(__this, method) (( bool (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14639_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20321(__this, method) (( bool (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m14641_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20322(__this, method) (( Object_t * (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m14643_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20323(__this, method) (( bool (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m14645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20324(__this, method) (( bool (*) (List_1_t566 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m14647_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20325(__this, ___index, method) (( Object_t * (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m14649_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20326(__this, ___index, ___value, method) (( void (*) (List_1_t566 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m14651_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::Add(T)
#define List_1_Add_m20327(__this, ___item, method) (( void (*) (List_1_t566 *, DropdownItem_t555 *, const MethodInfo*))List_1_Add_m14653_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20328(__this, ___newCount, method) (( void (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m14655_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m20329(__this, ___idx, ___count, method) (( void (*) (List_1_t566 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m14657_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20330(__this, ___collection, method) (( void (*) (List_1_t566 *, Object_t*, const MethodInfo*))List_1_AddCollection_m14659_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20331(__this, ___enumerable, method) (( void (*) (List_1_t566 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m14661_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20332(__this, ___collection, method) (( void (*) (List_1_t566 *, Object_t*, const MethodInfo*))List_1_AddRange_m14663_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::AsReadOnly()
#define List_1_AsReadOnly_m20333(__this, method) (( ReadOnlyCollection_1_t2907 * (*) (List_1_t566 *, const MethodInfo*))List_1_AsReadOnly_m14665_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::Clear()
#define List_1_Clear_m20334(__this, method) (( void (*) (List_1_t566 *, const MethodInfo*))List_1_Clear_m14667_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::Contains(T)
#define List_1_Contains_m20335(__this, ___item, method) (( bool (*) (List_1_t566 *, DropdownItem_t555 *, const MethodInfo*))List_1_Contains_m14669_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20336(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t566 *, DropdownItemU5BU5D_t2906*, int32_t, const MethodInfo*))List_1_CopyTo_m14671_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::Find(System.Predicate`1<T>)
#define List_1_Find_m20337(__this, ___match, method) (( DropdownItem_t555 * (*) (List_1_t566 *, Predicate_1_t2909 *, const MethodInfo*))List_1_Find_m14673_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20338(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2909 *, const MethodInfo*))List_1_CheckMatch_m14675_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20339(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t566 *, int32_t, int32_t, Predicate_1_t2909 *, const MethodInfo*))List_1_GetIndex_m14677_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::GetEnumerator()
#define List_1_GetEnumerator_m20340(__this, method) (( Enumerator_t2910  (*) (List_1_t566 *, const MethodInfo*))List_1_GetEnumerator_m14679_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::IndexOf(T)
#define List_1_IndexOf_m20341(__this, ___item, method) (( int32_t (*) (List_1_t566 *, DropdownItem_t555 *, const MethodInfo*))List_1_IndexOf_m14681_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20342(__this, ___start, ___delta, method) (( void (*) (List_1_t566 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m14683_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20343(__this, ___index, method) (( void (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_CheckIndex_m14685_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::Insert(System.Int32,T)
#define List_1_Insert_m20344(__this, ___index, ___item, method) (( void (*) (List_1_t566 *, int32_t, DropdownItem_t555 *, const MethodInfo*))List_1_Insert_m14687_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20345(__this, ___collection, method) (( void (*) (List_1_t566 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m14689_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::Remove(T)
#define List_1_Remove_m20346(__this, ___item, method) (( bool (*) (List_1_t566 *, DropdownItem_t555 *, const MethodInfo*))List_1_Remove_m14691_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20347(__this, ___match, method) (( int32_t (*) (List_1_t566 *, Predicate_1_t2909 *, const MethodInfo*))List_1_RemoveAll_m14693_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20348(__this, ___index, method) (( void (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_RemoveAt_m14695_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m20349(__this, ___index, ___count, method) (( void (*) (List_1_t566 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m14697_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::Reverse()
#define List_1_Reverse_m20350(__this, method) (( void (*) (List_1_t566 *, const MethodInfo*))List_1_Reverse_m14699_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::Sort()
#define List_1_Sort_m20351(__this, method) (( void (*) (List_1_t566 *, const MethodInfo*))List_1_Sort_m14701_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20352(__this, ___comparison, method) (( void (*) (List_1_t566 *, Comparison_1_t2911 *, const MethodInfo*))List_1_Sort_m14703_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::ToArray()
#define List_1_ToArray_m20353(__this, method) (( DropdownItemU5BU5D_t2906* (*) (List_1_t566 *, const MethodInfo*))List_1_ToArray_m14705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::TrimExcess()
#define List_1_TrimExcess_m20354(__this, method) (( void (*) (List_1_t566 *, const MethodInfo*))List_1_TrimExcess_m14707_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::get_Capacity()
#define List_1_get_Capacity_m20355(__this, method) (( int32_t (*) (List_1_t566 *, const MethodInfo*))List_1_get_Capacity_m14709_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20356(__this, ___value, method) (( void (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_set_Capacity_m14711_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::get_Count()
#define List_1_get_Count_m20357(__this, method) (( int32_t (*) (List_1_t566 *, const MethodInfo*))List_1_get_Count_m14713_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::get_Item(System.Int32)
#define List_1_get_Item_m20358(__this, ___index, method) (( DropdownItem_t555 * (*) (List_1_t566 *, int32_t, const MethodInfo*))List_1_get_Item_m14715_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>::set_Item(System.Int32,T)
#define List_1_set_Item_m20359(__this, ___index, ___value, method) (( void (*) (List_1_t566 *, int32_t, DropdownItem_t555 *, const MethodInfo*))List_1_set_Item_m14717_gshared)(__this, ___index, ___value, method)
