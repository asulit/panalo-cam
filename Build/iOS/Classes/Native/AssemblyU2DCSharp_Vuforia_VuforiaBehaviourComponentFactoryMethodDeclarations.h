﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t289;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t295;
// UnityEngine.GameObject
struct GameObject_t155;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t319;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t310;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t285;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t293;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t297;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t273;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t327;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t308;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t299;

// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C" void VuforiaBehaviourComponentFactory__ctor_m1239 (VuforiaBehaviourComponentFactory_t289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C" MaskOutAbstractBehaviour_t295 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m1240 (VuforiaBehaviourComponentFactory_t289 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t319 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m1241 (VuforiaBehaviourComponentFactory_t289 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C" TurnOffAbstractBehaviour_t310 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m1242 (VuforiaBehaviourComponentFactory_t289 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t285 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m1243 (VuforiaBehaviourComponentFactory_t289 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern "C" MarkerAbstractBehaviour_t293 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m1244 (VuforiaBehaviourComponentFactory_t289 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t297 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m1245 (VuforiaBehaviourComponentFactory_t289 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t273 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m1246 (VuforiaBehaviourComponentFactory_t289 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t327 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m1247 (VuforiaBehaviourComponentFactory_t289 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C" TextRecoAbstractBehaviour_t308 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m1248 (VuforiaBehaviourComponentFactory_t289 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t299 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m1249 (VuforiaBehaviourComponentFactory_t289 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
