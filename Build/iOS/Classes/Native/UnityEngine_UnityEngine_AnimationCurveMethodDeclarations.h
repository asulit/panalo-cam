﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationCurve
struct AnimationCurve_t884;
struct AnimationCurve_t884_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1007;

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m4514 (AnimationCurve_t884 * __this, KeyframeU5BU5D_t1007* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m4515 (AnimationCurve_t884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m4516 (AnimationCurve_t884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m4517 (AnimationCurve_t884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m4518 (AnimationCurve_t884 * __this, KeyframeU5BU5D_t1007* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void AnimationCurve_t884_marshal(const AnimationCurve_t884& unmarshaled, AnimationCurve_t884_marshaled& marshaled);
extern "C" void AnimationCurve_t884_marshal_back(const AnimationCurve_t884_marshaled& marshaled, AnimationCurve_t884& unmarshaled);
extern "C" void AnimationCurve_t884_marshal_cleanup(AnimationCurve_t884_marshaled& marshaled);
