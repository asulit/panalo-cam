﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AppDomainSetup
struct AppDomainSetup_t2312;

// System.Void System.AppDomainSetup::.ctor()
extern "C" void AppDomainSetup__ctor_m13758 (AppDomainSetup_t2312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
