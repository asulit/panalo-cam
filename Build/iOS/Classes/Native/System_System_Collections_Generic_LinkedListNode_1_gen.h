﻿#pragma once
#include <stdint.h>
// Common.Notification.NotificationHandler
struct NotificationHandler_t347;
// System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>
struct LinkedList_1_t90;
// System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>
struct LinkedListNode_1_t406;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>
struct  LinkedListNode_1_t406  : public Object_t
{
	// T System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>::item
	Object_t * ___item_0;
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>::container
	LinkedList_1_t90 * ___container_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>::forward
	LinkedListNode_1_t406 * ___forward_2;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>::back
	LinkedListNode_1_t406 * ___back_3;
};
