﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.RawImage
struct RawImage_t237;
// System.Collections.Generic.List`1<AnimatorFrame>
struct List_1_t238;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Animator
struct  Animator_t236  : public MonoBehaviour_t5
{
	// System.Boolean Animator::loop
	bool ___loop_2;
	// UnityEngine.UI.RawImage Animator::image
	RawImage_t237 * ___image_3;
	// System.Collections.Generic.List`1<AnimatorFrame> Animator::frames
	List_1_t238 * ___frames_4;
};
