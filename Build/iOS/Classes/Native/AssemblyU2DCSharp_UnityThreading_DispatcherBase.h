﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityThreading.TaskBase>
struct List_1_t159;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t160;
// System.Comparison`1<UnityThreading.TaskBase>
struct Comparison_1_t161;
// System.Object
#include "mscorlib_System_Object.h"
// UnityThreading.TaskSortingSystem
#include "AssemblyU2DCSharp_UnityThreading_TaskSortingSystem.h"
// UnityThreading.DispatcherBase
struct  DispatcherBase_t158  : public Object_t
{
	// System.Collections.Generic.List`1<UnityThreading.TaskBase> UnityThreading.DispatcherBase::taskList
	List_1_t159 * ___taskList_0;
	// System.Threading.ManualResetEvent UnityThreading.DispatcherBase::dataEvent
	ManualResetEvent_t160 * ___dataEvent_1;
	// UnityThreading.TaskSortingSystem UnityThreading.DispatcherBase::TaskSortingSystem
	int32_t ___TaskSortingSystem_2;
};
struct DispatcherBase_t158_StaticFields{
	// System.Comparison`1<UnityThreading.TaskBase> UnityThreading.DispatcherBase::<>f__am$cache3
	Comparison_1_t161 * ___U3CU3Ef__amU24cache3_3;
};
