﻿#pragma once
#include <stdint.h>
// System.ComponentModel.TypeConverterAttribute
struct TypeConverterAttribute_t1619;
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.ComponentModel.TypeConverterAttribute
struct  TypeConverterAttribute_t1619  : public Attribute_t463
{
	// System.String System.ComponentModel.TypeConverterAttribute::converter_type
	String_t* ___converter_type_1;
};
struct TypeConverterAttribute_t1619_StaticFields{
	// System.ComponentModel.TypeConverterAttribute System.ComponentModel.TypeConverterAttribute::Default
	TypeConverterAttribute_t1619 * ___Default_0;
};
