﻿#pragma once
#include <stdint.h>
// UnityEngine.Collider[]
struct ColliderU5BU5D_t59;
// System.String
struct String_t;
// UnityEngine.Camera
struct Camera_t6;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// InputLayerElement
struct  InputLayerElement_t58  : public MonoBehaviour_t5
{
	// UnityEngine.Collider[] InputLayerElement::touchColliders
	ColliderU5BU5D_t59* ___touchColliders_2;
	// System.String InputLayerElement::referenceCameraName
	String_t* ___referenceCameraName_3;
	// UnityEngine.Camera InputLayerElement::referenceCamera
	Camera_t6 * ___referenceCamera_4;
	// UnityEngine.RaycastHit InputLayerElement::hit
	RaycastHit_t60  ___hit_5;
};
