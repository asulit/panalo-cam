﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
struct Dictionary_2_t2665;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
// System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>
struct  Enumerator_t2672 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::dictionary
	Dictionary_2_t2665 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::current
	KeyValuePair_2_t2667  ___current_3;
};
