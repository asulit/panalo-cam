﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t365;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Component>
struct  Predicate_1_t670  : public MulticastDelegate_t28
{
};
