﻿#pragma once
#include <stdint.h>
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t1077;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t301;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct  ReconstructionFromTargetAbstractBehaviour_t303  : public MonoBehaviour_t5
{
	// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionFromTarget
	Object_t * ___mReconstructionFromTarget_2;
	// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::mReconstructionBehaviour
	ReconstructionAbstractBehaviour_t301 * ___mReconstructionBehaviour_3;
};
