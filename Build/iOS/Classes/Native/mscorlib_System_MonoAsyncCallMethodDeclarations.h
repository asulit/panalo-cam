﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoAsyncCall
struct MonoAsyncCall_t2353;

// System.Void System.MonoAsyncCall::.ctor()
extern "C" void MonoAsyncCall__ctor_m14230 (MonoAsyncCall_t2353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
