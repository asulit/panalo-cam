﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
struct Dictionary_2_t2665;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_41.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17245_gshared (Enumerator_t2675 * __this, Dictionary_2_t2665 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m17245(__this, ___host, method) (( void (*) (Enumerator_t2675 *, Dictionary_2_t2665 *, const MethodInfo*))Enumerator__ctor_m17245_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17246_gshared (Enumerator_t2675 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17246(__this, method) (( Object_t * (*) (Enumerator_t2675 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17246_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m17247_gshared (Enumerator_t2675 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17247(__this, method) (( void (*) (Enumerator_t2675 *, const MethodInfo*))Enumerator_Dispose_m17247_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17248_gshared (Enumerator_t2675 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17248(__this, method) (( bool (*) (Enumerator_t2675 *, const MethodInfo*))Enumerator_MoveNext_m17248_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m17249_gshared (Enumerator_t2675 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17249(__this, method) (( Object_t * (*) (Enumerator_t2675 *, const MethodInfo*))Enumerator_get_Current_m17249_gshared)(__this, method)
