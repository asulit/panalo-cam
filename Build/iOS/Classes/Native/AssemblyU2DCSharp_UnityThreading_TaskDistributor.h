﻿#pragma once
#include <stdint.h>
// UnityThreading.TaskWorker[]
struct TaskWorkerU5BU5D_t167;
// UnityThreading.TaskDistributor
struct TaskDistributor_t166;
// UnityThreading.DispatcherBase
#include "AssemblyU2DCSharp_UnityThreading_DispatcherBase.h"
// UnityThreading.TaskDistributor
struct  TaskDistributor_t166  : public DispatcherBase_t158
{
	// UnityThreading.TaskWorker[] UnityThreading.TaskDistributor::workerThreads
	TaskWorkerU5BU5D_t167* ___workerThreads_4;
};
struct TaskDistributor_t166_StaticFields{
	// UnityThreading.TaskDistributor UnityThreading.TaskDistributor::mainTaskDistributor
	TaskDistributor_t166 * ___mainTaskDistributor_5;
};
