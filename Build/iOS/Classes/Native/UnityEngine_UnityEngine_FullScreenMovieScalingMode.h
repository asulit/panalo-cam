﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.FullScreenMovieScalingMode
#include "UnityEngine_UnityEngine_FullScreenMovieScalingMode.h"
// UnityEngine.FullScreenMovieScalingMode
struct  FullScreenMovieScalingMode_t821 
{
	// System.Int32 UnityEngine.FullScreenMovieScalingMode::value__
	int32_t ___value___1;
};
