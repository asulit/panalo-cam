﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PrefabManager/PreloadData
struct PreloadData_t94;
// System.String
struct String_t;

// System.Void PrefabManager/PreloadData::.ctor()
extern "C" void PreloadData__ctor_m324 (PreloadData_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager/PreloadData::Set(System.String,System.Int32)
extern "C" void PreloadData_Set_m325 (PreloadData_t94 * __this, String_t* ___prefabName, int32_t ___preloadCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PrefabManager/PreloadData::get_PrefabName()
extern "C" String_t* PreloadData_get_PrefabName_m326 (PreloadData_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PrefabManager/PreloadData::get_PreloadCount()
extern "C" int32_t PreloadData_get_PreloadCount_m327 (PreloadData_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
