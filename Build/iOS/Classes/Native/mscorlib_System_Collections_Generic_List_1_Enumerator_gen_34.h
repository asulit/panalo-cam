﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_t343;
// UnityEngine.Collider
struct Collider_t369;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Collider>
struct  Enumerator_t2535 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.Collider>::l
	List_1_t343 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Collider>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Collider>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Collider>::current
	Collider_t369 * ___current_3;
};
