﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
#include "mscorlib_System_Array.h"

// System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_19.h"
// System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_19MethodDeclarations.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.BoneWeight>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.BoneWeight>(System.Int32)
extern "C" BoneWeight_t405  Array_InternalArray__get_Item_TisBoneWeight_t405_m30182_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoneWeight_t405_m30182(__this, p0, method) (( BoneWeight_t405  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoneWeight_t405_m30182_gshared)(__this, p0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>::.ctor(System.Array)
// System.Array
#include "mscorlib_System_Array.h"
extern "C" void InternalEnumerator_1__ctor_m15940_gshared (InternalEnumerator_1_t2577 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15941_gshared (InternalEnumerator_1_t2577 * __this, const MethodInfo* method)
{
	{
		BoneWeight_t405  L_0 = (( BoneWeight_t405  (*) (InternalEnumerator_1_t2577 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2577 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		BoneWeight_t405  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15942_gshared (InternalEnumerator_1_t2577 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>::MoveNext()
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern "C" bool InternalEnumerator_1_MoveNext_m15943_gshared (InternalEnumerator_1_t2577 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9469((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>::get_Current()
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3382;
extern Il2CppCodeGenString* _stringLiteral3383;
extern "C" BoneWeight_t405  InternalEnumerator_1_get_Current_m15944_gshared (InternalEnumerator_1_t2577 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3382 = il2cpp_codegen_string_literal_from_index(3382);
		_stringLiteral3383 = il2cpp_codegen_string_literal_from_index(3383);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, (String_t*)_stringLiteral3382, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t425 * L_3 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_3, (String_t*)_stringLiteral3383, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		BoneWeight_t405  L_8 = (( BoneWeight_t405  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Array/InternalEnumerator`1<UnityEngine.Vector2>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_20.h"
// System.Array/InternalEnumerator`1<UnityEngine.Vector2>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_20MethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern "C" Vector2_t2  Array_InternalArray__get_Item_TisVector2_t2_m30193_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector2_t2_m30193(__this, p0, method) (( Vector2_t2  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector2_t2_m30193_gshared)(__this, p0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
// System.Array
#include "mscorlib_System_Array.h"
extern "C" void InternalEnumerator_1__ctor_m15945_gshared (InternalEnumerator_1_t2578 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15946_gshared (InternalEnumerator_1_t2578 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (( Vector2_t2  (*) (InternalEnumerator_1_t2578 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2578 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector2_t2  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15947_gshared (InternalEnumerator_1_t2578 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern "C" bool InternalEnumerator_1_MoveNext_m15948_gshared (InternalEnumerator_1_t2578 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9469((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3382;
extern Il2CppCodeGenString* _stringLiteral3383;
extern "C" Vector2_t2  InternalEnumerator_1_get_Current_m15949_gshared (InternalEnumerator_1_t2578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3382 = il2cpp_codegen_string_literal_from_index(3382);
		_stringLiteral3383 = il2cpp_codegen_string_literal_from_index(3383);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, (String_t*)_stringLiteral3382, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t425 * L_3 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_3, (String_t*)_stringLiteral3383, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		Vector2_t2  L_8 = (( Vector2_t2  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_2.h"
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_2MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
struct ObjectU5BU5D_t356;
// Declaration System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C" void Array_Resize_TisObject_t_m30047_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t356** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisObject_t_m30047(__this /* static, unused */, p0, p1, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t356**, int32_t, const MethodInfo*))Array_Resize_TisObject_t_m30047_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void Stack_1__ctor_m15950_gshared (Stack_1_t2580 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Stack_1_System_Collections_ICollection_get_IsSynchronized_m15952_gshared (Stack_1_t2580 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Stack_1_System_Collections_ICollection_get_SyncRoot_m15954_gshared (Stack_1_t2580 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_Array.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern TypeInfo* ArrayTypeMismatchException_t2315_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern "C" void Stack_1_System_Collections_ICollection_CopyTo_m15956_gshared (Stack_1_t2580 * __this, Array_t * ___dest, int32_t ___idx, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayTypeMismatchException_t2315_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1197);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			ObjectU5BU5D_t356* L_0 = (ObjectU5BU5D_t356*)(__this->____array_1);
			if (!L_0)
			{
				goto IL_0025;
			}
		}

IL_000b:
		{
			ObjectU5BU5D_t356* L_1 = (ObjectU5BU5D_t356*)(__this->____array_1);
			Array_t * L_2 = ___dest;
			int32_t L_3 = ___idx;
			NullCheck((Array_t *)L_1);
			VirtActionInvoker2< Array_t *, int32_t >::Invoke(8 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Array_t *)L_1, (Array_t *)L_2, (int32_t)L_3);
			Array_t * L_4 = ___dest;
			int32_t L_5 = ___idx;
			int32_t L_6 = (int32_t)(__this->____size_2);
			Array_Reverse_m8498(NULL /*static, unused*/, (Array_t *)L_4, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		}

IL_0025:
		{
			goto IL_0036;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t359 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArrayTypeMismatchException_t2315_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002a;
		throw e;
	}

CATCH_002a:
	{ // begin catch(System.ArrayTypeMismatchException)
		ArgumentException_t764 * L_7 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m13764(L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
		goto IL_0036;
	} // end catch (depth: 1)

IL_0036:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15958_gshared (Stack_1_t2580 * __this, const MethodInfo* method)
{
	{
		NullCheck((Stack_1_t2580 *)__this);
		Enumerator_t2581  L_0 = (( Enumerator_t2581  (*) (Stack_1_t2580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Stack_1_t2580 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Enumerator_t2581  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Stack_1_System_Collections_IEnumerable_GetEnumerator_m15960_gshared (Stack_1_t2580 * __this, const MethodInfo* method)
{
	{
		NullCheck((Stack_1_t2580 *)__this);
		Enumerator_t2581  L_0 = (( Enumerator_t2581  (*) (Stack_1_t2580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Stack_1_t2580 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Enumerator_t2581  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return (Object_t *)L_2;
	}
}
// T System.Collections.Generic.Stack`1<System.Object>::Peek()
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern "C" Object_t * Stack_1_Peek_m15962_gshared (Stack_1_t2580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m9471(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		ObjectU5BU5D_t356* L_2 = (ObjectU5BU5D_t356*)(__this->____array_1);
		int32_t L_3 = (int32_t)(__this->____size_2);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, ((int32_t)((int32_t)L_3-(int32_t)1)));
		int32_t L_4 = ((int32_t)((int32_t)L_3-(int32_t)1));
		return (*(Object_t **)(Object_t **)SZArrayLdElema(L_2, L_4, sizeof(Object_t *)));
	}
}
// T System.Collections.Generic.Stack`1<System.Object>::Pop()
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" Object_t * Stack_1_Pop_m15963_gshared (Stack_1_t2580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	int32_t V_1 = 0;
	Object_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m9471(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		ObjectU5BU5D_t356* L_3 = (ObjectU5BU5D_t356*)(__this->____array_1);
		int32_t L_4 = (int32_t)(__this->____size_2);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_1 = (int32_t)L_5;
		__this->____size_2 = L_5;
		int32_t L_6 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		int32_t L_7 = L_6;
		V_0 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_7, sizeof(Object_t *)));
		ObjectU5BU5D_t356* L_8 = (ObjectU5BU5D_t356*)(__this->____array_1);
		int32_t L_9 = (int32_t)(__this->____size_2);
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_2));
		Object_t * L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, L_9, sizeof(Object_t *))) = (Object_t *)L_10;
		Object_t * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(T)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" void Stack_1_Push_m15964_gshared (Stack_1_t2580 * __this, Object_t * ___t, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ObjectU5BU5D_t356** G_B4_0 = {0};
	ObjectU5BU5D_t356** G_B3_0 = {0};
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t356** G_B5_1 = {0};
	{
		ObjectU5BU5D_t356* L_0 = (ObjectU5BU5D_t356*)(__this->____array_1);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)(__this->____size_2);
		ObjectU5BU5D_t356* L_2 = (ObjectU5BU5D_t356*)(__this->____array_1);
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)(((Array_t *)L_2)->max_length)))))))
		{
			goto IL_0043;
		}
	}

IL_001e:
	{
		ObjectU5BU5D_t356** L_3 = (ObjectU5BU5D_t356**)&(__this->____array_1);
		int32_t L_4 = (int32_t)(__this->____size_2);
		G_B3_0 = L_3;
		if (L_4)
		{
			G_B4_0 = L_3;
			goto IL_0036;
		}
	}
	{
		G_B5_0 = ((int32_t)16);
		G_B5_1 = G_B3_0;
		goto IL_003e;
	}

IL_0036:
	{
		int32_t L_5 = (int32_t)(__this->____size_2);
		G_B5_0 = ((int32_t)((int32_t)2*(int32_t)L_5));
		G_B5_1 = G_B4_0;
	}

IL_003e:
	{
		(( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t356**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t356**)G_B5_1, (int32_t)G_B5_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0043:
	{
		int32_t L_6 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_6+(int32_t)1));
		ObjectU5BU5D_t356* L_7 = (ObjectU5BU5D_t356*)(__this->____array_1);
		int32_t L_8 = (int32_t)(__this->____size_2);
		int32_t L_9 = (int32_t)L_8;
		V_0 = (int32_t)L_9;
		__this->____size_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
		int32_t L_10 = V_0;
		Object_t * L_11 = ___t;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, L_10, sizeof(Object_t *))) = (Object_t *)L_11;
		return;
	}
}
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C" int32_t Stack_1_get_Count_m15966_gshared (Stack_1_t2580 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		return L_0;
	}
}
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2581  Stack_1_GetEnumerator_m15968_gshared (Stack_1_t2580 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2581  L_0 = {0};
		(( void (*) (Enumerator_t2581 *, Stack_1_t2580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_0, (Stack_1_t2580 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_0;
	}
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_2.h"
extern "C" void Enumerator__ctor_m15969_gshared (Enumerator_t2581 * __this, Stack_1_t2580 * ___t, const MethodInfo* method)
{
	{
		Stack_1_t2580 * L_0 = ___t;
		__this->___parent_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		Stack_1_t2580 * L_1 = ___t;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->____version_3);
		__this->____version_2 = L_2;
		return;
	}
}
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15970_gshared (Enumerator_t2581 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (Enumerator_t2581 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2581 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15971_gshared (Enumerator_t2581 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern "C" bool Enumerator_MoveNext_m15972_gshared (Enumerator_t2581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->____version_2);
		Stack_1_t2580 * L_1 = (Stack_1_t2580 *)(__this->___parent_0);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->____version_3);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t425 * L_3 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m9471(L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_001c:
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_003a;
		}
	}
	{
		Stack_1_t2580 * L_5 = (Stack_1_t2580 *)(__this->___parent_0);
		NullCheck(L_5);
		int32_t L_6 = (int32_t)(L_5->____size_2);
		__this->___idx_1 = L_6;
	}

IL_003a:
	{
		int32_t L_7 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_8 = (int32_t)(__this->___idx_1);
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8-(int32_t)1));
		V_0 = (int32_t)L_9;
		__this->___idx_1 = L_9;
		int32_t L_10 = V_0;
		G_B7_0 = ((((int32_t)((((int32_t)L_10) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0060;
	}

IL_005f:
	{
		G_B7_0 = 0;
	}

IL_0060:
	{
		return G_B7_0;
	}
}
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern "C" Object_t * Enumerator_get_Current_m15973_gshared (Enumerator_t2581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m9471(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		Stack_1_t2580 * L_2 = (Stack_1_t2580 *)(__this->___parent_0);
		NullCheck(L_2);
		ObjectU5BU5D_t356* L_3 = (ObjectU5BU5D_t356*)(L_2->____array_1);
		int32_t L_4 = (int32_t)(__this->___idx_1);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return (*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5, sizeof(Object_t *)));
	}
}
// System.Collections.Generic.LinkedList`1<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_gen_2.h"
// System.Collections.Generic.LinkedList`1<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_gen_2MethodDeclarations.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.Collections.Generic.LinkedListNode`1<System.Object>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_2.h"
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.Collections.Generic.LinkedListNode`1<System.Object>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_2MethodDeclarations.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_geMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" void LinkedList_1__ctor_m15979_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m1306(L_0, /*hidden argument*/NULL);
		__this->___syncRoot_4 = L_0;
		__this->___first_5 = (LinkedListNode_1_t2584 *)NULL;
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->___version_3 = L_1;
		uint32_t L_2 = V_0;
		__this->___count_2 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" void LinkedList_1__ctor_m15981_gshared (LinkedList_1_t2583 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((LinkedList_1_t2583 *)__this);
		(( void (*) (LinkedList_1_t2583 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((LinkedList_1_t2583 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SerializationInfo_t1012 * L_0 = ___info;
		__this->___si_6 = L_0;
		Object_t * L_1 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m1306(L_1, /*hidden argument*/NULL);
		__this->___syncRoot_4 = L_1;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15983_gshared (LinkedList_1_t2583 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((LinkedList_1_t2583 *)__this);
		(( LinkedListNode_1_t2584 * (*) (LinkedList_1_t2583 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((LinkedList_1_t2583 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_Array.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1265;
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m15985_gshared (LinkedList_1_t2583 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral1265 = il2cpp_codegen_string_literal_from_index(1265);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t356* V_0 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (ObjectU5BU5D_t356*)((ObjectU5BU5D_t356*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		ObjectU5BU5D_t356* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t764 * L_2 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_2, (String_t*)_stringLiteral1265, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0018:
	{
		ObjectU5BU5D_t356* L_3 = V_0;
		int32_t L_4 = ___index;
		NullCheck((LinkedList_1_t2583 *)__this);
		VirtActionInvoker2< ObjectU5BU5D_t356*, int32_t >::Invoke(16 /* System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32) */, (LinkedList_1_t2583 *)__this, (ObjectU5BU5D_t356*)L_3, (int32_t)L_4);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15987_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2583 *)__this);
		Enumerator_t2585  L_0 = (( Enumerator_t2585  (*) (LinkedList_1_t2583 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((LinkedList_1_t2583 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Enumerator_t2585  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m15989_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2583 *)__this);
		Enumerator_t2585  L_0 = (( Enumerator_t2585  (*) (LinkedList_1_t2583 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((LinkedList_1_t2583 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Enumerator_t2585  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15991_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m15993_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m15995_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___syncRoot_4);
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
// System.Collections.Generic.LinkedListNode`1<System.Object>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_2.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3398;
extern "C" void LinkedList_1_VerifyReferencedNode_m15997_gshared (LinkedList_1_t2583 * __this, LinkedListNode_1_t2584 * ___node, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3398 = il2cpp_codegen_string_literal_from_index(3398);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedListNode_1_t2584 * L_0 = ___node;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral3398, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t2584 * L_2 = ___node;
		NullCheck((LinkedListNode_1_t2584 *)L_2);
		LinkedList_1_t2583 * L_3 = (( LinkedList_1_t2583 * (*) (LinkedListNode_1_t2584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((LinkedListNode_1_t2584 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if ((((Object_t*)(LinkedList_1_t2583 *)L_3) == ((Object_t*)(LinkedList_1_t2583 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t425 * L_4 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m9471(L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C" LinkedListNode_1_t2584 * LinkedList_1_AddLast_m15998_gshared (LinkedList_1_t2583 * __this, Object_t * ___value, const MethodInfo* method)
{
	LinkedListNode_1_t2584 * V_0 = {0};
	{
		LinkedListNode_1_t2584 * L_0 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Object_t * L_1 = ___value;
		LinkedListNode_1_t2584 * L_2 = (LinkedListNode_1_t2584 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (LinkedListNode_1_t2584 *, LinkedList_1_t2583 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_2, (LinkedList_1_t2583 *)__this, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t2584 *)L_2;
		LinkedListNode_1_t2584 * L_3 = V_0;
		__this->___first_5 = L_3;
		goto IL_0038;
	}

IL_001f:
	{
		Object_t * L_4 = ___value;
		LinkedListNode_1_t2584 * L_5 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		NullCheck(L_5);
		LinkedListNode_1_t2584 * L_6 = (LinkedListNode_1_t2584 *)(L_5->___back_3);
		LinkedListNode_1_t2584 * L_7 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		LinkedListNode_1_t2584 * L_8 = (LinkedListNode_1_t2584 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (LinkedListNode_1_t2584 *, LinkedList_1_t2583 *, Object_t *, LinkedListNode_1_t2584 *, LinkedListNode_1_t2584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_8, (LinkedList_1_t2583 *)__this, (Object_t *)L_4, (LinkedListNode_1_t2584 *)L_6, (LinkedListNode_1_t2584 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t2584 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)(__this->___count_2);
		__this->___count_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
		uint32_t L_10 = (uint32_t)(__this->___version_3);
		__this->___version_3 = ((int32_t)((int32_t)L_10+(int32_t)1));
		LinkedListNode_1_t2584 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C" void LinkedList_1_Clear_m16000_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t2583 *)__this);
		(( void (*) (LinkedList_1_t2583 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((LinkedList_1_t2583 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t2584 * L_0 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C" bool LinkedList_1_Contains_m16002_gshared (LinkedList_1_t2583 * __this, Object_t * ___value, const MethodInfo* method)
{
	LinkedListNode_1_t2584 * V_0 = {0};
	{
		LinkedListNode_1_t2584 * L_0 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		V_0 = (LinkedListNode_1_t2584 *)L_0;
		LinkedListNode_1_t2584 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		LinkedListNode_1_t2584 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t2584 *)L_2);
		Object_t * L_3 = (( Object_t * (*) (LinkedListNode_1_t2584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t2584 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((Object_t *)(*(&___value)));
		bool L_4 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)(*(&___value)), (Object_t *)L_3);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		LinkedListNode_1_t2584 * L_5 = V_0;
		NullCheck(L_5);
		LinkedListNode_1_t2584 * L_6 = (LinkedListNode_1_t2584 *)(L_5->___forward_2);
		V_0 = (LinkedListNode_1_t2584 *)L_6;
		LinkedListNode_1_t2584 * L_7 = V_0;
		LinkedListNode_1_t2584 * L_8 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		if ((!(((Object_t*)(LinkedListNode_1_t2584 *)L_7) == ((Object_t*)(LinkedListNode_1_t2584 *)L_8))))
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
#include "mscorlib_ArrayTypes.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1265;
extern Il2CppCodeGenString* _stringLiteral1267;
extern Il2CppCodeGenString* _stringLiteral3399;
extern Il2CppCodeGenString* _stringLiteral3400;
extern "C" void LinkedList_1_CopyTo_m16004_gshared (LinkedList_1_t2583 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(906);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral1265 = il2cpp_codegen_string_literal_from_index(1265);
		_stringLiteral1267 = il2cpp_codegen_string_literal_from_index(1267);
		_stringLiteral3399 = il2cpp_codegen_string_literal_from_index(3399);
		_stringLiteral3400 = il2cpp_codegen_string_literal_from_index(3400);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t2584 * V_0 = {0};
	{
		ObjectU5BU5D_t356* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral1265, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		ObjectU5BU5D_t356* L_3 = ___array;
		NullCheck((Array_t *)L_3);
		int32_t L_4 = Array_GetLowerBound_m10208((Array_t *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t1419 * L_5 = (ArgumentOutOfRangeException_t1419 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8460(L_5, (String_t*)_stringLiteral1267, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0029:
	{
		ObjectU5BU5D_t356* L_6 = ___array;
		NullCheck((Array_t *)L_6);
		int32_t L_7 = Array_get_Rank_m9472((Array_t *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t764 * L_8 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m7574(L_8, (String_t*)_stringLiteral1265, (String_t*)_stringLiteral3399, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0045:
	{
		ObjectU5BU5D_t356* L_9 = ___array;
		NullCheck(L_9);
		int32_t L_10 = ___index;
		ObjectU5BU5D_t356* L_11 = ___array;
		NullCheck((Array_t *)L_11);
		int32_t L_12 = Array_GetLowerBound_m10208((Array_t *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)(__this->___count_2);
		if ((((int64_t)(((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_9)->max_length)))-(int32_t)L_10))+(int32_t)L_12))))) >= ((int64_t)(((uint64_t)L_13)))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t764 * L_14 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_14, (String_t*)_stringLiteral3400, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t2584 * L_15 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		V_0 = (LinkedListNode_1_t2584 *)L_15;
		LinkedListNode_1_t2584 * L_16 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		ObjectU5BU5D_t356* L_17 = ___array;
		int32_t L_18 = ___index;
		LinkedListNode_1_t2584 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t2584 *)L_19);
		Object_t * L_20 = (( Object_t * (*) (LinkedListNode_1_t2584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t2584 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, L_18, sizeof(Object_t *))) = (Object_t *)L_20;
		int32_t L_21 = ___index;
		___index = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t2584 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t2584 * L_23 = (LinkedListNode_1_t2584 *)(L_22->___forward_2);
		V_0 = (LinkedListNode_1_t2584 *)L_23;
		LinkedListNode_1_t2584 * L_24 = V_0;
		LinkedListNode_1_t2584 * L_25 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		if ((!(((Object_t*)(LinkedListNode_1_t2584 *)L_24) == ((Object_t*)(LinkedListNode_1_t2584 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C" LinkedListNode_1_t2584 * LinkedList_1_Find_m16006_gshared (LinkedList_1_t2583 * __this, Object_t * ___value, const MethodInfo* method)
{
	LinkedListNode_1_t2584 * V_0 = {0};
	{
		LinkedListNode_1_t2584 * L_0 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		V_0 = (LinkedListNode_1_t2584 *)L_0;
		LinkedListNode_1_t2584 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t2584 *)NULL;
	}

IL_000f:
	{
		Object_t * L_2 = ___value;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		LinkedListNode_1_t2584 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t2584 *)L_3);
		Object_t * L_4 = (( Object_t * (*) (LinkedListNode_1_t2584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t2584 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		if (!L_4)
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		Object_t * L_5 = ___value;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		LinkedListNode_1_t2584 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t2584 *)L_6);
		Object_t * L_7 = (( Object_t * (*) (LinkedListNode_1_t2584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t2584 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((Object_t *)(*(&___value)));
		bool L_8 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)(*(&___value)), (Object_t *)L_7);
		if (!L_8)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t2584 * L_9 = V_0;
		return L_9;
	}

IL_0054:
	{
		LinkedListNode_1_t2584 * L_10 = V_0;
		NullCheck(L_10);
		LinkedListNode_1_t2584 * L_11 = (LinkedListNode_1_t2584 *)(L_10->___forward_2);
		V_0 = (LinkedListNode_1_t2584 *)L_11;
		LinkedListNode_1_t2584 * L_12 = V_0;
		LinkedListNode_1_t2584 * L_13 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		if ((!(((Object_t*)(LinkedListNode_1_t2584 *)L_12) == ((Object_t*)(LinkedListNode_1_t2584 *)L_13))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t2584 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2585  LinkedList_1_GetEnumerator_m16008_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2585  L_0 = {0};
		(( void (*) (Enumerator_t2585 *, LinkedList_1_t2583 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(&L_0, (LinkedList_1_t2583 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3401;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void LinkedList_1_GetObjectData_m16010_gshared (LinkedList_1_t2583 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		_stringLiteral3401 = il2cpp_codegen_string_literal_from_index(3401);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t356* V_0 = {0};
	{
		uint32_t L_0 = (uint32_t)(__this->___count_2);
		V_0 = (ObjectU5BU5D_t356*)((ObjectU5BU5D_t356*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), (((uintptr_t)L_0))));
		ObjectU5BU5D_t356* L_1 = V_0;
		NullCheck((LinkedList_1_t2583 *)__this);
		VirtActionInvoker2< ObjectU5BU5D_t356*, int32_t >::Invoke(16 /* System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32) */, (LinkedList_1_t2583 *)__this, (ObjectU5BU5D_t356*)L_1, (int32_t)0);
		SerializationInfo_t1012 * L_2 = ___info;
		ObjectU5BU5D_t356* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1012 *)L_2);
		SerializationInfo_AddValue_m9474((SerializationInfo_t1012 *)L_2, (String_t*)_stringLiteral3401, (Object_t *)(Object_t *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t1012 * L_5 = ___info;
		uint32_t L_6 = (uint32_t)(__this->___version_3);
		NullCheck((SerializationInfo_t1012 *)L_5);
		SerializationInfo_AddValue_m12890((SerializationInfo_t1012 *)L_5, (String_t*)_stringLiteral1325, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3401;
extern Il2CppCodeGenString* _stringLiteral1325;
extern "C" void LinkedList_1_OnDeserialization_m16012_gshared (LinkedList_1_t2583 * __this, Object_t * ___sender, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		_stringLiteral3401 = il2cpp_codegen_string_literal_from_index(3401);
		_stringLiteral1325 = il2cpp_codegen_string_literal_from_index(1325);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t356* V_0 = {0};
	Object_t * V_1 = {0};
	ObjectU5BU5D_t356* V_2 = {0};
	int32_t V_3 = 0;
	{
		SerializationInfo_t1012 * L_0 = (SerializationInfo_t1012 *)(__this->___si_6);
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t1012 * L_1 = (SerializationInfo_t1012 *)(__this->___si_6);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1012 *)L_1);
		Object_t * L_3 = SerializationInfo_GetValue_m9477((SerializationInfo_t1012 *)L_1, (String_t*)_stringLiteral3401, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t356*)((ObjectU5BU5D_t356*)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		ObjectU5BU5D_t356* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t356* L_5 = V_0;
		V_2 = (ObjectU5BU5D_t356*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		ObjectU5BU5D_t356* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_1 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_6, L_8, sizeof(Object_t *)));
		Object_t * L_9 = V_1;
		NullCheck((LinkedList_1_t2583 *)__this);
		(( LinkedListNode_1_t2584 * (*) (LinkedList_1_t2583 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((LinkedList_1_t2583 *)__this, (Object_t *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_10 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_11 = V_3;
		ObjectU5BU5D_t356* L_12 = V_2;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_12)->max_length))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t1012 * L_13 = (SerializationInfo_t1012 *)(__this->___si_6);
		NullCheck((SerializationInfo_t1012 *)L_13);
		uint32_t L_14 = SerializationInfo_GetUInt32_m12893((SerializationInfo_t1012 *)L_13, (String_t*)_stringLiteral1325, /*hidden argument*/NULL);
		__this->___version_3 = L_14;
		__this->___si_6 = (SerializationInfo_t1012 *)NULL;
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C" bool LinkedList_1_Remove_m16014_gshared (LinkedList_1_t2583 * __this, Object_t * ___value, const MethodInfo* method)
{
	LinkedListNode_1_t2584 * V_0 = {0};
	{
		Object_t * L_0 = ___value;
		NullCheck((LinkedList_1_t2583 *)__this);
		LinkedListNode_1_t2584 * L_1 = (( LinkedListNode_1_t2584 * (*) (LinkedList_1_t2583 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((LinkedList_1_t2583 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t2584 *)L_1;
		LinkedListNode_1_t2584 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return 0;
	}

IL_0010:
	{
		LinkedListNode_1_t2584 * L_3 = V_0;
		NullCheck((LinkedList_1_t2583 *)__this);
		(( void (*) (LinkedList_1_t2583 *, LinkedListNode_1_t2584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((LinkedList_1_t2583 *)__this, (LinkedListNode_1_t2584 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		return 1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m16016_gshared (LinkedList_1_t2583 * __this, LinkedListNode_1_t2584 * ___node, const MethodInfo* method)
{
	{
		LinkedListNode_1_t2584 * L_0 = ___node;
		NullCheck((LinkedList_1_t2583 *)__this);
		(( void (*) (LinkedList_1_t2583 *, LinkedListNode_1_t2584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((LinkedList_1_t2583 *)__this, (LinkedListNode_1_t2584 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)(__this->___count_2);
		__this->___count_2 = ((int32_t)((int32_t)L_1-(int32_t)1));
		uint32_t L_2 = (uint32_t)(__this->___count_2);
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->___first_5 = (LinkedListNode_1_t2584 *)NULL;
	}

IL_0027:
	{
		LinkedListNode_1_t2584 * L_3 = ___node;
		LinkedListNode_1_t2584 * L_4 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		if ((!(((Object_t*)(LinkedListNode_1_t2584 *)L_3) == ((Object_t*)(LinkedListNode_1_t2584 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t2584 * L_5 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		NullCheck(L_5);
		LinkedListNode_1_t2584 * L_6 = (LinkedListNode_1_t2584 *)(L_5->___forward_2);
		__this->___first_5 = L_6;
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)(__this->___version_3);
		__this->___version_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
		LinkedListNode_1_t2584 * L_8 = ___node;
		NullCheck((LinkedListNode_1_t2584 *)L_8);
		(( void (*) (LinkedListNode_1_t2584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((LinkedListNode_1_t2584 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C" void LinkedList_1_RemoveLast_m16018_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t2584 * L_0 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t2584 * L_1 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		NullCheck(L_1);
		LinkedListNode_1_t2584 * L_2 = (LinkedListNode_1_t2584 *)(L_1->___back_3);
		NullCheck((LinkedList_1_t2583 *)__this);
		(( void (*) (LinkedList_1_t2583 *, LinkedListNode_1_t2584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((LinkedList_1_t2583 *)__this, (LinkedListNode_1_t2584 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m16020_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)(__this->___count_2);
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C" LinkedListNode_1_t2584 * LinkedList_1_get_First_m16021_gshared (LinkedList_1_t2583 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t2584 * L_0 = (LinkedListNode_1_t2584 *)(__this->___first_5);
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
// System.Collections.Generic.LinkedList`1<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_gen_2.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void LinkedListNode_1__ctor_m16022_gshared (LinkedListNode_1_t2584 * __this, LinkedList_1_t2583 * ___list, Object_t * ___value, const MethodInfo* method)
{
	LinkedListNode_1_t2584 * V_0 = {0};
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2583 * L_0 = ___list;
		__this->___container_1 = L_0;
		Object_t * L_1 = ___value;
		__this->___item_0 = L_1;
		V_0 = (LinkedListNode_1_t2584 *)__this;
		__this->___forward_2 = __this;
		LinkedListNode_1_t2584 * L_2 = V_0;
		__this->___back_3 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
// System.Collections.Generic.LinkedListNode`1<System.Object>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_2.h"
extern "C" void LinkedListNode_1__ctor_m16023_gshared (LinkedListNode_1_t2584 * __this, LinkedList_1_t2583 * ___list, Object_t * ___value, LinkedListNode_1_t2584 * ___previousNode, LinkedListNode_1_t2584 * ___nextNode, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2583 * L_0 = ___list;
		__this->___container_1 = L_0;
		Object_t * L_1 = ___value;
		__this->___item_0 = L_1;
		LinkedListNode_1_t2584 * L_2 = ___previousNode;
		__this->___back_3 = L_2;
		LinkedListNode_1_t2584 * L_3 = ___nextNode;
		__this->___forward_2 = L_3;
		LinkedListNode_1_t2584 * L_4 = ___previousNode;
		NullCheck(L_4);
		L_4->___forward_2 = __this;
		LinkedListNode_1_t2584 * L_5 = ___nextNode;
		NullCheck(L_5);
		L_5->___back_3 = __this;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C" void LinkedListNode_1_Detach_m16024_gshared (LinkedListNode_1_t2584 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t2584 * V_0 = {0};
	{
		LinkedListNode_1_t2584 * L_0 = (LinkedListNode_1_t2584 *)(__this->___back_3);
		LinkedListNode_1_t2584 * L_1 = (LinkedListNode_1_t2584 *)(__this->___forward_2);
		NullCheck(L_0);
		L_0->___forward_2 = L_1;
		LinkedListNode_1_t2584 * L_2 = (LinkedListNode_1_t2584 *)(__this->___forward_2);
		LinkedListNode_1_t2584 * L_3 = (LinkedListNode_1_t2584 *)(__this->___back_3);
		NullCheck(L_2);
		L_2->___back_3 = L_3;
		V_0 = (LinkedListNode_1_t2584 *)NULL;
		__this->___back_3 = (LinkedListNode_1_t2584 *)NULL;
		LinkedListNode_1_t2584 * L_4 = V_0;
		__this->___forward_2 = L_4;
		__this->___container_1 = (LinkedList_1_t2583 *)NULL;
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C" LinkedList_1_t2583 * LinkedListNode_1_get_List_m16025_gshared (LinkedListNode_1_t2584 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t2583 * L_0 = (LinkedList_1_t2583 *)(__this->___container_1);
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
extern "C" LinkedListNode_1_t2584 * LinkedListNode_1_get_Next_m16026_gshared (LinkedListNode_1_t2584 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t2584 * G_B4_0 = {0};
	{
		LinkedList_1_t2583 * L_0 = (LinkedList_1_t2583 *)(__this->___container_1);
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t2584 * L_1 = (LinkedListNode_1_t2584 *)(__this->___forward_2);
		LinkedList_1_t2583 * L_2 = (LinkedList_1_t2583 *)(__this->___container_1);
		NullCheck(L_2);
		LinkedListNode_1_t2584 * L_3 = (LinkedListNode_1_t2584 *)(L_2->___first_5);
		if ((((Object_t*)(LinkedListNode_1_t2584 *)L_1) == ((Object_t*)(LinkedListNode_1_t2584 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t2584 * L_4 = (LinkedListNode_1_t2584 *)(__this->___forward_2);
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t2584 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C" Object_t * LinkedListNode_1_get_Value_m16027_gshared (LinkedListNode_1_t2584 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___item_0);
		return L_0;
	}
}
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
// System.Collections.Generic.LinkedList`1<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_gen_2.h"
extern "C" void Enumerator__ctor_m16028_gshared (Enumerator_t2585 * __this, LinkedList_1_t2583 * ___parent, const MethodInfo* method)
{
	{
		LinkedList_1_t2583 * L_0 = ___parent;
		__this->___list_0 = L_0;
		__this->___current_1 = (LinkedListNode_1_t2584 *)NULL;
		__this->___index_2 = (-1);
		LinkedList_1_t2583 * L_1 = ___parent;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)(L_1->___version_3);
		__this->___version_3 = L_2;
		return;
	}
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16029_gshared (Enumerator_t2585 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (Enumerator_t2585 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2585 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* ObjectDisposedException_t1420_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern "C" Object_t * Enumerator_get_Current_m16030_gshared (Enumerator_t2585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(907);
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2583 * L_0 = (LinkedList_1_t2583 *)(__this->___list_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1420 * L_1 = (ObjectDisposedException_t1420 *)il2cpp_codegen_object_new (ObjectDisposedException_t1420_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m7575(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t2584 * L_2 = (LinkedListNode_1_t2584 *)(__this->___current_1);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t425 * L_3 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m9471(L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t2584 * L_4 = (LinkedListNode_1_t2584 *)(__this->___current_1);
		NullCheck((LinkedListNode_1_t2584 *)L_4);
		Object_t * L_5 = (( Object_t * (*) (LinkedListNode_1_t2584 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((LinkedListNode_1_t2584 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern TypeInfo* ObjectDisposedException_t1420_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3402;
extern "C" bool Enumerator_MoveNext_m16031_gshared (Enumerator_t2585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(907);
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3402 = il2cpp_codegen_string_literal_from_index(3402);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2583 * L_0 = (LinkedList_1_t2583 *)(__this->___list_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1420 * L_1 = (ObjectDisposedException_t1420 *)il2cpp_codegen_object_new (ObjectDisposedException_t1420_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m7575(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)(__this->___version_3);
		LinkedList_1_t2583 * L_3 = (LinkedList_1_t2583 *)(__this->___list_0);
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)(L_3->___version_3);
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t425 * L_5 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_5, (String_t*)_stringLiteral3402, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t2584 * L_6 = (LinkedListNode_1_t2584 *)(__this->___current_1);
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t2583 * L_7 = (LinkedList_1_t2583 *)(__this->___list_0);
		NullCheck(L_7);
		LinkedListNode_1_t2584 * L_8 = (LinkedListNode_1_t2584 *)(L_7->___first_5);
		__this->___current_1 = L_8;
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t2584 * L_9 = (LinkedListNode_1_t2584 *)(__this->___current_1);
		NullCheck(L_9);
		LinkedListNode_1_t2584 * L_10 = (LinkedListNode_1_t2584 *)(L_9->___forward_2);
		__this->___current_1 = L_10;
		LinkedListNode_1_t2584 * L_11 = (LinkedListNode_1_t2584 *)(__this->___current_1);
		LinkedList_1_t2583 * L_12 = (LinkedList_1_t2583 *)(__this->___list_0);
		NullCheck(L_12);
		LinkedListNode_1_t2584 * L_13 = (LinkedListNode_1_t2584 *)(L_12->___first_5);
		if ((!(((Object_t*)(LinkedListNode_1_t2584 *)L_11) == ((Object_t*)(LinkedListNode_1_t2584 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->___current_1 = (LinkedListNode_1_t2584 *)NULL;
	}

IL_0082:
	{
		LinkedListNode_1_t2584 * L_14 = (LinkedListNode_1_t2584 *)(__this->___current_1);
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->___index_2 = (-1);
		return 0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)(__this->___index_2);
		__this->___index_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
		return 1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern TypeInfo* ObjectDisposedException_t1420_il2cpp_TypeInfo_var;
extern "C" void Enumerator_Dispose_m16032_gshared (Enumerator_t2585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(907);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2583 * L_0 = (LinkedList_1_t2583 *)(__this->___list_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1420 * L_1 = (ObjectDisposedException_t1420 *)il2cpp_codegen_object_new (ObjectDisposedException_t1420_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m7575(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		__this->___current_1 = (LinkedListNode_1_t2584 *)NULL;
		__this->___list_0 = (LinkedList_1_t2583 *)NULL;
		return;
	}
}
// Common.Utils.SimpleList`1<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_gen_1.h"
// Common.Utils.SimpleList`1<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_gen_1MethodDeclarations.h"
// Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_U3CGetEnumerator.h"
// System.Collections.Generic.EqualityComparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen.h"
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_4.h"
// Common.Utils.SimpleList`1/Visitor<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_Visitor_gen.h"
// Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_U3CGetEnumeratorMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// System.Collections.Generic.EqualityComparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_genMethodDeclarations.h"
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_4MethodDeclarations.h"
// Common.Utils.SimpleList`1/Visitor<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_Visitor_genMethodDeclarations.h"
// System.Void Common.Utils.SimpleList`1<System.Object>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void SimpleList_1__ctor_m16158_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Common.Utils.SimpleList`1<System.Object>::GetEnumerator()
extern "C" Object_t* SimpleList_1_GetEnumerator_m16160_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator4_t2599 * V_0 = {0};
	{
		U3CGetEnumeratorU3Ec__Iterator4_t2599 * L_0 = (U3CGetEnumeratorU3Ec__Iterator4_t2599 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (U3CGetEnumeratorU3Ec__Iterator4_t2599 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator4_t2599 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator4_t2599 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CGetEnumeratorU3Ec__Iterator4_t2599 * L_2 = V_0;
		return L_2;
	}
}
// T Common.Utils.SimpleList`1<System.Object>::get_Item(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern Il2CppCodeGenString* _stringLiteral3403;
extern "C" Object_t * SimpleList_1_get_Item_m16161_gshared (SimpleList_1_t2598 * __this, int32_t ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3403 = il2cpp_codegen_string_literal_from_index(3403);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___i;
		if ((((int32_t)0) > ((int32_t)L_0)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___i;
		int32_t L_2 = (int32_t)(__this->___size_2);
		G_B3_0 = ((((int32_t)L_1) < ((int32_t)L_2))? 1 : 0);
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		Assertion_Assert_m22(NULL /*static, unused*/, (bool)G_B3_0, (String_t*)_stringLiteral3403, /*hidden argument*/NULL);
		ObjectU5BU5D_t356* L_3 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_4 = ___i;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return (*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5, sizeof(Object_t *)));
	}
}
// System.Void Common.Utils.SimpleList`1<System.Object>::set_Item(System.Int32,T)
// System.Object
#include "mscorlib_System_Object.h"
extern Il2CppCodeGenString* _stringLiteral3403;
extern "C" void SimpleList_1_set_Item_m16163_gshared (SimpleList_1_t2598 * __this, int32_t ___i, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3403 = il2cpp_codegen_string_literal_from_index(3403);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___i;
		if ((((int32_t)0) > ((int32_t)L_0)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___i;
		int32_t L_2 = (int32_t)(__this->___size_2);
		G_B3_0 = ((((int32_t)L_1) < ((int32_t)L_2))? 1 : 0);
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		Assertion_Assert_m22(NULL /*static, unused*/, (bool)G_B3_0, (String_t*)_stringLiteral3403, /*hidden argument*/NULL);
		ObjectU5BU5D_t356* L_3 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_4 = ___i;
		Object_t * L_5 = ___value;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, L_4, sizeof(Object_t *))) = (Object_t *)L_5;
		return;
	}
}
// System.Void Common.Utils.SimpleList`1<System.Object>::AllocateMore()
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" void SimpleList_1_AllocateMore_m16165_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t356* V_0 = {0};
	ObjectU5BU5D_t356* G_B3_0 = {0};
	{
		ObjectU5BU5D_t356* L_0 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		ObjectU5BU5D_t356* L_1 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m3620(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))<<(int32_t)1)), (int32_t)4, /*hidden argument*/NULL);
		G_B3_0 = ((ObjectU5BU5D_t356*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_2));
		goto IL_002b;
	}

IL_0025:
	{
		G_B3_0 = ((ObjectU5BU5D_t356*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), 4));
	}

IL_002b:
	{
		V_0 = (ObjectU5BU5D_t356*)G_B3_0;
		ObjectU5BU5D_t356* L_3 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		if (!L_3)
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___size_2);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0050;
		}
	}
	{
		ObjectU5BU5D_t356* L_5 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		ObjectU5BU5D_t356* L_6 = V_0;
		NullCheck((Array_t *)L_5);
		VirtActionInvoker2< Array_t *, int32_t >::Invoke(8 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Array_t *)L_5, (Array_t *)(Array_t *)L_6, (int32_t)0);
	}

IL_0050:
	{
		ObjectU5BU5D_t356* L_7 = V_0;
		__this->___buffer_1 = L_7;
		return;
	}
}
// System.Void Common.Utils.SimpleList`1<System.Object>::Trim()
extern "C" void SimpleList_1_Trim_m16167_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t356* V_0 = {0};
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___size_2);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)(__this->___size_2);
		ObjectU5BU5D_t356* L_2 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)(((Array_t *)L_2)->max_length))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)(__this->___size_2);
		V_0 = (ObjectU5BU5D_t356*)((ObjectU5BU5D_t356*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		ObjectU5BU5D_t356* L_4 = V_0;
		int32_t L_5 = V_1;
		ObjectU5BU5D_t356* L_6 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_6, L_8, sizeof(Object_t *)));
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = (int32_t)(__this->___size_2);
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t356* L_12 = V_0;
		__this->___buffer_1 = L_12;
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->___buffer_1 = (ObjectU5BU5D_t356*)NULL;
	}

IL_0068:
	{
		return;
	}
}
// System.Void Common.Utils.SimpleList`1<System.Object>::Clear()
extern "C" void SimpleList_1_Clear_m16168_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method)
{
	{
		__this->___size_2 = 0;
		return;
	}
}
// System.Void Common.Utils.SimpleList`1<System.Object>::Release()
extern "C" void SimpleList_1_Release_m16170_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method)
{
	{
		__this->___size_2 = 0;
		__this->___buffer_1 = (ObjectU5BU5D_t356*)NULL;
		return;
	}
}
// System.Void Common.Utils.SimpleList`1<System.Object>::Add(T)
extern "C" void SimpleList_1_Add_m16171_gshared (SimpleList_1_t2598 * __this, Object_t * ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t356* L_0 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)(__this->___size_2);
		ObjectU5BU5D_t356* L_2 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)(((Array_t *)L_2)->max_length)))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((SimpleList_1_t2598 *)__this);
		(( void (*) (SimpleList_1_t2598 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((SimpleList_1_t2598 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		ObjectU5BU5D_t356* L_3 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_4 = (int32_t)(__this->___size_2);
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->___size_2 = ((int32_t)((int32_t)L_5+(int32_t)1));
		int32_t L_6 = V_0;
		Object_t * L_7 = ___item;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, L_6, sizeof(Object_t *))) = (Object_t *)L_7;
		return;
	}
}
// System.Void Common.Utils.SimpleList`1<System.Object>::Insert(System.Int32,T)
extern Il2CppCodeGenString* _stringLiteral3403;
extern "C" void SimpleList_1_Insert_m16173_gshared (SimpleList_1_t2598 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3403 = il2cpp_codegen_string_literal_from_index(3403);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___index;
		if ((((int32_t)0) > ((int32_t)L_0)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___index;
		int32_t L_2 = (int32_t)(__this->___size_2);
		G_B3_0 = ((((int32_t)L_1) < ((int32_t)L_2))? 1 : 0);
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		Assertion_Assert_m22(NULL /*static, unused*/, (bool)G_B3_0, (String_t*)_stringLiteral3403, /*hidden argument*/NULL);
		ObjectU5BU5D_t356* L_3 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___size_2);
		ObjectU5BU5D_t356* L_5 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		NullCheck(L_5);
		if ((!(((uint32_t)L_4) == ((uint32_t)(((int32_t)(((Array_t *)L_5)->max_length)))))))
		{
			goto IL_0041;
		}
	}

IL_003b:
	{
		NullCheck((SimpleList_1_t2598 *)__this);
		(( void (*) (SimpleList_1_t2598 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((SimpleList_1_t2598 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0041:
	{
		int32_t L_6 = ___index;
		int32_t L_7 = (int32_t)(__this->___size_2);
		if ((((int32_t)L_6) >= ((int32_t)L_7)))
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_8 = (int32_t)(__this->___size_2);
		V_0 = (int32_t)L_8;
		goto IL_0077;
	}

IL_0059:
	{
		ObjectU5BU5D_t356* L_9 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_10 = V_0;
		ObjectU5BU5D_t356* L_11 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12-(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12-(int32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, L_10, sizeof(Object_t *))) = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_11, L_13, sizeof(Object_t *)));
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14-(int32_t)1));
	}

IL_0077:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = ___index;
		if ((((int32_t)L_15) > ((int32_t)L_16)))
		{
			goto IL_0059;
		}
	}
	{
		ObjectU5BU5D_t356* L_17 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_18 = ___index;
		Object_t * L_19 = ___item;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, L_18, sizeof(Object_t *))) = (Object_t *)L_19;
		int32_t L_20 = (int32_t)(__this->___size_2);
		__this->___size_2 = ((int32_t)((int32_t)L_20+(int32_t)1));
		goto IL_00a5;
	}

IL_009e:
	{
		Object_t * L_21 = ___item;
		NullCheck((SimpleList_1_t2598 *)__this);
		(( void (*) (SimpleList_1_t2598 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((SimpleList_1_t2598 *)__this, (Object_t *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_00a5:
	{
		return;
	}
}
// System.Boolean Common.Utils.SimpleList`1<System.Object>::Contains(T)
extern "C" bool SimpleList_1_Contains_m16175_gshared (SimpleList_1_t2598 * __this, Object_t * ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t356* L_0 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		ObjectU5BU5D_t356* L_1 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Object_t * L_3 = ___item;
		NullCheck((Object_t *)(*((Object_t **)(Object_t **)SZArrayLdElema(L_1, L_2, sizeof(Object_t *)))));
		bool L_4 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)(*((Object_t **)(Object_t **)SZArrayLdElema(L_1, L_2, sizeof(Object_t *)))), (Object_t *)L_3);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		return 1;
	}

IL_0038:
	{
		int32_t L_5 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = (int32_t)(__this->___size_2);
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0014;
		}
	}
	{
		return 0;
	}
}
// System.Boolean Common.Utils.SimpleList`1<System.Object>::Remove(T)
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool SimpleList_1_Remove_m16177_gshared (SimpleList_1_t2598 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t2473 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Object_t * V_3 = {0};
	{
		ObjectU5BU5D_t356* L_0 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		if (!L_0)
		{
			goto IL_0096;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t2473 * L_1 = (( EqualityComparer_1_t2473 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t2473 *)L_1;
		V_1 = (int32_t)0;
		goto IL_008a;
	}

IL_0018:
	{
		EqualityComparer_1_t2473 * L_2 = V_0;
		ObjectU5BU5D_t356* L_3 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Object_t * L_6 = ___item;
		NullCheck((EqualityComparer_1_t2473 *)L_2);
		bool L_7 = (bool)VirtFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t2473 *)L_2, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5, sizeof(Object_t *))), (Object_t *)L_6);
		if (!L_7)
		{
			goto IL_0086;
		}
	}
	{
		int32_t L_8 = (int32_t)(__this->___size_2);
		__this->___size_2 = ((int32_t)((int32_t)L_8-(int32_t)1));
		ObjectU5BU5D_t356* L_9 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_10 = V_1;
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_3));
		Object_t * L_11 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, L_10, sizeof(Object_t *))) = (Object_t *)L_11;
		int32_t L_12 = V_1;
		V_2 = (int32_t)L_12;
		goto IL_0078;
	}

IL_005a:
	{
		ObjectU5BU5D_t356* L_13 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_14 = V_2;
		ObjectU5BU5D_t356* L_15 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, L_14, sizeof(Object_t *))) = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_15, L_17, sizeof(Object_t *)));
		int32_t L_18 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = (int32_t)(__this->___size_2);
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		return 1;
	}

IL_0086:
	{
		int32_t L_21 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_008a:
	{
		int32_t L_22 = V_1;
		int32_t L_23 = (int32_t)(__this->___size_2);
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0018;
		}
	}

IL_0096:
	{
		return 0;
	}
}
// System.Void Common.Utils.SimpleList`1<System.Object>::RemoveAt(System.Int32)
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3403;
extern "C" void SimpleList_1_RemoveAt_m16179_gshared (SimpleList_1_t2598 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral3403 = il2cpp_codegen_string_literal_from_index(3403);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Object_t * V_1 = {0};
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___index;
		if ((((int32_t)0) > ((int32_t)L_0)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___index;
		int32_t L_2 = (int32_t)(__this->___size_2);
		G_B3_0 = ((((int32_t)L_1) < ((int32_t)L_2))? 1 : 0);
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		Assertion_Assert_m22(NULL /*static, unused*/, (bool)G_B3_0, (String_t*)_stringLiteral3403, /*hidden argument*/NULL);
		ObjectU5BU5D_t356* L_3 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		if (!L_3)
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index;
		int32_t L_5 = (int32_t)(__this->___size_2);
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)(__this->___size_2);
		__this->___size_2 = ((int32_t)((int32_t)L_6-(int32_t)1));
		ObjectU5BU5D_t356* L_7 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_8 = ___index;
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_1));
		Object_t * L_9 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, L_8, sizeof(Object_t *))) = (Object_t *)L_9;
		int32_t L_10 = ___index;
		V_0 = (int32_t)L_10;
		goto IL_007c;
	}

IL_005e:
	{
		ObjectU5BU5D_t356* L_11 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_12 = V_0;
		ObjectU5BU5D_t356* L_13 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)((int32_t)L_14+(int32_t)1)));
		int32_t L_15 = ((int32_t)((int32_t)L_14+(int32_t)1));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, L_12, sizeof(Object_t *))) = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_13, L_15, sizeof(Object_t *)));
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_007c:
	{
		int32_t L_17 = V_0;
		int32_t L_18 = (int32_t)(__this->___size_2);
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_005e;
		}
	}

IL_0088:
	{
		return;
	}
}
// T[] Common.Utils.SimpleList`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t356* SimpleList_1_ToArray_m16181_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method)
{
	{
		NullCheck((SimpleList_1_t2598 *)__this);
		(( void (*) (SimpleList_1_t2598 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((SimpleList_1_t2598 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		ObjectU5BU5D_t356* L_0 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		return L_0;
	}
}
// System.Void Common.Utils.SimpleList`1<System.Object>::Sort(System.Comparison`1<T>)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_4.h"
extern "C" void SimpleList_1_Sort_m16183_gshared (SimpleList_1_t2598 * __this, Comparison_1_t2482 * ___comparer, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	Object_t * V_2 = {0};
	{
		V_0 = (bool)1;
		goto IL_007e;
	}

IL_0007:
	{
		V_0 = (bool)0;
		V_1 = (int32_t)1;
		goto IL_0072;
	}

IL_0010:
	{
		Comparison_1_t2482 * L_0 = ___comparer;
		ObjectU5BU5D_t356* L_1 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, ((int32_t)((int32_t)L_2-(int32_t)1)));
		int32_t L_3 = ((int32_t)((int32_t)L_2-(int32_t)1));
		ObjectU5BU5D_t356* L_4 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((Comparison_1_t2482 *)L_0);
		int32_t L_7 = (( int32_t (*) (Comparison_1_t2482 *, Object_t *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Comparison_1_t2482 *)L_0, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_1, L_3, sizeof(Object_t *))), (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_4, L_6, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		ObjectU5BU5D_t356* L_8 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_2 = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_8, L_10, sizeof(Object_t *)));
		ObjectU5BU5D_t356* L_11 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_12 = V_1;
		ObjectU5BU5D_t356* L_13 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_14 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)));
		int32_t L_15 = ((int32_t)((int32_t)L_14-(int32_t)1));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, L_12, sizeof(Object_t *))) = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_13, L_15, sizeof(Object_t *)));
		ObjectU5BU5D_t356* L_16 = (ObjectU5BU5D_t356*)(__this->___buffer_1);
		int32_t L_17 = V_1;
		Object_t * L_18 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17-(int32_t)1)));
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, ((int32_t)((int32_t)L_17-(int32_t)1)), sizeof(Object_t *))) = (Object_t *)L_18;
		V_0 = (bool)1;
	}

IL_006e:
	{
		int32_t L_19 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = (int32_t)(__this->___size_2);
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_0010;
		}
	}

IL_007e:
	{
		bool L_22 = V_0;
		if (L_22)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Int32 Common.Utils.SimpleList`1<System.Object>::get_Count()
extern "C" int32_t SimpleList_1_get_Count_m16184_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___size_2);
		return L_0;
	}
}
// System.Boolean Common.Utils.SimpleList`1<System.Object>::IsEmpty()
extern "C" bool SimpleList_1_IsEmpty_m16186_gshared (SimpleList_1_t2598 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___size_2);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Common.Utils.SimpleList`1<System.Object>::TraverseItems(Common.Utils.SimpleList`1/Visitor<T>)
// Common.Utils.SimpleList`1/Visitor<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_Visitor_gen.h"
extern "C" void SimpleList_1_TraverseItems_m16188_gshared (SimpleList_1_t2598 * __this, Visitor_t2600 * ___visitor, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		NullCheck((SimpleList_1_t2598 *)__this);
		int32_t L_0 = (( int32_t (*) (SimpleList_1_t2598 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((SimpleList_1_t2598 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (int32_t)L_0;
		V_1 = (int32_t)0;
		goto IL_001f;
	}

IL_000e:
	{
		Visitor_t2600 * L_1 = ___visitor;
		int32_t L_2 = V_1;
		NullCheck((SimpleList_1_t2598 *)__this);
		Object_t * L_3 = (( Object_t * (*) (SimpleList_1_t2598 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((SimpleList_1_t2598 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((Visitor_t2600 *)L_1);
		(( void (*) (Visitor_t2600 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Visitor_t2600 *)L_1, (Object_t *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		int32_t L_4 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
// System.Void Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void U3CGetEnumeratorU3Ec__Iterator4__ctor_m16189_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m16190_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___U24current_2);
		return L_0;
	}
}
// System.Object Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m16191_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator4_MoveNext_m16192_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)(__this->___U24PC_1);
		V_0 = (uint32_t)L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		SimpleList_1_t2598 * L_2 = (SimpleList_1_t2598 *)(__this->___U3CU3Ef__this_3);
		NullCheck(L_2);
		ObjectU5BU5D_t356* L_3 = (ObjectU5BU5D_t356*)(L_2->___buffer_1);
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->___U3CiU3E__0_0 = 0;
		goto IL_0073;
	}

IL_003d:
	{
		SimpleList_1_t2598 * L_4 = (SimpleList_1_t2598 *)(__this->___U3CU3Ef__this_3);
		NullCheck(L_4);
		ObjectU5BU5D_t356* L_5 = (ObjectU5BU5D_t356*)(L_4->___buffer_1);
		int32_t L_6 = (int32_t)(__this->___U3CiU3E__0_0);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->___U24current_2 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *)));
		__this->___U24PC_1 = 1;
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_8 = (int32_t)(__this->___U3CiU3E__0_0);
		__this->___U3CiU3E__0_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0073:
	{
		int32_t L_9 = (int32_t)(__this->___U3CiU3E__0_0);
		SimpleList_1_t2598 * L_10 = (SimpleList_1_t2598 *)(__this->___U3CU3Ef__this_3);
		NullCheck(L_10);
		int32_t L_11 = (int32_t)(L_10->___size_2);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->___U24PC_1 = (-1);
	}

IL_0090:
	{
		return 0;
	}

IL_0092:
	{
		return 1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator4_Dispose_m16193_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::Reset()
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern "C" void U3CGetEnumeratorU3Ec__Iterator4_Reset_m16194_gshared (U3CGetEnumeratorU3Ec__Iterator4_t2599 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1366(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
// System.Void Common.Utils.SimpleList`1/Visitor<System.Object>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Visitor__ctor_m16195_gshared (Visitor_t2600 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void Common.Utils.SimpleList`1/Visitor<System.Object>::Invoke(T)
extern "C" void Visitor_Invoke_m16196_gshared (Visitor_t2600 * __this, Object_t * ___item, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Visitor_Invoke_m16196((Visitor_t2600 *)__this->___prev_9,___item, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___item, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___item,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___item, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___item,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___item,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult Common.Utils.SimpleList`1/Visitor<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * Visitor_BeginInvoke_m16197_gshared (Visitor_t2600 * __this, Object_t * ___item, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___item;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void Common.Utils.SimpleList`1/Visitor<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void Visitor_EndInvoke_m16198_gshared (Visitor_t2600 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Common.Utils.ImmutableList`1<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_ImmutableList_1_gen.h"
// Common.Utils.ImmutableList`1<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_ImmutableList_1_genMethodDeclarations.h"
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_5.h"
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"
// System.Void Common.Utils.ImmutableList`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void ImmutableList_1__ctor_m16657_gshared (ImmutableList_1_t2632 * __this, Object_t* ___elements, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t* V_1 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		List_1_t344 * L_0 = (List_1_t344 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (List_1_t344 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->___list_0 = L_0;
		Object_t* L_1 = ___elements;
		NullCheck((Object_t*)L_1);
		Object_t* L_2 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_1);
		V_1 = (Object_t*)L_2;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_001d:
		{
			Object_t* L_3 = V_1;
			NullCheck((Object_t*)L_3);
			Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)L_3);
			V_0 = (Object_t *)L_4;
			List_1_t344 * L_5 = (List_1_t344 *)(__this->___list_0);
			Object_t * L_6 = V_0;
			NullCheck((List_1_t344 *)L_5);
			VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t344 *)L_5, (Object_t *)L_6);
		}

IL_0030:
		{
			Object_t* L_7 = V_1;
			NullCheck((Object_t *)L_7);
			bool L_8 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, (Object_t *)L_7);
			if (L_8)
			{
				goto IL_001d;
			}
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x4B, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		{
			Object_t* L_9 = V_1;
			if (L_9)
			{
				goto IL_0044;
			}
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(64)
		}

IL_0044:
		{
			Object_t* L_10 = V_1;
			NullCheck((Object_t *)L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, (Object_t *)L_10);
			IL2CPP_END_FINALLY(64)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_004b:
	{
		return;
	}
}
// System.Int32 Common.Utils.ImmutableList`1<System.Object>::get_Count()
extern "C" int32_t ImmutableList_1_get_Count_m16658_gshared (ImmutableList_1_t2632 * __this, const MethodInfo* method)
{
	{
		List_1_t344 * L_0 = (List_1_t344 *)(__this->___list_0);
		NullCheck((List_1_t344 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count() */, (List_1_t344 *)L_0);
		return L_1;
	}
}
// T Common.Utils.ImmutableList`1<System.Object>::GetAt(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" Object_t * ImmutableList_1_GetAt_m16659_gshared (ImmutableList_1_t2632 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		List_1_t344 * L_0 = (List_1_t344 *)(__this->___list_0);
		int32_t L_1 = ___index;
		NullCheck((List_1_t344 *)L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32) */, (List_1_t344 *)L_0, (int32_t)L_1);
		return L_2;
	}
}
// Common.Utils.Pool`1<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_Pool_1_gen.h"
// Common.Utils.Pool`1<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_Pool_1_genMethodDeclarations.h"
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
struct Object_t;
// System.Activator
#include "mscorlib_System_Activator.h"
// Declaration !!0 System.Activator::CreateInstance<System.Object>()
// !!0 System.Activator::CreateInstance<System.Object>()
extern "C" Object_t * Activator_CreateInstance_TisObject_t_m30216_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisObject_t_m30216(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisObject_t_m30216_gshared)(__this /* static, unused */, method)
// System.Void Common.Utils.Pool`1<System.Object>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void Pool_1__ctor_m16660_gshared (Pool_1_t2633 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		SimpleList_1_t2598 * L_0 = (SimpleList_1_t2598 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (SimpleList_1_t2598 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->___pooledList_0 = L_0;
		return;
	}
}
// T Common.Utils.Pool`1<System.Object>::Request()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" Object_t * Pool_1_Request_m16661_gshared (Pool_1_t2633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * G_B4_0 = {0};
	{
		SimpleList_1_t2598 * L_0 = (SimpleList_1_t2598 *)(__this->___pooledList_0);
		NullCheck((SimpleList_1_t2598 *)L_0);
		bool L_1 = (( bool (*) (SimpleList_1_t2598 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((SimpleList_1_t2598 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_1));
		Object_t * L_2 = V_1;
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_1));
		Object_t * L_3 = V_1;
		G_B4_0 = L_3;
		goto IL_0033;
	}

IL_002e:
	{
		Object_t * L_4 = (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = L_4;
	}

IL_0033:
	{
		return G_B4_0;
	}

IL_0034:
	{
		SimpleList_1_t2598 * L_5 = (SimpleList_1_t2598 *)(__this->___pooledList_0);
		SimpleList_1_t2598 * L_6 = (SimpleList_1_t2598 *)(__this->___pooledList_0);
		NullCheck((SimpleList_1_t2598 *)L_6);
		int32_t L_7 = (( int32_t (*) (SimpleList_1_t2598 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((SimpleList_1_t2598 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((SimpleList_1_t2598 *)L_5);
		Object_t * L_8 = (( Object_t * (*) (SimpleList_1_t2598 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((SimpleList_1_t2598 *)L_5, (int32_t)((int32_t)((int32_t)L_7-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (Object_t *)L_8;
		SimpleList_1_t2598 * L_9 = (SimpleList_1_t2598 *)(__this->___pooledList_0);
		SimpleList_1_t2598 * L_10 = (SimpleList_1_t2598 *)(__this->___pooledList_0);
		NullCheck((SimpleList_1_t2598 *)L_10);
		int32_t L_11 = (( int32_t (*) (SimpleList_1_t2598 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((SimpleList_1_t2598 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((SimpleList_1_t2598 *)L_9);
		(( void (*) (SimpleList_1_t2598 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((SimpleList_1_t2598 *)L_9, (int32_t)((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Object_t * L_12 = V_0;
		return L_12;
	}
}
// System.Void Common.Utils.Pool`1<System.Object>::Return(T)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" void Pool_1_Return_m16662_gshared (Pool_1_t2633 * __this, Object_t * ___instance, const MethodInfo* method)
{
	{
		SimpleList_1_t2598 * L_0 = (SimpleList_1_t2598 *)(__this->___pooledList_0);
		Object_t * L_1 = ___instance;
		NullCheck((SimpleList_1_t2598 *)L_0);
		(( void (*) (SimpleList_1_t2598 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((SimpleList_1_t2598 *)L_0, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// Common.Utils.SerializedKeyValue`2<System.Object,System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SerializedKeyValue_2_gen.h"
// Common.Utils.SerializedKeyValue`2<System.Object,System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SerializedKeyValue_2_genMethodDeclarations.h"
// System.Void Common.Utils.SerializedKeyValue`2<System.Object,System.Object>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void SerializedKeyValue_2__ctor_m16663_gshared (SerializedKeyValue_2_t2634 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// K Common.Utils.SerializedKeyValue`2<System.Object,System.Object>::get_Key()
extern "C" Object_t * SerializedKeyValue_2_get_Key_m16664_gshared (SerializedKeyValue_2_t2634 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___key_0);
		return L_0;
	}
}
// V Common.Utils.SerializedKeyValue`2<System.Object,System.Object>::get_Value()
extern "C" Object_t * SerializedKeyValue_2_get_Value_m16665_gshared (SerializedKeyValue_2_t2634 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___value_1);
		return L_0;
	}
}
// System.Array/InternalEnumerator`1<System.Byte>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_26.h"
// System.Array/InternalEnumerator`1<System.Byte>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_26MethodDeclarations.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// Declaration !!0 System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C" uint8_t Array_InternalArray__get_Item_TisByte_t391_m30217_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t391_m30217(__this, p0, method) (( uint8_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t391_m30217_gshared)(__this, p0, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
// System.Array
#include "mscorlib_System_Array.h"
extern "C" void InternalEnumerator_1__ctor_m16666_gshared (InternalEnumerator_1_t2635 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16667_gshared (InternalEnumerator_1_t2635 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (( uint8_t (*) (InternalEnumerator_1_t2635 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2635 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16668_gshared (InternalEnumerator_1_t2635 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern "C" bool InternalEnumerator_1_MoveNext_m16669_gshared (InternalEnumerator_1_t2635 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9469((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3382;
extern Il2CppCodeGenString* _stringLiteral3383;
extern "C" uint8_t InternalEnumerator_1_get_Current_m16670_gshared (InternalEnumerator_1_t2635 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3382 = il2cpp_codegen_string_literal_from_index(3382);
		_stringLiteral3383 = il2cpp_codegen_string_literal_from_index(3383);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, (String_t*)_stringLiteral3382, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t425 * L_3 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_3, (String_t*)_stringLiteral3383, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		uint8_t L_8 = (( uint8_t (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// UnityThreading.Task`1<System.Object>
#include "AssemblyU2DCSharp_UnityThreading_Task_1_gen.h"
// UnityThreading.Task`1<System.Object>
#include "AssemblyU2DCSharp_UnityThreading_Task_1_genMethodDeclarations.h"
// System.Func`1<System.Object>
#include "System_Core_System_Func_1_gen_0.h"
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBaseMethodDeclarations.h"
// System.Func`1<System.Object>
#include "System_Core_System_Func_1_gen_0MethodDeclarations.h"
// System.Void UnityThreading.Task`1<System.Object>::.ctor(System.Func`1<T>)
// System.Func`1<System.Object>
#include "System_Core_System_Func_1_gen_0.h"
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBaseMethodDeclarations.h"
extern "C" void Task_1__ctor_m16945_gshared (Task_1_t2649 * __this, Func_1_t2650 * ___function, const MethodInfo* method)
{
	{
		NullCheck((TaskBase_t163 *)__this);
		TaskBase__ctor_m550((TaskBase_t163 *)__this, /*hidden argument*/NULL);
		Func_1_t2650 * L_0 = ___function;
		__this->___function_4 = L_0;
		return;
	}
}
// System.Collections.IEnumerator UnityThreading.Task`1<System.Object>::Do()
extern "C" Object_t * Task_1_Do_m16946_gshared (Task_1_t2649 * __this, const MethodInfo* method)
{
	{
		Func_1_t2650 * L_0 = (Func_1_t2650 *)(__this->___function_4);
		NullCheck((Func_1_t2650 *)L_0);
		Object_t * L_1 = (( Object_t * (*) (Func_1_t2650 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t2650 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___result_5 = L_1;
		return (Object_t *)NULL;
	}
}
// T UnityThreading.Task`1<System.Object>::get_Result()
extern "C" Object_t * Task_1_get_Result_m16947_gshared (Task_1_t2649 * __this, const MethodInfo* method)
{
	{
		NullCheck((TaskBase_t163 *)__this);
		bool L_0 = TaskBase_get_HasEnded_m552((TaskBase_t163 *)__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((TaskBase_t163 *)__this);
		TaskBase_Wait_m558((TaskBase_t163 *)__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		Object_t * L_1 = (Object_t *)(__this->___result_5);
		return L_1;
	}
}
// System.Void System.Func`1<System.Object>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Func_1__ctor_m16948_gshared (Func_1_t2650 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TResult System.Func`1<System.Object>::Invoke()
extern "C" Object_t * Func_1_Invoke_m16949_gshared (Func_1_t2650 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Func_1_Invoke_m16949((Func_1_t2650 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Func`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * Func_1_BeginInvoke_m16950_gshared (Func_1_t2650 * __this, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TResult System.Func`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Func_1_EndInvoke_m16951_gshared (Func_1_t2650 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// UnityThreading.Dispatcher/<CreateSafeFunction>c__AnonStorey10`1<System.Object>
#include "AssemblyU2DCSharp_UnityThreading_Dispatcher_U3CCreateSafeFun.h"
// UnityThreading.Dispatcher/<CreateSafeFunction>c__AnonStorey10`1<System.Object>
#include "AssemblyU2DCSharp_UnityThreading_Dispatcher_U3CCreateSafeFunMethodDeclarations.h"
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBase.h"
// UnityThreading.Dispatcher
#include "AssemblyU2DCSharp_UnityThreading_DispatcherMethodDeclarations.h"
// System.Void UnityThreading.Dispatcher/<CreateSafeFunction>c__AnonStorey10`1<System.Object>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void U3CCreateSafeFunctionU3Ec__AnonStorey10_1__ctor_m16952_gshared (U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t2651 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T UnityThreading.Dispatcher/<CreateSafeFunction>c__AnonStorey10`1<System.Object>::<>m__4()
// UnityThreading.Dispatcher
#include "AssemblyU2DCSharp_UnityThreading_DispatcherMethodDeclarations.h"
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBaseMethodDeclarations.h"
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" Object_t * U3CCreateSafeFunctionU3Ec__AnonStorey10_1_U3CU3Em__4_m16953_gshared (U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t2651 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Func_1_t2650 * L_0 = (Func_1_t2650 *)(__this->___function_0);
			NullCheck((Func_1_t2650 *)L_0);
			Object_t * L_1 = (( Object_t * (*) (Func_1_t2650 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_1_t2650 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			V_0 = (Object_t *)L_1;
			goto IL_0035;
		}

IL_0011:
		{
			; // IL_0011: leave IL_0035
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t359 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0016;
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Object)
		{
			TaskBase_t163 * L_2 = Dispatcher_get_CurrentTask_m537(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck((TaskBase_t163 *)L_2);
			TaskBase_Abort_m555((TaskBase_t163 *)L_2, /*hidden argument*/NULL);
			Initobj (Object_t_il2cpp_TypeInfo_var, (&V_1));
			Object_t * L_3 = V_1;
			V_0 = (Object_t *)L_3;
			goto IL_0035;
		}

IL_0030:
		{
			; // IL_0030: leave IL_0035
		}
	} // end catch (depth: 1)

IL_0035:
	{
		Object_t * L_4 = V_0;
		return L_4;
	}
}
// UnityThreading.Dispatcher/<CreateSafeAction>c__AnonStorey11`1<System.Object>
#include "AssemblyU2DCSharp_UnityThreading_Dispatcher_U3CCreateSafeAct.h"
// UnityThreading.Dispatcher/<CreateSafeAction>c__AnonStorey11`1<System.Object>
#include "AssemblyU2DCSharp_UnityThreading_Dispatcher_U3CCreateSafeActMethodDeclarations.h"
// System.Action
#include "System_Core_System_Action.h"
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
// System.Void UnityThreading.Dispatcher/<CreateSafeAction>c__AnonStorey11`1<System.Object>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void U3CCreateSafeActionU3Ec__AnonStorey11_1__ctor_m16954_gshared (U3CCreateSafeActionU3Ec__AnonStorey11_1_t2652 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityThreading.Dispatcher/<CreateSafeAction>c__AnonStorey11`1<System.Object>::<>m__5()
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
// UnityThreading.Dispatcher
#include "AssemblyU2DCSharp_UnityThreading_DispatcherMethodDeclarations.h"
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBaseMethodDeclarations.h"
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" void U3CCreateSafeActionU3Ec__AnonStorey11_1_U3CU3Em__5_m16955_gshared (U3CCreateSafeActionU3Ec__AnonStorey11_1_t2652 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Action_t16 * L_0 = (Action_t16 *)(__this->___action_0);
		NullCheck((Action_t16 *)L_0);
		Action_Invoke_m1360((Action_t16 *)L_0, /*hidden argument*/NULL);
		goto IL_0020;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t359 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0010;
		throw e;
	}

CATCH_0010:
	{ // begin catch(System.Object)
		TaskBase_t163 * L_1 = Dispatcher_get_CurrentTask_m537(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((TaskBase_t163 *)L_1);
		TaskBase_Abort_m555((TaskBase_t163 *)L_1, /*hidden argument*/NULL);
		goto IL_0020;
	} // end catch (depth: 1)

IL_0020:
	{
		return;
	}
}
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_10.h"
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_10MethodDeclarations.h"
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Action_1__ctor_m16966_gshared (Action_1_t2655 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C" void Action_1_Invoke_m16967_gshared (Action_1_t2655 * __this, Object_t * ___obj, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Action_1_Invoke_m16967((Action_1_t2655 *)__this->___prev_9,___obj, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___obj, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___obj, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * Action_1_BeginInvoke_m16969_gshared (Action_1_t2655 * __this, Object_t * ___obj, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m16971_gshared (Action_1_t2655 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Func`2<System.Object,System.Object>
#include "System_Core_System_Func_2_gen_4.h"
// System.Func`2<System.Object,System.Object>
#include "System_Core_System_Func_2_gen_4MethodDeclarations.h"
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Func_2__ctor_m16972_gshared (Func_2_t2656 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
extern "C" Object_t * Func_2_Invoke_m16973_gshared (Func_2_t2656 * __this, Object_t * ___arg1, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Func_2_Invoke_m16973((Func_2_t2656 *)__this->___prev_9,___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___arg1, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, Object_t * ___arg1, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * Func_2_BeginInvoke_m16975_gshared (Func_2_t2656 * __this, Object_t * ___arg1, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg1;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TResult System.Func`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Func_2_EndInvoke_m16977_gshared (Func_2_t2656 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// System.Func`2<System.Object,System.Boolean>
#include "System_Core_System_Func_2_gen_5.h"
// System.Func`2<System.Object,System.Boolean>
#include "System_Core_System_Func_2_gen_5MethodDeclarations.h"
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Func_2__ctor_m17070_gshared (Func_2_t432 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TResult System.Func`2<System.Object,System.Boolean>::Invoke(T)
extern "C" bool Func_2_Invoke_m17072_gshared (Func_2_t432 * __this, Object_t * ___arg1, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Func_2_Invoke_m17072((Func_2_t432 *)__this->___prev_9,___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___arg1, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, Object_t * ___arg1, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * Func_2_BeginInvoke_m17074_gshared (Func_2_t432 * __this, Object_t * ___arg1, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg1;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TResult System.Func`2<System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C" bool Func_2_EndInvoke_m17076_gshared (Func_2_t432 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereIteratorU3E.h"
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereIteratorU3EMethodDeclarations.h"
// System.Threading.Interlocked
#include "mscorlib_System_Threading_InterlockedMethodDeclarations.h"
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m17080_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m17081_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___U24current_5);
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m17082_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___U24current_5);
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m17083_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 *)__this);
		Object_t* L_0 = (( Object_t* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// System.Threading.Interlocked
#include "mscorlib_System_Threading_InterlockedMethodDeclarations.h"
extern "C" Object_t* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m17084_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * V_0 = {0};
	{
		int32_t* L_0 = (int32_t*)&(__this->___U24PC_4);
		int32_t L_1 = Interlocked_CompareExchange_m1362(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * L_3 = V_0;
		Object_t* L_4 = (Object_t*)(__this->___U3CU24U3Esource_6);
		NullCheck(L_3);
		L_3->___source_0 = L_4;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * L_5 = V_0;
		Func_2_t432 * L_6 = (Func_2_t432 *)(__this->___U3CU24U3Epredicate_7);
		NullCheck(L_5);
		L_5->___predicate_3 = L_6;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m17085_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)(__this->___U24PC_4);
		V_0 = (uint32_t)L_0;
		__this->___U24PC_4 = (-1);
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Object_t* L_2 = (Object_t*)(__this->___source_0);
		NullCheck((Object_t*)L_2);
		Object_t* L_3 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Object_t*)L_2);
		__this->___U3CU24s_97U3E__0_1 = L_3;
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0089;
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Object_t* L_5 = (Object_t*)(__this->___U3CU24s_97U3E__0_1);
			NullCheck((Object_t*)L_5);
			Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Object_t*)L_5);
			__this->___U3CelementU3E__1_2 = L_6;
			Func_2_t432 * L_7 = (Func_2_t432 *)(__this->___predicate_3);
			Object_t * L_8 = (Object_t *)(__this->___U3CelementU3E__1_2);
			NullCheck((Func_2_t432 *)L_7);
			bool L_9 = (( bool (*) (Func_2_t432 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Func_2_t432 *)L_7, (Object_t *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			Object_t * L_10 = (Object_t *)(__this->___U3CelementU3E__1_2);
			__this->___U24current_5 = L_10;
			__this->___U24PC_4 = 1;
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Object_t* L_11 = (Object_t*)(__this->___U3CU24s_97U3E__0_1);
			NullCheck((Object_t *)L_11);
			bool L_12 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, (Object_t *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Object_t* L_14 = (Object_t*)(__this->___U3CU24s_97U3E__0_1);
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Object_t* L_15 = (Object_t*)(__this->___U3CU24s_97U3E__0_1);
			NullCheck((Object_t *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, (Object_t *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_00b7:
	{
		__this->___U24PC_4 = (-1);
	}

IL_00be:
	{
		return 0;
	}

IL_00c0:
	{
		return 1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m17086_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2661 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)(__this->___U24PC_4);
		V_0 = (uint32_t)L_0;
		__this->___U24PC_4 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Object_t* L_2 = (Object_t*)(__this->___U3CU24s_97U3E__0_1);
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Object_t* L_3 = (Object_t*)(__this->___U3CU24s_97U3E__0_1);
			NullCheck((Object_t *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, (Object_t *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_43.h"
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_43MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_16.h"
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_16.h"
// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_17.h"
// System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_1.h"
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_Link.h"
#include "Assembly-CSharp_ArrayTypes.h"
// System.Collections.Generic.KeyNotFoundException
#include "mscorlib_System_Collections_Generic_KeyNotFoundException.h"
// System.Collections.Generic.EqualityComparer`1<EScreens>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_1.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_40.h"
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_16MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_17MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_1MethodDeclarations.h"
// System.Collections.Generic.KeyNotFoundException
#include "mscorlib_System_Collections_Generic_KeyNotFoundExceptionMethodDeclarations.h"
// System.Collections.Generic.EqualityComparer`1<EScreens>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_1MethodDeclarations.h"
// System.Collections.Hashtable
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_16MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_40MethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
struct DictionaryEntryU5BU5D_t3822;
struct Transform_1_t2666;
// Declaration System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t451_TisDictionaryEntry_t451_m30268_gshared (Dictionary_2_t2665 * __this, DictionaryEntryU5BU5D_t3822* p0, int32_t p1, Transform_1_t2666 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisDictionaryEntry_t451_TisDictionaryEntry_t451_m30268(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2665 *, DictionaryEntryU5BU5D_t3822*, int32_t, Transform_1_t2666 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisDictionaryEntry_t451_TisDictionaryEntry_t451_m30268_gshared)(__this, p0, p1, p2, method)
struct Array_t;
struct Transform_1_t2677;
// Declaration System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2667_m30269_gshared (Dictionary_2_t2665 * __this, Array_t * p0, int32_t p1, Transform_1_t2677 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2667_m30269(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, Transform_1_t2677 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2667_m30269_gshared)(__this, p0, p1, p2, method)
struct KeyValuePair_2U5BU5D_t3649;
struct Transform_1_t2677;
// Declaration System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2667_TisKeyValuePair_2_t2667_m30271_gshared (Dictionary_2_t2665 * __this, KeyValuePair_2U5BU5D_t3649* p0, int32_t p1, Transform_1_t2677 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2667_TisKeyValuePair_2_t2667_m30271(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2665 *, KeyValuePair_2U5BU5D_t3649*, int32_t, Transform_1_t2677 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2667_TisKeyValuePair_2_t2667_m30271_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void Dictionary_2__ctor_m17087_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2665 *)__this, (int32_t)((int32_t)10), (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17089_gshared (Dictionary_2_t2665 * __this, Object_t* ___comparer, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___comparer;
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2665 *)__this, (int32_t)((int32_t)10), (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::.ctor(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void Dictionary_2__ctor_m17091_gshared (Dictionary_2_t2665 * __this, int32_t ___capacity, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity;
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2665 *)__this, (int32_t)L_0, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
extern "C" void Dictionary_2__ctor_m17093_gshared (Dictionary_2_t2665 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		SerializationInfo_t1012 * L_0 = ___info;
		__this->___serialization_info_13 = L_0;
		return;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m17095_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t2665 *)__this);
		KeyCollection_t2670 * L_0 = (( KeyCollection_t2670 * (*) (Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Dictionary_2_t2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17097_gshared (Dictionary_2_t2665 * __this, Object_t * ___key, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		if (!((Object_t *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_1 = ___key;
		NullCheck((Dictionary_2_t2665 *)__this);
		bool L_2 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::ContainsKey(TKey) */, (Dictionary_2_t2665 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t2665 *)__this);
		int32_t L_4 = (( int32_t (*) (Dictionary_2_t2665 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Dictionary_2_t2665 *)__this, (Object_t *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Dictionary_2_t2665 *)__this);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(19 /* TValue System.Collections.Generic.Dictionary`2<EScreens,System.Object>::get_Item(TKey) */, (Dictionary_2_t2665 *)__this, (int32_t)L_4);
		return L_5;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17099_gshared (Dictionary_2_t2665 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((Dictionary_2_t2665 *)__this);
		int32_t L_1 = (( int32_t (*) (Dictionary_2_t2665 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Dictionary_2_t2665 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Object_t * L_2 = ___value;
		NullCheck((Dictionary_2_t2665 *)__this);
		Object_t * L_3 = (( Object_t * (*) (Dictionary_2_t2665 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Dictionary_2_t2665 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Dictionary_2_t2665 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::set_Item(TKey,TValue) */, (Dictionary_2_t2665 *)__this, (int32_t)L_1, (Object_t *)L_3);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17101_gshared (Dictionary_2_t2665 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((Dictionary_2_t2665 *)__this);
		int32_t L_1 = (( int32_t (*) (Dictionary_2_t2665 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Dictionary_2_t2665 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Object_t * L_2 = ___value;
		NullCheck((Dictionary_2_t2665 *)__this);
		Object_t * L_3 = (( Object_t * (*) (Dictionary_2_t2665 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Dictionary_2_t2665 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Dictionary_2_t2665 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Add(TKey,TValue) */, (Dictionary_2_t2665 *)__this, (int32_t)L_1, (Object_t *)L_3);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.IDictionary.Contains(System.Object)
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17103_gshared (Dictionary_2_t2665 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (!((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t2665 *)__this);
		bool L_4 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::ContainsKey(TKey) */, (Dictionary_2_t2665 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))));
		return L_4;
	}

IL_0029:
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17105_gshared (Dictionary_2_t2665 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (!((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t2665 *)__this);
		VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Remove(TKey) */, (Dictionary_2_t2665 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17107_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17109_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17111_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17113_gshared (Dictionary_2_t2665 * __this, KeyValuePair_2_t2667  ___keyValuePair, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t2667 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Object_t * L_1 = (( Object_t * (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((KeyValuePair_2_t2667 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((Dictionary_2_t2665 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Add(TKey,TValue) */, (Dictionary_2_t2665 *)__this, (int32_t)L_0, (Object_t *)L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17115_gshared (Dictionary_2_t2665 * __this, KeyValuePair_2_t2667  ___keyValuePair, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2667  L_0 = ___keyValuePair;
		NullCheck((Dictionary_2_t2665 *)__this);
		bool L_1 = (( bool (*) (Dictionary_2_t2665 *, KeyValuePair_2_t2667 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Dictionary_2_t2665 *)__this, (KeyValuePair_2_t2667 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#include "mscorlib_ArrayTypes.h"
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17117_gshared (Dictionary_2_t2665 * __this, KeyValuePair_2U5BU5D_t3649* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t3649* L_0 = ___array;
		int32_t L_1 = ___index;
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, KeyValuePair_2U5BU5D_t3649*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Dictionary_2_t2665 *)__this, (KeyValuePair_2U5BU5D_t3649*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17119_gshared (Dictionary_2_t2665 * __this, KeyValuePair_2_t2667  ___keyValuePair, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2667  L_0 = ___keyValuePair;
		NullCheck((Dictionary_2_t2665 *)__this);
		bool L_1 = (( bool (*) (Dictionary_2_t2665 *, KeyValuePair_2_t2667 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Dictionary_2_t2665 *)__this, (KeyValuePair_2_t2667 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t2667 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((Dictionary_2_t2665 *)__this);
		bool L_3 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Remove(TKey) */, (Dictionary_2_t2665 *)__this, (int32_t)L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_Array.h"
extern TypeInfo* DictionaryEntryU5BU5D_t3822_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17121_gshared (Dictionary_2_t2665 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DictionaryEntryU5BU5D_t3822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4051);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t3649* V_0 = {0};
	DictionaryEntryU5BU5D_t3822* V_1 = {0};
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t3822* G_B5_1 = {0};
	Dictionary_2_t2665 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t3822* G_B4_1 = {0};
	Dictionary_2_t2665 * G_B4_2 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (KeyValuePair_2U5BU5D_t3649*)((KeyValuePair_2U5BU5D_t3649*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)));
		KeyValuePair_2U5BU5D_t3649* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t3649* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, KeyValuePair_2U5BU5D_t3649*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Dictionary_2_t2665 *)__this, (KeyValuePair_2U5BU5D_t3649*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return;
	}

IL_0016:
	{
		Array_t * L_4 = ___array;
		int32_t L_5 = ___index;
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Dictionary_2_t2665 *)__this, (Array_t *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		Array_t * L_6 = ___array;
		V_1 = (DictionaryEntryU5BU5D_t3822*)((DictionaryEntryU5BU5D_t3822*)IsInst(L_6, DictionaryEntryU5BU5D_t3822_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t3822* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t3822* L_8 = V_1;
		int32_t L_9 = ___index;
		Transform_1_t2666 * L_10 = ((Dictionary_2_t2665_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->static_fields)->___U3CU3Ef__amU24cacheB_15;
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t2665 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t2665 *)(__this));
			goto IL_0046;
		}
	}
	{
		IntPtr_t L_11 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18) };
		Transform_1_t2666 * L_12 = (Transform_1_t2666 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		(( void (*) (Transform_1_t2666 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)(L_12, (Object_t *)NULL, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		((Dictionary_2_t2665_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->static_fields)->___U3CU3Ef__amU24cacheB_15 = L_12;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t2665 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t2666 * L_13 = ((Dictionary_2_t2665_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->static_fields)->___U3CU3Ef__amU24cacheB_15;
		NullCheck((Dictionary_2_t2665 *)G_B5_2);
		(( void (*) (Dictionary_2_t2665 *, DictionaryEntryU5BU5D_t3822*, int32_t, Transform_1_t2666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((Dictionary_2_t2665 *)G_B5_2, (DictionaryEntryU5BU5D_t3822*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t2666 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		return;
	}

IL_0051:
	{
		Array_t * L_14 = ___array;
		int32_t L_15 = ___index;
		IntPtr_t L_16 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22) };
		Transform_1_t2677 * L_17 = (Transform_1_t2677 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		(( void (*) (Transform_1_t2677 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(L_17, (Object_t *)NULL, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, Transform_1_t2677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)((Dictionary_2_t2665 *)__this, (Array_t *)L_14, (int32_t)L_15, (Transform_1_t2677 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17123_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672  L_0 = {0};
		(( void (*) (Enumerator_t2672 *, Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(&L_0, (Dictionary_2_t2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		Enumerator_t2672  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17125_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672  L_0 = {0};
		(( void (*) (Enumerator_t2672 *, Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(&L_0, (Dictionary_2_t2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		Enumerator_t2672  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<EScreens,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17127_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		ShimEnumerator_t2678 * L_0 = (ShimEnumerator_t2678 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		(( void (*) (ShimEnumerator_t2678 *, Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)(L_0, (Dictionary_2_t2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<EScreens,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17129_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___count_10);
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<EScreens,System.Object>::get_Item(TKey)
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
// System.Collections.Generic.KeyNotFoundException
#include "mscorlib_System_Collections_Generic_KeyNotFoundExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* KeyNotFoundException_t1870_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" Object_t * Dictionary_2_get_Item_m17131_gshared (Dictionary_2_t2665 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		KeyNotFoundException_t1870_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4052);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<EScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t401* L_5 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t2500* L_9 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		EScreensU5BU5D_t2662* L_14 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<EScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_0089;
		}
	}
	{
		ObjectU5BU5D_t356* L_19 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		return (*(Object_t **)(Object_t **)SZArrayLdElema(L_19, L_21, sizeof(Object_t *)));
	}

IL_0089:
	{
		LinkU5BU5D_t2500* L_22 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1869 )))->___Next_1);
		V_1 = (int32_t)L_24;
	}

IL_009b:
	{
		int32_t L_25 = V_1;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t1870 * L_26 = (KeyNotFoundException_t1870 *)il2cpp_codegen_object_new (KeyNotFoundException_t1870_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m10895(L_26, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_26);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::set_Item(TKey,TValue)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" void Dictionary_2_set_Item_m17133_gshared (Dictionary_2_t2665 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<EScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))));
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t2500* L_11 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_11, L_12, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0087;
		}
	}
	{
		Object_t* L_15 = (Object_t*)(__this->___hcp_12);
		EScreensU5BU5D_t2662* L_16 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		int32_t L_19 = ___key;
		NullCheck((Object_t*)L_15);
		bool L_20 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<EScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_15, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_16, L_18, sizeof(int32_t))), (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_21 = V_2;
		V_3 = (int32_t)L_21;
		LinkU5BU5D_t2500* L_22 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_24;
		int32_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_27 = (int32_t)(__this->___count_10);
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_4 = (int32_t)L_28;
		__this->___count_10 = L_28;
		int32_t L_29 = V_4;
		int32_t L_30 = (int32_t)(__this->___threshold_11);
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)((Dictionary_2_t2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		int32_t L_31 = V_0;
		Int32U5BU5D_t401* L_32 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_32)->max_length)))));
	}

IL_00de:
	{
		int32_t L_33 = (int32_t)(__this->___emptySlot_9);
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_35 = (int32_t)(__this->___touchedSlots_8);
		int32_t L_36 = (int32_t)L_35;
		V_4 = (int32_t)L_36;
		__this->___touchedSlots_8 = ((int32_t)((int32_t)L_36+(int32_t)1));
		int32_t L_37 = V_4;
		V_2 = (int32_t)L_37;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t2500* L_38 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_38, L_39, sizeof(Link_t1869 )))->___Next_1);
		__this->___emptySlot_9 = L_40;
	}

IL_011c:
	{
		LinkU5BU5D_t2500* L_41 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		Int32U5BU5D_t401* L_43 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_44 = V_1;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		int32_t L_45 = L_44;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_41, L_42, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_43, L_45, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_46 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_47 = V_1;
		int32_t L_48 = V_2;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_46, L_47, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_48+(int32_t)1));
		LinkU5BU5D_t2500* L_49 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_50 = V_2;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		int32_t L_51 = V_0;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_49, L_50, sizeof(Link_t1869 )))->___HashCode_0 = L_51;
		EScreensU5BU5D_t2662* L_52 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		int32_t L_53 = V_2;
		int32_t L_54 = ___key;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_52, L_53, sizeof(int32_t))) = (int32_t)L_54;
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_55 = V_3;
		if ((((int32_t)L_55) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t2500* L_56 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_57 = V_3;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		LinkU5BU5D_t2500* L_58 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_59 = V_2;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		int32_t L_60 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_58, L_59, sizeof(Link_t1869 )))->___Next_1);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_56, L_57, sizeof(Link_t1869 )))->___Next_1 = L_60;
		LinkU5BU5D_t2500* L_61 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_62 = V_2;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		Int32U5BU5D_t401* L_63 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_64 = V_1;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, L_64);
		int32_t L_65 = L_64;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_61, L_62, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_63, L_65, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_66 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_67 = V_1;
		int32_t L_68 = V_2;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_66, L_67, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_01b5:
	{
		ObjectU5BU5D_t356* L_69 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_70 = V_2;
		Object_t * L_71 = ___value;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, L_70);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_69, L_70, sizeof(Object_t *))) = (Object_t *)L_71;
		int32_t L_72 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_72+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1870;
extern "C" void Dictionary_2_Init_m17135_gshared (Dictionary_2_t2665 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(906);
		_stringLiteral1870 = il2cpp_codegen_string_literal_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	Dictionary_2_t2665 * G_B4_0 = {0};
	Dictionary_2_t2665 * G_B3_0 = {0};
	Object_t* G_B5_0 = {0};
	Dictionary_2_t2665 * G_B5_1 = {0};
	{
		int32_t L_0 = ___capacity;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1419 * L_1 = (ArgumentOutOfRangeException_t1419 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8460(L_1, (String_t*)_stringLiteral1870, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		Object_t* L_2 = ___hcp;
		G_B3_0 = ((Dictionary_2_t2665 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t2665 *)(__this));
			goto IL_0021;
		}
	}
	{
		Object_t* L_3 = ___hcp;
		V_0 = (Object_t*)L_3;
		Object_t* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t2665 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		EqualityComparer_1_t2679 * L_5 = (( EqualityComparer_1_t2679 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		G_B5_0 = ((Object_t*)(L_5));
		G_B5_1 = ((Dictionary_2_t2665 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->___hcp_12 = G_B5_0;
		int32_t L_6 = ___capacity;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity;
		___capacity = (int32_t)((int32_t)((int32_t)(((int32_t)((float)((float)(((float)L_7))/(float)(0.9f)))))+(int32_t)1));
		int32_t L_8 = ___capacity;
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)((Dictionary_2_t2665 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		__this->___generation_14 = 0;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::InitArrays(System.Int32)
extern TypeInfo* Int32U5BU5D_t401_il2cpp_TypeInfo_var;
extern TypeInfo* LinkU5BU5D_t2500_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_InitArrays_m17137_gshared (Dictionary_2_t2665 * __this, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		LinkU5BU5D_t2500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4053);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size;
		__this->___table_4 = ((Int32U5BU5D_t401*)SZArrayNew(Int32U5BU5D_t401_il2cpp_TypeInfo_var, L_0));
		int32_t L_1 = ___size;
		__this->___linkSlots_5 = ((LinkU5BU5D_t2500*)SZArrayNew(LinkU5BU5D_t2500_il2cpp_TypeInfo_var, L_1));
		__this->___emptySlot_9 = (-1);
		int32_t L_2 = ___size;
		__this->___keySlots_6 = ((EScreensU5BU5D_t2662*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), L_2));
		int32_t L_3 = ___size;
		__this->___valueSlots_7 = ((ObjectU5BU5D_t356*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36), L_3));
		__this->___touchedSlots_8 = 0;
		Int32U5BU5D_t401* L_4 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_4);
		__this->___threshold_11 = (((int32_t)((float)((float)(((float)(((int32_t)(((Array_t *)L_4)->max_length)))))*(float)(0.9f)))));
		int32_t L_5 = (int32_t)(__this->___threshold_11);
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->___threshold_11 = 1;
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::CopyToCheck(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1265;
extern Il2CppCodeGenString* _stringLiteral1267;
extern Il2CppCodeGenString* _stringLiteral3389;
extern Il2CppCodeGenString* _stringLiteral3390;
extern "C" void Dictionary_2_CopyToCheck_m17139_gshared (Dictionary_2_t2665 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(906);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral1265 = il2cpp_codegen_string_literal_from_index(1265);
		_stringLiteral1267 = il2cpp_codegen_string_literal_from_index(1267);
		_stringLiteral3389 = il2cpp_codegen_string_literal_from_index(3389);
		_stringLiteral3390 = il2cpp_codegen_string_literal_from_index(3390);
		s_Il2CppMethodIntialized = true;
	}
	{
		Array_t * L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral1265, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t1419 * L_3 = (ArgumentOutOfRangeException_t1419 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8460(L_3, (String_t*)_stringLiteral1267, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index;
		Array_t * L_5 = ___array;
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t764 * L_7 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_7, (String_t*)_stringLiteral3389, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_003a:
	{
		Array_t * L_8 = ___array;
		NullCheck((Array_t *)L_8);
		int32_t L_9 = Array_get_Length_m9469((Array_t *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index;
		NullCheck((Dictionary_2_t2665 *)__this);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<EScreens,System.Object>::get_Count() */, (Dictionary_2_t2665 *)__this);
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t764 * L_12 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_12, (String_t*)_stringLiteral3390, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<EScreens,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2667  Dictionary_2_make_pair_m17141_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		Object_t * L_1 = ___value;
		KeyValuePair_2_t2667  L_2 = {0};
		(( void (*) (KeyValuePair_2_t2667 *, int32_t, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)(&L_2, (int32_t)L_0, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<EScreens,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m17143_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<EScreens,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m17145_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17147_gshared (Dictionary_2_t2665 * __this, KeyValuePair_2U5BU5D_t3649* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t3649* L_0 = ___array;
		int32_t L_1 = ___index;
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Dictionary_2_t2665 *)__this, (Array_t *)(Array_t *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		KeyValuePair_2U5BU5D_t3649* L_2 = ___array;
		int32_t L_3 = ___index;
		IntPtr_t L_4 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22) };
		Transform_1_t2677 * L_5 = (Transform_1_t2677 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		(( void (*) (Transform_1_t2677 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(L_5, (Object_t *)NULL, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, KeyValuePair_2U5BU5D_t3649*, int32_t, Transform_1_t2677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40)->method)((Dictionary_2_t2665 *)__this, (KeyValuePair_2U5BU5D_t3649*)L_2, (int32_t)L_3, (Transform_1_t2677 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Resize()
// System.Collections.Hashtable
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
extern TypeInfo* Hashtable_t261_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t401_il2cpp_TypeInfo_var;
extern TypeInfo* LinkU5BU5D_t2500_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_Resize_m17149_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(249);
		Int32U5BU5D_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		LinkU5BU5D_t2500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4053);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t401* V_1 = {0};
	LinkU5BU5D_t2500* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	EScreensU5BU5D_t2662* V_7 = {0};
	ObjectU5BU5D_t356* V_8 = {0};
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t401* L_0 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t261_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m11127(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t401*)((Int32U5BU5D_t401*)SZArrayNew(Int32U5BU5D_t401_il2cpp_TypeInfo_var, L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t2500*)((LinkU5BU5D_t2500*)SZArrayNew(LinkU5BU5D_t2500_il2cpp_TypeInfo_var, L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t401* L_4 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_4 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6, sizeof(int32_t)))-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t2500* L_7 = V_2;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Object_t* L_9 = (Object_t*)(__this->___hcp_12);
		EScreensU5BU5D_t2662* L_10 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		int32_t L_11 = V_4;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((Object_t*)L_9);
		int32_t L_13 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<EScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_9, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_10, L_12, sizeof(int32_t))));
		int32_t L_14 = (int32_t)((int32_t)((int32_t)L_13|(int32_t)((int32_t)-2147483648)));
		V_9 = (int32_t)L_14;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_7, L_8, sizeof(Link_t1869 )))->___HashCode_0 = L_14;
		int32_t L_15 = V_9;
		V_5 = (int32_t)L_15;
		int32_t L_16 = V_5;
		int32_t L_17 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)2147483647)))%(int32_t)L_17));
		LinkU5BU5D_t2500* L_18 = V_2;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		Int32U5BU5D_t401* L_20 = V_1;
		int32_t L_21 = V_6;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_18, L_19, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_20, L_22, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_23 = V_1;
		int32_t L_24 = V_6;
		int32_t L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_23, L_24, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		LinkU5BU5D_t2500* L_26 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_27 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_26, L_27, sizeof(Link_t1869 )))->___Next_1);
		V_4 = (int32_t)L_28;
	}

IL_00a5:
	{
		int32_t L_29 = V_4;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_30 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_31 = V_3;
		Int32U5BU5D_t401* L_32 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)(((Array_t *)L_32)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t401* L_33 = V_1;
		__this->___table_4 = L_33;
		LinkU5BU5D_t2500* L_34 = V_2;
		__this->___linkSlots_5 = L_34;
		int32_t L_35 = V_0;
		V_7 = (EScreensU5BU5D_t2662*)((EScreensU5BU5D_t2662*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), L_35));
		int32_t L_36 = V_0;
		V_8 = (ObjectU5BU5D_t356*)((ObjectU5BU5D_t356*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36), L_36));
		EScreensU5BU5D_t2662* L_37 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		EScreensU5BU5D_t2662* L_38 = V_7;
		int32_t L_39 = (int32_t)(__this->___touchedSlots_8);
		Array_Copy_m1754(NULL /*static, unused*/, (Array_t *)(Array_t *)L_37, (int32_t)0, (Array_t *)(Array_t *)L_38, (int32_t)0, (int32_t)L_39, /*hidden argument*/NULL);
		ObjectU5BU5D_t356* L_40 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		ObjectU5BU5D_t356* L_41 = V_8;
		int32_t L_42 = (int32_t)(__this->___touchedSlots_8);
		Array_Copy_m1754(NULL /*static, unused*/, (Array_t *)(Array_t *)L_40, (int32_t)0, (Array_t *)(Array_t *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		EScreensU5BU5D_t2662* L_43 = V_7;
		__this->___keySlots_6 = L_43;
		ObjectU5BU5D_t356* L_44 = V_8;
		__this->___valueSlots_7 = L_44;
		int32_t L_45 = V_0;
		__this->___threshold_11 = (((int32_t)((float)((float)(((float)L_45))*(float)(0.9f)))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Add(TKey,TValue)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern Il2CppCodeGenString* _stringLiteral3391;
extern "C" void Dictionary_2_Add_m17151_gshared (Dictionary_2_t2665 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		_stringLiteral3391 = il2cpp_codegen_string_literal_from_index(3391);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<EScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))));
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t2500* L_10 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_10, L_11, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_14 = (Object_t*)(__this->___hcp_12);
		EScreensU5BU5D_t2662* L_15 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = ___key;
		NullCheck((Object_t*)L_14);
		bool L_19 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<EScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_14, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_15, L_17, sizeof(int32_t))), (int32_t)L_18);
		if (!L_19)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t764 * L_20 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_20, (String_t*)_stringLiteral3391, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_20);
	}

IL_0089:
	{
		LinkU5BU5D_t2500* L_21 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_22 = V_2;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_21, L_22, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_23;
	}

IL_009b:
	{
		int32_t L_24 = V_2;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_25 = (int32_t)(__this->___count_10);
		int32_t L_26 = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		V_3 = (int32_t)L_26;
		__this->___count_10 = L_26;
		int32_t L_27 = V_3;
		int32_t L_28 = (int32_t)(__this->___threshold_11);
		if ((((int32_t)L_27) <= ((int32_t)L_28)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)((Dictionary_2_t2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		int32_t L_29 = V_0;
		Int32U5BU5D_t401* L_30 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_30);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_29&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_30)->max_length)))));
	}

IL_00d5:
	{
		int32_t L_31 = (int32_t)(__this->___emptySlot_9);
		V_2 = (int32_t)L_31;
		int32_t L_32 = V_2;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_33 = (int32_t)(__this->___touchedSlots_8);
		int32_t L_34 = (int32_t)L_33;
		V_3 = (int32_t)L_34;
		__this->___touchedSlots_8 = ((int32_t)((int32_t)L_34+(int32_t)1));
		int32_t L_35 = V_3;
		V_2 = (int32_t)L_35;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t2500* L_36 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_37 = V_2;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_36, L_37, sizeof(Link_t1869 )))->___Next_1);
		__this->___emptySlot_9 = L_38;
	}

IL_0111:
	{
		LinkU5BU5D_t2500* L_39 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_40 = V_2;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = V_0;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_39, L_40, sizeof(Link_t1869 )))->___HashCode_0 = L_41;
		LinkU5BU5D_t2500* L_42 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_43 = V_2;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		Int32U5BU5D_t401* L_44 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_45 = V_1;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		int32_t L_46 = L_45;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_42, L_43, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_44, L_46, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_47 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_48 = V_1;
		int32_t L_49 = V_2;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_47, L_48, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_49+(int32_t)1));
		EScreensU5BU5D_t2662* L_50 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		int32_t L_51 = V_2;
		int32_t L_52 = ___key;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_50, L_51, sizeof(int32_t))) = (int32_t)L_52;
		ObjectU5BU5D_t356* L_53 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_54 = V_2;
		Object_t * L_55 = ___value;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_53, L_54, sizeof(Object_t *))) = (Object_t *)L_55;
		int32_t L_56 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_56+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m17153_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		__this->___count_10 = 0;
		Int32U5BU5D_t401* L_0 = (Int32U5BU5D_t401*)(__this->___table_4);
		Int32U5BU5D_t401* L_1 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_1);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_1)->max_length))), /*hidden argument*/NULL);
		EScreensU5BU5D_t2662* L_2 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		EScreensU5BU5D_t2662* L_3 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		NullCheck(L_3);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_3)->max_length))), /*hidden argument*/NULL);
		ObjectU5BU5D_t356* L_4 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		ObjectU5BU5D_t356* L_5 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		NullCheck(L_5);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_4, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_5)->max_length))), /*hidden argument*/NULL);
		LinkU5BU5D_t2500* L_6 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		LinkU5BU5D_t2500* L_7 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		NullCheck(L_7);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_7)->max_length))), /*hidden argument*/NULL);
		__this->___emptySlot_9 = (-1);
		__this->___touchedSlots_8 = 0;
		int32_t L_8 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_8+(int32_t)1));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::ContainsKey(TKey)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_ContainsKey_m17155_gshared (Dictionary_2_t2665 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<EScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t401* L_5 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t2500* L_9 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_007e;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		EScreensU5BU5D_t2662* L_14 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<EScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_007e;
		}
	}
	{
		return 1;
	}

IL_007e:
	{
		LinkU5BU5D_t2500* L_19 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_19, L_20, sizeof(Link_t1869 )))->___Next_1);
		V_1 = (int32_t)L_21;
	}

IL_0090:
	{
		int32_t L_22 = V_1;
		if ((!(((uint32_t)L_22) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17157_gshared (Dictionary_2_t2665 * __this, Object_t * ___value, const MethodInfo* method)
{
	Object_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42));
		EqualityComparer_1_t2473 * L_0 = (( EqualityComparer_1_t2473 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
		V_0 = (Object_t*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t401* L_1 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_3, sizeof(int32_t)))-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		Object_t* L_4 = V_0;
		ObjectU5BU5D_t356* L_5 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Object_t * L_8 = ___value;
		NullCheck((Object_t*)L_4);
		bool L_9 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43), (Object_t*)L_4, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *))), (Object_t *)L_8);
		if (!L_9)
		{
			goto IL_0037;
		}
	}
	{
		return 1;
	}

IL_0037:
	{
		LinkU5BU5D_t2500* L_10 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_10, L_11, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_12;
	}

IL_0049:
	{
		int32_t L_13 = V_2;
		if ((!(((uint32_t)L_13) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_14 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_15 = V_1;
		Int32U5BU5D_t401* L_16 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)(((Array_t *)L_16)->max_length))))))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1277;
extern Il2CppCodeGenString* _stringLiteral1279;
extern Il2CppCodeGenString* _stringLiteral1281;
extern Il2CppCodeGenString* _stringLiteral1904;
extern Il2CppCodeGenString* _stringLiteral3392;
extern "C" void Dictionary_2_GetObjectData_m17159_gshared (Dictionary_2_t2665 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral1277 = il2cpp_codegen_string_literal_from_index(1277);
		_stringLiteral1279 = il2cpp_codegen_string_literal_from_index(1279);
		_stringLiteral1281 = il2cpp_codegen_string_literal_from_index(1281);
		_stringLiteral1904 = il2cpp_codegen_string_literal_from_index(1904);
		_stringLiteral3392 = il2cpp_codegen_string_literal_from_index(3392);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t3649* V_0 = {0};
	{
		SerializationInfo_t1012 * L_0 = ___info;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral1277, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SerializationInfo_t1012 * L_2 = ___info;
		int32_t L_3 = (int32_t)(__this->___generation_14);
		NullCheck((SerializationInfo_t1012 *)L_2);
		SerializationInfo_AddValue_m9476((SerializationInfo_t1012 *)L_2, (String_t*)_stringLiteral1279, (int32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t1012 * L_4 = ___info;
		Object_t* L_5 = (Object_t*)(__this->___hcp_12);
		NullCheck((SerializationInfo_t1012 *)L_4);
		SerializationInfo_AddValue_m8509((SerializationInfo_t1012 *)L_4, (String_t*)_stringLiteral1281, (Object_t *)L_5, /*hidden argument*/NULL);
		V_0 = (KeyValuePair_2U5BU5D_t3649*)NULL;
		int32_t L_6 = (int32_t)(__this->___count_10);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = (int32_t)(__this->___count_10);
		V_0 = (KeyValuePair_2U5BU5D_t3649*)((KeyValuePair_2U5BU5D_t3649*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44), L_7));
		KeyValuePair_2U5BU5D_t3649* L_8 = V_0;
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, KeyValuePair_2U5BU5D_t3649*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Dictionary_2_t2665 *)__this, (KeyValuePair_2U5BU5D_t3649*)L_8, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
	}

IL_0055:
	{
		SerializationInfo_t1012 * L_9 = ___info;
		Int32U5BU5D_t401* L_10 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_10);
		NullCheck((SerializationInfo_t1012 *)L_9);
		SerializationInfo_AddValue_m9476((SerializationInfo_t1012 *)L_9, (String_t*)_stringLiteral1904, (int32_t)(((int32_t)(((Array_t *)L_10)->max_length))), /*hidden argument*/NULL);
		SerializationInfo_t1012 * L_11 = ___info;
		KeyValuePair_2U5BU5D_t3649* L_12 = V_0;
		NullCheck((SerializationInfo_t1012 *)L_11);
		SerializationInfo_AddValue_m8509((SerializationInfo_t1012 *)L_11, (String_t*)_stringLiteral3392, (Object_t *)(Object_t *)L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::OnDeserialization(System.Object)
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1279;
extern Il2CppCodeGenString* _stringLiteral1281;
extern Il2CppCodeGenString* _stringLiteral1904;
extern Il2CppCodeGenString* _stringLiteral3392;
extern "C" void Dictionary_2_OnDeserialization_m17161_gshared (Dictionary_2_t2665 * __this, Object_t * ___sender, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		_stringLiteral1279 = il2cpp_codegen_string_literal_from_index(1279);
		_stringLiteral1281 = il2cpp_codegen_string_literal_from_index(1281);
		_stringLiteral1904 = il2cpp_codegen_string_literal_from_index(1904);
		_stringLiteral3392 = il2cpp_codegen_string_literal_from_index(3392);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t3649* V_1 = {0};
	int32_t V_2 = 0;
	{
		SerializationInfo_t1012 * L_0 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t1012 * L_1 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		NullCheck((SerializationInfo_t1012 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m9483((SerializationInfo_t1012 *)L_1, (String_t*)_stringLiteral1279, /*hidden argument*/NULL);
		__this->___generation_14 = L_2;
		SerializationInfo_t1012 * L_3 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1012 *)L_3);
		Object_t * L_5 = SerializationInfo_GetValue_m9477((SerializationInfo_t1012 *)L_3, (String_t*)_stringLiteral1281, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->___hcp_12 = ((Object_t*)Castclass(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30)));
		SerializationInfo_t1012 * L_6 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		NullCheck((SerializationInfo_t1012 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m9483((SerializationInfo_t1012 *)L_6, (String_t*)_stringLiteral1904, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t1012 * L_8 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		Type_t * L_9 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1012 *)L_8);
		Object_t * L_10 = SerializationInfo_GetValue_m9477((SerializationInfo_t1012 *)L_8, (String_t*)_stringLiteral3392, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t3649*)((KeyValuePair_2U5BU5D_t3649*)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t2665 *)__this);
		(( void (*) (Dictionary_2_t2665 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)((Dictionary_2_t2665 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		__this->___count_10 = 0;
		KeyValuePair_2U5BU5D_t3649* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t3649* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = (( int32_t (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t2667 *)((KeyValuePair_2_t2667 *)(KeyValuePair_2_t2667 *)SZArrayLdElema(L_14, L_15, sizeof(KeyValuePair_2_t2667 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		KeyValuePair_2U5BU5D_t3649* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		Object_t * L_19 = (( Object_t * (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((KeyValuePair_2_t2667 *)((KeyValuePair_2_t2667 *)(KeyValuePair_2_t2667 *)SZArrayLdElema(L_17, L_18, sizeof(KeyValuePair_2_t2667 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((Dictionary_2_t2665 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Add(TKey,TValue) */, (Dictionary_2_t2665 *)__this, (int32_t)L_16, (Object_t *)L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t3649* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)(((Array_t *)L_22)->max_length))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_23+(int32_t)1));
		__this->___serialization_info_13 = (SerializationInfo_t1012 *)NULL;
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Remove(TKey)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* EScreens_t188_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_Remove_m17163_gshared (Dictionary_2_t2665 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		EScreens_t188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = {0};
	Object_t * V_5 = {0};
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<EScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))));
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		int32_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return 0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t2500* L_11 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_11, L_12, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_15 = (Object_t*)(__this->___hcp_12);
		EScreensU5BU5D_t2662* L_16 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		int32_t L_19 = ___key;
		NullCheck((Object_t*)L_15);
		bool L_20 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<EScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_15, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_16, L_18, sizeof(int32_t))), (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_21 = V_2;
		V_3 = (int32_t)L_21;
		LinkU5BU5D_t2500* L_22 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_24;
		int32_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return 0;
	}

IL_00ad:
	{
		int32_t L_27 = (int32_t)(__this->___count_10);
		__this->___count_10 = ((int32_t)((int32_t)L_27-(int32_t)1));
		int32_t L_28 = V_3;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t401* L_29 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_30 = V_1;
		LinkU5BU5D_t2500* L_31 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_32 = V_2;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_31, L_32, sizeof(Link_t1869 )))->___Next_1);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, L_30, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t2500* L_34 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_35 = V_3;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		LinkU5BU5D_t2500* L_36 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_37 = V_2;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_36, L_37, sizeof(Link_t1869 )))->___Next_1);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_34, L_35, sizeof(Link_t1869 )))->___Next_1 = L_38;
	}

IL_0104:
	{
		LinkU5BU5D_t2500* L_39 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_40 = V_2;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = (int32_t)(__this->___emptySlot_9);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_39, L_40, sizeof(Link_t1869 )))->___Next_1 = L_41;
		int32_t L_42 = V_2;
		__this->___emptySlot_9 = L_42;
		LinkU5BU5D_t2500* L_43 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_44 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_43, L_44, sizeof(Link_t1869 )))->___HashCode_0 = 0;
		EScreensU5BU5D_t2662* L_45 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		int32_t L_46 = V_2;
		Initobj (EScreens_t188_il2cpp_TypeInfo_var, (&V_4));
		int32_t L_47 = V_4;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_45, L_46, sizeof(int32_t))) = (int32_t)L_47;
		ObjectU5BU5D_t356* L_48 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_49 = V_2;
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_5));
		Object_t * L_50 = V_5;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, L_49, sizeof(Object_t *))) = (Object_t *)L_50;
		int32_t L_51 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_51+(int32_t)1));
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::TryGetValue(TKey,TValue&)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_TryGetValue_m17165_gshared (Dictionary_2_t2665 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Object_t * V_2 = {0};
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<EScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t401* L_5 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t2500* L_9 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0090;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		EScreensU5BU5D_t2662* L_14 = (EScreensU5BU5D_t2662*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<EScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_0090;
		}
	}
	{
		Object_t ** L_19 = ___value;
		ObjectU5BU5D_t356* L_20 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_21 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		*L_19 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_20, L_22, sizeof(Object_t *)));
		return 1;
	}

IL_0090:
	{
		LinkU5BU5D_t2500* L_23 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_23, L_24, sizeof(Link_t1869 )))->___Next_1);
		V_1 = (int32_t)L_25;
	}

IL_00a2:
	{
		int32_t L_26 = V_1;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		Object_t ** L_27 = ___value;
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_2));
		Object_t * L_28 = V_2;
		*L_27 = L_28;
		return 0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<EScreens,System.Object>::get_Keys()
extern "C" KeyCollection_t2670 * Dictionary_2_get_Keys_m17167_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		KeyCollection_t2670 * L_0 = (KeyCollection_t2670 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		(( void (*) (KeyCollection_t2670 *, Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48)->method)(L_0, (Dictionary_2_t2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<EScreens,System.Object>::get_Values()
extern "C" ValueCollection_t2674 * Dictionary_2_get_Values_m17169_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		ValueCollection_t2674 * L_0 = (ValueCollection_t2674 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49));
		(( void (*) (ValueCollection_t2674 *, Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50)->method)(L_0, (Dictionary_2_t2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<EScreens,System.Object>::ToTKey(System.Object)
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern Il2CppCodeGenString* _stringLiteral3393;
extern "C" int32_t Dictionary_2_ToTKey_m17171_gshared (Dictionary_2_t2665 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		_stringLiteral3393 = il2cpp_codegen_string_literal_from_index(3393);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 51)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1319(NULL /*static, unused*/, (String_t*)_stringLiteral3393, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t764 * L_6 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m7574(L_6, (String_t*)L_5, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0040:
	{
		Object_t * L_7 = ___key;
		return ((*(int32_t*)((int32_t*)UnBox (L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)))));
	}
}
// TValue System.Collections.Generic.Dictionary`2<EScreens,System.Object>::ToTValue(System.Object)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3393;
extern Il2CppCodeGenString* _stringLiteral64;
extern "C" Object_t * Dictionary_2_ToTValue_m17173_gshared (Dictionary_2_t2665 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral3393 = il2cpp_codegen_string_literal_from_index(3393);
		_stringLiteral64 = il2cpp_codegen_string_literal_from_index(64);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = ___value;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean System.Type::get_IsValueType() */, (Type_t *)L_1);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_0));
		Object_t * L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		Object_t * L_4 = ___value;
		if (((Object_t *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1319(NULL /*static, unused*/, (String_t*)_stringLiteral3393, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t764 * L_8 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m7574(L_8, (String_t*)L_7, (String_t*)_stringLiteral64, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0053:
	{
		Object_t * L_9 = ___value;
		return ((Object_t *)Castclass(L_9, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17175_gshared (Dictionary_2_t2665 * __this, KeyValuePair_2_t2667  ___pair, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		int32_t L_0 = (( int32_t (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t2667 *)(&___pair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((Dictionary_2_t2665 *)__this);
		bool L_1 = (bool)VirtFuncInvoker2< bool, int32_t, Object_t ** >::Invoke(33 /* System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::TryGetValue(TKey,TValue&) */, (Dictionary_2_t2665 *)__this, (int32_t)L_0, (Object_t **)(&V_0));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return 0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42));
		EqualityComparer_1_t2473 * L_2 = (( EqualityComparer_1_t2473 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
		Object_t * L_3 = (( Object_t * (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((KeyValuePair_2_t2667 *)(&___pair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Object_t * L_4 = V_0;
		NullCheck((EqualityComparer_1_t2473 *)L_2);
		bool L_5 = (bool)VirtFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t2473 *)L_2, (Object_t *)L_3, (Object_t *)L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<EScreens,System.Object>::GetEnumerator()
extern "C" Enumerator_t2672  Dictionary_2_GetEnumerator_m17177_gshared (Dictionary_2_t2665 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672  L_0 = {0};
		(( void (*) (Enumerator_t2672 *, Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(&L_0, (Dictionary_2_t2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<EScreens,System.Object>::<CopyTo>m__0(TKey,TValue)
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m17179_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		Object_t * L_3 = ___value;
		DictionaryEntry_t451  L_4 = {0};
		DictionaryEntry__ctor_m9467(&L_4, (Object_t *)L_2, (Object_t *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_29.h"
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_29MethodDeclarations.h"
// Declaration !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>(System.Int32)
extern "C" KeyValuePair_2_t2667  Array_InternalArray__get_Item_TisKeyValuePair_2_t2667_m30241_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2667_m30241(__this, p0, method) (( KeyValuePair_2_t2667  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2667_m30241_gshared)(__this, p0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::.ctor(System.Array)
// System.Array
#include "mscorlib_System_Array.h"
extern "C" void InternalEnumerator_1__ctor_m17180_gshared (InternalEnumerator_1_t2668 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17181_gshared (InternalEnumerator_1_t2668 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2667  L_0 = (( KeyValuePair_2_t2667  (*) (InternalEnumerator_1_t2668 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2668 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2667  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17182_gshared (InternalEnumerator_1_t2668 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::MoveNext()
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern "C" bool InternalEnumerator_1_MoveNext_m17183_gshared (InternalEnumerator_1_t2668 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9469((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::get_Current()
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3382;
extern Il2CppCodeGenString* _stringLiteral3383;
extern "C" KeyValuePair_2_t2667  InternalEnumerator_1_get_Current_m17184_gshared (InternalEnumerator_1_t2668 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3382 = il2cpp_codegen_string_literal_from_index(3382);
		_stringLiteral3383 = il2cpp_codegen_string_literal_from_index(3383);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, (String_t*)_stringLiteral3382, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t425 * L_3 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_3, (String_t*)_stringLiteral3383, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		KeyValuePair_2_t2667  L_8 = (( KeyValuePair_2_t2667  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::.ctor(TKey,TValue)
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
// System.Object
#include "mscorlib_System_Object.h"
extern "C" void KeyValuePair_2__ctor_m17185_gshared (KeyValuePair_2_t2667 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		(( void (*) (KeyValuePair_2_t2667 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t2667 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t * L_1 = ___value;
		(( void (*) (KeyValuePair_2_t2667 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t2667 *)__this, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m17186_gshared (KeyValuePair_2_t2667 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___key_0);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m17187_gshared (KeyValuePair_2_t2667 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___key_0 = L_0;
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m17188_gshared (KeyValuePair_2_t2667 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___value_1);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m17189_gshared (KeyValuePair_2_t2667 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___value_1 = L_0;
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>::ToString()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* StringU5BU5D_t137_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral719;
extern "C" String_t* KeyValuePair_2_ToString_m17190_gshared (KeyValuePair_2_t2667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral58 = il2cpp_codegen_string_literal_from_index(58);
		_stringLiteral46 = il2cpp_codegen_string_literal_from_index(46);
		_stringLiteral719 = il2cpp_codegen_string_literal_from_index(719);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	int32_t G_B2_0 = 0;
	StringU5BU5D_t137* G_B2_1 = {0};
	StringU5BU5D_t137* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	StringU5BU5D_t137* G_B1_1 = {0};
	StringU5BU5D_t137* G_B1_2 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	StringU5BU5D_t137* G_B3_2 = {0};
	StringU5BU5D_t137* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	StringU5BU5D_t137* G_B5_1 = {0};
	StringU5BU5D_t137* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	StringU5BU5D_t137* G_B4_1 = {0};
	StringU5BU5D_t137* G_B4_2 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	StringU5BU5D_t137* G_B6_2 = {0};
	StringU5BU5D_t137* G_B6_3 = {0};
	{
		StringU5BU5D_t137* L_0 = (StringU5BU5D_t137*)((StringU5BU5D_t137*)SZArrayNew(StringU5BU5D_t137_il2cpp_TypeInfo_var, 5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral58);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral58;
		StringU5BU5D_t137* L_1 = (StringU5BU5D_t137*)L_0;
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		int32_t L_3 = (( int32_t (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0)));
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(String_t*))) = (String_t*)G_B3_0;
		StringU5BU5D_t137* L_6 = (StringU5BU5D_t137*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral46);
		*((String_t**)(String_t**)SZArrayLdElema(L_6, 2, sizeof(String_t*))) = (String_t*)_stringLiteral46;
		StringU5BU5D_t137* L_7 = (StringU5BU5D_t137*)L_6;
		Object_t * L_8 = (( Object_t * (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Object_t * L_9 = (( Object_t * (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Object_t *)L_9;
		NullCheck((Object_t *)(*(&V_1)));
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(String_t*))) = (String_t*)G_B6_0;
		StringU5BU5D_t137* L_12 = (StringU5BU5D_t137*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral719);
		*((String_t**)(String_t**)SZArrayLdElema(L_12, 4, sizeof(String_t*))) = (String_t*)_stringLiteral719;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1461(NULL /*static, unused*/, (StringU5BU5D_t137*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Array/InternalEnumerator`1<EScreens>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_30.h"
// System.Array/InternalEnumerator`1<EScreens>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_30MethodDeclarations.h"
// Declaration !!0 System.Array::InternalArray__get_Item<EScreens>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<EScreens>(System.Int32)
extern "C" int32_t Array_InternalArray__get_Item_TisEScreens_t188_m30252_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisEScreens_t188_m30252(__this, p0, method) (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisEScreens_t188_m30252_gshared)(__this, p0, method)
// System.Void System.Array/InternalEnumerator`1<EScreens>::.ctor(System.Array)
// System.Array
#include "mscorlib_System_Array.h"
extern "C" void InternalEnumerator_1__ctor_m17191_gshared (InternalEnumerator_1_t2669 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<EScreens>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17192_gshared (InternalEnumerator_1_t2669 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (InternalEnumerator_1_t2669 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2669 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<EScreens>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17193_gshared (InternalEnumerator_1_t2669 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<EScreens>::MoveNext()
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern "C" bool InternalEnumerator_1_MoveNext_m17194_gshared (InternalEnumerator_1_t2669 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9469((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<EScreens>::get_Current()
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3382;
extern Il2CppCodeGenString* _stringLiteral3383;
extern "C" int32_t InternalEnumerator_1_get_Current_m17195_gshared (InternalEnumerator_1_t2669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3382 = il2cpp_codegen_string_literal_from_index(3382);
		_stringLiteral3383 = il2cpp_codegen_string_literal_from_index(3383);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, (String_t*)_stringLiteral3382, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t425 * L_3 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_3, (String_t*)_stringLiteral3383, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		int32_t L_8 = (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_17.h"
// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,EScreens>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_14.h"
// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,EScreens>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_14MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_17MethodDeclarations.h"
struct Array_t;
struct Transform_1_t2673;
// Declaration System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_ICollectionCopyTo<EScreens>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_ICollectionCopyTo<EScreens>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisEScreens_t188_m30263_gshared (Dictionary_2_t2665 * __this, Array_t * p0, int32_t p1, Transform_1_t2673 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisEScreens_t188_m30263(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, Transform_1_t2673 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisEScreens_t188_m30263_gshared)(__this, p0, p1, p2, method)
struct EScreensU5BU5D_t2662;
struct Transform_1_t2673;
// Declaration System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_CopyTo<EScreens,EScreens>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_CopyTo<EScreens,EScreens>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisEScreens_t188_TisEScreens_t188_m30265_gshared (Dictionary_2_t2665 * __this, EScreensU5BU5D_t2662* p0, int32_t p1, Transform_1_t2673 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisEScreens_t188_TisEScreens_t188_m30265(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2665 *, EScreensU5BU5D_t2662*, int32_t, Transform_1_t2673 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisEScreens_t188_TisEScreens_t188_m30265_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_43.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1899;
extern "C" void KeyCollection__ctor_m17196_gshared (KeyCollection_t2670 * __this, Dictionary_2_t2665 * ___dictionary, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral1899 = il2cpp_codegen_string_literal_from_index(1899);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2665 * L_0 = ___dictionary;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral1899, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Dictionary_2_t2665 * L_2 = ___dictionary;
		__this->___dictionary_0 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17197_gshared (KeyCollection_t2670 * __this, int32_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17198_gshared (KeyCollection_t2670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17199_gshared (KeyCollection_t2670 * __this, int32_t ___item, const MethodInfo* method)
{
	{
		Dictionary_2_t2665 * L_0 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		int32_t L_1 = ___item;
		NullCheck((Dictionary_2_t2665 *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<EScreens,System.Object>::ContainsKey(TKey) */, (Dictionary_2_t2665 *)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17200_gshared (KeyCollection_t2670 * __this, int32_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17201_gshared (KeyCollection_t2670 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2670 *)__this);
		Enumerator_t2671  L_0 = (( Enumerator_t2671  (*) (KeyCollection_t2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyCollection_t2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t2671  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_Array.h"
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m17202_gshared (KeyCollection_t2670 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	EScreensU5BU5D_t2662* V_0 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (EScreensU5BU5D_t2662*)((EScreensU5BU5D_t2662*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		EScreensU5BU5D_t2662* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		EScreensU5BU5D_t2662* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((KeyCollection_t2670 *)__this);
		(( void (*) (KeyCollection_t2670 *, EScreensU5BU5D_t2662*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyCollection_t2670 *)__this, (EScreensU5BU5D_t2662*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2665 * L_4 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		Array_t * L_5 = ___array;
		int32_t L_6 = ___index;
		NullCheck((Dictionary_2_t2665 *)L_4);
		(( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t2665 *)L_4, (Array_t *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t2665 * L_7 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		Array_t * L_8 = ___array;
		int32_t L_9 = ___index;
		IntPtr_t L_10 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t2673 * L_11 = (Transform_1_t2673 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t2673 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_11, (Object_t *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t2665 *)L_7);
		(( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, Transform_1_t2673 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t2665 *)L_7, (Array_t *)L_8, (int32_t)L_9, (Transform_1_t2673 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17203_gshared (KeyCollection_t2670 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2670 *)__this);
		Enumerator_t2671  L_0 = (( Enumerator_t2671  (*) (KeyCollection_t2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyCollection_t2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t2671  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17204_gshared (KeyCollection_t2670 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17205_gshared (KeyCollection_t2670 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern TypeInfo* ICollection_t1751_il2cpp_TypeInfo_var;
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m17206_gshared (KeyCollection_t2670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1751_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1034);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2665 * L_0 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1751_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::CopyTo(TKey[],System.Int32)
#include "Assembly-CSharp_ArrayTypes.h"
extern "C" void KeyCollection_CopyTo_m17207_gshared (KeyCollection_t2670 * __this, EScreensU5BU5D_t2662* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Dictionary_2_t2665 * L_0 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		EScreensU5BU5D_t2662* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Dictionary_2_t2665 *)L_0);
		(( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t2665 *)L_0, (Array_t *)(Array_t *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t2665 * L_3 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		EScreensU5BU5D_t2662* L_4 = ___array;
		int32_t L_5 = ___index;
		IntPtr_t L_6 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t2673 * L_7 = (Transform_1_t2673 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t2673 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_7, (Object_t *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t2665 *)L_3);
		(( void (*) (Dictionary_2_t2665 *, EScreensU5BU5D_t2662*, int32_t, Transform_1_t2673 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Dictionary_2_t2665 *)L_3, (EScreensU5BU5D_t2662*)L_4, (int32_t)L_5, (Transform_1_t2673 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::GetEnumerator()
extern "C" Enumerator_t2671  KeyCollection_GetEnumerator_m17208_gshared (KeyCollection_t2670 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2665 * L_0 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		Enumerator_t2671  L_1 = {0};
		(( void (*) (Enumerator_t2671 *, Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(&L_1, (Dictionary_2_t2665 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m17209_gshared (KeyCollection_t2670 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2665 * L_0 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		NullCheck((Dictionary_2_t2665 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<EScreens,System.Object>::get_Count() */, (Dictionary_2_t2665 *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<EScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_43.h"
extern "C" void Enumerator__ctor_m17210_gshared (Enumerator_t2671 * __this, Dictionary_2_t2665 * ___host, const MethodInfo* method)
{
	{
		Dictionary_2_t2665 * L_0 = ___host;
		NullCheck((Dictionary_2_t2665 *)L_0);
		Enumerator_t2672  L_1 = (( Enumerator_t2672  (*) (Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2665 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<EScreens,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17211_gshared (Enumerator_t2671 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672 * L_0 = (Enumerator_t2672 *)&(__this->___host_enumerator_0);
		int32_t L_1 = (( int32_t (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t2672 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<EScreens,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m17212_gshared (Enumerator_t2671 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672 * L_0 = (Enumerator_t2672 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t2672 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<EScreens,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17213_gshared (Enumerator_t2671 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672 * L_0 = (Enumerator_t2672 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t2672 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_1;
	}
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<EScreens,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m17214_gshared (Enumerator_t2671 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672 * L_0 = (Enumerator_t2672 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t2667 * L_1 = (KeyValuePair_2_t2667 *)&(L_0->___current_3);
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t2667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_43.h"
extern "C" void Enumerator__ctor_m17215_gshared (Enumerator_t2672 * __this, Dictionary_2_t2665 * ___dictionary, const MethodInfo* method)
{
	{
		Dictionary_2_t2665 * L_0 = ___dictionary;
		__this->___dictionary_0 = L_0;
		Dictionary_2_t2665 * L_1 = ___dictionary;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___generation_14);
		__this->___stamp_2 = L_2;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17216_gshared (Enumerator_t2672 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2667  L_0 = (KeyValuePair_2_t2667 )(__this->___current_3);
		KeyValuePair_2_t2667  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17217_gshared (Enumerator_t2672 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2667 * L_0 = (KeyValuePair_2_t2667 *)&(__this->___current_3);
		int32_t L_1 = (( int32_t (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2667 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_2);
		KeyValuePair_2_t2667 * L_4 = (KeyValuePair_2_t2667 *)&(__this->___current_3);
		Object_t * L_5 = (( Object_t * (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2667 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		DictionaryEntry_t451  L_6 = {0};
		DictionaryEntry__ctor_m9467(&L_6, (Object_t *)L_3, (Object_t *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17218_gshared (Enumerator_t2672 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Enumerator_t2672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_1);
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17219_gshared (Enumerator_t2672 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t2672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17220_gshared (Enumerator_t2672 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		(( void (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t2672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return 0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)(__this->___next_1);
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->___next_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2665 * L_4 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		NullCheck(L_4);
		LinkU5BU5D_t2500* L_5 = (LinkU5BU5D_t2500*)(L_4->___linkSlots_5);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_5, L_6, sizeof(Link_t1869 )))->___HashCode_0);
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2665 * L_8 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		NullCheck(L_8);
		EScreensU5BU5D_t2662* L_9 = (EScreensU5BU5D_t2662*)(L_8->___keySlots_6);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Dictionary_2_t2665 * L_12 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		NullCheck(L_12);
		ObjectU5BU5D_t356* L_13 = (ObjectU5BU5D_t356*)(L_12->___valueSlots_7);
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		KeyValuePair_2_t2667  L_16 = {0};
		(( void (*) (KeyValuePair_2_t2667 *, int32_t, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(&L_16, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_9, L_11, sizeof(int32_t))), (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_13, L_15, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->___current_3 = L_16;
		return 1;
	}

IL_007b:
	{
		int32_t L_17 = (int32_t)(__this->___next_1);
		Dictionary_2_t2665 * L_18 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		NullCheck(L_18);
		int32_t L_19 = (int32_t)(L_18->___touchedSlots_8);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0019;
		}
	}
	{
		__this->___next_1 = (-1);
		return 0;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2667  Enumerator_get_Current_m17221_gshared (Enumerator_t2672 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2667  L_0 = (KeyValuePair_2_t2667 )(__this->___current_3);
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m17222_gshared (Enumerator_t2672 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2667 * L_0 = (KeyValuePair_2_t2667 *)&(__this->___current_3);
		int32_t L_1 = (( int32_t (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2667 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m17223_gshared (Enumerator_t2672 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2667 * L_0 = (KeyValuePair_2_t2667 *)&(__this->___current_3);
		Object_t * L_1 = (( Object_t * (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2667 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::VerifyState()
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* ObjectDisposedException_t1420_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3395;
extern "C" void Enumerator_VerifyState_m17224_gshared (Enumerator_t2672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(907);
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3395 = il2cpp_codegen_string_literal_from_index(3395);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2665 * L_0 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1420 * L_1 = (ObjectDisposedException_t1420 *)il2cpp_codegen_object_new (ObjectDisposedException_t1420_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m7575(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		Dictionary_2_t2665 * L_2 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)(L_2->___generation_14);
		int32_t L_4 = (int32_t)(__this->___stamp_2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t425 * L_5 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_5, (String_t*)_stringLiteral3395, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0033:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::VerifyCurrent()
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3396;
extern "C" void Enumerator_VerifyCurrent_m17225_gshared (Enumerator_t2672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3396 = il2cpp_codegen_string_literal_from_index(3396);
		s_Il2CppMethodIntialized = true;
	}
	{
		(( void (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t2672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, (String_t*)_stringLiteral3396, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_001d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<EScreens,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m17226_gshared (Enumerator_t2672 * __this, const MethodInfo* method)
{
	{
		__this->___dictionary_0 = (Dictionary_2_t2665 *)NULL;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,EScreens>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Transform_1__ctor_m17227_gshared (Transform_1_t2673 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,EScreens>::Invoke(TKey,TValue)
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
extern "C" int32_t Transform_1_Invoke_m17228_gshared (Transform_1_t2673 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m17228((Transform_1_t2673 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,EScreens>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* EScreens_t188_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m17229_gshared (Transform_1_t2673 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EScreens_t188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(EScreens_t188_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,EScreens>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Transform_1_EndInvoke_m17230_gshared (Transform_1_t2673 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_41.h"
// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_15.h"
// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_15MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_41MethodDeclarations.h"
struct Array_t;
struct Transform_1_t2676;
// Declaration System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30266_gshared (Dictionary_2_t2665 * __this, Array_t * p0, int32_t p1, Transform_1_t2676 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30266(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, Transform_1_t2676 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30266_gshared)(__this, p0, p1, p2, method)
struct ObjectU5BU5D_t356;
struct Transform_1_t2676;
// Declaration System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<EScreens,System.Object>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30267_gshared (Dictionary_2_t2665 * __this, ObjectU5BU5D_t356* p0, int32_t p1, Transform_1_t2676 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30267(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2665 *, ObjectU5BU5D_t356*, int32_t, Transform_1_t2676 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30267_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_43.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1899;
extern "C" void ValueCollection__ctor_m17231_gshared (ValueCollection_t2674 * __this, Dictionary_2_t2665 * ___dictionary, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral1899 = il2cpp_codegen_string_literal_from_index(1899);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2665 * L_0 = ___dictionary;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral1899, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Dictionary_2_t2665 * L_2 = ___dictionary;
		__this->___dictionary_0 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
// System.Object
#include "mscorlib_System_Object.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17232_gshared (ValueCollection_t2674 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17233_gshared (ValueCollection_t2674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17234_gshared (ValueCollection_t2674 * __this, Object_t * ___item, const MethodInfo* method)
{
	{
		Dictionary_2_t2665 * L_0 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		Object_t * L_1 = ___item;
		NullCheck((Dictionary_2_t2665 *)L_0);
		bool L_2 = (( bool (*) (Dictionary_2_t2665 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2665 *)L_0, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17235_gshared (ValueCollection_t2674 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17236_gshared (ValueCollection_t2674 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t2674 *)__this);
		Enumerator_t2675  L_0 = (( Enumerator_t2675  (*) (ValueCollection_t2674 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ValueCollection_t2674 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t2675  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_Array.h"
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m17237_gshared (ValueCollection_t2674 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	ObjectU5BU5D_t356* V_0 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (ObjectU5BU5D_t356*)((ObjectU5BU5D_t356*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		ObjectU5BU5D_t356* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t356* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((ValueCollection_t2674 *)__this);
		(( void (*) (ValueCollection_t2674 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((ValueCollection_t2674 *)__this, (ObjectU5BU5D_t356*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2665 * L_4 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		Array_t * L_5 = ___array;
		int32_t L_6 = ___index;
		NullCheck((Dictionary_2_t2665 *)L_4);
		(( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t2665 *)L_4, (Array_t *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t2665 * L_7 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		Array_t * L_8 = ___array;
		int32_t L_9 = ___index;
		IntPtr_t L_10 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t2676 * L_11 = (Transform_1_t2676 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t2676 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_11, (Object_t *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t2665 *)L_7);
		(( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, Transform_1_t2676 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t2665 *)L_7, (Array_t *)L_8, (int32_t)L_9, (Transform_1_t2676 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17238_gshared (ValueCollection_t2674 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t2674 *)__this);
		Enumerator_t2675  L_0 = (( Enumerator_t2675  (*) (ValueCollection_t2674 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ValueCollection_t2674 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t2675  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17239_gshared (ValueCollection_t2674 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17240_gshared (ValueCollection_t2674 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern TypeInfo* ICollection_t1751_il2cpp_TypeInfo_var;
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m17241_gshared (ValueCollection_t2674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1751_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1034);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2665 * L_0 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1751_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::CopyTo(TValue[],System.Int32)
#include "mscorlib_ArrayTypes.h"
extern "C" void ValueCollection_CopyTo_m17242_gshared (ValueCollection_t2674 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Dictionary_2_t2665 * L_0 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		ObjectU5BU5D_t356* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Dictionary_2_t2665 *)L_0);
		(( void (*) (Dictionary_2_t2665 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t2665 *)L_0, (Array_t *)(Array_t *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t2665 * L_3 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		ObjectU5BU5D_t356* L_4 = ___array;
		int32_t L_5 = ___index;
		IntPtr_t L_6 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t2676 * L_7 = (Transform_1_t2676 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t2676 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_7, (Object_t *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t2665 *)L_3);
		(( void (*) (Dictionary_2_t2665 *, ObjectU5BU5D_t356*, int32_t, Transform_1_t2676 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Dictionary_2_t2665 *)L_3, (ObjectU5BU5D_t356*)L_4, (int32_t)L_5, (Transform_1_t2676 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::GetEnumerator()
extern "C" Enumerator_t2675  ValueCollection_GetEnumerator_m17243_gshared (ValueCollection_t2674 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2665 * L_0 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		Enumerator_t2675  L_1 = {0};
		(( void (*) (Enumerator_t2675 *, Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(&L_1, (Dictionary_2_t2665 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m17244_gshared (ValueCollection_t2674 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2665 * L_0 = (Dictionary_2_t2665 *)(__this->___dictionary_0);
		NullCheck((Dictionary_2_t2665 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<EScreens,System.Object>::get_Count() */, (Dictionary_2_t2665 *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_43.h"
extern "C" void Enumerator__ctor_m17245_gshared (Enumerator_t2675 * __this, Dictionary_2_t2665 * ___host, const MethodInfo* method)
{
	{
		Dictionary_2_t2665 * L_0 = ___host;
		NullCheck((Dictionary_2_t2665 *)L_0);
		Enumerator_t2672  L_1 = (( Enumerator_t2672  (*) (Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2665 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17246_gshared (Enumerator_t2675 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672 * L_0 = (Enumerator_t2672 *)&(__this->___host_enumerator_0);
		Object_t * L_1 = (( Object_t * (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t2672 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m17247_gshared (Enumerator_t2675 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672 * L_0 = (Enumerator_t2672 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t2672 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17248_gshared (Enumerator_t2675 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672 * L_0 = (Enumerator_t2672 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t2672 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<EScreens,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m17249_gshared (Enumerator_t2675 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672 * L_0 = (Enumerator_t2672 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t2667 * L_1 = (KeyValuePair_2_t2667 *)&(L_0->___current_3);
		Object_t * L_2 = (( Object_t * (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t2667 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Transform_1__ctor_m17250_gshared (Transform_1_t2676 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Object>::Invoke(TKey,TValue)
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
extern "C" Object_t * Transform_1_Invoke_m17251_gshared (Transform_1_t2676 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m17251((Transform_1_t2676 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* EScreens_t188_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m17252_gshared (Transform_1_t2676 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EScreens_t188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(EScreens_t188_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Transform_1_EndInvoke_m17253_gshared (Transform_1_t2676 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Transform_1__ctor_m17254_gshared (Transform_1_t2666 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
extern "C" DictionaryEntry_t451  Transform_1_Invoke_m17255_gshared (Transform_1_t2666 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m17255((Transform_1_t2666 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t451  (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef DictionaryEntry_t451  (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* EScreens_t188_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m17256_gshared (Transform_1_t2666 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EScreens_t188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(EScreens_t188_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C" DictionaryEntry_t451  Transform_1_EndInvoke_m17257_gshared (Transform_1_t2666 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(DictionaryEntry_t451 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Transform_1__ctor_m17258_gshared (Transform_1_t2677 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::Invoke(TKey,TValue)
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
extern "C" KeyValuePair_2_t2667  Transform_1_Invoke_m17259_gshared (Transform_1_t2677 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m17259((Transform_1_t2677 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2667  (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef KeyValuePair_2_t2667  (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* EScreens_t188_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m17260_gshared (Transform_1_t2677 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EScreens_t188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(EScreens_t188_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t2667  Transform_1_EndInvoke_m17261_gshared (Transform_1_t2677 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(KeyValuePair_2_t2667 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_43.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void ShimEnumerator__ctor_m17262_gshared (ShimEnumerator_t2678 * __this, Dictionary_2_t2665 * ___host, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2665 * L_0 = ___host;
		NullCheck((Dictionary_2_t2665 *)L_0);
		Enumerator_t2672  L_1 = (( Enumerator_t2672  (*) (Dictionary_2_t2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2665 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17263_gshared (ShimEnumerator_t2678 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2672 * L_0 = (Enumerator_t2672 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t2672 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::get_Entry()
extern TypeInfo* IDictionaryEnumerator_t1606_il2cpp_TypeInfo_var;
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m17264_gshared (ShimEnumerator_t2678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryEnumerator_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1035);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t2672  L_0 = (Enumerator_t2672 )(__this->___host_enumerator_0);
		Enumerator_t2672  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Object_t *)L_2);
		DictionaryEntry_t451  L_3 = (DictionaryEntry_t451 )InterfaceFuncInvoker0< DictionaryEntry_t451  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t1606_il2cpp_TypeInfo_var, (Object_t *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17265_gshared (ShimEnumerator_t2678 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2667  V_0 = {0};
	{
		Enumerator_t2672 * L_0 = (Enumerator_t2672 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t2667  L_1 = (( KeyValuePair_2_t2667  (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t2672 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2667 )L_1;
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2667 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17266_gshared (ShimEnumerator_t2678 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2667  V_0 = {0};
	{
		Enumerator_t2672 * L_0 = (Enumerator_t2672 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t2667  L_1 = (( KeyValuePair_2_t2667  (*) (Enumerator_t2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t2672 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2667 )L_1;
		Object_t * L_2 = (( Object_t * (*) (KeyValuePair_2_t2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t2667 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::get_Current()
extern TypeInfo* DictionaryEntry_t451_il2cpp_TypeInfo_var;
extern "C" Object_t * ShimEnumerator_get_Current_m17267_gshared (ShimEnumerator_t2678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DictionaryEntry_t451_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(271);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2678 *)__this);
		DictionaryEntry_t451  L_0 = (DictionaryEntry_t451 )VirtFuncInvoker0< DictionaryEntry_t451  >::Invoke(6 /* System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<EScreens,System.Object>::get_Entry() */, (ShimEnumerator_t2678 *)__this);
		DictionaryEntry_t451  L_1 = L_0;
		Object_t * L_2 = Box(DictionaryEntry_t451_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<EScreens>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_1.h"
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<EScreens>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_1MethodDeclarations.h"
// System.Void System.Collections.Generic.EqualityComparer`1<EScreens>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void EqualityComparer_1__ctor_m17268_gshared (EqualityComparer_1_t2679 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<EScreens>::.cctor()
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
extern const Il2CppType* GenericEqualityComparer_1_t3938_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1005_il2cpp_TypeInfo_var;
extern "C" void EqualityComparer_1__cctor_m17269_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericEqualityComparer_1_t3938_0_0_0_var = il2cpp_codegen_type_from_index(4049);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		TypeU5BU5D_t1005_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(623);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(GenericEqualityComparer_1_t3938_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1005* L_4 = (TypeU5BU5D_t1005*)((TypeU5BU5D_t1005*)SZArrayNew(TypeU5BU5D_t1005_il2cpp_TypeInfo_var, 1));
		Type_t * L_5 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 0, sizeof(Type_t *))) = (Type_t *)L_5;
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1005* >::Invoke(77 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1005*)L_4);
		Object_t * L_7 = Activator_CreateInstance_m5150(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2679_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = ((EqualityComparer_1_t2679 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2680 * L_8 = (DefaultComparer_t2680 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (DefaultComparer_t2680 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2679_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = L_8;
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<EScreens>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17270_gshared (EqualityComparer_1_t2679 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___obj;
		NullCheck((EqualityComparer_1_t2679 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<EScreens>::GetHashCode(T) */, (EqualityComparer_1_t2679 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<EScreens>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17271_gshared (EqualityComparer_1_t2679 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___x;
		Object_t * L_1 = ___y;
		NullCheck((EqualityComparer_1_t2679 *)__this);
		bool L_2 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<EScreens>::Equals(T,T) */, (EqualityComparer_1_t2679 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<EScreens>::get_Default()
extern "C" EqualityComparer_1_t2679 * EqualityComparer_1_get_Default_m17272_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2679 * L_0 = ((EqualityComparer_1_t2679_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0;
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<EScreens>::.ctor()
extern "C" void DefaultComparer__ctor_m17273_gshared (DefaultComparer_t2680 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2679 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		(( void (*) (EqualityComparer_1_t2679 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((EqualityComparer_1_t2679 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<EScreens>::GetHashCode(T)
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
extern "C" int32_t DefaultComparer_GetHashCode_m17274_gshared (DefaultComparer_t2680 * __this, int32_t ___obj, const MethodInfo* method)
{
	{
		int32_t L_0 = ___obj;
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___obj)));
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___obj)));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<EScreens>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m17275_gshared (DefaultComparer_t2680 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x;
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return ((((Object_t*)(Object_t *)L_3) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y;
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___x)));
		bool L_7 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___x)), (Object_t *)L_6);
		return L_7;
	}
}
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_44.h"
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_44MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_19.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
// System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_21.h"
// System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_22.h"
// System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_2.h"
// System.Collections.Generic.EqualityComparer`1<ESubScreens>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_2.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_43.h"
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_21MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_22MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_2MethodDeclarations.h"
// System.Collections.Generic.EqualityComparer`1<ESubScreens>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_2MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_19MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_43MethodDeclarations.h"
struct DictionaryEntryU5BU5D_t3822;
struct Transform_1_t2689;
// Declaration System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t451_TisDictionaryEntry_t451_m30299_gshared (Dictionary_2_t2688 * __this, DictionaryEntryU5BU5D_t3822* p0, int32_t p1, Transform_1_t2689 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisDictionaryEntry_t451_TisDictionaryEntry_t451_m30299(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2688 *, DictionaryEntryU5BU5D_t3822*, int32_t, Transform_1_t2689 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisDictionaryEntry_t451_TisDictionaryEntry_t451_m30299_gshared)(__this, p0, p1, p2, method)
struct Array_t;
struct Transform_1_t2700;
// Declaration System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2690_m30300_gshared (Dictionary_2_t2688 * __this, Array_t * p0, int32_t p1, Transform_1_t2700 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2690_m30300(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, Transform_1_t2700 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2690_m30300_gshared)(__this, p0, p1, p2, method)
struct KeyValuePair_2U5BU5D_t3655;
struct Transform_1_t2700;
// Declaration System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>,System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>,System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2690_TisKeyValuePair_2_t2690_m30302_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2U5BU5D_t3655* p0, int32_t p1, Transform_1_t2700 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2690_TisKeyValuePair_2_t2690_m30302(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2688 *, KeyValuePair_2U5BU5D_t3655*, int32_t, Transform_1_t2700 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2690_TisKeyValuePair_2_t2690_m30302_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void Dictionary_2__ctor_m17326_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2688 *)__this, (int32_t)((int32_t)10), (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17328_gshared (Dictionary_2_t2688 * __this, Object_t* ___comparer, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___comparer;
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2688 *)__this, (int32_t)((int32_t)10), (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::.ctor(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void Dictionary_2__ctor_m17330_gshared (Dictionary_2_t2688 * __this, int32_t ___capacity, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity;
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2688 *)__this, (int32_t)L_0, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
extern "C" void Dictionary_2__ctor_m17332_gshared (Dictionary_2_t2688 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		SerializationInfo_t1012 * L_0 = ___info;
		__this->___serialization_info_13 = L_0;
		return;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m17334_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t2688 *)__this);
		KeyCollection_t2693 * L_0 = (( KeyCollection_t2693 * (*) (Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Dictionary_2_t2688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17336_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		if (!((Object_t *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_1 = ___key;
		NullCheck((Dictionary_2_t2688 *)__this);
		bool L_2 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ContainsKey(TKey) */, (Dictionary_2_t2688 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t2688 *)__this);
		int32_t L_4 = (( int32_t (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Dictionary_2_t2688 *)__this, (Object_t *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Dictionary_2_t2688 *)__this);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(19 /* TValue System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Item(TKey) */, (Dictionary_2_t2688 *)__this, (int32_t)L_4);
		return L_5;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17338_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((Dictionary_2_t2688 *)__this);
		int32_t L_1 = (( int32_t (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Dictionary_2_t2688 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Object_t * L_2 = ___value;
		NullCheck((Dictionary_2_t2688 *)__this);
		Object_t * L_3 = (( Object_t * (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Dictionary_2_t2688 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Dictionary_2_t2688 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::set_Item(TKey,TValue) */, (Dictionary_2_t2688 *)__this, (int32_t)L_1, (Object_t *)L_3);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17340_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((Dictionary_2_t2688 *)__this);
		int32_t L_1 = (( int32_t (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Dictionary_2_t2688 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Object_t * L_2 = ___value;
		NullCheck((Dictionary_2_t2688 *)__this);
		Object_t * L_3 = (( Object_t * (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Dictionary_2_t2688 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Dictionary_2_t2688 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Add(TKey,TValue) */, (Dictionary_2_t2688 *)__this, (int32_t)L_1, (Object_t *)L_3);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.Contains(System.Object)
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17342_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (!((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t2688 *)__this);
		bool L_4 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ContainsKey(TKey) */, (Dictionary_2_t2688 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))));
		return L_4;
	}

IL_0029:
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17344_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (!((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t2688 *)__this);
		VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Remove(TKey) */, (Dictionary_2_t2688 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17346_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17348_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17350_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17352_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2_t2690  ___keyValuePair, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t2690 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Object_t * L_1 = (( Object_t * (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((KeyValuePair_2_t2690 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((Dictionary_2_t2688 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Add(TKey,TValue) */, (Dictionary_2_t2688 *)__this, (int32_t)L_0, (Object_t *)L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17354_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2_t2690  ___keyValuePair, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2690  L_0 = ___keyValuePair;
		NullCheck((Dictionary_2_t2688 *)__this);
		bool L_1 = (( bool (*) (Dictionary_2_t2688 *, KeyValuePair_2_t2690 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Dictionary_2_t2688 *)__this, (KeyValuePair_2_t2690 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#include "mscorlib_ArrayTypes.h"
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17356_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2U5BU5D_t3655* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t3655* L_0 = ___array;
		int32_t L_1 = ___index;
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, KeyValuePair_2U5BU5D_t3655*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Dictionary_2_t2688 *)__this, (KeyValuePair_2U5BU5D_t3655*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17358_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2_t2690  ___keyValuePair, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2690  L_0 = ___keyValuePair;
		NullCheck((Dictionary_2_t2688 *)__this);
		bool L_1 = (( bool (*) (Dictionary_2_t2688 *, KeyValuePair_2_t2690 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Dictionary_2_t2688 *)__this, (KeyValuePair_2_t2690 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t2690 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((Dictionary_2_t2688 *)__this);
		bool L_3 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Remove(TKey) */, (Dictionary_2_t2688 *)__this, (int32_t)L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_Array.h"
extern TypeInfo* DictionaryEntryU5BU5D_t3822_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17360_gshared (Dictionary_2_t2688 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DictionaryEntryU5BU5D_t3822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4051);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t3655* V_0 = {0};
	DictionaryEntryU5BU5D_t3822* V_1 = {0};
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t3822* G_B5_1 = {0};
	Dictionary_2_t2688 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t3822* G_B4_1 = {0};
	Dictionary_2_t2688 * G_B4_2 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (KeyValuePair_2U5BU5D_t3655*)((KeyValuePair_2U5BU5D_t3655*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)));
		KeyValuePair_2U5BU5D_t3655* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t3655* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, KeyValuePair_2U5BU5D_t3655*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Dictionary_2_t2688 *)__this, (KeyValuePair_2U5BU5D_t3655*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return;
	}

IL_0016:
	{
		Array_t * L_4 = ___array;
		int32_t L_5 = ___index;
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Dictionary_2_t2688 *)__this, (Array_t *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		Array_t * L_6 = ___array;
		V_1 = (DictionaryEntryU5BU5D_t3822*)((DictionaryEntryU5BU5D_t3822*)IsInst(L_6, DictionaryEntryU5BU5D_t3822_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t3822* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t3822* L_8 = V_1;
		int32_t L_9 = ___index;
		Transform_1_t2689 * L_10 = ((Dictionary_2_t2688_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->static_fields)->___U3CU3Ef__amU24cacheB_15;
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t2688 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t2688 *)(__this));
			goto IL_0046;
		}
	}
	{
		IntPtr_t L_11 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18) };
		Transform_1_t2689 * L_12 = (Transform_1_t2689 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		(( void (*) (Transform_1_t2689 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)(L_12, (Object_t *)NULL, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		((Dictionary_2_t2688_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->static_fields)->___U3CU3Ef__amU24cacheB_15 = L_12;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t2688 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t2689 * L_13 = ((Dictionary_2_t2688_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->static_fields)->___U3CU3Ef__amU24cacheB_15;
		NullCheck((Dictionary_2_t2688 *)G_B5_2);
		(( void (*) (Dictionary_2_t2688 *, DictionaryEntryU5BU5D_t3822*, int32_t, Transform_1_t2689 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((Dictionary_2_t2688 *)G_B5_2, (DictionaryEntryU5BU5D_t3822*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t2689 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		return;
	}

IL_0051:
	{
		Array_t * L_14 = ___array;
		int32_t L_15 = ___index;
		IntPtr_t L_16 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22) };
		Transform_1_t2700 * L_17 = (Transform_1_t2700 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		(( void (*) (Transform_1_t2700 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(L_17, (Object_t *)NULL, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, Transform_1_t2700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)((Dictionary_2_t2688 *)__this, (Array_t *)L_14, (int32_t)L_15, (Transform_1_t2700 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17362_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695  L_0 = {0};
		(( void (*) (Enumerator_t2695 *, Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(&L_0, (Dictionary_2_t2688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		Enumerator_t2695  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17364_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695  L_0 = {0};
		(( void (*) (Enumerator_t2695 *, Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(&L_0, (Dictionary_2_t2688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		Enumerator_t2695  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17366_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		ShimEnumerator_t2701 * L_0 = (ShimEnumerator_t2701 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		(( void (*) (ShimEnumerator_t2701 *, Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)(L_0, (Dictionary_2_t2688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17368_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___count_10);
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Item(TKey)
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
// System.Collections.Generic.KeyNotFoundException
#include "mscorlib_System_Collections_Generic_KeyNotFoundExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* KeyNotFoundException_t1870_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" Object_t * Dictionary_2_get_Item_m17370_gshared (Dictionary_2_t2688 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		KeyNotFoundException_t1870_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4052);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ESubScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t401* L_5 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t2500* L_9 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		ESubScreensU5BU5D_t2685* L_14 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ESubScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_0089;
		}
	}
	{
		ObjectU5BU5D_t356* L_19 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		return (*(Object_t **)(Object_t **)SZArrayLdElema(L_19, L_21, sizeof(Object_t *)));
	}

IL_0089:
	{
		LinkU5BU5D_t2500* L_22 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1869 )))->___Next_1);
		V_1 = (int32_t)L_24;
	}

IL_009b:
	{
		int32_t L_25 = V_1;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t1870 * L_26 = (KeyNotFoundException_t1870 *)il2cpp_codegen_object_new (KeyNotFoundException_t1870_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m10895(L_26, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_26);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::set_Item(TKey,TValue)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" void Dictionary_2_set_Item_m17372_gshared (Dictionary_2_t2688 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ESubScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))));
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t2500* L_11 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_11, L_12, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0087;
		}
	}
	{
		Object_t* L_15 = (Object_t*)(__this->___hcp_12);
		ESubScreensU5BU5D_t2685* L_16 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		int32_t L_19 = ___key;
		NullCheck((Object_t*)L_15);
		bool L_20 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ESubScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_15, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_16, L_18, sizeof(int32_t))), (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_21 = V_2;
		V_3 = (int32_t)L_21;
		LinkU5BU5D_t2500* L_22 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_24;
		int32_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_27 = (int32_t)(__this->___count_10);
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_4 = (int32_t)L_28;
		__this->___count_10 = L_28;
		int32_t L_29 = V_4;
		int32_t L_30 = (int32_t)(__this->___threshold_11);
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)((Dictionary_2_t2688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		int32_t L_31 = V_0;
		Int32U5BU5D_t401* L_32 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_32)->max_length)))));
	}

IL_00de:
	{
		int32_t L_33 = (int32_t)(__this->___emptySlot_9);
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_35 = (int32_t)(__this->___touchedSlots_8);
		int32_t L_36 = (int32_t)L_35;
		V_4 = (int32_t)L_36;
		__this->___touchedSlots_8 = ((int32_t)((int32_t)L_36+(int32_t)1));
		int32_t L_37 = V_4;
		V_2 = (int32_t)L_37;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t2500* L_38 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_38, L_39, sizeof(Link_t1869 )))->___Next_1);
		__this->___emptySlot_9 = L_40;
	}

IL_011c:
	{
		LinkU5BU5D_t2500* L_41 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		Int32U5BU5D_t401* L_43 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_44 = V_1;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		int32_t L_45 = L_44;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_41, L_42, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_43, L_45, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_46 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_47 = V_1;
		int32_t L_48 = V_2;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_46, L_47, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_48+(int32_t)1));
		LinkU5BU5D_t2500* L_49 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_50 = V_2;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		int32_t L_51 = V_0;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_49, L_50, sizeof(Link_t1869 )))->___HashCode_0 = L_51;
		ESubScreensU5BU5D_t2685* L_52 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		int32_t L_53 = V_2;
		int32_t L_54 = ___key;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_52, L_53, sizeof(int32_t))) = (int32_t)L_54;
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_55 = V_3;
		if ((((int32_t)L_55) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t2500* L_56 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_57 = V_3;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		LinkU5BU5D_t2500* L_58 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_59 = V_2;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		int32_t L_60 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_58, L_59, sizeof(Link_t1869 )))->___Next_1);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_56, L_57, sizeof(Link_t1869 )))->___Next_1 = L_60;
		LinkU5BU5D_t2500* L_61 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_62 = V_2;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		Int32U5BU5D_t401* L_63 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_64 = V_1;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, L_64);
		int32_t L_65 = L_64;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_61, L_62, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_63, L_65, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_66 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_67 = V_1;
		int32_t L_68 = V_2;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_66, L_67, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_01b5:
	{
		ObjectU5BU5D_t356* L_69 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_70 = V_2;
		Object_t * L_71 = ___value;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, L_70);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_69, L_70, sizeof(Object_t *))) = (Object_t *)L_71;
		int32_t L_72 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_72+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1870;
extern "C" void Dictionary_2_Init_m17374_gshared (Dictionary_2_t2688 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(906);
		_stringLiteral1870 = il2cpp_codegen_string_literal_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	Dictionary_2_t2688 * G_B4_0 = {0};
	Dictionary_2_t2688 * G_B3_0 = {0};
	Object_t* G_B5_0 = {0};
	Dictionary_2_t2688 * G_B5_1 = {0};
	{
		int32_t L_0 = ___capacity;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1419 * L_1 = (ArgumentOutOfRangeException_t1419 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8460(L_1, (String_t*)_stringLiteral1870, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		Object_t* L_2 = ___hcp;
		G_B3_0 = ((Dictionary_2_t2688 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t2688 *)(__this));
			goto IL_0021;
		}
	}
	{
		Object_t* L_3 = ___hcp;
		V_0 = (Object_t*)L_3;
		Object_t* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t2688 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		EqualityComparer_1_t2702 * L_5 = (( EqualityComparer_1_t2702 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		G_B5_0 = ((Object_t*)(L_5));
		G_B5_1 = ((Dictionary_2_t2688 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->___hcp_12 = G_B5_0;
		int32_t L_6 = ___capacity;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity;
		___capacity = (int32_t)((int32_t)((int32_t)(((int32_t)((float)((float)(((float)L_7))/(float)(0.9f)))))+(int32_t)1));
		int32_t L_8 = ___capacity;
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)((Dictionary_2_t2688 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		__this->___generation_14 = 0;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::InitArrays(System.Int32)
extern TypeInfo* Int32U5BU5D_t401_il2cpp_TypeInfo_var;
extern TypeInfo* LinkU5BU5D_t2500_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_InitArrays_m17376_gshared (Dictionary_2_t2688 * __this, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		LinkU5BU5D_t2500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4053);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size;
		__this->___table_4 = ((Int32U5BU5D_t401*)SZArrayNew(Int32U5BU5D_t401_il2cpp_TypeInfo_var, L_0));
		int32_t L_1 = ___size;
		__this->___linkSlots_5 = ((LinkU5BU5D_t2500*)SZArrayNew(LinkU5BU5D_t2500_il2cpp_TypeInfo_var, L_1));
		__this->___emptySlot_9 = (-1);
		int32_t L_2 = ___size;
		__this->___keySlots_6 = ((ESubScreensU5BU5D_t2685*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), L_2));
		int32_t L_3 = ___size;
		__this->___valueSlots_7 = ((ObjectU5BU5D_t356*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36), L_3));
		__this->___touchedSlots_8 = 0;
		Int32U5BU5D_t401* L_4 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_4);
		__this->___threshold_11 = (((int32_t)((float)((float)(((float)(((int32_t)(((Array_t *)L_4)->max_length)))))*(float)(0.9f)))));
		int32_t L_5 = (int32_t)(__this->___threshold_11);
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->___threshold_11 = 1;
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::CopyToCheck(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1265;
extern Il2CppCodeGenString* _stringLiteral1267;
extern Il2CppCodeGenString* _stringLiteral3389;
extern Il2CppCodeGenString* _stringLiteral3390;
extern "C" void Dictionary_2_CopyToCheck_m17378_gshared (Dictionary_2_t2688 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(906);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral1265 = il2cpp_codegen_string_literal_from_index(1265);
		_stringLiteral1267 = il2cpp_codegen_string_literal_from_index(1267);
		_stringLiteral3389 = il2cpp_codegen_string_literal_from_index(3389);
		_stringLiteral3390 = il2cpp_codegen_string_literal_from_index(3390);
		s_Il2CppMethodIntialized = true;
	}
	{
		Array_t * L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral1265, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t1419 * L_3 = (ArgumentOutOfRangeException_t1419 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8460(L_3, (String_t*)_stringLiteral1267, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index;
		Array_t * L_5 = ___array;
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t764 * L_7 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_7, (String_t*)_stringLiteral3389, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_003a:
	{
		Array_t * L_8 = ___array;
		NullCheck((Array_t *)L_8);
		int32_t L_9 = Array_get_Length_m9469((Array_t *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index;
		NullCheck((Dictionary_2_t2688 *)__this);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Count() */, (Dictionary_2_t2688 *)__this);
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t764 * L_12 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_12, (String_t*)_stringLiteral3390, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2690  Dictionary_2_make_pair_m17380_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		Object_t * L_1 = ___value;
		KeyValuePair_2_t2690  L_2 = {0};
		(( void (*) (KeyValuePair_2_t2690 *, int32_t, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)(&L_2, (int32_t)L_0, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m17382_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m17384_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17386_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2U5BU5D_t3655* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t3655* L_0 = ___array;
		int32_t L_1 = ___index;
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Dictionary_2_t2688 *)__this, (Array_t *)(Array_t *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		KeyValuePair_2U5BU5D_t3655* L_2 = ___array;
		int32_t L_3 = ___index;
		IntPtr_t L_4 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22) };
		Transform_1_t2700 * L_5 = (Transform_1_t2700 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		(( void (*) (Transform_1_t2700 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(L_5, (Object_t *)NULL, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, KeyValuePair_2U5BU5D_t3655*, int32_t, Transform_1_t2700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40)->method)((Dictionary_2_t2688 *)__this, (KeyValuePair_2U5BU5D_t3655*)L_2, (int32_t)L_3, (Transform_1_t2700 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Resize()
// System.Collections.Hashtable
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
extern TypeInfo* Hashtable_t261_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t401_il2cpp_TypeInfo_var;
extern TypeInfo* LinkU5BU5D_t2500_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_Resize_m17388_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(249);
		Int32U5BU5D_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		LinkU5BU5D_t2500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4053);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t401* V_1 = {0};
	LinkU5BU5D_t2500* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ESubScreensU5BU5D_t2685* V_7 = {0};
	ObjectU5BU5D_t356* V_8 = {0};
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t401* L_0 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t261_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m11127(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t401*)((Int32U5BU5D_t401*)SZArrayNew(Int32U5BU5D_t401_il2cpp_TypeInfo_var, L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t2500*)((LinkU5BU5D_t2500*)SZArrayNew(LinkU5BU5D_t2500_il2cpp_TypeInfo_var, L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t401* L_4 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_4 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6, sizeof(int32_t)))-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t2500* L_7 = V_2;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Object_t* L_9 = (Object_t*)(__this->___hcp_12);
		ESubScreensU5BU5D_t2685* L_10 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		int32_t L_11 = V_4;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((Object_t*)L_9);
		int32_t L_13 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ESubScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_9, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_10, L_12, sizeof(int32_t))));
		int32_t L_14 = (int32_t)((int32_t)((int32_t)L_13|(int32_t)((int32_t)-2147483648)));
		V_9 = (int32_t)L_14;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_7, L_8, sizeof(Link_t1869 )))->___HashCode_0 = L_14;
		int32_t L_15 = V_9;
		V_5 = (int32_t)L_15;
		int32_t L_16 = V_5;
		int32_t L_17 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)2147483647)))%(int32_t)L_17));
		LinkU5BU5D_t2500* L_18 = V_2;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		Int32U5BU5D_t401* L_20 = V_1;
		int32_t L_21 = V_6;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_18, L_19, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_20, L_22, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_23 = V_1;
		int32_t L_24 = V_6;
		int32_t L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_23, L_24, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		LinkU5BU5D_t2500* L_26 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_27 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_26, L_27, sizeof(Link_t1869 )))->___Next_1);
		V_4 = (int32_t)L_28;
	}

IL_00a5:
	{
		int32_t L_29 = V_4;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_30 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_31 = V_3;
		Int32U5BU5D_t401* L_32 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)(((Array_t *)L_32)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t401* L_33 = V_1;
		__this->___table_4 = L_33;
		LinkU5BU5D_t2500* L_34 = V_2;
		__this->___linkSlots_5 = L_34;
		int32_t L_35 = V_0;
		V_7 = (ESubScreensU5BU5D_t2685*)((ESubScreensU5BU5D_t2685*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), L_35));
		int32_t L_36 = V_0;
		V_8 = (ObjectU5BU5D_t356*)((ObjectU5BU5D_t356*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36), L_36));
		ESubScreensU5BU5D_t2685* L_37 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		ESubScreensU5BU5D_t2685* L_38 = V_7;
		int32_t L_39 = (int32_t)(__this->___touchedSlots_8);
		Array_Copy_m1754(NULL /*static, unused*/, (Array_t *)(Array_t *)L_37, (int32_t)0, (Array_t *)(Array_t *)L_38, (int32_t)0, (int32_t)L_39, /*hidden argument*/NULL);
		ObjectU5BU5D_t356* L_40 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		ObjectU5BU5D_t356* L_41 = V_8;
		int32_t L_42 = (int32_t)(__this->___touchedSlots_8);
		Array_Copy_m1754(NULL /*static, unused*/, (Array_t *)(Array_t *)L_40, (int32_t)0, (Array_t *)(Array_t *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		ESubScreensU5BU5D_t2685* L_43 = V_7;
		__this->___keySlots_6 = L_43;
		ObjectU5BU5D_t356* L_44 = V_8;
		__this->___valueSlots_7 = L_44;
		int32_t L_45 = V_0;
		__this->___threshold_11 = (((int32_t)((float)((float)(((float)L_45))*(float)(0.9f)))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Add(TKey,TValue)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern Il2CppCodeGenString* _stringLiteral3391;
extern "C" void Dictionary_2_Add_m17390_gshared (Dictionary_2_t2688 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		_stringLiteral3391 = il2cpp_codegen_string_literal_from_index(3391);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ESubScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))));
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t2500* L_10 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_10, L_11, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_14 = (Object_t*)(__this->___hcp_12);
		ESubScreensU5BU5D_t2685* L_15 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = ___key;
		NullCheck((Object_t*)L_14);
		bool L_19 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ESubScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_14, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_15, L_17, sizeof(int32_t))), (int32_t)L_18);
		if (!L_19)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t764 * L_20 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_20, (String_t*)_stringLiteral3391, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_20);
	}

IL_0089:
	{
		LinkU5BU5D_t2500* L_21 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_22 = V_2;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_21, L_22, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_23;
	}

IL_009b:
	{
		int32_t L_24 = V_2;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_25 = (int32_t)(__this->___count_10);
		int32_t L_26 = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		V_3 = (int32_t)L_26;
		__this->___count_10 = L_26;
		int32_t L_27 = V_3;
		int32_t L_28 = (int32_t)(__this->___threshold_11);
		if ((((int32_t)L_27) <= ((int32_t)L_28)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)((Dictionary_2_t2688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		int32_t L_29 = V_0;
		Int32U5BU5D_t401* L_30 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_30);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_29&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_30)->max_length)))));
	}

IL_00d5:
	{
		int32_t L_31 = (int32_t)(__this->___emptySlot_9);
		V_2 = (int32_t)L_31;
		int32_t L_32 = V_2;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_33 = (int32_t)(__this->___touchedSlots_8);
		int32_t L_34 = (int32_t)L_33;
		V_3 = (int32_t)L_34;
		__this->___touchedSlots_8 = ((int32_t)((int32_t)L_34+(int32_t)1));
		int32_t L_35 = V_3;
		V_2 = (int32_t)L_35;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t2500* L_36 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_37 = V_2;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_36, L_37, sizeof(Link_t1869 )))->___Next_1);
		__this->___emptySlot_9 = L_38;
	}

IL_0111:
	{
		LinkU5BU5D_t2500* L_39 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_40 = V_2;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = V_0;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_39, L_40, sizeof(Link_t1869 )))->___HashCode_0 = L_41;
		LinkU5BU5D_t2500* L_42 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_43 = V_2;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		Int32U5BU5D_t401* L_44 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_45 = V_1;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		int32_t L_46 = L_45;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_42, L_43, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_44, L_46, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_47 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_48 = V_1;
		int32_t L_49 = V_2;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_47, L_48, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_49+(int32_t)1));
		ESubScreensU5BU5D_t2685* L_50 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		int32_t L_51 = V_2;
		int32_t L_52 = ___key;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_50, L_51, sizeof(int32_t))) = (int32_t)L_52;
		ObjectU5BU5D_t356* L_53 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_54 = V_2;
		Object_t * L_55 = ___value;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_53, L_54, sizeof(Object_t *))) = (Object_t *)L_55;
		int32_t L_56 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_56+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m17392_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		__this->___count_10 = 0;
		Int32U5BU5D_t401* L_0 = (Int32U5BU5D_t401*)(__this->___table_4);
		Int32U5BU5D_t401* L_1 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_1);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_1)->max_length))), /*hidden argument*/NULL);
		ESubScreensU5BU5D_t2685* L_2 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		ESubScreensU5BU5D_t2685* L_3 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		NullCheck(L_3);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_3)->max_length))), /*hidden argument*/NULL);
		ObjectU5BU5D_t356* L_4 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		ObjectU5BU5D_t356* L_5 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		NullCheck(L_5);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_4, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_5)->max_length))), /*hidden argument*/NULL);
		LinkU5BU5D_t2500* L_6 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		LinkU5BU5D_t2500* L_7 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		NullCheck(L_7);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_7)->max_length))), /*hidden argument*/NULL);
		__this->___emptySlot_9 = (-1);
		__this->___touchedSlots_8 = 0;
		int32_t L_8 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_8+(int32_t)1));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ContainsKey(TKey)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_ContainsKey_m17394_gshared (Dictionary_2_t2688 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ESubScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t401* L_5 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t2500* L_9 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_007e;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		ESubScreensU5BU5D_t2685* L_14 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ESubScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_007e;
		}
	}
	{
		return 1;
	}

IL_007e:
	{
		LinkU5BU5D_t2500* L_19 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_19, L_20, sizeof(Link_t1869 )))->___Next_1);
		V_1 = (int32_t)L_21;
	}

IL_0090:
	{
		int32_t L_22 = V_1;
		if ((!(((uint32_t)L_22) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17396_gshared (Dictionary_2_t2688 * __this, Object_t * ___value, const MethodInfo* method)
{
	Object_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42));
		EqualityComparer_1_t2473 * L_0 = (( EqualityComparer_1_t2473 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
		V_0 = (Object_t*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t401* L_1 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_3, sizeof(int32_t)))-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		Object_t* L_4 = V_0;
		ObjectU5BU5D_t356* L_5 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Object_t * L_8 = ___value;
		NullCheck((Object_t*)L_4);
		bool L_9 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43), (Object_t*)L_4, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *))), (Object_t *)L_8);
		if (!L_9)
		{
			goto IL_0037;
		}
	}
	{
		return 1;
	}

IL_0037:
	{
		LinkU5BU5D_t2500* L_10 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_10, L_11, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_12;
	}

IL_0049:
	{
		int32_t L_13 = V_2;
		if ((!(((uint32_t)L_13) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_14 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_15 = V_1;
		Int32U5BU5D_t401* L_16 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)(((Array_t *)L_16)->max_length))))))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1277;
extern Il2CppCodeGenString* _stringLiteral1279;
extern Il2CppCodeGenString* _stringLiteral1281;
extern Il2CppCodeGenString* _stringLiteral1904;
extern Il2CppCodeGenString* _stringLiteral3392;
extern "C" void Dictionary_2_GetObjectData_m17398_gshared (Dictionary_2_t2688 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral1277 = il2cpp_codegen_string_literal_from_index(1277);
		_stringLiteral1279 = il2cpp_codegen_string_literal_from_index(1279);
		_stringLiteral1281 = il2cpp_codegen_string_literal_from_index(1281);
		_stringLiteral1904 = il2cpp_codegen_string_literal_from_index(1904);
		_stringLiteral3392 = il2cpp_codegen_string_literal_from_index(3392);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t3655* V_0 = {0};
	{
		SerializationInfo_t1012 * L_0 = ___info;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral1277, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SerializationInfo_t1012 * L_2 = ___info;
		int32_t L_3 = (int32_t)(__this->___generation_14);
		NullCheck((SerializationInfo_t1012 *)L_2);
		SerializationInfo_AddValue_m9476((SerializationInfo_t1012 *)L_2, (String_t*)_stringLiteral1279, (int32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t1012 * L_4 = ___info;
		Object_t* L_5 = (Object_t*)(__this->___hcp_12);
		NullCheck((SerializationInfo_t1012 *)L_4);
		SerializationInfo_AddValue_m8509((SerializationInfo_t1012 *)L_4, (String_t*)_stringLiteral1281, (Object_t *)L_5, /*hidden argument*/NULL);
		V_0 = (KeyValuePair_2U5BU5D_t3655*)NULL;
		int32_t L_6 = (int32_t)(__this->___count_10);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = (int32_t)(__this->___count_10);
		V_0 = (KeyValuePair_2U5BU5D_t3655*)((KeyValuePair_2U5BU5D_t3655*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44), L_7));
		KeyValuePair_2U5BU5D_t3655* L_8 = V_0;
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, KeyValuePair_2U5BU5D_t3655*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Dictionary_2_t2688 *)__this, (KeyValuePair_2U5BU5D_t3655*)L_8, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
	}

IL_0055:
	{
		SerializationInfo_t1012 * L_9 = ___info;
		Int32U5BU5D_t401* L_10 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_10);
		NullCheck((SerializationInfo_t1012 *)L_9);
		SerializationInfo_AddValue_m9476((SerializationInfo_t1012 *)L_9, (String_t*)_stringLiteral1904, (int32_t)(((int32_t)(((Array_t *)L_10)->max_length))), /*hidden argument*/NULL);
		SerializationInfo_t1012 * L_11 = ___info;
		KeyValuePair_2U5BU5D_t3655* L_12 = V_0;
		NullCheck((SerializationInfo_t1012 *)L_11);
		SerializationInfo_AddValue_m8509((SerializationInfo_t1012 *)L_11, (String_t*)_stringLiteral3392, (Object_t *)(Object_t *)L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::OnDeserialization(System.Object)
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1279;
extern Il2CppCodeGenString* _stringLiteral1281;
extern Il2CppCodeGenString* _stringLiteral1904;
extern Il2CppCodeGenString* _stringLiteral3392;
extern "C" void Dictionary_2_OnDeserialization_m17400_gshared (Dictionary_2_t2688 * __this, Object_t * ___sender, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		_stringLiteral1279 = il2cpp_codegen_string_literal_from_index(1279);
		_stringLiteral1281 = il2cpp_codegen_string_literal_from_index(1281);
		_stringLiteral1904 = il2cpp_codegen_string_literal_from_index(1904);
		_stringLiteral3392 = il2cpp_codegen_string_literal_from_index(3392);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t3655* V_1 = {0};
	int32_t V_2 = 0;
	{
		SerializationInfo_t1012 * L_0 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t1012 * L_1 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		NullCheck((SerializationInfo_t1012 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m9483((SerializationInfo_t1012 *)L_1, (String_t*)_stringLiteral1279, /*hidden argument*/NULL);
		__this->___generation_14 = L_2;
		SerializationInfo_t1012 * L_3 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1012 *)L_3);
		Object_t * L_5 = SerializationInfo_GetValue_m9477((SerializationInfo_t1012 *)L_3, (String_t*)_stringLiteral1281, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->___hcp_12 = ((Object_t*)Castclass(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30)));
		SerializationInfo_t1012 * L_6 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		NullCheck((SerializationInfo_t1012 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m9483((SerializationInfo_t1012 *)L_6, (String_t*)_stringLiteral1904, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t1012 * L_8 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		Type_t * L_9 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1012 *)L_8);
		Object_t * L_10 = SerializationInfo_GetValue_m9477((SerializationInfo_t1012 *)L_8, (String_t*)_stringLiteral3392, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t3655*)((KeyValuePair_2U5BU5D_t3655*)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t2688 *)__this);
		(( void (*) (Dictionary_2_t2688 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)((Dictionary_2_t2688 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		__this->___count_10 = 0;
		KeyValuePair_2U5BU5D_t3655* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t3655* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = (( int32_t (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t2690 *)((KeyValuePair_2_t2690 *)(KeyValuePair_2_t2690 *)SZArrayLdElema(L_14, L_15, sizeof(KeyValuePair_2_t2690 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		KeyValuePair_2U5BU5D_t3655* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		Object_t * L_19 = (( Object_t * (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((KeyValuePair_2_t2690 *)((KeyValuePair_2_t2690 *)(KeyValuePair_2_t2690 *)SZArrayLdElema(L_17, L_18, sizeof(KeyValuePair_2_t2690 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((Dictionary_2_t2688 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Add(TKey,TValue) */, (Dictionary_2_t2688 *)__this, (int32_t)L_16, (Object_t *)L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t3655* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)(((Array_t *)L_22)->max_length))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_23+(int32_t)1));
		__this->___serialization_info_13 = (SerializationInfo_t1012 *)NULL;
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Remove(TKey)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* ESubScreens_t189_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_Remove_m17402_gshared (Dictionary_2_t2688 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		ESubScreens_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = {0};
	Object_t * V_5 = {0};
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ESubScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))));
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		int32_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return 0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t2500* L_11 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_11, L_12, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_15 = (Object_t*)(__this->___hcp_12);
		ESubScreensU5BU5D_t2685* L_16 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		int32_t L_19 = ___key;
		NullCheck((Object_t*)L_15);
		bool L_20 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ESubScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_15, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_16, L_18, sizeof(int32_t))), (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_21 = V_2;
		V_3 = (int32_t)L_21;
		LinkU5BU5D_t2500* L_22 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_24;
		int32_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return 0;
	}

IL_00ad:
	{
		int32_t L_27 = (int32_t)(__this->___count_10);
		__this->___count_10 = ((int32_t)((int32_t)L_27-(int32_t)1));
		int32_t L_28 = V_3;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t401* L_29 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_30 = V_1;
		LinkU5BU5D_t2500* L_31 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_32 = V_2;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_31, L_32, sizeof(Link_t1869 )))->___Next_1);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, L_30, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t2500* L_34 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_35 = V_3;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		LinkU5BU5D_t2500* L_36 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_37 = V_2;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_36, L_37, sizeof(Link_t1869 )))->___Next_1);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_34, L_35, sizeof(Link_t1869 )))->___Next_1 = L_38;
	}

IL_0104:
	{
		LinkU5BU5D_t2500* L_39 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_40 = V_2;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = (int32_t)(__this->___emptySlot_9);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_39, L_40, sizeof(Link_t1869 )))->___Next_1 = L_41;
		int32_t L_42 = V_2;
		__this->___emptySlot_9 = L_42;
		LinkU5BU5D_t2500* L_43 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_44 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_43, L_44, sizeof(Link_t1869 )))->___HashCode_0 = 0;
		ESubScreensU5BU5D_t2685* L_45 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		int32_t L_46 = V_2;
		Initobj (ESubScreens_t189_il2cpp_TypeInfo_var, (&V_4));
		int32_t L_47 = V_4;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_45, L_46, sizeof(int32_t))) = (int32_t)L_47;
		ObjectU5BU5D_t356* L_48 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_49 = V_2;
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_5));
		Object_t * L_50 = V_5;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, L_49, sizeof(Object_t *))) = (Object_t *)L_50;
		int32_t L_51 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_51+(int32_t)1));
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::TryGetValue(TKey,TValue&)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_TryGetValue_m17404_gshared (Dictionary_2_t2688 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Object_t * V_2 = {0};
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ESubScreens>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t401* L_5 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t2500* L_9 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0090;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		ESubScreensU5BU5D_t2685* L_14 = (ESubScreensU5BU5D_t2685*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ESubScreens>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_0090;
		}
	}
	{
		Object_t ** L_19 = ___value;
		ObjectU5BU5D_t356* L_20 = (ObjectU5BU5D_t356*)(__this->___valueSlots_7);
		int32_t L_21 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		*L_19 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_20, L_22, sizeof(Object_t *)));
		return 1;
	}

IL_0090:
	{
		LinkU5BU5D_t2500* L_23 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_23, L_24, sizeof(Link_t1869 )))->___Next_1);
		V_1 = (int32_t)L_25;
	}

IL_00a2:
	{
		int32_t L_26 = V_1;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		Object_t ** L_27 = ___value;
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_2));
		Object_t * L_28 = V_2;
		*L_27 = L_28;
		return 0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Keys()
extern "C" KeyCollection_t2693 * Dictionary_2_get_Keys_m17406_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		KeyCollection_t2693 * L_0 = (KeyCollection_t2693 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		(( void (*) (KeyCollection_t2693 *, Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48)->method)(L_0, (Dictionary_2_t2688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Values()
extern "C" ValueCollection_t2697 * Dictionary_2_get_Values_m17408_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		ValueCollection_t2697 * L_0 = (ValueCollection_t2697 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49));
		(( void (*) (ValueCollection_t2697 *, Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50)->method)(L_0, (Dictionary_2_t2688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ToTKey(System.Object)
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern Il2CppCodeGenString* _stringLiteral3393;
extern "C" int32_t Dictionary_2_ToTKey_m17410_gshared (Dictionary_2_t2688 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		_stringLiteral3393 = il2cpp_codegen_string_literal_from_index(3393);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 51)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1319(NULL /*static, unused*/, (String_t*)_stringLiteral3393, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t764 * L_6 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m7574(L_6, (String_t*)L_5, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0040:
	{
		Object_t * L_7 = ___key;
		return ((*(int32_t*)((int32_t*)UnBox (L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)))));
	}
}
// TValue System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ToTValue(System.Object)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3393;
extern Il2CppCodeGenString* _stringLiteral64;
extern "C" Object_t * Dictionary_2_ToTValue_m17412_gshared (Dictionary_2_t2688 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral3393 = il2cpp_codegen_string_literal_from_index(3393);
		_stringLiteral64 = il2cpp_codegen_string_literal_from_index(64);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = ___value;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean System.Type::get_IsValueType() */, (Type_t *)L_1);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_0));
		Object_t * L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		Object_t * L_4 = ___value;
		if (((Object_t *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1319(NULL /*static, unused*/, (String_t*)_stringLiteral3393, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t764 * L_8 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m7574(L_8, (String_t*)L_7, (String_t*)_stringLiteral64, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0053:
	{
		Object_t * L_9 = ___value;
		return ((Object_t *)Castclass(L_9, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17414_gshared (Dictionary_2_t2688 * __this, KeyValuePair_2_t2690  ___pair, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		int32_t L_0 = (( int32_t (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t2690 *)(&___pair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((Dictionary_2_t2688 *)__this);
		bool L_1 = (bool)VirtFuncInvoker2< bool, int32_t, Object_t ** >::Invoke(33 /* System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::TryGetValue(TKey,TValue&) */, (Dictionary_2_t2688 *)__this, (int32_t)L_0, (Object_t **)(&V_0));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return 0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42));
		EqualityComparer_1_t2473 * L_2 = (( EqualityComparer_1_t2473 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
		Object_t * L_3 = (( Object_t * (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((KeyValuePair_2_t2690 *)(&___pair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Object_t * L_4 = V_0;
		NullCheck((EqualityComparer_1_t2473 *)L_2);
		bool L_5 = (bool)VirtFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t2473 *)L_2, (Object_t *)L_3, (Object_t *)L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::GetEnumerator()
extern "C" Enumerator_t2695  Dictionary_2_GetEnumerator_m17416_gshared (Dictionary_2_t2688 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695  L_0 = {0};
		(( void (*) (Enumerator_t2695 *, Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(&L_0, (Dictionary_2_t2688 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::<CopyTo>m__0(TKey,TValue)
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m17418_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		Object_t * L_3 = ___value;
		DictionaryEntry_t451  L_4 = {0};
		DictionaryEntry__ctor_m9467(&L_4, (Object_t *)L_2, (Object_t *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_31.h"
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_31MethodDeclarations.h"
// Declaration !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>(System.Int32)
extern "C" KeyValuePair_2_t2690  Array_InternalArray__get_Item_TisKeyValuePair_2_t2690_m30272_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2690_m30272(__this, p0, method) (( KeyValuePair_2_t2690  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2690_m30272_gshared)(__this, p0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>::.ctor(System.Array)
// System.Array
#include "mscorlib_System_Array.h"
extern "C" void InternalEnumerator_1__ctor_m17419_gshared (InternalEnumerator_1_t2691 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17420_gshared (InternalEnumerator_1_t2691 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2690  L_0 = (( KeyValuePair_2_t2690  (*) (InternalEnumerator_1_t2691 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2691 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2690  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17421_gshared (InternalEnumerator_1_t2691 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>::MoveNext()
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern "C" bool InternalEnumerator_1_MoveNext_m17422_gshared (InternalEnumerator_1_t2691 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9469((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>::get_Current()
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3382;
extern Il2CppCodeGenString* _stringLiteral3383;
extern "C" KeyValuePair_2_t2690  InternalEnumerator_1_get_Current_m17423_gshared (InternalEnumerator_1_t2691 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3382 = il2cpp_codegen_string_literal_from_index(3382);
		_stringLiteral3383 = il2cpp_codegen_string_literal_from_index(3383);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, (String_t*)_stringLiteral3382, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t425 * L_3 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_3, (String_t*)_stringLiteral3383, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		KeyValuePair_2_t2690  L_8 = (( KeyValuePair_2_t2690  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::.ctor(TKey,TValue)
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
// System.Object
#include "mscorlib_System_Object.h"
extern "C" void KeyValuePair_2__ctor_m17424_gshared (KeyValuePair_2_t2690 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		(( void (*) (KeyValuePair_2_t2690 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t2690 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t * L_1 = ___value;
		(( void (*) (KeyValuePair_2_t2690 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t2690 *)__this, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m17425_gshared (KeyValuePair_2_t2690 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___key_0);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m17426_gshared (KeyValuePair_2_t2690 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___key_0 = L_0;
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m17427_gshared (KeyValuePair_2_t2690 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___value_1);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m17428_gshared (KeyValuePair_2_t2690 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___value_1 = L_0;
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::ToString()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* StringU5BU5D_t137_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral719;
extern "C" String_t* KeyValuePair_2_ToString_m17429_gshared (KeyValuePair_2_t2690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral58 = il2cpp_codegen_string_literal_from_index(58);
		_stringLiteral46 = il2cpp_codegen_string_literal_from_index(46);
		_stringLiteral719 = il2cpp_codegen_string_literal_from_index(719);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	int32_t G_B2_0 = 0;
	StringU5BU5D_t137* G_B2_1 = {0};
	StringU5BU5D_t137* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	StringU5BU5D_t137* G_B1_1 = {0};
	StringU5BU5D_t137* G_B1_2 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	StringU5BU5D_t137* G_B3_2 = {0};
	StringU5BU5D_t137* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	StringU5BU5D_t137* G_B5_1 = {0};
	StringU5BU5D_t137* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	StringU5BU5D_t137* G_B4_1 = {0};
	StringU5BU5D_t137* G_B4_2 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	StringU5BU5D_t137* G_B6_2 = {0};
	StringU5BU5D_t137* G_B6_3 = {0};
	{
		StringU5BU5D_t137* L_0 = (StringU5BU5D_t137*)((StringU5BU5D_t137*)SZArrayNew(StringU5BU5D_t137_il2cpp_TypeInfo_var, 5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral58);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral58;
		StringU5BU5D_t137* L_1 = (StringU5BU5D_t137*)L_0;
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2690 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		int32_t L_3 = (( int32_t (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2690 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0)));
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(String_t*))) = (String_t*)G_B3_0;
		StringU5BU5D_t137* L_6 = (StringU5BU5D_t137*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral46);
		*((String_t**)(String_t**)SZArrayLdElema(L_6, 2, sizeof(String_t*))) = (String_t*)_stringLiteral46;
		StringU5BU5D_t137* L_7 = (StringU5BU5D_t137*)L_6;
		Object_t * L_8 = (( Object_t * (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2690 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Object_t * L_9 = (( Object_t * (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2690 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Object_t *)L_9;
		NullCheck((Object_t *)(*(&V_1)));
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(String_t*))) = (String_t*)G_B6_0;
		StringU5BU5D_t137* L_12 = (StringU5BU5D_t137*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral719);
		*((String_t**)(String_t**)SZArrayLdElema(L_12, 4, sizeof(String_t*))) = (String_t*)_stringLiteral719;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1461(NULL /*static, unused*/, (StringU5BU5D_t137*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Array/InternalEnumerator`1<ESubScreens>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_32.h"
// System.Array/InternalEnumerator`1<ESubScreens>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_32MethodDeclarations.h"
// Declaration !!0 System.Array::InternalArray__get_Item<ESubScreens>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<ESubScreens>(System.Int32)
extern "C" int32_t Array_InternalArray__get_Item_TisESubScreens_t189_m30283_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisESubScreens_t189_m30283(__this, p0, method) (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisESubScreens_t189_m30283_gshared)(__this, p0, method)
// System.Void System.Array/InternalEnumerator`1<ESubScreens>::.ctor(System.Array)
// System.Array
#include "mscorlib_System_Array.h"
extern "C" void InternalEnumerator_1__ctor_m17430_gshared (InternalEnumerator_1_t2692 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<ESubScreens>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17431_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (InternalEnumerator_1_t2692 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2692 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<ESubScreens>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17432_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<ESubScreens>::MoveNext()
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern "C" bool InternalEnumerator_1_MoveNext_m17433_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9469((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<ESubScreens>::get_Current()
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3382;
extern Il2CppCodeGenString* _stringLiteral3383;
extern "C" int32_t InternalEnumerator_1_get_Current_m17434_gshared (InternalEnumerator_1_t2692 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3382 = il2cpp_codegen_string_literal_from_index(3382);
		_stringLiteral3383 = il2cpp_codegen_string_literal_from_index(3383);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, (String_t*)_stringLiteral3382, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t425 * L_3 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_3, (String_t*)_stringLiteral3383, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		int32_t L_8 = (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_20.h"
// System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,ESubScreens>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_19.h"
// System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,ESubScreens>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_19MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_20MethodDeclarations.h"
struct Array_t;
struct Transform_1_t2696;
// Declaration System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_ICollectionCopyTo<ESubScreens>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_ICollectionCopyTo<ESubScreens>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisESubScreens_t189_m30294_gshared (Dictionary_2_t2688 * __this, Array_t * p0, int32_t p1, Transform_1_t2696 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisESubScreens_t189_m30294(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, Transform_1_t2696 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisESubScreens_t189_m30294_gshared)(__this, p0, p1, p2, method)
struct ESubScreensU5BU5D_t2685;
struct Transform_1_t2696;
// Declaration System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_CopyTo<ESubScreens,ESubScreens>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_CopyTo<ESubScreens,ESubScreens>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisESubScreens_t189_TisESubScreens_t189_m30296_gshared (Dictionary_2_t2688 * __this, ESubScreensU5BU5D_t2685* p0, int32_t p1, Transform_1_t2696 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisESubScreens_t189_TisESubScreens_t189_m30296(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2688 *, ESubScreensU5BU5D_t2685*, int32_t, Transform_1_t2696 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisESubScreens_t189_TisESubScreens_t189_m30296_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_44.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1899;
extern "C" void KeyCollection__ctor_m17435_gshared (KeyCollection_t2693 * __this, Dictionary_2_t2688 * ___dictionary, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral1899 = il2cpp_codegen_string_literal_from_index(1899);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2688 * L_0 = ___dictionary;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral1899, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Dictionary_2_t2688 * L_2 = ___dictionary;
		__this->___dictionary_0 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17436_gshared (KeyCollection_t2693 * __this, int32_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17437_gshared (KeyCollection_t2693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17438_gshared (KeyCollection_t2693 * __this, int32_t ___item, const MethodInfo* method)
{
	{
		Dictionary_2_t2688 * L_0 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		int32_t L_1 = ___item;
		NullCheck((Dictionary_2_t2688 *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::ContainsKey(TKey) */, (Dictionary_2_t2688 *)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17439_gshared (KeyCollection_t2693 * __this, int32_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17440_gshared (KeyCollection_t2693 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2693 *)__this);
		Enumerator_t2694  L_0 = (( Enumerator_t2694  (*) (KeyCollection_t2693 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyCollection_t2693 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t2694  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_Array.h"
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m17441_gshared (KeyCollection_t2693 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	ESubScreensU5BU5D_t2685* V_0 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (ESubScreensU5BU5D_t2685*)((ESubScreensU5BU5D_t2685*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		ESubScreensU5BU5D_t2685* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ESubScreensU5BU5D_t2685* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((KeyCollection_t2693 *)__this);
		(( void (*) (KeyCollection_t2693 *, ESubScreensU5BU5D_t2685*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyCollection_t2693 *)__this, (ESubScreensU5BU5D_t2685*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2688 * L_4 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		Array_t * L_5 = ___array;
		int32_t L_6 = ___index;
		NullCheck((Dictionary_2_t2688 *)L_4);
		(( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t2688 *)L_4, (Array_t *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t2688 * L_7 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		Array_t * L_8 = ___array;
		int32_t L_9 = ___index;
		IntPtr_t L_10 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t2696 * L_11 = (Transform_1_t2696 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t2696 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_11, (Object_t *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t2688 *)L_7);
		(( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, Transform_1_t2696 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t2688 *)L_7, (Array_t *)L_8, (int32_t)L_9, (Transform_1_t2696 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17442_gshared (KeyCollection_t2693 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2693 *)__this);
		Enumerator_t2694  L_0 = (( Enumerator_t2694  (*) (KeyCollection_t2693 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyCollection_t2693 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t2694  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17443_gshared (KeyCollection_t2693 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17444_gshared (KeyCollection_t2693 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern TypeInfo* ICollection_t1751_il2cpp_TypeInfo_var;
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m17445_gshared (KeyCollection_t2693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1751_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1034);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2688 * L_0 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1751_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::CopyTo(TKey[],System.Int32)
#include "Assembly-CSharp_ArrayTypes.h"
extern "C" void KeyCollection_CopyTo_m17446_gshared (KeyCollection_t2693 * __this, ESubScreensU5BU5D_t2685* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Dictionary_2_t2688 * L_0 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		ESubScreensU5BU5D_t2685* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Dictionary_2_t2688 *)L_0);
		(( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t2688 *)L_0, (Array_t *)(Array_t *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t2688 * L_3 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		ESubScreensU5BU5D_t2685* L_4 = ___array;
		int32_t L_5 = ___index;
		IntPtr_t L_6 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t2696 * L_7 = (Transform_1_t2696 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t2696 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_7, (Object_t *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t2688 *)L_3);
		(( void (*) (Dictionary_2_t2688 *, ESubScreensU5BU5D_t2685*, int32_t, Transform_1_t2696 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Dictionary_2_t2688 *)L_3, (ESubScreensU5BU5D_t2685*)L_4, (int32_t)L_5, (Transform_1_t2696 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::GetEnumerator()
extern "C" Enumerator_t2694  KeyCollection_GetEnumerator_m17447_gshared (KeyCollection_t2693 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2688 * L_0 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		Enumerator_t2694  L_1 = {0};
		(( void (*) (Enumerator_t2694 *, Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(&L_1, (Dictionary_2_t2688 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m17448_gshared (KeyCollection_t2693 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2688 * L_0 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		NullCheck((Dictionary_2_t2688 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Count() */, (Dictionary_2_t2688 *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_44.h"
extern "C" void Enumerator__ctor_m17449_gshared (Enumerator_t2694 * __this, Dictionary_2_t2688 * ___host, const MethodInfo* method)
{
	{
		Dictionary_2_t2688 * L_0 = ___host;
		NullCheck((Dictionary_2_t2688 *)L_0);
		Enumerator_t2695  L_1 = (( Enumerator_t2695  (*) (Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2688 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17450_gshared (Enumerator_t2694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695 * L_0 = (Enumerator_t2695 *)&(__this->___host_enumerator_0);
		int32_t L_1 = (( int32_t (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t2695 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m17451_gshared (Enumerator_t2694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695 * L_0 = (Enumerator_t2695 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t2695 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17452_gshared (Enumerator_t2694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695 * L_0 = (Enumerator_t2695 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t2695 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_1;
	}
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m17453_gshared (Enumerator_t2694 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695 * L_0 = (Enumerator_t2695 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t2690 * L_1 = (KeyValuePair_2_t2690 *)&(L_0->___current_3);
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t2690 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_44.h"
extern "C" void Enumerator__ctor_m17454_gshared (Enumerator_t2695 * __this, Dictionary_2_t2688 * ___dictionary, const MethodInfo* method)
{
	{
		Dictionary_2_t2688 * L_0 = ___dictionary;
		__this->___dictionary_0 = L_0;
		Dictionary_2_t2688 * L_1 = ___dictionary;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___generation_14);
		__this->___stamp_2 = L_2;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17455_gshared (Enumerator_t2695 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2690  L_0 = (KeyValuePair_2_t2690 )(__this->___current_3);
		KeyValuePair_2_t2690  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17456_gshared (Enumerator_t2695 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2690 * L_0 = (KeyValuePair_2_t2690 *)&(__this->___current_3);
		int32_t L_1 = (( int32_t (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2690 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_2);
		KeyValuePair_2_t2690 * L_4 = (KeyValuePair_2_t2690 *)&(__this->___current_3);
		Object_t * L_5 = (( Object_t * (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2690 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		DictionaryEntry_t451  L_6 = {0};
		DictionaryEntry__ctor_m9467(&L_6, (Object_t *)L_3, (Object_t *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17457_gshared (Enumerator_t2695 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Enumerator_t2695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_1);
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17458_gshared (Enumerator_t2695 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t2695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17459_gshared (Enumerator_t2695 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		(( void (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t2695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return 0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)(__this->___next_1);
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->___next_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2688 * L_4 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		NullCheck(L_4);
		LinkU5BU5D_t2500* L_5 = (LinkU5BU5D_t2500*)(L_4->___linkSlots_5);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_5, L_6, sizeof(Link_t1869 )))->___HashCode_0);
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2688 * L_8 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		NullCheck(L_8);
		ESubScreensU5BU5D_t2685* L_9 = (ESubScreensU5BU5D_t2685*)(L_8->___keySlots_6);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Dictionary_2_t2688 * L_12 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		NullCheck(L_12);
		ObjectU5BU5D_t356* L_13 = (ObjectU5BU5D_t356*)(L_12->___valueSlots_7);
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		KeyValuePair_2_t2690  L_16 = {0};
		(( void (*) (KeyValuePair_2_t2690 *, int32_t, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(&L_16, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_9, L_11, sizeof(int32_t))), (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_13, L_15, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->___current_3 = L_16;
		return 1;
	}

IL_007b:
	{
		int32_t L_17 = (int32_t)(__this->___next_1);
		Dictionary_2_t2688 * L_18 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		NullCheck(L_18);
		int32_t L_19 = (int32_t)(L_18->___touchedSlots_8);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0019;
		}
	}
	{
		__this->___next_1 = (-1);
		return 0;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2690  Enumerator_get_Current_m17460_gshared (Enumerator_t2695 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2690  L_0 = (KeyValuePair_2_t2690 )(__this->___current_3);
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m17461_gshared (Enumerator_t2695 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2690 * L_0 = (KeyValuePair_2_t2690 *)&(__this->___current_3);
		int32_t L_1 = (( int32_t (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2690 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m17462_gshared (Enumerator_t2695 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2690 * L_0 = (KeyValuePair_2_t2690 *)&(__this->___current_3);
		Object_t * L_1 = (( Object_t * (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2690 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::VerifyState()
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* ObjectDisposedException_t1420_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3395;
extern "C" void Enumerator_VerifyState_m17463_gshared (Enumerator_t2695 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(907);
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3395 = il2cpp_codegen_string_literal_from_index(3395);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2688 * L_0 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1420 * L_1 = (ObjectDisposedException_t1420 *)il2cpp_codegen_object_new (ObjectDisposedException_t1420_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m7575(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		Dictionary_2_t2688 * L_2 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)(L_2->___generation_14);
		int32_t L_4 = (int32_t)(__this->___stamp_2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t425 * L_5 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_5, (String_t*)_stringLiteral3395, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0033:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::VerifyCurrent()
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3396;
extern "C" void Enumerator_VerifyCurrent_m17464_gshared (Enumerator_t2695 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3396 = il2cpp_codegen_string_literal_from_index(3396);
		s_Il2CppMethodIntialized = true;
	}
	{
		(( void (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t2695 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, (String_t*)_stringLiteral3396, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_001d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m17465_gshared (Enumerator_t2695 * __this, const MethodInfo* method)
{
	{
		__this->___dictionary_0 = (Dictionary_2_t2688 *)NULL;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,ESubScreens>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Transform_1__ctor_m17466_gshared (Transform_1_t2696 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,ESubScreens>::Invoke(TKey,TValue)
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
extern "C" int32_t Transform_1_Invoke_m17467_gshared (Transform_1_t2696 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m17467((Transform_1_t2696 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,ESubScreens>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* ESubScreens_t189_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m17468_gshared (Transform_1_t2696 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ESubScreens_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ESubScreens_t189_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,ESubScreens>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Transform_1_EndInvoke_m17469_gshared (Transform_1_t2696 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_44.h"
// System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_20.h"
// System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_20MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_44MethodDeclarations.h"
struct Array_t;
struct Transform_1_t2699;
// Declaration System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30297_gshared (Dictionary_2_t2688 * __this, Array_t * p0, int32_t p1, Transform_1_t2699 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30297(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, Transform_1_t2699 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m30297_gshared)(__this, p0, p1, p2, method)
struct ObjectU5BU5D_t356;
struct Transform_1_t2699;
// Declaration System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30298_gshared (Dictionary_2_t2688 * __this, ObjectU5BU5D_t356* p0, int32_t p1, Transform_1_t2699 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30298(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t2688 *, ObjectU5BU5D_t356*, int32_t, Transform_1_t2699 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m30298_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_44.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1899;
extern "C" void ValueCollection__ctor_m17470_gshared (ValueCollection_t2697 * __this, Dictionary_2_t2688 * ___dictionary, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral1899 = il2cpp_codegen_string_literal_from_index(1899);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2688 * L_0 = ___dictionary;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral1899, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Dictionary_2_t2688 * L_2 = ___dictionary;
		__this->___dictionary_0 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
// System.Object
#include "mscorlib_System_Object.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17471_gshared (ValueCollection_t2697 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17472_gshared (ValueCollection_t2697 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17473_gshared (ValueCollection_t2697 * __this, Object_t * ___item, const MethodInfo* method)
{
	{
		Dictionary_2_t2688 * L_0 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		Object_t * L_1 = ___item;
		NullCheck((Dictionary_2_t2688 *)L_0);
		bool L_2 = (( bool (*) (Dictionary_2_t2688 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2688 *)L_0, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3394;
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17474_gshared (ValueCollection_t2697 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral3394 = il2cpp_codegen_string_literal_from_index(3394);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m8461(L_0, (String_t*)_stringLiteral3394, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17475_gshared (ValueCollection_t2697 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t2697 *)__this);
		Enumerator_t2698  L_0 = (( Enumerator_t2698  (*) (ValueCollection_t2697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ValueCollection_t2697 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t2698  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_Array.h"
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m17476_gshared (ValueCollection_t2697 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	ObjectU5BU5D_t356* V_0 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (ObjectU5BU5D_t356*)((ObjectU5BU5D_t356*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		ObjectU5BU5D_t356* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t356* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((ValueCollection_t2697 *)__this);
		(( void (*) (ValueCollection_t2697 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((ValueCollection_t2697 *)__this, (ObjectU5BU5D_t356*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2688 * L_4 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		Array_t * L_5 = ___array;
		int32_t L_6 = ___index;
		NullCheck((Dictionary_2_t2688 *)L_4);
		(( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t2688 *)L_4, (Array_t *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t2688 * L_7 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		Array_t * L_8 = ___array;
		int32_t L_9 = ___index;
		IntPtr_t L_10 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t2699 * L_11 = (Transform_1_t2699 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t2699 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_11, (Object_t *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t2688 *)L_7);
		(( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, Transform_1_t2699 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t2688 *)L_7, (Array_t *)L_8, (int32_t)L_9, (Transform_1_t2699 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17477_gshared (ValueCollection_t2697 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t2697 *)__this);
		Enumerator_t2698  L_0 = (( Enumerator_t2698  (*) (ValueCollection_t2697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ValueCollection_t2697 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t2698  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17478_gshared (ValueCollection_t2697 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17479_gshared (ValueCollection_t2697 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern TypeInfo* ICollection_t1751_il2cpp_TypeInfo_var;
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m17480_gshared (ValueCollection_t2697 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1751_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1034);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2688 * L_0 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1751_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::CopyTo(TValue[],System.Int32)
#include "mscorlib_ArrayTypes.h"
extern "C" void ValueCollection_CopyTo_m17481_gshared (ValueCollection_t2697 * __this, ObjectU5BU5D_t356* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Dictionary_2_t2688 * L_0 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		ObjectU5BU5D_t356* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Dictionary_2_t2688 *)L_0);
		(( void (*) (Dictionary_2_t2688 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t2688 *)L_0, (Array_t *)(Array_t *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t2688 * L_3 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		ObjectU5BU5D_t356* L_4 = ___array;
		int32_t L_5 = ___index;
		IntPtr_t L_6 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t2699 * L_7 = (Transform_1_t2699 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t2699 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_7, (Object_t *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t2688 *)L_3);
		(( void (*) (Dictionary_2_t2688 *, ObjectU5BU5D_t356*, int32_t, Transform_1_t2699 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Dictionary_2_t2688 *)L_3, (ObjectU5BU5D_t356*)L_4, (int32_t)L_5, (Transform_1_t2699 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::GetEnumerator()
extern "C" Enumerator_t2698  ValueCollection_GetEnumerator_m17482_gshared (ValueCollection_t2697 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2688 * L_0 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		Enumerator_t2698  L_1 = {0};
		(( void (*) (Enumerator_t2698 *, Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(&L_1, (Dictionary_2_t2688 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m17483_gshared (ValueCollection_t2697 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2688 * L_0 = (Dictionary_2_t2688 *)(__this->___dictionary_0);
		NullCheck((Dictionary_2_t2688 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>::get_Count() */, (Dictionary_2_t2688 *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ESubScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_44.h"
extern "C" void Enumerator__ctor_m17484_gshared (Enumerator_t2698 * __this, Dictionary_2_t2688 * ___host, const MethodInfo* method)
{
	{
		Dictionary_2_t2688 * L_0 = ___host;
		NullCheck((Dictionary_2_t2688 *)L_0);
		Enumerator_t2695  L_1 = (( Enumerator_t2695  (*) (Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2688 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ESubScreens,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17485_gshared (Enumerator_t2698 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695 * L_0 = (Enumerator_t2695 *)&(__this->___host_enumerator_0);
		Object_t * L_1 = (( Object_t * (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t2695 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ESubScreens,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m17486_gshared (Enumerator_t2698 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695 * L_0 = (Enumerator_t2695 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t2695 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ESubScreens,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17487_gshared (Enumerator_t2698 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695 * L_0 = (Enumerator_t2695 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t2695 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ESubScreens,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m17488_gshared (Enumerator_t2698 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695 * L_0 = (Enumerator_t2695 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t2690 * L_1 = (KeyValuePair_2_t2690 *)&(L_0->___current_3);
		Object_t * L_2 = (( Object_t * (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t2690 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Transform_1__ctor_m17489_gshared (Transform_1_t2699 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Object>::Invoke(TKey,TValue)
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
extern "C" Object_t * Transform_1_Invoke_m17490_gshared (Transform_1_t2699 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m17490((Transform_1_t2699 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* ESubScreens_t189_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m17491_gshared (Transform_1_t2699 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ESubScreens_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ESubScreens_t189_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Transform_1_EndInvoke_m17492_gshared (Transform_1_t2699 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Transform_1__ctor_m17493_gshared (Transform_1_t2689 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
extern "C" DictionaryEntry_t451  Transform_1_Invoke_m17494_gshared (Transform_1_t2689 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m17494((Transform_1_t2689 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t451  (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef DictionaryEntry_t451  (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* ESubScreens_t189_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m17495_gshared (Transform_1_t2689 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ESubScreens_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ESubScreens_t189_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C" DictionaryEntry_t451  Transform_1_EndInvoke_m17496_gshared (Transform_1_t2689 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(DictionaryEntry_t451 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void Transform_1__ctor_m17497_gshared (Transform_1_t2700 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>::Invoke(TKey,TValue)
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
extern "C" KeyValuePair_2_t2690  Transform_1_Invoke_m17498_gshared (Transform_1_t2700 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m17498((Transform_1_t2700 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t2690  (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef KeyValuePair_2_t2690  (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* ESubScreens_t189_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m17499_gshared (Transform_1_t2700 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ESubScreens_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ESubScreens_t189_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t2690  Transform_1_EndInvoke_m17500_gshared (Transform_1_t2700 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(KeyValuePair_2_t2690 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_44.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void ShimEnumerator__ctor_m17501_gshared (ShimEnumerator_t2701 * __this, Dictionary_2_t2688 * ___host, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2688 * L_0 = ___host;
		NullCheck((Dictionary_2_t2688 *)L_0);
		Enumerator_t2695  L_1 = (( Enumerator_t2695  (*) (Dictionary_2_t2688 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t2688 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17502_gshared (ShimEnumerator_t2701 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2695 * L_0 = (Enumerator_t2695 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t2695 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::get_Entry()
extern TypeInfo* IDictionaryEnumerator_t1606_il2cpp_TypeInfo_var;
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m17503_gshared (ShimEnumerator_t2701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryEnumerator_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1035);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t2695  L_0 = (Enumerator_t2695 )(__this->___host_enumerator_0);
		Enumerator_t2695  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Object_t *)L_2);
		DictionaryEntry_t451  L_3 = (DictionaryEntry_t451 )InterfaceFuncInvoker0< DictionaryEntry_t451  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t1606_il2cpp_TypeInfo_var, (Object_t *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17504_gshared (ShimEnumerator_t2701 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2690  V_0 = {0};
	{
		Enumerator_t2695 * L_0 = (Enumerator_t2695 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t2690  L_1 = (( KeyValuePair_2_t2690  (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t2695 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2690 )L_1;
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2690 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17505_gshared (ShimEnumerator_t2701 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2690  V_0 = {0};
	{
		Enumerator_t2695 * L_0 = (Enumerator_t2695 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t2690  L_1 = (( KeyValuePair_2_t2690  (*) (Enumerator_t2695 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t2695 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2690 )L_1;
		Object_t * L_2 = (( Object_t * (*) (KeyValuePair_2_t2690 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t2690 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::get_Current()
extern TypeInfo* DictionaryEntry_t451_il2cpp_TypeInfo_var;
extern "C" Object_t * ShimEnumerator_get_Current_m17506_gshared (ShimEnumerator_t2701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DictionaryEntry_t451_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(271);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2701 *)__this);
		DictionaryEntry_t451  L_0 = (DictionaryEntry_t451 )VirtFuncInvoker0< DictionaryEntry_t451  >::Invoke(6 /* System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::get_Entry() */, (ShimEnumerator_t2701 *)__this);
		DictionaryEntry_t451  L_1 = L_0;
		Object_t * L_2 = Box(DictionaryEntry_t451_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ESubScreens>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_2.h"
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ESubScreens>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_2MethodDeclarations.h"
// System.Void System.Collections.Generic.EqualityComparer`1<ESubScreens>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void EqualityComparer_1__ctor_m17507_gshared (EqualityComparer_1_t2702 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<ESubScreens>::.cctor()
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
extern const Il2CppType* GenericEqualityComparer_1_t3938_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1005_il2cpp_TypeInfo_var;
extern "C" void EqualityComparer_1__cctor_m17508_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericEqualityComparer_1_t3938_0_0_0_var = il2cpp_codegen_type_from_index(4049);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		TypeU5BU5D_t1005_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(623);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(GenericEqualityComparer_1_t3938_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1005* L_4 = (TypeU5BU5D_t1005*)((TypeU5BU5D_t1005*)SZArrayNew(TypeU5BU5D_t1005_il2cpp_TypeInfo_var, 1));
		Type_t * L_5 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 0, sizeof(Type_t *))) = (Type_t *)L_5;
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1005* >::Invoke(77 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1005*)L_4);
		Object_t * L_7 = Activator_CreateInstance_m5150(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t2702_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = ((EqualityComparer_1_t2702 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2703 * L_8 = (DefaultComparer_t2703 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (DefaultComparer_t2703 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t2702_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = L_8;
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<ESubScreens>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17509_gshared (EqualityComparer_1_t2702 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___obj;
		NullCheck((EqualityComparer_1_t2702 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<ESubScreens>::GetHashCode(T) */, (EqualityComparer_1_t2702 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<ESubScreens>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17510_gshared (EqualityComparer_1_t2702 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___x;
		Object_t * L_1 = ___y;
		NullCheck((EqualityComparer_1_t2702 *)__this);
		bool L_2 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<ESubScreens>::Equals(T,T) */, (EqualityComparer_1_t2702 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<ESubScreens>::get_Default()
extern "C" EqualityComparer_1_t2702 * EqualityComparer_1_get_Default_m17511_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t2702 * L_0 = ((EqualityComparer_1_t2702_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0;
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ESubScreens>::.ctor()
extern "C" void DefaultComparer__ctor_m17512_gshared (DefaultComparer_t2703 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t2702 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		(( void (*) (EqualityComparer_1_t2702 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((EqualityComparer_1_t2702 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ESubScreens>::GetHashCode(T)
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
extern "C" int32_t DefaultComparer_GetHashCode_m17513_gshared (DefaultComparer_t2703 * __this, int32_t ___obj, const MethodInfo* method)
{
	{
		int32_t L_0 = ___obj;
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___obj)));
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___obj)));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ESubScreens>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m17514_gshared (DefaultComparer_t2703 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x;
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return ((((Object_t*)(Object_t *)L_3) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y;
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___x)));
		bool L_7 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___x)), (Object_t *)L_6);
		return L_7;
	}
}
// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9.h"
// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_22.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ERaffleResult,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_26.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ERaffleResult,System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_27.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_3.h"
// System.Collections.Generic.EqualityComparer`1<System.Int32>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_0.h"
// System.Collections.Generic.EqualityComparer`1<ERaffleResult>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_3.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_46.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_genMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ERaffleResult,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_26MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ERaffleResult,System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_27MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_3MethodDeclarations.h"
// System.Collections.Generic.EqualityComparer`1<System.Int32>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_0MethodDeclarations.h"
// System.Collections.Generic.EqualityComparer`1<ERaffleResult>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_3MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_22MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_46MethodDeclarations.h"
struct DictionaryEntryU5BU5D_t3822;
struct Transform_1_t2709;
// Declaration System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t451_TisDictionaryEntry_t451_m30331_gshared (Dictionary_2_t208 * __this, DictionaryEntryU5BU5D_t3822* p0, int32_t p1, Transform_1_t2709 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisDictionaryEntry_t451_TisDictionaryEntry_t451_m30331(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t208 *, DictionaryEntryU5BU5D_t3822*, int32_t, Transform_1_t2709 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisDictionaryEntry_t451_TisDictionaryEntry_t451_m30331_gshared)(__this, p0, p1, p2, method)
struct Array_t;
struct Transform_1_t2719;
// Declaration System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t352_m30332_gshared (Dictionary_2_t208 * __this, Array_t * p0, int32_t p1, Transform_1_t2719 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t352_m30332(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t208 *, Array_t *, int32_t, Transform_1_t2719 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t352_m30332_gshared)(__this, p0, p1, p2, method)
struct KeyValuePair_2U5BU5D_t3660;
struct Transform_1_t2719;
// Declaration System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>,System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>,System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t352_TisKeyValuePair_2_t352_m30334_gshared (Dictionary_2_t208 * __this, KeyValuePair_2U5BU5D_t3660* p0, int32_t p1, Transform_1_t2719 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t352_TisKeyValuePair_2_t352_m30334(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t208 *, KeyValuePair_2U5BU5D_t3660*, int32_t, Transform_1_t2719 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t352_TisKeyValuePair_2_t352_m30334_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void Dictionary_2__ctor_m1658_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t208 *)__this, (int32_t)((int32_t)10), (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17565_gshared (Dictionary_2_t208 * __this, Object_t* ___comparer, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___comparer;
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t208 *)__this, (int32_t)((int32_t)10), (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::.ctor(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void Dictionary_2__ctor_m17566_gshared (Dictionary_2_t208 * __this, int32_t ___capacity, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity;
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t208 *)__this, (int32_t)L_0, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
extern "C" void Dictionary_2__ctor_m17567_gshared (Dictionary_2_t208 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1306((Object_t *)__this, /*hidden argument*/NULL);
		SerializationInfo_t1012 * L_0 = ___info;
		__this->___serialization_info_13 = L_0;
		return;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m17568_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t208 *)__this);
		KeyCollection_t2713 * L_0 = (( KeyCollection_t2713 * (*) (Dictionary_2_t208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Dictionary_2_t208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.get_Item(System.Object)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17569_gshared (Dictionary_2_t208 * __this, Object_t * ___key, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		if (!((Object_t *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_1 = ___key;
		NullCheck((Dictionary_2_t208 *)__this);
		bool L_2 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ContainsKey(TKey) */, (Dictionary_2_t208 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t208 *)__this);
		int32_t L_4 = (( int32_t (*) (Dictionary_2_t208 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Dictionary_2_t208 *)__this, (Object_t *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Dictionary_2_t208 *)__this);
		int32_t L_5 = (int32_t)VirtFuncInvoker1< int32_t, int32_t >::Invoke(19 /* TValue System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::get_Item(TKey) */, (Dictionary_2_t208 *)__this, (int32_t)L_4);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		return L_7;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17570_gshared (Dictionary_2_t208 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((Dictionary_2_t208 *)__this);
		int32_t L_1 = (( int32_t (*) (Dictionary_2_t208 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Dictionary_2_t208 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Object_t * L_2 = ___value;
		NullCheck((Dictionary_2_t208 *)__this);
		int32_t L_3 = (( int32_t (*) (Dictionary_2_t208 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Dictionary_2_t208 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Dictionary_2_t208 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::set_Item(TKey,TValue) */, (Dictionary_2_t208 *)__this, (int32_t)L_1, (int32_t)L_3);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17571_gshared (Dictionary_2_t208 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((Dictionary_2_t208 *)__this);
		int32_t L_1 = (( int32_t (*) (Dictionary_2_t208 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Dictionary_2_t208 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Object_t * L_2 = ___value;
		NullCheck((Dictionary_2_t208 *)__this);
		int32_t L_3 = (( int32_t (*) (Dictionary_2_t208 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Dictionary_2_t208 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((Dictionary_2_t208 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Add(TKey,TValue) */, (Dictionary_2_t208 *)__this, (int32_t)L_1, (int32_t)L_3);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.Contains(System.Object)
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17572_gshared (Dictionary_2_t208 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (!((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t208 *)__this);
		bool L_4 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ContainsKey(TKey) */, (Dictionary_2_t208 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))));
		return L_4;
	}

IL_0029:
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.Remove(System.Object)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17573_gshared (Dictionary_2_t208 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (!((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t208 *)__this);
		VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Remove(TKey) */, (Dictionary_2_t208 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17574_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17575_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17576_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17577_gshared (Dictionary_2_t208 * __this, KeyValuePair_2_t352  ___keyValuePair, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t352 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		int32_t L_1 = (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((KeyValuePair_2_t352 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((Dictionary_2_t208 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Add(TKey,TValue) */, (Dictionary_2_t208 *)__this, (int32_t)L_0, (int32_t)L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17578_gshared (Dictionary_2_t208 * __this, KeyValuePair_2_t352  ___keyValuePair, const MethodInfo* method)
{
	{
		KeyValuePair_2_t352  L_0 = ___keyValuePair;
		NullCheck((Dictionary_2_t208 *)__this);
		bool L_1 = (( bool (*) (Dictionary_2_t208 *, KeyValuePair_2_t352 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Dictionary_2_t208 *)__this, (KeyValuePair_2_t352 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#include "mscorlib_ArrayTypes.h"
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17579_gshared (Dictionary_2_t208 * __this, KeyValuePair_2U5BU5D_t3660* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t3660* L_0 = ___array;
		int32_t L_1 = ___index;
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, KeyValuePair_2U5BU5D_t3660*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Dictionary_2_t208 *)__this, (KeyValuePair_2U5BU5D_t3660*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17580_gshared (Dictionary_2_t208 * __this, KeyValuePair_2_t352  ___keyValuePair, const MethodInfo* method)
{
	{
		KeyValuePair_2_t352  L_0 = ___keyValuePair;
		NullCheck((Dictionary_2_t208 *)__this);
		bool L_1 = (( bool (*) (Dictionary_2_t208 *, KeyValuePair_2_t352 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Dictionary_2_t208 *)__this, (KeyValuePair_2_t352 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t352 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((Dictionary_2_t208 *)__this);
		bool L_3 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Remove(TKey) */, (Dictionary_2_t208 *)__this, (int32_t)L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_Array.h"
extern TypeInfo* DictionaryEntryU5BU5D_t3822_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17581_gshared (Dictionary_2_t208 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DictionaryEntryU5BU5D_t3822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4051);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t3660* V_0 = {0};
	DictionaryEntryU5BU5D_t3822* V_1 = {0};
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t3822* G_B5_1 = {0};
	Dictionary_2_t208 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t3822* G_B4_1 = {0};
	Dictionary_2_t208 * G_B4_2 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (KeyValuePair_2U5BU5D_t3660*)((KeyValuePair_2U5BU5D_t3660*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)));
		KeyValuePair_2U5BU5D_t3660* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t3660* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, KeyValuePair_2U5BU5D_t3660*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Dictionary_2_t208 *)__this, (KeyValuePair_2U5BU5D_t3660*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return;
	}

IL_0016:
	{
		Array_t * L_4 = ___array;
		int32_t L_5 = ___index;
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Dictionary_2_t208 *)__this, (Array_t *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		Array_t * L_6 = ___array;
		V_1 = (DictionaryEntryU5BU5D_t3822*)((DictionaryEntryU5BU5D_t3822*)IsInst(L_6, DictionaryEntryU5BU5D_t3822_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t3822* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t3822* L_8 = V_1;
		int32_t L_9 = ___index;
		Transform_1_t2709 * L_10 = ((Dictionary_2_t208_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->static_fields)->___U3CU3Ef__amU24cacheB_15;
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t208 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t208 *)(__this));
			goto IL_0046;
		}
	}
	{
		IntPtr_t L_11 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18) };
		Transform_1_t2709 * L_12 = (Transform_1_t2709 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		(( void (*) (Transform_1_t2709 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)(L_12, (Object_t *)NULL, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		((Dictionary_2_t208_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->static_fields)->___U3CU3Ef__amU24cacheB_15 = L_12;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t208 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t2709 * L_13 = ((Dictionary_2_t208_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->static_fields)->___U3CU3Ef__amU24cacheB_15;
		NullCheck((Dictionary_2_t208 *)G_B5_2);
		(( void (*) (Dictionary_2_t208 *, DictionaryEntryU5BU5D_t3822*, int32_t, Transform_1_t2709 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((Dictionary_2_t208 *)G_B5_2, (DictionaryEntryU5BU5D_t3822*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t2709 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		return;
	}

IL_0051:
	{
		Array_t * L_14 = ___array;
		int32_t L_15 = ___index;
		IntPtr_t L_16 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22) };
		Transform_1_t2719 * L_17 = (Transform_1_t2719 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		(( void (*) (Transform_1_t2719 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(L_17, (Object_t *)NULL, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, Array_t *, int32_t, Transform_1_t2719 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)((Dictionary_2_t208 *)__this, (Array_t *)L_14, (int32_t)L_15, (Transform_1_t2719 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17582_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		Enumerator_t436  L_0 = {0};
		(( void (*) (Enumerator_t436 *, Dictionary_2_t208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(&L_0, (Dictionary_2_t208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		Enumerator_t436  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17583_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		Enumerator_t436  L_0 = {0};
		(( void (*) (Enumerator_t436 *, Dictionary_2_t208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(&L_0, (Dictionary_2_t208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		Enumerator_t436  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17584_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		ShimEnumerator_t2720 * L_0 = (ShimEnumerator_t2720 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		(( void (*) (ShimEnumerator_t2720 *, Dictionary_2_t208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)(L_0, (Dictionary_2_t208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17585_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___count_10);
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::get_Item(TKey)
// System.Collections.Generic.KeyNotFoundException
#include "mscorlib_System_Collections_Generic_KeyNotFoundExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* KeyNotFoundException_t1870_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" int32_t Dictionary_2_get_Item_m17586_gshared (Dictionary_2_t208 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		KeyNotFoundException_t1870_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4052);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Int32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t401* L_5 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t2500* L_9 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		Int32U5BU5D_t401* L_14 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Int32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_0089;
		}
	}
	{
		ERaffleResultU5BU5D_t2708* L_19 = (ERaffleResultU5BU5D_t2708*)(__this->___valueSlots_7);
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		return (*(int32_t*)(int32_t*)SZArrayLdElema(L_19, L_21, sizeof(int32_t)));
	}

IL_0089:
	{
		LinkU5BU5D_t2500* L_22 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1869 )))->___Next_1);
		V_1 = (int32_t)L_24;
	}

IL_009b:
	{
		int32_t L_25 = V_1;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t1870 * L_26 = (KeyNotFoundException_t1870 *)il2cpp_codegen_object_new (KeyNotFoundException_t1870_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m10895(L_26, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_26);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::set_Item(TKey,TValue)
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" void Dictionary_2_set_Item_m17587_gshared (Dictionary_2_t208 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Int32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))));
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t2500* L_11 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_11, L_12, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0087;
		}
	}
	{
		Object_t* L_15 = (Object_t*)(__this->___hcp_12);
		Int32U5BU5D_t401* L_16 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		int32_t L_19 = ___key;
		NullCheck((Object_t*)L_15);
		bool L_20 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Int32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_15, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_16, L_18, sizeof(int32_t))), (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_21 = V_2;
		V_3 = (int32_t)L_21;
		LinkU5BU5D_t2500* L_22 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_24;
		int32_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_27 = (int32_t)(__this->___count_10);
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_4 = (int32_t)L_28;
		__this->___count_10 = L_28;
		int32_t L_29 = V_4;
		int32_t L_30 = (int32_t)(__this->___threshold_11);
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)((Dictionary_2_t208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		int32_t L_31 = V_0;
		Int32U5BU5D_t401* L_32 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_32)->max_length)))));
	}

IL_00de:
	{
		int32_t L_33 = (int32_t)(__this->___emptySlot_9);
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_35 = (int32_t)(__this->___touchedSlots_8);
		int32_t L_36 = (int32_t)L_35;
		V_4 = (int32_t)L_36;
		__this->___touchedSlots_8 = ((int32_t)((int32_t)L_36+(int32_t)1));
		int32_t L_37 = V_4;
		V_2 = (int32_t)L_37;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t2500* L_38 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_38, L_39, sizeof(Link_t1869 )))->___Next_1);
		__this->___emptySlot_9 = L_40;
	}

IL_011c:
	{
		LinkU5BU5D_t2500* L_41 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		Int32U5BU5D_t401* L_43 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_44 = V_1;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		int32_t L_45 = L_44;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_41, L_42, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_43, L_45, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_46 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_47 = V_1;
		int32_t L_48 = V_2;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_46, L_47, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_48+(int32_t)1));
		LinkU5BU5D_t2500* L_49 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_50 = V_2;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		int32_t L_51 = V_0;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_49, L_50, sizeof(Link_t1869 )))->___HashCode_0 = L_51;
		Int32U5BU5D_t401* L_52 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		int32_t L_53 = V_2;
		int32_t L_54 = ___key;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_52, L_53, sizeof(int32_t))) = (int32_t)L_54;
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_55 = V_3;
		if ((((int32_t)L_55) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t2500* L_56 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_57 = V_3;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		LinkU5BU5D_t2500* L_58 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_59 = V_2;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		int32_t L_60 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_58, L_59, sizeof(Link_t1869 )))->___Next_1);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_56, L_57, sizeof(Link_t1869 )))->___Next_1 = L_60;
		LinkU5BU5D_t2500* L_61 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_62 = V_2;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		Int32U5BU5D_t401* L_63 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_64 = V_1;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, L_64);
		int32_t L_65 = L_64;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_61, L_62, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_63, L_65, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_66 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_67 = V_1;
		int32_t L_68 = V_2;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_66, L_67, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_01b5:
	{
		ERaffleResultU5BU5D_t2708* L_69 = (ERaffleResultU5BU5D_t2708*)(__this->___valueSlots_7);
		int32_t L_70 = V_2;
		int32_t L_71 = ___value;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, L_70);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_69, L_70, sizeof(int32_t))) = (int32_t)L_71;
		int32_t L_72 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_72+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1870;
extern "C" void Dictionary_2_Init_m17588_gshared (Dictionary_2_t208 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(906);
		_stringLiteral1870 = il2cpp_codegen_string_literal_from_index(1870);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	Dictionary_2_t208 * G_B4_0 = {0};
	Dictionary_2_t208 * G_B3_0 = {0};
	Object_t* G_B5_0 = {0};
	Dictionary_2_t208 * G_B5_1 = {0};
	{
		int32_t L_0 = ___capacity;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1419 * L_1 = (ArgumentOutOfRangeException_t1419 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8460(L_1, (String_t*)_stringLiteral1870, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		Object_t* L_2 = ___hcp;
		G_B3_0 = ((Dictionary_2_t208 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t208 *)(__this));
			goto IL_0021;
		}
	}
	{
		Object_t* L_3 = ___hcp;
		V_0 = (Object_t*)L_3;
		Object_t* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t208 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		EqualityComparer_1_t2557 * L_5 = (( EqualityComparer_1_t2557 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		G_B5_0 = ((Object_t*)(L_5));
		G_B5_1 = ((Dictionary_2_t208 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->___hcp_12 = G_B5_0;
		int32_t L_6 = ___capacity;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity;
		___capacity = (int32_t)((int32_t)((int32_t)(((int32_t)((float)((float)(((float)L_7))/(float)(0.9f)))))+(int32_t)1));
		int32_t L_8 = ___capacity;
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)((Dictionary_2_t208 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		__this->___generation_14 = 0;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::InitArrays(System.Int32)
extern TypeInfo* Int32U5BU5D_t401_il2cpp_TypeInfo_var;
extern TypeInfo* LinkU5BU5D_t2500_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_InitArrays_m17589_gshared (Dictionary_2_t208 * __this, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		LinkU5BU5D_t2500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4053);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size;
		__this->___table_4 = ((Int32U5BU5D_t401*)SZArrayNew(Int32U5BU5D_t401_il2cpp_TypeInfo_var, L_0));
		int32_t L_1 = ___size;
		__this->___linkSlots_5 = ((LinkU5BU5D_t2500*)SZArrayNew(LinkU5BU5D_t2500_il2cpp_TypeInfo_var, L_1));
		__this->___emptySlot_9 = (-1);
		int32_t L_2 = ___size;
		__this->___keySlots_6 = ((Int32U5BU5D_t401*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), L_2));
		int32_t L_3 = ___size;
		__this->___valueSlots_7 = ((ERaffleResultU5BU5D_t2708*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36), L_3));
		__this->___touchedSlots_8 = 0;
		Int32U5BU5D_t401* L_4 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_4);
		__this->___threshold_11 = (((int32_t)((float)((float)(((float)(((int32_t)(((Array_t *)L_4)->max_length)))))*(float)(0.9f)))));
		int32_t L_5 = (int32_t)(__this->___threshold_11);
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->___threshold_11 = 1;
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::CopyToCheck(System.Array,System.Int32)
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1265;
extern Il2CppCodeGenString* _stringLiteral1267;
extern Il2CppCodeGenString* _stringLiteral3389;
extern Il2CppCodeGenString* _stringLiteral3390;
extern "C" void Dictionary_2_CopyToCheck_m17590_gshared (Dictionary_2_t208 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(906);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral1265 = il2cpp_codegen_string_literal_from_index(1265);
		_stringLiteral1267 = il2cpp_codegen_string_literal_from_index(1267);
		_stringLiteral3389 = il2cpp_codegen_string_literal_from_index(3389);
		_stringLiteral3390 = il2cpp_codegen_string_literal_from_index(3390);
		s_Il2CppMethodIntialized = true;
	}
	{
		Array_t * L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral1265, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t1419 * L_3 = (ArgumentOutOfRangeException_t1419 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1419_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8460(L_3, (String_t*)_stringLiteral1267, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index;
		Array_t * L_5 = ___array;
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t764 * L_7 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_7, (String_t*)_stringLiteral3389, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_003a:
	{
		Array_t * L_8 = ___array;
		NullCheck((Array_t *)L_8);
		int32_t L_9 = Array_get_Length_m9469((Array_t *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index;
		NullCheck((Dictionary_2_t208 *)__this);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::get_Count() */, (Dictionary_2_t208 *)__this);
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t764 * L_12 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_12, (String_t*)_stringLiteral3390, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t352  Dictionary_2_make_pair_m17591_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		int32_t L_1 = ___value;
		KeyValuePair_2_t352  L_2 = {0};
		(( void (*) (KeyValuePair_2_t352 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)(&L_2, (int32_t)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m17592_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m17593_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17594_gshared (Dictionary_2_t208 * __this, KeyValuePair_2U5BU5D_t3660* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t3660* L_0 = ___array;
		int32_t L_1 = ___index;
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Dictionary_2_t208 *)__this, (Array_t *)(Array_t *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		KeyValuePair_2U5BU5D_t3660* L_2 = ___array;
		int32_t L_3 = ___index;
		IntPtr_t L_4 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22) };
		Transform_1_t2719 * L_5 = (Transform_1_t2719 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		(( void (*) (Transform_1_t2719 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(L_5, (Object_t *)NULL, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, KeyValuePair_2U5BU5D_t3660*, int32_t, Transform_1_t2719 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40)->method)((Dictionary_2_t208 *)__this, (KeyValuePair_2U5BU5D_t3660*)L_2, (int32_t)L_3, (Transform_1_t2719 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Resize()
// System.Collections.Hashtable
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
extern TypeInfo* Hashtable_t261_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t401_il2cpp_TypeInfo_var;
extern TypeInfo* LinkU5BU5D_t2500_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_Resize_m17595_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(249);
		Int32U5BU5D_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		LinkU5BU5D_t2500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4053);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t401* V_1 = {0};
	LinkU5BU5D_t2500* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Int32U5BU5D_t401* V_7 = {0};
	ERaffleResultU5BU5D_t2708* V_8 = {0};
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t401* L_0 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t261_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m11127(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t401*)((Int32U5BU5D_t401*)SZArrayNew(Int32U5BU5D_t401_il2cpp_TypeInfo_var, L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t2500*)((LinkU5BU5D_t2500*)SZArrayNew(LinkU5BU5D_t2500_il2cpp_TypeInfo_var, L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t401* L_4 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_4 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6, sizeof(int32_t)))-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t2500* L_7 = V_2;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Object_t* L_9 = (Object_t*)(__this->___hcp_12);
		Int32U5BU5D_t401* L_10 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		int32_t L_11 = V_4;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((Object_t*)L_9);
		int32_t L_13 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Int32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_9, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_10, L_12, sizeof(int32_t))));
		int32_t L_14 = (int32_t)((int32_t)((int32_t)L_13|(int32_t)((int32_t)-2147483648)));
		V_9 = (int32_t)L_14;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_7, L_8, sizeof(Link_t1869 )))->___HashCode_0 = L_14;
		int32_t L_15 = V_9;
		V_5 = (int32_t)L_15;
		int32_t L_16 = V_5;
		int32_t L_17 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)2147483647)))%(int32_t)L_17));
		LinkU5BU5D_t2500* L_18 = V_2;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		Int32U5BU5D_t401* L_20 = V_1;
		int32_t L_21 = V_6;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_18, L_19, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_20, L_22, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_23 = V_1;
		int32_t L_24 = V_6;
		int32_t L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_23, L_24, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		LinkU5BU5D_t2500* L_26 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_27 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_26, L_27, sizeof(Link_t1869 )))->___Next_1);
		V_4 = (int32_t)L_28;
	}

IL_00a5:
	{
		int32_t L_29 = V_4;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_30 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_31 = V_3;
		Int32U5BU5D_t401* L_32 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)(((Array_t *)L_32)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t401* L_33 = V_1;
		__this->___table_4 = L_33;
		LinkU5BU5D_t2500* L_34 = V_2;
		__this->___linkSlots_5 = L_34;
		int32_t L_35 = V_0;
		V_7 = (Int32U5BU5D_t401*)((Int32U5BU5D_t401*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), L_35));
		int32_t L_36 = V_0;
		V_8 = (ERaffleResultU5BU5D_t2708*)((ERaffleResultU5BU5D_t2708*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36), L_36));
		Int32U5BU5D_t401* L_37 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		Int32U5BU5D_t401* L_38 = V_7;
		int32_t L_39 = (int32_t)(__this->___touchedSlots_8);
		Array_Copy_m1754(NULL /*static, unused*/, (Array_t *)(Array_t *)L_37, (int32_t)0, (Array_t *)(Array_t *)L_38, (int32_t)0, (int32_t)L_39, /*hidden argument*/NULL);
		ERaffleResultU5BU5D_t2708* L_40 = (ERaffleResultU5BU5D_t2708*)(__this->___valueSlots_7);
		ERaffleResultU5BU5D_t2708* L_41 = V_8;
		int32_t L_42 = (int32_t)(__this->___touchedSlots_8);
		Array_Copy_m1754(NULL /*static, unused*/, (Array_t *)(Array_t *)L_40, (int32_t)0, (Array_t *)(Array_t *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		Int32U5BU5D_t401* L_43 = V_7;
		__this->___keySlots_6 = L_43;
		ERaffleResultU5BU5D_t2708* L_44 = V_8;
		__this->___valueSlots_7 = L_44;
		int32_t L_45 = V_0;
		__this->___threshold_11 = (((int32_t)((float)((float)(((float)L_45))*(float)(0.9f)))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Add(TKey,TValue)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern Il2CppCodeGenString* _stringLiteral3391;
extern "C" void Dictionary_2_Add_m17596_gshared (Dictionary_2_t208 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		_stringLiteral3391 = il2cpp_codegen_string_literal_from_index(3391);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Int32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))));
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t2500* L_10 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_10, L_11, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_14 = (Object_t*)(__this->___hcp_12);
		Int32U5BU5D_t401* L_15 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = ___key;
		NullCheck((Object_t*)L_14);
		bool L_19 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Int32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_14, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_15, L_17, sizeof(int32_t))), (int32_t)L_18);
		if (!L_19)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t764 * L_20 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3737(L_20, (String_t*)_stringLiteral3391, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_20);
	}

IL_0089:
	{
		LinkU5BU5D_t2500* L_21 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_22 = V_2;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_21, L_22, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_23;
	}

IL_009b:
	{
		int32_t L_24 = V_2;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_25 = (int32_t)(__this->___count_10);
		int32_t L_26 = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		V_3 = (int32_t)L_26;
		__this->___count_10 = L_26;
		int32_t L_27 = V_3;
		int32_t L_28 = (int32_t)(__this->___threshold_11);
		if ((((int32_t)L_27) <= ((int32_t)L_28)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)((Dictionary_2_t208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		int32_t L_29 = V_0;
		Int32U5BU5D_t401* L_30 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_30);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_29&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_30)->max_length)))));
	}

IL_00d5:
	{
		int32_t L_31 = (int32_t)(__this->___emptySlot_9);
		V_2 = (int32_t)L_31;
		int32_t L_32 = V_2;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_33 = (int32_t)(__this->___touchedSlots_8);
		int32_t L_34 = (int32_t)L_33;
		V_3 = (int32_t)L_34;
		__this->___touchedSlots_8 = ((int32_t)((int32_t)L_34+(int32_t)1));
		int32_t L_35 = V_3;
		V_2 = (int32_t)L_35;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t2500* L_36 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_37 = V_2;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_36, L_37, sizeof(Link_t1869 )))->___Next_1);
		__this->___emptySlot_9 = L_38;
	}

IL_0111:
	{
		LinkU5BU5D_t2500* L_39 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_40 = V_2;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = V_0;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_39, L_40, sizeof(Link_t1869 )))->___HashCode_0 = L_41;
		LinkU5BU5D_t2500* L_42 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_43 = V_2;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		Int32U5BU5D_t401* L_44 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_45 = V_1;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		int32_t L_46 = L_45;
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_42, L_43, sizeof(Link_t1869 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_44, L_46, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t401* L_47 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_48 = V_1;
		int32_t L_49 = V_2;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_47, L_48, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_49+(int32_t)1));
		Int32U5BU5D_t401* L_50 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		int32_t L_51 = V_2;
		int32_t L_52 = ___key;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_50, L_51, sizeof(int32_t))) = (int32_t)L_52;
		ERaffleResultU5BU5D_t2708* L_53 = (ERaffleResultU5BU5D_t2708*)(__this->___valueSlots_7);
		int32_t L_54 = V_2;
		int32_t L_55 = ___value;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_53, L_54, sizeof(int32_t))) = (int32_t)L_55;
		int32_t L_56 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_56+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Clear()
extern "C" void Dictionary_2_Clear_m17597_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		__this->___count_10 = 0;
		Int32U5BU5D_t401* L_0 = (Int32U5BU5D_t401*)(__this->___table_4);
		Int32U5BU5D_t401* L_1 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_1);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_1)->max_length))), /*hidden argument*/NULL);
		Int32U5BU5D_t401* L_2 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		Int32U5BU5D_t401* L_3 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		NullCheck(L_3);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_3)->max_length))), /*hidden argument*/NULL);
		ERaffleResultU5BU5D_t2708* L_4 = (ERaffleResultU5BU5D_t2708*)(__this->___valueSlots_7);
		ERaffleResultU5BU5D_t2708* L_5 = (ERaffleResultU5BU5D_t2708*)(__this->___valueSlots_7);
		NullCheck(L_5);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_4, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_5)->max_length))), /*hidden argument*/NULL);
		LinkU5BU5D_t2500* L_6 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		LinkU5BU5D_t2500* L_7 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		NullCheck(L_7);
		Array_Clear_m7571(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_7)->max_length))), /*hidden argument*/NULL);
		__this->___emptySlot_9 = (-1);
		__this->___touchedSlots_8 = 0;
		int32_t L_8 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_8+(int32_t)1));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ContainsKey(TKey)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_ContainsKey_m17598_gshared (Dictionary_2_t208 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Int32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t401* L_5 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t2500* L_9 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_007e;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		Int32U5BU5D_t401* L_14 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Int32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_007e;
		}
	}
	{
		return 1;
	}

IL_007e:
	{
		LinkU5BU5D_t2500* L_19 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_19, L_20, sizeof(Link_t1869 )))->___Next_1);
		V_1 = (int32_t)L_21;
	}

IL_0090:
	{
		int32_t L_22 = V_1;
		if ((!(((uint32_t)L_22) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17599_gshared (Dictionary_2_t208 * __this, int32_t ___value, const MethodInfo* method)
{
	Object_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42));
		EqualityComparer_1_t2721 * L_0 = (( EqualityComparer_1_t2721 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
		V_0 = (Object_t*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t401* L_1 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_3, sizeof(int32_t)))-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		Object_t* L_4 = V_0;
		ERaffleResultU5BU5D_t2708* L_5 = (ERaffleResultU5BU5D_t2708*)(__this->___valueSlots_7);
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		int32_t L_8 = ___value;
		NullCheck((Object_t*)L_4);
		bool L_9 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ERaffleResult>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43), (Object_t*)L_4, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_7, sizeof(int32_t))), (int32_t)L_8);
		if (!L_9)
		{
			goto IL_0037;
		}
	}
	{
		return 1;
	}

IL_0037:
	{
		LinkU5BU5D_t2500* L_10 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_10, L_11, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_12;
	}

IL_0049:
	{
		int32_t L_13 = V_2;
		if ((!(((uint32_t)L_13) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_14 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_15 = V_1;
		Int32U5BU5D_t401* L_16 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)(((Array_t *)L_16)->max_length))))))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1277;
extern Il2CppCodeGenString* _stringLiteral1279;
extern Il2CppCodeGenString* _stringLiteral1281;
extern Il2CppCodeGenString* _stringLiteral1904;
extern Il2CppCodeGenString* _stringLiteral3392;
extern "C" void Dictionary_2_GetObjectData_m17600_gshared (Dictionary_2_t208 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		_stringLiteral1277 = il2cpp_codegen_string_literal_from_index(1277);
		_stringLiteral1279 = il2cpp_codegen_string_literal_from_index(1279);
		_stringLiteral1281 = il2cpp_codegen_string_literal_from_index(1281);
		_stringLiteral1904 = il2cpp_codegen_string_literal_from_index(1904);
		_stringLiteral3392 = il2cpp_codegen_string_literal_from_index(3392);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t3660* V_0 = {0};
	{
		SerializationInfo_t1012 * L_0 = ___info;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral1277, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SerializationInfo_t1012 * L_2 = ___info;
		int32_t L_3 = (int32_t)(__this->___generation_14);
		NullCheck((SerializationInfo_t1012 *)L_2);
		SerializationInfo_AddValue_m9476((SerializationInfo_t1012 *)L_2, (String_t*)_stringLiteral1279, (int32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t1012 * L_4 = ___info;
		Object_t* L_5 = (Object_t*)(__this->___hcp_12);
		NullCheck((SerializationInfo_t1012 *)L_4);
		SerializationInfo_AddValue_m8509((SerializationInfo_t1012 *)L_4, (String_t*)_stringLiteral1281, (Object_t *)L_5, /*hidden argument*/NULL);
		V_0 = (KeyValuePair_2U5BU5D_t3660*)NULL;
		int32_t L_6 = (int32_t)(__this->___count_10);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = (int32_t)(__this->___count_10);
		V_0 = (KeyValuePair_2U5BU5D_t3660*)((KeyValuePair_2U5BU5D_t3660*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44), L_7));
		KeyValuePair_2U5BU5D_t3660* L_8 = V_0;
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, KeyValuePair_2U5BU5D_t3660*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Dictionary_2_t208 *)__this, (KeyValuePair_2U5BU5D_t3660*)L_8, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
	}

IL_0055:
	{
		SerializationInfo_t1012 * L_9 = ___info;
		Int32U5BU5D_t401* L_10 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_10);
		NullCheck((SerializationInfo_t1012 *)L_9);
		SerializationInfo_AddValue_m9476((SerializationInfo_t1012 *)L_9, (String_t*)_stringLiteral1904, (int32_t)(((int32_t)(((Array_t *)L_10)->max_length))), /*hidden argument*/NULL);
		SerializationInfo_t1012 * L_11 = ___info;
		KeyValuePair_2U5BU5D_t3660* L_12 = V_0;
		NullCheck((SerializationInfo_t1012 *)L_11);
		SerializationInfo_AddValue_m8509((SerializationInfo_t1012 *)L_11, (String_t*)_stringLiteral3392, (Object_t *)(Object_t *)L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::OnDeserialization(System.Object)
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1279;
extern Il2CppCodeGenString* _stringLiteral1281;
extern Il2CppCodeGenString* _stringLiteral1904;
extern Il2CppCodeGenString* _stringLiteral3392;
extern "C" void Dictionary_2_OnDeserialization_m17601_gshared (Dictionary_2_t208 * __this, Object_t * ___sender, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		_stringLiteral1279 = il2cpp_codegen_string_literal_from_index(1279);
		_stringLiteral1281 = il2cpp_codegen_string_literal_from_index(1281);
		_stringLiteral1904 = il2cpp_codegen_string_literal_from_index(1904);
		_stringLiteral3392 = il2cpp_codegen_string_literal_from_index(3392);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t3660* V_1 = {0};
	int32_t V_2 = 0;
	{
		SerializationInfo_t1012 * L_0 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t1012 * L_1 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		NullCheck((SerializationInfo_t1012 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m9483((SerializationInfo_t1012 *)L_1, (String_t*)_stringLiteral1279, /*hidden argument*/NULL);
		__this->___generation_14 = L_2;
		SerializationInfo_t1012 * L_3 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1012 *)L_3);
		Object_t * L_5 = SerializationInfo_GetValue_m9477((SerializationInfo_t1012 *)L_3, (String_t*)_stringLiteral1281, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->___hcp_12 = ((Object_t*)Castclass(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30)));
		SerializationInfo_t1012 * L_6 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		NullCheck((SerializationInfo_t1012 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m9483((SerializationInfo_t1012 *)L_6, (String_t*)_stringLiteral1904, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t1012 * L_8 = (SerializationInfo_t1012 *)(__this->___serialization_info_13);
		Type_t * L_9 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1012 *)L_8);
		Object_t * L_10 = SerializationInfo_GetValue_m9477((SerializationInfo_t1012 *)L_8, (String_t*)_stringLiteral3392, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t3660*)((KeyValuePair_2U5BU5D_t3660*)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t208 *)__this);
		(( void (*) (Dictionary_2_t208 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)((Dictionary_2_t208 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		__this->___count_10 = 0;
		KeyValuePair_2U5BU5D_t3660* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t3660* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t352 *)((KeyValuePair_2_t352 *)(KeyValuePair_2_t352 *)SZArrayLdElema(L_14, L_15, sizeof(KeyValuePair_2_t352 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		KeyValuePair_2U5BU5D_t3660* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((KeyValuePair_2_t352 *)((KeyValuePair_2_t352 *)(KeyValuePair_2_t352 *)SZArrayLdElema(L_17, L_18, sizeof(KeyValuePair_2_t352 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((Dictionary_2_t208 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Add(TKey,TValue) */, (Dictionary_2_t208 *)__this, (int32_t)L_16, (int32_t)L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t3660* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)(((Array_t *)L_22)->max_length))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_23+(int32_t)1));
		__this->___serialization_info_13 = (SerializationInfo_t1012 *)NULL;
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::Remove(TKey)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* ERaffleResult_t207_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_Remove_m17602_gshared (Dictionary_2_t208 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		ERaffleResult_t207_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(203);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = {0};
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Int32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t401* L_6 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))));
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		int32_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return 0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t2500* L_11 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_11, L_12, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_15 = (Object_t*)(__this->___hcp_12);
		Int32U5BU5D_t401* L_16 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		int32_t L_19 = ___key;
		NullCheck((Object_t*)L_15);
		bool L_20 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Int32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_15, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_16, L_18, sizeof(int32_t))), (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_21 = V_2;
		V_3 = (int32_t)L_21;
		LinkU5BU5D_t2500* L_22 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1869 )))->___Next_1);
		V_2 = (int32_t)L_24;
		int32_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return 0;
	}

IL_00ad:
	{
		int32_t L_27 = (int32_t)(__this->___count_10);
		__this->___count_10 = ((int32_t)((int32_t)L_27-(int32_t)1));
		int32_t L_28 = V_3;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t401* L_29 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_30 = V_1;
		LinkU5BU5D_t2500* L_31 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_32 = V_2;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_31, L_32, sizeof(Link_t1869 )))->___Next_1);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, L_30, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t2500* L_34 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_35 = V_3;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		LinkU5BU5D_t2500* L_36 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_37 = V_2;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_36, L_37, sizeof(Link_t1869 )))->___Next_1);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_34, L_35, sizeof(Link_t1869 )))->___Next_1 = L_38;
	}

IL_0104:
	{
		LinkU5BU5D_t2500* L_39 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_40 = V_2;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = (int32_t)(__this->___emptySlot_9);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_39, L_40, sizeof(Link_t1869 )))->___Next_1 = L_41;
		int32_t L_42 = V_2;
		__this->___emptySlot_9 = L_42;
		LinkU5BU5D_t2500* L_43 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_44 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_43, L_44, sizeof(Link_t1869 )))->___HashCode_0 = 0;
		Int32U5BU5D_t401* L_45 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		int32_t L_46 = V_2;
		Initobj (Int32_t372_il2cpp_TypeInfo_var, (&V_4));
		int32_t L_47 = V_4;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_45, L_46, sizeof(int32_t))) = (int32_t)L_47;
		ERaffleResultU5BU5D_t2708* L_48 = (ERaffleResultU5BU5D_t2708*)(__this->___valueSlots_7);
		int32_t L_49 = V_2;
		Initobj (ERaffleResult_t207_il2cpp_TypeInfo_var, (&V_5));
		int32_t L_50 = V_5;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_48, L_49, sizeof(int32_t))) = (int32_t)L_50;
		int32_t L_51 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_51+(int32_t)1));
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::TryGetValue(TKey,TValue&)
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* ERaffleResult_t207_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern "C" bool Dictionary_2_TryGetValue_m17603_gshared (Dictionary_2_t208 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		ERaffleResult_t207_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(203);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Int32>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t401* L_5 = (Int32U5BU5D_t401*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t401* L_7 = (Int32U5BU5D_t401*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t2500* L_9 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1869 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0090;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		Int32U5BU5D_t401* L_14 = (Int32U5BU5D_t401*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Int32>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_0090;
		}
	}
	{
		int32_t* L_19 = ___value;
		ERaffleResultU5BU5D_t2708* L_20 = (ERaffleResultU5BU5D_t2708*)(__this->___valueSlots_7);
		int32_t L_21 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		*L_19 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_20, L_22, sizeof(int32_t)));
		return 1;
	}

IL_0090:
	{
		LinkU5BU5D_t2500* L_23 = (LinkU5BU5D_t2500*)(__this->___linkSlots_5);
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = (int32_t)(((Link_t1869 *)(Link_t1869 *)SZArrayLdElema(L_23, L_24, sizeof(Link_t1869 )))->___Next_1);
		V_1 = (int32_t)L_25;
	}

IL_00a2:
	{
		int32_t L_26 = V_1;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		int32_t* L_27 = ___value;
		Initobj (ERaffleResult_t207_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_28 = V_2;
		*L_27 = L_28;
		return 0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::get_Keys()
extern "C" KeyCollection_t2713 * Dictionary_2_get_Keys_m17604_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		KeyCollection_t2713 * L_0 = (KeyCollection_t2713 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		(( void (*) (KeyCollection_t2713 *, Dictionary_2_t208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48)->method)(L_0, (Dictionary_2_t208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::get_Values()
extern "C" ValueCollection_t2716 * Dictionary_2_get_Values_m17605_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		ValueCollection_t2716 * L_0 = (ValueCollection_t2716 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49));
		(( void (*) (ValueCollection_t2716 *, Dictionary_2_t208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50)->method)(L_0, (Dictionary_2_t208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ToTKey(System.Object)
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* ArgumentNullException_t1037_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral63;
extern Il2CppCodeGenString* _stringLiteral3393;
extern "C" int32_t Dictionary_2_ToTKey_m17606_gshared (Dictionary_2_t208 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1037_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		_stringLiteral3393 = il2cpp_codegen_string_literal_from_index(3393);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1037 * L_1 = (ArgumentNullException_t1037 *)il2cpp_codegen_object_new (ArgumentNullException_t1037_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m5176(L_1, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 51)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1319(NULL /*static, unused*/, (String_t*)_stringLiteral3393, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t764 * L_6 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m7574(L_6, (String_t*)L_5, (String_t*)_stringLiteral63, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0040:
	{
		Object_t * L_7 = ___key;
		return ((*(int32_t*)((int32_t*)UnBox (L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)))));
	}
}
// TValue System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ToTValue(System.Object)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ERaffleResult_t207_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t764_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3393;
extern Il2CppCodeGenString* _stringLiteral64;
extern "C" int32_t Dictionary_2_ToTValue_m17607_gshared (Dictionary_2_t208 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		ERaffleResult_t207_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(203);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		_stringLiteral3393 = il2cpp_codegen_string_literal_from_index(3393);
		_stringLiteral64 = il2cpp_codegen_string_literal_from_index(64);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		Object_t * L_0 = ___value;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean System.Type::get_IsValueType() */, (Type_t *)L_1);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (ERaffleResult_t207_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		Object_t * L_4 = ___value;
		if (((Object_t *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, (RuntimeTypeHandle_t1772 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1319(NULL /*static, unused*/, (String_t*)_stringLiteral3393, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t764 * L_8 = (ArgumentException_t764 *)il2cpp_codegen_object_new (ArgumentException_t764_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m7574(L_8, (String_t*)L_7, (String_t*)_stringLiteral64, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0053:
	{
		Object_t * L_9 = ___value;
		return ((*(int32_t*)((int32_t*)UnBox (L_9, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)))));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17608_gshared (Dictionary_2_t208 * __this, KeyValuePair_2_t352  ___pair, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((KeyValuePair_2_t352 *)(&___pair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((Dictionary_2_t208 *)__this);
		bool L_1 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t* >::Invoke(33 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::TryGetValue(TKey,TValue&) */, (Dictionary_2_t208 *)__this, (int32_t)L_0, (int32_t*)(&V_0));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return 0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42));
		EqualityComparer_1_t2721 * L_2 = (( EqualityComparer_1_t2721 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41));
		int32_t L_3 = (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((KeyValuePair_2_t352 *)(&___pair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_4 = V_0;
		NullCheck((EqualityComparer_1_t2721 *)L_2);
		bool L_5 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<ERaffleResult>::Equals(T,T) */, (EqualityComparer_1_t2721 *)L_2, (int32_t)L_3, (int32_t)L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::GetEnumerator()
extern "C" Enumerator_t436  Dictionary_2_GetEnumerator_m1673_gshared (Dictionary_2_t208 * __this, const MethodInfo* method)
{
	{
		Enumerator_t436  L_0 = {0};
		(( void (*) (Enumerator_t436 *, Dictionary_2_t208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(&L_0, (Dictionary_2_t208 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>::<CopyTo>m__0(TKey,TValue)
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m17609_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		int32_t L_3 = ___value;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t451  L_6 = {0};
		DictionaryEntry__ctor_m9467(&L_6, (Object_t *)L_2, (Object_t *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_33.h"
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_33MethodDeclarations.h"
// Declaration !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>(System.Int32)
extern "C" KeyValuePair_2_t352  Array_InternalArray__get_Item_TisKeyValuePair_2_t352_m30303_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t352_m30303(__this, p0, method) (( KeyValuePair_2_t352  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t352_m30303_gshared)(__this, p0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::.ctor(System.Array)
// System.Array
#include "mscorlib_System_Array.h"
extern "C" void InternalEnumerator_1__ctor_m17610_gshared (InternalEnumerator_1_t2711 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17611_gshared (InternalEnumerator_1_t2711 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t352  L_0 = (( KeyValuePair_2_t352  (*) (InternalEnumerator_1_t2711 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2711 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t352  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17612_gshared (InternalEnumerator_1_t2711 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::MoveNext()
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern "C" bool InternalEnumerator_1_MoveNext_m17613_gshared (InternalEnumerator_1_t2711 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9469((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::get_Current()
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3382;
extern Il2CppCodeGenString* _stringLiteral3383;
extern "C" KeyValuePair_2_t352  InternalEnumerator_1_get_Current_m17614_gshared (InternalEnumerator_1_t2711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3382 = il2cpp_codegen_string_literal_from_index(3382);
		_stringLiteral3383 = il2cpp_codegen_string_literal_from_index(3383);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, (String_t*)_stringLiteral3382, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t425 * L_3 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_3, (String_t*)_stringLiteral3383, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		KeyValuePair_2_t352  L_8 = (( KeyValuePair_2_t352  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::.ctor(TKey,TValue)
// System.Int32
#include "mscorlib_System_Int32.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
extern "C" void KeyValuePair_2__ctor_m17615_gshared (KeyValuePair_2_t352 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		(( void (*) (KeyValuePair_2_t352 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t352 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value;
		(( void (*) (KeyValuePair_2_t352 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t352 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m1676_gshared (KeyValuePair_2_t352 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___key_0);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m17616_gshared (KeyValuePair_2_t352 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___key_0 = L_0;
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m1675_gshared (KeyValuePair_2_t352 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___value_1);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m17617_gshared (KeyValuePair_2_t352 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___value_1 = L_0;
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::ToString()
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* StringU5BU5D_t137_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral719;
extern "C" String_t* KeyValuePair_2_ToString_m17618_gshared (KeyValuePair_2_t352 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral58 = il2cpp_codegen_string_literal_from_index(58);
		_stringLiteral46 = il2cpp_codegen_string_literal_from_index(46);
		_stringLiteral719 = il2cpp_codegen_string_literal_from_index(719);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	int32_t G_B2_0 = 0;
	StringU5BU5D_t137* G_B2_1 = {0};
	StringU5BU5D_t137* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	StringU5BU5D_t137* G_B1_1 = {0};
	StringU5BU5D_t137* G_B1_2 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	StringU5BU5D_t137* G_B3_2 = {0};
	StringU5BU5D_t137* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	StringU5BU5D_t137* G_B5_1 = {0};
	StringU5BU5D_t137* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	StringU5BU5D_t137* G_B4_1 = {0};
	StringU5BU5D_t137* G_B4_2 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	StringU5BU5D_t137* G_B6_2 = {0};
	StringU5BU5D_t137* G_B6_3 = {0};
	{
		StringU5BU5D_t137* L_0 = (StringU5BU5D_t137*)((StringU5BU5D_t137*)SZArrayNew(StringU5BU5D_t137_il2cpp_TypeInfo_var, 5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral58);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral58;
		StringU5BU5D_t137* L_1 = (StringU5BU5D_t137*)L_0;
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t352 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}
	{
		int32_t L_3 = (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t352 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		NullCheck((int32_t*)(&V_0));
		String_t* L_4 = Int32_ToString_m1380((int32_t*)(&V_0), NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(String_t*))) = (String_t*)G_B3_0;
		StringU5BU5D_t137* L_6 = (StringU5BU5D_t137*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral46);
		*((String_t**)(String_t**)SZArrayLdElema(L_6, 2, sizeof(String_t*))) = (String_t*)_stringLiteral46;
		StringU5BU5D_t137* L_7 = (StringU5BU5D_t137*)L_6;
		int32_t L_8 = (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t352 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}
	{
		int32_t L_9 = (( int32_t (*) (KeyValuePair_2_t352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t352 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1)));
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(String_t*))) = (String_t*)G_B6_0;
		StringU5BU5D_t137* L_12 = (StringU5BU5D_t137*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral719);
		*((String_t**)(String_t**)SZArrayLdElema(L_12, 4, sizeof(String_t*))) = (String_t*)_stringLiteral719;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1461(NULL /*static, unused*/, (StringU5BU5D_t137*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Array/InternalEnumerator`1<ERaffleResult>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_34.h"
// System.Array/InternalEnumerator`1<ERaffleResult>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_34MethodDeclarations.h"
// Declaration !!0 System.Array::InternalArray__get_Item<ERaffleResult>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<ERaffleResult>(System.Int32)
extern "C" int32_t Array_InternalArray__get_Item_TisERaffleResult_t207_m30314_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisERaffleResult_t207_m30314(__this, p0, method) (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisERaffleResult_t207_m30314_gshared)(__this, p0, method)
// System.Void System.Array/InternalEnumerator`1<ERaffleResult>::.ctor(System.Array)
// System.Array
#include "mscorlib_System_Array.h"
extern "C" void InternalEnumerator_1__ctor_m17619_gshared (InternalEnumerator_1_t2712 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<ERaffleResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17620_gshared (InternalEnumerator_1_t2712 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (InternalEnumerator_1_t2712 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2712 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<ERaffleResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17621_gshared (InternalEnumerator_1_t2712 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<ERaffleResult>::MoveNext()
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern "C" bool InternalEnumerator_1_MoveNext_m17622_gshared (InternalEnumerator_1_t2712 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9469((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<ERaffleResult>::get_Current()
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3382;
extern Il2CppCodeGenString* _stringLiteral3383;
extern "C" int32_t InternalEnumerator_1_get_Current_m17623_gshared (InternalEnumerator_1_t2712 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral3382 = il2cpp_codegen_string_literal_from_index(3382);
		_stringLiteral3383 = il2cpp_codegen_string_literal_from_index(3383);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, (String_t*)_stringLiteral3382, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t425 * L_3 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_3, (String_t*)_stringLiteral3383, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9469((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		int32_t L_8 = (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
