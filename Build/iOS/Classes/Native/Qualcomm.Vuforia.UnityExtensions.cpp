﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>

#include <map>
struct TypeInfo;
struct MethodInfo;
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
Il2CppAssembly g_Qualcomm_Vuforia_UnityExtensions_Assembly = 
{
	{ "Qualcomm.Vuforia.UnityExtensions", NULL, NULL, NULL, { 0 }, 32772, 0, 0, 0, 0, 0, 0 },
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image,
	1455,
};
Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image = 
{
	 "Qualcomm.Vuforia.UnityExtensions.dll" ,
	&g_Qualcomm_Vuforia_UnityExtensions_Assembly,
	705,
	221,
	NULL,
	0,
};
