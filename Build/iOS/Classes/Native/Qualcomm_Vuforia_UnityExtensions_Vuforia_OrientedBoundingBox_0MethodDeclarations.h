﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.OrientedBoundingBox3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.OrientedBoundingBox3D::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" void OrientedBoundingBox3D__ctor_m5554 (OrientedBoundingBox3D_t1092 * __this, Vector3_t36  ___center, Vector3_t36  ___halfExtents, float ___rotationY, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_Center()
extern "C" Vector3_t36  OrientedBoundingBox3D_get_Center_m5555 (OrientedBoundingBox3D_t1092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_Center(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_Center_m5556 (OrientedBoundingBox3D_t1092 * __this, Vector3_t36  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_HalfExtents()
extern "C" Vector3_t36  OrientedBoundingBox3D_get_HalfExtents_m5557 (OrientedBoundingBox3D_t1092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_HalfExtents(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_HalfExtents_m5558 (OrientedBoundingBox3D_t1092 * __this, Vector3_t36  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.OrientedBoundingBox3D::get_RotationY()
extern "C" float OrientedBoundingBox3D_get_RotationY_m5559 (OrientedBoundingBox3D_t1092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_RotationY(System.Single)
extern "C" void OrientedBoundingBox3D_set_RotationY_m5560 (OrientedBoundingBox3D_t1092 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
