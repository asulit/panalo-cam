﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>
struct List_1_t1166;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t301;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>
struct  Enumerator_t1320 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::l
	List_1_t1166 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::current
	ReconstructionAbstractBehaviour_t301 * ___current_3;
};
