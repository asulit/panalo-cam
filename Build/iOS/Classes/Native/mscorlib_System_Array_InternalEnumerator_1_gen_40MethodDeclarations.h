﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.Color>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_40.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18624_gshared (InternalEnumerator_1_t2791 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18624(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2791 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18624_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18625_gshared (InternalEnumerator_1_t2791 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18625(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2791 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18625_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18626_gshared (InternalEnumerator_1_t2791 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18626(__this, method) (( void (*) (InternalEnumerator_1_t2791 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18626_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18627_gshared (InternalEnumerator_1_t2791 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18627(__this, method) (( bool (*) (InternalEnumerator_1_t2791 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18627_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern "C" Color_t9  InternalEnumerator_1_get_Current_m18628_gshared (InternalEnumerator_1_t2791 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18628(__this, method) (( Color_t9  (*) (InternalEnumerator_1_t2791 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18628_gshared)(__this, method)
