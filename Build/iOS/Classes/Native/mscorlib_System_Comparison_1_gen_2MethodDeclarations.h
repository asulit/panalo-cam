﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Comparison`1<UnityEngine.UI.ICanvasElement>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_4MethodDeclarations.h"
#define Comparison_1__ctor_m3403(__this, ___object, ___method, method) (( void (*) (Comparison_1_t549 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m14828_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.UI.ICanvasElement>::Invoke(T,T)
#define Comparison_1_Invoke_m20198(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t549 *, Object_t *, Object_t *, const MethodInfo*))Comparison_1_Invoke_m14829_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.UI.ICanvasElement>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m20199(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t549 *, Object_t *, Object_t *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m14830_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.UI.ICanvasElement>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m20200(__this, ___result, method) (( int32_t (*) (Comparison_1_t549 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m14831_gshared)(__this, ___result, method)
