﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<AnimatorFrame>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_38.h"
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"

// System.Void System.Array/InternalEnumerator`1<AnimatorFrame>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18340_gshared (InternalEnumerator_1_t2771 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18340(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2771 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18340_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<AnimatorFrame>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18341_gshared (InternalEnumerator_1_t2771 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18341(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2771 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18341_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AnimatorFrame>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18342_gshared (InternalEnumerator_1_t2771 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18342(__this, method) (( void (*) (InternalEnumerator_1_t2771 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18342_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AnimatorFrame>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18343_gshared (InternalEnumerator_1_t2771 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18343(__this, method) (( bool (*) (InternalEnumerator_1_t2771 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18343_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AnimatorFrame>::get_Current()
extern "C" AnimatorFrame_t235  InternalEnumerator_1_get_Current_m18344_gshared (InternalEnumerator_1_t2771 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18344(__this, method) (( AnimatorFrame_t235  (*) (InternalEnumerator_1_t2771 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18344_gshared)(__this, method)
