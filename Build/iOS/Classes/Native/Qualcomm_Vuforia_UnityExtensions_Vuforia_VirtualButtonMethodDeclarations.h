﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VirtualButton
struct VirtualButton_t1223;

// System.Void Vuforia.VirtualButton::.ctor()
extern "C" void VirtualButton__ctor_m6869 (VirtualButton_t1223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
