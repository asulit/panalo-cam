﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<InputLayer>
struct IList_1_t2539;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<InputLayer>
struct  ReadOnlyCollection_1_t2538  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<InputLayer>::list
	Object_t* ___list_0;
};
