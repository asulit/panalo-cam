﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Surface,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_37MethodDeclarations.h"
#define Transform_1__ctor_m27407(__this, ___object, ___method, method) (( void (*) (Transform_1_t3396 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m19875_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Surface,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m27408(__this, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Transform_1_t3396 *, int32_t, Object_t *, const MethodInfo*))Transform_1_Invoke_m19876_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Surface,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m27409(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3396 *, int32_t, Object_t *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m19877_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Surface,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m27410(__this, ___result, method) (( DictionaryEntry_t451  (*) (Transform_1_t3396 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m19878_gshared)(__this, ___result, method)
