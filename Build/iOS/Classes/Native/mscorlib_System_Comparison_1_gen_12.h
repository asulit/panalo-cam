﻿#pragma once
#include <stdint.h>
// Common.Command
struct Command_t348;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Common.Command>
struct  Comparison_1_t2625  : public MulticastDelegate_t28
{
};
