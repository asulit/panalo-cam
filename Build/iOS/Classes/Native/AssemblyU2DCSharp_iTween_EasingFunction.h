﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Single
#include "mscorlib_System_Single.h"
// iTween/EasingFunction
struct  EasingFunction_t255  : public MulticastDelegate_t28
{
};
