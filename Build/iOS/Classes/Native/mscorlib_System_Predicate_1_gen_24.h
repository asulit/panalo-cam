﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t35;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Transform>
struct  Predicate_1_t2858  : public MulticastDelegate_t28
{
};
