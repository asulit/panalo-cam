﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<EScreens>
struct DefaultComparer_t2680;
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<EScreens>::.ctor()
extern "C" void DefaultComparer__ctor_m17273_gshared (DefaultComparer_t2680 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m17273(__this, method) (( void (*) (DefaultComparer_t2680 *, const MethodInfo*))DefaultComparer__ctor_m17273_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<EScreens>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m17274_gshared (DefaultComparer_t2680 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m17274(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2680 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m17274_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<EScreens>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m17275_gshared (DefaultComparer_t2680 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m17275(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2680 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m17275_gshared)(__this, ___x, ___y, method)
