﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2544;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15697_gshared (Enumerator_t2553 * __this, Dictionary_2_t2544 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15697(__this, ___host, method) (( void (*) (Enumerator_t2553 *, Dictionary_2_t2544 *, const MethodInfo*))Enumerator__ctor_m15697_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15698_gshared (Enumerator_t2553 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15698(__this, method) (( Object_t * (*) (Enumerator_t2553 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15698_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m15699_gshared (Enumerator_t2553 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15699(__this, method) (( void (*) (Enumerator_t2553 *, const MethodInfo*))Enumerator_Dispose_m15699_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15700_gshared (Enumerator_t2553 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15700(__this, method) (( bool (*) (Enumerator_t2553 *, const MethodInfo*))Enumerator_MoveNext_m15700_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m15701_gshared (Enumerator_t2553 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15701(__this, method) (( int32_t (*) (Enumerator_t2553 *, const MethodInfo*))Enumerator_get_Current_m15701_gshared)(__this, method)
