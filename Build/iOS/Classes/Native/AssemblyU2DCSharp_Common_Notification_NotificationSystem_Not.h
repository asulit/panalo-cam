﻿#pragma once
#include <stdint.h>
// Common.Notification.NotificationInstance
struct NotificationInstance_t85;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// Common.Notification.NotificationSystem/NotificationInitializer
struct  NotificationInitializer_t88  : public MulticastDelegate_t28
{
};
