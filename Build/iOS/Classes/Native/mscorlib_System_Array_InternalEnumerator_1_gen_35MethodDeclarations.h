﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_35.h"
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17753_gshared (InternalEnumerator_1_t2724 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17753(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2724 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17753_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17754_gshared (InternalEnumerator_1_t2724 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17754(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2724 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17754_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17755_gshared (InternalEnumerator_1_t2724 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17755(__this, method) (( void (*) (InternalEnumerator_1_t2724 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17755_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17756_gshared (InternalEnumerator_1_t2724 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17756(__this, method) (( bool (*) (InternalEnumerator_1_t2724 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17756_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.CameraDevice/FocusMode>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m17757_gshared (InternalEnumerator_1_t2724 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17757(__this, method) (( int32_t (*) (InternalEnumerator_1_t2724 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17757_gshared)(__this, method)
