﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m28017(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3421 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14611_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28018(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3421 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14612_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m28019(__this, method) (( void (*) (InternalEnumerator_1_t3421 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14613_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m28020(__this, method) (( bool (*) (InternalEnumerator_1_t3421 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14614_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m28021(__this, method) (( MarkerAbstractBehaviour_t293 * (*) (InternalEnumerator_1_t3421 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14615_gshared)(__this, method)
