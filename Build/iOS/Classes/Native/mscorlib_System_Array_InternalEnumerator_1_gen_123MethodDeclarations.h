﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Array/InternalEnumerator`1<System.Reflection.ConstructorInfo>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m29701(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3559 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14611_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ConstructorInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29702(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3559 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14612_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ConstructorInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m29703(__this, method) (( void (*) (InternalEnumerator_1_t3559 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14613_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ConstructorInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m29704(__this, method) (( bool (*) (InternalEnumerator_1_t3559 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14614_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ConstructorInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m29705(__this, method) (( ConstructorInfo_t1042 * (*) (InternalEnumerator_1_t3559 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14615_gshared)(__this, method)
