﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Dropdown/DropdownItem
struct DropdownItem_t555;
// UnityEngine.UI.Dropdown
struct Dropdown_t564;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.Dropdown/<Show>c__AnonStorey6
struct  U3CShowU3Ec__AnonStorey6_t565  : public Object_t
{
	// UnityEngine.UI.Dropdown/DropdownItem UnityEngine.UI.Dropdown/<Show>c__AnonStorey6::item
	DropdownItem_t555 * ___item_0;
	// UnityEngine.UI.Dropdown UnityEngine.UI.Dropdown/<Show>c__AnonStorey6::<>f__this
	Dropdown_t564 * ___U3CU3Ef__this_1;
};
