﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerDisplayAttribute
struct DebuggerDisplayAttribute_t1898;
// System.String
struct String_t;

// System.Void System.Diagnostics.DebuggerDisplayAttribute::.ctor(System.String)
extern "C" void DebuggerDisplayAttribute__ctor_m11207 (DebuggerDisplayAttribute_t1898 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerDisplayAttribute::set_Name(System.String)
extern "C" void DebuggerDisplayAttribute_set_Name_m11208 (DebuggerDisplayAttribute_t1898 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
