﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Threading.ThreadInterruptedException
struct ThreadInterruptedException_t2297;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Threading.ThreadInterruptedException::.ctor()
extern "C" void ThreadInterruptedException__ctor_m13697 (ThreadInterruptedException_t2297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ThreadInterruptedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ThreadInterruptedException__ctor_m13698 (ThreadInterruptedException_t2297 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
