﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>
struct Transform_1_t3484;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m28790_gshared (Transform_1_t3484 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m28790(__this, ___object, ___method, method) (( void (*) (Transform_1_t3484 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m28790_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::Invoke(TKey,TValue)
extern "C" Object_t * Transform_1_Invoke_m28791_gshared (Transform_1_t3484 * __this, Object_t * ___key, ProfileData_t1226  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m28791(__this, ___key, ___value, method) (( Object_t * (*) (Transform_1_t3484 *, Object_t *, ProfileData_t1226 , const MethodInfo*))Transform_1_Invoke_m28791_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m28792_gshared (Transform_1_t3484 * __this, Object_t * ___key, ProfileData_t1226  ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m28792(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3484 *, Object_t *, ProfileData_t1226 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m28792_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Transform_1_EndInvoke_m28793_gshared (Transform_1_t3484 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m28793(__this, ___result, method) (( Object_t * (*) (Transform_1_t3484 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m28793_gshared)(__this, ___result, method)
