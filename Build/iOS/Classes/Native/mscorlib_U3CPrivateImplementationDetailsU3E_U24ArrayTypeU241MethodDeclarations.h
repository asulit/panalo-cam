﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2416_t2387_marshal(const U24ArrayTypeU2416_t2387& unmarshaled, U24ArrayTypeU2416_t2387_marshaled& marshaled);
extern "C" void U24ArrayTypeU2416_t2387_marshal_back(const U24ArrayTypeU2416_t2387_marshaled& marshaled, U24ArrayTypeU2416_t2387& unmarshaled);
extern "C" void U24ArrayTypeU2416_t2387_marshal_cleanup(U24ArrayTypeU2416_t2387_marshaled& marshaled);
