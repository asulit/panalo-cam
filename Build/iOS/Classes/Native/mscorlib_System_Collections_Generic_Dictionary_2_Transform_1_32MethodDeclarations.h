﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.InputField,System.Boolean,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_30MethodDeclarations.h"
#define Transform_1__ctor_m18047(__this, ___object, ___method, method) (( void (*) (Transform_1_t2736 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m18022_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.InputField,System.Boolean,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m18048(__this, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Transform_1_t2736 *, InputField_t226 *, bool, const MethodInfo*))Transform_1_Invoke_m18023_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.InputField,System.Boolean,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m18049(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2736 *, InputField_t226 *, bool, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m18024_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.InputField,System.Boolean,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m18050(__this, ___result, method) (( DictionaryEntry_t451  (*) (Transform_1_t2736 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m18025_gshared)(__this, ___result, method)
