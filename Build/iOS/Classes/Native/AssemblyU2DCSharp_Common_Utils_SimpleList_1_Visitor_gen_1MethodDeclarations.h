﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void Common.Utils.SimpleList`1/Visitor<Common.Signal.Signal/SignalListener>::.ctor(System.Object,System.IntPtr)
// Common.Utils.SimpleList`1/Visitor<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_Visitor_genMethodDeclarations.h"
#define Visitor__ctor_m16270(__this, ___object, ___method, method) (( void (*) (Visitor_t2608 *, Object_t *, IntPtr_t, const MethodInfo*))Visitor__ctor_m16195_gshared)(__this, ___object, ___method, method)
// System.Void Common.Utils.SimpleList`1/Visitor<Common.Signal.Signal/SignalListener>::Invoke(T)
#define Visitor_Invoke_m16271(__this, ___item, method) (( void (*) (Visitor_t2608 *, SignalListener_t114 *, const MethodInfo*))Visitor_Invoke_m16196_gshared)(__this, ___item, method)
// System.IAsyncResult Common.Utils.SimpleList`1/Visitor<Common.Signal.Signal/SignalListener>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Visitor_BeginInvoke_m16272(__this, ___item, ___callback, ___object, method) (( Object_t * (*) (Visitor_t2608 *, SignalListener_t114 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Visitor_BeginInvoke_m16197_gshared)(__this, ___item, ___callback, ___object, method)
// System.Void Common.Utils.SimpleList`1/Visitor<Common.Signal.Signal/SignalListener>::EndInvoke(System.IAsyncResult)
#define Visitor_EndInvoke_m16273(__this, ___result, method) (( void (*) (Visitor_t2608 *, Object_t *, const MethodInfo*))Visitor_EndInvoke_m16198_gshared)(__this, ___result, method)
