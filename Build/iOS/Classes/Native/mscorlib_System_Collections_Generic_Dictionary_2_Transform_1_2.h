﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,Common.Fsm.FsmState,System.Collections.DictionaryEntry>
struct  Transform_1_t2502  : public MulticastDelegate_t28
{
};
