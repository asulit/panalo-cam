﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_43MethodDeclarations.h"
#define ValueCollection__ctor_m17539(__this, ___dictionary, method) (( void (*) (ValueCollection_t2706 *, Dictionary_2_t192 *, const MethodInfo*))ValueCollection__ctor_m17470_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17540(__this, ___item, method) (( void (*) (ValueCollection_t2706 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17471_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17541(__this, method) (( void (*) (ValueCollection_t2706 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17472_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17542(__this, ___item, method) (( bool (*) (ValueCollection_t2706 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17473_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17543(__this, ___item, method) (( bool (*) (ValueCollection_t2706 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17474_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17544(__this, method) (( Object_t* (*) (ValueCollection_t2706 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17475_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m17545(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2706 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m17476_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17546(__this, method) (( Object_t * (*) (ValueCollection_t2706 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17477_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17547(__this, method) (( bool (*) (ValueCollection_t2706 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17478_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17548(__this, method) (( bool (*) (ValueCollection_t2706 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17479_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m17549(__this, method) (( Object_t * (*) (ValueCollection_t2706 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m17480_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m17550(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2706 *, StringU5BU5D_t137*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m17481_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::GetEnumerator()
#define ValueCollection_GetEnumerator_m17551(__this, method) (( Enumerator_t3659  (*) (ValueCollection_t2706 *, const MethodInfo*))ValueCollection_GetEnumerator_m17482_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ESubScreens,System.String>::get_Count()
#define ValueCollection_get_Count_m17552(__this, method) (( int32_t (*) (ValueCollection_t2706 *, const MethodInfo*))ValueCollection_get_Count_m17483_gshared)(__this, method)
