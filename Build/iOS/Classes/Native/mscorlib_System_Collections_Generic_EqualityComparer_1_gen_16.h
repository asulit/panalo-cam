﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>
struct EqualityComparer_1_t3269;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>
struct  EqualityComparer_1_t3269  : public Object_t
{
};
struct EqualityComparer_1_t3269_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>::_default
	EqualityComparer_1_t3269 * ____default_0;
};
