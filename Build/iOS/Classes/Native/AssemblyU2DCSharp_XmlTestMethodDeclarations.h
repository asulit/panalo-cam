﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// XmlTest
struct XmlTest_t147;

// System.Void XmlTest::.ctor()
extern "C" void XmlTest__ctor_m488 (XmlTest_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void XmlTest::Awake()
extern "C" void XmlTest_Awake_m489 (XmlTest_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
