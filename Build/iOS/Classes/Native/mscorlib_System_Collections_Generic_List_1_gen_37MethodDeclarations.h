﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t676;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Color32>
struct IEnumerable_1_t3713;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t3714;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Color32>
struct ICollection_1_t3715;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color32>
struct ReadOnlyCollection_1_t3040;
// UnityEngine.Color32[]
struct Color32U5BU5D_t778;
// System.Predicate`1<UnityEngine.Color32>
struct Predicate_1_t3045;
// System.Comparison`1<UnityEngine.Color32>
struct Comparison_1_t3048;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_62.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor()
extern "C" void List_1__ctor_m22259_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1__ctor_m22259(__this, method) (( void (*) (List_1_t676 *, const MethodInfo*))List_1__ctor_m22259_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m22260_gshared (List_1_t676 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m22260(__this, ___collection, method) (( void (*) (List_1_t676 *, Object_t*, const MethodInfo*))List_1__ctor_m22260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor(System.Int32)
extern "C" void List_1__ctor_m22261_gshared (List_1_t676 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m22261(__this, ___capacity, method) (( void (*) (List_1_t676 *, int32_t, const MethodInfo*))List_1__ctor_m22261_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.cctor()
extern "C" void List_1__cctor_m22262_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m22262(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m22262_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22263_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22263(__this, method) (( Object_t* (*) (List_1_t676 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22263_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22264_gshared (List_1_t676 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m22264(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t676 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m22264_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m22265_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22265(__this, method) (( Object_t * (*) (List_1_t676 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m22265_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m22266_gshared (List_1_t676 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m22266(__this, ___item, method) (( int32_t (*) (List_1_t676 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m22266_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m22267_gshared (List_1_t676 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m22267(__this, ___item, method) (( bool (*) (List_1_t676 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m22267_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m22268_gshared (List_1_t676 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m22268(__this, ___item, method) (( int32_t (*) (List_1_t676 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m22268_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m22269_gshared (List_1_t676 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m22269(__this, ___index, ___item, method) (( void (*) (List_1_t676 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m22269_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m22270_gshared (List_1_t676 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m22270(__this, ___item, method) (( void (*) (List_1_t676 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m22270_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22271_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22271(__this, method) (( bool (*) (List_1_t676 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22271_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m22272_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22272(__this, method) (( bool (*) (List_1_t676 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m22272_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m22273_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m22273(__this, method) (( Object_t * (*) (List_1_t676 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m22273_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m22274_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m22274(__this, method) (( bool (*) (List_1_t676 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m22274_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m22275_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m22275(__this, method) (( bool (*) (List_1_t676 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m22275_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m22276_gshared (List_1_t676 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m22276(__this, ___index, method) (( Object_t * (*) (List_1_t676 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m22276_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m22277_gshared (List_1_t676 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m22277(__this, ___index, ___value, method) (( void (*) (List_1_t676 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m22277_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Add(T)
extern "C" void List_1_Add_m22278_gshared (List_1_t676 * __this, Color32_t711  ___item, const MethodInfo* method);
#define List_1_Add_m22278(__this, ___item, method) (( void (*) (List_1_t676 *, Color32_t711 , const MethodInfo*))List_1_Add_m22278_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m22279_gshared (List_1_t676 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m22279(__this, ___newCount, method) (( void (*) (List_1_t676 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m22279_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m22280_gshared (List_1_t676 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m22280(__this, ___idx, ___count, method) (( void (*) (List_1_t676 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m22280_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m22281_gshared (List_1_t676 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m22281(__this, ___collection, method) (( void (*) (List_1_t676 *, Object_t*, const MethodInfo*))List_1_AddCollection_m22281_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m22282_gshared (List_1_t676 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m22282(__this, ___enumerable, method) (( void (*) (List_1_t676 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m22282_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m3786_gshared (List_1_t676 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m3786(__this, ___collection, method) (( void (*) (List_1_t676 *, Object_t*, const MethodInfo*))List_1_AddRange_m3786_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Color32>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3040 * List_1_AsReadOnly_m22283_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m22283(__this, method) (( ReadOnlyCollection_1_t3040 * (*) (List_1_t676 *, const MethodInfo*))List_1_AsReadOnly_m22283_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Clear()
extern "C" void List_1_Clear_m22284_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_Clear_m22284(__this, method) (( void (*) (List_1_t676 *, const MethodInfo*))List_1_Clear_m22284_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::Contains(T)
extern "C" bool List_1_Contains_m22285_gshared (List_1_t676 * __this, Color32_t711  ___item, const MethodInfo* method);
#define List_1_Contains_m22285(__this, ___item, method) (( bool (*) (List_1_t676 *, Color32_t711 , const MethodInfo*))List_1_Contains_m22285_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m22286_gshared (List_1_t676 * __this, Color32U5BU5D_t778* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m22286(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t676 *, Color32U5BU5D_t778*, int32_t, const MethodInfo*))List_1_CopyTo_m22286_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Color32>::Find(System.Predicate`1<T>)
extern "C" Color32_t711  List_1_Find_m22287_gshared (List_1_t676 * __this, Predicate_1_t3045 * ___match, const MethodInfo* method);
#define List_1_Find_m22287(__this, ___match, method) (( Color32_t711  (*) (List_1_t676 *, Predicate_1_t3045 *, const MethodInfo*))List_1_Find_m22287_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m22288_gshared (Object_t * __this /* static, unused */, Predicate_1_t3045 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m22288(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3045 *, const MethodInfo*))List_1_CheckMatch_m22288_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m22289_gshared (List_1_t676 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3045 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m22289(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t676 *, int32_t, int32_t, Predicate_1_t3045 *, const MethodInfo*))List_1_GetIndex_m22289_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Color32>::GetEnumerator()
extern "C" Enumerator_t3039  List_1_GetEnumerator_m22290_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m22290(__this, method) (( Enumerator_t3039  (*) (List_1_t676 *, const MethodInfo*))List_1_GetEnumerator_m22290_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m22291_gshared (List_1_t676 * __this, Color32_t711  ___item, const MethodInfo* method);
#define List_1_IndexOf_m22291(__this, ___item, method) (( int32_t (*) (List_1_t676 *, Color32_t711 , const MethodInfo*))List_1_IndexOf_m22291_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m22292_gshared (List_1_t676 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m22292(__this, ___start, ___delta, method) (( void (*) (List_1_t676 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m22292_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m22293_gshared (List_1_t676 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m22293(__this, ___index, method) (( void (*) (List_1_t676 *, int32_t, const MethodInfo*))List_1_CheckIndex_m22293_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m22294_gshared (List_1_t676 * __this, int32_t ___index, Color32_t711  ___item, const MethodInfo* method);
#define List_1_Insert_m22294(__this, ___index, ___item, method) (( void (*) (List_1_t676 *, int32_t, Color32_t711 , const MethodInfo*))List_1_Insert_m22294_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m22295_gshared (List_1_t676 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m22295(__this, ___collection, method) (( void (*) (List_1_t676 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m22295_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color32>::Remove(T)
extern "C" bool List_1_Remove_m22296_gshared (List_1_t676 * __this, Color32_t711  ___item, const MethodInfo* method);
#define List_1_Remove_m22296(__this, ___item, method) (( bool (*) (List_1_t676 *, Color32_t711 , const MethodInfo*))List_1_Remove_m22296_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m22297_gshared (List_1_t676 * __this, Predicate_1_t3045 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m22297(__this, ___match, method) (( int32_t (*) (List_1_t676 *, Predicate_1_t3045 *, const MethodInfo*))List_1_RemoveAll_m22297_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m22298_gshared (List_1_t676 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m22298(__this, ___index, method) (( void (*) (List_1_t676 *, int32_t, const MethodInfo*))List_1_RemoveAt_m22298_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m22299_gshared (List_1_t676 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m22299(__this, ___index, ___count, method) (( void (*) (List_1_t676 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m22299_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Reverse()
extern "C" void List_1_Reverse_m22300_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_Reverse_m22300(__this, method) (( void (*) (List_1_t676 *, const MethodInfo*))List_1_Reverse_m22300_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Sort()
extern "C" void List_1_Sort_m22301_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_Sort_m22301(__this, method) (( void (*) (List_1_t676 *, const MethodInfo*))List_1_Sort_m22301_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m22302_gshared (List_1_t676 * __this, Comparison_1_t3048 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m22302(__this, ___comparison, method) (( void (*) (List_1_t676 *, Comparison_1_t3048 *, const MethodInfo*))List_1_Sort_m22302_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Color32>::ToArray()
extern "C" Color32U5BU5D_t778* List_1_ToArray_m22303_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_ToArray_m22303(__this, method) (( Color32U5BU5D_t778* (*) (List_1_t676 *, const MethodInfo*))List_1_ToArray_m22303_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::TrimExcess()
extern "C" void List_1_TrimExcess_m22304_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m22304(__this, method) (( void (*) (List_1_t676 *, const MethodInfo*))List_1_TrimExcess_m22304_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m22305_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m22305(__this, method) (( int32_t (*) (List_1_t676 *, const MethodInfo*))List_1_get_Capacity_m22305_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m22306_gshared (List_1_t676 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m22306(__this, ___value, method) (( void (*) (List_1_t676 *, int32_t, const MethodInfo*))List_1_set_Capacity_m22306_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color32>::get_Count()
extern "C" int32_t List_1_get_Count_m22307_gshared (List_1_t676 * __this, const MethodInfo* method);
#define List_1_get_Count_m22307(__this, method) (( int32_t (*) (List_1_t676 *, const MethodInfo*))List_1_get_Count_m22307_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C" Color32_t711  List_1_get_Item_m22308_gshared (List_1_t676 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m22308(__this, ___index, method) (( Color32_t711  (*) (List_1_t676 *, int32_t, const MethodInfo*))List_1_get_Item_m22308_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m22309_gshared (List_1_t676 * __this, int32_t ___index, Color32_t711  ___value, const MethodInfo* method);
#define List_1_set_Item_m22309(__this, ___index, ___value, method) (( void (*) (List_1_t676 *, int32_t, Color32_t711 , const MethodInfo*))List_1_set_Item_m22309_gshared)(__this, ___index, ___value, method)
