﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.KeepAliveBehaviour
struct KeepAliveBehaviour_t290;

// System.Void Vuforia.KeepAliveBehaviour::.ctor()
extern "C" void KeepAliveBehaviour__ctor_m1250 (KeepAliveBehaviour_t290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
