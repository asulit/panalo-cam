﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Common.Notification.NotificationHandler>::.ctor(System.Collections.Generic.LinkedList`1<T>)
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_geMethodDeclarations.h"
#define Enumerator__ctor_m16037(__this, ___parent, method) (( void (*) (Enumerator_t2586 *, LinkedList_1_t90 *, const MethodInfo*))Enumerator__ctor_m16028_gshared)(__this, ___parent, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<Common.Notification.NotificationHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16038(__this, method) (( Object_t * (*) (Enumerator_t2586 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16029_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<Common.Notification.NotificationHandler>::get_Current()
#define Enumerator_get_Current_m16039(__this, method) (( Object_t * (*) (Enumerator_t2586 *, const MethodInfo*))Enumerator_get_Current_m16030_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<Common.Notification.NotificationHandler>::MoveNext()
#define Enumerator_MoveNext_m16040(__this, method) (( bool (*) (Enumerator_t2586 *, const MethodInfo*))Enumerator_MoveNext_m16031_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Common.Notification.NotificationHandler>::Dispose()
#define Enumerator_Dispose_m16041(__this, method) (( void (*) (Enumerator_t2586 *, const MethodInfo*))Enumerator_Dispose_m16032_gshared)(__this, method)
