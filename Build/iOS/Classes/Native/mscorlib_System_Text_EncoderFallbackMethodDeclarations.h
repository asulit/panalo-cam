﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderFallback
struct EncoderFallback_t2272;

// System.Void System.Text.EncoderFallback::.ctor()
extern "C" void EncoderFallback__ctor_m13441 (EncoderFallback_t2272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.EncoderFallback::.cctor()
extern "C" void EncoderFallback__cctor_m13442 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_ExceptionFallback()
extern "C" EncoderFallback_t2272 * EncoderFallback_get_ExceptionFallback_m13443 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_ReplacementFallback()
extern "C" EncoderFallback_t2272 * EncoderFallback_get_ReplacementFallback_m13444 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_StandardSafeFallback()
extern "C" EncoderFallback_t2272 * EncoderFallback_get_StandardSafeFallback_m13445 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
