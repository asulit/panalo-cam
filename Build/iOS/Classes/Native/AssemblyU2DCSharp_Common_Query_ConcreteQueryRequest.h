﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t86;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Query.ConcreteQueryRequest
struct  ConcreteQueryRequest_t106  : public Object_t
{
	// System.String Common.Query.ConcreteQueryRequest::queryId
	String_t* ___queryId_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Common.Query.ConcreteQueryRequest::paramMap
	Dictionary_2_t86 * ___paramMap_1;
};
