﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t782;
// UnityEngine.AssetBundle
struct AssetBundle_t785;

// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C" void AssetBundleCreateRequest__ctor_m3812 (AssetBundleCreateRequest_t782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C" AssetBundle_t785 * AssetBundleCreateRequest_get_assetBundle_m3813 (AssetBundleCreateRequest_t782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m3814 (AssetBundleCreateRequest_t782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
