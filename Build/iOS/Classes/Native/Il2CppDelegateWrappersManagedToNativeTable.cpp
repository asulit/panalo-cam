﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern "C" void pinvoke_delegate_wrapper_FsmActionRoutine_t27 ();
extern "C" void pinvoke_delegate_wrapper_StateActionProcessor_t50 ();
extern "C" void pinvoke_delegate_wrapper_DoubleClickCallback_t53 ();
extern "C" void pinvoke_delegate_wrapper_NotificationInitializer_t88 ();
extern "C" void pinvoke_delegate_wrapper_SignalListener_t114 ();
extern "C" void pinvoke_delegate_wrapper_NodeProcessor_t141 ();
extern "C" void pinvoke_delegate_wrapper_EasingFunction_t255 ();
extern "C" void pinvoke_delegate_wrapper_ApplyTween_t256 ();
extern "C" void pinvoke_delegate_wrapper_QueryResultResolver_t328 ();
extern "C" void pinvoke_delegate_wrapper_ListenRaffle_t331 ();
extern "C" void pinvoke_delegate_wrapper_ListenRaffleResult_t332 ();
extern "C" void pinvoke_delegate_wrapper_RequestHandler_t200 ();
extern "C" void pinvoke_delegate_wrapper_Handle_t221 ();
extern "C" void pinvoke_delegate_wrapper_OnValidateInput_t599 ();
extern "C" void pinvoke_delegate_wrapper_StateChanged_t814 ();
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t769 ();
extern "C" void pinvoke_delegate_wrapper_LogCallback_t840 ();
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t842 ();
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t844 ();
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t872 ();
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t874 ();
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t875 ();
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t893 ();
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t732 ();
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t456 ();
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t914 ();
extern "C" void pinvoke_delegate_wrapper_UnityAdsDelegate_t858 ();
extern "C" void pinvoke_delegate_wrapper_UnityAction_t575 ();
extern "C" void pinvoke_delegate_wrapper_Action_t16 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1451 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t1548 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback_t1524 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback2_t1525 ();
extern "C" void pinvoke_delegate_wrapper_CertificateSelectionCallback_t1509 ();
extern "C" void pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1510 ();
extern "C" void pinvoke_delegate_wrapper_MatchAppendEvaluator_t1679 ();
extern "C" void pinvoke_delegate_wrapper_CostDelegate_t1713 ();
extern "C" void pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t1585 ();
extern "C" void pinvoke_delegate_wrapper_MatchEvaluator_t1747 ();
extern "C" void pinvoke_delegate_wrapper_Swapper_t1770 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCallback_t31 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1834 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1842 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t1932 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t1933 ();
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t2008 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t2022 ();
extern "C" void pinvoke_delegate_wrapper_RenewalDelegate_t2086 ();
extern "C" void pinvoke_delegate_wrapper_CallbackHandler_t2190 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t2380 ();
extern "C" void pinvoke_delegate_wrapper_MemberFilter_t1773 ();
extern "C" void pinvoke_delegate_wrapper_TypeFilter_t2015 ();
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t2381 ();
extern "C" void pinvoke_delegate_wrapper_HeaderHandler_t2382 ();
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t428 ();
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t2303 ();
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t2384 ();
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t2313 ();
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t2309 ();
extern "C" void pinvoke_delegate_wrapper_EventHandler_t2311 ();
extern "C" void pinvoke_delegate_wrapper_ResolveEventHandler_t2310 ();
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t1016 ();
extern const methodPointerType g_DelegateWrappersManagedToNative[62] = 
{
	pinvoke_delegate_wrapper_FsmActionRoutine_t27,
	pinvoke_delegate_wrapper_StateActionProcessor_t50,
	pinvoke_delegate_wrapper_DoubleClickCallback_t53,
	pinvoke_delegate_wrapper_NotificationInitializer_t88,
	pinvoke_delegate_wrapper_SignalListener_t114,
	pinvoke_delegate_wrapper_NodeProcessor_t141,
	pinvoke_delegate_wrapper_EasingFunction_t255,
	pinvoke_delegate_wrapper_ApplyTween_t256,
	pinvoke_delegate_wrapper_QueryResultResolver_t328,
	pinvoke_delegate_wrapper_ListenRaffle_t331,
	pinvoke_delegate_wrapper_ListenRaffleResult_t332,
	pinvoke_delegate_wrapper_RequestHandler_t200,
	pinvoke_delegate_wrapper_Handle_t221,
	pinvoke_delegate_wrapper_OnValidateInput_t599,
	pinvoke_delegate_wrapper_StateChanged_t814,
	pinvoke_delegate_wrapper_ReapplyDrivenProperties_t769,
	pinvoke_delegate_wrapper_LogCallback_t840,
	pinvoke_delegate_wrapper_CameraCallback_t842,
	pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t844,
	pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t872,
	pinvoke_delegate_wrapper_PCMReaderCallback_t874,
	pinvoke_delegate_wrapper_PCMSetPositionCallback_t875,
	pinvoke_delegate_wrapper_FontTextureRebuildCallback_t893,
	pinvoke_delegate_wrapper_WillRenderCanvases_t732,
	pinvoke_delegate_wrapper_WindowFunction_t456,
	pinvoke_delegate_wrapper_SkinChangedDelegate_t914,
	pinvoke_delegate_wrapper_UnityAdsDelegate_t858,
	pinvoke_delegate_wrapper_UnityAction_t575,
	pinvoke_delegate_wrapper_Action_t16,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1451,
	pinvoke_delegate_wrapper_PrimalityTest_t1548,
	pinvoke_delegate_wrapper_CertificateValidationCallback_t1524,
	pinvoke_delegate_wrapper_CertificateValidationCallback2_t1525,
	pinvoke_delegate_wrapper_CertificateSelectionCallback_t1509,
	pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1510,
	pinvoke_delegate_wrapper_MatchAppendEvaluator_t1679,
	pinvoke_delegate_wrapper_CostDelegate_t1713,
	pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t1585,
	pinvoke_delegate_wrapper_MatchEvaluator_t1747,
	pinvoke_delegate_wrapper_Swapper_t1770,
	pinvoke_delegate_wrapper_AsyncCallback_t31,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1834,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1842,
	pinvoke_delegate_wrapper_ReadDelegate_t1932,
	pinvoke_delegate_wrapper_WriteDelegate_t1933,
	pinvoke_delegate_wrapper_AddEventAdapter_t2008,
	pinvoke_delegate_wrapper_GetterAdapter_t2022,
	pinvoke_delegate_wrapper_RenewalDelegate_t2086,
	pinvoke_delegate_wrapper_CallbackHandler_t2190,
	pinvoke_delegate_wrapper_PrimalityTest_t2380,
	pinvoke_delegate_wrapper_MemberFilter_t1773,
	pinvoke_delegate_wrapper_TypeFilter_t2015,
	pinvoke_delegate_wrapper_CrossContextDelegate_t2381,
	pinvoke_delegate_wrapper_HeaderHandler_t2382,
	pinvoke_delegate_wrapper_ThreadStart_t428,
	pinvoke_delegate_wrapper_TimerCallback_t2303,
	pinvoke_delegate_wrapper_WaitCallback_t2384,
	pinvoke_delegate_wrapper_AppDomainInitializer_t2313,
	pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t2309,
	pinvoke_delegate_wrapper_EventHandler_t2311,
	pinvoke_delegate_wrapper_ResolveEventHandler_t2310,
	pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t1016,
};
