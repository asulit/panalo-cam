﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PropBehaviour
struct PropBehaviour_t279;

// System.Void Vuforia.PropBehaviour::.ctor()
extern "C" void PropBehaviour__ctor_m1256 (PropBehaviour_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
