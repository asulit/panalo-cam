﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,ESubScreens>
struct Transform_1_t2696;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,ESubScreens>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m17466_gshared (Transform_1_t2696 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m17466(__this, ___object, ___method, method) (( void (*) (Transform_1_t2696 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m17466_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,ESubScreens>::Invoke(TKey,TValue)
extern "C" int32_t Transform_1_Invoke_m17467_gshared (Transform_1_t2696 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Transform_1_Invoke_m17467(__this, ___key, ___value, method) (( int32_t (*) (Transform_1_t2696 *, int32_t, Object_t *, const MethodInfo*))Transform_1_Invoke_m17467_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,ESubScreens>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m17468_gshared (Transform_1_t2696 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m17468(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2696 *, int32_t, Object_t *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m17468_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ESubScreens,System.Object,ESubScreens>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Transform_1_EndInvoke_m17469_gshared (Transform_1_t2696 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m17469(__this, ___result, method) (( int32_t (*) (Transform_1_t2696 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m17469_gshared)(__this, ___result, method)
