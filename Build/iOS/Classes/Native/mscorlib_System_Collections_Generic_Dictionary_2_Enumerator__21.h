﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>
struct Dictionary_2_t231;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"
// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,UnityEngine.UI.Image>
struct  Enumerator_t2769 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,UnityEngine.UI.Image>::dictionary
	Dictionary_2_t231 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,UnityEngine.UI.Image>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,UnityEngine.UI.Image>::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.InputField,UnityEngine.UI.Image>::current
	KeyValuePair_2_t2766  ___current_3;
};
