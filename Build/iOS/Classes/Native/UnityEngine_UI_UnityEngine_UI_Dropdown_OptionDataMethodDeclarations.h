﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t558;
// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t553;

// System.Void UnityEngine.UI.Dropdown/OptionData::.ctor()
extern "C" void OptionData__ctor_m2300 (OptionData_t558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/OptionData::.ctor(System.String)
extern "C" void OptionData__ctor_m2301 (OptionData_t558 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/OptionData::.ctor(UnityEngine.Sprite)
extern "C" void OptionData__ctor_m2302 (OptionData_t558 * __this, Sprite_t553 * ___image, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/OptionData::.ctor(System.String,UnityEngine.Sprite)
extern "C" void OptionData__ctor_m2303 (OptionData_t558 * __this, String_t* ___text, Sprite_t553 * ___image, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.Dropdown/OptionData::get_text()
extern "C" String_t* OptionData_get_text_m2304 (OptionData_t558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/OptionData::set_text(System.String)
extern "C" void OptionData_set_text_m2305 (OptionData_t558 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.UI.Dropdown/OptionData::get_image()
extern "C" Sprite_t553 * OptionData_get_image_m2306 (OptionData_t558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/OptionData::set_image(UnityEngine.Sprite)
extern "C" void OptionData_set_image_m2307 (OptionData_t558 * __this, Sprite_t553 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
