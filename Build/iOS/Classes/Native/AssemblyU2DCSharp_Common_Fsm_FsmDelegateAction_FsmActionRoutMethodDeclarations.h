﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.FsmDelegateAction/FsmActionRoutine
struct FsmActionRoutine_t27;
// System.Object
struct Object_t;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Common.Fsm.FsmDelegateAction/FsmActionRoutine::.ctor(System.Object,System.IntPtr)
extern "C" void FsmActionRoutine__ctor_m97 (FsmActionRoutine_t27 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.FsmDelegateAction/FsmActionRoutine::Invoke(Common.Fsm.FsmState)
extern "C" void FsmActionRoutine_Invoke_m98 (FsmActionRoutine_t27 * __this, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_FsmActionRoutine_t27(Il2CppObject* delegate, Object_t * ___owner);
// System.IAsyncResult Common.Fsm.FsmDelegateAction/FsmActionRoutine::BeginInvoke(Common.Fsm.FsmState,System.AsyncCallback,System.Object)
extern "C" Object_t * FsmActionRoutine_BeginInvoke_m99 (FsmActionRoutine_t27 * __this, Object_t * ___owner, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.FsmDelegateAction/FsmActionRoutine::EndInvoke(System.IAsyncResult)
extern "C" void FsmActionRoutine_EndInvoke_m100 (FsmActionRoutine_t27 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
