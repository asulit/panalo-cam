﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Type[]
struct TypeU5BU5D_t1005;
// System.Object
#include "mscorlib_System_Object.h"
// System.Convert
struct  Convert_t373  : public Object_t
{
};
struct Convert_t373_StaticFields{
	// System.Object System.Convert::DBNull
	Object_t * ___DBNull_0;
	// System.Type[] System.Convert::conversionTable
	TypeU5BU5D_t1005* ___conversionTable_1;
};
