﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1372;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2710;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.ICollection
struct ICollection_t1751;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>[]
struct KeyValuePair_2U5BU5D_t3786;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>
struct IEnumerator_1_t3787;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1606;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct KeyCollection_t3443;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct ValueCollection_t3447;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_46.h"
// Vuforia.VuforiaManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__46.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor()
extern "C" void Dictionary_2__ctor_m7432_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m7432(__this, method) (( void (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2__ctor_m7432_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m28169_gshared (Dictionary_2_t1372 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m28169(__this, ___comparer, method) (( void (*) (Dictionary_2_t1372 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m28169_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m28170_gshared (Dictionary_2_t1372 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m28170(__this, ___capacity, method) (( void (*) (Dictionary_2_t1372 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m28170_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m28171_gshared (Dictionary_2_t1372 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m28171(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1372 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m28171_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m28172_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m28172(__this, method) (( Object_t * (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m28172_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m28173_gshared (Dictionary_2_t1372 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m28173(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1372 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m28173_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m28174_gshared (Dictionary_2_t1372 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m28174(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1372 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m28174_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m28175_gshared (Dictionary_2_t1372 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m28175(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1372 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m28175_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m28176_gshared (Dictionary_2_t1372 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m28176(__this, ___key, method) (( bool (*) (Dictionary_2_t1372 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m28176_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m28177_gshared (Dictionary_2_t1372 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m28177(__this, ___key, method) (( void (*) (Dictionary_2_t1372 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m28177_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m28178_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m28178(__this, method) (( bool (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m28178_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m28179_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m28179(__this, method) (( Object_t * (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m28179_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m28180_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m28180(__this, method) (( bool (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m28180_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m28181_gshared (Dictionary_2_t1372 * __this, KeyValuePair_2_t3440  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m28181(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1372 *, KeyValuePair_2_t3440 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m28181_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m28182_gshared (Dictionary_2_t1372 * __this, KeyValuePair_2_t3440  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m28182(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1372 *, KeyValuePair_2_t3440 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m28182_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m28183_gshared (Dictionary_2_t1372 * __this, KeyValuePair_2U5BU5D_t3786* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m28183(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1372 *, KeyValuePair_2U5BU5D_t3786*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m28183_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m28184_gshared (Dictionary_2_t1372 * __this, KeyValuePair_2_t3440  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m28184(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1372 *, KeyValuePair_2_t3440 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m28184_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m28185_gshared (Dictionary_2_t1372 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m28185(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1372 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m28185_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m28186_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m28186(__this, method) (( Object_t * (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m28186_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m28187_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m28187(__this, method) (( Object_t* (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m28187_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m28188_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m28188(__this, method) (( Object_t * (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m28188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m28189_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m28189(__this, method) (( int32_t (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_get_Count_m28189_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Item(TKey)
extern "C" VirtualButtonData_t1135  Dictionary_2_get_Item_m28190_gshared (Dictionary_2_t1372 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m28190(__this, ___key, method) (( VirtualButtonData_t1135  (*) (Dictionary_2_t1372 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m28190_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m28191_gshared (Dictionary_2_t1372 * __this, int32_t ___key, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m28191(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1372 *, int32_t, VirtualButtonData_t1135 , const MethodInfo*))Dictionary_2_set_Item_m28191_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m28192_gshared (Dictionary_2_t1372 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m28192(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1372 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m28192_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m28193_gshared (Dictionary_2_t1372 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m28193(__this, ___size, method) (( void (*) (Dictionary_2_t1372 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m28193_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m28194_gshared (Dictionary_2_t1372 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m28194(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1372 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m28194_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3440  Dictionary_2_make_pair_m28195_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m28195(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3440  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t1135 , const MethodInfo*))Dictionary_2_make_pair_m28195_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m28196_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m28196(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t1135 , const MethodInfo*))Dictionary_2_pick_key_m28196_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::pick_value(TKey,TValue)
extern "C" VirtualButtonData_t1135  Dictionary_2_pick_value_m28197_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m28197(__this /* static, unused */, ___key, ___value, method) (( VirtualButtonData_t1135  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t1135 , const MethodInfo*))Dictionary_2_pick_value_m28197_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m28198_gshared (Dictionary_2_t1372 * __this, KeyValuePair_2U5BU5D_t3786* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m28198(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1372 *, KeyValuePair_2U5BU5D_t3786*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m28198_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Resize()
extern "C" void Dictionary_2_Resize_m28199_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m28199(__this, method) (( void (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_Resize_m28199_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m28200_gshared (Dictionary_2_t1372 * __this, int32_t ___key, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m28200(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1372 *, int32_t, VirtualButtonData_t1135 , const MethodInfo*))Dictionary_2_Add_m28200_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Clear()
extern "C" void Dictionary_2_Clear_m28201_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m28201(__this, method) (( void (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_Clear_m28201_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m28202_gshared (Dictionary_2_t1372 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m28202(__this, ___key, method) (( bool (*) (Dictionary_2_t1372 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m28202_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m28203_gshared (Dictionary_2_t1372 * __this, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m28203(__this, ___value, method) (( bool (*) (Dictionary_2_t1372 *, VirtualButtonData_t1135 , const MethodInfo*))Dictionary_2_ContainsValue_m28203_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m28204_gshared (Dictionary_2_t1372 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m28204(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1372 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m28204_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m28205_gshared (Dictionary_2_t1372 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m28205(__this, ___sender, method) (( void (*) (Dictionary_2_t1372 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m28205_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m28206_gshared (Dictionary_2_t1372 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m28206(__this, ___key, method) (( bool (*) (Dictionary_2_t1372 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m28206_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m28207_gshared (Dictionary_2_t1372 * __this, int32_t ___key, VirtualButtonData_t1135 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m28207(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1372 *, int32_t, VirtualButtonData_t1135 *, const MethodInfo*))Dictionary_2_TryGetValue_m28207_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Keys()
extern "C" KeyCollection_t3443 * Dictionary_2_get_Keys_m28208_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m28208(__this, method) (( KeyCollection_t3443 * (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_get_Keys_m28208_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Values()
extern "C" ValueCollection_t3447 * Dictionary_2_get_Values_m28209_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m28209(__this, method) (( ValueCollection_t3447 * (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_get_Values_m28209_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m28210_gshared (Dictionary_2_t1372 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m28210(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1372 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m28210_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToTValue(System.Object)
extern "C" VirtualButtonData_t1135  Dictionary_2_ToTValue_m28211_gshared (Dictionary_2_t1372 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m28211(__this, ___value, method) (( VirtualButtonData_t1135  (*) (Dictionary_2_t1372 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m28211_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m28212_gshared (Dictionary_2_t1372 * __this, KeyValuePair_2_t3440  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m28212(__this, ___pair, method) (( bool (*) (Dictionary_2_t1372 *, KeyValuePair_2_t3440 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m28212_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::GetEnumerator()
extern "C" Enumerator_t3445  Dictionary_2_GetEnumerator_m28213_gshared (Dictionary_2_t1372 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m28213(__this, method) (( Enumerator_t3445  (*) (Dictionary_2_t1372 *, const MethodInfo*))Dictionary_2_GetEnumerator_m28213_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m28214_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m28214(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t1135 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m28214_gshared)(__this /* static, unused */, ___key, ___value, method)
