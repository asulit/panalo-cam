﻿#pragma once
#include <stdint.h>
// UnityEngine.TextMesh
struct TextMesh_t26;
// FrameRate
struct FrameRate_t24;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// FrameRateView
struct  FrameRateView_t25  : public MonoBehaviour_t5
{
	// UnityEngine.TextMesh FrameRateView::text
	TextMesh_t26 * ___text_2;
	// FrameRate FrameRateView::frameRate
	FrameRate_t24 * ___frameRate_3;
};
