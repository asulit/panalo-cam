﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2869;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_30.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m19831_gshared (Enumerator_t2874 * __this, Dictionary_2_t2869 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m19831(__this, ___host, method) (( void (*) (Enumerator_t2874 *, Dictionary_2_t2869 *, const MethodInfo*))Enumerator__ctor_m19831_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m19832_gshared (Enumerator_t2874 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m19832(__this, method) (( Object_t * (*) (Enumerator_t2874 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19832_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m19833_gshared (Enumerator_t2874 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m19833(__this, method) (( void (*) (Enumerator_t2874 *, const MethodInfo*))Enumerator_Dispose_m19833_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m19834_gshared (Enumerator_t2874 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m19834(__this, method) (( bool (*) (Enumerator_t2874 *, const MethodInfo*))Enumerator_MoveNext_m19834_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m19835_gshared (Enumerator_t2874 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m19835(__this, method) (( int32_t (*) (Enumerator_t2874 *, const MethodInfo*))Enumerator_get_Current_m19835_gshared)(__this, method)
