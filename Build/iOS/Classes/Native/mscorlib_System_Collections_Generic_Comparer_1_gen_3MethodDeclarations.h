﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>
struct Comparer_1_t2946;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Comparer_1__ctor_m20868_gshared (Comparer_1_t2946 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m20868(__this, method) (( void (*) (Comparer_1_t2946 *, const MethodInfo*))Comparer_1__ctor_m20868_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.cctor()
extern "C" void Comparer_1__cctor_m20869_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m20869(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m20869_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m20870_gshared (Comparer_1_t2946 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m20870(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2946 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m20870_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::get_Default()
extern "C" Comparer_1_t2946 * Comparer_1_get_Default_m20871_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m20871(__this /* static, unused */, method) (( Comparer_1_t2946 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m20871_gshared)(__this /* static, unused */, method)
