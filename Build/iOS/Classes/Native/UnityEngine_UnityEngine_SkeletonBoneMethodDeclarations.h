﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void SkeletonBone_t887_marshal(const SkeletonBone_t887& unmarshaled, SkeletonBone_t887_marshaled& marshaled);
extern "C" void SkeletonBone_t887_marshal_back(const SkeletonBone_t887_marshaled& marshaled, SkeletonBone_t887& unmarshaled);
extern "C" void SkeletonBone_t887_marshal_cleanup(SkeletonBone_t887_marshaled& marshaled);
