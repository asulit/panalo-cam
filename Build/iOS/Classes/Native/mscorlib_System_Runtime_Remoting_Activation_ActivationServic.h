﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t2060;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Activation.ActivationServices
struct  ActivationServices_t2059  : public Object_t
{
};
struct ActivationServices_t2059_StaticFields{
	// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ActivationServices::_constructionActivator
	Object_t * ____constructionActivator_0;
};
