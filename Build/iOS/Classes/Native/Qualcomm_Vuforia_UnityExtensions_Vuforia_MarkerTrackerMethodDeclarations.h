﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerTracker
struct MarkerTracker_t1121;

// System.Void Vuforia.MarkerTracker::.ctor()
extern "C" void MarkerTracker__ctor_m5702 (MarkerTracker_t1121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
