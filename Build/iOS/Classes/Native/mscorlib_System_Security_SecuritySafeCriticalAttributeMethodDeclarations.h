﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SecuritySafeCriticalAttribute
struct SecuritySafeCriticalAttribute_t2260;

// System.Void System.Security.SecuritySafeCriticalAttribute::.ctor()
extern "C" void SecuritySafeCriticalAttribute__ctor_m13381 (SecuritySafeCriticalAttribute_t2260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
