﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Logger.LogLevel
struct LogLevel_t73;
// System.String
struct String_t;

// System.Void Common.Logger.LogLevel::.ctor(System.String)
extern "C" void LogLevel__ctor_m253 (LogLevel_t73 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Logger.LogLevel::.cctor()
extern "C" void LogLevel__cctor_m254 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Logger.LogLevel::get_Name()
extern "C" String_t* LogLevel_get_Name_m255 (LogLevel_t73 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
