﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m18644(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2794 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14611_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Material>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18645(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2794 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14612_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::Dispose()
#define InternalEnumerator_1_Dispose_m18646(__this, method) (( void (*) (InternalEnumerator_1_t2794 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14613_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Material>::MoveNext()
#define InternalEnumerator_1_MoveNext_m18647(__this, method) (( bool (*) (InternalEnumerator_1_t2794 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14614_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Material>::get_Current()
#define InternalEnumerator_1_get_Current_m18648(__this, method) (( Material_t82 * (*) (InternalEnumerator_1_t2794 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14615_gshared)(__this, method)
