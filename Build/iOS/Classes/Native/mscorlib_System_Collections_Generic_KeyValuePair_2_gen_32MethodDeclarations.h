﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m24582(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3227 *, Camera_t6 *, VideoBackgroundAbstractBehaviour_t315 *, const MethodInfo*))KeyValuePair_2__ctor_m15105_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m24583(__this, method) (( Camera_t6 * (*) (KeyValuePair_2_t3227 *, const MethodInfo*))KeyValuePair_2_get_Key_m15106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m24584(__this, ___value, method) (( void (*) (KeyValuePair_2_t3227 *, Camera_t6 *, const MethodInfo*))KeyValuePair_2_set_Key_m15107_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m24585(__this, method) (( VideoBackgroundAbstractBehaviour_t315 * (*) (KeyValuePair_2_t3227 *, const MethodInfo*))KeyValuePair_2_get_Value_m15108_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m24586(__this, ___value, method) (( void (*) (KeyValuePair_2_t3227 *, VideoBackgroundAbstractBehaviour_t315 *, const MethodInfo*))KeyValuePair_2_set_Value_m15109_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m24587(__this, method) (( String_t* (*) (KeyValuePair_2_t3227 *, const MethodInfo*))KeyValuePair_2_ToString_m15110_gshared)(__this, method)
