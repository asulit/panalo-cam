﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.EnumeratableActionThread
struct EnumeratableActionThread_t173;
// System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>
struct Func_2_t174;
// System.Collections.IEnumerator
struct IEnumerator_t337;

// System.Void UnityThreading.EnumeratableActionThread::.ctor(System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>)
extern "C" void EnumeratableActionThread__ctor_m598 (EnumeratableActionThread_t173 * __this, Func_2_t174 * ___enumeratableAction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.EnumeratableActionThread::.ctor(System.Func`2<UnityThreading.ThreadBase,System.Collections.IEnumerator>,System.Boolean)
extern "C" void EnumeratableActionThread__ctor_m599 (EnumeratableActionThread_t173 * __this, Func_2_t174 * ___enumeratableAction, bool ___autoStartThread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityThreading.EnumeratableActionThread::Do()
extern "C" Object_t * EnumeratableActionThread_Do_m600 (EnumeratableActionThread_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
