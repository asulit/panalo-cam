﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>
struct Collection_1_t3031;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t254;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t3711;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t3030;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::.ctor()
extern "C" void Collection_1__ctor_m22201_gshared (Collection_1_t3031 * __this, const MethodInfo* method);
#define Collection_1__ctor_m22201(__this, method) (( void (*) (Collection_1_t3031 *, const MethodInfo*))Collection_1__ctor_m22201_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22202_gshared (Collection_1_t3031 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22202(__this, method) (( bool (*) (Collection_1_t3031 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22202_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m22203_gshared (Collection_1_t3031 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m22203(__this, ___array, ___index, method) (( void (*) (Collection_1_t3031 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m22203_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m22204_gshared (Collection_1_t3031 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m22204(__this, method) (( Object_t * (*) (Collection_1_t3031 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m22204_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m22205_gshared (Collection_1_t3031 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m22205(__this, ___value, method) (( int32_t (*) (Collection_1_t3031 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m22205_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m22206_gshared (Collection_1_t3031 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m22206(__this, ___value, method) (( bool (*) (Collection_1_t3031 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m22206_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m22207_gshared (Collection_1_t3031 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m22207(__this, ___value, method) (( int32_t (*) (Collection_1_t3031 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m22207_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m22208_gshared (Collection_1_t3031 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m22208(__this, ___index, ___value, method) (( void (*) (Collection_1_t3031 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m22208_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m22209_gshared (Collection_1_t3031 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m22209(__this, ___value, method) (( void (*) (Collection_1_t3031 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m22209_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m22210_gshared (Collection_1_t3031 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m22210(__this, method) (( bool (*) (Collection_1_t3031 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m22210_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m22211_gshared (Collection_1_t3031 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m22211(__this, method) (( Object_t * (*) (Collection_1_t3031 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m22211_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m22212_gshared (Collection_1_t3031 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m22212(__this, method) (( bool (*) (Collection_1_t3031 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m22212_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m22213_gshared (Collection_1_t3031 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m22213(__this, method) (( bool (*) (Collection_1_t3031 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m22213_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m22214_gshared (Collection_1_t3031 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m22214(__this, ___index, method) (( Object_t * (*) (Collection_1_t3031 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m22214_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m22215_gshared (Collection_1_t3031 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m22215(__this, ___index, ___value, method) (( void (*) (Collection_1_t3031 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m22215_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Add(T)
extern "C" void Collection_1_Add_m22216_gshared (Collection_1_t3031 * __this, Vector3_t36  ___item, const MethodInfo* method);
#define Collection_1_Add_m22216(__this, ___item, method) (( void (*) (Collection_1_t3031 *, Vector3_t36 , const MethodInfo*))Collection_1_Add_m22216_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Clear()
extern "C" void Collection_1_Clear_m22217_gshared (Collection_1_t3031 * __this, const MethodInfo* method);
#define Collection_1_Clear_m22217(__this, method) (( void (*) (Collection_1_t3031 *, const MethodInfo*))Collection_1_Clear_m22217_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ClearItems()
extern "C" void Collection_1_ClearItems_m22218_gshared (Collection_1_t3031 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m22218(__this, method) (( void (*) (Collection_1_t3031 *, const MethodInfo*))Collection_1_ClearItems_m22218_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool Collection_1_Contains_m22219_gshared (Collection_1_t3031 * __this, Vector3_t36  ___item, const MethodInfo* method);
#define Collection_1_Contains_m22219(__this, ___item, method) (( bool (*) (Collection_1_t3031 *, Vector3_t36 , const MethodInfo*))Collection_1_Contains_m22219_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m22220_gshared (Collection_1_t3031 * __this, Vector3U5BU5D_t254* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m22220(__this, ___array, ___index, method) (( void (*) (Collection_1_t3031 *, Vector3U5BU5D_t254*, int32_t, const MethodInfo*))Collection_1_CopyTo_m22220_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m22221_gshared (Collection_1_t3031 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m22221(__this, method) (( Object_t* (*) (Collection_1_t3031 *, const MethodInfo*))Collection_1_GetEnumerator_m22221_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m22222_gshared (Collection_1_t3031 * __this, Vector3_t36  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m22222(__this, ___item, method) (( int32_t (*) (Collection_1_t3031 *, Vector3_t36 , const MethodInfo*))Collection_1_IndexOf_m22222_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m22223_gshared (Collection_1_t3031 * __this, int32_t ___index, Vector3_t36  ___item, const MethodInfo* method);
#define Collection_1_Insert_m22223(__this, ___index, ___item, method) (( void (*) (Collection_1_t3031 *, int32_t, Vector3_t36 , const MethodInfo*))Collection_1_Insert_m22223_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m22224_gshared (Collection_1_t3031 * __this, int32_t ___index, Vector3_t36  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m22224(__this, ___index, ___item, method) (( void (*) (Collection_1_t3031 *, int32_t, Vector3_t36 , const MethodInfo*))Collection_1_InsertItem_m22224_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool Collection_1_Remove_m22225_gshared (Collection_1_t3031 * __this, Vector3_t36  ___item, const MethodInfo* method);
#define Collection_1_Remove_m22225(__this, ___item, method) (( bool (*) (Collection_1_t3031 *, Vector3_t36 , const MethodInfo*))Collection_1_Remove_m22225_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m22226_gshared (Collection_1_t3031 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m22226(__this, ___index, method) (( void (*) (Collection_1_t3031 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m22226_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m22227_gshared (Collection_1_t3031 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m22227(__this, ___index, method) (( void (*) (Collection_1_t3031 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m22227_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t Collection_1_get_Count_m22228_gshared (Collection_1_t3031 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m22228(__this, method) (( int32_t (*) (Collection_1_t3031 *, const MethodInfo*))Collection_1_get_Count_m22228_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t36  Collection_1_get_Item_m22229_gshared (Collection_1_t3031 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m22229(__this, ___index, method) (( Vector3_t36  (*) (Collection_1_t3031 *, int32_t, const MethodInfo*))Collection_1_get_Item_m22229_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m22230_gshared (Collection_1_t3031 * __this, int32_t ___index, Vector3_t36  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m22230(__this, ___index, ___value, method) (( void (*) (Collection_1_t3031 *, int32_t, Vector3_t36 , const MethodInfo*))Collection_1_set_Item_m22230_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m22231_gshared (Collection_1_t3031 * __this, int32_t ___index, Vector3_t36  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m22231(__this, ___index, ___item, method) (( void (*) (Collection_1_t3031 *, int32_t, Vector3_t36 , const MethodInfo*))Collection_1_SetItem_m22231_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m22232_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m22232(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m22232_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ConvertItem(System.Object)
extern "C" Vector3_t36  Collection_1_ConvertItem_m22233_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m22233(__this /* static, unused */, ___item, method) (( Vector3_t36  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m22233_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m22234_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m22234(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m22234_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m22235_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m22235(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m22235_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m22236_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m22236(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m22236_gshared)(__this /* static, unused */, ___list, method)
