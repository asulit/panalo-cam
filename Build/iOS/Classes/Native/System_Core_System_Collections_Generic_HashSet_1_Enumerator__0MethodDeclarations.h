﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3524;
// System.Object
struct Object_t;
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C" void Enumerator__ctor_m29396_gshared (Enumerator_t3528 * __this, HashSet_1_t3524 * ___hashset, const MethodInfo* method);
#define Enumerator__ctor_m29396(__this, ___hashset, method) (( void (*) (Enumerator_t3528 *, HashSet_1_t3524 *, const MethodInfo*))Enumerator__ctor_m29396_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m29397_gshared (Enumerator_t3528 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m29397(__this, method) (( Object_t * (*) (Enumerator_t3528 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m29397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m29398_gshared (Enumerator_t3528 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m29398(__this, method) (( bool (*) (Enumerator_t3528 *, const MethodInfo*))Enumerator_MoveNext_m29398_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m29399_gshared (Enumerator_t3528 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m29399(__this, method) (( Object_t * (*) (Enumerator_t3528 *, const MethodInfo*))Enumerator_get_Current_m29399_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m29400_gshared (Enumerator_t3528 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m29400(__this, method) (( void (*) (Enumerator_t3528 *, const MethodInfo*))Enumerator_Dispose_m29400_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
extern "C" void Enumerator_CheckState_m29401_gshared (Enumerator_t3528 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m29401(__this, method) (( void (*) (Enumerator_t3528 *, const MethodInfo*))Enumerator_CheckState_m29401_gshared)(__this, method)
