﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30MethodDeclarations.h"
#define Enumerator__ctor_m28394(__this, ___l, method) (( void (*) (Enumerator_t1375 *, List_1_t1373 *, const MethodInfo*))Enumerator__ctor_m14718_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m28395(__this, method) (( Object_t * (*) (Enumerator_t1375 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14719_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m7441(__this, method) (( void (*) (Enumerator_t1375 *, const MethodInfo*))Enumerator_Dispose_m14720_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m28396(__this, method) (( void (*) (Enumerator_t1375 *, const MethodInfo*))Enumerator_VerifyState_m14721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m7440(__this, method) (( bool (*) (Enumerator_t1375 *, const MethodInfo*))Enumerator_MoveNext_m14722_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m7439(__this, method) (( VirtualButtonAbstractBehaviour_t319 * (*) (Enumerator_t1375 *, const MethodInfo*))Enumerator_get_Current_m14723_gshared)(__this, method)
