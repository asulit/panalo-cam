﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.TimedKill
struct TimedKill_t123;
// System.Collections.IEnumerator
struct IEnumerator_t337;

// System.Void Common.TimedKill::.ctor()
extern "C" void TimedKill__ctor_m428 (TimedKill_t123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.TimedKill::Awake()
extern "C" void TimedKill_Awake_m429 (TimedKill_t123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.TimedKill::Run(System.Single)
extern "C" void TimedKill_Run_m430 (TimedKill_t123 * __this, float ___duration, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Common.TimedKill::KillAfterDuration(System.Single)
extern "C" Object_t * TimedKill_KillAfterDuration_m431 (TimedKill_t123 * __this, float ___duration, const MethodInfo* method) IL2CPP_METHOD_ATTR;
