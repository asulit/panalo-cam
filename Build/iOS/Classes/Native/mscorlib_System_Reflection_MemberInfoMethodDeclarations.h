﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Module
struct Module_t1970;

// System.Void System.Reflection.MemberInfo::.ctor()
extern "C" void MemberInfo__ctor_m10348 (MemberInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.MemberInfo::get_Module()
extern "C" Module_t1970 * MemberInfo_get_Module_m10349 (MemberInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
