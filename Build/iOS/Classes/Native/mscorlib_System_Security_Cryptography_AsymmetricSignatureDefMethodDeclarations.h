﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.AsymmetricSignatureDeformatter
struct AsymmetricSignatureDeformatter_t1516;

// System.Void System.Security.Cryptography.AsymmetricSignatureDeformatter::.ctor()
extern "C" void AsymmetricSignatureDeformatter__ctor_m8552 (AsymmetricSignatureDeformatter_t1516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
