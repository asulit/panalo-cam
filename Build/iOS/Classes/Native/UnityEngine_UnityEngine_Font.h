﻿#pragma once
#include <stdint.h>
// System.Action`1<UnityEngine.Font>
struct Action_1_t739;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t893;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Font
struct  Font_t569  : public Object_t335
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t893 * ___m_FontTextureRebuildCallback_3;
};
struct Font_t569_StaticFields{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t739 * ___textureRebuilt_2;
};
