﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0
struct U3CGetFlagValuesU3Ec__Iterator0_t20;
// System.Enum
struct Enum_t21;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Enum>
struct IEnumerator_1_t22;

// System.Void Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::.ctor()
extern "C" void U3CGetFlagValuesU3Ec__Iterator0__ctor_m79 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Enum Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::System.Collections.Generic.IEnumerator<System.Enum>.get_Current()
extern "C" Enum_t21 * U3CGetFlagValuesU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_EnumU3E_get_Current_m80 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetFlagValuesU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m81 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CGetFlagValuesU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m82 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Enum> Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::System.Collections.Generic.IEnumerable<System.Enum>.GetEnumerator()
extern "C" Object_t* U3CGetFlagValuesU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSystem_EnumU3E_GetEnumerator_m83 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::MoveNext()
extern "C" bool U3CGetFlagValuesU3Ec__Iterator0_MoveNext_m84 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::Dispose()
extern "C" void U3CGetFlagValuesU3Ec__Iterator0_Dispose_m85 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::Reset()
extern "C" void U3CGetFlagValuesU3Ec__Iterator0_Reset_m86 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
