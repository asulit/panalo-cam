﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DontDestroyOnLoadComponent
struct DontDestroyOnLoadComponent_t17;

// System.Void DontDestroyOnLoadComponent::.ctor()
extern "C" void DontDestroyOnLoadComponent__ctor_m77 (DontDestroyOnLoadComponent_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DontDestroyOnLoadComponent::Awake()
extern "C" void DontDestroyOnLoadComponent_Awake_m78 (DontDestroyOnLoadComponent_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
