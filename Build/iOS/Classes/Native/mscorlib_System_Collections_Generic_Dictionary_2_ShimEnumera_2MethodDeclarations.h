﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>
struct ShimEnumerator_t2701;
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
struct Dictionary_2_t2688;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m17501_gshared (ShimEnumerator_t2701 * __this, Dictionary_2_t2688 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m17501(__this, ___host, method) (( void (*) (ShimEnumerator_t2701 *, Dictionary_2_t2688 *, const MethodInfo*))ShimEnumerator__ctor_m17501_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17502_gshared (ShimEnumerator_t2701 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m17502(__this, method) (( bool (*) (ShimEnumerator_t2701 *, const MethodInfo*))ShimEnumerator_MoveNext_m17502_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m17503_gshared (ShimEnumerator_t2701 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m17503(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t2701 *, const MethodInfo*))ShimEnumerator_get_Entry_m17503_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17504_gshared (ShimEnumerator_t2701 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m17504(__this, method) (( Object_t * (*) (ShimEnumerator_t2701 *, const MethodInfo*))ShimEnumerator_get_Key_m17504_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17505_gshared (ShimEnumerator_t2701 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m17505(__this, method) (( Object_t * (*) (ShimEnumerator_t2701 *, const MethodInfo*))ShimEnumerator_get_Value_m17505_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ESubScreens,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m17506_gshared (ShimEnumerator_t2701 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m17506(__this, method) (( Object_t * (*) (ShimEnumerator_t2701 *, const MethodInfo*))ShimEnumerator_get_Current_m17506_gshared)(__this, method)
