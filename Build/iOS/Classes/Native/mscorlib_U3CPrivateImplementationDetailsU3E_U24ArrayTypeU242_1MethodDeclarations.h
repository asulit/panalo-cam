﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU242048_t2400_marshal(const U24ArrayTypeU242048_t2400& unmarshaled, U24ArrayTypeU242048_t2400_marshaled& marshaled);
extern "C" void U24ArrayTypeU242048_t2400_marshal_back(const U24ArrayTypeU242048_t2400_marshaled& marshaled, U24ArrayTypeU242048_t2400& unmarshaled);
extern "C" void U24ArrayTypeU242048_t2400_marshal_cleanup(U24ArrayTypeU242048_t2400_marshaled& marshaled);
