﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>
struct Collection_1_t3063;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t779;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t3720;
// System.Collections.Generic.IList`1<UnityEngine.Vector4>
struct IList_1_t3062;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::.ctor()
extern "C" void Collection_1__ctor_m22646_gshared (Collection_1_t3063 * __this, const MethodInfo* method);
#define Collection_1__ctor_m22646(__this, method) (( void (*) (Collection_1_t3063 *, const MethodInfo*))Collection_1__ctor_m22646_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22647_gshared (Collection_1_t3063 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22647(__this, method) (( bool (*) (Collection_1_t3063 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22647_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m22648_gshared (Collection_1_t3063 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m22648(__this, ___array, ___index, method) (( void (*) (Collection_1_t3063 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m22648_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m22649_gshared (Collection_1_t3063 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m22649(__this, method) (( Object_t * (*) (Collection_1_t3063 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m22649_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m22650_gshared (Collection_1_t3063 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m22650(__this, ___value, method) (( int32_t (*) (Collection_1_t3063 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m22650_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m22651_gshared (Collection_1_t3063 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m22651(__this, ___value, method) (( bool (*) (Collection_1_t3063 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m22651_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m22652_gshared (Collection_1_t3063 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m22652(__this, ___value, method) (( int32_t (*) (Collection_1_t3063 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m22652_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m22653_gshared (Collection_1_t3063 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m22653(__this, ___index, ___value, method) (( void (*) (Collection_1_t3063 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m22653_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m22654_gshared (Collection_1_t3063 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m22654(__this, ___value, method) (( void (*) (Collection_1_t3063 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m22654_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m22655_gshared (Collection_1_t3063 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m22655(__this, method) (( bool (*) (Collection_1_t3063 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m22655_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m22656_gshared (Collection_1_t3063 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m22656(__this, method) (( Object_t * (*) (Collection_1_t3063 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m22656_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m22657_gshared (Collection_1_t3063 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m22657(__this, method) (( bool (*) (Collection_1_t3063 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m22657_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m22658_gshared (Collection_1_t3063 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m22658(__this, method) (( bool (*) (Collection_1_t3063 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m22658_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m22659_gshared (Collection_1_t3063 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m22659(__this, ___index, method) (( Object_t * (*) (Collection_1_t3063 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m22659_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m22660_gshared (Collection_1_t3063 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m22660(__this, ___index, ___value, method) (( void (*) (Collection_1_t3063 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m22660_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Add(T)
extern "C" void Collection_1_Add_m22661_gshared (Collection_1_t3063 * __this, Vector4_t680  ___item, const MethodInfo* method);
#define Collection_1_Add_m22661(__this, ___item, method) (( void (*) (Collection_1_t3063 *, Vector4_t680 , const MethodInfo*))Collection_1_Add_m22661_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Clear()
extern "C" void Collection_1_Clear_m22662_gshared (Collection_1_t3063 * __this, const MethodInfo* method);
#define Collection_1_Clear_m22662(__this, method) (( void (*) (Collection_1_t3063 *, const MethodInfo*))Collection_1_Clear_m22662_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::ClearItems()
extern "C" void Collection_1_ClearItems_m22663_gshared (Collection_1_t3063 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m22663(__this, method) (( void (*) (Collection_1_t3063 *, const MethodInfo*))Collection_1_ClearItems_m22663_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Contains(T)
extern "C" bool Collection_1_Contains_m22664_gshared (Collection_1_t3063 * __this, Vector4_t680  ___item, const MethodInfo* method);
#define Collection_1_Contains_m22664(__this, ___item, method) (( bool (*) (Collection_1_t3063 *, Vector4_t680 , const MethodInfo*))Collection_1_Contains_m22664_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m22665_gshared (Collection_1_t3063 * __this, Vector4U5BU5D_t779* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m22665(__this, ___array, ___index, method) (( void (*) (Collection_1_t3063 *, Vector4U5BU5D_t779*, int32_t, const MethodInfo*))Collection_1_CopyTo_m22665_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m22666_gshared (Collection_1_t3063 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m22666(__this, method) (( Object_t* (*) (Collection_1_t3063 *, const MethodInfo*))Collection_1_GetEnumerator_m22666_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m22667_gshared (Collection_1_t3063 * __this, Vector4_t680  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m22667(__this, ___item, method) (( int32_t (*) (Collection_1_t3063 *, Vector4_t680 , const MethodInfo*))Collection_1_IndexOf_m22667_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m22668_gshared (Collection_1_t3063 * __this, int32_t ___index, Vector4_t680  ___item, const MethodInfo* method);
#define Collection_1_Insert_m22668(__this, ___index, ___item, method) (( void (*) (Collection_1_t3063 *, int32_t, Vector4_t680 , const MethodInfo*))Collection_1_Insert_m22668_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m22669_gshared (Collection_1_t3063 * __this, int32_t ___index, Vector4_t680  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m22669(__this, ___index, ___item, method) (( void (*) (Collection_1_t3063 *, int32_t, Vector4_t680 , const MethodInfo*))Collection_1_InsertItem_m22669_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Remove(T)
extern "C" bool Collection_1_Remove_m22670_gshared (Collection_1_t3063 * __this, Vector4_t680  ___item, const MethodInfo* method);
#define Collection_1_Remove_m22670(__this, ___item, method) (( bool (*) (Collection_1_t3063 *, Vector4_t680 , const MethodInfo*))Collection_1_Remove_m22670_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m22671_gshared (Collection_1_t3063 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m22671(__this, ___index, method) (( void (*) (Collection_1_t3063 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m22671_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m22672_gshared (Collection_1_t3063 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m22672(__this, ___index, method) (( void (*) (Collection_1_t3063 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m22672_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::get_Count()
extern "C" int32_t Collection_1_get_Count_m22673_gshared (Collection_1_t3063 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m22673(__this, method) (( int32_t (*) (Collection_1_t3063 *, const MethodInfo*))Collection_1_get_Count_m22673_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C" Vector4_t680  Collection_1_get_Item_m22674_gshared (Collection_1_t3063 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m22674(__this, ___index, method) (( Vector4_t680  (*) (Collection_1_t3063 *, int32_t, const MethodInfo*))Collection_1_get_Item_m22674_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m22675_gshared (Collection_1_t3063 * __this, int32_t ___index, Vector4_t680  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m22675(__this, ___index, ___value, method) (( void (*) (Collection_1_t3063 *, int32_t, Vector4_t680 , const MethodInfo*))Collection_1_set_Item_m22675_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m22676_gshared (Collection_1_t3063 * __this, int32_t ___index, Vector4_t680  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m22676(__this, ___index, ___item, method) (( void (*) (Collection_1_t3063 *, int32_t, Vector4_t680 , const MethodInfo*))Collection_1_SetItem_m22676_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m22677_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m22677(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m22677_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::ConvertItem(System.Object)
extern "C" Vector4_t680  Collection_1_ConvertItem_m22678_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m22678(__this /* static, unused */, ___item, method) (( Vector4_t680  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m22678_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m22679_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m22679(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m22679_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m22680_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m22680(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m22680_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m22681_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m22681(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m22681_gshared)(__this /* static, unused */, ___list, method)
