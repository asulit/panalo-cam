﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2432_t2391_marshal(const U24ArrayTypeU2432_t2391& unmarshaled, U24ArrayTypeU2432_t2391_marshaled& marshaled);
extern "C" void U24ArrayTypeU2432_t2391_marshal_back(const U24ArrayTypeU2432_t2391_marshaled& marshaled, U24ArrayTypeU2432_t2391& unmarshaled);
extern "C" void U24ArrayTypeU2432_t2391_marshal_cleanup(U24ArrayTypeU2432_t2391_marshaled& marshaled);
