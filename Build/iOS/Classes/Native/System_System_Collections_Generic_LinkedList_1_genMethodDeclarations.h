﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::.ctor()
// System.Collections.Generic.LinkedList`1<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_gen_2MethodDeclarations.h"
#define LinkedList_1__ctor_m1492(__this, method) (( void (*) (LinkedList_1_t90 *, const MethodInfo*))LinkedList_1__ctor_m15979_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m15980(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t90 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))LinkedList_1__ctor_m15981_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15982(__this, ___value, method) (( void (*) (LinkedList_1_t90 *, Object_t *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15983_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m15984(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t90 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m15985_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15986(__this, method) (( Object_t* (*) (LinkedList_1_t90 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15987_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m15988(__this, method) (( Object_t * (*) (LinkedList_1_t90 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m15989_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15990(__this, method) (( bool (*) (LinkedList_1_t90 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15991_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m15992(__this, method) (( bool (*) (LinkedList_1_t90 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m15993_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m15994(__this, method) (( Object_t * (*) (LinkedList_1_t90 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m15995_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m15996(__this, ___node, method) (( void (*) (LinkedList_1_t90 *, LinkedListNode_1_t406 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m15997_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::AddLast(T)
#define LinkedList_1_AddLast_m1493(__this, ___value, method) (( LinkedListNode_1_t406 * (*) (LinkedList_1_t90 *, Object_t *, const MethodInfo*))LinkedList_1_AddLast_m15998_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::Clear()
#define LinkedList_1_Clear_m15999(__this, method) (( void (*) (LinkedList_1_t90 *, const MethodInfo*))LinkedList_1_Clear_m16000_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::Contains(T)
#define LinkedList_1_Contains_m16001(__this, ___value, method) (( bool (*) (LinkedList_1_t90 *, Object_t *, const MethodInfo*))LinkedList_1_Contains_m16002_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m16003(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t90 *, NotificationHandlerU5BU5D_t3632*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m16004_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::Find(T)
#define LinkedList_1_Find_m16005(__this, ___value, method) (( LinkedListNode_1_t406 * (*) (LinkedList_1_t90 *, Object_t *, const MethodInfo*))LinkedList_1_Find_m16006_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m16007(__this, method) (( Enumerator_t2586  (*) (LinkedList_1_t90 *, const MethodInfo*))LinkedList_1_GetEnumerator_m16008_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m16009(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t90 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))LinkedList_1_GetObjectData_m16010_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m16011(__this, ___sender, method) (( void (*) (LinkedList_1_t90 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m16012_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::Remove(T)
#define LinkedList_1_Remove_m16013(__this, ___value, method) (( bool (*) (LinkedList_1_t90 *, Object_t *, const MethodInfo*))LinkedList_1_Remove_m16014_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m16015(__this, ___node, method) (( void (*) (LinkedList_1_t90 *, LinkedListNode_1_t406 *, const MethodInfo*))LinkedList_1_Remove_m16016_gshared)(__this, ___node, method)
// System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::RemoveLast()
#define LinkedList_1_RemoveLast_m16017(__this, method) (( void (*) (LinkedList_1_t90 *, const MethodInfo*))LinkedList_1_RemoveLast_m16018_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::get_Count()
#define LinkedList_1_get_Count_m16019(__this, method) (( int32_t (*) (LinkedList_1_t90 *, const MethodInfo*))LinkedList_1_get_Count_m16020_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::get_First()
#define LinkedList_1_get_First_m1494(__this, method) (( LinkedListNode_1_t406 * (*) (LinkedList_1_t90 *, const MethodInfo*))LinkedList_1_get_First_m16021_gshared)(__this, method)
