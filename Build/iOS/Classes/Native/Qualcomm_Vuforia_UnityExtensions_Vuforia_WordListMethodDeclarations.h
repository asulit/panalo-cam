﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordList
struct WordList_t1173;

// System.Void Vuforia.WordList::.ctor()
extern "C" void WordList__ctor_m5894 (WordList_t1173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
