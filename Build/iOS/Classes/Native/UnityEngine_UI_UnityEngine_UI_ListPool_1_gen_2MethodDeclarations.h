﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t676;

// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::.cctor()
extern "C" void ListPool_1__cctor_m22861_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m22861(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m22861_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Get()
extern "C" List_1_t676 * ListPool_1_Get_m3780_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m3780(__this /* static, unused */, method) (( List_1_t676 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m3780_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m3803_gshared (Object_t * __this /* static, unused */, List_1_t676 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m3803(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t676 *, const MethodInfo*))ListPool_1_Release_m3803_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::<s_ListPool>m__15(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__15_m22862_gshared (Object_t * __this /* static, unused */, List_1_t676 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__15_m22862(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t676 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__15_m22862_gshared)(__this /* static, unused */, ___l, method)
