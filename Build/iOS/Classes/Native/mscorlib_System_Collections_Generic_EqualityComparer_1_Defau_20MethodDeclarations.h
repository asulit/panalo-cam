﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>
struct DefaultComparer_t3465;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void DefaultComparer__ctor_m28536_gshared (DefaultComparer_t3465 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m28536(__this, method) (( void (*) (DefaultComparer_t3465 *, const MethodInfo*))DefaultComparer__ctor_m28536_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m28537_gshared (DefaultComparer_t3465 * __this, TargetSearchResult_t1212  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m28537(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3465 *, TargetSearchResult_t1212 , const MethodInfo*))DefaultComparer_GetHashCode_m28537_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m28538_gshared (DefaultComparer_t3465 * __this, TargetSearchResult_t1212  ___x, TargetSearchResult_t1212  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m28538(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3465 *, TargetSearchResult_t1212 , TargetSearchResult_t1212 , const MethodInfo*))DefaultComparer_Equals_m28538_gshared)(__this, ___x, ___y, method)
