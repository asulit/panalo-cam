﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<OrthographicCameraObserver>
struct IList_1_t7;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<OrthographicCameraObserver>
struct  ReadOnlyCollection_1_t2483  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<OrthographicCameraObserver>::list
	Object_t* ___list_0;
};
