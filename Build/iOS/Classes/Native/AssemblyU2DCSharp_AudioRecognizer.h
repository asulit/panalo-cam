﻿#pragma once
#include <stdint.h>
// BridgeHandler
struct BridgeHandler_t185;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// AudioRecognizer
struct  AudioRecognizer_t229  : public MonoBehaviour_t5
{
	// BridgeHandler AudioRecognizer::nativeCalls
	BridgeHandler_t185 * ___nativeCalls_2;
};
