﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>
struct Transform_1_t3446;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.VuforiaManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m28262_gshared (Transform_1_t3446 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m28262(__this, ___object, ___method, method) (( void (*) (Transform_1_t3446 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m28262_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::Invoke(TKey,TValue)
extern "C" int32_t Transform_1_Invoke_m28263_gshared (Transform_1_t3446 * __this, int32_t ___key, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m28263(__this, ___key, ___value, method) (( int32_t (*) (Transform_1_t3446 *, int32_t, VirtualButtonData_t1135 , const MethodInfo*))Transform_1_Invoke_m28263_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m28264_gshared (Transform_1_t3446 * __this, int32_t ___key, VirtualButtonData_t1135  ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m28264(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3446 *, int32_t, VirtualButtonData_t1135 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m28264_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Transform_1_EndInvoke_m28265_gshared (Transform_1_t3446 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m28265(__this, ___result, method) (( int32_t (*) (Transform_1_t3446 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m28265_gshared)(__this, ___result, method)
