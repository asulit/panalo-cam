﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_88.h"
// Vuforia.VuforiaManagerImpl/WordResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__4.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m26039_gshared (InternalEnumerator_1_t3317 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m26039(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3317 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m26039_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26040_gshared (InternalEnumerator_1_t3317 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26040(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3317 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26040_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m26041_gshared (InternalEnumerator_1_t3317 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m26041(__this, method) (( void (*) (InternalEnumerator_1_t3317 *, const MethodInfo*))InternalEnumerator_1_Dispose_m26041_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m26042_gshared (InternalEnumerator_1_t3317 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m26042(__this, method) (( bool (*) (InternalEnumerator_1_t3317 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m26042_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::get_Current()
extern "C" WordResultData_t1138  InternalEnumerator_1_get_Current_m26043_gshared (InternalEnumerator_1_t3317 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m26043(__this, method) (( WordResultData_t1138  (*) (InternalEnumerator_1_t3317 *, const MethodInfo*))InternalEnumerator_1_get_Current_m26043_gshared)(__this, method)
