﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct Action_1_t455;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.VuforiaUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"

// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m1811_gshared (Action_1_t455 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Action_1__ctor_m1811(__this, ___object, ___method, method) (( void (*) (Action_1_t455 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1811_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::Invoke(T)
extern "C" void Action_1_Invoke_m7461_gshared (Action_1_t455 * __this, int32_t ___obj, const MethodInfo* method);
#define Action_1_Invoke_m7461(__this, ___obj, method) (( void (*) (Action_1_t455 *, int32_t, const MethodInfo*))Action_1_Invoke_m7461_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<Vuforia.VuforiaUnity/InitError>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m18649_gshared (Action_1_t455 * __this, int32_t ___obj, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Action_1_BeginInvoke_m18649(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t455 *, int32_t, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m18649_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m18650_gshared (Action_1_t455 * __this, Object_t * ___result, const MethodInfo* method);
#define Action_1_EndInvoke_m18650(__this, ___result, method) (( void (*) (Action_1_t455 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m18650_gshared)(__this, ___result, method)
