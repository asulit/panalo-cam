﻿#pragma once
#include <stdint.h>
// Common.Logger.LogLevel
struct LogLevel_t73;
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Logger.LogLevel
struct  LogLevel_t73  : public Object_t
{
	// System.String Common.Logger.LogLevel::name
	String_t* ___name_3;
};
struct LogLevel_t73_StaticFields{
	// Common.Logger.LogLevel Common.Logger.LogLevel::NORMAL
	LogLevel_t73 * ___NORMAL_0;
	// Common.Logger.LogLevel Common.Logger.LogLevel::WARNING
	LogLevel_t73 * ___WARNING_1;
	// Common.Logger.LogLevel Common.Logger.LogLevel::ERROR
	LogLevel_t73 * ___ERROR_2;
};
