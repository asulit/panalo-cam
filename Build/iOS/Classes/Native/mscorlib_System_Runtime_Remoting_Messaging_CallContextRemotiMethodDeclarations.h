﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.CallContextRemotingData
struct CallContextRemotingData_t2109;

// System.Void System.Runtime.Remoting.Messaging.CallContextRemotingData::.ctor()
extern "C" void CallContextRemotingData__ctor_m12486 (CallContextRemotingData_t2109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
