﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// PrefabManager/PreloadData
struct  PreloadData_t94  : public Object_t
{
	// System.String PrefabManager/PreloadData::prefabName
	String_t* ___prefabName_0;
	// System.Int32 PrefabManager/PreloadData::preloadCount
	int32_t ___preloadCount_1;
};
