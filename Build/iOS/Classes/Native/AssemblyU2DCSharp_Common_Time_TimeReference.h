﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Time.TimeReference
struct TimeReference_t14;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Time.TimeReference
struct  TimeReference_t14  : public Object_t
{
	// System.String Common.Time.TimeReference::name
	String_t* ___name_0;
	// System.Single Common.Time.TimeReference::timeScale
	float ___timeScale_1;
	// System.Boolean Common.Time.TimeReference::affectsGlobalTimeScale
	bool ___affectsGlobalTimeScale_2;
};
struct TimeReference_t14_StaticFields{
	// Common.Time.TimeReference Common.Time.TimeReference::DEFAULT_INSTANCE
	TimeReference_t14 * ___DEFAULT_INSTANCE_3;
};
