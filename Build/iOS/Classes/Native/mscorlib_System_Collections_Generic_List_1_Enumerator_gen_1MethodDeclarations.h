﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1/Enumerator<UnityThreading.ThreadBase>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30MethodDeclarations.h"
#define Enumerator__ctor_m17062(__this, ___l, method) (( void (*) (Enumerator_t429 *, List_1_t182 *, const MethodInfo*))Enumerator__ctor_m14718_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityThreading.ThreadBase>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17063(__this, method) (( Object_t * (*) (Enumerator_t429 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14719_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityThreading.ThreadBase>::Dispose()
#define Enumerator_Dispose_m17064(__this, method) (( void (*) (Enumerator_t429 *, const MethodInfo*))Enumerator_Dispose_m14720_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityThreading.ThreadBase>::VerifyState()
#define Enumerator_VerifyState_m17065(__this, method) (( void (*) (Enumerator_t429 *, const MethodInfo*))Enumerator_VerifyState_m14721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityThreading.ThreadBase>::MoveNext()
#define Enumerator_MoveNext_m1625(__this, method) (( bool (*) (Enumerator_t429 *, const MethodInfo*))Enumerator_MoveNext_m14722_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityThreading.ThreadBase>::get_Current()
#define Enumerator_get_Current_m1624(__this, method) (( ThreadBase_t169 * (*) (Enumerator_t429 *, const MethodInfo*))Enumerator_get_Current_m14723_gshared)(__this, method)
