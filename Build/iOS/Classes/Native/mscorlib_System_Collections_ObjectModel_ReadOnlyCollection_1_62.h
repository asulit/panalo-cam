﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.Word>
struct IList_1_t3367;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Word>
struct  ReadOnlyCollection_1_t3366  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Word>::list
	Object_t* ___list_0;
};
