﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>
struct DefaultComparer_t3033;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::.ctor()
extern "C" void DefaultComparer__ctor_m22242_gshared (DefaultComparer_t3033 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22242(__this, method) (( void (*) (DefaultComparer_t3033 *, const MethodInfo*))DefaultComparer__ctor_m22242_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m22243_gshared (DefaultComparer_t3033 * __this, Vector3_t36  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m22243(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3033 *, Vector3_t36 , const MethodInfo*))DefaultComparer_GetHashCode_m22243_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m22244_gshared (DefaultComparer_t3033 * __this, Vector3_t36  ___x, Vector3_t36  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m22244(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3033 *, Vector3_t36 , Vector3_t36 , const MethodInfo*))DefaultComparer_Equals_m22244_gshared)(__this, ___x, ___y, method)
