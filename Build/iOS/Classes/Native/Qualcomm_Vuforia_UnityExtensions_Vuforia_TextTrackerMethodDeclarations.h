﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TextTracker
struct TextTracker_t1170;

// System.Void Vuforia.TextTracker::.ctor()
extern "C" void TextTracker__ctor_m5835 (TextTracker_t1170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
