﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TurnOffBehaviour
struct TurnOffBehaviour_t309;

// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern "C" void TurnOffBehaviour__ctor_m1262 (TurnOffBehaviour_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern "C" void TurnOffBehaviour_Awake_m1263 (TurnOffBehaviour_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
