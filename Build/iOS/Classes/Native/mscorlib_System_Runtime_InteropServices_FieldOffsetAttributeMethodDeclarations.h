﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.FieldOffsetAttribute
struct FieldOffsetAttribute_t1790;

// System.Void System.Runtime.InteropServices.FieldOffsetAttribute::.ctor(System.Int32)
extern "C" void FieldOffsetAttribute__ctor_m10388 (FieldOffsetAttribute_t1790 * __this, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
