﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Selectable
struct Selectable_t545;
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"

// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::get_mode()
extern "C" int32_t Navigation_get_mode_m2686 (Navigation_t613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_mode(UnityEngine.UI.Navigation/Mode)
extern "C" void Navigation_set_mode_m2687 (Navigation_t613 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnUp()
extern "C" Selectable_t545 * Navigation_get_selectOnUp_m2688 (Navigation_t613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnUp(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnUp_m2689 (Navigation_t613 * __this, Selectable_t545 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnDown()
extern "C" Selectable_t545 * Navigation_get_selectOnDown_m2690 (Navigation_t613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnDown(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnDown_m2691 (Navigation_t613 * __this, Selectable_t545 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnLeft()
extern "C" Selectable_t545 * Navigation_get_selectOnLeft_m2692 (Navigation_t613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnLeft(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnLeft_m2693 (Navigation_t613 * __this, Selectable_t545 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnRight()
extern "C" Selectable_t545 * Navigation_get_selectOnRight_m2694 (Navigation_t613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnRight(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnRight_m2695 (Navigation_t613 * __this, Selectable_t545 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Navigation UnityEngine.UI.Navigation::get_defaultNavigation()
extern "C" Navigation_t613  Navigation_get_defaultNavigation_m2696 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
