﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ChannelInfo
struct ChannelInfo_t2067;
// System.Object[]
struct ObjectU5BU5D_t356;

// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
extern "C" void ChannelInfo__ctor_m12336 (ChannelInfo_t2067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
extern "C" ObjectU5BU5D_t356* ChannelInfo_get_ChannelData_m12337 (ChannelInfo_t2067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
