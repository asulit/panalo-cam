﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_46.h"
// Vuforia.VuforiaManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m28220_gshared (KeyValuePair_2_t3440 * __this, int32_t ___key, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m28220(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3440 *, int32_t, VirtualButtonData_t1135 , const MethodInfo*))KeyValuePair_2__ctor_m28220_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m28221_gshared (KeyValuePair_2_t3440 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m28221(__this, method) (( int32_t (*) (KeyValuePair_2_t3440 *, const MethodInfo*))KeyValuePair_2_get_Key_m28221_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m28222_gshared (KeyValuePair_2_t3440 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m28222(__this, ___value, method) (( void (*) (KeyValuePair_2_t3440 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m28222_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C" VirtualButtonData_t1135  KeyValuePair_2_get_Value_m28223_gshared (KeyValuePair_2_t3440 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m28223(__this, method) (( VirtualButtonData_t1135  (*) (KeyValuePair_2_t3440 *, const MethodInfo*))KeyValuePair_2_get_Value_m28223_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m28224_gshared (KeyValuePair_2_t3440 * __this, VirtualButtonData_t1135  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m28224(__this, ___value, method) (( void (*) (KeyValuePair_2_t3440 *, VirtualButtonData_t1135 , const MethodInfo*))KeyValuePair_2_set_Value_m28224_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m28225_gshared (KeyValuePair_2_t3440 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m28225(__this, method) (( String_t* (*) (KeyValuePair_2_t3440 *, const MethodInfo*))KeyValuePair_2_ToString_m28225_gshared)(__this, method)
