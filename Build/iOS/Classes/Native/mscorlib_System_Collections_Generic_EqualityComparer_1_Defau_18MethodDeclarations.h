﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct DefaultComparer_t3437;
// Vuforia.VuforiaManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor()
extern "C" void DefaultComparer__ctor_m28166_gshared (DefaultComparer_t3437 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m28166(__this, method) (( void (*) (DefaultComparer_t3437 *, const MethodInfo*))DefaultComparer__ctor_m28166_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m28167_gshared (DefaultComparer_t3437 * __this, TrackableResultData_t1134  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m28167(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3437 *, TrackableResultData_t1134 , const MethodInfo*))DefaultComparer_GetHashCode_m28167_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VuforiaManagerImpl/TrackableResultData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m28168_gshared (DefaultComparer_t3437 * __this, TrackableResultData_t1134  ___x, TrackableResultData_t1134  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m28168(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3437 *, TrackableResultData_t1134 , TrackableResultData_t1134 , const MethodInfo*))DefaultComparer_Equals_m28168_gshared)(__this, ___x, ___y, method)
