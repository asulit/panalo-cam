﻿#pragma once
#include <stdint.h>
// Vuforia.Prop
struct Prop_t357;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Prop>
struct  Comparison_1_t3411  : public MulticastDelegate_t28
{
};
