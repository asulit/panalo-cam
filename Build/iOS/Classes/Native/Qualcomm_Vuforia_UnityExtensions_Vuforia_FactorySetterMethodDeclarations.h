﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.FactorySetter
struct FactorySetter_t465;

// System.Void Vuforia.FactorySetter::.ctor()
extern "C" void FactorySetter__ctor_m5196 (FactorySetter_t465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
