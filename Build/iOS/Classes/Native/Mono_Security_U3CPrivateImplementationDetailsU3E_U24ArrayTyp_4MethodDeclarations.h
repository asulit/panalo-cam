﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2464_t1555_marshal(const U24ArrayTypeU2464_t1555& unmarshaled, U24ArrayTypeU2464_t1555_marshaled& marshaled);
extern "C" void U24ArrayTypeU2464_t1555_marshal_back(const U24ArrayTypeU2464_t1555_marshaled& marshaled, U24ArrayTypeU2464_t1555& unmarshaled);
extern "C" void U24ArrayTypeU2464_t1555_marshal_cleanup(U24ArrayTypeU2464_t1555_marshaled& marshaled);
