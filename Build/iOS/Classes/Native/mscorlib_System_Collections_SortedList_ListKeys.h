﻿#pragma once
#include <stdint.h>
// System.Collections.SortedList
struct SortedList_t1757;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.SortedList/ListKeys
struct  ListKeys_t1891  : public Object_t
{
	// System.Collections.SortedList System.Collections.SortedList/ListKeys::host
	SortedList_t1757 * ___host_0;
};
