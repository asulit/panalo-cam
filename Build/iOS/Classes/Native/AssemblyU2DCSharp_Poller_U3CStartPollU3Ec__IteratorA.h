﻿#pragma once
#include <stdint.h>
// Raffle
struct Raffle_t202;
// RaffleData
struct RaffleData_t206;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// Poller/<StartPoll>c__IteratorA
struct  U3CStartPollU3Ec__IteratorA_t239  : public Object_t
{
	// Raffle Poller/<StartPoll>c__IteratorA::<raffle>__0
	Raffle_t202 * ___U3CraffleU3E__0_0;
	// RaffleData Poller/<StartPoll>c__IteratorA::<raffleData>__1
	RaffleData_t206 * ___U3CraffleDataU3E__1_1;
	// System.Int32 Poller/<StartPoll>c__IteratorA::$PC
	int32_t ___U24PC_2;
	// System.Object Poller/<StartPoll>c__IteratorA::$current
	Object_t * ___U24current_3;
};
