﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Metadata.SoapAttribute
struct SoapAttribute_t2123;
// System.Object
struct Object_t;

// System.Void System.Runtime.Remoting.InternalRemotingServices::.cctor()
extern "C" void InternalRemotingServices__cctor_m12640 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.SoapAttribute System.Runtime.Remoting.InternalRemotingServices::GetCachedSoapAttribute(System.Object)
extern "C" SoapAttribute_t2123 * InternalRemotingServices_GetCachedSoapAttribute_m12641 (Object_t * __this /* static, unused */, Object_t * ___reflectionObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
