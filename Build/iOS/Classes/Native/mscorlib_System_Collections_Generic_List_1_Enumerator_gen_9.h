﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct List_1_t1070;
// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t284;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>
struct  Enumerator_t1294 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>::l
	List_1_t1070 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.HideExcessAreaAbstractBehaviour>::current
	HideExcessAreaAbstractBehaviour_t284 * ___current_3;
};
