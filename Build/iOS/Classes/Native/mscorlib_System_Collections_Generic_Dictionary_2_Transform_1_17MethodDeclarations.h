﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>
struct Transform_1_t2677;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m17258_gshared (Transform_1_t2677 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m17258(__this, ___object, ___method, method) (( void (*) (Transform_1_t2677 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m17258_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t2667  Transform_1_Invoke_m17259_gshared (Transform_1_t2677 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Transform_1_Invoke_m17259(__this, ___key, ___value, method) (( KeyValuePair_2_t2667  (*) (Transform_1_t2677 *, int32_t, Object_t *, const MethodInfo*))Transform_1_Invoke_m17259_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m17260_gshared (Transform_1_t2677 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m17260(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2677 *, int32_t, Object_t *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m17260_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.Generic.KeyValuePair`2<EScreens,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t2667  Transform_1_EndInvoke_m17261_gshared (Transform_1_t2677 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m17261(__this, ___result, method) (( KeyValuePair_2_t2667  (*) (Transform_1_t2677 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m17261_gshared)(__this, ___result, method)
