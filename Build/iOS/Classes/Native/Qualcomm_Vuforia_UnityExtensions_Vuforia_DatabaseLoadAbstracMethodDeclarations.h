﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DatabaseLoadAbstractBehaviour
struct DatabaseLoadAbstractBehaviour_t275;
// System.String
struct String_t;

// System.Void Vuforia.DatabaseLoadAbstractBehaviour::LoadDatasets()
extern "C" void DatabaseLoadAbstractBehaviour_LoadDatasets_m5543 (DatabaseLoadAbstractBehaviour_t275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DatabaseLoadAbstractBehaviour_AddOSSpecificExternalDatasetSearchDirs_m5544 (DatabaseLoadAbstractBehaviour_t275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::AddExternalDatasetSearchDir(System.String)
extern "C" void DatabaseLoadAbstractBehaviour_AddExternalDatasetSearchDir_m5545 (DatabaseLoadAbstractBehaviour_t275 * __this, String_t* ___searchDir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::Start()
extern "C" void DatabaseLoadAbstractBehaviour_Start_m5546 (DatabaseLoadAbstractBehaviour_t275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::.ctor()
extern "C" void DatabaseLoadAbstractBehaviour__ctor_m1810 (DatabaseLoadAbstractBehaviour_t275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
