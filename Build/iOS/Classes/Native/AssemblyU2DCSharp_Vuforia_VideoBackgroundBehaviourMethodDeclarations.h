﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoBackgroundBehaviour
struct VideoBackgroundBehaviour_t314;

// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern "C" void VideoBackgroundBehaviour__ctor_m1267 (VideoBackgroundBehaviour_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
