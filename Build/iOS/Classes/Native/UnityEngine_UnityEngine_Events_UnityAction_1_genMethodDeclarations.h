﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor(System.Object,System.IntPtr)
// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_4MethodDeclarations.h"
#define UnityAction_1__ctor_m3302(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t508 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m18971_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Invoke(T0)
#define UnityAction_1_Invoke_m18986(__this, ___arg0, method) (( void (*) (UnityAction_1_t508 *, List_1_t686 *, const MethodInfo*))UnityAction_1_Invoke_m18972_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m18987(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t508 *, List_1_t686 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m18973_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m18988(__this, ___result, method) (( void (*) (UnityAction_1_t508 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m18974_gshared)(__this, ___result, method)
