﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator5
struct U3CClickRepeatU3Ec__Iterator5_t620;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator5::.ctor()
extern "C" void U3CClickRepeatU3Ec__Iterator5__ctor_m2716 (U3CClickRepeatU3Ec__Iterator5_t620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CClickRepeatU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2717 (U3CClickRepeatU3Ec__Iterator5_t620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CClickRepeatU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m2718 (U3CClickRepeatU3Ec__Iterator5_t620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator5::MoveNext()
extern "C" bool U3CClickRepeatU3Ec__Iterator5_MoveNext_m2719 (U3CClickRepeatU3Ec__Iterator5_t620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator5::Dispose()
extern "C" void U3CClickRepeatU3Ec__Iterator5_Dispose_m2720 (U3CClickRepeatU3Ec__Iterator5_t620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator5::Reset()
extern "C" void U3CClickRepeatU3Ec__Iterator5_Reset_m2721 (U3CClickRepeatU3Ec__Iterator5_t620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
