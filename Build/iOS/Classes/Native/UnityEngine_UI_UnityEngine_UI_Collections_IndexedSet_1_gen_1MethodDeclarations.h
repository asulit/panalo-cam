﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_2MethodDeclarations.h"
#define IndexedSet_1__ctor_m3742(__this, method) (( void (*) (IndexedSet_1_t648 *, const MethodInfo*))IndexedSet_1__ctor_m20065_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m21838(__this, method) (( Object_t * (*) (IndexedSet_1_t648 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20067_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Add(T)
#define IndexedSet_1_Add_m21839(__this, ___item, method) (( void (*) (IndexedSet_1_t648 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m20069_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Remove(T)
#define IndexedSet_1_Remove_m21840(__this, ___item, method) (( bool (*) (IndexedSet_1_t648 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m20071_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m21841(__this, method) (( Object_t* (*) (IndexedSet_1_t648 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m20073_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Clear()
#define IndexedSet_1_Clear_m21842(__this, method) (( void (*) (IndexedSet_1_t648 *, const MethodInfo*))IndexedSet_1_Clear_m20075_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Contains(T)
#define IndexedSet_1_Contains_m21843(__this, ___item, method) (( bool (*) (IndexedSet_1_t648 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m20077_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m21844(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t648 *, IClipperU5BU5D_t3012*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m20079_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::get_Count()
#define IndexedSet_1_get_Count_m21845(__this, method) (( int32_t (*) (IndexedSet_1_t648 *, const MethodInfo*))IndexedSet_1_get_Count_m20081_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m21846(__this, method) (( bool (*) (IndexedSet_1_t648 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m20083_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::IndexOf(T)
#define IndexedSet_1_IndexOf_m21847(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t648 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m20085_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m21848(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t648 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m20087_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m21849(__this, ___index, method) (( void (*) (IndexedSet_1_t648 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m20089_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m21850(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t648 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m20091_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m21851(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t648 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m20093_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m21852(__this, ___match, method) (( void (*) (IndexedSet_1_t648 *, Predicate_1_t3015 *, const MethodInfo*))IndexedSet_1_RemoveAll_m20094_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m21853(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t648 *, Comparison_1_t3016 *, const MethodInfo*))IndexedSet_1_Sort_m20095_gshared)(__this, ___sortLayoutFunction, method)
