﻿#pragma once
#include <stdint.h>
// UnityThreading.ActionThread
struct ActionThread_t171;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Action`1<UnityThreading.ActionThread>
struct  Action_1_t172  : public MulticastDelegate_t28
{
};
