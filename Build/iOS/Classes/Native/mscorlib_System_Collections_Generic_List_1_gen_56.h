﻿#pragma once
#include <stdint.h>
// Vuforia.DataSetImpl[]
struct DataSetImplU5BU5D_t3295;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
struct  List_1_t1117  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.DataSetImpl>::_items
	DataSetImplU5BU5D_t3295* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::_version
	int32_t ____version_3;
};
struct List_1_t1117_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.DataSetImpl>::EmptyArray
	DataSetImplU5BU5D_t3295* ___EmptyArray_4;
};
