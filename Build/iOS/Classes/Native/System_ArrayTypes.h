﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// System.Collections.Generic.Queue`1<Common.Logger.Log>[]
// System.Collections.Generic.Queue`1<Common.Logger.Log>[]
struct  Queue_1U5BU5D_t2571  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct  X509ChainStatusU5BU5D_t1659  : public Array_t
{
};
// System.Text.RegularExpressions.Capture[]
// System.Text.RegularExpressions.Capture[]
struct  CaptureU5BU5D_t1683  : public Array_t
{
};
// System.Text.RegularExpressions.Group[]
// System.Text.RegularExpressions.Group[]
struct  GroupU5BU5D_t1684  : public Array_t
{
};
struct GroupU5BU5D_t1684_StaticFields{
};
// System.Text.RegularExpressions.Mark[]
// System.Text.RegularExpressions.Mark[]
struct  MarkU5BU5D_t1710  : public Array_t
{
};
// System.Uri/UriScheme[]
// System.Uri/UriScheme[]
struct  UriSchemeU5BU5D_t1741  : public Array_t
{
};
