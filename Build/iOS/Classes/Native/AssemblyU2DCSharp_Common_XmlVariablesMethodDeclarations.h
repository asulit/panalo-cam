﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.XmlVariables
struct XmlVariables_t149;
// System.String
struct String_t;

// System.Void Common.XmlVariables::.ctor()
extern "C" void XmlVariables__ctor_m490 (XmlVariables_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.XmlVariables::Awake()
extern "C" void XmlVariables_Awake_m491 (XmlVariables_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.XmlVariables::Parse()
extern "C" void XmlVariables_Parse_m492 (XmlVariables_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.XmlVariables::Get(System.String)
extern "C" String_t* XmlVariables_Get_m493 (XmlVariables_t149 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.XmlVariables::GetAsBool(System.String)
extern "C" bool XmlVariables_GetAsBool_m494 (XmlVariables_t149 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.XmlVariables::ToBool(System.String)
extern "C" bool XmlVariables_ToBool_m495 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Common.XmlVariables::GetAsInt(System.String)
extern "C" int32_t XmlVariables_GetAsInt_m496 (XmlVariables_t149 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Common.XmlVariables::GetAsFloat(System.String)
extern "C" float XmlVariables_GetAsFloat_m497 (XmlVariables_t149 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
