﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void EyewearCalibrationReading_t1080_marshal(const EyewearCalibrationReading_t1080& unmarshaled, EyewearCalibrationReading_t1080_marshaled& marshaled);
extern "C" void EyewearCalibrationReading_t1080_marshal_back(const EyewearCalibrationReading_t1080_marshaled& marshaled, EyewearCalibrationReading_t1080& unmarshaled);
extern "C" void EyewearCalibrationReading_t1080_marshal_cleanup(EyewearCalibrationReading_t1080_marshaled& marshaled);
