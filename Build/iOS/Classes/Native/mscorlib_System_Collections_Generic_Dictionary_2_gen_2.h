﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t401;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t2500;
// System.String[]
struct StringU5BU5D_t137;
// Common.Query.QueryResultResolver[]
struct QueryResultResolverU5BU5D_t2591;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t2503;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,Common.Query.QueryResultResolver,System.Collections.DictionaryEntry>
struct Transform_1_t2592;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>
struct  Dictionary_2_t110  : public Object_t
{
	// System.Int32[] System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::table
	Int32U5BU5D_t401* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::linkSlots
	LinkU5BU5D_t2500* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::keySlots
	StringU5BU5D_t137* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::valueSlots
	QueryResultResolverU5BU5D_t2591* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::hcp
	Object_t* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::serialization_info
	SerializationInfo_t1012 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::generation
	int32_t ___generation_14;
};
struct Dictionary_2_t110_StaticFields{
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::<>f__am$cacheB
	Transform_1_t2592 * ___U3CU3Ef__amU24cacheB_15;
};
