﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Lifetime.LeaseSink
struct LeaseSink_t2091;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t2077;

// System.Void System.Runtime.Remoting.Lifetime.LeaseSink::.ctor(System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" void LeaseSink__ctor_m12433 (LeaseSink_t2091 * __this, Object_t * ___nextSink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
