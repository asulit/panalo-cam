﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>
struct LinkedListNode_1_t406;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>
struct  LinkedList_1_t90  : public Object_t
{
	// System.UInt32 System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::count
	uint32_t ___count_2;
	// System.UInt32 System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::version
	uint32_t ___version_3;
	// System.Object System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::syncRoot
	Object_t * ___syncRoot_4;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::first
	LinkedListNode_1_t406 * ___first_5;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::si
	SerializationInfo_t1012 * ___si_6;
};
