﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PlayModeUnityPlayer
struct PlayModeUnityPlayer_t470;
// System.String
struct String_t;
// Vuforia.VuforiaUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"

// System.Void Vuforia.PlayModeUnityPlayer::LoadNativeLibraries()
extern "C" void PlayModeUnityPlayer_LoadNativeLibraries_m5436 (PlayModeUnityPlayer_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::InitializePlatform()
extern "C" void PlayModeUnityPlayer_InitializePlatform_m5437 (PlayModeUnityPlayer_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::InitializeSurface()
extern "C" void PlayModeUnityPlayer_InitializeSurface_m5438 (PlayModeUnityPlayer_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaUnity/InitError Vuforia.PlayModeUnityPlayer::Start(System.String)
extern "C" int32_t PlayModeUnityPlayer_Start_m5439 (PlayModeUnityPlayer_t470 * __this, String_t* ___licenseKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::Update()
extern "C" void PlayModeUnityPlayer_Update_m5440 (PlayModeUnityPlayer_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::Dispose()
extern "C" void PlayModeUnityPlayer_Dispose_m5441 (PlayModeUnityPlayer_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnPause()
extern "C" void PlayModeUnityPlayer_OnPause_m5442 (PlayModeUnityPlayer_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnResume()
extern "C" void PlayModeUnityPlayer_OnResume_m5443 (PlayModeUnityPlayer_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::OnDestroy()
extern "C" void PlayModeUnityPlayer_OnDestroy_m5444 (PlayModeUnityPlayer_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeUnityPlayer::.ctor()
extern "C" void PlayModeUnityPlayer__ctor_m1881 (PlayModeUnityPlayer_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
