﻿#pragma once
#include <stdint.h>
// System.Single[]
struct SingleU5BU5D_t264;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.AudioClip/PCMReaderCallback
struct  PCMReaderCallback_t874  : public MulticastDelegate_t28
{
};
