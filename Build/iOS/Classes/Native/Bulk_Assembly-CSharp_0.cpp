﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
#include "mscorlib_System_Array.h"

// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"
// Aabb2
#include "AssemblyU2DCSharp_Aabb2.h"
// Aabb2
#include "AssemblyU2DCSharp_Aabb2MethodDeclarations.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// System.Single
#include "mscorlib_System_Single.h"
#include "mscorlib_ArrayTypes.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.Void Aabb2::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// Aabb2
#include "AssemblyU2DCSharp_Aabb2MethodDeclarations.h"
extern TypeInfo* Vector2_t2_il2cpp_TypeInfo_var;
extern "C" void Aabb2__ctor_m0 (Aabb2_t1 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2  V_0 = {0};
	Vector2_t2  V_1 = {0};
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Initobj (Vector2_t2_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2  L_0 = V_0;
		__this->___min_1 = L_0;
		Initobj (Vector2_t2_il2cpp_TypeInfo_var, (&V_1));
		Vector2_t2  L_1 = V_1;
		__this->___max_2 = L_1;
		Aabb2_Empty_m3(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Aabb2::.ctor(UnityEngine.Vector2,UnityEngine.Vector2)
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern TypeInfo* Vector2_t2_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral0;
extern "C" void Aabb2__ctor_m1 (Aabb2_t1 * __this, Vector2_t2  ___v1, Vector2_t2  ___v2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		_stringLiteral0 = il2cpp_codegen_string_literal_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2  V_0 = {0};
	Vector2_t2  V_1 = {0};
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Initobj (Vector2_t2_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2  L_0 = V_0;
		__this->___min_1 = L_0;
		Initobj (Vector2_t2_il2cpp_TypeInfo_var, (&V_1));
		Vector2_t2  L_1 = V_1;
		__this->___max_2 = L_1;
		Aabb2_Empty_m3(__this, /*hidden argument*/NULL);
		Vector2_t2  L_2 = ___v1;
		Aabb2_AddToContain_m11(__this, L_2, /*hidden argument*/NULL);
		Vector2_t2  L_3 = ___v2;
		Aabb2_AddToContain_m11(__this, L_3, /*hidden argument*/NULL);
		bool L_4 = Aabb2_IsEmpty_m4(__this, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)L_4) == ((int32_t)0))? 1 : 0), _stringLiteral0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Aabb2::.ctor(UnityEngine.Rect)
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
extern TypeInfo* Vector2_t2_il2cpp_TypeInfo_var;
extern "C" void Aabb2__ctor_m2 (Aabb2_t1 * __this, Rect_t267  ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2  V_0 = {0};
	Vector2_t2  V_1 = {0};
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Initobj (Vector2_t2_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2  L_0 = V_0;
		__this->___min_1 = L_0;
		Initobj (Vector2_t2_il2cpp_TypeInfo_var, (&V_1));
		Vector2_t2  L_1 = V_1;
		__this->___max_2 = L_1;
		Aabb2_Empty_m3(__this, /*hidden argument*/NULL);
		float L_2 = Rect_get_xMin_m1307((&___rect), /*hidden argument*/NULL);
		float L_3 = Rect_get_yMin_m1308((&___rect), /*hidden argument*/NULL);
		Vector2_t2  L_4 = {0};
		Vector2__ctor_m1309(&L_4, L_2, L_3, /*hidden argument*/NULL);
		Aabb2_AddToContain_m11(__this, L_4, /*hidden argument*/NULL);
		float L_5 = Rect_get_xMax_m1310((&___rect), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m1311((&___rect), /*hidden argument*/NULL);
		Vector2_t2  L_7 = {0};
		Vector2__ctor_m1309(&L_7, L_5, L_6, /*hidden argument*/NULL);
		Aabb2_AddToContain_m11(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Aabb2::Empty()
extern "C" void Aabb2_Empty_m3 (Aabb2_t1 * __this, const MethodInfo* method)
{
	{
		Vector2_t2 * L_0 = &(__this->___min_1);
		L_0->___x_1 = (1.0E+37f);
		Vector2_t2 * L_1 = &(__this->___min_1);
		L_1->___y_2 = (1.0E+37f);
		Vector2_t2 * L_2 = &(__this->___max_2);
		L_2->___x_1 = (-1.0E+37f);
		Vector2_t2 * L_3 = &(__this->___max_2);
		L_3->___y_2 = (-1.0E+37f);
		return;
	}
}
// System.Boolean Aabb2::IsEmpty()
extern "C" bool Aabb2_IsEmpty_m4 (Aabb2_t1 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Vector2_t2 * L_0 = &(__this->___min_1);
		float L_1 = (L_0->___x_1);
		Vector2_t2 * L_2 = &(__this->___max_2);
		float L_3 = (L_2->___x_1);
		if ((!(((float)L_1) > ((float)L_3))))
		{
			goto IL_0035;
		}
	}
	{
		Vector2_t2 * L_4 = &(__this->___min_1);
		float L_5 = (L_4->___y_2);
		Vector2_t2 * L_6 = &(__this->___max_2);
		float L_7 = (L_6->___y_2);
		G_B3_0 = ((((float)L_5) > ((float)L_7))? 1 : 0);
		goto IL_0036;
	}

IL_0035:
	{
		G_B3_0 = 0;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// UnityEngine.Vector2 Aabb2::GetMinimum()
extern "C" Vector2_t2  Aabb2_GetMinimum_m5 (Aabb2_t1 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___min_1);
		return L_0;
	}
}
// UnityEngine.Vector2 Aabb2::GetMaximum()
extern "C" Vector2_t2  Aabb2_GetMaximum_m6 (Aabb2_t1 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___max_2);
		return L_0;
	}
}
// UnityEngine.Vector2 Aabb2::GetCenter()
extern "C" Vector2_t2  Aabb2_GetCenter_m7 (Aabb2_t1 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___min_1);
		Vector2_t2  L_1 = (__this->___max_2);
		Vector2_t2  L_2 = Vector2_op_Addition_m1312(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector2_t2  L_3 = Vector2_op_Multiply_m1313(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector2 Aabb2::GetSize()
extern "C" Vector2_t2  Aabb2_GetSize_m8 (Aabb2_t1 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___max_2);
		Vector2_t2  L_1 = (__this->___min_1);
		Vector2_t2  L_2 = Vector2_op_Subtraction_m1314(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector2 Aabb2::GetRadiusVector()
extern "C" Vector2_t2  Aabb2_GetRadiusVector_m9 (Aabb2_t1 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = Aabb2_GetSize_m8(__this, /*hidden argument*/NULL);
		Vector2_t2  L_1 = Vector2_op_Multiply_m1313(NULL /*static, unused*/, L_0, (0.5f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single Aabb2::GetRadius()
extern "C" float Aabb2_GetRadius_m10 (Aabb2_t1 * __this, const MethodInfo* method)
{
	Vector2_t2  V_0 = {0};
	{
		Vector2_t2  L_0 = Aabb2_GetRadiusVector_m9(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Vector2_get_magnitude_m1315((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Aabb2::AddToContain(UnityEngine.Vector2)
extern "C" void Aabb2_AddToContain_m11 (Aabb2_t1 * __this, Vector2_t2  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		Vector2_t2 * L_1 = &(__this->___min_1);
		float L_2 = (L_1->___x_1);
		if ((!(((float)L_0) < ((float)L_2))))
		{
			goto IL_0029;
		}
	}
	{
		Vector2_t2 * L_3 = &(__this->___min_1);
		float L_4 = ((&___v)->___x_1);
		L_3->___x_1 = L_4;
	}

IL_0029:
	{
		float L_5 = ((&___v)->___y_2);
		Vector2_t2 * L_6 = &(__this->___min_1);
		float L_7 = (L_6->___y_2);
		if ((!(((float)L_5) < ((float)L_7))))
		{
			goto IL_0052;
		}
	}
	{
		Vector2_t2 * L_8 = &(__this->___min_1);
		float L_9 = ((&___v)->___y_2);
		L_8->___y_2 = L_9;
	}

IL_0052:
	{
		float L_10 = ((&___v)->___x_1);
		Vector2_t2 * L_11 = &(__this->___max_2);
		float L_12 = (L_11->___x_1);
		if ((!(((float)L_10) > ((float)L_12))))
		{
			goto IL_007b;
		}
	}
	{
		Vector2_t2 * L_13 = &(__this->___max_2);
		float L_14 = ((&___v)->___x_1);
		L_13->___x_1 = L_14;
	}

IL_007b:
	{
		float L_15 = ((&___v)->___y_2);
		Vector2_t2 * L_16 = &(__this->___max_2);
		float L_17 = (L_16->___y_2);
		if ((!(((float)L_15) > ((float)L_17))))
		{
			goto IL_00a4;
		}
	}
	{
		Vector2_t2 * L_18 = &(__this->___max_2);
		float L_19 = ((&___v)->___y_2);
		L_18->___y_2 = L_19;
	}

IL_00a4:
	{
		return;
	}
}
// System.Boolean Aabb2::Contains(UnityEngine.Vector2)
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
extern "C" bool Aabb2_Contains_m12 (Aabb2_t1 * __this, Vector2_t2  ___v, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		Vector2_t2 * L_0 = &(__this->___min_1);
		float L_1 = (L_0->___x_1);
		float L_2 = ((&___v)->___x_1);
		bool L_3 = Comparison_TolerantLesserThanOrEquals_m60(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0070;
		}
	}
	{
		float L_4 = ((&___v)->___x_1);
		Vector2_t2 * L_5 = &(__this->___max_2);
		float L_6 = (L_5->___x_1);
		bool L_7 = Comparison_TolerantLesserThanOrEquals_m60(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0070;
		}
	}
	{
		Vector2_t2 * L_8 = &(__this->___min_1);
		float L_9 = (L_8->___y_2);
		float L_10 = ((&___v)->___y_2);
		bool L_11 = Comparison_TolerantLesserThanOrEquals_m60(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006d;
		}
	}
	{
		float L_12 = ((&___v)->___y_2);
		Vector2_t2 * L_13 = &(__this->___max_2);
		float L_14 = (L_13->___y_2);
		bool L_15 = Comparison_TolerantLesserThanOrEquals_m60(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_15));
		goto IL_006e;
	}

IL_006d:
	{
		G_B5_0 = 0;
	}

IL_006e:
	{
		G_B7_0 = G_B5_0;
		goto IL_0071;
	}

IL_0070:
	{
		G_B7_0 = 0;
	}

IL_0071:
	{
		return G_B7_0;
	}
}
// System.Boolean Aabb2::IsOverlapping(Aabb2)
// Aabb2
#include "AssemblyU2DCSharp_Aabb2.h"
extern "C" bool Aabb2_IsOverlapping_m13 (Aabb2_t1 * __this, Aabb2_t1 * ___otherBox, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		Aabb2_t1 * L_0 = ___otherBox;
		NullCheck(L_0);
		Vector2_t2  L_1 = Aabb2_GetTopLeft_m16(L_0, /*hidden argument*/NULL);
		bool L_2 = Aabb2_Contains_m12(__this, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0041;
		}
	}
	{
		Aabb2_t1 * L_3 = ___otherBox;
		NullCheck(L_3);
		Vector2_t2  L_4 = Aabb2_GetBottomLeft_m17(L_3, /*hidden argument*/NULL);
		bool L_5 = Aabb2_Contains_m12(__this, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		Aabb2_t1 * L_6 = ___otherBox;
		NullCheck(L_6);
		Vector2_t2  L_7 = Aabb2_GetTopRight_m18(L_6, /*hidden argument*/NULL);
		bool L_8 = Aabb2_Contains_m12(__this, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0041;
		}
	}
	{
		Aabb2_t1 * L_9 = ___otherBox;
		NullCheck(L_9);
		Vector2_t2  L_10 = Aabb2_GetBottomRight_m19(L_9, /*hidden argument*/NULL);
		bool L_11 = Aabb2_Contains_m12(__this, L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0042;
	}

IL_0041:
	{
		G_B5_0 = 1;
	}

IL_0042:
	{
		return G_B5_0;
	}
}
// System.Void Aabb2::Translate(UnityEngine.Vector2)
extern "C" void Aabb2_Translate_m14 (Aabb2_t1 * __this, Vector2_t2  ___translation, const MethodInfo* method)
{
	Vector2_t2  V_0 = {0};
	{
		bool L_0 = Aabb2_IsEmpty_m4(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Vector2_t2  L_1 = Aabb2_GetCenter_m7(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector2_t2  L_2 = (__this->___min_1);
		Vector2_t2  L_3 = V_0;
		Vector2_t2  L_4 = Vector2_op_Subtraction_m1314(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->___min_1 = L_4;
		Vector2_t2  L_5 = (__this->___max_2);
		Vector2_t2  L_6 = V_0;
		Vector2_t2  L_7 = Vector2_op_Subtraction_m1314(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		__this->___max_2 = L_7;
		Vector2_t2  L_8 = (__this->___min_1);
		Vector2_t2  L_9 = ___translation;
		Vector2_t2  L_10 = Vector2_op_Addition_m1312(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		__this->___min_1 = L_10;
		Vector2_t2  L_11 = (__this->___max_2);
		Vector2_t2  L_12 = ___translation;
		Vector2_t2  L_13 = Vector2_op_Addition_m1312(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		__this->___max_2 = L_13;
		return;
	}
}
// Aabb2 Aabb2::GetAabbInLocalSpace()
extern TypeInfo* Aabb2_t1_il2cpp_TypeInfo_var;
extern "C" Aabb2_t1 * Aabb2_GetAabbInLocalSpace_m15 (Aabb2_t1 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Aabb2_t1_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	Aabb2_t1 * V_0 = {0};
	Vector2_t2  V_1 = {0};
	{
		Aabb2_t1 * L_0 = (Aabb2_t1 *)il2cpp_codegen_object_new (Aabb2_t1_il2cpp_TypeInfo_var);
		Aabb2__ctor_m0(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector2_t2  L_1 = Aabb2_GetCenter_m7(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		Aabb2_t1 * L_2 = V_0;
		Vector2_t2  L_3 = (__this->___min_1);
		Vector2_t2  L_4 = V_1;
		Vector2_t2  L_5 = Vector2_op_Subtraction_m1314(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		Aabb2_AddToContain_m11(L_2, L_5, /*hidden argument*/NULL);
		Aabb2_t1 * L_6 = V_0;
		Vector2_t2  L_7 = (__this->___max_2);
		Vector2_t2  L_8 = V_1;
		Vector2_t2  L_9 = Vector2_op_Subtraction_m1314(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		Aabb2_AddToContain_m11(L_6, L_9, /*hidden argument*/NULL);
		Aabb2_t1 * L_10 = V_0;
		return L_10;
	}
}
// UnityEngine.Vector2 Aabb2::GetTopLeft()
extern "C" Vector2_t2  Aabb2_GetTopLeft_m16 (Aabb2_t1 * __this, const MethodInfo* method)
{
	{
		Vector2_t2 * L_0 = &(__this->___min_1);
		float L_1 = (L_0->___x_1);
		Vector2_t2 * L_2 = &(__this->___max_2);
		float L_3 = (L_2->___y_2);
		Vector2_t2  L_4 = {0};
		Vector2__ctor_m1309(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 Aabb2::GetBottomLeft()
extern "C" Vector2_t2  Aabb2_GetBottomLeft_m17 (Aabb2_t1 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___min_1);
		return L_0;
	}
}
// UnityEngine.Vector2 Aabb2::GetTopRight()
extern "C" Vector2_t2  Aabb2_GetTopRight_m18 (Aabb2_t1 * __this, const MethodInfo* method)
{
	{
		Vector2_t2  L_0 = (__this->___max_2);
		return L_0;
	}
}
// UnityEngine.Vector2 Aabb2::GetBottomRight()
extern "C" Vector2_t2  Aabb2_GetBottomRight_m19 (Aabb2_t1 * __this, const MethodInfo* method)
{
	{
		Vector2_t2 * L_0 = &(__this->___max_2);
		float L_1 = (L_0->___x_1);
		Vector2_t2 * L_2 = &(__this->___min_1);
		float L_3 = (L_2->___y_2);
		Vector2_t2  L_4 = {0};
		Vector2__ctor_m1309(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String Aabb2::ToString()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2_t2_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern "C" String_t* Aabb2_ToString_m20 (Aabb2_t1 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Vector2_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t356* L_0 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral1;
		ObjectU5BU5D_t356* L_1 = L_0;
		Vector2_t2  L_2 = (__this->___min_1);
		Vector2_t2  L_3 = L_2;
		Object_t * L_4 = Box(Vector2_t2_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t356* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2;
		ObjectU5BU5D_t356* L_6 = L_5;
		Vector2_t2  L_7 = (__this->___max_2);
		Vector2_t2  L_8 = L_7;
		Object_t * L_9 = Box(Vector2_t2_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1316(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_10;
	}
}
// Assertion
#include "AssemblyU2DCSharp_Assertion.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// Common.Logger.Logger
#include "AssemblyU2DCSharp_Common_Logger_Logger.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
// Common.Logger.Logger
#include "AssemblyU2DCSharp_Common_Logger_LoggerMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// System.Void Assertion::Assert(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern Il2CppCodeGenString* _stringLiteral3;
extern "C" void Assertion_Assert_m21 (Object_t * __this /* static, unused */, bool ___expression, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___expression;
		Assertion_Assert_m22(NULL /*static, unused*/, L_0, _stringLiteral3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Assertion::Assert(System.Boolean,System.String)
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
// Common.Logger.Logger
#include "AssemblyU2DCSharp_Common_Logger_LoggerMethodDeclarations.h"
extern TypeInfo* Exception_t359_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t75_il2cpp_TypeInfo_var;
extern "C" void Assertion_Assert_m22 (Object_t * __this /* static, unused */, bool ___expression, String_t* ___assertErrorMessage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t359_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Logger_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t359 * V_0 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___expression;
		if (L_0)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_1 = ___assertErrorMessage;
		Debug_LogError_m1317(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		String_t* L_2 = ___assertErrorMessage;
		Exception_t359 * L_3 = (Exception_t359 *)il2cpp_codegen_object_new (Exception_t359_il2cpp_TypeInfo_var);
		Exception__ctor_m1318(L_3, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
		goto IL_003b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t359 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t359_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0018;
		throw e;
	}

CATCH_0018:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t359 *)__exception_local);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t75_il2cpp_TypeInfo_var);
		Logger_t75 * L_4 = Logger_GetInstance_m258(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = ___assertErrorMessage;
		NullCheck(L_4);
		Logger_LogError_m262(L_4, L_5, /*hidden argument*/NULL);
		Logger_t75 * L_6 = Logger_GetInstance_m258(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t359 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_7);
		NullCheck(L_6);
		Logger_LogError_m262(L_6, L_8, /*hidden argument*/NULL);
		Exception_t359 * L_9 = V_0;
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
		goto IL_003b;
	} // end catch (depth: 1)

IL_003b:
	{
		return;
	}
}
// System.Void Assertion::AssertNotNull(System.Object,System.String)
// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4;
extern "C" void Assertion_AssertNotNull_m23 (Object_t * __this /* static, unused */, Object_t * ___pointer, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___pointer;
		String_t* L_1 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1319(NULL /*static, unused*/, L_1, _stringLiteral4, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)((((Object_t*)(Object_t *)L_0) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Assertion::AssertNotNull(System.Object)
extern Il2CppCodeGenString* _stringLiteral3;
extern "C" void Assertion_AssertNotNull_m24 (Object_t * __this /* static, unused */, Object_t * ___pointer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___pointer;
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)((((Object_t*)(Object_t *)L_0) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), _stringLiteral3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Assertion::AssertNotNull(UnityEngine.Object,System.String)
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4;
extern "C" void Assertion_AssertNotNull_m25 (Object_t * __this /* static, unused */, Object_t335 * ___pointer, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t335 * L_0 = ___pointer;
		bool L_1 = Object_op_Implicit_m1320(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1319(NULL /*static, unused*/, L_2, _stringLiteral4, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, 0, L_3, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void Assertion::AssertNotNull(UnityEngine.Object)
extern Il2CppCodeGenString* _stringLiteral3;
extern "C" void Assertion_AssertNotNull_m26 (Object_t * __this /* static, unused */, Object_t335 * ___pointer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t335 * L_0 = ___pointer;
		bool L_1 = Object_op_Implicit_m1320(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		Assertion_Assert_m22(NULL /*static, unused*/, 0, _stringLiteral3, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Assertion::AssertNotEmpty(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5;
extern "C" void Assertion_AssertNotEmpty_m27 (Object_t * __this /* static, unused */, String_t* ___s, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral5 = il2cpp_codegen_string_literal_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___name;
		String_t* L_3 = String_Concat_m1319(NULL /*static, unused*/, L_2, _stringLiteral5, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)L_1) == ((int32_t)0))? 1 : 0), L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Assertion::AssertNotEmpty(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3;
extern "C" void Assertion_AssertNotEmpty_m28 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)L_1) == ((int32_t)0))? 1 : 0), _stringLiteral3, /*hidden argument*/NULL);
		return;
	}
}
// OrthographicCamera
#include "AssemblyU2DCSharp_OrthographicCamera.h"
// OrthographicCamera
#include "AssemblyU2DCSharp_OrthographicCameraMethodDeclarations.h"
// System.Collections.Generic.List`1<OrthographicCameraObserver>
#include "mscorlib_System_Collections_Generic_List_1_gen_0.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// System.Collections.Generic.List`1<OrthographicCameraObserver>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
struct Camera_t6;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m1326_gshared (Component_t365 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m1326(__this, method) (( Object_t * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t6_m1324(__this, method) (( Camera_t6 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void OrthographicCamera::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void OrthographicCamera__ctor_m29 (OrthographicCamera_t4 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrthographicCamera::Awake()
// System.Collections.Generic.List`1<OrthographicCameraObserver>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
extern TypeInfo* List_1_t360_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1323_MethodInfo_var;
extern "C" void OrthographicCamera_Awake_m30 (OrthographicCamera_t4 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t360_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		List_1__ctor_m1323_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t360 * L_0 = (List_1_t360 *)il2cpp_codegen_object_new (List_1_t360_il2cpp_TypeInfo_var);
		List_1__ctor_m1323(L_0, /*hidden argument*/List_1__ctor_m1323_MethodInfo_var);
		__this->___observerList_3 = L_0;
		return;
	}
}
// System.Void OrthographicCamera::Start()
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisCamera_t6_m1324_MethodInfo_var;
extern "C" void OrthographicCamera_Start_m31 (OrthographicCamera_t4 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t6_m1324_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t6 * L_0 = Component_GetComponent_TisCamera_t6_m1324(__this, /*hidden argument*/Component_GetComponent_TisCamera_t6_m1324_MethodInfo_var);
		__this->___selfCamera_2 = L_0;
		return;
	}
}
// System.Void OrthographicCamera::AddObserver(OrthographicCameraObserver)
extern TypeInfo* ICollection_1_t361_il2cpp_TypeInfo_var;
extern "C" void OrthographicCamera_AddObserver_m32 (OrthographicCamera_t4 * __this, Object_t * ___observer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_1_t361_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (__this->___observerList_3);
		Object_t * L_1 = ___observer;
		NullCheck(L_0);
		InterfaceActionInvoker1< Object_t * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<OrthographicCameraObserver>::Add(!0) */, ICollection_1_t361_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void OrthographicCamera::SetOrthoSize(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
extern TypeInfo* IEnumerable_1_t362_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t363_il2cpp_TypeInfo_var;
extern TypeInfo* OrthographicCameraObserver_t336_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void OrthographicCamera_SetOrthoSize_m33 (OrthographicCamera_t4 * __this, float ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_1_t362_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		IEnumerator_1_t363_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		OrthographicCameraObserver_t336_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t* V_1 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Camera_t6 * L_0 = (__this->___selfCamera_2);
		float L_1 = ___size;
		NullCheck(L_0);
		Camera_set_orthographicSize_m1325(L_0, L_1, /*hidden argument*/NULL);
		Object_t* L_2 = (__this->___observerList_3);
		NullCheck(L_2);
		Object_t* L_3 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<OrthographicCameraObserver>::GetEnumerator() */, IEnumerable_1_t362_il2cpp_TypeInfo_var, L_2);
		V_1 = L_3;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002b;
		}

IL_001d:
		{
			Object_t* L_4 = V_1;
			NullCheck(L_4);
			Object_t * L_5 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<OrthographicCameraObserver>::get_Current() */, IEnumerator_1_t363_il2cpp_TypeInfo_var, L_4);
			V_0 = L_5;
			Object_t * L_6 = V_0;
			float L_7 = ___size;
			NullCheck(L_6);
			InterfaceActionInvoker1< float >::Invoke(0 /* System.Void OrthographicCameraObserver::OnChangeOrthoSize(System.Single) */, OrthographicCameraObserver_t336_il2cpp_TypeInfo_var, L_6, L_7);
		}

IL_002b:
		{
			Object_t* L_8 = V_1;
			NullCheck(L_8);
			bool L_9 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_001d;
			}
		}

IL_0036:
		{
			IL2CPP_LEAVE(0x46, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		{
			Object_t* L_10 = V_1;
			if (L_10)
			{
				goto IL_003f;
			}
		}

IL_003e:
		{
			IL2CPP_END_FINALLY(59)
		}

IL_003f:
		{
			Object_t* L_11 = V_1;
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_11);
			IL2CPP_END_FINALLY(59)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0046:
	{
		return;
	}
}
// Common.ColorUtils
#include "AssemblyU2DCSharp_Common_ColorUtils.h"
// Common.ColorUtils
#include "AssemblyU2DCSharp_Common_ColorUtilsMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// System.Void Common.ColorUtils::.cctor()
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
extern TypeInfo* ColorUtils_t8_il2cpp_TypeInfo_var;
extern "C" void ColorUtils__cctor_m34 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ColorUtils_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t9  L_0 = {0};
		Color__ctor_m1327(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((ColorUtils_t8_StaticFields*)ColorUtils_t8_il2cpp_TypeInfo_var->static_fields)->___BLACK_0 = L_0;
		Color_t9  L_1 = {0};
		Color__ctor_m1327(&L_1, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((ColorUtils_t8_StaticFields*)ColorUtils_t8_il2cpp_TypeInfo_var->static_fields)->___WHITE_1 = L_1;
		Color_t9  L_2 = {0};
		Color__ctor_m1327(&L_2, (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((ColorUtils_t8_StaticFields*)ColorUtils_t8_il2cpp_TypeInfo_var->static_fields)->___GREEN_2 = L_2;
		return;
	}
}
// UnityEngine.Color Common.ColorUtils::NewColor(System.Int32,System.Int32,System.Int32,System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" Color_t9  ColorUtils_NewColor_m35 (Object_t * __this /* static, unused */, int32_t ___r, int32_t ___g, int32_t ___b, int32_t ___a, const MethodInfo* method)
{
	{
		int32_t L_0 = ___r;
		int32_t L_1 = ___g;
		int32_t L_2 = ___b;
		int32_t L_3 = ___a;
		Color_t9  L_4 = {0};
		Color__ctor_m1327(&L_4, ((float)((float)(((float)L_0))/(float)(255.0f))), ((float)((float)(((float)L_1))/(float)(255.0f))), ((float)((float)(((float)L_2))/(float)(255.0f))), ((float)((float)(((float)L_3))/(float)(255.0f))), /*hidden argument*/NULL);
		return L_4;
	}
}
// Comment
#include "AssemblyU2DCSharp_Comment.h"
// Comment
#include "AssemblyU2DCSharp_CommentMethodDeclarations.h"
// System.Void Comment::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void Comment__ctor_m36 (Comment_t10 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Comment::Awake()
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern "C" void Comment_Awake_m37 (Comment_t10 * __this, const MethodInfo* method)
{
	{
		Object_DestroyImmediate_m1328(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Comment::get_Text()
extern "C" String_t* Comment_get_Text_m38 (Comment_t10 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___text_2);
		return L_0;
	}
}
// System.Void Comment::set_Text(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" void Comment_set_Text_m39 (Comment_t10 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___text_2 = L_0;
		return;
	}
}
// CommonUtils
#include "AssemblyU2DCSharp_CommonUtils.h"
// CommonUtils
#include "AssemblyU2DCSharp_CommonUtilsMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// System.IO.StreamReader
#include "mscorlib_System_IO_StreamReader.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
// System.IO.File
#include "mscorlib_System_IO_FileMethodDeclarations.h"
// System.IO.StreamReader
#include "mscorlib_System_IO_StreamReaderMethodDeclarations.h"
struct RendererU5BU5D_t366;
struct ObjectU5BU5D_t356;
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t356* GameObject_GetComponentsInChildren_TisObject_t_m1355_gshared (GameObject_t155 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisObject_t_m1355(__this, method) (( ObjectU5BU5D_t356* (*) (GameObject_t155 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m1355_gshared)(__this, method)
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Renderer>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Renderer>()
#define GameObject_GetComponentsInChildren_TisRenderer_t367_m1333(__this, method) (( RendererU5BU5D_t366* (*) (GameObject_t155 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m1355_gshared)(__this, method)
struct RendererU5BU5D_t366;
struct ObjectU5BU5D_t356;
// Declaration !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C" ObjectU5BU5D_t356* GameObject_GetComponents_TisObject_t_m1356_gshared (GameObject_t155 * __this, const MethodInfo* method);
#define GameObject_GetComponents_TisObject_t_m1356(__this, method) (( ObjectU5BU5D_t356* (*) (GameObject_t155 *, const MethodInfo*))GameObject_GetComponents_TisObject_t_m1356_gshared)(__this, method)
// Declaration !!0[] UnityEngine.GameObject::GetComponents<UnityEngine.Renderer>()
// !!0[] UnityEngine.GameObject::GetComponents<UnityEngine.Renderer>()
#define GameObject_GetComponents_TisRenderer_t367_m1334(__this, method) (( RendererU5BU5D_t366* (*) (GameObject_t155 *, const MethodInfo*))GameObject_GetComponents_TisObject_t_m1356_gshared)(__this, method)
struct MonoBehaviourU5BU5D_t368;
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.MonoBehaviour>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.MonoBehaviour>()
#define GameObject_GetComponentsInChildren_TisMonoBehaviour_t5_m1345(__this, method) (( MonoBehaviourU5BU5D_t368* (*) (GameObject_t155 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m1355_gshared)(__this, method)
struct ColliderU5BU5D_t59;
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Collider>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Collider>()
#define GameObject_GetComponentsInChildren_TisCollider_t369_m1347(__this, method) (( ColliderU5BU5D_t59* (*) (GameObject_t155 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m1355_gshared)(__this, method)
// System.Void CommonUtils::SetAsParent(UnityEngine.Transform,UnityEngine.Transform)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern "C" void CommonUtils_SetAsParent_m40 (Object_t * __this /* static, unused */, Transform_t35 * ___parentTransform, Transform_t35 * ___childTransform, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = ___childTransform;
		Transform_t35 * L_1 = ___parentTransform;
		NullCheck(L_0);
		Transform_set_parent_m1329(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CommonUtils::SeedRandomizer()
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
extern "C" void CommonUtils_SeedRandomizer_m41 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		float L_0 = Time_get_time_m1330(NULL /*static, unused*/, /*hidden argument*/NULL);
		Random_set_seed_m1331(NULL /*static, unused*/, (((int32_t)((float)((float)L_0*(float)(10000.0f))))), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CommonUtils::RandomBoolean()
extern "C" bool CommonUtils_RandomBoolean_m42 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Random_Range_m1332(NULL /*static, unused*/, 0, 2, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		return ((((int32_t)L_1) > ((int32_t)0))? 1 : 0);
	}
}
// System.Void CommonUtils::HideObject(UnityEngine.GameObject,System.Boolean)
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
extern const MethodInfo* GameObject_GetComponentsInChildren_TisRenderer_t367_m1333_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponents_TisRenderer_t367_m1334_MethodInfo_var;
extern "C" void CommonUtils_HideObject_m43 (Object_t * __this /* static, unused */, GameObject_t155 * ___gameObject, bool ___recurseChildren, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponentsInChildren_TisRenderer_t367_m1333_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		GameObject_GetComponents_TisRenderer_t367_m1334_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t366* V_0 = {0};
	Renderer_t367 * V_1 = {0};
	RendererU5BU5D_t366* V_2 = {0};
	int32_t V_3 = 0;
	{
		V_0 = (RendererU5BU5D_t366*)NULL;
		bool L_0 = ___recurseChildren;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		GameObject_t155 * L_1 = ___gameObject;
		NullCheck(L_1);
		RendererU5BU5D_t366* L_2 = GameObject_GetComponentsInChildren_TisRenderer_t367_m1333(L_1, /*hidden argument*/GameObject_GetComponentsInChildren_TisRenderer_t367_m1333_MethodInfo_var);
		V_0 = L_2;
		goto IL_001b;
	}

IL_0014:
	{
		GameObject_t155 * L_3 = ___gameObject;
		NullCheck(L_3);
		RendererU5BU5D_t366* L_4 = GameObject_GetComponents_TisRenderer_t367_m1334(L_3, /*hidden argument*/GameObject_GetComponents_TisRenderer_t367_m1334_MethodInfo_var);
		V_0 = L_4;
	}

IL_001b:
	{
		RendererU5BU5D_t366* L_5 = V_0;
		V_2 = L_5;
		V_3 = 0;
		goto IL_0033;
	}

IL_0024:
	{
		RendererU5BU5D_t366* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_1 = (*(Renderer_t367 **)(Renderer_t367 **)SZArrayLdElema(L_6, L_8, sizeof(Renderer_t367 *)));
		Renderer_t367 * L_9 = V_1;
		NullCheck(L_9);
		Renderer_set_enabled_m1335(L_9, 0, /*hidden argument*/NULL);
		int32_t L_10 = V_3;
		V_3 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_11 = V_3;
		RendererU5BU5D_t366* L_12 = V_2;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_12)->max_length))))))
		{
			goto IL_0024;
		}
	}
	{
		return;
	}
}
// System.Void CommonUtils::ShowObject(UnityEngine.GameObject,System.Boolean)
extern const MethodInfo* GameObject_GetComponentsInChildren_TisRenderer_t367_m1333_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponents_TisRenderer_t367_m1334_MethodInfo_var;
extern "C" void CommonUtils_ShowObject_m44 (Object_t * __this /* static, unused */, GameObject_t155 * ___gameObject, bool ___recurseChildren, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponentsInChildren_TisRenderer_t367_m1333_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		GameObject_GetComponents_TisRenderer_t367_m1334_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t366* V_0 = {0};
	Renderer_t367 * V_1 = {0};
	RendererU5BU5D_t366* V_2 = {0};
	int32_t V_3 = 0;
	{
		V_0 = (RendererU5BU5D_t366*)NULL;
		bool L_0 = ___recurseChildren;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		GameObject_t155 * L_1 = ___gameObject;
		NullCheck(L_1);
		RendererU5BU5D_t366* L_2 = GameObject_GetComponentsInChildren_TisRenderer_t367_m1333(L_1, /*hidden argument*/GameObject_GetComponentsInChildren_TisRenderer_t367_m1333_MethodInfo_var);
		V_0 = L_2;
		goto IL_001b;
	}

IL_0014:
	{
		GameObject_t155 * L_3 = ___gameObject;
		NullCheck(L_3);
		RendererU5BU5D_t366* L_4 = GameObject_GetComponents_TisRenderer_t367_m1334(L_3, /*hidden argument*/GameObject_GetComponents_TisRenderer_t367_m1334_MethodInfo_var);
		V_0 = L_4;
	}

IL_001b:
	{
		RendererU5BU5D_t366* L_5 = V_0;
		V_2 = L_5;
		V_3 = 0;
		goto IL_0033;
	}

IL_0024:
	{
		RendererU5BU5D_t366* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_1 = (*(Renderer_t367 **)(Renderer_t367 **)SZArrayLdElema(L_6, L_8, sizeof(Renderer_t367 *)));
		Renderer_t367 * L_9 = V_1;
		NullCheck(L_9);
		Renderer_set_enabled_m1335(L_9, 1, /*hidden argument*/NULL);
		int32_t L_10 = V_3;
		V_3 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_11 = V_3;
		RendererU5BU5D_t366* L_12 = V_2;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_12)->max_length))))))
		{
			goto IL_0024;
		}
	}
	{
		return;
	}
}
// System.Single CommonUtils::Clamp(System.Single,System.Single,System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" float CommonUtils_Clamp_m45 (Object_t * __this /* static, unused */, float ___aValue, float ___min, float ___max, const MethodInfo* method)
{
	{
		float L_0 = ___aValue;
		float L_1 = ___min;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		float L_2 = ___min;
		return L_2;
	}

IL_0009:
	{
		float L_3 = ___aValue;
		float L_4 = ___max;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0012;
		}
	}
	{
		float L_5 = ___max;
		return L_5;
	}

IL_0012:
	{
		float L_6 = ___aValue;
		return L_6;
	}
}
// System.Int32 CommonUtils::Clamp(System.Int32,System.Int32,System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" int32_t CommonUtils_Clamp_m46 (Object_t * __this /* static, unused */, int32_t ___aValue, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		int32_t L_0 = ___aValue;
		int32_t L_1 = ___min;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_2 = ___min;
		return L_2;
	}

IL_0009:
	{
		int32_t L_3 = ___aValue;
		int32_t L_4 = ___max;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_5 = ___max;
		return L_5;
	}

IL_0012:
	{
		int32_t L_6 = ___aValue;
		return L_6;
	}
}
// System.Boolean CommonUtils::IsEmpty(System.String)
// System.String
#include "mscorlib_System_String.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern "C" bool CommonUtils_IsEmpty_m47 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___str;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___str;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1336(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
	}

IL_0012:
	{
		return G_B3_0;
	}
}
// System.Boolean CommonUtils::ContainsObjectWithName(UnityEngine.Transform,System.String)
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern "C" bool CommonUtils_ContainsObjectWithName_m48 (Object_t * __this /* static, unused */, Transform_t35 * ___transform, String_t* ___name, const MethodInfo* method)
{
	Transform_t35 * V_0 = {0};
	Transform_t35 * V_1 = {0};
	{
		String_t* L_0 = ___name;
		Transform_t35 * L_1 = ___transform;
		NullCheck(L_1);
		GameObject_t155 * L_2 = Component_get_gameObject_m1337(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m1338(L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_4 = String_Equals_m1339(L_0, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0018;
		}
	}
	{
		return 1;
	}

IL_0018:
	{
		Transform_t35 * L_5 = ___transform;
		String_t* L_6 = ___name;
		NullCheck(L_5);
		Transform_t35 * L_7 = Transform_Find_m1340(L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Transform_t35 * L_8 = V_0;
		bool L_9 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_8, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		Transform_t35 * L_10 = ___transform;
		NullCheck(L_10);
		Transform_t35 * L_11 = Transform_get_parent_m1342(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
	}

IL_0035:
	{
		Transform_t35 * L_12 = V_1;
		bool L_13 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_12, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0060;
		}
	}
	{
		String_t* L_14 = ___name;
		Transform_t35 * L_15 = V_1;
		NullCheck(L_15);
		GameObject_t155 * L_16 = Component_get_gameObject_m1337(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		String_t* L_17 = Object_get_name_m1338(L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		bool L_18 = String_Equals_m1339(L_14, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0059;
		}
	}
	{
		return 1;
	}

IL_0059:
	{
		Transform_t35 * L_19 = V_1;
		NullCheck(L_19);
		Transform_t35 * L_20 = Transform_get_parent_m1342(L_19, /*hidden argument*/NULL);
		V_1 = L_20;
	}

IL_0060:
	{
		Transform_t35 * L_21 = V_1;
		bool L_22 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_21, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_0035;
		}
	}
	{
		return 0;
	}
}
// UnityEngine.Transform CommonUtils::FindTransformByName(UnityEngine.Transform,System.String)
// CommonUtils
#include "AssemblyU2DCSharp_CommonUtilsMethodDeclarations.h"
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t35_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" Transform_t35 * CommonUtils_FindTransformByName_m49 (Object_t * __this /* static, unused */, Transform_t35 * ___transformRoot, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		Transform_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t35 * V_0 = {0};
	Transform_t35 * V_1 = {0};
	Object_t * V_2 = {0};
	Transform_t35 * V_3 = {0};
	Object_t * V_4 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name;
		Transform_t35 * L_1 = ___transformRoot;
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m1338(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_3 = String_Equals_m1339(L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0013;
		}
	}
	{
		Transform_t35 * L_4 = ___transformRoot;
		return L_4;
	}

IL_0013:
	{
		V_0 = (Transform_t35 *)NULL;
		Transform_t35 * L_5 = ___transformRoot;
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_5);
		V_2 = L_6;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0048;
		}

IL_0021:
		{
			Object_t * L_7 = V_2;
			NullCheck(L_7);
			Object_t * L_8 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_7);
			V_1 = ((Transform_t35 *)CastclassClass(L_8, Transform_t35_il2cpp_TypeInfo_var));
			Transform_t35 * L_9 = V_1;
			String_t* L_10 = ___name;
			Transform_t35 * L_11 = CommonUtils_FindTransformByName_m49(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
			V_0 = L_11;
			Transform_t35 * L_12 = V_0;
			bool L_13 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_12, (Object_t335 *)NULL, /*hidden argument*/NULL);
			if (!L_13)
			{
				goto IL_0048;
			}
		}

IL_0041:
		{
			Transform_t35 * L_14 = V_0;
			V_3 = L_14;
			IL2CPP_LEAVE(0x6F, FINALLY_0058);
		}

IL_0048:
		{
			Object_t * L_15 = V_2;
			NullCheck(L_15);
			bool L_16 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0021;
			}
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x6D, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		{
			Object_t * L_17 = V_2;
			V_4 = ((Object_t *)IsInst(L_17, IDisposable_t364_il2cpp_TypeInfo_var));
			Object_t * L_18 = V_4;
			if (L_18)
			{
				goto IL_0065;
			}
		}

IL_0064:
		{
			IL2CPP_END_FINALLY(88)
		}

IL_0065:
		{
			Object_t * L_19 = V_4;
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_19);
			IL2CPP_END_FINALLY(88)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_006d:
	{
		return (Transform_t35 *)NULL;
	}

IL_006f:
	{
		Transform_t35 * L_20 = V_3;
		return L_20;
	}
}
// System.Single CommonUtils::ComputeTravelTime(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
extern "C" float CommonUtils_ComputeTravelTime_m50 (Object_t * __this /* static, unused */, Vector3_t36  ___start, Vector3_t36  ___destination, float ___velocity, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t36  V_1 = {0};
	{
		Vector3_t36  L_0 = ___destination;
		Vector3_t36  L_1 = ___start;
		Vector3_t36  L_2 = Vector3_op_Subtraction_m1343(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Vector3_get_magnitude_m1344((&V_1), /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = V_0;
		float L_5 = ___velocity;
		return ((float)((float)L_4/(float)L_5));
	}
}
// System.Boolean CommonUtils::TolerantEquals(UnityEngine.Vector3,UnityEngine.Vector3)
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
extern "C" bool CommonUtils_TolerantEquals_m51 (Object_t * __this /* static, unused */, Vector3_t36  ___a, Vector3_t36  ___b, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		bool L_2 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		float L_3 = ((&___a)->___y_2);
		float L_4 = ((&___b)->___y_2);
		bool L_5 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0045;
		}
	}
	{
		float L_6 = ((&___a)->___z_3);
		float L_7 = ((&___b)->___z_3);
		bool L_8 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_8));
		goto IL_0046;
	}

IL_0045:
	{
		G_B4_0 = 0;
	}

IL_0046:
	{
		return G_B4_0;
	}
}
// System.Single CommonUtils::RandomSign()
extern "C" float CommonUtils_RandomSign_m52 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		int32_t L_0 = Random_Range_m1332(NULL /*static, unused*/, (-1), 1, /*hidden argument*/NULL);
		V_0 = (((float)L_0));
		float L_1 = V_0;
		if ((!(((float)L_1) < ((float)(0.0f)))))
		{
			goto IL_001a;
		}
	}
	{
		return (-1.0f);
	}

IL_001a:
	{
		return (1.0f);
	}
}
// System.Void CommonUtils::SetAllScriptsEnabled(UnityEngine.GameObject,System.Boolean,System.Boolean)
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t35_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisMonoBehaviour_t5_m1345_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisCollider_t369_m1347_MethodInfo_var;
extern "C" void CommonUtils_SetAllScriptsEnabled_m53 (Object_t * __this /* static, unused */, GameObject_t155 * ___gameObject, bool ___enabled, bool ___traverseChildren, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		Transform_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GameObject_GetComponentsInChildren_TisMonoBehaviour_t5_m1345_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		GameObject_GetComponentsInChildren_TisCollider_t369_m1347_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		s_Il2CppMethodIntialized = true;
	}
	MonoBehaviourU5BU5D_t368* V_0 = {0};
	MonoBehaviour_t5 * V_1 = {0};
	MonoBehaviourU5BU5D_t368* V_2 = {0};
	int32_t V_3 = 0;
	ColliderU5BU5D_t59* V_4 = {0};
	Collider_t369 * V_5 = {0};
	ColliderU5BU5D_t59* V_6 = {0};
	int32_t V_7 = 0;
	Transform_t35 * V_8 = {0};
	Transform_t35 * V_9 = {0};
	Object_t * V_10 = {0};
	Object_t * V_11 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t155 * L_0 = ___gameObject;
		Assertion_AssertNotNull_m26(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		GameObject_t155 * L_1 = ___gameObject;
		NullCheck(L_1);
		MonoBehaviourU5BU5D_t368* L_2 = GameObject_GetComponentsInChildren_TisMonoBehaviour_t5_m1345(L_1, /*hidden argument*/GameObject_GetComponentsInChildren_TisMonoBehaviour_t5_m1345_MethodInfo_var);
		V_0 = L_2;
		MonoBehaviourU5BU5D_t368* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_0025;
	}

IL_0016:
	{
		MonoBehaviourU5BU5D_t368* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_1 = (*(MonoBehaviour_t5 **)(MonoBehaviour_t5 **)SZArrayLdElema(L_4, L_6, sizeof(MonoBehaviour_t5 *)));
		MonoBehaviour_t5 * L_7 = V_1;
		bool L_8 = ___enabled;
		NullCheck(L_7);
		Behaviour_set_enabled_m1346(L_7, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_10 = V_3;
		MonoBehaviourU5BU5D_t368* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)(((Array_t *)L_11)->max_length))))))
		{
			goto IL_0016;
		}
	}
	{
		bool L_12 = ___enabled;
		if (!L_12)
		{
			goto IL_0040;
		}
	}
	{
		GameObject_t155 * L_13 = ___gameObject;
		CommonUtils_ShowObject_m44(NULL /*static, unused*/, L_13, 1, /*hidden argument*/NULL);
		goto IL_0047;
	}

IL_0040:
	{
		GameObject_t155 * L_14 = ___gameObject;
		CommonUtils_HideObject_m43(NULL /*static, unused*/, L_14, 1, /*hidden argument*/NULL);
	}

IL_0047:
	{
		GameObject_t155 * L_15 = ___gameObject;
		NullCheck(L_15);
		ColliderU5BU5D_t59* L_16 = GameObject_GetComponentsInChildren_TisCollider_t369_m1347(L_15, /*hidden argument*/GameObject_GetComponentsInChildren_TisCollider_t369_m1347_MethodInfo_var);
		V_4 = L_16;
		ColliderU5BU5D_t59* L_17 = V_4;
		V_6 = L_17;
		V_7 = 0;
		goto IL_0070;
	}

IL_005b:
	{
		ColliderU5BU5D_t59* L_18 = V_6;
		int32_t L_19 = V_7;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		V_5 = (*(Collider_t369 **)(Collider_t369 **)SZArrayLdElema(L_18, L_20, sizeof(Collider_t369 *)));
		Collider_t369 * L_21 = V_5;
		bool L_22 = ___enabled;
		NullCheck(L_21);
		Collider_set_enabled_m1348(L_21, L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_7;
		V_7 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0070:
	{
		int32_t L_24 = V_7;
		ColliderU5BU5D_t59* L_25 = V_6;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)(((Array_t *)L_25)->max_length))))))
		{
			goto IL_005b;
		}
	}
	{
		bool L_26 = ___traverseChildren;
		if (!L_26)
		{
			goto IL_00da;
		}
	}
	{
		GameObject_t155 * L_27 = ___gameObject;
		NullCheck(L_27);
		Transform_t35 * L_28 = GameObject_get_transform_m1349(L_27, /*hidden argument*/NULL);
		V_8 = L_28;
		Transform_t35 * L_29 = V_8;
		NullCheck(L_29);
		Object_t * L_30 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_29);
		V_10 = L_30;
	}

IL_0092:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b3;
		}

IL_0097:
		{
			Object_t * L_31 = V_10;
			NullCheck(L_31);
			Object_t * L_32 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_31);
			V_9 = ((Transform_t35 *)CastclassClass(L_32, Transform_t35_il2cpp_TypeInfo_var));
			Transform_t35 * L_33 = V_9;
			NullCheck(L_33);
			GameObject_t155 * L_34 = Component_get_gameObject_m1337(L_33, /*hidden argument*/NULL);
			bool L_35 = ___enabled;
			bool L_36 = ___traverseChildren;
			CommonUtils_SetAllScriptsEnabled_m53(NULL /*static, unused*/, L_34, L_35, L_36, /*hidden argument*/NULL);
		}

IL_00b3:
		{
			Object_t * L_37 = V_10;
			NullCheck(L_37);
			bool L_38 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_37);
			if (L_38)
			{
				goto IL_0097;
			}
		}

IL_00bf:
		{
			IL2CPP_LEAVE(0xDA, FINALLY_00c4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_00c4;
	}

FINALLY_00c4:
	{ // begin finally (depth: 1)
		{
			Object_t * L_39 = V_10;
			V_11 = ((Object_t *)IsInst(L_39, IDisposable_t364_il2cpp_TypeInfo_var));
			Object_t * L_40 = V_11;
			if (L_40)
			{
				goto IL_00d2;
			}
		}

IL_00d1:
		{
			IL2CPP_END_FINALLY(196)
		}

IL_00d2:
		{
			Object_t * L_41 = V_11;
			NullCheck(L_41);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_41);
			IL2CPP_END_FINALLY(196)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(196)
	{
		IL2CPP_JUMP_TBL(0xDA, IL_00da)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_00da:
	{
		return;
	}
}
// System.Void CommonUtils::SetLayer(UnityEngine.GameObject,System.String,System.Boolean)
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
extern "C" void CommonUtils_SetLayer_m54 (Object_t * __this /* static, unused */, GameObject_t155 * ___go, String_t* ___layerName, bool ___recurseToChildren, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___layerName;
		int32_t L_1 = LayerMask_NameToLayer_m1350(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t155 * L_2 = ___go;
		int32_t L_3 = V_0;
		bool L_4 = ___recurseToChildren;
		CommonUtils_SetLayer_m55(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CommonUtils::SetLayer(UnityEngine.GameObject,System.Int32,System.Boolean)
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t35_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void CommonUtils_SetLayer_m55 (Object_t * __this /* static, unused */, GameObject_t155 * ___go, int32_t ___layer, bool ___recurseToChildren, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		Transform_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t35 * V_0 = {0};
	Transform_t35 * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t155 * L_0 = ___go;
		Assertion_AssertNotNull_m26(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		GameObject_t155 * L_1 = ___go;
		int32_t L_2 = ___layer;
		NullCheck(L_1);
		GameObject_set_layer_m1351(L_1, L_2, /*hidden argument*/NULL);
		bool L_3 = ___recurseToChildren;
		if (L_3)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		GameObject_t155 * L_4 = ___go;
		NullCheck(L_4);
		Transform_t35 * L_5 = GameObject_get_transform_m1349(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t35 * L_6 = V_0;
		NullCheck(L_6);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_6);
		V_2 = L_7;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0040;
		}

IL_0027:
		{
			Object_t * L_8 = V_2;
			NullCheck(L_8);
			Object_t * L_9 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_8);
			V_1 = ((Transform_t35 *)CastclassClass(L_9, Transform_t35_il2cpp_TypeInfo_var));
			Transform_t35 * L_10 = V_1;
			NullCheck(L_10);
			GameObject_t155 * L_11 = Component_get_gameObject_m1337(L_10, /*hidden argument*/NULL);
			int32_t L_12 = ___layer;
			bool L_13 = ___recurseToChildren;
			CommonUtils_SetLayer_m55(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		}

IL_0040:
		{
			Object_t * L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0027;
			}
		}

IL_004b:
		{
			IL2CPP_LEAVE(0x62, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		{
			Object_t * L_16 = V_2;
			V_3 = ((Object_t *)IsInst(L_16, IDisposable_t364_il2cpp_TypeInfo_var));
			Object_t * L_17 = V_3;
			if (L_17)
			{
				goto IL_005b;
			}
		}

IL_005a:
		{
			IL2CPP_END_FINALLY(80)
		}

IL_005b:
		{
			Object_t * L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_18);
			IL2CPP_END_FINALLY(80)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0062:
	{
		return;
	}
}
// System.String CommonUtils::ReadTextFile(System.String)
// System.IO.File
#include "mscorlib_System_IO_FileMethodDeclarations.h"
// System.IO.StreamReader
#include "mscorlib_System_IO_StreamReaderMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StreamReader_t370_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral6;
extern "C" String_t* CommonUtils_ReadTextFile_m56 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		StreamReader_t370_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		_stringLiteral6 = il2cpp_codegen_string_literal_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	StreamReader_t370 * V_0 = {0};
	String_t* V_1 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___path;
		bool L_1 = File_Exists_m1352(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___path;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1319(NULL /*static, unused*/, _stringLiteral6, L_2, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ___path;
		StreamReader_t370 * L_5 = (StreamReader_t370 *)il2cpp_codegen_object_new (StreamReader_t370_il2cpp_TypeInfo_var);
		StreamReader__ctor_m1353(L_5, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			StreamReader_t370 * L_6 = V_0;
			NullCheck(L_6);
			String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.IO.StreamReader::ReadToEnd() */, L_6);
			V_1 = L_7;
			IL2CPP_LEAVE(0x35, FINALLY_002e);
		}

IL_0029:
		{
			; // IL_0029: leave IL_0035
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		StreamReader_t370 * L_8 = V_0;
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(5 /* System.Void System.IO.StreamReader::Close() */, L_8);
		IL2CPP_END_FINALLY(46)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0035:
	{
		String_t* L_9 = V_1;
		return L_9;
	}
}
// System.Void CommonUtils::SetChildrenActiveRecursively(UnityEngine.GameObject,System.Boolean)
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t35_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void CommonUtils_SetChildrenActiveRecursively_m57 (Object_t * __this /* static, unused */, GameObject_t155 * ___gameObject, bool ___active, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		Transform_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t35 * V_0 = {0};
	Transform_t35 * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t155 * L_0 = ___gameObject;
		NullCheck(L_0);
		Transform_t35 * L_1 = GameObject_get_transform_m1349(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t35 * L_2 = V_0;
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_2);
		V_2 = L_3;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002b;
		}

IL_0013:
		{
			Object_t * L_4 = V_2;
			NullCheck(L_4);
			Object_t * L_5 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_4);
			V_1 = ((Transform_t35 *)CastclassClass(L_5, Transform_t35_il2cpp_TypeInfo_var));
			Transform_t35 * L_6 = V_1;
			NullCheck(L_6);
			GameObject_t155 * L_7 = Component_get_gameObject_m1337(L_6, /*hidden argument*/NULL);
			bool L_8 = ___active;
			NullCheck(L_7);
			GameObject_SetActiveRecursively_m1354(L_7, L_8, /*hidden argument*/NULL);
		}

IL_002b:
		{
			Object_t * L_9 = V_2;
			NullCheck(L_9);
			bool L_10 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0013;
			}
		}

IL_0036:
		{
			IL2CPP_LEAVE(0x4D, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		{
			Object_t * L_11 = V_2;
			V_3 = ((Object_t *)IsInst(L_11, IDisposable_t364_il2cpp_TypeInfo_var));
			Object_t * L_12 = V_3;
			if (L_12)
			{
				goto IL_0046;
			}
		}

IL_0045:
		{
			IL2CPP_END_FINALLY(59)
		}

IL_0046:
		{
			Object_t * L_13 = V_3;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_13);
			IL2CPP_END_FINALLY(59)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x4D, IL_004d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_004d:
	{
		return;
	}
}
// Comparison
#include "AssemblyU2DCSharp_Comparison.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// System.Boolean Comparison::TolerantEquals(System.Single,System.Single)
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" bool Comparison_TolerantEquals_m58 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___a;
		float L_1 = ___b;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m1357(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Comparison::TolerantGreaterThanOrEquals(System.Single,System.Single)
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
extern "C" bool Comparison_TolerantGreaterThanOrEquals_m59 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		float L_0 = ___a;
		float L_1 = ___b;
		if ((((float)L_0) > ((float)L_1)))
		{
			goto IL_0010;
		}
	}
	{
		float L_2 = ___a;
		float L_3 = ___b;
		bool L_4 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 1;
	}

IL_0011:
	{
		return G_B3_0;
	}
}
// System.Boolean Comparison::TolerantLesserThanOrEquals(System.Single,System.Single)
extern "C" bool Comparison_TolerantLesserThanOrEquals_m60 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		float L_0 = ___a;
		float L_1 = ___b;
		if ((((float)L_0) < ((float)L_1)))
		{
			goto IL_0010;
		}
	}
	{
		float L_2 = ___a;
		float L_3 = ___b;
		bool L_4 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 1;
	}

IL_0011:
	{
		return G_B3_0;
	}
}
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimer.h"
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimerMethodDeclarations.h"
// Common.Time.TimeReference
#include "AssemblyU2DCSharp_Common_Time_TimeReference.h"
// Common.Time.TimeReferencePool
#include "AssemblyU2DCSharp_Common_Time_TimeReferencePool.h"
// Common.Time.TimeReferencePool
#include "AssemblyU2DCSharp_Common_Time_TimeReferencePoolMethodDeclarations.h"
// Common.Time.TimeReference
#include "AssemblyU2DCSharp_Common_Time_TimeReferenceMethodDeclarations.h"
// System.Void CountdownTimer::.ctor(System.Single,Common.Time.TimeReference)
// System.Single
#include "mscorlib_System_Single.h"
// Common.Time.TimeReference
#include "AssemblyU2DCSharp_Common_Time_TimeReference.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimerMethodDeclarations.h"
extern Il2CppCodeGenString* _stringLiteral7;
extern "C" void CountdownTimer__ctor_m61 (CountdownTimer_t13 * __this, float ___countdownTime, TimeReference_t14 * ___timeReference, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		TimeReference_t14 * L_0 = ___timeReference;
		__this->___timeReference_2 = L_0;
		float L_1 = ___countdownTime;
		Assertion_Assert_m22(NULL /*static, unused*/, ((((float)L_1) > ((float)(0.0f)))? 1 : 0), _stringLiteral7, /*hidden argument*/NULL);
		float L_2 = ___countdownTime;
		CountdownTimer_Reset_m66(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CountdownTimer::.ctor(System.Single,System.String)
// System.String
#include "mscorlib_System_String.h"
// Common.Time.TimeReferencePool
#include "AssemblyU2DCSharp_Common_Time_TimeReferencePoolMethodDeclarations.h"
extern TypeInfo* TimeReferencePool_t120_il2cpp_TypeInfo_var;
extern "C" void CountdownTimer__ctor_m62 (CountdownTimer_t13 * __this, float ___countdownTime, String_t* ___timeReferenceName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeReferencePool_t120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___countdownTime;
		IL2CPP_RUNTIME_CLASS_INIT(TimeReferencePool_t120_il2cpp_TypeInfo_var);
		TimeReferencePool_t120 * L_1 = TimeReferencePool_GetInstance_m418(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = ___timeReferenceName;
		NullCheck(L_1);
		TimeReference_t14 * L_3 = TimeReferencePool_Get_m420(L_1, L_2, /*hidden argument*/NULL);
		CountdownTimer__ctor_m61(__this, L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CountdownTimer::.ctor(System.Single)
// Common.Time.TimeReference
#include "AssemblyU2DCSharp_Common_Time_TimeReferenceMethodDeclarations.h"
extern "C" void CountdownTimer__ctor_m63 (CountdownTimer_t13 * __this, float ___countdownTime, const MethodInfo* method)
{
	{
		float L_0 = ___countdownTime;
		TimeReference_t14 * L_1 = TimeReference_GetDefaultInstance_m413(NULL /*static, unused*/, /*hidden argument*/NULL);
		CountdownTimer__ctor_m61(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CountdownTimer::Update()
extern "C" void CountdownTimer_Update_m64 (CountdownTimer_t13 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___polledTime_0);
		TimeReference_t14 * L_1 = (__this->___timeReference_2);
		NullCheck(L_1);
		float L_2 = TimeReference_get_DeltaTime_m412(L_1, /*hidden argument*/NULL);
		__this->___polledTime_0 = ((float)((float)L_0+(float)L_2));
		return;
	}
}
// System.Void CountdownTimer::Reset()
extern "C" void CountdownTimer_Reset_m65 (CountdownTimer_t13 * __this, const MethodInfo* method)
{
	{
		__this->___polledTime_0 = (0.0f);
		return;
	}
}
// System.Void CountdownTimer::Reset(System.Single)
extern "C" void CountdownTimer_Reset_m66 (CountdownTimer_t13 * __this, float ___countdownTime, const MethodInfo* method)
{
	{
		CountdownTimer_Reset_m65(__this, /*hidden argument*/NULL);
		float L_0 = ___countdownTime;
		__this->___countdownTime_1 = L_0;
		return;
	}
}
// System.Boolean CountdownTimer::HasElapsed()
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
extern "C" bool CountdownTimer_HasElapsed_m67 (CountdownTimer_t13 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___polledTime_0);
		float L_1 = (__this->___countdownTime_1);
		bool L_2 = Comparison_TolerantGreaterThanOrEquals_m59(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single CountdownTimer::GetRatio()
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" float CountdownTimer_GetRatio_m68 (CountdownTimer_t13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = (__this->___polledTime_0);
		float L_1 = (__this->___countdownTime_1);
		V_0 = ((float)((float)L_0/(float)L_1));
		float L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m1358(NULL /*static, unused*/, L_2, (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single CountdownTimer::GetPolledTime()
extern "C" float CountdownTimer_GetPolledTime_m69 (CountdownTimer_t13 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___polledTime_0);
		return L_0;
	}
}
// System.Void CountdownTimer::SetPolledTime(System.Single)
extern "C" void CountdownTimer_SetPolledTime_m70 (CountdownTimer_t13 * __this, float ___polledTime, const MethodInfo* method)
{
	{
		float L_0 = ___polledTime;
		__this->___polledTime_0 = L_0;
		return;
	}
}
// System.Void CountdownTimer::EndTimer()
extern "C" void CountdownTimer_EndTimer_m71 (CountdownTimer_t13 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___countdownTime_1);
		__this->___polledTime_0 = L_0;
		return;
	}
}
// System.Void CountdownTimer::SetCountdownTime(System.Single)
extern "C" void CountdownTimer_SetCountdownTime_m72 (CountdownTimer_t13 * __this, float ___newTime, const MethodInfo* method)
{
	{
		float L_0 = ___newTime;
		__this->___countdownTime_1 = L_0;
		return;
	}
}
// System.Single CountdownTimer::GetCountdownTime()
extern "C" float CountdownTimer_GetCountdownTime_m73 (CountdownTimer_t13 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___countdownTime_1);
		return L_0;
	}
}
// System.String CountdownTimer::GetCountdownTimeString()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral8;
extern "C" String_t* CountdownTimer_GetCountdownTimeString_m74 (CountdownTimer_t13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral8 = il2cpp_codegen_string_literal_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		float L_0 = (__this->___countdownTime_1);
		float L_1 = (__this->___polledTime_0);
		V_0 = ((float)((float)L_0-(float)L_1));
		float L_2 = V_0;
		V_1 = (((int32_t)((float)((float)L_2/(float)(60.0f)))));
		float L_3 = V_0;
		V_2 = ((int32_t)((int32_t)(((int32_t)L_3))%(int32_t)((int32_t)60)));
		int32_t L_4 = V_1;
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_5);
		int32_t L_7 = V_2;
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m1359(NULL /*static, unused*/, _stringLiteral8, L_6, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// Common.DelegateCommand
#include "AssemblyU2DCSharp_Common_DelegateCommand.h"
// Common.DelegateCommand
#include "AssemblyU2DCSharp_Common_DelegateCommandMethodDeclarations.h"
// System.Action
#include "System_Core_System_Action.h"
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
// System.Void Common.DelegateCommand::.ctor(System.Action)
// System.Action
#include "System_Core_System_Action.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void DelegateCommand__ctor_m75 (DelegateCommand_t15 * __this, Action_t16 * ___action, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Action_t16 * L_0 = ___action;
		__this->___action_0 = L_0;
		return;
	}
}
// System.Void Common.DelegateCommand::Execute()
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
extern "C" void DelegateCommand_Execute_m76 (DelegateCommand_t15 * __this, const MethodInfo* method)
{
	{
		Action_t16 * L_0 = (__this->___action_0);
		NullCheck(L_0);
		Action_Invoke_m1360(L_0, /*hidden argument*/NULL);
		return;
	}
}
// DontDestroyOnLoadComponent
#include "AssemblyU2DCSharp_DontDestroyOnLoadComponent.h"
// DontDestroyOnLoadComponent
#include "AssemblyU2DCSharp_DontDestroyOnLoadComponentMethodDeclarations.h"
// System.Void DontDestroyOnLoadComponent::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void DontDestroyOnLoadComponent__ctor_m77 (DontDestroyOnLoadComponent_t17 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DontDestroyOnLoadComponent::Awake()
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern "C" void DontDestroyOnLoadComponent_Awake_m78 (DontDestroyOnLoadComponent_t17 * __this, const MethodInfo* method)
{
	{
		GameObject_t155 * L_0 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m1361(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Common.Extensions.StringEnumExtension
#include "AssemblyU2DCSharp_Common_Extensions_StringEnumExtension.h"
// Common.Extensions.StringEnumExtension
#include "AssemblyU2DCSharp_Common_Extensions_StringEnumExtensionMethodDeclarations.h"
// Common.Extensions.IntEnumExtension
#include "AssemblyU2DCSharp_Common_Extensions_IntEnumExtension.h"
// Common.Extensions.IntEnumExtension
#include "AssemblyU2DCSharp_Common_Extensions_IntEnumExtensionMethodDeclarations.h"
// Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0
#include "AssemblyU2DCSharp_Common_Extensions_EnumExtension_U3CGetFlag.h"
// Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0
#include "AssemblyU2DCSharp_Common_Extensions_EnumExtension_U3CGetFlagMethodDeclarations.h"
// System.Enum
#include "mscorlib_System_Enum.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.UInt64
#include "mscorlib_System_UInt64.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// System.Threading.Interlocked
#include "mscorlib_System_Threading_InterlockedMethodDeclarations.h"
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
struct IEnumerable_1_t339;
struct IEnumerable_t375;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct IEnumerable_1_t376;
struct IEnumerable_t375;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
extern "C" Object_t* Enumerable_Cast_TisObject_t_m1367_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Enumerable_Cast_TisObject_t_m1367(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Enumerable_Cast_TisObject_t_m1367_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Enum>(System.Collections.IEnumerable)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Enum>(System.Collections.IEnumerable)
#define Enumerable_Cast_TisEnum_t21_m1364(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Enumerable_Cast_TisObject_t_m1367_gshared)(__this /* static, unused */, p0, method)
// System.Void Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void U3CGetFlagValuesU3Ec__Iterator0__ctor_m79 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Enum Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::System.Collections.Generic.IEnumerator<System.Enum>.get_Current()
extern "C" Enum_t21 * U3CGetFlagValuesU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_EnumU3E_get_Current_m80 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method)
{
	{
		Enum_t21 * L_0 = (__this->___U24current_6);
		return L_0;
	}
}
// System.Object Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetFlagValuesU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m81 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method)
{
	{
		Enum_t21 * L_0 = (__this->___U24current_6);
		return L_0;
	}
}
// System.Collections.IEnumerator Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
// Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0
#include "AssemblyU2DCSharp_Common_Extensions_EnumExtension_U3CGetFlagMethodDeclarations.h"
extern "C" Object_t * U3CGetFlagValuesU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m82 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = U3CGetFlagValuesU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSystem_EnumU3E_GetEnumerator_m83(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Enum> Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::System.Collections.Generic.IEnumerable<System.Enum>.GetEnumerator()
// System.Threading.Interlocked
#include "mscorlib_System_Threading_InterlockedMethodDeclarations.h"
extern TypeInfo* U3CGetFlagValuesU3Ec__Iterator0_t20_il2cpp_TypeInfo_var;
extern "C" Object_t* U3CGetFlagValuesU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSystem_EnumU3E_GetEnumerator_m83 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetFlagValuesU3Ec__Iterator0_t20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetFlagValuesU3Ec__Iterator0_t20 * V_0 = {0};
	{
		int32_t* L_0 = &(__this->___U24PC_5);
		int32_t L_1 = Interlocked_CompareExchange_m1362(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetFlagValuesU3Ec__Iterator0_t20 * L_2 = (U3CGetFlagValuesU3Ec__Iterator0_t20 *)il2cpp_codegen_object_new (U3CGetFlagValuesU3Ec__Iterator0_t20_il2cpp_TypeInfo_var);
		U3CGetFlagValuesU3Ec__Iterator0__ctor_m79(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CGetFlagValuesU3Ec__Iterator0_t20 * L_3 = V_0;
		Type_t * L_4 = (__this->___U3CU24U3Ep_enumType_7);
		NullCheck(L_3);
		L_3->___p_enumType_1 = L_4;
		U3CGetFlagValuesU3Ec__Iterator0_t20 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::MoveNext()
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"
extern TypeInfo* Enum_t21_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t339_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t22_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t373_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Cast_TisEnum_t21_m1364_MethodInfo_var;
extern "C" bool U3CGetFlagValuesU3Ec__Iterator0_MoveNext_m84 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enum_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		IEnumerable_1_t339_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		IEnumerator_1_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		Convert_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Enumerable_Cast_TisEnum_t21_m1364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___U24PC_5);
		V_0 = L_0;
		__this->___U24PC_5 = (-1);
		V_1 = 0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0049;
		}
	}
	{
		goto IL_0110;
	}

IL_0023:
	{
		__this->___U3CflagU3E__0_0 = (((int64_t)1));
		Type_t * L_2 = (__this->___p_enumType_1);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t21_il2cpp_TypeInfo_var);
		Array_t * L_3 = Enum_GetValues_m1363(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Object_t* L_4 = Enumerable_Cast_TisEnum_t21_m1364(NULL /*static, unused*/, L_3, /*hidden argument*/Enumerable_Cast_TisEnum_t21_m1364_MethodInfo_var);
		NullCheck(L_4);
		Object_t* L_5 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Enum>::GetEnumerator() */, IEnumerable_1_t339_il2cpp_TypeInfo_var, L_4);
		__this->___U3CU24s_14U3E__1_2 = L_5;
		V_0 = ((int32_t)-3);
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_6 = V_0;
			if (((int32_t)((int32_t)L_6-(int32_t)1)) == 0)
			{
				goto IL_00db;
			}
		}

IL_0055:
		{
			goto IL_00db;
		}

IL_005a:
		{
			Object_t* L_7 = (__this->___U3CU24s_14U3E__1_2);
			NullCheck(L_7);
			Enum_t21 * L_8 = (Enum_t21 *)InterfaceFuncInvoker0< Enum_t21 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Enum>::get_Current() */, IEnumerator_1_t22_il2cpp_TypeInfo_var, L_7);
			__this->___U3CvalueU3E__2_3 = L_8;
			Enum_t21 * L_9 = (__this->___U3CvalueU3E__2_3);
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
			uint64_t L_10 = Convert_ToUInt64_m1365(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
			__this->___U3CbitsU3E__3_4 = L_10;
			uint64_t L_11 = (__this->___U3CbitsU3E__3_4);
			if (L_11)
			{
				goto IL_008c;
			}
		}

IL_0087:
		{
			goto IL_00db;
		}

IL_008c:
		{
			goto IL_009f;
		}

IL_0091:
		{
			uint64_t L_12 = (__this->___U3CflagU3E__0_0);
			__this->___U3CflagU3E__0_0 = ((int64_t)((int64_t)L_12<<(int32_t)1));
		}

IL_009f:
		{
			uint64_t L_13 = (__this->___U3CflagU3E__0_0);
			uint64_t L_14 = (__this->___U3CbitsU3E__3_4);
			if ((!(((uint64_t)L_13) >= ((uint64_t)L_14))))
			{
				goto IL_0091;
			}
		}

IL_00b0:
		{
			uint64_t L_15 = (__this->___U3CflagU3E__0_0);
			uint64_t L_16 = (__this->___U3CbitsU3E__3_4);
			if ((!(((uint64_t)L_15) == ((uint64_t)L_16))))
			{
				goto IL_00db;
			}
		}

IL_00c1:
		{
			Enum_t21 * L_17 = (__this->___U3CvalueU3E__2_3);
			__this->___U24current_6 = L_17;
			__this->___U24PC_5 = 1;
			V_1 = 1;
			IL2CPP_LEAVE(0x112, FINALLY_00f0);
		}

IL_00db:
		{
			Object_t* L_18 = (__this->___U3CU24s_14U3E__1_2);
			NullCheck(L_18);
			bool L_19 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_18);
			if (L_19)
			{
				goto IL_005a;
			}
		}

IL_00eb:
		{
			IL2CPP_LEAVE(0x109, FINALLY_00f0);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_00f0;
	}

FINALLY_00f0:
	{ // begin finally (depth: 1)
		{
			bool L_20 = V_1;
			if (!L_20)
			{
				goto IL_00f4;
			}
		}

IL_00f3:
		{
			IL2CPP_END_FINALLY(240)
		}

IL_00f4:
		{
			Object_t* L_21 = (__this->___U3CU24s_14U3E__1_2);
			if (L_21)
			{
				goto IL_00fd;
			}
		}

IL_00fc:
		{
			IL2CPP_END_FINALLY(240)
		}

IL_00fd:
		{
			Object_t* L_22 = (__this->___U3CU24s_14U3E__1_2);
			NullCheck(L_22);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_22);
			IL2CPP_END_FINALLY(240)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(240)
	{
		IL2CPP_JUMP_TBL(0x112, IL_0112)
		IL2CPP_JUMP_TBL(0x109, IL_0109)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0109:
	{
		__this->___U24PC_5 = (-1);
	}

IL_0110:
	{
		return 0;
	}

IL_0112:
	{
		return 1;
	}
	// Dead block : IL_0114: ldloc.2
}
// System.Void Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::Dispose()
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void U3CGetFlagValuesU3Ec__Iterator0_Dispose_m85 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___U24PC_5);
		V_0 = L_0;
		__this->___U24PC_5 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Object_t* L_2 = (__this->___U3CU24s_14U3E__1_2);
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Object_t* L_3 = (__this->___U3CU24s_14U3E__1_2);
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0::Reset()
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern "C" void U3CGetFlagValuesU3Ec__Iterator0_Reset_m86 (U3CGetFlagValuesU3Ec__Iterator0_t20 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1366(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// Common.Extensions.EnumExtension
#include "AssemblyU2DCSharp_Common_Extensions_EnumExtension.h"
// Common.Extensions.EnumExtension
#include "AssemblyU2DCSharp_Common_Extensions_EnumExtensionMethodDeclarations.h"
// System.Collections.Generic.List`1<System.Enum>
#include "mscorlib_System_Collections_Generic_List_1_gen_1.h"
// System.Collections.Generic.List`1<System.Enum>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
struct EnumU5BU5D_t338;
struct IEnumerable_1_t339;
struct ObjectU5BU5D_t356;
struct IEnumerable_1_t376;
// Declaration !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" ObjectU5BU5D_t356* Enumerable_ToArray_TisObject_t_m1374_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisObject_t_m1374(__this /* static, unused */, p0, method) (( ObjectU5BU5D_t356* (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToArray_TisObject_t_m1374_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0[] System.Linq.Enumerable::ToArray<System.Enum>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0[] System.Linq.Enumerable::ToArray<System.Enum>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisEnum_t21_m1369(__this /* static, unused */, p0, method) (( EnumU5BU5D_t338* (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToArray_TisObject_t_m1374_gshared)(__this /* static, unused */, p0, method)
struct IEnumerable_1_t339;
struct IEnumerable_1_t376;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Empty<System.Object>()
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Empty<System.Object>()
extern "C" Object_t* Enumerable_Empty_TisObject_t_m1375_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Enumerable_Empty_TisObject_t_m1375(__this /* static, unused */, method) (( Object_t* (*) (Object_t * /* static, unused */, const MethodInfo*))Enumerable_Empty_TisObject_t_m1375_gshared)(__this /* static, unused */, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Empty<System.Enum>()
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Empty<System.Enum>()
#define Enumerable_Empty_TisEnum_t21_m1371(__this /* static, unused */, method) (( Object_t* (*) (Object_t * /* static, unused */, const MethodInfo*))Enumerable_Empty_TisObject_t_m1375_gshared)(__this /* static, unused */, method)
struct IEnumerable_1_t339;
struct IEnumerable_1_t376;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Reverse<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Reverse<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" Object_t* Enumerable_Reverse_TisObject_t_m1376_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_Reverse_TisObject_t_m1376(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_Reverse_TisObject_t_m1376_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Reverse<System.Enum>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Reverse<System.Enum>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Reverse_TisEnum_t21_m1372(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_Reverse_TisObject_t_m1376_gshared)(__this /* static, unused */, p0, method)
struct IEnumerable_1_t339;
struct IEnumerable_1_t376;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C" Object_t* Enumerable_Take_TisObject_t_m1377_gshared (Object_t * __this /* static, unused */, Object_t* p0, int32_t p1, const MethodInfo* method);
#define Enumerable_Take_TisObject_t_m1377(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, int32_t, const MethodInfo*))Enumerable_Take_TisObject_t_m1377_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Enum>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Enum>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_Take_TisEnum_t21_m1373(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, int32_t, const MethodInfo*))Enumerable_Take_TisObject_t_m1377_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<System.Enum> Common.Extensions.EnumExtension::GetFlags(System.Enum)
// System.Enum
#include "mscorlib_System_Enum.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// Common.Extensions.EnumExtension
#include "AssemblyU2DCSharp_Common_Extensions_EnumExtensionMethodDeclarations.h"
extern TypeInfo* Enum_t21_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Cast_TisEnum_t21_m1364_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisEnum_t21_m1369_MethodInfo_var;
extern "C" Object_t* EnumExtension_GetFlags_m87 (Object_t * __this /* static, unused */, Enum_t21 * ___p_value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enum_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		Enumerable_Cast_TisEnum_t21_m1364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		Enumerable_ToArray_TisEnum_t21_m1369_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enum_t21 * L_0 = ___p_value;
		Enum_t21 * L_1 = ___p_value;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m1368(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t21_il2cpp_TypeInfo_var);
		Array_t * L_3 = Enum_GetValues_m1363(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Object_t* L_4 = Enumerable_Cast_TisEnum_t21_m1364(NULL /*static, unused*/, L_3, /*hidden argument*/Enumerable_Cast_TisEnum_t21_m1364_MethodInfo_var);
		EnumU5BU5D_t338* L_5 = Enumerable_ToArray_TisEnum_t21_m1369(NULL /*static, unused*/, L_4, /*hidden argument*/Enumerable_ToArray_TisEnum_t21_m1369_MethodInfo_var);
		Object_t* L_6 = EnumExtension_GetFlags_m89(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Enum> Common.Extensions.EnumExtension::GetIndividualFlags(System.Enum)
extern const MethodInfo* Enumerable_ToArray_TisEnum_t21_m1369_MethodInfo_var;
extern "C" Object_t* EnumExtension_GetIndividualFlags_m88 (Object_t * __this /* static, unused */, Enum_t21 * ___p_value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerable_ToArray_TisEnum_t21_m1369_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enum_t21 * L_0 = ___p_value;
		Enum_t21 * L_1 = ___p_value;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m1368(L_1, /*hidden argument*/NULL);
		Object_t* L_3 = EnumExtension_GetFlagValues_m90(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		EnumU5BU5D_t338* L_4 = Enumerable_ToArray_TisEnum_t21_m1369(NULL /*static, unused*/, L_3, /*hidden argument*/Enumerable_ToArray_TisEnum_t21_m1369_MethodInfo_var);
		Object_t* L_5 = EnumExtension_GetFlags_m89(NULL /*static, unused*/, L_0, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Enum> Common.Extensions.EnumExtension::GetFlags(System.Enum,System.Enum[])
#include "mscorlib_ArrayTypes.h"
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"
// System.Collections.Generic.List`1<System.Enum>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
extern TypeInfo* Convert_t373_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t377_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1370_MethodInfo_var;
extern const MethodInfo* Enumerable_Empty_TisEnum_t21_m1371_MethodInfo_var;
extern const MethodInfo* Enumerable_Reverse_TisEnum_t21_m1372_MethodInfo_var;
extern const MethodInfo* Enumerable_Take_TisEnum_t21_m1373_MethodInfo_var;
extern "C" Object_t* EnumExtension_GetFlags_m89 (Object_t * __this /* static, unused */, Enum_t21 * ___p_value, EnumU5BU5D_t338* ___p_values, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		List_1_t377_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		List_1__ctor_m1370_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483657);
		Enumerable_Empty_TisEnum_t21_m1371_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		Enumerable_Reverse_TisEnum_t21_m1372_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		Enumerable_Take_TisEnum_t21_m1373_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483660);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	List_1_t377 * V_1 = {0};
	int32_t V_2 = 0;
	uint64_t V_3 = 0;
	{
		Enum_t21 * L_0 = ___p_value;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		uint64_t L_1 = Convert_ToUInt64_m1365(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		List_1_t377 * L_2 = (List_1_t377 *)il2cpp_codegen_object_new (List_1_t377_il2cpp_TypeInfo_var);
		List_1__ctor_m1370(L_2, /*hidden argument*/List_1__ctor_m1370_MethodInfo_var);
		V_1 = L_2;
		EnumU5BU5D_t338* L_3 = ___p_values;
		NullCheck(L_3);
		V_2 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_3)->max_length)))-(int32_t)1));
		goto IL_004c;
	}

IL_0018:
	{
		EnumU5BU5D_t338* L_4 = ___p_values;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		uint64_t L_7 = Convert_ToUInt64_m1365(NULL /*static, unused*/, (*(Enum_t21 **)(Enum_t21 **)SZArrayLdElema(L_4, L_6, sizeof(Enum_t21 *))), /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_2;
		if (L_8)
		{
			goto IL_0032;
		}
	}
	{
		uint64_t L_9 = V_3;
		if (L_9)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0053;
	}

IL_0032:
	{
		uint64_t L_10 = V_0;
		uint64_t L_11 = V_3;
		uint64_t L_12 = V_3;
		if ((!(((uint64_t)((int64_t)((int64_t)L_10&(int64_t)L_11))) == ((uint64_t)L_12))))
		{
			goto IL_0048;
		}
	}
	{
		List_1_t377 * L_13 = V_1;
		EnumU5BU5D_t338* L_14 = ___p_values;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		NullCheck(L_13);
		VirtActionInvoker1< Enum_t21 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Enum>::Add(!0) */, L_13, (*(Enum_t21 **)(Enum_t21 **)SZArrayLdElema(L_14, L_16, sizeof(Enum_t21 *))));
		uint64_t L_17 = V_0;
		uint64_t L_18 = V_3;
		V_0 = ((int64_t)((int64_t)L_17-(int64_t)L_18));
	}

IL_0048:
	{
		int32_t L_19 = V_2;
		V_2 = ((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_004c:
	{
		int32_t L_20 = V_2;
		if ((((int32_t)L_20) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}

IL_0053:
	{
		uint64_t L_21 = V_0;
		if (!L_21)
		{
			goto IL_005f;
		}
	}
	{
		Object_t* L_22 = Enumerable_Empty_TisEnum_t21_m1371(NULL /*static, unused*/, /*hidden argument*/Enumerable_Empty_TisEnum_t21_m1371_MethodInfo_var);
		return L_22;
	}

IL_005f:
	{
		Enum_t21 * L_23 = ___p_value;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		uint64_t L_24 = Convert_ToUInt64_m1365(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0071;
		}
	}
	{
		List_1_t377 * L_25 = V_1;
		Object_t* L_26 = Enumerable_Reverse_TisEnum_t21_m1372(NULL /*static, unused*/, L_25, /*hidden argument*/Enumerable_Reverse_TisEnum_t21_m1372_MethodInfo_var);
		return L_26;
	}

IL_0071:
	{
		uint64_t L_27 = V_0;
		Enum_t21 * L_28 = ___p_value;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		uint64_t L_29 = Convert_ToUInt64_m1365(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_27) == ((uint64_t)L_29))))
		{
			goto IL_009b;
		}
	}
	{
		EnumU5BU5D_t338* L_30 = ___p_values;
		NullCheck(L_30);
		if ((((int32_t)(((int32_t)(((Array_t *)L_30)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_009b;
		}
	}
	{
		EnumU5BU5D_t338* L_31 = ___p_values;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 0);
		int32_t L_32 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		uint64_t L_33 = Convert_ToUInt64_m1365(NULL /*static, unused*/, (*(Enum_t21 **)(Enum_t21 **)SZArrayLdElema(L_31, L_32, sizeof(Enum_t21 *))), /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_009b;
		}
	}
	{
		EnumU5BU5D_t338* L_34 = ___p_values;
		Object_t* L_35 = Enumerable_Take_TisEnum_t21_m1373(NULL /*static, unused*/, (Object_t*)(Object_t*)L_34, 1, /*hidden argument*/Enumerable_Take_TisEnum_t21_m1373_MethodInfo_var);
		return L_35;
	}

IL_009b:
	{
		Object_t* L_36 = Enumerable_Empty_TisEnum_t21_m1371(NULL /*static, unused*/, /*hidden argument*/Enumerable_Empty_TisEnum_t21_m1371_MethodInfo_var);
		return L_36;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Enum> Common.Extensions.EnumExtension::GetFlagValues(System.Type)
// System.Type
#include "mscorlib_System_Type.h"
// Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0
#include "AssemblyU2DCSharp_Common_Extensions_EnumExtension_U3CGetFlagMethodDeclarations.h"
extern TypeInfo* U3CGetFlagValuesU3Ec__Iterator0_t20_il2cpp_TypeInfo_var;
extern "C" Object_t* EnumExtension_GetFlagValues_m90 (Object_t * __this /* static, unused */, Type_t * ___p_enumType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetFlagValuesU3Ec__Iterator0_t20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetFlagValuesU3Ec__Iterator0_t20 * V_0 = {0};
	{
		U3CGetFlagValuesU3Ec__Iterator0_t20 * L_0 = (U3CGetFlagValuesU3Ec__Iterator0_t20 *)il2cpp_codegen_object_new (U3CGetFlagValuesU3Ec__Iterator0_t20_il2cpp_TypeInfo_var);
		U3CGetFlagValuesU3Ec__Iterator0__ctor_m79(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetFlagValuesU3Ec__Iterator0_t20 * L_1 = V_0;
		Type_t * L_2 = ___p_enumType;
		NullCheck(L_1);
		L_1->___p_enumType_1 = L_2;
		U3CGetFlagValuesU3Ec__Iterator0_t20 * L_3 = V_0;
		Type_t * L_4 = ___p_enumType;
		NullCheck(L_3);
		L_3->___U3CU24U3Ep_enumType_7 = L_4;
		U3CGetFlagValuesU3Ec__Iterator0_t20 * L_5 = V_0;
		U3CGetFlagValuesU3Ec__Iterator0_t20 * L_6 = L_5;
		NullCheck(L_6);
		L_6->___U24PC_5 = ((int32_t)-2);
		return L_6;
	}
}
// FrameRate
#include "AssemblyU2DCSharp_FrameRate.h"
// FrameRate
#include "AssemblyU2DCSharp_FrameRateMethodDeclarations.h"
// System.Void FrameRate::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void FrameRate__ctor_m91 (FrameRate_t24 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FrameRate::Update(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
extern "C" void FrameRate_Update_m92 (FrameRate_t24 * __this, float ___timeElapsed, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___numFrames_1);
		__this->___numFrames_1 = ((int32_t)((int32_t)L_0+(int32_t)1));
		float L_1 = (__this->___polledTime_2);
		float L_2 = ___timeElapsed;
		__this->___polledTime_2 = ((float)((float)L_1+(float)L_2));
		float L_3 = (__this->___polledTime_2);
		bool L_4 = Comparison_TolerantGreaterThanOrEquals_m59(NULL /*static, unused*/, L_3, (1.0f), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_5 = (__this->___numFrames_1);
		__this->___frameRate_0 = L_5;
		__this->___numFrames_1 = 0;
		__this->___polledTime_2 = (0.0f);
	}

IL_004f:
	{
		return;
	}
}
// System.Int32 FrameRate::GetFrameRate()
extern "C" int32_t FrameRate_GetFrameRate_m93 (FrameRate_t24 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___frameRate_0);
		return L_0;
	}
}
// FrameRateView
#include "AssemblyU2DCSharp_FrameRateView.h"
// FrameRateView
#include "AssemblyU2DCSharp_FrameRateViewMethodDeclarations.h"
// UnityEngine.TextMesh
#include "UnityEngine_UnityEngine_TextMesh.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
// UnityEngine.TextMesh
#include "UnityEngine_UnityEngine_TextMeshMethodDeclarations.h"
struct TextMesh_t26;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.TextMesh>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.TextMesh>()
#define Component_GetComponent_TisTextMesh_t26_m1378(__this, method) (( TextMesh_t26 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void FrameRateView::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void FrameRateView__ctor_m94 (FrameRateView_t25 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FrameRateView::Start()
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// FrameRate
#include "AssemblyU2DCSharp_FrameRateMethodDeclarations.h"
extern TypeInfo* FrameRate_t24_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTextMesh_t26_m1378_MethodInfo_var;
extern "C" void FrameRateView_Start_m95 (FrameRateView_t25 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FrameRate_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(32);
		Component_GetComponent_TisTextMesh_t26_m1378_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483661);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextMesh_t26 * L_0 = Component_GetComponent_TisTextMesh_t26_m1378(__this, /*hidden argument*/Component_GetComponent_TisTextMesh_t26_m1378_MethodInfo_var);
		__this->___text_2 = L_0;
		FrameRate_t24 * L_1 = (FrameRate_t24 *)il2cpp_codegen_object_new (FrameRate_t24_il2cpp_TypeInfo_var);
		FrameRate__ctor_m91(L_1, /*hidden argument*/NULL);
		__this->___frameRate_3 = L_1;
		return;
	}
}
// System.Void FrameRateView::Update()
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
// UnityEngine.TextMesh
#include "UnityEngine_UnityEngine_TextMeshMethodDeclarations.h"
extern "C" void FrameRateView_Update_m96 (FrameRateView_t25 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FrameRate_t24 * L_0 = (__this->___frameRate_3);
		float L_1 = Time_get_deltaTime_m1379(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		FrameRate_Update_m92(L_0, L_1, /*hidden argument*/NULL);
		TextMesh_t26 * L_2 = (__this->___text_2);
		FrameRate_t24 * L_3 = (__this->___frameRate_3);
		NullCheck(L_3);
		int32_t L_4 = FrameRate_GetFrameRate_m93(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = Int32_ToString_m1380((&V_0), /*hidden argument*/NULL);
		NullCheck(L_2);
		TextMesh_set_text_m1381(L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// Common.Fsm.FsmDelegateAction/FsmActionRoutine
#include "AssemblyU2DCSharp_Common_Fsm_FsmDelegateAction_FsmActionRout.h"
// Common.Fsm.FsmDelegateAction/FsmActionRoutine
#include "AssemblyU2DCSharp_Common_Fsm_FsmDelegateAction_FsmActionRoutMethodDeclarations.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
// System.Void Common.Fsm.FsmDelegateAction/FsmActionRoutine::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void FsmActionRoutine__ctor_m97 (FsmActionRoutine_t27 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void Common.Fsm.FsmDelegateAction/FsmActionRoutine::Invoke(Common.Fsm.FsmState)
extern "C" void FsmActionRoutine_Invoke_m98 (FsmActionRoutine_t27 * __this, Object_t * ___owner, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FsmActionRoutine_Invoke_m98((FsmActionRoutine_t27 *)__this->___prev_9,___owner, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___owner, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___owner,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___owner, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___owner,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___owner,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FsmActionRoutine_t27(Il2CppObject* delegate, Object_t * ___owner)
{
	// Marshaling of parameter '___owner' to native representation
	Object_t * ____owner_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'Common.Fsm.FsmState'."));
}
// System.IAsyncResult Common.Fsm.FsmDelegateAction/FsmActionRoutine::BeginInvoke(Common.Fsm.FsmState,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * FsmActionRoutine_BeginInvoke_m99 (FsmActionRoutine_t27 * __this, Object_t * ___owner, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___owner;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void Common.Fsm.FsmDelegateAction/FsmActionRoutine::EndInvoke(System.IAsyncResult)
extern "C" void FsmActionRoutine_EndInvoke_m100 (FsmActionRoutine_t27 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Common.Fsm.FsmDelegateAction
#include "AssemblyU2DCSharp_Common_Fsm_FsmDelegateAction.h"
// Common.Fsm.FsmDelegateAction
#include "AssemblyU2DCSharp_Common_Fsm_FsmDelegateActionMethodDeclarations.h"
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapter.h"
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapterMethodDeclarations.h"
// System.Void Common.Fsm.FsmDelegateAction::.ctor(Common.Fsm.FsmState,Common.Fsm.FsmDelegateAction/FsmActionRoutine)
// Common.Fsm.FsmDelegateAction/FsmActionRoutine
#include "AssemblyU2DCSharp_Common_Fsm_FsmDelegateAction_FsmActionRout.h"
// Common.Fsm.FsmDelegateAction
#include "AssemblyU2DCSharp_Common_Fsm_FsmDelegateActionMethodDeclarations.h"
extern "C" void FsmDelegateAction__ctor_m101 (FsmDelegateAction_t32 * __this, Object_t * ___owner, FsmActionRoutine_t27 * ___onEnterRoutine, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___owner;
		FsmActionRoutine_t27 * L_1 = ___onEnterRoutine;
		FsmDelegateAction__ctor_m102(__this, L_0, L_1, (FsmActionRoutine_t27 *)NULL, (FsmActionRoutine_t27 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.FsmDelegateAction::.ctor(Common.Fsm.FsmState,Common.Fsm.FsmDelegateAction/FsmActionRoutine,Common.Fsm.FsmDelegateAction/FsmActionRoutine,Common.Fsm.FsmDelegateAction/FsmActionRoutine)
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapterMethodDeclarations.h"
extern "C" void FsmDelegateAction__ctor_m102 (FsmDelegateAction_t32 * __this, Object_t * ___owner, FsmActionRoutine_t27 * ___onEnterRoutine, FsmActionRoutine_t27 * ___onUpdateRoutine, FsmActionRoutine_t27 * ___onExitRoutine, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___owner;
		FsmActionAdapter__ctor_m179(__this, L_0, /*hidden argument*/NULL);
		FsmActionRoutine_t27 * L_1 = ___onEnterRoutine;
		__this->___onEnterRoutine_1 = L_1;
		FsmActionRoutine_t27 * L_2 = ___onUpdateRoutine;
		__this->___onUpdateRoutine_2 = L_2;
		FsmActionRoutine_t27 * L_3 = ___onExitRoutine;
		__this->___onExitRoutine_3 = L_3;
		return;
	}
}
// System.Void Common.Fsm.FsmDelegateAction::OnEnter()
// Common.Fsm.FsmDelegateAction/FsmActionRoutine
#include "AssemblyU2DCSharp_Common_Fsm_FsmDelegateAction_FsmActionRoutMethodDeclarations.h"
extern "C" void FsmDelegateAction_OnEnter_m103 (FsmDelegateAction_t32 * __this, const MethodInfo* method)
{
	{
		FsmActionRoutine_t27 * L_0 = (__this->___onEnterRoutine_1);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		FsmActionRoutine_t27 * L_1 = (__this->___onEnterRoutine_1);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::GetOwner() */, __this);
		NullCheck(L_1);
		FsmActionRoutine_Invoke_m98(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void Common.Fsm.FsmDelegateAction::OnUpdate()
extern "C" void FsmDelegateAction_OnUpdate_m104 (FsmDelegateAction_t32 * __this, const MethodInfo* method)
{
	{
		FsmActionRoutine_t27 * L_0 = (__this->___onUpdateRoutine_2);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		FsmActionRoutine_t27 * L_1 = (__this->___onUpdateRoutine_2);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::GetOwner() */, __this);
		NullCheck(L_1);
		FsmActionRoutine_Invoke_m98(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void Common.Fsm.FsmDelegateAction::OnExit()
extern "C" void FsmDelegateAction_OnExit_m105 (FsmDelegateAction_t32 * __this, const MethodInfo* method)
{
	{
		FsmActionRoutine_t27 * L_0 = (__this->___onExitRoutine_3);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		FsmActionRoutine_t27 * L_1 = (__this->___onExitRoutine_3);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::GetOwner() */, __this);
		NullCheck(L_1);
		FsmActionRoutine_Invoke_m98(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// Common.Fsm.Action.MoveAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_MoveAction.h"
// Common.Fsm.Action.MoveAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_MoveActionMethodDeclarations.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
// VectorUtils
#include "AssemblyU2DCSharp_VectorUtilsMethodDeclarations.h"
// System.Void Common.Fsm.Action.MoveAction::.ctor(Common.Fsm.FsmState)
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapterMethodDeclarations.h"
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimerMethodDeclarations.h"
extern TypeInfo* CountdownTimer_t13_il2cpp_TypeInfo_var;
extern "C" void MoveAction__ctor_m106 (MoveAction_t34 * __this, Object_t * ___owner, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CountdownTimer_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___owner;
		FsmActionAdapter__ctor_m179(__this, L_0, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_1 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m63(L_1, (1.0f), /*hidden argument*/NULL);
		__this->___timer_8 = L_1;
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAction::.ctor(Common.Fsm.FsmState,System.String)
// System.String
#include "mscorlib_System_String.h"
extern TypeInfo* CountdownTimer_t13_il2cpp_TypeInfo_var;
extern "C" void MoveAction__ctor_m107 (MoveAction_t34 * __this, Object_t * ___owner, String_t* ___timeReferenceName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CountdownTimer_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___owner;
		FsmActionAdapter__ctor_m179(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___timeReferenceName;
		CountdownTimer_t13 * L_2 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m62(L_2, (1.0f), L_1, /*hidden argument*/NULL);
		__this->___timer_8 = L_2;
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAction::Init(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.String,UnityEngine.Space)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
extern "C" void MoveAction_Init_m108 (MoveAction_t34 * __this, Transform_t35 * ___transform, Vector3_t36  ___positionFrom, Vector3_t36  ___positionTo, float ___duration, String_t* ___finishEvent, int32_t ___space, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = ___transform;
		__this->___transform_1 = L_0;
		Vector3_t36  L_1 = ___positionFrom;
		__this->___positionFrom_2 = L_1;
		Vector3_t36  L_2 = ___positionTo;
		__this->___positionTo_3 = L_2;
		float L_3 = ___duration;
		__this->___duration_4 = L_3;
		String_t* L_4 = ___finishEvent;
		__this->___finishEvent_6 = L_4;
		int32_t L_5 = ___space;
		__this->___space_7 = L_5;
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAction::OnEnter()
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
// Common.Fsm.Action.MoveAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_MoveActionMethodDeclarations.h"
// VectorUtils
#include "AssemblyU2DCSharp_VectorUtilsMethodDeclarations.h"
extern TypeInfo* VectorUtils_t140_il2cpp_TypeInfo_var;
extern "C" void MoveAction_OnEnter_m109 (MoveAction_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VectorUtils_t140_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___duration_4);
		bool L_1 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_0, (0.0f), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		MoveAction_Finish_m111(__this, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		Vector3_t36  L_2 = (__this->___positionFrom_2);
		Vector3_t36  L_3 = (__this->___positionTo_3);
		IL2CPP_RUNTIME_CLASS_INIT(VectorUtils_t140_il2cpp_TypeInfo_var);
		bool L_4 = VectorUtils_Equals_m464(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0039;
		}
	}
	{
		MoveAction_Finish_m111(__this, /*hidden argument*/NULL);
		return;
	}

IL_0039:
	{
		Vector3_t36  L_5 = (__this->___positionFrom_2);
		MoveAction_SetPosition_m112(__this, L_5, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_6 = (__this->___timer_8);
		float L_7 = (__this->___duration_4);
		NullCheck(L_6);
		CountdownTimer_Reset_m66(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAction::OnUpdate()
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
extern "C" void MoveAction_OnUpdate_m110 (MoveAction_t34 * __this, const MethodInfo* method)
{
	{
		CountdownTimer_t13 * L_0 = (__this->___timer_8);
		NullCheck(L_0);
		CountdownTimer_Update_m64(L_0, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_1 = (__this->___timer_8);
		NullCheck(L_1);
		bool L_2 = CountdownTimer_HasElapsed_m67(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		MoveAction_Finish_m111(__this, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		Vector3_t36  L_3 = (__this->___positionFrom_2);
		Vector3_t36  L_4 = (__this->___positionTo_3);
		CountdownTimer_t13 * L_5 = (__this->___timer_8);
		NullCheck(L_5);
		float L_6 = CountdownTimer_GetRatio_m68(L_5, /*hidden argument*/NULL);
		Vector3_t36  L_7 = Vector3_Lerp_m1382(NULL /*static, unused*/, L_3, L_4, L_6, /*hidden argument*/NULL);
		MoveAction_SetPosition_m112(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAction::Finish()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FsmState_t29_il2cpp_TypeInfo_var;
extern "C" void MoveAction_Finish_m111 (MoveAction_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		FsmState_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t36  L_0 = (__this->___positionTo_3);
		MoveAction_SetPosition_m112(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = (__this->___finishEvent_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Object_t * L_3 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::GetOwner() */, __this);
		String_t* L_4 = (__this->___finishEvent_6);
		NullCheck(L_3);
		InterfaceActionInvoker1< String_t* >::Invoke(5 /* System.Void Common.Fsm.FsmState::SendEvent(System.String) */, FsmState_t29_il2cpp_TypeInfo_var, L_3, L_4);
	}

IL_002d:
	{
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAction::SetPosition(UnityEngine.Vector3)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern "C" void MoveAction_SetPosition_m112 (MoveAction_t34 * __this, Vector3_t36  ___position, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = (__this->___space_7);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		goto IL_003b;
	}

IL_0019:
	{
		Transform_t35 * L_3 = (__this->___transform_1);
		Vector3_t36  L_4 = ___position;
		NullCheck(L_3);
		Transform_set_position_m1383(L_3, L_4, /*hidden argument*/NULL);
		goto IL_003b;
	}

IL_002a:
	{
		Transform_t35 * L_5 = (__this->___transform_1);
		Vector3_t36  L_6 = ___position;
		NullCheck(L_5);
		Transform_set_localPosition_m1384(L_5, L_6, /*hidden argument*/NULL);
		goto IL_003b;
	}

IL_003b:
	{
		return;
	}
}
// Common.Fsm.Action.MoveAlongDirection
#include "AssemblyU2DCSharp_Common_Fsm_Action_MoveAlongDirection.h"
// Common.Fsm.Action.MoveAlongDirection
#include "AssemblyU2DCSharp_Common_Fsm_Action_MoveAlongDirectionMethodDeclarations.h"
// System.Void Common.Fsm.Action.MoveAlongDirection::.ctor(Common.Fsm.FsmState,System.String)
// System.String
#include "mscorlib_System_String.h"
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapterMethodDeclarations.h"
// Common.Time.TimeReferencePool
#include "AssemblyU2DCSharp_Common_Time_TimeReferencePoolMethodDeclarations.h"
extern TypeInfo* TimeReferencePool_t120_il2cpp_TypeInfo_var;
extern "C" void MoveAlongDirection__ctor_m113 (MoveAlongDirection_t37 * __this, Object_t * ___owner, String_t* ___timeReferenceName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeReferencePool_t120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___owner;
		FsmActionAdapter__ctor_m179(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeReferencePool_t120_il2cpp_TypeInfo_var);
		TimeReferencePool_t120 * L_1 = TimeReferencePool_GetInstance_m418(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = ___timeReferenceName;
		NullCheck(L_1);
		TimeReference_t14 * L_3 = TimeReferencePool_Get_m420(L_1, L_2, /*hidden argument*/NULL);
		__this->___timeReference_4 = L_3;
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAlongDirection::Init(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void MoveAlongDirection_Init_m114 (MoveAlongDirection_t37 * __this, Transform_t35 * ___actor, Vector3_t36  ___direction, float ___velocity, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = ___actor;
		__this->___actor_1 = L_0;
		Vector3_t36  L_1 = ___direction;
		__this->___direction_2 = L_1;
		float L_2 = ___velocity;
		__this->___velocity_3 = L_2;
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAlongDirection::OnEnter()
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern "C" void MoveAlongDirection_OnEnter_m115 (MoveAlongDirection_t37 * __this, const MethodInfo* method)
{
	{
		Vector3_t36 * L_0 = &(__this->___direction_2);
		float L_1 = Vector3_get_sqrMagnitude_m1385(L_0, /*hidden argument*/NULL);
		bool L_2 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_1, (0.0f), /*hidden argument*/NULL);
		Assertion_Assert_m21(NULL /*static, unused*/, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Vector3_t36 * L_3 = &(__this->___direction_2);
		float L_4 = Vector3_get_sqrMagnitude_m1385(L_3, /*hidden argument*/NULL);
		bool L_5 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_4, (1.0f), /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0042;
		}
	}
	{
		Vector3_t36 * L_6 = &(__this->___direction_2);
		Vector3_Normalize_m1386(L_6, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAlongDirection::OnUpdate()
// Common.Time.TimeReference
#include "AssemblyU2DCSharp_Common_Time_TimeReferenceMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern "C" void MoveAlongDirection_OnUpdate_m116 (MoveAlongDirection_t37 * __this, const MethodInfo* method)
{
	Vector3_t36  V_0 = {0};
	{
		Vector3_t36  L_0 = (__this->___direction_2);
		float L_1 = (__this->___velocity_3);
		TimeReference_t14 * L_2 = (__this->___timeReference_4);
		NullCheck(L_2);
		float L_3 = TimeReference_get_DeltaTime_m412(L_2, /*hidden argument*/NULL);
		Vector3_t36  L_4 = Vector3_op_Multiply_m1387(NULL /*static, unused*/, L_0, ((float)((float)L_1*(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		Transform_t35 * L_5 = (__this->___actor_1);
		Transform_t35 * L_6 = (__this->___actor_1);
		NullCheck(L_6);
		Vector3_t36  L_7 = Transform_get_position_m1388(L_6, /*hidden argument*/NULL);
		Vector3_t36  L_8 = V_0;
		Vector3_t36  L_9 = Vector3_op_Addition_m1389(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_position_m1383(L_5, L_9, /*hidden argument*/NULL);
		return;
	}
}
// Common.Fsm.Action.MoveAlongDirectionByPolledTime
#include "AssemblyU2DCSharp_Common_Fsm_Action_MoveAlongDirectionByPoll.h"
// Common.Fsm.Action.MoveAlongDirectionByPolledTime
#include "AssemblyU2DCSharp_Common_Fsm_Action_MoveAlongDirectionByPollMethodDeclarations.h"
// System.Void Common.Fsm.Action.MoveAlongDirectionByPolledTime::.ctor(Common.Fsm.FsmState,System.String)
// System.String
#include "mscorlib_System_String.h"
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapterMethodDeclarations.h"
// Common.Time.TimeReferencePool
#include "AssemblyU2DCSharp_Common_Time_TimeReferencePoolMethodDeclarations.h"
extern TypeInfo* TimeReferencePool_t120_il2cpp_TypeInfo_var;
extern "C" void MoveAlongDirectionByPolledTime__ctor_m117 (MoveAlongDirectionByPolledTime_t38 * __this, Object_t * ___owner, String_t* ___timeReferenceName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeReferencePool_t120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___owner;
		FsmActionAdapter__ctor_m179(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeReferencePool_t120_il2cpp_TypeInfo_var);
		TimeReferencePool_t120 * L_1 = TimeReferencePool_GetInstance_m418(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = ___timeReferenceName;
		NullCheck(L_1);
		TimeReference_t14 * L_3 = TimeReferencePool_Get_m420(L_1, L_2, /*hidden argument*/NULL);
		__this->___timeReference_4 = L_3;
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAlongDirectionByPolledTime::Init(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void MoveAlongDirectionByPolledTime_Init_m118 (MoveAlongDirectionByPolledTime_t38 * __this, Transform_t35 * ___actor, Vector3_t36  ___direction, float ___velocity, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = ___actor;
		__this->___actor_1 = L_0;
		Vector3_t36  L_1 = ___direction;
		__this->___direction_2 = L_1;
		float L_2 = ___velocity;
		__this->___velocity_3 = L_2;
		return;
	}
}
// UnityEngine.Vector3 Common.Fsm.Action.MoveAlongDirectionByPolledTime::get_StartPosition()
extern "C" Vector3_t36  MoveAlongDirectionByPolledTime_get_StartPosition_m119 (MoveAlongDirectionByPolledTime_t38 * __this, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = (__this->___startPosition_6);
		return L_0;
	}
}
// System.Void Common.Fsm.Action.MoveAlongDirectionByPolledTime::set_StartPosition(UnityEngine.Vector3)
extern "C" void MoveAlongDirectionByPolledTime_set_StartPosition_m120 (MoveAlongDirectionByPolledTime_t38 * __this, Vector3_t36  ___value, const MethodInfo* method)
{
	{
		Vector3_t36  L_0 = ___value;
		__this->___startPosition_6 = L_0;
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAlongDirectionByPolledTime::OnEnter()
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern "C" void MoveAlongDirectionByPolledTime_OnEnter_m121 (MoveAlongDirectionByPolledTime_t38 * __this, const MethodInfo* method)
{
	{
		Vector3_t36 * L_0 = &(__this->___direction_2);
		float L_1 = Vector3_get_sqrMagnitude_m1385(L_0, /*hidden argument*/NULL);
		bool L_2 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_1, (0.0f), /*hidden argument*/NULL);
		Assertion_Assert_m21(NULL /*static, unused*/, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Vector3_t36 * L_3 = &(__this->___direction_2);
		float L_4 = Vector3_get_sqrMagnitude_m1385(L_3, /*hidden argument*/NULL);
		bool L_5 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_4, (1.0f), /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0042;
		}
	}
	{
		Vector3_t36 * L_6 = &(__this->___direction_2);
		Vector3_Normalize_m1386(L_6, /*hidden argument*/NULL);
	}

IL_0042:
	{
		__this->___polledTime_5 = (0.0f);
		return;
	}
}
// System.Void Common.Fsm.Action.MoveAlongDirectionByPolledTime::OnUpdate()
// Common.Time.TimeReference
#include "AssemblyU2DCSharp_Common_Time_TimeReferenceMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern "C" void MoveAlongDirectionByPolledTime_OnUpdate_m122 (MoveAlongDirectionByPolledTime_t38 * __this, const MethodInfo* method)
{
	Vector3_t36  V_0 = {0};
	{
		float L_0 = (__this->___polledTime_5);
		TimeReference_t14 * L_1 = (__this->___timeReference_4);
		NullCheck(L_1);
		float L_2 = TimeReference_get_DeltaTime_m412(L_1, /*hidden argument*/NULL);
		__this->___polledTime_5 = ((float)((float)L_0+(float)L_2));
		Vector3_t36  L_3 = (__this->___startPosition_6);
		Vector3_t36  L_4 = (__this->___direction_2);
		float L_5 = (__this->___velocity_3);
		float L_6 = (__this->___polledTime_5);
		Vector3_t36  L_7 = Vector3_op_Multiply_m1387(NULL /*static, unused*/, L_4, ((float)((float)L_5*(float)L_6)), /*hidden argument*/NULL);
		Vector3_t36  L_8 = Vector3_op_Addition_m1389(NULL /*static, unused*/, L_3, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Transform_t35 * L_9 = (__this->___actor_1);
		Vector3_t36  L_10 = V_0;
		NullCheck(L_9);
		Transform_set_position_m1383(L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// Common.Fsm.Action.NonResettingTimedWait
#include "AssemblyU2DCSharp_Common_Fsm_Action_NonResettingTimedWait.h"
// Common.Fsm.Action.NonResettingTimedWait
#include "AssemblyU2DCSharp_Common_Fsm_Action_NonResettingTimedWaitMethodDeclarations.h"
// System.Void Common.Fsm.Action.NonResettingTimedWait::.ctor(Common.Fsm.FsmState,System.String,System.String)
// System.String
#include "mscorlib_System_String.h"
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapterMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimerMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CountdownTimer_t13_il2cpp_TypeInfo_var;
extern "C" void NonResettingTimedWait__ctor_m123 (NonResettingTimedWait_t39 * __this, Object_t * ___owner, String_t* ___timeReferenceName, String_t* ___finishEvent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		CountdownTimer_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___owner;
		FsmActionAdapter__ctor_m179(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___timeReferenceName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		CountdownTimer_t13 * L_3 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m63(L_3, (1.0f), /*hidden argument*/NULL);
		__this->___timer_2 = L_3;
		goto IL_0038;
	}

IL_0027:
	{
		String_t* L_4 = ___timeReferenceName;
		CountdownTimer_t13 * L_5 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m62(L_5, (1.0f), L_4, /*hidden argument*/NULL);
		__this->___timer_2 = L_5;
	}

IL_0038:
	{
		String_t* L_6 = ___finishEvent;
		__this->___finishEvent_4 = L_6;
		return;
	}
}
// System.Void Common.Fsm.Action.NonResettingTimedWait::Init(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void NonResettingTimedWait_Init_m124 (NonResettingTimedWait_t39 * __this, float ___waitTime, const MethodInfo* method)
{
	{
		float L_0 = ___waitTime;
		__this->___waitTime_1 = L_0;
		CountdownTimer_t13 * L_1 = (__this->___timer_2);
		float L_2 = (__this->___waitTime_1);
		NullCheck(L_1);
		CountdownTimer_Reset_m66(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Action.NonResettingTimedWait::OnEnter()
// Common.Fsm.Action.NonResettingTimedWait
#include "AssemblyU2DCSharp_Common_Fsm_Action_NonResettingTimedWaitMethodDeclarations.h"
extern "C" void NonResettingTimedWait_OnEnter_m125 (NonResettingTimedWait_t39 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___waitTime_1);
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		NonResettingTimedWait_Finish_m127(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Common.Fsm.Action.NonResettingTimedWait::OnUpdate()
extern "C" void NonResettingTimedWait_OnUpdate_m126 (NonResettingTimedWait_t39 * __this, const MethodInfo* method)
{
	{
		CountdownTimer_t13 * L_0 = (__this->___timer_2);
		NullCheck(L_0);
		CountdownTimer_Update_m64(L_0, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_1 = (__this->___timer_2);
		NullCheck(L_1);
		bool L_2 = CountdownTimer_HasElapsed_m67(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		NonResettingTimedWait_Finish_m127(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void Common.Fsm.Action.NonResettingTimedWait::Finish()
extern TypeInfo* FsmState_t29_il2cpp_TypeInfo_var;
extern "C" void NonResettingTimedWait_Finish_m127 (NonResettingTimedWait_t39 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FsmState_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::GetOwner() */, __this);
		String_t* L_1 = (__this->___finishEvent_4);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(5 /* System.Void Common.Fsm.FsmState::SendEvent(System.String) */, FsmState_t29_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Single Common.Fsm.Action.NonResettingTimedWait::GetRatio()
extern "C" float NonResettingTimedWait_GetRatio_m128 (NonResettingTimedWait_t39 * __this, const MethodInfo* method)
{
	{
		CountdownTimer_t13 * L_0 = (__this->___timer_2);
		NullCheck(L_0);
		float L_1 = CountdownTimer_GetRatio_m68(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Common.Fsm.Action.RotateAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_RotateAction.h"
// Common.Fsm.Action.RotateAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_RotateActionMethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
// System.Void Common.Fsm.Action.RotateAction::.ctor(Common.Fsm.FsmState)
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapterMethodDeclarations.h"
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimerMethodDeclarations.h"
extern TypeInfo* CountdownTimer_t13_il2cpp_TypeInfo_var;
extern "C" void RotateAction__ctor_m129 (RotateAction_t40 * __this, Object_t * ___owner, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CountdownTimer_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___owner;
		FsmActionAdapter__ctor_m179(__this, L_0, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_1 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m63(L_1, (1.0f), /*hidden argument*/NULL);
		__this->___timer_6 = L_1;
		return;
	}
}
// System.Void Common.Fsm.Action.RotateAction::.ctor(Common.Fsm.FsmState,System.String)
// System.String
#include "mscorlib_System_String.h"
extern TypeInfo* CountdownTimer_t13_il2cpp_TypeInfo_var;
extern "C" void RotateAction__ctor_m130 (RotateAction_t40 * __this, Object_t * ___owner, String_t* ___timeReferenceName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CountdownTimer_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___owner;
		FsmActionAdapter__ctor_m179(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___timeReferenceName;
		CountdownTimer_t13 * L_2 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m62(L_2, (1.0f), L_1, /*hidden argument*/NULL);
		__this->___timer_6 = L_2;
		return;
	}
}
// System.Void Common.Fsm.Action.RotateAction::Init(UnityEngine.Transform,UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single,System.String)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void RotateAction_Init_m131 (RotateAction_t40 * __this, Transform_t35 * ___transform, Quaternion_t41  ___quatFrom, Quaternion_t41  ___quatTo, float ___duration, String_t* ___finishEvent, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = ___transform;
		__this->___transform_1 = L_0;
		Quaternion_t41  L_1 = ___quatFrom;
		__this->___quatFrom_2 = L_1;
		Quaternion_t41  L_2 = ___quatTo;
		__this->___quatTo_3 = L_2;
		float L_3 = ___duration;
		__this->___duration_4 = L_3;
		String_t* L_4 = ___finishEvent;
		__this->___finishEvent_5 = L_4;
		return;
	}
}
// System.Void Common.Fsm.Action.RotateAction::OnEnter()
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
// Common.Fsm.Action.RotateAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_RotateActionMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern TypeInfo* Quaternion_t41_il2cpp_TypeInfo_var;
extern "C" void RotateAction_OnEnter_m132 (RotateAction_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quaternion_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___duration_4);
		bool L_1 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_0, (0.0f), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		RotateAction_Finish_m134(__this, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		Quaternion_t41  L_2 = (__this->___quatFrom_2);
		Quaternion_t41  L_3 = L_2;
		Object_t * L_4 = Box(Quaternion_t41_il2cpp_TypeInfo_var, &L_3);
		Quaternion_t41  L_5 = (__this->___quatTo_3);
		Quaternion_t41  L_6 = L_5;
		Object_t * L_7 = Box(Quaternion_t41_il2cpp_TypeInfo_var, &L_6);
		bool L_8 = Object_Equals_m1390(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		RotateAction_Finish_m134(__this, /*hidden argument*/NULL);
		return;
	}

IL_0043:
	{
		Transform_t35 * L_9 = (__this->___transform_1);
		Quaternion_t41  L_10 = (__this->___quatFrom_2);
		NullCheck(L_9);
		Transform_set_rotation_m1391(L_9, L_10, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_11 = (__this->___timer_6);
		float L_12 = (__this->___duration_4);
		NullCheck(L_11);
		CountdownTimer_Reset_m66(L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Action.RotateAction::OnUpdate()
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
extern "C" void RotateAction_OnUpdate_m133 (RotateAction_t40 * __this, const MethodInfo* method)
{
	{
		CountdownTimer_t13 * L_0 = (__this->___timer_6);
		NullCheck(L_0);
		CountdownTimer_Update_m64(L_0, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_1 = (__this->___timer_6);
		NullCheck(L_1);
		bool L_2 = CountdownTimer_HasElapsed_m67(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		RotateAction_Finish_m134(__this, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		Transform_t35 * L_3 = (__this->___transform_1);
		Quaternion_t41  L_4 = (__this->___quatFrom_2);
		Quaternion_t41  L_5 = (__this->___quatTo_3);
		CountdownTimer_t13 * L_6 = (__this->___timer_6);
		NullCheck(L_6);
		float L_7 = CountdownTimer_GetRatio_m68(L_6, /*hidden argument*/NULL);
		Quaternion_t41  L_8 = Quaternion_Lerp_m1392(NULL /*static, unused*/, L_4, L_5, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m1391(L_3, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Action.RotateAction::Finish()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FsmState_t29_il2cpp_TypeInfo_var;
extern "C" void RotateAction_Finish_m134 (RotateAction_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		FsmState_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t35 * L_0 = (__this->___transform_1);
		Quaternion_t41  L_1 = (__this->___quatTo_3);
		NullCheck(L_0);
		Transform_set_rotation_m1391(L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = (__this->___finishEvent_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		Object_t * L_4 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::GetOwner() */, __this);
		String_t* L_5 = (__this->___finishEvent_5);
		NullCheck(L_4);
		InterfaceActionInvoker1< String_t* >::Invoke(5 /* System.Void Common.Fsm.FsmState::SendEvent(System.String) */, FsmState_t29_il2cpp_TypeInfo_var, L_4, L_5);
	}

IL_0032:
	{
		return;
	}
}
// Common.Fsm.Action.ScaleAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_ScaleAction.h"
// Common.Fsm.Action.ScaleAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_ScaleActionMethodDeclarations.h"
// System.Void Common.Fsm.Action.ScaleAction::.ctor(Common.Fsm.FsmState,System.String)
// System.String
#include "mscorlib_System_String.h"
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapterMethodDeclarations.h"
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimerMethodDeclarations.h"
extern TypeInfo* CountdownTimer_t13_il2cpp_TypeInfo_var;
extern "C" void ScaleAction__ctor_m135 (ScaleAction_t42 * __this, Object_t * ___owner, String_t* ___timeReference, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CountdownTimer_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___owner;
		FsmActionAdapter__ctor_m179(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___timeReference;
		CountdownTimer_t13 * L_2 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m62(L_2, (1.0f), L_1, /*hidden argument*/NULL);
		__this->___timer_7 = L_2;
		return;
	}
}
// System.Void Common.Fsm.Action.ScaleAction::Init(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.String)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void ScaleAction_Init_m136 (ScaleAction_t42 * __this, Transform_t35 * ___transform, Vector3_t36  ___scaleFrom, Vector3_t36  ___scaleTo, float ___duration, String_t* ___finishEvent, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = ___transform;
		__this->___transform_1 = L_0;
		Vector3_t36  L_1 = ___scaleFrom;
		__this->___scaleFrom_2 = L_1;
		Vector3_t36  L_2 = ___scaleTo;
		__this->___scaleTo_3 = L_2;
		float L_3 = ___duration;
		__this->___duration_4 = L_3;
		String_t* L_4 = ___finishEvent;
		__this->___finishEvent_6 = L_4;
		return;
	}
}
// System.Void Common.Fsm.Action.ScaleAction::OnEnter()
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
// Common.Fsm.Action.ScaleAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_ScaleActionMethodDeclarations.h"
// VectorUtils
#include "AssemblyU2DCSharp_VectorUtilsMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern TypeInfo* VectorUtils_t140_il2cpp_TypeInfo_var;
extern "C" void ScaleAction_OnEnter_m137 (ScaleAction_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VectorUtils_t140_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___duration_4);
		bool L_1 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_0, (0.0f), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		ScaleAction_Finish_m139(__this, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		Vector3_t36  L_2 = (__this->___scaleFrom_2);
		Vector3_t36  L_3 = (__this->___scaleTo_3);
		IL2CPP_RUNTIME_CLASS_INIT(VectorUtils_t140_il2cpp_TypeInfo_var);
		bool L_4 = VectorUtils_Equals_m464(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0039;
		}
	}
	{
		ScaleAction_Finish_m139(__this, /*hidden argument*/NULL);
		return;
	}

IL_0039:
	{
		Transform_t35 * L_5 = (__this->___transform_1);
		Vector3_t36  L_6 = (__this->___scaleFrom_2);
		NullCheck(L_5);
		Transform_set_localScale_m1393(L_5, L_6, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_7 = (__this->___timer_7);
		float L_8 = (__this->___duration_4);
		NullCheck(L_7);
		CountdownTimer_Reset_m66(L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Action.ScaleAction::OnUpdate()
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
extern "C" void ScaleAction_OnUpdate_m138 (ScaleAction_t42 * __this, const MethodInfo* method)
{
	{
		CountdownTimer_t13 * L_0 = (__this->___timer_7);
		NullCheck(L_0);
		CountdownTimer_Update_m64(L_0, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_1 = (__this->___timer_7);
		NullCheck(L_1);
		bool L_2 = CountdownTimer_HasElapsed_m67(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		ScaleAction_Finish_m139(__this, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		Transform_t35 * L_3 = (__this->___transform_1);
		Vector3_t36  L_4 = (__this->___scaleFrom_2);
		Vector3_t36  L_5 = (__this->___scaleTo_3);
		CountdownTimer_t13 * L_6 = (__this->___timer_7);
		NullCheck(L_6);
		float L_7 = CountdownTimer_GetRatio_m68(L_6, /*hidden argument*/NULL);
		Vector3_t36  L_8 = Vector3_Lerp_m1382(NULL /*static, unused*/, L_4, L_5, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localScale_m1393(L_3, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Action.ScaleAction::Finish()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FsmState_t29_il2cpp_TypeInfo_var;
extern "C" void ScaleAction_Finish_m139 (ScaleAction_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		FsmState_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t35 * L_0 = (__this->___transform_1);
		Vector3_t36  L_1 = (__this->___scaleTo_3);
		NullCheck(L_0);
		Transform_set_localScale_m1393(L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = (__this->___finishEvent_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		Object_t * L_4 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::GetOwner() */, __this);
		String_t* L_5 = (__this->___finishEvent_6);
		NullCheck(L_4);
		InterfaceActionInvoker1< String_t* >::Invoke(5 /* System.Void Common.Fsm.FsmState::SendEvent(System.String) */, FsmState_t29_il2cpp_TypeInfo_var, L_4, L_5);
	}

IL_0032:
	{
		return;
	}
}
// Common.Fsm.Action.TimedFadeVolume
#include "AssemblyU2DCSharp_Common_Fsm_Action_TimedFadeVolume.h"
// Common.Fsm.Action.TimedFadeVolume
#include "AssemblyU2DCSharp_Common_Fsm_Action_TimedFadeVolumeMethodDeclarations.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
// System.Void Common.Fsm.Action.TimedFadeVolume::.ctor(Common.Fsm.FsmState,System.String)
// System.String
#include "mscorlib_System_String.h"
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapterMethodDeclarations.h"
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimerMethodDeclarations.h"
extern TypeInfo* CountdownTimer_t13_il2cpp_TypeInfo_var;
extern "C" void TimedFadeVolume__ctor_m140 (TimedFadeVolume_t43 * __this, Object_t * ___owner, String_t* ___timeReferenceName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CountdownTimer_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___owner;
		FsmActionAdapter__ctor_m179(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___timeReferenceName;
		CountdownTimer_t13 * L_2 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m62(L_2, (1.0f), L_1, /*hidden argument*/NULL);
		__this->___timer_6 = L_2;
		return;
	}
}
// System.Void Common.Fsm.Action.TimedFadeVolume::Init(UnityEngine.AudioSource,System.Single,System.Single,System.Single,System.String)
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void TimedFadeVolume_Init_m141 (TimedFadeVolume_t43 * __this, AudioSource_t44 * ___audioSource, float ___volumeFrom, float ___volumeTo, float ___duration, String_t* ___finishEvent, const MethodInfo* method)
{
	{
		AudioSource_t44 * L_0 = ___audioSource;
		__this->___audioSource_1 = L_0;
		float L_1 = ___volumeFrom;
		__this->___volumeFrom_2 = L_1;
		float L_2 = ___volumeTo;
		__this->___volumeTo_3 = L_2;
		float L_3 = ___duration;
		__this->___duration_4 = L_3;
		String_t* L_4 = ___finishEvent;
		__this->___finishEvent_5 = L_4;
		return;
	}
}
// System.Void Common.Fsm.Action.TimedFadeVolume::OnEnter()
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
extern "C" void TimedFadeVolume_OnEnter_m142 (TimedFadeVolume_t43 * __this, const MethodInfo* method)
{
	{
		CountdownTimer_t13 * L_0 = (__this->___timer_6);
		float L_1 = (__this->___duration_4);
		NullCheck(L_0);
		CountdownTimer_Reset_m66(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t44 * L_2 = (__this->___audioSource_1);
		float L_3 = (__this->___volumeFrom_2);
		NullCheck(L_2);
		AudioSource_set_volume_m1394(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Action.TimedFadeVolume::OnUpdate()
// Common.Fsm.Action.TimedFadeVolume
#include "AssemblyU2DCSharp_Common_Fsm_Action_TimedFadeVolumeMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" void TimedFadeVolume_OnUpdate_m143 (TimedFadeVolume_t43 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	{
		CountdownTimer_t13 * L_0 = (__this->___timer_6);
		NullCheck(L_0);
		CountdownTimer_Update_m64(L_0, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_1 = (__this->___timer_6);
		NullCheck(L_1);
		bool L_2 = CountdownTimer_HasElapsed_m67(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		TimedFadeVolume_Finish_m144(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		AudioSource_t44 * L_3 = (__this->___audioSource_1);
		float L_4 = (__this->___volumeFrom_2);
		float L_5 = (__this->___volumeTo_3);
		CountdownTimer_t13 * L_6 = (__this->___timer_6);
		NullCheck(L_6);
		float L_7 = CountdownTimer_GetRatio_m68(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Lerp_m1395(NULL /*static, unused*/, L_4, L_5, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		AudioSource_set_volume_m1394(L_3, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Action.TimedFadeVolume::Finish()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FsmState_t29_il2cpp_TypeInfo_var;
extern "C" void TimedFadeVolume_Finish_m144 (TimedFadeVolume_t43 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		FsmState_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t44 * L_0 = (__this->___audioSource_1);
		float L_1 = (__this->___volumeTo_3);
		NullCheck(L_0);
		AudioSource_set_volume_m1394(L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = (__this->___finishEvent_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		Object_t * L_4 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::GetOwner() */, __this);
		String_t* L_5 = (__this->___finishEvent_5);
		NullCheck(L_4);
		InterfaceActionInvoker1< String_t* >::Invoke(5 /* System.Void Common.Fsm.FsmState::SendEvent(System.String) */, FsmState_t29_il2cpp_TypeInfo_var, L_4, L_5);
	}

IL_0032:
	{
		return;
	}
}
// Common.Fsm.Action.TimedWaitAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_TimedWaitAction.h"
// Common.Fsm.Action.TimedWaitAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_TimedWaitActionMethodDeclarations.h"
// System.Void Common.Fsm.Action.TimedWaitAction::.ctor(Common.Fsm.FsmState,System.String,System.String)
// System.String
#include "mscorlib_System_String.h"
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapterMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimerMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CountdownTimer_t13_il2cpp_TypeInfo_var;
extern "C" void TimedWaitAction__ctor_m145 (TimedWaitAction_t45 * __this, Object_t * ___owner, String_t* ___timeReferenceName, String_t* ___finishEvent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		CountdownTimer_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___owner;
		FsmActionAdapter__ctor_m179(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___timeReferenceName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		CountdownTimer_t13 * L_3 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m63(L_3, (1.0f), /*hidden argument*/NULL);
		__this->___timer_2 = L_3;
		goto IL_0038;
	}

IL_0027:
	{
		String_t* L_4 = ___timeReferenceName;
		CountdownTimer_t13 * L_5 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m62(L_5, (1.0f), L_4, /*hidden argument*/NULL);
		__this->___timer_2 = L_5;
	}

IL_0038:
	{
		String_t* L_6 = ___finishEvent;
		__this->___finishEvent_4 = L_6;
		return;
	}
}
// System.Void Common.Fsm.Action.TimedWaitAction::Init(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void TimedWaitAction_Init_m146 (TimedWaitAction_t45 * __this, float ___waitTime, const MethodInfo* method)
{
	{
		float L_0 = ___waitTime;
		__this->___waitTime_1 = L_0;
		return;
	}
}
// System.Void Common.Fsm.Action.TimedWaitAction::OnEnter()
// Common.Fsm.Action.TimedWaitAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_TimedWaitActionMethodDeclarations.h"
extern "C" void TimedWaitAction_OnEnter_m147 (TimedWaitAction_t45 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___waitTime_1);
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		TimedWaitAction_Finish_m149(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		CountdownTimer_t13 * L_1 = (__this->___timer_2);
		float L_2 = (__this->___waitTime_1);
		NullCheck(L_1);
		CountdownTimer_Reset_m66(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Action.TimedWaitAction::OnUpdate()
extern "C" void TimedWaitAction_OnUpdate_m148 (TimedWaitAction_t45 * __this, const MethodInfo* method)
{
	{
		CountdownTimer_t13 * L_0 = (__this->___timer_2);
		NullCheck(L_0);
		CountdownTimer_Update_m64(L_0, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_1 = (__this->___timer_2);
		NullCheck(L_1);
		bool L_2 = CountdownTimer_HasElapsed_m67(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		TimedWaitAction_Finish_m149(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void Common.Fsm.Action.TimedWaitAction::Finish()
extern TypeInfo* FsmState_t29_il2cpp_TypeInfo_var;
extern "C" void TimedWaitAction_Finish_m149 (TimedWaitAction_t45 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FsmState_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::GetOwner() */, __this);
		String_t* L_1 = (__this->___finishEvent_4);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(5 /* System.Void Common.Fsm.FsmState::SendEvent(System.String) */, FsmState_t29_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Single Common.Fsm.Action.TimedWaitAction::GetRatio()
extern "C" float TimedWaitAction_GetRatio_m150 (TimedWaitAction_t45 * __this, const MethodInfo* method)
{
	{
		CountdownTimer_t13 * L_0 = (__this->___timer_2);
		NullCheck(L_0);
		float L_1 = CountdownTimer_GetRatio_m68(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Common.Fsm.ConcreteFsmState
#include "AssemblyU2DCSharp_Common_Fsm_ConcreteFsmState.h"
// Common.Fsm.ConcreteFsmState
#include "AssemblyU2DCSharp_Common_Fsm_ConcreteFsmStateMethodDeclarations.h"
// Common.Fsm.Fsm
#include "AssemblyU2DCSharp_Common_Fsm_Fsm.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
// System.Collections.Generic.List`1<Common.Fsm.FsmAction>
#include "mscorlib_System_Collections_Generic_List_1_gen_2.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
// System.Collections.Generic.List`1<Common.Fsm.FsmAction>
#include "mscorlib_System_Collections_Generic_List_1_gen_2MethodDeclarations.h"
// Common.Fsm.Fsm
#include "AssemblyU2DCSharp_Common_Fsm_FsmMethodDeclarations.h"
// System.Void Common.Fsm.ConcreteFsmState::.ctor(System.String,Common.Fsm.Fsm)
// System.String
#include "mscorlib_System_String.h"
// Common.Fsm.Fsm
#include "AssemblyU2DCSharp_Common_Fsm_Fsm.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
// System.Collections.Generic.List`1<Common.Fsm.FsmAction>
#include "mscorlib_System_Collections_Generic_List_1_gen_2MethodDeclarations.h"
extern TypeInfo* Dictionary_2_t48_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t49_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1396_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1397_MethodInfo_var;
extern "C" void ConcreteFsmState__ctor_m151 (ConcreteFsmState_t46 * __this, String_t* ___name, Fsm_t47 * ___owner, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		List_1_t49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		Dictionary_2__ctor_m1396_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		List_1__ctor_m1397_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483663);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___name_0 = L_0;
		Fsm_t47 * L_1 = ___owner;
		__this->___owner_1 = L_1;
		Dictionary_2_t48 * L_2 = (Dictionary_2_t48 *)il2cpp_codegen_object_new (Dictionary_2_t48_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1396(L_2, /*hidden argument*/Dictionary_2__ctor_m1396_MethodInfo_var);
		__this->___transitionMap_2 = L_2;
		List_1_t49 * L_3 = (List_1_t49 *)il2cpp_codegen_object_new (List_1_t49_il2cpp_TypeInfo_var);
		List_1__ctor_m1397(L_3, /*hidden argument*/List_1__ctor_m1397_MethodInfo_var);
		__this->___actionList_3 = L_3;
		return;
	}
}
// System.String Common.Fsm.ConcreteFsmState::GetName()
extern "C" String_t* ConcreteFsmState_GetName_m152 (ConcreteFsmState_t46 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_0);
		return L_0;
	}
}
// System.Void Common.Fsm.ConcreteFsmState::AddTransition(System.String,Common.Fsm.FsmState)
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral9;
extern "C" void ConcreteFsmState_AddTransition_m153 (ConcreteFsmState_t46 * __this, String_t* ___eventId, Object_t * ___destinationState, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral9 = il2cpp_codegen_string_literal_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t48 * L_0 = (__this->___transitionMap_2);
		String_t* L_1 = ___eventId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::ContainsKey(!0) */, L_0, L_1);
		String_t* L_3 = (__this->___name_0);
		String_t* L_4 = ___eventId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1359(NULL /*static, unused*/, _stringLiteral9, L_3, L_4, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), L_5, /*hidden argument*/NULL);
		Dictionary_2_t48 * L_6 = (__this->___transitionMap_2);
		String_t* L_7 = ___eventId;
		Object_t * L_8 = ___destinationState;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::set_Item(!0,!1) */, L_6, L_7, L_8);
		return;
	}
}
// Common.Fsm.FsmState Common.Fsm.ConcreteFsmState::GetTransition(System.String)
extern "C" Object_t * ConcreteFsmState_GetTransition_m154 (ConcreteFsmState_t46 * __this, String_t* ___eventId, const MethodInfo* method)
{
	{
		Dictionary_2_t48 * L_0 = (__this->___transitionMap_2);
		String_t* L_1 = ___eventId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t48 * L_3 = (__this->___transitionMap_2);
		String_t* L_4 = ___eventId;
		NullCheck(L_3);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, String_t* >::Invoke(19 /* !1 System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::get_Item(!0) */, L_3, L_4);
		return L_5;
	}

IL_001e:
	{
		return (Object_t *)NULL;
	}
}
// System.Void Common.Fsm.ConcreteFsmState::AddAction(Common.Fsm.FsmAction)
extern TypeInfo* FsmAction_t51_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral10;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void ConcreteFsmState_AddAction_m155 (ConcreteFsmState_t46 * __this, Object_t * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FsmAction_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		_stringLiteral10 = il2cpp_codegen_string_literal_from_index(10);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t49 * L_0 = (__this->___actionList_3);
		Object_t * L_1 = ___action;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Common.Fsm.FsmAction>::Contains(!0) */, L_0, L_1);
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), _stringLiteral10, /*hidden argument*/NULL);
		Object_t * L_3 = ___action;
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* Common.Fsm.FsmState Common.Fsm.FsmAction::GetOwner() */, FsmAction_t51_il2cpp_TypeInfo_var, L_3);
		Assertion_Assert_m22(NULL /*static, unused*/, ((((Object_t*)(Object_t *)L_4) == ((Object_t*)(ConcreteFsmState_t46 *)__this))? 1 : 0), _stringLiteral11, /*hidden argument*/NULL);
		List_1_t49 * L_5 = (__this->___actionList_3);
		Object_t * L_6 = ___action;
		NullCheck(L_5);
		VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Common.Fsm.FsmAction>::Add(!0) */, L_5, L_6);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<Common.Fsm.FsmAction> Common.Fsm.ConcreteFsmState::GetActions()
extern "C" Object_t* ConcreteFsmState_GetActions_m156 (ConcreteFsmState_t46 * __this, const MethodInfo* method)
{
	{
		List_1_t49 * L_0 = (__this->___actionList_3);
		return L_0;
	}
}
// System.Void Common.Fsm.ConcreteFsmState::SendEvent(System.String)
// Common.Fsm.Fsm
#include "AssemblyU2DCSharp_Common_Fsm_FsmMethodDeclarations.h"
extern "C" void ConcreteFsmState_SendEvent_m157 (ConcreteFsmState_t46 * __this, String_t* ___eventId, const MethodInfo* method)
{
	{
		Fsm_t47 * L_0 = (__this->___owner_1);
		String_t* L_1 = ___eventId;
		NullCheck(L_0);
		Fsm_SendEvent_m174(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Common.Fsm.Fsm/StateActionProcessor
#include "AssemblyU2DCSharp_Common_Fsm_Fsm_StateActionProcessor.h"
// Common.Fsm.Fsm/StateActionProcessor
#include "AssemblyU2DCSharp_Common_Fsm_Fsm_StateActionProcessorMethodDeclarations.h"
// System.Void Common.Fsm.Fsm/StateActionProcessor::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void StateActionProcessor__ctor_m158 (StateActionProcessor_t50 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void Common.Fsm.Fsm/StateActionProcessor::Invoke(Common.Fsm.FsmAction)
extern "C" void StateActionProcessor_Invoke_m159 (StateActionProcessor_t50 * __this, Object_t * ___action, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		StateActionProcessor_Invoke_m159((StateActionProcessor_t50 *)__this->___prev_9,___action, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___action, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___action,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___action, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___action,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___action,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_StateActionProcessor_t50(Il2CppObject* delegate, Object_t * ___action)
{
	// Marshaling of parameter '___action' to native representation
	Object_t * ____action_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'Common.Fsm.FsmAction'."));
}
// System.IAsyncResult Common.Fsm.Fsm/StateActionProcessor::BeginInvoke(Common.Fsm.FsmAction,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * StateActionProcessor_BeginInvoke_m160 (StateActionProcessor_t50 * __this, Object_t * ___action, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___action;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void Common.Fsm.Fsm/StateActionProcessor::EndInvoke(System.IAsyncResult)
extern "C" void StateActionProcessor_EndInvoke_m161 (StateActionProcessor_t50 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Common.Fsm.Fsm/<ExitState>c__AnonStoreyF
#include "AssemblyU2DCSharp_Common_Fsm_Fsm_U3CExitStateU3Ec__AnonStore.h"
// Common.Fsm.Fsm/<ExitState>c__AnonStoreyF
#include "AssemblyU2DCSharp_Common_Fsm_Fsm_U3CExitStateU3Ec__AnonStoreMethodDeclarations.h"
// System.Void Common.Fsm.Fsm/<ExitState>c__AnonStoreyF::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void U3CExitStateU3Ec__AnonStoreyF__ctor_m162 (U3CExitStateU3Ec__AnonStoreyF_t52 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Fsm/<ExitState>c__AnonStoreyF::<>m__1(Common.Fsm.FsmAction)
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
extern TypeInfo* FsmAction_t51_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t359_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral12;
extern "C" void U3CExitStateU3Ec__AnonStoreyF_U3CU3Em__1_m163 (U3CExitStateU3Ec__AnonStoreyF_t52 * __this, Object_t * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FsmAction_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		Exception_t359_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral12 = il2cpp_codegen_string_literal_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___action;
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void Common.Fsm.FsmAction::OnExit() */, FsmAction_t51_il2cpp_TypeInfo_var, L_0);
		Fsm_t47 * L_1 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_1);
		Object_t * L_2 = (L_1->___currentState_1);
		Object_t * L_3 = (__this->___currentStateOnInvoke_0);
		if ((((Object_t*)(Object_t *)L_2) == ((Object_t*)(Object_t *)L_3)))
		{
			goto IL_0027;
		}
	}
	{
		Exception_t359 * L_4 = (Exception_t359 *)il2cpp_codegen_object_new (Exception_t359_il2cpp_TypeInfo_var);
		Exception__ctor_m1318(L_4, _stringLiteral12, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0027:
	{
		return;
	}
}
// System.Void Common.Fsm.Fsm::.ctor(System.String)
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
extern TypeInfo* Dictionary_2_t48_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1396_MethodInfo_var;
extern "C" void Fsm__ctor_m164 (Fsm_t47 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Dictionary_2__ctor_m1396_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___name_0 = L_0;
		__this->___currentState_1 = (Object_t *)NULL;
		Dictionary_2_t48 * L_1 = (Dictionary_2_t48 *)il2cpp_codegen_object_new (Dictionary_2_t48_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1396(L_1, /*hidden argument*/Dictionary_2__ctor_m1396_MethodInfo_var);
		__this->___stateMap_2 = L_1;
		return;
	}
}
// System.String Common.Fsm.Fsm::get_Name()
extern "C" String_t* Fsm_get_Name_m165 (Fsm_t47 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_0);
		return L_0;
	}
}
// Common.Fsm.FsmState Common.Fsm.Fsm::AddState(System.String)
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
// Common.Fsm.ConcreteFsmState
#include "AssemblyU2DCSharp_Common_Fsm_ConcreteFsmStateMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ConcreteFsmState_t46_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral13;
extern "C" Object_t * Fsm_AddState_m166 (Fsm_t47 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ConcreteFsmState_t46_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		_stringLiteral13 = il2cpp_codegen_string_literal_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Dictionary_2_t48 * L_0 = (__this->___stateMap_2);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::ContainsKey(!0) */, L_0, L_1);
		String_t* L_3 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1319(NULL /*static, unused*/, _stringLiteral13, L_3, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), L_4, /*hidden argument*/NULL);
		String_t* L_5 = ___name;
		ConcreteFsmState_t46 * L_6 = (ConcreteFsmState_t46 *)il2cpp_codegen_object_new (ConcreteFsmState_t46_il2cpp_TypeInfo_var);
		ConcreteFsmState__ctor_m151(L_6, L_5, __this, /*hidden argument*/NULL);
		V_0 = L_6;
		Dictionary_2_t48 * L_7 = (__this->___stateMap_2);
		String_t* L_8 = ___name;
		Object_t * L_9 = V_0;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::set_Item(!0,!1) */, L_7, L_8, L_9);
		Object_t * L_10 = V_0;
		return L_10;
	}
}
// System.Void Common.Fsm.Fsm::ProcessStateActions(Common.Fsm.FsmState,Common.Fsm.Fsm/StateActionProcessor)
// Common.Fsm.Fsm/StateActionProcessor
#include "AssemblyU2DCSharp_Common_Fsm_Fsm_StateActionProcessor.h"
// Common.Fsm.Fsm/StateActionProcessor
#include "AssemblyU2DCSharp_Common_Fsm_Fsm_StateActionProcessorMethodDeclarations.h"
extern TypeInfo* FsmState_t29_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t340_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t378_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void Fsm_ProcessStateActions_m167 (Fsm_t47 * __this, Object_t * ___state, StateActionProcessor_t50 * ___actionProcessor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FsmState_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		IEnumerable_1_t340_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		IEnumerator_1_t378_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t* V_1 = {0};
	Object_t * V_2 = {0};
	Object_t* V_3 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___currentState_1);
		V_0 = L_0;
		Object_t * L_1 = ___state;
		NullCheck(L_1);
		Object_t* L_2 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(4 /* System.Collections.Generic.IEnumerable`1<Common.Fsm.FsmAction> Common.Fsm.FsmState::GetActions() */, FsmState_t29_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
		Object_t* L_3 = V_1;
		NullCheck(L_3);
		Object_t* L_4 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Common.Fsm.FsmAction>::GetEnumerator() */, IEnumerable_1_t340_il2cpp_TypeInfo_var, L_3);
		V_3 = L_4;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0039;
		}

IL_001a:
		{
			Object_t* L_5 = V_3;
			NullCheck(L_5);
			Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Common.Fsm.FsmAction>::get_Current() */, IEnumerator_1_t378_il2cpp_TypeInfo_var, L_5);
			V_2 = L_6;
			StateActionProcessor_t50 * L_7 = ___actionProcessor;
			Object_t * L_8 = V_2;
			NullCheck(L_7);
			StateActionProcessor_Invoke_m159(L_7, L_8, /*hidden argument*/NULL);
			Object_t * L_9 = (__this->___currentState_1);
			Object_t * L_10 = V_0;
			if ((((Object_t*)(Object_t *)L_9) == ((Object_t*)(Object_t *)L_10)))
			{
				goto IL_0039;
			}
		}

IL_0034:
		{
			goto IL_0044;
		}

IL_0039:
		{
			Object_t* L_11 = V_3;
			NullCheck(L_11);
			bool L_12 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_11);
			if (L_12)
			{
				goto IL_001a;
			}
		}

IL_0044:
		{
			IL2CPP_LEAVE(0x54, FINALLY_0049);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		{
			Object_t* L_13 = V_3;
			if (L_13)
			{
				goto IL_004d;
			}
		}

IL_004c:
		{
			IL2CPP_END_FINALLY(73)
		}

IL_004d:
		{
			Object_t* L_14 = V_3;
			NullCheck(L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_14);
			IL2CPP_END_FINALLY(73)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0054:
	{
		return;
	}
}
// System.Void Common.Fsm.Fsm::Start(System.String)
// Common.Fsm.Fsm
#include "AssemblyU2DCSharp_Common_Fsm_FsmMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral14;
extern "C" void Fsm_Start_m168 (Fsm_t47 * __this, String_t* ___stateName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral14 = il2cpp_codegen_string_literal_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t48 * L_0 = (__this->___stateMap_2);
		String_t* L_1 = ___stateName;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::ContainsKey(!0) */, L_0, L_1);
		String_t* L_3 = ___stateName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1319(NULL /*static, unused*/, _stringLiteral14, L_3, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		Dictionary_2_t48 * L_5 = (__this->___stateMap_2);
		String_t* L_6 = ___stateName;
		NullCheck(L_5);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker1< Object_t *, String_t* >::Invoke(19 /* !1 System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::get_Item(!0) */, L_5, L_6);
		Fsm_ChangeToState_m169(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Fsm::ChangeToState(Common.Fsm.FsmState)
extern "C" void Fsm_ChangeToState_m169 (Fsm_t47 * __this, Object_t * ___state, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___currentState_1);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Object_t * L_1 = (__this->___currentState_1);
		Fsm_ExitState_m171(__this, L_1, /*hidden argument*/NULL);
	}

IL_0017:
	{
		Object_t * L_2 = ___state;
		__this->___currentState_1 = L_2;
		Object_t * L_3 = (__this->___currentState_1);
		Fsm_EnterState_m170(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Fsm::EnterState(Common.Fsm.FsmState)
extern TypeInfo* Fsm_t47_il2cpp_TypeInfo_var;
extern TypeInfo* StateActionProcessor_t50_il2cpp_TypeInfo_var;
extern const MethodInfo* Fsm_U3CEnterStateU3Em__0_m177_MethodInfo_var;
extern "C" void Fsm_EnterState_m170 (Fsm_t47 * __this, Object_t * ___state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fsm_t47_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		StateActionProcessor_t50_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Fsm_U3CEnterStateU3Em__0_m177_MethodInfo_var = il2cpp_codegen_method_info_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * G_B2_0 = {0};
	Fsm_t47 * G_B2_1 = {0};
	Object_t * G_B1_0 = {0};
	Fsm_t47 * G_B1_1 = {0};
	{
		Object_t * L_0 = ___state;
		StateActionProcessor_t50 * L_1 = ((Fsm_t47_StaticFields*)Fsm_t47_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001a;
		}
	}
	{
		IntPtr_t L_2 = { (void*)Fsm_U3CEnterStateU3Em__0_m177_MethodInfo_var };
		StateActionProcessor_t50 * L_3 = (StateActionProcessor_t50 *)il2cpp_codegen_object_new (StateActionProcessor_t50_il2cpp_TypeInfo_var);
		StateActionProcessor__ctor_m158(L_3, NULL, L_2, /*hidden argument*/NULL);
		((Fsm_t47_StaticFields*)Fsm_t47_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4 = L_3;
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001a:
	{
		StateActionProcessor_t50 * L_4 = ((Fsm_t47_StaticFields*)Fsm_t47_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache4_4;
		NullCheck(G_B2_1);
		Fsm_ProcessStateActions_m167(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Fsm::ExitState(Common.Fsm.FsmState)
// Common.Fsm.Fsm/<ExitState>c__AnonStoreyF
#include "AssemblyU2DCSharp_Common_Fsm_Fsm_U3CExitStateU3Ec__AnonStoreMethodDeclarations.h"
extern TypeInfo* U3CExitStateU3Ec__AnonStoreyF_t52_il2cpp_TypeInfo_var;
extern TypeInfo* StateActionProcessor_t50_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CExitStateU3Ec__AnonStoreyF_U3CU3Em__1_m163_MethodInfo_var;
extern "C" void Fsm_ExitState_m171 (Fsm_t47 * __this, Object_t * ___state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CExitStateU3Ec__AnonStoreyF_t52_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		StateActionProcessor_t50_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		U3CExitStateU3Ec__AnonStoreyF_U3CU3Em__1_m163_MethodInfo_var = il2cpp_codegen_method_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	U3CExitStateU3Ec__AnonStoreyF_t52 * V_0 = {0};
	{
		U3CExitStateU3Ec__AnonStoreyF_t52 * L_0 = (U3CExitStateU3Ec__AnonStoreyF_t52 *)il2cpp_codegen_object_new (U3CExitStateU3Ec__AnonStoreyF_t52_il2cpp_TypeInfo_var);
		U3CExitStateU3Ec__AnonStoreyF__ctor_m162(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CExitStateU3Ec__AnonStoreyF_t52 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_1 = __this;
		U3CExitStateU3Ec__AnonStoreyF_t52 * L_2 = V_0;
		Object_t * L_3 = (__this->___currentState_1);
		NullCheck(L_2);
		L_2->___currentStateOnInvoke_0 = L_3;
		Object_t * L_4 = ___state;
		U3CExitStateU3Ec__AnonStoreyF_t52 * L_5 = V_0;
		IntPtr_t L_6 = { (void*)U3CExitStateU3Ec__AnonStoreyF_U3CU3Em__1_m163_MethodInfo_var };
		StateActionProcessor_t50 * L_7 = (StateActionProcessor_t50 *)il2cpp_codegen_object_new (StateActionProcessor_t50_il2cpp_TypeInfo_var);
		StateActionProcessor__ctor_m158(L_7, L_5, L_6, /*hidden argument*/NULL);
		Fsm_ProcessStateActions_m167(__this, L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Fsm.Fsm::Update()
extern TypeInfo* Fsm_t47_il2cpp_TypeInfo_var;
extern TypeInfo* StateActionProcessor_t50_il2cpp_TypeInfo_var;
extern const MethodInfo* Fsm_U3CUpdateU3Em__2_m178_MethodInfo_var;
extern "C" void Fsm_Update_m172 (Fsm_t47 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fsm_t47_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		StateActionProcessor_t50_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Fsm_U3CUpdateU3Em__2_m178_MethodInfo_var = il2cpp_codegen_method_info_from_index(18);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * G_B4_0 = {0};
	Fsm_t47 * G_B4_1 = {0};
	Object_t * G_B3_0 = {0};
	Fsm_t47 * G_B3_1 = {0};
	{
		Object_t * L_0 = (__this->___currentState_1);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Object_t * L_1 = (__this->___currentState_1);
		StateActionProcessor_t50 * L_2 = ((Fsm_t47_StaticFields*)Fsm_t47_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		G_B3_0 = L_1;
		G_B3_1 = __this;
		if (L_2)
		{
			G_B4_0 = L_1;
			G_B4_1 = __this;
			goto IL_002b;
		}
	}
	{
		IntPtr_t L_3 = { (void*)Fsm_U3CUpdateU3Em__2_m178_MethodInfo_var };
		StateActionProcessor_t50 * L_4 = (StateActionProcessor_t50 *)il2cpp_codegen_object_new (StateActionProcessor_t50_il2cpp_TypeInfo_var);
		StateActionProcessor__ctor_m158(L_4, NULL, L_3, /*hidden argument*/NULL);
		((Fsm_t47_StaticFields*)Fsm_t47_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5 = L_4;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_002b:
	{
		StateActionProcessor_t50 * L_5 = ((Fsm_t47_StaticFields*)Fsm_t47_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache5_5;
		NullCheck(G_B4_1);
		Fsm_ProcessStateActions_m167(G_B4_1, G_B4_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// Common.Fsm.FsmState Common.Fsm.Fsm::GetCurrentState()
extern "C" Object_t * Fsm_GetCurrentState_m173 (Fsm_t47 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___currentState_1);
		return L_0;
	}
}
// System.Void Common.Fsm.Fsm::SendEvent(System.String)
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FsmState_t29_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral15;
extern Il2CppCodeGenString* _stringLiteral16;
extern Il2CppCodeGenString* _stringLiteral17;
extern "C" void Fsm_SendEvent_m174 (Fsm_t47 * __this, String_t* ___eventId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		FsmState_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		_stringLiteral15 = il2cpp_codegen_string_literal_from_index(15);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		_stringLiteral17 = il2cpp_codegen_string_literal_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		String_t* L_0 = ___eventId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)L_1) == ((int32_t)0))? 1 : 0), _stringLiteral15, /*hidden argument*/NULL);
		Object_t * L_2 = (__this->___currentState_1);
		if (L_2)
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_3 = (__this->___name_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m1398(NULL /*static, unused*/, _stringLiteral16, L_3, /*hidden argument*/NULL);
		Debug_LogWarning_m1399(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}

IL_0034:
	{
		String_t* L_5 = ___eventId;
		Object_t * L_6 = Fsm_ResolveTransition_m175(__this, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Object_t * L_7 = V_0;
		if (L_7)
		{
			goto IL_0062;
		}
	}
	{
		Object_t * L_8 = (__this->___currentState_1);
		NullCheck(L_8);
		String_t* L_9 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Common.Fsm.FsmState::GetName() */, FsmState_t29_il2cpp_TypeInfo_var, L_8);
		String_t* L_10 = ___eventId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m1359(NULL /*static, unused*/, _stringLiteral17, L_9, L_10, /*hidden argument*/NULL);
		Debug_LogWarning_m1399(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_0062:
	{
		Object_t * L_12 = V_0;
		Fsm_ChangeToState_m169(__this, L_12, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return;
	}
}
// Common.Fsm.FsmState Common.Fsm.Fsm::ResolveTransition(System.String)
extern TypeInfo* FsmState_t29_il2cpp_TypeInfo_var;
extern "C" Object_t * Fsm_ResolveTransition_m175 (Fsm_t47 * __this, String_t* ___eventId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FsmState_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = (__this->___currentState_1);
		String_t* L_1 = ___eventId;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker1< Object_t *, String_t* >::Invoke(2 /* Common.Fsm.FsmState Common.Fsm.FsmState::GetTransition(System.String) */, FsmState_t29_il2cpp_TypeInfo_var, L_0, L_1);
		V_0 = L_2;
		Object_t * L_3 = V_0;
		if (L_3)
		{
			goto IL_0041;
		}
	}
	{
		Dictionary_2_t48 * L_4 = (__this->___globalTransitionMap_3);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		Dictionary_2_t48 * L_5 = (__this->___globalTransitionMap_3);
		String_t* L_6 = ___eventId;
		NullCheck(L_5);
		bool L_7 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::ContainsKey(!0) */, L_5, L_6);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		Dictionary_2_t48 * L_8 = (__this->___globalTransitionMap_3);
		String_t* L_9 = ___eventId;
		NullCheck(L_8);
		Object_t * L_10 = (Object_t *)VirtFuncInvoker1< Object_t *, String_t* >::Invoke(19 /* !1 System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::get_Item(!0) */, L_8, L_9);
		return L_10;
	}

IL_003c:
	{
		goto IL_0043;
	}

IL_0041:
	{
		Object_t * L_11 = V_0;
		return L_11;
	}

IL_0043:
	{
		return (Object_t *)NULL;
	}
}
// System.Void Common.Fsm.Fsm::AddGlobalTransition(System.String,Common.Fsm.FsmState)
extern TypeInfo* Dictionary_2_t48_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1396_MethodInfo_var;
extern "C" void Fsm_AddGlobalTransition_m176 (Fsm_t47 * __this, String_t* ___eventId, Object_t * ___destinationState, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t48_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		Dictionary_2__ctor_m1396_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t48 * L_0 = (__this->___globalTransitionMap_3);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Dictionary_2_t48 * L_1 = (Dictionary_2_t48 *)il2cpp_codegen_object_new (Dictionary_2_t48_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1396(L_1, /*hidden argument*/Dictionary_2__ctor_m1396_MethodInfo_var);
		__this->___globalTransitionMap_3 = L_1;
	}

IL_0016:
	{
		Dictionary_2_t48 * L_2 = (__this->___globalTransitionMap_3);
		String_t* L_3 = ___eventId;
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::ContainsKey(!0) */, L_2, L_3);
		Assertion_Assert_m21(NULL /*static, unused*/, ((((int32_t)L_4) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Dictionary_2_t48 * L_5 = (__this->___globalTransitionMap_3);
		String_t* L_6 = ___eventId;
		Object_t * L_7 = ___destinationState;
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>::set_Item(!0,!1) */, L_5, L_6, L_7);
		return;
	}
}
// System.Void Common.Fsm.Fsm::<EnterState>m__0(Common.Fsm.FsmAction)
extern TypeInfo* FsmAction_t51_il2cpp_TypeInfo_var;
extern "C" void Fsm_U3CEnterStateU3Em__0_m177 (Object_t * __this /* static, unused */, Object_t * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FsmAction_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___action;
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(1 /* System.Void Common.Fsm.FsmAction::OnEnter() */, FsmAction_t51_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Common.Fsm.Fsm::<Update>m__2(Common.Fsm.FsmAction)
extern TypeInfo* FsmAction_t51_il2cpp_TypeInfo_var;
extern "C" void Fsm_U3CUpdateU3Em__2_m178 (Object_t * __this /* static, unused */, Object_t * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FsmAction_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___action;
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void Common.Fsm.FsmAction::OnUpdate() */, FsmAction_t51_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Common.Fsm.FsmActionAdapter::.ctor(Common.Fsm.FsmState)
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void FsmActionAdapter__ctor_m179 (FsmActionAdapter_t33 * __this, Object_t * ___owner, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___owner;
		__this->___owner_0 = L_0;
		return;
	}
}
// Common.Fsm.FsmState Common.Fsm.FsmActionAdapter::GetOwner()
extern "C" Object_t * FsmActionAdapter_GetOwner_m180 (FsmActionAdapter_t33 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___owner_0);
		return L_0;
	}
}
// System.Void Common.Fsm.FsmActionAdapter::OnEnter()
extern "C" void FsmActionAdapter_OnEnter_m181 (FsmActionAdapter_t33 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Common.Fsm.FsmActionAdapter::OnUpdate()
extern "C" void FsmActionAdapter_OnUpdate_m182 (FsmActionAdapter_t33 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Common.Fsm.FsmActionAdapter::OnExit()
extern "C" void FsmActionAdapter_OnExit_m183 (FsmActionAdapter_t33 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// SelectionListGui/DoubleClickCallback
#include "AssemblyU2DCSharp_SelectionListGui_DoubleClickCallback.h"
// SelectionListGui/DoubleClickCallback
#include "AssemblyU2DCSharp_SelectionListGui_DoubleClickCallbackMethodDeclarations.h"
// System.Void SelectionListGui/DoubleClickCallback::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void DoubleClickCallback__ctor_m184 (DoubleClickCallback_t53 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void SelectionListGui/DoubleClickCallback::Invoke(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void DoubleClickCallback_Invoke_m185 (DoubleClickCallback_t53 * __this, int32_t ___index, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DoubleClickCallback_Invoke_m185((DoubleClickCallback_t53 *)__this->___prev_9,___index, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___index, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___index,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___index, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___index,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_DoubleClickCallback_t53(Il2CppObject* delegate, int32_t ___index)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___index' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___index);

	// Marshaling cleanup of parameter '___index' native representation

}
// System.IAsyncResult SelectionListGui/DoubleClickCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern "C" Object_t * DoubleClickCallback_BeginInvoke_m186 (DoubleClickCallback_t53 * __this, int32_t ___index, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t372_il2cpp_TypeInfo_var, &___index);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void SelectionListGui/DoubleClickCallback::EndInvoke(System.IAsyncResult)
extern "C" void DoubleClickCallback_EndInvoke_m187 (DoubleClickCallback_t53 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// SelectionListGui
#include "AssemblyU2DCSharp_SelectionListGui.h"
// SelectionListGui
#include "AssemblyU2DCSharp_SelectionListGuiMethodDeclarations.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_Event.h"
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
// System.Int32 SelectionListGui::SelectionList(System.Int32,UnityEngine.GUIContent[])
// System.Int32
#include "mscorlib_System_Int32.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// SelectionListGui
#include "AssemblyU2DCSharp_SelectionListGuiMethodDeclarations.h"
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral18;
extern "C" int32_t SelectionListGui_SelectionList_m188 (Object_t * __this /* static, unused */, int32_t ___selected, GUIContentU5BU5D_t341* ___list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral18 = il2cpp_codegen_string_literal_from_index(18);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___selected;
		GUIContentU5BU5D_t341* L_1 = ___list;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_2 = GUIStyle_op_Implicit_m1400(NULL /*static, unused*/, _stringLiteral18, /*hidden argument*/NULL);
		int32_t L_3 = SelectionListGui_SelectionList_m191(NULL /*static, unused*/, L_0, L_1, L_2, (DoubleClickCallback_t53 *)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 SelectionListGui::SelectionList(System.Int32,UnityEngine.GUIContent[],UnityEngine.GUIStyle)
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
extern "C" int32_t SelectionListGui_SelectionList_m189 (Object_t * __this /* static, unused */, int32_t ___selected, GUIContentU5BU5D_t341* ___list, GUIStyle_t342 * ___elementStyle, const MethodInfo* method)
{
	{
		int32_t L_0 = ___selected;
		GUIContentU5BU5D_t341* L_1 = ___list;
		GUIStyle_t342 * L_2 = ___elementStyle;
		int32_t L_3 = SelectionListGui_SelectionList_m191(NULL /*static, unused*/, L_0, L_1, L_2, (DoubleClickCallback_t53 *)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 SelectionListGui::SelectionList(System.Int32,UnityEngine.GUIContent[],SelectionListGui/DoubleClickCallback)
// SelectionListGui/DoubleClickCallback
#include "AssemblyU2DCSharp_SelectionListGui_DoubleClickCallback.h"
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral18;
extern "C" int32_t SelectionListGui_SelectionList_m190 (Object_t * __this /* static, unused */, int32_t ___selected, GUIContentU5BU5D_t341* ___list, DoubleClickCallback_t53 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral18 = il2cpp_codegen_string_literal_from_index(18);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___selected;
		GUIContentU5BU5D_t341* L_1 = ___list;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_2 = GUIStyle_op_Implicit_m1400(NULL /*static, unused*/, _stringLiteral18, /*hidden argument*/NULL);
		DoubleClickCallback_t53 * L_3 = ___callback;
		int32_t L_4 = SelectionListGui_SelectionList_m191(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 SelectionListGui::SelectionList(System.Int32,UnityEngine.GUIContent[],UnityEngine.GUIStyle,SelectionListGui/DoubleClickCallback)
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// SelectionListGui/DoubleClickCallback
#include "AssemblyU2DCSharp_SelectionListGui_DoubleClickCallbackMethodDeclarations.h"
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern "C" int32_t SelectionListGui_SelectionList_m191 (Object_t * __this /* static, unused */, int32_t ___selected, GUIContentU5BU5D_t341* ___list, GUIStyle_t342 * ___elementStyle, DoubleClickCallback_t53 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Rect_t267  V_1 = {0};
	bool V_2 = false;
	{
		V_0 = 0;
		goto IL_00b1;
	}

IL_0007:
	{
		GUIContentU5BU5D_t341* L_0 = ___list;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		GUIStyle_t342 * L_3 = ___elementStyle;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		Rect_t267  L_4 = GUILayoutUtility_GetRect_m1401(NULL /*static, unused*/, (*(GUIContent_t379 **)(GUIContent_t379 **)SZArrayLdElema(L_0, L_2, sizeof(GUIContent_t379 *))), L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Event_t381 * L_5 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector2_t2  L_6 = Event_get_mousePosition_m1403(L_5, /*hidden argument*/NULL);
		bool L_7 = Rect_Contains_m1404((&V_1), L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		bool L_8 = V_2;
		if (!L_8)
		{
			goto IL_004a;
		}
	}
	{
		Event_t381 * L_9 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = Event_get_type_m1405(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_11 = V_0;
		___selected = L_11;
		Event_t381 * L_12 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		Event_Use_m1406(L_12, /*hidden argument*/NULL);
		goto IL_00ad;
	}

IL_004a:
	{
		bool L_13 = V_2;
		if (!L_13)
		{
			goto IL_008c;
		}
	}
	{
		DoubleClickCallback_t53 * L_14 = ___callback;
		if (!L_14)
		{
			goto IL_008c;
		}
	}
	{
		Event_t381 * L_15 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = Event_get_type_m1405(L_15, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_008c;
		}
	}
	{
		Event_t381 * L_17 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		int32_t L_18 = Event_get_clickCount_m1407(L_17, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)2))))
		{
			goto IL_008c;
		}
	}
	{
		DoubleClickCallback_t53 * L_19 = ___callback;
		int32_t L_20 = V_0;
		NullCheck(L_19);
		DoubleClickCallback_Invoke_m185(L_19, L_20, /*hidden argument*/NULL);
		Event_t381 * L_21 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		Event_Use_m1406(L_21, /*hidden argument*/NULL);
		goto IL_00ad;
	}

IL_008c:
	{
		Event_t381 * L_22 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = Event_get_type_m1405(L_22, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)7))))
		{
			goto IL_00ad;
		}
	}
	{
		GUIStyle_t342 * L_24 = ___elementStyle;
		Rect_t267  L_25 = V_1;
		GUIContentU5BU5D_t341* L_26 = ___list;
		int32_t L_27 = V_0;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		bool L_29 = V_2;
		int32_t L_30 = V_0;
		int32_t L_31 = ___selected;
		NullCheck(L_24);
		GUIStyle_Draw_m1408(L_24, L_25, (*(GUIContent_t379 **)(GUIContent_t379 **)SZArrayLdElema(L_26, L_28, sizeof(GUIContent_t379 *))), L_29, 0, ((((int32_t)L_30) == ((int32_t)L_31))? 1 : 0), 0, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		int32_t L_32 = V_0;
		V_0 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_33 = V_0;
		GUIContentU5BU5D_t341* L_34 = ___list;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)(((Array_t *)L_34)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_35 = ___selected;
		return L_35;
	}
}
// System.Int32 SelectionListGui::SelectionList(System.Int32,System.String[])
#include "mscorlib_ArrayTypes.h"
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral18;
extern "C" int32_t SelectionListGui_SelectionList_m192 (Object_t * __this /* static, unused */, int32_t ___selected, StringU5BU5D_t137* ___list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral18 = il2cpp_codegen_string_literal_from_index(18);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___selected;
		StringU5BU5D_t137* L_1 = ___list;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_2 = GUIStyle_op_Implicit_m1400(NULL /*static, unused*/, _stringLiteral18, /*hidden argument*/NULL);
		int32_t L_3 = SelectionListGui_SelectionList_m195(NULL /*static, unused*/, L_0, L_1, L_2, (DoubleClickCallback_t53 *)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 SelectionListGui::SelectionList(System.Int32,System.String[],UnityEngine.GUIStyle)
extern "C" int32_t SelectionListGui_SelectionList_m193 (Object_t * __this /* static, unused */, int32_t ___selected, StringU5BU5D_t137* ___list, GUIStyle_t342 * ___elementStyle, const MethodInfo* method)
{
	{
		int32_t L_0 = ___selected;
		StringU5BU5D_t137* L_1 = ___list;
		GUIStyle_t342 * L_2 = ___elementStyle;
		int32_t L_3 = SelectionListGui_SelectionList_m195(NULL /*static, unused*/, L_0, L_1, L_2, (DoubleClickCallback_t53 *)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 SelectionListGui::SelectionList(System.Int32,System.String[],SelectionListGui/DoubleClickCallback)
extern TypeInfo* GUIStyle_t342_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral18;
extern "C" int32_t SelectionListGui_SelectionList_m194 (Object_t * __this /* static, unused */, int32_t ___selected, StringU5BU5D_t137* ___list, DoubleClickCallback_t53 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t342_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral18 = il2cpp_codegen_string_literal_from_index(18);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___selected;
		StringU5BU5D_t137* L_1 = ___list;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t342_il2cpp_TypeInfo_var);
		GUIStyle_t342 * L_2 = GUIStyle_op_Implicit_m1400(NULL /*static, unused*/, _stringLiteral18, /*hidden argument*/NULL);
		DoubleClickCallback_t53 * L_3 = ___callback;
		int32_t L_4 = SelectionListGui_SelectionList_m195(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 SelectionListGui::SelectionList(System.Int32,System.String[],UnityEngine.GUIStyle,SelectionListGui/DoubleClickCallback)
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern TypeInfo* GUIContent_t379_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t380_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral19;
extern "C" int32_t SelectionListGui_SelectionList_m195 (Object_t * __this /* static, unused */, int32_t ___selected, StringU5BU5D_t137* ___list, GUIStyle_t342 * ___elementStyle, DoubleClickCallback_t53 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		GUILayoutUtility_t380_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral19 = il2cpp_codegen_string_literal_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Rect_t267  V_1 = {0};
	bool V_2 = false;
	{
		V_0 = 0;
		goto IL_00ce;
	}

IL_0007:
	{
		StringU5BU5D_t137* L_0 = ___list;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		GUIContent_t379 * L_3 = (GUIContent_t379 *)il2cpp_codegen_object_new (GUIContent_t379_il2cpp_TypeInfo_var);
		GUIContent__ctor_m1409(L_3, (*(String_t**)(String_t**)SZArrayLdElema(L_0, L_2, sizeof(String_t*))), /*hidden argument*/NULL);
		GUIStyle_t342 * L_4 = ___elementStyle;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t380_il2cpp_TypeInfo_var);
		Rect_t267  L_5 = GUILayoutUtility_GetRect_m1401(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Event_t381 * L_6 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t2  L_7 = Event_get_mousePosition_m1403(L_6, /*hidden argument*/NULL);
		bool L_8 = Rect_Contains_m1404((&V_1), L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		bool L_9 = V_2;
		if (!L_9)
		{
			goto IL_0064;
		}
	}
	{
		Event_t381 * L_10 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = Event_get_type_m1405(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = V_0;
		___selected = L_12;
		int32_t L_13 = V_0;
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1410(NULL /*static, unused*/, _stringLiteral19, L_15, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Event_t381 * L_17 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		Event_Use_m1406(L_17, /*hidden argument*/NULL);
		goto IL_00ca;
	}

IL_0064:
	{
		bool L_18 = V_2;
		if (!L_18)
		{
			goto IL_00a6;
		}
	}
	{
		DoubleClickCallback_t53 * L_19 = ___callback;
		if (!L_19)
		{
			goto IL_00a6;
		}
	}
	{
		Event_t381 * L_20 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		int32_t L_21 = Event_get_type_m1405(L_20, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_00a6;
		}
	}
	{
		Event_t381 * L_22 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = Event_get_clickCount_m1407(L_22, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)2))))
		{
			goto IL_00a6;
		}
	}
	{
		DoubleClickCallback_t53 * L_24 = ___callback;
		int32_t L_25 = V_0;
		NullCheck(L_24);
		DoubleClickCallback_Invoke_m185(L_24, L_25, /*hidden argument*/NULL);
		Event_t381 * L_26 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		Event_Use_m1406(L_26, /*hidden argument*/NULL);
		goto IL_00ca;
	}

IL_00a6:
	{
		Event_t381 * L_27 = Event_get_current_m1402(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_27);
		int32_t L_28 = Event_get_type_m1405(L_27, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_28) == ((uint32_t)7))))
		{
			goto IL_00ca;
		}
	}
	{
		GUIStyle_t342 * L_29 = ___elementStyle;
		Rect_t267  L_30 = V_1;
		StringU5BU5D_t137* L_31 = ___list;
		int32_t L_32 = V_0;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		bool L_34 = V_2;
		int32_t L_35 = V_0;
		int32_t L_36 = ___selected;
		int32_t L_37 = V_0;
		int32_t L_38 = ___selected;
		NullCheck(L_29);
		GUIStyle_Draw_m1412(L_29, L_30, (*(String_t**)(String_t**)SZArrayLdElema(L_31, L_33, sizeof(String_t*))), L_34, 0, ((((int32_t)L_35) == ((int32_t)L_36))? 1 : 0), ((((int32_t)L_37) == ((int32_t)L_38))? 1 : 0), /*hidden argument*/NULL);
	}

IL_00ca:
	{
		int32_t L_39 = V_0;
		V_0 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_00ce:
	{
		int32_t L_40 = V_0;
		StringU5BU5D_t137* L_41 = ___list;
		NullCheck(L_41);
		if ((((int32_t)L_40) < ((int32_t)(((int32_t)(((Array_t *)L_41)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_42 = ___selected;
		return L_42;
	}
}
// HudOrthoAutoScale
#include "AssemblyU2DCSharp_HudOrthoAutoScale.h"
// HudOrthoAutoScale
#include "AssemblyU2DCSharp_HudOrthoAutoScaleMethodDeclarations.h"
struct OrthographicCamera_t4;
// Declaration !!0 UnityEngine.Component::GetComponent<OrthographicCamera>()
// !!0 UnityEngine.Component::GetComponent<OrthographicCamera>()
#define Component_GetComponent_TisOrthographicCamera_t4_m1416(__this, method) (( OrthographicCamera_t4 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void HudOrthoAutoScale::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void HudOrthoAutoScale__ctor_m196 (HudOrthoAutoScale_t55 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HudOrthoAutoScale::Start()
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// OrthographicCamera
#include "AssemblyU2DCSharp_OrthographicCameraMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisOrthographicCamera_t4_m1416_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral20;
extern "C" void HudOrthoAutoScale_Start_m197 (HudOrthoAutoScale_t55 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisOrthographicCamera_t4_m1416_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483667);
		_stringLiteral20 = il2cpp_codegen_string_literal_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t6 * L_0 = (__this->___selfCamera_2);
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Camera_t6 * L_2 = Camera_get_main_m1414(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___selfCamera_2 = L_2;
	}

IL_001c:
	{
		Camera_t6 * L_3 = (__this->___selfCamera_2);
		NullCheck(L_3);
		bool L_4 = Camera_get_orthographic_m1415(L_3, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, L_4, _stringLiteral20, /*hidden argument*/NULL);
		Camera_t6 * L_5 = (__this->___selfCamera_2);
		NullCheck(L_5);
		OrthographicCamera_t4 * L_6 = Component_GetComponent_TisOrthographicCamera_t4_m1416(L_5, /*hidden argument*/Component_GetComponent_TisOrthographicCamera_t4_m1416_MethodInfo_var);
		__this->___orthoCamera_3 = L_6;
		OrthographicCamera_t4 * L_7 = (__this->___orthoCamera_3);
		bool L_8 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_7, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005f;
		}
	}
	{
		OrthographicCamera_t4 * L_9 = (__this->___orthoCamera_3);
		NullCheck(L_9);
		OrthographicCamera_AddObserver_m32(L_9, __this, /*hidden argument*/NULL);
	}

IL_005f:
	{
		Transform_t35 * L_10 = Component_get_transform_m1417(__this, /*hidden argument*/NULL);
		__this->___selfTransform_4 = L_10;
		Camera_t6 * L_11 = (__this->___selfCamera_2);
		NullCheck(L_11);
		float L_12 = Camera_get_orthographicSize_m1418(L_11, /*hidden argument*/NULL);
		__this->___originalOrthoSize_5 = L_12;
		Camera_t6 * L_13 = (__this->___selfCamera_2);
		NullCheck(L_13);
		float L_14 = Camera_get_orthographicSize_m1418(L_13, /*hidden argument*/NULL);
		__this->___prevOrthoSize_6 = L_14;
		Transform_t35 * L_15 = (__this->___selfTransform_4);
		NullCheck(L_15);
		Vector3_t36  L_16 = Transform_get_position_m1388(L_15, /*hidden argument*/NULL);
		__this->___originalPosition_7 = L_16;
		Transform_t35 * L_17 = (__this->___selfTransform_4);
		NullCheck(L_17);
		Vector3_t36  L_18 = Transform_get_localScale_m1419(L_17, /*hidden argument*/NULL);
		__this->___originalScale_8 = L_18;
		return;
	}
}
// System.Void HudOrthoAutoScale::Update()
extern "C" void HudOrthoAutoScale_Update_m198 (HudOrthoAutoScale_t55 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HudOrthoAutoScale::OnChangeOrthoSize(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
extern "C" void HudOrthoAutoScale_OnChangeOrthoSize_m199 (HudOrthoAutoScale_t55 * __this, float ___newSize, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t36  V_1 = {0};
	Vector3_t36  V_2 = {0};
	{
		float L_0 = ___newSize;
		__this->___prevOrthoSize_6 = L_0;
		float L_1 = (__this->___prevOrthoSize_6);
		float L_2 = (__this->___originalOrthoSize_5);
		V_0 = ((float)((float)L_1/(float)L_2));
		Vector3_t36  L_3 = (__this->___originalScale_8);
		float L_4 = V_0;
		Vector3_t36  L_5 = Vector3_op_Multiply_m1387(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Transform_t35 * L_6 = (__this->___selfTransform_4);
		Vector3_t36  L_7 = V_1;
		NullCheck(L_6);
		Transform_set_localScale_m1393(L_6, L_7, /*hidden argument*/NULL);
		Vector3_t36  L_8 = (__this->___originalPosition_7);
		float L_9 = V_0;
		Vector3_t36  L_10 = Vector3_op_Multiply_m1387(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		Vector3_t36 * L_11 = &(__this->___originalPosition_7);
		float L_12 = (L_11->___z_3);
		(&V_2)->___z_3 = L_12;
		Transform_t35 * L_13 = (__this->___selfTransform_4);
		Vector3_t36  L_14 = V_2;
		NullCheck(L_13);
		Transform_set_position_m1383(L_13, L_14, /*hidden argument*/NULL);
		return;
	}
}
// InputLayer
#include "AssemblyU2DCSharp_InputLayer.h"
// InputLayer
#include "AssemblyU2DCSharp_InputLayerMethodDeclarations.h"
// InputLayerElement
#include "AssemblyU2DCSharp_InputLayerElement.h"
#include "Assembly-CSharp_ArrayTypes.h"
// InputLayerElement
#include "AssemblyU2DCSharp_InputLayerElementMethodDeclarations.h"
// System.Void InputLayer::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void InputLayer__ctor_m200 (InputLayer_t56 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean InputLayer::get_IsModal()
extern "C" bool InputLayer_get_IsModal_m201 (InputLayer_t56 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___modal_2);
		return L_0;
	}
}
// System.Boolean InputLayer::get_IsActive()
extern "C" bool InputLayer_get_IsActive_m202 (InputLayer_t56 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___active_3);
		return L_0;
	}
}
// System.Void InputLayer::Activate()
extern "C" void InputLayer_Activate_m203 (InputLayer_t56 * __this, const MethodInfo* method)
{
	{
		__this->___active_3 = 1;
		return;
	}
}
// System.Void InputLayer::Deactivate()
extern "C" void InputLayer_Deactivate_m204 (InputLayer_t56 * __this, const MethodInfo* method)
{
	{
		__this->___active_3 = 0;
		return;
	}
}
// System.Boolean InputLayer::RespondsToTouchPosition(UnityEngine.Vector3,InputLayer)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// InputLayer
#include "AssemblyU2DCSharp_InputLayer.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// InputLayerElement
#include "AssemblyU2DCSharp_InputLayerElementMethodDeclarations.h"
extern "C" bool InputLayer_RespondsToTouchPosition_m205 (InputLayer_t56 * __this, Vector3_t36  ___touchPos, InputLayer_t56 * ___requesterLayer, const MethodInfo* method)
{
	InputLayerElement_t58 * V_0 = {0};
	InputLayerElementU5BU5D_t57* V_1 = {0};
	int32_t V_2 = 0;
	{
		InputLayer_t56 * L_0 = ___requesterLayer;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		InputLayerElementU5BU5D_t57* L_2 = (__this->___elements_4);
		V_1 = L_2;
		V_2 = 0;
		goto IL_0032;
	}

IL_001c:
	{
		InputLayerElementU5BU5D_t57* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_0 = (*(InputLayerElement_t58 **)(InputLayerElement_t58 **)SZArrayLdElema(L_3, L_5, sizeof(InputLayerElement_t58 *)));
		InputLayerElement_t58 * L_6 = V_0;
		Vector3_t36  L_7 = ___touchPos;
		NullCheck(L_6);
		bool L_8 = InputLayerElement_RespondsToTouchPosition_m207(L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_10 = V_2;
		InputLayerElementU5BU5D_t57* L_11 = V_1;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)(((Array_t *)L_11)->max_length))))))
		{
			goto IL_001c;
		}
	}
	{
		return 0;
	}
}
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// System.Collections.Generic.List`1<UnityEngine.Collider>
#include "mscorlib_System_Collections_Generic_List_1_gen_3.h"
// System.Collections.Generic.List`1<UnityEngine.Collider>
#include "mscorlib_System_Collections_Generic_List_1_gen_3MethodDeclarations.h"
struct Camera_t6;
struct String_t;
struct Object_t;
struct String_t;
// Declaration !!0 CommonUtils::GetComponent<System.Object>(System.String)
// !!0 CommonUtils::GetComponent<System.Object>(System.String)
extern "C" Object_t * CommonUtils_GetComponent_TisObject_t_m1423_gshared (Object_t * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define CommonUtils_GetComponent_TisObject_t_m1423(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, String_t*, const MethodInfo*))CommonUtils_GetComponent_TisObject_t_m1423_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 CommonUtils::GetComponent<UnityEngine.Camera>(System.String)
// !!0 CommonUtils::GetComponent<UnityEngine.Camera>(System.String)
#define CommonUtils_GetComponent_TisCamera_t6_m1422(__this /* static, unused */, p0, method) (( Camera_t6 * (*) (Object_t * /* static, unused */, String_t*, const MethodInfo*))CommonUtils_GetComponent_TisObject_t_m1423_gshared)(__this /* static, unused */, p0, method)
// System.Void InputLayerElement::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern TypeInfo* RaycastHit_t60_il2cpp_TypeInfo_var;
extern "C" void InputLayerElement__ctor_m206 (InputLayerElement_t58 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RaycastHit_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t60  V_0 = {0};
	{
		Initobj (RaycastHit_t60_il2cpp_TypeInfo_var, (&V_0));
		RaycastHit_t60  L_0 = V_0;
		__this->___hit_5 = L_0;
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean InputLayerElement::RespondsToTouchPosition(UnityEngine.Vector3)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// InputLayerElement
#include "AssemblyU2DCSharp_InputLayerElementMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
extern "C" bool InputLayerElement_RespondsToTouchPosition_m207 (InputLayerElement_t58 * __this, Vector3_t36  ___touchPos, const MethodInfo* method)
{
	Ray_t382  V_0 = {0};
	Collider_t369 * V_1 = {0};
	ColliderU5BU5D_t59* V_2 = {0};
	int32_t V_3 = 0;
	{
		Camera_t6 * L_0 = InputLayerElement_get_ReferenceCamera_m208(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		Camera_t6 * L_2 = InputLayerElement_get_ReferenceCamera_m208(__this, /*hidden argument*/NULL);
		Vector3_t36  L_3 = ___touchPos;
		NullCheck(L_2);
		Ray_t382  L_4 = Camera_ScreenPointToRay_m1420(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ColliderU5BU5D_t59* L_5 = (__this->___touchColliders_2);
		V_2 = L_5;
		V_3 = 0;
		goto IL_004f;
	}

IL_002e:
	{
		ColliderU5BU5D_t59* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_1 = (*(Collider_t369 **)(Collider_t369 **)SZArrayLdElema(L_6, L_8, sizeof(Collider_t369 *)));
		Collider_t369 * L_9 = V_1;
		Ray_t382  L_10 = V_0;
		RaycastHit_t60 * L_11 = &(__this->___hit_5);
		NullCheck(L_9);
		bool L_12 = Collider_Raycast_m1421(L_9, L_10, L_11, (1000.0f), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004b;
		}
	}
	{
		return 1;
	}

IL_004b:
	{
		int32_t L_13 = V_3;
		V_3 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004f:
	{
		int32_t L_14 = V_3;
		ColliderU5BU5D_t59* L_15 = V_2;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)(((Array_t *)L_15)->max_length))))))
		{
			goto IL_002e;
		}
	}
	{
		return 0;
	}
}
// UnityEngine.Camera InputLayerElement::get_ReferenceCamera()
// CommonUtils
#include "AssemblyU2DCSharp_CommonUtilsMethodDeclarations.h"
extern const MethodInfo* CommonUtils_GetComponent_TisCamera_t6_m1422_MethodInfo_var;
extern "C" Camera_t6 * InputLayerElement_get_ReferenceCamera_m208 (InputLayerElement_t58 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CommonUtils_GetComponent_TisCamera_t6_m1422_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483668);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t6 * L_0 = (__this->___referenceCamera_4);
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_2 = (__this->___referenceCameraName_3);
		Camera_t6 * L_3 = CommonUtils_GetComponent_TisCamera_t6_m1422(NULL /*static, unused*/, L_2, /*hidden argument*/CommonUtils_GetComponent_TisCamera_t6_m1422_MethodInfo_var);
		__this->___referenceCamera_4 = L_3;
	}

IL_0022:
	{
		Camera_t6 * L_4 = (__this->___referenceCamera_4);
		return L_4;
	}
}
// System.Void InputLayerElement::SetColliders(System.Collections.Generic.List`1<UnityEngine.Collider>)
// System.Collections.Generic.List`1<UnityEngine.Collider>
#include "mscorlib_System_Collections_Generic_List_1_gen_3.h"
extern TypeInfo* ColliderU5BU5D_t59_il2cpp_TypeInfo_var;
extern "C" void InputLayerElement_SetColliders_m209 (InputLayerElement_t58 * __this, List_1_t343 * ___colliderList, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ColliderU5BU5D_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t343 * L_0 = ___colliderList;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Collider>::get_Count() */, L_0);
		__this->___touchColliders_2 = ((ColliderU5BU5D_t59*)SZArrayNew(ColliderU5BU5D_t59_il2cpp_TypeInfo_var, L_1));
		V_0 = 0;
		goto IL_002b;
	}

IL_0018:
	{
		ColliderU5BU5D_t59* L_2 = (__this->___touchColliders_2);
		int32_t L_3 = V_0;
		List_1_t343 * L_4 = ___colliderList;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		Collider_t369 * L_6 = (Collider_t369 *)VirtFuncInvoker1< Collider_t369 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Collider>::get_Item(System.Int32) */, L_4, L_5);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		*((Collider_t369 **)(Collider_t369 **)SZArrayLdElema(L_2, L_3, sizeof(Collider_t369 *))) = (Collider_t369 *)L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_8 = V_0;
		List_1_t343 * L_9 = ___colliderList;
		NullCheck(L_9);
		int32_t L_10 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Collider>::get_Count() */, L_9);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0018;
		}
	}
	{
		return;
	}
}
// InputLayerStack
#include "AssemblyU2DCSharp_InputLayerStack.h"
// InputLayerStack
#include "AssemblyU2DCSharp_InputLayerStackMethodDeclarations.h"
// System.Collections.Generic.List`1<InputLayer>
#include "mscorlib_System_Collections_Generic_List_1_gen_4.h"
// System.Collections.Generic.List`1<InputLayer>
#include "mscorlib_System_Collections_Generic_List_1_gen_4MethodDeclarations.h"
// System.Void InputLayerStack::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void InputLayerStack__ctor_m210 (InputLayerStack_t61 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InputLayerStack::Awake()
// System.Collections.Generic.List`1<InputLayer>
#include "mscorlib_System_Collections_Generic_List_1_gen_4MethodDeclarations.h"
// InputLayerStack
#include "AssemblyU2DCSharp_InputLayerStackMethodDeclarations.h"
extern TypeInfo* List_1_t63_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1424_MethodInfo_var;
extern "C" void InputLayerStack_Awake_m211 (InputLayerStack_t61 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		List_1__ctor_m1424_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483669);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t63 * L_0 = (List_1_t63 *)il2cpp_codegen_object_new (List_1_t63_il2cpp_TypeInfo_var);
		List_1__ctor_m1424(L_0, /*hidden argument*/List_1__ctor_m1424_MethodInfo_var);
		__this->___layerStack_4 = L_0;
		InputLayerStack_ResetStack_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InputLayerStack::ResetStack()
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern "C" void InputLayerStack_ResetStack_m212 (InputLayerStack_t61 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		InputLayerStack_Clear_m218(__this, /*hidden argument*/NULL);
		InputLayerU5BU5D_t62* L_0 = (__this->___initialLayers_2);
		NullCheck(L_0);
		V_0 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))-(int32_t)1));
		goto IL_0035;
	}

IL_0016:
	{
		InputLayerU5BU5D_t62* L_1 = (__this->___initialLayers_2);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		Assertion_AssertNotNull_m26(NULL /*static, unused*/, (*(InputLayer_t56 **)(InputLayer_t56 **)SZArrayLdElema(L_1, L_3, sizeof(InputLayer_t56 *))), /*hidden argument*/NULL);
		InputLayerU5BU5D_t62* L_4 = (__this->___initialLayers_2);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		InputLayerStack_Push_m213(__this, (*(InputLayer_t56 **)(InputLayer_t56 **)SZArrayLdElema(L_4, L_6, sizeof(InputLayer_t56 *))), /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7-(int32_t)1));
	}

IL_0035:
	{
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_0016;
		}
	}
	{
		return;
	}
}
// System.Void InputLayerStack::Push(InputLayer)
// InputLayer
#include "AssemblyU2DCSharp_InputLayer.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern "C" void InputLayerStack_Push_m213 (InputLayerStack_t61 * __this, InputLayer_t56 * ___layer, const MethodInfo* method)
{
	{
		bool L_0 = InputLayerStack_IsEmpty_m217(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		InputLayer_t56 * L_1 = ___layer;
		InputLayer_t56 * L_2 = InputLayerStack_Top_m216(__this, /*hidden argument*/NULL);
		bool L_3 = Object_op_Equality_m1413(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		return;
	}

IL_001d:
	{
		List_1_t63 * L_4 = (__this->___layerStack_4);
		InputLayer_t56 * L_5 = ___layer;
		NullCheck(L_4);
		VirtActionInvoker1< InputLayer_t56 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<InputLayer>::Add(!0) */, L_4, L_5);
		InputLayerStack_EnsureModality_m215(__this, /*hidden argument*/NULL);
		InputLayer_t56 * L_6 = InputLayerStack_Top_m216(__this, /*hidden argument*/NULL);
		__this->___topLayer_3 = L_6;
		return;
	}
}
// System.Void InputLayerStack::Pop()
// InputLayer
#include "AssemblyU2DCSharp_InputLayerMethodDeclarations.h"
extern "C" void InputLayerStack_Pop_m214 (InputLayerStack_t61 * __this, const MethodInfo* method)
{
	InputLayer_t56 * V_0 = {0};
	{
		bool L_0 = InputLayerStack_IsEmpty_m217(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		InputLayer_t56 * L_1 = InputLayerStack_Top_m216(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		InputLayer_t56 * L_2 = V_0;
		NullCheck(L_2);
		InputLayer_Deactivate_m204(L_2, /*hidden argument*/NULL);
		List_1_t63 * L_3 = (__this->___layerStack_4);
		List_1_t63 * L_4 = (__this->___layerStack_4);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<InputLayer>::get_Count() */, L_4);
		NullCheck(L_3);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<InputLayer>::RemoveAt(System.Int32) */, L_3, ((int32_t)((int32_t)L_5-(int32_t)1)));
		InputLayerStack_EnsureModality_m215(__this, /*hidden argument*/NULL);
		bool L_6 = InputLayerStack_IsEmpty_m217(__this, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004e;
		}
	}
	{
		InputLayer_t56 * L_7 = InputLayerStack_Top_m216(__this, /*hidden argument*/NULL);
		__this->___topLayer_3 = L_7;
	}

IL_004e:
	{
		return;
	}
}
// System.Void InputLayerStack::EnsureModality()
extern "C" void InputLayerStack_EnsureModality_m215 (InputLayerStack_t61 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	InputLayer_t56 * V_2 = {0};
	{
		V_0 = 1;
		List_1_t63 * L_0 = (__this->___layerStack_4);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<InputLayer>::get_Count() */, L_0);
		V_1 = ((int32_t)((int32_t)L_1-(int32_t)1));
		goto IL_0050;
	}

IL_0015:
	{
		List_1_t63 * L_2 = (__this->___layerStack_4);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		InputLayer_t56 * L_4 = (InputLayer_t56 *)VirtFuncInvoker1< InputLayer_t56 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<InputLayer>::get_Item(System.Int32) */, L_2, L_3);
		V_2 = L_4;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		InputLayer_t56 * L_6 = V_2;
		NullCheck(L_6);
		InputLayer_Activate_m203(L_6, /*hidden argument*/NULL);
		goto IL_0039;
	}

IL_0033:
	{
		InputLayer_t56 * L_7 = V_2;
		NullCheck(L_7);
		InputLayer_Deactivate_m204(L_7, /*hidden argument*/NULL);
	}

IL_0039:
	{
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_004c;
		}
	}
	{
		InputLayer_t56 * L_9 = V_2;
		NullCheck(L_9);
		bool L_10 = InputLayer_get_IsModal_m201(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
	}

IL_004c:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11-(int32_t)1));
	}

IL_0050:
	{
		int32_t L_12 = V_1;
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		return;
	}
}
// InputLayer InputLayerStack::Top()
extern Il2CppCodeGenString* _stringLiteral21;
extern "C" InputLayer_t56 * InputLayerStack_Top_m216 (InputLayerStack_t61 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral21 = il2cpp_codegen_string_literal_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = InputLayerStack_IsEmpty_m217(__this, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)L_0) == ((int32_t)0))? 1 : 0), _stringLiteral21, /*hidden argument*/NULL);
		List_1_t63 * L_1 = (__this->___layerStack_4);
		List_1_t63 * L_2 = (__this->___layerStack_4);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<InputLayer>::get_Count() */, L_2);
		NullCheck(L_1);
		InputLayer_t56 * L_4 = (InputLayer_t56 *)VirtFuncInvoker1< InputLayer_t56 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<InputLayer>::get_Item(System.Int32) */, L_1, ((int32_t)((int32_t)L_3-(int32_t)1)));
		return L_4;
	}
}
// System.Boolean InputLayerStack::IsEmpty()
extern "C" bool InputLayerStack_IsEmpty_m217 (InputLayerStack_t61 * __this, const MethodInfo* method)
{
	{
		List_1_t63 * L_0 = (__this->___layerStack_4);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<InputLayer>::get_Count() */, L_0);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void InputLayerStack::Clear()
extern "C" void InputLayerStack_Clear_m218 (InputLayerStack_t61 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		InputLayerStack_Pop_m214(__this, /*hidden argument*/NULL);
	}

IL_000b:
	{
		bool L_0 = InputLayerStack_IsEmpty_m217(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean InputLayerStack::RespondsToTouchPosition(UnityEngine.Vector3,InputLayer)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
extern "C" bool InputLayerStack_RespondsToTouchPosition_m219 (InputLayerStack_t61 * __this, Vector3_t36  ___touchPos, InputLayer_t56 * ___requesterLayer, const MethodInfo* method)
{
	int32_t V_0 = 0;
	InputLayer_t56 * V_1 = {0};
	{
		List_1_t63 * L_0 = (__this->___layerStack_4);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<InputLayer>::get_Count() */, L_0);
		V_0 = ((int32_t)((int32_t)L_1-(int32_t)1));
		goto IL_004c;
	}

IL_0013:
	{
		List_1_t63 * L_2 = (__this->___layerStack_4);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		InputLayer_t56 * L_4 = (InputLayer_t56 *)VirtFuncInvoker1< InputLayer_t56 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<InputLayer>::get_Item(System.Int32) */, L_2, L_3);
		V_1 = L_4;
		InputLayer_t56 * L_5 = V_1;
		InputLayer_t56 * L_6 = ___requesterLayer;
		bool L_7 = Object_op_Equality_m1413(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_002e;
		}
	}
	{
		return 0;
	}

IL_002e:
	{
		InputLayer_t56 * L_8 = V_1;
		NullCheck(L_8);
		bool L_9 = InputLayer_get_IsActive_m202(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0048;
		}
	}
	{
		InputLayer_t56 * L_10 = V_1;
		Vector3_t36  L_11 = ___touchPos;
		NullCheck(L_10);
		bool L_12 = InputLayer_RespondsToTouchPosition_m205(L_10, L_11, (InputLayer_t56 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0048;
		}
	}
	{
		return 1;
	}

IL_0048:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_004c:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}
}
// InterpolationUtils
#include "AssemblyU2DCSharp_InterpolationUtils.h"
// InterpolationUtils
#include "AssemblyU2DCSharp_InterpolationUtilsMethodDeclarations.h"
// System.Void InterpolationUtils::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void InterpolationUtils__ctor_m220 (InterpolationUtils_t64 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single InterpolationUtils::Lerp(System.Single,System.Single,System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" float InterpolationUtils_Lerp_m221 (Object_t * __this /* static, unused */, float ___from, float ___to, float ___t, const MethodInfo* method)
{
	{
		float L_0 = ___from;
		float L_1 = ___to;
		float L_2 = ___from;
		float L_3 = ___t;
		return ((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)L_3))));
	}
}
// System.Single InterpolationUtils::SmoothStep(System.Single)
extern "C" float InterpolationUtils_SmoothStep_m222 (Object_t * __this /* static, unused */, float ___interpolationValue, const MethodInfo* method)
{
	{
		float L_0 = ___interpolationValue;
		float L_1 = ___interpolationValue;
		float L_2 = ___interpolationValue;
		return ((float)((float)((float)((float)L_0*(float)L_1))*(float)((float)((float)(3.0f)-(float)((float)((float)(2.0f)*(float)L_2))))));
	}
}
// System.Single InterpolationUtils::GetParabolicPos(System.Single,System.Single,System.Single,System.Single)
// InterpolationUtils
#include "AssemblyU2DCSharp_InterpolationUtilsMethodDeclarations.h"
extern "C" float InterpolationUtils_GetParabolicPos_m223 (Object_t * __this /* static, unused */, float ___t, float ___y0, float ___y1, float ___dh, const MethodInfo* method)
{
	{
		float L_0 = ___t;
		float L_1 = ___y0;
		float L_2 = ___y1;
		float L_3 = ___dh;
		float L_4 = InterpolationUtils_GetParabolicPos_m224(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (0.2f), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single InterpolationUtils::GetParabolicPos(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" float InterpolationUtils_GetParabolicPos_m224 (Object_t * __this /* static, unused */, float ___t, float ___y0, float ___y1, float ___dh, float ___tFracIncrease, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		float L_0 = ___y0;
		V_0 = L_0;
		float L_1 = ___tFracIncrease;
		V_1 = L_1;
		float L_2 = V_1;
		float L_3 = V_1;
		V_2 = ((float)((float)L_2*(float)L_3));
		float L_4 = ___y0;
		float L_5 = ___dh;
		V_3 = ((float)((float)L_4-(float)L_5));
		float L_6 = ___y1;
		float L_7 = V_1;
		float L_8 = V_3;
		float L_9 = ___y0;
		float L_10 = V_1;
		float L_11 = V_1;
		float L_12 = V_2;
		V_4 = ((float)((float)((float)((float)((float)((float)((float)((float)L_6*(float)L_7))-(float)L_8))-(float)((float)((float)L_9*(float)((float)((float)L_10-(float)(1.0f)))))))/(float)((float)((float)L_11-(float)L_12))));
		float L_13 = ___y1;
		float L_14 = ___y0;
		float L_15 = V_4;
		V_5 = ((float)((float)((float)((float)L_13-(float)L_14))-(float)L_15));
		float L_16 = V_4;
		float L_17 = ___t;
		float L_18 = ___t;
		float L_19 = V_5;
		float L_20 = ___t;
		float L_21 = ___y0;
		V_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))*(float)L_18))+(float)((float)((float)L_19*(float)L_20))))+(float)L_21));
		float L_22 = V_0;
		return L_22;
	}
}
// MiniJSON.Json/Parser/TOKEN
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser_TOKEN.h"
// MiniJSON.Json/Parser/TOKEN
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser_TOKENMethodDeclarations.h"
// MiniJSON.Json/Parser
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser.h"
// MiniJSON.Json/Parser
#include "AssemblyU2DCSharp_MiniJSON_Json_ParserMethodDeclarations.h"
// System.IO.StringReader
#include "mscorlib_System_IO_StringReader.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.IO.TextReader
#include "mscorlib_System_IO_TextReader.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_0.h"
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_5.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// System.Int64
#include "mscorlib_System_Int64.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1.h"
// System.IO.StringReader
#include "mscorlib_System_IO_StringReaderMethodDeclarations.h"
// System.Char
#include "mscorlib_System_CharMethodDeclarations.h"
// System.IO.TextReader
#include "mscorlib_System_IO_TextReaderMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_0MethodDeclarations.h"
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
// System.Int64
#include "mscorlib_System_Int64MethodDeclarations.h"
// System.Double
#include "mscorlib_System_DoubleMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
// System.Void MiniJSON.Json/Parser::.ctor(System.String)
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.IO.StringReader
#include "mscorlib_System_IO_StringReaderMethodDeclarations.h"
extern TypeInfo* StringReader_t67_il2cpp_TypeInfo_var;
extern "C" void Parser__ctor_m225 (Parser_t66 * __this, String_t* ___jsonString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringReader_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___jsonString;
		StringReader_t67 * L_1 = (StringReader_t67 *)il2cpp_codegen_object_new (StringReader_t67_il2cpp_TypeInfo_var);
		StringReader__ctor_m1425(L_1, L_0, /*hidden argument*/NULL);
		__this->___json_1 = L_1;
		return;
	}
}
// System.Boolean MiniJSON.Json/Parser::IsWordBreak(System.Char)
// System.Char
#include "mscorlib_System_Char.h"
// System.Char
#include "mscorlib_System_CharMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* Char_t383_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral22;
extern "C" bool Parser_IsWordBreak_m226 (Object_t * __this /* static, unused */, uint16_t ___c, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		uint16_t L_0 = ___c;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t383_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsWhiteSpace_m1426(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		uint16_t L_2 = ___c;
		NullCheck(_stringLiteral22);
		int32_t L_3 = String_IndexOf_m1427(_stringLiteral22, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Object MiniJSON.Json/Parser::Parse(System.String)
// MiniJSON.Json/Parser
#include "AssemblyU2DCSharp_MiniJSON_Json_ParserMethodDeclarations.h"
extern TypeInfo* Parser_t66_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" Object_t * Parser_Parse_m227 (Object_t * __this /* static, unused */, String_t* ___jsonString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Parser_t66_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(56);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Parser_t66 * V_0 = {0};
	Object_t * V_1 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___jsonString;
		Parser_t66 * L_1 = (Parser_t66 *)il2cpp_codegen_object_new (Parser_t66_il2cpp_TypeInfo_var);
		Parser__ctor_m225(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			Parser_t66 * L_2 = V_0;
			NullCheck(L_2);
			Object_t * L_3 = Parser_ParseValue_m231(L_2, /*hidden argument*/NULL);
			V_1 = L_3;
			IL2CPP_LEAVE(0x25, FINALLY_0018);
		}

IL_0013:
		{
			; // IL_0013: leave IL_0025
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0018;
	}

FINALLY_0018:
	{ // begin finally (depth: 1)
		{
			Parser_t66 * L_4 = V_0;
			if (!L_4)
			{
				goto IL_0024;
			}
		}

IL_001e:
		{
			Parser_t66 * L_5 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_5);
		}

IL_0024:
		{
			IL2CPP_END_FINALLY(24)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(24)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0025:
	{
		Object_t * L_6 = V_1;
		return L_6;
	}
}
// System.Void MiniJSON.Json/Parser::Dispose()
extern "C" void Parser_Dispose_m228 (Parser_t66 * __this, const MethodInfo* method)
{
	{
		StringReader_t67 * L_0 = (__this->___json_1);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void System.IO.TextReader::Dispose() */, L_0);
		__this->___json_1 = (StringReader_t67 *)NULL;
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> MiniJSON.Json/Parser::ParseObject()
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_0MethodDeclarations.h"
extern TypeInfo* Dictionary_2_t86_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1428_MethodInfo_var;
extern "C" Dictionary_2_t86 * Parser_ParseObject_m229 (Parser_t66 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t86_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(57);
		Dictionary_2__ctor_m1428_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483670);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t86 * V_0 = {0};
	String_t* V_1 = {0};
	int32_t V_2 = {0};
	{
		Dictionary_2_t86 * L_0 = (Dictionary_2_t86 *)il2cpp_codegen_object_new (Dictionary_2_t86_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1428(L_0, /*hidden argument*/Dictionary_2__ctor_m1428_MethodInfo_var);
		V_0 = L_0;
		StringReader_t67 * L_1 = (__this->___json_1);
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_1);
	}

IL_0012:
	{
		int32_t L_2 = Parser_get_NextToken_m239(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_2;
		if (L_3 == 0)
		{
			goto IL_0037;
		}
		if (L_3 == 1)
		{
			goto IL_002b;
		}
		if (L_3 == 2)
		{
			goto IL_003e;
		}
	}

IL_002b:
	{
		int32_t L_4 = V_2;
		if ((((int32_t)L_4) == ((int32_t)6)))
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0040;
	}

IL_0037:
	{
		return (Dictionary_2_t86 *)NULL;
	}

IL_0039:
	{
		goto IL_0012;
	}

IL_003e:
	{
		Dictionary_2_t86 * L_5 = V_0;
		return L_5;
	}

IL_0040:
	{
		String_t* L_6 = Parser_ParseString_m233(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = V_1;
		if (L_7)
		{
			goto IL_004f;
		}
	}
	{
		return (Dictionary_2_t86 *)NULL;
	}

IL_004f:
	{
		int32_t L_8 = Parser_get_NextToken_m239(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)5)))
		{
			goto IL_005d;
		}
	}
	{
		return (Dictionary_2_t86 *)NULL;
	}

IL_005d:
	{
		StringReader_t67 * L_9 = (__this->___json_1);
		NullCheck(L_9);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_9);
		Dictionary_2_t86 * L_10 = V_0;
		String_t* L_11 = V_1;
		Object_t * L_12 = Parser_ParseValue_m231(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_10, L_11, L_12);
		goto IL_007b;
	}

IL_007b:
	{
		goto IL_0012;
	}
}
// System.Collections.Generic.List`1<System.Object> MiniJSON.Json/Parser::ParseArray()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"
extern TypeInfo* List_1_t344_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1429_MethodInfo_var;
extern "C" List_1_t344 * Parser_ParseArray_m230 (Parser_t66 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t344_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		List_1__ctor_m1429_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483671);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t344 * V_0 = {0};
	bool V_1 = false;
	int32_t V_2 = {0};
	Object_t * V_3 = {0};
	int32_t V_4 = {0};
	{
		List_1_t344 * L_0 = (List_1_t344 *)il2cpp_codegen_object_new (List_1_t344_il2cpp_TypeInfo_var);
		List_1__ctor_m1429(L_0, /*hidden argument*/List_1__ctor_m1429_MethodInfo_var);
		V_0 = L_0;
		StringReader_t67 * L_1 = (__this->___json_1);
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_1);
		V_1 = 1;
		goto IL_0066;
	}

IL_0019:
	{
		int32_t L_2 = Parser_get_NextToken_m239(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_2;
		V_4 = L_3;
		int32_t L_4 = V_4;
		if (((int32_t)((int32_t)L_4-(int32_t)4)) == 0)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)4)) == 1)
		{
			goto IL_0038;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)4)) == 2)
		{
			goto IL_0046;
		}
	}

IL_0038:
	{
		int32_t L_5 = V_4;
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_0052;
	}

IL_0044:
	{
		return (List_1_t344 *)NULL;
	}

IL_0046:
	{
		goto IL_0066;
	}

IL_004b:
	{
		V_1 = 0;
		goto IL_0066;
	}

IL_0052:
	{
		int32_t L_6 = V_2;
		Object_t * L_7 = Parser_ParseByToken_m232(__this, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		List_1_t344 * L_8 = V_0;
		Object_t * L_9 = V_3;
		NullCheck(L_8);
		VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, L_8, L_9);
		goto IL_0066;
	}

IL_0066:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_0019;
		}
	}
	{
		List_1_t344 * L_11 = V_0;
		return L_11;
	}
}
// System.Object MiniJSON.Json/Parser::ParseValue()
extern "C" Object_t * Parser_ParseValue_m231 (Parser_t66 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = Parser_get_NextToken_m239(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		Object_t * L_2 = Parser_ParseByToken_m232(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object MiniJSON.Json/Parser::ParseByToken(MiniJSON.Json/Parser/TOKEN)
// MiniJSON.Json/Parser/TOKEN
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser_TOKEN.h"
extern TypeInfo* Boolean_t384_il2cpp_TypeInfo_var;
extern "C" Object_t * Parser_ParseByToken_m232 (Parser_t66 * __this, int32_t ___token, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t384_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___token;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0049;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0050;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 7)
		{
			goto IL_0042;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 8)
		{
			goto IL_0057;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 9)
		{
			goto IL_005e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 10)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0067;
	}

IL_003b:
	{
		String_t* L_2 = Parser_ParseString_m233(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_0042:
	{
		Object_t * L_3 = Parser_ParseNumber_m234(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0049:
	{
		Dictionary_2_t86 * L_4 = Parser_ParseObject_m229(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_0050:
	{
		List_1_t344 * L_5 = Parser_ParseArray_m230(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0057:
	{
		bool L_6 = 1;
		Object_t * L_7 = Box(Boolean_t384_il2cpp_TypeInfo_var, &L_6);
		return L_7;
	}

IL_005e:
	{
		bool L_8 = 0;
		Object_t * L_9 = Box(Boolean_t384_il2cpp_TypeInfo_var, &L_8);
		return L_9;
	}

IL_0065:
	{
		return NULL;
	}

IL_0067:
	{
		return NULL;
	}
}
// System.String MiniJSON.Json/Parser::ParseString()
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"
extern TypeInfo* StringBuilder_t70_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t385_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t373_il2cpp_TypeInfo_var;
extern "C" String_t* Parser_ParseString_m233 (Parser_t66 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		CharU5BU5D_t385_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(61);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Convert_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t70 * V_0 = {0};
	uint16_t V_1 = 0x0;
	bool V_2 = false;
	CharU5BU5D_t385* V_3 = {0};
	int32_t V_4 = 0;
	uint16_t V_5 = 0x0;
	uint16_t V_6 = 0x0;
	{
		StringBuilder_t70 * L_0 = (StringBuilder_t70 *)il2cpp_codegen_object_new (StringBuilder_t70_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1430(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringReader_t67 * L_1 = (__this->___json_1);
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_1);
		V_2 = 1;
		goto IL_017c;
	}

IL_0019:
	{
		StringReader_t67 * L_2 = (__this->___json_1);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0031;
		}
	}
	{
		V_2 = 0;
		goto IL_0182;
	}

IL_0031:
	{
		uint16_t L_4 = Parser_get_NextChar_m237(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		uint16_t L_5 = V_1;
		V_5 = L_5;
		uint16_t L_6 = V_5;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)34))))
		{
			goto IL_0052;
		}
	}
	{
		uint16_t L_7 = V_5;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)92))))
		{
			goto IL_0059;
		}
	}
	{
		goto IL_016f;
	}

IL_0052:
	{
		V_2 = 0;
		goto IL_017c;
	}

IL_0059:
	{
		StringReader_t67 * L_8 = (__this->___json_1);
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_8);
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_0071;
		}
	}
	{
		V_2 = 0;
		goto IL_017c;
	}

IL_0071:
	{
		uint16_t L_10 = Parser_get_NextChar_m237(__this, /*hidden argument*/NULL);
		V_1 = L_10;
		uint16_t L_11 = V_1;
		V_6 = L_11;
		uint16_t L_12 = V_6;
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 0)
		{
			goto IL_00ff;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 1)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 2)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 3)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 4)
		{
			goto IL_010d;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 5)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 6)
		{
			goto IL_011b;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 7)
		{
			goto IL_0129;
		}
	}

IL_00a5:
	{
		uint16_t L_13 = V_6;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)34))))
		{
			goto IL_00d7;
		}
	}
	{
		uint16_t L_14 = V_6;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)47))))
		{
			goto IL_00d7;
		}
	}
	{
		uint16_t L_15 = V_6;
		if ((((int32_t)L_15) == ((int32_t)((int32_t)92))))
		{
			goto IL_00d7;
		}
	}
	{
		uint16_t L_16 = V_6;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)98))))
		{
			goto IL_00e4;
		}
	}
	{
		uint16_t L_17 = V_6;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)102))))
		{
			goto IL_00f1;
		}
	}
	{
		goto IL_016a;
	}

IL_00d7:
	{
		StringBuilder_t70 * L_18 = V_0;
		uint16_t L_19 = V_1;
		NullCheck(L_18);
		StringBuilder_Append_m1431(L_18, L_19, /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_00e4:
	{
		StringBuilder_t70 * L_20 = V_0;
		NullCheck(L_20);
		StringBuilder_Append_m1431(L_20, 8, /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_00f1:
	{
		StringBuilder_t70 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m1431(L_21, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_00ff:
	{
		StringBuilder_t70 * L_22 = V_0;
		NullCheck(L_22);
		StringBuilder_Append_m1431(L_22, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_010d:
	{
		StringBuilder_t70 * L_23 = V_0;
		NullCheck(L_23);
		StringBuilder_Append_m1431(L_23, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_011b:
	{
		StringBuilder_t70 * L_24 = V_0;
		NullCheck(L_24);
		StringBuilder_Append_m1431(L_24, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_0129:
	{
		V_3 = ((CharU5BU5D_t385*)SZArrayNew(CharU5BU5D_t385_il2cpp_TypeInfo_var, 4));
		V_4 = 0;
		goto IL_0148;
	}

IL_0138:
	{
		CharU5BU5D_t385* L_25 = V_3;
		int32_t L_26 = V_4;
		uint16_t L_27 = Parser_get_NextChar_m237(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_25, L_26, sizeof(uint16_t))) = (uint16_t)L_27;
		int32_t L_28 = V_4;
		V_4 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_0148:
	{
		int32_t L_29 = V_4;
		if ((((int32_t)L_29) < ((int32_t)4)))
		{
			goto IL_0138;
		}
	}
	{
		StringBuilder_t70 * L_30 = V_0;
		CharU5BU5D_t385* L_31 = V_3;
		String_t* L_32 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_32 = String_CreateString_m1432(L_32, L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		int32_t L_33 = Convert_ToInt32_m1433(NULL /*static, unused*/, L_32, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_Append_m1431(L_30, (((uint16_t)L_33)), /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_016a:
	{
		goto IL_017c;
	}

IL_016f:
	{
		StringBuilder_t70 * L_34 = V_0;
		uint16_t L_35 = V_1;
		NullCheck(L_34);
		StringBuilder_Append_m1431(L_34, L_35, /*hidden argument*/NULL);
		goto IL_017c;
	}

IL_017c:
	{
		bool L_36 = V_2;
		if (L_36)
		{
			goto IL_0019;
		}
	}

IL_0182:
	{
		StringBuilder_t70 * L_37 = V_0;
		NullCheck(L_37);
		String_t* L_38 = StringBuilder_ToString_m1434(L_37, /*hidden argument*/NULL);
		return L_38;
	}
}
// System.Object MiniJSON.Json/Parser::ParseNumber()
// System.Int64
#include "mscorlib_System_Int64MethodDeclarations.h"
// System.Double
#include "mscorlib_System_DoubleMethodDeclarations.h"
extern TypeInfo* Int64_t386_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t387_il2cpp_TypeInfo_var;
extern "C" Object_t * Parser_ParseNumber_m234 (Parser_t66 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int64_t386_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(62);
		Double_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int64_t V_1 = 0;
	double V_2 = 0.0;
	{
		String_t* L_0 = Parser_get_NextWord_m238(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = String_IndexOf_m1427(L_1, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_3 = V_0;
		Int64_TryParse_m1435(NULL /*static, unused*/, L_3, (&V_1), /*hidden argument*/NULL);
		int64_t L_4 = V_1;
		int64_t L_5 = L_4;
		Object_t * L_6 = Box(Int64_t386_il2cpp_TypeInfo_var, &L_5);
		return L_6;
	}

IL_0025:
	{
		String_t* L_7 = V_0;
		Double_TryParse_m1436(NULL /*static, unused*/, L_7, (&V_2), /*hidden argument*/NULL);
		double L_8 = V_2;
		double L_9 = L_8;
		Object_t * L_10 = Box(Double_t387_il2cpp_TypeInfo_var, &L_9);
		return L_10;
	}
}
// System.Void MiniJSON.Json/Parser::EatWhitespace()
extern TypeInfo* Char_t383_il2cpp_TypeInfo_var;
extern "C" void Parser_EatWhitespace_m235 (Parser_t66 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_0027;
	}

IL_0005:
	{
		StringReader_t67 * L_0 = (__this->___json_1);
		NullCheck(L_0);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_0);
		StringReader_t67 * L_1 = (__this->___json_1);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0027;
		}
	}
	{
		goto IL_0037;
	}

IL_0027:
	{
		uint16_t L_3 = Parser_get_PeekChar_m236(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t383_il2cpp_TypeInfo_var);
		bool L_4 = Char_IsWhiteSpace_m1426(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0005;
		}
	}

IL_0037:
	{
		return;
	}
}
// System.Char MiniJSON.Json/Parser::get_PeekChar()
extern TypeInfo* Convert_t373_il2cpp_TypeInfo_var;
extern "C" uint16_t Parser_get_PeekChar_m236 (Parser_t66 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringReader_t67 * L_0 = (__this->___json_1);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		uint16_t L_2 = Convert_ToChar_m1437(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Char MiniJSON.Json/Parser::get_NextChar()
extern TypeInfo* Convert_t373_il2cpp_TypeInfo_var;
extern "C" uint16_t Parser_get_NextChar_m237 (Parser_t66 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringReader_t67 * L_0 = (__this->___json_1);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		uint16_t L_2 = Convert_ToChar_m1437(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String MiniJSON.Json/Parser::get_NextWord()
extern TypeInfo* StringBuilder_t70_il2cpp_TypeInfo_var;
extern "C" String_t* Parser_get_NextWord_m238 (Parser_t66 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t70 * V_0 = {0};
	{
		StringBuilder_t70 * L_0 = (StringBuilder_t70 *)il2cpp_codegen_object_new (StringBuilder_t70_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1430(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_002e;
	}

IL_000b:
	{
		StringBuilder_t70 * L_1 = V_0;
		uint16_t L_2 = Parser_get_NextChar_m237(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m1431(L_1, L_2, /*hidden argument*/NULL);
		StringReader_t67 * L_3 = (__this->___json_1);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_3);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_002e;
		}
	}
	{
		goto IL_003e;
	}

IL_002e:
	{
		uint16_t L_5 = Parser_get_PeekChar_m236(__this, /*hidden argument*/NULL);
		bool L_6 = Parser_IsWordBreak_m226(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_000b;
		}
	}

IL_003e:
	{
		StringBuilder_t70 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = StringBuilder_ToString_m1434(L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// MiniJSON.Json/Parser/TOKEN MiniJSON.Json/Parser::get_NextToken()
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
extern TypeInfo* Parser_t66_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t68_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1438_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral24;
extern Il2CppCodeGenString* _stringLiteral25;
extern "C" int32_t Parser_get_NextToken_m239 (Parser_t66 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Parser_t66_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(56);
		Dictionary_2_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(64);
		Dictionary_2__ctor_m1438_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483672);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral24 = il2cpp_codegen_string_literal_from_index(24);
		_stringLiteral25 = il2cpp_codegen_string_literal_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0x0;
	String_t* V_1 = {0};
	Dictionary_2_t68 * V_2 = {0};
	int32_t V_3 = 0;
	{
		Parser_EatWhitespace_m235(__this, /*hidden argument*/NULL);
		StringReader_t67 * L_0 = (__this->___json_1);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_0);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0019;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0019:
	{
		uint16_t L_2 = Parser_get_PeekChar_m236(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		uint16_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 0)
		{
			goto IL_00ea;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 1)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 2)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 3)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 5)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 6)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 7)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 8)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 9)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 10)
		{
			goto IL_00dc;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 11)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 12)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 13)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 14)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 15)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 16)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 17)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 18)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 19)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 20)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 21)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 22)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 23)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 24)
		{
			goto IL_00ec;
		}
	}

IL_008d:
	{
		uint16_t L_4 = V_0;
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_00cc;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_00a2;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_00ce;
		}
	}

IL_00a2:
	{
		uint16_t L_5 = V_0;
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_00bc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_00be;
		}
	}
	{
		goto IL_00f0;
	}

IL_00bc:
	{
		return (int32_t)(1);
	}

IL_00be:
	{
		StringReader_t67 * L_6 = (__this->___json_1);
		NullCheck(L_6);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_6);
		return (int32_t)(2);
	}

IL_00cc:
	{
		return (int32_t)(3);
	}

IL_00ce:
	{
		StringReader_t67 * L_7 = (__this->___json_1);
		NullCheck(L_7);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_7);
		return (int32_t)(4);
	}

IL_00dc:
	{
		StringReader_t67 * L_8 = (__this->___json_1);
		NullCheck(L_8);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_8);
		return (int32_t)(6);
	}

IL_00ea:
	{
		return (int32_t)(7);
	}

IL_00ec:
	{
		return (int32_t)(5);
	}

IL_00ee:
	{
		return (int32_t)(8);
	}

IL_00f0:
	{
		String_t* L_9 = Parser_get_NextWord_m238(__this, /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = V_1;
		if (!L_10)
		{
			goto IL_016a;
		}
	}
	{
		Dictionary_2_t68 * L_11 = ((Parser_t66_StaticFields*)Parser_t66_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_2;
		if (L_11)
		{
			goto IL_0138;
		}
	}
	{
		Dictionary_2_t68 * L_12 = (Dictionary_2_t68 *)il2cpp_codegen_object_new (Dictionary_2_t68_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1438(L_12, 3, /*hidden argument*/Dictionary_2__ctor_m1438_MethodInfo_var);
		V_2 = L_12;
		Dictionary_2_t68 * L_13 = V_2;
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_13, _stringLiteral23, 0);
		Dictionary_2_t68 * L_14 = V_2;
		NullCheck(L_14);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_14, _stringLiteral24, 1);
		Dictionary_2_t68 * L_15 = V_2;
		NullCheck(L_15);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_15, _stringLiteral25, 2);
		Dictionary_2_t68 * L_16 = V_2;
		((Parser_t66_StaticFields*)Parser_t66_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_2 = L_16;
	}

IL_0138:
	{
		Dictionary_2_t68 * L_17 = ((Parser_t66_StaticFields*)Parser_t66_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_2;
		String_t* L_18 = V_1;
		NullCheck(L_17);
		bool L_19 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(33 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_17, L_18, (&V_3));
		if (!L_19)
		{
			goto IL_016a;
		}
	}
	{
		int32_t L_20 = V_3;
		if (L_20 == 0)
		{
			goto IL_0161;
		}
		if (L_20 == 1)
		{
			goto IL_0164;
		}
		if (L_20 == 2)
		{
			goto IL_0167;
		}
	}
	{
		goto IL_016a;
	}

IL_0161:
	{
		return (int32_t)(((int32_t)10));
	}

IL_0164:
	{
		return (int32_t)(((int32_t)9));
	}

IL_0167:
	{
		return (int32_t)(((int32_t)11));
	}

IL_016a:
	{
		return (int32_t)(0);
	}
}
// MiniJSON.Json/Serializer
#include "AssemblyU2DCSharp_MiniJSON_Json_Serializer.h"
// MiniJSON.Json/Serializer
#include "AssemblyU2DCSharp_MiniJSON_Json_SerializerMethodDeclarations.h"
// System.SByte
#include "mscorlib_System_SByte.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// System.Int16
#include "mscorlib_System_Int16.h"
// System.UInt16
#include "mscorlib_System_UInt16.h"
// System.Decimal
#include "mscorlib_System_Decimal.h"
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
// System.Void MiniJSON.Json/Serializer::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
extern TypeInfo* StringBuilder_t70_il2cpp_TypeInfo_var;
extern "C" void Serializer__ctor_m240 (Serializer_t69 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		StringBuilder_t70 * L_0 = (StringBuilder_t70 *)il2cpp_codegen_object_new (StringBuilder_t70_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1430(L_0, /*hidden argument*/NULL);
		__this->___builder_0 = L_0;
		return;
	}
}
// System.String MiniJSON.Json/Serializer::Serialize(System.Object)
// System.Object
#include "mscorlib_System_Object.h"
// MiniJSON.Json/Serializer
#include "AssemblyU2DCSharp_MiniJSON_Json_SerializerMethodDeclarations.h"
extern TypeInfo* Serializer_t69_il2cpp_TypeInfo_var;
extern "C" String_t* Serializer_Serialize_m241 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Serializer_t69_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(65);
		s_Il2CppMethodIntialized = true;
	}
	Serializer_t69 * V_0 = {0};
	{
		Serializer_t69 * L_0 = (Serializer_t69 *)il2cpp_codegen_object_new (Serializer_t69_il2cpp_TypeInfo_var);
		Serializer__ctor_m240(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Serializer_t69 * L_1 = V_0;
		Object_t * L_2 = ___obj;
		NullCheck(L_1);
		Serializer_SerializeValue_m242(L_1, L_2, /*hidden argument*/NULL);
		Serializer_t69 * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_t70 * L_4 = (L_3->___builder_0);
		NullCheck(L_4);
		String_t* L_5 = StringBuilder_ToString_m1434(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void MiniJSON.Json/Serializer::SerializeValue(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t384_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t346_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t345_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t383_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral25;
extern Il2CppCodeGenString* _stringLiteral24;
extern Il2CppCodeGenString* _stringLiteral23;
extern "C" void Serializer_SerializeValue_m242 (Serializer_t69 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Boolean_t384_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		IList_t346_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(66);
		IDictionary_t345_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Char_t383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		_stringLiteral25 = il2cpp_codegen_string_literal_from_index(25);
		_stringLiteral24 = il2cpp_codegen_string_literal_from_index(24);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	String_t* V_2 = {0};
	StringBuilder_t70 * G_B7_0 = {0};
	StringBuilder_t70 * G_B6_0 = {0};
	String_t* G_B8_0 = {0};
	StringBuilder_t70 * G_B8_1 = {0};
	{
		Object_t * L_0 = ___value;
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		StringBuilder_t70 * L_1 = (__this->___builder_0);
		NullCheck(L_1);
		StringBuilder_Append_m1439(L_1, _stringLiteral25, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_001c:
	{
		Object_t * L_2 = ___value;
		String_t* L_3 = ((String_t*)IsInstSealed(L_2, String_t_il2cpp_TypeInfo_var));
		V_2 = L_3;
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_4 = V_2;
		Serializer_SerializeString_m245(__this, L_4, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_0035:
	{
		Object_t * L_5 = ___value;
		if (!((Object_t *)IsInstSealed(L_5, Boolean_t384_il2cpp_TypeInfo_var)))
		{
			goto IL_006b;
		}
	}
	{
		StringBuilder_t70 * L_6 = (__this->___builder_0);
		Object_t * L_7 = ___value;
		G_B6_0 = L_6;
		if (!((*(bool*)((bool*)UnBox (L_7, Boolean_t384_il2cpp_TypeInfo_var)))))
		{
			G_B7_0 = L_6;
			goto IL_005b;
		}
	}
	{
		G_B8_0 = _stringLiteral24;
		G_B8_1 = G_B6_0;
		goto IL_0060;
	}

IL_005b:
	{
		G_B8_0 = _stringLiteral23;
		G_B8_1 = G_B7_0;
	}

IL_0060:
	{
		NullCheck(G_B8_1);
		StringBuilder_Append_m1439(G_B8_1, G_B8_0, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_006b:
	{
		Object_t * L_8 = ___value;
		Object_t * L_9 = ((Object_t *)IsInst(L_8, IList_t346_il2cpp_TypeInfo_var));
		V_0 = L_9;
		if (!L_9)
		{
			goto IL_0084;
		}
	}
	{
		Object_t * L_10 = V_0;
		Serializer_SerializeArray_m244(__this, L_10, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_0084:
	{
		Object_t * L_11 = ___value;
		Object_t * L_12 = ((Object_t *)IsInst(L_11, IDictionary_t345_il2cpp_TypeInfo_var));
		V_1 = L_12;
		if (!L_12)
		{
			goto IL_009d;
		}
	}
	{
		Object_t * L_13 = V_1;
		Serializer_SerializeObject_m243(__this, L_13, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_009d:
	{
		Object_t * L_14 = ___value;
		if (!((Object_t *)IsInstSealed(L_14, Char_t383_il2cpp_TypeInfo_var)))
		{
			goto IL_00bf;
		}
	}
	{
		Object_t * L_15 = ___value;
		String_t* L_16 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_16 = String_CreateString_m1440(L_16, ((*(uint16_t*)((uint16_t*)UnBox (L_15, Char_t383_il2cpp_TypeInfo_var)))), 1, /*hidden argument*/NULL);
		Serializer_SerializeString_m245(__this, L_16, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		Object_t * L_17 = ___value;
		Serializer_SerializeOther_m246(__this, L_17, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void MiniJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern TypeInfo* IDictionary_t345_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t375_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void Serializer_SerializeObject_m243 (Serializer_t69 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t345_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		IEnumerable_t375_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(68);
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 1;
		StringBuilder_t70 * L_0 = (__this->___builder_0);
		NullCheck(L_0);
		StringBuilder_Append_m1431(L_0, ((int32_t)123), /*hidden argument*/NULL);
		Object_t * L_1 = ___obj;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t345_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t375_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0065;
		}

IL_0021:
		{
			Object_t * L_4 = V_2;
			NullCheck(L_4);
			Object_t * L_5 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			bool L_6 = V_0;
			if (L_6)
			{
				goto IL_003c;
			}
		}

IL_002e:
		{
			StringBuilder_t70 * L_7 = (__this->___builder_0);
			NullCheck(L_7);
			StringBuilder_Append_m1431(L_7, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_003c:
		{
			Object_t * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
			Serializer_SerializeString_m245(__this, L_9, /*hidden argument*/NULL);
			StringBuilder_t70 * L_10 = (__this->___builder_0);
			NullCheck(L_10);
			StringBuilder_Append_m1431(L_10, ((int32_t)58), /*hidden argument*/NULL);
			Object_t * L_11 = ___obj;
			Object_t * L_12 = V_1;
			NullCheck(L_11);
			Object_t * L_13 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t345_il2cpp_TypeInfo_var, L_11, L_12);
			Serializer_SerializeValue_m242(__this, L_13, /*hidden argument*/NULL);
			V_0 = 0;
		}

IL_0065:
		{
			Object_t * L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0021;
			}
		}

IL_0070:
		{
			IL2CPP_LEAVE(0x87, FINALLY_0075);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0075;
	}

FINALLY_0075:
	{ // begin finally (depth: 1)
		{
			Object_t * L_16 = V_2;
			V_3 = ((Object_t *)IsInst(L_16, IDisposable_t364_il2cpp_TypeInfo_var));
			Object_t * L_17 = V_3;
			if (L_17)
			{
				goto IL_0080;
			}
		}

IL_007f:
		{
			IL2CPP_END_FINALLY(117)
		}

IL_0080:
		{
			Object_t * L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_18);
			IL2CPP_END_FINALLY(117)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(117)
	{
		IL2CPP_JUMP_TBL(0x87, IL_0087)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0087:
	{
		StringBuilder_t70 * L_19 = (__this->___builder_0);
		NullCheck(L_19);
		StringBuilder_Append_m1431(L_19, ((int32_t)125), /*hidden argument*/NULL);
		return;
	}
}
// System.Void MiniJSON.Json/Serializer::SerializeArray(System.Collections.IList)
extern TypeInfo* IEnumerable_t375_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void Serializer_SerializeArray_m244 (Serializer_t69 * __this, Object_t * ___anArray, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_t375_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(68);
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t70 * L_0 = (__this->___builder_0);
		NullCheck(L_0);
		StringBuilder_Append_m1431(L_0, ((int32_t)91), /*hidden argument*/NULL);
		V_0 = 1;
		Object_t * L_1 = ___anArray;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t375_il2cpp_TypeInfo_var, L_1);
		V_2 = L_2;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0040;
		}

IL_001c:
		{
			Object_t * L_3 = V_2;
			NullCheck(L_3);
			Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			bool L_5 = V_0;
			if (L_5)
			{
				goto IL_0037;
			}
		}

IL_0029:
		{
			StringBuilder_t70 * L_6 = (__this->___builder_0);
			NullCheck(L_6);
			StringBuilder_Append_m1431(L_6, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_0037:
		{
			Object_t * L_7 = V_1;
			Serializer_SerializeValue_m242(__this, L_7, /*hidden argument*/NULL);
			V_0 = 0;
		}

IL_0040:
		{
			Object_t * L_8 = V_2;
			NullCheck(L_8);
			bool L_9 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_001c;
			}
		}

IL_004b:
		{
			IL2CPP_LEAVE(0x62, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		{
			Object_t * L_10 = V_2;
			V_3 = ((Object_t *)IsInst(L_10, IDisposable_t364_il2cpp_TypeInfo_var));
			Object_t * L_11 = V_3;
			if (L_11)
			{
				goto IL_005b;
			}
		}

IL_005a:
		{
			IL2CPP_END_FINALLY(80)
		}

IL_005b:
		{
			Object_t * L_12 = V_3;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_12);
			IL2CPP_END_FINALLY(80)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0062:
	{
		StringBuilder_t70 * L_13 = (__this->___builder_0);
		NullCheck(L_13);
		StringBuilder_Append_m1431(L_13, ((int32_t)93), /*hidden argument*/NULL);
		return;
	}
}
// System.Void MiniJSON.Json/Serializer::SerializeString(System.String)
// System.String
#include "mscorlib_System_String.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
extern TypeInfo* Convert_t373_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral26;
extern Il2CppCodeGenString* _stringLiteral27;
extern Il2CppCodeGenString* _stringLiteral28;
extern Il2CppCodeGenString* _stringLiteral29;
extern Il2CppCodeGenString* _stringLiteral30;
extern Il2CppCodeGenString* _stringLiteral31;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral33;
extern Il2CppCodeGenString* _stringLiteral34;
extern "C" void Serializer_SerializeString_m245 (Serializer_t69 * __this, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		_stringLiteral26 = il2cpp_codegen_string_literal_from_index(26);
		_stringLiteral27 = il2cpp_codegen_string_literal_from_index(27);
		_stringLiteral28 = il2cpp_codegen_string_literal_from_index(28);
		_stringLiteral29 = il2cpp_codegen_string_literal_from_index(29);
		_stringLiteral30 = il2cpp_codegen_string_literal_from_index(30);
		_stringLiteral31 = il2cpp_codegen_string_literal_from_index(31);
		_stringLiteral32 = il2cpp_codegen_string_literal_from_index(32);
		_stringLiteral33 = il2cpp_codegen_string_literal_from_index(33);
		_stringLiteral34 = il2cpp_codegen_string_literal_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t385* V_0 = {0};
	uint16_t V_1 = 0x0;
	CharU5BU5D_t385* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	uint16_t V_5 = 0x0;
	{
		StringBuilder_t70 * L_0 = (__this->___builder_0);
		NullCheck(L_0);
		StringBuilder_Append_m1431(L_0, ((int32_t)34), /*hidden argument*/NULL);
		String_t* L_1 = ___str;
		NullCheck(L_1);
		CharU5BU5D_t385* L_2 = String_ToCharArray_m1441(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		CharU5BU5D_t385* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_0155;
	}

IL_001e:
	{
		CharU5BU5D_t385* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_1 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_4, L_6, sizeof(uint16_t)));
		uint16_t L_7 = V_1;
		V_5 = L_7;
		uint16_t L_8 = V_5;
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 0)
		{
			goto IL_0089;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 1)
		{
			goto IL_00e1;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 2)
		{
			goto IL_00b5;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 3)
		{
			goto IL_0046;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 4)
		{
			goto IL_009f;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 5)
		{
			goto IL_00cb;
		}
	}

IL_0046:
	{
		uint16_t L_9 = V_5;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)34))))
		{
			goto IL_005d;
		}
	}
	{
		uint16_t L_10 = V_5;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)92))))
		{
			goto IL_0073;
		}
	}
	{
		goto IL_00f7;
	}

IL_005d:
	{
		StringBuilder_t70 * L_11 = (__this->___builder_0);
		NullCheck(L_11);
		StringBuilder_Append_m1439(L_11, _stringLiteral26, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_0073:
	{
		StringBuilder_t70 * L_12 = (__this->___builder_0);
		NullCheck(L_12);
		StringBuilder_Append_m1439(L_12, _stringLiteral27, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_0089:
	{
		StringBuilder_t70 * L_13 = (__this->___builder_0);
		NullCheck(L_13);
		StringBuilder_Append_m1439(L_13, _stringLiteral28, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_009f:
	{
		StringBuilder_t70 * L_14 = (__this->___builder_0);
		NullCheck(L_14);
		StringBuilder_Append_m1439(L_14, _stringLiteral29, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_00b5:
	{
		StringBuilder_t70 * L_15 = (__this->___builder_0);
		NullCheck(L_15);
		StringBuilder_Append_m1439(L_15, _stringLiteral30, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_00cb:
	{
		StringBuilder_t70 * L_16 = (__this->___builder_0);
		NullCheck(L_16);
		StringBuilder_Append_m1439(L_16, _stringLiteral31, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_00e1:
	{
		StringBuilder_t70 * L_17 = (__this->___builder_0);
		NullCheck(L_17);
		StringBuilder_Append_m1439(L_17, _stringLiteral32, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_00f7:
	{
		uint16_t L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		int32_t L_19 = Convert_ToInt32_m1442(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		int32_t L_20 = V_4;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)32))))
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_21 = V_4;
		if ((((int32_t)L_21) > ((int32_t)((int32_t)126))))
		{
			goto IL_0123;
		}
	}
	{
		StringBuilder_t70 * L_22 = (__this->___builder_0);
		uint16_t L_23 = V_1;
		NullCheck(L_22);
		StringBuilder_Append_m1431(L_22, L_23, /*hidden argument*/NULL);
		goto IL_014c;
	}

IL_0123:
	{
		StringBuilder_t70 * L_24 = (__this->___builder_0);
		NullCheck(L_24);
		StringBuilder_Append_m1439(L_24, _stringLiteral33, /*hidden argument*/NULL);
		StringBuilder_t70 * L_25 = (__this->___builder_0);
		String_t* L_26 = Int32_ToString_m1443((&V_4), _stringLiteral34, /*hidden argument*/NULL);
		NullCheck(L_25);
		StringBuilder_Append_m1439(L_25, L_26, /*hidden argument*/NULL);
	}

IL_014c:
	{
		goto IL_0151;
	}

IL_0151:
	{
		int32_t L_27 = V_3;
		V_3 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0155:
	{
		int32_t L_28 = V_3;
		CharU5BU5D_t385* L_29 = V_2;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)(((Array_t *)L_29)->max_length))))))
		{
			goto IL_001e;
		}
	}
	{
		StringBuilder_t70 * L_30 = (__this->___builder_0);
		NullCheck(L_30);
		StringBuilder_Append_m1431(L_30, ((int32_t)34), /*hidden argument*/NULL);
		return;
	}
}
// System.Void MiniJSON.Json/Serializer::SerializeOther(System.Object)
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
// System.Double
#include "mscorlib_System_DoubleMethodDeclarations.h"
extern TypeInfo* Single_t388_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t389_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t386_il2cpp_TypeInfo_var;
extern TypeInfo* SByte_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t391_il2cpp_TypeInfo_var;
extern TypeInfo* Int16_t392_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t393_il2cpp_TypeInfo_var;
extern TypeInfo* UInt64_t394_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t387_il2cpp_TypeInfo_var;
extern TypeInfo* Decimal_t395_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t373_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral35;
extern "C" void Serializer_SerializeOther_m246 (Serializer_t69 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		UInt32_t389_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(70);
		Int64_t386_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(62);
		SByte_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		Byte_t391_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(72);
		Int16_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(73);
		UInt16_t393_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(74);
		UInt64_t394_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(75);
		Double_t387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Decimal_t395_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		Convert_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		_stringLiteral35 = il2cpp_codegen_string_literal_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	double V_1 = 0.0;
	{
		Object_t * L_0 = ___value;
		if (!((Object_t *)IsInstSealed(L_0, Single_t388_il2cpp_TypeInfo_var)))
		{
			goto IL_002f;
		}
	}
	{
		StringBuilder_t70 * L_1 = (__this->___builder_0);
		Object_t * L_2 = ___value;
		V_0 = ((*(float*)((float*)UnBox (L_2, Single_t388_il2cpp_TypeInfo_var))));
		String_t* L_3 = Single_ToString_m1444((&V_0), _stringLiteral35, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m1439(L_1, L_3, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_002f:
	{
		Object_t * L_4 = ___value;
		if (((Object_t *)IsInstSealed(L_4, Int32_t372_il2cpp_TypeInfo_var)))
		{
			goto IL_0087;
		}
	}
	{
		Object_t * L_5 = ___value;
		if (((Object_t *)IsInstSealed(L_5, UInt32_t389_il2cpp_TypeInfo_var)))
		{
			goto IL_0087;
		}
	}
	{
		Object_t * L_6 = ___value;
		if (((Object_t *)IsInstSealed(L_6, Int64_t386_il2cpp_TypeInfo_var)))
		{
			goto IL_0087;
		}
	}
	{
		Object_t * L_7 = ___value;
		if (((Object_t *)IsInstSealed(L_7, SByte_t390_il2cpp_TypeInfo_var)))
		{
			goto IL_0087;
		}
	}
	{
		Object_t * L_8 = ___value;
		if (((Object_t *)IsInstSealed(L_8, Byte_t391_il2cpp_TypeInfo_var)))
		{
			goto IL_0087;
		}
	}
	{
		Object_t * L_9 = ___value;
		if (((Object_t *)IsInstSealed(L_9, Int16_t392_il2cpp_TypeInfo_var)))
		{
			goto IL_0087;
		}
	}
	{
		Object_t * L_10 = ___value;
		if (((Object_t *)IsInstSealed(L_10, UInt16_t393_il2cpp_TypeInfo_var)))
		{
			goto IL_0087;
		}
	}
	{
		Object_t * L_11 = ___value;
		if (!((Object_t *)IsInstSealed(L_11, UInt64_t394_il2cpp_TypeInfo_var)))
		{
			goto IL_0099;
		}
	}

IL_0087:
	{
		StringBuilder_t70 * L_12 = (__this->___builder_0);
		Object_t * L_13 = ___value;
		NullCheck(L_12);
		StringBuilder_Append_m1445(L_12, L_13, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_0099:
	{
		Object_t * L_14 = ___value;
		if (((Object_t *)IsInstSealed(L_14, Double_t387_il2cpp_TypeInfo_var)))
		{
			goto IL_00af;
		}
	}
	{
		Object_t * L_15 = ___value;
		if (!((Object_t *)IsInstSealed(L_15, Decimal_t395_il2cpp_TypeInfo_var)))
		{
			goto IL_00d3;
		}
	}

IL_00af:
	{
		StringBuilder_t70 * L_16 = (__this->___builder_0);
		Object_t * L_17 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		double L_18 = Convert_ToDouble_m1446(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		String_t* L_19 = Double_ToString_m1447((&V_1), _stringLiteral35, /*hidden argument*/NULL);
		NullCheck(L_16);
		StringBuilder_Append_m1439(L_16, L_19, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00d3:
	{
		Object_t * L_20 = ___value;
		NullCheck(L_20);
		String_t* L_21 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		Serializer_SerializeString_m245(__this, L_21, /*hidden argument*/NULL);
	}

IL_00df:
	{
		return;
	}
}
// MiniJSON.Json
#include "AssemblyU2DCSharp_MiniJSON_Json.h"
// MiniJSON.Json
#include "AssemblyU2DCSharp_MiniJSON_JsonMethodDeclarations.h"
// System.Object MiniJSON.Json::Deserialize(System.String)
// System.String
#include "mscorlib_System_String.h"
// MiniJSON.Json/Parser
#include "AssemblyU2DCSharp_MiniJSON_Json_ParserMethodDeclarations.h"
extern "C" Object_t * Json_Deserialize_m247 (Object_t * __this /* static, unused */, String_t* ___json, const MethodInfo* method)
{
	{
		String_t* L_0 = ___json;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return NULL;
	}

IL_0008:
	{
		String_t* L_1 = ___json;
		Object_t * L_2 = Parser_Parse_m227(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String MiniJSON.Json::Serialize(System.Object)
// System.Object
#include "mscorlib_System_Object.h"
// MiniJSON.Json/Serializer
#include "AssemblyU2DCSharp_MiniJSON_Json_SerializerMethodDeclarations.h"
extern "C" String_t* Json_Serialize_m248 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___obj;
		String_t* L_1 = Serializer_Serialize_m241(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Common.Logger.Log
#include "AssemblyU2DCSharp_Common_Logger_Log.h"
// Common.Logger.Log
#include "AssemblyU2DCSharp_Common_Logger_LogMethodDeclarations.h"
// Common.Logger.LogLevel
#include "AssemblyU2DCSharp_Common_Logger_LogLevel.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// System.Void Common.Logger.Log::.ctor(Common.Logger.LogLevel,System.String)
// Common.Logger.LogLevel
#include "AssemblyU2DCSharp_Common_Logger_LogLevel.h"
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
extern TypeInfo* DateTime_t74_il2cpp_TypeInfo_var;
extern "C" void Log__ctor_m249 (Log_t72 * __this, LogLevel_t73 * ___level, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t74_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(77);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		LogLevel_t73 * L_0 = ___level;
		__this->___level_0 = L_0;
		String_t* L_1 = ___message;
		__this->___message_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t74_il2cpp_TypeInfo_var);
		DateTime_t74  L_2 = DateTime_get_Now_m1448(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___timestamp_2 = L_2;
		return;
	}
}
// Common.Logger.LogLevel Common.Logger.Log::get_Level()
extern "C" LogLevel_t73 * Log_get_Level_m250 (Log_t72 * __this, const MethodInfo* method)
{
	{
		LogLevel_t73 * L_0 = (__this->___level_0);
		return L_0;
	}
}
// System.String Common.Logger.Log::get_Message()
extern "C" String_t* Log_get_Message_m251 (Log_t72 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___message_1);
		return L_0;
	}
}
// System.DateTime Common.Logger.Log::get_Timestamp()
extern "C" DateTime_t74  Log_get_Timestamp_m252 (Log_t72 * __this, const MethodInfo* method)
{
	{
		DateTime_t74  L_0 = (__this->___timestamp_2);
		return L_0;
	}
}
// Common.Logger.LogLevel
#include "AssemblyU2DCSharp_Common_Logger_LogLevelMethodDeclarations.h"
// System.Void Common.Logger.LogLevel::.ctor(System.String)
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void LogLevel__ctor_m253 (LogLevel_t73 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___name_3 = L_0;
		return;
	}
}
// System.Void Common.Logger.LogLevel::.cctor()
// Common.Logger.LogLevel
#include "AssemblyU2DCSharp_Common_Logger_LogLevelMethodDeclarations.h"
extern TypeInfo* LogLevel_t73_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral36;
extern Il2CppCodeGenString* _stringLiteral37;
extern Il2CppCodeGenString* _stringLiteral38;
extern "C" void LogLevel__cctor_m254 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogLevel_t73_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral36 = il2cpp_codegen_string_literal_from_index(36);
		_stringLiteral37 = il2cpp_codegen_string_literal_from_index(37);
		_stringLiteral38 = il2cpp_codegen_string_literal_from_index(38);
		s_Il2CppMethodIntialized = true;
	}
	{
		LogLevel_t73 * L_0 = (LogLevel_t73 *)il2cpp_codegen_object_new (LogLevel_t73_il2cpp_TypeInfo_var);
		LogLevel__ctor_m253(L_0, _stringLiteral36, /*hidden argument*/NULL);
		((LogLevel_t73_StaticFields*)LogLevel_t73_il2cpp_TypeInfo_var->static_fields)->___NORMAL_0 = L_0;
		LogLevel_t73 * L_1 = (LogLevel_t73 *)il2cpp_codegen_object_new (LogLevel_t73_il2cpp_TypeInfo_var);
		LogLevel__ctor_m253(L_1, _stringLiteral37, /*hidden argument*/NULL);
		((LogLevel_t73_StaticFields*)LogLevel_t73_il2cpp_TypeInfo_var->static_fields)->___WARNING_1 = L_1;
		LogLevel_t73 * L_2 = (LogLevel_t73 *)il2cpp_codegen_object_new (LogLevel_t73_il2cpp_TypeInfo_var);
		LogLevel__ctor_m253(L_2, _stringLiteral38, /*hidden argument*/NULL);
		((LogLevel_t73_StaticFields*)LogLevel_t73_il2cpp_TypeInfo_var->static_fields)->___ERROR_2 = L_2;
		return;
	}
}
// System.String Common.Logger.LogLevel::get_Name()
extern "C" String_t* LogLevel_get_Name_m255 (LogLevel_t73 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_3);
		return L_0;
	}
}
// System.Collections.Generic.Queue`1<Common.Logger.Log>
#include "System_System_Collections_Generic_Queue_1_gen.h"
// System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>
#include "System_System_Collections_Generic_Queue_1_gen_0.h"
// UnityThreading.ActionThread
#include "AssemblyU2DCSharp_UnityThreading_ActionThread.h"
// System.IO.StreamWriter
#include "mscorlib_System_IO_StreamWriter.h"
// System.IO.TextWriter
#include "mscorlib_System_IO_TextWriter.h"
// System.Collections.Generic.Queue`1<Common.Logger.Log>
#include "System_System_Collections_Generic_Queue_1_genMethodDeclarations.h"
// System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>
#include "System_System_Collections_Generic_Queue_1_gen_0MethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// UnityThreadHelper
#include "AssemblyU2DCSharp_UnityThreadHelperMethodDeclarations.h"
// System.IO.TextWriter
#include "mscorlib_System_IO_TextWriterMethodDeclarations.h"
// System.IO.StreamWriter
#include "mscorlib_System_IO_StreamWriterMethodDeclarations.h"
// System.Void Common.Logger.Logger::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.Queue`1<Common.Logger.Log>
#include "System_System_Collections_Generic_Queue_1_genMethodDeclarations.h"
// System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>
#include "System_System_Collections_Generic_Queue_1_gen_0MethodDeclarations.h"
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimerMethodDeclarations.h"
extern TypeInfo* Queue_1_t76_il2cpp_TypeInfo_var;
extern TypeInfo* Queue_1_t77_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t75_il2cpp_TypeInfo_var;
extern TypeInfo* CountdownTimer_t13_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m1449_MethodInfo_var;
extern const MethodInfo* Queue_1__ctor_m1450_MethodInfo_var;
extern "C" void Logger__ctor_m256 (Logger_t75 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Queue_1_t76_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(80);
		Queue_1_t77_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		Logger_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		CountdownTimer_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		Queue_1__ctor_m1449_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483673);
		Queue_1__ctor_m1450_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483674);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Queue_1_t76 * L_0 = (Queue_1_t76 *)il2cpp_codegen_object_new (Queue_1_t76_il2cpp_TypeInfo_var);
		Queue_1__ctor_m1449(L_0, /*hidden argument*/Queue_1__ctor_m1449_MethodInfo_var);
		__this->___bufferQueue_2 = L_0;
		Queue_1_t77 * L_1 = (Queue_1_t77 *)il2cpp_codegen_object_new (Queue_1_t77_il2cpp_TypeInfo_var);
		Queue_1__ctor_m1450(L_1, /*hidden argument*/Queue_1__ctor_m1450_MethodInfo_var);
		__this->___writeQueue_3 = L_1;
		__this->___currentlyWriting_4 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t75_il2cpp_TypeInfo_var);
		int32_t L_2 = ((Logger_t75_StaticFields*)Logger_t75_il2cpp_TypeInfo_var->static_fields)->___WRITE_TIME_INTERVAL_5;
		CountdownTimer_t13 * L_3 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m63(L_3, (((float)L_2)), /*hidden argument*/NULL);
		__this->___writeTimer_6 = L_3;
		return;
	}
}
// System.Void Common.Logger.Logger::.cctor()
extern TypeInfo* Logger_t75_il2cpp_TypeInfo_var;
extern "C" void Logger__cctor_m257 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Logger_t75_StaticFields*)Logger_t75_il2cpp_TypeInfo_var->static_fields)->___WRITE_TIME_INTERVAL_5 = 5;
		return;
	}
}
// Common.Logger.Logger Common.Logger.Logger::GetInstance()
// Common.Logger.Logger
#include "AssemblyU2DCSharp_Common_Logger_LoggerMethodDeclarations.h"
extern TypeInfo* Logger_t75_il2cpp_TypeInfo_var;
extern "C" Logger_t75 * Logger_GetInstance_m258 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t75_il2cpp_TypeInfo_var);
		Logger_t75 * L_0 = ((Logger_t75_StaticFields*)Logger_t75_il2cpp_TypeInfo_var->static_fields)->___ONLY_INSTANCE_7;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Logger_t75 * L_1 = (Logger_t75 *)il2cpp_codegen_object_new (Logger_t75_il2cpp_TypeInfo_var);
		Logger__ctor_m256(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t75_il2cpp_TypeInfo_var);
		((Logger_t75_StaticFields*)Logger_t75_il2cpp_TypeInfo_var->static_fields)->___ONLY_INSTANCE_7 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t75_il2cpp_TypeInfo_var);
		Logger_t75 * L_2 = ((Logger_t75_StaticFields*)Logger_t75_il2cpp_TypeInfo_var->static_fields)->___ONLY_INSTANCE_7;
		return L_2;
	}
}
// System.Void Common.Logger.Logger::SetName(System.String)
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral39;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral41;
extern "C" void Logger_SetName_m259 (Logger_t75 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral39 = il2cpp_codegen_string_literal_from_index(39);
		_stringLiteral40 = il2cpp_codegen_string_literal_from_index(40);
		_stringLiteral41 = il2cpp_codegen_string_literal_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		__this->___name_0 = L_0;
		String_t* L_1 = Application_get_persistentDataPath_m1451(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1452(NULL /*static, unused*/, L_1, _stringLiteral39, L_2, _stringLiteral40, /*hidden argument*/NULL);
		__this->___logFilePath_1 = L_3;
		String_t* L_4 = (__this->___logFilePath_1);
		String_t* L_5 = String_Concat_m1319(NULL /*static, unused*/, _stringLiteral41, L_4, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Logger.Logger::Log(System.String)
extern TypeInfo* LogLevel_t73_il2cpp_TypeInfo_var;
extern "C" void Logger_Log_m260 (Logger_t75 * __this, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogLevel_t73_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogLevel_t73_il2cpp_TypeInfo_var);
		LogLevel_t73 * L_0 = ((LogLevel_t73_StaticFields*)LogLevel_t73_il2cpp_TypeInfo_var->static_fields)->___NORMAL_0;
		String_t* L_1 = ___message;
		Logger_Log_m263(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Logger.Logger::LogWarning(System.String)
extern TypeInfo* LogLevel_t73_il2cpp_TypeInfo_var;
extern "C" void Logger_LogWarning_m261 (Logger_t75 * __this, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogLevel_t73_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogLevel_t73_il2cpp_TypeInfo_var);
		LogLevel_t73 * L_0 = ((LogLevel_t73_StaticFields*)LogLevel_t73_il2cpp_TypeInfo_var->static_fields)->___WARNING_1;
		String_t* L_1 = ___message;
		Logger_Log_m263(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Logger.Logger::LogError(System.String)
extern TypeInfo* LogLevel_t73_il2cpp_TypeInfo_var;
extern "C" void Logger_LogError_m262 (Logger_t75 * __this, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogLevel_t73_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogLevel_t73_il2cpp_TypeInfo_var);
		LogLevel_t73 * L_0 = ((LogLevel_t73_StaticFields*)LogLevel_t73_il2cpp_TypeInfo_var->static_fields)->___ERROR_2;
		String_t* L_1 = ___message;
		Logger_Log_m263(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Logger.Logger::Log(Common.Logger.LogLevel,System.String)
// Common.Logger.LogLevel
#include "AssemblyU2DCSharp_Common_Logger_LogLevel.h"
// Common.Logger.Log
#include "AssemblyU2DCSharp_Common_Logger_LogMethodDeclarations.h"
extern TypeInfo* LogLevel_t73_il2cpp_TypeInfo_var;
extern TypeInfo* Log_t72_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m1453_MethodInfo_var;
extern "C" void Logger_Log_m263 (Logger_t75 * __this, LogLevel_t73 * ___level, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogLevel_t73_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		Log_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(79);
		Queue_1_Enqueue_m1453_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483675);
		s_Il2CppMethodIntialized = true;
	}
	{
		LogLevel_t73 * L_0 = ___level;
		IL2CPP_RUNTIME_CLASS_INIT(LogLevel_t73_il2cpp_TypeInfo_var);
		LogLevel_t73 * L_1 = ((LogLevel_t73_StaticFields*)LogLevel_t73_il2cpp_TypeInfo_var->static_fields)->___NORMAL_0;
		if ((!(((Object_t*)(LogLevel_t73 *)L_0) == ((Object_t*)(LogLevel_t73 *)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = ___message;
		Debug_Log_m1411(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0016:
	{
		LogLevel_t73 * L_3 = ___level;
		IL2CPP_RUNTIME_CLASS_INIT(LogLevel_t73_il2cpp_TypeInfo_var);
		LogLevel_t73 * L_4 = ((LogLevel_t73_StaticFields*)LogLevel_t73_il2cpp_TypeInfo_var->static_fields)->___WARNING_1;
		if ((!(((Object_t*)(LogLevel_t73 *)L_3) == ((Object_t*)(LogLevel_t73 *)L_4))))
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_5 = ___message;
		Debug_LogWarning_m1399(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_002c:
	{
		LogLevel_t73 * L_6 = ___level;
		IL2CPP_RUNTIME_CLASS_INIT(LogLevel_t73_il2cpp_TypeInfo_var);
		LogLevel_t73 * L_7 = ((LogLevel_t73_StaticFields*)LogLevel_t73_il2cpp_TypeInfo_var->static_fields)->___ERROR_2;
		if ((!(((Object_t*)(LogLevel_t73 *)L_6) == ((Object_t*)(LogLevel_t73 *)L_7))))
		{
			goto IL_003d;
		}
	}
	{
		String_t* L_8 = ___message;
		Debug_LogError_m1317(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003d:
	{
		Queue_1_t76 * L_9 = (__this->___bufferQueue_2);
		LogLevel_t73 * L_10 = ___level;
		String_t* L_11 = ___message;
		Log_t72 * L_12 = (Log_t72 *)il2cpp_codegen_object_new (Log_t72_il2cpp_TypeInfo_var);
		Log__ctor_m249(L_12, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		Queue_1_Enqueue_m1453(L_9, L_12, /*hidden argument*/Queue_1_Enqueue_m1453_MethodInfo_var);
		return;
	}
}
// System.Void Common.Logger.Logger::EnqueueWriteTask()
extern TypeInfo* Queue_1_t76_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m1449_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m1454_MethodInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m1453_MethodInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m1455_MethodInfo_var;
extern "C" void Logger_EnqueueWriteTask_m264 (Logger_t75 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Queue_1_t76_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(80);
		Queue_1__ctor_m1449_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483673);
		Queue_1_Dequeue_m1454_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483676);
		Queue_1_Enqueue_m1453_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483675);
		Queue_1_Enqueue_m1455_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483677);
		s_Il2CppMethodIntialized = true;
	}
	Queue_1_t76 * V_0 = {0};
	Log_t72 * V_1 = {0};
	{
		Queue_1_t76 * L_0 = (Queue_1_t76 *)il2cpp_codegen_object_new (Queue_1_t76_il2cpp_TypeInfo_var);
		Queue_1__ctor_m1449(L_0, /*hidden argument*/Queue_1__ctor_m1449_MethodInfo_var);
		V_0 = L_0;
		goto IL_001e;
	}

IL_000b:
	{
		Queue_1_t76 * L_1 = (__this->___bufferQueue_2);
		NullCheck(L_1);
		Log_t72 * L_2 = Queue_1_Dequeue_m1454(L_1, /*hidden argument*/Queue_1_Dequeue_m1454_MethodInfo_var);
		V_1 = L_2;
		Queue_1_t76 * L_3 = V_0;
		Log_t72 * L_4 = V_1;
		NullCheck(L_3);
		Queue_1_Enqueue_m1453(L_3, L_4, /*hidden argument*/Queue_1_Enqueue_m1453_MethodInfo_var);
	}

IL_001e:
	{
		Queue_1_t76 * L_5 = (__this->___bufferQueue_2);
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<Common.Logger.Log>::get_Count() */, L_5);
		if ((((int32_t)L_6) > ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		Queue_1_t77 * L_7 = (__this->___writeQueue_3);
		Queue_1_t76 * L_8 = V_0;
		NullCheck(L_7);
		Queue_1_Enqueue_m1455(L_7, L_8, /*hidden argument*/Queue_1_Enqueue_m1455_MethodInfo_var);
		return;
	}
}
// System.Void Common.Logger.Logger::WriteRemainingLogs()
extern "C" void Logger_WriteRemainingLogs_m265 (Logger_t75 * __this, const MethodInfo* method)
{
	{
		Logger_EnqueueWriteTask_m264(__this, /*hidden argument*/NULL);
		goto IL_0011;
	}

IL_000b:
	{
		Logger_ProcessTaskQueue_m267(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		Queue_1_t77 * L_0 = (__this->___writeQueue_3);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<System.Collections.Generic.Queue`1<Common.Logger.Log>>::get_Count() */, L_0);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		return;
	}
}
// System.Void Common.Logger.Logger::Update()
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
// UnityThreadHelper
#include "AssemblyU2DCSharp_UnityThreadHelperMethodDeclarations.h"
extern TypeInfo* Action_t16_il2cpp_TypeInfo_var;
extern TypeInfo* UnityThreadHelper_t181_il2cpp_TypeInfo_var;
extern const MethodInfo* Logger_ProcessTaskQueue_m267_MethodInfo_var;
extern "C" void Logger_Update_m266 (Logger_t75 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(82);
		UnityThreadHelper_t181_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		Logger_ProcessTaskQueue_m267_MethodInfo_var = il2cpp_codegen_method_info_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	Action_t16 * V_0 = {0};
	{
		bool L_0 = (__this->___currentlyWriting_4);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Queue_1_t76 * L_1 = (__this->___bufferQueue_2);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<Common.Logger.Log>::get_Count() */, L_1);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		return;
	}

IL_001d:
	{
		CountdownTimer_t13 * L_3 = (__this->___writeTimer_6);
		NullCheck(L_3);
		CountdownTimer_Update_m64(L_3, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_4 = (__this->___writeTimer_6);
		NullCheck(L_4);
		bool L_5 = CountdownTimer_HasElapsed_m67(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005d;
		}
	}
	{
		Logger_EnqueueWriteTask_m264(__this, /*hidden argument*/NULL);
		IntPtr_t L_6 = { (void*)Logger_ProcessTaskQueue_m267_MethodInfo_var };
		Action_t16 * L_7 = (Action_t16 *)il2cpp_codegen_object_new (Action_t16_il2cpp_TypeInfo_var);
		Action__ctor_m1456(L_7, __this, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Action_t16 * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityThreadHelper_t181_il2cpp_TypeInfo_var);
		UnityThreadHelper_CreateThread_m623(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		CountdownTimer_t13 * L_9 = (__this->___writeTimer_6);
		NullCheck(L_9);
		CountdownTimer_Reset_m65(L_9, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void Common.Logger.Logger::ProcessTaskQueue()
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
// System.IO.File
#include "mscorlib_System_IO_FileMethodDeclarations.h"
// Common.Logger.LogLevel
#include "AssemblyU2DCSharp_Common_Logger_LogLevelMethodDeclarations.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t137_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m1459_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m1454_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral42;
extern Il2CppCodeGenString* _stringLiteral43;
extern Il2CppCodeGenString* _stringLiteral44;
extern "C" void Logger_ProcessTaskQueue_m267 (Logger_t75 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		StringU5BU5D_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		Queue_1_Dequeue_m1459_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483679);
		Queue_1_Dequeue_m1454_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483676);
		_stringLiteral42 = il2cpp_codegen_string_literal_from_index(42);
		_stringLiteral43 = il2cpp_codegen_string_literal_from_index(43);
		_stringLiteral44 = il2cpp_codegen_string_literal_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	StreamWriter_t396 * V_0 = {0};
	Queue_1_t76 * V_1 = {0};
	Log_t72 * V_2 = {0};
	String_t* V_3 = {0};
	DateTime_t74  V_4 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		__this->___currentlyWriting_4 = 1;
		String_t* L_0 = (__this->___logFilePath_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)L_1) == ((int32_t)0))? 1 : 0), _stringLiteral42, /*hidden argument*/NULL);
		V_0 = (StreamWriter_t396 *)NULL;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_2 = (__this->___logFilePath_1);
			bool L_3 = File_Exists_m1352(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
			if (!L_3)
			{
				goto IL_0042;
			}
		}

IL_0031:
		{
			String_t* L_4 = (__this->___logFilePath_1);
			StreamWriter_t396 * L_5 = File_AppendText_m1457(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
			V_0 = L_5;
			goto IL_004e;
		}

IL_0042:
		{
			String_t* L_6 = (__this->___logFilePath_1);
			StreamWriter_t396 * L_7 = File_CreateText_m1458(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			V_0 = L_7;
		}

IL_004e:
		{
			Queue_1_t77 * L_8 = (__this->___writeQueue_3);
			NullCheck(L_8);
			Queue_1_t76 * L_9 = Queue_1_Dequeue_m1459(L_8, /*hidden argument*/Queue_1_Dequeue_m1459_MethodInfo_var);
			V_1 = L_9;
			goto IL_00b2;
		}

IL_005f:
		{
			Queue_1_t76 * L_10 = V_1;
			NullCheck(L_10);
			Log_t72 * L_11 = Queue_1_Dequeue_m1454(L_10, /*hidden argument*/Queue_1_Dequeue_m1454_MethodInfo_var);
			V_2 = L_11;
			StringU5BU5D_t137* L_12 = ((StringU5BU5D_t137*)SZArrayNew(StringU5BU5D_t137_il2cpp_TypeInfo_var, 5));
			Log_t72 * L_13 = V_2;
			NullCheck(L_13);
			LogLevel_t73 * L_14 = Log_get_Level_m250(L_13, /*hidden argument*/NULL);
			NullCheck(L_14);
			String_t* L_15 = LogLevel_get_Name_m255(L_14, /*hidden argument*/NULL);
			NullCheck(L_12);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
			ArrayElementTypeCheck (L_12, L_15);
			*((String_t**)(String_t**)SZArrayLdElema(L_12, 0, sizeof(String_t*))) = (String_t*)L_15;
			StringU5BU5D_t137* L_16 = L_12;
			NullCheck(L_16);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
			ArrayElementTypeCheck (L_16, _stringLiteral43);
			*((String_t**)(String_t**)SZArrayLdElema(L_16, 1, sizeof(String_t*))) = (String_t*)_stringLiteral43;
			StringU5BU5D_t137* L_17 = L_16;
			Log_t72 * L_18 = V_2;
			NullCheck(L_18);
			DateTime_t74  L_19 = Log_get_Timestamp_m252(L_18, /*hidden argument*/NULL);
			V_4 = L_19;
			String_t* L_20 = DateTime_ToString_m1460((&V_4), /*hidden argument*/NULL);
			NullCheck(L_17);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
			ArrayElementTypeCheck (L_17, L_20);
			*((String_t**)(String_t**)SZArrayLdElema(L_17, 2, sizeof(String_t*))) = (String_t*)L_20;
			StringU5BU5D_t137* L_21 = L_17;
			NullCheck(L_21);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 3);
			ArrayElementTypeCheck (L_21, _stringLiteral44);
			*((String_t**)(String_t**)SZArrayLdElema(L_21, 3, sizeof(String_t*))) = (String_t*)_stringLiteral44;
			StringU5BU5D_t137* L_22 = L_21;
			Log_t72 * L_23 = V_2;
			NullCheck(L_23);
			String_t* L_24 = Log_get_Message_m251(L_23, /*hidden argument*/NULL);
			NullCheck(L_22);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 4);
			ArrayElementTypeCheck (L_22, L_24);
			*((String_t**)(String_t**)SZArrayLdElema(L_22, 4, sizeof(String_t*))) = (String_t*)L_24;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_25 = String_Concat_m1461(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
			V_3 = L_25;
			StreamWriter_t396 * L_26 = V_0;
			String_t* L_27 = V_3;
			NullCheck(L_26);
			VirtActionInvoker1< String_t* >::Invoke(13 /* System.Void System.IO.TextWriter::WriteLine(System.String) */, L_26, L_27);
		}

IL_00b2:
		{
			Queue_1_t76 * L_28 = V_1;
			NullCheck(L_28);
			int32_t L_29 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<Common.Logger.Log>::get_Count() */, L_28);
			if ((((int32_t)L_29) > ((int32_t)0)))
			{
				goto IL_005f;
			}
		}

IL_00be:
		{
			IL2CPP_LEAVE(0xD0, FINALLY_00c3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_00c3;
	}

FINALLY_00c3:
	{ // begin finally (depth: 1)
		StreamWriter_t396 * L_30 = V_0;
		NullCheck(L_30);
		VirtActionInvoker0::Invoke(7 /* System.Void System.IO.StreamWriter::Flush() */, L_30);
		StreamWriter_t396 * L_31 = V_0;
		NullCheck(L_31);
		VirtActionInvoker0::Invoke(5 /* System.Void System.IO.StreamWriter::Close() */, L_31);
		IL2CPP_END_FINALLY(195)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(195)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_00d0:
	{
		__this->___currentlyWriting_4 = 0;
		return;
	}
}
// System.String Common.Logger.Logger::get_LogFilePath()
extern "C" String_t* Logger_get_LogFilePath_m268 (Logger_t75 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___logFilePath_1);
		return L_0;
	}
}
// LoggerComponent
#include "AssemblyU2DCSharp_LoggerComponent.h"
// LoggerComponent
#include "AssemblyU2DCSharp_LoggerComponentMethodDeclarations.h"
// System.Void LoggerComponent::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void LoggerComponent__ctor_m269 (LoggerComponent_t78 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoggerComponent::Awake()
// Common.Logger.Logger
#include "AssemblyU2DCSharp_Common_Logger_LoggerMethodDeclarations.h"
extern TypeInfo* Logger_t75_il2cpp_TypeInfo_var;
extern "C" void LoggerComponent_Awake_m270 (LoggerComponent_t78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t75_il2cpp_TypeInfo_var);
		Logger_t75 * L_0 = Logger_GetInstance_m258(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___logger_3 = L_0;
		Logger_t75 * L_1 = (__this->___logger_3);
		String_t* L_2 = (__this->___name_2);
		NullCheck(L_1);
		Logger_SetName_m259(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoggerComponent::Update()
extern "C" void LoggerComponent_Update_m271 (LoggerComponent_t78 * __this, const MethodInfo* method)
{
	{
		Logger_t75 * L_0 = (__this->___logger_3);
		NullCheck(L_0);
		Logger_Update_m266(L_0, /*hidden argument*/NULL);
		return;
	}
}
// Common.Math.IntVector2
#include "AssemblyU2DCSharp_Common_Math_IntVector2.h"
// Common.Math.IntVector2
#include "AssemblyU2DCSharp_Common_Math_IntVector2MethodDeclarations.h"
// System.Void Common.Math.IntVector2::.ctor()
// Common.Math.IntVector2
#include "AssemblyU2DCSharp_Common_Math_IntVector2MethodDeclarations.h"
extern "C" void IntVector2__ctor_m272 (IntVector2_t79 * __this, const MethodInfo* method)
{
	{
		IntVector2__ctor_m273(__this, 0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Math.IntVector2::.ctor(System.Int32,System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void IntVector2__ctor_m273 (IntVector2_t79 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___x;
		__this->___x_1 = L_0;
		int32_t L_1 = ___y;
		__this->___y_2 = L_1;
		return;
	}
}
// System.Void Common.Math.IntVector2::.ctor(Common.Math.IntVector2)
// Common.Math.IntVector2
#include "AssemblyU2DCSharp_Common_Math_IntVector2.h"
extern "C" void IntVector2__ctor_m274 (IntVector2_t79 * __this, IntVector2_t79 * ___other, const MethodInfo* method)
{
	{
		IntVector2_t79 * L_0 = ___other;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___x_1);
		IntVector2_t79 * L_2 = ___other;
		NullCheck(L_2);
		int32_t L_3 = (L_2->___y_2);
		IntVector2__ctor_m273(__this, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Math.IntVector2::.cctor()
extern TypeInfo* IntVector2_t79_il2cpp_TypeInfo_var;
extern "C" void IntVector2__cctor_m275 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntVector2_t79_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntVector2_t79 * L_0 = (IntVector2_t79 *)il2cpp_codegen_object_new (IntVector2_t79_il2cpp_TypeInfo_var);
		IntVector2__ctor_m273(L_0, 0, 0, /*hidden argument*/NULL);
		((IntVector2_t79_StaticFields*)IntVector2_t79_il2cpp_TypeInfo_var->static_fields)->___ZERO_0 = L_0;
		return;
	}
}
// System.Void Common.Math.IntVector2::Set(Common.Math.IntVector2)
extern "C" void IntVector2_Set_m276 (IntVector2_t79 * __this, IntVector2_t79 * ___other, const MethodInfo* method)
{
	{
		IntVector2_t79 * L_0 = ___other;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___x_1);
		__this->___x_1 = L_1;
		IntVector2_t79 * L_2 = ___other;
		NullCheck(L_2);
		int32_t L_3 = (L_2->___y_2);
		__this->___y_2 = L_3;
		return;
	}
}
// System.Boolean Common.Math.IntVector2::Equals(Common.Math.IntVector2)
extern "C" bool IntVector2_Equals_m277 (IntVector2_t79 * __this, IntVector2_t79 * ___other, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		IntVector2_t79 * L_0 = ___other;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		int32_t L_1 = (__this->___x_1);
		IntVector2_t79 * L_2 = ___other;
		NullCheck(L_2);
		int32_t L_3 = (L_2->___x_1);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_4 = (__this->___y_2);
		IntVector2_t79 * L_5 = ___other;
		NullCheck(L_5);
		int32_t L_6 = (L_5->___y_2);
		G_B5_0 = ((((int32_t)L_4) == ((int32_t)L_6))? 1 : 0);
		goto IL_002a;
	}

IL_0029:
	{
		G_B5_0 = 0;
	}

IL_002a:
	{
		return G_B5_0;
	}
}
// System.String Common.Math.IntVector2::ToString()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral45;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral47;
extern "C" String_t* IntVector2_ToString_m278 (IntVector2_t79 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral45 = il2cpp_codegen_string_literal_from_index(45);
		_stringLiteral46 = il2cpp_codegen_string_literal_from_index(46);
		_stringLiteral47 = il2cpp_codegen_string_literal_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t356* L_0 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral45);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral45;
		ObjectU5BU5D_t356* L_1 = L_0;
		int32_t L_2 = (__this->___x_1);
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t356* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral46);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral46;
		ObjectU5BU5D_t356* L_6 = L_5;
		int32_t L_7 = (__this->___y_2);
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t356* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral47;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m1316(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// MeshMerger
#include "AssemblyU2DCSharp_MeshMerger.h"
// MeshMerger
#include "AssemblyU2DCSharp_MeshMergerMethodDeclarations.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRenderer.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityEngine.SkinnedMeshRenderer
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeightMethodDeclarations.h"
// UnityEngine.SkinnedMeshRenderer
#include "UnityEngine_UnityEngine_SkinnedMeshRendererMethodDeclarations.h"
struct Renderer_t367;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::GetComponent<System.Object>()
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m1488_gshared (GameObject_t155 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m1488(__this, method) (( Object_t * (*) (GameObject_t155 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m1488_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t367_m1469(__this, method) (( Renderer_t367 * (*) (GameObject_t155 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m1488_gshared)(__this, method)
struct Renderer_t367;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t367_m1486(__this, method) (( Renderer_t367 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void MeshMerger::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void MeshMerger__ctor_m279 (MeshMerger_t80 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MeshMerger::Start()
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeightMethodDeclarations.h"
// UnityEngine.SkinnedMeshRenderer
#include "UnityEngine_UnityEngine_SkinnedMeshRendererMethodDeclarations.h"
extern const Il2CppType* MeshFilter_t398_0_0_0_var;
extern const Il2CppType* MeshRenderer_t402_0_0_0_var;
extern const Il2CppType* SkinnedMeshRenderer_t403_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* MeshFilterU5BU5D_t81_il2cpp_TypeInfo_var;
extern TypeInfo* MeshFilter_t398_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3U5BU5D_t254_il2cpp_TypeInfo_var;
extern TypeInfo* TransformU5BU5D_t354_il2cpp_TypeInfo_var;
extern TypeInfo* Matrix4x4U5BU5D_t399_il2cpp_TypeInfo_var;
extern TypeInfo* BoneWeightU5BU5D_t400_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t401_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2U5BU5D_t262_il2cpp_TypeInfo_var;
extern TypeInfo* MeshRenderer_t402_il2cpp_TypeInfo_var;
extern TypeInfo* Mesh_t104_il2cpp_TypeInfo_var;
extern TypeInfo* SkinnedMeshRenderer_t403_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t367_m1469_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t367_m1486_MethodInfo_var;
extern "C" void MeshMerger_Start_m280 (MeshMerger_t80 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MeshFilter_t398_0_0_0_var = il2cpp_codegen_type_from_index(86);
		MeshRenderer_t402_0_0_0_var = il2cpp_codegen_type_from_index(87);
		SkinnedMeshRenderer_t403_0_0_0_var = il2cpp_codegen_type_from_index(88);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		MeshFilterU5BU5D_t81_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(90);
		MeshFilter_t398_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		Vector3U5BU5D_t254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(91);
		TransformU5BU5D_t354_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(93);
		Matrix4x4U5BU5D_t399_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(94);
		BoneWeightU5BU5D_t400_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(96);
		Int32U5BU5D_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		Vector2U5BU5D_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(99);
		MeshRenderer_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(87);
		Mesh_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(100);
		SkinnedMeshRenderer_t403_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		GameObject_GetComponent_TisRenderer_t367_m1469_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483680);
		Component_GetComponent_TisRenderer_t367_m1486_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483681);
		s_Il2CppMethodIntialized = true;
	}
	ComponentU5BU5D_t397* V_0 = {0};
	int32_t V_1 = 0;
	Component_t365 * V_2 = {0};
	ComponentU5BU5D_t397* V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	MeshFilter_t398 * V_9 = {0};
	MeshFilterU5BU5D_t81* V_10 = {0};
	int32_t V_11 = 0;
	Vector3U5BU5D_t254* V_12 = {0};
	Vector3U5BU5D_t254* V_13 = {0};
	TransformU5BU5D_t354* V_14 = {0};
	Matrix4x4U5BU5D_t399* V_15 = {0};
	BoneWeightU5BU5D_t400* V_16 = {0};
	Int32U5BU5D_t401* V_17 = {0};
	Vector2U5BU5D_t262* V_18 = {0};
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	MeshFilter_t398 * V_24 = {0};
	MeshFilterU5BU5D_t81* V_25 = {0};
	int32_t V_26 = 0;
	int32_t V_27 = 0;
	Int32U5BU5D_t401* V_28 = {0};
	int32_t V_29 = 0;
	Vector3_t36  V_30 = {0};
	Vector3U5BU5D_t254* V_31 = {0};
	int32_t V_32 = 0;
	Vector3_t36  V_33 = {0};
	Vector3U5BU5D_t254* V_34 = {0};
	int32_t V_35 = 0;
	Vector2_t2  V_36 = {0};
	Vector2U5BU5D_t262* V_37 = {0};
	int32_t V_38 = 0;
	MeshRenderer_t402 * V_39 = {0};
	Mesh_t104 * V_40 = {0};
	SkinnedMeshRenderer_t403 * V_41 = {0};
	{
		MeshFilterU5BU5D_t81* L_0 = (__this->___meshFilters_2);
		NullCheck(L_0);
		if ((((int32_t)(((Array_t *)L_0)->max_length))))
		{
			goto IL_005f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(MeshFilter_t398_0_0_0_var), /*hidden argument*/NULL);
		ComponentU5BU5D_t397* L_2 = Component_GetComponentsInChildren_m1463(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ComponentU5BU5D_t397* L_3 = V_0;
		NullCheck(L_3);
		__this->___meshFilters_2 = ((MeshFilterU5BU5D_t81*)SZArrayNew(MeshFilterU5BU5D_t81_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_3)->max_length)))));
		V_1 = 0;
		ComponentU5BU5D_t397* L_4 = V_0;
		V_3 = L_4;
		V_4 = 0;
		goto IL_0055;
	}

IL_0038:
	{
		ComponentU5BU5D_t397* L_5 = V_3;
		int32_t L_6 = V_4;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_2 = (*(Component_t365 **)(Component_t365 **)SZArrayLdElema(L_5, L_7, sizeof(Component_t365 *)));
		MeshFilterU5BU5D_t81* L_8 = (__this->___meshFilters_2);
		int32_t L_9 = V_1;
		int32_t L_10 = L_9;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
		Component_t365 * L_11 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_10);
		ArrayElementTypeCheck (L_8, ((MeshFilter_t398 *)CastclassSealed(L_11, MeshFilter_t398_il2cpp_TypeInfo_var)));
		*((MeshFilter_t398 **)(MeshFilter_t398 **)SZArrayLdElema(L_8, L_10, sizeof(MeshFilter_t398 *))) = (MeshFilter_t398 *)((MeshFilter_t398 *)CastclassSealed(L_11, MeshFilter_t398_il2cpp_TypeInfo_var));
		int32_t L_12 = V_4;
		V_4 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0055:
	{
		int32_t L_13 = V_4;
		ComponentU5BU5D_t397* L_14 = V_3;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)(((Array_t *)L_14)->max_length))))))
		{
			goto IL_0038;
		}
	}

IL_005f:
	{
		V_5 = 0;
		V_6 = 0;
		V_7 = 0;
		V_8 = 0;
		MeshFilterU5BU5D_t81* L_15 = (__this->___meshFilters_2);
		V_10 = L_15;
		V_11 = 0;
		goto IL_00fc;
	}

IL_007b:
	{
		MeshFilterU5BU5D_t81* L_16 = V_10;
		int32_t L_17 = V_11;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		V_9 = (*(MeshFilter_t398 **)(MeshFilter_t398 **)SZArrayLdElema(L_16, L_18, sizeof(MeshFilter_t398 *)));
		int32_t L_19 = V_5;
		MeshFilter_t398 * L_20 = V_9;
		NullCheck(L_20);
		Mesh_t104 * L_21 = MeshFilter_get_mesh_m1464(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3U5BU5D_t254* L_22 = Mesh_get_vertices_m1465(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		V_5 = ((int32_t)((int32_t)L_19+(int32_t)(((int32_t)(((Array_t *)L_22)->max_length)))));
		int32_t L_23 = V_6;
		MeshFilter_t398 * L_24 = V_9;
		NullCheck(L_24);
		Mesh_t104 * L_25 = MeshFilter_get_mesh_m1464(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3U5BU5D_t254* L_26 = Mesh_get_normals_m1466(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		V_6 = ((int32_t)((int32_t)L_23+(int32_t)(((int32_t)(((Array_t *)L_26)->max_length)))));
		int32_t L_27 = V_7;
		MeshFilter_t398 * L_28 = V_9;
		NullCheck(L_28);
		Mesh_t104 * L_29 = MeshFilter_get_mesh_m1464(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Int32U5BU5D_t401* L_30 = Mesh_get_triangles_m1467(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		V_7 = ((int32_t)((int32_t)L_27+(int32_t)(((int32_t)(((Array_t *)L_30)->max_length)))));
		int32_t L_31 = V_8;
		MeshFilter_t398 * L_32 = V_9;
		NullCheck(L_32);
		Mesh_t104 * L_33 = MeshFilter_get_mesh_m1464(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector2U5BU5D_t262* L_34 = Mesh_get_uv_m1468(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		V_8 = ((int32_t)((int32_t)L_31+(int32_t)(((int32_t)(((Array_t *)L_34)->max_length)))));
		Material_t82 * L_35 = (__this->___material_3);
		bool L_36 = Object_op_Equality_m1413(NULL /*static, unused*/, L_35, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00f6;
		}
	}
	{
		MeshFilter_t398 * L_37 = V_9;
		NullCheck(L_37);
		GameObject_t155 * L_38 = Component_get_gameObject_m1337(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		Renderer_t367 * L_39 = GameObject_GetComponent_TisRenderer_t367_m1469(L_38, /*hidden argument*/GameObject_GetComponent_TisRenderer_t367_m1469_MethodInfo_var);
		NullCheck(L_39);
		Material_t82 * L_40 = Renderer_get_material_m1470(L_39, /*hidden argument*/NULL);
		__this->___material_3 = L_40;
	}

IL_00f6:
	{
		int32_t L_41 = V_11;
		V_11 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00fc:
	{
		int32_t L_42 = V_11;
		MeshFilterU5BU5D_t81* L_43 = V_10;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)(((Array_t *)L_43)->max_length))))))
		{
			goto IL_007b;
		}
	}
	{
		int32_t L_44 = V_5;
		V_12 = ((Vector3U5BU5D_t254*)SZArrayNew(Vector3U5BU5D_t254_il2cpp_TypeInfo_var, L_44));
		int32_t L_45 = V_6;
		V_13 = ((Vector3U5BU5D_t254*)SZArrayNew(Vector3U5BU5D_t254_il2cpp_TypeInfo_var, L_45));
		MeshFilterU5BU5D_t81* L_46 = (__this->___meshFilters_2);
		NullCheck(L_46);
		V_14 = ((TransformU5BU5D_t354*)SZArrayNew(TransformU5BU5D_t354_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_46)->max_length)))));
		MeshFilterU5BU5D_t81* L_47 = (__this->___meshFilters_2);
		NullCheck(L_47);
		V_15 = ((Matrix4x4U5BU5D_t399*)SZArrayNew(Matrix4x4U5BU5D_t399_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_47)->max_length)))));
		int32_t L_48 = V_5;
		V_16 = ((BoneWeightU5BU5D_t400*)SZArrayNew(BoneWeightU5BU5D_t400_il2cpp_TypeInfo_var, L_48));
		int32_t L_49 = V_7;
		V_17 = ((Int32U5BU5D_t401*)SZArrayNew(Int32U5BU5D_t401_il2cpp_TypeInfo_var, L_49));
		int32_t L_50 = V_8;
		V_18 = ((Vector2U5BU5D_t262*)SZArrayNew(Vector2U5BU5D_t262_il2cpp_TypeInfo_var, L_50));
		V_19 = 0;
		V_20 = 0;
		V_21 = 0;
		V_22 = 0;
		V_23 = 0;
		MeshFilterU5BU5D_t81* L_51 = (__this->___meshFilters_2);
		V_25 = L_51;
		V_26 = 0;
		goto IL_0318;
	}

IL_0171:
	{
		MeshFilterU5BU5D_t81* L_52 = V_25;
		int32_t L_53 = V_26;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		int32_t L_54 = L_53;
		V_24 = (*(MeshFilter_t398 **)(MeshFilter_t398 **)SZArrayLdElema(L_52, L_54, sizeof(MeshFilter_t398 *)));
		MeshFilter_t398 * L_55 = V_24;
		NullCheck(L_55);
		Mesh_t104 * L_56 = MeshFilter_get_mesh_m1464(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		Int32U5BU5D_t401* L_57 = Mesh_get_triangles_m1467(L_56, /*hidden argument*/NULL);
		V_28 = L_57;
		V_29 = 0;
		goto IL_01aa;
	}

IL_018e:
	{
		Int32U5BU5D_t401* L_58 = V_28;
		int32_t L_59 = V_29;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		int32_t L_60 = L_59;
		V_27 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_58, L_60, sizeof(int32_t)));
		Int32U5BU5D_t401* L_61 = V_17;
		int32_t L_62 = V_21;
		int32_t L_63 = L_62;
		V_21 = ((int32_t)((int32_t)L_63+(int32_t)1));
		int32_t L_64 = V_27;
		int32_t L_65 = V_19;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_63);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_61, L_63, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_64+(int32_t)L_65));
		int32_t L_66 = V_29;
		V_29 = ((int32_t)((int32_t)L_66+(int32_t)1));
	}

IL_01aa:
	{
		int32_t L_67 = V_29;
		Int32U5BU5D_t401* L_68 = V_28;
		NullCheck(L_68);
		if ((((int32_t)L_67) < ((int32_t)(((int32_t)(((Array_t *)L_68)->max_length))))))
		{
			goto IL_018e;
		}
	}
	{
		TransformU5BU5D_t354* L_69 = V_14;
		int32_t L_70 = V_23;
		MeshFilter_t398 * L_71 = V_24;
		NullCheck(L_71);
		Transform_t35 * L_72 = Component_get_transform_m1417(L_71, /*hidden argument*/NULL);
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, L_70);
		ArrayElementTypeCheck (L_69, L_72);
		*((Transform_t35 **)(Transform_t35 **)SZArrayLdElema(L_69, L_70, sizeof(Transform_t35 *))) = (Transform_t35 *)L_72;
		Matrix4x4U5BU5D_t399* L_73 = V_15;
		int32_t L_74 = V_23;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, L_74);
		Matrix4x4_t404  L_75 = Matrix4x4_get_identity_m1471(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Matrix4x4_t404 *)(Matrix4x4_t404 *)SZArrayLdElema(L_73, L_74, sizeof(Matrix4x4_t404 ))) = L_75;
		MeshFilter_t398 * L_76 = V_24;
		NullCheck(L_76);
		Mesh_t104 * L_77 = MeshFilter_get_mesh_m1464(L_76, /*hidden argument*/NULL);
		NullCheck(L_77);
		Vector3U5BU5D_t254* L_78 = Mesh_get_vertices_m1465(L_77, /*hidden argument*/NULL);
		V_31 = L_78;
		V_32 = 0;
		goto IL_0238;
	}

IL_01ea:
	{
		Vector3U5BU5D_t254* L_79 = V_31;
		int32_t L_80 = V_32;
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, L_80);
		V_30 = (*(Vector3_t36 *)((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_79, L_80, sizeof(Vector3_t36 ))));
		BoneWeightU5BU5D_t400* L_81 = V_16;
		int32_t L_82 = V_19;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, L_82);
		BoneWeight_set_weight0_m1472(((BoneWeight_t405 *)(BoneWeight_t405 *)SZArrayLdElema(L_81, L_82, sizeof(BoneWeight_t405 ))), (1.0f), /*hidden argument*/NULL);
		BoneWeightU5BU5D_t400* L_83 = V_16;
		int32_t L_84 = V_19;
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, L_84);
		int32_t L_85 = V_23;
		BoneWeight_set_boneIndex0_m1473(((BoneWeight_t405 *)(BoneWeight_t405 *)SZArrayLdElema(L_83, L_84, sizeof(BoneWeight_t405 ))), L_85, /*hidden argument*/NULL);
		Vector3U5BU5D_t254* L_86 = V_12;
		int32_t L_87 = V_19;
		int32_t L_88 = L_87;
		V_19 = ((int32_t)((int32_t)L_88+(int32_t)1));
		NullCheck(L_86);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_86, L_88);
		Vector3_t36  L_89 = V_30;
		*((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_86, L_88, sizeof(Vector3_t36 ))) = L_89;
		int32_t L_90 = V_32;
		V_32 = ((int32_t)((int32_t)L_90+(int32_t)1));
	}

IL_0238:
	{
		int32_t L_91 = V_32;
		Vector3U5BU5D_t254* L_92 = V_31;
		NullCheck(L_92);
		if ((((int32_t)L_91) < ((int32_t)(((int32_t)(((Array_t *)L_92)->max_length))))))
		{
			goto IL_01ea;
		}
	}
	{
		MeshFilter_t398 * L_93 = V_24;
		NullCheck(L_93);
		Mesh_t104 * L_94 = MeshFilter_get_mesh_m1464(L_93, /*hidden argument*/NULL);
		NullCheck(L_94);
		Vector3U5BU5D_t254* L_95 = Mesh_get_normals_m1466(L_94, /*hidden argument*/NULL);
		V_34 = L_95;
		V_35 = 0;
		goto IL_0284;
	}

IL_0259:
	{
		Vector3U5BU5D_t254* L_96 = V_34;
		int32_t L_97 = V_35;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, L_97);
		V_33 = (*(Vector3_t36 *)((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_96, L_97, sizeof(Vector3_t36 ))));
		Vector3U5BU5D_t254* L_98 = V_13;
		int32_t L_99 = V_20;
		int32_t L_100 = L_99;
		V_20 = ((int32_t)((int32_t)L_100+(int32_t)1));
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, L_100);
		Vector3_t36  L_101 = V_33;
		*((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_98, L_100, sizeof(Vector3_t36 ))) = L_101;
		int32_t L_102 = V_35;
		V_35 = ((int32_t)((int32_t)L_102+(int32_t)1));
	}

IL_0284:
	{
		int32_t L_103 = V_35;
		Vector3U5BU5D_t254* L_104 = V_34;
		NullCheck(L_104);
		if ((((int32_t)L_103) < ((int32_t)(((int32_t)(((Array_t *)L_104)->max_length))))))
		{
			goto IL_0259;
		}
	}
	{
		MeshFilter_t398 * L_105 = V_24;
		NullCheck(L_105);
		Mesh_t104 * L_106 = MeshFilter_get_mesh_m1464(L_105, /*hidden argument*/NULL);
		NullCheck(L_106);
		Vector2U5BU5D_t262* L_107 = Mesh_get_uv_m1468(L_106, /*hidden argument*/NULL);
		V_37 = L_107;
		V_38 = 0;
		goto IL_02d0;
	}

IL_02a5:
	{
		Vector2U5BU5D_t262* L_108 = V_37;
		int32_t L_109 = V_38;
		NullCheck(L_108);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_108, L_109);
		V_36 = (*(Vector2_t2 *)((Vector2_t2 *)(Vector2_t2 *)SZArrayLdElema(L_108, L_109, sizeof(Vector2_t2 ))));
		Vector2U5BU5D_t262* L_110 = V_18;
		int32_t L_111 = V_22;
		int32_t L_112 = L_111;
		V_22 = ((int32_t)((int32_t)L_112+(int32_t)1));
		NullCheck(L_110);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_110, L_112);
		Vector2_t2  L_113 = V_36;
		*((Vector2_t2 *)(Vector2_t2 *)SZArrayLdElema(L_110, L_112, sizeof(Vector2_t2 ))) = L_113;
		int32_t L_114 = V_38;
		V_38 = ((int32_t)((int32_t)L_114+(int32_t)1));
	}

IL_02d0:
	{
		int32_t L_115 = V_38;
		Vector2U5BU5D_t262* L_116 = V_37;
		NullCheck(L_116);
		if ((((int32_t)L_115) < ((int32_t)(((int32_t)(((Array_t *)L_116)->max_length))))))
		{
			goto IL_02a5;
		}
	}
	{
		int32_t L_117 = V_23;
		V_23 = ((int32_t)((int32_t)L_117+(int32_t)1));
		MeshFilter_t398 * L_118 = V_24;
		NullCheck(L_118);
		GameObject_t155 * L_119 = Component_get_gameObject_m1337(L_118, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_120 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(MeshRenderer_t402_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_119);
		Component_t365 * L_121 = GameObject_GetComponent_m1474(L_119, L_120, /*hidden argument*/NULL);
		V_39 = ((MeshRenderer_t402 *)IsInstSealed(L_121, MeshRenderer_t402_il2cpp_TypeInfo_var));
		MeshRenderer_t402 * L_122 = V_39;
		bool L_123 = Object_op_Implicit_m1320(NULL /*static, unused*/, L_122, /*hidden argument*/NULL);
		if (!L_123)
		{
			goto IL_0312;
		}
	}
	{
		MeshRenderer_t402 * L_124 = V_39;
		NullCheck(L_124);
		Renderer_set_enabled_m1335(L_124, 0, /*hidden argument*/NULL);
	}

IL_0312:
	{
		int32_t L_125 = V_26;
		V_26 = ((int32_t)((int32_t)L_125+(int32_t)1));
	}

IL_0318:
	{
		int32_t L_126 = V_26;
		MeshFilterU5BU5D_t81* L_127 = V_25;
		NullCheck(L_127);
		if ((((int32_t)L_126) < ((int32_t)(((int32_t)(((Array_t *)L_127)->max_length))))))
		{
			goto IL_0171;
		}
	}
	{
		Mesh_t104 * L_128 = (Mesh_t104 *)il2cpp_codegen_object_new (Mesh_t104_il2cpp_TypeInfo_var);
		Mesh__ctor_m1475(L_128, /*hidden argument*/NULL);
		V_40 = L_128;
		Mesh_t104 * L_129 = V_40;
		GameObject_t155 * L_130 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		NullCheck(L_130);
		String_t* L_131 = Object_get_name_m1338(L_130, /*hidden argument*/NULL);
		NullCheck(L_129);
		Object_set_name_m1476(L_129, L_131, /*hidden argument*/NULL);
		Mesh_t104 * L_132 = V_40;
		Vector3U5BU5D_t254* L_133 = V_12;
		NullCheck(L_132);
		Mesh_set_vertices_m1477(L_132, L_133, /*hidden argument*/NULL);
		Mesh_t104 * L_134 = V_40;
		Vector3U5BU5D_t254* L_135 = V_13;
		NullCheck(L_134);
		Mesh_set_normals_m1478(L_134, L_135, /*hidden argument*/NULL);
		Mesh_t104 * L_136 = V_40;
		BoneWeightU5BU5D_t400* L_137 = V_16;
		NullCheck(L_136);
		Mesh_set_boneWeights_m1479(L_136, L_137, /*hidden argument*/NULL);
		Mesh_t104 * L_138 = V_40;
		Vector2U5BU5D_t262* L_139 = V_18;
		NullCheck(L_138);
		Mesh_set_uv_m1480(L_138, L_139, /*hidden argument*/NULL);
		Mesh_t104 * L_140 = V_40;
		Int32U5BU5D_t401* L_141 = V_17;
		NullCheck(L_140);
		Mesh_set_triangles_m1481(L_140, L_141, /*hidden argument*/NULL);
		Mesh_t104 * L_142 = V_40;
		Matrix4x4U5BU5D_t399* L_143 = V_15;
		NullCheck(L_142);
		Mesh_set_bindposes_m1482(L_142, L_143, /*hidden argument*/NULL);
		GameObject_t155 * L_144 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_145 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(SkinnedMeshRenderer_t403_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_144);
		Component_t365 * L_146 = GameObject_AddComponent_m1483(L_144, L_145, /*hidden argument*/NULL);
		V_41 = ((SkinnedMeshRenderer_t403 *)IsInstClass(L_146, SkinnedMeshRenderer_t403_il2cpp_TypeInfo_var));
		SkinnedMeshRenderer_t403 * L_147 = V_41;
		Mesh_t104 * L_148 = V_40;
		NullCheck(L_147);
		SkinnedMeshRenderer_set_sharedMesh_m1484(L_147, L_148, /*hidden argument*/NULL);
		SkinnedMeshRenderer_t403 * L_149 = V_41;
		TransformU5BU5D_t354* L_150 = V_14;
		NullCheck(L_149);
		SkinnedMeshRenderer_set_bones_m1485(L_149, L_150, /*hidden argument*/NULL);
		Renderer_t367 * L_151 = Component_GetComponent_TisRenderer_t367_m1486(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t367_m1486_MethodInfo_var);
		Material_t82 * L_152 = (__this->___material_3);
		NullCheck(L_151);
		Renderer_set_material_m1487(L_151, L_152, /*hidden argument*/NULL);
		return;
	}
}
// MeshShaderSetter
#include "AssemblyU2DCSharp_MeshShaderSetter.h"
// MeshShaderSetter
#include "AssemblyU2DCSharp_MeshShaderSetterMethodDeclarations.h"
// System.Void MeshShaderSetter::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void MeshShaderSetter__ctor_m281 (MeshShaderSetter_t83 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String MeshShaderSetter::GetShaderToSet()
extern "C" String_t* MeshShaderSetter_GetShaderToSet_m282 (MeshShaderSetter_t83 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___shaderToSet_2);
		return L_0;
	}
}
// NotificationHandlerComponent
#include "AssemblyU2DCSharp_NotificationHandlerComponent.h"
// NotificationHandlerComponent
#include "AssemblyU2DCSharp_NotificationHandlerComponentMethodDeclarations.h"
// System.Void NotificationHandlerComponent::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void NotificationHandlerComponent__ctor_m283 (NotificationHandlerComponent_t84 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// Common.Notification.NotificationInstance
#include "AssemblyU2DCSharp_Common_Notification_NotificationInstance.h"
// Common.Notification.NotificationInstance
#include "AssemblyU2DCSharp_Common_Notification_NotificationInstanceMethodDeclarations.h"
// System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>
#include "System_System_Collections_Generic_Stack_1_gen.h"
// System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>
#include "System_System_Collections_Generic_Stack_1_genMethodDeclarations.h"
// System.Void Common.Notification.NotificationInstance::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void NotificationInstance__ctor_m284 (NotificationInstance_t85 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		__this->___active_1 = 0;
		return;
	}
}
// System.Void Common.Notification.NotificationInstance::.cctor()
// System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>
#include "System_System_Collections_Generic_Stack_1_genMethodDeclarations.h"
extern TypeInfo* Stack_1_t87_il2cpp_TypeInfo_var;
extern TypeInfo* NotificationInstance_t85_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m1489_MethodInfo_var;
extern "C" void NotificationInstance__cctor_m285 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Stack_1_t87_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(102);
		NotificationInstance_t85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		Stack_1__ctor_m1489_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483682);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stack_1_t87 * L_0 = (Stack_1_t87 *)il2cpp_codegen_object_new (Stack_1_t87_il2cpp_TypeInfo_var);
		Stack_1__ctor_m1489(L_0, /*hidden argument*/Stack_1__ctor_m1489_MethodInfo_var);
		((NotificationInstance_t85_StaticFields*)NotificationInstance_t85_il2cpp_TypeInfo_var->static_fields)->___instanceStack_4 = L_0;
		return;
	}
}
// System.Void Common.Notification.NotificationInstance::Init(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" void NotificationInstance_Init_m286 (NotificationInstance_t85 * __this, String_t* ___id, const MethodInfo* method)
{
	{
		String_t* L_0 = ___id;
		__this->___id_0 = L_0;
		Dictionary_2_t86 * L_1 = (__this->___parameterMap_2);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Dictionary_2_t86 * L_2 = (__this->___parameterMap_2);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Clear() */, L_2);
	}

IL_001d:
	{
		__this->___handled_3 = 0;
		return;
	}
}
// System.Void Common.Notification.NotificationInstance::Activate()
extern "C" void NotificationInstance_Activate_m287 (NotificationInstance_t85 * __this, const MethodInfo* method)
{
	{
		__this->___active_1 = 1;
		return;
	}
}
// System.Void Common.Notification.NotificationInstance::Deactivate()
extern "C" void NotificationInstance_Deactivate_m288 (NotificationInstance_t85 * __this, const MethodInfo* method)
{
	{
		__this->___active_1 = 0;
		return;
	}
}
// System.Boolean Common.Notification.NotificationInstance::IsActive()
extern "C" bool NotificationInstance_IsActive_m289 (NotificationInstance_t85 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___active_1);
		return L_0;
	}
}
// Common.Notification.NotificationInstance Common.Notification.NotificationInstance::AddParameter(System.String,System.Object)
// System.Object
#include "mscorlib_System_Object.h"
// Common.Notification.NotificationInstance
#include "AssemblyU2DCSharp_Common_Notification_NotificationInstanceMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_0MethodDeclarations.h"
extern TypeInfo* Dictionary_2_t86_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1428_MethodInfo_var;
extern "C" NotificationInstance_t85 * NotificationInstance_AddParameter_m290 (NotificationInstance_t85 * __this, String_t* ___key, Object_t * ___parameterValue, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t86_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(57);
		Dictionary_2__ctor_m1428_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483670);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = NotificationInstance_IsActive_m289(__this, /*hidden argument*/NULL);
		Assertion_Assert_m21(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Dictionary_2_t86 * L_1 = (__this->___parameterMap_2);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		Dictionary_2_t86 * L_2 = (Dictionary_2_t86 *)il2cpp_codegen_object_new (Dictionary_2_t86_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1428(L_2, /*hidden argument*/Dictionary_2__ctor_m1428_MethodInfo_var);
		__this->___parameterMap_2 = L_2;
	}

IL_0021:
	{
		Dictionary_2_t86 * L_3 = (__this->___parameterMap_2);
		String_t* L_4 = ___key;
		Object_t * L_5 = ___parameterValue;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_3, L_4, L_5);
		return __this;
	}
}
// System.String Common.Notification.NotificationInstance::GetId()
extern "C" String_t* NotificationInstance_GetId_m291 (NotificationInstance_t85 * __this, const MethodInfo* method)
{
	{
		bool L_0 = NotificationInstance_IsActive_m289(__this, /*hidden argument*/NULL);
		Assertion_Assert_m21(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_1 = (__this->___id_0);
		return L_1;
	}
}
// System.Boolean Common.Notification.NotificationInstance::HasParameter(System.String)
extern "C" bool NotificationInstance_HasParameter_m292 (NotificationInstance_t85 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		Dictionary_2_t86 * L_0 = (__this->___parameterMap_2);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Dictionary_2_t86 * L_1 = (__this->___parameterMap_2);
		String_t* L_2 = ___key;
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_1, L_2);
		return L_3;
	}
}
// System.Object Common.Notification.NotificationInstance::GetParameter(System.String)
extern "C" Object_t * NotificationInstance_GetParameter_m293 (NotificationInstance_t85 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		bool L_0 = NotificationInstance_IsActive_m289(__this, /*hidden argument*/NULL);
		Assertion_Assert_m21(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Dictionary_2_t86 * L_1 = (__this->___parameterMap_2);
		String_t* L_2 = ___key;
		NullCheck(L_1);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker1< Object_t *, String_t* >::Invoke(19 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_1, L_2);
		return L_3;
	}
}
// System.Void Common.Notification.NotificationInstance::MarkAsHandled()
extern "C" void NotificationInstance_MarkAsHandled_m294 (NotificationInstance_t85 * __this, const MethodInfo* method)
{
	{
		__this->___handled_3 = 1;
		return;
	}
}
// System.Boolean Common.Notification.NotificationInstance::IsHandled()
extern "C" bool NotificationInstance_IsHandled_m295 (NotificationInstance_t85 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___handled_3);
		return L_0;
	}
}
// Common.Notification.NotificationInstance Common.Notification.NotificationInstance::Borrow(System.String)
extern TypeInfo* NotificationInstance_t85_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Pop_m1490_MethodInfo_var;
extern "C" NotificationInstance_t85 * NotificationInstance_Borrow_m296 (Object_t * __this /* static, unused */, String_t* ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationInstance_t85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		Stack_1_Pop_m1490_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483683);
		s_Il2CppMethodIntialized = true;
	}
	NotificationInstance_t85 * V_0 = {0};
	{
		V_0 = (NotificationInstance_t85 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(NotificationInstance_t85_il2cpp_TypeInfo_var);
		Stack_1_t87 * L_0 = ((NotificationInstance_t85_StaticFields*)NotificationInstance_t85_il2cpp_TypeInfo_var->static_fields)->___instanceStack_4;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::get_Count() */, L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NotificationInstance_t85_il2cpp_TypeInfo_var);
		Stack_1_t87 * L_2 = ((NotificationInstance_t85_StaticFields*)NotificationInstance_t85_il2cpp_TypeInfo_var->static_fields)->___instanceStack_4;
		NullCheck(L_2);
		NotificationInstance_t85 * L_3 = Stack_1_Pop_m1490(L_2, /*hidden argument*/Stack_1_Pop_m1490_MethodInfo_var);
		V_0 = L_3;
		goto IL_0028;
	}

IL_0022:
	{
		NotificationInstance_t85 * L_4 = (NotificationInstance_t85 *)il2cpp_codegen_object_new (NotificationInstance_t85_il2cpp_TypeInfo_var);
		NotificationInstance__ctor_m284(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_0028:
	{
		NotificationInstance_t85 * L_5 = V_0;
		String_t* L_6 = ___id;
		NullCheck(L_5);
		NotificationInstance_Init_m286(L_5, L_6, /*hidden argument*/NULL);
		NotificationInstance_t85 * L_7 = V_0;
		NullCheck(L_7);
		NotificationInstance_Activate_m287(L_7, /*hidden argument*/NULL);
		NotificationInstance_t85 * L_8 = V_0;
		return L_8;
	}
}
// System.Void Common.Notification.NotificationInstance::Return(Common.Notification.NotificationInstance)
// Common.Notification.NotificationInstance
#include "AssemblyU2DCSharp_Common_Notification_NotificationInstance.h"
extern TypeInfo* NotificationInstance_t85_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Push_m1491_MethodInfo_var;
extern "C" void NotificationInstance_Return_m297 (Object_t * __this /* static, unused */, NotificationInstance_t85 * ___instance, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationInstance_t85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		Stack_1_Push_m1491_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483684);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotificationInstance_t85 * L_0 = ___instance;
		NullCheck(L_0);
		NotificationInstance_Deactivate_m288(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NotificationInstance_t85_il2cpp_TypeInfo_var);
		Stack_1_t87 * L_1 = ((NotificationInstance_t85_StaticFields*)NotificationInstance_t85_il2cpp_TypeInfo_var->static_fields)->___instanceStack_4;
		NotificationInstance_t85 * L_2 = ___instance;
		NullCheck(L_1);
		Stack_1_Push_m1491(L_1, L_2, /*hidden argument*/Stack_1_Push_m1491_MethodInfo_var);
		return;
	}
}
// Common.Notification.NotificationSystem/NotificationInitializer
#include "AssemblyU2DCSharp_Common_Notification_NotificationSystem_Not.h"
// Common.Notification.NotificationSystem/NotificationInitializer
#include "AssemblyU2DCSharp_Common_Notification_NotificationSystem_NotMethodDeclarations.h"
// System.Void Common.Notification.NotificationSystem/NotificationInitializer::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void NotificationInitializer__ctor_m298 (NotificationInitializer_t88 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void Common.Notification.NotificationSystem/NotificationInitializer::Invoke(Common.Notification.NotificationInstance)
// Common.Notification.NotificationInstance
#include "AssemblyU2DCSharp_Common_Notification_NotificationInstance.h"
extern "C" void NotificationInitializer_Invoke_m299 (NotificationInitializer_t88 * __this, NotificationInstance_t85 * ___notif, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		NotificationInitializer_Invoke_m299((NotificationInitializer_t88 *)__this->___prev_9,___notif, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, NotificationInstance_t85 * ___notif, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___notif,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, NotificationInstance_t85 * ___notif, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___notif,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___notif,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_NotificationInitializer_t88(Il2CppObject* delegate, NotificationInstance_t85 * ___notif)
{
	// Marshaling of parameter '___notif' to native representation
	NotificationInstance_t85 * ____notif_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'Common.Notification.NotificationInstance'."));
}
// System.IAsyncResult Common.Notification.NotificationSystem/NotificationInitializer::BeginInvoke(Common.Notification.NotificationInstance,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * NotificationInitializer_BeginInvoke_m300 (NotificationInitializer_t88 * __this, NotificationInstance_t85 * ___notif, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___notif;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void Common.Notification.NotificationSystem/NotificationInitializer::EndInvoke(System.IAsyncResult)
extern "C" void NotificationInitializer_EndInvoke_m301 (NotificationInitializer_t88 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Common.Notification.NotificationSystem
#include "AssemblyU2DCSharp_Common_Notification_NotificationSystem.h"
// Common.Notification.NotificationSystem
#include "AssemblyU2DCSharp_Common_Notification_NotificationSystemMethodDeclarations.h"
// System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>
#include "System_System_Collections_Generic_LinkedList_1_gen.h"
// System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>
#include "System_System_Collections_Generic_LinkedListNode_1_gen.h"
// System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>
#include "System_System_Collections_Generic_LinkedList_1_genMethodDeclarations.h"
// System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>
#include "System_System_Collections_Generic_LinkedListNode_1_genMethodDeclarations.h"
// System.Void Common.Notification.NotificationSystem::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>
#include "System_System_Collections_Generic_LinkedList_1_genMethodDeclarations.h"
extern TypeInfo* LinkedList_1_t90_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1__ctor_m1492_MethodInfo_var;
extern "C" void NotificationSystem__ctor_m302 (NotificationSystem_t89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LinkedList_1_t90_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		LinkedList_1__ctor_m1492_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483685);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		LinkedList_1_t90 * L_0 = (LinkedList_1_t90 *)il2cpp_codegen_object_new (LinkedList_1_t90_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m1492(L_0, /*hidden argument*/LinkedList_1__ctor_m1492_MethodInfo_var);
		__this->___handlerList_0 = L_0;
		return;
	}
}
// System.Void Common.Notification.NotificationSystem::AddHandler(Common.Notification.NotificationHandler)
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern const MethodInfo* LinkedList_1_AddLast_m1493_MethodInfo_var;
extern "C" void NotificationSystem_AddHandler_m303 (NotificationSystem_t89 * __this, Object_t * ___handler, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LinkedList_1_AddLast_m1493_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483686);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t90 * L_0 = (__this->___handlerList_0);
		Object_t * L_1 = ___handler;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(15 /* System.Boolean System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::Contains(!0) */, L_0, L_1);
		Assertion_Assert_m21(NULL /*static, unused*/, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		LinkedList_1_t90 * L_3 = (__this->___handlerList_0);
		Object_t * L_4 = ___handler;
		NullCheck(L_3);
		LinkedList_1_AddLast_m1493(L_3, L_4, /*hidden argument*/LinkedList_1_AddLast_m1493_MethodInfo_var);
		return;
	}
}
// System.Void Common.Notification.NotificationSystem::RemoveHandler(Common.Notification.NotificationHandler)
extern "C" void NotificationSystem_RemoveHandler_m304 (NotificationSystem_t89 * __this, Object_t * ___handler, const MethodInfo* method)
{
	{
		LinkedList_1_t90 * L_0 = (__this->___handlerList_0);
		Object_t * L_1 = ___handler;
		NullCheck(L_0);
		VirtFuncInvoker1< bool, Object_t * >::Invoke(17 /* System.Boolean System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::Remove(!0) */, L_0, L_1);
		return;
	}
}
// System.Void Common.Notification.NotificationSystem::ClearHandlers()
extern "C" void NotificationSystem_ClearHandlers_m305 (NotificationSystem_t89 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t90 * L_0 = (__this->___handlerList_0);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(14 /* System.Void System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::Clear() */, L_0);
		return;
	}
}
// System.Void Common.Notification.NotificationSystem::Post(System.String)
// System.String
#include "mscorlib_System_String.h"
// Common.Notification.NotificationInstance
#include "AssemblyU2DCSharp_Common_Notification_NotificationInstanceMethodDeclarations.h"
// Common.Notification.NotificationSystem
#include "AssemblyU2DCSharp_Common_Notification_NotificationSystemMethodDeclarations.h"
extern TypeInfo* NotificationInstance_t85_il2cpp_TypeInfo_var;
extern "C" void NotificationSystem_Post_m306 (NotificationSystem_t89 * __this, String_t* ___notificationId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationInstance_t85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___notificationId;
		IL2CPP_RUNTIME_CLASS_INIT(NotificationInstance_t85_il2cpp_TypeInfo_var);
		NotificationInstance_t85 * L_1 = NotificationInstance_Borrow_m296(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NotificationSystem_Post_m310(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Notification.NotificationSystem::Post(System.String,Common.Notification.NotificationSystem/NotificationInitializer)
// Common.Notification.NotificationSystem/NotificationInitializer
#include "AssemblyU2DCSharp_Common_Notification_NotificationSystem_Not.h"
// Common.Notification.NotificationSystem/NotificationInitializer
#include "AssemblyU2DCSharp_Common_Notification_NotificationSystem_NotMethodDeclarations.h"
extern TypeInfo* NotificationInstance_t85_il2cpp_TypeInfo_var;
extern "C" void NotificationSystem_Post_m307 (NotificationSystem_t89 * __this, String_t* ___notificationId, NotificationInitializer_t88 * ___initializer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationInstance_t85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		s_Il2CppMethodIntialized = true;
	}
	NotificationInstance_t85 * V_0 = {0};
	{
		String_t* L_0 = ___notificationId;
		IL2CPP_RUNTIME_CLASS_INIT(NotificationInstance_t85_il2cpp_TypeInfo_var);
		NotificationInstance_t85 * L_1 = NotificationInstance_Borrow_m296(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		NotificationInitializer_t88 * L_2 = ___initializer;
		NotificationInstance_t85 * L_3 = V_0;
		NullCheck(L_2);
		NotificationInitializer_Invoke_m299(L_2, L_3, /*hidden argument*/NULL);
		NotificationInstance_t85 * L_4 = V_0;
		NotificationSystem_Post_m310(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// Common.Notification.NotificationInstance Common.Notification.NotificationSystem::Begin(System.String)
extern TypeInfo* NotificationInstance_t85_il2cpp_TypeInfo_var;
extern "C" NotificationInstance_t85 * NotificationSystem_Begin_m308 (NotificationSystem_t89 * __this, String_t* ___notificationId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationInstance_t85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___notificationId;
		IL2CPP_RUNTIME_CLASS_INIT(NotificationInstance_t85_il2cpp_TypeInfo_var);
		NotificationInstance_t85 * L_1 = NotificationInstance_Borrow_m296(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Common.Notification.NotificationInstance Common.Notification.NotificationSystem::Start(System.String)
extern "C" NotificationInstance_t85 * NotificationSystem_Start_m309 (NotificationSystem_t89 * __this, String_t* ___notificationId, const MethodInfo* method)
{
	{
		String_t* L_0 = ___notificationId;
		NotificationInstance_t85 * L_1 = NotificationSystem_Begin_m308(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Common.Notification.NotificationSystem::Post(Common.Notification.NotificationInstance)
// Common.Notification.NotificationInstance
#include "AssemblyU2DCSharp_Common_Notification_NotificationInstance.h"
// System.Collections.Generic.LinkedListNode`1<Common.Notification.NotificationHandler>
#include "System_System_Collections_Generic_LinkedListNode_1_genMethodDeclarations.h"
extern TypeInfo* NotificationInstance_t85_il2cpp_TypeInfo_var;
extern TypeInfo* NotificationHandler_t347_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1_get_First_m1494_MethodInfo_var;
extern const MethodInfo* LinkedListNode_1_get_Value_m1495_MethodInfo_var;
extern const MethodInfo* LinkedListNode_1_get_Next_m1496_MethodInfo_var;
extern "C" void NotificationSystem_Post_m310 (NotificationSystem_t89 * __this, NotificationInstance_t85 * ___notification, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationInstance_t85_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		NotificationHandler_t347_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(103);
		LinkedList_1_get_First_m1494_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483687);
		LinkedListNode_1_get_Value_m1495_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483688);
		LinkedListNode_1_get_Next_m1496_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483689);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t406 * V_0 = {0};
	{
		LinkedList_1_t90 * L_0 = (__this->___handlerList_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 System.Collections.Generic.LinkedList`1<Common.Notification.NotificationHandler>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		NotificationInstance_t85 * L_2 = ___notification;
		IL2CPP_RUNTIME_CLASS_INIT(NotificationInstance_t85_il2cpp_TypeInfo_var);
		NotificationInstance_Return_m297(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0017:
	{
		LinkedList_1_t90 * L_3 = (__this->___handlerList_0);
		NullCheck(L_3);
		LinkedListNode_1_t406 * L_4 = LinkedList_1_get_First_m1494(L_3, /*hidden argument*/LinkedList_1_get_First_m1494_MethodInfo_var);
		V_0 = L_4;
	}

IL_0023:
	{
		LinkedListNode_1_t406 * L_5 = V_0;
		NullCheck(L_5);
		Object_t * L_6 = LinkedListNode_1_get_Value_m1495(L_5, /*hidden argument*/LinkedListNode_1_get_Value_m1495_MethodInfo_var);
		NotificationInstance_t85 * L_7 = ___notification;
		NullCheck(L_6);
		InterfaceActionInvoker1< Object_t * >::Invoke(1 /* System.Void Common.Notification.NotificationHandler::HandleNotification(Common.Notification.Notification) */, NotificationHandler_t347_il2cpp_TypeInfo_var, L_6, L_7);
		LinkedListNode_1_t406 * L_8 = V_0;
		NullCheck(L_8);
		LinkedListNode_1_t406 * L_9 = LinkedListNode_1_get_Next_m1496(L_8, /*hidden argument*/LinkedListNode_1_get_Next_m1496_MethodInfo_var);
		V_0 = L_9;
		LinkedListNode_1_t406 * L_10 = V_0;
		if (L_10)
		{
			goto IL_0023;
		}
	}
	{
		NotificationInstance_t85 * L_11 = ___notification;
		IL2CPP_RUNTIME_CLASS_INIT(NotificationInstance_t85_il2cpp_TypeInfo_var);
		NotificationInstance_Return_m297(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// Common.Notification.NotificationSystem Common.Notification.NotificationSystem::GetInstance()
extern TypeInfo* NotificationSystem_t89_il2cpp_TypeInfo_var;
extern "C" NotificationSystem_t89 * NotificationSystem_GetInstance_m311 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationSystem_t89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(105);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotificationSystem_t89 * L_0 = ((NotificationSystem_t89_StaticFields*)NotificationSystem_t89_il2cpp_TypeInfo_var->static_fields)->___ONLY_INSTANCE_1;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		NotificationSystem_t89 * L_1 = (NotificationSystem_t89 *)il2cpp_codegen_object_new (NotificationSystem_t89_il2cpp_TypeInfo_var);
		NotificationSystem__ctor_m302(L_1, /*hidden argument*/NULL);
		((NotificationSystem_t89_StaticFields*)NotificationSystem_t89_il2cpp_TypeInfo_var->static_fields)->___ONLY_INSTANCE_1 = L_1;
	}

IL_0014:
	{
		NotificationSystem_t89 * L_2 = ((NotificationSystem_t89_StaticFields*)NotificationSystem_t89_il2cpp_TypeInfo_var->static_fields)->___ONLY_INSTANCE_1;
		return L_2;
	}
}
// NotificationSystemComponent
#include "AssemblyU2DCSharp_NotificationSystemComponent.h"
// NotificationSystemComponent
#include "AssemblyU2DCSharp_NotificationSystemComponentMethodDeclarations.h"
// System.Void NotificationSystemComponent::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void NotificationSystemComponent__ctor_m312 (NotificationSystemComponent_t91 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NotificationSystemComponent::Awake()
// NotificationSystemComponent
#include "AssemblyU2DCSharp_NotificationSystemComponentMethodDeclarations.h"
extern "C" void NotificationSystemComponent_Awake_m313 (NotificationSystemComponent_t91 * __this, const MethodInfo* method)
{
	NotificationHandlerComponent_t84 * V_0 = {0};
	NotificationHandlerComponentU5BU5D_t92* V_1 = {0};
	int32_t V_2 = 0;
	{
		NotificationHandlerComponentU5BU5D_t92* L_0 = (__this->___handlerList_2);
		V_1 = L_0;
		V_2 = 0;
		goto IL_001d;
	}

IL_000e:
	{
		NotificationHandlerComponentU5BU5D_t92* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(NotificationHandlerComponent_t84 **)(NotificationHandlerComponent_t84 **)SZArrayLdElema(L_1, L_3, sizeof(NotificationHandlerComponent_t84 *)));
		NotificationHandlerComponent_t84 * L_4 = V_0;
		NotificationSystemComponent_AddHandler_m314(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_2;
		V_2 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_6 = V_2;
		NotificationHandlerComponentU5BU5D_t92* L_7 = V_1;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void NotificationSystemComponent::AddHandler(Common.Notification.NotificationHandler)
// Common.Notification.NotificationSystem
#include "AssemblyU2DCSharp_Common_Notification_NotificationSystemMethodDeclarations.h"
extern "C" void NotificationSystemComponent_AddHandler_m314 (NotificationSystemComponent_t91 * __this, Object_t * ___handler, const MethodInfo* method)
{
	{
		NotificationSystem_t89 * L_0 = NotificationSystemComponent_get_System_m319(__this, /*hidden argument*/NULL);
		Object_t * L_1 = ___handler;
		NullCheck(L_0);
		NotificationSystem_AddHandler_m303(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NotificationSystemComponent::RemoveHandler(Common.Notification.NotificationHandler)
extern "C" void NotificationSystemComponent_RemoveHandler_m315 (NotificationSystemComponent_t91 * __this, Object_t * ___handler, const MethodInfo* method)
{
	{
		NotificationSystem_t89 * L_0 = NotificationSystemComponent_get_System_m319(__this, /*hidden argument*/NULL);
		Object_t * L_1 = ___handler;
		NullCheck(L_0);
		NotificationSystem_RemoveHandler_m304(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NotificationSystemComponent::ClearHandlers()
extern "C" void NotificationSystemComponent_ClearHandlers_m316 (NotificationSystemComponent_t91 * __this, const MethodInfo* method)
{
	{
		NotificationSystem_t89 * L_0 = NotificationSystemComponent_get_System_m319(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		NotificationSystem_ClearHandlers_m305(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NotificationSystemComponent::Post(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" void NotificationSystemComponent_Post_m317 (NotificationSystemComponent_t91 * __this, String_t* ___notificationId, const MethodInfo* method)
{
	{
		NotificationSystem_t89 * L_0 = NotificationSystemComponent_get_System_m319(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___notificationId;
		NullCheck(L_0);
		NotificationSystem_Post_m306(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NotificationSystemComponent::Post(Common.Notification.NotificationInstance)
// Common.Notification.NotificationInstance
#include "AssemblyU2DCSharp_Common_Notification_NotificationInstance.h"
extern "C" void NotificationSystemComponent_Post_m318 (NotificationSystemComponent_t91 * __this, NotificationInstance_t85 * ___notification, const MethodInfo* method)
{
	{
		NotificationSystem_t89 * L_0 = NotificationSystemComponent_get_System_m319(__this, /*hidden argument*/NULL);
		NotificationInstance_t85 * L_1 = ___notification;
		NullCheck(L_0);
		NotificationSystem_Post_m310(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Common.Notification.NotificationSystem NotificationSystemComponent::get_System()
extern TypeInfo* NotificationSystem_t89_il2cpp_TypeInfo_var;
extern "C" NotificationSystem_t89 * NotificationSystemComponent_get_System_m319 (NotificationSystemComponent_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotificationSystem_t89_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(105);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotificationSystem_t89 * L_0 = (__this->___system_3);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		NotificationSystem_t89 * L_1 = (NotificationSystem_t89 *)il2cpp_codegen_object_new (NotificationSystem_t89_il2cpp_TypeInfo_var);
		NotificationSystem__ctor_m302(L_1, /*hidden argument*/NULL);
		__this->___system_3 = L_1;
	}

IL_0016:
	{
		NotificationSystem_t89 * L_2 = (__this->___system_3);
		return L_2;
	}
}
// PrefabManager/PruneData
#include "AssemblyU2DCSharp_PrefabManager_PruneData.h"
// PrefabManager/PruneData
#include "AssemblyU2DCSharp_PrefabManager_PruneDataMethodDeclarations.h"
// System.Void PrefabManager/PruneData::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void PruneData__ctor_m320 (PruneData_t93 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PrefabManager/PruneData::Set(System.String,System.Int32)
// System.String
#include "mscorlib_System_String.h"
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void PruneData_Set_m321 (PruneData_t93 * __this, String_t* ___prefabName, int32_t ___maxInactiveCount, const MethodInfo* method)
{
	{
		String_t* L_0 = ___prefabName;
		__this->___prefabName_0 = L_0;
		int32_t L_1 = ___maxInactiveCount;
		__this->___maxInactiveCount_1 = L_1;
		return;
	}
}
// System.String PrefabManager/PruneData::get_PrefabName()
extern "C" String_t* PruneData_get_PrefabName_m322 (PruneData_t93 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___prefabName_0);
		return L_0;
	}
}
// System.Int32 PrefabManager/PruneData::get_MaxInactiveCount()
extern "C" int32_t PruneData_get_MaxInactiveCount_m323 (PruneData_t93 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___maxInactiveCount_1);
		return L_0;
	}
}
// PrefabManager/PreloadData
#include "AssemblyU2DCSharp_PrefabManager_PreloadData.h"
// PrefabManager/PreloadData
#include "AssemblyU2DCSharp_PrefabManager_PreloadDataMethodDeclarations.h"
// System.Void PrefabManager/PreloadData::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void PreloadData__ctor_m324 (PreloadData_t94 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PrefabManager/PreloadData::Set(System.String,System.Int32)
// System.String
#include "mscorlib_System_String.h"
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" void PreloadData_Set_m325 (PreloadData_t94 * __this, String_t* ___prefabName, int32_t ___preloadCount, const MethodInfo* method)
{
	{
		String_t* L_0 = ___prefabName;
		__this->___prefabName_0 = L_0;
		int32_t L_1 = ___preloadCount;
		__this->___preloadCount_1 = L_1;
		return;
	}
}
// System.String PrefabManager/PreloadData::get_PrefabName()
extern "C" String_t* PreloadData_get_PrefabName_m326 (PreloadData_t94 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___prefabName_0);
		return L_0;
	}
}
// System.Int32 PrefabManager/PreloadData::get_PreloadCount()
extern "C" int32_t PreloadData_get_PreloadCount_m327 (PreloadData_t94 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___preloadCount_1);
		return L_0;
	}
}
// PrefabManager/<Preload>c__Iterator1
#include "AssemblyU2DCSharp_PrefabManager_U3CPreloadU3Ec__Iterator1.h"
// PrefabManager/<Preload>c__Iterator1
#include "AssemblyU2DCSharp_PrefabManager_U3CPreloadU3Ec__Iterator1MethodDeclarations.h"
// PrefabManager
#include "AssemblyU2DCSharp_PrefabManager.h"
// PrefabManager
#include "AssemblyU2DCSharp_PrefabManagerMethodDeclarations.h"
// System.Void PrefabManager/<Preload>c__Iterator1::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void U3CPreloadU3Ec__Iterator1__ctor_m328 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object PrefabManager/<Preload>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CPreloadU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m329 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object PrefabManager/<Preload>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CPreloadU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m330 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean PrefabManager/<Preload>c__Iterator1::MoveNext()
// PrefabManager/PreloadData
#include "AssemblyU2DCSharp_PrefabManager_PreloadDataMethodDeclarations.h"
// PrefabManager
#include "AssemblyU2DCSharp_PrefabManagerMethodDeclarations.h"
extern TypeInfo* Boolean_t384_il2cpp_TypeInfo_var;
extern "C" bool U3CPreloadU3Ec__Iterator1_MoveNext_m331 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t384_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
		if (L_1 == 2)
		{
			goto IL_0077;
		}
	}
	{
		goto IL_00e9;
	}

IL_0025:
	{
		bool L_2 = 1;
		Object_t * L_3 = Box(Boolean_t384_il2cpp_TypeInfo_var, &L_2);
		__this->___U24current_2 = L_3;
		__this->___U24PC_1 = 1;
		goto IL_00eb;
	}

IL_003d:
	{
		PrefabManager_t96 * L_4 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_4);
		PreloadDataU5BU5D_t100* L_5 = (L_4->___preloadDataList_4);
		if (!L_5)
		{
			goto IL_005f;
		}
	}
	{
		PrefabManager_t96 * L_6 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_6);
		PreloadDataU5BU5D_t100* L_7 = (L_6->___preloadDataList_4);
		NullCheck(L_7);
		if ((((int32_t)(((Array_t *)L_7)->max_length))))
		{
			goto IL_0077;
		}
	}

IL_005f:
	{
		bool L_8 = 0;
		Object_t * L_9 = Box(Boolean_t384_il2cpp_TypeInfo_var, &L_8);
		__this->___U24current_2 = L_9;
		__this->___U24PC_1 = 2;
		goto IL_00eb;
	}

IL_0077:
	{
		__this->___U3CiU3E__0_0 = 0;
		goto IL_00ca;
	}

IL_0083:
	{
		PrefabManager_t96 * L_10 = (__this->___U3CU3Ef__this_3);
		PrefabManager_t96 * L_11 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_11);
		PreloadDataU5BU5D_t100* L_12 = (L_11->___preloadDataList_4);
		int32_t L_13 = (__this->___U3CiU3E__0_0);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		NullCheck((*(PreloadData_t94 **)(PreloadData_t94 **)SZArrayLdElema(L_12, L_14, sizeof(PreloadData_t94 *))));
		String_t* L_15 = PreloadData_get_PrefabName_m326((*(PreloadData_t94 **)(PreloadData_t94 **)SZArrayLdElema(L_12, L_14, sizeof(PreloadData_t94 *))), /*hidden argument*/NULL);
		PrefabManager_t96 * L_16 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_16);
		PreloadDataU5BU5D_t100* L_17 = (L_16->___preloadDataList_4);
		int32_t L_18 = (__this->___U3CiU3E__0_0);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		NullCheck((*(PreloadData_t94 **)(PreloadData_t94 **)SZArrayLdElema(L_17, L_19, sizeof(PreloadData_t94 *))));
		int32_t L_20 = PreloadData_get_PreloadCount_m327((*(PreloadData_t94 **)(PreloadData_t94 **)SZArrayLdElema(L_17, L_19, sizeof(PreloadData_t94 *))), /*hidden argument*/NULL);
		NullCheck(L_10);
		PrefabManager_Preload_m348(L_10, L_15, L_20, /*hidden argument*/NULL);
		int32_t L_21 = (__this->___U3CiU3E__0_0);
		__this->___U3CiU3E__0_0 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_00ca:
	{
		int32_t L_22 = (__this->___U3CiU3E__0_0);
		PrefabManager_t96 * L_23 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_23);
		PreloadDataU5BU5D_t100* L_24 = (L_23->___preloadDataList_4);
		NullCheck(L_24);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)(((Array_t *)L_24)->max_length))))))
		{
			goto IL_0083;
		}
	}
	{
		__this->___U24PC_1 = (-1);
	}

IL_00e9:
	{
		return 0;
	}

IL_00eb:
	{
		return 1;
	}
	// Dead block : IL_00ed: ldloc.1
}
// System.Void PrefabManager/<Preload>c__Iterator1::Dispose()
extern "C" void U3CPreloadU3Ec__Iterator1_Dispose_m332 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void PrefabManager/<Preload>c__Iterator1::Reset()
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern "C" void U3CPreloadU3Ec__Iterator1_Reset_m333 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1366(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// PrefabManager/<PruneItems>c__Iterator2
#include "AssemblyU2DCSharp_PrefabManager_U3CPruneItemsU3Ec__Iterator2.h"
// PrefabManager/<PruneItems>c__Iterator2
#include "AssemblyU2DCSharp_PrefabManager_U3CPruneItemsU3Ec__Iterator2MethodDeclarations.h"
// SelfManagingSwarmItemManager
#include "AssemblyU2DCSharp_SelfManagingSwarmItemManager.h"
// SelfManagingSwarmItemManager
#include "AssemblyU2DCSharp_SelfManagingSwarmItemManagerMethodDeclarations.h"
// System.Void PrefabManager/<PruneItems>c__Iterator2::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void U3CPruneItemsU3Ec__Iterator2__ctor_m334 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object PrefabManager/<PruneItems>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CPruneItemsU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m335 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Object PrefabManager/<PruneItems>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CPruneItemsU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m336 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Boolean PrefabManager/<PruneItems>c__Iterator2::MoveNext()
// PrefabManager/PruneData
#include "AssemblyU2DCSharp_PrefabManager_PruneDataMethodDeclarations.h"
// SelfManagingSwarmItemManager
#include "AssemblyU2DCSharp_SelfManagingSwarmItemManagerMethodDeclarations.h"
extern TypeInfo* IDictionary_2_t101_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern "C" bool U3CPruneItemsU3Ec__Iterator2_MoveNext_m337 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(106);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_4);
		V_0 = L_0;
		__this->___U24PC_4 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ab;
		}
	}
	{
		goto IL_00d3;
	}

IL_0021:
	{
		PrefabManager_t96 * L_2 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_2);
		PruneDataU5BU5D_t98* L_3 = (L_2->___pruneDataList_3);
		__this->___U3CU24s_40U3E__0_0 = L_3;
		__this->___U3CU24s_41U3E__1_1 = 0;
		goto IL_00b9;
	}

IL_003e:
	{
		PruneDataU5BU5D_t98* L_4 = (__this->___U3CU24s_40U3E__0_0);
		int32_t L_5 = (__this->___U3CU24s_41U3E__1_1);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		__this->___U3CdataU3E__2_2 = (*(PruneData_t93 **)(PruneData_t93 **)SZArrayLdElema(L_4, L_6, sizeof(PruneData_t93 *)));
		PrefabManager_t96 * L_7 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_7);
		Object_t* L_8 = (L_7->___nameToIndexMapping_6);
		PruneData_t93 * L_9 = (__this->___U3CdataU3E__2_2);
		NullCheck(L_9);
		String_t* L_10 = PruneData_get_PrefabName_m322(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_11 = (int32_t)InterfaceFuncInvoker1< int32_t, String_t* >::Invoke(1 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Int32>::get_Item(!0) */, IDictionary_2_t101_il2cpp_TypeInfo_var, L_8, L_10);
		__this->___U3CitemPrefabIndexU3E__3_3 = L_11;
		PrefabManager_t96 * L_12 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_12);
		SelfManagingSwarmItemManager_t99 * L_13 = (L_12->___itemManager_2);
		int32_t L_14 = (__this->___U3CitemPrefabIndexU3E__3_3);
		PruneData_t93 * L_15 = (__this->___U3CdataU3E__2_2);
		NullCheck(L_15);
		int32_t L_16 = PruneData_get_MaxInactiveCount_m323(L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		SelfManagingSwarmItemManager_PruneInactiveList_m384(L_13, L_14, L_16, /*hidden argument*/NULL);
		int32_t L_17 = 0;
		Object_t * L_18 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_17);
		__this->___U24current_5 = L_18;
		__this->___U24PC_4 = 1;
		goto IL_00d5;
	}

IL_00ab:
	{
		int32_t L_19 = (__this->___U3CU24s_41U3E__1_1);
		__this->___U3CU24s_41U3E__1_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_00b9:
	{
		int32_t L_20 = (__this->___U3CU24s_41U3E__1_1);
		PruneDataU5BU5D_t98* L_21 = (__this->___U3CU24s_40U3E__0_0);
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)(((Array_t *)L_21)->max_length))))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___U24PC_4 = (-1);
	}

IL_00d3:
	{
		return 0;
	}

IL_00d5:
	{
		return 1;
	}
	// Dead block : IL_00d7: ldloc.1
}
// System.Void PrefabManager/<PruneItems>c__Iterator2::Dispose()
extern "C" void U3CPruneItemsU3Ec__Iterator2_Dispose_m338 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_4 = (-1);
		return;
	}
}
// System.Void PrefabManager/<PruneItems>c__Iterator2::Reset()
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern "C" void U3CPruneItemsU3Ec__Iterator2_Reset_m339 (U3CPruneItemsU3Ec__Iterator2_t97 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1366(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// SwarmItemManager/PrefabItem
#include "AssemblyU2DCSharp_SwarmItemManager_PrefabItem.h"
// SwarmItemManager
#include "AssemblyU2DCSharp_SwarmItemManager.h"
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"
// SwarmItem
#include "AssemblyU2DCSharp_SwarmItem.h"
// SwarmItemManager
#include "AssemblyU2DCSharp_SwarmItemManagerMethodDeclarations.h"
// System.Void PrefabManager::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void PrefabManager__ctor_m340 (PrefabManager_t96 * __this, const MethodInfo* method)
{
	{
		__this->___pruneIntervalTime_5 = (1.0f);
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PrefabManager::Awake()
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// PrefabManager
#include "AssemblyU2DCSharp_PrefabManagerMethodDeclarations.h"
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimerMethodDeclarations.h"
extern TypeInfo* Dictionary_2_t68_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t101_il2cpp_TypeInfo_var;
extern TypeInfo* CountdownTimer_t13_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1497_MethodInfo_var;
extern "C" void PrefabManager_Awake_m341 (PrefabManager_t96 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(64);
		IDictionary_2_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(106);
		CountdownTimer_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		Dictionary_2__ctor_m1497_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483690);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	PrefabItem_t154 * V_1 = {0};
	{
		Dictionary_2_t68 * L_0 = (Dictionary_2_t68 *)il2cpp_codegen_object_new (Dictionary_2_t68_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1497(L_0, /*hidden argument*/Dictionary_2__ctor_m1497_MethodInfo_var);
		__this->___nameToIndexMapping_6 = L_0;
		V_0 = 0;
		goto IL_003b;
	}

IL_0012:
	{
		SelfManagingSwarmItemManager_t99 * L_1 = (__this->___itemManager_2);
		NullCheck(L_1);
		PrefabItemU5BU5D_t157* L_2 = (((SwarmItemManager_t111 *)L_1)->___itemPrefabs_10);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_1 = (*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_2, L_4, sizeof(PrefabItem_t154 *)));
		Object_t* L_5 = (__this->___nameToIndexMapping_6);
		PrefabItem_t154 * L_6 = V_1;
		NullCheck(L_6);
		GameObject_t155 * L_7 = (L_6->___prefab_0);
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m1338(L_7, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		NullCheck(L_5);
		InterfaceActionInvoker2< String_t*, int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Int32>::set_Item(!0,!1) */, IDictionary_2_t101_il2cpp_TypeInfo_var, L_5, L_8, L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_11 = V_0;
		SelfManagingSwarmItemManager_t99 * L_12 = (__this->___itemManager_2);
		NullCheck(L_12);
		PrefabItemU5BU5D_t157* L_13 = (((SwarmItemManager_t111 *)L_12)->___itemPrefabs_10);
		NullCheck(L_13);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_13)->max_length))))))
		{
			goto IL_0012;
		}
	}
	{
		Object_t * L_14 = PrefabManager_Preload_m342(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1498(__this, L_14, /*hidden argument*/NULL);
		PruneDataU5BU5D_t98* L_15 = (__this->___pruneDataList_3);
		if (!L_15)
		{
			goto IL_0084;
		}
	}
	{
		PruneDataU5BU5D_t98* L_16 = (__this->___pruneDataList_3);
		NullCheck(L_16);
		if (!(((int32_t)(((Array_t *)L_16)->max_length))))
		{
			goto IL_0084;
		}
	}
	{
		float L_17 = (__this->___pruneIntervalTime_5);
		CountdownTimer_t13 * L_18 = (CountdownTimer_t13 *)il2cpp_codegen_object_new (CountdownTimer_t13_il2cpp_TypeInfo_var);
		CountdownTimer__ctor_m63(L_18, L_17, /*hidden argument*/NULL);
		__this->___pruneTimer_7 = L_18;
	}

IL_0084:
	{
		return;
	}
}
// System.Collections.IEnumerator PrefabManager::Preload()
// PrefabManager/<Preload>c__Iterator1
#include "AssemblyU2DCSharp_PrefabManager_U3CPreloadU3Ec__Iterator1MethodDeclarations.h"
extern TypeInfo* U3CPreloadU3Ec__Iterator1_t95_il2cpp_TypeInfo_var;
extern "C" Object_t * PrefabManager_Preload_m342 (PrefabManager_t96 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CPreloadU3Ec__Iterator1_t95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(107);
		s_Il2CppMethodIntialized = true;
	}
	U3CPreloadU3Ec__Iterator1_t95 * V_0 = {0};
	{
		U3CPreloadU3Ec__Iterator1_t95 * L_0 = (U3CPreloadU3Ec__Iterator1_t95 *)il2cpp_codegen_object_new (U3CPreloadU3Ec__Iterator1_t95_il2cpp_TypeInfo_var);
		U3CPreloadU3Ec__Iterator1__ctor_m328(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPreloadU3Ec__Iterator1_t95 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CPreloadU3Ec__Iterator1_t95 * L_2 = V_0;
		return L_2;
	}
}
// System.Void PrefabManager::Prune()
extern "C" void PrefabManager_Prune_m343 (PrefabManager_t96 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = PrefabManager_PruneItems_m344(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1498(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator PrefabManager::PruneItems()
// PrefabManager/<PruneItems>c__Iterator2
#include "AssemblyU2DCSharp_PrefabManager_U3CPruneItemsU3Ec__Iterator2MethodDeclarations.h"
extern TypeInfo* U3CPruneItemsU3Ec__Iterator2_t97_il2cpp_TypeInfo_var;
extern "C" Object_t * PrefabManager_PruneItems_m344 (PrefabManager_t96 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CPruneItemsU3Ec__Iterator2_t97_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	U3CPruneItemsU3Ec__Iterator2_t97 * V_0 = {0};
	{
		U3CPruneItemsU3Ec__Iterator2_t97 * L_0 = (U3CPruneItemsU3Ec__Iterator2_t97 *)il2cpp_codegen_object_new (U3CPruneItemsU3Ec__Iterator2_t97_il2cpp_TypeInfo_var);
		U3CPruneItemsU3Ec__Iterator2__ctor_m334(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPruneItemsU3Ec__Iterator2_t97 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_6 = __this;
		U3CPruneItemsU3Ec__Iterator2_t97 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.GameObject PrefabManager::Request(System.String)
// System.String
#include "mscorlib_System_String.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern TypeInfo* IDictionary_2_t101_il2cpp_TypeInfo_var;
extern "C" GameObject_t155 * PrefabManager_Request_m345 (PrefabManager_t96 * __this, String_t* ___prefabName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(106);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Object_t* L_0 = (__this->___nameToIndexMapping_6);
		String_t* L_1 = ___prefabName;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, String_t* >::Invoke(0 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Int32>::ContainsKey(!0) */, IDictionary_2_t101_il2cpp_TypeInfo_var, L_0, L_1);
		Assertion_Assert_m21(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Object_t* L_3 = (__this->___nameToIndexMapping_6);
		String_t* L_4 = ___prefabName;
		NullCheck(L_3);
		int32_t L_5 = (int32_t)InterfaceFuncInvoker1< int32_t, String_t* >::Invoke(1 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Int32>::get_Item(!0) */, IDictionary_2_t101_il2cpp_TypeInfo_var, L_3, L_4);
		V_0 = L_5;
		int32_t L_6 = V_0;
		GameObject_t155 * L_7 = PrefabManager_Request_m347(__this, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.GameObject PrefabManager::Request(System.String,UnityEngine.Vector3,UnityEngine.Quaternion)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern "C" GameObject_t155 * PrefabManager_Request_m346 (PrefabManager_t96 * __this, String_t* ___prefabName, Vector3_t36  ___position, Quaternion_t41  ___rotation, const MethodInfo* method)
{
	GameObject_t155 * V_0 = {0};
	{
		String_t* L_0 = ___prefabName;
		GameObject_t155 * L_1 = PrefabManager_Request_m345(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t155 * L_2 = V_0;
		NullCheck(L_2);
		Transform_t35 * L_3 = GameObject_get_transform_m1349(L_2, /*hidden argument*/NULL);
		Vector3_t36  L_4 = ___position;
		NullCheck(L_3);
		Transform_set_position_m1383(L_3, L_4, /*hidden argument*/NULL);
		GameObject_t155 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t35 * L_6 = GameObject_get_transform_m1349(L_5, /*hidden argument*/NULL);
		Quaternion_t41  L_7 = ___rotation;
		NullCheck(L_6);
		Transform_set_rotation_m1391(L_6, L_7, /*hidden argument*/NULL);
		GameObject_t155 * L_8 = V_0;
		return L_8;
	}
}
// UnityEngine.GameObject PrefabManager::Request(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern "C" GameObject_t155 * PrefabManager_Request_m347 (PrefabManager_t96 * __this, int32_t ___prefabIndex, const MethodInfo* method)
{
	SwarmItem_t124 * V_0 = {0};
	{
		SelfManagingSwarmItemManager_t99 * L_0 = (__this->___itemManager_2);
		int32_t L_1 = ___prefabIndex;
		NullCheck(L_0);
		SwarmItem_t124 * L_2 = (SwarmItem_t124 *)VirtFuncInvoker1< SwarmItem_t124 *, int32_t >::Invoke(6 /* SwarmItem SwarmItemManager::ActivateItem(System.Int32) */, L_0, L_1);
		V_0 = L_2;
		SwarmItem_t124 * L_3 = V_0;
		NullCheck(L_3);
		GameObject_t155 * L_4 = Component_get_gameObject_m1337(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void PrefabManager::Preload(System.String,System.Int32)
// SelfManagingSwarmItemManager
#include "AssemblyU2DCSharp_SelfManagingSwarmItemManagerMethodDeclarations.h"
extern TypeInfo* IDictionary_2_t101_il2cpp_TypeInfo_var;
extern "C" void PrefabManager_Preload_m348 (PrefabManager_t96 * __this, String_t* ___prefabName, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(106);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Object_t* L_0 = (__this->___nameToIndexMapping_6);
		String_t* L_1 = ___prefabName;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, String_t* >::Invoke(0 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Int32>::ContainsKey(!0) */, IDictionary_2_t101_il2cpp_TypeInfo_var, L_0, L_1);
		Assertion_Assert_m21(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Object_t* L_3 = (__this->___nameToIndexMapping_6);
		String_t* L_4 = ___prefabName;
		NullCheck(L_3);
		int32_t L_5 = (int32_t)InterfaceFuncInvoker1< int32_t, String_t* >::Invoke(1 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Int32>::get_Item(!0) */, IDictionary_2_t101_il2cpp_TypeInfo_var, L_3, L_4);
		V_0 = L_5;
		SelfManagingSwarmItemManager_t99 * L_6 = (__this->___itemManager_2);
		int32_t L_7 = V_0;
		int32_t L_8 = ___count;
		NullCheck(L_6);
		SelfManagingSwarmItemManager_Preload_m385(L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PrefabManager::KillAllActiveItems()
extern "C" void PrefabManager_KillAllActiveItems_m349 (PrefabManager_t96 * __this, const MethodInfo* method)
{
	{
		SelfManagingSwarmItemManager_t99 * L_0 = (__this->___itemManager_2);
		NullCheck(L_0);
		SelfManagingSwarmItemManager_KillAllActiveItems_m383(L_0, /*hidden argument*/NULL);
		return;
	}
}
// SelfManagingSwarmItemManager PrefabManager::get_ItemManager()
extern "C" SelfManagingSwarmItemManager_t99 * PrefabManager_get_ItemManager_m350 (PrefabManager_t96 * __this, const MethodInfo* method)
{
	{
		SelfManagingSwarmItemManager_t99 * L_0 = (__this->___itemManager_2);
		return L_0;
	}
}
// System.Void PrefabManager::SetPruneDataList(PrefabManager/PruneData[])
#include "Assembly-CSharp_ArrayTypes.h"
extern "C" void PrefabManager_SetPruneDataList_m351 (PrefabManager_t96 * __this, PruneDataU5BU5D_t98* ___pruneDataList, const MethodInfo* method)
{
	{
		PruneDataU5BU5D_t98* L_0 = ___pruneDataList;
		__this->___pruneDataList_3 = L_0;
		return;
	}
}
// System.Void PrefabManager::SetPreloadDataList(PrefabManager/PreloadData[])
extern "C" void PrefabManager_SetPreloadDataList_m352 (PrefabManager_t96 * __this, PreloadDataU5BU5D_t100* ___dataList, const MethodInfo* method)
{
	{
		PreloadDataU5BU5D_t100* L_0 = ___dataList;
		__this->___preloadDataList_4 = L_0;
		return;
	}
}
// System.Int32 PrefabManager::get_PrefabCount()
extern "C" int32_t PrefabManager_get_PrefabCount_m353 (PrefabManager_t96 * __this, const MethodInfo* method)
{
	{
		SelfManagingSwarmItemManager_t99 * L_0 = (__this->___itemManager_2);
		NullCheck(L_0);
		PrefabItemU5BU5D_t157* L_1 = (((SwarmItemManager_t111 *)L_0)->___itemPrefabs_10);
		NullCheck(L_1);
		return (((int32_t)(((Array_t *)L_1)->max_length)));
	}
}
// ProceduralTexturedQuad
#include "AssemblyU2DCSharp_ProceduralTexturedQuad.h"
// ProceduralTexturedQuad
#include "AssemblyU2DCSharp_ProceduralTexturedQuadMethodDeclarations.h"
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E.h"
// <PrivateImplementationDetails>/$ArrayType$24
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24Arra.h"
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeHelpers
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpersMethodDeclarations.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
struct MeshFilter_t398;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t398_m1502(__this, method) (( MeshFilter_t398 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
struct MeshRenderer_t402;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t402_m1507(__this, method) (( MeshRenderer_t402 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void ProceduralTexturedQuad::.ctor()
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern TypeInfo* Vector2_t2_il2cpp_TypeInfo_var;
extern "C" void ProceduralTexturedQuad__ctor_m354 (ProceduralTexturedQuad_t102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2  V_0 = {0};
	Vector2_t2  V_1 = {0};
	Vector2_t2  V_2 = {0};
	Vector2_t2  V_3 = {0};
	{
		Initobj (Vector2_t2_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2  L_0 = V_0;
		__this->___topLeftUv_2 = L_0;
		Initobj (Vector2_t2_il2cpp_TypeInfo_var, (&V_1));
		Vector2_t2  L_1 = V_1;
		__this->___topRightUv_3 = L_1;
		Initobj (Vector2_t2_il2cpp_TypeInfo_var, (&V_2));
		Vector2_t2  L_2 = V_2;
		__this->___bottomRightUv_4 = L_2;
		Initobj (Vector2_t2_il2cpp_TypeInfo_var, (&V_3));
		Vector2_t2  L_3 = V_3;
		__this->___bottomLeftUv_5 = L_3;
		Color_t9  L_4 = Color_get_white_m1499(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___modulationColor_7 = L_4;
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ProceduralTexturedQuad::Start()
// ProceduralTexturedQuad
#include "AssemblyU2DCSharp_ProceduralTexturedQuadMethodDeclarations.h"
extern "C" void ProceduralTexturedQuad_Start_m355 (ProceduralTexturedQuad_t102 * __this, const MethodInfo* method)
{
	{
		ProceduralTexturedQuad_GenerateMesh_m356(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ProceduralTexturedQuad::GenerateMesh()
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeHelpers
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpersMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
extern TypeInfo* Mesh_t104_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3U5BU5D_t254_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2U5BU5D_t262_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t401_il2cpp_TypeInfo_var;
extern TypeInfo* Material_t82_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t398_m1502_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t402_m1507_MethodInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t334____U24U24fieldU2D0_0_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral48;
extern Il2CppCodeGenString* _stringLiteral49;
extern "C" void ProceduralTexturedQuad_GenerateMesh_m356 (ProceduralTexturedQuad_t102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mesh_t104_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(100);
		Vector3U5BU5D_t254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(91);
		Vector2U5BU5D_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(99);
		Int32U5BU5D_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		Material_t82_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		Component_GetComponent_TisMeshFilter_t398_m1502_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483691);
		Component_GetComponent_TisMeshRenderer_t402_m1507_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483692);
		U3CPrivateImplementationDetailsU3E_t334____U24U24fieldU2D0_0_FieldInfo_var = il2cpp_codegen_field_info_from_index(110, 0);
		_stringLiteral48 = il2cpp_codegen_string_literal_from_index(48);
		_stringLiteral49 = il2cpp_codegen_string_literal_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	Vector3U5BU5D_t254* V_0 = {0};
	Vector2U5BU5D_t262* V_1 = {0};
	Int32U5BU5D_t401* V_2 = {0};
	MeshFilter_t398 * V_3 = {0};
	MeshRenderer_t402 * V_4 = {0};
	{
		Mesh_t104 * L_0 = (Mesh_t104 *)il2cpp_codegen_object_new (Mesh_t104_il2cpp_TypeInfo_var);
		Mesh__ctor_m1475(L_0, /*hidden argument*/NULL);
		__this->___mesh_10 = L_0;
		Vector3U5BU5D_t254* L_1 = ((Vector3U5BU5D_t254*)SZArrayNew(Vector3U5BU5D_t254_il2cpp_TypeInfo_var, 4));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		Vector3_t36  L_2 = {0};
		Vector3__ctor_m1500(&L_2, (-0.5f), (0.5f), (0.0f), /*hidden argument*/NULL);
		*((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_1, 0, sizeof(Vector3_t36 ))) = L_2;
		Vector3U5BU5D_t254* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		Vector3_t36  L_4 = {0};
		Vector3__ctor_m1500(&L_4, (0.5f), (0.5f), (0.0f), /*hidden argument*/NULL);
		*((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_3, 1, sizeof(Vector3_t36 ))) = L_4;
		Vector3U5BU5D_t254* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		Vector3_t36  L_6 = {0};
		Vector3__ctor_m1500(&L_6, (0.5f), (-0.5f), (0.0f), /*hidden argument*/NULL);
		*((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_5, 2, sizeof(Vector3_t36 ))) = L_6;
		Vector3U5BU5D_t254* L_7 = L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		Vector3_t36  L_8 = {0};
		Vector3__ctor_m1500(&L_8, (-0.5f), (-0.5f), (0.0f), /*hidden argument*/NULL);
		*((Vector3_t36 *)(Vector3_t36 *)SZArrayLdElema(L_7, 3, sizeof(Vector3_t36 ))) = L_8;
		V_0 = L_7;
		Mesh_t104 * L_9 = (__this->___mesh_10);
		Vector3U5BU5D_t254* L_10 = V_0;
		NullCheck(L_9);
		Mesh_set_vertices_m1477(L_9, L_10, /*hidden argument*/NULL);
		Vector2U5BU5D_t262* L_11 = ((Vector2U5BU5D_t262*)SZArrayNew(Vector2U5BU5D_t262_il2cpp_TypeInfo_var, 4));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		Vector2_t2  L_12 = (__this->___topLeftUv_2);
		*((Vector2_t2 *)(Vector2_t2 *)SZArrayLdElema(L_11, 0, sizeof(Vector2_t2 ))) = L_12;
		Vector2U5BU5D_t262* L_13 = L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		Vector2_t2  L_14 = (__this->___topRightUv_3);
		*((Vector2_t2 *)(Vector2_t2 *)SZArrayLdElema(L_13, 1, sizeof(Vector2_t2 ))) = L_14;
		Vector2U5BU5D_t262* L_15 = L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		Vector2_t2  L_16 = (__this->___bottomRightUv_4);
		*((Vector2_t2 *)(Vector2_t2 *)SZArrayLdElema(L_15, 2, sizeof(Vector2_t2 ))) = L_16;
		Vector2U5BU5D_t262* L_17 = L_15;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		Vector2_t2  L_18 = (__this->___bottomLeftUv_5);
		*((Vector2_t2 *)(Vector2_t2 *)SZArrayLdElema(L_17, 3, sizeof(Vector2_t2 ))) = L_18;
		V_1 = L_17;
		Mesh_t104 * L_19 = (__this->___mesh_10);
		Vector2U5BU5D_t262* L_20 = V_1;
		NullCheck(L_19);
		Mesh_set_uv_m1480(L_19, L_20, /*hidden argument*/NULL);
		Int32U5BU5D_t401* L_21 = ((Int32U5BU5D_t401*)SZArrayNew(Int32U5BU5D_t401_il2cpp_TypeInfo_var, 6));
		RuntimeHelpers_InitializeArray_m1501(NULL /*static, unused*/, (Array_t *)(Array_t *)L_21, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t334____U24U24fieldU2D0_0_FieldInfo_var), /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t104 * L_22 = (__this->___mesh_10);
		Int32U5BU5D_t401* L_23 = V_2;
		NullCheck(L_22);
		Mesh_set_triangles_m1481(L_22, L_23, /*hidden argument*/NULL);
		MeshFilter_t398 * L_24 = Component_GetComponent_TisMeshFilter_t398_m1502(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t398_m1502_MethodInfo_var);
		V_3 = L_24;
		MeshFilter_t398 * L_25 = V_3;
		Mesh_t104 * L_26 = (__this->___mesh_10);
		NullCheck(L_25);
		MeshFilter_set_mesh_m1503(L_25, L_26, /*hidden argument*/NULL);
		Material_t82 * L_27 = (__this->___material_9);
		bool L_28 = Object_op_Equality_m1413(NULL /*static, unused*/, L_27, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0166;
		}
	}
	{
		Shader_t407 * L_29 = Shader_Find_m1504(NULL /*static, unused*/, _stringLiteral48, /*hidden argument*/NULL);
		Material_t82 * L_30 = (Material_t82 *)il2cpp_codegen_object_new (Material_t82_il2cpp_TypeInfo_var);
		Material__ctor_m1505(L_30, L_29, /*hidden argument*/NULL);
		__this->___material_9 = L_30;
		Material_t82 * L_31 = (__this->___material_9);
		float L_32 = (__this->___alphaCutoff_8);
		NullCheck(L_31);
		Material_SetFloat_m1506(L_31, _stringLiteral49, L_32, /*hidden argument*/NULL);
	}

IL_0166:
	{
		MeshRenderer_t402 * L_33 = Component_GetComponent_TisMeshRenderer_t402_m1507(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t402_m1507_MethodInfo_var);
		V_4 = L_33;
		MeshRenderer_t402 * L_34 = V_4;
		Material_t82 * L_35 = (__this->___material_9);
		NullCheck(L_34);
		Renderer_set_material_m1487(L_34, L_35, /*hidden argument*/NULL);
		Texture_t103 * L_36 = (__this->___texture_6);
		bool L_37 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_36, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0198;
		}
	}
	{
		Texture_t103 * L_38 = (__this->___texture_6);
		VirtActionInvoker1< Texture_t103 * >::Invoke(4 /* System.Void ProceduralTexturedQuad::SetTexture(UnityEngine.Texture) */, __this, L_38);
	}

IL_0198:
	{
		Color_t9  L_39 = (__this->___modulationColor_7);
		ProceduralTexturedQuad_SetModulationColor_m359(__this, L_39, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ProceduralTexturedQuad::HasTexture()
extern "C" bool ProceduralTexturedQuad_HasTexture_m357 (ProceduralTexturedQuad_t102 * __this, const MethodInfo* method)
{
	{
		Texture_t103 * L_0 = (__this->___texture_6);
		bool L_1 = Object_op_Inequality_m1341(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ProceduralTexturedQuad::SetTexture(UnityEngine.Texture)
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern "C" void ProceduralTexturedQuad_SetTexture_m358 (ProceduralTexturedQuad_t102 * __this, Texture_t103 * ___texture, const MethodInfo* method)
{
	{
		Texture_t103 * L_0 = ___texture;
		Assertion_AssertNotNull_m26(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t103 * L_1 = ___texture;
		__this->___texture_6 = L_1;
		Material_t82 * L_2 = (__this->___material_9);
		Texture_t103 * L_3 = ___texture;
		NullCheck(L_2);
		Material_set_mainTexture_m1508(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ProceduralTexturedQuad::SetModulationColor(UnityEngine.Color)
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
extern "C" void ProceduralTexturedQuad_SetModulationColor_m359 (ProceduralTexturedQuad_t102 * __this, Color_t9  ___color, const MethodInfo* method)
{
	{
		Material_t82 * L_0 = (__this->___material_9);
		Color_t9  L_1 = ___color;
		NullCheck(L_0);
		Material_set_color_m1509(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// QuaternionUtils
#include "AssemblyU2DCSharp_QuaternionUtils.h"
// QuaternionUtils
#include "AssemblyU2DCSharp_QuaternionUtilsMethodDeclarations.h"
// System.Boolean QuaternionUtils::Equals(UnityEngine.Quaternion,UnityEngine.Quaternion)
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern TypeInfo* Quaternion_t41_il2cpp_TypeInfo_var;
extern "C" bool QuaternionUtils_Equals_m360 (Object_t * __this /* static, unused */, Quaternion_t41  ___a, Quaternion_t41  ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quaternion_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		s_Il2CppMethodIntialized = true;
	}
	{
		Quaternion_t41  L_0 = ___a;
		Quaternion_t41  L_1 = L_0;
		Object_t * L_2 = Box(Quaternion_t41_il2cpp_TypeInfo_var, &L_1);
		Quaternion_t41  L_3 = ___b;
		Quaternion_t41  L_4 = L_3;
		Object_t * L_5 = Box(Quaternion_t41_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Object_Equals_m1390(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Common.Query.ConcreteQueryRequest
#include "AssemblyU2DCSharp_Common_Query_ConcreteQueryRequest.h"
// Common.Query.ConcreteQueryRequest
#include "AssemblyU2DCSharp_Common_Query_ConcreteQueryRequestMethodDeclarations.h"
// System.Void Common.Query.ConcreteQueryRequest::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_0MethodDeclarations.h"
extern TypeInfo* Dictionary_2_t86_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1428_MethodInfo_var;
extern "C" void ConcreteQueryRequest__ctor_m361 (ConcreteQueryRequest_t106 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t86_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(57);
		Dictionary_2__ctor_m1428_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483670);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Dictionary_2_t86 * L_0 = (Dictionary_2_t86 *)il2cpp_codegen_object_new (Dictionary_2_t86_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1428(L_0, /*hidden argument*/Dictionary_2__ctor_m1428_MethodInfo_var);
		__this->___paramMap_1 = L_0;
		return;
	}
}
// System.Void Common.Query.ConcreteQueryRequest::Clear()
extern "C" void ConcreteQueryRequest_Clear_m362 (ConcreteQueryRequest_t106 * __this, const MethodInfo* method)
{
	{
		__this->___queryId_0 = (String_t*)NULL;
		Dictionary_2_t86 * L_0 = (__this->___paramMap_1);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Clear() */, L_0);
		return;
	}
}
// System.String Common.Query.ConcreteQueryRequest::get_QueryId()
extern "C" String_t* ConcreteQueryRequest_get_QueryId_m363 (ConcreteQueryRequest_t106 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___queryId_0);
		return L_0;
	}
}
// System.Void Common.Query.ConcreteQueryRequest::set_QueryId(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" void ConcreteQueryRequest_set_QueryId_m364 (ConcreteQueryRequest_t106 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___queryId_0 = L_0;
		return;
	}
}
// System.Void Common.Query.ConcreteQueryRequest::AddParameter(System.String,System.Object)
// System.Object
#include "mscorlib_System_Object.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern "C" void ConcreteQueryRequest_AddParameter_m365 (ConcreteQueryRequest_t106 * __this, String_t* ___paramId, Object_t * ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t86 * L_0 = (__this->___paramMap_1);
		String_t* L_1 = ___paramId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		Assertion_Assert_m21(NULL /*static, unused*/, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Dictionary_2_t86 * L_3 = (__this->___paramMap_1);
		String_t* L_4 = ___paramId;
		Object_t * L_5 = ___value;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_3, L_4, L_5);
		return;
	}
}
// System.Object Common.Query.ConcreteQueryRequest::GetParameter(System.String)
extern "C" Object_t * ConcreteQueryRequest_GetParameter_m366 (ConcreteQueryRequest_t106 * __this, String_t* ___paramId, const MethodInfo* method)
{
	{
		Dictionary_2_t86 * L_0 = (__this->___paramMap_1);
		String_t* L_1 = ___paramId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		Assertion_Assert_m21(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Dictionary_2_t86 * L_3 = (__this->___paramMap_1);
		String_t* L_4 = ___paramId;
		NullCheck(L_3);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, String_t* >::Invoke(19 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_3, L_4);
		return L_5;
	}
}
// Common.Query.ConcreteQueryResult
#include "AssemblyU2DCSharp_Common_Query_ConcreteQueryResult.h"
// Common.Query.ConcreteQueryResult
#include "AssemblyU2DCSharp_Common_Query_ConcreteQueryResultMethodDeclarations.h"
// System.Void Common.Query.ConcreteQueryResult::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void ConcreteQueryResult__ctor_m367 (ConcreteQueryResult_t107 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Query.ConcreteQueryResult::Clear()
extern "C" void ConcreteQueryResult_Clear_m368 (ConcreteQueryResult_t107 * __this, const MethodInfo* method)
{
	{
		__this->___result_0 = NULL;
		return;
	}
}
// System.Void Common.Query.ConcreteQueryResult::Set(System.Object)
// System.Object
#include "mscorlib_System_Object.h"
extern "C" void ConcreteQueryResult_Set_m369 (ConcreteQueryResult_t107 * __this, Object_t * ___result, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___result;
		__this->___result_0 = L_0;
		return;
	}
}
// Common.Query.QuerySystem
#include "AssemblyU2DCSharp_Common_Query_QuerySystem.h"
// Common.Query.QuerySystem
#include "AssemblyU2DCSharp_Common_Query_QuerySystemMethodDeclarations.h"
// Common.Query.QuerySystemImplementation
#include "AssemblyU2DCSharp_Common_Query_QuerySystemImplementation.h"
// Common.Query.QueryResultResolver
#include "AssemblyU2DCSharp_Common_Query_QueryResultResolver.h"
// Common.Query.QuerySystemImplementation
#include "AssemblyU2DCSharp_Common_Query_QuerySystemImplementationMethodDeclarations.h"
// System.Void Common.Query.QuerySystem::.cctor()
// Common.Query.QuerySystemImplementation
#include "AssemblyU2DCSharp_Common_Query_QuerySystemImplementationMethodDeclarations.h"
extern TypeInfo* QuerySystemImplementation_t109_il2cpp_TypeInfo_var;
extern TypeInfo* QuerySystem_t108_il2cpp_TypeInfo_var;
extern "C" void QuerySystem__cctor_m370 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuerySystemImplementation_t109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		QuerySystem_t108_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	{
		QuerySystemImplementation_t109 * L_0 = (QuerySystemImplementation_t109 *)il2cpp_codegen_object_new (QuerySystemImplementation_t109_il2cpp_TypeInfo_var);
		QuerySystemImplementation__ctor_m375(L_0, /*hidden argument*/NULL);
		((QuerySystem_t108_StaticFields*)QuerySystem_t108_il2cpp_TypeInfo_var->static_fields)->___systemInstance_0 = L_0;
		return;
	}
}
// System.Void Common.Query.QuerySystem::RegisterResolver(System.String,Common.Query.QueryResultResolver)
// System.String
#include "mscorlib_System_String.h"
// Common.Query.QueryResultResolver
#include "AssemblyU2DCSharp_Common_Query_QueryResultResolver.h"
extern TypeInfo* QuerySystem_t108_il2cpp_TypeInfo_var;
extern "C" void QuerySystem_RegisterResolver_m371 (Object_t * __this /* static, unused */, String_t* ___queryId, QueryResultResolver_t328 * ___resolver, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuerySystem_t108_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QuerySystem_t108_il2cpp_TypeInfo_var);
		QuerySystemImplementation_t109 * L_0 = ((QuerySystem_t108_StaticFields*)QuerySystem_t108_il2cpp_TypeInfo_var->static_fields)->___systemInstance_0;
		String_t* L_1 = ___queryId;
		QueryResultResolver_t328 * L_2 = ___resolver;
		NullCheck(L_0);
		QuerySystemImplementation_RegisterResolver_m376(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Query.QuerySystem::RemoveResolver(System.String)
extern TypeInfo* QuerySystem_t108_il2cpp_TypeInfo_var;
extern "C" void QuerySystem_RemoveResolver_m372 (Object_t * __this /* static, unused */, String_t* ___queryId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuerySystem_t108_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QuerySystem_t108_il2cpp_TypeInfo_var);
		QuerySystemImplementation_t109 * L_0 = ((QuerySystem_t108_StaticFields*)QuerySystem_t108_il2cpp_TypeInfo_var->static_fields)->___systemInstance_0;
		String_t* L_1 = ___queryId;
		NullCheck(L_0);
		QuerySystemImplementation_RemoveResolver_m377(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Common.Query.IQueryRequest Common.Query.QuerySystem::Start(System.String)
extern TypeInfo* QuerySystem_t108_il2cpp_TypeInfo_var;
extern "C" Object_t * QuerySystem_Start_m373 (Object_t * __this /* static, unused */, String_t* ___queryId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuerySystem_t108_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QuerySystem_t108_il2cpp_TypeInfo_var);
		QuerySystemImplementation_t109 * L_0 = ((QuerySystem_t108_StaticFields*)QuerySystem_t108_il2cpp_TypeInfo_var->static_fields)->___systemInstance_0;
		String_t* L_1 = ___queryId;
		NullCheck(L_0);
		Object_t * L_2 = QuerySystemImplementation_Start_m378(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Common.Query.QuerySystem::HasResolver(System.String)
extern TypeInfo* QuerySystem_t108_il2cpp_TypeInfo_var;
extern "C" bool QuerySystem_HasResolver_m374 (Object_t * __this /* static, unused */, String_t* ___queryId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuerySystem_t108_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QuerySystem_t108_il2cpp_TypeInfo_var);
		QuerySystemImplementation_t109 * L_0 = ((QuerySystem_t108_StaticFields*)QuerySystem_t108_il2cpp_TypeInfo_var->static_fields)->___systemInstance_0;
		String_t* L_1 = ___queryId;
		NullCheck(L_0);
		bool L_2 = QuerySystemImplementation_HasResolver_m379(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_2.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_2MethodDeclarations.h"
// System.Void Common.Query.QuerySystemImplementation::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_2MethodDeclarations.h"
// Common.Query.ConcreteQueryResult
#include "AssemblyU2DCSharp_Common_Query_ConcreteQueryResultMethodDeclarations.h"
// Common.Query.ConcreteQueryRequest
#include "AssemblyU2DCSharp_Common_Query_ConcreteQueryRequestMethodDeclarations.h"
extern TypeInfo* Dictionary_2_t110_il2cpp_TypeInfo_var;
extern TypeInfo* ConcreteQueryResult_t107_il2cpp_TypeInfo_var;
extern TypeInfo* ConcreteQueryRequest_t106_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1510_MethodInfo_var;
extern "C" void QuerySystemImplementation__ctor_m375 (QuerySystemImplementation_t109 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(114);
		ConcreteQueryResult_t107_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		ConcreteQueryRequest_t106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Dictionary_2__ctor_m1510_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483693);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Dictionary_2_t110 * L_0 = (Dictionary_2_t110 *)il2cpp_codegen_object_new (Dictionary_2_t110_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1510(L_0, /*hidden argument*/Dictionary_2__ctor_m1510_MethodInfo_var);
		__this->___resolverMap_0 = L_0;
		ConcreteQueryResult_t107 * L_1 = (ConcreteQueryResult_t107 *)il2cpp_codegen_object_new (ConcreteQueryResult_t107_il2cpp_TypeInfo_var);
		ConcreteQueryResult__ctor_m367(L_1, /*hidden argument*/NULL);
		__this->___result_1 = L_1;
		ConcreteQueryRequest_t106 * L_2 = (ConcreteQueryRequest_t106 *)il2cpp_codegen_object_new (ConcreteQueryRequest_t106_il2cpp_TypeInfo_var);
		ConcreteQueryRequest__ctor_m361(L_2, /*hidden argument*/NULL);
		__this->___request_2 = L_2;
		return;
	}
}
// System.Void Common.Query.QuerySystemImplementation::RegisterResolver(System.String,Common.Query.QueryResultResolver)
// System.String
#include "mscorlib_System_String.h"
// Common.Query.QueryResultResolver
#include "AssemblyU2DCSharp_Common_Query_QueryResultResolver.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern "C" void QuerySystemImplementation_RegisterResolver_m376 (QuerySystemImplementation_t109 * __this, String_t* ___queryId, QueryResultResolver_t328 * ___resolver, const MethodInfo* method)
{
	{
		Dictionary_2_t110 * L_0 = (__this->___resolverMap_0);
		String_t* L_1 = ___queryId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::ContainsKey(!0) */, L_0, L_1);
		Assertion_Assert_m21(NULL /*static, unused*/, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Dictionary_2_t110 * L_3 = (__this->___resolverMap_0);
		String_t* L_4 = ___queryId;
		QueryResultResolver_t328 * L_5 = ___resolver;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, QueryResultResolver_t328 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::set_Item(!0,!1) */, L_3, L_4, L_5);
		return;
	}
}
// System.Void Common.Query.QuerySystemImplementation::RemoveResolver(System.String)
extern "C" void QuerySystemImplementation_RemoveResolver_m377 (QuerySystemImplementation_t109 * __this, String_t* ___queryId, const MethodInfo* method)
{
	{
		Dictionary_2_t110 * L_0 = (__this->___resolverMap_0);
		String_t* L_1 = ___queryId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::ContainsKey(!0) */, L_0, L_1);
		Assertion_Assert_m21(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Dictionary_2_t110 * L_3 = (__this->___resolverMap_0);
		String_t* L_4 = ___queryId;
		NullCheck(L_3);
		VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::Remove(!0) */, L_3, L_4);
		return;
	}
}
// Common.Query.IQueryRequest Common.Query.QuerySystemImplementation::Start(System.String)
extern "C" Object_t * QuerySystemImplementation_Start_m378 (QuerySystemImplementation_t109 * __this, String_t* ___queryId, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___locked_3);
		Assertion_Assert_m21(NULL /*static, unused*/, ((((int32_t)L_0) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Dictionary_2_t110 * L_1 = (__this->___resolverMap_0);
		String_t* L_2 = ___queryId;
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::ContainsKey(!0) */, L_1, L_2);
		Assertion_Assert_m21(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ConcreteQueryRequest_t106 * L_4 = (__this->___request_2);
		String_t* L_5 = ___queryId;
		NullCheck(L_4);
		ConcreteQueryRequest_set_QueryId_m364(L_4, L_5, /*hidden argument*/NULL);
		__this->___locked_3 = 1;
		ConcreteQueryRequest_t106 * L_6 = (__this->___request_2);
		return L_6;
	}
}
// System.Boolean Common.Query.QuerySystemImplementation::HasResolver(System.String)
extern "C" bool QuerySystemImplementation_HasResolver_m379 (QuerySystemImplementation_t109 * __this, String_t* ___queryId, const MethodInfo* method)
{
	{
		Dictionary_2_t110 * L_0 = (__this->___resolverMap_0);
		String_t* L_1 = ___queryId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Collections.Generic.LinkedListNode`1<SwarmItem>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_0.h"
// SwarmItemManager/PrefabItemLists
#include "AssemblyU2DCSharp_SwarmItemManager_PrefabItemLists.h"
// System.Collections.Generic.LinkedList`1<SwarmItem>
#include "System_System_Collections_Generic_LinkedList_1_gen_0.h"
// System.Collections.Generic.Stack`1<SwarmItem>
#include "System_System_Collections_Generic_Stack_1_gen_0.h"
// Common.Utils.SimpleList`1<SwarmItem>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_gen.h"
// System.Collections.Generic.LinkedList`1<SwarmItem>
#include "System_System_Collections_Generic_LinkedList_1_gen_0MethodDeclarations.h"
// System.Collections.Generic.LinkedListNode`1<SwarmItem>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_0MethodDeclarations.h"
// System.Collections.Generic.Stack`1<SwarmItem>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
// Common.Utils.SimpleList`1<SwarmItem>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_genMethodDeclarations.h"
// SwarmItem
#include "AssemblyU2DCSharp_SwarmItemMethodDeclarations.h"
// System.Void SelfManagingSwarmItemManager::.ctor()
// SwarmItemManager
#include "AssemblyU2DCSharp_SwarmItemManagerMethodDeclarations.h"
extern "C" void SelfManagingSwarmItemManager__ctor_m380 (SelfManagingSwarmItemManager_t99 * __this, const MethodInfo* method)
{
	{
		SwarmItemManager__ctor_m514(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SelfManagingSwarmItemManager::Awake()
extern "C" void SelfManagingSwarmItemManager_Awake_m381 (SelfManagingSwarmItemManager_t99 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(4 /* System.Void SwarmItemManager::Initialize() */, __this);
		return;
	}
}
// System.Void SelfManagingSwarmItemManager::Update()
extern "C" void SelfManagingSwarmItemManager_Update_m382 (SelfManagingSwarmItemManager_t99 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(9 /* System.Void SwarmItemManager::FrameUpdate() */, __this);
		return;
	}
}
// System.Void SelfManagingSwarmItemManager::KillAllActiveItems()
// System.Collections.Generic.LinkedList`1<SwarmItem>
#include "System_System_Collections_Generic_LinkedList_1_gen_0MethodDeclarations.h"
// System.Collections.Generic.LinkedListNode`1<SwarmItem>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_0MethodDeclarations.h"
extern const MethodInfo* LinkedList_1_get_First_m1511_MethodInfo_var;
extern const MethodInfo* LinkedListNode_1_get_Next_m1512_MethodInfo_var;
extern const MethodInfo* LinkedListNode_1_get_Value_m1513_MethodInfo_var;
extern "C" void SelfManagingSwarmItemManager_KillAllActiveItems_m383 (SelfManagingSwarmItemManager_t99 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LinkedList_1_get_First_m1511_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483694);
		LinkedListNode_1_get_Next_m1512_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483695);
		LinkedListNode_1_get_Value_m1513_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483696);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LinkedListNode_1_t408 * V_2 = {0};
	LinkedListNode_1_t408 * V_3 = {0};
	{
		PrefabItemListsU5BU5D_t156* L_0 = (((SwarmItemManager_t111 *)__this)->____prefabItemLists_8);
		NullCheck(L_0);
		V_0 = (((int32_t)(((Array_t *)L_0)->max_length)));
		V_1 = 0;
		goto IL_0068;
	}

IL_0010:
	{
		PrefabItemListsU5BU5D_t156* L_1 = (((SwarmItemManager_t111 *)__this)->____prefabItemLists_8);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_1, L_3, sizeof(PrefabItemLists_t151 *))));
		LinkedList_1_t152 * L_4 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_1, L_3, sizeof(PrefabItemLists_t151 *)))->___activeItems_0);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 System.Collections.Generic.LinkedList`1<SwarmItem>::get_Count() */, L_4);
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		goto IL_0064;
	}

IL_002d:
	{
		V_2 = (LinkedListNode_1_t408 *)NULL;
		V_3 = (LinkedListNode_1_t408 *)NULL;
		PrefabItemListsU5BU5D_t156* L_6 = (((SwarmItemManager_t111 *)__this)->____prefabItemLists_8);
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_6, L_8, sizeof(PrefabItemLists_t151 *))));
		LinkedList_1_t152 * L_9 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_6, L_8, sizeof(PrefabItemLists_t151 *)))->___activeItems_0);
		NullCheck(L_9);
		LinkedListNode_1_t408 * L_10 = LinkedList_1_get_First_m1511(L_9, /*hidden argument*/LinkedList_1_get_First_m1511_MethodInfo_var);
		V_2 = L_10;
		goto IL_005e;
	}

IL_0049:
	{
		LinkedListNode_1_t408 * L_11 = V_2;
		NullCheck(L_11);
		LinkedListNode_1_t408 * L_12 = LinkedListNode_1_get_Next_m1512(L_11, /*hidden argument*/LinkedListNode_1_get_Next_m1512_MethodInfo_var);
		V_3 = L_12;
		LinkedListNode_1_t408 * L_13 = V_2;
		NullCheck(L_13);
		SwarmItem_t124 * L_14 = LinkedListNode_1_get_Value_m1513(L_13, /*hidden argument*/LinkedListNode_1_get_Value_m1513_MethodInfo_var);
		VirtActionInvoker1< SwarmItem_t124 * >::Invoke(7 /* System.Void SwarmItemManager::DeactiveItem(SwarmItem) */, __this, L_14);
		LinkedListNode_1_t408 * L_15 = V_3;
		V_2 = L_15;
	}

IL_005e:
	{
		LinkedListNode_1_t408 * L_16 = V_2;
		if (L_16)
		{
			goto IL_0049;
		}
	}

IL_0064:
	{
		int32_t L_17 = V_1;
		V_1 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_18 = V_1;
		int32_t L_19 = V_0;
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
// System.Void SelfManagingSwarmItemManager::PruneInactiveList(System.Int32,System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern Il2CppCodeGenString* _stringLiteral50;
extern "C" void SelfManagingSwarmItemManager_PruneInactiveList_m384 (SelfManagingSwarmItemManager_t99 * __this, int32_t ___itemPrefabIndex, int32_t ___maxCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral50 = il2cpp_codegen_string_literal_from_index(50);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___maxCount;
		Assertion_Assert_m22(NULL /*static, unused*/, ((((int32_t)L_0) > ((int32_t)0))? 1 : 0), _stringLiteral50, /*hidden argument*/NULL);
		PrefabItemListsU5BU5D_t156* L_1 = (((SwarmItemManager_t111 *)__this)->____prefabItemLists_8);
		int32_t L_2 = ___itemPrefabIndex;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_1, L_3, sizeof(PrefabItemLists_t151 *))));
		Stack_1_t153 * L_4 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_1, L_3, sizeof(PrefabItemLists_t151 *)))->___inactiveItems_1);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<SwarmItem>::get_Count() */, L_4);
		int32_t L_6 = ___maxCount;
		V_0 = ((int32_t)((int32_t)L_5-(int32_t)L_6));
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		int32_t L_8 = ___itemPrefabIndex;
		int32_t L_9 = V_0;
		SwarmItemManager_PruneList_m523(__this, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SelfManagingSwarmItemManager::Preload(System.Int32,System.Int32)
// Common.Utils.SimpleList`1<SwarmItem>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_genMethodDeclarations.h"
extern TypeInfo* SimpleList_1_t112_il2cpp_TypeInfo_var;
extern const MethodInfo* SimpleList_1__ctor_m1514_MethodInfo_var;
extern const MethodInfo* SimpleList_1_Clear_m1515_MethodInfo_var;
extern const MethodInfo* SimpleList_1_Add_m1516_MethodInfo_var;
extern const MethodInfo* SimpleList_1_get_Item_m1517_MethodInfo_var;
extern const MethodInfo* SimpleList_1_get_Count_m1518_MethodInfo_var;
extern "C" void SelfManagingSwarmItemManager_Preload_m385 (SelfManagingSwarmItemManager_t99 * __this, int32_t ___itemPrefabIndex, int32_t ___preloadCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleList_1_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		SimpleList_1__ctor_m1514_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483697);
		SimpleList_1_Clear_m1515_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483698);
		SimpleList_1_Add_m1516_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483699);
		SimpleList_1_get_Item_m1517_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		SimpleList_1_get_Count_m1518_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483701);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	SwarmItem_t124 * V_2 = {0};
	int32_t V_3 = 0;
	{
		PrefabItemListsU5BU5D_t156* L_0 = (((SwarmItemManager_t111 *)__this)->____prefabItemLists_8);
		int32_t L_1 = ___itemPrefabIndex;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_0, L_2, sizeof(PrefabItemLists_t151 *))));
		Stack_1_t153 * L_3 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_0, L_2, sizeof(PrefabItemLists_t151 *)))->___inactiveItems_1);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<SwarmItem>::get_Count() */, L_3);
		int32_t L_5 = ___preloadCount;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		SimpleList_1_t112 * L_6 = (__this->___preloadList_11);
		if (L_6)
		{
			goto IL_002f;
		}
	}
	{
		SimpleList_1_t112 * L_7 = (SimpleList_1_t112 *)il2cpp_codegen_object_new (SimpleList_1_t112_il2cpp_TypeInfo_var);
		SimpleList_1__ctor_m1514(L_7, /*hidden argument*/SimpleList_1__ctor_m1514_MethodInfo_var);
		__this->___preloadList_11 = L_7;
	}

IL_002f:
	{
		SimpleList_1_t112 * L_8 = (__this->___preloadList_11);
		NullCheck(L_8);
		SimpleList_1_Clear_m1515(L_8, /*hidden argument*/SimpleList_1_Clear_m1515_MethodInfo_var);
		int32_t L_9 = ___preloadCount;
		PrefabItemListsU5BU5D_t156* L_10 = (((SwarmItemManager_t111 *)__this)->____prefabItemLists_8);
		int32_t L_11 = ___itemPrefabIndex;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_10, L_12, sizeof(PrefabItemLists_t151 *))));
		Stack_1_t153 * L_13 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_10, L_12, sizeof(PrefabItemLists_t151 *)))->___inactiveItems_1);
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<SwarmItem>::get_Count() */, L_13);
		V_0 = ((int32_t)((int32_t)L_9-(int32_t)L_14));
		V_1 = 0;
		goto IL_006e;
	}

IL_0056:
	{
		int32_t L_15 = ___itemPrefabIndex;
		SwarmItem_t124 * L_16 = (SwarmItem_t124 *)VirtFuncInvoker1< SwarmItem_t124 *, int32_t >::Invoke(6 /* SwarmItem SwarmItemManager::ActivateItem(System.Int32) */, __this, L_15);
		V_2 = L_16;
		SimpleList_1_t112 * L_17 = (__this->___preloadList_11);
		SwarmItem_t124 * L_18 = V_2;
		NullCheck(L_17);
		SimpleList_1_Add_m1516(L_17, L_18, /*hidden argument*/SimpleList_1_Add_m1516_MethodInfo_var);
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_006e:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = V_0;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_0056;
		}
	}
	{
		V_3 = 0;
		goto IL_0091;
	}

IL_007c:
	{
		SimpleList_1_t112 * L_22 = (__this->___preloadList_11);
		int32_t L_23 = V_3;
		NullCheck(L_22);
		SwarmItem_t124 * L_24 = SimpleList_1_get_Item_m1517(L_22, L_23, /*hidden argument*/SimpleList_1_get_Item_m1517_MethodInfo_var);
		NullCheck(L_24);
		VirtActionInvoker0::Invoke(7 /* System.Void SwarmItem::Kill() */, L_24);
		int32_t L_25 = V_3;
		V_3 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0091:
	{
		int32_t L_26 = V_3;
		SimpleList_1_t112 * L_27 = (__this->___preloadList_11);
		NullCheck(L_27);
		int32_t L_28 = SimpleList_1_get_Count_m1518(L_27, /*hidden argument*/SimpleList_1_get_Count_m1518_MethodInfo_var);
		if ((((int32_t)L_26) < ((int32_t)L_28)))
		{
			goto IL_007c;
		}
	}
	{
		SimpleList_1_t112 * L_29 = (__this->___preloadList_11);
		NullCheck(L_29);
		SimpleList_1_Clear_m1515(L_29, /*hidden argument*/SimpleList_1_Clear_m1515_MethodInfo_var);
		return;
	}
}
// Common.Signal.ConcreteSignalParameters
#include "AssemblyU2DCSharp_Common_Signal_ConcreteSignalParameters.h"
// Common.Signal.ConcreteSignalParameters
#include "AssemblyU2DCSharp_Common_Signal_ConcreteSignalParametersMethodDeclarations.h"
// System.Void Common.Signal.ConcreteSignalParameters::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_0MethodDeclarations.h"
extern TypeInfo* Dictionary_2_t86_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1428_MethodInfo_var;
extern "C" void ConcreteSignalParameters__ctor_m386 (ConcreteSignalParameters_t113 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t86_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(57);
		Dictionary_2__ctor_m1428_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483670);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Dictionary_2_t86 * L_0 = (Dictionary_2_t86 *)il2cpp_codegen_object_new (Dictionary_2_t86_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1428(L_0, /*hidden argument*/Dictionary_2__ctor_m1428_MethodInfo_var);
		__this->___parameterMap_0 = L_0;
		return;
	}
}
// System.Void Common.Signal.ConcreteSignalParameters::AddParameter(System.String,System.Object)
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_Object.h"
extern "C" void ConcreteSignalParameters_AddParameter_m387 (ConcreteSignalParameters_t113 * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t86 * L_0 = (__this->___parameterMap_0);
		String_t* L_1 = ___key;
		Object_t * L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Object Common.Signal.ConcreteSignalParameters::GetParameter(System.String)
extern "C" Object_t * ConcreteSignalParameters_GetParameter_m388 (ConcreteSignalParameters_t113 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		Dictionary_2_t86 * L_0 = (__this->___parameterMap_0);
		String_t* L_1 = ___key;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, String_t* >::Invoke(19 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Common.Signal.ConcreteSignalParameters::HasParameter(System.String)
extern "C" bool ConcreteSignalParameters_HasParameter_m389 (ConcreteSignalParameters_t113 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		Dictionary_2_t86 * L_0 = (__this->___parameterMap_0);
		String_t* L_1 = ___key;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void Common.Signal.ConcreteSignalParameters::Clear()
extern "C" void ConcreteSignalParameters_Clear_m390 (ConcreteSignalParameters_t113 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t86 * L_0 = (__this->___parameterMap_0);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Clear() */, L_0);
		return;
	}
}
// Common.Signal.Signal/SignalListener
#include "AssemblyU2DCSharp_Common_Signal_Signal_SignalListener.h"
// Common.Signal.Signal/SignalListener
#include "AssemblyU2DCSharp_Common_Signal_Signal_SignalListenerMethodDeclarations.h"
// System.Void Common.Signal.Signal/SignalListener::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void SignalListener__ctor_m391 (SignalListener_t114 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void Common.Signal.Signal/SignalListener::Invoke(Common.Signal.ISignalParameters)
extern "C" void SignalListener_Invoke_m392 (SignalListener_t114 * __this, Object_t * ___parameters, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SignalListener_Invoke_m392((SignalListener_t114 *)__this->___prev_9,___parameters, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___parameters, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___parameters,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___parameters, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___parameters,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___parameters,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SignalListener_t114(Il2CppObject* delegate, Object_t * ___parameters)
{
	// Marshaling of parameter '___parameters' to native representation
	Object_t * ____parameters_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'Common.Signal.ISignalParameters'."));
}
// System.IAsyncResult Common.Signal.Signal/SignalListener::BeginInvoke(Common.Signal.ISignalParameters,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * SignalListener_BeginInvoke_m393 (SignalListener_t114 * __this, Object_t * ___parameters, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___parameters;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void Common.Signal.Signal/SignalListener::EndInvoke(System.IAsyncResult)
extern "C" void SignalListener_EndInvoke_m394 (SignalListener_t114 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Common.Signal.Signal
#include "AssemblyU2DCSharp_Common_Signal_Signal.h"
// Common.Signal.Signal
#include "AssemblyU2DCSharp_Common_Signal_SignalMethodDeclarations.h"
// Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_gen_0.h"
// Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_gen_0MethodDeclarations.h"
// System.Void Common.Signal.Signal::.ctor(System.String)
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_gen_0MethodDeclarations.h"
extern TypeInfo* SimpleList_1_t117_il2cpp_TypeInfo_var;
extern const MethodInfo* SimpleList_1__ctor_m1519_MethodInfo_var;
extern "C" void Signal__ctor_m395 (Signal_t116 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleList_1_t117_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(120);
		SimpleList_1__ctor_m1519_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483702);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___name_0 = L_0;
		SimpleList_1_t117 * L_1 = (SimpleList_1_t117 *)il2cpp_codegen_object_new (SimpleList_1_t117_il2cpp_TypeInfo_var);
		SimpleList_1__ctor_m1519(L_1, /*hidden argument*/SimpleList_1__ctor_m1519_MethodInfo_var);
		__this->___listenerList_2 = L_1;
		return;
	}
}
// System.Void Common.Signal.Signal::ClearParameters()
extern "C" void Signal_ClearParameters_m396 (Signal_t116 * __this, const MethodInfo* method)
{
	{
		ConcreteSignalParameters_t113 * L_0 = (__this->___parameters_1);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		ConcreteSignalParameters_t113 * L_1 = (__this->___parameters_1);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(7 /* System.Void Common.Signal.ConcreteSignalParameters::Clear() */, L_1);
		return;
	}
}
// System.Void Common.Signal.Signal::AddParameter(System.String,System.Object)
// System.Object
#include "mscorlib_System_Object.h"
// Common.Signal.ConcreteSignalParameters
#include "AssemblyU2DCSharp_Common_Signal_ConcreteSignalParametersMethodDeclarations.h"
extern TypeInfo* ConcreteSignalParameters_t113_il2cpp_TypeInfo_var;
extern "C" void Signal_AddParameter_m397 (Signal_t116 * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConcreteSignalParameters_t113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(121);
		s_Il2CppMethodIntialized = true;
	}
	{
		ConcreteSignalParameters_t113 * L_0 = (__this->___parameters_1);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ConcreteSignalParameters_t113 * L_1 = (ConcreteSignalParameters_t113 *)il2cpp_codegen_object_new (ConcreteSignalParameters_t113_il2cpp_TypeInfo_var);
		ConcreteSignalParameters__ctor_m386(L_1, /*hidden argument*/NULL);
		__this->___parameters_1 = L_1;
	}

IL_0016:
	{
		ConcreteSignalParameters_t113 * L_2 = (__this->___parameters_1);
		String_t* L_3 = ___key;
		Object_t * L_4 = ___value;
		NullCheck(L_2);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(4 /* System.Void Common.Signal.ConcreteSignalParameters::AddParameter(System.String,System.Object) */, L_2, L_3, L_4);
		return;
	}
}
// System.Void Common.Signal.Signal::AddListener(Common.Signal.Signal/SignalListener)
// Common.Signal.Signal/SignalListener
#include "AssemblyU2DCSharp_Common_Signal_Signal_SignalListener.h"
extern const MethodInfo* SimpleList_1_Add_m1520_MethodInfo_var;
extern "C" void Signal_AddListener_m398 (Signal_t116 * __this, SignalListener_t114 * ___listener, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleList_1_Add_m1520_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483703);
		s_Il2CppMethodIntialized = true;
	}
	{
		SimpleList_1_t117 * L_0 = (__this->___listenerList_2);
		SignalListener_t114 * L_1 = ___listener;
		NullCheck(L_0);
		SimpleList_1_Add_m1520(L_0, L_1, /*hidden argument*/SimpleList_1_Add_m1520_MethodInfo_var);
		return;
	}
}
// System.Void Common.Signal.Signal::RemoveListener(Common.Signal.Signal/SignalListener)
extern const MethodInfo* SimpleList_1_Remove_m1521_MethodInfo_var;
extern "C" void Signal_RemoveListener_m399 (Signal_t116 * __this, SignalListener_t114 * ___listener, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleList_1_Remove_m1521_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483704);
		s_Il2CppMethodIntialized = true;
	}
	{
		SimpleList_1_t117 * L_0 = (__this->___listenerList_2);
		SignalListener_t114 * L_1 = ___listener;
		NullCheck(L_0);
		SimpleList_1_Remove_m1521(L_0, L_1, /*hidden argument*/SimpleList_1_Remove_m1521_MethodInfo_var);
		return;
	}
}
// System.Boolean Common.Signal.Signal::HasListener()
extern const MethodInfo* SimpleList_1_get_Count_m1522_MethodInfo_var;
extern "C" bool Signal_HasListener_m400 (Signal_t116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleList_1_get_Count_m1522_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483705);
		s_Il2CppMethodIntialized = true;
	}
	{
		SimpleList_1_t117 * L_0 = (__this->___listenerList_2);
		NullCheck(L_0);
		int32_t L_1 = SimpleList_1_get_Count_m1522(L_0, /*hidden argument*/SimpleList_1_get_Count_m1522_MethodInfo_var);
		return ((((int32_t)L_1) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Common.Signal.Signal::HasListener(Common.Signal.Signal/SignalListener)
extern const MethodInfo* SimpleList_1_Contains_m1523_MethodInfo_var;
extern "C" bool Signal_HasListener_m401 (Signal_t116 * __this, SignalListener_t114 * ___listener, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleList_1_Contains_m1523_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	{
		SimpleList_1_t117 * L_0 = (__this->___listenerList_2);
		SignalListener_t114 * L_1 = ___listener;
		NullCheck(L_0);
		bool L_2 = SimpleList_1_Contains_m1523(L_0, L_1, /*hidden argument*/SimpleList_1_Contains_m1523_MethodInfo_var);
		return L_2;
	}
}
// System.Void Common.Signal.Signal::Dispatch()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// Common.Signal.Signal/SignalListener
#include "AssemblyU2DCSharp_Common_Signal_Signal_SignalListenerMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* SimpleList_1_get_Count_m1522_MethodInfo_var;
extern const MethodInfo* SimpleList_1_get_Item_m1524_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral51;
extern "C" void Signal_Dispatch_m402 (Signal_t116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		SimpleList_1_get_Count_m1522_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483705);
		SimpleList_1_get_Item_m1524_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483707);
		_stringLiteral51 = il2cpp_codegen_string_literal_from_index(51);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		SimpleList_1_t117 * L_0 = (__this->___listenerList_2);
		NullCheck(L_0);
		int32_t L_1 = SimpleList_1_get_Count_m1522(L_0, /*hidden argument*/SimpleList_1_get_Count_m1522_MethodInfo_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_2 = (__this->___name_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1319(NULL /*static, unused*/, _stringLiteral51, L_2, /*hidden argument*/NULL);
		Debug_LogWarning_m1399(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0025:
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_002c:
	{
		SimpleList_1_t117 * L_4 = (__this->___listenerList_2);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		SignalListener_t114 * L_6 = SimpleList_1_get_Item_m1524(L_4, L_5, /*hidden argument*/SimpleList_1_get_Item_m1524_MethodInfo_var);
		ConcreteSignalParameters_t113 * L_7 = (__this->___parameters_1);
		NullCheck(L_6);
		SignalListener_Invoke_m392(L_6, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_9 = V_0;
		SimpleList_1_t117 * L_10 = (__this->___listenerList_2);
		NullCheck(L_10);
		int32_t L_11 = SimpleList_1_get_Count_m1522(L_10, /*hidden argument*/SimpleList_1_get_Count_m1522_MethodInfo_var);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_002c;
		}
	}
	{
		return;
	}
}
// System.String Common.Signal.Signal::get_Name()
extern "C" String_t* Signal_get_Name_m403 (Signal_t116 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_0);
		return L_0;
	}
}
// Common.Signal.StringToSignalMapper
#include "AssemblyU2DCSharp_Common_Signal_StringToSignalMapper.h"
// Common.Signal.StringToSignalMapper
#include "AssemblyU2DCSharp_Common_Signal_StringToSignalMapperMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_3.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_3MethodDeclarations.h"
// System.Void Common.Signal.StringToSignalMapper::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_3MethodDeclarations.h"
extern TypeInfo* Dictionary_2_t119_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1525_MethodInfo_var;
extern "C" void StringToSignalMapper__ctor_m404 (StringToSignalMapper_t118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		Dictionary_2__ctor_m1525_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483708);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Dictionary_2_t119 * L_0 = (Dictionary_2_t119 *)il2cpp_codegen_object_new (Dictionary_2_t119_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1525(L_0, /*hidden argument*/Dictionary_2__ctor_m1525_MethodInfo_var);
		__this->___mapping_0 = L_0;
		return;
	}
}
// System.Void Common.Signal.StringToSignalMapper::Add(Common.Signal.Signal)
// Common.Signal.Signal
#include "AssemblyU2DCSharp_Common_Signal_Signal.h"
// Common.Signal.Signal
#include "AssemblyU2DCSharp_Common_Signal_SignalMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern "C" void StringToSignalMapper_Add_m405 (StringToSignalMapper_t118 * __this, Signal_t116 * ___signal, const MethodInfo* method)
{
	{
		Dictionary_2_t119 * L_0 = (__this->___mapping_0);
		Signal_t116 * L_1 = ___signal;
		NullCheck(L_1);
		String_t* L_2 = Signal_get_Name_m403(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_3 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::ContainsKey(!0) */, L_0, L_2);
		Assertion_Assert_m21(NULL /*static, unused*/, ((((int32_t)L_3) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Dictionary_2_t119 * L_4 = (__this->___mapping_0);
		Signal_t116 * L_5 = ___signal;
		NullCheck(L_5);
		String_t* L_6 = Signal_get_Name_m403(L_5, /*hidden argument*/NULL);
		Signal_t116 * L_7 = ___signal;
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, Signal_t116 * >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::set_Item(!0,!1) */, L_4, L_6, L_7);
		return;
	}
}
// System.Void Common.Signal.StringToSignalMapper::Dispatch(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" void StringToSignalMapper_Dispatch_m406 (StringToSignalMapper_t118 * __this, String_t* ___signalName, const MethodInfo* method)
{
	{
		Dictionary_2_t119 * L_0 = (__this->___mapping_0);
		String_t* L_1 = ___signalName;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::ContainsKey(!0) */, L_0, L_1);
		Assertion_Assert_m21(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Dictionary_2_t119 * L_3 = (__this->___mapping_0);
		String_t* L_4 = ___signalName;
		NullCheck(L_3);
		Signal_t116 * L_5 = (Signal_t116 *)VirtFuncInvoker1< Signal_t116 *, String_t* >::Invoke(19 /* !1 System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::get_Item(!0) */, L_3, L_4);
		NullCheck(L_5);
		Signal_Dispatch_m402(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Common.Signal.StringToSignalMapper::IsEmpty()
extern "C" bool StringToSignalMapper_IsEmpty_m407 (StringToSignalMapper_t118 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t119 * L_0 = (__this->___mapping_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::get_Count() */, L_0);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Common.Time.TimeReference::.ctor(System.String)
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void TimeReference__ctor_m408 (TimeReference_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___name_0 = L_0;
		__this->___timeScale_1 = (1.0f);
		__this->___affectsGlobalTimeScale_2 = 0;
		return;
	}
}
// System.String Common.Time.TimeReference::get_Name()
extern "C" String_t* TimeReference_get_Name_m409 (TimeReference_t14 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_0);
		return L_0;
	}
}
// System.Single Common.Time.TimeReference::get_TimeScale()
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
extern "C" float TimeReference_get_TimeScale_m410 (TimeReference_t14 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_timeScale_m1526(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_0, (0.0f), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		float L_2 = (__this->___timeScale_1);
		return L_2;
	}

IL_001b:
	{
		float L_3 = (__this->___timeScale_1);
		float L_4 = Time_get_timeScale_m1526(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((float)((float)L_3/(float)L_4));
	}
}
// System.Void Common.Time.TimeReference::set_TimeScale(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern "C" void TimeReference_set_TimeScale_m411 (TimeReference_t14 * __this, float ___value, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___affectsGlobalTimeScale_2);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		float L_1 = ___value;
		Time_set_timeScale_m1527(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0011:
	{
		float L_2 = ___value;
		__this->___timeScale_1 = L_2;
		return;
	}
}
// System.Single Common.Time.TimeReference::get_DeltaTime()
// Common.Time.TimeReference
#include "AssemblyU2DCSharp_Common_Time_TimeReferenceMethodDeclarations.h"
extern "C" float TimeReference_get_DeltaTime_m412 (TimeReference_t14 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_timeScale_m1526(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_0, (0.0f), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		float L_2 = Time_get_fixedDeltaTime_m1528(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = (__this->___timeScale_1);
		return ((float)((float)L_2*(float)L_3));
	}

IL_0021:
	{
		float L_4 = Time_get_deltaTime_m1379(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = TimeReference_get_TimeScale_m410(__this, /*hidden argument*/NULL);
		return ((float)((float)L_4*(float)L_5));
	}
}
// Common.Time.TimeReference Common.Time.TimeReference::GetDefaultInstance()
extern TypeInfo* TimeReference_t14_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral52;
extern "C" TimeReference_t14 * TimeReference_GetDefaultInstance_m413 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeReference_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(124);
		_stringLiteral52 = il2cpp_codegen_string_literal_from_index(52);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeReference_t14 * L_0 = ((TimeReference_t14_StaticFields*)TimeReference_t14_il2cpp_TypeInfo_var->static_fields)->___DEFAULT_INSTANCE_3;
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		TimeReference_t14 * L_1 = (TimeReference_t14 *)il2cpp_codegen_object_new (TimeReference_t14_il2cpp_TypeInfo_var);
		TimeReference__ctor_m408(L_1, _stringLiteral52, /*hidden argument*/NULL);
		((TimeReference_t14_StaticFields*)TimeReference_t14_il2cpp_TypeInfo_var->static_fields)->___DEFAULT_INSTANCE_3 = L_1;
	}

IL_0019:
	{
		TimeReference_t14 * L_2 = ((TimeReference_t14_StaticFields*)TimeReference_t14_il2cpp_TypeInfo_var->static_fields)->___DEFAULT_INSTANCE_3;
		return L_2;
	}
}
// System.Boolean Common.Time.TimeReference::get_AffectsGlobalTimeScale()
extern "C" bool TimeReference_get_AffectsGlobalTimeScale_m414 (TimeReference_t14 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___affectsGlobalTimeScale_2);
		return L_0;
	}
}
// System.Void Common.Time.TimeReference::set_AffectsGlobalTimeScale(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern "C" void TimeReference_set_AffectsGlobalTimeScale_m415 (TimeReference_t14 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___affectsGlobalTimeScale_2 = L_0;
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_4.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_4MethodDeclarations.h"
// System.Void Common.Time.TimeReferencePool::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_4MethodDeclarations.h"
extern TypeInfo* Dictionary_2_t409_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1529_MethodInfo_var;
extern "C" void TimeReferencePool__ctor_m416 (TimeReferencePool_t120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(125);
		Dictionary_2__ctor_m1529_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483709);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		Dictionary_2_t409 * L_0 = (Dictionary_2_t409 *)il2cpp_codegen_object_new (Dictionary_2_t409_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1529(L_0, /*hidden argument*/Dictionary_2__ctor_m1529_MethodInfo_var);
		__this->___instanceMap_0 = L_0;
		return;
	}
}
// System.Void Common.Time.TimeReferencePool::.cctor()
extern "C" void TimeReferencePool__cctor_m417 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// Common.Time.TimeReferencePool Common.Time.TimeReferencePool::GetInstance()
// Common.Time.TimeReferencePool
#include "AssemblyU2DCSharp_Common_Time_TimeReferencePoolMethodDeclarations.h"
extern TypeInfo* TimeReferencePool_t120_il2cpp_TypeInfo_var;
extern "C" TimeReferencePool_t120 * TimeReferencePool_GetInstance_m418 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeReferencePool_t120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeReferencePool_t120_il2cpp_TypeInfo_var);
		TimeReferencePool_t120 * L_0 = ((TimeReferencePool_t120_StaticFields*)TimeReferencePool_t120_il2cpp_TypeInfo_var->static_fields)->___ONLY_INSTANCE_1;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		TimeReferencePool_t120 * L_1 = (TimeReferencePool_t120 *)il2cpp_codegen_object_new (TimeReferencePool_t120_il2cpp_TypeInfo_var);
		TimeReferencePool__ctor_m416(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeReferencePool_t120_il2cpp_TypeInfo_var);
		((TimeReferencePool_t120_StaticFields*)TimeReferencePool_t120_il2cpp_TypeInfo_var->static_fields)->___ONLY_INSTANCE_1 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeReferencePool_t120_il2cpp_TypeInfo_var);
		TimeReferencePool_t120 * L_2 = ((TimeReferencePool_t120_StaticFields*)TimeReferencePool_t120_il2cpp_TypeInfo_var->static_fields)->___ONLY_INSTANCE_1;
		return L_2;
	}
}
// Common.Time.TimeReference Common.Time.TimeReferencePool::Add(System.String)
// System.String
#include "mscorlib_System_String.h"
// Common.Time.TimeReference
#include "AssemblyU2DCSharp_Common_Time_TimeReferenceMethodDeclarations.h"
extern TypeInfo* TimeReference_t14_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t121_il2cpp_TypeInfo_var;
extern "C" TimeReference_t14 * TimeReferencePool_Add_m419 (TimeReferencePool_t120 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeReference_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(124);
		IDictionary_2_t121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(126);
		s_Il2CppMethodIntialized = true;
	}
	TimeReference_t14 * V_0 = {0};
	{
		String_t* L_0 = ___name;
		TimeReference_t14 * L_1 = (TimeReference_t14 *)il2cpp_codegen_object_new (TimeReference_t14_il2cpp_TypeInfo_var);
		TimeReference__ctor_m408(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Object_t* L_2 = (__this->___instanceMap_0);
		String_t* L_3 = ___name;
		TimeReference_t14 * L_4 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker2< String_t*, TimeReference_t14 * >::Invoke(2 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Common.Time.TimeReference>::set_Item(!0,!1) */, IDictionary_2_t121_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		TimeReference_t14 * L_5 = V_0;
		return L_5;
	}
}
// Common.Time.TimeReference Common.Time.TimeReferencePool::Get(System.String)
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t121_il2cpp_TypeInfo_var;
extern "C" TimeReference_t14 * TimeReferencePool_Get_m420 (TimeReferencePool_t120 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IDictionary_2_t121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(126);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1321(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		TimeReference_t14 * L_2 = TimeReference_GetDefaultInstance_m413(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0011:
	{
		Object_t* L_3 = (__this->___instanceMap_0);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		bool L_5 = (bool)InterfaceFuncInvoker1< bool, String_t* >::Invoke(0 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,Common.Time.TimeReference>::ContainsKey(!0) */, IDictionary_2_t121_il2cpp_TypeInfo_var, L_3, L_4);
		Assertion_Assert_m21(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Object_t* L_6 = (__this->___instanceMap_0);
		String_t* L_7 = ___name;
		NullCheck(L_6);
		TimeReference_t14 * L_8 = (TimeReference_t14 *)InterfaceFuncInvoker1< TimeReference_t14 *, String_t* >::Invoke(1 /* !1 System.Collections.Generic.IDictionary`2<System.String,Common.Time.TimeReference>::get_Item(!0) */, IDictionary_2_t121_il2cpp_TypeInfo_var, L_6, L_7);
		return L_8;
	}
}
// Common.Time.TimeReference Common.Time.TimeReferencePool::GetDefault()
extern "C" TimeReference_t14 * TimeReferencePool_GetDefault_m421 (TimeReferencePool_t120 * __this, const MethodInfo* method)
{
	{
		TimeReference_t14 * L_0 = TimeReference_GetDefaultInstance_m413(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// Common.TimedKill/<KillAfterDuration>c__Iterator3
#include "AssemblyU2DCSharp_Common_TimedKill_U3CKillAfterDurationU3Ec_.h"
// Common.TimedKill/<KillAfterDuration>c__Iterator3
#include "AssemblyU2DCSharp_Common_TimedKill_U3CKillAfterDurationU3Ec_MethodDeclarations.h"
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
// Common.TimedKill
#include "AssemblyU2DCSharp_Common_TimedKill.h"
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
// System.Void Common.TimedKill/<KillAfterDuration>c__Iterator3::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void U3CKillAfterDurationU3Ec__Iterator3__ctor_m422 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Common.TimedKill/<KillAfterDuration>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CKillAfterDurationU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m423 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object Common.TimedKill/<KillAfterDuration>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CKillAfterDurationU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m424 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean Common.TimedKill/<KillAfterDuration>c__Iterator3::MoveNext()
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
extern TypeInfo* WaitForSeconds_t410_il2cpp_TypeInfo_var;
extern "C" bool U3CKillAfterDurationU3Ec__Iterator3_MoveNext_m425 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(127);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0055;
	}

IL_0021:
	{
		float L_2 = (__this->___duration_0);
		WaitForSeconds_t410 * L_3 = (WaitForSeconds_t410 *)il2cpp_codegen_object_new (WaitForSeconds_t410_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1530(L_3, L_2, /*hidden argument*/NULL);
		__this->___U24current_2 = L_3;
		__this->___U24PC_1 = 1;
		goto IL_0057;
	}

IL_003e:
	{
		TimedKill_t123 * L_4 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_4);
		SwarmItem_t124 * L_5 = (L_4->___swarm_2);
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(7 /* System.Void SwarmItem::Kill() */, L_5);
		__this->___U24PC_1 = (-1);
	}

IL_0055:
	{
		return 0;
	}

IL_0057:
	{
		return 1;
	}
	// Dead block : IL_0059: ldloc.1
}
// System.Void Common.TimedKill/<KillAfterDuration>c__Iterator3::Dispose()
extern "C" void U3CKillAfterDurationU3Ec__Iterator3_Dispose_m426 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void Common.TimedKill/<KillAfterDuration>c__Iterator3::Reset()
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern TypeInfo* NotSupportedException_t374_il2cpp_TypeInfo_var;
extern "C" void U3CKillAfterDurationU3Ec__Iterator3_Reset_m427 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t374 * L_0 = (NotSupportedException_t374 *)il2cpp_codegen_object_new (NotSupportedException_t374_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1366(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// Common.TimedKill
#include "AssemblyU2DCSharp_Common_TimedKillMethodDeclarations.h"
struct SwarmItem_t124;
// Declaration !!0 UnityEngine.Component::GetComponent<SwarmItem>()
// !!0 UnityEngine.Component::GetComponent<SwarmItem>()
#define Component_GetComponent_TisSwarmItem_t124_m1531(__this, method) (( SwarmItem_t124 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void Common.TimedKill::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void TimedKill__ctor_m428 (TimedKill_t123 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.TimedKill::Awake()
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisSwarmItem_t124_m1531_MethodInfo_var;
extern "C" void TimedKill_Awake_m429 (TimedKill_t123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisSwarmItem_t124_m1531_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483710);
		s_Il2CppMethodIntialized = true;
	}
	{
		SwarmItem_t124 * L_0 = Component_GetComponent_TisSwarmItem_t124_m1531(__this, /*hidden argument*/Component_GetComponent_TisSwarmItem_t124_m1531_MethodInfo_var);
		__this->___swarm_2 = L_0;
		SwarmItem_t124 * L_1 = (__this->___swarm_2);
		Assertion_AssertNotNull_m26(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.TimedKill::Run(System.Single)
// System.Single
#include "mscorlib_System_Single.h"
// Common.TimedKill
#include "AssemblyU2DCSharp_Common_TimedKillMethodDeclarations.h"
extern "C" void TimedKill_Run_m430 (TimedKill_t123 * __this, float ___duration, const MethodInfo* method)
{
	{
		float L_0 = ___duration;
		Object_t * L_1 = TimedKill_KillAfterDuration_m431(__this, L_0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1498(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Common.TimedKill::KillAfterDuration(System.Single)
// Common.TimedKill/<KillAfterDuration>c__Iterator3
#include "AssemblyU2DCSharp_Common_TimedKill_U3CKillAfterDurationU3Ec_MethodDeclarations.h"
extern TypeInfo* U3CKillAfterDurationU3Ec__Iterator3_t122_il2cpp_TypeInfo_var;
extern "C" Object_t * TimedKill_KillAfterDuration_m431 (TimedKill_t123 * __this, float ___duration, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CKillAfterDurationU3Ec__Iterator3_t122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(128);
		s_Il2CppMethodIntialized = true;
	}
	U3CKillAfterDurationU3Ec__Iterator3_t122 * V_0 = {0};
	{
		U3CKillAfterDurationU3Ec__Iterator3_t122 * L_0 = (U3CKillAfterDurationU3Ec__Iterator3_t122 *)il2cpp_codegen_object_new (U3CKillAfterDurationU3Ec__Iterator3_t122_il2cpp_TypeInfo_var);
		U3CKillAfterDurationU3Ec__Iterator3__ctor_m422(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CKillAfterDurationU3Ec__Iterator3_t122 * L_1 = V_0;
		float L_2 = ___duration;
		NullCheck(L_1);
		L_1->___duration_0 = L_2;
		U3CKillAfterDurationU3Ec__Iterator3_t122 * L_3 = V_0;
		float L_4 = ___duration;
		NullCheck(L_3);
		L_3->___U3CU24U3Eduration_3 = L_4;
		U3CKillAfterDurationU3Ec__Iterator3_t122 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_4 = __this;
		U3CKillAfterDurationU3Ec__Iterator3_t122 * L_6 = V_0;
		return L_6;
	}
}
// TouchCollider
#include "AssemblyU2DCSharp_TouchCollider.h"
// TouchCollider
#include "AssemblyU2DCSharp_TouchColliderMethodDeclarations.h"
// System.Collections.Generic.List`1<Common.Command>
#include "mscorlib_System_Collections_Generic_List_1_gen_6.h"
// UnityEngine.BoxCollider
#include "UnityEngine_UnityEngine_BoxCollider.h"
// System.Collections.Generic.List`1<Common.Command>
#include "mscorlib_System_Collections_Generic_List_1_gen_6MethodDeclarations.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
struct BoxCollider_t126;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider>()
#define Component_GetComponent_TisBoxCollider_t126_m1533(__this, method) (( BoxCollider_t126 * (*) (Component_t365 *, const MethodInfo*))Component_GetComponent_TisObject_t_m1326_gshared)(__this, method)
// System.Void TouchCollider::.ctor()
// System.Collections.Generic.List`1<Common.Command>
#include "mscorlib_System_Collections_Generic_List_1_gen_6MethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern TypeInfo* List_1_t411_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1532_MethodInfo_var;
extern "C" void TouchCollider__ctor_m432 (TouchCollider_t125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t411_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(130);
		List_1__ctor_m1532_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483711);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t411 * L_0 = (List_1_t411 *)il2cpp_codegen_object_new (List_1_t411_il2cpp_TypeInfo_var);
		List_1__ctor_m1532(L_0, /*hidden argument*/List_1__ctor_m1532_MethodInfo_var);
		__this->___commandList_3 = L_0;
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchCollider::Start()
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern const MethodInfo* Component_GetComponent_TisBoxCollider_t126_m1533_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral53;
extern "C" void TouchCollider_Start_m433 (TouchCollider_t125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisBoxCollider_t126_m1533_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483712);
		_stringLiteral53 = il2cpp_codegen_string_literal_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		BoxCollider_t126 * L_0 = Component_GetComponent_TisBoxCollider_t126_m1533(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider_t126_m1533_MethodInfo_var);
		__this->___boxCollider_2 = L_0;
		BoxCollider_t126 * L_1 = (__this->___boxCollider_2);
		Assertion_AssertNotNull_m25(NULL /*static, unused*/, L_1, _stringLiteral53, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchCollider::Update()
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
extern TypeInfo* Input_t412_il2cpp_TypeInfo_var;
extern TypeInfo* RaycastHit_t60_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t413_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t414_il2cpp_TypeInfo_var;
extern TypeInfo* Command_t348_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral54;
extern "C" void TouchCollider_Update_m434 (TouchCollider_t125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(132);
		RaycastHit_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		IEnumerable_1_t413_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(133);
		IEnumerator_1_t414_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(134);
		Command_t348_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral54 = il2cpp_codegen_string_literal_from_index(54);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t382  V_0 = {0};
	RaycastHit_t60  V_1 = {0};
	Object_t * V_2 = {0};
	Object_t* V_3 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t412_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButtonUp_m1534(NULL /*static, unused*/, _stringLiteral54, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0079;
		}
	}
	{
		Camera_t6 * L_1 = Camera_get_main_m1414(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t412_il2cpp_TypeInfo_var);
		Vector3_t36  L_2 = Input_get_mousePosition_m1535(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Ray_t382  L_3 = Camera_ScreenPointToRay_m1420(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Initobj (RaycastHit_t60_il2cpp_TypeInfo_var, (&V_1));
		BoxCollider_t126 * L_4 = (__this->___boxCollider_2);
		Ray_t382  L_5 = V_0;
		NullCheck(L_4);
		bool L_6 = Collider_Raycast_m1421(L_4, L_5, (&V_1), (1000.0f), /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0040;
		}
	}
	{
		return;
	}

IL_0040:
	{
		Object_t* L_7 = (__this->___commandList_3);
		NullCheck(L_7);
		Object_t* L_8 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Common.Command>::GetEnumerator() */, IEnumerable_1_t413_il2cpp_TypeInfo_var, L_7);
		V_3 = L_8;
	}

IL_004c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005e;
		}

IL_0051:
		{
			Object_t* L_9 = V_3;
			NullCheck(L_9);
			Object_t * L_10 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Common.Command>::get_Current() */, IEnumerator_1_t414_il2cpp_TypeInfo_var, L_9);
			V_2 = L_10;
			Object_t * L_11 = V_2;
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void Common.Command::Execute() */, Command_t348_il2cpp_TypeInfo_var, L_11);
		}

IL_005e:
		{
			Object_t* L_12 = V_3;
			NullCheck(L_12);
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_0051;
			}
		}

IL_0069:
		{
			IL2CPP_LEAVE(0x79, FINALLY_006e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_006e;
	}

FINALLY_006e:
	{ // begin finally (depth: 1)
		{
			Object_t* L_14 = V_3;
			if (L_14)
			{
				goto IL_0072;
			}
		}

IL_0071:
		{
			IL2CPP_END_FINALLY(110)
		}

IL_0072:
		{
			Object_t* L_15 = V_3;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_15);
			IL2CPP_END_FINALLY(110)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(110)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0079:
	{
		return;
	}
}
// System.Void TouchCollider::AddCommand(Common.Command)
extern TypeInfo* ICollection_1_t415_il2cpp_TypeInfo_var;
extern "C" void TouchCollider_AddCommand_m435 (TouchCollider_t125 * __this, Object_t * ___command, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_1_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(135);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (__this->___commandList_3);
		Object_t * L_1 = ___command;
		NullCheck(L_0);
		InterfaceActionInvoker1< Object_t * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<Common.Command>::Add(!0) */, ICollection_1_t415_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// RotationInRadians
#include "AssemblyU2DCSharp_RotationInRadians.h"
// RotationInRadians
#include "AssemblyU2DCSharp_RotationInRadiansMethodDeclarations.h"
// System.Void RotationInRadians::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void RotationInRadians__ctor_m436 (RotationInRadians_t128 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// Common.BoundsExtensions
#include "AssemblyU2DCSharp_Common_BoundsExtensions.h"
// Common.BoundsExtensions
#include "AssemblyU2DCSharp_Common_BoundsExtensionsMethodDeclarations.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
// System.Boolean Common.BoundsExtensions::Intersects2D(UnityEngine.Bounds,UnityEngine.Bounds)
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
extern "C" bool BoundsExtensions_Intersects2D_m437 (Object_t * __this /* static, unused */, Bounds_t349  ___self, Bounds_t349  ___other, const MethodInfo* method)
{
	Vector3_t36  V_0 = {0};
	Vector3_t36  V_1 = {0};
	Vector3_t36  V_2 = {0};
	Vector3_t36  V_3 = {0};
	Vector3_t36  V_4 = {0};
	Vector3_t36  V_5 = {0};
	Vector3_t36  V_6 = {0};
	Vector3_t36  V_7 = {0};
	{
		Vector3_t36  L_0 = Bounds_get_max_m1536((&___self), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___x_1);
		Vector3_t36  L_2 = Bounds_get_min_m1537((&___other), /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = ((&V_1)->___x_1);
		if ((((float)L_1) < ((float)L_3)))
		{
			goto IL_0046;
		}
	}
	{
		Vector3_t36  L_4 = Bounds_get_max_m1536((&___other), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = ((&V_2)->___x_1);
		Vector3_t36  L_6 = Bounds_get_min_m1537((&___self), /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = ((&V_3)->___x_1);
		if ((!(((float)L_5) < ((float)L_7))))
		{
			goto IL_0048;
		}
	}

IL_0046:
	{
		return 0;
	}

IL_0048:
	{
		Vector3_t36  L_8 = Bounds_get_max_m1536((&___self), /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = ((&V_4)->___y_2);
		Vector3_t36  L_10 = Bounds_get_min_m1537((&___other), /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = ((&V_5)->___y_2);
		if ((((float)L_9) < ((float)L_11)))
		{
			goto IL_0092;
		}
	}
	{
		Vector3_t36  L_12 = Bounds_get_max_m1536((&___other), /*hidden argument*/NULL);
		V_6 = L_12;
		float L_13 = ((&V_6)->___y_2);
		Vector3_t36  L_14 = Bounds_get_min_m1537((&___self), /*hidden argument*/NULL);
		V_7 = L_14;
		float L_15 = ((&V_7)->___y_2);
		if ((!(((float)L_13) < ((float)L_15))))
		{
			goto IL_0094;
		}
	}

IL_0092:
	{
		return 0;
	}

IL_0094:
	{
		return 1;
	}
}
// Common.GameObjectExtensions
#include "AssemblyU2DCSharp_Common_GameObjectExtensions.h"
// Common.GameObjectExtensions
#include "AssemblyU2DCSharp_Common_GameObjectExtensionsMethodDeclarations.h"
// System.Void Common.GameObjectExtensions::Deactivate(UnityEngine.GameObject)
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
extern "C" void GameObjectExtensions_Deactivate_m438 (Object_t * __this /* static, unused */, GameObject_t155 * ___self, const MethodInfo* method)
{
	{
		GameObject_t155 * L_0 = ___self;
		NullCheck(L_0);
		GameObject_SetActive_m1538(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.GameObjectExtensions::Activate(UnityEngine.GameObject)
extern "C" void GameObjectExtensions_Activate_m439 (Object_t * __this /* static, unused */, GameObject_t155 * ___self, const MethodInfo* method)
{
	{
		GameObject_t155 * L_0 = ___self;
		NullCheck(L_0);
		GameObject_SetActive_m1538(L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// Common.TransformExtensions
#include "AssemblyU2DCSharp_Common_TransformExtensions.h"
// Common.TransformExtensions
#include "AssemblyU2DCSharp_Common_TransformExtensionsMethodDeclarations.h"
// System.Void Common.TransformExtensions::SetEulerAngleY(UnityEngine.Transform,System.Single)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern "C" void TransformExtensions_SetEulerAngleY_m440 (Object_t * __this /* static, unused */, Transform_t35 * ___transform, float ___angle, const MethodInfo* method)
{
	Vector3_t36  V_0 = {0};
	{
		Transform_t35 * L_0 = ___transform;
		NullCheck(L_0);
		Vector3_t36  L_1 = Transform_get_eulerAngles_m1539(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ___angle;
		(&V_0)->___y_2 = L_2;
		Transform_t35 * L_3 = ___transform;
		Vector3_t36  L_4 = V_0;
		NullCheck(L_3);
		Transform_set_eulerAngles_m1540(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.TransformExtensions::SetEulerAngleZ(UnityEngine.Transform,System.Single)
extern "C" void TransformExtensions_SetEulerAngleZ_m441 (Object_t * __this /* static, unused */, Transform_t35 * ___transform, float ___angle, const MethodInfo* method)
{
	Vector3_t36  V_0 = {0};
	{
		Transform_t35 * L_0 = ___transform;
		NullCheck(L_0);
		Vector3_t36  L_1 = Transform_get_eulerAngles_m1539(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ___angle;
		(&V_0)->___z_3 = L_2;
		Transform_t35 * L_3 = ___transform;
		Vector3_t36  L_4 = V_0;
		NullCheck(L_3);
		Transform_set_eulerAngles_m1540(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// Common.UnityUtils
#include "AssemblyU2DCSharp_Common_UnityUtils.h"
// Common.UnityUtils
#include "AssemblyU2DCSharp_Common_UnityUtilsMethodDeclarations.h"
// Common.Utils.Factory
#include "AssemblyU2DCSharp_Common_Utils_Factory.h"
// Common.Utils.Factory
#include "AssemblyU2DCSharp_Common_Utils_FactoryMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5.h"
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5MethodDeclarations.h"
// System.Void Common.Utils.Factory::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void Factory__ctor_m442 (Factory_t133 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Utils.Factory::.cctor()
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5MethodDeclarations.h"
extern TypeInfo* Dictionary_2_t134_il2cpp_TypeInfo_var;
extern TypeInfo* Factory_t133_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1541_MethodInfo_var;
extern "C" void Factory__cctor_m443 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		Factory_t133_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(137);
		Dictionary_2__ctor_m1541_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483713);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t134 * L_0 = (Dictionary_2_t134 *)il2cpp_codegen_object_new (Dictionary_2_t134_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1541(L_0, /*hidden argument*/Dictionary_2__ctor_m1541_MethodInfo_var);
		((Factory_t133_StaticFields*)Factory_t133_il2cpp_TypeInfo_var->static_fields)->___factoryMap_0 = L_0;
		return;
	}
}
// System.Void Common.Utils.Factory::Clean()
extern TypeInfo* Factory_t133_il2cpp_TypeInfo_var;
extern "C" void Factory_Clean_m444 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Factory_t133_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(137);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Factory_t133_il2cpp_TypeInfo_var);
		Dictionary_2_t134 * L_0 = ((Factory_t133_StaticFields*)Factory_t133_il2cpp_TypeInfo_var->static_fields)->___factoryMap_0;
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Object>::Clear() */, L_0);
		return;
	}
}
// Common.Utils.Platform
#include "AssemblyU2DCSharp_Common_Utils_Platform.h"
// Common.Utils.Platform
#include "AssemblyU2DCSharp_Common_Utils_PlatformMethodDeclarations.h"
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
// System.Void Common.Utils.Platform::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void Platform__ctor_m445 (Platform_t135 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Common.Utils.Platform::get_DeviceId()
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
extern "C" String_t* Platform_get_DeviceId_m446 (Platform_t135 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = SystemInfo_get_deviceUniqueIdentifier_m1542(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean Common.Utils.Platform::IsEditor()
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
extern "C" bool Platform_IsEditor_m447 (Platform_t135 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m1543(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)4)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m1543(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_2 = Application_get_platform_m1543(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_3 = Application_get_platform_m1543(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)3)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_4 = Application_get_platform_m1543(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_5 = Application_get_platform_m1543(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)2)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_6 = Application_get_platform_m1543(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)5))))
		{
			goto IL_004e;
		}
	}

IL_004c:
	{
		return 1;
	}

IL_004e:
	{
		return 0;
	}
}
// System.Boolean Common.Utils.Platform::IsMobileAndroid()
extern "C" bool Platform_IsMobileAndroid_m448 (Platform_t135 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m1543(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_000e;
		}
	}
	{
		return 1;
	}

IL_000e:
	{
		return 0;
	}
}
// System.Boolean Common.Utils.Platform::IsMobileIOS()
extern "C" bool Platform_IsMobileIOS_m449 (Platform_t135 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m1543(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_000d;
		}
	}
	{
		return 1;
	}

IL_000d:
	{
		return 0;
	}
}
// Common.Utils.PopupValueSet
#include "AssemblyU2DCSharp_Common_Utils_PopupValueSet.h"
// Common.Utils.PopupValueSet
#include "AssemblyU2DCSharp_Common_Utils_PopupValueSetMethodDeclarations.h"
// System.Void Common.Utils.PopupValueSet::.ctor(System.String[])
#include "mscorlib_ArrayTypes.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void PopupValueSet__ctor_m450 (PopupValueSet_t136 * __this, StringU5BU5D_t137* ___valueList, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		StringU5BU5D_t137* L_0 = ___valueList;
		__this->___valueList_0 = L_0;
		return;
	}
}
// System.String[] Common.Utils.PopupValueSet::get_ValueList()
extern "C" StringU5BU5D_t137* PopupValueSet_get_ValueList_m451 (PopupValueSet_t136 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t137* L_0 = (__this->___valueList_0);
		return L_0;
	}
}
// System.Int32 Common.Utils.PopupValueSet::ResolveIndex(System.String)
// System.String
#include "mscorlib_System_String.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral55;
extern "C" int32_t PopupValueSet_ResolveIndex_m452 (PopupValueSet_t136 * __this, String_t* ___textValue, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral55 = il2cpp_codegen_string_literal_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0020;
	}

IL_0007:
	{
		StringU5BU5D_t137* L_0 = (__this->___valueList_0);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		String_t* L_3 = ___textValue;
		NullCheck((*(String_t**)(String_t**)SZArrayLdElema(L_0, L_2, sizeof(String_t*))));
		bool L_4 = String_Equals_m1339((*(String_t**)(String_t**)SZArrayLdElema(L_0, L_2, sizeof(String_t*))), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_5 = V_0;
		return L_5;
	}

IL_001c:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_7 = V_0;
		StringU5BU5D_t137* L_8 = (__this->___valueList_0);
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)(((Array_t *)L_8)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		String_t* L_9 = ___textValue;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1319(NULL /*static, unused*/, _stringLiteral55, L_9, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, 0, L_10, /*hidden argument*/NULL);
		return (-1);
	}
}
// System.String Common.Utils.PopupValueSet::GetValue(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" String_t* PopupValueSet_GetValue_m453 (PopupValueSet_t136 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		StringU5BU5D_t137* L_0 = (__this->___valueList_0);
		int32_t L_1 = ___index;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return (*(String_t**)(String_t**)SZArrayLdElema(L_0, L_2, sizeof(String_t*)));
	}
}
// Common.Utils.SimpleEncryption
#include "AssemblyU2DCSharp_Common_Utils_SimpleEncryption.h"
// Common.Utils.SimpleEncryption
#include "AssemblyU2DCSharp_Common_Utils_SimpleEncryptionMethodDeclarations.h"
// System.IO.MemoryStream
#include "mscorlib_System_IO_MemoryStream.h"
// System.Security.Cryptography.Rijndael
#include "mscorlib_System_Security_Cryptography_Rijndael.h"
// System.Security.Cryptography.CryptoStream
#include "mscorlib_System_Security_Cryptography_CryptoStream.h"
// System.Security.Cryptography.SymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithm.h"
// System.IO.Stream
#include "mscorlib_System_IO_Stream.h"
// System.Security.Cryptography.CryptoStreamMode
#include "mscorlib_System_Security_Cryptography_CryptoStreamMode.h"
// System.Security.Cryptography.PasswordDeriveBytes
#include "mscorlib_System_Security_Cryptography_PasswordDeriveBytes.h"
// System.Text.Encoding
#include "mscorlib_System_Text_Encoding.h"
// System.IO.MemoryStream
#include "mscorlib_System_IO_MemoryStreamMethodDeclarations.h"
// System.Security.Cryptography.Rijndael
#include "mscorlib_System_Security_Cryptography_RijndaelMethodDeclarations.h"
// System.Security.Cryptography.SymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithmMethodDeclarations.h"
// System.Security.Cryptography.CryptoStream
#include "mscorlib_System_Security_Cryptography_CryptoStreamMethodDeclarations.h"
// System.Text.Encoding
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
// System.Security.Cryptography.PasswordDeriveBytes
#include "mscorlib_System_Security_Cryptography_PasswordDeriveBytesMethodDeclarations.h"
// System.Void Common.Utils.SimpleEncryption::.ctor(System.Byte[],System.String)
#include "mscorlib_ArrayTypes.h"
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void SimpleEncryption__ctor_m454 (SimpleEncryption_t138 * __this, ByteU5BU5D_t139* ___salt, String_t* ___password, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t139* L_0 = ___salt;
		__this->___salt_0 = L_0;
		String_t* L_1 = ___password;
		__this->___password_1 = L_1;
		return;
	}
}
// System.Byte[] Common.Utils.SimpleEncryption::Encrypt(System.Byte[],System.Byte[],System.Byte[])
// System.IO.MemoryStream
#include "mscorlib_System_IO_MemoryStreamMethodDeclarations.h"
// System.Security.Cryptography.Rijndael
#include "mscorlib_System_Security_Cryptography_RijndaelMethodDeclarations.h"
// System.Security.Cryptography.CryptoStream
#include "mscorlib_System_Security_Cryptography_CryptoStreamMethodDeclarations.h"
extern TypeInfo* MemoryStream_t416_il2cpp_TypeInfo_var;
extern TypeInfo* CryptoStream_t418_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t139* SimpleEncryption_Encrypt_m455 (Object_t * __this /* static, unused */, ByteU5BU5D_t139* ___clearData, ByteU5BU5D_t139* ___Key, ByteU5BU5D_t139* ___IV, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemoryStream_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(138);
		CryptoStream_t418_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t416 * V_0 = {0};
	Rijndael_t417 * V_1 = {0};
	CryptoStream_t418 * V_2 = {0};
	ByteU5BU5D_t139* V_3 = {0};
	{
		MemoryStream_t416 * L_0 = (MemoryStream_t416 *)il2cpp_codegen_object_new (MemoryStream_t416_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1544(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Rijndael_t417 * L_1 = Rijndael_Create_m1545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		Rijndael_t417 * L_2 = V_1;
		ByteU5BU5D_t139* L_3 = ___Key;
		NullCheck(L_2);
		VirtActionInvoker1< ByteU5BU5D_t139* >::Invoke(12 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_2, L_3);
		Rijndael_t417 * L_4 = V_1;
		ByteU5BU5D_t139* L_5 = ___IV;
		NullCheck(L_4);
		VirtActionInvoker1< ByteU5BU5D_t139* >::Invoke(10 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_IV(System.Byte[]) */, L_4, L_5);
		MemoryStream_t416 * L_6 = V_0;
		Rijndael_t417 * L_7 = V_1;
		NullCheck(L_7);
		Object_t * L_8 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(22 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor() */, L_7);
		CryptoStream_t418 * L_9 = (CryptoStream_t418 *)il2cpp_codegen_object_new (CryptoStream_t418_il2cpp_TypeInfo_var);
		CryptoStream__ctor_m1546(L_9, L_6, L_8, 1, /*hidden argument*/NULL);
		V_2 = L_9;
		CryptoStream_t418 * L_10 = V_2;
		ByteU5BU5D_t139* L_11 = ___clearData;
		ByteU5BU5D_t139* L_12 = ___clearData;
		NullCheck(L_12);
		NullCheck(L_10);
		VirtActionInvoker3< ByteU5BU5D_t139*, int32_t, int32_t >::Invoke(18 /* System.Void System.Security.Cryptography.CryptoStream::Write(System.Byte[],System.Int32,System.Int32) */, L_10, L_11, 0, (((int32_t)(((Array_t *)L_12)->max_length))));
		CryptoStream_t418 * L_13 = V_2;
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Security.Cryptography.CryptoStream::Close() */, L_13);
		MemoryStream_t416 * L_14 = V_0;
		NullCheck(L_14);
		ByteU5BU5D_t139* L_15 = (ByteU5BU5D_t139*)VirtFuncInvoker0< ByteU5BU5D_t139* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_14);
		V_3 = L_15;
		ByteU5BU5D_t139* L_16 = V_3;
		return L_16;
	}
}
// System.String Common.Utils.SimpleEncryption::Encrypt(System.String)
// System.Text.Encoding
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
// System.Security.Cryptography.PasswordDeriveBytes
#include "mscorlib_System_Security_Cryptography_PasswordDeriveBytesMethodDeclarations.h"
// Common.Utils.SimpleEncryption
#include "AssemblyU2DCSharp_Common_Utils_SimpleEncryptionMethodDeclarations.h"
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"
extern TypeInfo* Encoding_t420_il2cpp_TypeInfo_var;
extern TypeInfo* PasswordDeriveBytes_t419_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t373_il2cpp_TypeInfo_var;
extern "C" String_t* SimpleEncryption_Encrypt_m456 (SimpleEncryption_t138 * __this, String_t* ___clearText, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(140);
		PasswordDeriveBytes_t419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(141);
		Convert_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t139* V_0 = {0};
	PasswordDeriveBytes_t419 * V_1 = {0};
	ByteU5BU5D_t139* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t420_il2cpp_TypeInfo_var);
		Encoding_t420 * L_0 = Encoding_get_Unicode_m1547(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___clearText;
		NullCheck(L_0);
		ByteU5BU5D_t139* L_2 = (ByteU5BU5D_t139*)VirtFuncInvoker1< ByteU5BU5D_t139*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		V_0 = L_2;
		String_t* L_3 = (__this->___password_1);
		ByteU5BU5D_t139* L_4 = (__this->___salt_0);
		PasswordDeriveBytes_t419 * L_5 = (PasswordDeriveBytes_t419 *)il2cpp_codegen_object_new (PasswordDeriveBytes_t419_il2cpp_TypeInfo_var);
		PasswordDeriveBytes__ctor_m1548(L_5, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		ByteU5BU5D_t139* L_6 = V_0;
		PasswordDeriveBytes_t419 * L_7 = V_1;
		NullCheck(L_7);
		ByteU5BU5D_t139* L_8 = (ByteU5BU5D_t139*)VirtFuncInvoker1< ByteU5BU5D_t139*, int32_t >::Invoke(4 /* System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::GetBytes(System.Int32) */, L_7, ((int32_t)32));
		PasswordDeriveBytes_t419 * L_9 = V_1;
		NullCheck(L_9);
		ByteU5BU5D_t139* L_10 = (ByteU5BU5D_t139*)VirtFuncInvoker1< ByteU5BU5D_t139*, int32_t >::Invoke(4 /* System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::GetBytes(System.Int32) */, L_9, ((int32_t)16));
		ByteU5BU5D_t139* L_11 = SimpleEncryption_Encrypt_m455(NULL /*static, unused*/, L_6, L_8, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		ByteU5BU5D_t139* L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		String_t* L_13 = Convert_ToBase64String_m1549(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Byte[] Common.Utils.SimpleEncryption::Encrypt(System.Byte[])
extern TypeInfo* PasswordDeriveBytes_t419_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t139* SimpleEncryption_Encrypt_m457 (SimpleEncryption_t138 * __this, ByteU5BU5D_t139* ___clearData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PasswordDeriveBytes_t419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(141);
		s_Il2CppMethodIntialized = true;
	}
	PasswordDeriveBytes_t419 * V_0 = {0};
	{
		String_t* L_0 = (__this->___password_1);
		ByteU5BU5D_t139* L_1 = (__this->___salt_0);
		PasswordDeriveBytes_t419 * L_2 = (PasswordDeriveBytes_t419 *)il2cpp_codegen_object_new (PasswordDeriveBytes_t419_il2cpp_TypeInfo_var);
		PasswordDeriveBytes__ctor_m1548(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ByteU5BU5D_t139* L_3 = ___clearData;
		PasswordDeriveBytes_t419 * L_4 = V_0;
		NullCheck(L_4);
		ByteU5BU5D_t139* L_5 = (ByteU5BU5D_t139*)VirtFuncInvoker1< ByteU5BU5D_t139*, int32_t >::Invoke(4 /* System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::GetBytes(System.Int32) */, L_4, ((int32_t)32));
		PasswordDeriveBytes_t419 * L_6 = V_0;
		NullCheck(L_6);
		ByteU5BU5D_t139* L_7 = (ByteU5BU5D_t139*)VirtFuncInvoker1< ByteU5BU5D_t139*, int32_t >::Invoke(4 /* System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::GetBytes(System.Int32) */, L_6, ((int32_t)16));
		ByteU5BU5D_t139* L_8 = SimpleEncryption_Encrypt_m455(NULL /*static, unused*/, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Byte[] Common.Utils.SimpleEncryption::Decrypt(System.Byte[],System.Byte[],System.Byte[])
extern TypeInfo* MemoryStream_t416_il2cpp_TypeInfo_var;
extern TypeInfo* CryptoStream_t418_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t139* SimpleEncryption_Decrypt_m458 (Object_t * __this /* static, unused */, ByteU5BU5D_t139* ___cipherData, ByteU5BU5D_t139* ___Key, ByteU5BU5D_t139* ___IV, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemoryStream_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(138);
		CryptoStream_t418_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t416 * V_0 = {0};
	Rijndael_t417 * V_1 = {0};
	CryptoStream_t418 * V_2 = {0};
	ByteU5BU5D_t139* V_3 = {0};
	{
		MemoryStream_t416 * L_0 = (MemoryStream_t416 *)il2cpp_codegen_object_new (MemoryStream_t416_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1544(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Rijndael_t417 * L_1 = Rijndael_Create_m1545(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		Rijndael_t417 * L_2 = V_1;
		ByteU5BU5D_t139* L_3 = ___Key;
		NullCheck(L_2);
		VirtActionInvoker1< ByteU5BU5D_t139* >::Invoke(12 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Key(System.Byte[]) */, L_2, L_3);
		Rijndael_t417 * L_4 = V_1;
		ByteU5BU5D_t139* L_5 = ___IV;
		NullCheck(L_4);
		VirtActionInvoker1< ByteU5BU5D_t139* >::Invoke(10 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_IV(System.Byte[]) */, L_4, L_5);
		MemoryStream_t416 * L_6 = V_0;
		Rijndael_t417 * L_7 = V_1;
		NullCheck(L_7);
		Object_t * L_8 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(20 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateDecryptor() */, L_7);
		CryptoStream_t418 * L_9 = (CryptoStream_t418 *)il2cpp_codegen_object_new (CryptoStream_t418_il2cpp_TypeInfo_var);
		CryptoStream__ctor_m1546(L_9, L_6, L_8, 1, /*hidden argument*/NULL);
		V_2 = L_9;
		CryptoStream_t418 * L_10 = V_2;
		ByteU5BU5D_t139* L_11 = ___cipherData;
		ByteU5BU5D_t139* L_12 = ___cipherData;
		NullCheck(L_12);
		NullCheck(L_10);
		VirtActionInvoker3< ByteU5BU5D_t139*, int32_t, int32_t >::Invoke(18 /* System.Void System.Security.Cryptography.CryptoStream::Write(System.Byte[],System.Int32,System.Int32) */, L_10, L_11, 0, (((int32_t)(((Array_t *)L_12)->max_length))));
		CryptoStream_t418 * L_13 = V_2;
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Security.Cryptography.CryptoStream::Close() */, L_13);
		MemoryStream_t416 * L_14 = V_0;
		NullCheck(L_14);
		ByteU5BU5D_t139* L_15 = (ByteU5BU5D_t139*)VirtFuncInvoker0< ByteU5BU5D_t139* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_14);
		V_3 = L_15;
		ByteU5BU5D_t139* L_16 = V_3;
		return L_16;
	}
}
// System.String Common.Utils.SimpleEncryption::Decrypt(System.String)
extern TypeInfo* Convert_t373_il2cpp_TypeInfo_var;
extern TypeInfo* PasswordDeriveBytes_t419_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t420_il2cpp_TypeInfo_var;
extern "C" String_t* SimpleEncryption_Decrypt_m459 (SimpleEncryption_t138 * __this, String_t* ___cipherText, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t373_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		PasswordDeriveBytes_t419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(141);
		Encoding_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(140);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t139* V_0 = {0};
	PasswordDeriveBytes_t419 * V_1 = {0};
	ByteU5BU5D_t139* V_2 = {0};
	{
		String_t* L_0 = ___cipherText;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t373_il2cpp_TypeInfo_var);
		ByteU5BU5D_t139* L_1 = Convert_FromBase64String_m1550(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = (__this->___password_1);
		ByteU5BU5D_t139* L_3 = (__this->___salt_0);
		PasswordDeriveBytes_t419 * L_4 = (PasswordDeriveBytes_t419 *)il2cpp_codegen_object_new (PasswordDeriveBytes_t419_il2cpp_TypeInfo_var);
		PasswordDeriveBytes__ctor_m1548(L_4, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		ByteU5BU5D_t139* L_5 = V_0;
		PasswordDeriveBytes_t419 * L_6 = V_1;
		NullCheck(L_6);
		ByteU5BU5D_t139* L_7 = (ByteU5BU5D_t139*)VirtFuncInvoker1< ByteU5BU5D_t139*, int32_t >::Invoke(4 /* System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::GetBytes(System.Int32) */, L_6, ((int32_t)32));
		PasswordDeriveBytes_t419 * L_8 = V_1;
		NullCheck(L_8);
		ByteU5BU5D_t139* L_9 = (ByteU5BU5D_t139*)VirtFuncInvoker1< ByteU5BU5D_t139*, int32_t >::Invoke(4 /* System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::GetBytes(System.Int32) */, L_8, ((int32_t)16));
		ByteU5BU5D_t139* L_10 = SimpleEncryption_Decrypt_m458(NULL /*static, unused*/, L_5, L_7, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t420_il2cpp_TypeInfo_var);
		Encoding_t420 * L_11 = Encoding_get_Unicode_m1547(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t139* L_12 = V_2;
		NullCheck(L_11);
		String_t* L_13 = (String_t*)VirtFuncInvoker1< String_t*, ByteU5BU5D_t139* >::Invoke(20 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_11, L_12);
		return L_13;
	}
}
// System.Byte[] Common.Utils.SimpleEncryption::Decrypt(System.Byte[])
extern TypeInfo* PasswordDeriveBytes_t419_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t139* SimpleEncryption_Decrypt_m460 (SimpleEncryption_t138 * __this, ByteU5BU5D_t139* ___cipherData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PasswordDeriveBytes_t419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(141);
		s_Il2CppMethodIntialized = true;
	}
	PasswordDeriveBytes_t419 * V_0 = {0};
	{
		String_t* L_0 = (__this->___password_1);
		ByteU5BU5D_t139* L_1 = (__this->___salt_0);
		PasswordDeriveBytes_t419 * L_2 = (PasswordDeriveBytes_t419 *)il2cpp_codegen_object_new (PasswordDeriveBytes_t419_il2cpp_TypeInfo_var);
		PasswordDeriveBytes__ctor_m1548(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ByteU5BU5D_t139* L_3 = ___cipherData;
		PasswordDeriveBytes_t419 * L_4 = V_0;
		NullCheck(L_4);
		ByteU5BU5D_t139* L_5 = (ByteU5BU5D_t139*)VirtFuncInvoker1< ByteU5BU5D_t139*, int32_t >::Invoke(4 /* System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::GetBytes(System.Int32) */, L_4, ((int32_t)32));
		PasswordDeriveBytes_t419 * L_6 = V_0;
		NullCheck(L_6);
		ByteU5BU5D_t139* L_7 = (ByteU5BU5D_t139*)VirtFuncInvoker1< ByteU5BU5D_t139*, int32_t >::Invoke(4 /* System.Byte[] System.Security.Cryptography.PasswordDeriveBytes::GetBytes(System.Int32) */, L_6, ((int32_t)16));
		ByteU5BU5D_t139* L_8 = SimpleEncryption_Decrypt_m458(NULL /*static, unused*/, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean Common.Utils.SimpleEncryption::CanDecrypt(System.String)
extern TypeInfo* Exception_t359_il2cpp_TypeInfo_var;
extern "C" bool SimpleEncryption_CanDecrypt_m461 (SimpleEncryption_t138 * __this, String_t* ___encrypted, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t359_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t359 * V_0 = {0};
	bool V_1 = false;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		String_t* L_0 = ___encrypted;
		VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String Common.Utils.SimpleEncryption::Decrypt(System.String) */, __this, L_0);
		goto IL_001a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t359 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t359_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000d;
		throw e;
	}

CATCH_000d:
	{ // begin catch(System.Exception)
		{
			V_0 = ((Exception_t359 *)__exception_local);
			V_1 = 0;
			goto IL_001c;
		}

IL_0015:
		{
			; // IL_0015: leave IL_001a
		}
	} // end catch (depth: 1)

IL_001a:
	{
		return 1;
	}

IL_001c:
	{
		bool L_1 = V_1;
		return L_1;
	}
}
// System.Boolean Common.Utils.SimpleEncryption::TryDecrypt(System.String,System.String&)
extern TypeInfo* Exception_t359_il2cpp_TypeInfo_var;
extern "C" bool SimpleEncryption_TryDecrypt_m462 (SimpleEncryption_t138 * __this, String_t* ___encrypted, String_t** ___decrypted, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t359_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t359 * V_0 = {0};
	bool V_1 = false;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		String_t** L_0 = ___decrypted;
		String_t* L_1 = ___encrypted;
		String_t* L_2 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String Common.Utils.SimpleEncryption::Decrypt(System.String) */, __this, L_1);
		*((Object_t **)(L_0)) = (Object_t *)L_2;
		goto IL_001e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t359 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t359_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Exception)
		{
			V_0 = ((Exception_t359 *)__exception_local);
			String_t** L_3 = ___decrypted;
			*((Object_t **)(L_3)) = (Object_t *)NULL;
			V_1 = 0;
			goto IL_0020;
		}

IL_0019:
		{
			; // IL_0019: leave IL_001e
		}
	} // end catch (depth: 1)

IL_001e:
	{
		return 1;
	}

IL_0020:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// VectorUtils
#include "AssemblyU2DCSharp_VectorUtils.h"
// System.Void VectorUtils::.cctor()
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
extern TypeInfo* VectorUtils_t140_il2cpp_TypeInfo_var;
extern "C" void VectorUtils__cctor_m463 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VectorUtils_t140_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t36  L_0 = {0};
		Vector3__ctor_m1500(&L_0, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((VectorUtils_t140_StaticFields*)VectorUtils_t140_il2cpp_TypeInfo_var->static_fields)->___ZERO_0 = L_0;
		Vector3_t36  L_1 = {0};
		Vector3__ctor_m1500(&L_1, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((VectorUtils_t140_StaticFields*)VectorUtils_t140_il2cpp_TypeInfo_var->static_fields)->___ONE_1 = L_1;
		Vector3_t36  L_2 = {0};
		Vector3__ctor_m1500(&L_2, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		((VectorUtils_t140_StaticFields*)VectorUtils_t140_il2cpp_TypeInfo_var->static_fields)->___UP_2 = L_2;
		Vector3_t36  L_3 = {0};
		Vector3__ctor_m1500(&L_3, (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((VectorUtils_t140_StaticFields*)VectorUtils_t140_il2cpp_TypeInfo_var->static_fields)->___RIGHT_3 = L_3;
		return;
	}
}
// System.Boolean VectorUtils::Equals(UnityEngine.Vector3,UnityEngine.Vector3)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Comparison
#include "AssemblyU2DCSharp_ComparisonMethodDeclarations.h"
extern "C" bool VectorUtils_Equals_m464 (Object_t * __this /* static, unused */, Vector3_t36  ___a, Vector3_t36  ___b, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		bool L_2 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		float L_3 = ((&___a)->___y_2);
		float L_4 = ((&___b)->___y_2);
		bool L_5 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0045;
		}
	}
	{
		float L_6 = ((&___a)->___z_3);
		float L_7 = ((&___b)->___z_3);
		bool L_8 = Comparison_TolerantEquals_m58(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_8));
		goto IL_0046;
	}

IL_0045:
	{
		G_B4_0 = 0;
	}

IL_0046:
	{
		return G_B4_0;
	}
}
// Common.Xml.SimpleXmlNode/NodeProcessor
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNode_NodeProcessor.h"
// Common.Xml.SimpleXmlNode/NodeProcessor
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNode_NodeProcessorMethodDeclarations.h"
// Common.Xml.SimpleXmlNode
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNode.h"
// System.Void Common.Xml.SimpleXmlNode/NodeProcessor::.ctor(System.Object,System.IntPtr)
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern "C" void NodeProcessor__ctor_m465 (NodeProcessor_t141 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void Common.Xml.SimpleXmlNode/NodeProcessor::Invoke(Common.Xml.SimpleXmlNode)
// Common.Xml.SimpleXmlNode
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNode.h"
extern "C" void NodeProcessor_Invoke_m466 (NodeProcessor_t141 * __this, SimpleXmlNode_t142 * ___node, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		NodeProcessor_Invoke_m466((NodeProcessor_t141 *)__this->___prev_9,___node, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, SimpleXmlNode_t142 * ___node, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___node,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, SimpleXmlNode_t142 * ___node, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___node,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___node,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_NodeProcessor_t141(Il2CppObject* delegate, SimpleXmlNode_t142 * ___node)
{
	// Marshaling of parameter '___node' to native representation
	SimpleXmlNode_t142 * ____node_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'Common.Xml.SimpleXmlNode'."));
}
// System.IAsyncResult Common.Xml.SimpleXmlNode/NodeProcessor::BeginInvoke(Common.Xml.SimpleXmlNode,System.AsyncCallback,System.Object)
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
extern "C" Object_t * NodeProcessor_BeginInvoke_m467 (NodeProcessor_t141 * __this, SimpleXmlNode_t142 * ___node, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___node;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void Common.Xml.SimpleXmlNode/NodeProcessor::EndInvoke(System.IAsyncResult)
extern "C" void NodeProcessor_EndInvoke_m468 (NodeProcessor_t141 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Common.Xml.SimpleXmlNode
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNodeMethodDeclarations.h"
// System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6.h"
// System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6MethodDeclarations.h"
// System.Void Common.Xml.SimpleXmlNode::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6MethodDeclarations.h"
extern TypeInfo* List_1_t143_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1551_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1552_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral56;
extern "C" void SimpleXmlNode__ctor_m469 (SimpleXmlNode_t142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t143_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(143);
		Dictionary_2_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		List_1__ctor_m1551_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483714);
		Dictionary_2__ctor_m1552_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483715);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		__this->___TagName_2 = _stringLiteral56;
		__this->___ParentNode_3 = (SimpleXmlNode_t142 *)NULL;
		List_1_t143 * L_0 = (List_1_t143 *)il2cpp_codegen_object_new (List_1_t143_il2cpp_TypeInfo_var);
		List_1__ctor_m1551(L_0, /*hidden argument*/List_1__ctor_m1551_MethodInfo_var);
		__this->___Children_4 = L_0;
		Dictionary_2_t144 * L_1 = (Dictionary_2_t144 *)il2cpp_codegen_object_new (Dictionary_2_t144_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1552(L_1, /*hidden argument*/Dictionary_2__ctor_m1552_MethodInfo_var);
		__this->___Attributes_5 = L_1;
		return;
	}
}
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlNode::FindFirstNodeInChildren(System.String)
// System.String
#include "mscorlib_System_String.h"
// Common.Xml.SimpleXmlNode
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNodeMethodDeclarations.h"
extern "C" SimpleXmlNode_t142 * SimpleXmlNode_FindFirstNodeInChildren_m470 (SimpleXmlNode_t142 * __this, String_t* ___tagName, const MethodInfo* method)
{
	{
		String_t* L_0 = ___tagName;
		SimpleXmlNode_t142 * L_1 = SimpleXmlNode_FindFirstNodeInChildren_m471(__this, __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlNode::FindFirstNodeInChildren(Common.Xml.SimpleXmlNode,System.String)
// Common.Xml.SimpleXmlNode
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNode.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern "C" SimpleXmlNode_t142 * SimpleXmlNode_FindFirstNodeInChildren_m471 (SimpleXmlNode_t142 * __this, SimpleXmlNode_t142 * ___node, String_t* ___tagName, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	SimpleXmlNode_t142 * V_2 = {0};
	{
		V_0 = 0;
		goto IL_0034;
	}

IL_0007:
	{
		SimpleXmlNode_t142 * L_0 = ___node;
		NullCheck(L_0);
		List_1_t143 * L_1 = (L_0->___Children_4);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		SimpleXmlNode_t142 * L_3 = (SimpleXmlNode_t142 *)VirtFuncInvoker1< SimpleXmlNode_t142 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Item(System.Int32) */, L_1, L_2);
		NullCheck(L_3);
		String_t* L_4 = (L_3->___TagName_2);
		String_t* L_5 = ___tagName;
		NullCheck(L_4);
		bool L_6 = String_Equals_m1339(L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0030;
		}
	}
	{
		SimpleXmlNode_t142 * L_7 = ___node;
		NullCheck(L_7);
		List_1_t143 * L_8 = (L_7->___Children_4);
		int32_t L_9 = V_0;
		NullCheck(L_8);
		SimpleXmlNode_t142 * L_10 = (SimpleXmlNode_t142 *)VirtFuncInvoker1< SimpleXmlNode_t142 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Item(System.Int32) */, L_8, L_9);
		return L_10;
	}

IL_0030:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_12 = V_0;
		SimpleXmlNode_t142 * L_13 = ___node;
		NullCheck(L_13);
		List_1_t143 * L_14 = (L_13->___Children_4);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Count() */, L_14);
		if ((((int32_t)L_12) < ((int32_t)L_15)))
		{
			goto IL_0007;
		}
	}
	{
		V_1 = 0;
		goto IL_006c;
	}

IL_004c:
	{
		SimpleXmlNode_t142 * L_16 = ___node;
		NullCheck(L_16);
		List_1_t143 * L_17 = (L_16->___Children_4);
		int32_t L_18 = V_1;
		NullCheck(L_17);
		SimpleXmlNode_t142 * L_19 = (SimpleXmlNode_t142 *)VirtFuncInvoker1< SimpleXmlNode_t142 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Item(System.Int32) */, L_17, L_18);
		String_t* L_20 = ___tagName;
		SimpleXmlNode_t142 * L_21 = SimpleXmlNode_FindFirstNodeInChildren_m471(__this, L_19, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		SimpleXmlNode_t142 * L_22 = V_2;
		if (!L_22)
		{
			goto IL_0068;
		}
	}
	{
		SimpleXmlNode_t142 * L_23 = V_2;
		return L_23;
	}

IL_0068:
	{
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_006c:
	{
		int32_t L_25 = V_1;
		SimpleXmlNode_t142 * L_26 = ___node;
		NullCheck(L_26);
		List_1_t143 * L_27 = (L_26->___Children_4);
		NullCheck(L_27);
		int32_t L_28 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Count() */, L_27);
		if ((((int32_t)L_25) < ((int32_t)L_28)))
		{
			goto IL_004c;
		}
	}
	{
		return (SimpleXmlNode_t142 *)NULL;
	}
}
// System.Boolean Common.Xml.SimpleXmlNode::HasAttribute(System.String)
extern "C" bool SimpleXmlNode_HasAttribute_m472 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method)
{
	{
		Dictionary_2_t144 * L_0 = (__this->___Attributes_5);
		String_t* L_1 = ___attributeKey;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.String Common.Xml.SimpleXmlNode::GetAttribute(System.String)
extern "C" String_t* SimpleXmlNode_GetAttribute_m473 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method)
{
	{
		Dictionary_2_t144 * L_0 = (__this->___Attributes_5);
		String_t* L_1 = ___attributeKey;
		NullCheck(L_0);
		String_t* L_2 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(19 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_0, L_1);
		NullCheck(L_2);
		String_t* L_3 = String_Trim_m1553(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Common.Xml.SimpleXmlNode::GetAttributeAsInt(System.String)
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
extern "C" int32_t SimpleXmlNode_GetAttributeAsInt_m474 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method)
{
	{
		String_t* L_0 = ___attributeKey;
		bool L_1 = SimpleXmlNode_ContainsAttribute_m477(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		String_t* L_2 = ___attributeKey;
		String_t* L_3 = SimpleXmlNode_GetAttribute_m473(__this, L_2, /*hidden argument*/NULL);
		int32_t L_4 = Int32_Parse_m1554(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single Common.Xml.SimpleXmlNode::GetAttributeAsFloat(System.String)
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
extern "C" float SimpleXmlNode_GetAttributeAsFloat_m475 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method)
{
	{
		String_t* L_0 = ___attributeKey;
		bool L_1 = SimpleXmlNode_ContainsAttribute_m477(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (0.0f);
	}

IL_0012:
	{
		String_t* L_2 = ___attributeKey;
		String_t* L_3 = SimpleXmlNode_GetAttribute_m473(__this, L_2, /*hidden argument*/NULL);
		float L_4 = Single_Parse_m1555(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean Common.Xml.SimpleXmlNode::GetAttributeAsBool(System.String)
extern Il2CppCodeGenString* _stringLiteral57;
extern Il2CppCodeGenString* _stringLiteral24;
extern "C" bool SimpleXmlNode_GetAttributeAsBool_m476 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral57 = il2cpp_codegen_string_literal_from_index(57);
		_stringLiteral24 = il2cpp_codegen_string_literal_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		String_t* L_0 = ___attributeKey;
		bool L_1 = SimpleXmlNode_ContainsAttribute_m477(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		String_t* L_2 = ___attributeKey;
		String_t* L_3 = SimpleXmlNode_GetAttribute_m473(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		NullCheck(_stringLiteral57);
		bool L_5 = String_Equals_m1339(_stringLiteral57, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_6 = V_0;
		NullCheck(_stringLiteral24);
		bool L_7 = String_Equals_m1339(_stringLiteral24, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0034;
	}

IL_0033:
	{
		G_B5_0 = 1;
	}

IL_0034:
	{
		return G_B5_0;
	}
}
// System.Boolean Common.Xml.SimpleXmlNode::ContainsAttribute(System.String)
extern "C" bool SimpleXmlNode_ContainsAttribute_m477 (SimpleXmlNode_t142 * __this, String_t* ___attributeKey, const MethodInfo* method)
{
	{
		Dictionary_2_t144 * L_0 = (__this->___Attributes_5);
		String_t* L_1 = ___attributeKey;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void Common.Xml.SimpleXmlNode::TraverseChildren(Common.Xml.SimpleXmlNode/NodeProcessor)
// Common.Xml.SimpleXmlNode/NodeProcessor
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNode_NodeProcessor.h"
// Common.Xml.SimpleXmlNode/NodeProcessor
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNode_NodeProcessorMethodDeclarations.h"
extern "C" void SimpleXmlNode_TraverseChildren_m478 (SimpleXmlNode_t142 * __this, NodeProcessor_t141 * ___visitor, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		List_1_t143 * L_0 = (__this->___Children_4);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Count() */, L_0);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0029;
	}

IL_0013:
	{
		NodeProcessor_t141 * L_2 = ___visitor;
		List_1_t143 * L_3 = (__this->___Children_4);
		int32_t L_4 = V_1;
		NullCheck(L_3);
		SimpleXmlNode_t142 * L_5 = (SimpleXmlNode_t142 *)VirtFuncInvoker1< SimpleXmlNode_t142 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Item(System.Int32) */, L_3, L_4);
		NullCheck(L_2);
		NodeProcessor_Invoke_m466(L_2, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// Common.Xml.SimpleXmlNodeList
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNodeList.h"
// Common.Xml.SimpleXmlNodeList
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNodeListMethodDeclarations.h"
// System.Void Common.Xml.SimpleXmlNodeList::.ctor()
// System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
extern TypeInfo* List_1_t143_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1551_MethodInfo_var;
extern "C" void SimpleXmlNodeList__ctor_m479 (SimpleXmlNodeList_t145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t143_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(143);
		List_1__ctor_m1551_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483714);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t143_il2cpp_TypeInfo_var);
		List_1__ctor_m1551(__this, /*hidden argument*/List_1__ctor_m1551_MethodInfo_var);
		return;
	}
}
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlNodeList::Pop()
extern "C" SimpleXmlNode_t142 * SimpleXmlNodeList_Pop_m480 (SimpleXmlNodeList_t145 * __this, const MethodInfo* method)
{
	SimpleXmlNode_t142 * V_0 = {0};
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Count() */, __this);
		SimpleXmlNode_t142 * L_1 = (SimpleXmlNode_t142 *)VirtFuncInvoker1< SimpleXmlNode_t142 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Item(System.Int32) */, __this, ((int32_t)((int32_t)L_0-(int32_t)1)));
		V_0 = L_1;
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Count() */, __this);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::RemoveAt(System.Int32) */, __this, ((int32_t)((int32_t)L_2-(int32_t)1)));
		SimpleXmlNode_t142 * L_3 = V_0;
		return L_3;
	}
}
// System.Void Common.Xml.SimpleXmlNodeList::Push(Common.Xml.SimpleXmlNode)
// Common.Xml.SimpleXmlNode
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNode.h"
extern "C" void SimpleXmlNodeList_Push_m481 (SimpleXmlNodeList_t145 * __this, SimpleXmlNode_t142 * ___node, const MethodInfo* method)
{
	{
		SimpleXmlNode_t142 * L_0 = ___node;
		VirtActionInvoker1< SimpleXmlNode_t142 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::Add(!0) */, __this, L_0);
		return;
	}
}
// Common.Xml.SimpleXmlReader
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlReader.h"
// Common.Xml.SimpleXmlReader
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlReaderMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Common.Xml.SimpleXmlNode>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_.h"
// System.Collections.Generic.List`1/Enumerator<Common.Xml.SimpleXmlNode>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_MethodDeclarations.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0MethodDeclarations.h"
// System.Void Common.Xml.SimpleXmlReader::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void SimpleXmlReader__ctor_m482 (SimpleXmlReader_t146 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.Xml.SimpleXmlReader::.cctor()
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t383_il2cpp_TypeInfo_var;
extern TypeInfo* SimpleXmlReader_t146_il2cpp_TypeInfo_var;
extern "C" void SimpleXmlReader__cctor_m483 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Char_t383_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		SimpleXmlReader_t146_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		uint16_t L_1 = ((int32_t)61);
		Object_t * L_2 = Box(Char_t383_il2cpp_TypeInfo_var, &L_1);
		uint16_t L_3 = ((int32_t)34);
		Object_t * L_4 = Box(Char_t383_il2cpp_TypeInfo_var, &L_3);
		String_t* L_5 = String_Concat_m1556(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/NULL);
		((SimpleXmlReader_t146_StaticFields*)SimpleXmlReader_t146_il2cpp_TypeInfo_var->static_fields)->___BEGIN_QUOTE_8 = L_5;
		return;
	}
}
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlReader::Read(System.String)
// System.String
#include "mscorlib_System_String.h"
// Common.Xml.SimpleXmlNode
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNodeMethodDeclarations.h"
// Common.Xml.SimpleXmlReader
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlReaderMethodDeclarations.h"
extern TypeInfo* SimpleXmlNode_t142_il2cpp_TypeInfo_var;
extern "C" SimpleXmlNode_t142 * SimpleXmlReader_Read_m484 (SimpleXmlReader_t146 * __this, String_t* ___xml, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleXmlNode_t142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	SimpleXmlNode_t142 * V_1 = {0};
	SimpleXmlNode_t142 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	String_t* V_6 = {0};
	bool V_7 = false;
	SimpleXmlNode_t142 * V_8 = {0};
	{
		V_0 = 0;
		SimpleXmlNode_t142 * L_0 = (SimpleXmlNode_t142 *)il2cpp_codegen_object_new (SimpleXmlNode_t142_il2cpp_TypeInfo_var);
		SimpleXmlNode__ctor_m469(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		SimpleXmlNode_t142 * L_1 = V_1;
		V_2 = L_1;
		String_t* L_2 = ___xml;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1336(L_2, /*hidden argument*/NULL);
		V_3 = L_3;
	}

IL_0011:
	{
		String_t* L_4 = ___xml;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = String_IndexOf_m1557(L_4, ((int32_t)60), L_5, /*hidden argument*/NULL);
		V_4 = L_6;
		int32_t L_7 = V_4;
		if ((((int32_t)L_7) < ((int32_t)0)))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_8 = V_4;
		int32_t L_9 = V_3;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0031;
		}
	}

IL_002c:
	{
		goto IL_00f7;
	}

IL_0031:
	{
		int32_t L_10 = V_4;
		V_4 = ((int32_t)((int32_t)L_10+(int32_t)1));
		String_t* L_11 = ___xml;
		int32_t L_12 = V_4;
		NullCheck(L_11);
		int32_t L_13 = String_IndexOf_m1557(L_11, ((int32_t)62), L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) < ((int32_t)0)))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_15 = V_0;
		int32_t L_16 = V_3;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0055;
		}
	}

IL_0050:
	{
		goto IL_00f7;
	}

IL_0055:
	{
		int32_t L_17 = V_0;
		int32_t L_18 = V_4;
		V_5 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
		String_t* L_19 = ___xml;
		int32_t L_20 = V_4;
		int32_t L_21 = V_5;
		NullCheck(L_19);
		String_t* L_22 = String_Substring_m1558(L_19, L_20, L_21, /*hidden argument*/NULL);
		V_6 = L_22;
		String_t* L_23 = V_6;
		NullCheck(L_23);
		uint16_t L_24 = String_get_Chars_m1559(L_23, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0082;
		}
	}
	{
		SimpleXmlNode_t142 * L_25 = V_2;
		NullCheck(L_25);
		SimpleXmlNode_t142 * L_26 = (L_25->___ParentNode_3);
		V_2 = L_26;
		goto IL_0011;
	}

IL_0082:
	{
		V_7 = 1;
		String_t* L_27 = V_6;
		NullCheck(L_27);
		uint16_t L_28 = String_get_Chars_m1559(L_27, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_28) == ((int32_t)((int32_t)33))))
		{
			goto IL_00a3;
		}
	}
	{
		String_t* L_29 = V_6;
		NullCheck(L_29);
		uint16_t L_30 = String_get_Chars_m1559(L_29, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)63)))))
		{
			goto IL_00a6;
		}
	}

IL_00a3:
	{
		V_7 = 0;
	}

IL_00a6:
	{
		String_t* L_31 = V_6;
		int32_t L_32 = V_5;
		NullCheck(L_31);
		uint16_t L_33 = String_get_Chars_m1559(L_31, ((int32_t)((int32_t)L_32-(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_33) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00c9;
		}
	}
	{
		String_t* L_34 = V_6;
		int32_t L_35 = V_5;
		NullCheck(L_34);
		String_t* L_36 = String_Substring_m1558(L_34, 0, ((int32_t)((int32_t)L_35-(int32_t)1)), /*hidden argument*/NULL);
		V_6 = L_36;
		V_7 = 0;
	}

IL_00c9:
	{
		String_t* L_37 = V_6;
		SimpleXmlNode_t142 * L_38 = SimpleXmlReader_ParseTag_m485(__this, L_37, /*hidden argument*/NULL);
		V_8 = L_38;
		SimpleXmlNode_t142 * L_39 = V_8;
		SimpleXmlNode_t142 * L_40 = V_2;
		NullCheck(L_39);
		L_39->___ParentNode_3 = L_40;
		SimpleXmlNode_t142 * L_41 = V_2;
		NullCheck(L_41);
		List_1_t143 * L_42 = (L_41->___Children_4);
		SimpleXmlNode_t142 * L_43 = V_8;
		NullCheck(L_42);
		VirtActionInvoker1< SimpleXmlNode_t142 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::Add(!0) */, L_42, L_43);
		bool L_44 = V_7;
		if (!L_44)
		{
			goto IL_00f2;
		}
	}
	{
		SimpleXmlNode_t142 * L_45 = V_8;
		V_2 = L_45;
	}

IL_00f2:
	{
		goto IL_0011;
	}

IL_00f7:
	{
		SimpleXmlNode_t142 * L_46 = V_1;
		return L_46;
	}
}
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlReader::ParseTag(System.String)
extern TypeInfo* SimpleXmlNode_t142_il2cpp_TypeInfo_var;
extern "C" SimpleXmlNode_t142 * SimpleXmlReader_ParseTag_m485 (SimpleXmlReader_t146 * __this, String_t* ___xmlTag, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleXmlNode_t142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		s_Il2CppMethodIntialized = true;
	}
	SimpleXmlNode_t142 * V_0 = {0};
	int32_t V_1 = 0;
	String_t* V_2 = {0};
	String_t* V_3 = {0};
	{
		SimpleXmlNode_t142 * L_0 = (SimpleXmlNode_t142 *)il2cpp_codegen_object_new (SimpleXmlNode_t142_il2cpp_TypeInfo_var);
		SimpleXmlNode__ctor_m469(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___xmlTag;
		NullCheck(L_1);
		int32_t L_2 = String_IndexOf_m1557(L_1, ((int32_t)32), 0, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		SimpleXmlNode_t142 * L_4 = V_0;
		String_t* L_5 = ___xmlTag;
		NullCheck(L_4);
		L_4->___TagName_2 = L_5;
		SimpleXmlNode_t142 * L_6 = V_0;
		return L_6;
	}

IL_0020:
	{
		String_t* L_7 = ___xmlTag;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		String_t* L_9 = String_Substring_m1558(L_7, 0, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		SimpleXmlNode_t142 * L_10 = V_0;
		String_t* L_11 = V_2;
		NullCheck(L_10);
		L_10->___TagName_2 = L_11;
		String_t* L_12 = ___xmlTag;
		int32_t L_13 = V_1;
		String_t* L_14 = ___xmlTag;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1336(L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_1;
		NullCheck(L_12);
		String_t* L_17 = String_Substring_m1558(L_12, L_13, ((int32_t)((int32_t)L_15-(int32_t)L_16)), /*hidden argument*/NULL);
		V_3 = L_17;
		String_t* L_18 = V_3;
		SimpleXmlNode_t142 * L_19 = V_0;
		SimpleXmlNode_t142 * L_20 = SimpleXmlReader_ParseAttributes_m486(__this, L_18, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlReader::ParseAttributes(System.String,Common.Xml.SimpleXmlNode)
// Common.Xml.SimpleXmlNode
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNode.h"
extern TypeInfo* SimpleXmlReader_t146_il2cpp_TypeInfo_var;
extern "C" SimpleXmlNode_t142 * SimpleXmlReader_ParseAttributes_m486 (SimpleXmlReader_t146 * __this, String_t* ___xmlTag, SimpleXmlNode_t142 * ___node, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleXmlReader_t146_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	int32_t V_4 = 0;
	String_t* V_5 = {0};
	{
		V_0 = 0;
	}

IL_0002:
	{
		String_t* L_0 = ___xmlTag;
		IL2CPP_RUNTIME_CLASS_INIT(SimpleXmlReader_t146_il2cpp_TypeInfo_var);
		String_t* L_1 = ((SimpleXmlReader_t146_StaticFields*)SimpleXmlReader_t146_il2cpp_TypeInfo_var->static_fields)->___BEGIN_QUOTE_8;
		int32_t L_2 = V_0;
		NullCheck(L_0);
		int32_t L_3 = String_IndexOf_m1560(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_5 = V_1;
		String_t* L_6 = ___xmlTag;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1336(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)L_7)))
		{
			goto IL_0027;
		}
	}

IL_0022:
	{
		goto IL_00a1;
	}

IL_0027:
	{
		String_t* L_8 = ___xmlTag;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = String_LastIndexOf_m1561(L_8, ((int32_t)32), L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) < ((int32_t)0)))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_12 = V_2;
		String_t* L_13 = ___xmlTag;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m1336(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_12) <= ((int32_t)L_14)))
		{
			goto IL_0049;
		}
	}

IL_0044:
	{
		goto IL_00a1;
	}

IL_0049:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
		String_t* L_16 = ___xmlTag;
		int32_t L_17 = V_2;
		int32_t L_18 = V_1;
		int32_t L_19 = V_2;
		NullCheck(L_16);
		String_t* L_20 = String_Substring_m1558(L_16, L_17, ((int32_t)((int32_t)L_18-(int32_t)L_19)), /*hidden argument*/NULL);
		V_3 = L_20;
		int32_t L_21 = V_1;
		V_1 = ((int32_t)((int32_t)L_21+(int32_t)2));
		String_t* L_22 = ___xmlTag;
		int32_t L_23 = V_1;
		NullCheck(L_22);
		int32_t L_24 = String_IndexOf_m1557(L_22, ((int32_t)34), L_23, /*hidden argument*/NULL);
		V_0 = L_24;
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) < ((int32_t)0)))
		{
			goto IL_0079;
		}
	}
	{
		int32_t L_26 = V_0;
		String_t* L_27 = ___xmlTag;
		NullCheck(L_27);
		int32_t L_28 = String_get_Length_m1336(L_27, /*hidden argument*/NULL);
		if ((((int32_t)L_26) <= ((int32_t)L_28)))
		{
			goto IL_007e;
		}
	}

IL_0079:
	{
		goto IL_00a1;
	}

IL_007e:
	{
		int32_t L_29 = V_0;
		int32_t L_30 = V_1;
		V_4 = ((int32_t)((int32_t)L_29-(int32_t)L_30));
		String_t* L_31 = ___xmlTag;
		int32_t L_32 = V_1;
		int32_t L_33 = V_4;
		NullCheck(L_31);
		String_t* L_34 = String_Substring_m1558(L_31, L_32, L_33, /*hidden argument*/NULL);
		V_5 = L_34;
		SimpleXmlNode_t142 * L_35 = ___node;
		NullCheck(L_35);
		Dictionary_2_t144 * L_36 = (L_35->___Attributes_5);
		String_t* L_37 = V_3;
		String_t* L_38 = V_5;
		NullCheck(L_36);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_36, L_37, L_38);
		goto IL_0002;
	}

IL_00a1:
	{
		SimpleXmlNode_t142 * L_39 = ___node;
		return L_39;
	}
}
// System.Void Common.Xml.SimpleXmlReader::PrintXML(Common.Xml.SimpleXmlNode,System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Common.Xml.SimpleXmlNode>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_MethodDeclarations.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0MethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern TypeInfo* StringU5BU5D_t137_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t423_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t421_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1562_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1563_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m1564_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1565_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1566_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1567_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1568_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1569_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral43;
extern Il2CppCodeGenString* _stringLiteral59;
extern Il2CppCodeGenString* _stringLiteral39;
extern "C" void SimpleXmlReader_PrintXML_m487 (SimpleXmlReader_t146 * __this, SimpleXmlNode_t142 * ___node, int32_t ___indent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enumerator_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(146);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Enumerator_t421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(147);
		List_1_GetEnumerator_m1562_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483716);
		Enumerator_get_Current_m1563_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483717);
		Dictionary_2_GetEnumerator_m1564_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483718);
		Enumerator_get_Current_m1565_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483719);
		KeyValuePair_2_get_Key_m1566_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483720);
		KeyValuePair_2_get_Value_m1567_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483721);
		Enumerator_MoveNext_m1568_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483722);
		Enumerator_MoveNext_m1569_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483723);
		_stringLiteral44 = il2cpp_codegen_string_literal_from_index(44);
		_stringLiteral58 = il2cpp_codegen_string_literal_from_index(58);
		_stringLiteral43 = il2cpp_codegen_string_literal_from_index(43);
		_stringLiteral59 = il2cpp_codegen_string_literal_from_index(59);
		_stringLiteral39 = il2cpp_codegen_string_literal_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	SimpleXmlNode_t142 * V_0 = {0};
	Enumerator_t421  V_1 = {0};
	String_t* V_2 = {0};
	KeyValuePair_2_t422  V_3 = {0};
	Enumerator_t423  V_4 = {0};
	String_t* V_5 = {0};
	int32_t V_6 = 0;
	String_t* V_7 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___indent;
		___indent = ((int32_t)((int32_t)L_0+(int32_t)1));
		SimpleXmlNode_t142 * L_1 = ___node;
		NullCheck(L_1);
		List_1_t143 * L_2 = (L_1->___Children_4);
		NullCheck(L_2);
		Enumerator_t421  L_3 = List_1_GetEnumerator_m1562(L_2, /*hidden argument*/List_1_GetEnumerator_m1562_MethodInfo_var);
		V_1 = L_3;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00e7;
		}

IL_0016:
		{
			SimpleXmlNode_t142 * L_4 = Enumerator_get_Current_m1563((&V_1), /*hidden argument*/Enumerator_get_Current_m1563_MethodInfo_var);
			V_0 = L_4;
			V_2 = _stringLiteral44;
			SimpleXmlNode_t142 * L_5 = V_0;
			NullCheck(L_5);
			Dictionary_2_t144 * L_6 = (L_5->___Attributes_5);
			NullCheck(L_6);
			Enumerator_t423  L_7 = Dictionary_2_GetEnumerator_m1564(L_6, /*hidden argument*/Dictionary_2_GetEnumerator_m1564_MethodInfo_var);
			V_4 = L_7;
		}

IL_0031:
		try
		{ // begin try (depth: 2)
			{
				goto IL_007e;
			}

IL_0036:
			{
				KeyValuePair_2_t422  L_8 = Enumerator_get_Current_m1565((&V_4), /*hidden argument*/Enumerator_get_Current_m1565_MethodInfo_var);
				V_3 = L_8;
				String_t* L_9 = V_2;
				V_7 = L_9;
				StringU5BU5D_t137* L_10 = ((StringU5BU5D_t137*)SZArrayNew(StringU5BU5D_t137_il2cpp_TypeInfo_var, 6));
				String_t* L_11 = V_7;
				NullCheck(L_10);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
				ArrayElementTypeCheck (L_10, L_11);
				*((String_t**)(String_t**)SZArrayLdElema(L_10, 0, sizeof(String_t*))) = (String_t*)L_11;
				StringU5BU5D_t137* L_12 = L_10;
				NullCheck(L_12);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
				ArrayElementTypeCheck (L_12, _stringLiteral58);
				*((String_t**)(String_t**)SZArrayLdElema(L_12, 1, sizeof(String_t*))) = (String_t*)_stringLiteral58;
				StringU5BU5D_t137* L_13 = L_12;
				String_t* L_14 = KeyValuePair_2_get_Key_m1566((&V_3), /*hidden argument*/KeyValuePair_2_get_Key_m1566_MethodInfo_var);
				NullCheck(L_13);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
				ArrayElementTypeCheck (L_13, L_14);
				*((String_t**)(String_t**)SZArrayLdElema(L_13, 2, sizeof(String_t*))) = (String_t*)L_14;
				StringU5BU5D_t137* L_15 = L_13;
				NullCheck(L_15);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 3);
				ArrayElementTypeCheck (L_15, _stringLiteral43);
				*((String_t**)(String_t**)SZArrayLdElema(L_15, 3, sizeof(String_t*))) = (String_t*)_stringLiteral43;
				StringU5BU5D_t137* L_16 = L_15;
				String_t* L_17 = KeyValuePair_2_get_Value_m1567((&V_3), /*hidden argument*/KeyValuePair_2_get_Value_m1567_MethodInfo_var);
				NullCheck(L_16);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
				ArrayElementTypeCheck (L_16, L_17);
				*((String_t**)(String_t**)SZArrayLdElema(L_16, 4, sizeof(String_t*))) = (String_t*)L_17;
				StringU5BU5D_t137* L_18 = L_16;
				NullCheck(L_18);
				IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 5);
				ArrayElementTypeCheck (L_18, _stringLiteral59);
				*((String_t**)(String_t**)SZArrayLdElema(L_18, 5, sizeof(String_t*))) = (String_t*)_stringLiteral59;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_19 = String_Concat_m1461(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
				V_2 = L_19;
			}

IL_007e:
			{
				bool L_20 = Enumerator_MoveNext_m1568((&V_4), /*hidden argument*/Enumerator_MoveNext_m1568_MethodInfo_var);
				if (L_20)
				{
					goto IL_0036;
				}
			}

IL_008a:
			{
				IL2CPP_LEAVE(0x9C, FINALLY_008f);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t359 *)e.ex;
			goto FINALLY_008f;
		}

FINALLY_008f:
		{ // begin finally (depth: 2)
			Enumerator_t423  L_21 = V_4;
			Enumerator_t423  L_22 = L_21;
			Object_t * L_23 = Box(Enumerator_t423_il2cpp_TypeInfo_var, &L_22);
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_23);
			IL2CPP_END_FINALLY(143)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(143)
		{
			IL2CPP_JUMP_TBL(0x9C, IL_009c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
		}

IL_009c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_24 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
			V_5 = L_24;
			V_6 = 0;
			goto IL_00bf;
		}

IL_00ab:
		{
			String_t* L_25 = V_5;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_26 = String_Concat_m1319(NULL /*static, unused*/, L_25, _stringLiteral39, /*hidden argument*/NULL);
			V_5 = L_26;
			int32_t L_27 = V_6;
			V_6 = ((int32_t)((int32_t)L_27+(int32_t)1));
		}

IL_00bf:
		{
			int32_t L_28 = V_6;
			int32_t L_29 = ___indent;
			if ((((int32_t)L_28) < ((int32_t)L_29)))
			{
				goto IL_00ab;
			}
		}

IL_00c7:
		{
			String_t* L_30 = V_5;
			SimpleXmlNode_t142 * L_31 = V_0;
			NullCheck(L_31);
			String_t* L_32 = (L_31->___TagName_2);
			String_t* L_33 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_34 = String_Concat_m1452(NULL /*static, unused*/, L_30, _stringLiteral44, L_32, L_33, /*hidden argument*/NULL);
			Debug_Log_m1411(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
			SimpleXmlNode_t142 * L_35 = V_0;
			int32_t L_36 = ___indent;
			SimpleXmlReader_PrintXML_m487(__this, L_35, L_36, /*hidden argument*/NULL);
		}

IL_00e7:
		{
			bool L_37 = Enumerator_MoveNext_m1569((&V_1), /*hidden argument*/Enumerator_MoveNext_m1569_MethodInfo_var);
			if (L_37)
			{
				goto IL_0016;
			}
		}

IL_00f3:
		{
			IL2CPP_LEAVE(0x104, FINALLY_00f8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_00f8;
	}

FINALLY_00f8:
	{ // begin finally (depth: 1)
		Enumerator_t421  L_38 = V_1;
		Enumerator_t421  L_39 = L_38;
		Object_t * L_40 = Box(Enumerator_t421_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_40);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_40);
		IL2CPP_END_FINALLY(248)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(248)
	{
		IL2CPP_JUMP_TBL(0x104, IL_0104)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0104:
	{
		return;
	}
}
// XmlTest
#include "AssemblyU2DCSharp_XmlTest.h"
// XmlTest
#include "AssemblyU2DCSharp_XmlTestMethodDeclarations.h"
// UnityEngine.TextAsset
#include "UnityEngine_UnityEngine_TextAsset.h"
// UnityEngine.TextAsset
#include "UnityEngine_UnityEngine_TextAssetMethodDeclarations.h"
// System.Void XmlTest::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void XmlTest__ctor_m488 (XmlTest_t147 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void XmlTest::Awake()
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
// Common.Xml.SimpleXmlReader
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlReaderMethodDeclarations.h"
// UnityEngine.TextAsset
#include "UnityEngine_UnityEngine_TextAssetMethodDeclarations.h"
extern TypeInfo* SimpleXmlReader_t146_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral60;
extern "C" void XmlTest_Awake_m489 (XmlTest_t147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleXmlReader_t146_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral60 = il2cpp_codegen_string_literal_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	SimpleXmlReader_t146 * V_0 = {0};
	{
		TextAsset_t148 * L_0 = (__this->___sampleXml_2);
		Assertion_AssertNotNull_m25(NULL /*static, unused*/, L_0, _stringLiteral60, /*hidden argument*/NULL);
		SimpleXmlReader_t146 * L_1 = (SimpleXmlReader_t146 *)il2cpp_codegen_object_new (SimpleXmlReader_t146_il2cpp_TypeInfo_var);
		SimpleXmlReader__ctor_m482(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		SimpleXmlReader_t146 * L_2 = V_0;
		SimpleXmlReader_t146 * L_3 = V_0;
		TextAsset_t148 * L_4 = (__this->___sampleXml_2);
		NullCheck(L_4);
		String_t* L_5 = TextAsset_get_text_m1570(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		SimpleXmlNode_t142 * L_6 = SimpleXmlReader_Read_m484(L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		SimpleXmlReader_PrintXML_m487(L_2, L_6, 0, /*hidden argument*/NULL);
		return;
	}
}
// Common.XmlVariables
#include "AssemblyU2DCSharp_Common_XmlVariables.h"
// Common.XmlVariables
#include "AssemblyU2DCSharp_Common_XmlVariablesMethodDeclarations.h"
// System.Void Common.XmlVariables::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void XmlVariables__ctor_m490 (XmlVariables_t149 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.XmlVariables::Awake()
// Assertion
#include "AssemblyU2DCSharp_AssertionMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6MethodDeclarations.h"
// Common.XmlVariables
#include "AssemblyU2DCSharp_Common_XmlVariablesMethodDeclarations.h"
extern TypeInfo* Dictionary_2_t144_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1552_MethodInfo_var;
extern "C" void XmlVariables_Awake_m491 (XmlVariables_t149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		Dictionary_2__ctor_m1552_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483715);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextAsset_t148 * L_0 = (__this->___xmlFile_7);
		Assertion_AssertNotNull_m26(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Dictionary_2_t144 * L_1 = (Dictionary_2_t144 *)il2cpp_codegen_object_new (Dictionary_2_t144_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1552(L_1, /*hidden argument*/Dictionary_2__ctor_m1552_MethodInfo_var);
		__this->___varMap_8 = L_1;
		XmlVariables_Parse_m492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common.XmlVariables::Parse()
// Common.Xml.SimpleXmlReader
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlReaderMethodDeclarations.h"
// UnityEngine.TextAsset
#include "UnityEngine_UnityEngine_TextAssetMethodDeclarations.h"
// Common.Xml.SimpleXmlNode
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNodeMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern TypeInfo* SimpleXmlReader_t146_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral61;
extern Il2CppCodeGenString* _stringLiteral62;
extern Il2CppCodeGenString* _stringLiteral63;
extern Il2CppCodeGenString* _stringLiteral64;
extern "C" void XmlVariables_Parse_m492 (XmlVariables_t149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleXmlReader_t146_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral61 = il2cpp_codegen_string_literal_from_index(61);
		_stringLiteral62 = il2cpp_codegen_string_literal_from_index(62);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		_stringLiteral64 = il2cpp_codegen_string_literal_from_index(64);
		s_Il2CppMethodIntialized = true;
	}
	SimpleXmlReader_t146 * V_0 = {0};
	SimpleXmlNode_t142 * V_1 = {0};
	int32_t V_2 = 0;
	SimpleXmlNode_t142 * V_3 = {0};
	String_t* V_4 = {0};
	String_t* V_5 = {0};
	{
		SimpleXmlReader_t146 * L_0 = (SimpleXmlReader_t146 *)il2cpp_codegen_object_new (SimpleXmlReader_t146_il2cpp_TypeInfo_var);
		SimpleXmlReader__ctor_m482(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		SimpleXmlReader_t146 * L_1 = V_0;
		TextAsset_t148 * L_2 = (__this->___xmlFile_7);
		NullCheck(L_2);
		String_t* L_3 = TextAsset_get_text_m1570(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		SimpleXmlNode_t142 * L_4 = SimpleXmlReader_Read_m484(L_1, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		SimpleXmlNode_t142 * L_5 = SimpleXmlNode_FindFirstNodeInChildren_m470(L_4, _stringLiteral61, /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = 0;
		goto IL_0078;
	}

IL_0029:
	{
		SimpleXmlNode_t142 * L_6 = V_1;
		NullCheck(L_6);
		List_1_t143 * L_7 = (L_6->___Children_4);
		int32_t L_8 = V_2;
		NullCheck(L_7);
		SimpleXmlNode_t142 * L_9 = (SimpleXmlNode_t142 *)VirtFuncInvoker1< SimpleXmlNode_t142 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Item(System.Int32) */, L_7, L_8);
		V_3 = L_9;
		SimpleXmlNode_t142 * L_10 = V_3;
		NullCheck(L_10);
		String_t* L_11 = (L_10->___TagName_2);
		NullCheck(_stringLiteral62);
		bool L_12 = String_Equals_m1339(_stringLiteral62, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0074;
		}
	}
	{
		SimpleXmlNode_t142 * L_13 = V_3;
		NullCheck(L_13);
		String_t* L_14 = SimpleXmlNode_GetAttribute_m473(L_13, _stringLiteral63, /*hidden argument*/NULL);
		V_4 = L_14;
		SimpleXmlNode_t142 * L_15 = V_3;
		NullCheck(L_15);
		String_t* L_16 = SimpleXmlNode_GetAttribute_m473(L_15, _stringLiteral64, /*hidden argument*/NULL);
		V_5 = L_16;
		Dictionary_2_t144 * L_17 = (__this->___varMap_8);
		String_t* L_18 = V_4;
		String_t* L_19 = V_5;
		NullCheck(L_17);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(20 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_17, L_18, L_19);
	}

IL_0074:
	{
		int32_t L_20 = V_2;
		V_2 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_21 = V_2;
		SimpleXmlNode_t142 * L_22 = V_1;
		NullCheck(L_22);
		List_1_t143 * L_23 = (L_22->___Children_4);
		NullCheck(L_23);
		int32_t L_24 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::get_Count() */, L_23);
		if ((((int32_t)L_21) < ((int32_t)L_24)))
		{
			goto IL_0029;
		}
	}
	{
		return;
	}
}
// System.String Common.XmlVariables::Get(System.String)
// System.String
#include "mscorlib_System_String.h"
extern "C" String_t* XmlVariables_Get_m493 (XmlVariables_t149 * __this, String_t* ___key, const MethodInfo* method)
{
	String_t* V_0 = {0};
	{
		V_0 = (String_t*)NULL;
		Dictionary_2_t144 * L_0 = (__this->___varMap_8);
		String_t* L_1 = ___key;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker2< bool, String_t*, String_t** >::Invoke(33 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, L_0, L_1, (&V_0));
		Assertion_Assert_m21(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Boolean Common.XmlVariables::GetAsBool(System.String)
extern "C" bool XmlVariables_GetAsBool_m494 (XmlVariables_t149 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key;
		String_t* L_1 = XmlVariables_Get_m493(__this, L_0, /*hidden argument*/NULL);
		bool L_2 = XmlVariables_ToBool_m495(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Common.XmlVariables::ToBool(System.String)
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
extern TypeInfo* XmlVariables_t149_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t68_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1438_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral24;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral65;
extern "C" bool XmlVariables_ToBool_m495 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlVariables_t149_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		Dictionary_2_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(64);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Dictionary_2__ctor_m1438_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483672);
		_stringLiteral24 = il2cpp_codegen_string_literal_from_index(24);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral65 = il2cpp_codegen_string_literal_from_index(65);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Dictionary_2_t68 * V_1 = {0};
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___value;
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_005f;
		}
	}
	{
		Dictionary_2_t68 * L_2 = ((XmlVariables_t149_StaticFields*)XmlVariables_t149_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map1_9;
		if (L_2)
		{
			goto IL_0037;
		}
	}
	{
		Dictionary_2_t68 * L_3 = (Dictionary_2_t68 *)il2cpp_codegen_object_new (Dictionary_2_t68_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1438(L_3, 2, /*hidden argument*/Dictionary_2__ctor_m1438_MethodInfo_var);
		V_1 = L_3;
		Dictionary_2_t68 * L_4 = V_1;
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_4, _stringLiteral24, 0);
		Dictionary_2_t68 * L_5 = V_1;
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_5, _stringLiteral23, 1);
		Dictionary_2_t68 * L_6 = V_1;
		((XmlVariables_t149_StaticFields*)XmlVariables_t149_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map1_9 = L_6;
	}

IL_0037:
	{
		Dictionary_2_t68 * L_7 = ((XmlVariables_t149_StaticFields*)XmlVariables_t149_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map1_9;
		String_t* L_8 = V_0;
		NullCheck(L_7);
		bool L_9 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(33 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_7, L_8, (&V_2));
		if (!L_9)
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_10 = V_2;
		if (!L_10)
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) == ((int32_t)1)))
		{
			goto IL_005d;
		}
	}
	{
		goto IL_005f;
	}

IL_005b:
	{
		return 1;
	}

IL_005d:
	{
		return 0;
	}

IL_005f:
	{
		String_t* L_12 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1319(NULL /*static, unused*/, _stringLiteral65, L_12, /*hidden argument*/NULL);
		Assertion_Assert_m22(NULL /*static, unused*/, 0, L_13, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Int32 Common.XmlVariables::GetAsInt(System.String)
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
extern "C" int32_t XmlVariables_GetAsInt_m496 (XmlVariables_t149 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key;
		int32_t L_1 = Int32_Parse_m1554(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single Common.XmlVariables::GetAsFloat(System.String)
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
extern "C" float XmlVariables_GetAsFloat_m497 (XmlVariables_t149 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key;
		float L_1 = Single_Parse_m1555(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// SwarmItem/STATE
#include "AssemblyU2DCSharp_SwarmItem_STATE.h"
// SwarmItem/STATE
#include "AssemblyU2DCSharp_SwarmItem_STATEMethodDeclarations.h"
// System.Void SwarmItem::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void SwarmItem__ctor_m498 (SwarmItem_t124 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// SwarmItem/STATE SwarmItem::get_State()
extern "C" int32_t SwarmItem_get_State_m499 (SwarmItem_t124 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____state_2);
		return L_0;
	}
}
// System.Void SwarmItem::set_State(SwarmItem/STATE)
// SwarmItem/STATE
#include "AssemblyU2DCSharp_SwarmItem_STATE.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
extern "C" void SwarmItem_set_State_m500 (SwarmItem_t124 * __this, int32_t ___value, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = {0};
	{
		int32_t L_0 = (__this->____state_2);
		int32_t L_1 = ___value;
		V_0 = ((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int32_t L_2 = ___value;
		__this->____state_2 = L_2;
		int32_t L_3 = (__this->____state_2);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0066;
	}

IL_002d:
	{
		GameObject_t155 * L_6 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m1538(L_6, 0, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_003e:
	{
		GameObject_t155 * L_7 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_m1538(L_7, 1, /*hidden argument*/NULL);
		float L_8 = (__this->___minimumLifeSpan_8);
		float L_9 = (__this->___maximumLifeSpan_9);
		float L_10 = Random_Range_m1571(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		__this->____lifeSpanLeft_6 = L_10;
		goto IL_0066;
	}

IL_0066:
	{
		bool L_11 = V_0;
		if (!L_11)
		{
			goto IL_0072;
		}
	}
	{
		VirtActionInvoker0::Invoke(4 /* System.Void SwarmItem::OnStateChange() */, __this);
	}

IL_0072:
	{
		return;
	}
}
// System.Void SwarmItem::OnStateChange()
extern "C" void SwarmItem_OnStateChange_m501 (SwarmItem_t124 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.Transform SwarmItem::get_ThisTransform()
extern "C" Transform_t35 * SwarmItem_get_ThisTransform_m502 (SwarmItem_t124 * __this, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = (__this->____thisTransform_4);
		return L_0;
	}
}
// UnityEngine.Vector3 SwarmItem::get_Position()
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern "C" Vector3_t36  SwarmItem_get_Position_m503 (SwarmItem_t124 * __this, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = (__this->____thisTransform_4);
		NullCheck(L_0);
		Vector3_t36  L_1 = Transform_get_position_m1388(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void SwarmItem::set_Position(UnityEngine.Vector3)
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
extern "C" void SwarmItem_set_Position_m504 (SwarmItem_t124 * __this, Vector3_t36  ___value, const MethodInfo* method)
{
	{
		Transform_t35 * L_0 = (__this->____thisTransform_4);
		Vector3_t36  L_1 = ___value;
		NullCheck(L_0);
		Transform_set_position_m1383(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 SwarmItem::get_PrefabIndex()
extern "C" int32_t SwarmItem_get_PrefabIndex_m505 (SwarmItem_t124 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____prefabIndex_5);
		return L_0;
	}
}
// System.Void SwarmItem::Initialize(SwarmItemManager,System.Int32,System.Boolean)
// SwarmItemManager
#include "AssemblyU2DCSharp_SwarmItemManager.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// SwarmItem
#include "AssemblyU2DCSharp_SwarmItemMethodDeclarations.h"
extern "C" void SwarmItem_Initialize_m506 (SwarmItem_t124 * __this, SwarmItemManager_t111 * ___swarmItemManager, int32_t ___prefabIndex, bool ___debugEvents, const MethodInfo* method)
{
	{
		SwarmItemManager_t111 * L_0 = ___swarmItemManager;
		__this->____swarmItemManager_3 = L_0;
		int32_t L_1 = ___prefabIndex;
		__this->____prefabIndex_5 = L_1;
		bool L_2 = ___debugEvents;
		__this->____debugEvents_7 = L_2;
		Transform_t35 * L_3 = Component_get_transform_m1417(__this, /*hidden argument*/NULL);
		__this->____thisTransform_4 = L_3;
		SwarmItem_set_State_m500(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SwarmItem::OnSetParentTransform()
extern "C" void SwarmItem_OnSetParentTransform_m507 (SwarmItem_t124 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SwarmItem::Kill()
extern "C" void SwarmItem_Kill_m508 (SwarmItem_t124 * __this, const MethodInfo* method)
{
	{
		SwarmItem_set_State_m500(__this, 0, /*hidden argument*/NULL);
		SwarmItemManager_t111 * L_0 = (__this->____swarmItemManager_3);
		NullCheck(L_0);
		VirtActionInvoker1< SwarmItem_t124 * >::Invoke(7 /* System.Void SwarmItemManager::DeactiveItem(SwarmItem) */, L_0, __this);
		return;
	}
}
// System.Void SwarmItem::PreDestroy()
extern "C" void SwarmItem_PreDestroy_m509 (SwarmItem_t124 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SwarmItem::FrameUpdate()
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral66;
extern Il2CppCodeGenString* _stringLiteral67;
extern "C" void SwarmItem_FrameUpdate_m510 (SwarmItem_t124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral66 = il2cpp_codegen_string_literal_from_index(66);
		_stringLiteral67 = il2cpp_codegen_string_literal_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->____lifeSpanLeft_6);
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0079;
		}
	}
	{
		float L_1 = (__this->____lifeSpanLeft_6);
		float L_2 = Time_get_deltaTime_m1379(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->____lifeSpanLeft_6 = ((float)((float)L_1-(float)L_2));
		float L_3 = (__this->____lifeSpanLeft_6);
		if ((!(((float)L_3) <= ((float)(0.0f)))))
		{
			goto IL_0079;
		}
	}
	{
		VirtActionInvoker0::Invoke(7 /* System.Void SwarmItem::Kill() */, __this);
		bool L_4 = (__this->____debugEvents_7);
		if (!L_4)
		{
			goto IL_0079;
		}
	}
	{
		ObjectU5BU5D_t356* L_5 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 4));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, _stringLiteral66);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral66;
		ObjectU5BU5D_t356* L_6 = L_5;
		String_t* L_7 = Object_get_name_m1338(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t356* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, _stringLiteral67);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral67;
		ObjectU5BU5D_t356* L_9 = L_8;
		int32_t L_10 = Time_get_frameCount_m1572(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		Object_t * L_12 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 3, sizeof(Object_t *))) = (Object_t *)L_12;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1316(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// SwarmItemManager/PrefabItemLists
#include "AssemblyU2DCSharp_SwarmItemManager_PrefabItemListsMethodDeclarations.h"
// System.Void SwarmItemManager/PrefabItemLists::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.LinkedList`1<SwarmItem>
#include "System_System_Collections_Generic_LinkedList_1_gen_0MethodDeclarations.h"
// System.Collections.Generic.Stack`1<SwarmItem>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
extern TypeInfo* LinkedList_1_t152_il2cpp_TypeInfo_var;
extern TypeInfo* Stack_1_t153_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1__ctor_m1573_MethodInfo_var;
extern const MethodInfo* Stack_1__ctor_m1574_MethodInfo_var;
extern "C" void PrefabItemLists__ctor_m511 (PrefabItemLists_t151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LinkedList_1_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		Stack_1_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(150);
		LinkedList_1__ctor_m1573_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483724);
		Stack_1__ctor_m1574_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483725);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		LinkedList_1_t152 * L_0 = (LinkedList_1_t152 *)il2cpp_codegen_object_new (LinkedList_1_t152_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m1573(L_0, /*hidden argument*/LinkedList_1__ctor_m1573_MethodInfo_var);
		__this->___activeItems_0 = L_0;
		Stack_1_t153 * L_1 = (Stack_1_t153 *)il2cpp_codegen_object_new (Stack_1_t153_il2cpp_TypeInfo_var);
		Stack_1__ctor_m1574(L_1, /*hidden argument*/Stack_1__ctor_m1574_MethodInfo_var);
		__this->___inactiveItems_1 = L_1;
		return;
	}
}
// System.Int32 SwarmItemManager/PrefabItemLists::get_ItemCount()
extern "C" int32_t PrefabItemLists_get_ItemCount_m512 (PrefabItemLists_t151 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t152 * L_0 = (__this->___activeItems_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 System.Collections.Generic.LinkedList`1<SwarmItem>::get_Count() */, L_0);
		Stack_1_t153 * L_2 = (__this->___inactiveItems_1);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<SwarmItem>::get_Count() */, L_2);
		return ((int32_t)((int32_t)L_1+(int32_t)L_3));
	}
}
// SwarmItemManager/PrefabItem
#include "AssemblyU2DCSharp_SwarmItemManager_PrefabItemMethodDeclarations.h"
// System.Void SwarmItemManager/PrefabItem::.ctor()
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern "C" void PrefabItem__ctor_m513 (PrefabItem_t154 * __this, const MethodInfo* method)
{
	{
		__this->___inactiveThreshold_2 = (1.0f);
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
struct GameObject_t155;
struct Object_t;
// Declaration !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" Object_t * Object_Instantiate_TisObject_t_m1589_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Object_Instantiate_TisObject_t_m1589(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Object_Instantiate_TisObject_t_m1589_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t155_m1585(__this /* static, unused */, p0, method) (( GameObject_t155 * (*) (Object_t * /* static, unused */, GameObject_t155 *, const MethodInfo*))Object_Instantiate_TisObject_t_m1589_gshared)(__this /* static, unused */, p0, method)
// System.Void SwarmItemManager::.ctor()
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern "C" void SwarmItemManager__ctor_m514 (SwarmItemManager_t111 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m1322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SwarmItemManager::Initialize()
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// SwarmItemManager/PrefabItemLists
#include "AssemblyU2DCSharp_SwarmItemManager_PrefabItemListsMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern TypeInfo* PrefabItemListsU5BU5D_t156_il2cpp_TypeInfo_var;
extern TypeInfo* PrefabItemLists_t151_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t155_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral68;
extern Il2CppCodeGenString* _stringLiteral69;
extern Il2CppCodeGenString* _stringLiteral70;
extern Il2CppCodeGenString* _stringLiteral71;
extern "C" void SwarmItemManager_Initialize_m515 (SwarmItemManager_t111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		PrefabItemListsU5BU5D_t156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(151);
		PrefabItemLists_t151_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(152);
		GameObject_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(153);
		_stringLiteral68 = il2cpp_codegen_string_literal_from_index(68);
		_stringLiteral69 = il2cpp_codegen_string_literal_from_index(69);
		_stringLiteral70 = il2cpp_codegen_string_literal_from_index(70);
		_stringLiteral71 = il2cpp_codegen_string_literal_from_index(71);
		s_Il2CppMethodIntialized = true;
	}
	PrefabItem_t154 * V_0 = {0};
	PrefabItemU5BU5D_t157* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		PrefabItemU5BU5D_t157* L_0 = (__this->___itemPrefabs_10);
		NullCheck(L_0);
		if ((((int32_t)(((Array_t *)L_0)->max_length))))
		{
			goto IL_002c;
		}
	}
	{
		GameObject_t155 * L_1 = Component_get_gameObject_m1337(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m1338(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1575(NULL /*static, unused*/, _stringLiteral68, L_2, _stringLiteral69, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_002c:
	{
		PrefabItemU5BU5D_t157* L_4 = (__this->___itemPrefabs_10);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0064;
	}

IL_003a:
	{
		PrefabItemU5BU5D_t157* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = (*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_5, L_7, sizeof(PrefabItem_t154 *)));
		PrefabItem_t154 * L_8 = V_0;
		PrefabItem_t154 * L_9 = V_0;
		NullCheck(L_9);
		float L_10 = (L_9->___inactiveThreshold_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Clamp01_m1576(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->___inactiveThreshold_2 = L_11;
		PrefabItem_t154 * L_12 = V_0;
		PrefabItem_t154 * L_13 = V_0;
		NullCheck(L_13);
		float L_14 = (L_13->___inactivePrunePercentage_4);
		float L_15 = Mathf_Clamp01_m1576(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		L_12->___inactivePrunePercentage_4 = L_15;
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0064:
	{
		int32_t L_17 = V_2;
		PrefabItemU5BU5D_t157* L_18 = V_1;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)(((Array_t *)L_18)->max_length))))))
		{
			goto IL_003a;
		}
	}
	{
		PrefabItemU5BU5D_t157* L_19 = (__this->___itemPrefabs_10);
		NullCheck(L_19);
		__this->____prefabItemLists_8 = ((PrefabItemListsU5BU5D_t156*)SZArrayNew(PrefabItemListsU5BU5D_t156_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_19)->max_length)))));
		V_3 = 0;
		goto IL_00aa;
	}

IL_0087:
	{
		PrefabItemListsU5BU5D_t156* L_20 = (__this->____prefabItemLists_8);
		int32_t L_21 = V_3;
		PrefabItemLists_t151 * L_22 = (PrefabItemLists_t151 *)il2cpp_codegen_object_new (PrefabItemLists_t151_il2cpp_TypeInfo_var);
		PrefabItemLists__ctor_m511(L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		ArrayElementTypeCheck (L_20, L_22);
		*((PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_20, L_21, sizeof(PrefabItemLists_t151 *))) = (PrefabItemLists_t151 *)L_22;
		PrefabItemListsU5BU5D_t156* L_23 = (__this->____prefabItemLists_8);
		int32_t L_24 = V_3;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_23, L_25, sizeof(PrefabItemLists_t151 *))));
		(*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_23, L_25, sizeof(PrefabItemLists_t151 *)))->___inactivePruneTimeLeft_2 = (0.0f);
		int32_t L_26 = V_3;
		V_3 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00aa:
	{
		int32_t L_27 = V_3;
		PrefabItemListsU5BU5D_t156* L_28 = (__this->____prefabItemLists_8);
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)(((Array_t *)L_28)->max_length))))))
		{
			goto IL_0087;
		}
	}
	{
		GameObject_t155 * L_29 = (GameObject_t155 *)il2cpp_codegen_object_new (GameObject_t155_il2cpp_TypeInfo_var);
		GameObject__ctor_m1577(L_29, _stringLiteral70, /*hidden argument*/NULL);
		__this->____go_3 = L_29;
		GameObject_t155 * L_30 = (__this->____go_3);
		NullCheck(L_30);
		Transform_t35 * L_31 = GameObject_get_transform_m1349(L_30, /*hidden argument*/NULL);
		__this->____activeParentTransform_5 = L_31;
		Transform_t35 * L_32 = (__this->____activeParentTransform_5);
		Transform_t35 * L_33 = Component_get_transform_m1417(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_set_parent_m1329(L_32, L_33, /*hidden argument*/NULL);
		Transform_t35 * L_34 = (__this->____activeParentTransform_5);
		Vector3_t36  L_35 = Vector3_get_zero_m1578(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_localPosition_m1384(L_34, L_35, /*hidden argument*/NULL);
		Transform_t35 * L_36 = (__this->____activeParentTransform_5);
		Quaternion_t41  L_37 = Quaternion_get_identity_m1579(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_set_localRotation_m1580(L_36, L_37, /*hidden argument*/NULL);
		Transform_t35 * L_38 = (__this->____activeParentTransform_5);
		Vector3_t36  L_39 = Vector3_get_one_m1581(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_set_localScale_m1393(L_38, L_39, /*hidden argument*/NULL);
		GameObject_t155 * L_40 = (GameObject_t155 *)il2cpp_codegen_object_new (GameObject_t155_il2cpp_TypeInfo_var);
		GameObject__ctor_m1577(L_40, _stringLiteral71, /*hidden argument*/NULL);
		__this->____go_3 = L_40;
		GameObject_t155 * L_41 = (__this->____go_3);
		NullCheck(L_41);
		Transform_t35 * L_42 = GameObject_get_transform_m1349(L_41, /*hidden argument*/NULL);
		__this->____inactiveParentTransform_6 = L_42;
		Transform_t35 * L_43 = (__this->____inactiveParentTransform_6);
		Transform_t35 * L_44 = Component_get_transform_m1417(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_set_parent_m1329(L_43, L_44, /*hidden argument*/NULL);
		Transform_t35 * L_45 = (__this->____inactiveParentTransform_6);
		Vector3_t36  L_46 = Vector3_get_zero_m1578(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_set_localPosition_m1384(L_45, L_46, /*hidden argument*/NULL);
		Transform_t35 * L_47 = (__this->____inactiveParentTransform_6);
		Quaternion_t41  L_48 = Quaternion_get_identity_m1579(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_set_localRotation_m1580(L_47, L_48, /*hidden argument*/NULL);
		Transform_t35 * L_49 = (__this->____inactiveParentTransform_6);
		Vector3_t36  L_50 = Vector3_get_one_m1581(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_set_localScale_m1393(L_49, L_50, /*hidden argument*/NULL);
		return;
	}
}
// SwarmItem SwarmItemManager::ActivateItem()
extern "C" SwarmItem_t124 * SwarmItemManager_ActivateItem_m516 (SwarmItemManager_t111 * __this, const MethodInfo* method)
{
	{
		SwarmItem_t124 * L_0 = (SwarmItem_t124 *)VirtFuncInvoker1< SwarmItem_t124 *, int32_t >::Invoke(6 /* SwarmItem SwarmItemManager::ActivateItem(System.Int32) */, __this, 0);
		return L_0;
	}
}
// SwarmItem SwarmItemManager::ActivateItem(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// System.Collections.Generic.Stack`1<SwarmItem>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
// System.Collections.Generic.LinkedList`1<SwarmItem>
#include "System_System_Collections_Generic_LinkedList_1_gen_0MethodDeclarations.h"
// SwarmItemManager
#include "AssemblyU2DCSharp_SwarmItemManagerMethodDeclarations.h"
// SwarmItem
#include "AssemblyU2DCSharp_SwarmItemMethodDeclarations.h"
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t388_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Pop_m1582_MethodInfo_var;
extern const MethodInfo* LinkedList_1_AddLast_m1583_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral72;
extern Il2CppCodeGenString* _stringLiteral73;
extern Il2CppCodeGenString* _stringLiteral74;
extern Il2CppCodeGenString* _stringLiteral67;
extern Il2CppCodeGenString* _stringLiteral75;
extern Il2CppCodeGenString* _stringLiteral76;
extern Il2CppCodeGenString* _stringLiteral77;
extern Il2CppCodeGenString* _stringLiteral78;
extern "C" SwarmItem_t124 * SwarmItemManager_ActivateItem_m517 (SwarmItemManager_t111 * __this, int32_t ___itemPrefabIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Single_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		Stack_1_Pop_m1582_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483726);
		LinkedList_1_AddLast_m1583_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483727);
		_stringLiteral72 = il2cpp_codegen_string_literal_from_index(72);
		_stringLiteral73 = il2cpp_codegen_string_literal_from_index(73);
		_stringLiteral74 = il2cpp_codegen_string_literal_from_index(74);
		_stringLiteral67 = il2cpp_codegen_string_literal_from_index(67);
		_stringLiteral75 = il2cpp_codegen_string_literal_from_index(75);
		_stringLiteral76 = il2cpp_codegen_string_literal_from_index(76);
		_stringLiteral77 = il2cpp_codegen_string_literal_from_index(77);
		_stringLiteral78 = il2cpp_codegen_string_literal_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	SwarmItem_t124 * V_0 = {0};
	{
		PrefabItemListsU5BU5D_t156* L_0 = (__this->____prefabItemLists_8);
		int32_t L_1 = ___itemPrefabIndex;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_0, L_2, sizeof(PrefabItemLists_t151 *))));
		LinkedList_1_t152 * L_3 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_0, L_2, sizeof(PrefabItemLists_t151 *)))->___activeItems_0);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 System.Collections.Generic.LinkedList`1<SwarmItem>::get_Count() */, L_3);
		PrefabItemU5BU5D_t157* L_5 = (__this->___itemPrefabs_10);
		int32_t L_6 = ___itemPrefabIndex;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_5, L_7, sizeof(PrefabItem_t154 *))));
		int32_t L_8 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_5, L_7, sizeof(PrefabItem_t154 *)))->___maxItemCount_1);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_8))))
		{
			goto IL_008b;
		}
	}
	{
		PrefabItemU5BU5D_t157* L_9 = (__this->___itemPrefabs_10);
		int32_t L_10 = ___itemPrefabIndex;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_9, L_11, sizeof(PrefabItem_t154 *))));
		int32_t L_12 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_9, L_11, sizeof(PrefabItem_t154 *)))->___maxItemCount_1);
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_008b;
		}
	}
	{
		bool L_13 = (__this->___debugEvents_9);
		if (!L_13)
		{
			goto IL_0089;
		}
	}
	{
		ObjectU5BU5D_t356* L_14 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 4));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		ArrayElementTypeCheck (L_14, _stringLiteral72);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral72;
		ObjectU5BU5D_t356* L_15 = L_14;
		PrefabItemListsU5BU5D_t156* L_16 = (__this->____prefabItemLists_8);
		int32_t L_17 = ___itemPrefabIndex;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_16, L_18, sizeof(PrefabItemLists_t151 *))));
		LinkedList_1_t152 * L_19 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_16, L_18, sizeof(PrefabItemLists_t151 *)))->___activeItems_0);
		NullCheck(L_19);
		int32_t L_20 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 System.Collections.Generic.LinkedList`1<SwarmItem>::get_Count() */, L_19);
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		ArrayElementTypeCheck (L_15, L_22);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 1, sizeof(Object_t *))) = (Object_t *)L_22;
		ObjectU5BU5D_t356* L_23 = L_15;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral73);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral73;
		ObjectU5BU5D_t356* L_24 = L_23;
		int32_t L_25 = Time_get_frameCount_m1572(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_26 = L_25;
		Object_t * L_27 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 3, sizeof(Object_t *))) = (Object_t *)L_27;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m1316(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return (SwarmItem_t124 *)NULL;
	}

IL_008b:
	{
		V_0 = (SwarmItem_t124 *)NULL;
		goto IL_00a5;
	}

IL_0092:
	{
		PrefabItemListsU5BU5D_t156* L_29 = (__this->____prefabItemLists_8);
		int32_t L_30 = ___itemPrefabIndex;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_29, L_31, sizeof(PrefabItemLists_t151 *))));
		Stack_1_t153 * L_32 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_29, L_31, sizeof(PrefabItemLists_t151 *)))->___inactiveItems_1);
		NullCheck(L_32);
		SwarmItem_t124 * L_33 = Stack_1_Pop_m1582(L_32, /*hidden argument*/Stack_1_Pop_m1582_MethodInfo_var);
		V_0 = L_33;
	}

IL_00a5:
	{
		SwarmItem_t124 * L_34 = V_0;
		bool L_35 = Object_op_Equality_m1413(NULL /*static, unused*/, L_34, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00c9;
		}
	}
	{
		PrefabItemListsU5BU5D_t156* L_36 = (__this->____prefabItemLists_8);
		int32_t L_37 = ___itemPrefabIndex;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = L_37;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_36, L_38, sizeof(PrefabItemLists_t151 *))));
		Stack_1_t153 * L_39 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_36, L_38, sizeof(PrefabItemLists_t151 *)))->___inactiveItems_1);
		NullCheck(L_39);
		int32_t L_40 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<SwarmItem>::get_Count() */, L_39);
		if ((((int32_t)L_40) > ((int32_t)0)))
		{
			goto IL_0092;
		}
	}

IL_00c9:
	{
		SwarmItem_t124 * L_41 = V_0;
		bool L_42 = Object_op_Equality_m1413(NULL /*static, unused*/, L_41, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_013c;
		}
	}
	{
		int32_t L_43 = ___itemPrefabIndex;
		SwarmItem_t124 * L_44 = (SwarmItem_t124 *)VirtFuncInvoker1< SwarmItem_t124 *, int32_t >::Invoke(8 /* SwarmItem SwarmItemManager::InstantiateItem(System.Int32) */, __this, L_43);
		V_0 = L_44;
		PrefabItemListsU5BU5D_t156* L_45 = (__this->____prefabItemLists_8);
		int32_t L_46 = ___itemPrefabIndex;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_45, L_47, sizeof(PrefabItemLists_t151 *))));
		LinkedList_1_t152 * L_48 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_45, L_47, sizeof(PrefabItemLists_t151 *)))->___activeItems_0);
		SwarmItem_t124 * L_49 = V_0;
		NullCheck(L_48);
		LinkedList_1_AddLast_m1583(L_48, L_49, /*hidden argument*/LinkedList_1_AddLast_m1583_MethodInfo_var);
		bool L_50 = (__this->___debugEvents_9);
		if (!L_50)
		{
			goto IL_0137;
		}
	}
	{
		ObjectU5BU5D_t356* L_51 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 4));
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 0);
		ArrayElementTypeCheck (L_51, _stringLiteral74);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_51, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral74;
		ObjectU5BU5D_t356* L_52 = L_51;
		GameObject_t155 * L_53 = (__this->____go_3);
		NullCheck(L_53);
		String_t* L_54 = Object_get_name_m1338(L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 1);
		ArrayElementTypeCheck (L_52, L_54);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, 1, sizeof(Object_t *))) = (Object_t *)L_54;
		ObjectU5BU5D_t356* L_55 = L_52;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 2);
		ArrayElementTypeCheck (L_55, _stringLiteral67);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_55, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral67;
		ObjectU5BU5D_t356* L_56 = L_55;
		int32_t L_57 = Time_get_frameCount_m1572(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_58 = L_57;
		Object_t * L_59 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 3);
		ArrayElementTypeCheck (L_56, L_59);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, 3, sizeof(Object_t *))) = (Object_t *)L_59;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = String_Concat_m1316(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
	}

IL_0137:
	{
		goto IL_0191;
	}

IL_013c:
	{
		PrefabItemListsU5BU5D_t156* L_61 = (__this->____prefabItemLists_8);
		int32_t L_62 = ___itemPrefabIndex;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		int32_t L_63 = L_62;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_61, L_63, sizeof(PrefabItemLists_t151 *))));
		LinkedList_1_t152 * L_64 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_61, L_63, sizeof(PrefabItemLists_t151 *)))->___activeItems_0);
		SwarmItem_t124 * L_65 = V_0;
		NullCheck(L_64);
		LinkedList_1_AddLast_m1583(L_64, L_65, /*hidden argument*/LinkedList_1_AddLast_m1583_MethodInfo_var);
		bool L_66 = (__this->___debugEvents_9);
		if (!L_66)
		{
			goto IL_0191;
		}
	}
	{
		ObjectU5BU5D_t356* L_67 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 4));
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, 0);
		ArrayElementTypeCheck (L_67, _stringLiteral75);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_67, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral75;
		ObjectU5BU5D_t356* L_68 = L_67;
		SwarmItem_t124 * L_69 = V_0;
		NullCheck(L_69);
		String_t* L_70 = Object_get_name_m1338(L_69, /*hidden argument*/NULL);
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, 1);
		ArrayElementTypeCheck (L_68, L_70);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_68, 1, sizeof(Object_t *))) = (Object_t *)L_70;
		ObjectU5BU5D_t356* L_71 = L_68;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, 2);
		ArrayElementTypeCheck (L_71, _stringLiteral67);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_71, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral67;
		ObjectU5BU5D_t356* L_72 = L_71;
		int32_t L_73 = Time_get_frameCount_m1572(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_74 = L_73;
		Object_t * L_75 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_74);
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 3);
		ArrayElementTypeCheck (L_72, L_75);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_72, 3, sizeof(Object_t *))) = (Object_t *)L_75;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_76 = String_Concat_m1316(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_76, /*hidden argument*/NULL);
	}

IL_0191:
	{
		SwarmItem_t124 * L_77 = V_0;
		Transform_t35 * L_78 = (__this->____activeParentTransform_5);
		SwarmItemManager_SetItemParentTransform_m520(__this, L_77, L_78, /*hidden argument*/NULL);
		SwarmItem_t124 * L_79 = V_0;
		NullCheck(L_79);
		SwarmItem_set_State_m500(L_79, 1, /*hidden argument*/NULL);
		PrefabItemListsU5BU5D_t156* L_80 = (__this->____prefabItemLists_8);
		int32_t L_81 = ___itemPrefabIndex;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, L_81);
		int32_t L_82 = L_81;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_80, L_82, sizeof(PrefabItemLists_t151 *))));
		float L_83 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_80, L_82, sizeof(PrefabItemLists_t151 *)))->___inactivePruneTimeLeft_2);
		if ((!(((float)L_83) > ((float)(0.0f)))))
		{
			goto IL_0272;
		}
	}
	{
		PrefabItemListsU5BU5D_t156* L_84 = (__this->____prefabItemLists_8);
		int32_t L_85 = ___itemPrefabIndex;
		NullCheck(L_84);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_84, L_85);
		int32_t L_86 = L_85;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_84, L_86, sizeof(PrefabItemLists_t151 *))));
		Stack_1_t153 * L_87 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_84, L_86, sizeof(PrefabItemLists_t151 *)))->___inactiveItems_1);
		NullCheck(L_87);
		int32_t L_88 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<SwarmItem>::get_Count() */, L_87);
		PrefabItemListsU5BU5D_t156* L_89 = (__this->____prefabItemLists_8);
		int32_t L_90 = ___itemPrefabIndex;
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, L_90);
		int32_t L_91 = L_90;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_89, L_91, sizeof(PrefabItemLists_t151 *))));
		int32_t L_92 = PrefabItemLists_get_ItemCount_m512((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_89, L_91, sizeof(PrefabItemLists_t151 *))), /*hidden argument*/NULL);
		PrefabItemU5BU5D_t157* L_93 = (__this->___itemPrefabs_10);
		int32_t L_94 = ___itemPrefabIndex;
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, L_94);
		int32_t L_95 = L_94;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_93, L_95, sizeof(PrefabItem_t154 *))));
		float L_96 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_93, L_95, sizeof(PrefabItem_t154 *)))->___inactiveThreshold_2);
		if ((!(((float)((float)((float)(((float)L_88))/(float)(((float)L_92))))) < ((float)L_96))))
		{
			goto IL_0272;
		}
	}
	{
		bool L_97 = (__this->___debugEvents_9);
		if (!L_97)
		{
			goto IL_0260;
		}
	}
	{
		ObjectU5BU5D_t356* L_98 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 6));
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, 0);
		ArrayElementTypeCheck (L_98, _stringLiteral76);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_98, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral76;
		ObjectU5BU5D_t356* L_99 = L_98;
		PrefabItemU5BU5D_t157* L_100 = (__this->___itemPrefabs_10);
		int32_t L_101 = ___itemPrefabIndex;
		NullCheck(L_100);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_100, L_101);
		int32_t L_102 = L_101;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_100, L_102, sizeof(PrefabItem_t154 *))));
		float L_103 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_100, L_102, sizeof(PrefabItem_t154 *)))->___inactiveThreshold_2);
		float L_104 = ((float)((float)L_103*(float)(100.0f)));
		Object_t * L_105 = Box(Single_t388_il2cpp_TypeInfo_var, &L_104);
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, 1);
		ArrayElementTypeCheck (L_99, L_105);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_99, 1, sizeof(Object_t *))) = (Object_t *)L_105;
		ObjectU5BU5D_t356* L_106 = L_99;
		NullCheck(L_106);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_106, 2);
		ArrayElementTypeCheck (L_106, _stringLiteral77);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_106, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral77;
		ObjectU5BU5D_t356* L_107 = L_106;
		PrefabItemU5BU5D_t157* L_108 = (__this->___itemPrefabs_10);
		int32_t L_109 = ___itemPrefabIndex;
		NullCheck(L_108);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_108, L_109);
		int32_t L_110 = L_109;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_108, L_110, sizeof(PrefabItem_t154 *))));
		GameObject_t155 * L_111 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_108, L_110, sizeof(PrefabItem_t154 *)))->___prefab_0);
		NullCheck(L_111);
		String_t* L_112 = Object_get_name_m1338(L_111, /*hidden argument*/NULL);
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, 3);
		ArrayElementTypeCheck (L_107, L_112);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_107, 3, sizeof(Object_t *))) = (Object_t *)L_112;
		ObjectU5BU5D_t356* L_113 = L_107;
		NullCheck(L_113);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_113, 4);
		ArrayElementTypeCheck (L_113, _stringLiteral78);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_113, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral78;
		ObjectU5BU5D_t356* L_114 = L_113;
		int32_t L_115 = Time_get_frameCount_m1572(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_116 = L_115;
		Object_t * L_117 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_116);
		NullCheck(L_114);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_114, 5);
		ArrayElementTypeCheck (L_114, L_117);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_114, 5, sizeof(Object_t *))) = (Object_t *)L_117;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_118 = String_Concat_m1316(NULL /*static, unused*/, L_114, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_118, /*hidden argument*/NULL);
	}

IL_0260:
	{
		PrefabItemListsU5BU5D_t156* L_119 = (__this->____prefabItemLists_8);
		int32_t L_120 = ___itemPrefabIndex;
		NullCheck(L_119);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_119, L_120);
		int32_t L_121 = L_120;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_119, L_121, sizeof(PrefabItemLists_t151 *))));
		(*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_119, L_121, sizeof(PrefabItemLists_t151 *)))->___inactivePruneTimeLeft_2 = (0.0f);
	}

IL_0272:
	{
		SwarmItem_t124 * L_122 = V_0;
		return L_122;
	}
}
// System.Void SwarmItemManager::DeactiveItem(SwarmItem)
// SwarmItem
#include "AssemblyU2DCSharp_SwarmItem.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
extern TypeInfo* Exception_t359_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t388_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Push_m1584_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral79;
extern Il2CppCodeGenString* _stringLiteral80;
extern Il2CppCodeGenString* _stringLiteral67;
extern Il2CppCodeGenString* _stringLiteral81;
extern Il2CppCodeGenString* _stringLiteral82;
extern Il2CppCodeGenString* _stringLiteral83;
extern Il2CppCodeGenString* _stringLiteral84;
extern "C" void SwarmItemManager_DeactiveItem_m518 (SwarmItemManager_t111 * __this, SwarmItem_t124 * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t359_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Single_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		Stack_1_Push_m1584_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483728);
		_stringLiteral79 = il2cpp_codegen_string_literal_from_index(79);
		_stringLiteral80 = il2cpp_codegen_string_literal_from_index(80);
		_stringLiteral67 = il2cpp_codegen_string_literal_from_index(67);
		_stringLiteral81 = il2cpp_codegen_string_literal_from_index(81);
		_stringLiteral82 = il2cpp_codegen_string_literal_from_index(82);
		_stringLiteral83 = il2cpp_codegen_string_literal_from_index(83);
		_stringLiteral84 = il2cpp_codegen_string_literal_from_index(84);
		s_Il2CppMethodIntialized = true;
	}
	{
		SwarmItem_t124 * L_0 = ___item;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		Exception_t359 * L_2 = (Exception_t359 *)il2cpp_codegen_object_new (Exception_t359_il2cpp_TypeInfo_var);
		Exception__ctor_m1318(L_2, _stringLiteral79, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		PrefabItemListsU5BU5D_t156* L_3 = (__this->____prefabItemLists_8);
		SwarmItem_t124 * L_4 = ___item;
		NullCheck(L_4);
		int32_t L_5 = SwarmItem_get_PrefabIndex_m505(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_5);
		int32_t L_6 = L_5;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_3, L_6, sizeof(PrefabItemLists_t151 *))));
		LinkedList_1_t152 * L_7 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_3, L_6, sizeof(PrefabItemLists_t151 *)))->___activeItems_0);
		SwarmItem_t124 * L_8 = ___item;
		NullCheck(L_7);
		VirtFuncInvoker1< bool, SwarmItem_t124 * >::Invoke(17 /* System.Boolean System.Collections.Generic.LinkedList`1<SwarmItem>::Remove(!0) */, L_7, L_8);
		PrefabItemListsU5BU5D_t156* L_9 = (__this->____prefabItemLists_8);
		SwarmItem_t124 * L_10 = ___item;
		NullCheck(L_10);
		int32_t L_11 = SwarmItem_get_PrefabIndex_m505(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_11);
		int32_t L_12 = L_11;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_9, L_12, sizeof(PrefabItemLists_t151 *))));
		Stack_1_t153 * L_13 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_9, L_12, sizeof(PrefabItemLists_t151 *)))->___inactiveItems_1);
		SwarmItem_t124 * L_14 = ___item;
		NullCheck(L_13);
		Stack_1_Push_m1584(L_13, L_14, /*hidden argument*/Stack_1_Push_m1584_MethodInfo_var);
		SwarmItem_t124 * L_15 = ___item;
		Transform_t35 * L_16 = (__this->____inactiveParentTransform_6);
		SwarmItemManager_SetItemParentTransform_m520(__this, L_15, L_16, /*hidden argument*/NULL);
		bool L_17 = (__this->___debugEvents_9);
		if (!L_17)
		{
			goto IL_0096;
		}
	}
	{
		ObjectU5BU5D_t356* L_18 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 4));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		ArrayElementTypeCheck (L_18, _stringLiteral80);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral80;
		ObjectU5BU5D_t356* L_19 = L_18;
		SwarmItem_t124 * L_20 = ___item;
		NullCheck(L_20);
		String_t* L_21 = Object_get_name_m1338(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		ArrayElementTypeCheck (L_19, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 1, sizeof(Object_t *))) = (Object_t *)L_21;
		ObjectU5BU5D_t356* L_22 = L_19;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 2);
		ArrayElementTypeCheck (L_22, _stringLiteral67);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_22, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral67;
		ObjectU5BU5D_t356* L_23 = L_22;
		int32_t L_24 = Time_get_frameCount_m1572(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_25 = L_24;
		Object_t * L_26 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 3);
		ArrayElementTypeCheck (L_23, L_26);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, 3, sizeof(Object_t *))) = (Object_t *)L_26;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m1316(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
	}

IL_0096:
	{
		PrefabItemListsU5BU5D_t156* L_28 = (__this->____prefabItemLists_8);
		SwarmItem_t124 * L_29 = ___item;
		NullCheck(L_29);
		int32_t L_30 = SwarmItem_get_PrefabIndex_m505(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_30);
		int32_t L_31 = L_30;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_28, L_31, sizeof(PrefabItemLists_t151 *))));
		float L_32 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_28, L_31, sizeof(PrefabItemLists_t151 *)))->___inactivePruneTimeLeft_2);
		if ((!(((float)L_32) == ((float)(0.0f)))))
		{
			goto IL_0210;
		}
	}
	{
		PrefabItemU5BU5D_t157* L_33 = (__this->___itemPrefabs_10);
		SwarmItem_t124 * L_34 = ___item;
		NullCheck(L_34);
		int32_t L_35 = SwarmItem_get_PrefabIndex_m505(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_35);
		int32_t L_36 = L_35;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_33, L_36, sizeof(PrefabItem_t154 *))));
		float L_37 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_33, L_36, sizeof(PrefabItem_t154 *)))->___inactivePrunePercentage_4);
		if ((!(((float)L_37) > ((float)(0.0f)))))
		{
			goto IL_0210;
		}
	}
	{
		PrefabItemListsU5BU5D_t156* L_38 = (__this->____prefabItemLists_8);
		SwarmItem_t124 * L_39 = ___item;
		NullCheck(L_39);
		int32_t L_40 = SwarmItem_get_PrefabIndex_m505(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_40);
		int32_t L_41 = L_40;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_38, L_41, sizeof(PrefabItemLists_t151 *))));
		Stack_1_t153 * L_42 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_38, L_41, sizeof(PrefabItemLists_t151 *)))->___inactiveItems_1);
		NullCheck(L_42);
		int32_t L_43 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<SwarmItem>::get_Count() */, L_42);
		PrefabItemListsU5BU5D_t156* L_44 = (__this->____prefabItemLists_8);
		SwarmItem_t124 * L_45 = ___item;
		NullCheck(L_45);
		int32_t L_46 = SwarmItem_get_PrefabIndex_m505(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_46);
		int32_t L_47 = L_46;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_44, L_47, sizeof(PrefabItemLists_t151 *))));
		int32_t L_48 = PrefabItemLists_get_ItemCount_m512((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_44, L_47, sizeof(PrefabItemLists_t151 *))), /*hidden argument*/NULL);
		PrefabItemU5BU5D_t157* L_49 = (__this->___itemPrefabs_10);
		SwarmItem_t124 * L_50 = ___item;
		NullCheck(L_50);
		int32_t L_51 = SwarmItem_get_PrefabIndex_m505(L_50, /*hidden argument*/NULL);
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_51);
		int32_t L_52 = L_51;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_49, L_52, sizeof(PrefabItem_t154 *))));
		float L_53 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_49, L_52, sizeof(PrefabItem_t154 *)))->___inactiveThreshold_2);
		if ((!(((float)((float)((float)(((float)L_43))/(float)(((float)L_48))))) >= ((float)L_53))))
		{
			goto IL_0210;
		}
	}
	{
		bool L_54 = (__this->___debugEvents_9);
		if (!L_54)
		{
			goto IL_01ad;
		}
	}
	{
		ObjectU5BU5D_t356* L_55 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 8));
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 0);
		ArrayElementTypeCheck (L_55, _stringLiteral81);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_55, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral81;
		ObjectU5BU5D_t356* L_56 = L_55;
		PrefabItemU5BU5D_t157* L_57 = (__this->___itemPrefabs_10);
		SwarmItem_t124 * L_58 = ___item;
		NullCheck(L_58);
		int32_t L_59 = SwarmItem_get_PrefabIndex_m505(L_58, /*hidden argument*/NULL);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, L_59);
		int32_t L_60 = L_59;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_57, L_60, sizeof(PrefabItem_t154 *))));
		float L_61 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_57, L_60, sizeof(PrefabItem_t154 *)))->___inactiveThreshold_2);
		float L_62 = ((float)((float)L_61*(float)(100.0f)));
		Object_t * L_63 = Box(Single_t388_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		ArrayElementTypeCheck (L_56, L_63);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, 1, sizeof(Object_t *))) = (Object_t *)L_63;
		ObjectU5BU5D_t356* L_64 = L_56;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, 2);
		ArrayElementTypeCheck (L_64, _stringLiteral82);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_64, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral82;
		ObjectU5BU5D_t356* L_65 = L_64;
		PrefabItemU5BU5D_t157* L_66 = (__this->___itemPrefabs_10);
		SwarmItem_t124 * L_67 = ___item;
		NullCheck(L_67);
		int32_t L_68 = SwarmItem_get_PrefabIndex_m505(L_67, /*hidden argument*/NULL);
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_68);
		int32_t L_69 = L_68;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_66, L_69, sizeof(PrefabItem_t154 *))));
		GameObject_t155 * L_70 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_66, L_69, sizeof(PrefabItem_t154 *)))->___prefab_0);
		NullCheck(L_70);
		String_t* L_71 = Object_get_name_m1338(L_70, /*hidden argument*/NULL);
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, 3);
		ArrayElementTypeCheck (L_65, L_71);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_65, 3, sizeof(Object_t *))) = (Object_t *)L_71;
		ObjectU5BU5D_t356* L_72 = L_65;
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 4);
		ArrayElementTypeCheck (L_72, _stringLiteral83);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_72, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral83;
		ObjectU5BU5D_t356* L_73 = L_72;
		PrefabItemU5BU5D_t157* L_74 = (__this->___itemPrefabs_10);
		SwarmItem_t124 * L_75 = ___item;
		NullCheck(L_75);
		int32_t L_76 = SwarmItem_get_PrefabIndex_m505(L_75, /*hidden argument*/NULL);
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, L_76);
		int32_t L_77 = L_76;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_74, L_77, sizeof(PrefabItem_t154 *))));
		float L_78 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_74, L_77, sizeof(PrefabItem_t154 *)))->___inactivePruneTimer_3);
		float L_79 = L_78;
		Object_t * L_80 = Box(Single_t388_il2cpp_TypeInfo_var, &L_79);
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, 5);
		ArrayElementTypeCheck (L_73, L_80);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_73, 5, sizeof(Object_t *))) = (Object_t *)L_80;
		ObjectU5BU5D_t356* L_81 = L_73;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, 6);
		ArrayElementTypeCheck (L_81, _stringLiteral84);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_81, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral84;
		ObjectU5BU5D_t356* L_82 = L_81;
		int32_t L_83 = Time_get_frameCount_m1572(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_84 = L_83;
		Object_t * L_85 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_84);
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, 7);
		ArrayElementTypeCheck (L_82, L_85);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_82, 7, sizeof(Object_t *))) = (Object_t *)L_85;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_86 = String_Concat_m1316(NULL /*static, unused*/, L_82, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
	}

IL_01ad:
	{
		PrefabItemU5BU5D_t157* L_87 = (__this->___itemPrefabs_10);
		SwarmItem_t124 * L_88 = ___item;
		NullCheck(L_88);
		int32_t L_89 = SwarmItem_get_PrefabIndex_m505(L_88, /*hidden argument*/NULL);
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, L_89);
		int32_t L_90 = L_89;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_87, L_90, sizeof(PrefabItem_t154 *))));
		float L_91 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_87, L_90, sizeof(PrefabItem_t154 *)))->___inactivePruneTimer_3);
		if ((!(((float)L_91) == ((float)(0.0f)))))
		{
			goto IL_01ec;
		}
	}
	{
		SwarmItem_t124 * L_92 = ___item;
		NullCheck(L_92);
		int32_t L_93 = SwarmItem_get_PrefabIndex_m505(L_92, /*hidden argument*/NULL);
		PrefabItemU5BU5D_t157* L_94 = (__this->___itemPrefabs_10);
		SwarmItem_t124 * L_95 = ___item;
		NullCheck(L_95);
		int32_t L_96 = SwarmItem_get_PrefabIndex_m505(L_95, /*hidden argument*/NULL);
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, L_96);
		int32_t L_97 = L_96;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_94, L_97, sizeof(PrefabItem_t154 *))));
		float L_98 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_94, L_97, sizeof(PrefabItem_t154 *)))->___inactivePrunePercentage_4);
		SwarmItemManager_PruneList_m522(__this, L_93, L_98, /*hidden argument*/NULL);
		goto IL_0210;
	}

IL_01ec:
	{
		PrefabItemListsU5BU5D_t156* L_99 = (__this->____prefabItemLists_8);
		SwarmItem_t124 * L_100 = ___item;
		NullCheck(L_100);
		int32_t L_101 = SwarmItem_get_PrefabIndex_m505(L_100, /*hidden argument*/NULL);
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, L_101);
		int32_t L_102 = L_101;
		PrefabItemU5BU5D_t157* L_103 = (__this->___itemPrefabs_10);
		SwarmItem_t124 * L_104 = ___item;
		NullCheck(L_104);
		int32_t L_105 = SwarmItem_get_PrefabIndex_m505(L_104, /*hidden argument*/NULL);
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, L_105);
		int32_t L_106 = L_105;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_103, L_106, sizeof(PrefabItem_t154 *))));
		float L_107 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_103, L_106, sizeof(PrefabItem_t154 *)))->___inactivePruneTimer_3);
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_99, L_102, sizeof(PrefabItemLists_t151 *))));
		(*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_99, L_102, sizeof(PrefabItemLists_t151 *)))->___inactivePruneTimeLeft_2 = L_107;
	}

IL_0210:
	{
		SwarmItem_t124 * L_108 = ___item;
		NullCheck(L_108);
		GameObject_t155 * L_109 = Component_get_gameObject_m1337(L_108, /*hidden argument*/NULL);
		NullCheck(L_109);
		GameObject_SetActive_m1538(L_109, 0, /*hidden argument*/NULL);
		return;
	}
}
// SwarmItem SwarmItemManager::InstantiateItem(System.Int32)
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
extern const Il2CppType* SwarmItem_t124_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* SwarmItem_t124_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t155_m1585_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral85;
extern Il2CppCodeGenString* _stringLiteral59;
extern Il2CppCodeGenString* _stringLiteral86;
extern "C" SwarmItem_t124 * SwarmItemManager_InstantiateItem_m519 (SwarmItemManager_t111 * __this, int32_t ___itemPrefabIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SwarmItem_t124_0_0_0_var = il2cpp_codegen_type_from_index(117);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(89);
		SwarmItem_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(117);
		Object_Instantiate_TisGameObject_t155_m1585_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483729);
		_stringLiteral58 = il2cpp_codegen_string_literal_from_index(58);
		_stringLiteral85 = il2cpp_codegen_string_literal_from_index(85);
		_stringLiteral59 = il2cpp_codegen_string_literal_from_index(59);
		_stringLiteral86 = il2cpp_codegen_string_literal_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	SwarmItem_t124 * V_0 = {0};
	{
		PrefabItemU5BU5D_t157* L_0 = (__this->___itemPrefabs_10);
		int32_t L_1 = ___itemPrefabIndex;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_0, L_2, sizeof(PrefabItem_t154 *))));
		GameObject_t155 * L_3 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_0, L_2, sizeof(PrefabItem_t154 *)))->___prefab_0);
		GameObject_t155 * L_4 = Object_Instantiate_TisGameObject_t155_m1585(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t155_m1585_MethodInfo_var);
		__this->____go_3 = L_4;
		GameObject_t155 * L_5 = (__this->____go_3);
		int32_t* L_6 = &(__this->____itemCount_7);
		String_t* L_7 = Int32_ToString_m1443(L_6, _stringLiteral85, /*hidden argument*/NULL);
		GameObject_t155 * L_8 = (__this->____go_3);
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m1338(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_9);
		String_t* L_11 = String_Replace_m1586(L_9, _stringLiteral86, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Concat_m1452(NULL /*static, unused*/, _stringLiteral58, L_7, _stringLiteral59, L_11, /*hidden argument*/NULL);
		NullCheck(L_5);
		Object_set_name_m1476(L_5, L_12, /*hidden argument*/NULL);
		GameObject_t155 * L_13 = (__this->____go_3);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m1462(NULL /*static, unused*/, LoadTypeToken(SwarmItem_t124_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_13);
		Component_t365 * L_15 = GameObject_GetComponent_m1474(L_13, L_14, /*hidden argument*/NULL);
		V_0 = ((SwarmItem_t124 *)CastclassClass(L_15, SwarmItem_t124_il2cpp_TypeInfo_var));
		SwarmItem_t124 * L_16 = V_0;
		int32_t L_17 = ___itemPrefabIndex;
		bool L_18 = (__this->___debugEvents_9);
		NullCheck(L_16);
		VirtActionInvoker3< SwarmItemManager_t111 *, int32_t, bool >::Invoke(5 /* System.Void SwarmItem::Initialize(SwarmItemManager,System.Int32,System.Boolean) */, L_16, __this, L_17, L_18);
		int32_t L_19 = (__this->____itemCount_7);
		__this->____itemCount_7 = ((int32_t)((int32_t)L_19+(int32_t)1));
		SwarmItem_t124 * L_20 = V_0;
		return L_20;
	}
}
// System.Void SwarmItemManager::SetItemParentTransform(SwarmItem,UnityEngine.Transform)
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
extern TypeInfo* Exception_t359_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral87;
extern Il2CppCodeGenString* _stringLiteral88;
extern "C" void SwarmItemManager_SetItemParentTransform_m520 (SwarmItemManager_t111 * __this, SwarmItem_t124 * ___item, Transform_t35 * ___parentTransform, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t359_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral87 = il2cpp_codegen_string_literal_from_index(87);
		_stringLiteral88 = il2cpp_codegen_string_literal_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	{
		SwarmItem_t124 * L_0 = ___item;
		bool L_1 = Object_op_Equality_m1413(NULL /*static, unused*/, L_0, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		Exception_t359 * L_2 = (Exception_t359 *)il2cpp_codegen_object_new (Exception_t359_il2cpp_TypeInfo_var);
		Exception__ctor_m1318(L_2, _stringLiteral87, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		Transform_t35 * L_3 = ___parentTransform;
		bool L_4 = Object_op_Equality_m1413(NULL /*static, unused*/, L_3, (Object_t335 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		Exception_t359 * L_5 = (Exception_t359 *)il2cpp_codegen_object_new (Exception_t359_il2cpp_TypeInfo_var);
		Exception__ctor_m1318(L_5, _stringLiteral88, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_002e:
	{
		SwarmItem_t124 * L_6 = ___item;
		NullCheck(L_6);
		Transform_t35 * L_7 = SwarmItem_get_ThisTransform_m502(L_6, /*hidden argument*/NULL);
		Transform_t35 * L_8 = ___parentTransform;
		NullCheck(L_7);
		Transform_set_parent_m1329(L_7, L_8, /*hidden argument*/NULL);
		SwarmItem_t124 * L_9 = ___item;
		NullCheck(L_9);
		Transform_t35 * L_10 = SwarmItem_get_ThisTransform_m502(L_9, /*hidden argument*/NULL);
		Vector3_t36  L_11 = Vector3_get_zero_m1578(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_localPosition_m1384(L_10, L_11, /*hidden argument*/NULL);
		SwarmItem_t124 * L_12 = ___item;
		NullCheck(L_12);
		Transform_t35 * L_13 = SwarmItem_get_ThisTransform_m502(L_12, /*hidden argument*/NULL);
		Quaternion_t41  L_14 = Quaternion_get_identity_m1579(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localRotation_m1580(L_13, L_14, /*hidden argument*/NULL);
		SwarmItem_t124 * L_15 = ___item;
		NullCheck(L_15);
		Transform_t35 * L_16 = SwarmItem_get_ThisTransform_m502(L_15, /*hidden argument*/NULL);
		Vector3_t36  L_17 = Vector3_get_one_m1581(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_localScale_m1393(L_16, L_17, /*hidden argument*/NULL);
		SwarmItem_t124 * L_18 = ___item;
		NullCheck(L_18);
		VirtActionInvoker0::Invoke(6 /* System.Void SwarmItem::OnSetParentTransform() */, L_18);
		return;
	}
}
// System.Void SwarmItemManager::FrameUpdate()
// System.Collections.Generic.LinkedListNode`1<SwarmItem>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_0MethodDeclarations.h"
extern const MethodInfo* LinkedList_1_get_First_m1511_MethodInfo_var;
extern const MethodInfo* LinkedListNode_1_get_Next_m1512_MethodInfo_var;
extern const MethodInfo* LinkedListNode_1_get_Value_m1513_MethodInfo_var;
extern "C" void SwarmItemManager_FrameUpdate_m521 (SwarmItemManager_t111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LinkedList_1_get_First_m1511_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483694);
		LinkedListNode_1_get_Next_m1512_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483695);
		LinkedListNode_1_get_Value_m1513_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483696);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	LinkedListNode_1_t408 * V_1 = {0};
	LinkedListNode_1_t408 * V_2 = {0};
	{
		V_0 = 0;
		goto IL_00b0;
	}

IL_0007:
	{
		PrefabItemListsU5BU5D_t156* L_0 = (__this->____prefabItemLists_8);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_0, L_2, sizeof(PrefabItemLists_t151 *))));
		LinkedList_1_t152 * L_3 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_0, L_2, sizeof(PrefabItemLists_t151 *)))->___activeItems_0);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 System.Collections.Generic.LinkedList`1<SwarmItem>::get_Count() */, L_3);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0051;
		}
	}
	{
		PrefabItemListsU5BU5D_t156* L_5 = (__this->____prefabItemLists_8);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_5, L_7, sizeof(PrefabItemLists_t151 *))));
		LinkedList_1_t152 * L_8 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_5, L_7, sizeof(PrefabItemLists_t151 *)))->___activeItems_0);
		NullCheck(L_8);
		LinkedListNode_1_t408 * L_9 = LinkedList_1_get_First_m1511(L_8, /*hidden argument*/LinkedList_1_get_First_m1511_MethodInfo_var);
		V_1 = L_9;
		goto IL_004b;
	}

IL_0037:
	{
		LinkedListNode_1_t408 * L_10 = V_1;
		NullCheck(L_10);
		LinkedListNode_1_t408 * L_11 = LinkedListNode_1_get_Next_m1512(L_10, /*hidden argument*/LinkedListNode_1_get_Next_m1512_MethodInfo_var);
		V_2 = L_11;
		LinkedListNode_1_t408 * L_12 = V_1;
		NullCheck(L_12);
		SwarmItem_t124 * L_13 = LinkedListNode_1_get_Value_m1513(L_12, /*hidden argument*/LinkedListNode_1_get_Value_m1513_MethodInfo_var);
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(9 /* System.Void SwarmItem::FrameUpdate() */, L_13);
		LinkedListNode_1_t408 * L_14 = V_2;
		V_1 = L_14;
	}

IL_004b:
	{
		LinkedListNode_1_t408 * L_15 = V_1;
		if (L_15)
		{
			goto IL_0037;
		}
	}

IL_0051:
	{
		PrefabItemListsU5BU5D_t156* L_16 = (__this->____prefabItemLists_8);
		int32_t L_17 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_16, L_18, sizeof(PrefabItemLists_t151 *))));
		float L_19 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_16, L_18, sizeof(PrefabItemLists_t151 *)))->___inactivePruneTimeLeft_2);
		if ((!(((float)L_19) > ((float)(0.0f)))))
		{
			goto IL_00ac;
		}
	}
	{
		PrefabItemListsU5BU5D_t156* L_20 = (__this->____prefabItemLists_8);
		int32_t L_21 = V_0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		PrefabItemLists_t151 * L_23 = (*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_20, L_22, sizeof(PrefabItemLists_t151 *)));
		NullCheck(L_23);
		float L_24 = (L_23->___inactivePruneTimeLeft_2);
		float L_25 = Time_get_deltaTime_m1379(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		L_23->___inactivePruneTimeLeft_2 = ((float)((float)L_24-(float)L_25));
		PrefabItemListsU5BU5D_t156* L_26 = (__this->____prefabItemLists_8);
		int32_t L_27 = V_0;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_26, L_28, sizeof(PrefabItemLists_t151 *))));
		float L_29 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_26, L_28, sizeof(PrefabItemLists_t151 *)))->___inactivePruneTimeLeft_2);
		if ((!(((float)L_29) <= ((float)(0.0f)))))
		{
			goto IL_00ac;
		}
	}
	{
		int32_t L_30 = V_0;
		PrefabItemU5BU5D_t157* L_31 = (__this->___itemPrefabs_10);
		int32_t L_32 = V_0;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_31, L_33, sizeof(PrefabItem_t154 *))));
		float L_34 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_31, L_33, sizeof(PrefabItem_t154 *)))->___inactivePrunePercentage_4);
		SwarmItemManager_PruneList_m522(__this, L_30, L_34, /*hidden argument*/NULL);
	}

IL_00ac:
	{
		int32_t L_35 = V_0;
		V_0 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00b0:
	{
		int32_t L_36 = V_0;
		PrefabItemListsU5BU5D_t156* L_37 = (__this->____prefabItemLists_8);
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)(((Array_t *)L_37)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void SwarmItemManager::PruneList(System.Int32,System.Single)
// System.Single
#include "mscorlib_System_Single.h"
extern TypeInfo* Mathf_t371_il2cpp_TypeInfo_var;
extern "C" void SwarmItemManager_PruneList_m522 (SwarmItemManager_t111 * __this, int32_t ___itemPrefabIndex, float ___prunePercentage, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t371_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		PrefabItemListsU5BU5D_t156* L_0 = (__this->____prefabItemLists_8);
		int32_t L_1 = ___itemPrefabIndex;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_0, L_2, sizeof(PrefabItemLists_t151 *))));
		(*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_0, L_2, sizeof(PrefabItemLists_t151 *)))->___inactivePruneTimeLeft_2 = (0.0f);
		float L_3 = ___prunePercentage;
		PrefabItemListsU5BU5D_t156* L_4 = (__this->____prefabItemLists_8);
		int32_t L_5 = ___itemPrefabIndex;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_4, L_6, sizeof(PrefabItemLists_t151 *))));
		Stack_1_t153 * L_7 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_4, L_6, sizeof(PrefabItemLists_t151 *)))->___inactiveItems_1);
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<SwarmItem>::get_Count() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t371_il2cpp_TypeInfo_var);
		int32_t L_9 = Mathf_FloorToInt_m1587(NULL /*static, unused*/, ((float)((float)L_3*(float)(((float)L_8)))), /*hidden argument*/NULL);
		V_0 = L_9;
		int32_t L_10 = ___itemPrefabIndex;
		int32_t L_11 = V_0;
		SwarmItemManager_PruneList_m523(__this, L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SwarmItemManager::PruneList(System.Int32,System.Int32)
extern TypeInfo* ObjectU5BU5D_t356_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t372_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t388_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Pop_m1582_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral89;
extern Il2CppCodeGenString* _stringLiteral90;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral92;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral94;
extern Il2CppCodeGenString* _stringLiteral67;
extern "C" void SwarmItemManager_PruneList_m523 (SwarmItemManager_t111 * __this, int32_t ___itemPrefabIndex, int32_t ___pruneCount, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Int32_t372_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Single_t388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Stack_1_Pop_m1582_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483726);
		_stringLiteral89 = il2cpp_codegen_string_literal_from_index(89);
		_stringLiteral90 = il2cpp_codegen_string_literal_from_index(90);
		_stringLiteral91 = il2cpp_codegen_string_literal_from_index(91);
		_stringLiteral92 = il2cpp_codegen_string_literal_from_index(92);
		_stringLiteral93 = il2cpp_codegen_string_literal_from_index(93);
		_stringLiteral94 = il2cpp_codegen_string_literal_from_index(94);
		_stringLiteral67 = il2cpp_codegen_string_literal_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	SwarmItem_t124 * V_0 = {0};
	{
		bool L_0 = (__this->___debugEvents_9);
		if (!L_0)
		{
			goto IL_00a5;
		}
	}
	{
		ObjectU5BU5D_t356* L_1 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, ((int32_t)10)));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral89);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral89;
		ObjectU5BU5D_t356* L_2 = L_1;
		int32_t L_3 = ___pruneCount;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t356* L_6 = L_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral90);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral90;
		ObjectU5BU5D_t356* L_7 = L_6;
		PrefabItemU5BU5D_t157* L_8 = (__this->___itemPrefabs_10);
		int32_t L_9 = ___itemPrefabIndex;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_8, L_10, sizeof(PrefabItem_t154 *))));
		float L_11 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_8, L_10, sizeof(PrefabItem_t154 *)))->___inactivePrunePercentage_4);
		float L_12 = ((float)((float)L_11*(float)(100.0f)));
		Object_t * L_13 = Box(Single_t388_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3, sizeof(Object_t *))) = (Object_t *)L_13;
		ObjectU5BU5D_t356* L_14 = L_7;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 4);
		ArrayElementTypeCheck (L_14, _stringLiteral91);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral91;
		ObjectU5BU5D_t356* L_15 = L_14;
		PrefabItemListsU5BU5D_t156* L_16 = (__this->____prefabItemLists_8);
		int32_t L_17 = ___itemPrefabIndex;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_16, L_18, sizeof(PrefabItemLists_t151 *))));
		Stack_1_t153 * L_19 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_16, L_18, sizeof(PrefabItemLists_t151 *)))->___inactiveItems_1);
		NullCheck(L_19);
		int32_t L_20 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<SwarmItem>::get_Count() */, L_19);
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 5);
		ArrayElementTypeCheck (L_15, L_22);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 5, sizeof(Object_t *))) = (Object_t *)L_22;
		ObjectU5BU5D_t356* L_23 = L_15;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 6);
		ArrayElementTypeCheck (L_23, _stringLiteral92);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral92;
		ObjectU5BU5D_t356* L_24 = L_23;
		PrefabItemU5BU5D_t157* L_25 = (__this->___itemPrefabs_10);
		int32_t L_26 = ___itemPrefabIndex;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = L_26;
		NullCheck((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_25, L_27, sizeof(PrefabItem_t154 *))));
		GameObject_t155 * L_28 = ((*(PrefabItem_t154 **)(PrefabItem_t154 **)SZArrayLdElema(L_25, L_27, sizeof(PrefabItem_t154 *)))->___prefab_0);
		NullCheck(L_28);
		String_t* L_29 = Object_get_name_m1338(L_28, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 7);
		ArrayElementTypeCheck (L_24, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 7, sizeof(Object_t *))) = (Object_t *)L_29;
		ObjectU5BU5D_t356* L_30 = L_24;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 8);
		ArrayElementTypeCheck (L_30, _stringLiteral93);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 8, sizeof(Object_t *))) = (Object_t *)_stringLiteral93;
		ObjectU5BU5D_t356* L_31 = L_30;
		int32_t L_32 = Time_get_frameCount_m1572(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_33 = L_32;
		Object_t * L_34 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)9));
		ArrayElementTypeCheck (L_31, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_31, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_34;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m1316(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		goto IL_0124;
	}

IL_00aa:
	{
		PrefabItemListsU5BU5D_t156* L_36 = (__this->____prefabItemLists_8);
		int32_t L_37 = ___itemPrefabIndex;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = L_37;
		NullCheck((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_36, L_38, sizeof(PrefabItemLists_t151 *))));
		Stack_1_t153 * L_39 = ((*(PrefabItemLists_t151 **)(PrefabItemLists_t151 **)SZArrayLdElema(L_36, L_38, sizeof(PrefabItemLists_t151 *)))->___inactiveItems_1);
		NullCheck(L_39);
		SwarmItem_t124 * L_40 = Stack_1_Pop_m1582(L_39, /*hidden argument*/Stack_1_Pop_m1582_MethodInfo_var);
		V_0 = L_40;
		SwarmItem_t124 * L_41 = V_0;
		NullCheck(L_41);
		VirtActionInvoker0::Invoke(8 /* System.Void SwarmItem::PreDestroy() */, L_41);
		bool L_42 = (__this->___debugEvents_9);
		if (!L_42)
		{
			goto IL_0104;
		}
	}
	{
		ObjectU5BU5D_t356* L_43 = ((ObjectU5BU5D_t356*)SZArrayNew(ObjectU5BU5D_t356_il2cpp_TypeInfo_var, 4));
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		ArrayElementTypeCheck (L_43, _stringLiteral94);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_43, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral94;
		ObjectU5BU5D_t356* L_44 = L_43;
		SwarmItem_t124 * L_45 = V_0;
		NullCheck(L_45);
		String_t* L_46 = Object_get_name_m1338(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 1);
		ArrayElementTypeCheck (L_44, L_46);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, 1, sizeof(Object_t *))) = (Object_t *)L_46;
		ObjectU5BU5D_t356* L_47 = L_44;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 2);
		ArrayElementTypeCheck (L_47, _stringLiteral67);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_47, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral67;
		ObjectU5BU5D_t356* L_48 = L_47;
		int32_t L_49 = Time_get_frameCount_m1572(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_50 = L_49;
		Object_t * L_51 = Box(Int32_t372_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 3);
		ArrayElementTypeCheck (L_48, L_51);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, 3, sizeof(Object_t *))) = (Object_t *)L_51;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_52 = String_Concat_m1316(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		Debug_Log_m1411(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
	}

IL_0104:
	{
		SwarmItem_t124 * L_53 = V_0;
		NullCheck(L_53);
		GameObject_t155 * L_54 = Component_get_gameObject_m1337(L_53, /*hidden argument*/NULL);
		Object_Destroy_m1588(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		V_0 = (SwarmItem_t124 *)NULL;
		int32_t L_55 = (__this->____itemCount_7);
		__this->____itemCount_7 = ((int32_t)((int32_t)L_55-(int32_t)1));
		int32_t L_56 = ___pruneCount;
		___pruneCount = ((int32_t)((int32_t)L_56-(int32_t)1));
	}

IL_0124:
	{
		int32_t L_57 = ___pruneCount;
		if ((((int32_t)L_57) > ((int32_t)0)))
		{
			goto IL_00aa;
		}
	}
	{
		return;
	}
}
// UnityThreading.DispatcherBase
#include "AssemblyU2DCSharp_UnityThreading_DispatcherBase.h"
// UnityThreading.DispatcherBase
#include "AssemblyU2DCSharp_UnityThreading_DispatcherBaseMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityThreading.TaskBase>
#include "mscorlib_System_Collections_Generic_List_1_gen_7.h"
// System.Threading.ManualResetEvent
#include "mscorlib_System_Threading_ManualResetEvent.h"
// UnityThreading.Task
#include "AssemblyU2DCSharp_UnityThreading_Task.h"
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBase.h"
// UnityThreading.TaskSortingSystem
#include "AssemblyU2DCSharp_UnityThreading_TaskSortingSystem.h"
// System.Comparison`1<UnityThreading.TaskBase>
#include "mscorlib_System_Comparison_1_gen.h"
// System.Threading.WaitHandle
#include "mscorlib_System_Threading_WaitHandle.h"
// System.Collections.Generic.List`1<UnityThreading.TaskBase>
#include "mscorlib_System_Collections_Generic_List_1_gen_7MethodDeclarations.h"
// System.Threading.ManualResetEvent
#include "mscorlib_System_Threading_ManualResetEventMethodDeclarations.h"
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
// UnityThreading.Task
#include "AssemblyU2DCSharp_UnityThreading_TaskMethodDeclarations.h"
// System.Threading.EventWaitHandle
#include "mscorlib_System_Threading_EventWaitHandleMethodDeclarations.h"
// System.Comparison`1<UnityThreading.TaskBase>
#include "mscorlib_System_Comparison_1_genMethodDeclarations.h"
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBaseMethodDeclarations.h"
// System.Threading.WaitHandle
#include "mscorlib_System_Threading_WaitHandleMethodDeclarations.h"
struct IEnumerable_1_t350;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<UnityThreading.TaskBase>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<UnityThreading.TaskBase>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_Take_TisTaskBase_t163_m1597(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, int32_t, const MethodInfo*))Enumerable_Take_TisObject_t_m1377_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void UnityThreading.DispatcherBase::.ctor()
// System.Collections.Generic.List`1<UnityThreading.TaskBase>
#include "mscorlib_System_Collections_Generic_List_1_gen_7MethodDeclarations.h"
// System.Threading.ManualResetEvent
#include "mscorlib_System_Threading_ManualResetEventMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern TypeInfo* List_1_t159_il2cpp_TypeInfo_var;
extern TypeInfo* ManualResetEvent_t160_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1590_MethodInfo_var;
extern "C" void DispatcherBase__ctor_m524 (DispatcherBase_t158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t159_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		ManualResetEvent_t160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(156);
		List_1__ctor_m1590_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483730);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t159 * L_0 = (List_1_t159 *)il2cpp_codegen_object_new (List_1_t159_il2cpp_TypeInfo_var);
		List_1__ctor_m1590(L_0, /*hidden argument*/List_1__ctor_m1590_MethodInfo_var);
		__this->___taskList_0 = L_0;
		ManualResetEvent_t160 * L_1 = (ManualResetEvent_t160 *)il2cpp_codegen_object_new (ManualResetEvent_t160_il2cpp_TypeInfo_var);
		ManualResetEvent__ctor_m1591(L_1, 0, /*hidden argument*/NULL);
		__this->___dataEvent_1 = L_1;
		Object__ctor_m1306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityThreading.DispatcherBase::get_TaskCount()
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
extern "C" int32_t DispatcherBase_get_TaskCount_m525 (DispatcherBase_t158 * __this, const MethodInfo* method)
{
	List_1_t159 * V_0 = {0};
	int32_t V_1 = 0;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t159 * L_0 = (__this->___taskList_0);
		V_0 = L_0;
		List_1_t159 * L_1 = V_0;
		Monitor_Enter_m1592(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			List_1_t159 * L_2 = (__this->___taskList_0);
			NullCheck(L_2);
			int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityThreading.TaskBase>::get_Count() */, L_2);
			V_1 = L_3;
			IL2CPP_LEAVE(0x2A, FINALLY_0023);
		}

IL_001e:
		{
			; // IL_001e: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		List_1_t159 * L_4 = V_0;
		Monitor_Exit_m1593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_002a:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// UnityThreading.Task UnityThreading.DispatcherBase::Dispatch(System.Action)
// System.Action
#include "System_Core_System_Action.h"
// UnityThreading.Task
#include "AssemblyU2DCSharp_UnityThreading_TaskMethodDeclarations.h"
// UnityThreading.DispatcherBase
#include "AssemblyU2DCSharp_UnityThreading_DispatcherBaseMethodDeclarations.h"
extern TypeInfo* Task_t165_il2cpp_TypeInfo_var;
extern "C" Task_t165 * DispatcherBase_Dispatch_m526 (DispatcherBase_t158 * __this, Action_t16 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Task_t165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(157);
		s_Il2CppMethodIntialized = true;
	}
	Task_t165 * V_0 = {0};
	{
		VirtActionInvoker0::Invoke(5 /* System.Void UnityThreading.DispatcherBase::CheckAccessLimitation() */, __this);
		Action_t16 * L_0 = ___action;
		Task_t165 * L_1 = (Task_t165 *)il2cpp_codegen_object_new (Task_t165_il2cpp_TypeInfo_var);
		Task__ctor_m562(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Task_t165 * L_2 = V_0;
		DispatcherBase_AddTask_m528(__this, L_2, /*hidden argument*/NULL);
		Task_t165 * L_3 = V_0;
		return L_3;
	}
}
// UnityThreading.TaskBase UnityThreading.DispatcherBase::Dispatch(UnityThreading.TaskBase)
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBase.h"
extern "C" TaskBase_t163 * DispatcherBase_Dispatch_m527 (DispatcherBase_t158 * __this, TaskBase_t163 * ___task, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(5 /* System.Void UnityThreading.DispatcherBase::CheckAccessLimitation() */, __this);
		TaskBase_t163 * L_0 = ___task;
		DispatcherBase_AddTask_m528(__this, L_0, /*hidden argument*/NULL);
		TaskBase_t163 * L_1 = ___task;
		return L_1;
	}
}
// System.Void UnityThreading.DispatcherBase::AddTask(UnityThreading.TaskBase)
// System.Threading.EventWaitHandle
#include "mscorlib_System_Threading_EventWaitHandleMethodDeclarations.h"
extern "C" void DispatcherBase_AddTask_m528 (DispatcherBase_t158 * __this, TaskBase_t163 * ___task, const MethodInfo* method)
{
	List_1_t159 * V_0 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t159 * L_0 = (__this->___taskList_0);
		V_0 = L_0;
		List_1_t159 * L_1 = V_0;
		Monitor_Enter_m1592(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			List_1_t159 * L_2 = (__this->___taskList_0);
			TaskBase_t163 * L_3 = ___task;
			NullCheck(L_2);
			VirtActionInvoker1< TaskBase_t163 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityThreading.TaskBase>::Add(!0) */, L_2, L_3);
			int32_t L_4 = (__this->___TaskSortingSystem_2);
			if ((((int32_t)L_4) == ((int32_t)1)))
			{
				goto IL_0031;
			}
		}

IL_0025:
		{
			int32_t L_5 = (__this->___TaskSortingSystem_2);
			if ((!(((uint32_t)L_5) == ((uint32_t)2))))
			{
				goto IL_0037;
			}
		}

IL_0031:
		{
			DispatcherBase_ReorderTasks_m530(__this, /*hidden argument*/NULL);
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x43, FINALLY_003c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		List_1_t159 * L_6 = V_0;
		Monitor_Exit_m1593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(60)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0043:
	{
		ManualResetEvent_t160 * L_7 = (__this->___dataEvent_1);
		NullCheck(L_7);
		EventWaitHandle_Set_m1594(L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityThreading.DispatcherBase::AddTasks(System.Collections.Generic.IEnumerable`1<UnityThreading.TaskBase>)
extern TypeInfo* IEnumerable_1_t350_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t424_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t337_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t364_il2cpp_TypeInfo_var;
extern "C" void DispatcherBase_AddTasks_m529 (DispatcherBase_t158 * __this, Object_t* ___tasks, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_1_t350_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(158);
		IEnumerator_1_t424_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		IEnumerator_t337_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		IDisposable_t364_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t159 * V_0 = {0};
	TaskBase_t163 * V_1 = {0};
	Object_t* V_2 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t159 * L_0 = (__this->___taskList_0);
		V_0 = L_0;
		List_1_t159 * L_1 = V_0;
		Monitor_Enter_m1592(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Object_t* L_2 = ___tasks;
			NullCheck(L_2);
			Object_t* L_3 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityThreading.TaskBase>::GetEnumerator() */, IEnumerable_1_t350_il2cpp_TypeInfo_var, L_2);
			V_2 = L_3;
		}

IL_0014:
		try
		{ // begin try (depth: 2)
			{
				goto IL_002c;
			}

IL_0019:
			{
				Object_t* L_4 = V_2;
				NullCheck(L_4);
				TaskBase_t163 * L_5 = (TaskBase_t163 *)InterfaceFuncInvoker0< TaskBase_t163 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityThreading.TaskBase>::get_Current() */, IEnumerator_1_t424_il2cpp_TypeInfo_var, L_4);
				V_1 = L_5;
				List_1_t159 * L_6 = (__this->___taskList_0);
				TaskBase_t163 * L_7 = V_1;
				NullCheck(L_6);
				VirtActionInvoker1< TaskBase_t163 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityThreading.TaskBase>::Add(!0) */, L_6, L_7);
			}

IL_002c:
			{
				Object_t* L_8 = V_2;
				NullCheck(L_8);
				bool L_9 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t337_il2cpp_TypeInfo_var, L_8);
				if (L_9)
				{
					goto IL_0019;
				}
			}

IL_0037:
			{
				IL2CPP_LEAVE(0x47, FINALLY_003c);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t359 *)e.ex;
			goto FINALLY_003c;
		}

FINALLY_003c:
		{ // begin finally (depth: 2)
			{
				Object_t* L_10 = V_2;
				if (L_10)
				{
					goto IL_0040;
				}
			}

IL_003f:
			{
				IL2CPP_END_FINALLY(60)
			}

IL_0040:
			{
				Object_t* L_11 = V_2;
				NullCheck(L_11);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t364_il2cpp_TypeInfo_var, L_11);
				IL2CPP_END_FINALLY(60)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(60)
		{
			IL2CPP_JUMP_TBL(0x47, IL_0047)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
		}

IL_0047:
		{
			int32_t L_12 = (__this->___TaskSortingSystem_2);
			if ((((int32_t)L_12) == ((int32_t)1)))
			{
				goto IL_005f;
			}
		}

IL_0053:
		{
			int32_t L_13 = (__this->___TaskSortingSystem_2);
			if ((!(((uint32_t)L_13) == ((uint32_t)2))))
			{
				goto IL_0065;
			}
		}

IL_005f:
		{
			DispatcherBase_ReorderTasks_m530(__this, /*hidden argument*/NULL);
		}

IL_0065:
		{
			IL2CPP_LEAVE(0x71, FINALLY_006a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_006a;
	}

FINALLY_006a:
	{ // begin finally (depth: 1)
		List_1_t159 * L_14 = V_0;
		Monitor_Exit_m1593(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(106)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(106)
	{
		IL2CPP_JUMP_TBL(0x71, IL_0071)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0071:
	{
		ManualResetEvent_t160 * L_15 = (__this->___dataEvent_1);
		NullCheck(L_15);
		EventWaitHandle_Set_m1594(L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityThreading.DispatcherBase::ReorderTasks()
// System.Comparison`1<UnityThreading.TaskBase>
#include "mscorlib_System_Comparison_1_genMethodDeclarations.h"
extern TypeInfo* DispatcherBase_t158_il2cpp_TypeInfo_var;
extern TypeInfo* Comparison_1_t161_il2cpp_TypeInfo_var;
extern const MethodInfo* DispatcherBase_U3CReorderTasksU3Em__3_m534_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m1595_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m1596_MethodInfo_var;
extern "C" void DispatcherBase_ReorderTasks_m530 (DispatcherBase_t158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DispatcherBase_t158_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		Comparison_1_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		DispatcherBase_U3CReorderTasksU3Em__3_m534_MethodInfo_var = il2cpp_codegen_method_info_from_index(83);
		Comparison_1__ctor_m1595_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483732);
		List_1_Sort_m1596_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483733);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t159 * G_B2_0 = {0};
	List_1_t159 * G_B1_0 = {0};
	{
		List_1_t159 * L_0 = (__this->___taskList_0);
		Comparison_1_t161 * L_1 = ((DispatcherBase_t158_StaticFields*)DispatcherBase_t158_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2 = { (void*)DispatcherBase_U3CReorderTasksU3Em__3_m534_MethodInfo_var };
		Comparison_1_t161 * L_3 = (Comparison_1_t161 *)il2cpp_codegen_object_new (Comparison_1_t161_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1595(L_3, NULL, L_2, /*hidden argument*/Comparison_1__ctor_m1595_MethodInfo_var);
		((DispatcherBase_t158_StaticFields*)DispatcherBase_t158_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3 = L_3;
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Comparison_1_t161 * L_4 = ((DispatcherBase_t158_StaticFields*)DispatcherBase_t158_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_3;
		NullCheck(G_B2_0);
		List_1_Sort_m1596(G_B2_0, L_4, /*hidden argument*/List_1_Sort_m1596_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<UnityThreading.TaskBase> UnityThreading.DispatcherBase::SplitTasks(System.Int32)
// System.Int32
#include "mscorlib_System_Int32.h"
extern "C" Object_t* DispatcherBase_SplitTasks_m531 (DispatcherBase_t158 * __this, int32_t ___divisor, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___divisor;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		___divisor = 2;
	}

IL_0009:
	{
		int32_t L_1 = DispatcherBase_get_TaskCount_m525(__this, /*hidden argument*/NULL);
		int32_t L_2 = ___divisor;
		V_0 = ((int32_t)((int32_t)L_1/(int32_t)L_2));
		int32_t L_3 = V_0;
		Object_t* L_4 = DispatcherBase_IsolateTasks_m532(__this, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Collections.Generic.IEnumerable`1<UnityThreading.TaskBase> UnityThreading.DispatcherBase::IsolateTasks(System.Int32)
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
extern TypeInfo* List_1_t159_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1590_MethodInfo_var;
extern const MethodInfo* Enumerable_Take_TisTaskBase_t163_m1597_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1598_MethodInfo_var;
extern const MethodInfo* List_1_RemoveRange_m1600_MethodInfo_var;
extern "C" Object_t* DispatcherBase_IsolateTasks_m532 (DispatcherBase_t158 * __this, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t159_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		List_1__ctor_m1590_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483730);
		Enumerable_Take_TisTaskBase_t163_m1597_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483734);
		List_1_AddRange_m1598_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483735);
		List_1_RemoveRange_m1600_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483736);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t159 * V_0 = {0};
	List_1_t159 * V_1 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t159 * L_0 = (List_1_t159 *)il2cpp_codegen_object_new (List_1_t159_il2cpp_TypeInfo_var);
		List_1__ctor_m1590(L_0, /*hidden argument*/List_1__ctor_m1590_MethodInfo_var);
		V_0 = L_0;
		int32_t L_1 = ___count;
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		List_1_t159 * L_2 = (__this->___taskList_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityThreading.TaskBase>::get_Count() */, L_2);
		___count = L_3;
	}

IL_0019:
	{
		List_1_t159 * L_4 = (__this->___taskList_0);
		V_1 = L_4;
		List_1_t159 * L_5 = V_1;
		Monitor_Enter_m1592(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0026:
	try
	{ // begin try (depth: 1)
		{
			List_1_t159 * L_6 = V_0;
			List_1_t159 * L_7 = (__this->___taskList_0);
			int32_t L_8 = ___count;
			Object_t* L_9 = Enumerable_Take_TisTaskBase_t163_m1597(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/Enumerable_Take_TisTaskBase_t163_m1597_MethodInfo_var);
			NullCheck(L_6);
			List_1_AddRange_m1598(L_6, L_9, /*hidden argument*/List_1_AddRange_m1598_MethodInfo_var);
			List_1_t159 * L_10 = (__this->___taskList_0);
			int32_t L_11 = ___count;
			List_1_t159 * L_12 = (__this->___taskList_0);
			NullCheck(L_12);
			int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityThreading.TaskBase>::get_Count() */, L_12);
			int32_t L_14 = Math_Min_m1599(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
			NullCheck(L_10);
			List_1_RemoveRange_m1600(L_10, 0, L_14, /*hidden argument*/List_1_RemoveRange_m1600_MethodInfo_var);
			int32_t L_15 = (__this->___TaskSortingSystem_2);
			if ((!(((uint32_t)L_15) == ((uint32_t)2))))
			{
				goto IL_0067;
			}
		}

IL_0061:
		{
			DispatcherBase_ReorderTasks_m530(__this, /*hidden argument*/NULL);
		}

IL_0067:
		{
			IL2CPP_LEAVE(0x73, FINALLY_006c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		List_1_t159 * L_16 = V_1;
		Monitor_Exit_m1593(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0073:
	{
		int32_t L_17 = DispatcherBase_get_TaskCount_m525(__this, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_008a;
		}
	}
	{
		ManualResetEvent_t160 * L_18 = (__this->___dataEvent_1);
		NullCheck(L_18);
		EventWaitHandle_Reset_m1601(L_18, /*hidden argument*/NULL);
	}

IL_008a:
	{
		List_1_t159 * L_19 = V_0;
		return L_19;
	}
}
// System.Void UnityThreading.DispatcherBase::Dispose()
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBaseMethodDeclarations.h"
extern "C" void DispatcherBase_Dispose_m533 (DispatcherBase_t158 * __this, const MethodInfo* method)
{
	TaskBase_t163 * V_0 = {0};
	List_1_t159 * V_1 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		List_1_t159 * L_0 = (__this->___taskList_0);
		V_1 = L_0;
		List_1_t159 * L_1 = V_1;
		Monitor_Enter_m1592(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			List_1_t159 * L_2 = (__this->___taskList_0);
			NullCheck(L_2);
			int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityThreading.TaskBase>::get_Count() */, L_2);
			if (!L_3)
			{
				goto IL_003b;
			}
		}

IL_001d:
		{
			List_1_t159 * L_4 = (__this->___taskList_0);
			NullCheck(L_4);
			TaskBase_t163 * L_5 = (TaskBase_t163 *)VirtFuncInvoker1< TaskBase_t163 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityThreading.TaskBase>::get_Item(System.Int32) */, L_4, 0);
			V_0 = L_5;
			List_1_t159 * L_6 = (__this->___taskList_0);
			NullCheck(L_6);
			VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<UnityThreading.TaskBase>::RemoveAt(System.Int32) */, L_6, 0);
			goto IL_0040;
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x57, FINALLY_0045);
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x4C, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		List_1_t159 * L_7 = V_1;
		Monitor_Exit_m1593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x57, IL_0057)
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_004c:
	{
		TaskBase_t163 * L_8 = V_0;
		NullCheck(L_8);
		TaskBase_Dispose_m561(L_8, /*hidden argument*/NULL);
		goto IL_0000;
	}

IL_0057:
	{
		ManualResetEvent_t160 * L_9 = (__this->___dataEvent_1);
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(5 /* System.Void System.Threading.WaitHandle::Close() */, L_9);
		__this->___dataEvent_1 = (ManualResetEvent_t160 *)NULL;
		return;
	}
}
// System.Int32 UnityThreading.DispatcherBase::<ReorderTasks>m__3(UnityThreading.TaskBase,UnityThreading.TaskBase)
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
extern "C" int32_t DispatcherBase_U3CReorderTasksU3Em__3_m534 (Object_t * __this /* static, unused */, TaskBase_t163 * ___a, TaskBase_t163 * ___b, const MethodInfo* method)
{
	{
		TaskBase_t163 * L_0 = ___a;
		NullCheck(L_0);
		int32_t* L_1 = &(L_0->___Priority_0);
		il2cpp_codegen_memory_barrier();
		TaskBase_t163 * L_2 = ___b;
		NullCheck(L_2);
		int32_t L_3 = (L_2->___Priority_0);
		il2cpp_codegen_memory_barrier();
		int32_t L_4 = Int32_CompareTo_m1602(L_1, L_3, /*hidden argument*/NULL);
		return ((-L_4));
	}
}
// UnityThreading.Dispatcher
#include "AssemblyU2DCSharp_UnityThreading_Dispatcher.h"
// UnityThreading.Dispatcher
#include "AssemblyU2DCSharp_UnityThreading_DispatcherMethodDeclarations.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Void UnityThreading.Dispatcher::.ctor()
// UnityThreading.Dispatcher
#include "AssemblyU2DCSharp_UnityThreading_DispatcherMethodDeclarations.h"
extern "C" void Dispatcher__ctor_m535 (Dispatcher_t162 * __this, const MethodInfo* method)
{
	{
		Dispatcher__ctor_m536(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityThreading.Dispatcher::.ctor(System.Boolean)
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityThreading.DispatcherBase
#include "AssemblyU2DCSharp_UnityThreading_DispatcherBaseMethodDeclarations.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern TypeInfo* Dispatcher_t162_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral95;
extern "C" void Dispatcher__ctor_m536 (Dispatcher_t162 * __this, bool ___setThreadDefaults, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dispatcher_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral95 = il2cpp_codegen_string_literal_from_index(95);
		s_Il2CppMethodIntialized = true;
	}
	{
		DispatcherBase__ctor_m524(__this, /*hidden argument*/NULL);
		bool L_0 = ___setThreadDefaults;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Dispatcher_t162 * L_1 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentDispatcher_5;
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		InvalidOperationException_t425 * L_2 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_2, _stringLiteral95, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0022:
	{
		((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentDispatcher_5 = __this;
		Dispatcher_t162 * L_3 = ((Dispatcher_t162_StaticFields*)Dispatcher_t162_il2cpp_TypeInfo_var->static_fields)->___mainDispatcher_6;
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		((Dispatcher_t162_StaticFields*)Dispatcher_t162_il2cpp_TypeInfo_var->static_fields)->___mainDispatcher_6 = __this;
	}

IL_0038:
	{
		return;
	}
}
// UnityThreading.TaskBase UnityThreading.Dispatcher::get_CurrentTask()
extern TypeInfo* Dispatcher_t162_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96;
extern "C" TaskBase_t163 * Dispatcher_get_CurrentTask_m537 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dispatcher_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral96 = il2cpp_codegen_string_literal_from_index(96);
		s_Il2CppMethodIntialized = true;
	}
	{
		TaskBase_t163 * L_0 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentTask_4;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, _stringLiteral96, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0015:
	{
		TaskBase_t163 * L_2 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentTask_4;
		return L_2;
	}
}
// UnityThreading.Dispatcher UnityThreading.Dispatcher::get_Current()
extern TypeInfo* Dispatcher_t162_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral97;
extern "C" Dispatcher_t162 * Dispatcher_get_Current_m538 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dispatcher_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral97 = il2cpp_codegen_string_literal_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dispatcher_t162 * L_0 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentDispatcher_5;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, _stringLiteral97, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0015:
	{
		Dispatcher_t162 * L_2 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentDispatcher_5;
		return L_2;
	}
}
// System.Void UnityThreading.Dispatcher::set_Current(UnityThreading.Dispatcher)
// UnityThreading.Dispatcher
#include "AssemblyU2DCSharp_UnityThreading_Dispatcher.h"
extern TypeInfo* Dispatcher_t162_il2cpp_TypeInfo_var;
extern "C" void Dispatcher_set_Current_m539 (Object_t * __this /* static, unused */, Dispatcher_t162 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dispatcher_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dispatcher_t162 * L_0 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentDispatcher_5;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Dispatcher_t162 * L_1 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentDispatcher_5;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityThreading.Dispatcher::Dispose() */, L_1);
	}

IL_0014:
	{
		Dispatcher_t162 * L_2 = ___value;
		((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentDispatcher_5 = L_2;
		return;
	}
}
// UnityThreading.Dispatcher UnityThreading.Dispatcher::get_Main()
extern TypeInfo* Dispatcher_t162_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral98;
extern "C" Dispatcher_t162 * Dispatcher_get_Main_m540 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dispatcher_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral98 = il2cpp_codegen_string_literal_from_index(98);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dispatcher_t162 * L_0 = ((Dispatcher_t162_StaticFields*)Dispatcher_t162_il2cpp_TypeInfo_var->static_fields)->___mainDispatcher_6;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, _stringLiteral98, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0015:
	{
		Dispatcher_t162 * L_2 = ((Dispatcher_t162_StaticFields*)Dispatcher_t162_il2cpp_TypeInfo_var->static_fields)->___mainDispatcher_6;
		return L_2;
	}
}
// System.Void UnityThreading.Dispatcher::ProcessTasks()
extern "C" void Dispatcher_ProcessTasks_m541 (Dispatcher_t162 * __this, const MethodInfo* method)
{
	{
		ManualResetEvent_t160 * L_0 = (((DispatcherBase_t158 *)__this)->___dataEvent_1);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker2< bool, int32_t, bool >::Invoke(10 /* System.Boolean System.Threading.WaitHandle::WaitOne(System.Int32,System.Boolean) */, L_0, 0, 0);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Dispatcher_ProcessTasksInternal_m545(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Boolean UnityThreading.Dispatcher::ProcessTasks(System.Threading.WaitHandle)
// System.Threading.WaitHandle
#include "mscorlib_System_Threading_WaitHandle.h"
// System.Threading.WaitHandle
#include "mscorlib_System_Threading_WaitHandleMethodDeclarations.h"
extern TypeInfo* WaitHandleU5BU5D_t426_il2cpp_TypeInfo_var;
extern TypeInfo* WaitHandle_t351_il2cpp_TypeInfo_var;
extern "C" bool Dispatcher_ProcessTasks_m542 (Dispatcher_t162 * __this, WaitHandle_t351 * ___exitHandle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitHandleU5BU5D_t426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(164);
		WaitHandle_t351_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(165);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		WaitHandleU5BU5D_t426* L_0 = ((WaitHandleU5BU5D_t426*)SZArrayNew(WaitHandleU5BU5D_t426_il2cpp_TypeInfo_var, 2));
		WaitHandle_t351 * L_1 = ___exitHandle;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((WaitHandle_t351 **)(WaitHandle_t351 **)SZArrayLdElema(L_0, 0, sizeof(WaitHandle_t351 *))) = (WaitHandle_t351 *)L_1;
		WaitHandleU5BU5D_t426* L_2 = L_0;
		ManualResetEvent_t160 * L_3 = (((DispatcherBase_t158 *)__this)->___dataEvent_1);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((WaitHandle_t351 **)(WaitHandle_t351 **)SZArrayLdElema(L_2, 1, sizeof(WaitHandle_t351 *))) = (WaitHandle_t351 *)L_3;
		IL2CPP_RUNTIME_CLASS_INIT(WaitHandle_t351_il2cpp_TypeInfo_var);
		int32_t L_4 = WaitHandle_WaitAny_m1604(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if (L_5)
		{
			goto IL_0021;
		}
	}
	{
		return 0;
	}

IL_0021:
	{
		Dispatcher_ProcessTasksInternal_m545(__this, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Boolean UnityThreading.Dispatcher::ProcessNextTask()
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
// System.Threading.EventWaitHandle
#include "mscorlib_System_Threading_EventWaitHandleMethodDeclarations.h"
extern "C" bool Dispatcher_ProcessNextTask_m543 (Dispatcher_t162 * __this, const MethodInfo* method)
{
	List_1_t159 * V_0 = {0};
	bool V_1 = false;
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t159 * L_0 = (((DispatcherBase_t158 *)__this)->___taskList_0);
		V_0 = L_0;
		List_1_t159 * L_1 = V_0;
		Monitor_Enter_m1592(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			List_1_t159 * L_2 = (((DispatcherBase_t158 *)__this)->___taskList_0);
			NullCheck(L_2);
			int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityThreading.TaskBase>::get_Count() */, L_2);
			if (L_3)
			{
				goto IL_0024;
			}
		}

IL_001d:
		{
			V_1 = 0;
			IL2CPP_LEAVE(0x4F, FINALLY_002f);
		}

IL_0024:
		{
			Dispatcher_ProcessSingleTask_m546(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		List_1_t159 * L_4 = V_0;
		Monitor_Exit_m1593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_JUMP_TBL(0x36, IL_0036)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0036:
	{
		int32_t L_5 = DispatcherBase_get_TaskCount_m525(__this, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004d;
		}
	}
	{
		ManualResetEvent_t160 * L_6 = (((DispatcherBase_t158 *)__this)->___dataEvent_1);
		NullCheck(L_6);
		EventWaitHandle_Reset_m1601(L_6, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return 1;
	}

IL_004f:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityThreading.Dispatcher::ProcessNextTask(System.Threading.WaitHandle)
extern TypeInfo* WaitHandleU5BU5D_t426_il2cpp_TypeInfo_var;
extern TypeInfo* WaitHandle_t351_il2cpp_TypeInfo_var;
extern "C" bool Dispatcher_ProcessNextTask_m544 (Dispatcher_t162 * __this, WaitHandle_t351 * ___exitHandle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitHandleU5BU5D_t426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(164);
		WaitHandle_t351_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(165);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	List_1_t159 * V_1 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		WaitHandleU5BU5D_t426* L_0 = ((WaitHandleU5BU5D_t426*)SZArrayNew(WaitHandleU5BU5D_t426_il2cpp_TypeInfo_var, 2));
		WaitHandle_t351 * L_1 = ___exitHandle;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((WaitHandle_t351 **)(WaitHandle_t351 **)SZArrayLdElema(L_0, 0, sizeof(WaitHandle_t351 *))) = (WaitHandle_t351 *)L_1;
		WaitHandleU5BU5D_t426* L_2 = L_0;
		ManualResetEvent_t160 * L_3 = (((DispatcherBase_t158 *)__this)->___dataEvent_1);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((WaitHandle_t351 **)(WaitHandle_t351 **)SZArrayLdElema(L_2, 1, sizeof(WaitHandle_t351 *))) = (WaitHandle_t351 *)L_3;
		IL2CPP_RUNTIME_CLASS_INIT(WaitHandle_t351_il2cpp_TypeInfo_var);
		int32_t L_4 = WaitHandle_WaitAny_m1604(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if (L_5)
		{
			goto IL_0021;
		}
	}
	{
		return 0;
	}

IL_0021:
	{
		List_1_t159 * L_6 = (((DispatcherBase_t158 *)__this)->___taskList_0);
		V_1 = L_6;
		List_1_t159 * L_7 = V_1;
		Monitor_Enter_m1592(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		Dispatcher_ProcessSingleTask_m546(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x40, FINALLY_0039);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		List_1_t159 * L_8 = V_1;
		Monitor_Exit_m1593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(57)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0040:
	{
		int32_t L_9 = DispatcherBase_get_TaskCount_m525(__this, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0057;
		}
	}
	{
		ManualResetEvent_t160 * L_10 = (((DispatcherBase_t158 *)__this)->___dataEvent_1);
		NullCheck(L_10);
		EventWaitHandle_Reset_m1601(L_10, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return 1;
	}
}
// System.Void UnityThreading.Dispatcher::ProcessTasksInternal()
extern "C" void Dispatcher_ProcessTasksInternal_m545 (Dispatcher_t162 * __this, const MethodInfo* method)
{
	List_1_t159 * V_0 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t159 * L_0 = (((DispatcherBase_t158 *)__this)->___taskList_0);
		V_0 = L_0;
		List_1_t159 * L_1 = V_0;
		Monitor_Enter_m1592(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0018;
		}

IL_0012:
		{
			Dispatcher_ProcessSingleTask_m546(__this, /*hidden argument*/NULL);
		}

IL_0018:
		{
			List_1_t159 * L_2 = (((DispatcherBase_t158 *)__this)->___taskList_0);
			NullCheck(L_2);
			int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityThreading.TaskBase>::get_Count() */, L_2);
			if (L_3)
			{
				goto IL_0012;
			}
		}

IL_0028:
		{
			IL2CPP_LEAVE(0x34, FINALLY_002d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		List_1_t159 * L_4 = V_0;
		Monitor_Exit_m1593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(45)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x34, IL_0034)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0034:
	{
		int32_t L_5 = DispatcherBase_get_TaskCount_m525(__this, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004b;
		}
	}
	{
		ManualResetEvent_t160 * L_6 = (((DispatcherBase_t158 *)__this)->___dataEvent_1);
		NullCheck(L_6);
		EventWaitHandle_Reset_m1601(L_6, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
// System.Void UnityThreading.Dispatcher::ProcessSingleTask()
extern "C" void Dispatcher_ProcessSingleTask_m546 (Dispatcher_t162 * __this, const MethodInfo* method)
{
	TaskBase_t163 * V_0 = {0};
	{
		List_1_t159 * L_0 = (((DispatcherBase_t158 *)__this)->___taskList_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityThreading.TaskBase>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		List_1_t159 * L_2 = (((DispatcherBase_t158 *)__this)->___taskList_0);
		NullCheck(L_2);
		TaskBase_t163 * L_3 = (TaskBase_t163 *)VirtFuncInvoker1< TaskBase_t163 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityThreading.TaskBase>::get_Item(System.Int32) */, L_2, 0);
		V_0 = L_3;
		List_1_t159 * L_4 = (((DispatcherBase_t158 *)__this)->___taskList_0);
		NullCheck(L_4);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<UnityThreading.TaskBase>::RemoveAt(System.Int32) */, L_4, 0);
		TaskBase_t163 * L_5 = V_0;
		Dispatcher_RunTask_m547(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = (((DispatcherBase_t158 *)__this)->___TaskSortingSystem_2);
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_0043;
		}
	}
	{
		DispatcherBase_ReorderTasks_m530(__this, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityThreading.Dispatcher::RunTask(UnityThreading.TaskBase)
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBase.h"
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBaseMethodDeclarations.h"
extern TypeInfo* Dispatcher_t162_il2cpp_TypeInfo_var;
extern "C" void Dispatcher_RunTask_m547 (Dispatcher_t162 * __this, TaskBase_t163 * ___task, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dispatcher_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	TaskBase_t163 * V_0 = {0};
	{
		TaskBase_t163 * L_0 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentTask_4;
		V_0 = L_0;
		TaskBase_t163 * L_1 = ___task;
		((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentTask_4 = L_1;
		TaskBase_t163 * L_2 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentTask_4;
		NullCheck(L_2);
		TaskBase_DoInternal_m560(L_2, /*hidden argument*/NULL);
		TaskBase_t163 * L_3 = V_0;
		((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentTask_4 = L_3;
		return;
	}
}
// System.Void UnityThreading.Dispatcher::CheckAccessLimitation()
extern TypeInfo* Dispatcher_t162_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral99;
extern "C" void Dispatcher_CheckAccessLimitation_m548 (Dispatcher_t162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dispatcher_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		InvalidOperationException_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral99 = il2cpp_codegen_string_literal_from_index(99);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dispatcher_t162 * L_0 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentDispatcher_5;
		if ((!(((Object_t*)(Dispatcher_t162 *)L_0) == ((Object_t*)(Dispatcher_t162 *)__this))))
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t425 * L_1 = (InvalidOperationException_t425 *)il2cpp_codegen_object_new (InvalidOperationException_t425_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1603(L_1, _stringLiteral99, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityThreading.Dispatcher::Dispose()
extern TypeInfo* Dispatcher_t162_il2cpp_TypeInfo_var;
extern "C" void Dispatcher_Dispose_m549 (Dispatcher_t162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dispatcher_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(162);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t159 * V_0 = {0};
	Exception_t359 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t359 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		List_1_t159 * L_0 = (((DispatcherBase_t158 *)__this)->___taskList_0);
		V_0 = L_0;
		List_1_t159 * L_1 = V_0;
		Monitor_Enter_m1592(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			List_1_t159 * L_2 = (((DispatcherBase_t158 *)__this)->___taskList_0);
			NullCheck(L_2);
			int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityThreading.TaskBase>::get_Count() */, L_2);
			if (!L_3)
			{
				goto IL_003f;
			}
		}

IL_001d:
		{
			List_1_t159 * L_4 = (((DispatcherBase_t158 *)__this)->___taskList_0);
			NullCheck(L_4);
			TaskBase_t163 * L_5 = (TaskBase_t163 *)VirtFuncInvoker1< TaskBase_t163 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityThreading.TaskBase>::get_Item(System.Int32) */, L_4, 0);
			((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentTask_4 = L_5;
			List_1_t159 * L_6 = (((DispatcherBase_t158 *)__this)->___taskList_0);
			NullCheck(L_6);
			VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<UnityThreading.TaskBase>::RemoveAt(System.Int32) */, L_6, 0);
			goto IL_0044;
		}

IL_003f:
		{
			IL2CPP_LEAVE(0x5F, FINALLY_0049);
		}

IL_0044:
		{
			IL2CPP_LEAVE(0x50, FINALLY_0049);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t359 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		List_1_t159 * L_7 = V_0;
		Monitor_Exit_m1593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(73)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t359 *)
	}

IL_0050:
	{
		TaskBase_t163 * L_8 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentTask_4;
		NullCheck(L_8);
		TaskBase_Dispose_m561(L_8, /*hidden argument*/NULL);
		goto IL_0000;
	}

IL_005f:
	{
		ManualResetEvent_t160 * L_9 = (((DispatcherBase_t158 *)__this)->___dataEvent_1);
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(5 /* System.Void System.Threading.WaitHandle::Close() */, L_9);
		((DispatcherBase_t158 *)__this)->___dataEvent_1 = (ManualResetEvent_t160 *)NULL;
		Dispatcher_t162 * L_10 = ((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentDispatcher_5;
		if ((!(((Object_t*)(Dispatcher_t162 *)L_10) == ((Object_t*)(Dispatcher_t162 *)__this))))
		{
			goto IL_0082;
		}
	}
	{
		((Dispatcher_t162_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(Dispatcher_t162_il2cpp_TypeInfo_var))->___currentDispatcher_5 = (Dispatcher_t162 *)NULL;
	}

IL_0082:
	{
		Dispatcher_t162 * L_11 = ((Dispatcher_t162_StaticFields*)Dispatcher_t162_il2cpp_TypeInfo_var->static_fields)->___mainDispatcher_6;
		if ((!(((Object_t*)(Dispatcher_t162 *)L_11) == ((Object_t*)(Dispatcher_t162 *)__this))))
		{
			goto IL_0093;
		}
	}
	{
		((Dispatcher_t162_StaticFields*)Dispatcher_t162_il2cpp_TypeInfo_var->static_fields)->___mainDispatcher_6 = (Dispatcher_t162 *)NULL;
	}

IL_0093:
	{
		return;
	}
}
// UnityThreading.TaskSortingSystem
#include "AssemblyU2DCSharp_UnityThreading_TaskSortingSystemMethodDeclarations.h"
#ifdef __clang__
#pragma clang diagnostic pop
#endif
