﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// Common.ColorUtils
struct  ColorUtils_t8  : public Object_t
{
};
struct ColorUtils_t8_StaticFields{
	// UnityEngine.Color Common.ColorUtils::BLACK
	Color_t9  ___BLACK_0;
	// UnityEngine.Color Common.ColorUtils::WHITE
	Color_t9  ___WHITE_1;
	// UnityEngine.Color Common.ColorUtils::GREEN
	Color_t9  ___GREEN_2;
};
