﻿#pragma once
#include <stdint.h>
// System.DBNull
struct DBNull_t2320;
// System.Object
#include "mscorlib_System_Object.h"
// System.DBNull
struct  DBNull_t2320  : public Object_t
{
};
struct DBNull_t2320_StaticFields{
	// System.DBNull System.DBNull::Value
	DBNull_t2320 * ___Value_0;
};
