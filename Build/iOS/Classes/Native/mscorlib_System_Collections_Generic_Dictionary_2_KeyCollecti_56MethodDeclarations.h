﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_30MethodDeclarations.h"
#define Enumerator__ctor_m27919(__this, ___host, method) (( void (*) (Enumerator_t3417 *, Dictionary_2_t1202 *, const MethodInfo*))Enumerator__ctor_m19831_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27920(__this, method) (( Object_t * (*) (Enumerator_t3417 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19832_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::Dispose()
#define Enumerator_Dispose_m27921(__this, method) (( void (*) (Enumerator_t3417 *, const MethodInfo*))Enumerator_Dispose_m19833_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::MoveNext()
#define Enumerator_MoveNext_m27922(__this, method) (( bool (*) (Enumerator_t3417 *, const MethodInfo*))Enumerator_MoveNext_m19834_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::get_Current()
#define Enumerator_get_Current_m27923(__this, method) (( int32_t (*) (Enumerator_t3417 *, const MethodInfo*))Enumerator_get_Current_m19835_gshared)(__this, method)
