﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.EventArgs
struct EventArgs_t1452;

// System.Void System.EventArgs::.ctor()
extern "C" void EventArgs__ctor_m14157 (EventArgs_t1452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventArgs::.cctor()
extern "C" void EventArgs__cctor_m14158 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
