﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Utils.SerializedKeyValue`2<System.Object,System.Object>
struct  SerializedKeyValue_2_t2634  : public Object_t
{
	// K Common.Utils.SerializedKeyValue`2<System.Object,System.Object>::key
	Object_t * ___key_0;
	// V Common.Utils.SerializedKeyValue`2<System.Object,System.Object>::value
	Object_t * ___value_1;
};
