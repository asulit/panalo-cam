﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16MethodDeclarations.h"
#define Enumerator__ctor_m17553(__this, ___dictionary, method) (( void (*) (Enumerator_t2707 *, Dictionary_2_t192 *, const MethodInfo*))Enumerator__ctor_m17454_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17554(__this, method) (( Object_t * (*) (Enumerator_t2707 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17455_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17555(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t2707 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17456_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17556(__this, method) (( Object_t * (*) (Enumerator_t2707 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17457_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17557(__this, method) (( Object_t * (*) (Enumerator_t2707 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17458_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::MoveNext()
#define Enumerator_MoveNext_m17558(__this, method) (( bool (*) (Enumerator_t2707 *, const MethodInfo*))Enumerator_MoveNext_m17459_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::get_Current()
#define Enumerator_get_Current_m17559(__this, method) (( KeyValuePair_2_t2704  (*) (Enumerator_t2707 *, const MethodInfo*))Enumerator_get_Current_m17460_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17560(__this, method) (( int32_t (*) (Enumerator_t2707 *, const MethodInfo*))Enumerator_get_CurrentKey_m17461_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17561(__this, method) (( String_t* (*) (Enumerator_t2707 *, const MethodInfo*))Enumerator_get_CurrentValue_m17462_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::VerifyState()
#define Enumerator_VerifyState_m17562(__this, method) (( void (*) (Enumerator_t2707 *, const MethodInfo*))Enumerator_VerifyState_m17463_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17563(__this, method) (( void (*) (Enumerator_t2707 *, const MethodInfo*))Enumerator_VerifyCurrent_m17464_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::Dispose()
#define Enumerator_Dispose_m17564(__this, method) (( void (*) (Enumerator_t2707 *, const MethodInfo*))Enumerator_Dispose_m17465_gshared)(__this, method)
