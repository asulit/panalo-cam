﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t676;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_62.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m22315_gshared (Enumerator_t3039 * __this, List_1_t676 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m22315(__this, ___l, method) (( void (*) (Enumerator_t3039 *, List_1_t676 *, const MethodInfo*))Enumerator__ctor_m22315_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22316_gshared (Enumerator_t3039 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22316(__this, method) (( Object_t * (*) (Enumerator_t3039 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22316_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C" void Enumerator_Dispose_m22317_gshared (Enumerator_t3039 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22317(__this, method) (( void (*) (Enumerator_t3039 *, const MethodInfo*))Enumerator_Dispose_m22317_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern "C" void Enumerator_VerifyState_m22318_gshared (Enumerator_t3039 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22318(__this, method) (( void (*) (Enumerator_t3039 *, const MethodInfo*))Enumerator_VerifyState_m22318_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22319_gshared (Enumerator_t3039 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22319(__this, method) (( bool (*) (Enumerator_t3039 *, const MethodInfo*))Enumerator_MoveNext_m22319_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C" Color32_t711  Enumerator_get_Current_m22320_gshared (Enumerator_t3039 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22320(__this, method) (( Color32_t711  (*) (Enumerator_t3039 *, const MethodInfo*))Enumerator_get_Current_m22320_gshared)(__this, method)
