﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void TargetSearchResult_t1212_marshal(const TargetSearchResult_t1212& unmarshaled, TargetSearchResult_t1212_marshaled& marshaled);
extern "C" void TargetSearchResult_t1212_marshal_back(const TargetSearchResult_t1212_marshaled& marshaled, TargetSearchResult_t1212& unmarshaled);
extern "C" void TargetSearchResult_t1212_marshal_cleanup(TargetSearchResult_t1212_marshaled& marshaled);
