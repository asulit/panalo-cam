﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparer_1_t3279;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C" void Comparer_1__ctor_m25366_gshared (Comparer_1_t3279 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m25366(__this, method) (( void (*) (Comparer_1_t3279 *, const MethodInfo*))Comparer_1__ctor_m25366_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
extern "C" void Comparer_1__cctor_m25367_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m25367(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m25367_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m25368_gshared (Comparer_1_t3279 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m25368(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3279 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m25368_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.Image/PIXEL_FORMAT>::get_Default()
extern "C" Comparer_1_t3279 * Comparer_1_get_Default_m25369_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m25369(__this /* static, unused */, method) (( Comparer_1_t3279 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m25369_gshared)(__this /* static, unused */, method)
