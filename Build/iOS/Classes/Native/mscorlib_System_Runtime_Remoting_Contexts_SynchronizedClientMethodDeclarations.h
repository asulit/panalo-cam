﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Contexts.SynchronizedClientContextSink
struct SynchronizedClientContextSink_t2084;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t2077;
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
struct SynchronizationAttribute_t2082;

// System.Void System.Runtime.Remoting.Contexts.SynchronizedClientContextSink::.ctor(System.Runtime.Remoting.Messaging.IMessageSink,System.Runtime.Remoting.Contexts.SynchronizationAttribute)
extern "C" void SynchronizedClientContextSink__ctor_m12425 (SynchronizedClientContextSink_t2084 * __this, Object_t * ___next, SynchronizationAttribute_t2082 * ___att, const MethodInfo* method) IL2CPP_METHOD_ATTR;
