﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.IEnumerable`1<System.Enum>
struct IEnumerable_1_t339;
// System.Enum
struct Enum_t21;
// System.Enum[]
struct EnumU5BU5D_t338;
// System.Type
struct Type_t;

// System.Collections.Generic.IEnumerable`1<System.Enum> Common.Extensions.EnumExtension::GetFlags(System.Enum)
extern "C" Object_t* EnumExtension_GetFlags_m87 (Object_t * __this /* static, unused */, Enum_t21 * ___p_value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Enum> Common.Extensions.EnumExtension::GetIndividualFlags(System.Enum)
extern "C" Object_t* EnumExtension_GetIndividualFlags_m88 (Object_t * __this /* static, unused */, Enum_t21 * ___p_value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Enum> Common.Extensions.EnumExtension::GetFlags(System.Enum,System.Enum[])
extern "C" Object_t* EnumExtension_GetFlags_m89 (Object_t * __this /* static, unused */, Enum_t21 * ___p_value, EnumU5BU5D_t338* ___p_values, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Enum> Common.Extensions.EnumExtension::GetFlagValues(System.Type)
extern "C" Object_t* EnumExtension_GetFlagValues_m90 (Object_t * __this /* static, unused */, Type_t * ___p_enumType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
