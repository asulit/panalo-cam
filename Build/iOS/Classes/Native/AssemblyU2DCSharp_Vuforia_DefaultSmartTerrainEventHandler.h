﻿#pragma once
#include <stdint.h>
// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t278;
// Vuforia.PropBehaviour
struct PropBehaviour_t279;
// Vuforia.SurfaceBehaviour
struct SurfaceBehaviour_t280;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.DefaultSmartTerrainEventHandler
struct  DefaultSmartTerrainEventHandler_t277  : public MonoBehaviour_t5
{
	// Vuforia.ReconstructionBehaviour Vuforia.DefaultSmartTerrainEventHandler::mReconstructionBehaviour
	ReconstructionBehaviour_t278 * ___mReconstructionBehaviour_2;
	// Vuforia.PropBehaviour Vuforia.DefaultSmartTerrainEventHandler::PropTemplate
	PropBehaviour_t279 * ___PropTemplate_3;
	// Vuforia.SurfaceBehaviour Vuforia.DefaultSmartTerrainEventHandler::SurfaceTemplate
	SurfaceBehaviour_t280 * ___SurfaceTemplate_4;
};
