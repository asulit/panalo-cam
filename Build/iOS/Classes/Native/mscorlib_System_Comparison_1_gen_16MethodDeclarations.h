﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<AnimatorFrame>
struct Comparison_1_t2780;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"

// System.Void System.Comparison`1<AnimatorFrame>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m18433_gshared (Comparison_1_t2780 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m18433(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2780 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m18433_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<AnimatorFrame>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m18434_gshared (Comparison_1_t2780 * __this, AnimatorFrame_t235  ___x, AnimatorFrame_t235  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m18434(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2780 *, AnimatorFrame_t235 , AnimatorFrame_t235 , const MethodInfo*))Comparison_1_Invoke_m18434_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<AnimatorFrame>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m18435_gshared (Comparison_1_t2780 * __this, AnimatorFrame_t235  ___x, AnimatorFrame_t235  ___y, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m18435(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2780 *, AnimatorFrame_t235 , AnimatorFrame_t235 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m18435_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<AnimatorFrame>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m18436_gshared (Comparison_1_t2780 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m18436(__this, ___result, method) (( int32_t (*) (Comparison_1_t2780 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m18436_gshared)(__this, ___result, method)
