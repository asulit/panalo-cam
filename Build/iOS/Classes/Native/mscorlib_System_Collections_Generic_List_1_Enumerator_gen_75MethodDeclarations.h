﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t1216;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_75.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m28459_gshared (Enumerator_t3460 * __this, List_1_t1216 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m28459(__this, ___l, method) (( void (*) (Enumerator_t3460 *, List_1_t1216 *, const MethodInfo*))Enumerator__ctor_m28459_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m28460_gshared (Enumerator_t3460 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m28460(__this, method) (( Object_t * (*) (Enumerator_t3460 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m28460_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C" void Enumerator_Dispose_m28461_gshared (Enumerator_t3460 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m28461(__this, method) (( void (*) (Enumerator_t3460 *, const MethodInfo*))Enumerator_Dispose_m28461_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m28462_gshared (Enumerator_t3460 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m28462(__this, method) (( void (*) (Enumerator_t3460 *, const MethodInfo*))Enumerator_VerifyState_m28462_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m28463_gshared (Enumerator_t3460 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m28463(__this, method) (( bool (*) (Enumerator_t3460 *, const MethodInfo*))Enumerator_MoveNext_m28463_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C" TargetSearchResult_t1212  Enumerator_get_Current_m28464_gshared (Enumerator_t3460 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m28464(__this, method) (( TargetSearchResult_t1212  (*) (Enumerator_t3460 *, const MethodInfo*))Enumerator_get_Current_m28464_gshared)(__this, method)
