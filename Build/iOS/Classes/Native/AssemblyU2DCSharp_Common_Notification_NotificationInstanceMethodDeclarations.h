﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Notification.NotificationInstance
struct NotificationInstance_t85;
// System.String
struct String_t;
// System.Object
struct Object_t;

// System.Void Common.Notification.NotificationInstance::.ctor()
extern "C" void NotificationInstance__ctor_m284 (NotificationInstance_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationInstance::.cctor()
extern "C" void NotificationInstance__cctor_m285 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationInstance::Init(System.String)
extern "C" void NotificationInstance_Init_m286 (NotificationInstance_t85 * __this, String_t* ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationInstance::Activate()
extern "C" void NotificationInstance_Activate_m287 (NotificationInstance_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationInstance::Deactivate()
extern "C" void NotificationInstance_Deactivate_m288 (NotificationInstance_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Notification.NotificationInstance::IsActive()
extern "C" bool NotificationInstance_IsActive_m289 (NotificationInstance_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Notification.NotificationInstance Common.Notification.NotificationInstance::AddParameter(System.String,System.Object)
extern "C" NotificationInstance_t85 * NotificationInstance_AddParameter_m290 (NotificationInstance_t85 * __this, String_t* ___key, Object_t * ___parameterValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Notification.NotificationInstance::GetId()
extern "C" String_t* NotificationInstance_GetId_m291 (NotificationInstance_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Notification.NotificationInstance::HasParameter(System.String)
extern "C" bool NotificationInstance_HasParameter_m292 (NotificationInstance_t85 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Common.Notification.NotificationInstance::GetParameter(System.String)
extern "C" Object_t * NotificationInstance_GetParameter_m293 (NotificationInstance_t85 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationInstance::MarkAsHandled()
extern "C" void NotificationInstance_MarkAsHandled_m294 (NotificationInstance_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Notification.NotificationInstance::IsHandled()
extern "C" bool NotificationInstance_IsHandled_m295 (NotificationInstance_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Notification.NotificationInstance Common.Notification.NotificationInstance::Borrow(System.String)
extern "C" NotificationInstance_t85 * NotificationInstance_Borrow_m296 (Object_t * __this /* static, unused */, String_t* ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Notification.NotificationInstance::Return(Common.Notification.NotificationInstance)
extern "C" void NotificationInstance_Return_m297 (Object_t * __this /* static, unused */, NotificationInstance_t85 * ___instance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
