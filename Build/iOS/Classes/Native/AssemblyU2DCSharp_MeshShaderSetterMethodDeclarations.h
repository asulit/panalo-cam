﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MeshShaderSetter
struct MeshShaderSetter_t83;
// System.String
struct String_t;

// System.Void MeshShaderSetter::.ctor()
extern "C" void MeshShaderSetter__ctor_m281 (MeshShaderSetter_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MeshShaderSetter::GetShaderToSet()
extern "C" String_t* MeshShaderSetter_GetShaderToSet_m282 (MeshShaderSetter_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
