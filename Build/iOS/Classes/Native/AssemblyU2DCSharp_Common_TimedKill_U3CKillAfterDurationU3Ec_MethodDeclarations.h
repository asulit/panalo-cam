﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.TimedKill/<KillAfterDuration>c__Iterator3
struct U3CKillAfterDurationU3Ec__Iterator3_t122;
// System.Object
struct Object_t;

// System.Void Common.TimedKill/<KillAfterDuration>c__Iterator3::.ctor()
extern "C" void U3CKillAfterDurationU3Ec__Iterator3__ctor_m422 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Common.TimedKill/<KillAfterDuration>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CKillAfterDurationU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m423 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Common.TimedKill/<KillAfterDuration>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CKillAfterDurationU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m424 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.TimedKill/<KillAfterDuration>c__Iterator3::MoveNext()
extern "C" bool U3CKillAfterDurationU3Ec__Iterator3_MoveNext_m425 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.TimedKill/<KillAfterDuration>c__Iterator3::Dispose()
extern "C" void U3CKillAfterDurationU3Ec__Iterator3_Dispose_m426 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.TimedKill/<KillAfterDuration>c__Iterator3::Reset()
extern "C" void U3CKillAfterDurationU3Ec__Iterator3_Reset_m427 (U3CKillAfterDurationU3Ec__Iterator3_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
