﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_18.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15935_gshared (InternalEnumerator_1_t2576 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15935(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2576 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15935_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15936_gshared (InternalEnumerator_1_t2576 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15936(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2576 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15936_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15937_gshared (InternalEnumerator_1_t2576 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15937(__this, method) (( void (*) (InternalEnumerator_1_t2576 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15937_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15938_gshared (InternalEnumerator_1_t2576 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15938(__this, method) (( bool (*) (InternalEnumerator_1_t2576 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15938_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::get_Current()
extern "C" Matrix4x4_t404  InternalEnumerator_1_get_Current_m15939_gshared (InternalEnumerator_1_t2576 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15939(__this, method) (( Matrix4x4_t404  (*) (InternalEnumerator_1_t2576 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15939_gshared)(__this, method)
