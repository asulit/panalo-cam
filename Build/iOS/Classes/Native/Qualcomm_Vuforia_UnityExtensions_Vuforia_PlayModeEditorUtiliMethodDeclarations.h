﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility
struct NullPlayModeEditorUtility_t1127;
// System.String
struct String_t;
// Vuforia.WebCamProfile/ProfileCollection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"

// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::DisplayDialog(System.String,System.String,System.String)
extern "C" void NullPlayModeEditorUtility_DisplayDialog_m5723 (NullPlayModeEditorUtility_t1127 * __this, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamProfile/ProfileCollection Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::LoadAndParseWebcamProfiles(System.String)
extern "C" ProfileCollection_t1227  NullPlayModeEditorUtility_LoadAndParseWebcamProfiles_m5724 (NullPlayModeEditorUtility_t1127 * __this, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::RestartPlayMode()
extern "C" void NullPlayModeEditorUtility_RestartPlayMode_m5725 (NullPlayModeEditorUtility_t1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::.ctor()
extern "C" void NullPlayModeEditorUtility__ctor_m5726 (NullPlayModeEditorUtility_t1127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
