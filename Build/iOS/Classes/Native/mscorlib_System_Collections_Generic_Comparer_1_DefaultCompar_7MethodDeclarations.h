﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>
struct DefaultComparer_t3068;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>::.ctor()
extern "C" void DefaultComparer__ctor_m22698_gshared (DefaultComparer_t3068 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22698(__this, method) (( void (*) (DefaultComparer_t3068 *, const MethodInfo*))DefaultComparer__ctor_m22698_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector4>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m22699_gshared (DefaultComparer_t3068 * __this, Vector4_t680  ___x, Vector4_t680  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m22699(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3068 *, Vector4_t680 , Vector4_t680 , const MethodInfo*))DefaultComparer_Compare_m22699_gshared)(__this, ___x, ___y, method)
