﻿#pragma once
#include <stdint.h>
// Vuforia.Prop
struct Prop_t357;
// UnityEngine.BoxCollider
struct BoxCollider_t126;
// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"
// Vuforia.PropAbstractBehaviour
struct  PropAbstractBehaviour_t300  : public SmartTerrainTrackableBehaviour_t1084
{
	// Vuforia.Prop Vuforia.PropAbstractBehaviour::mProp
	Object_t * ___mProp_13;
	// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::mBoxColliderToUpdate
	BoxCollider_t126 * ___mBoxColliderToUpdate_14;
};
