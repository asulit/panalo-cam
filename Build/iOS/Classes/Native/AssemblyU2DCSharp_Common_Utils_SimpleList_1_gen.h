﻿#pragma once
#include <stdint.h>
// SwarmItem[]
struct SwarmItemU5BU5D_t2597;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Utils.SimpleList`1<SwarmItem>
struct  SimpleList_1_t112  : public Object_t
{
	// T[] Common.Utils.SimpleList`1<SwarmItem>::buffer
	SwarmItemU5BU5D_t2597* ___buffer_1;
	// System.Int32 Common.Utils.SimpleList`1<SwarmItem>::size
	int32_t ___size_2;
};
