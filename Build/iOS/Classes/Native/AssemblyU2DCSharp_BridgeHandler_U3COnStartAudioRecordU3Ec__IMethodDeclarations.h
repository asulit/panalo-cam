﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// BridgeHandler/<OnStartAudioRecord>c__Iterator5
struct U3COnStartAudioRecordU3Ec__Iterator5_t184;
// System.Object
struct Object_t;

// System.Void BridgeHandler/<OnStartAudioRecord>c__Iterator5::.ctor()
extern "C" void U3COnStartAudioRecordU3Ec__Iterator5__ctor_m632 (U3COnStartAudioRecordU3Ec__Iterator5_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BridgeHandler/<OnStartAudioRecord>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3COnStartAudioRecordU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m633 (U3COnStartAudioRecordU3Ec__Iterator5_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BridgeHandler/<OnStartAudioRecord>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3COnStartAudioRecordU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m634 (U3COnStartAudioRecordU3Ec__Iterator5_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BridgeHandler/<OnStartAudioRecord>c__Iterator5::MoveNext()
extern "C" bool U3COnStartAudioRecordU3Ec__Iterator5_MoveNext_m635 (U3COnStartAudioRecordU3Ec__Iterator5_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler/<OnStartAudioRecord>c__Iterator5::Dispose()
extern "C" void U3COnStartAudioRecordU3Ec__Iterator5_Dispose_m636 (U3COnStartAudioRecordU3Ec__Iterator5_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler/<OnStartAudioRecord>c__Iterator5::Reset()
extern "C" void U3COnStartAudioRecordU3Ec__Iterator5_Reset_m637 (U3COnStartAudioRecordU3Ec__Iterator5_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
