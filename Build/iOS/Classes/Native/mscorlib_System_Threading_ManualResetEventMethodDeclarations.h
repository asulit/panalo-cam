﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Threading.ManualResetEvent
struct ManualResetEvent_t160;

// System.Void System.Threading.ManualResetEvent::.ctor(System.Boolean)
extern "C" void ManualResetEvent__ctor_m1591 (ManualResetEvent_t160 * __this, bool ___initialState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
