﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8
struct U3CCheckRegisterMessageU3Ec__Iterator8_t220;
// System.Object
struct Object_t;

// System.Void LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::.ctor()
extern "C" void U3CCheckRegisterMessageU3Ec__Iterator8__ctor_m763 (U3CCheckRegisterMessageU3Ec__Iterator8_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CCheckRegisterMessageU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m764 (U3CCheckRegisterMessageU3Ec__Iterator8_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCheckRegisterMessageU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m765 (U3CCheckRegisterMessageU3Ec__Iterator8_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::MoveNext()
extern "C" bool U3CCheckRegisterMessageU3Ec__Iterator8_MoveNext_m766 (U3CCheckRegisterMessageU3Ec__Iterator8_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::Dispose()
extern "C" void U3CCheckRegisterMessageU3Ec__Iterator8_Dispose_m767 (U3CCheckRegisterMessageU3Ec__Iterator8_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::Reset()
extern "C" void U3CCheckRegisterMessageU3Ec__Iterator8_Reset_m768 (U3CCheckRegisterMessageU3Ec__Iterator8_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8::<>m__1C()
extern "C" void U3CCheckRegisterMessageU3Ec__Iterator8_U3CU3Em__1C_m769 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
