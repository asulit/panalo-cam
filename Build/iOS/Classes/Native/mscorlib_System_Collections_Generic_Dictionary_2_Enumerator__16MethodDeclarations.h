﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
struct Dictionary_2_t2688;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17454_gshared (Enumerator_t2695 * __this, Dictionary_2_t2688 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m17454(__this, ___dictionary, method) (( void (*) (Enumerator_t2695 *, Dictionary_2_t2688 *, const MethodInfo*))Enumerator__ctor_m17454_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17455_gshared (Enumerator_t2695 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17455(__this, method) (( Object_t * (*) (Enumerator_t2695 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17455_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17456_gshared (Enumerator_t2695 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17456(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t2695 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17456_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17457_gshared (Enumerator_t2695 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17457(__this, method) (( Object_t * (*) (Enumerator_t2695 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17457_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17458_gshared (Enumerator_t2695 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17458(__this, method) (( Object_t * (*) (Enumerator_t2695 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17458_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17459_gshared (Enumerator_t2695 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17459(__this, method) (( bool (*) (Enumerator_t2695 *, const MethodInfo*))Enumerator_MoveNext_m17459_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2690  Enumerator_get_Current_m17460_gshared (Enumerator_t2695 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17460(__this, method) (( KeyValuePair_2_t2690  (*) (Enumerator_t2695 *, const MethodInfo*))Enumerator_get_Current_m17460_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m17461_gshared (Enumerator_t2695 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m17461(__this, method) (( int32_t (*) (Enumerator_t2695 *, const MethodInfo*))Enumerator_get_CurrentKey_m17461_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m17462_gshared (Enumerator_t2695 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m17462(__this, method) (( Object_t * (*) (Enumerator_t2695 *, const MethodInfo*))Enumerator_get_CurrentValue_m17462_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m17463_gshared (Enumerator_t2695 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17463(__this, method) (( void (*) (Enumerator_t2695 *, const MethodInfo*))Enumerator_VerifyState_m17463_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m17464_gshared (Enumerator_t2695 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m17464(__this, method) (( void (*) (Enumerator_t2695 *, const MethodInfo*))Enumerator_VerifyCurrent_m17464_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m17465_gshared (Enumerator_t2695 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17465(__this, method) (( void (*) (Enumerator_t2695 *, const MethodInfo*))Enumerator_Dispose_m17465_gshared)(__this, method)
