﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.EventArgs
#include "mscorlib_System_EventArgs.h"
// System.ResolveEventArgs
struct  ResolveEventArgs_t2366  : public EventArgs_t1452
{
	// System.String System.ResolveEventArgs::m_Name
	String_t* ___m_Name_1;
};
