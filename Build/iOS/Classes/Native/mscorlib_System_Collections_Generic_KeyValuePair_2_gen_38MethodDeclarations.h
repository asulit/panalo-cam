﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_38.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m26385_gshared (KeyValuePair_2_t3337 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m26385(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3337 *, Object_t *, uint16_t, const MethodInfo*))KeyValuePair_2__ctor_m26385_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m26386_gshared (KeyValuePair_2_t3337 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m26386(__this, method) (( Object_t * (*) (KeyValuePair_2_t3337 *, const MethodInfo*))KeyValuePair_2_get_Key_m26386_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m26387_gshared (KeyValuePair_2_t3337 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m26387(__this, ___value, method) (( void (*) (KeyValuePair_2_t3337 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m26387_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
extern "C" uint16_t KeyValuePair_2_get_Value_m26388_gshared (KeyValuePair_2_t3337 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m26388(__this, method) (( uint16_t (*) (KeyValuePair_2_t3337 *, const MethodInfo*))KeyValuePair_2_get_Value_m26388_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m26389_gshared (KeyValuePair_2_t3337 * __this, uint16_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m26389(__this, ___value, method) (( void (*) (KeyValuePair_2_t3337 *, uint16_t, const MethodInfo*))KeyValuePair_2_set_Value_m26389_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m26390_gshared (KeyValuePair_2_t3337 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m26390(__this, method) (( String_t* (*) (KeyValuePair_2_t3337 *, const MethodInfo*))KeyValuePair_2_ToString_m26390_gshared)(__this, method)
