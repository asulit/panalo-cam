﻿#pragma once
#include <stdint.h>
// Mono.Math.BigInteger
struct BigInteger_t1428;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
// Mono.Math.Prime.PrimalityTest
struct  PrimalityTest_t1548  : public MulticastDelegate_t28
{
};
