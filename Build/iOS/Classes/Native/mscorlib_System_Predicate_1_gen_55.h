﻿#pragma once
#include <stdint.h>
// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t284;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct  Predicate_1_t3243  : public MulticastDelegate_t28
{
};
