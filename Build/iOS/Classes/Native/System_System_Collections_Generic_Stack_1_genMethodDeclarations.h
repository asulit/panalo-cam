﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_2MethodDeclarations.h"
#define Stack_1__ctor_m1489(__this, method) (( void (*) (Stack_1_t87 *, const MethodInfo*))Stack_1__ctor_m15950_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m15951(__this, method) (( bool (*) (Stack_1_t87 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m15952_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m15953(__this, method) (( Object_t * (*) (Stack_1_t87 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m15954_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m15955(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t87 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m15956_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15957(__this, method) (( Object_t* (*) (Stack_1_t87 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15958_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m15959(__this, method) (( Object_t * (*) (Stack_1_t87 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m15960_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::Peek()
#define Stack_1_Peek_m15961(__this, method) (( NotificationInstance_t85 * (*) (Stack_1_t87 *, const MethodInfo*))Stack_1_Peek_m15962_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::Pop()
#define Stack_1_Pop_m1490(__this, method) (( NotificationInstance_t85 * (*) (Stack_1_t87 *, const MethodInfo*))Stack_1_Pop_m15963_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::Push(T)
#define Stack_1_Push_m1491(__this, ___t, method) (( void (*) (Stack_1_t87 *, NotificationInstance_t85 *, const MethodInfo*))Stack_1_Push_m15964_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::get_Count()
#define Stack_1_get_Count_m15965(__this, method) (( int32_t (*) (Stack_1_t87 *, const MethodInfo*))Stack_1_get_Count_m15966_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>::GetEnumerator()
#define Stack_1_GetEnumerator_m15967(__this, method) (( Enumerator_t2582  (*) (Stack_1_t87 *, const MethodInfo*))Stack_1_GetEnumerator_m15968_gshared)(__this, method)
