﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioRecognizer
struct AudioRecognizer_t229;

// System.Void AudioRecognizer::.ctor()
extern "C" void AudioRecognizer__ctor_m816 (AudioRecognizer_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecognizer::Share()
extern "C" void AudioRecognizer_Share_m817 (AudioRecognizer_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecognizer::SendSMS()
extern "C" void AudioRecognizer_SendSMS_m818 (AudioRecognizer_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecognizer::StartRecord()
extern "C" void AudioRecognizer_StartRecord_m819 (AudioRecognizer_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecognizer::StopRecord()
extern "C" void AudioRecognizer_StopRecord_m820 (AudioRecognizer_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecognizer::CancelRecord()
extern "C" void AudioRecognizer_CancelRecord_m821 (AudioRecognizer_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioRecognizer::CheckData()
extern "C" void AudioRecognizer_CheckData_m822 (AudioRecognizer_t229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
