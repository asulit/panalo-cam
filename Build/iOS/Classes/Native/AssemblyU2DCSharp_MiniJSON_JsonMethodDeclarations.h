﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Object MiniJSON.Json::Deserialize(System.String)
extern "C" Object_t * Json_Deserialize_m247 (Object_t * __this /* static, unused */, String_t* ___json, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MiniJSON.Json::Serialize(System.Object)
extern "C" String_t* Json_Serialize_m248 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
