﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Utils.PopupValueSet
struct PopupValueSet_t136;
// System.String[]
struct StringU5BU5D_t137;
// System.String
struct String_t;

// System.Void Common.Utils.PopupValueSet::.ctor(System.String[])
extern "C" void PopupValueSet__ctor_m450 (PopupValueSet_t136 * __this, StringU5BU5D_t137* ___valueList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Common.Utils.PopupValueSet::get_ValueList()
extern "C" StringU5BU5D_t137* PopupValueSet_get_ValueList_m451 (PopupValueSet_t136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Common.Utils.PopupValueSet::ResolveIndex(System.String)
extern "C" int32_t PopupValueSet_ResolveIndex_m452 (PopupValueSet_t136 * __this, String_t* ___textValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Utils.PopupValueSet::GetValue(System.Int32)
extern "C" String_t* PopupValueSet_GetValue_m453 (PopupValueSet_t136 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
