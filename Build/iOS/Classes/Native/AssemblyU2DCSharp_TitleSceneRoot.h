﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.InputField
struct InputField_t226;
// UnityEngine.UI.Image
struct Image_t213;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,System.Boolean>
struct Dictionary_2_t227;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,System.String>
struct Dictionary_2_t228;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>
struct Dictionary_2_t231;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// TitleSceneRoot
struct  TitleSceneRoot_t230  : public MonoBehaviour_t5
{
	// UnityEngine.UI.InputField TitleSceneRoot::usernameField
	InputField_t226 * ___usernameField_2;
	// UnityEngine.UI.InputField TitleSceneRoot::passwordField
	InputField_t226 * ___passwordField_3;
	// UnityEngine.UI.Image TitleSceneRoot::usernameLabelImage
	Image_t213 * ___usernameLabelImage_4;
	// UnityEngine.UI.Image TitleSceneRoot::passwordLabelImage
	Image_t213 * ___passwordLabelImage_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,System.Boolean> TitleSceneRoot::prevFocusValue
	Dictionary_2_t227 * ___prevFocusValue_6;
	// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,System.String> TitleSceneRoot::defaultValue
	Dictionary_2_t228 * ___defaultValue_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image> TitleSceneRoot::defaultLabel
	Dictionary_2_t231 * ___defaultLabel_8;
};
