﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Boolean QuaternionUtils::Equals(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" bool QuaternionUtils_Equals_m360 (Object_t * __this /* static, unused */, Quaternion_t41  ___a, Quaternion_t41  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
