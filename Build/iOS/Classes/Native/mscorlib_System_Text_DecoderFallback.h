﻿#pragma once
#include <stdint.h>
// System.Text.DecoderFallback
struct DecoderFallback_t2264;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.DecoderFallback
struct  DecoderFallback_t2264  : public Object_t
{
};
struct DecoderFallback_t2264_StaticFields{
	// System.Text.DecoderFallback System.Text.DecoderFallback::exception_fallback
	DecoderFallback_t2264 * ___exception_fallback_0;
	// System.Text.DecoderFallback System.Text.DecoderFallback::replacement_fallback
	DecoderFallback_t2264 * ___replacement_fallback_1;
	// System.Text.DecoderFallback System.Text.DecoderFallback::standard_safe_fallback
	DecoderFallback_t2264 * ___standard_safe_fallback_2;
};
