﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t311;

// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern "C" void TurnOffWordBehaviour__ctor_m1264 (TurnOffWordBehaviour_t311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern "C" void TurnOffWordBehaviour_Awake_m1265 (TurnOffWordBehaviour_t311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
