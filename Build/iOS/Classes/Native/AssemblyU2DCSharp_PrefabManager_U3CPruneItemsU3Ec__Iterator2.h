﻿#pragma once
#include <stdint.h>
// PrefabManager/PruneData[]
struct PruneDataU5BU5D_t98;
// PrefabManager/PruneData
struct PruneData_t93;
// System.Object
struct Object_t;
// PrefabManager
struct PrefabManager_t96;
// System.Object
#include "mscorlib_System_Object.h"
// PrefabManager/<PruneItems>c__Iterator2
struct  U3CPruneItemsU3Ec__Iterator2_t97  : public Object_t
{
	// PrefabManager/PruneData[] PrefabManager/<PruneItems>c__Iterator2::<$s_40>__0
	PruneDataU5BU5D_t98* ___U3CU24s_40U3E__0_0;
	// System.Int32 PrefabManager/<PruneItems>c__Iterator2::<$s_41>__1
	int32_t ___U3CU24s_41U3E__1_1;
	// PrefabManager/PruneData PrefabManager/<PruneItems>c__Iterator2::<data>__2
	PruneData_t93 * ___U3CdataU3E__2_2;
	// System.Int32 PrefabManager/<PruneItems>c__Iterator2::<itemPrefabIndex>__3
	int32_t ___U3CitemPrefabIndexU3E__3_3;
	// System.Int32 PrefabManager/<PruneItems>c__Iterator2::$PC
	int32_t ___U24PC_4;
	// System.Object PrefabManager/<PruneItems>c__Iterator2::$current
	Object_t * ___U24current_5;
	// PrefabManager PrefabManager/<PruneItems>c__Iterator2::<>f__this
	PrefabManager_t96 * ___U3CU3Ef__this_6;
};
