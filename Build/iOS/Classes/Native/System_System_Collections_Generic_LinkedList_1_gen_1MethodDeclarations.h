﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t1152;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3626;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t1312;
// System.Int32[]
struct Int32U5BU5D_t401;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_2.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor()
extern "C" void LinkedList_1__ctor_m7281_gshared (LinkedList_1_t1152 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m7281(__this, method) (( void (*) (LinkedList_1_t1152 *, const MethodInfo*))LinkedList_1__ctor_m7281_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1__ctor_m26044_gshared (LinkedList_1_t1152 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define LinkedList_1__ctor_m26044(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t1152 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))LinkedList_1__ctor_m26044_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26045_gshared (LinkedList_1_t1152 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26045(__this, ___value, method) (( void (*) (LinkedList_1_t1152 *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26045_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m26046_gshared (LinkedList_1_t1152 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m26046(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t1152 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m26046_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26047_gshared (LinkedList_1_t1152 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26047(__this, method) (( Object_t* (*) (LinkedList_1_t1152 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26047_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m26048_gshared (LinkedList_1_t1152 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m26048(__this, method) (( Object_t * (*) (LinkedList_1_t1152 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m26048_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26049_gshared (LinkedList_1_t1152 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26049(__this, method) (( bool (*) (LinkedList_1_t1152 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26049_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m26050_gshared (LinkedList_1_t1152 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m26050(__this, method) (( bool (*) (LinkedList_1_t1152 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m26050_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m26051_gshared (LinkedList_1_t1152 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m26051(__this, method) (( Object_t * (*) (LinkedList_1_t1152 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m26051_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_VerifyReferencedNode_m26052_gshared (LinkedList_1_t1152 * __this, LinkedListNode_1_t1312 * ___node, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m26052(__this, ___node, method) (( void (*) (LinkedList_1_t1152 *, LinkedListNode_1_t1312 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m26052_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::AddLast(T)
extern "C" LinkedListNode_1_t1312 * LinkedList_1_AddLast_m7282_gshared (LinkedList_1_t1152 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_AddLast_m7282(__this, ___value, method) (( LinkedListNode_1_t1312 * (*) (LinkedList_1_t1152 *, int32_t, const MethodInfo*))LinkedList_1_AddLast_m7282_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Clear()
extern "C" void LinkedList_1_Clear_m26053_gshared (LinkedList_1_t1152 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m26053(__this, method) (( void (*) (LinkedList_1_t1152 *, const MethodInfo*))LinkedList_1_Clear_m26053_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(T)
extern "C" bool LinkedList_1_Contains_m26054_gshared (LinkedList_1_t1152 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Contains_m26054(__this, ___value, method) (( bool (*) (LinkedList_1_t1152 *, int32_t, const MethodInfo*))LinkedList_1_Contains_m26054_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void LinkedList_1_CopyTo_m26055_gshared (LinkedList_1_t1152 * __this, Int32U5BU5D_t401* ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_CopyTo_m26055(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t1152 *, Int32U5BU5D_t401*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m26055_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::Find(T)
extern "C" LinkedListNode_1_t1312 * LinkedList_1_Find_m26056_gshared (LinkedList_1_t1152 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Find_m26056(__this, ___value, method) (( LinkedListNode_1_t1312 * (*) (LinkedList_1_t1152 *, int32_t, const MethodInfo*))LinkedList_1_Find_m26056_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t3318  LinkedList_1_GetEnumerator_m26057_gshared (LinkedList_1_t1152 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m26057(__this, method) (( Enumerator_t3318  (*) (LinkedList_1_t1152 *, const MethodInfo*))LinkedList_1_GetEnumerator_m26057_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1_GetObjectData_m26058_gshared (LinkedList_1_t1152 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m26058(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t1152 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))LinkedList_1_GetObjectData_m26058_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::OnDeserialization(System.Object)
extern "C" void LinkedList_1_OnDeserialization_m26059_gshared (LinkedList_1_t1152 * __this, Object_t * ___sender, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m26059(__this, ___sender, method) (( void (*) (LinkedList_1_t1152 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m26059_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(T)
extern "C" bool LinkedList_1_Remove_m26060_gshared (LinkedList_1_t1152 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Remove_m26060(__this, ___value, method) (( bool (*) (LinkedList_1_t1152 *, int32_t, const MethodInfo*))LinkedList_1_Remove_m26060_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m7429_gshared (LinkedList_1_t1152 * __this, LinkedListNode_1_t1312 * ___node, const MethodInfo* method);
#define LinkedList_1_Remove_m7429(__this, ___node, method) (( void (*) (LinkedList_1_t1152 *, LinkedListNode_1_t1312 *, const MethodInfo*))LinkedList_1_Remove_m7429_gshared)(__this, ___node, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::RemoveLast()
extern "C" void LinkedList_1_RemoveLast_m26061_gshared (LinkedList_1_t1152 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveLast_m26061(__this, method) (( void (*) (LinkedList_1_t1152 *, const MethodInfo*))LinkedList_1_RemoveLast_m26061_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m26062_gshared (LinkedList_1_t1152 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m26062(__this, method) (( int32_t (*) (LinkedList_1_t1152 *, const MethodInfo*))LinkedList_1_get_Count_m26062_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::get_First()
extern "C" LinkedListNode_1_t1312 * LinkedList_1_get_First_m7290_gshared (LinkedList_1_t1152 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m7290(__this, method) (( LinkedListNode_1_t1312 * (*) (LinkedList_1_t1152 *, const MethodInfo*))LinkedList_1_get_First_m7290_gshared)(__this, method)
