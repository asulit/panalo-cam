﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Xml.SimpleXmlNode/NodeProcessor
struct NodeProcessor_t141;
// System.Object
struct Object_t;
// Common.Xml.SimpleXmlNode
struct SimpleXmlNode_t142;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Common.Xml.SimpleXmlNode/NodeProcessor::.ctor(System.Object,System.IntPtr)
extern "C" void NodeProcessor__ctor_m465 (NodeProcessor_t141 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Xml.SimpleXmlNode/NodeProcessor::Invoke(Common.Xml.SimpleXmlNode)
extern "C" void NodeProcessor_Invoke_m466 (NodeProcessor_t141 * __this, SimpleXmlNode_t142 * ___node, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_NodeProcessor_t141(Il2CppObject* delegate, SimpleXmlNode_t142 * ___node);
// System.IAsyncResult Common.Xml.SimpleXmlNode/NodeProcessor::BeginInvoke(Common.Xml.SimpleXmlNode,System.AsyncCallback,System.Object)
extern "C" Object_t * NodeProcessor_BeginInvoke_m467 (NodeProcessor_t141 * __this, SimpleXmlNode_t142 * ___node, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Xml.SimpleXmlNode/NodeProcessor::EndInvoke(System.IAsyncResult)
extern "C" void NodeProcessor_EndInvoke_m468 (NodeProcessor_t141 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
