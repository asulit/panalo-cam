﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.IBehaviourComponentFactory
struct IBehaviourComponentFactory_t1095;
// Vuforia.BehaviourComponentFactory
struct BehaviourComponentFactory_t1094;

// Vuforia.IBehaviourComponentFactory Vuforia.BehaviourComponentFactory::get_Instance()
extern "C" Object_t * BehaviourComponentFactory_get_Instance_m5572 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory::set_Instance(Vuforia.IBehaviourComponentFactory)
extern "C" void BehaviourComponentFactory_set_Instance_m1846 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory::.ctor()
extern "C" void BehaviourComponentFactory__ctor_m5573 (BehaviourComponentFactory_t1094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
