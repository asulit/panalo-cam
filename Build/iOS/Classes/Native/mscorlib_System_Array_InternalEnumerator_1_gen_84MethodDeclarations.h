﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_84.h"
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_33.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25094_gshared (InternalEnumerator_1_t3258 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m25094(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3258 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m25094_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25095_gshared (InternalEnumerator_1_t3258 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25095(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3258 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25095_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25096_gshared (InternalEnumerator_1_t3258 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25096(__this, method) (( void (*) (InternalEnumerator_1_t3258 *, const MethodInfo*))InternalEnumerator_1_Dispose_m25096_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25097_gshared (InternalEnumerator_1_t3258 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25097(__this, method) (( bool (*) (InternalEnumerator_1_t3258 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m25097_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t3257  InternalEnumerator_1_get_Current_m25098_gshared (InternalEnumerator_1_t3258 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25098(__this, method) (( KeyValuePair_2_t3257  (*) (InternalEnumerator_1_t3258 *, const MethodInfo*))InternalEnumerator_1_get_Current_m25098_gshared)(__this, method)
