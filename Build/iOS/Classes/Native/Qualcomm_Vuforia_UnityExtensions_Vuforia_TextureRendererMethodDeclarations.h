﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TextureRenderer
struct TextureRenderer_t1219;
// UnityEngine.Texture
struct Texture_t103;
// UnityEngine.RenderTexture
struct RenderTexture_t811;
// Vuforia.VuforiaRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec.h"

// System.Int32 Vuforia.TextureRenderer::get_Width()
extern "C" int32_t TextureRenderer_get_Width_m6859 (TextureRenderer_t1219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.TextureRenderer::get_Height()
extern "C" int32_t TextureRenderer_get_Height_m6860 (TextureRenderer_t1219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextureRenderer::.ctor(UnityEngine.Texture,System.Int32,Vuforia.VuforiaRenderer/Vec2I)
extern "C" void TextureRenderer__ctor_m6861 (TextureRenderer_t1219 * __this, Texture_t103 * ___textureToRender, int32_t ___renderTextureLayer, Vec2I_t1157  ___requestedTextureSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture Vuforia.TextureRenderer::Render()
extern "C" RenderTexture_t811 * TextureRenderer_Render_m6862 (TextureRenderer_t1219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextureRenderer::Destroy()
extern "C" void TextureRenderer_Destroy_m6863 (TextureRenderer_t1219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
