﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t268;

// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern "C" void BackgroundPlaneBehaviour__ctor_m1177 (BackgroundPlaneBehaviour_t268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
