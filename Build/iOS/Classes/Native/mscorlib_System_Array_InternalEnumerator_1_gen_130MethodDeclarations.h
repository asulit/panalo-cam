﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_130.h"
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29741_gshared (InternalEnumerator_1_t3570 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29741(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3570 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29741_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29742_gshared (InternalEnumerator_1_t3570 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29742(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3570 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29742_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29743_gshared (InternalEnumerator_1_t3570 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29743(__this, method) (( void (*) (InternalEnumerator_1_t3570 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29743_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29744_gshared (InternalEnumerator_1_t3570 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29744(__this, method) (( bool (*) (InternalEnumerator_1_t3570 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29744_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern "C" Slot_t1888  InternalEnumerator_1_get_Current_m29745_gshared (InternalEnumerator_1_t3570 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29745(__this, method) (( Slot_t1888  (*) (InternalEnumerator_1_t3570 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29745_gshared)(__this, method)
