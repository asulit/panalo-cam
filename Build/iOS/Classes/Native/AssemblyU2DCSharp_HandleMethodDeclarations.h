﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Handle
struct Handle_t221;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Handle::.ctor(System.Object,System.IntPtr)
extern "C" void Handle__ctor_m1301 (Handle_t221 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handle::Invoke()
extern "C" void Handle_Invoke_m1302 (Handle_t221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_Handle_t221(Il2CppObject* delegate);
// System.IAsyncResult Handle::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * Handle_BeginInvoke_m1303 (Handle_t221 * __this, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handle::EndInvoke(System.IAsyncResult)
extern "C" void Handle_EndInvoke_m1304 (Handle_t221 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
