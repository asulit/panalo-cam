﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.VuforiaManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_.h"
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
// Vuforia.VuforiaManagerImpl/TrackableResultData
struct  TrackableResultData_t1134 
{
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.VuforiaManagerImpl/TrackableResultData::pose
	PoseData_t1133  ___pose_0;
	// Vuforia.TrackableBehaviour/Status Vuforia.VuforiaManagerImpl/TrackableResultData::status
	int32_t ___status_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/TrackableResultData::id
	int32_t ___id_2;
};
// Native definition for marshalling of: Vuforia.VuforiaManagerImpl/TrackableResultData
// Vuforia.VuforiaManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_.h"
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
#pragma pack(push, tp, 1)
struct TrackableResultData_t1134_marshaled
{
	PoseData_t1133  ___pose_0;
	int32_t ___status_1;
	int32_t ___id_2;
};
#pragma pack(pop, tp)
