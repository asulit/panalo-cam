﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2265;

// System.Void System.Text.DecoderFallbackBuffer::.ctor()
extern "C" void DecoderFallbackBuffer__ctor_m13416 (DecoderFallbackBuffer_t2265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderFallbackBuffer::Reset()
extern "C" void DecoderFallbackBuffer_Reset_m13417 (DecoderFallbackBuffer_t2265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
