﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t86;
// System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance>
struct Stack_1_t87;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Notification.NotificationInstance
struct  NotificationInstance_t85  : public Object_t
{
	// System.String Common.Notification.NotificationInstance::id
	String_t* ___id_0;
	// System.Boolean Common.Notification.NotificationInstance::active
	bool ___active_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Common.Notification.NotificationInstance::parameterMap
	Dictionary_2_t86 * ___parameterMap_2;
	// System.Boolean Common.Notification.NotificationInstance::handled
	bool ___handled_3;
};
struct NotificationInstance_t85_StaticFields{
	// System.Collections.Generic.Stack`1<Common.Notification.NotificationInstance> Common.Notification.NotificationInstance::instanceStack
	Stack_1_t87 * ___instanceStack_4;
};
