﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.UILineInfo>
struct Comparison_1_t3149;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Comparison`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m23631_gshared (Comparison_1_t3149 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m23631(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3149 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m23631_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.UILineInfo>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m23632_gshared (Comparison_1_t3149 * __this, UILineInfo_t752  ___x, UILineInfo_t752  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m23632(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3149 *, UILineInfo_t752 , UILineInfo_t752 , const MethodInfo*))Comparison_1_Invoke_m23632_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.UILineInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m23633_gshared (Comparison_1_t3149 * __this, UILineInfo_t752  ___x, UILineInfo_t752  ___y, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m23633(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3149 *, UILineInfo_t752 , UILineInfo_t752 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m23633_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m23634_gshared (Comparison_1_t3149 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m23634(__this, ___result, method) (( int32_t (*) (Comparison_1_t3149 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m23634_gshared)(__this, ___result, method)
