﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<EScreens,System.String>
struct Dictionary_2_t191;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>
struct  KeyCollection_t2682  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::dictionary
	Dictionary_2_t191 * ___dictionary_0;
};
