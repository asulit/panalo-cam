﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void Escape_t1815_marshal(const Escape_t1815& unmarshaled, Escape_t1815_marshaled& marshaled);
extern "C" void Escape_t1815_marshal_back(const Escape_t1815_marshaled& marshaled, Escape_t1815& unmarshaled);
extern "C" void Escape_t1815_marshal_cleanup(Escape_t1815_marshaled& marshaled);
