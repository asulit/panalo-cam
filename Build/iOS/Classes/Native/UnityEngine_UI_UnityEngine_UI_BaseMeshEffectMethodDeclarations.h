﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.BaseMeshEffect
struct BaseMeshEffect_t682;
// UnityEngine.UI.Graphic
struct Graphic_t572;

// System.Void UnityEngine.UI.BaseMeshEffect::.ctor()
extern "C" void BaseMeshEffect__ctor_m3250 (BaseMeshEffect_t682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::get_graphic()
extern "C" Graphic_t572 * BaseMeshEffect_get_graphic_m3251 (BaseMeshEffect_t682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseMeshEffect::OnEnable()
extern "C" void BaseMeshEffect_OnEnable_m3252 (BaseMeshEffect_t682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseMeshEffect::OnDisable()
extern "C" void BaseMeshEffect_OnDisable_m3253 (BaseMeshEffect_t682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseMeshEffect::OnDidApplyAnimationProperties()
extern "C" void BaseMeshEffect_OnDidApplyAnimationProperties_m3254 (BaseMeshEffect_t682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
