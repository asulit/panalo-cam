﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Raffle
struct Raffle_t202;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// UnityEngine.WWW
struct WWW_t199;
// RequestHandler
struct RequestHandler_t200;

// System.Void Raffle::.ctor()
extern "C" void Raffle__ctor_m712 (Raffle_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle::OnEnable()
extern "C" void Raffle_OnEnable_m713 (Raffle_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle::OnDisable()
extern "C" void Raffle_OnDisable_m714 (Raffle_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle::RegisterUser(System.String,System.String,System.String,System.String)
extern "C" void Raffle_RegisterUser_m715 (Raffle_t202 * __this, String_t* ___firstName, String_t* ___lastName, String_t* ___mobileNumber, String_t* ___company, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle::GetPlayerInformation()
extern "C" void Raffle_GetPlayerInformation_m716 (Raffle_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle::RegisterPlayerToPromo()
extern "C" void Raffle_RegisterPlayerToPromo_m717 (Raffle_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle::GetPromoWinner()
extern "C" void Raffle_GetPromoWinner_m718 (Raffle_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle::GetPromoWinner(System.Int32)
extern "C" void Raffle_GetPromoWinner_m719 (Raffle_t202 * __this, int32_t ___raffleId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Raffle::ProcessRequest(UnityEngine.WWW,RequestHandler)
extern "C" Object_t * Raffle_ProcessRequest_m720 (Raffle_t202 * __this, WWW_t199 * ___request, RequestHandler_t200 * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raffle::ResolveRaffleData()
extern "C" void Raffle_ResolveRaffleData_m721 (Raffle_t202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
