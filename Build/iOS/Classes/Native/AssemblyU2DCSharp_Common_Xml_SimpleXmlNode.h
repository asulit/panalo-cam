﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Xml.SimpleXmlNode
struct SimpleXmlNode_t142;
// System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>
struct List_1_t143;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t144;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Xml.SimpleXmlNode
struct  SimpleXmlNode_t142  : public Object_t
{
	// System.String Common.Xml.SimpleXmlNode::TagName
	String_t* ___TagName_2;
	// Common.Xml.SimpleXmlNode Common.Xml.SimpleXmlNode::ParentNode
	SimpleXmlNode_t142 * ___ParentNode_3;
	// System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode> Common.Xml.SimpleXmlNode::Children
	List_1_t143 * ___Children_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Common.Xml.SimpleXmlNode::Attributes
	Dictionary_2_t144 * ___Attributes_5;
};
