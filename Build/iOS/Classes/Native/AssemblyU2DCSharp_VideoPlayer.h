﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// PanaloVideoPlayer
struct PanaloVideoPlayer_t242;
// UnityEngine.AudioSource
struct AudioSource_t44;
// UnityEngine.GUITexture
struct GUITexture_t243;
// System.Collections.Generic.List`1<System.String>
struct List_1_t244;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// VideoPlayer
struct  VideoPlayer_t241  : public MonoBehaviour_t5
{
	// System.String VideoPlayer::MOVIE_TEXTURE_PATH_GLOBAL
	String_t* ___MOVIE_TEXTURE_PATH_GLOBAL_2;
	// System.String VideoPlayer::MOVIE_TEXTURE_ENG_1
	String_t* ___MOVIE_TEXTURE_ENG_1_3;
	// System.String VideoPlayer::MOVIE_TEXTURE_ENG_2
	String_t* ___MOVIE_TEXTURE_ENG_2_4;
	// System.String VideoPlayer::MOVIE_TEXTURE_TAG_1
	String_t* ___MOVIE_TEXTURE_TAG_1_5;
	// System.String VideoPlayer::MOVIE_TEXTURE_TAG_2
	String_t* ___MOVIE_TEXTURE_TAG_2_6;
	// UnityEngine.Vector3 VideoPlayer::POSITION
	Vector3_t36  ___POSITION_7;
	// System.Boolean VideoPlayer::ready
	bool ___ready_8;
	// PanaloVideoPlayer VideoPlayer::player
	PanaloVideoPlayer_t242 * ___player_9;
	// UnityEngine.AudioSource VideoPlayer::audioSource
	AudioSource_t44 * ___audioSource_10;
	// UnityEngine.GUITexture VideoPlayer::guiTexture
	GUITexture_t243 * ___guiTexture_11;
	// System.Collections.Generic.List`1<System.String> VideoPlayer::movies
	List_1_t244 * ___movies_12;
};
