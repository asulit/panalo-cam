﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
struct  KeyValuePair_2_t352 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>::value
	int32_t ___value_1;
};
