﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.String,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_16MethodDeclarations.h"
#define Transform_1__ctor_m17276(__this, ___object, ___method, method) (( void (*) (Transform_1_t2663 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m17254_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.String,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m17277(__this, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Transform_1_t2663 *, int32_t, String_t*, const MethodInfo*))Transform_1_Invoke_m17255_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.String,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m17278(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2663 *, int32_t, String_t*, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m17256_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<EScreens,System.String,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m17279(__this, ___result, method) (( DictionaryEntry_t451  (*) (Transform_1_t2663 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m17257_gshared)(__this, ___result, method)
