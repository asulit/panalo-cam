﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.PositionAsUV1
struct PositionAsUV1_t685;
// UnityEngine.Mesh
struct Mesh_t104;

// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
extern "C" void PositionAsUV1__ctor_m3257 (PositionAsUV1_t685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.PositionAsUV1::ModifyMesh(UnityEngine.Mesh)
extern "C" void PositionAsUV1_ModifyMesh_m3258 (PositionAsUV1_t685 * __this, Mesh_t104 * ___mesh, const MethodInfo* method) IL2CPP_METHOD_ATTR;
