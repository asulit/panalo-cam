﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1951;
// System.String
struct String_t;
// System.Reflection.Module[]
struct ModuleU5BU5D_t1953;
// System.Type[]
struct TypeU5BU5D_t1005;
// System.Exception
struct Exception_t359;
// System.Reflection.AssemblyName
struct AssemblyName_t1998;

// System.String System.Reflection.Emit.AssemblyBuilder::get_Location()
extern "C" String_t* AssemblyBuilder_get_Location_m11730 (AssemblyBuilder_t1951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module[] System.Reflection.Emit.AssemblyBuilder::GetModulesInternal()
extern "C" ModuleU5BU5D_t1953* AssemblyBuilder_GetModulesInternal_m11731 (AssemblyBuilder_t1951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.AssemblyBuilder::GetTypes(System.Boolean)
extern "C" TypeU5BU5D_t1005* AssemblyBuilder_GetTypes_m11732 (AssemblyBuilder_t1951 * __this, bool ___exportedOnly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.AssemblyBuilder::get_IsCompilerContext()
extern "C" bool AssemblyBuilder_get_IsCompilerContext_m11733 (AssemblyBuilder_t1951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.AssemblyBuilder::not_supported()
extern "C" Exception_t359 * AssemblyBuilder_not_supported_m11734 (AssemblyBuilder_t1951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Reflection.Emit.AssemblyBuilder::UnprotectedGetName()
extern "C" AssemblyName_t1998 * AssemblyBuilder_UnprotectedGetName_m11735 (AssemblyBuilder_t1951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
