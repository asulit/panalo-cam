﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ListenRaffleResult
struct ListenRaffleResult_t332;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"

// System.Void ListenRaffleResult::.ctor(System.Object,System.IntPtr)
extern "C" void ListenRaffleResult__ctor_m1293 (ListenRaffleResult_t332 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ListenRaffleResult::Invoke(System.Int32,ERaffleResult)
extern "C" void ListenRaffleResult_Invoke_m1294 (ListenRaffleResult_t332 * __this, int32_t ___raffleId, int32_t ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
extern "C" void pinvoke_delegate_wrapper_ListenRaffleResult_t332(Il2CppObject* delegate, int32_t ___raffleId, int32_t ___result);
// System.IAsyncResult ListenRaffleResult::BeginInvoke(System.Int32,ERaffleResult,System.AsyncCallback,System.Object)
extern "C" Object_t * ListenRaffleResult_BeginInvoke_m1295 (ListenRaffleResult_t332 * __this, int32_t ___raffleId, int32_t ___result, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ListenRaffleResult::EndInvoke(System.IAsyncResult)
extern "C" void ListenRaffleResult_EndInvoke_m1296 (ListenRaffleResult_t332 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
