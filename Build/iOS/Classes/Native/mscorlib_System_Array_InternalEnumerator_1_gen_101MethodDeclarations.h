﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_101.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_46.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m28215_gshared (InternalEnumerator_1_t3441 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m28215(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3441 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m28215_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28216_gshared (InternalEnumerator_1_t3441 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28216(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3441 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28216_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m28217_gshared (InternalEnumerator_1_t3441 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m28217(__this, method) (( void (*) (InternalEnumerator_1_t3441 *, const MethodInfo*))InternalEnumerator_1_Dispose_m28217_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m28218_gshared (InternalEnumerator_1_t3441 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m28218(__this, method) (( bool (*) (InternalEnumerator_1_t3441 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m28218_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::get_Current()
extern "C" KeyValuePair_2_t3440  InternalEnumerator_1_get_Current_m28219_gshared (InternalEnumerator_1_t3441 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m28219(__this, method) (( KeyValuePair_2_t3440  (*) (InternalEnumerator_1_t3441 *, const MethodInfo*))InternalEnumerator_1_get_Current_m28219_gshared)(__this, method)
