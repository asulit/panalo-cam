﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"

// System.Boolean Common.BoundsExtensions::Intersects2D(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C" bool BoundsExtensions_Intersects2D_m437 (Object_t * __this /* static, unused */, Bounds_t349  ___self, Bounds_t349  ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
