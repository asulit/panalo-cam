﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Animator/<PlayAnimationLoop>c__Iterator9
struct U3CPlayAnimationLoopU3Ec__Iterator9_t233;
// System.Object
struct Object_t;

// System.Void Animator/<PlayAnimationLoop>c__Iterator9::.ctor()
extern "C" void U3CPlayAnimationLoopU3Ec__Iterator9__ctor_m837 (U3CPlayAnimationLoopU3Ec__Iterator9_t233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Animator/<PlayAnimationLoop>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CPlayAnimationLoopU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m838 (U3CPlayAnimationLoopU3Ec__Iterator9_t233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Animator/<PlayAnimationLoop>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CPlayAnimationLoopU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m839 (U3CPlayAnimationLoopU3Ec__Iterator9_t233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Animator/<PlayAnimationLoop>c__Iterator9::MoveNext()
extern "C" bool U3CPlayAnimationLoopU3Ec__Iterator9_MoveNext_m840 (U3CPlayAnimationLoopU3Ec__Iterator9_t233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Animator/<PlayAnimationLoop>c__Iterator9::Dispose()
extern "C" void U3CPlayAnimationLoopU3Ec__Iterator9_Dispose_m841 (U3CPlayAnimationLoopU3Ec__Iterator9_t233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Animator/<PlayAnimationLoop>c__Iterator9::Reset()
extern "C" void U3CPlayAnimationLoopU3Ec__Iterator9_Reset_m842 (U3CPlayAnimationLoopU3Ec__Iterator9_t233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
