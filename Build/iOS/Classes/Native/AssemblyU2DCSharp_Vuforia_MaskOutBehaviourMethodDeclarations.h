﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MaskOutBehaviour
struct MaskOutBehaviour_t294;

// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern "C" void MaskOutBehaviour__ctor_m1252 (MaskOutBehaviour_t294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MaskOutBehaviour::Start()
extern "C" void MaskOutBehaviour_Start_m1253 (MaskOutBehaviour_t294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
