﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__32.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>
struct  Enumerator_t1285 
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::host_enumerator
	Enumerator_t3229  ___host_enumerator_0;
};
