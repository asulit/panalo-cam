﻿#pragma once
#include <stdint.h>
// UnityThreadHelper
struct UnityThreadHelper_t181;
// UnityThreading.Dispatcher
struct Dispatcher_t162;
// UnityThreading.TaskDistributor
struct TaskDistributor_t166;
// System.Collections.Generic.List`1<UnityThreading.ThreadBase>
struct List_1_t182;
// System.Func`2<UnityThreading.ThreadBase,System.Boolean>
struct Func_2_t183;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityThreadHelper
struct  UnityThreadHelper_t181  : public MonoBehaviour_t5
{
	// UnityThreading.Dispatcher UnityThreadHelper::dispatcher
	Dispatcher_t162 * ___dispatcher_3;
	// UnityThreading.TaskDistributor UnityThreadHelper::taskDistributor
	TaskDistributor_t166 * ___taskDistributor_4;
	// System.Collections.Generic.List`1<UnityThreading.ThreadBase> UnityThreadHelper::registeredThreads
	List_1_t182 * ___registeredThreads_5;
};
struct UnityThreadHelper_t181_StaticFields{
	// UnityThreadHelper UnityThreadHelper::instance
	UnityThreadHelper_t181 * ___instance_2;
	// System.Func`2<UnityThreading.ThreadBase,System.Boolean> UnityThreadHelper::<>f__am$cache4
	Func_2_t183 * ___U3CU3Ef__amU24cache4_6;
};
