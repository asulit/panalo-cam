﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>

#ifndef _MSC_VER
#else
#endif

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Aabb2
#include "AssemblyU2DCSharp_Aabb2.h"
// Assertion
#include "AssemblyU2DCSharp_Assertion.h"
// OrthographicCamera
#include "AssemblyU2DCSharp_OrthographicCamera.h"
// Common.ColorUtils
#include "AssemblyU2DCSharp_Common_ColorUtils.h"
// Comment
#include "AssemblyU2DCSharp_Comment.h"
// CountdownTimer
#include "AssemblyU2DCSharp_CountdownTimer.h"
// Common.DelegateCommand
#include "AssemblyU2DCSharp_Common_DelegateCommand.h"
// Common.Extensions.EnumExtension/<GetFlagValues>c__Iterator0
#include "AssemblyU2DCSharp_Common_Extensions_EnumExtension_U3CGetFlag.h"
// FrameRate
#include "AssemblyU2DCSharp_FrameRate.h"
// FrameRateView
#include "AssemblyU2DCSharp_FrameRateView.h"
// Common.Fsm.FsmDelegateAction
#include "AssemblyU2DCSharp_Common_Fsm_FsmDelegateAction.h"
// Common.Fsm.Action.MoveAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_MoveAction.h"
// Common.Fsm.Action.MoveAlongDirection
#include "AssemblyU2DCSharp_Common_Fsm_Action_MoveAlongDirection.h"
// Common.Fsm.Action.MoveAlongDirectionByPolledTime
#include "AssemblyU2DCSharp_Common_Fsm_Action_MoveAlongDirectionByPoll.h"
// Common.Fsm.Action.NonResettingTimedWait
#include "AssemblyU2DCSharp_Common_Fsm_Action_NonResettingTimedWait.h"
// Common.Fsm.Action.RotateAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_RotateAction.h"
// Common.Fsm.Action.ScaleAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_ScaleAction.h"
// Common.Fsm.Action.TimedFadeVolume
#include "AssemblyU2DCSharp_Common_Fsm_Action_TimedFadeVolume.h"
// Common.Fsm.Action.TimedWaitAction
#include "AssemblyU2DCSharp_Common_Fsm_Action_TimedWaitAction.h"
// Common.Fsm.ConcreteFsmState
#include "AssemblyU2DCSharp_Common_Fsm_ConcreteFsmState.h"
// Common.Fsm.Fsm
#include "AssemblyU2DCSharp_Common_Fsm_Fsm.h"
// Common.Fsm.Fsm/<ExitState>c__AnonStoreyF
#include "AssemblyU2DCSharp_Common_Fsm_Fsm_U3CExitStateU3Ec__AnonStore.h"
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapter.h"
// HudOrthoAutoScale
#include "AssemblyU2DCSharp_HudOrthoAutoScale.h"
// InputLayer
#include "AssemblyU2DCSharp_InputLayer.h"
// InputLayerElement
#include "AssemblyU2DCSharp_InputLayerElement.h"
// InputLayerStack
#include "AssemblyU2DCSharp_InputLayerStack.h"
// MiniJSON.Json/Parser
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser.h"
// MiniJSON.Json/Parser/TOKEN
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser_TOKEN.h"
// MiniJSON.Json/Serializer
#include "AssemblyU2DCSharp_MiniJSON_Json_Serializer.h"
// Common.Logger.Log
#include "AssemblyU2DCSharp_Common_Logger_Log.h"
// Common.Logger.LogLevel
#include "AssemblyU2DCSharp_Common_Logger_LogLevel.h"
// Common.Logger.Logger
#include "AssemblyU2DCSharp_Common_Logger_Logger.h"
// LoggerComponent
#include "AssemblyU2DCSharp_LoggerComponent.h"
// Common.Math.IntVector2
#include "AssemblyU2DCSharp_Common_Math_IntVector2.h"
// MeshMerger
#include "AssemblyU2DCSharp_MeshMerger.h"
// MeshShaderSetter
#include "AssemblyU2DCSharp_MeshShaderSetter.h"
// Common.Notification.NotificationInstance
#include "AssemblyU2DCSharp_Common_Notification_NotificationInstance.h"
// Common.Notification.NotificationSystem
#include "AssemblyU2DCSharp_Common_Notification_NotificationSystem.h"
// NotificationSystemComponent
#include "AssemblyU2DCSharp_NotificationSystemComponent.h"
// PrefabManager
#include "AssemblyU2DCSharp_PrefabManager.h"
// PrefabManager/PruneData
#include "AssemblyU2DCSharp_PrefabManager_PruneData.h"
// PrefabManager/PreloadData
#include "AssemblyU2DCSharp_PrefabManager_PreloadData.h"
// PrefabManager/<Preload>c__Iterator1
#include "AssemblyU2DCSharp_PrefabManager_U3CPreloadU3Ec__Iterator1.h"
// PrefabManager/<PruneItems>c__Iterator2
#include "AssemblyU2DCSharp_PrefabManager_U3CPruneItemsU3Ec__Iterator2.h"
// ProceduralTexturedQuad
#include "AssemblyU2DCSharp_ProceduralTexturedQuad.h"
// Common.Query.ConcreteQueryRequest
#include "AssemblyU2DCSharp_Common_Query_ConcreteQueryRequest.h"
// Common.Query.ConcreteQueryResult
#include "AssemblyU2DCSharp_Common_Query_ConcreteQueryResult.h"
// Common.Query.QuerySystem
#include "AssemblyU2DCSharp_Common_Query_QuerySystem.h"
// Common.Query.QuerySystemImplementation
#include "AssemblyU2DCSharp_Common_Query_QuerySystemImplementation.h"
// SelfManagingSwarmItemManager
#include "AssemblyU2DCSharp_SelfManagingSwarmItemManager.h"
// Common.Signal.ConcreteSignalParameters
#include "AssemblyU2DCSharp_Common_Signal_ConcreteSignalParameters.h"
// Common.Signal.Signal
#include "AssemblyU2DCSharp_Common_Signal_Signal.h"
// Common.Signal.StringToSignalMapper
#include "AssemblyU2DCSharp_Common_Signal_StringToSignalMapper.h"
// Common.Time.TimeReference
#include "AssemblyU2DCSharp_Common_Time_TimeReference.h"
// Common.Time.TimeReferencePool
#include "AssemblyU2DCSharp_Common_Time_TimeReferencePool.h"
// Common.TimedKill
#include "AssemblyU2DCSharp_Common_TimedKill.h"
// Common.TimedKill/<KillAfterDuration>c__Iterator3
#include "AssemblyU2DCSharp_Common_TimedKill_U3CKillAfterDurationU3Ec_.h"
// TouchCollider
#include "AssemblyU2DCSharp_TouchCollider.h"
// Common.Utils.Factory
#include "AssemblyU2DCSharp_Common_Utils_Factory.h"
// Common.Utils.PopupValueSet
#include "AssemblyU2DCSharp_Common_Utils_PopupValueSet.h"
// Common.Utils.SimpleEncryption
#include "AssemblyU2DCSharp_Common_Utils_SimpleEncryption.h"
// VectorUtils
#include "AssemblyU2DCSharp_VectorUtils.h"
// Common.Xml.SimpleXmlNode
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNode.h"
// Common.Xml.SimpleXmlReader
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlReader.h"
// XmlTest
#include "AssemblyU2DCSharp_XmlTest.h"
// Common.XmlVariables
#include "AssemblyU2DCSharp_Common_XmlVariables.h"
// SwarmItem
#include "AssemblyU2DCSharp_SwarmItem.h"
// SwarmItem/STATE
#include "AssemblyU2DCSharp_SwarmItem_STATE.h"
// SwarmItemManager
#include "AssemblyU2DCSharp_SwarmItemManager.h"
// SwarmItemManager/PrefabItemLists
#include "AssemblyU2DCSharp_SwarmItemManager_PrefabItemLists.h"
// SwarmItemManager/PrefabItem
#include "AssemblyU2DCSharp_SwarmItemManager_PrefabItem.h"
// UnityThreading.DispatcherBase
#include "AssemblyU2DCSharp_UnityThreading_DispatcherBase.h"
// UnityThreading.Dispatcher
#include "AssemblyU2DCSharp_UnityThreading_Dispatcher.h"
// UnityThreading.TaskSortingSystem
#include "AssemblyU2DCSharp_UnityThreading_TaskSortingSystem.h"
// UnityThreading.TaskBase
#include "AssemblyU2DCSharp_UnityThreading_TaskBase.h"
// UnityThreading.Task
#include "AssemblyU2DCSharp_UnityThreading_Task.h"
// UnityThreading.TaskDistributor
#include "AssemblyU2DCSharp_UnityThreading_TaskDistributor.h"
// UnityThreading.TaskWorker
#include "AssemblyU2DCSharp_UnityThreading_TaskWorker.h"
// UnityThreading.ThreadBase
#include "AssemblyU2DCSharp_UnityThreading_ThreadBase.h"
// UnityThreading.ActionThread
#include "AssemblyU2DCSharp_UnityThreading_ActionThread.h"
// UnityThreading.EnumeratableActionThread
#include "AssemblyU2DCSharp_UnityThreading_EnumeratableActionThread.h"
// UnityThreadHelper
#include "AssemblyU2DCSharp_UnityThreadHelper.h"
// UnityThreadHelper/<CreateThread>c__AnonStorey12
#include "AssemblyU2DCSharp_UnityThreadHelper_U3CCreateThreadU3Ec__Ano.h"
// UnityThreadHelper/<CreateThread>c__AnonStorey13
#include "AssemblyU2DCSharp_UnityThreadHelper_U3CCreateThreadU3Ec__Ano_0.h"
// UnityThreadHelper/<CreateThread>c__AnonStorey14
#include "AssemblyU2DCSharp_UnityThreadHelper_U3CCreateThreadU3Ec__Ano_1.h"
// UnityThreadHelper/<CreateThread>c__AnonStorey15
#include "AssemblyU2DCSharp_UnityThreadHelper_U3CCreateThreadU3Ec__Ano_2.h"
// UnityThreadHelper/<CreateThread>c__AnonStorey16
#include "AssemblyU2DCSharp_UnityThreadHelper_U3CCreateThreadU3Ec__Ano_3.h"
// BridgeHandler
#include "AssemblyU2DCSharp_BridgeHandler.h"
// BridgeHandler/<OnStartAudioRecord>c__Iterator5
#include "AssemblyU2DCSharp_BridgeHandler_U3COnStartAudioRecordU3Ec__I.h"
// EScreens
#include "AssemblyU2DCSharp_EScreens.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
// Screens
#include "AssemblyU2DCSharp_Screens.h"
// GameSignals
#include "AssemblyU2DCSharp_GameSignals.h"
// PanaloCam
#include "AssemblyU2DCSharp_PanaloCam.h"
// PanaloCamSystem
#include "AssemblyU2DCSharp_PanaloCamSystem.h"
// Params
#include "AssemblyU2DCSharp_Params.h"
// Raffle
#include "AssemblyU2DCSharp_Raffle.h"
// Raffle/<ProcessRequest>c__Iterator6
#include "AssemblyU2DCSharp_Raffle_U3CProcessRequestU3Ec__Iterator6.h"
// Raffle/<RegisterUser>c__AnonStorey17
#include "AssemblyU2DCSharp_Raffle_U3CRegisterUserU3Ec__AnonStorey17.h"
// Raffle/<GetPlayerInformation>c__AnonStorey18
#include "AssemblyU2DCSharp_Raffle_U3CGetPlayerInformationU3Ec__AnonSt.h"
// Raffle/<RegisterPlayerToPromo>c__AnonStorey19
#include "AssemblyU2DCSharp_Raffle_U3CRegisterPlayerToPromoU3Ec__AnonS.h"
// Raffle/<GetPromoWinner>c__AnonStorey1A
#include "AssemblyU2DCSharp_Raffle_U3CGetPromoWinnerU3Ec__AnonStorey1A.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
// RaffleData
#include "AssemblyU2DCSharp_RaffleData.h"
// ScannerHandler
#include "AssemblyU2DCSharp_ScannerHandler.h"
// ScannerHudRoot
#include "AssemblyU2DCSharp_ScannerHudRoot.h"
// EStatus
#include "AssemblyU2DCSharp_EStatus.h"
// LoadingSceneRoot
#include "AssemblyU2DCSharp_LoadingSceneRoot.h"
// LoadingSceneRoot/<CheckRaffleResults>c__Iterator7
#include "AssemblyU2DCSharp_LoadingSceneRoot_U3CCheckRaffleResultsU3Ec.h"
// LoadingSceneRoot/<CheckRegisterMessage>c__Iterator8
#include "AssemblyU2DCSharp_LoadingSceneRoot_U3CCheckRegisterMessageU3.h"
// LoadingSceneRoot/<PrepareFsm>c__AnonStorey1B
#include "AssemblyU2DCSharp_LoadingSceneRoot_U3CPrepareFsmU3Ec__AnonSt.h"
// RaffleEntryRoot
#include "AssemblyU2DCSharp_RaffleEntryRoot.h"
// RegisterSceneRoot
#include "AssemblyU2DCSharp_RegisterSceneRoot.h"
// AudioRecognizer
#include "AssemblyU2DCSharp_AudioRecognizer.h"
// TitleSceneRoot
#include "AssemblyU2DCSharp_TitleSceneRoot.h"
// Animator
#include "AssemblyU2DCSharp_Animator.h"
// Animator/<PlayAnimationLoop>c__Iterator9
#include "AssemblyU2DCSharp_Animator_U3CPlayAnimationLoopU3Ec__Iterato.h"
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"
// Poller
#include "AssemblyU2DCSharp_Poller.h"
// Poller/<StartPoll>c__IteratorA
#include "AssemblyU2DCSharp_Poller_U3CStartPollU3Ec__IteratorA.h"
// Window
#include "AssemblyU2DCSharp_Window.h"
// VideoPlayer
#include "AssemblyU2DCSharp_VideoPlayer.h"
// PanaloVideoPlayer
#include "AssemblyU2DCSharp_PanaloVideoPlayer.h"
// PanaloVideoPlayer/<Play>c__IteratorB
#include "AssemblyU2DCSharp_PanaloVideoPlayer_U3CPlayU3Ec__IteratorB.h"
// iTween
#include "AssemblyU2DCSharp_iTween.h"
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"
// iTween/NamedValueColor
#include "AssemblyU2DCSharp_iTween_NamedValueColor.h"
// iTween/Defaults
#include "AssemblyU2DCSharp_iTween_Defaults.h"
// iTween/CRSpline
#include "AssemblyU2DCSharp_iTween_CRSpline.h"
// iTween/<TweenDelay>c__IteratorC
#include "AssemblyU2DCSharp_iTween_U3CTweenDelayU3Ec__IteratorC.h"
// iTween/<TweenRestart>c__IteratorD
#include "AssemblyU2DCSharp_iTween_U3CTweenRestartU3Ec__IteratorD.h"
// iTween/<Start>c__IteratorE
#include "AssemblyU2DCSharp_iTween_U3CStartU3Ec__IteratorE.h"
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer.h"
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer.h"
// Vuforia.VuforiaBehaviour
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour.h"
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E.h"
// UnityEngine.EventSystems.EventHandle
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle.h"
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"
// UnityEngine.EventSystems.EventTrigger
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger.h"
// UnityEngine.EventSystems.EventTrigger/Entry
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger_Entry.h"
// UnityEngine.EventSystems.EventTriggerType
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTriggerType.h"
// UnityEngine.EventSystems.ExecuteEvents
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents.h"
// UnityEngine.EventSystems.MoveDirection
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
// UnityEngine.EventSystems.RaycasterManager
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterManager.h"
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
// UnityEngine.EventSystems.AxisEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventData.h"
// UnityEngine.EventSystems.BaseEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventData.h"
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
// UnityEngine.EventSystems.PointerEventData/InputButton
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
// UnityEngine.EventSystems.PointerEventData/FramePressState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
// UnityEngine.EventSystems.BaseInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputModule.h"
// UnityEngine.EventSystems.PointerInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule.h"
// UnityEngine.EventSystems.PointerInputModule/ButtonState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule_B.h"
// UnityEngine.EventSystems.PointerInputModule/MouseState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule_M.h"
// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule_M_0.h"
// UnityEngine.EventSystems.StandaloneInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul_0.h"
// UnityEngine.EventSystems.StandaloneInputModule/InputMode
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
// UnityEngine.EventSystems.TouchInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInputModule.h"
// UnityEngine.EventSystems.PhysicsRaycaster
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRaycaster.h"
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
// UnityEngine.UI.CoroutineTween.FloatTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatTween.h"
// UnityEngine.UI.AnimationTriggers
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggers.h"
// UnityEngine.UI.Button
#include "UnityEngine_UI_UnityEngine_UI_Button.h"
// UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1
#include "UnityEngine_UI_UnityEngine_UI_Button_U3COnFinishSubmitU3Ec__.h"
// UnityEngine.UI.CanvasUpdate
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate.h"
// UnityEngine.UI.CanvasUpdateRegistry
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistry.h"
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
// UnityEngine.UI.DefaultControls
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls.h"
// UnityEngine.UI.DefaultControls/Resources
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls_Resources.h"
// UnityEngine.UI.Dropdown
#include "UnityEngine_UI_UnityEngine_UI_Dropdown.h"
// UnityEngine.UI.Dropdown/DropdownItem
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownItem.h"
// UnityEngine.UI.Dropdown/OptionData
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData.h"
// UnityEngine.UI.Dropdown/OptionDataList
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionDataList.h"
// UnityEngine.UI.Dropdown/<DelayedDestroyDropdownList>c__Iterator2
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CDelayedDestroyDrop.h"
// UnityEngine.UI.Dropdown/<Show>c__AnonStorey6
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CShowU3Ec__AnonStor.h"
// UnityEngine.UI.FontData
#include "UnityEngine_UI_UnityEngine_UI_FontData.h"
// UnityEngine.UI.FontUpdateTracker
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTracker.h"
// UnityEngine.UI.Graphic
#include "UnityEngine_UI_UnityEngine_UI_Graphic.h"
// UnityEngine.UI.GraphicRaycaster
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster.h"
// UnityEngine.UI.GraphicRaycaster/BlockingObjects
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
// UnityEngine.UI.GraphicRegistry
#include "UnityEngine_UI_UnityEngine_UI_GraphicRegistry.h"
// UnityEngine.UI.Image
#include "UnityEngine_UI_UnityEngine_UI_Image.h"
// UnityEngine.UI.Image/Type
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
// UnityEngine.UI.Image/FillMethod
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
// UnityEngine.UI.Image/OriginHorizontal
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginHorizontal.h"
// UnityEngine.UI.Image/OriginVertical
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginVertical.h"
// UnityEngine.UI.Image/Origin90
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin90.h"
// UnityEngine.UI.Image/Origin180
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin180.h"
// UnityEngine.UI.Image/Origin360
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin360.h"
// UnityEngine.UI.InputField
#include "UnityEngine_UI_UnityEngine_UI_InputField.h"
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
// UnityEngine.UI.InputField/InputType
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
// UnityEngine.UI.InputField/CharacterValidation
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
// UnityEngine.UI.InputField/LineType
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
// UnityEngine.UI.InputField/EditState
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
// UnityEngine.UI.InputField/<CaretBlink>c__Iterator3
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CCaretBlinkU3Ec__.h"
// UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CMouseDragOutside.h"
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_Mask.h"
// UnityEngine.UI.MaskableGraphic
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic.h"
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
// UnityEngine.UI.RawImage
#include "UnityEngine_UI_UnityEngine_UI_RawImage.h"
// UnityEngine.UI.RectMask2D
#include "UnityEngine_UI_UnityEngine_UI_RectMask2D.h"
// UnityEngine.UI.Scrollbar
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar.h"
// UnityEngine.UI.Scrollbar/Direction
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
// UnityEngine.UI.Scrollbar/Axis
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
// UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator5
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRepeatU3Ec__.h"
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect.h"
// UnityEngine.UI.ScrollRect/MovementType
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
// UnityEngine.UI.ScrollRect/ScrollbarVisibility
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarVisibility.h"
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_Slider.h"
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial.h"
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntry.h"
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransition.h"
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup.h"
// UnityEngine.UI.ClipperRegistry
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry.h"
// UnityEngine.UI.RectangularVertexClipper
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexClipper.h"
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter.h"
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler.h"
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter.h"
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup.h"
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrou.h"
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement.h"
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup.h"
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility.h"
// UnityEngine.UI.VertexHelper
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper.h"
// UnityEngine.UI.BaseMeshEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect.h"
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_Shadow.h"
// UnityEngine.AssetBundleRequest
#include "UnityEngine_UnityEngine_AssetBundleRequest.h"
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
// UnityEngine.PrimitiveType
#include "UnityEngine_UnityEngine_PrimitiveType.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCente.h"
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderb.h"
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
// UnityEngine.CullingGroupEvent
#include "UnityEngine_UnityEngine_CullingGroupEvent.h"
// UnityEngine.CullingGroup
#include "UnityEngine_UnityEngine_CullingGroup.h"
// UnityEngine.GradientColorKey
#include "UnityEngine_UnityEngine_GradientColorKey.h"
// UnityEngine.GradientAlphaKey
#include "UnityEngine_UnityEngine_GradientAlphaKey.h"
// UnityEngine.Gradient
#include "UnityEngine_UnityEngine_Gradient.h"
// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
// UnityEngine.FullScreenMovieControlMode
#include "UnityEngine_UnityEngine_FullScreenMovieControlMode.h"
// UnityEngine.FullScreenMovieScalingMode
#include "UnityEngine_UnityEngine_FullScreenMovieScalingMode.h"
// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
// UnityEngine.TouchScreenKeyboard
#include "UnityEngine_UnityEngine_TouchScreenKeyboard.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_Plane.h"
// UnityEngineInternal.MathfInternal
#include "UnityEngine_UnityEngineInternal_MathfInternal.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_Mathf.h"
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.RectTransform/Edge
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"
// UnityEngine.RectTransform/Axis
#include "UnityEngine_UnityEngine_RectTransform_Axis.h"
// UnityEngine.ResourceRequest
#include "UnityEngine_UnityEngine_ResourceRequest.h"
// UnityEngine.SortingLayer
#include "UnityEngine_UnityEngine_SortingLayer.h"
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
// UnityEngine.WWWForm
#include "UnityEngine_UnityEngine_WWWForm.h"
// UnityEngine.WWWTranscoder
#include "UnityEngine_UnityEngine_WWWTranscoder.h"
// UnityEngine.CacheIndex
#include "UnityEngine_UnityEngine_CacheIndex.h"
// UnityEngine.AsyncOperation
#include "UnityEngine_UnityEngine_AsyncOperation.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_Application.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.Display
#include "UnityEngine_UnityEngine_Display.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
// UnityEngine.IMECompositionMode
#include "UnityEngine_UnityEngine_IMECompositionMode.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_Enumerator.h"
// UnityEngine.iOS.ActivityIndicatorStyle
#include "UnityEngine_UnityEngine_iOS_ActivityIndicatorStyle.h"
// UnityEngine.Advertisements.UnityAdsInternal
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsInternal.h"
// UnityEngine.Particle
#include "UnityEngine_UnityEngine_Particle.h"
// UnityEngine.Collision
#include "UnityEngine_UnityEngine_Collision.h"
// UnityEngine.QueryTriggerInteraction
#include "UnityEngine_UnityEngine_QueryTriggerInteraction.h"
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPoint.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityEngine.Physics2D
#include "UnityEngine_UnityEngine_Physics2D.h"
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
// UnityEngine.ContactPoint2D
#include "UnityEngine_UnityEngine_ContactPoint2D.h"
// UnityEngine.Collision2D
#include "UnityEngine_UnityEngine_Collision2D.h"
// UnityEngine.AudioSettings
#include "UnityEngine_UnityEngine_AudioSettings.h"
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"
// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSource.h"
// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEvent.h"
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurve.h"
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
// UnityEngine.AnimatorTransitionInfo
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo.h"
// UnityEngine.SkeletonBone
#include "UnityEngine_UnityEngine_SkeletonBone.h"
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimit.h"
// UnityEngine.HumanBone
#include "UnityEngine_UnityEngine_HumanBone.h"
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfo.h"
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGenerator.h"
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtility.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_Event.h"
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUI.h"
// UnityEngine.GUI/ScrollViewState
#include "UnityEngine_UnityEngine_GUI_ScrollViewState.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtility.h"
// UnityEngine.GUILayoutUtility/LayoutCache
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCache.h"
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroup.h"
// UnityEngine.GUIScrollGroup
#include "UnityEngine_UnityEngine_GUIScrollGroup.h"
// UnityEngine.GUIWordWrapSizer
#include "UnityEngine_UnityEngine_GUIWordWrapSizer.h"
// UnityEngine.GUILayoutOption
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_Type.h"
// UnityEngine.GUISettings
#include "UnityEngine_UnityEngine_GUISettings.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkin.h"
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleState.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// UnityEngine.ImagePosition
#include "UnityEngine_UnityEngine_ImagePosition.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// UnityEngine.FocusType
#include "UnityEngine_UnityEngine_FocusType.h"
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtility.h"
// UnityEngine.Internal_DrawArguments
#include "UnityEngine_UnityEngine_Internal_DrawArguments.h"
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_Resolution.h"
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapMode.h"
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWrite.h"
// UnityEngine.Rendering.CompareFunction
#include "UnityEngine_UnityEngine_Rendering_CompareFunction.h"
// UnityEngine.Rendering.ColorWriteMask
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask.h"
// UnityEngine.Rendering.StencilOp
#include "UnityEngine_UnityEngine_Rendering_StencilOp.h"
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfo.h"
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderState.h"
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityException.h"
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditor.h"
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReference.h"
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCall.h"
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbst.h"
// Vuforia.IOSCamRecoveringHelper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_IOSCamRecoveringHel.h"
// Vuforia.MonoCameraConfiguration
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MonoCameraConfigura.h"
// Vuforia.StereoCameraConfiguration
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StereoCameraConfigu.h"
// Vuforia.NullCameraConfiguration
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullCameraConfigura.h"
// Vuforia.UnityCameraExtensions
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityCameraExtensio.h"
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
// Vuforia.DataSetTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTrackableBeh.h"
// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstrac.h"
// Vuforia.CameraDevice
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice.h"
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
// Vuforia.CloudRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBe.h"
// Vuforia.ReconstructionImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionImpl.h"
// Vuforia.HideExcessAreaAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstr.h"
// Vuforia.TrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableImpl.h"
// Vuforia.ObjectTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetImpl.h"
// Vuforia.UnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityPlayer.h"
// Vuforia.ReconstructionFromTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT_0.h"
// Vuforia.ReconstructionFromTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT.h"
// Vuforia.Eyewear
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear.h"
// Vuforia.Eyewear/EyeID
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_EyeID.h"
// Vuforia.Eyewear/EyewearCalibrationReading
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Eyewear_EyewearCali.h"
// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"
// Vuforia.SmartTerrainTrackerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker.h"
// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"
// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr.h"
// Vuforia.DataSet/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet_StorageType.h"
// Vuforia.DatabaseLoadAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DatabaseLoadAbstrac.h"
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"
// Vuforia.RectangleIntData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleIntData.h"
// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"
// Vuforia.OrientedBoundingBox3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0.h"
// Vuforia.BehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentF.h"
// Vuforia.CloudRecoImageTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoImageTarge.h"
// Vuforia.CylinderTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetImpl.h"
// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"
// Vuforia.ImageTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetData.h"
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"
// Vuforia.CameraDeviceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDeviceImpl.h"
// Vuforia.DataSetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetImpl.h"
// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// Vuforia.ImageImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageImpl.h"
// Vuforia.ImageTargetBuilderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilderI.h"
// Vuforia.ImageTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetImpl.h"
// Vuforia.Tracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Tracker.h"
// Vuforia.ObjectTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrackerImpl.h"
// Vuforia.MarkerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerImpl.h"
// Vuforia.MarkerTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTrackerImpl.h"
// Vuforia.NullWebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullWebCamTexAdapto.h"
// Vuforia.PlayModeEditorUtility
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtili_0.h"
// Vuforia.PremiumObjectFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor_0.h"
// Vuforia.VuforiaManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManager.h"
// Vuforia.VuforiaManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl.h"
// Vuforia.VuforiaManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_.h"
// Vuforia.VuforiaManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"
// Vuforia.VuforiaManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"
// Vuforia.VuforiaManagerImpl/Obb2D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__2.h"
// Vuforia.VuforiaManagerImpl/Obb3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__3.h"
// Vuforia.VuforiaManagerImpl/WordResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__4.h"
// Vuforia.VuforiaManagerImpl/WordData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__5.h"
// Vuforia.VuforiaManagerImpl/ImageHeaderData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__6.h"
// Vuforia.VuforiaManagerImpl/MeshData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__7.h"
// Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__8.h"
// Vuforia.VuforiaManagerImpl/SurfaceData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__9.h"
// Vuforia.VuforiaManagerImpl/PropData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__10.h"
// Vuforia.VuforiaManagerImpl/FrameState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__11.h"
// Vuforia.VuforiaManagerImpl/AutoRotationState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__12.h"
// Vuforia.VuforiaManagerImpl/<>c__DisplayClass3
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__13.h"
// Vuforia.VuforiaRenderer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer.h"
// Vuforia.VuforiaRenderer/FpsHint
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Fps.h"
// Vuforia.VuforiaRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid_0.h"
// Vuforia.VuforiaRenderer/VideoBGCfgData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid_1.h"
// Vuforia.VuforiaRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec.h"
// Vuforia.VuforiaRenderer/VideoTextureInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vid.h"
// Vuforia.VuforiaRendererImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRendererImpl_0.h"
// Vuforia.VuforiaRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRendererImpl.h"
// Vuforia.VuforiaUnityImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnityImpl.h"
// Vuforia.SmartTerrainTrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab_0.h"
// Vuforia.SurfaceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceImpl.h"
// Vuforia.SmartTerrainBuilderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder_0.h"
// Vuforia.PropImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropImpl.h"
// Vuforia.SmartTerrainTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_1.h"
// Vuforia.TextTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerImpl.h"
// Vuforia.TextTrackerImpl/UpDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_UpD.h"
// Vuforia.TypeMapping
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TypeMapping.h"
// Vuforia.WebCamTexAdaptorImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptorImp.h"
// Vuforia.WordImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordImpl.h"
// Vuforia.WordPrefabCreationMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefabCreationM.h"
// Vuforia.WordManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManagerImpl.h"
// Vuforia.WordResultImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordResultImpl.h"
// Vuforia.VuforiaWrapper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaWrapper.h"
// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBe.h"
// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstr.h"
// Vuforia.PropAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavio.h"
// Vuforia.StateManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManagerImpl.h"
// Vuforia.TargetFinder/InitState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"
// Vuforia.TargetFinder/UpdateState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"
// Vuforia.TargetFinder/FilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Filter.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
// Vuforia.TargetFinderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinderImpl.h"
// Vuforia.TargetFinderImpl/TargetFinderState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_Ta.h"
// Vuforia.TargetFinderImpl/InternalTargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_In.h"
// Vuforia.TrackableSourceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableSourceImpl.h"
// Vuforia.TextureRenderer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextureRenderer.h"
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManager.h"
// Vuforia.TrackerManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManagerImpl.h"
// Vuforia.VirtualButton
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton.h"
// Vuforia.VirtualButton/Sensitivity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"
// Vuforia.VirtualButtonImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonImpl.h"
// Vuforia.WebCamImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamImpl.h"
// Vuforia.WebCamProfile
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
// Vuforia.WebCamProfile/ProfileCollection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"
// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"
// Vuforia.MarkerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehav.h"
// Vuforia.MaskOutAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeha.h"
// Vuforia.MultiTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstract.h"
// Vuforia.VuforiaUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"
// Vuforia.VuforiaUnity/VuforiaHint
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Vufori.h"
// Vuforia.VuforiaUnity/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Storag.h"
// Vuforia.VuforiaAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeha.h"
// Vuforia.VuforiaAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeha_0.h"
// Vuforia.VuforiaMacros
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaMacros.h"
// Vuforia.VuforiaRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtili.h"
// Vuforia.VuforiaRuntimeUtilities/WebCamUsed
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtili_0.h"
// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilities.h"
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"
// Vuforia.SimpleTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SimpleTargetData.h"
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBu.h"
// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst.h"
// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendere.h"
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehav.h"
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterMode.h"
// <PrivateImplementationDetails>{C4A9B53A-936F-421A-9398-89108BD15C01}
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDet_0.h"
// Mono.Security.Cryptography.KeyBuilder
#include "System_Core_Mono_Security_Cryptography_KeyBuilder.h"
// Mono.Security.Cryptography.SymmetricTransform
#include "System_Core_Mono_Security_Cryptography_SymmetricTransform.h"
// System.Linq.Enumerable/Fallback
#include "System_Core_System_Linq_Enumerable_Fallback.h"
// System.Security.Cryptography.AesTransform
#include "System_Core_System_Security_Cryptography_AesTransform.h"
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3E.h"
// Mono.Math.BigInteger
#include "Mono_Security_Mono_Math_BigInteger.h"
// Mono.Math.BigInteger/Sign
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
// Mono.Math.BigInteger/ModulusRing
#include "Mono_Security_Mono_Math_BigInteger_ModulusRing.h"
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
// Mono.Security.ASN1
#include "Mono_Security_Mono_Security_ASN1.h"
// Mono.Security.PKCS7/ContentInfo
#include "Mono_Security_Mono_Security_PKCS7_ContentInfo.h"
// Mono.Security.PKCS7/EncryptedData
#include "Mono_Security_Mono_Security_PKCS7_EncryptedData.h"
// Mono.Security.Cryptography.ARC4Managed
#include "Mono_Security_Mono_Security_Cryptography_ARC4Managed.h"
// Mono.Security.Cryptography.KeyBuilder
#include "Mono_Security_Mono_Security_Cryptography_KeyBuilder.h"
// Mono.Security.Cryptography.MD2Managed
#include "Mono_Security_Mono_Security_Cryptography_MD2Managed.h"
// Mono.Security.Cryptography.PKCS1
#include "Mono_Security_Mono_Security_Cryptography_PKCS1.h"
// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_PrivateKeyInf.h"
// Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_EncryptedPriv.h"
// Mono.Security.Cryptography.RC4
#include "Mono_Security_Mono_Security_Cryptography_RC4.h"
// Mono.Security.Cryptography.RSAManaged
#include "Mono_Security_Mono_Security_Cryptography_RSAManaged.h"
// Mono.Security.X509.SafeBag
#include "Mono_Security_Mono_Security_X509_SafeBag.h"
// Mono.Security.X509.PKCS12
#include "Mono_Security_Mono_Security_X509_PKCS12.h"
// Mono.Security.X509.PKCS12/DeriveBytes
#include "Mono_Security_Mono_Security_X509_PKCS12_DeriveBytes.h"
// Mono.Security.X509.X501
#include "Mono_Security_Mono_Security_X509_X501.h"
// Mono.Security.X509.X509Certificate
#include "Mono_Security_Mono_Security_X509_X509Certificate.h"
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
#include "Mono_Security_Mono_Security_X509_X509CertificateCollection_X.h"
// Mono.Security.X509.X509Chain
#include "Mono_Security_Mono_Security_X509_X509Chain.h"
// Mono.Security.X509.X509ChainStatusFlags
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
// Mono.Security.X509.X509Crl
#include "Mono_Security_Mono_Security_X509_X509Crl.h"
// Mono.Security.X509.X509Crl/X509CrlEntry
#include "Mono_Security_Mono_Security_X509_X509Crl_X509CrlEntry.h"
// Mono.Security.X509.X509Extension
#include "Mono_Security_Mono_Security_X509_X509Extension.h"
// Mono.Security.X509.X509ExtensionCollection
#include "Mono_Security_Mono_Security_X509_X509ExtensionCollection.h"
// Mono.Security.X509.X509Store
#include "Mono_Security_Mono_Security_X509_X509Store.h"
// Mono.Security.X509.X509StoreManager
#include "Mono_Security_Mono_Security_X509_X509StoreManager.h"
// Mono.Security.X509.X509Stores
#include "Mono_Security_Mono_Security_X509_X509Stores.h"
// Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension
#include "Mono_Security_Mono_Security_X509_Extensions_AuthorityKeyIden.h"
// Mono.Security.X509.Extensions.BasicConstraintsExtension
#include "Mono_Security_Mono_Security_X509_Extensions_BasicConstraints.h"
// Mono.Security.X509.Extensions.ExtendedKeyUsageExtension
#include "Mono_Security_Mono_Security_X509_Extensions_ExtendedKeyUsage.h"
// Mono.Security.X509.Extensions.GeneralNames
#include "Mono_Security_Mono_Security_X509_Extensions_GeneralNames.h"
// Mono.Security.X509.Extensions.KeyUsages
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsages.h"
// Mono.Security.X509.Extensions.KeyUsageExtension
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsageExtensio.h"
// Mono.Security.X509.Extensions.NetscapeCertTypeExtension
#include "Mono_Security_Mono_Security_X509_Extensions_NetscapeCertType_0.h"
// Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes
#include "Mono_Security_Mono_Security_X509_Extensions_NetscapeCertType.h"
// Mono.Security.X509.Extensions.SubjectAltNameExtension
#include "Mono_Security_Mono_Security_X509_Extensions_SubjectAltNameEx.h"
// Mono.Security.Cryptography.HMAC
#include "Mono_Security_Mono_Security_Cryptography_HMAC.h"
// Mono.Security.Cryptography.MD5SHA1
#include "Mono_Security_Mono_Security_Cryptography_MD5SHA1.h"
// Mono.Security.Protocol.Tls.AlertLevel
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
// Mono.Security.Protocol.Tls.Alert
#include "Mono_Security_Mono_Security_Protocol_Tls_Alert.h"
// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
// Mono.Security.Protocol.Tls.CipherSuite
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuite.h"
// Mono.Security.Protocol.Tls.CipherSuiteCollection
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuiteCollecti.h"
// Mono.Security.Protocol.Tls.ClientContext
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientContext.h"
// Mono.Security.Protocol.Tls.ClientSessionInfo
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSessionInfo.h"
// Mono.Security.Protocol.Tls.ClientSessionCache
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSessionCache.h"
// Mono.Security.Protocol.Tls.ContentType
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
// Mono.Security.Protocol.Tls.Context
#include "Mono_Security_Mono_Security_Protocol_Tls_Context.h"
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
// Mono.Security.Protocol.Tls.HashAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
// Mono.Security.Protocol.Tls.HttpsClientStream
#include "Mono_Security_Mono_Security_Protocol_Tls_HttpsClientStream.h"
// Mono.Security.Protocol.Tls.RecordProtocol
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProtocol.h"
// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProtocol_Rece.h"
// Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProtocol_Send.h"
// Mono.Security.Protocol.Tls.RSASslSignatureDeformatter
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSignatureDefo.h"
// Mono.Security.Protocol.Tls.RSASslSignatureFormatter
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSignatureForm.h"
// Mono.Security.Protocol.Tls.SecurityCompressionType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
// Mono.Security.Protocol.Tls.SecurityParameters
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityParameters.h"
// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
// Mono.Security.Protocol.Tls.ValidationResult
#include "Mono_Security_Mono_Security_Protocol_Tls_ValidationResult.h"
// Mono.Security.Protocol.Tls.SslClientStream
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClientStream.h"
// Mono.Security.Protocol.Tls.SslCipherSuite
#include "Mono_Security_Mono_Security_Protocol_Tls_SslCipherSuite.h"
// Mono.Security.Protocol.Tls.SslHandshakeHash
#include "Mono_Security_Mono_Security_Protocol_Tls_SslHandshakeHash.h"
// Mono.Security.Protocol.Tls.SslStreamBase
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStreamBase.h"
// Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStreamBase_Inter.h"
// Mono.Security.Protocol.Tls.TlsCipherSuite
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsCipherSuite.h"
// Mono.Security.Protocol.Tls.TlsClientSettings
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsClientSettings.h"
// Mono.Security.Protocol.Tls.TlsException
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsException.h"
// Mono.Security.Protocol.Tls.TlsServerSettings
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsServerSettings.h"
// Mono.Security.Protocol.Tls.TlsStream
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsStream.h"
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake_0.h"
// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_1.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_2.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9.h"
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3E.h"
// System.MonoTODOAttribute
#include "System_System_MonoTODOAttribute.h"
// System.Collections.Specialized.HybridDictionary
#include "System_System_Collections_Specialized_HybridDictionary.h"
// System.Collections.Specialized.ListDictionary
#include "System_System_Collections_Specialized_ListDictionary.h"
// System.Collections.Specialized.ListDictionary/DictionaryNode
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
// System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator
#include "System_System_Collections_Specialized_ListDictionary_Diction_0.h"
// System.Collections.Specialized.ListDictionary/DictionaryNodeCollection
#include "System_System_Collections_Specialized_ListDictionary_Diction_2.h"
// System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator
#include "System_System_Collections_Specialized_ListDictionary_Diction_1.h"
// System.Collections.Specialized.NameObjectCollectionBase
#include "System_System_Collections_Specialized_NameObjectCollectionBa_2.h"
// System.Collections.Specialized.NameObjectCollectionBase/_Item
#include "System_System_Collections_Specialized_NameObjectCollectionBa.h"
// System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator
#include "System_System_Collections_Specialized_NameObjectCollectionBa_0.h"
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
#include "System_System_Collections_Specialized_NameObjectCollectionBa_1.h"
// System.Collections.Specialized.NameValueCollection
#include "System_System_Collections_Specialized_NameValueCollection.h"
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttribute.h"
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"
// System.ComponentModel.TypeConverterAttribute
#include "System_System_ComponentModel_TypeConverterAttribute.h"
// System.Net.Security.AuthenticationLevel
#include "System_System_Net_Security_AuthenticationLevel.h"
// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrors.h"
// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamily.h"
// System.Net.FileWebRequest
#include "System_System_Net_FileWebRequest.h"
// System.Net.FtpWebRequest
#include "System_System_Net_FtpWebRequest.h"
// System.Net.HttpVersion
#include "System_System_Net_HttpVersion.h"
// System.Net.HttpWebRequest
#include "System_System_Net_HttpWebRequest.h"
// System.Net.IPAddress
#include "System_System_Net_IPAddress.h"
// System.Net.IPv6Address
#include "System_System_Net_IPv6Address.h"
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolType.h"
// System.Net.ServicePoint
#include "System_System_Net_ServicePoint.h"
// System.Net.ServicePointManager
#include "System_System_Net_ServicePointManager.h"
// System.Net.ServicePointManager/SPKey
#include "System_System_Net_ServicePointManager_SPKey.h"
// System.Net.WebHeaderCollection
#include "System_System_Net_WebHeaderCollection.h"
// System.Net.WebProxy
#include "System_System_Net_WebProxy.h"
// System.Net.WebRequest
#include "System_System_Net_WebRequest.h"
// System.Security.Cryptography.X509Certificates.OpenFlags
#include "System_System_Security_Cryptography_X509Certificates_OpenFla.h"
// System.Security.Cryptography.X509Certificates.PublicKey
#include "System_System_Security_Cryptography_X509Certificates_PublicK.h"
// System.Security.Cryptography.X509Certificates.StoreLocation
#include "System_System_Security_Cryptography_X509Certificates_StoreLo.h"
// System.Security.Cryptography.X509Certificates.StoreName
#include "System_System_Security_Cryptography_X509Certificates_StoreNa.h"
// System.Security.Cryptography.X509Certificates.X500DistinguishedName
#include "System_System_Security_Cryptography_X509Certificates_X500Dis.h"
// System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags
#include "System_System_Security_Cryptography_X509Certificates_X500Dis_0.h"
// System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Bas.h"
// System.Security.Cryptography.X509Certificates.X509Certificate2
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_0.h"
// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_3.h"
// System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_1.h"
// System.Security.Cryptography.X509Certificates.X509Chain
#include "System_System_Security_Cryptography_X509Certificates_X509Cha.h"
// System.Security.Cryptography.X509Certificates.X509ChainElement
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_0.h"
// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_2.h"
// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_3.h"
// System.Security.Cryptography.X509Certificates.X509ChainPolicy
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_4.h"
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
// System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Enh.h"
// System.Security.Cryptography.X509Certificates.X509Extension
#include "System_System_Security_Cryptography_X509Certificates_X509Ext.h"
// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Ext_0.h"
// System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Ext_1.h"
// System.Security.Cryptography.X509Certificates.X509FindType
#include "System_System_Security_Cryptography_X509Certificates_X509Fin.h"
// System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Key.h"
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
// System.Security.Cryptography.X509Certificates.X509NameType
#include "System_System_Security_Cryptography_X509Certificates_X509Nam.h"
// System.Security.Cryptography.X509Certificates.X509RevocationFlag
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
// System.Security.Cryptography.X509Certificates.X509RevocationMode
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
// System.Security.Cryptography.X509Certificates.X509Store
#include "System_System_Security_Cryptography_X509Certificates_X509Sto.h"
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Sub.h"
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
#include "System_System_Security_Cryptography_X509Certificates_X509Sub_0.h"
// System.Security.Cryptography.X509Certificates.X509VerificationFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
// System.Security.Cryptography.AsnDecodeStatus
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
// System.Security.Cryptography.AsnEncodedData
#include "System_System_Security_Cryptography_AsnEncodedData.h"
// System.Security.Cryptography.Oid
#include "System_System_Security_Cryptography_Oid.h"
// System.Security.Cryptography.OidCollection
#include "System_System_Security_Cryptography_OidCollection.h"
// System.Security.Cryptography.OidEnumerator
#include "System_System_Security_Cryptography_OidEnumerator.h"
// System.Text.RegularExpressions.BaseMachine
#include "System_System_Text_RegularExpressions_BaseMachine.h"
// System.Text.RegularExpressions.Capture
#include "System_System_Text_RegularExpressions_Capture.h"
// System.Text.RegularExpressions.CaptureCollection
#include "System_System_Text_RegularExpressions_CaptureCollection.h"
// System.Text.RegularExpressions.Group
#include "System_System_Text_RegularExpressions_Group.h"
// System.Text.RegularExpressions.GroupCollection
#include "System_System_Text_RegularExpressions_GroupCollection.h"
// System.Text.RegularExpressions.Match
#include "System_System_Text_RegularExpressions_Match.h"
// System.Text.RegularExpressions.MatchCollection
#include "System_System_Text_RegularExpressions_MatchCollection.h"
// System.Text.RegularExpressions.MatchCollection/Enumerator
#include "System_System_Text_RegularExpressions_MatchCollection_Enumer.h"
// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_Regex.h"
// System.Text.RegularExpressions.RegexOptions
#include "System_System_Text_RegularExpressions_RegexOptions.h"
// System.Text.RegularExpressions.OpCode
#include "System_System_Text_RegularExpressions_OpCode.h"
// System.Text.RegularExpressions.OpFlags
#include "System_System_Text_RegularExpressions_OpFlags.h"
// System.Text.RegularExpressions.Position
#include "System_System_Text_RegularExpressions_Position.h"
// System.Text.RegularExpressions.FactoryCache
#include "System_System_Text_RegularExpressions_FactoryCache.h"
// System.Text.RegularExpressions.FactoryCache/Key
#include "System_System_Text_RegularExpressions_FactoryCache_Key.h"
// System.Text.RegularExpressions.MRUList
#include "System_System_Text_RegularExpressions_MRUList.h"
// System.Text.RegularExpressions.MRUList/Node
#include "System_System_Text_RegularExpressions_MRUList_Node.h"
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
// System.Text.RegularExpressions.InterpreterFactory
#include "System_System_Text_RegularExpressions_InterpreterFactory.h"
// System.Text.RegularExpressions.PatternCompiler
#include "System_System_Text_RegularExpressions_PatternCompiler.h"
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter_0.h"
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter.h"
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStack.h"
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
// System.Text.RegularExpressions.Interpreter
#include "System_System_Text_RegularExpressions_Interpreter.h"
// System.Text.RegularExpressions.Interpreter/IntStack
#include "System_System_Text_RegularExpressions_Interpreter_IntStack.h"
// System.Text.RegularExpressions.Interpreter/RepeatContext
#include "System_System_Text_RegularExpressions_Interpreter_RepeatCont.h"
// System.Text.RegularExpressions.Interpreter/Mode
#include "System_System_Text_RegularExpressions_Interpreter_Mode.h"
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
// System.Text.RegularExpressions.IntervalCollection
#include "System_System_Text_RegularExpressions_IntervalCollection.h"
// System.Text.RegularExpressions.IntervalCollection/Enumerator
#include "System_System_Text_RegularExpressions_IntervalCollection_Enu.h"
// System.Text.RegularExpressions.Syntax.Parser
#include "System_System_Text_RegularExpressions_Syntax_Parser.h"
// System.Text.RegularExpressions.QuickSearch
#include "System_System_Text_RegularExpressions_QuickSearch.h"
// System.Text.RegularExpressions.ReplacementEvaluator
#include "System_System_Text_RegularExpressions_ReplacementEvaluator.h"
// System.Text.RegularExpressions.Syntax.CompositeExpression
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpres.h"
// System.Text.RegularExpressions.Syntax.RegularExpression
#include "System_System_Text_RegularExpressions_Syntax_RegularExpressi.h"
// System.Text.RegularExpressions.Syntax.CapturingGroup
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroup.h"
// System.Text.RegularExpressions.Syntax.BalancingGroup
#include "System_System_Text_RegularExpressions_Syntax_BalancingGroup.h"
// System.Text.RegularExpressions.Syntax.Repetition
#include "System_System_Text_RegularExpressions_Syntax_Repetition.h"
// System.Text.RegularExpressions.Syntax.CaptureAssertion
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertio.h"
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsser.h"
// System.Text.RegularExpressions.Syntax.Literal
#include "System_System_Text_RegularExpressions_Syntax_Literal.h"
// System.Text.RegularExpressions.Syntax.PositionAssertion
#include "System_System_Text_RegularExpressions_Syntax_PositionAsserti.h"
// System.Text.RegularExpressions.Syntax.Reference
#include "System_System_Text_RegularExpressions_Syntax_Reference.h"
// System.Text.RegularExpressions.Syntax.BackslashNumber
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumber.h"
// System.Text.RegularExpressions.Syntax.CharacterClass
#include "System_System_Text_RegularExpressions_Syntax_CharacterClass.h"
// System.Text.RegularExpressions.Syntax.AnchorInfo
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfo.h"
// System.Uri
#include "System_System_Uri.h"
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
// System.UriKind
#include "System_System_UriKind.h"
// System.UriParser
#include "System_System_UriParser.h"
// System.UriPartial
#include "System_System_UriPartial.h"
// <PrivateImplementationDetails>
#include "System_U3CPrivateImplementationDetailsU3E.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Int64
#include "mscorlib_System_Int64.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.UInt64
#include "mscorlib_System_UInt64.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// System.SByte
#include "mscorlib_System_SByte.h"
// System.Int16
#include "mscorlib_System_Int16.h"
// System.UInt16
#include "mscorlib_System_UInt16.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.String
#include "mscorlib_System_String.h"
// System.Single
#include "mscorlib_System_Single.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.Decimal
#include "mscorlib_System_Decimal.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.Enum
#include "mscorlib_System_Enum.h"
// System.Array/SimpleEnumerator
#include "mscorlib_System_Array_SimpleEnumerator.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.Runtime.InteropServices.DllImportAttribute
#include "mscorlib_System_Runtime_InteropServices_DllImportAttribute.h"
// System.Runtime.InteropServices.MarshalAsAttribute
#include "mscorlib_System_Runtime_InteropServices_MarshalAsAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttr.h"
// System.Runtime.InteropServices.FieldOffsetAttribute
#include "mscorlib_System_Runtime_InteropServices_FieldOffsetAttribute.h"
// System.RuntimeArgumentHandle
#include "mscorlib_System_RuntimeArgumentHandle.h"
// System.TypedReference
#include "mscorlib_System_TypedReference.h"
// System.ArgIterator
#include "mscorlib_System_ArgIterator.h"
// System.MarshalByRefObject
#include "mscorlib_System_MarshalByRefObject.h"
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttribute.h"
// Mono.Globalization.Unicode.CodePointIndexer
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
// Mono.Globalization.Unicode.TailoringInfo
#include "mscorlib_Mono_Globalization_Unicode_TailoringInfo.h"
// Mono.Globalization.Unicode.Contraction
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
// Mono.Globalization.Unicode.ContractionComparer
#include "mscorlib_Mono_Globalization_Unicode_ContractionComparer.h"
// Mono.Globalization.Unicode.Level2Map
#include "mscorlib_Mono_Globalization_Unicode_Level2Map.h"
// Mono.Globalization.Unicode.Level2MapComparer
#include "mscorlib_Mono_Globalization_Unicode_Level2MapComparer.h"
// Mono.Globalization.Unicode.MSCompatUnicodeTable
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicodeTable.h"
// Mono.Globalization.Unicode.MSCompatUnicodeTableUtil
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicodeTableUtil.h"
// Mono.Globalization.Unicode.SimpleCollator
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator.h"
// Mono.Globalization.Unicode.SimpleCollator/Context
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
// Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_PreviousI.h"
// Mono.Globalization.Unicode.SimpleCollator/Escape
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Escape.h"
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
// System.Globalization.SortKey
#include "mscorlib_System_Globalization_SortKey.h"
// Mono.Globalization.Unicode.SortKeyBuffer
#include "mscorlib_Mono_Globalization_Unicode_SortKeyBuffer.h"
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
// Mono.Math.BigInteger
#include "mscorlib_Mono_Math_BigInteger.h"
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
// Mono.Math.BigInteger/ModulusRing
#include "mscorlib_Mono_Math_BigInteger_ModulusRing.h"
// Mono.Security.Cryptography.KeyBuilder
#include "mscorlib_Mono_Security_Cryptography_KeyBuilder.h"
// Mono.Security.Cryptography.BlockProcessor
#include "mscorlib_Mono_Security_Cryptography_BlockProcessor.h"
// Mono.Security.Cryptography.DSAManaged
#include "mscorlib_Mono_Security_Cryptography_DSAManaged.h"
// Mono.Security.Cryptography.KeyPairPersistence
#include "mscorlib_Mono_Security_Cryptography_KeyPairPersistence.h"
// Mono.Security.Cryptography.MACAlgorithm
#include "mscorlib_Mono_Security_Cryptography_MACAlgorithm.h"
// Mono.Security.Cryptography.PKCS1
#include "mscorlib_Mono_Security_Cryptography_PKCS1.h"
// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
#include "mscorlib_Mono_Security_Cryptography_PKCS8_PrivateKeyInfo.h"
// Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
#include "mscorlib_Mono_Security_Cryptography_PKCS8_EncryptedPrivateKe.h"
// Mono.Security.Cryptography.RSAManaged
#include "mscorlib_Mono_Security_Cryptography_RSAManaged.h"
// Mono.Security.Cryptography.SymmetricTransform
#include "mscorlib_Mono_Security_Cryptography_SymmetricTransform.h"
// Mono.Security.X509.SafeBag
#include "mscorlib_Mono_Security_X509_SafeBag.h"
// Mono.Security.X509.PKCS12
#include "mscorlib_Mono_Security_X509_PKCS12.h"
// Mono.Security.X509.PKCS12/DeriveBytes
#include "mscorlib_Mono_Security_X509_PKCS12_DeriveBytes.h"
// Mono.Security.X509.X501
#include "mscorlib_Mono_Security_X509_X501.h"
// Mono.Security.X509.X509Certificate
#include "mscorlib_Mono_Security_X509_X509Certificate.h"
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
#include "mscorlib_Mono_Security_X509_X509CertificateCollection_X509Ce.h"
// Mono.Security.X509.X509Extension
#include "mscorlib_Mono_Security_X509_X509Extension.h"
// Mono.Security.X509.X509ExtensionCollection
#include "mscorlib_Mono_Security_X509_X509ExtensionCollection.h"
// Mono.Security.ASN1
#include "mscorlib_Mono_Security_ASN1.h"
// Mono.Security.PKCS7/ContentInfo
#include "mscorlib_Mono_Security_PKCS7_ContentInfo.h"
// Mono.Security.PKCS7/EncryptedData
#include "mscorlib_Mono_Security_PKCS7_EncryptedData.h"
// Mono.Security.StrongName
#include "mscorlib_Mono_Security_StrongName.h"
// Mono.Xml.SecurityParser
#include "mscorlib_Mono_Xml_SecurityParser.h"
// Mono.Xml.SmallXmlParser
#include "mscorlib_Mono_Xml_SmallXmlParser.h"
// Mono.Xml.SmallXmlParser/AttrListImpl
#include "mscorlib_Mono_Xml_SmallXmlParser_AttrListImpl.h"
// Mono.Xml.SmallXmlParserException
#include "mscorlib_Mono_Xml_SmallXmlParserException.h"
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_Link.h"
// System.Collections.ArrayList
#include "mscorlib_System_Collections_ArrayList.h"
// System.Collections.ArrayList/SimpleEnumerator
#include "mscorlib_System_Collections_ArrayList_SimpleEnumerator.h"
// System.Collections.ArrayList/ArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_ArrayListWrapper.h"
// System.Collections.ArrayList/SynchronizedArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_SynchronizedArrayListW.h"
// System.Collections.BitArray
#include "mscorlib_System_Collections_BitArray.h"
// System.Collections.BitArray/BitArrayEnumerator
#include "mscorlib_System_Collections_BitArray_BitArrayEnumerator.h"
// System.Collections.CaseInsensitiveComparer
#include "mscorlib_System_Collections_CaseInsensitiveComparer.h"
// System.Collections.CaseInsensitiveHashCodeProvider
#include "mscorlib_System_Collections_CaseInsensitiveHashCodeProvider.h"
// System.Collections.CollectionBase
#include "mscorlib_System_Collections_CollectionBase.h"
// System.Collections.Comparer
#include "mscorlib_System_Collections_Comparer.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Hashtable
#include "mscorlib_System_Collections_Hashtable.h"
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"
// System.Collections.Hashtable/KeyMarker
#include "mscorlib_System_Collections_Hashtable_KeyMarker.h"
// System.Collections.Hashtable/EnumeratorMode
#include "mscorlib_System_Collections_Hashtable_EnumeratorMode.h"
// System.Collections.Hashtable/Enumerator
#include "mscorlib_System_Collections_Hashtable_Enumerator.h"
// System.Collections.Hashtable/HashKeys
#include "mscorlib_System_Collections_Hashtable_HashKeys.h"
// System.Collections.Hashtable/HashValues
#include "mscorlib_System_Collections_Hashtable_HashValues.h"
// System.Collections.Queue
#include "mscorlib_System_Collections_Queue.h"
// System.Collections.Queue/QueueEnumerator
#include "mscorlib_System_Collections_Queue_QueueEnumerator.h"
// System.Collections.SortedList
#include "mscorlib_System_Collections_SortedList.h"
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"
// System.Collections.SortedList/EnumeratorMode
#include "mscorlib_System_Collections_SortedList_EnumeratorMode.h"
// System.Collections.SortedList/Enumerator
#include "mscorlib_System_Collections_SortedList_Enumerator.h"
// System.Collections.SortedList/ListKeys
#include "mscorlib_System_Collections_SortedList_ListKeys.h"
// System.Collections.Stack
#include "mscorlib_System_Collections_Stack.h"
// System.Collections.Stack/Enumerator
#include "mscorlib_System_Collections_Stack_Enumerator.h"
// System.Configuration.Assemblies.AssemblyHashAlgorithm
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgorit.h"
// System.Configuration.Assemblies.AssemblyVersionCompatibility
#include "mscorlib_System_Configuration_Assemblies_AssemblyVersionComp.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute/DebuggingModes
#include "mscorlib_System_Diagnostics_DebuggableAttribute_DebuggingMod.h"
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttribute.h"
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttribute.h"
// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrame.h"
// System.Diagnostics.StackTrace
#include "mscorlib_System_Diagnostics_StackTrace.h"
// System.Globalization.Calendar
#include "mscorlib_System_Globalization_Calendar.h"
// System.Globalization.CompareInfo
#include "mscorlib_System_Globalization_CompareInfo.h"
// System.Globalization.CompareOptions
#include "mscorlib_System_Globalization_CompareOptions.h"
// System.Globalization.CultureInfo
#include "mscorlib_System_Globalization_CultureInfo.h"
// System.Globalization.DateTimeFormatFlags
#include "mscorlib_System_Globalization_DateTimeFormatFlags.h"
// System.Globalization.DateTimeFormatInfo
#include "mscorlib_System_Globalization_DateTimeFormatInfo.h"
// System.Globalization.DateTimeStyles
#include "mscorlib_System_Globalization_DateTimeStyles.h"
// System.Globalization.DaylightTime
#include "mscorlib_System_Globalization_DaylightTime.h"
// System.Globalization.GregorianCalendar
#include "mscorlib_System_Globalization_GregorianCalendar.h"
// System.Globalization.GregorianCalendarTypes
#include "mscorlib_System_Globalization_GregorianCalendarTypes.h"
// System.Globalization.NumberFormatInfo
#include "mscorlib_System_Globalization_NumberFormatInfo.h"
// System.Globalization.NumberStyles
#include "mscorlib_System_Globalization_NumberStyles.h"
// System.Globalization.TextInfo
#include "mscorlib_System_Globalization_TextInfo.h"
// System.Globalization.TextInfo/Data
#include "mscorlib_System_Globalization_TextInfo_Data.h"
// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategory.h"
// System.IO.BinaryReader
#include "mscorlib_System_IO_BinaryReader.h"
// System.IO.DirectoryInfo
#include "mscorlib_System_IO_DirectoryInfo.h"
// System.IO.FileAccess
#include "mscorlib_System_IO_FileAccess.h"
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"
// System.IO.FileMode
#include "mscorlib_System_IO_FileMode.h"
// System.IO.FileNotFoundException
#include "mscorlib_System_IO_FileNotFoundException.h"
// System.IO.FileOptions
#include "mscorlib_System_IO_FileOptions.h"
// System.IO.FileShare
#include "mscorlib_System_IO_FileShare.h"
// System.IO.FileStream
#include "mscorlib_System_IO_FileStream.h"
// System.IO.FileStreamAsyncResult
#include "mscorlib_System_IO_FileStreamAsyncResult.h"
// System.IO.FileSystemInfo
#include "mscorlib_System_IO_FileSystemInfo.h"
// System.IO.MemoryStream
#include "mscorlib_System_IO_MemoryStream.h"
// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileType.h"
// System.IO.MonoIO
#include "mscorlib_System_IO_MonoIO.h"
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"
// System.IO.MonoIOStat
#include "mscorlib_System_IO_MonoIOStat.h"
// System.IO.Path
#include "mscorlib_System_IO_Path.h"
// System.IO.SearchPattern
#include "mscorlib_System_IO_SearchPattern.h"
// System.IO.SeekOrigin
#include "mscorlib_System_IO_SeekOrigin.h"
// System.IO.Stream
#include "mscorlib_System_IO_Stream.h"
// System.IO.StreamAsyncResult
#include "mscorlib_System_IO_StreamAsyncResult.h"
// System.IO.StreamReader
#include "mscorlib_System_IO_StreamReader.h"
// System.IO.StreamWriter
#include "mscorlib_System_IO_StreamWriter.h"
// System.IO.StringReader
#include "mscorlib_System_IO_StringReader.h"
// System.IO.TextReader
#include "mscorlib_System_IO_TextReader.h"
// System.IO.SynchronizedReader
#include "mscorlib_System_IO_SynchronizedReader.h"
// System.IO.TextWriter
#include "mscorlib_System_IO_TextWriter.h"
// System.IO.SynchronizedWriter
#include "mscorlib_System_IO_SynchronizedWriter.h"
// System.IO.UnexceptionalStreamReader
#include "mscorlib_System_IO_UnexceptionalStreamReader.h"
// System.Reflection.Emit.AssemblyBuilder
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder.h"
// System.Reflection.Emit.ConstructorBuilder
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder.h"
// System.Reflection.Emit.EnumBuilder
#include "mscorlib_System_Reflection_Emit_EnumBuilder.h"
// System.Reflection.Emit.FieldBuilder
#include "mscorlib_System_Reflection_Emit_FieldBuilder.h"
// System.Reflection.Emit.GenericTypeParameterBuilder
#include "mscorlib_System_Reflection_Emit_GenericTypeParameterBuilder.h"
// System.Reflection.Emit.ILTokenInfo
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
// System.Reflection.Emit.ILGenerator
#include "mscorlib_System_Reflection_Emit_ILGenerator.h"
// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
// System.Reflection.Emit.ILGenerator/LabelData
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
// System.Reflection.Emit.MethodBuilder
#include "mscorlib_System_Reflection_Emit_MethodBuilder.h"
// System.Reflection.Emit.MethodToken
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
// System.Reflection.Emit.ModuleBuilder
#include "mscorlib_System_Reflection_Emit_ModuleBuilder.h"
// System.Reflection.Emit.ModuleBuilderTokenGenerator
#include "mscorlib_System_Reflection_Emit_ModuleBuilderTokenGenerator.h"
// System.Reflection.Emit.OpCode
#include "mscorlib_System_Reflection_Emit_OpCode.h"
// System.Reflection.Emit.OpCodeNames
#include "mscorlib_System_Reflection_Emit_OpCodeNames.h"
// System.Reflection.Emit.OpCodes
#include "mscorlib_System_Reflection_Emit_OpCodes.h"
// System.Reflection.Emit.ParameterBuilder
#include "mscorlib_System_Reflection_Emit_ParameterBuilder.h"
// System.Reflection.Emit.StackBehaviour
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
// System.Reflection.Emit.TypeBuilder
#include "mscorlib_System_Reflection_Emit_TypeBuilder.h"
// System.Reflection.Emit.UnmanagedMarshal
#include "mscorlib_System_Reflection_Emit_UnmanagedMarshal.h"
// System.Reflection.Assembly
#include "mscorlib_System_Reflection_Assembly.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyName
#include "mscorlib_System_Reflection_AssemblyName.h"
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttribute.h"
// System.Reflection.Binder
#include "mscorlib_System_Reflection_Binder.h"
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfo.h"
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributes.h"
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfo.h"
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"
// System.Reflection.MemberInfoSerializationHolder
#include "mscorlib_System_Reflection_MemberInfoSerializationHolder.h"
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributes.h"
// System.Reflection.MethodImplAttributes
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
// System.Reflection.Missing
#include "mscorlib_System_Reflection_Missing.h"
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"
// System.Reflection.MonoEvent
#include "mscorlib_System_Reflection_MonoEvent.h"
// System.Reflection.MonoField
#include "mscorlib_System_Reflection_MonoField.h"
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
// System.Reflection.MonoMethod
#include "mscorlib_System_Reflection_MonoMethod.h"
// System.Reflection.MonoCMethod
#include "mscorlib_System_Reflection_MonoCMethod.h"
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
// System.Reflection.PInfo
#include "mscorlib_System_Reflection_PInfo.h"
// System.Reflection.MonoProperty
#include "mscorlib_System_Reflection_MonoProperty.h"
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributes.h"
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfo.h"
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
// System.Reflection.Pointer
#include "mscorlib_System_Reflection_Pointer.h"
// System.Reflection.ProcessorArchitecture
#include "mscorlib_System_Reflection_ProcessorArchitecture.h"
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"
// System.Reflection.StrongNameKeyPair
#include "mscorlib_System_Reflection_StrongNameKeyPair.h"
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Runtime.CompilerServices.CompilationRelaxations
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati_0.h"
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAt.h"
// System.Runtime.CompilerServices.LoadHint
#include "mscorlib_System_Runtime_CompilerServices_LoadHint.h"
// System.Runtime.ConstrainedExecution.Cer
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer.h"
// System.Runtime.ConstrainedExecution.Consistency
#include "mscorlib_System_Runtime_ConstrainedExecution_Consistency.h"
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityCont.h"
// System.Runtime.InteropServices.CallingConvention
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"
// System.Runtime.InteropServices.CharSet
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttrib.h"
// System.Runtime.InteropServices.ClassInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceType.h"
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceA.h"
// System.Runtime.InteropServices.ComInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceType.h"
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttribute.h"
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleType.h"
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribu.h"
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_Marshal.h"
// System.Runtime.InteropServices.MarshalDirectiveException
#include "mscorlib_System_Runtime_InteropServices_MarshalDirectiveExce.h"
// System.Runtime.InteropServices.SafeHandle
#include "mscorlib_System_Runtime_InteropServices_SafeHandle.h"
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAt.h"
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
// System.Runtime.InteropServices.UnmanagedType
#include "mscorlib_System_Runtime_InteropServices_UnmanagedType.h"
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServic.h"
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAc.h"
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActi.h"
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttribute.h"
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfo.h"
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServices.h"
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainData.h"
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChan.h"
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSink.h"
// System.Runtime.Remoting.Channels.SinkProviderData
#include "mscorlib_System_Runtime_Remoting_Channels_SinkProviderData.h"
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_Context.h"
// System.Runtime.Remoting.Contexts.DynamicPropertyCollection
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicPropertyCol_0.h"
// System.Runtime.Remoting.Contexts.DynamicPropertyCollection/DynamicPropertyReg
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicPropertyCol.h"
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttribute.h"
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAtt.h"
// System.Runtime.Remoting.Contexts.SynchronizedClientContextSink
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizedClient.h"
// System.Runtime.Remoting.Contexts.SynchronizedServerContextSink
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizedServer.h"
// System.Runtime.Remoting.Lifetime.Lease
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lease.h"
// System.Runtime.Remoting.Lifetime.LeaseManager
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseManager.h"
// System.Runtime.Remoting.Lifetime.LeaseSink
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseSink.h"
// System.Runtime.Remoting.Lifetime.LeaseState
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseState.h"
// System.Runtime.Remoting.Lifetime.LifetimeServices
#include "mscorlib_System_Runtime_Remoting_Lifetime_LifetimeServices.h"
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoType.h"
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo.h"
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResult.h"
// System.Runtime.Remoting.Messaging.ClientContextTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_ClientContextTerm.h"
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCall.h"
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallD.h"
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSi.h"
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_Header.h"
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContex.h"
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCall.h"
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDiction.h"
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_.h"
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDicti.h"
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessage.h"
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0.h"
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessage.h"
// System.Runtime.Remoting.Messaging.ServerObjectTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerObjectTermi.h"
// System.Runtime.Remoting.Messaging.StackBuilderSink
#include "mscorlib_System_Runtime_Remoting_Messaging_StackBuilderSink.h"
// System.Runtime.Remoting.Metadata.SoapAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapAttribute.h"
// System.Runtime.Remoting.Metadata.SoapFieldAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapFieldAttribute.h"
// System.Runtime.Remoting.Metadata.SoapMethodAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapMethodAttribut.h"
// System.Runtime.Remoting.Metadata.SoapTypeAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapTypeAttribute.h"
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxy.h"
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxy.h"
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxy.h"
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServices.h"
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntry.h"
// System.Runtime.Remoting.ActivatedServiceTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedServiceTypeEntry.h"
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfo.h"
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentity.h"
// System.Runtime.Remoting.InternalRemotingServices
#include "mscorlib_System_Runtime_Remoting_InternalRemotingServices.h"
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRef.h"
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfiguration.h"
// System.Runtime.Remoting.ConfigHandler
#include "mscorlib_System_Runtime_Remoting_ConfigHandler.h"
// System.Runtime.Remoting.ChannelData
#include "mscorlib_System_Runtime_Remoting_ChannelData.h"
// System.Runtime.Remoting.ProviderData
#include "mscorlib_System_Runtime_Remoting_ProviderData.h"
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServices.h"
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentity.h"
// System.Runtime.Remoting.SoapServices
#include "mscorlib_System_Runtime_Remoting_SoapServices.h"
// System.Runtime.Remoting.SoapServices/TypeInfo
#include "mscorlib_System_Runtime_Remoting_SoapServices_TypeInfo.h"
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntry.h"
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfo.h"
// System.Runtime.Remoting.WellKnownClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_WellKnownClientTypeEntry.h"
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
// System.Runtime.Remoting.WellKnownServiceTypeEntry
#include "mscorlib_System_Runtime_Remoting_WellKnownServiceTypeEntry.h"
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina.h"
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0.h"
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Meth.h"
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Retu.h"
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1.h"
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1.h"
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje.h"
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0.h"
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAs.h"
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTy.h"
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
// System.Runtime.Serialization.ObjectManager
#include "mscorlib_System_Runtime_Serialization_ObjectManager.h"
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecord.h"
// System.Runtime.Serialization.ArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_ArrayFixupRecord.h"
// System.Runtime.Serialization.MultiArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_MultiArrayFixupRecord.h"
// System.Runtime.Serialization.FixupRecord
#include "mscorlib_System_Runtime_Serialization_FixupRecord.h"
// System.Runtime.Serialization.DelayedFixupRecord
#include "mscorlib_System_Runtime_Serialization_DelayedFixupRecord.h"
// System.Runtime.Serialization.ObjectRecordStatus
#include "mscorlib_System_Runtime_Serialization_ObjectRecordStatus.h"
// System.Runtime.Serialization.ObjectRecord
#include "mscorlib_System_Runtime_Serialization_ObjectRecord.h"
// System.Runtime.Serialization.SerializationCallbacks
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacks_0.h"
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// System.Runtime.Serialization.SerializationInfoEnumerator
#include "mscorlib_System_Runtime_Serialization_SerializationInfoEnume.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
// System.Security.Cryptography.X509Certificates.X509Certificate
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509C.h"
// System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509K.h"
// System.Security.Cryptography.AsymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_AsymmetricAlgorithm.h"
// System.Security.Cryptography.Base64Constants
#include "mscorlib_System_Security_Cryptography_Base64Constants.h"
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
// System.Security.Cryptography.CryptoConfig
#include "mscorlib_System_Security_Cryptography_CryptoConfig.h"
// System.Security.Cryptography.CryptoStream
#include "mscorlib_System_Security_Cryptography_CryptoStream.h"
// System.Security.Cryptography.CryptoStreamMode
#include "mscorlib_System_Security_Cryptography_CryptoStreamMode.h"
// System.Security.Cryptography.CspParameters
#include "mscorlib_System_Security_Cryptography_CspParameters.h"
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
// System.Security.Cryptography.DES
#include "mscorlib_System_Security_Cryptography_DES.h"
// System.Security.Cryptography.DESTransform
#include "mscorlib_System_Security_Cryptography_DESTransform.h"
// System.Security.Cryptography.DSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DSACryptoServiceProvid.h"
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
// System.Security.Cryptography.DSASignatureDeformatter
#include "mscorlib_System_Security_Cryptography_DSASignatureDeformatte.h"
// System.Security.Cryptography.DSASignatureFormatter
#include "mscorlib_System_Security_Cryptography_DSASignatureFormatter.h"
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMAC.h"
// System.Security.Cryptography.HMACSHA384
#include "mscorlib_System_Security_Cryptography_HMACSHA384.h"
// System.Security.Cryptography.HMACSHA512
#include "mscorlib_System_Security_Cryptography_HMACSHA512.h"
// System.Security.Cryptography.HashAlgorithm
#include "mscorlib_System_Security_Cryptography_HashAlgorithm.h"
// System.Security.Cryptography.KeySizes
#include "mscorlib_System_Security_Cryptography_KeySizes.h"
// System.Security.Cryptography.KeyedHashAlgorithm
#include "mscorlib_System_Security_Cryptography_KeyedHashAlgorithm.h"
// System.Security.Cryptography.MACTripleDES
#include "mscorlib_System_Security_Cryptography_MACTripleDES.h"
// System.Security.Cryptography.MD5CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_MD5CryptoServiceProvid.h"
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
// System.Security.Cryptography.PasswordDeriveBytes
#include "mscorlib_System_Security_Cryptography_PasswordDeriveBytes.h"
// System.Security.Cryptography.RC2
#include "mscorlib_System_Security_Cryptography_RC2.h"
// System.Security.Cryptography.RC2Transform
#include "mscorlib_System_Security_Cryptography_RC2Transform.h"
// System.Security.Cryptography.RIPEMD160Managed
#include "mscorlib_System_Security_Cryptography_RIPEMD160Managed.h"
// System.Security.Cryptography.RNGCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RNGCryptoServiceProvid.h"
// System.Security.Cryptography.RSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RSACryptoServiceProvid.h"
// System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1KeyExchangeFor.h"
// System.Security.Cryptography.RSAPKCS1SignatureDeformatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SignatureDefor.h"
// System.Security.Cryptography.RSAPKCS1SignatureFormatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SignatureForma.h"
// System.Security.Cryptography.RSAParameters
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
// System.Security.Cryptography.RijndaelTransform
#include "mscorlib_System_Security_Cryptography_RijndaelTransform.h"
// System.Security.Cryptography.RijndaelManagedTransform
#include "mscorlib_System_Security_Cryptography_RijndaelManagedTransfo.h"
// System.Security.Cryptography.SHA1Internal
#include "mscorlib_System_Security_Cryptography_SHA1Internal.h"
// System.Security.Cryptography.SHA1CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_SHA1CryptoServiceProvi.h"
// System.Security.Cryptography.SHA1Managed
#include "mscorlib_System_Security_Cryptography_SHA1Managed.h"
// System.Security.Cryptography.SHA256Managed
#include "mscorlib_System_Security_Cryptography_SHA256Managed.h"
// System.Security.Cryptography.SHA384Managed
#include "mscorlib_System_Security_Cryptography_SHA384Managed.h"
// System.Security.Cryptography.SHA512Managed
#include "mscorlib_System_Security_Cryptography_SHA512Managed.h"
// System.Security.Cryptography.SHAConstants
#include "mscorlib_System_Security_Cryptography_SHAConstants.h"
// System.Security.Cryptography.SignatureDescription
#include "mscorlib_System_Security_Cryptography_SignatureDescription.h"
// System.Security.Cryptography.SymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithm.h"
// System.Security.Cryptography.ToBase64Transform
#include "mscorlib_System_Security_Cryptography_ToBase64Transform.h"
// System.Security.Cryptography.TripleDESTransform
#include "mscorlib_System_Security_Cryptography_TripleDESTransform.h"
// System.Security.Permissions.SecurityPermission
#include "mscorlib_System_Security_Permissions_SecurityPermission.h"
// System.Security.Permissions.SecurityPermissionFlag
#include "mscorlib_System_Security_Permissions_SecurityPermissionFlag.h"
// System.Security.Permissions.StrongNamePublicKeyBlob
#include "mscorlib_System_Security_Permissions_StrongNamePublicKeyBlob.h"
// System.Security.Policy.ApplicationTrust
#include "mscorlib_System_Security_Policy_ApplicationTrust.h"
// System.Security.Policy.Evidence
#include "mscorlib_System_Security_Policy_Evidence.h"
// System.Security.Policy.Evidence/EvidenceEnumerator
#include "mscorlib_System_Security_Policy_Evidence_EvidenceEnumerator.h"
// System.Security.Policy.Hash
#include "mscorlib_System_Security_Policy_Hash.h"
// System.Security.Policy.StrongName
#include "mscorlib_System_Security_Policy_StrongName.h"
// System.Security.Principal.PrincipalPolicy
#include "mscorlib_System_Security_Principal_PrincipalPolicy.h"
// System.Security.Principal.WindowsAccountType
#include "mscorlib_System_Security_Principal_WindowsAccountType.h"
// System.Security.Principal.WindowsIdentity
#include "mscorlib_System_Security_Principal_WindowsIdentity.h"
// System.Security.PermissionSet
#include "mscorlib_System_Security_PermissionSet.h"
// System.Security.SecurityContext
#include "mscorlib_System_Security_SecurityContext.h"
// System.Security.SecurityElement
#include "mscorlib_System_Security_SecurityElement.h"
// System.Security.SecurityElement/SecurityAttribute
#include "mscorlib_System_Security_SecurityElement_SecurityAttribute.h"
// System.Security.SecurityException
#include "mscorlib_System_Security_SecurityException.h"
// System.Security.RuntimeDeclSecurityEntry
#include "mscorlib_System_Security_RuntimeDeclSecurityEntry.h"
// System.Security.RuntimeSecurityFrame
#include "mscorlib_System_Security_RuntimeSecurityFrame.h"
// System.Security.SecurityFrame
#include "mscorlib_System_Security_SecurityFrame.h"
// System.Security.SecurityManager
#include "mscorlib_System_Security_SecurityManager.h"
// System.Text.Decoder
#include "mscorlib_System_Text_Decoder.h"
// System.Text.DecoderFallback
#include "mscorlib_System_Text_DecoderFallback.h"
// System.Text.DecoderFallbackException
#include "mscorlib_System_Text_DecoderFallbackException.h"
// System.Text.DecoderReplacementFallback
#include "mscorlib_System_Text_DecoderReplacementFallback.h"
// System.Text.DecoderReplacementFallbackBuffer
#include "mscorlib_System_Text_DecoderReplacementFallbackBuffer.h"
// System.Text.EncoderFallback
#include "mscorlib_System_Text_EncoderFallback.h"
// System.Text.EncoderFallbackException
#include "mscorlib_System_Text_EncoderFallbackException.h"
// System.Text.EncoderReplacementFallback
#include "mscorlib_System_Text_EncoderReplacementFallback.h"
// System.Text.EncoderReplacementFallbackBuffer
#include "mscorlib_System_Text_EncoderReplacementFallbackBuffer.h"
// System.Text.Encoding
#include "mscorlib_System_Text_Encoding.h"
// System.Text.Encoding/ForwardingDecoder
#include "mscorlib_System_Text_Encoding_ForwardingDecoder.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// System.Text.UTF32Encoding
#include "mscorlib_System_Text_UTF32Encoding.h"
// System.Text.UTF32Encoding/UTF32Decoder
#include "mscorlib_System_Text_UTF32Encoding_UTF32Decoder.h"
// System.Text.UTF7Encoding
#include "mscorlib_System_Text_UTF7Encoding.h"
// System.Text.UTF7Encoding/UTF7Decoder
#include "mscorlib_System_Text_UTF7Encoding_UTF7Decoder.h"
// System.Text.UTF8Encoding
#include "mscorlib_System_Text_UTF8Encoding.h"
// System.Text.UTF8Encoding/UTF8Decoder
#include "mscorlib_System_Text_UTF8Encoding_UTF8Decoder.h"
// System.Text.UnicodeEncoding
#include "mscorlib_System_Text_UnicodeEncoding.h"
// System.Text.UnicodeEncoding/UnicodeDecoder
#include "mscorlib_System_Text_UnicodeEncoding_UnicodeDecoder.h"
// System.Threading.CompressedStack
#include "mscorlib_System_Threading_CompressedStack.h"
// System.Threading.EventResetMode
#include "mscorlib_System_Threading_EventResetMode.h"
// System.Threading.ExecutionContext
#include "mscorlib_System_Threading_ExecutionContext.h"
// System.Threading.Thread
#include "mscorlib_System_Threading_Thread.h"
// System.Threading.ThreadState
#include "mscorlib_System_Threading_ThreadState.h"
// System.Threading.Timer
#include "mscorlib_System_Threading_Timer.h"
// System.Threading.Timer/Scheduler
#include "mscorlib_System_Threading_Timer_Scheduler.h"
// System.Threading.WaitHandle
#include "mscorlib_System_Threading_WaitHandle.h"
// System.ActivationContext
#include "mscorlib_System_ActivationContext.h"
// System.AppDomain
#include "mscorlib_System_AppDomain.h"
// System.AppDomainSetup
#include "mscorlib_System_AppDomainSetup.h"
// System.ApplicationIdentity
#include "mscorlib_System_ApplicationIdentity.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// System.ArrayTypeMismatchException
#include "mscorlib_System_ArrayTypeMismatchException.h"
// System.AttributeTargets
#include "mscorlib_System_AttributeTargets.h"
// System.BitConverter
#include "mscorlib_System_BitConverter.h"
// System.CharEnumerator
#include "mscorlib_System_CharEnumerator.h"
// System.Console
#include "mscorlib_System_Console.h"
// System.Convert
#include "mscorlib_System_Convert.h"
// System.DBNull
#include "mscorlib_System_DBNull.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.DateTime/Which
#include "mscorlib_System_DateTime_Which.h"
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"
// System.DelegateData
#include "mscorlib_System_DelegateData.h"
// System.DelegateSerializationHolder
#include "mscorlib_System_DelegateSerializationHolder.h"
// System.DelegateSerializationHolder/DelegateEntry
#include "mscorlib_System_DelegateSerializationHolder_DelegateEntry.h"
// System.DllNotFoundException
#include "mscorlib_System_DllNotFoundException.h"
// System.EntryPointNotFoundException
#include "mscorlib_System_EntryPointNotFoundException.h"
// System.MonoEnumInfo
#include "mscorlib_System_MonoEnumInfo.h"
// System.Environment
#include "mscorlib_System_Environment.h"
// System.Environment/SpecialFolder
#include "mscorlib_System_Environment_SpecialFolder.h"
// System.EventArgs
#include "mscorlib_System_EventArgs.h"
// System.FormatException
#include "mscorlib_System_FormatException.h"
// System.Guid
#include "mscorlib_System_Guid.h"
// System.InvalidCastException
#include "mscorlib_System_InvalidCastException.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimization.h"
// System.LocalDataStoreSlot
#include "mscorlib_System_LocalDataStoreSlot.h"
// System.MissingMemberException
#include "mscorlib_System_MissingMemberException.h"
// System.MissingMethodException
#include "mscorlib_System_MissingMethodException.h"
// System.MonoAsyncCall
#include "mscorlib_System_MonoAsyncCall.h"
// System.MonoCustomAttrs
#include "mscorlib_System_MonoCustomAttrs.h"
// System.MonoCustomAttrs/AttributeInfo
#include "mscorlib_System_MonoCustomAttrs_AttributeInfo.h"
// System.MonoTouchAOTHelper
#include "mscorlib_System_MonoTouchAOTHelper.h"
// System.MonoTypeInfo
#include "mscorlib_System_MonoTypeInfo.h"
// System.MonoType
#include "mscorlib_System_MonoType.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// System.NullReferenceException
#include "mscorlib_System_NullReferenceException.h"
// System.NumberFormatter
#include "mscorlib_System_NumberFormatter.h"
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfo.h"
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
// System.OperatingSystem
#include "mscorlib_System_OperatingSystem.h"
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryException.h"
// System.OverflowException
#include "mscorlib_System_OverflowException.h"
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgs.h"
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
// System.StringComparer
#include "mscorlib_System_StringComparer.h"
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparer.h"
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparer.h"
// System.StringComparison
#include "mscorlib_System_StringComparison.h"
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptions.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.TimeZone
#include "mscorlib_System_TimeZone.h"
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZone.h"
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
// System.TypeInitializationException
#include "mscorlib_System_TypeInitializationException.h"
// System.TypeLoadException
#include "mscorlib_System_TypeLoadException.h"
// System.UnhandledExceptionEventArgs
#include "mscorlib_System_UnhandledExceptionEventArgs.h"
// System.UnitySerializationHolder
#include "mscorlib_System_UnitySerializationHolder.h"
// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityType.h"
// System.Version
#include "mscorlib_System_Version.h"
// System.WeakReference
#include "mscorlib_System_WeakReference.h"
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3E.h"
extern const int32_t g_FieldOffsetTable[8047] = 
{
	0,
	offsetof(Aabb2_t1, ___min_1),
	offsetof(Aabb2_t1, ___max_2),
	0,
	offsetof(OrthographicCamera_t4, ___selfCamera_2),
	offsetof(OrthographicCamera_t4, ___observerList_3),
	offsetof(ColorUtils_t8_StaticFields, ___BLACK_0),
	offsetof(ColorUtils_t8_StaticFields, ___WHITE_1),
	offsetof(ColorUtils_t8_StaticFields, ___GREEN_2),
	offsetof(Comment_t10, ___text_2),
	offsetof(CountdownTimer_t13, ___polledTime_0),
	offsetof(CountdownTimer_t13, ___countdownTime_1),
	offsetof(CountdownTimer_t13, ___timeReference_2),
	offsetof(DelegateCommand_t15, ___action_0),
	offsetof(U3CGetFlagValuesU3Ec__Iterator0_t20, ___U3CflagU3E__0_0),
	offsetof(U3CGetFlagValuesU3Ec__Iterator0_t20, ___p_enumType_1),
	offsetof(U3CGetFlagValuesU3Ec__Iterator0_t20, ___U3CU24s_14U3E__1_2),
	offsetof(U3CGetFlagValuesU3Ec__Iterator0_t20, ___U3CvalueU3E__2_3),
	offsetof(U3CGetFlagValuesU3Ec__Iterator0_t20, ___U3CbitsU3E__3_4),
	offsetof(U3CGetFlagValuesU3Ec__Iterator0_t20, ___U24PC_5),
	offsetof(U3CGetFlagValuesU3Ec__Iterator0_t20, ___U24current_6),
	offsetof(U3CGetFlagValuesU3Ec__Iterator0_t20, ___U3CU24U3Ep_enumType_7),
	offsetof(FrameRate_t24, ___frameRate_0),
	offsetof(FrameRate_t24, ___numFrames_1),
	offsetof(FrameRate_t24, ___polledTime_2),
	offsetof(FrameRateView_t25, ___text_2),
	offsetof(FrameRateView_t25, ___frameRate_3),
	offsetof(FsmDelegateAction_t32, ___onEnterRoutine_1),
	offsetof(FsmDelegateAction_t32, ___onUpdateRoutine_2),
	offsetof(FsmDelegateAction_t32, ___onExitRoutine_3),
	offsetof(MoveAction_t34, ___transform_1),
	offsetof(MoveAction_t34, ___positionFrom_2),
	offsetof(MoveAction_t34, ___positionTo_3),
	offsetof(MoveAction_t34, ___duration_4),
	offsetof(MoveAction_t34, ___timeReference_5),
	offsetof(MoveAction_t34, ___finishEvent_6),
	offsetof(MoveAction_t34, ___space_7),
	offsetof(MoveAction_t34, ___timer_8),
	offsetof(MoveAlongDirection_t37, ___actor_1),
	offsetof(MoveAlongDirection_t37, ___direction_2),
	offsetof(MoveAlongDirection_t37, ___velocity_3),
	offsetof(MoveAlongDirection_t37, ___timeReference_4),
	offsetof(MoveAlongDirectionByPolledTime_t38, ___actor_1),
	offsetof(MoveAlongDirectionByPolledTime_t38, ___direction_2),
	offsetof(MoveAlongDirectionByPolledTime_t38, ___velocity_3),
	offsetof(MoveAlongDirectionByPolledTime_t38, ___timeReference_4),
	offsetof(MoveAlongDirectionByPolledTime_t38, ___polledTime_5),
	offsetof(MoveAlongDirectionByPolledTime_t38, ___startPosition_6),
	offsetof(NonResettingTimedWait_t39, ___waitTime_1),
	offsetof(NonResettingTimedWait_t39, ___timer_2),
	offsetof(NonResettingTimedWait_t39, ___timeReference_3),
	offsetof(NonResettingTimedWait_t39, ___finishEvent_4),
	offsetof(RotateAction_t40, ___transform_1),
	offsetof(RotateAction_t40, ___quatFrom_2),
	offsetof(RotateAction_t40, ___quatTo_3),
	offsetof(RotateAction_t40, ___duration_4),
	offsetof(RotateAction_t40, ___finishEvent_5),
	offsetof(RotateAction_t40, ___timer_6),
	offsetof(ScaleAction_t42, ___transform_1),
	offsetof(ScaleAction_t42, ___scaleFrom_2),
	offsetof(ScaleAction_t42, ___scaleTo_3),
	offsetof(ScaleAction_t42, ___duration_4),
	offsetof(ScaleAction_t42, ___timeReference_5),
	offsetof(ScaleAction_t42, ___finishEvent_6),
	offsetof(ScaleAction_t42, ___timer_7),
	offsetof(TimedFadeVolume_t43, ___audioSource_1),
	offsetof(TimedFadeVolume_t43, ___volumeFrom_2),
	offsetof(TimedFadeVolume_t43, ___volumeTo_3),
	offsetof(TimedFadeVolume_t43, ___duration_4),
	offsetof(TimedFadeVolume_t43, ___finishEvent_5),
	offsetof(TimedFadeVolume_t43, ___timer_6),
	offsetof(TimedWaitAction_t45, ___waitTime_1),
	offsetof(TimedWaitAction_t45, ___timer_2),
	offsetof(TimedWaitAction_t45, ___timeReference_3),
	offsetof(TimedWaitAction_t45, ___finishEvent_4),
	offsetof(ConcreteFsmState_t46, ___name_0),
	offsetof(ConcreteFsmState_t46, ___owner_1),
	offsetof(ConcreteFsmState_t46, ___transitionMap_2),
	offsetof(ConcreteFsmState_t46, ___actionList_3),
	offsetof(Fsm_t47, ___name_0),
	offsetof(Fsm_t47, ___currentState_1),
	offsetof(Fsm_t47, ___stateMap_2),
	offsetof(Fsm_t47, ___globalTransitionMap_3),
	offsetof(Fsm_t47_StaticFields, ___U3CU3Ef__amU24cache4_4),
	offsetof(Fsm_t47_StaticFields, ___U3CU3Ef__amU24cache5_5),
	offsetof(U3CExitStateU3Ec__AnonStoreyF_t52, ___currentStateOnInvoke_0),
	offsetof(U3CExitStateU3Ec__AnonStoreyF_t52, ___U3CU3Ef__this_1),
	offsetof(FsmActionAdapter_t33, ___owner_0),
	offsetof(HudOrthoAutoScale_t55, ___selfCamera_2),
	offsetof(HudOrthoAutoScale_t55, ___orthoCamera_3),
	offsetof(HudOrthoAutoScale_t55, ___selfTransform_4),
	offsetof(HudOrthoAutoScale_t55, ___originalOrthoSize_5),
	offsetof(HudOrthoAutoScale_t55, ___prevOrthoSize_6),
	offsetof(HudOrthoAutoScale_t55, ___originalPosition_7),
	offsetof(HudOrthoAutoScale_t55, ___originalScale_8),
	offsetof(InputLayer_t56, ___modal_2),
	offsetof(InputLayer_t56, ___active_3),
	offsetof(InputLayer_t56, ___elements_4),
	offsetof(InputLayerElement_t58, ___touchColliders_2),
	offsetof(InputLayerElement_t58, ___referenceCameraName_3),
	offsetof(InputLayerElement_t58, ___referenceCamera_4),
	offsetof(InputLayerElement_t58, ___hit_5),
	offsetof(InputLayerStack_t61, ___initialLayers_2),
	offsetof(InputLayerStack_t61, ___topLayer_3),
	offsetof(InputLayerStack_t61, ___layerStack_4),
	0,
	offsetof(Parser_t66, ___json_1),
	offsetof(Parser_t66_StaticFields, ___U3CU3Ef__switchU24map0_2),
	offsetof(TOKEN_t65, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(Serializer_t69, ___builder_0),
	offsetof(Log_t72, ___level_0),
	offsetof(Log_t72, ___message_1),
	offsetof(Log_t72, ___timestamp_2),
	offsetof(LogLevel_t73_StaticFields, ___NORMAL_0),
	offsetof(LogLevel_t73_StaticFields, ___WARNING_1),
	offsetof(LogLevel_t73_StaticFields, ___ERROR_2),
	offsetof(LogLevel_t73, ___name_3),
	offsetof(Logger_t75, ___name_0),
	offsetof(Logger_t75, ___logFilePath_1),
	offsetof(Logger_t75, ___bufferQueue_2),
	offsetof(Logger_t75, ___writeQueue_3),
	offsetof(Logger_t75, ___currentlyWriting_4),
	offsetof(Logger_t75_StaticFields, ___WRITE_TIME_INTERVAL_5),
	offsetof(Logger_t75, ___writeTimer_6),
	offsetof(Logger_t75_StaticFields, ___ONLY_INSTANCE_7),
	offsetof(LoggerComponent_t78, ___name_2),
	offsetof(LoggerComponent_t78, ___logger_3),
	offsetof(IntVector2_t79_StaticFields, ___ZERO_0),
	offsetof(IntVector2_t79, ___x_1),
	offsetof(IntVector2_t79, ___y_2),
	offsetof(MeshMerger_t80, ___meshFilters_2),
	offsetof(MeshMerger_t80, ___material_3),
	offsetof(MeshShaderSetter_t83, ___shaderToSet_2),
	offsetof(NotificationInstance_t85, ___id_0),
	offsetof(NotificationInstance_t85, ___active_1),
	offsetof(NotificationInstance_t85, ___parameterMap_2),
	offsetof(NotificationInstance_t85, ___handled_3),
	offsetof(NotificationInstance_t85_StaticFields, ___instanceStack_4),
	offsetof(NotificationSystem_t89, ___handlerList_0),
	offsetof(NotificationSystem_t89_StaticFields, ___ONLY_INSTANCE_1),
	offsetof(NotificationSystemComponent_t91, ___handlerList_2),
	offsetof(NotificationSystemComponent_t91, ___system_3),
	offsetof(PrefabManager_t96, ___itemManager_2),
	offsetof(PrefabManager_t96, ___pruneDataList_3),
	offsetof(PrefabManager_t96, ___preloadDataList_4),
	offsetof(PrefabManager_t96, ___pruneIntervalTime_5),
	offsetof(PrefabManager_t96, ___nameToIndexMapping_6),
	offsetof(PrefabManager_t96, ___pruneTimer_7),
	offsetof(PruneData_t93, ___prefabName_0),
	offsetof(PruneData_t93, ___maxInactiveCount_1),
	offsetof(PreloadData_t94, ___prefabName_0),
	offsetof(PreloadData_t94, ___preloadCount_1),
	offsetof(U3CPreloadU3Ec__Iterator1_t95, ___U3CiU3E__0_0),
	offsetof(U3CPreloadU3Ec__Iterator1_t95, ___U24PC_1),
	offsetof(U3CPreloadU3Ec__Iterator1_t95, ___U24current_2),
	offsetof(U3CPreloadU3Ec__Iterator1_t95, ___U3CU3Ef__this_3),
	offsetof(U3CPruneItemsU3Ec__Iterator2_t97, ___U3CU24s_40U3E__0_0),
	offsetof(U3CPruneItemsU3Ec__Iterator2_t97, ___U3CU24s_41U3E__1_1),
	offsetof(U3CPruneItemsU3Ec__Iterator2_t97, ___U3CdataU3E__2_2),
	offsetof(U3CPruneItemsU3Ec__Iterator2_t97, ___U3CitemPrefabIndexU3E__3_3),
	offsetof(U3CPruneItemsU3Ec__Iterator2_t97, ___U24PC_4),
	offsetof(U3CPruneItemsU3Ec__Iterator2_t97, ___U24current_5),
	offsetof(U3CPruneItemsU3Ec__Iterator2_t97, ___U3CU3Ef__this_6),
	offsetof(ProceduralTexturedQuad_t102, ___topLeftUv_2),
	offsetof(ProceduralTexturedQuad_t102, ___topRightUv_3),
	offsetof(ProceduralTexturedQuad_t102, ___bottomRightUv_4),
	offsetof(ProceduralTexturedQuad_t102, ___bottomLeftUv_5),
	offsetof(ProceduralTexturedQuad_t102, ___texture_6),
	offsetof(ProceduralTexturedQuad_t102, ___modulationColor_7),
	offsetof(ProceduralTexturedQuad_t102, ___alphaCutoff_8),
	offsetof(ProceduralTexturedQuad_t102, ___material_9),
	offsetof(ProceduralTexturedQuad_t102, ___mesh_10),
	offsetof(ConcreteQueryRequest_t106, ___queryId_0),
	offsetof(ConcreteQueryRequest_t106, ___paramMap_1),
	offsetof(ConcreteQueryResult_t107, ___result_0),
	offsetof(QuerySystem_t108_StaticFields, ___systemInstance_0),
	offsetof(QuerySystemImplementation_t109, ___resolverMap_0),
	offsetof(QuerySystemImplementation_t109, ___result_1),
	offsetof(QuerySystemImplementation_t109, ___request_2),
	offsetof(QuerySystemImplementation_t109, ___locked_3),
	offsetof(SelfManagingSwarmItemManager_t99, ___preloadList_11),
	offsetof(ConcreteSignalParameters_t113, ___parameterMap_0),
	offsetof(Signal_t116, ___name_0),
	offsetof(Signal_t116, ___parameters_1),
	offsetof(Signal_t116, ___listenerList_2),
	offsetof(StringToSignalMapper_t118, ___mapping_0),
	offsetof(TimeReference_t14, ___name_0),
	offsetof(TimeReference_t14, ___timeScale_1),
	offsetof(TimeReference_t14, ___affectsGlobalTimeScale_2),
	offsetof(TimeReference_t14_StaticFields, ___DEFAULT_INSTANCE_3),
	offsetof(TimeReferencePool_t120, ___instanceMap_0),
	offsetof(TimeReferencePool_t120_StaticFields, ___ONLY_INSTANCE_1),
	offsetof(TimedKill_t123, ___swarm_2),
	offsetof(U3CKillAfterDurationU3Ec__Iterator3_t122, ___duration_0),
	offsetof(U3CKillAfterDurationU3Ec__Iterator3_t122, ___U24PC_1),
	offsetof(U3CKillAfterDurationU3Ec__Iterator3_t122, ___U24current_2),
	offsetof(U3CKillAfterDurationU3Ec__Iterator3_t122, ___U3CU24U3Eduration_3),
	offsetof(U3CKillAfterDurationU3Ec__Iterator3_t122, ___U3CU3Ef__this_4),
	offsetof(TouchCollider_t125, ___boxCollider_2),
	offsetof(TouchCollider_t125, ___commandList_3),
	offsetof(Factory_t133_StaticFields, ___factoryMap_0),
	0,
	0,
	offsetof(PopupValueSet_t136, ___valueList_0),
	0,
	0,
	offsetof(SimpleEncryption_t138, ___salt_0),
	offsetof(SimpleEncryption_t138, ___password_1),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(VectorUtils_t140_StaticFields, ___ZERO_0),
	offsetof(VectorUtils_t140_StaticFields, ___ONE_1),
	offsetof(VectorUtils_t140_StaticFields, ___UP_2),
	offsetof(VectorUtils_t140_StaticFields, ___RIGHT_3),
	0,
	0,
	offsetof(SimpleXmlNode_t142, ___TagName_2),
	offsetof(SimpleXmlNode_t142, ___ParentNode_3),
	offsetof(SimpleXmlNode_t142, ___Children_4),
	offsetof(SimpleXmlNode_t142, ___Attributes_5),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(SimpleXmlReader_t146_StaticFields, ___BEGIN_QUOTE_8),
	offsetof(XmlTest_t147, ___sampleXml_2),
	0,
	0,
	0,
	0,
	0,
	offsetof(XmlVariables_t149, ___xmlFile_7),
	offsetof(XmlVariables_t149, ___varMap_8),
	offsetof(XmlVariables_t149_StaticFields, ___U3CU3Ef__switchU24map1_9),
	offsetof(SwarmItem_t124, ____state_2),
	offsetof(SwarmItem_t124, ____swarmItemManager_3),
	offsetof(SwarmItem_t124, ____thisTransform_4),
	offsetof(SwarmItem_t124, ____prefabIndex_5),
	offsetof(SwarmItem_t124, ____lifeSpanLeft_6),
	offsetof(SwarmItem_t124, ____debugEvents_7),
	offsetof(SwarmItem_t124, ___minimumLifeSpan_8),
	offsetof(SwarmItem_t124, ___maximumLifeSpan_9),
	offsetof(STATE_t150, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(SwarmItemManager_t111, ____item_2),
	offsetof(SwarmItemManager_t111, ____go_3),
	offsetof(SwarmItemManager_t111, ____transform_4),
	offsetof(SwarmItemManager_t111, ____activeParentTransform_5),
	offsetof(SwarmItemManager_t111, ____inactiveParentTransform_6),
	offsetof(SwarmItemManager_t111, ____itemCount_7),
	offsetof(SwarmItemManager_t111, ____prefabItemLists_8),
	offsetof(SwarmItemManager_t111, ___debugEvents_9),
	offsetof(SwarmItemManager_t111, ___itemPrefabs_10),
	offsetof(PrefabItemLists_t151, ___activeItems_0),
	offsetof(PrefabItemLists_t151, ___inactiveItems_1),
	offsetof(PrefabItemLists_t151, ___inactivePruneTimeLeft_2),
	offsetof(PrefabItem_t154, ___prefab_0),
	offsetof(PrefabItem_t154, ___maxItemCount_1),
	offsetof(PrefabItem_t154, ___inactiveThreshold_2),
	offsetof(PrefabItem_t154, ___inactivePruneTimer_3),
	offsetof(PrefabItem_t154, ___inactivePrunePercentage_4),
	offsetof(DispatcherBase_t158, ___taskList_0),
	offsetof(DispatcherBase_t158, ___dataEvent_1),
	offsetof(DispatcherBase_t158, ___TaskSortingSystem_2),
	offsetof(DispatcherBase_t158_StaticFields, ___U3CU3Ef__amU24cache3_3),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	offsetof(Dispatcher_t162_StaticFields, ___mainDispatcher_6),
	0,
	0,
	offsetof(TaskSortingSystem_t164, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(TaskBase_t163, ___Priority_0),
	offsetof(TaskBase_t163, ___abortEvent_1),
	offsetof(TaskBase_t163, ___endedEvent_2),
	offsetof(TaskBase_t163, ___hasStarted_3),
	offsetof(Task_t165, ___action_4),
	0,
	0,
	offsetof(TaskDistributor_t166, ___workerThreads_4),
	offsetof(TaskDistributor_t166_StaticFields, ___mainTaskDistributor_5),
	offsetof(TaskWorker_t168, ___Dispatcher_4),
	offsetof(TaskWorker_t168, ___TaskDistributor_5),
	offsetof(ThreadBase_t169, ___targetDispatcher_0),
	offsetof(ThreadBase_t169, ___thread_1),
	offsetof(ThreadBase_t169, ___exitEvent_2),
	THREAD_STATIC_FIELD_OFFSET,
	offsetof(ActionThread_t171, ___action_4),
	offsetof(EnumeratableActionThread_t173, ___enumeratableAction_4),
	offsetof(UnityThreadHelper_t181_StaticFields, ___instance_2),
	offsetof(UnityThreadHelper_t181, ___dispatcher_3),
	offsetof(UnityThreadHelper_t181, ___taskDistributor_4),
	offsetof(UnityThreadHelper_t181, ___registeredThreads_5),
	offsetof(UnityThreadHelper_t181_StaticFields, ___U3CU3Ef__amU24cache4_6),
	offsetof(U3CCreateThreadU3Ec__AnonStorey12_t175, ___action_0),
	offsetof(U3CCreateThreadU3Ec__AnonStorey13_t176, ___action_0),
	offsetof(U3CCreateThreadU3Ec__AnonStorey14_t177, ___action_0),
	offsetof(U3CCreateThreadU3Ec__AnonStorey15_t178, ___action_0),
	offsetof(U3CCreateThreadU3Ec__AnonStorey16_t180, ___action_0),
	offsetof(BridgeHandler_t185_StaticFields, ___BRIDGE_2),
	offsetof(BridgeHandler_t185_StaticFields, ___SHARE_3),
	offsetof(BridgeHandler_t185_StaticFields, ___SMS_4),
	offsetof(BridgeHandler_t185_StaticFields, ___START_RECORD_5),
	offsetof(BridgeHandler_t185_StaticFields, ___STOP_RECORD_6),
	offsetof(BridgeHandler_t185_StaticFields, ___CANCEL_RECORD_7),
	offsetof(BridgeHandler_t185_StaticFields, ___RECEIVER_8),
	offsetof(BridgeHandler_t185_StaticFields, ___MESSAGE_9),
	offsetof(BridgeHandler_t185, ___nativeCalls_10),
	offsetof(U3COnStartAudioRecordU3Ec__Iterator5_t184, ___U24PC_0),
	offsetof(U3COnStartAudioRecordU3Ec__Iterator5_t184, ___U24current_1),
	offsetof(U3COnStartAudioRecordU3Ec__Iterator5_t184, ___U3CU3Ef__this_2),
	offsetof(EScreens_t188, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(ESubScreens_t189, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(Screens_t190_StaticFields, ___SCREENS_0),
	offsetof(Screens_t190_StaticFields, ___SUB_SCREENS_1),
	offsetof(GameSignals_t193_StaticFields, ___ON_LOAD_TITLE_SCREEN_0),
	offsetof(GameSignals_t193_StaticFields, ___ON_LOAD_SCAN_SCREEN_1),
	offsetof(GameSignals_t193_StaticFields, ___ON_START_SCANNING_2),
	offsetof(GameSignals_t193_StaticFields, ___ON_EXIT_SCANNING_3),
	offsetof(GameSignals_t193_StaticFields, ___ON_TRACKING_DETECTED_4),
	offsetof(GameSignals_t193_StaticFields, ___ON_TRACKING_LOST_5),
	offsetof(GameSignals_t193_StaticFields, ___ON_UPDATE_CAMERA_FOCUS_6),
	offsetof(GameSignals_t193_StaticFields, ___ON_UPDATE_CAMERA_FLASH_7),
	offsetof(GameSignals_t193_StaticFields, ___ON_SHARE_8),
	offsetof(GameSignals_t193_StaticFields, ___ON_SEND_SMS_9),
	offsetof(GameSignals_t193_StaticFields, ___ON_START_AUDIO_RECORD_10),
	offsetof(GameSignals_t193_StaticFields, ___ON_STOP_AUDIO_RECORD_11),
	offsetof(GameSignals_t193_StaticFields, ___ON_CANCEL_AUDIO_RECORD_12),
	offsetof(GameSignals_t193_StaticFields, ___ON_AUDIO_RECOGNIZED_13),
	offsetof(GameSignals_t193_StaticFields, ___ON_REGISTER_USER_SUCCESSFUL_14),
	offsetof(GameSignals_t193_StaticFields, ___ON_REGISTER_RESULT_15),
	offsetof(GameSignals_t193_StaticFields, ___ON_REGISTER_RAFFLE_RESULT_16),
	offsetof(GameSignals_t193_StaticFields, ___ON_RAFFLE_WINNERS_RESULT_17),
	offsetof(GameSignals_t193_StaticFields, ___ON_LOAD_OLD_SCANNER_18),
	offsetof(GameSignals_t193_StaticFields, ___ON_VIDEO_PLAYER_DONE_19),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(PanaloCam_t194, ___fsm_11),
	offsetof(PanaloCam_t194_StaticFields, ___U3CU3Ef__amU24cache1_12),
	offsetof(PanaloCam_t194_StaticFields, ___U3CU3Ef__amU24cache2_13),
	offsetof(PanaloCam_t194_StaticFields, ___U3CU3Ef__amU24cache3_14),
	offsetof(PanaloCam_t194_StaticFields, ___U3CU3Ef__amU24cache4_15),
	offsetof(PanaloCam_t194_StaticFields, ___U3CU3Ef__amU24cache5_16),
	offsetof(PanaloCamSystem_t195, ___version_2),
	offsetof(PanaloCamSystem_t195, ___buildText_3),
	offsetof(Params_t197_StaticFields, ___SCANNED_ID_0),
	offsetof(Params_t197_StaticFields, ___CAMERA_FOCUS_SETTINGS_1),
	offsetof(Params_t197_StaticFields, ___SMS_NUMBER_2),
	offsetof(Params_t197_StaticFields, ___MESSAGE_3),
	offsetof(Params_t197_StaticFields, ___SHARE_SUBJECT_4),
	offsetof(Params_t197_StaticFields, ___SHARE_BODY_5),
	offsetof(Params_t197_StaticFields, ___PROMO_ID_6),
	offsetof(Params_t197_StaticFields, ___RAFFLE_RESULT_7),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(Raffle_t202, ___raffleData_32),
	offsetof(Raffle_t202, ___RegisterUserMessage_33),
	offsetof(U3CProcessRequestU3Ec__Iterator6_t198, ___request_0),
	offsetof(U3CProcessRequestU3Ec__Iterator6_t198, ___handler_1),
	offsetof(U3CProcessRequestU3Ec__Iterator6_t198, ___U24PC_2),
	offsetof(U3CProcessRequestU3Ec__Iterator6_t198, ___U24current_3),
	offsetof(U3CProcessRequestU3Ec__Iterator6_t198, ___U3CU24U3Erequest_4),
	offsetof(U3CProcessRequestU3Ec__Iterator6_t198, ___U3CU24U3Ehandler_5),
	offsetof(U3CRegisterUserU3Ec__AnonStorey17_t201, ___api_0),
	offsetof(U3CRegisterUserU3Ec__AnonStorey17_t201, ___U3CU3Ef__this_1),
	offsetof(U3CGetPlayerInformationU3Ec__AnonStorey18_t203, ___api_0),
	offsetof(U3CGetPlayerInformationU3Ec__AnonStorey18_t203, ___U3CU3Ef__this_1),
	offsetof(U3CRegisterPlayerToPromoU3Ec__AnonStorey19_t204, ___api_0),
	offsetof(U3CRegisterPlayerToPromoU3Ec__AnonStorey19_t204, ___U3CU3Ef__this_1),
	offsetof(U3CGetPromoWinnerU3Ec__AnonStorey1A_t205, ___raffleId_0),
	offsetof(U3CGetPromoWinnerU3Ec__AnonStorey1A_t205, ___api_1),
	offsetof(ERaffleResult_t207, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(RaffleData_t206, ___PlayerId_4),
	offsetof(RaffleData_t206, ___PromoId_5),
	offsetof(RaffleData_t206, ___FirstName_6),
	offsetof(RaffleData_t206, ___LastName_7),
	offsetof(RaffleData_t206, ___MobileNumber_8),
	offsetof(RaffleData_t206, ___CompanyName_9),
	offsetof(RaffleData_t206, ___Gender_10),
	offsetof(RaffleData_t206, ___DeviceId_11),
	offsetof(RaffleData_t206, ___Raffles_12),
	offsetof(RaffleData_t206, ___U3CHasDataU3Ek__BackingField_13),
	offsetof(ScannerHandler_t209, ___targetBehaviour_2),
	offsetof(ScannerHandler_t209, ___trackBehaviour_3),
	offsetof(ScannerHandler_t209, ___isScanning_4),
	offsetof(ScannerHandler_t209, ___flashFlag_5),
	offsetof(ScannerHudRoot_t212, ___focusText_2),
	offsetof(ScannerHudRoot_t212, ___border_3),
	offsetof(ScannerHudRoot_t212, ___focusModeIndex_4),
	offsetof(ScannerHudRoot_t212, ___focusModes_5),
	offsetof(EStatus_t216, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(LoadingSceneRoot_t218, ___window_6),
	offsetof(LoadingSceneRoot_t218, ___errorWindow_7),
	offsetof(LoadingSceneRoot_t218, ___results_8),
	offsetof(LoadingSceneRoot_t218, ___fsm_9),
	offsetof(LoadingSceneRoot_t218_StaticFields, ___U3CU3Ef__amU24cache4_10),
	offsetof(LoadingSceneRoot_t218_StaticFields, ___U3CU3Ef__amU24cache5_11),
	offsetof(LoadingSceneRoot_t218_StaticFields, ___U3CU3Ef__amU24cache6_12),
	offsetof(U3CCheckRaffleResultsU3Ec__Iterator7_t217, ___U3CraffleU3E__0_0),
	offsetof(U3CCheckRaffleResultsU3Ec__Iterator7_t217, ___U3CraffleDataU3E__1_1),
	offsetof(U3CCheckRaffleResultsU3Ec__Iterator7_t217, ___U24PC_2),
	offsetof(U3CCheckRaffleResultsU3Ec__Iterator7_t217, ___U24current_3),
	offsetof(U3CCheckRaffleResultsU3Ec__Iterator7_t217, ___U3CU3Ef__this_4),
	offsetof(U3CCheckRaffleResultsU3Ec__Iterator7_t217_StaticFields, ___U3CU3Ef__amU24cache5_5),
	offsetof(U3CCheckRegisterMessageU3Ec__Iterator8_t220, ___U3CmessageU3E__0_0),
	offsetof(U3CCheckRegisterMessageU3Ec__Iterator8_t220, ___U24PC_1),
	offsetof(U3CCheckRegisterMessageU3Ec__Iterator8_t220, ___U24current_2),
	offsetof(U3CCheckRegisterMessageU3Ec__Iterator8_t220, ___U3CU3Ef__this_3),
	offsetof(U3CCheckRegisterMessageU3Ec__Iterator8_t220_StaticFields, ___U3CU3Ef__amU24cache4_4),
	offsetof(U3CPrepareFsmU3Ec__AnonStorey1B_t222, ___raffleId_0),
	offsetof(U3CPrepareFsmU3Ec__AnonStorey1B_t222, ___U3CU3Ef__this_1),
	offsetof(RaffleEntryRoot_t224, ___button_2),
	offsetof(RaffleEntryRoot_t224, ___label_3),
	offsetof(RegisterSceneRoot_t225, ___firstnameField_2),
	offsetof(RegisterSceneRoot_t225, ___lastnameField_3),
	offsetof(RegisterSceneRoot_t225, ___mobileField_4),
	offsetof(RegisterSceneRoot_t225, ___companyField_5),
	offsetof(RegisterSceneRoot_t225, ___prevFocusValue_6),
	offsetof(RegisterSceneRoot_t225, ___defaultValue_7),
	offsetof(RegisterSceneRoot_t225, ___hasValidFields_8),
	offsetof(AudioRecognizer_t229, ___nativeCalls_2),
	offsetof(TitleSceneRoot_t230, ___usernameField_2),
	offsetof(TitleSceneRoot_t230, ___passwordField_3),
	offsetof(TitleSceneRoot_t230, ___usernameLabelImage_4),
	offsetof(TitleSceneRoot_t230, ___passwordLabelImage_5),
	offsetof(TitleSceneRoot_t230, ___prevFocusValue_6),
	offsetof(TitleSceneRoot_t230, ___defaultValue_7),
	offsetof(TitleSceneRoot_t230, ___defaultLabel_8),
	offsetof(Animator_t236, ___loop_2),
	offsetof(Animator_t236, ___image_3),
	offsetof(Animator_t236, ___frames_4),
	offsetof(U3CPlayAnimationLoopU3Ec__Iterator9_t233, ___delay_0),
	offsetof(U3CPlayAnimationLoopU3Ec__Iterator9_t233, ___U3CU24s_68U3E__0_1),
	offsetof(U3CPlayAnimationLoopU3Ec__Iterator9_t233, ___U3CframeU3E__1_2),
	offsetof(U3CPlayAnimationLoopU3Ec__Iterator9_t233, ___U24PC_3),
	offsetof(U3CPlayAnimationLoopU3Ec__Iterator9_t233, ___U24current_4),
	offsetof(U3CPlayAnimationLoopU3Ec__Iterator9_t233, ___U3CU24U3Edelay_5),
	offsetof(U3CPlayAnimationLoopU3Ec__Iterator9_t233, ___U3CU3Ef__this_6),
	offsetof(AnimatorFrame_t235, ___Duration_0) + sizeof(Object_t),
	offsetof(AnimatorFrame_t235, ___Texture_1) + sizeof(Object_t),
	0,
	0,
	offsetof(Poller_t240, ___duration_4),
	offsetof(U3CStartPollU3Ec__IteratorA_t239, ___U3CraffleU3E__0_0),
	offsetof(U3CStartPollU3Ec__IteratorA_t239, ___U3CraffleDataU3E__1_1),
	offsetof(U3CStartPollU3Ec__IteratorA_t239, ___U24PC_2),
	offsetof(U3CStartPollU3Ec__IteratorA_t239, ___U24current_3),
	offsetof(Window_t223, ___message_2),
	offsetof(Window_t223, ___button_3),
	offsetof(Window_t223, ___body_4),
	offsetof(Window_t223, ___handler_5),
	offsetof(VideoPlayer_t241, ___MOVIE_TEXTURE_PATH_GLOBAL_2),
	offsetof(VideoPlayer_t241, ___MOVIE_TEXTURE_ENG_1_3),
	offsetof(VideoPlayer_t241, ___MOVIE_TEXTURE_ENG_2_4),
	offsetof(VideoPlayer_t241, ___MOVIE_TEXTURE_TAG_1_5),
	offsetof(VideoPlayer_t241, ___MOVIE_TEXTURE_TAG_2_6),
	offsetof(VideoPlayer_t241, ___POSITION_7),
	offsetof(VideoPlayer_t241, ___ready_8),
	offsetof(VideoPlayer_t241, ___player_9),
	offsetof(VideoPlayer_t241, ___audioSource_10),
	offsetof(VideoPlayer_t241, ___guiTexture_11),
	offsetof(VideoPlayer_t241, ___movies_12),
	offsetof(PanaloVideoPlayer_t242, ___EXT_0),
	offsetof(PanaloVideoPlayer_t242, ___movieTexturePath_1),
	offsetof(PanaloVideoPlayer_t242, ___isPlaying_2),
	offsetof(U3CPlayU3Ec__IteratorB_t245, ___movieTexturePath_0),
	offsetof(U3CPlayU3Ec__IteratorB_t245, ___U24PC_1),
	offsetof(U3CPlayU3Ec__IteratorB_t245, ___U24current_2),
	offsetof(U3CPlayU3Ec__IteratorB_t245, ___U3CU24U3EmovieTexturePath_3),
	offsetof(U3CPlayU3Ec__IteratorB_t245, ___U3CU3Ef__this_4),
	offsetof(iTween_t258_StaticFields, ___tweens_2),
	offsetof(iTween_t258_StaticFields, ___cameraFade_3),
	offsetof(iTween_t258, ___id_4),
	offsetof(iTween_t258, ___type_5),
	offsetof(iTween_t258, ___method_6),
	offsetof(iTween_t258, ___easeType_7),
	offsetof(iTween_t258, ___time_8),
	offsetof(iTween_t258, ___delay_9),
	offsetof(iTween_t258, ___loopType_10),
	offsetof(iTween_t258, ___isRunning_11),
	offsetof(iTween_t258, ___isPaused_12),
	offsetof(iTween_t258, ____name_13),
	offsetof(iTween_t258, ___runningTime_14),
	offsetof(iTween_t258, ___percentage_15),
	offsetof(iTween_t258, ___delayStarted_16),
	offsetof(iTween_t258, ___kinematic_17),
	offsetof(iTween_t258, ___isLocal_18),
	offsetof(iTween_t258, ___loop_19),
	offsetof(iTween_t258, ___reverse_20),
	offsetof(iTween_t258, ___wasPaused_21),
	offsetof(iTween_t258, ___physics_22),
	offsetof(iTween_t258, ___tweenArguments_23),
	offsetof(iTween_t258, ___space_24),
	offsetof(iTween_t258, ___ease_25),
	offsetof(iTween_t258, ___apply_26),
	offsetof(iTween_t258, ___audioSource_27),
	offsetof(iTween_t258, ___vector3s_28),
	offsetof(iTween_t258, ___vector2s_29),
	offsetof(iTween_t258, ___colors_30),
	offsetof(iTween_t258, ___floats_31),
	offsetof(iTween_t258, ___rects_32),
	offsetof(iTween_t258, ___path_33),
	offsetof(iTween_t258, ___preUpdate_34),
	offsetof(iTween_t258, ___postUpdate_35),
	offsetof(iTween_t258, ___namedcolorvalue_36),
	offsetof(iTween_t258, ___lastRealTime_37),
	offsetof(iTween_t258, ___useRealTime_38),
	offsetof(iTween_t258, ___thisTransform_39),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24map2_40),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24map3_41),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24map4_42),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24map5_43),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24map6_44),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24map7_45),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24map8_46),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24map9_47),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24mapA_48),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24mapB_49),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24mapC_50),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24mapD_51),
	offsetof(iTween_t258_StaticFields, ___U3CU3Ef__switchU24mapE_52),
	offsetof(EaseType_t249, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(LoopType_t250, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(NamedValueColor_t251, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(Defaults_t252_StaticFields, ___time_0),
	offsetof(Defaults_t252_StaticFields, ___delay_1),
	offsetof(Defaults_t252_StaticFields, ___namedColorValue_2),
	offsetof(Defaults_t252_StaticFields, ___loopType_3),
	offsetof(Defaults_t252_StaticFields, ___easeType_4),
	offsetof(Defaults_t252_StaticFields, ___lookSpeed_5),
	offsetof(Defaults_t252_StaticFields, ___isLocal_6),
	offsetof(Defaults_t252_StaticFields, ___space_7),
	offsetof(Defaults_t252_StaticFields, ___orientToPath_8),
	offsetof(Defaults_t252_StaticFields, ___color_9),
	offsetof(Defaults_t252_StaticFields, ___updateTimePercentage_10),
	offsetof(Defaults_t252_StaticFields, ___updateTime_11),
	offsetof(Defaults_t252_StaticFields, ___cameraFadeDepth_12),
	offsetof(Defaults_t252_StaticFields, ___lookAhead_13),
	offsetof(Defaults_t252_StaticFields, ___useRealTime_14),
	offsetof(Defaults_t252_StaticFields, ___up_15),
	offsetof(CRSpline_t253, ___pts_0),
	offsetof(U3CTweenDelayU3Ec__IteratorC_t257, ___U24PC_0),
	offsetof(U3CTweenDelayU3Ec__IteratorC_t257, ___U24current_1),
	offsetof(U3CTweenDelayU3Ec__IteratorC_t257, ___U3CU3Ef__this_2),
	offsetof(U3CTweenRestartU3Ec__IteratorD_t259, ___U24PC_0),
	offsetof(U3CTweenRestartU3Ec__IteratorD_t259, ___U24current_1),
	offsetof(U3CTweenRestartU3Ec__IteratorD_t259, ___U3CU3Ef__this_2),
	offsetof(U3CStartU3Ec__IteratorE_t260, ___U24PC_0),
	offsetof(U3CStartU3Ec__IteratorE_t260, ___U24current_1),
	offsetof(U3CStartU3Ec__IteratorE_t260, ___U3CU3Ef__this_2),
	0,
	offsetof(DefaultInitializationErrorHandler_t276, ___mErrorText_3),
	offsetof(DefaultInitializationErrorHandler_t276, ___mErrorOccurred_4),
	offsetof(DefaultSmartTerrainEventHandler_t277, ___mReconstructionBehaviour_2),
	offsetof(DefaultSmartTerrainEventHandler_t277, ___PropTemplate_3),
	offsetof(DefaultSmartTerrainEventHandler_t277, ___SurfaceTemplate_4),
	offsetof(DefaultTrackableEventHandler_t281, ___mTrackableBehaviour_2),
	0,
	offsetof(GLErrorHandler_t282_StaticFields, ___mErrorText_3),
	offsetof(GLErrorHandler_t282_StaticFields, ___mErrorOccurred_4),
	0,
	0,
	offsetof(AndroidUnityPlayer_t286, ___mScreenOrientation_2),
	offsetof(AndroidUnityPlayer_t286, ___mJavaScreenOrientation_3),
	offsetof(AndroidUnityPlayer_t286, ___mFramesSinceLastOrientationReset_4),
	offsetof(AndroidUnityPlayer_t286, ___mFramesSinceLastJavaOrientationCheck_5),
	offsetof(IOSUnityPlayer_t288, ___mScreenOrientation_0),
	offsetof(VuforiaBehaviour_t320_StaticFields, ___mVuforiaBehaviour_48),
	offsetof(WireframeBehaviour_t324, ___mLineMaterial_2),
	offsetof(WireframeBehaviour_t324, ___ShowLines_3),
	offsetof(WireframeBehaviour_t324, ___LineColor_4),
	offsetof(WireframeTrackableEventHandler_t325, ___mTrackableBehaviour_2),
	offsetof(U3CPrivateImplementationDetailsU3E_t334_StaticFields, ___U24U24fieldU2D0_0),
	offsetof(EventHandle_t475, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(EventSystem_t476, ___m_SystemInputModules_2),
	offsetof(EventSystem_t476, ___m_CurrentInputModule_3),
	offsetof(EventSystem_t476, ___m_FirstSelected_4),
	offsetof(EventSystem_t476, ___m_sendNavigationEvents_5),
	offsetof(EventSystem_t476, ___m_DragThreshold_6),
	offsetof(EventSystem_t476, ___m_CurrentSelected_7),
	offsetof(EventSystem_t476, ___m_SelectionGuard_8),
	offsetof(EventSystem_t476, ___m_DummyData_9),
	offsetof(EventSystem_t476_StaticFields, ___s_RaycastComparer_10),
	offsetof(EventSystem_t476_StaticFields, ___U3CcurrentU3Ek__BackingField_11),
	offsetof(EventTrigger_t485, ___m_Delegates_2),
	offsetof(EventTrigger_t485, ___delegates_3),
	offsetof(Entry_t484, ___eventID_0),
	offsetof(Entry_t484, ___callback_1),
	offsetof(EventTriggerType_t487, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(ExecuteEvents_t488_StaticFields, ___s_PointerEnterHandler_0),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_PointerExitHandler_1),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_PointerDownHandler_2),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_PointerUpHandler_3),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_PointerClickHandler_4),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_InitializePotentialDragHandler_5),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_BeginDragHandler_6),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_DragHandler_7),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_EndDragHandler_8),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_DropHandler_9),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_ScrollHandler_10),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_UpdateSelectedHandler_11),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_SelectHandler_12),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_DeselectHandler_13),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_MoveHandler_14),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_SubmitHandler_15),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_CancelHandler_16),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_HandlerListPool_17),
	offsetof(ExecuteEvents_t488_StaticFields, ___s_InternalTransformList_18),
	offsetof(ExecuteEvents_t488_StaticFields, ___U3CU3Ef__amU24cache13_19),
	offsetof(MoveDirection_t509, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(RaycasterManager_t510_StaticFields, ___s_Raycasters_0),
	offsetof(RaycastResult_t512, ___m_GameObject_0) + sizeof(Object_t),
	offsetof(RaycastResult_t512, ___module_1) + sizeof(Object_t),
	offsetof(RaycastResult_t512, ___distance_2) + sizeof(Object_t),
	offsetof(RaycastResult_t512, ___index_3) + sizeof(Object_t),
	offsetof(RaycastResult_t512, ___depth_4) + sizeof(Object_t),
	offsetof(RaycastResult_t512, ___sortingLayer_5) + sizeof(Object_t),
	offsetof(RaycastResult_t512, ___sortingOrder_6) + sizeof(Object_t),
	offsetof(RaycastResult_t512, ___worldPosition_7) + sizeof(Object_t),
	offsetof(RaycastResult_t512, ___worldNormal_8) + sizeof(Object_t),
	offsetof(RaycastResult_t512, ___screenPosition_9) + sizeof(Object_t),
	offsetof(AxisEventData_t514, ___U3CmoveVectorU3Ek__BackingField_2),
	offsetof(AxisEventData_t514, ___U3CmoveDirU3Ek__BackingField_3),
	offsetof(BaseEventData_t480, ___m_EventSystem_0),
	offsetof(BaseEventData_t480, ___m_Used_1),
	offsetof(PointerEventData_t517, ___m_PointerPress_2),
	offsetof(PointerEventData_t517, ___hovered_3),
	offsetof(PointerEventData_t517, ___U3CpointerEnterU3Ek__BackingField_4),
	offsetof(PointerEventData_t517, ___U3ClastPressU3Ek__BackingField_5),
	offsetof(PointerEventData_t517, ___U3CrawPointerPressU3Ek__BackingField_6),
	offsetof(PointerEventData_t517, ___U3CpointerDragU3Ek__BackingField_7),
	offsetof(PointerEventData_t517, ___U3CpointerCurrentRaycastU3Ek__BackingField_8),
	offsetof(PointerEventData_t517, ___U3CpointerPressRaycastU3Ek__BackingField_9),
	offsetof(PointerEventData_t517, ___U3CeligibleForClickU3Ek__BackingField_10),
	offsetof(PointerEventData_t517, ___U3CpointerIdU3Ek__BackingField_11),
	offsetof(PointerEventData_t517, ___U3CpositionU3Ek__BackingField_12),
	offsetof(PointerEventData_t517, ___U3CdeltaU3Ek__BackingField_13),
	offsetof(PointerEventData_t517, ___U3CpressPositionU3Ek__BackingField_14),
	offsetof(PointerEventData_t517, ___U3CworldPositionU3Ek__BackingField_15),
	offsetof(PointerEventData_t517, ___U3CworldNormalU3Ek__BackingField_16),
	offsetof(PointerEventData_t517, ___U3CclickTimeU3Ek__BackingField_17),
	offsetof(PointerEventData_t517, ___U3CclickCountU3Ek__BackingField_18),
	offsetof(PointerEventData_t517, ___U3CscrollDeltaU3Ek__BackingField_19),
	offsetof(PointerEventData_t517, ___U3CuseDragThresholdU3Ek__BackingField_20),
	offsetof(PointerEventData_t517, ___U3CdraggingU3Ek__BackingField_21),
	offsetof(PointerEventData_t517, ___U3CbuttonU3Ek__BackingField_22),
	offsetof(InputButton_t515, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(FramePressState_t516, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(BaseInputModule_t479, ___m_RaycastResultCache_2),
	offsetof(BaseInputModule_t479, ___m_AxisEventData_3),
	offsetof(BaseInputModule_t479, ___m_EventSystem_4),
	offsetof(BaseInputModule_t479, ___m_BaseEventData_5),
	0,
	0,
	0,
	0,
	offsetof(PointerInputModule_t524, ___m_PointerData_10),
	offsetof(PointerInputModule_t524, ___m_MouseState_11),
	offsetof(ButtonState_t520, ___m_Button_0),
	offsetof(ButtonState_t520, ___m_EventData_1),
	offsetof(MouseState_t522, ___m_TrackedButtons_0),
	offsetof(MouseButtonEventData_t521, ___buttonState_0),
	offsetof(MouseButtonEventData_t521, ___buttonData_1),
	offsetof(StandaloneInputModule_t527, ___m_PrevActionTime_12),
	offsetof(StandaloneInputModule_t527, ___m_LastMoveVector_13),
	offsetof(StandaloneInputModule_t527, ___m_ConsecutiveMoveCount_14),
	offsetof(StandaloneInputModule_t527, ___m_LastMousePosition_15),
	offsetof(StandaloneInputModule_t527, ___m_MousePosition_16),
	offsetof(StandaloneInputModule_t527, ___m_HorizontalAxis_17),
	offsetof(StandaloneInputModule_t527, ___m_VerticalAxis_18),
	offsetof(StandaloneInputModule_t527, ___m_SubmitButton_19),
	offsetof(StandaloneInputModule_t527, ___m_CancelButton_20),
	offsetof(StandaloneInputModule_t527, ___m_InputActionsPerSecond_21),
	offsetof(StandaloneInputModule_t527, ___m_RepeatDelay_22),
	offsetof(StandaloneInputModule_t527, ___m_ForceModuleActive_23),
	offsetof(InputMode_t526, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(TouchInputModule_t528, ___m_LastMousePosition_12),
	offsetof(TouchInputModule_t528, ___m_MousePosition_13),
	offsetof(TouchInputModule_t528, ___m_ForceModuleActive_14),
	0,
	offsetof(PhysicsRaycaster_t530, ___m_EventCamera_3),
	offsetof(PhysicsRaycaster_t530, ___m_EventMask_4),
	offsetof(PhysicsRaycaster_t530_StaticFields, ___U3CU3Ef__amU24cache2_5),
	offsetof(ColorTween_t536, ___m_Target_0) + sizeof(Object_t),
	offsetof(ColorTween_t536, ___m_StartColor_1) + sizeof(Object_t),
	offsetof(ColorTween_t536, ___m_TargetColor_2) + sizeof(Object_t),
	offsetof(ColorTween_t536, ___m_TweenMode_3) + sizeof(Object_t),
	offsetof(ColorTween_t536, ___m_Duration_4) + sizeof(Object_t),
	offsetof(ColorTween_t536, ___m_IgnoreTimeScale_5) + sizeof(Object_t),
	offsetof(ColorTweenMode_t533, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(FloatTween_t539, ___m_Target_0) + sizeof(Object_t),
	offsetof(FloatTween_t539, ___m_StartValue_1) + sizeof(Object_t),
	offsetof(FloatTween_t539, ___m_TargetValue_2) + sizeof(Object_t),
	offsetof(FloatTween_t539, ___m_Duration_3) + sizeof(Object_t),
	offsetof(FloatTween_t539, ___m_IgnoreTimeScale_4) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(AnimationTriggers_t540, ___m_NormalTrigger_4),
	offsetof(AnimationTriggers_t540, ___m_HighlightedTrigger_5),
	offsetof(AnimationTriggers_t540, ___m_PressedTrigger_6),
	offsetof(AnimationTriggers_t540, ___m_DisabledTrigger_7),
	offsetof(Button_t544, ___m_OnClick_16),
	offsetof(U3COnFinishSubmitU3Ec__Iterator1_t543, ___U3CfadeTimeU3E__0_0),
	offsetof(U3COnFinishSubmitU3Ec__Iterator1_t543, ___U3CelapsedTimeU3E__1_1),
	offsetof(U3COnFinishSubmitU3Ec__Iterator1_t543, ___U24PC_2),
	offsetof(U3COnFinishSubmitU3Ec__Iterator1_t543, ___U24current_3),
	offsetof(U3COnFinishSubmitU3Ec__Iterator1_t543, ___U3CU3Ef__this_4),
	offsetof(CanvasUpdate_t546, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(CanvasUpdateRegistry_t547_StaticFields, ___s_Instance_0),
	offsetof(CanvasUpdateRegistry_t547, ___m_PerformingLayoutUpdate_1),
	offsetof(CanvasUpdateRegistry_t547, ___m_PerformingGraphicUpdate_2),
	offsetof(CanvasUpdateRegistry_t547, ___m_LayoutRebuildQueue_3),
	offsetof(CanvasUpdateRegistry_t547, ___m_GraphicRebuildQueue_4),
	offsetof(CanvasUpdateRegistry_t547_StaticFields, ___s_SortLayoutFunction_5),
	offsetof(CanvasUpdateRegistry_t547_StaticFields, ___U3CU3Ef__amU24cache6_6),
	offsetof(CanvasUpdateRegistry_t547_StaticFields, ___U3CU3Ef__amU24cache7_7),
	offsetof(ColorBlock_t551, ___m_NormalColor_0) + sizeof(Object_t),
	offsetof(ColorBlock_t551, ___m_HighlightedColor_1) + sizeof(Object_t),
	offsetof(ColorBlock_t551, ___m_PressedColor_2) + sizeof(Object_t),
	offsetof(ColorBlock_t551, ___m_DisabledColor_3) + sizeof(Object_t),
	offsetof(ColorBlock_t551, ___m_ColorMultiplier_4) + sizeof(Object_t),
	offsetof(ColorBlock_t551, ___m_FadeDuration_5) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(DefaultControls_t554_StaticFields, ___s_ThickElementSize_3),
	offsetof(DefaultControls_t554_StaticFields, ___s_ThinElementSize_4),
	offsetof(DefaultControls_t554_StaticFields, ___s_ImageElementSize_5),
	offsetof(DefaultControls_t554_StaticFields, ___s_DefaultSelectableColor_6),
	offsetof(DefaultControls_t554_StaticFields, ___s_PanelColor_7),
	offsetof(DefaultControls_t554_StaticFields, ___s_TextColor_8),
	offsetof(Resources_t552, ___standard_0) + sizeof(Object_t),
	offsetof(Resources_t552, ___background_1) + sizeof(Object_t),
	offsetof(Resources_t552, ___inputField_2) + sizeof(Object_t),
	offsetof(Resources_t552, ___knob_3) + sizeof(Object_t),
	offsetof(Resources_t552, ___checkmark_4) + sizeof(Object_t),
	offsetof(Resources_t552, ___dropdown_5) + sizeof(Object_t),
	offsetof(Resources_t552, ___mask_6) + sizeof(Object_t),
	offsetof(Dropdown_t564, ___m_Template_16),
	offsetof(Dropdown_t564, ___m_CaptionText_17),
	offsetof(Dropdown_t564, ___m_CaptionImage_18),
	offsetof(Dropdown_t564, ___m_ItemText_19),
	offsetof(Dropdown_t564, ___m_ItemImage_20),
	offsetof(Dropdown_t564, ___m_Value_21),
	offsetof(Dropdown_t564, ___m_Options_22),
	offsetof(Dropdown_t564, ___m_OnValueChanged_23),
	offsetof(Dropdown_t564, ___m_Dropdown_24),
	offsetof(Dropdown_t564, ___m_Blocker_25),
	offsetof(Dropdown_t564, ___m_Items_26),
	offsetof(Dropdown_t564, ___m_AlphaTweenRunner_27),
	offsetof(Dropdown_t564, ___validTemplate_28),
	offsetof(DropdownItem_t555, ___m_Text_2),
	offsetof(DropdownItem_t555, ___m_Image_3),
	offsetof(DropdownItem_t555, ___m_RectTransform_4),
	offsetof(DropdownItem_t555, ___m_Toggle_5),
	offsetof(OptionData_t558, ___m_Text_0),
	offsetof(OptionData_t558, ___m_Image_1),
	offsetof(OptionDataList_t559, ___m_Options_0),
	offsetof(U3CDelayedDestroyDropdownListU3Ec__Iterator2_t563, ___delay_0),
	offsetof(U3CDelayedDestroyDropdownListU3Ec__Iterator2_t563, ___U3CiU3E__0_1),
	offsetof(U3CDelayedDestroyDropdownListU3Ec__Iterator2_t563, ___U24PC_2),
	offsetof(U3CDelayedDestroyDropdownListU3Ec__Iterator2_t563, ___U24current_3),
	offsetof(U3CDelayedDestroyDropdownListU3Ec__Iterator2_t563, ___U3CU24U3Edelay_4),
	offsetof(U3CDelayedDestroyDropdownListU3Ec__Iterator2_t563, ___U3CU3Ef__this_5),
	offsetof(U3CShowU3Ec__AnonStorey6_t565, ___item_0),
	offsetof(U3CShowU3Ec__AnonStorey6_t565, ___U3CU3Ef__this_1),
	offsetof(FontData_t568, ___m_Font_0),
	offsetof(FontData_t568, ___m_FontSize_1),
	offsetof(FontData_t568, ___m_FontStyle_2),
	offsetof(FontData_t568, ___m_BestFit_3),
	offsetof(FontData_t568, ___m_MinSize_4),
	offsetof(FontData_t568, ___m_MaxSize_5),
	offsetof(FontData_t568, ___m_Alignment_6),
	offsetof(FontData_t568, ___m_RichText_7),
	offsetof(FontData_t568, ___m_HorizontalOverflow_8),
	offsetof(FontData_t568, ___m_VerticalOverflow_9),
	offsetof(FontData_t568, ___m_LineSpacing_10),
	offsetof(FontUpdateTracker_t570_StaticFields, ___m_Tracked_0),
	offsetof(Graphic_t572_StaticFields, ___s_DefaultUI_2),
	offsetof(Graphic_t572_StaticFields, ___s_WhiteTexture_3),
	offsetof(Graphic_t572, ___m_Material_4),
	offsetof(Graphic_t572, ___m_Color_5),
	offsetof(Graphic_t572, ___m_RaycastTarget_6),
	offsetof(Graphic_t572, ___m_RectTransform_7),
	offsetof(Graphic_t572, ___m_CanvasRender_8),
	offsetof(Graphic_t572, ___m_Canvas_9),
	offsetof(Graphic_t572, ___m_VertsDirty_10),
	offsetof(Graphic_t572, ___m_MaterialDirty_11),
	offsetof(Graphic_t572, ___m_OnDirtyLayoutCallback_12),
	offsetof(Graphic_t572, ___m_OnDirtyVertsCallback_13),
	offsetof(Graphic_t572, ___m_OnDirtyMaterialCallback_14),
	offsetof(Graphic_t572_StaticFields, ___s_Mesh_15),
	offsetof(Graphic_t572, ___m_ColorTweenRunner_16),
	0,
	offsetof(GraphicRaycaster_t578, ___m_IgnoreReversedGraphics_3),
	offsetof(GraphicRaycaster_t578, ___m_BlockingObjects_4),
	offsetof(GraphicRaycaster_t578, ___m_BlockingMask_5),
	offsetof(GraphicRaycaster_t578, ___m_Canvas_6),
	offsetof(GraphicRaycaster_t578, ___m_RaycastResults_7),
	offsetof(GraphicRaycaster_t578_StaticFields, ___s_SortedGraphics_8),
	offsetof(GraphicRaycaster_t578_StaticFields, ___U3CU3Ef__amU24cache6_9),
	offsetof(BlockingObjects_t577, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(GraphicRegistry_t581_StaticFields, ___s_Instance_0),
	offsetof(GraphicRegistry_t581, ___m_Graphics_1),
	offsetof(GraphicRegistry_t581_StaticFields, ___s_EmptyList_2),
	offsetof(Image_t213, ___m_Sprite_26),
	offsetof(Image_t213, ___m_OverrideSprite_27),
	offsetof(Image_t213, ___m_Type_28),
	offsetof(Image_t213, ___m_PreserveAspect_29),
	offsetof(Image_t213, ___m_FillCenter_30),
	offsetof(Image_t213, ___m_FillMethod_31),
	offsetof(Image_t213, ___m_FillAmount_32),
	offsetof(Image_t213, ___m_FillClockwise_33),
	offsetof(Image_t213, ___m_FillOrigin_34),
	offsetof(Image_t213, ___m_EventAlphaThreshold_35),
	offsetof(Image_t213_StaticFields, ___s_VertScratch_36),
	offsetof(Image_t213_StaticFields, ___s_UVScratch_37),
	offsetof(Image_t213_StaticFields, ___s_Xy_38),
	offsetof(Image_t213_StaticFields, ___s_Uv_39),
	offsetof(Type_t583, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(FillMethod_t584, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(OriginHorizontal_t585, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(OriginVertical_t586, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(Origin90_t587, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(Origin180_t588, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(Origin360_t589, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(InputField_t226_StaticFields, ___m_Keyboard_19),
	offsetof(InputField_t226_StaticFields, ___kSeparators_20),
	offsetof(InputField_t226, ___m_TextComponent_21),
	offsetof(InputField_t226, ___m_Placeholder_22),
	offsetof(InputField_t226, ___m_ContentType_23),
	offsetof(InputField_t226, ___m_InputType_24),
	offsetof(InputField_t226, ___m_AsteriskChar_25),
	offsetof(InputField_t226, ___m_KeyboardType_26),
	offsetof(InputField_t226, ___m_LineType_27),
	offsetof(InputField_t226, ___m_HideMobileInput_28),
	offsetof(InputField_t226, ___m_CharacterValidation_29),
	offsetof(InputField_t226, ___m_CharacterLimit_30),
	offsetof(InputField_t226, ___m_EndEdit_31),
	offsetof(InputField_t226, ___m_OnValueChange_32),
	offsetof(InputField_t226, ___m_OnValidateInput_33),
	offsetof(InputField_t226, ___m_SelectionColor_34),
	offsetof(InputField_t226, ___m_Text_35),
	offsetof(InputField_t226, ___m_CaretBlinkRate_36),
	offsetof(InputField_t226, ___m_CaretPosition_37),
	offsetof(InputField_t226, ___m_CaretSelectPosition_38),
	offsetof(InputField_t226, ___caretRectTrans_39),
	offsetof(InputField_t226, ___m_CursorVerts_40),
	offsetof(InputField_t226, ___m_InputTextCache_41),
	offsetof(InputField_t226, ___m_CachedInputRenderer_42),
	offsetof(InputField_t226, ___m_PreventFontCallback_43),
	offsetof(InputField_t226, ___m_Mesh_44),
	offsetof(InputField_t226, ___m_AllowInput_45),
	offsetof(InputField_t226, ___m_ShouldActivateNextUpdate_46),
	offsetof(InputField_t226, ___m_UpdateDrag_47),
	offsetof(InputField_t226, ___m_DragPositionOutOfBounds_48),
	offsetof(InputField_t226, ___m_CaretVisible_49),
	offsetof(InputField_t226, ___m_BlinkCoroutine_50),
	offsetof(InputField_t226, ___m_BlinkStartTime_51),
	offsetof(InputField_t226, ___m_DrawStart_52),
	offsetof(InputField_t226, ___m_DrawEnd_53),
	offsetof(InputField_t226, ___m_DragCoroutine_54),
	offsetof(InputField_t226, ___m_OriginalText_55),
	offsetof(InputField_t226, ___m_WasCanceled_56),
	offsetof(InputField_t226, ___m_HasDoneFocusTransition_57),
	offsetof(InputField_t226, ___m_ProcessingEvent_58),
	offsetof(InputField_t226_StaticFields, ___U3CU3Ef__switchU24map0_59),
	offsetof(ContentType_t591, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(InputType_t592, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(CharacterValidation_t593, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(LineType_t594, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(EditState_t598, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(U3CCaretBlinkU3Ec__Iterator3_t600, ___U3CblinkPeriodU3E__0_0),
	offsetof(U3CCaretBlinkU3Ec__Iterator3_t600, ___U3CblinkStateU3E__1_1),
	offsetof(U3CCaretBlinkU3Ec__Iterator3_t600, ___U24PC_2),
	offsetof(U3CCaretBlinkU3Ec__Iterator3_t600, ___U24current_3),
	offsetof(U3CCaretBlinkU3Ec__Iterator3_t600, ___U3CU3Ef__this_4),
	offsetof(U3CMouseDragOutsideRectU3Ec__Iterator4_t601, ___eventData_0),
	offsetof(U3CMouseDragOutsideRectU3Ec__Iterator4_t601, ___U3ClocalMousePosU3E__0_1),
	offsetof(U3CMouseDragOutsideRectU3Ec__Iterator4_t601, ___U3CrectU3E__1_2),
	offsetof(U3CMouseDragOutsideRectU3Ec__Iterator4_t601, ___U3CdelayU3E__2_3),
	offsetof(U3CMouseDragOutsideRectU3Ec__Iterator4_t601, ___U24PC_4),
	offsetof(U3CMouseDragOutsideRectU3Ec__Iterator4_t601, ___U24current_5),
	offsetof(U3CMouseDragOutsideRectU3Ec__Iterator4_t601, ___U3CU24U3EeventData_6),
	offsetof(U3CMouseDragOutsideRectU3Ec__Iterator4_t601, ___U3CU3Ef__this_7),
	offsetof(Mask_t606, ___m_RectTransform_2),
	offsetof(Mask_t606, ___m_ShowMaskGraphic_3),
	offsetof(Mask_t606, ___m_Graphic_4),
	offsetof(Mask_t606, ___m_MaskMaterial_5),
	offsetof(Mask_t606, ___m_UnmaskMaterial_6),
	offsetof(MaskableGraphic_t590, ___m_ShouldRecalculateStencil_17),
	offsetof(MaskableGraphic_t590, ___m_MaskMaterial_18),
	offsetof(MaskableGraphic_t590, ___m_ParentMask_19),
	offsetof(MaskableGraphic_t590, ___m_Maskable_20),
	offsetof(MaskableGraphic_t590, ___m_IncludeForMasking_21),
	offsetof(MaskableGraphic_t590, ___m_OnCullStateChanged_22),
	offsetof(MaskableGraphic_t590, ___m_ShouldRecalculate_23),
	offsetof(MaskableGraphic_t590, ___m_StencilValue_24),
	offsetof(MaskableGraphic_t590, ___m_Corners_25),
	offsetof(Navigation_t613, ___m_Mode_0) + sizeof(Object_t),
	offsetof(Navigation_t613, ___m_SelectOnUp_1) + sizeof(Object_t),
	offsetof(Navigation_t613, ___m_SelectOnDown_2) + sizeof(Object_t),
	offsetof(Navigation_t613, ___m_SelectOnLeft_3) + sizeof(Object_t),
	offsetof(Navigation_t613, ___m_SelectOnRight_4) + sizeof(Object_t),
	offsetof(Mode_t612, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(RawImage_t237, ___m_Texture_26),
	offsetof(RawImage_t237, ___m_UVRect_27),
	offsetof(RectMask2D_t609, ___m_VertexClipper_2),
	offsetof(RectMask2D_t609, ___m_RectTransform_3),
	offsetof(RectMask2D_t609, ___m_ClipTargets_4),
	offsetof(RectMask2D_t609, ___m_ShouldRecalculateClipRects_5),
	offsetof(RectMask2D_t609, ___m_Clippers_6),
	offsetof(RectMask2D_t609, ___m_LastClipRectCanvasSpace_7),
	offsetof(RectMask2D_t609, ___m_LastClipRectValid_8),
	offsetof(Scrollbar_t621, ___m_HandleRect_16),
	offsetof(Scrollbar_t621, ___m_Direction_17),
	offsetof(Scrollbar_t621, ___m_Value_18),
	offsetof(Scrollbar_t621, ___m_Size_19),
	offsetof(Scrollbar_t621, ___m_NumberOfSteps_20),
	offsetof(Scrollbar_t621, ___m_OnValueChanged_21),
	offsetof(Scrollbar_t621, ___m_ContainerRect_22),
	offsetof(Scrollbar_t621, ___m_Offset_23),
	offsetof(Scrollbar_t621, ___m_Tracker_24),
	offsetof(Scrollbar_t621, ___m_PointerDownRepeat_25),
	offsetof(Scrollbar_t621, ___isPointerDownAndNotDragging_26),
	offsetof(Direction_t617, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(Axis_t619, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(U3CClickRepeatU3Ec__Iterator5_t620, ___eventData_0),
	offsetof(U3CClickRepeatU3Ec__Iterator5_t620, ___U3ClocalMousePosU3E__0_1),
	offsetof(U3CClickRepeatU3Ec__Iterator5_t620, ___U3CaxisCoordinateU3E__1_2),
	offsetof(U3CClickRepeatU3Ec__Iterator5_t620, ___U24PC_3),
	offsetof(U3CClickRepeatU3Ec__Iterator5_t620, ___U24current_4),
	offsetof(U3CClickRepeatU3Ec__Iterator5_t620, ___U3CU24U3EeventData_5),
	offsetof(U3CClickRepeatU3Ec__Iterator5_t620, ___U3CU3Ef__this_6),
	offsetof(ScrollRect_t627, ___m_Content_2),
	offsetof(ScrollRect_t627, ___m_Horizontal_3),
	offsetof(ScrollRect_t627, ___m_Vertical_4),
	offsetof(ScrollRect_t627, ___m_MovementType_5),
	offsetof(ScrollRect_t627, ___m_Elasticity_6),
	offsetof(ScrollRect_t627, ___m_Inertia_7),
	offsetof(ScrollRect_t627, ___m_DecelerationRate_8),
	offsetof(ScrollRect_t627, ___m_ScrollSensitivity_9),
	offsetof(ScrollRect_t627, ___m_Viewport_10),
	offsetof(ScrollRect_t627, ___m_HorizontalScrollbar_11),
	offsetof(ScrollRect_t627, ___m_VerticalScrollbar_12),
	offsetof(ScrollRect_t627, ___m_HorizontalScrollbarVisibility_13),
	offsetof(ScrollRect_t627, ___m_VerticalScrollbarVisibility_14),
	offsetof(ScrollRect_t627, ___m_HorizontalScrollbarSpacing_15),
	offsetof(ScrollRect_t627, ___m_VerticalScrollbarSpacing_16),
	offsetof(ScrollRect_t627, ___m_OnValueChanged_17),
	offsetof(ScrollRect_t627, ___m_PointerStartLocalCursor_18),
	offsetof(ScrollRect_t627, ___m_ContentStartPosition_19),
	offsetof(ScrollRect_t627, ___m_ViewRect_20),
	offsetof(ScrollRect_t627, ___m_ContentBounds_21),
	offsetof(ScrollRect_t627, ___m_ViewBounds_22),
	offsetof(ScrollRect_t627, ___m_Velocity_23),
	offsetof(ScrollRect_t627, ___m_Dragging_24),
	offsetof(ScrollRect_t627, ___m_PrevPosition_25),
	offsetof(ScrollRect_t627, ___m_PrevContentBounds_26),
	offsetof(ScrollRect_t627, ___m_PrevViewBounds_27),
	offsetof(ScrollRect_t627, ___m_HasRebuiltLayout_28),
	offsetof(ScrollRect_t627, ___m_HSliderExpand_29),
	offsetof(ScrollRect_t627, ___m_VSliderExpand_30),
	offsetof(ScrollRect_t627, ___m_HSliderHeight_31),
	offsetof(ScrollRect_t627, ___m_VSliderWidth_32),
	offsetof(ScrollRect_t627, ___m_Rect_33),
	offsetof(ScrollRect_t627, ___m_HorizontalScrollbarRect_34),
	offsetof(ScrollRect_t627, ___m_VerticalScrollbarRect_35),
	offsetof(ScrollRect_t627, ___m_Tracker_36),
	offsetof(ScrollRect_t627, ___m_Corners_37),
	offsetof(MovementType_t623, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(ScrollbarVisibility_t624, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(Selectable_t545_StaticFields, ___s_List_2),
	offsetof(Selectable_t545, ___m_Navigation_3),
	offsetof(Selectable_t545, ___m_Transition_4),
	offsetof(Selectable_t545, ___m_Colors_5),
	offsetof(Selectable_t545, ___m_SpriteState_6),
	offsetof(Selectable_t545, ___m_AnimationTriggers_7),
	offsetof(Selectable_t545, ___m_Interactable_8),
	offsetof(Selectable_t545, ___m_TargetGraphic_9),
	offsetof(Selectable_t545, ___m_GroupsAllowInteraction_10),
	offsetof(Selectable_t545, ___m_CurrentSelectionState_11),
	offsetof(Selectable_t545, ___m_CanvasGroupCache_12),
	offsetof(Selectable_t545, ___U3CisPointerInsideU3Ek__BackingField_13),
	offsetof(Selectable_t545, ___U3CisPointerDownU3Ek__BackingField_14),
	offsetof(Selectable_t545, ___U3ChasSelectionU3Ek__BackingField_15),
	offsetof(Transition_t628, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(SelectionState_t629, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(Slider_t637, ___m_FillRect_16),
	offsetof(Slider_t637, ___m_HandleRect_17),
	offsetof(Slider_t637, ___m_Direction_18),
	offsetof(Slider_t637, ___m_MinValue_19),
	offsetof(Slider_t637, ___m_MaxValue_20),
	offsetof(Slider_t637, ___m_WholeNumbers_21),
	offsetof(Slider_t637, ___m_Value_22),
	offsetof(Slider_t637, ___m_OnValueChanged_23),
	offsetof(Slider_t637, ___m_FillImage_24),
	offsetof(Slider_t637, ___m_FillTransform_25),
	offsetof(Slider_t637, ___m_FillContainerRect_26),
	offsetof(Slider_t637, ___m_HandleTransform_27),
	offsetof(Slider_t637, ___m_HandleContainerRect_28),
	offsetof(Slider_t637, ___m_Offset_29),
	offsetof(Slider_t637, ___m_Tracker_30),
	offsetof(Direction_t634, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(Axis_t636, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(SpriteState_t630, ___m_HighlightedSprite_0) + sizeof(Object_t),
	offsetof(SpriteState_t630, ___m_PressedSprite_1) + sizeof(Object_t),
	offsetof(SpriteState_t630, ___m_DisabledSprite_2) + sizeof(Object_t),
	offsetof(StencilMaterial_t639_StaticFields, ___m_List_0),
	offsetof(MatEntry_t638, ___baseMat_0),
	offsetof(MatEntry_t638, ___customMat_1),
	offsetof(MatEntry_t638, ___count_2),
	offsetof(MatEntry_t638, ___stencilId_3),
	offsetof(MatEntry_t638, ___operation_4),
	offsetof(MatEntry_t638, ___compareFunction_5),
	offsetof(MatEntry_t638, ___readMask_6),
	offsetof(MatEntry_t638, ___writeMask_7),
	offsetof(MatEntry_t638, ___useAlphaClip_8),
	offsetof(MatEntry_t638, ___colorMask_9),
	offsetof(Text_t196, ___m_FontData_26),
	offsetof(Text_t196, ___m_Text_27),
	offsetof(Text_t196, ___m_TextCache_28),
	offsetof(Text_t196, ___m_TextCacheForLayout_29),
	offsetof(Text_t196_StaticFields, ___s_DefaultText_30),
	offsetof(Text_t196, ___m_DisableFontTextureRebuiltCallback_31),
	offsetof(Text_t196, ___m_TempVerts_32),
	offsetof(Toggle_t557, ___toggleTransition_16),
	offsetof(Toggle_t557, ___graphic_17),
	offsetof(Toggle_t557, ___m_Group_18),
	offsetof(Toggle_t557, ___onValueChanged_19),
	offsetof(Toggle_t557, ___m_IsOn_20),
	offsetof(ToggleTransition_t641, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(ToggleGroup_t643, ___m_AllowSwitchOff_2),
	offsetof(ToggleGroup_t643, ___m_Toggles_3),
	offsetof(ToggleGroup_t643_StaticFields, ___U3CU3Ef__amU24cache2_4),
	offsetof(ToggleGroup_t643_StaticFields, ___U3CU3Ef__amU24cache3_5),
	offsetof(ClipperRegistry_t647_StaticFields, ___s_Instance_0),
	offsetof(ClipperRegistry_t647, ___m_Clippers_1),
	offsetof(RectangularVertexClipper_t614, ___m_WorldCorners_0),
	offsetof(RectangularVertexClipper_t614, ___m_CanvasCorners_1),
	offsetof(AspectRatioFitter_t651, ___m_AspectMode_2),
	offsetof(AspectRatioFitter_t651, ___m_AspectRatio_3),
	offsetof(AspectRatioFitter_t651, ___m_Rect_4),
	offsetof(AspectRatioFitter_t651, ___m_Tracker_5),
	offsetof(AspectMode_t650, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(CanvasScaler_t655, ___m_UiScaleMode_3),
	offsetof(CanvasScaler_t655, ___m_ReferencePixelsPerUnit_4),
	offsetof(CanvasScaler_t655, ___m_ScaleFactor_5),
	offsetof(CanvasScaler_t655, ___m_ReferenceResolution_6),
	offsetof(CanvasScaler_t655, ___m_ScreenMatchMode_7),
	offsetof(CanvasScaler_t655, ___m_MatchWidthOrHeight_8),
	offsetof(CanvasScaler_t655, ___m_PhysicalUnit_9),
	offsetof(CanvasScaler_t655, ___m_FallbackScreenDPI_10),
	offsetof(CanvasScaler_t655, ___m_DefaultSpriteDPI_11),
	offsetof(CanvasScaler_t655, ___m_DynamicPixelsPerUnit_12),
	offsetof(CanvasScaler_t655, ___m_Canvas_13),
	offsetof(CanvasScaler_t655, ___m_PrevScaleFactor_14),
	offsetof(CanvasScaler_t655, ___m_PrevReferencePixelsPerUnit_15),
	offsetof(ScaleMode_t652, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(ScreenMatchMode_t653, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(Unit_t654, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(ContentSizeFitter_t657, ___m_HorizontalFit_2),
	offsetof(ContentSizeFitter_t657, ___m_VerticalFit_3),
	offsetof(ContentSizeFitter_t657, ___m_Rect_4),
	offsetof(ContentSizeFitter_t657, ___m_Tracker_5),
	offsetof(FitMode_t656, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(GridLayoutGroup_t661, ___m_StartCorner_10),
	offsetof(GridLayoutGroup_t661, ___m_StartAxis_11),
	offsetof(GridLayoutGroup_t661, ___m_CellSize_12),
	offsetof(GridLayoutGroup_t661, ___m_Spacing_13),
	offsetof(GridLayoutGroup_t661, ___m_Constraint_14),
	offsetof(GridLayoutGroup_t661, ___m_ConstraintCount_15),
	offsetof(Corner_t658, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(Axis_t659, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(Constraint_t660, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(HorizontalOrVerticalLayoutGroup_t664, ___m_Spacing_10),
	offsetof(HorizontalOrVerticalLayoutGroup_t664, ___m_ChildForceExpandWidth_11),
	offsetof(HorizontalOrVerticalLayoutGroup_t664, ___m_ChildForceExpandHeight_12),
	offsetof(LayoutElement_t665, ___m_IgnoreLayout_2),
	offsetof(LayoutElement_t665, ___m_MinWidth_3),
	offsetof(LayoutElement_t665, ___m_MinHeight_4),
	offsetof(LayoutElement_t665, ___m_PreferredWidth_5),
	offsetof(LayoutElement_t665, ___m_PreferredHeight_6),
	offsetof(LayoutElement_t665, ___m_FlexibleWidth_7),
	offsetof(LayoutElement_t665, ___m_FlexibleHeight_8),
	offsetof(LayoutGroup_t662, ___m_Padding_2),
	offsetof(LayoutGroup_t662, ___m_ChildAlignment_3),
	offsetof(LayoutGroup_t662, ___m_Rect_4),
	offsetof(LayoutGroup_t662, ___m_Tracker_5),
	offsetof(LayoutGroup_t662, ___m_TotalMinSize_6),
	offsetof(LayoutGroup_t662, ___m_TotalPreferredSize_7),
	offsetof(LayoutGroup_t662, ___m_TotalFlexibleSize_8),
	offsetof(LayoutGroup_t662, ___m_RectChildren_9),
	offsetof(LayoutRebuilder_t668, ___m_ToRebuild_0) + sizeof(Object_t),
	offsetof(LayoutRebuilder_t668, ___m_CachedHashFromTransform_1) + sizeof(Object_t),
	offsetof(LayoutRebuilder_t668_StaticFields, ___U3CU3Ef__amU24cache2_2),
	offsetof(LayoutRebuilder_t668_StaticFields, ___U3CU3Ef__amU24cache3_3),
	offsetof(LayoutRebuilder_t668_StaticFields, ___U3CU3Ef__amU24cache4_4),
	offsetof(LayoutRebuilder_t668_StaticFields, ___U3CU3Ef__amU24cache5_5),
	offsetof(LayoutRebuilder_t668_StaticFields, ___U3CU3Ef__amU24cache6_6),
	offsetof(LayoutUtility_t671_StaticFields, ___U3CU3Ef__amU24cache0_0),
	offsetof(LayoutUtility_t671_StaticFields, ___U3CU3Ef__amU24cache1_1),
	offsetof(LayoutUtility_t671_StaticFields, ___U3CU3Ef__amU24cache2_2),
	offsetof(LayoutUtility_t671_StaticFields, ___U3CU3Ef__amU24cache3_3),
	offsetof(LayoutUtility_t671_StaticFields, ___U3CU3Ef__amU24cache4_4),
	offsetof(LayoutUtility_t671_StaticFields, ___U3CU3Ef__amU24cache5_5),
	offsetof(LayoutUtility_t671_StaticFields, ___U3CU3Ef__amU24cache6_6),
	offsetof(LayoutUtility_t671_StaticFields, ___U3CU3Ef__amU24cache7_7),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(VertexHelper_t674, ___m_Positions_0),
	offsetof(VertexHelper_t674, ___m_Colors_1),
	offsetof(VertexHelper_t674, ___m_Uv0S_2),
	offsetof(VertexHelper_t674, ___m_Uv1S_3),
	offsetof(VertexHelper_t674, ___m_Normals_4),
	offsetof(VertexHelper_t674, ___m_Tangents_5),
	offsetof(VertexHelper_t674, ___m_Indicies_6),
	offsetof(VertexHelper_t674_StaticFields, ___s_DefaultTangent_7),
	offsetof(VertexHelper_t674_StaticFields, ___s_DefaultNormal_8),
	offsetof(BaseMeshEffect_t682, ___m_Graphic_2),
	offsetof(Shadow_t684, ___m_EffectColor_3),
	offsetof(Shadow_t684, ___m_EffectDistance_4),
	offsetof(Shadow_t684, ___m_UseGraphicAlpha_5),
	offsetof(AssetBundleRequest_t784, ___m_AssetBundle_1),
	offsetof(AssetBundleRequest_t784, ___m_Path_2),
	offsetof(AssetBundleRequest_t784, ___m_Type_3),
	offsetof(SendMessageOptions_t786, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(PrimitiveType_t787, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(Space_t454, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(RuntimePlatform_t788, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(LogType_t789, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(WaitForSeconds_t410, ___m_Seconds_0),
	offsetof(Coroutine_t442, ___m_Ptr_0),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_AuthenticateCallback_0),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_FriendsCallback_1),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_AchievementDescriptionLoaderCallback_2),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_AchievementLoaderCallback_3),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_ProgressCallback_4),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_ScoreCallback_5),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_ScoreLoaderCallback_6),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_LeaderboardCallback_7),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_UsersCallback_8),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_adCache_9),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_friends_10),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_users_11),
	offsetof(GameCenterPlatform_t795_StaticFields, ___s_ResetAchievements_12),
	offsetof(GameCenterPlatform_t795_StaticFields, ___m_LocalUser_13),
	offsetof(GameCenterPlatform_t795_StaticFields, ___m_GcBoards_14),
	offsetof(GcLeaderboard_t805, ___m_InternalLeaderboard_0),
	offsetof(GcLeaderboard_t805, ___m_GenericLeaderboard_1),
	offsetof(BoneWeight_t405, ___m_Weight0_0) + sizeof(Object_t),
	offsetof(BoneWeight_t405, ___m_Weight1_1) + sizeof(Object_t),
	offsetof(BoneWeight_t405, ___m_Weight2_2) + sizeof(Object_t),
	offsetof(BoneWeight_t405, ___m_Weight3_3) + sizeof(Object_t),
	offsetof(BoneWeight_t405, ___m_BoneIndex0_4) + sizeof(Object_t),
	offsetof(BoneWeight_t405, ___m_BoneIndex1_5) + sizeof(Object_t),
	offsetof(BoneWeight_t405, ___m_BoneIndex2_6) + sizeof(Object_t),
	offsetof(BoneWeight_t405, ___m_BoneIndex3_7) + sizeof(Object_t),
	offsetof(CullingGroupEvent_t813, ___m_Index_0) + sizeof(Object_t),
	offsetof(CullingGroupEvent_t813, ___m_PrevState_1) + sizeof(Object_t),
	offsetof(CullingGroupEvent_t813, ___m_ThisState_2) + sizeof(Object_t),
	offsetof(CullingGroup_t815, ___m_Ptr_0),
	offsetof(CullingGroup_t815, ___m_OnStateChanged_1),
	offsetof(GradientColorKey_t816, ___color_0) + sizeof(Object_t),
	offsetof(GradientColorKey_t816, ___time_1) + sizeof(Object_t),
	offsetof(GradientAlphaKey_t817, ___alpha_0) + sizeof(Object_t),
	offsetof(GradientAlphaKey_t817, ___time_1) + sizeof(Object_t),
	offsetof(Gradient_t818, ___m_Ptr_0),
	offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t819, ___keyboardType_0) + sizeof(Object_t),
	offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t819, ___autocorrection_1) + sizeof(Object_t),
	offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t819, ___multiline_2) + sizeof(Object_t),
	offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t819, ___secure_3) + sizeof(Object_t),
	offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t819, ___alert_4) + sizeof(Object_t),
	offsetof(FullScreenMovieControlMode_t820, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(FullScreenMovieScalingMode_t821, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(TouchScreenKeyboardType_t749, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(TouchScreenKeyboard_t604, ___m_Ptr_0),
	offsetof(LayerMask_t531, ___m_Mask_0) + sizeof(Object_t),
	0,
	offsetof(Vector2_t2, ___x_1) + sizeof(Object_t),
	offsetof(Vector2_t2, ___y_2) + sizeof(Object_t),
	0,
	offsetof(Vector3_t36, ___x_1) + sizeof(Object_t),
	offsetof(Vector3_t36, ___y_2) + sizeof(Object_t),
	offsetof(Vector3_t36, ___z_3) + sizeof(Object_t),
	offsetof(Color_t9, ___r_0) + sizeof(Object_t),
	offsetof(Color_t9, ___g_1) + sizeof(Object_t),
	offsetof(Color_t9, ___b_2) + sizeof(Object_t),
	offsetof(Color_t9, ___a_3) + sizeof(Object_t),
	offsetof(Color32_t711, ___r_0) + sizeof(Object_t),
	offsetof(Color32_t711, ___g_1) + sizeof(Object_t),
	offsetof(Color32_t711, ___b_2) + sizeof(Object_t),
	offsetof(Color32_t711, ___a_3) + sizeof(Object_t),
	0,
	offsetof(Quaternion_t41, ___x_1) + sizeof(Object_t),
	offsetof(Quaternion_t41, ___y_2) + sizeof(Object_t),
	offsetof(Quaternion_t41, ___z_3) + sizeof(Object_t),
	offsetof(Quaternion_t41, ___w_4) + sizeof(Object_t),
	offsetof(Rect_t267, ___m_XMin_0) + sizeof(Object_t),
	offsetof(Rect_t267, ___m_YMin_1) + sizeof(Object_t),
	offsetof(Rect_t267, ___m_Width_2) + sizeof(Object_t),
	offsetof(Rect_t267, ___m_Height_3) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m00_0) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m10_1) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m20_2) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m30_3) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m01_4) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m11_5) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m21_6) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m31_7) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m02_8) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m12_9) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m22_10) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m32_11) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m03_12) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m13_13) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m23_14) + sizeof(Object_t),
	offsetof(Matrix4x4_t404, ___m33_15) + sizeof(Object_t),
	offsetof(Bounds_t349, ___m_Center_0) + sizeof(Object_t),
	offsetof(Bounds_t349, ___m_Extents_1) + sizeof(Object_t),
	0,
	offsetof(Vector4_t680, ___x_1) + sizeof(Object_t),
	offsetof(Vector4_t680, ___y_2) + sizeof(Object_t),
	offsetof(Vector4_t680, ___z_3) + sizeof(Object_t),
	offsetof(Vector4_t680, ___w_4) + sizeof(Object_t),
	offsetof(Ray_t382, ___m_Origin_0) + sizeof(Object_t),
	offsetof(Ray_t382, ___m_Direction_1) + sizeof(Object_t),
	offsetof(Plane_t751, ___m_Normal_0) + sizeof(Object_t),
	offsetof(Plane_t751, ___m_Distance_1) + sizeof(Object_t),
	offsetof(MathfInternal_t824_StaticFields, ___FloatMinNormal_0),
	offsetof(MathfInternal_t824_StaticFields, ___FloatMinDenormal_1),
	offsetof(MathfInternal_t824_StaticFields, ___IsFlushToZeroEnabled_2),
	offsetof(Mathf_t371_StaticFields, ___Epsilon_0),
	offsetof(DrivenTransformProperties_t825, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(RectTransform_t556_StaticFields, ___reapplyDrivenProperties_2),
	offsetof(Edge_t826, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(Axis_t827, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(ResourceRequest_t828, ___m_Path_1),
	offsetof(ResourceRequest_t828, ___m_Type_2),
	offsetof(SortingLayer_t832, ___m_Id_0) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shr0_0) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shr1_1) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shr2_2) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shr3_3) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shr4_4) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shr5_5) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shr6_6) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shr7_7) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shr8_8) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shg0_9) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shg1_10) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shg2_11) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shg3_12) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shg4_13) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shg5_14) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shg6_15) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shg7_16) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shg8_17) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shb0_18) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shb1_19) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shb2_20) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shb3_21) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shb4_22) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shb5_23) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shb6_24) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shb7_25) + sizeof(Object_t),
	offsetof(SphericalHarmonicsL2_t833, ___shb8_26) + sizeof(Object_t),
	offsetof(WWWForm_t433, ___formData_0),
	offsetof(WWWForm_t433, ___fieldNames_1),
	offsetof(WWWForm_t433, ___fileNames_2),
	offsetof(WWWForm_t433, ___types_3),
	offsetof(WWWForm_t433, ___boundary_4),
	offsetof(WWWForm_t433, ___containsFiles_5),
	offsetof(WWWTranscoder_t836_StaticFields, ___ucHexChars_0),
	offsetof(WWWTranscoder_t836_StaticFields, ___lcHexChars_1),
	offsetof(WWWTranscoder_t836_StaticFields, ___urlEscapeChar_2),
	offsetof(WWWTranscoder_t836_StaticFields, ___urlSpace_3),
	offsetof(WWWTranscoder_t836_StaticFields, ___urlForbidden_4),
	offsetof(WWWTranscoder_t836_StaticFields, ___qpEscapeChar_5),
	offsetof(WWWTranscoder_t836_StaticFields, ___qpSpace_6),
	offsetof(WWWTranscoder_t836_StaticFields, ___qpForbidden_7),
	offsetof(CacheIndex_t837, ___name_0) + sizeof(Object_t),
	offsetof(CacheIndex_t837, ___bytesUsed_1) + sizeof(Object_t),
	offsetof(CacheIndex_t837, ___expires_2) + sizeof(Object_t),
	offsetof(AsyncOperation_t783, ___m_Ptr_0),
	offsetof(Application_t841_StaticFields, ___s_LogCallbackHandler_0),
	offsetof(Application_t841_StaticFields, ___s_LogCallbackHandlerThreaded_1),
	offsetof(Camera_t6_StaticFields, ___onPreCull_2),
	offsetof(Camera_t6_StaticFields, ___onPreRender_3),
	offsetof(Camera_t6_StaticFields, ___onPostRender_4),
	offsetof(Display_t845, ___nativeDisplay_0),
	offsetof(Display_t845_StaticFields, ___displays_1),
	offsetof(Display_t845_StaticFields, ____mainDisplay_2),
	offsetof(Display_t845_StaticFields, ___onDisplaysUpdated_3),
	offsetof(TouchPhase_t847, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(IMECompositionMode_t848, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(Touch_t705, ___m_FingerId_0) + sizeof(Object_t),
	offsetof(Touch_t705, ___m_Position_1) + sizeof(Object_t),
	offsetof(Touch_t705, ___m_RawPosition_2) + sizeof(Object_t),
	offsetof(Touch_t705, ___m_PositionDelta_3) + sizeof(Object_t),
	offsetof(Touch_t705, ___m_TimeDelta_4) + sizeof(Object_t),
	offsetof(Touch_t705, ___m_TapCount_5) + sizeof(Object_t),
	offsetof(Touch_t705, ___m_Phase_6) + sizeof(Object_t),
	offsetof(HideFlags_t849, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(Object_t335, ___m_InstanceID_0),
	offsetof(Object_t335, ___m_CachedPtr_1),
	offsetof(Enumerator_t850, ___outer_0),
	offsetof(Enumerator_t850, ___currentIndex_1),
	offsetof(ActivityIndicatorStyle_t855, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(UnityAdsInternal_t857_StaticFields, ___onCampaignsAvailable_0),
	offsetof(UnityAdsInternal_t857_StaticFields, ___onCampaignsFetchFailed_1),
	offsetof(UnityAdsInternal_t857_StaticFields, ___onShow_2),
	offsetof(UnityAdsInternal_t857_StaticFields, ___onHide_3),
	offsetof(UnityAdsInternal_t857_StaticFields, ___onVideoCompleted_4),
	offsetof(UnityAdsInternal_t857_StaticFields, ___onVideoStarted_5),
	offsetof(Particle_t860, ___m_Position_0) + sizeof(Object_t),
	offsetof(Particle_t860, ___m_Velocity_1) + sizeof(Object_t),
	offsetof(Particle_t860, ___m_Size_2) + sizeof(Object_t),
	offsetof(Particle_t860, ___m_Rotation_3) + sizeof(Object_t),
	offsetof(Particle_t860, ___m_AngularVelocity_4) + sizeof(Object_t),
	offsetof(Particle_t860, ___m_Energy_5) + sizeof(Object_t),
	offsetof(Particle_t860, ___m_StartEnergy_6) + sizeof(Object_t),
	offsetof(Particle_t860, ___m_Color_7) + sizeof(Object_t),
	offsetof(Collision_t861, ___m_Impulse_0),
	offsetof(Collision_t861, ___m_RelativeVelocity_1),
	offsetof(Collision_t861, ___m_Rigidbody_2),
	offsetof(Collision_t861, ___m_Collider_3),
	offsetof(Collision_t861, ___m_Contacts_4),
	offsetof(QueryTriggerInteraction_t864, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(ContactPoint_t863, ___m_Point_0) + sizeof(Object_t),
	offsetof(ContactPoint_t863, ___m_Normal_1) + sizeof(Object_t),
	offsetof(ContactPoint_t863, ___m_ThisColliderInstanceID_2) + sizeof(Object_t),
	offsetof(ContactPoint_t863, ___m_OtherColliderInstanceID_3) + sizeof(Object_t),
	offsetof(RaycastHit_t60, ___m_Point_0) + sizeof(Object_t),
	offsetof(RaycastHit_t60, ___m_Normal_1) + sizeof(Object_t),
	offsetof(RaycastHit_t60, ___m_FaceID_2) + sizeof(Object_t),
	offsetof(RaycastHit_t60, ___m_Distance_3) + sizeof(Object_t),
	offsetof(RaycastHit_t60, ___m_UV_4) + sizeof(Object_t),
	offsetof(RaycastHit_t60, ___m_Collider_5) + sizeof(Object_t),
	offsetof(Physics2D_t728_StaticFields, ___m_LastDisabledRigidbody2D_0),
	offsetof(RaycastHit2D_t729, ___m_Centroid_0) + sizeof(Object_t),
	offsetof(RaycastHit2D_t729, ___m_Point_1) + sizeof(Object_t),
	offsetof(RaycastHit2D_t729, ___m_Normal_2) + sizeof(Object_t),
	offsetof(RaycastHit2D_t729, ___m_Distance_3) + sizeof(Object_t),
	offsetof(RaycastHit2D_t729, ___m_Fraction_4) + sizeof(Object_t),
	offsetof(RaycastHit2D_t729, ___m_Collider_5) + sizeof(Object_t),
	offsetof(ContactPoint2D_t869, ___m_Point_0) + sizeof(Object_t),
	offsetof(ContactPoint2D_t869, ___m_Normal_1) + sizeof(Object_t),
	offsetof(ContactPoint2D_t869, ___m_Collider_2) + sizeof(Object_t),
	offsetof(ContactPoint2D_t869, ___m_OtherCollider_3) + sizeof(Object_t),
	offsetof(Collision2D_t870, ___m_Rigidbody_0),
	offsetof(Collision2D_t870, ___m_Collider_1),
	offsetof(Collision2D_t870, ___m_Contacts_2),
	offsetof(Collision2D_t870, ___m_RelativeVelocity_3),
	offsetof(Collision2D_t870, ___m_Enabled_4),
	offsetof(AudioSettings_t873_StaticFields, ___OnAudioConfigurationChanged_0),
	offsetof(AudioClip_t353, ___m_PCMReaderCallback_2),
	offsetof(AudioClip_t353, ___m_PCMSetPositionCallback_3),
	offsetof(WebCamDevice_t876, ___m_Name_0) + sizeof(Object_t),
	offsetof(WebCamDevice_t876, ___m_Flags_1) + sizeof(Object_t),
	offsetof(AnimationEventSource_t878, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(AnimationEvent_t879, ___m_Time_0),
	offsetof(AnimationEvent_t879, ___m_FunctionName_1),
	offsetof(AnimationEvent_t879, ___m_StringParameter_2),
	offsetof(AnimationEvent_t879, ___m_ObjectReferenceParameter_3),
	offsetof(AnimationEvent_t879, ___m_FloatParameter_4),
	offsetof(AnimationEvent_t879, ___m_IntParameter_5),
	offsetof(AnimationEvent_t879, ___m_MessageOptions_6),
	offsetof(AnimationEvent_t879, ___m_Source_7),
	offsetof(AnimationEvent_t879, ___m_StateSender_8),
	offsetof(AnimationEvent_t879, ___m_AnimatorStateInfo_9),
	offsetof(AnimationEvent_t879, ___m_AnimatorClipInfo_10),
	offsetof(Keyframe_t883, ___m_Time_0) + sizeof(Object_t),
	offsetof(Keyframe_t883, ___m_Value_1) + sizeof(Object_t),
	offsetof(Keyframe_t883, ___m_InTangent_2) + sizeof(Object_t),
	offsetof(Keyframe_t883, ___m_OutTangent_3) + sizeof(Object_t),
	offsetof(AnimationCurve_t884, ___m_Ptr_0),
	offsetof(AnimatorClipInfo_t882, ___m_ClipInstanceID_0) + sizeof(Object_t),
	offsetof(AnimatorClipInfo_t882, ___m_Weight_1) + sizeof(Object_t),
	offsetof(AnimatorStateInfo_t881, ___m_Name_0) + sizeof(Object_t),
	offsetof(AnimatorStateInfo_t881, ___m_Path_1) + sizeof(Object_t),
	offsetof(AnimatorStateInfo_t881, ___m_FullPath_2) + sizeof(Object_t),
	offsetof(AnimatorStateInfo_t881, ___m_NormalizedTime_3) + sizeof(Object_t),
	offsetof(AnimatorStateInfo_t881, ___m_Length_4) + sizeof(Object_t),
	offsetof(AnimatorStateInfo_t881, ___m_Speed_5) + sizeof(Object_t),
	offsetof(AnimatorStateInfo_t881, ___m_SpeedMultiplier_6) + sizeof(Object_t),
	offsetof(AnimatorStateInfo_t881, ___m_Tag_7) + sizeof(Object_t),
	offsetof(AnimatorStateInfo_t881, ___m_Loop_8) + sizeof(Object_t),
	offsetof(AnimatorTransitionInfo_t886, ___m_FullPath_0) + sizeof(Object_t),
	offsetof(AnimatorTransitionInfo_t886, ___m_UserName_1) + sizeof(Object_t),
	offsetof(AnimatorTransitionInfo_t886, ___m_Name_2) + sizeof(Object_t),
	offsetof(AnimatorTransitionInfo_t886, ___m_NormalizedTime_3) + sizeof(Object_t),
	offsetof(AnimatorTransitionInfo_t886, ___m_AnyState_4) + sizeof(Object_t),
	offsetof(AnimatorTransitionInfo_t886, ___m_TransitionType_5) + sizeof(Object_t),
	offsetof(SkeletonBone_t887, ___name_0) + sizeof(Object_t),
	offsetof(SkeletonBone_t887, ___position_1) + sizeof(Object_t),
	offsetof(SkeletonBone_t887, ___rotation_2) + sizeof(Object_t),
	offsetof(SkeletonBone_t887, ___scale_3) + sizeof(Object_t),
	offsetof(SkeletonBone_t887, ___transformModified_4) + sizeof(Object_t),
	offsetof(HumanLimit_t888, ___m_Min_0) + sizeof(Object_t),
	offsetof(HumanLimit_t888, ___m_Max_1) + sizeof(Object_t),
	offsetof(HumanLimit_t888, ___m_Center_2) + sizeof(Object_t),
	offsetof(HumanLimit_t888, ___m_AxisLength_3) + sizeof(Object_t),
	offsetof(HumanLimit_t888, ___m_UseDefaultValues_4) + sizeof(Object_t),
	offsetof(HumanBone_t889, ___m_BoneName_0) + sizeof(Object_t),
	offsetof(HumanBone_t889, ___m_HumanName_1) + sizeof(Object_t),
	offsetof(HumanBone_t889, ___limit_2) + sizeof(Object_t),
	offsetof(TextAnchor_t766, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(HorizontalWrapMode_t890, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(VerticalWrapMode_t891, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(CharacterInfo_t892, ___index_0) + sizeof(Object_t),
	offsetof(CharacterInfo_t892, ___uv_1) + sizeof(Object_t),
	offsetof(CharacterInfo_t892, ___vert_2) + sizeof(Object_t),
	offsetof(CharacterInfo_t892, ___width_3) + sizeof(Object_t),
	offsetof(CharacterInfo_t892, ___size_4) + sizeof(Object_t),
	offsetof(CharacterInfo_t892, ___style_5) + sizeof(Object_t),
	offsetof(CharacterInfo_t892, ___flipped_6) + sizeof(Object_t),
	offsetof(CharacterInfo_t892, ___ascent_7) + sizeof(Object_t),
	offsetof(Font_t569_StaticFields, ___textureRebuilt_2),
	offsetof(Font_t569, ___m_FontTextureRebuildCallback_3),
	offsetof(UICharInfo_t754, ___cursorPos_0) + sizeof(Object_t),
	offsetof(UICharInfo_t754, ___charWidth_1) + sizeof(Object_t),
	offsetof(UILineInfo_t752, ___startCharIdx_0) + sizeof(Object_t),
	offsetof(UILineInfo_t752, ___height_1) + sizeof(Object_t),
	offsetof(TextGenerator_t603, ___m_Ptr_0),
	offsetof(TextGenerator_t603, ___m_LastString_1),
	offsetof(TextGenerator_t603, ___m_LastSettings_2),
	offsetof(TextGenerator_t603, ___m_HasGenerated_3),
	offsetof(TextGenerator_t603, ___m_LastValid_4),
	offsetof(TextGenerator_t603, ___m_Verts_5),
	offsetof(TextGenerator_t603, ___m_Characters_6),
	offsetof(TextGenerator_t603, ___m_Lines_7),
	offsetof(TextGenerator_t603, ___m_CachedVerts_8),
	offsetof(TextGenerator_t603, ___m_CachedCharacters_9),
	offsetof(TextGenerator_t603, ___m_CachedLines_10),
	offsetof(RenderMode_t896, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(Canvas_t574_StaticFields, ___willRenderCanvases_2),
	offsetof(UIVertex_t605, ___position_0) + sizeof(Object_t),
	offsetof(UIVertex_t605, ___normal_1) + sizeof(Object_t),
	offsetof(UIVertex_t605, ___color_2) + sizeof(Object_t),
	offsetof(UIVertex_t605, ___uv0_3) + sizeof(Object_t),
	offsetof(UIVertex_t605, ___uv1_4) + sizeof(Object_t),
	offsetof(UIVertex_t605, ___tangent_5) + sizeof(Object_t),
	offsetof(UIVertex_t605_StaticFields, ___s_DefaultColor_6),
	offsetof(UIVertex_t605_StaticFields, ___s_DefaultTangent_7),
	offsetof(UIVertex_t605_StaticFields, ___simpleVert_8),
	offsetof(RectTransformUtility_t737_StaticFields, ___s_Corners_0),
	offsetof(Event_t381, ___m_Ptr_0),
	offsetof(Event_t381_StaticFields, ___s_Current_1),
	offsetof(Event_t381_StaticFields, ___s_MasterEvent_2),
	offsetof(Event_t381_StaticFields, ___U3CU3Ef__switchU24map0_3),
	offsetof(KeyCode_t897, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(EventType_t898, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(EventModifiers_t899, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(GUI_t457_StaticFields, ___s_ScrollStepSize_0),
	offsetof(GUI_t457_StaticFields, ___s_ScrollControlId_1),
	offsetof(GUI_t457_StaticFields, ___s_HotTextField_2),
	offsetof(GUI_t457_StaticFields, ___s_BoxHash_3),
	offsetof(GUI_t457_StaticFields, ___s_RepeatButtonHash_4),
	offsetof(GUI_t457_StaticFields, ___s_ToggleHash_5),
	offsetof(GUI_t457_StaticFields, ___s_ButtonGridHash_6),
	offsetof(GUI_t457_StaticFields, ___s_SliderHash_7),
	offsetof(GUI_t457_StaticFields, ___s_BeginGroupHash_8),
	offsetof(GUI_t457_StaticFields, ___s_ScrollviewHash_9),
	offsetof(GUI_t457_StaticFields, ___s_Skin_10),
	offsetof(GUI_t457_StaticFields, ___s_ToolTipRect_11),
	offsetof(GUI_t457_StaticFields, ___s_ScrollViewStates_12),
	offsetof(GUI_t457_StaticFields, ___U3CnextScrollStepTimeU3Ek__BackingField_13),
	offsetof(GUI_t457_StaticFields, ___U3CscrollTroughSideU3Ek__BackingField_14),
	offsetof(ScrollViewState_t900, ___position_0),
	offsetof(ScrollViewState_t900, ___visibleRect_1),
	offsetof(ScrollViewState_t900, ___viewRect_2),
	offsetof(ScrollViewState_t900, ___scrollPosition_3),
	offsetof(ScrollViewState_t900, ___apply_4),
	offsetof(ScrollViewState_t900, ___hasScrollTo_5),
	offsetof(GUIContent_t379, ___m_Text_0),
	offsetof(GUIContent_t379, ___m_Image_1),
	offsetof(GUIContent_t379, ___m_Tooltip_2),
	offsetof(GUIContent_t379_StaticFields, ___s_Text_3),
	offsetof(GUIContent_t379_StaticFields, ___s_Image_4),
	offsetof(GUIContent_t379_StaticFields, ___s_TextImage_5),
	offsetof(GUIContent_t379_StaticFields, ___none_6),
	offsetof(GUILayoutUtility_t380_StaticFields, ___s_StoredLayouts_0),
	offsetof(GUILayoutUtility_t380_StaticFields, ___s_StoredWindows_1),
	offsetof(GUILayoutUtility_t380_StaticFields, ___current_2),
	offsetof(GUILayoutUtility_t380_StaticFields, ___kDummyRect_3),
	offsetof(GUILayoutUtility_t380_StaticFields, ___s_SpaceStyle_4),
	offsetof(LayoutCache_t904, ___topLevel_0),
	offsetof(LayoutCache_t904, ___layoutGroups_1),
	offsetof(LayoutCache_t904, ___windows_2),
	offsetof(GUILayoutEntry_t907, ___minWidth_0),
	offsetof(GUILayoutEntry_t907, ___maxWidth_1),
	offsetof(GUILayoutEntry_t907, ___minHeight_2),
	offsetof(GUILayoutEntry_t907, ___maxHeight_3),
	offsetof(GUILayoutEntry_t907, ___rect_4),
	offsetof(GUILayoutEntry_t907, ___stretchWidth_5),
	offsetof(GUILayoutEntry_t907, ___stretchHeight_6),
	offsetof(GUILayoutEntry_t907, ___m_Style_7),
	offsetof(GUILayoutEntry_t907_StaticFields, ___kDummyRect_8),
	offsetof(GUILayoutEntry_t907_StaticFields, ___indent_9),
	offsetof(GUILayoutGroup_t905, ___entries_10),
	offsetof(GUILayoutGroup_t905, ___isVertical_11),
	offsetof(GUILayoutGroup_t905, ___resetCoords_12),
	offsetof(GUILayoutGroup_t905, ___spacing_13),
	offsetof(GUILayoutGroup_t905, ___sameSize_14),
	offsetof(GUILayoutGroup_t905, ___isWindow_15),
	offsetof(GUILayoutGroup_t905, ___windowID_16),
	offsetof(GUILayoutGroup_t905, ___m_Cursor_17),
	offsetof(GUILayoutGroup_t905, ___m_StretchableCountX_18),
	offsetof(GUILayoutGroup_t905, ___m_StretchableCountY_19),
	offsetof(GUILayoutGroup_t905, ___m_UserSpecifiedWidth_20),
	offsetof(GUILayoutGroup_t905, ___m_UserSpecifiedHeight_21),
	offsetof(GUILayoutGroup_t905, ___m_ChildMinWidth_22),
	offsetof(GUILayoutGroup_t905, ___m_ChildMaxWidth_23),
	offsetof(GUILayoutGroup_t905, ___m_ChildMinHeight_24),
	offsetof(GUILayoutGroup_t905, ___m_ChildMaxHeight_25),
	offsetof(GUILayoutGroup_t905, ___m_Margin_26),
	offsetof(GUIScrollGroup_t909, ___calcMinWidth_27),
	offsetof(GUIScrollGroup_t909, ___calcMaxWidth_28),
	offsetof(GUIScrollGroup_t909, ___calcMinHeight_29),
	offsetof(GUIScrollGroup_t909, ___calcMaxHeight_30),
	offsetof(GUIScrollGroup_t909, ___clientWidth_31),
	offsetof(GUIScrollGroup_t909, ___clientHeight_32),
	offsetof(GUIScrollGroup_t909, ___allowHorizontalScroll_33),
	offsetof(GUIScrollGroup_t909, ___allowVerticalScroll_34),
	offsetof(GUIScrollGroup_t909, ___needsHorizontalScrollbar_35),
	offsetof(GUIScrollGroup_t909, ___needsVerticalScrollbar_36),
	offsetof(GUIScrollGroup_t909, ___horizontalScrollbar_37),
	offsetof(GUIScrollGroup_t909, ___verticalScrollbar_38),
	offsetof(GUIWordWrapSizer_t910, ___m_Content_10),
	offsetof(GUIWordWrapSizer_t910, ___m_ForcedMinHeight_11),
	offsetof(GUIWordWrapSizer_t910, ___m_ForcedMaxHeight_12),
	offsetof(GUILayoutOption_t912, ___type_0),
	offsetof(GUILayoutOption_t912, ___value_1),
	offsetof(Type_t911, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(GUISettings_t913, ___m_DoubleClickSelectsWord_0),
	offsetof(GUISettings_t913, ___m_TripleClickSelectsLine_1),
	offsetof(GUISettings_t913, ___m_CursorColor_2),
	offsetof(GUISettings_t913, ___m_CursorFlashSpeed_3),
	offsetof(GUISettings_t913, ___m_SelectionColor_4),
	offsetof(GUISkin_t901, ___m_Font_2),
	offsetof(GUISkin_t901, ___m_box_3),
	offsetof(GUISkin_t901, ___m_button_4),
	offsetof(GUISkin_t901, ___m_toggle_5),
	offsetof(GUISkin_t901, ___m_label_6),
	offsetof(GUISkin_t901, ___m_textField_7),
	offsetof(GUISkin_t901, ___m_textArea_8),
	offsetof(GUISkin_t901, ___m_window_9),
	offsetof(GUISkin_t901, ___m_horizontalSlider_10),
	offsetof(GUISkin_t901, ___m_horizontalSliderThumb_11),
	offsetof(GUISkin_t901, ___m_verticalSlider_12),
	offsetof(GUISkin_t901, ___m_verticalSliderThumb_13),
	offsetof(GUISkin_t901, ___m_horizontalScrollbar_14),
	offsetof(GUISkin_t901, ___m_horizontalScrollbarThumb_15),
	offsetof(GUISkin_t901, ___m_horizontalScrollbarLeftButton_16),
	offsetof(GUISkin_t901, ___m_horizontalScrollbarRightButton_17),
	offsetof(GUISkin_t901, ___m_verticalScrollbar_18),
	offsetof(GUISkin_t901, ___m_verticalScrollbarThumb_19),
	offsetof(GUISkin_t901, ___m_verticalScrollbarUpButton_20),
	offsetof(GUISkin_t901, ___m_verticalScrollbarDownButton_21),
	offsetof(GUISkin_t901, ___m_ScrollView_22),
	offsetof(GUISkin_t901, ___m_CustomStyles_23),
	offsetof(GUISkin_t901, ___m_Settings_24),
	offsetof(GUISkin_t901_StaticFields, ___ms_Error_25),
	offsetof(GUISkin_t901, ___m_Styles_26),
	offsetof(GUISkin_t901_StaticFields, ___m_SkinChanged_27),
	offsetof(GUISkin_t901_StaticFields, ___current_28),
	offsetof(GUIStyleState_t917, ___m_Ptr_0),
	offsetof(GUIStyleState_t917, ___m_SourceStyle_1),
	offsetof(GUIStyleState_t917, ___m_Background_2),
	offsetof(RectOffset_t666, ___m_Ptr_0),
	offsetof(RectOffset_t666, ___m_SourceStyle_1),
	offsetof(FontStyle_t918, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(ImagePosition_t919, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(GUIStyle_t342, ___m_Ptr_0),
	offsetof(GUIStyle_t342, ___m_Normal_1),
	offsetof(GUIStyle_t342, ___m_Hover_2),
	offsetof(GUIStyle_t342, ___m_Active_3),
	offsetof(GUIStyle_t342, ___m_Focused_4),
	offsetof(GUIStyle_t342, ___m_OnNormal_5),
	offsetof(GUIStyle_t342, ___m_OnHover_6),
	offsetof(GUIStyle_t342, ___m_OnActive_7),
	offsetof(GUIStyle_t342, ___m_OnFocused_8),
	offsetof(GUIStyle_t342, ___m_Border_9),
	offsetof(GUIStyle_t342, ___m_Padding_10),
	offsetof(GUIStyle_t342, ___m_Margin_11),
	offsetof(GUIStyle_t342, ___m_Overflow_12),
	offsetof(GUIStyle_t342, ___m_FontInternal_13),
	offsetof(GUIStyle_t342_StaticFields, ___showKeyboardFocus_14),
	offsetof(GUIStyle_t342_StaticFields, ___s_None_15),
	offsetof(FocusType_t921, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(GUIUtility_t750_StaticFields, ___s_SkinMode_0),
	offsetof(GUIUtility_t750_StaticFields, ___s_OriginalID_1),
	offsetof(GUIUtility_t750_StaticFields, ___s_EditorScreenPointOffset_2),
	offsetof(GUIUtility_t750_StaticFields, ___s_HasKeyboardFocus_3),
	offsetof(Internal_DrawArguments_t922, ___target_0) + sizeof(Object_t),
	offsetof(Internal_DrawArguments_t922, ___position_1) + sizeof(Object_t),
	offsetof(Internal_DrawArguments_t922, ___isHover_2) + sizeof(Object_t),
	offsetof(Internal_DrawArguments_t922, ___isActive_3) + sizeof(Object_t),
	offsetof(Internal_DrawArguments_t922, ___on_4) + sizeof(Object_t),
	offsetof(Internal_DrawArguments_t922, ___hasKeyboardFocus_5) + sizeof(Object_t),
	offsetof(IL2CPPStructAlignmentAttribute_t924, ___Align_0),
	offsetof(AttributeHelperEngine_t925_StaticFields, ____disallowMultipleComponentArray_0),
	offsetof(AttributeHelperEngine_t925_StaticFields, ____executeInEditModeArray_1),
	offsetof(AttributeHelperEngine_t925_StaticFields, ____requireComponentArray_2),
	offsetof(RequireComponent_t930, ___m_Type0_0),
	offsetof(RequireComponent_t930, ___m_Type1_1),
	offsetof(RequireComponent_t930, ___m_Type2_2),
	offsetof(AddComponentMenu_t931, ___m_AddComponentMenu_0),
	offsetof(AddComponentMenu_t931, ___m_Ordering_1),
	0,
	0,
	offsetof(GcUserProfileData_t937, ___userName_0) + sizeof(Object_t),
	offsetof(GcUserProfileData_t937, ___userID_1) + sizeof(Object_t),
	offsetof(GcUserProfileData_t937, ___isFriend_2) + sizeof(Object_t),
	offsetof(GcUserProfileData_t937, ___image_3) + sizeof(Object_t),
	offsetof(GcAchievementDescriptionData_t938, ___m_Identifier_0) + sizeof(Object_t),
	offsetof(GcAchievementDescriptionData_t938, ___m_Title_1) + sizeof(Object_t),
	offsetof(GcAchievementDescriptionData_t938, ___m_Image_2) + sizeof(Object_t),
	offsetof(GcAchievementDescriptionData_t938, ___m_AchievedDescription_3) + sizeof(Object_t),
	offsetof(GcAchievementDescriptionData_t938, ___m_UnachievedDescription_4) + sizeof(Object_t),
	offsetof(GcAchievementDescriptionData_t938, ___m_Hidden_5) + sizeof(Object_t),
	offsetof(GcAchievementDescriptionData_t938, ___m_Points_6) + sizeof(Object_t),
	offsetof(GcAchievementData_t939, ___m_Identifier_0) + sizeof(Object_t),
	offsetof(GcAchievementData_t939, ___m_PercentCompleted_1) + sizeof(Object_t),
	offsetof(GcAchievementData_t939, ___m_Completed_2) + sizeof(Object_t),
	offsetof(GcAchievementData_t939, ___m_Hidden_3) + sizeof(Object_t),
	offsetof(GcAchievementData_t939, ___m_LastReportedDate_4) + sizeof(Object_t),
	offsetof(GcScoreData_t940, ___m_Category_0) + sizeof(Object_t),
	offsetof(GcScoreData_t940, ___m_ValueLow_1) + sizeof(Object_t),
	offsetof(GcScoreData_t940, ___m_ValueHigh_2) + sizeof(Object_t),
	offsetof(GcScoreData_t940, ___m_Date_3) + sizeof(Object_t),
	offsetof(GcScoreData_t940, ___m_FormattedValue_4) + sizeof(Object_t),
	offsetof(GcScoreData_t940, ___m_PlayerID_5) + sizeof(Object_t),
	offsetof(GcScoreData_t940, ___m_Rank_6) + sizeof(Object_t),
	offsetof(Resolution_t941, ___m_Width_0) + sizeof(Object_t),
	offsetof(Resolution_t941, ___m_Height_1) + sizeof(Object_t),
	offsetof(Resolution_t941, ___m_RefreshRate_2) + sizeof(Object_t),
	offsetof(RenderBuffer_t942, ___m_RenderTextureInstanceID_0) + sizeof(Object_t),
	offsetof(RenderBuffer_t942, ___m_BufferPtr_1) + sizeof(Object_t),
	offsetof(CameraClearFlags_t943, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(ScreenOrientation_t944, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(FilterMode_t945, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(TextureWrapMode_t946, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(TextureFormat_t947, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(RenderTextureFormat_t948, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(RenderTextureReadWrite_t949, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(CompareFunction_t760, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(ColorWriteMask_t761, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(StencilOp_t759, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(ReflectionProbeBlendInfo_t950, ___probe_0) + sizeof(Object_t),
	offsetof(ReflectionProbeBlendInfo_t950, ___weight_1) + sizeof(Object_t),
	offsetof(LocalUser_t803, ___m_Friends_5),
	offsetof(LocalUser_t803, ___m_Authenticated_6),
	offsetof(LocalUser_t803, ___m_Underage_7),
	offsetof(UserProfile_t951, ___m_UserName_0),
	offsetof(UserProfile_t951, ___m_ID_1),
	offsetof(UserProfile_t951, ___m_IsFriend_2),
	offsetof(UserProfile_t951, ___m_State_3),
	offsetof(UserProfile_t951, ___m_Image_4),
	offsetof(Achievement_t953, ___m_Completed_0),
	offsetof(Achievement_t953, ___m_Hidden_1),
	offsetof(Achievement_t953, ___m_LastReportedDate_2),
	offsetof(Achievement_t953, ___U3CidU3Ek__BackingField_3),
	offsetof(Achievement_t953, ___U3CpercentCompletedU3Ek__BackingField_4),
	offsetof(AchievementDescription_t954, ___m_Title_0),
	offsetof(AchievementDescription_t954, ___m_Image_1),
	offsetof(AchievementDescription_t954, ___m_AchievedDescription_2),
	offsetof(AchievementDescription_t954, ___m_UnachievedDescription_3),
	offsetof(AchievementDescription_t954, ___m_Hidden_4),
	offsetof(AchievementDescription_t954, ___m_Points_5),
	offsetof(AchievementDescription_t954, ___U3CidU3Ek__BackingField_6),
	offsetof(Score_t955, ___m_Date_0),
	offsetof(Score_t955, ___m_FormattedValue_1),
	offsetof(Score_t955, ___m_UserID_2),
	offsetof(Score_t955, ___m_Rank_3),
	offsetof(Score_t955, ___U3CleaderboardIDU3Ek__BackingField_4),
	offsetof(Score_t955, ___U3CvalueU3Ek__BackingField_5),
	offsetof(Leaderboard_t806, ___m_Loading_0),
	offsetof(Leaderboard_t806, ___m_LocalUserScore_1),
	offsetof(Leaderboard_t806, ___m_MaxRange_2),
	offsetof(Leaderboard_t806, ___m_Scores_3),
	offsetof(Leaderboard_t806, ___m_Title_4),
	offsetof(Leaderboard_t806, ___m_UserIDs_5),
	offsetof(Leaderboard_t806, ___U3CidU3Ek__BackingField_6),
	offsetof(Leaderboard_t806, ___U3CuserScopeU3Ek__BackingField_7),
	offsetof(Leaderboard_t806, ___U3CrangeU3Ek__BackingField_8),
	offsetof(Leaderboard_t806, ___U3CtimeScopeU3Ek__BackingField_9),
	0,
	0,
	0,
	offsetof(SendMouseEvents_t960_StaticFields, ___s_MouseUsed_3),
	offsetof(SendMouseEvents_t960_StaticFields, ___m_LastHit_4),
	offsetof(SendMouseEvents_t960_StaticFields, ___m_MouseDownHit_5),
	offsetof(SendMouseEvents_t960_StaticFields, ___m_CurrentHit_6),
	offsetof(SendMouseEvents_t960_StaticFields, ___m_Cameras_7),
	offsetof(HitInfo_t959, ___target_0) + sizeof(Object_t),
	offsetof(HitInfo_t959, ___camera_1) + sizeof(Object_t),
	offsetof(UserState_t962, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(UserScope_t963, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(TimeScope_t964, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(Range_t957, ___from_0) + sizeof(Object_t),
	offsetof(Range_t957, ___count_1) + sizeof(Object_t),
	offsetof(TooltipAttribute_t966, ___tooltip_0),
	offsetof(SpaceAttribute_t967, ___height_0),
	offsetof(RangeAttribute_t968, ___min_0),
	offsetof(RangeAttribute_t968, ___max_1),
	offsetof(TextAreaAttribute_t969, ___minLines_0),
	offsetof(TextAreaAttribute_t969, ___maxLines_1),
	offsetof(SliderState_t971, ___dragStartPos_0),
	offsetof(SliderState_t971, ___dragStartValue_1),
	offsetof(SliderState_t971, ___isDragging_2),
	offsetof(StackTraceUtility_t972_StaticFields, ___projectFolder_0),
	0,
	offsetof(UnityException_t748, ___unityStackTrace_12),
	offsetof(TextEditor_t977, ___keyboardOnScreen_0),
	offsetof(TextEditor_t977, ___controlID_1),
	offsetof(TextEditor_t977, ___content_2),
	offsetof(TextEditor_t977, ___style_3),
	offsetof(TextEditor_t977, ___multiline_4),
	offsetof(TextEditor_t977, ___hasHorizontalCursorPos_5),
	offsetof(TextEditor_t977, ___isPasswordField_6),
	offsetof(TextEditor_t977, ___m_HasFocus_7),
	offsetof(TextEditor_t977, ___scrollOffset_8),
	offsetof(TextEditor_t977, ___m_Position_9),
	offsetof(TextEditor_t977, ___m_CursorIndex_10),
	offsetof(TextEditor_t977, ___m_SelectIndex_11),
	offsetof(TextEditor_t977, ___m_RevealCursor_12),
	offsetof(TextEditor_t977, ___graphicalCursorPos_13),
	offsetof(TextEditor_t977, ___graphicalSelectCursorPos_14),
	offsetof(TextEditor_t977, ___m_MouseDragSelectsWholeWords_15),
	offsetof(TextEditor_t977, ___m_DblClickInitPos_16),
	offsetof(TextEditor_t977, ___m_DblClickSnap_17),
	offsetof(TextEditor_t977, ___m_bJustSelected_18),
	offsetof(TextEditor_t977, ___m_iAltCursorPos_19),
	offsetof(TextEditor_t977, ___oldText_20),
	offsetof(TextEditor_t977, ___oldPos_21),
	offsetof(TextEditor_t977, ___oldSelectPos_22),
	offsetof(TextEditor_t977_StaticFields, ___s_Keyactions_23),
	offsetof(DblClickSnapping_t975, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(TextEditOp_t976, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(TextGenerationSettings_t715, ___font_0) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___color_1) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___fontSize_2) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___lineSpacing_3) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___richText_4) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___scaleFactor_5) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___fontStyle_6) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___textAnchor_7) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___resizeTextForBestFit_8) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___resizeTextMinSize_9) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___resizeTextMaxSize_10) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___updateBounds_11) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___verticalOverflow_12) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___horizontalOverflow_13) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___generationExtents_14) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___pivot_15) + sizeof(Object_t),
	offsetof(TextGenerationSettings_t715, ___generateOutOfBounds_16) + sizeof(Object_t),
	offsetof(TrackedReference_t885, ___m_Ptr_0),
	offsetof(PersistentListenerMode_t979, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(ArgumentCache_t980, ___m_ObjectArgument_0),
	offsetof(ArgumentCache_t980, ___m_ObjectArgumentAssemblyTypeName_1),
	offsetof(ArgumentCache_t980, ___m_IntArgument_2),
	offsetof(ArgumentCache_t980, ___m_FloatArgument_3),
	offsetof(ArgumentCache_t980, ___m_StringArgument_4),
	offsetof(ArgumentCache_t980, ___m_BoolArgument_5),
	offsetof(InvokableCall_t982, ___Delegate_0),
	0,
	0,
	0,
	0,
	0,
	offsetof(UnityEventCallState_t983, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(PersistentCall_t984, ___m_Target_0),
	offsetof(PersistentCall_t984, ___m_MethodName_1),
	offsetof(PersistentCall_t984, ___m_Mode_2),
	offsetof(PersistentCall_t984, ___m_Arguments_3),
	offsetof(PersistentCall_t984, ___m_CallState_4),
	offsetof(PersistentCallGroup_t985, ___m_Calls_0),
	offsetof(InvokableCallList_t987, ___m_PersistentCalls_0),
	offsetof(InvokableCallList_t987, ___m_RuntimeCalls_1),
	offsetof(InvokableCallList_t987, ___m_ExecutingCalls_2),
	offsetof(InvokableCallList_t987, ___m_NeedsUpdate_3),
	offsetof(UnityEventBase_t989, ___m_Calls_0),
	offsetof(UnityEventBase_t989, ___m_PersistentCalls_1),
	offsetof(UnityEventBase_t989, ___m_TypeName_2),
	offsetof(UnityEventBase_t989, ___m_CallsDirty_3),
	offsetof(UnityEvent_t542, ___m_InvokeArray_4),
	0,
	0,
	0,
	0,
	offsetof(DefaultValueAttribute_t990, ___DefaultValue_0),
	offsetof(FormerlySerializedAsAttribute_t992, ___m_oldName_0),
	offsetof(TypeInferenceRules_t993, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(TypeInferenceRuleAttribute_t994, ____rule_0),
	offsetof(BackgroundPlaneAbstractBehaviour_t269, ___mTextureInfo_2),
	offsetof(BackgroundPlaneAbstractBehaviour_t269, ___mViewWidth_3),
	offsetof(BackgroundPlaneAbstractBehaviour_t269, ___mViewHeight_4),
	offsetof(BackgroundPlaneAbstractBehaviour_t269, ___mVideoBgConfigChanged_5),
	offsetof(BackgroundPlaneAbstractBehaviour_t269, ___mCamera_6),
	offsetof(BackgroundPlaneAbstractBehaviour_t269, ___mQBehaviour_7),
	offsetof(BackgroundPlaneAbstractBehaviour_t269_StaticFields, ___maxDisplacement_8),
	offsetof(BackgroundPlaneAbstractBehaviour_t269, ___defaultNumDivisions_9),
	offsetof(BackgroundPlaneAbstractBehaviour_t269, ___mMesh_10),
	offsetof(BackgroundPlaneAbstractBehaviour_t269, ___mStereoDepth_11),
	offsetof(BackgroundPlaneAbstractBehaviour_t269, ___mNumDivisions_12),
	0,
	0,
	0,
	offsetof(IOSCamRecoveringHelper_t1052_StaticFields, ___sHasJustResumed_3),
	offsetof(IOSCamRecoveringHelper_t1052_StaticFields, ___sCheckFailedFrameAfterResume_4),
	offsetof(IOSCamRecoveringHelper_t1052_StaticFields, ___sCheckedFailedFrameCounter_5),
	offsetof(IOSCamRecoveringHelper_t1052_StaticFields, ___sWaitToRecoverCameraRestart_6),
	offsetof(IOSCamRecoveringHelper_t1052_StaticFields, ___sWaitedFrameRecoverCounter_7),
	offsetof(IOSCamRecoveringHelper_t1052_StaticFields, ___sRecoveryAttemptCounter_8),
	offsetof(MonoCameraConfiguration_t1053, ___mPrimaryCamera_0),
	offsetof(MonoCameraConfiguration_t1053, ___mCameraDeviceMode_1),
	offsetof(MonoCameraConfiguration_t1053, ___mLastVideoBackGroundMirroredFromSDK_2),
	offsetof(MonoCameraConfiguration_t1053, ___mOnVideoBackgroundConfigChanged_3),
	offsetof(MonoCameraConfiguration_t1053, ___mVideoBackgroundBehaviours_4),
	offsetof(MonoCameraConfiguration_t1053, ___mViewportRect_5),
	offsetof(MonoCameraConfiguration_t1053, ___mRenderVideoBackground_6),
	offsetof(MonoCameraConfiguration_t1053, ___mCameraWidthFactor_7),
	offsetof(MonoCameraConfiguration_t1053, ___mViewPortWidth_8),
	offsetof(MonoCameraConfiguration_t1053, ___mViewPortHeight_9),
	offsetof(MonoCameraConfiguration_t1053, ___mProjectionOrientation_10),
	offsetof(MonoCameraConfiguration_t1053, ___mInitialReflection_11),
	0,
	offsetof(StereoCameraConfiguration_t1055_StaticFields, ___MIN_CENTER_13),
	offsetof(StereoCameraConfiguration_t1055_StaticFields, ___MAX_CENTER_14),
	offsetof(StereoCameraConfiguration_t1055_StaticFields, ___MAX_BOTTOM_15),
	offsetof(StereoCameraConfiguration_t1055_StaticFields, ___MAX_TOP_16),
	offsetof(StereoCameraConfiguration_t1055, ___mSecondaryCamera_17),
	offsetof(StereoCameraConfiguration_t1055, ___mSkewFrustum_18),
	offsetof(StereoCameraConfiguration_t1055, ___mNeedToCheckStereo_19),
	offsetof(StereoCameraConfiguration_t1055, ___mLastAppliedNearClipPlane_20),
	offsetof(StereoCameraConfiguration_t1055, ___mLastAppliedFarClipPlane_21),
	offsetof(StereoCameraConfiguration_t1055, ___mLastAppliedVirtualFoV_22),
	offsetof(StereoCameraConfiguration_t1055, ___mNewNearClipPlane_23),
	offsetof(StereoCameraConfiguration_t1055, ___mNewFarClipPlane_24),
	offsetof(StereoCameraConfiguration_t1055, ___mNewVirtualFoV_25),
	offsetof(StereoCameraConfiguration_t1055, ___mCameraOffset_26),
	offsetof(StereoCameraConfiguration_t1055, ___mEyewearUserCalibrationProfileId_27),
	offsetof(NullCameraConfiguration_t1056, ___mProjectionOrientation_0),
	0,
	0,
	offsetof(TrackableBehaviour_t211, ___mTrackableName_2),
	offsetof(TrackableBehaviour_t211, ___mPreviousScale_3),
	offsetof(TrackableBehaviour_t211, ___mPreserveChildSize_4),
	offsetof(TrackableBehaviour_t211, ___mInitializedInEditor_5),
	offsetof(TrackableBehaviour_t211, ___mStatus_6),
	offsetof(TrackableBehaviour_t211, ___mTrackable_7),
	offsetof(TrackableBehaviour_t211, ___mTrackableEventHandlers_8),
	offsetof(Status_t1058, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(DataSetTrackableBehaviour_t1061, ___mDataSetPath_9),
	offsetof(DataSetTrackableBehaviour_t1061, ___mExtendedTracking_10),
	offsetof(DataSetTrackableBehaviour_t1061, ___mInitializeSmartTerrain_11),
	offsetof(DataSetTrackableBehaviour_t1061, ___mReconstructionToInitialize_12),
	offsetof(DataSetTrackableBehaviour_t1061, ___mSmartTerrainOccluderBoundsMin_13),
	offsetof(DataSetTrackableBehaviour_t1061, ___mSmartTerrainOccluderBoundsMax_14),
	offsetof(DataSetTrackableBehaviour_t1061, ___mIsSmartTerrainOccluderOffset_15),
	offsetof(DataSetTrackableBehaviour_t1061, ___mSmartTerrainOccluderOffset_16),
	offsetof(DataSetTrackableBehaviour_t1061, ___mSmartTerrainOccluderRotation_17),
	offsetof(DataSetTrackableBehaviour_t1061, ___mSmartTerrainOccluderLockedInPlace_18),
	offsetof(DataSetTrackableBehaviour_t1061, ___mAutoSetOccluderFromTargetSize_19),
	offsetof(ObjectTargetAbstractBehaviour_t299, ___mObjectTarget_20),
	offsetof(ObjectTargetAbstractBehaviour_t299, ___mAspectRatioXY_21),
	offsetof(ObjectTargetAbstractBehaviour_t299, ___mAspectRatioXZ_22),
	offsetof(ObjectTargetAbstractBehaviour_t299, ___mShowBoundingBox_23),
	offsetof(ObjectTargetAbstractBehaviour_t299, ___mBBoxMin_24),
	offsetof(ObjectTargetAbstractBehaviour_t299, ___mBBoxMax_25),
	offsetof(ObjectTargetAbstractBehaviour_t299, ___mPreviewImage_26),
	offsetof(CameraDevice_t437_StaticFields, ___mInstance_0),
	offsetof(CameraDeviceMode_t1063, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(FocusMode_t438, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(CameraDirection_t1064, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(VideoModeData_t1065, ___width_0) + sizeof(Object_t),
	offsetof(VideoModeData_t1065, ___height_1) + sizeof(Object_t),
	offsetof(VideoModeData_t1065, ___frameRate_2) + sizeof(Object_t),
	offsetof(VideoModeData_t1065, ___unused_3) + sizeof(Object_t),
	offsetof(CloudRecoAbstractBehaviour_t271, ___mObjectTracker_2),
	offsetof(CloudRecoAbstractBehaviour_t271, ___mCurrentlyInitializing_3),
	offsetof(CloudRecoAbstractBehaviour_t271, ___mInitSuccess_4),
	offsetof(CloudRecoAbstractBehaviour_t271, ___mCloudRecoStarted_5),
	offsetof(CloudRecoAbstractBehaviour_t271, ___mOnInitializedCalled_6),
	offsetof(CloudRecoAbstractBehaviour_t271, ___mHandlers_7),
	offsetof(CloudRecoAbstractBehaviour_t271, ___mTargetFinderStartedBeforeDisable_8),
	offsetof(CloudRecoAbstractBehaviour_t271, ___AccessKey_9),
	offsetof(CloudRecoAbstractBehaviour_t271, ___SecretKey_10),
	offsetof(CloudRecoAbstractBehaviour_t271, ___ScanlineColor_11),
	offsetof(CloudRecoAbstractBehaviour_t271, ___FeaturePointColor_12),
	offsetof(ReconstructionImpl_t1068, ___mNativeReconstructionPtr_0),
	offsetof(ReconstructionImpl_t1068, ___mMaximumAreaIsSet_1),
	offsetof(ReconstructionImpl_t1068, ___mMaximumArea_2),
	offsetof(ReconstructionImpl_t1068, ___mNavMeshPadding_3),
	offsetof(ReconstructionImpl_t1068, ___mNavMeshUpdatesEnabled_4),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___matteShader_2),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___disableMattes_3),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mBgPlane_4),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mLeftPlane_5),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mRightPlane_6),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mTopPlane_7),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mBottomPlane_8),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mCamera_9),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mSceneIsScaledDown_10),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mBgPlaneLocalPos_11),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mBgPlaneLocalScale_12),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mCameraNearPlane_13),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mCameraPixelRect_14),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mCameraFieldOFView_15),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mVuforiaBehaviour_16),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mHideBehaviours_17),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mDeactivatedHideBehaviours_18),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mPlanesActivated_19),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mLeftPlaneCachedScale_20),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mRightPlaneCachedScale_21),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mBottomPlaneCachedScale_22),
	offsetof(HideExcessAreaAbstractBehaviour_t284, ___mTopPlaneCachedScale_23),
	offsetof(TrackableImpl_t1071, ___U3CNameU3Ek__BackingField_0),
	offsetof(TrackableImpl_t1071, ___U3CIDU3Ek__BackingField_1),
	offsetof(ObjectTargetImpl_t1072, ___mSize_2),
	offsetof(ObjectTargetImpl_t1072, ___mDataSet_3),
	offsetof(UnityPlayer_t1074_StaticFields, ___sPlayer_0),
	offsetof(ReconstructionFromTargetImpl_t1076, ___mOccluderMin_5),
	offsetof(ReconstructionFromTargetImpl_t1076, ___mOccluderMax_6),
	offsetof(ReconstructionFromTargetImpl_t1076, ___mOccluderOffset_7),
	offsetof(ReconstructionFromTargetImpl_t1076, ___mOccluderRotation_8),
	offsetof(ReconstructionFromTargetImpl_t1076, ___mInitializationTarget_9),
	offsetof(ReconstructionFromTargetImpl_t1076, ___mCanAutoSetInitializationTarget_10),
	offsetof(ReconstructionFromTargetAbstractBehaviour_t303, ___mReconstructionFromTarget_2),
	offsetof(ReconstructionFromTargetAbstractBehaviour_t303, ___mReconstructionBehaviour_3),
	offsetof(Eyewear_t1081_StaticFields, ___mInstance_0),
	offsetof(Eyewear_t1081, ___mProfileManager_1),
	offsetof(Eyewear_t1081, ___mCalibrator_2),
	offsetof(EyeID_t1079, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(EyewearCalibrationReading_t1080, ___pose_0) + sizeof(Object_t),
	offsetof(EyewearCalibrationReading_t1080, ___scale_1) + sizeof(Object_t),
	offsetof(EyewearCalibrationReading_t1080, ___centerX_2) + sizeof(Object_t),
	offsetof(EyewearCalibrationReading_t1080, ___centerY_3) + sizeof(Object_t),
	offsetof(EyewearCalibrationReading_t1080, ___unused_4) + sizeof(Object_t),
	offsetof(SmartTerrainTrackableBehaviour_t1084, ___mSmartTerrainTrackable_9),
	offsetof(SmartTerrainTrackableBehaviour_t1084, ___mDisableAutomaticUpdates_10),
	offsetof(SmartTerrainTrackableBehaviour_t1084, ___mMeshFilterToUpdate_11),
	offsetof(SmartTerrainTrackableBehaviour_t1084, ___mMeshColliderToUpdate_12),
	offsetof(SmartTerrainTrackerAbstractBehaviour_t305, ___mAutoInitTracker_2),
	offsetof(SmartTerrainTrackerAbstractBehaviour_t305, ___mAutoStartTracker_3),
	offsetof(SmartTerrainTrackerAbstractBehaviour_t305, ___mAutoInitBuilder_4),
	offsetof(SmartTerrainTrackerAbstractBehaviour_t305, ___mSceneUnitsToMillimeter_5),
	offsetof(SmartTerrainTrackerAbstractBehaviour_t305, ___mTrackerStarted_6),
	offsetof(SmartTerrainTrackerAbstractBehaviour_t305, ___mTrackerWasActiveBeforePause_7),
	offsetof(SmartTerrainTrackerAbstractBehaviour_t305, ___mTrackerWasActiveBeforeDisabling_8),
	offsetof(SurfaceAbstractBehaviour_t306, ___mSurface_13),
	offsetof(CylinderTargetAbstractBehaviour_t273, ___mCylinderTarget_20),
	offsetof(CylinderTargetAbstractBehaviour_t273, ___mTopDiameterRatio_21),
	offsetof(CylinderTargetAbstractBehaviour_t273, ___mBottomDiameterRatio_22),
	offsetof(CylinderTargetAbstractBehaviour_t273, ___mFrameIndex_23),
	offsetof(CylinderTargetAbstractBehaviour_t273, ___mUpdateFrameIndex_24),
	offsetof(CylinderTargetAbstractBehaviour_t273, ___mFutureScale_25),
	offsetof(StorageType_t1087, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(DatabaseLoadAbstractBehaviour_t275, ___mDatasetsLoaded_2),
	offsetof(DatabaseLoadAbstractBehaviour_t275, ___mDataSetsToLoad_3),
	offsetof(DatabaseLoadAbstractBehaviour_t275, ___mDataSetsToActivate_4),
	offsetof(DatabaseLoadAbstractBehaviour_t275, ___mExternalDatasetRoots_5),
	offsetof(RectangleData_t1089, ___leftTopX_0) + sizeof(Object_t),
	offsetof(RectangleData_t1089, ___leftTopY_1) + sizeof(Object_t),
	offsetof(RectangleData_t1089, ___rightBottomX_2) + sizeof(Object_t),
	offsetof(RectangleData_t1089, ___rightBottomY_3) + sizeof(Object_t),
	offsetof(RectangleIntData_t1090, ___leftTopX_0) + sizeof(Object_t),
	offsetof(RectangleIntData_t1090, ___leftTopY_1) + sizeof(Object_t),
	offsetof(RectangleIntData_t1090, ___rightBottomX_2) + sizeof(Object_t),
	offsetof(RectangleIntData_t1090, ___rightBottomY_3) + sizeof(Object_t),
	offsetof(OrientedBoundingBox_t1091, ___U3CCenterU3Ek__BackingField_0) + sizeof(Object_t),
	offsetof(OrientedBoundingBox_t1091, ___U3CHalfExtentsU3Ek__BackingField_1) + sizeof(Object_t),
	offsetof(OrientedBoundingBox_t1091, ___U3CRotationU3Ek__BackingField_2) + sizeof(Object_t),
	offsetof(OrientedBoundingBox3D_t1092, ___U3CCenterU3Ek__BackingField_0) + sizeof(Object_t),
	offsetof(OrientedBoundingBox3D_t1092, ___U3CHalfExtentsU3Ek__BackingField_1) + sizeof(Object_t),
	offsetof(OrientedBoundingBox3D_t1092, ___U3CRotationYU3Ek__BackingField_2) + sizeof(Object_t),
	offsetof(BehaviourComponentFactory_t1094_StaticFields, ___sInstance_0),
	offsetof(CloudRecoImageTargetImpl_t1096, ___mSize_2),
	offsetof(CylinderTargetImpl_t1097, ___mSideLength_4),
	offsetof(CylinderTargetImpl_t1097, ___mTopDiameter_5),
	offsetof(CylinderTargetImpl_t1097, ___mBottomDiameter_6),
	offsetof(ImageTargetType_t1098, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(ImageTargetData_t1099, ___id_0) + sizeof(Object_t),
	offsetof(ImageTargetData_t1099, ___size_1) + sizeof(Object_t),
	offsetof(FrameQuality_t1100, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(CameraDeviceImpl_t1102, ___mCameraImages_1),
	offsetof(CameraDeviceImpl_t1102, ___mForcedCameraFormats_2),
	offsetof(CameraDeviceImpl_t1102_StaticFields, ___mWebCam_3),
	offsetof(CameraDeviceImpl_t1102, ___mCameraReady_4),
	offsetof(CameraDeviceImpl_t1102, ___mIsDirty_5),
	offsetof(CameraDeviceImpl_t1102, ___mCameraDirection_6),
	offsetof(CameraDeviceImpl_t1102, ___mCameraDeviceMode_7),
	offsetof(CameraDeviceImpl_t1102, ___mVideoModeData_8),
	offsetof(CameraDeviceImpl_t1102, ___mVideoModeDataNeedsUpdate_9),
	offsetof(CameraDeviceImpl_t1102, ___mHasCameraDeviceModeBeenSet_10),
	offsetof(DataSetImpl_t1073, ___mDataSetPtr_0),
	offsetof(DataSetImpl_t1073, ___mPath_1),
	offsetof(DataSetImpl_t1073, ___mStorageType_2),
	offsetof(DataSetImpl_t1073, ___mTrackablesDict_3),
	offsetof(WordTemplateMode_t1107, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(PIXEL_FORMAT_t1108, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(ImageImpl_t1110, ___mWidth_0),
	offsetof(ImageImpl_t1110, ___mHeight_1),
	offsetof(ImageImpl_t1110, ___mStride_2),
	offsetof(ImageImpl_t1110, ___mBufferWidth_3),
	offsetof(ImageImpl_t1110, ___mBufferHeight_4),
	offsetof(ImageImpl_t1110, ___mPixelFormat_5),
	offsetof(ImageImpl_t1110, ___mData_6),
	offsetof(ImageImpl_t1110, ___mUnmanagedData_7),
	offsetof(ImageImpl_t1110, ___mDataSet_8),
	offsetof(ImageImpl_t1110, ___mPixel32_9),
	offsetof(ImageTargetBuilderImpl_t1111, ___mTrackableSource_0),
	offsetof(ImageTargetImpl_t1113, ___mImageTargetType_4),
	offsetof(ImageTargetImpl_t1113, ___mVirtualButtons_5),
	offsetof(Tracker_t1115, ___U3CIsActiveU3Ek__BackingField_0),
	offsetof(ObjectTrackerImpl_t1116, ___mActiveDataSets_1),
	offsetof(ObjectTrackerImpl_t1116, ___mDataSets_2),
	offsetof(ObjectTrackerImpl_t1116, ___mImageTargetBuilder_3),
	offsetof(ObjectTrackerImpl_t1116, ___mTargetFinder_4),
	offsetof(MarkerImpl_t1120, ___mSize_2),
	offsetof(MarkerImpl_t1120, ___U3CMarkerIDU3Ek__BackingField_3),
	offsetof(MarkerTrackerImpl_t1122, ___mMarkerDict_1),
	0,
	offsetof(NullWebCamTexAdaptor_t1126, ___mTexture_1),
	offsetof(NullWebCamTexAdaptor_t1126, ___mPseudoPlaying_2),
	offsetof(NullWebCamTexAdaptor_t1126, ___mMsBetweenFrames_3),
	offsetof(NullWebCamTexAdaptor_t1126, ___mLastFrame_4),
	offsetof(PlayModeEditorUtility_t1128_StaticFields, ___sInstance_0),
	offsetof(PremiumObjectFactory_t1131_StaticFields, ___sInstance_0),
	offsetof(VuforiaManager_t472_StaticFields, ___sInstance_0),
	offsetof(VuforiaManagerImpl_t1148, ___mWorldCenterMode_1),
	offsetof(VuforiaManagerImpl_t1148, ___mWorldCenter_2),
	offsetof(VuforiaManagerImpl_t1148, ___mARCameraTransform_3),
	offsetof(VuforiaManagerImpl_t1148, ___mCentralAnchorPoint_4),
	offsetof(VuforiaManagerImpl_t1148, ___mParentAnchorPoint_5),
	offsetof(VuforiaManagerImpl_t1148, ___mTrackableResultDataArray_6),
	offsetof(VuforiaManagerImpl_t1148, ___mWordDataArray_7),
	offsetof(VuforiaManagerImpl_t1148, ___mWordResultDataArray_8),
	offsetof(VuforiaManagerImpl_t1148, ___mTrackableFoundQueue_9),
	offsetof(VuforiaManagerImpl_t1148, ___mImageHeaderData_10),
	offsetof(VuforiaManagerImpl_t1148, ___mNumImageHeaders_11),
	offsetof(VuforiaManagerImpl_t1148, ___mInjectedFrameIdx_12),
	offsetof(VuforiaManagerImpl_t1148, ___mLastProcessedFrameStatePtr_13),
	offsetof(VuforiaManagerImpl_t1148, ___mInitialized_14),
	offsetof(VuforiaManagerImpl_t1148, ___mPaused_15),
	offsetof(VuforiaManagerImpl_t1148, ___mFrameState_16),
	offsetof(VuforiaManagerImpl_t1148, ___mAutoRotationState_17),
	offsetof(VuforiaManagerImpl_t1148, ___mVideoBackgroundNeedsRedrawing_18),
	offsetof(VuforiaManagerImpl_t1148, ___mDiscardStatesForRendering_19),
	offsetof(VuforiaManagerImpl_t1148, ___U3CVideoBackgroundTextureSetU3Ek__BackingField_20),
	offsetof(PoseData_t1133, ___position_0) + sizeof(Object_t),
	offsetof(PoseData_t1133, ___orientation_1) + sizeof(Object_t),
	offsetof(PoseData_t1133, ___unused_2) + sizeof(Object_t),
	offsetof(TrackableResultData_t1134, ___pose_0) + sizeof(Object_t),
	offsetof(TrackableResultData_t1134, ___status_1) + sizeof(Object_t),
	offsetof(TrackableResultData_t1134, ___id_2) + sizeof(Object_t),
	offsetof(VirtualButtonData_t1135, ___id_0) + sizeof(Object_t),
	offsetof(VirtualButtonData_t1135, ___isPressed_1) + sizeof(Object_t),
	offsetof(Obb2D_t1136, ___center_0) + sizeof(Object_t),
	offsetof(Obb2D_t1136, ___halfExtents_1) + sizeof(Object_t),
	offsetof(Obb2D_t1136, ___rotation_2) + sizeof(Object_t),
	offsetof(Obb2D_t1136, ___unused_3) + sizeof(Object_t),
	offsetof(Obb3D_t1137, ___center_0) + sizeof(Object_t),
	offsetof(Obb3D_t1137, ___halfExtents_1) + sizeof(Object_t),
	offsetof(Obb3D_t1137, ___rotationZ_2) + sizeof(Object_t),
	offsetof(Obb3D_t1137, ___unused_3) + sizeof(Object_t),
	offsetof(WordResultData_t1138, ___pose_0) + sizeof(Object_t),
	offsetof(WordResultData_t1138, ___status_1) + sizeof(Object_t),
	offsetof(WordResultData_t1138, ___id_2) + sizeof(Object_t),
	offsetof(WordResultData_t1138, ___orientedBoundingBox_3) + sizeof(Object_t),
	offsetof(WordData_t1139, ___stringValue_0) + sizeof(Object_t),
	offsetof(WordData_t1139, ___id_1) + sizeof(Object_t),
	offsetof(WordData_t1139, ___size_2) + sizeof(Object_t),
	offsetof(WordData_t1139, ___unused_3) + sizeof(Object_t),
	offsetof(ImageHeaderData_t1140, ___data_0) + sizeof(Object_t),
	offsetof(ImageHeaderData_t1140, ___width_1) + sizeof(Object_t),
	offsetof(ImageHeaderData_t1140, ___height_2) + sizeof(Object_t),
	offsetof(ImageHeaderData_t1140, ___stride_3) + sizeof(Object_t),
	offsetof(ImageHeaderData_t1140, ___bufferWidth_4) + sizeof(Object_t),
	offsetof(ImageHeaderData_t1140, ___bufferHeight_5) + sizeof(Object_t),
	offsetof(ImageHeaderData_t1140, ___format_6) + sizeof(Object_t),
	offsetof(ImageHeaderData_t1140, ___reallocate_7) + sizeof(Object_t),
	offsetof(ImageHeaderData_t1140, ___updated_8) + sizeof(Object_t),
	offsetof(MeshData_t1141, ___positionsArray_0) + sizeof(Object_t),
	offsetof(MeshData_t1141, ___normalsArray_1) + sizeof(Object_t),
	offsetof(MeshData_t1141, ___triangleIdxArray_2) + sizeof(Object_t),
	offsetof(MeshData_t1141, ___numVertexValues_3) + sizeof(Object_t),
	offsetof(MeshData_t1141, ___hasNormals_4) + sizeof(Object_t),
	offsetof(MeshData_t1141, ___numTriangleIndices_5) + sizeof(Object_t),
	offsetof(MeshData_t1141, ___unused_6) + sizeof(Object_t),
	offsetof(SmartTerrainRevisionData_t1142, ___id_0) + sizeof(Object_t),
	offsetof(SmartTerrainRevisionData_t1142, ___revision_1) + sizeof(Object_t),
	offsetof(SurfaceData_t1143, ___meshBoundaryArray_0) + sizeof(Object_t),
	offsetof(SurfaceData_t1143, ___meshData_1) + sizeof(Object_t),
	offsetof(SurfaceData_t1143, ___navMeshData_2) + sizeof(Object_t),
	offsetof(SurfaceData_t1143, ___boundingBox_3) + sizeof(Object_t),
	offsetof(SurfaceData_t1143, ___localPose_4) + sizeof(Object_t),
	offsetof(SurfaceData_t1143, ___id_5) + sizeof(Object_t),
	offsetof(SurfaceData_t1143, ___parentID_6) + sizeof(Object_t),
	offsetof(SurfaceData_t1143, ___numBoundaryIndices_7) + sizeof(Object_t),
	offsetof(SurfaceData_t1143, ___revision_8) + sizeof(Object_t),
	offsetof(PropData_t1144, ___meshData_0) + sizeof(Object_t),
	offsetof(PropData_t1144, ___id_1) + sizeof(Object_t),
	offsetof(PropData_t1144, ___parentID_2) + sizeof(Object_t),
	offsetof(PropData_t1144, ___boundingBox_3) + sizeof(Object_t),
	offsetof(PropData_t1144, ___localPosition_4) + sizeof(Object_t),
	offsetof(PropData_t1144, ___localPose_5) + sizeof(Object_t),
	offsetof(PropData_t1144, ___revision_6) + sizeof(Object_t),
	offsetof(PropData_t1144, ___unused_7) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___trackableDataArray_0) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___vbDataArray_1) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___wordResultArray_2) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___newWordDataArray_3) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___propTrackableDataArray_4) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___smartTerrainRevisionsArray_5) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___updatedSurfacesArray_6) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___updatedPropsArray_7) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___numTrackableResults_8) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___numVirtualButtonResults_9) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___frameIndex_10) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___numWordResults_11) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___numNewWords_12) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___numPropTrackableResults_13) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___numSmartTerrainRevisions_14) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___numUpdatedSurfaces_15) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___numUpdatedProps_16) + sizeof(Object_t),
	offsetof(FrameState_t1145, ___unused_17) + sizeof(Object_t),
	offsetof(AutoRotationState_t1146, ___setOnPause_0) + sizeof(Object_t),
	offsetof(AutoRotationState_t1146, ___autorotateToPortrait_1) + sizeof(Object_t),
	offsetof(AutoRotationState_t1146, ___autorotateToPortraitUpsideDown_2) + sizeof(Object_t),
	offsetof(AutoRotationState_t1146, ___autorotateToLandscapeLeft_3) + sizeof(Object_t),
	offsetof(AutoRotationState_t1146, ___autorotateToLandscapeRight_4) + sizeof(Object_t),
	offsetof(U3CU3Ec__DisplayClass3_t1147, ___id_0),
	offsetof(VuforiaRenderer_t1158_StaticFields, ___sInstance_0),
	offsetof(FpsHint_t1154, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(VideoBackgroundReflection_t1155, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(VideoBGCfgData_t1156, ___position_0) + sizeof(Object_t),
	offsetof(VideoBGCfgData_t1156, ___size_1) + sizeof(Object_t),
	offsetof(VideoBGCfgData_t1156, ___enabled_2) + sizeof(Object_t),
	offsetof(VideoBGCfgData_t1156, ___reflection_3) + sizeof(Object_t),
	offsetof(Vec2I_t1157, ___x_0) + sizeof(Object_t),
	offsetof(Vec2I_t1157, ___y_1) + sizeof(Object_t),
	offsetof(VideoTextureInfo_t1049, ___textureSize_0) + sizeof(Object_t),
	offsetof(VideoTextureInfo_t1049, ___imageSize_1) + sizeof(Object_t),
	offsetof(VuforiaRendererImpl_t1160, ___mVideoBGConfig_1),
	offsetof(VuforiaRendererImpl_t1160, ___mVideoBGConfigSet_2),
	offsetof(VuforiaRendererImpl_t1160, ___mVideoBackgroundTexture_3),
	offsetof(VuforiaRendererImpl_t1160, ___mBackgroundTextureHasChanged_4),
	offsetof(VuforiaRendererImpl_t1160, ___mLastSetReflection_5),
	offsetof(RenderEvent_t1159, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(VuforiaUnityImpl_t1161_StaticFields, ___mRendererDirty_0),
	offsetof(SmartTerrainTrackableImpl_t1162, ___mChildren_2),
	offsetof(SmartTerrainTrackableImpl_t1162, ___mMesh_3),
	offsetof(SmartTerrainTrackableImpl_t1162, ___mMeshRevision_4),
	offsetof(SmartTerrainTrackableImpl_t1162, ___mLocalPose_5),
	offsetof(SmartTerrainTrackableImpl_t1162, ___U3CParentU3Ek__BackingField_6),
	offsetof(SurfaceImpl_t1164, ___mNavMesh_7),
	offsetof(SurfaceImpl_t1164, ___mMeshBoundaries_8),
	offsetof(SurfaceImpl_t1164, ___mBoundingBox_9),
	offsetof(SurfaceImpl_t1164, ___mSurfaceArea_10),
	offsetof(SurfaceImpl_t1164, ___mAreaNeedsUpdate_11),
	offsetof(SmartTerrainBuilderImpl_t1165, ___mReconstructionBehaviours_0),
	offsetof(SmartTerrainBuilderImpl_t1165, ___mIsInitialized_1),
	offsetof(PropImpl_t1167, ___mOrientedBoundingBox3D_7),
	offsetof(SmartTerrainTrackerImpl_t1169, ___mScaleToMillimeter_1),
	offsetof(SmartTerrainTrackerImpl_t1169, ___mSmartTerrainBuilder_2),
	offsetof(TextTrackerImpl_t1172, ___mWordList_1),
	offsetof(UpDirection_t1171, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(TypeMapping_t1174_StaticFields, ___sTypes_0),
	offsetof(WebCamTexAdaptorImpl_t1176, ___mWebCamTexture_0),
	offsetof(WordImpl_t1177, ___mText_2),
	offsetof(WordImpl_t1177, ___mSize_3),
	offsetof(WordImpl_t1177, ___mLetterMask_4),
	offsetof(WordImpl_t1177, ___mLetterImageHeader_5),
	offsetof(WordImpl_t1177, ___mLetterBoundingBoxes_6),
	offsetof(WordPrefabCreationMode_t1179, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(WordManagerImpl_t1181, ___mTrackedWords_1),
	offsetof(WordManagerImpl_t1181, ___mNewWords_2),
	offsetof(WordManagerImpl_t1181, ___mLostWords_3),
	offsetof(WordManagerImpl_t1181, ___mActiveWordBehaviours_4),
	offsetof(WordManagerImpl_t1181, ___mWordBehavioursMarkedForDeletion_5),
	offsetof(WordManagerImpl_t1181, ___mWaitingQueue_6),
	offsetof(WordManagerImpl_t1181, ___mWordBehaviours_7),
	offsetof(WordManagerImpl_t1181, ___mAutomaticTemplate_8),
	offsetof(WordManagerImpl_t1181, ___mMaxInstances_9),
	offsetof(WordManagerImpl_t1181, ___mWordPrefabCreationMode_10),
	offsetof(WordResultImpl_t1189, ___mObb_0),
	offsetof(WordResultImpl_t1189, ___mPosition_1),
	offsetof(WordResultImpl_t1189, ___mOrientation_2),
	offsetof(WordResultImpl_t1189, ___mWord_3),
	offsetof(WordResultImpl_t1189, ___mStatus_4),
	offsetof(VuforiaWrapper_t1195_StaticFields, ___sWrapper_0),
	offsetof(KeepAliveAbstractBehaviour_t291, ___mKeepARCameraAlive_2),
	offsetof(KeepAliveAbstractBehaviour_t291, ___mKeepTrackableBehavioursAlive_3),
	offsetof(KeepAliveAbstractBehaviour_t291, ___mKeepTextRecoBehaviourAlive_4),
	offsetof(KeepAliveAbstractBehaviour_t291, ___mKeepUDTBuildingBehaviourAlive_5),
	offsetof(KeepAliveAbstractBehaviour_t291, ___mKeepCloudRecoBehaviourAlive_6),
	offsetof(KeepAliveAbstractBehaviour_t291, ___mKeepSmartTerrainAlive_7),
	offsetof(KeepAliveAbstractBehaviour_t291_StaticFields, ___sKeepAliveBehaviour_8),
	offsetof(KeepAliveAbstractBehaviour_t291, ___mHandlers_9),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mHasInitialized_2),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mSmartTerrainEventHandlers_3),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mOnInitialized_4),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mOnPropCreated_5),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mOnPropUpdated_6),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mOnPropDeleted_7),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mOnSurfaceCreated_8),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mOnSurfaceUpdated_9),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mOnSurfaceDeleted_10),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mInitializedInEditor_11),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mMaximumExtentEnabled_12),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mMaximumExtent_13),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mAutomaticStart_14),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mNavMeshUpdates_15),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mNavMeshPadding_16),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mReconstruction_17),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mSurfaces_18),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mActiveSurfaceBehaviours_19),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mProps_20),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mActivePropBehaviours_21),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mPreviouslySetWorldCenterSurfaceTemplate_22),
	offsetof(ReconstructionAbstractBehaviour_t301, ___mIgnoreNextUpdate_23),
	offsetof(PropAbstractBehaviour_t300, ___mProp_13),
	offsetof(PropAbstractBehaviour_t300, ___mBoxColliderToUpdate_14),
	offsetof(StateManagerImpl_t1206, ___mTrackableBehaviours_0),
	offsetof(StateManagerImpl_t1206, ___mAutomaticallyCreatedBehaviours_1),
	offsetof(StateManagerImpl_t1206, ___mBehavioursMarkedForDeletion_2),
	offsetof(StateManagerImpl_t1206, ___mActiveTrackableBehaviours_3),
	offsetof(StateManagerImpl_t1206, ___mWordManager_4),
	offsetof(StateManagerImpl_t1206, ___mCameraPositioningHelper_5),
	offsetof(InitState_t1209, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(UpdateState_t1210, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(FilterMode_t1211, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(TargetSearchResult_t1212, ___TargetName_0) + sizeof(Object_t),
	offsetof(TargetSearchResult_t1212, ___UniqueTargetId_1) + sizeof(Object_t),
	offsetof(TargetSearchResult_t1212, ___TargetSize_2) + sizeof(Object_t),
	offsetof(TargetSearchResult_t1212, ___MetaData_3) + sizeof(Object_t),
	offsetof(TargetSearchResult_t1212, ___TrackingRating_4) + sizeof(Object_t),
	offsetof(TargetSearchResult_t1212, ___TargetSearchResultPtr_5) + sizeof(Object_t),
	offsetof(TargetFinderImpl_t1215, ___mTargetFinderStatePtr_0),
	offsetof(TargetFinderImpl_t1215, ___mTargetFinderState_1),
	offsetof(TargetFinderImpl_t1215, ___mNewResults_2),
	offsetof(TargetFinderImpl_t1215, ___mImageTargets_3),
	offsetof(TargetFinderState_t1213, ___IsRequesting_0) + sizeof(Object_t),
	offsetof(TargetFinderState_t1213, ___UpdateState_1) + sizeof(Object_t),
	offsetof(TargetFinderState_t1213, ___ResultCount_2) + sizeof(Object_t),
	offsetof(TargetFinderState_t1213, ___unused_3) + sizeof(Object_t),
	offsetof(InternalTargetSearchResult_t1214, ___TargetNamePtr_0) + sizeof(Object_t),
	offsetof(InternalTargetSearchResult_t1214, ___UniqueTargetIdPtr_1) + sizeof(Object_t),
	offsetof(InternalTargetSearchResult_t1214, ___MetaDataPtr_2) + sizeof(Object_t),
	offsetof(InternalTargetSearchResult_t1214, ___TargetSearchResultPtr_3) + sizeof(Object_t),
	offsetof(InternalTargetSearchResult_t1214, ___TargetSize_4) + sizeof(Object_t),
	offsetof(InternalTargetSearchResult_t1214, ___TrackingRating_5) + sizeof(Object_t),
	offsetof(TrackableSourceImpl_t1218, ___U3CTrackableSourcePtrU3Ek__BackingField_0),
	offsetof(TextureRenderer_t1219, ___mTextureBufferCamera_0),
	offsetof(TextureRenderer_t1219, ___mTextureWidth_1),
	offsetof(TextureRenderer_t1219, ___mTextureHeight_2),
	offsetof(TrackerManager_t1220_StaticFields, ___mInstance_0),
	offsetof(TrackerManagerImpl_t1221, ___mObjectTracker_1),
	offsetof(TrackerManagerImpl_t1221, ___mMarkerTracker_2),
	offsetof(TrackerManagerImpl_t1221, ___mTextTracker_3),
	offsetof(TrackerManagerImpl_t1221, ___mSmartTerrainTracker_4),
	offsetof(TrackerManagerImpl_t1221, ___mStateManager_5),
	0,
	offsetof(Sensitivity_t1222, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(VirtualButtonImpl_t1224, ___mName_1),
	offsetof(VirtualButtonImpl_t1224, ___mID_2),
	offsetof(VirtualButtonImpl_t1224, ___mArea_3),
	offsetof(VirtualButtonImpl_t1224, ___mIsEnabled_4),
	offsetof(VirtualButtonImpl_t1224, ___mParentImageTarget_5),
	offsetof(VirtualButtonImpl_t1224, ___mParentDataSet_6),
	offsetof(WebCamImpl_t1105, ___mARCameras_0),
	offsetof(WebCamImpl_t1105, ___mOriginalCameraCullMask_1),
	offsetof(WebCamImpl_t1105, ___mWebCamTexture_2),
	offsetof(WebCamImpl_t1105, ___mVideoModeData_3),
	offsetof(WebCamImpl_t1105, ___mVideoTextureInfo_4),
	offsetof(WebCamImpl_t1105, ___mTextureRenderer_5),
	offsetof(WebCamImpl_t1105, ___mBufferReadTexture_6),
	offsetof(WebCamImpl_t1105, ___mReadPixelsRect_7),
	offsetof(WebCamImpl_t1105, ___mWebCamProfile_8),
	offsetof(WebCamImpl_t1105, ___mFlipHorizontally_9),
	offsetof(WebCamImpl_t1105, ___mIsDirty_10),
	offsetof(WebCamImpl_t1105, ___mLastFrameIdx_11),
	offsetof(WebCamImpl_t1105, ___mRenderTextureLayer_12),
	offsetof(WebCamImpl_t1105, ___mWebcamPlaying_13),
	offsetof(WebCamImpl_t1105, ___U3CIsTextureSizeAvailableU3Ek__BackingField_14),
	offsetof(WebCamProfile_t1229, ___mProfileCollection_0),
	offsetof(ProfileData_t1226, ___RequestedTextureSize_0) + sizeof(Object_t),
	offsetof(ProfileData_t1226, ___ResampledTextureSize_1) + sizeof(Object_t),
	offsetof(ProfileData_t1226, ___RequestedFPS_2) + sizeof(Object_t),
	offsetof(ProfileCollection_t1227, ___DefaultProfile_0) + sizeof(Object_t),
	offsetof(ProfileCollection_t1227, ___Profiles_1) + sizeof(Object_t),
	offsetof(ImageTargetAbstractBehaviour_t285, ___mAspectRatio_20),
	offsetof(ImageTargetAbstractBehaviour_t285, ___mImageTargetType_21),
	offsetof(ImageTargetAbstractBehaviour_t285, ___mImageTarget_22),
	offsetof(ImageTargetAbstractBehaviour_t285, ___mVirtualButtonBehaviours_23),
	offsetof(MarkerAbstractBehaviour_t293, ___mMarkerID_9),
	offsetof(MarkerAbstractBehaviour_t293, ___mMarker_10),
	offsetof(MaskOutAbstractBehaviour_t295, ___maskMaterial_2),
	offsetof(MultiTargetAbstractBehaviour_t297, ___mMultiTarget_20),
	offsetof(InitError_t1233, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(VuforiaHint_t1234, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(StorageType_t1235, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(VuforiaAbstractBehaviour_t321, ___VuforiaLicenseKey_2),
	offsetof(VuforiaAbstractBehaviour_t321, ___mCameraOffset_3),
	offsetof(VuforiaAbstractBehaviour_t321, ___mSkewFrustum_4),
	offsetof(VuforiaAbstractBehaviour_t321, ___CameraDeviceModeSetting_5),
	offsetof(VuforiaAbstractBehaviour_t321, ___MaxSimultaneousImageTargets_6),
	offsetof(VuforiaAbstractBehaviour_t321, ___MaxSimultaneousObjectTargets_7),
	offsetof(VuforiaAbstractBehaviour_t321, ___UseDelayedLoadingObjectTargets_8),
	offsetof(VuforiaAbstractBehaviour_t321, ___CameraDirection_9),
	offsetof(VuforiaAbstractBehaviour_t321, ___MirrorVideoBackground_10),
	offsetof(VuforiaAbstractBehaviour_t321, ___mWorldCenterMode_11),
	offsetof(VuforiaAbstractBehaviour_t321, ___mWorldCenter_12),
	offsetof(VuforiaAbstractBehaviour_t321, ___mCentralAnchorPoint_13),
	offsetof(VuforiaAbstractBehaviour_t321, ___mParentAnchorPoint_14),
	offsetof(VuforiaAbstractBehaviour_t321, ___mPrimaryCamera_15),
	offsetof(VuforiaAbstractBehaviour_t321, ___mPrimaryCameraOriginalRect_16),
	offsetof(VuforiaAbstractBehaviour_t321, ___mSecondaryCamera_17),
	offsetof(VuforiaAbstractBehaviour_t321, ___mSecondaryCameraOriginalRect_18),
	offsetof(VuforiaAbstractBehaviour_t321, ___mSecondaryCameraDisabledLocally_19),
	offsetof(VuforiaAbstractBehaviour_t321, ___mUsingHeadset_20),
	offsetof(VuforiaAbstractBehaviour_t321, ___mHeadsetID_21),
	offsetof(VuforiaAbstractBehaviour_t321, ___mEyewearUserCalibrationProfileId_22),
	offsetof(VuforiaAbstractBehaviour_t321, ___mSynchronizePoseUpdates_23),
	offsetof(VuforiaAbstractBehaviour_t321, ___mTrackerEventHandlers_24),
	offsetof(VuforiaAbstractBehaviour_t321, ___mVideoBgEventHandlers_25),
	offsetof(VuforiaAbstractBehaviour_t321, ___mOnVuforiaInitError_26),
	offsetof(VuforiaAbstractBehaviour_t321, ___mOnVuforiaInitialized_27),
	offsetof(VuforiaAbstractBehaviour_t321, ___mOnVuforiaStarted_28),
	offsetof(VuforiaAbstractBehaviour_t321, ___mOnTrackablesUpdated_29),
	offsetof(VuforiaAbstractBehaviour_t321, ___mRenderOnUpdate_30),
	offsetof(VuforiaAbstractBehaviour_t321, ___mOnPause_31),
	offsetof(VuforiaAbstractBehaviour_t321, ___mOnBackgroundTextureChanged_32),
	offsetof(VuforiaAbstractBehaviour_t321, ___mStartHasBeenInvoked_33),
	offsetof(VuforiaAbstractBehaviour_t321, ___mHasStarted_34),
	offsetof(VuforiaAbstractBehaviour_t321, ___mFailedToInitialize_35),
	offsetof(VuforiaAbstractBehaviour_t321, ___mBackgroundTextureHasChanged_36),
	offsetof(VuforiaAbstractBehaviour_t321, ___mInitError_37),
	offsetof(VuforiaAbstractBehaviour_t321, ___mCameraConfiguration_38),
	offsetof(VuforiaAbstractBehaviour_t321, ___mClearMaterial_39),
	offsetof(VuforiaAbstractBehaviour_t321, ___mHasStartedOnce_40),
	offsetof(VuforiaAbstractBehaviour_t321, ___mWasEnabledBeforePause_41),
	offsetof(VuforiaAbstractBehaviour_t321, ___mObjectTrackerWasActiveBeforePause_42),
	offsetof(VuforiaAbstractBehaviour_t321, ___mObjectTrackerWasActiveBeforeDisabling_43),
	offsetof(VuforiaAbstractBehaviour_t321, ___mMarkerTrackerWasActiveBeforePause_44),
	offsetof(VuforiaAbstractBehaviour_t321, ___mMarkerTrackerWasActiveBeforeDisabling_45),
	offsetof(VuforiaAbstractBehaviour_t321, ___mLastUpdatedFrame_46),
	offsetof(VuforiaAbstractBehaviour_t321, ___mTrackersRequestedToDeinit_47),
	offsetof(WorldCenterMode_t1237, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(VuforiaRuntimeUtilities_t468_StaticFields, ___sWebCamUsed_0),
	offsetof(WebCamUsed_t1242, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(SurfaceUtilities_t460_StaticFields, ___mScreenOrientation_0),
	offsetof(TextRecoAbstractBehaviour_t308, ___mHasInitialized_2),
	offsetof(TextRecoAbstractBehaviour_t308, ___mTrackerWasActiveBeforePause_3),
	offsetof(TextRecoAbstractBehaviour_t308, ___mTrackerWasActiveBeforeDisabling_4),
	offsetof(TextRecoAbstractBehaviour_t308, ___mWordListFile_5),
	offsetof(TextRecoAbstractBehaviour_t308, ___mCustomWordListFile_6),
	offsetof(TextRecoAbstractBehaviour_t308, ___mAdditionalCustomWords_7),
	offsetof(TextRecoAbstractBehaviour_t308, ___mFilterMode_8),
	offsetof(TextRecoAbstractBehaviour_t308, ___mFilterListFile_9),
	offsetof(TextRecoAbstractBehaviour_t308, ___mAdditionalFilterWords_10),
	offsetof(TextRecoAbstractBehaviour_t308, ___mWordPrefabCreationMode_11),
	offsetof(TextRecoAbstractBehaviour_t308, ___mMaximumWordInstances_12),
	offsetof(TextRecoAbstractBehaviour_t308, ___mTextRecoEventHandlers_13),
	offsetof(SimpleTargetData_t1244, ___id_0) + sizeof(Object_t),
	offsetof(SimpleTargetData_t1244, ___unused_1) + sizeof(Object_t),
	offsetof(UserDefinedTargetBuildingAbstractBehaviour_t313, ___mObjectTracker_2),
	offsetof(UserDefinedTargetBuildingAbstractBehaviour_t313, ___mLastFrameQuality_3),
	offsetof(UserDefinedTargetBuildingAbstractBehaviour_t313, ___mCurrentlyScanning_4),
	offsetof(UserDefinedTargetBuildingAbstractBehaviour_t313, ___mWasScanningBeforeDisable_5),
	offsetof(UserDefinedTargetBuildingAbstractBehaviour_t313, ___mCurrentlyBuilding_6),
	offsetof(UserDefinedTargetBuildingAbstractBehaviour_t313, ___mWasBuildingBeforeDisable_7),
	offsetof(UserDefinedTargetBuildingAbstractBehaviour_t313, ___mOnInitializedCalled_8),
	offsetof(UserDefinedTargetBuildingAbstractBehaviour_t313, ___mHandlers_9),
	offsetof(UserDefinedTargetBuildingAbstractBehaviour_t313, ___StopTrackerWhileScanning_10),
	offsetof(UserDefinedTargetBuildingAbstractBehaviour_t313, ___StartScanningAutomatically_11),
	offsetof(UserDefinedTargetBuildingAbstractBehaviour_t313, ___StopScanningWhenFinshedBuilding_12),
	offsetof(VideoBackgroundAbstractBehaviour_t315, ___mClearBuffers_2),
	offsetof(VideoBackgroundAbstractBehaviour_t315, ___mSkipStateUpdates_3),
	offsetof(VideoBackgroundAbstractBehaviour_t315, ___mVuforiaAbstractBehaviour_4),
	offsetof(VideoBackgroundAbstractBehaviour_t315, ___mCamera_5),
	offsetof(VideoBackgroundAbstractBehaviour_t315, ___mBackgroundBehaviour_6),
	offsetof(VideoBackgroundAbstractBehaviour_t315, ___mStereoDepth_7),
	offsetof(VideoBackgroundAbstractBehaviour_t315, ___mDisabledMeshRenderers_8),
	offsetof(VideoTextureRendererAbstractBehaviour_t317, ___mTexture_2),
	offsetof(VideoTextureRendererAbstractBehaviour_t317, ___mVideoBgConfigChanged_3),
	offsetof(VideoTextureRendererAbstractBehaviour_t317, ___mNativeTextureID_4),
	0,
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mName_3),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mSensitivity_4),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mHasUpdatedPose_5),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mPrevTransform_6),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mPrevParent_7),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mSensitivityDirty_8),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mPreviouslyEnabled_9),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mPressed_10),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mHandlers_11),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mLeftTop_12),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mRightBottom_13),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mUnregisterOnDestroy_14),
	offsetof(VirtualButtonAbstractBehaviour_t319, ___mVirtualButton_15),
	offsetof(WebCamAbstractBehaviour_t323, ___RenderTextureLayer_2),
	offsetof(WebCamAbstractBehaviour_t323, ___mPlayModeRenderVideo_3),
	offsetof(WebCamAbstractBehaviour_t323, ___mDeviceNameSetInEditor_4),
	offsetof(WebCamAbstractBehaviour_t323, ___mFlipHorizontally_5),
	offsetof(WebCamAbstractBehaviour_t323, ___mTurnOffWebCam_6),
	offsetof(WebCamAbstractBehaviour_t323, ___mWebCamImpl_7),
	offsetof(WebCamAbstractBehaviour_t323, ___mBackgroundCameraInstance_8),
	offsetof(WordAbstractBehaviour_t327, ___mMode_9),
	offsetof(WordAbstractBehaviour_t327, ___mSpecificWord_10),
	offsetof(WordAbstractBehaviour_t327, ___mWord_11),
	offsetof(WordFilterMode_t1248, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(U3CPrivateImplementationDetailsU3EU7BC4A9B53AU2D936FU2D421AU2D9398U2D89108BD15C01U7D_t1250_StaticFields, ___U24U24method0x60008eeU2D1_0),
	offsetof(KeyBuilder_t1399_StaticFields, ___rng_0),
	offsetof(SymmetricTransform_t1401, ___algo_0),
	offsetof(SymmetricTransform_t1401, ___encrypt_1),
	offsetof(SymmetricTransform_t1401, ___BlockSizeByte_2),
	offsetof(SymmetricTransform_t1401, ___temp_3),
	offsetof(SymmetricTransform_t1401, ___temp2_4),
	offsetof(SymmetricTransform_t1401, ___workBuff_5),
	offsetof(SymmetricTransform_t1401, ___workout_6),
	offsetof(SymmetricTransform_t1401, ___FeedBackByte_7),
	offsetof(SymmetricTransform_t1401, ___FeedBackIter_8),
	offsetof(SymmetricTransform_t1401, ___m_disposed_9),
	offsetof(SymmetricTransform_t1401, ___lastBlock_10),
	offsetof(SymmetricTransform_t1401, ____rng_11),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(Fallback_t1404, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(AesTransform_t1408, ___expandedKey_12),
	offsetof(AesTransform_t1408, ___Nk_13),
	offsetof(AesTransform_t1408, ___Nr_14),
	offsetof(AesTransform_t1408_StaticFields, ___Rcon_15),
	offsetof(AesTransform_t1408_StaticFields, ___SBox_16),
	offsetof(AesTransform_t1408_StaticFields, ___iSBox_17),
	offsetof(AesTransform_t1408_StaticFields, ___T0_18),
	offsetof(AesTransform_t1408_StaticFields, ___T1_19),
	offsetof(AesTransform_t1408_StaticFields, ___T2_20),
	offsetof(AesTransform_t1408_StaticFields, ___T3_21),
	offsetof(AesTransform_t1408_StaticFields, ___iT0_22),
	offsetof(AesTransform_t1408_StaticFields, ___iT1_23),
	offsetof(AesTransform_t1408_StaticFields, ___iT2_24),
	offsetof(AesTransform_t1408_StaticFields, ___iT3_25),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D0_0),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D1_1),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D2_2),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D3_3),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D4_4),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D5_5),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D6_6),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D7_7),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D8_8),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D9_9),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D10_10),
	offsetof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields, ___U24U24fieldU2D11_11),
	offsetof(BigInteger_t1428, ___length_0),
	offsetof(BigInteger_t1428, ___data_1),
	offsetof(BigInteger_t1428_StaticFields, ___smallPrimes_2),
	offsetof(BigInteger_t1428_StaticFields, ___rng_3),
	offsetof(Sign_t1426, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(ModulusRing_t1427, ___mod_0),
	offsetof(ModulusRing_t1427, ___constant_1),
	offsetof(ConfidenceFactor_t1430, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(ASN1_t1434, ___m_nTag_0),
	offsetof(ASN1_t1434, ___m_aValue_1),
	offsetof(ASN1_t1434, ___elist_2),
	offsetof(ContentInfo_t1437, ___contentType_0),
	offsetof(ContentInfo_t1437, ___content_1),
	offsetof(EncryptedData_t1438, ____version_0),
	offsetof(EncryptedData_t1438, ____content_1),
	offsetof(EncryptedData_t1438, ____encryptionAlgorithm_2),
	offsetof(EncryptedData_t1438, ____encrypted_3),
	offsetof(ARC4Managed_t1440, ___key_12),
	offsetof(ARC4Managed_t1440, ___state_13),
	offsetof(ARC4Managed_t1440, ___x_14),
	offsetof(ARC4Managed_t1440, ___y_15),
	offsetof(ARC4Managed_t1440, ___m_disposed_16),
	offsetof(KeyBuilder_t1443_StaticFields, ___rng_0),
	offsetof(MD2Managed_t1446, ___state_4),
	offsetof(MD2Managed_t1446, ___checksum_5),
	offsetof(MD2Managed_t1446, ___buffer_6),
	offsetof(MD2Managed_t1446, ___count_7),
	offsetof(MD2Managed_t1446, ___x_8),
	offsetof(MD2Managed_t1446_StaticFields, ___PI_SUBST_9),
	offsetof(PKCS1_t1447_StaticFields, ___emptySHA1_0),
	offsetof(PKCS1_t1447_StaticFields, ___emptySHA256_1),
	offsetof(PKCS1_t1447_StaticFields, ___emptySHA384_2),
	offsetof(PKCS1_t1447_StaticFields, ___emptySHA512_3),
	offsetof(PrivateKeyInfo_t1448, ____version_0),
	offsetof(PrivateKeyInfo_t1448, ____algorithm_1),
	offsetof(PrivateKeyInfo_t1448, ____key_2),
	offsetof(PrivateKeyInfo_t1448, ____list_3),
	offsetof(EncryptedPrivateKeyInfo_t1449, ____algorithm_0),
	offsetof(EncryptedPrivateKeyInfo_t1449, ____salt_1),
	offsetof(EncryptedPrivateKeyInfo_t1449, ____iterations_2),
	offsetof(EncryptedPrivateKeyInfo_t1449, ____data_3),
	offsetof(RC4_t1441_StaticFields, ___s_legalBlockSizes_10),
	offsetof(RC4_t1441_StaticFields, ___s_legalKeySizes_11),
	offsetof(RSAManaged_t1453, ___isCRTpossible_2),
	offsetof(RSAManaged_t1453, ___keyBlinding_3),
	offsetof(RSAManaged_t1453, ___keypairGenerated_4),
	offsetof(RSAManaged_t1453, ___m_disposed_5),
	offsetof(RSAManaged_t1453, ___d_6),
	offsetof(RSAManaged_t1453, ___p_7),
	offsetof(RSAManaged_t1453, ___q_8),
	offsetof(RSAManaged_t1453, ___dp_9),
	offsetof(RSAManaged_t1453, ___dq_10),
	offsetof(RSAManaged_t1453, ___qInv_11),
	offsetof(RSAManaged_t1453, ___n_12),
	offsetof(RSAManaged_t1453, ___e_13),
	offsetof(RSAManaged_t1453, ___KeyGenerated_14),
	offsetof(SafeBag_t1455, ____bagOID_0),
	offsetof(SafeBag_t1455, ____asn1_1),
	offsetof(PKCS12_t1457_StaticFields, ___recommendedIterationCount_0),
	offsetof(PKCS12_t1457, ____password_1),
	offsetof(PKCS12_t1457, ____keyBags_2),
	offsetof(PKCS12_t1457, ____secretBags_3),
	offsetof(PKCS12_t1457, ____certs_4),
	offsetof(PKCS12_t1457, ____keyBagsChanged_5),
	offsetof(PKCS12_t1457, ____secretBagsChanged_6),
	offsetof(PKCS12_t1457, ____certsChanged_7),
	offsetof(PKCS12_t1457, ____iterations_8),
	offsetof(PKCS12_t1457, ____safeBags_9),
	offsetof(PKCS12_t1457_StaticFields, ___password_max_length_10),
	offsetof(PKCS12_t1457_StaticFields, ___U3CU3Ef__switchU24map5_11),
	offsetof(PKCS12_t1457_StaticFields, ___U3CU3Ef__switchU24map6_12),
	offsetof(PKCS12_t1457_StaticFields, ___U3CU3Ef__switchU24map7_13),
	offsetof(PKCS12_t1457_StaticFields, ___U3CU3Ef__switchU24map8_14),
	offsetof(DeriveBytes_t1456_StaticFields, ___keyDiversifier_0),
	offsetof(DeriveBytes_t1456_StaticFields, ___ivDiversifier_1),
	offsetof(DeriveBytes_t1456_StaticFields, ___macDiversifier_2),
	offsetof(DeriveBytes_t1456, ____hashName_3),
	offsetof(DeriveBytes_t1456, ____iterations_4),
	offsetof(DeriveBytes_t1456, ____password_5),
	offsetof(DeriveBytes_t1456, ____salt_6),
	offsetof(X501_t1459_StaticFields, ___countryName_0),
	offsetof(X501_t1459_StaticFields, ___organizationName_1),
	offsetof(X501_t1459_StaticFields, ___organizationalUnitName_2),
	offsetof(X501_t1459_StaticFields, ___commonName_3),
	offsetof(X501_t1459_StaticFields, ___localityName_4),
	offsetof(X501_t1459_StaticFields, ___stateOrProvinceName_5),
	offsetof(X501_t1459_StaticFields, ___streetAddress_6),
	offsetof(X501_t1459_StaticFields, ___domainComponent_7),
	offsetof(X501_t1459_StaticFields, ___userid_8),
	offsetof(X501_t1459_StaticFields, ___email_9),
	offsetof(X501_t1459_StaticFields, ___dnQualifier_10),
	offsetof(X501_t1459_StaticFields, ___title_11),
	offsetof(X501_t1459_StaticFields, ___surname_12),
	offsetof(X501_t1459_StaticFields, ___givenName_13),
	offsetof(X501_t1459_StaticFields, ___initial_14),
	offsetof(X509Certificate_t1460, ___decoder_0),
	offsetof(X509Certificate_t1460, ___m_encodedcert_1),
	offsetof(X509Certificate_t1460, ___m_from_2),
	offsetof(X509Certificate_t1460, ___m_until_3),
	offsetof(X509Certificate_t1460, ___issuer_4),
	offsetof(X509Certificate_t1460, ___m_issuername_5),
	offsetof(X509Certificate_t1460, ___m_keyalgo_6),
	offsetof(X509Certificate_t1460, ___m_keyalgoparams_7),
	offsetof(X509Certificate_t1460, ___subject_8),
	offsetof(X509Certificate_t1460, ___m_subject_9),
	offsetof(X509Certificate_t1460, ___m_publickey_10),
	offsetof(X509Certificate_t1460, ___signature_11),
	offsetof(X509Certificate_t1460, ___m_signaturealgo_12),
	offsetof(X509Certificate_t1460, ___m_signaturealgoparams_13),
	offsetof(X509Certificate_t1460, ___certhash_14),
	offsetof(X509Certificate_t1460, ____rsa_15),
	offsetof(X509Certificate_t1460, ____dsa_16),
	offsetof(X509Certificate_t1460, ___version_17),
	offsetof(X509Certificate_t1460, ___serialnumber_18),
	offsetof(X509Certificate_t1460, ___issuerUniqueID_19),
	offsetof(X509Certificate_t1460, ___subjectUniqueID_20),
	offsetof(X509Certificate_t1460, ___extensions_21),
	offsetof(X509Certificate_t1460_StaticFields, ___encoding_error_22),
	offsetof(X509Certificate_t1460_StaticFields, ___U3CU3Ef__switchU24mapF_23),
	offsetof(X509Certificate_t1460_StaticFields, ___U3CU3Ef__switchU24map10_24),
	offsetof(X509Certificate_t1460_StaticFields, ___U3CU3Ef__switchU24map11_25),
	offsetof(X509CertificateEnumerator_t1463, ___enumerator_0),
	offsetof(X509Chain_t1465, ___roots_0),
	offsetof(X509Chain_t1465, ___certs_1),
	offsetof(X509Chain_t1465, ____root_2),
	offsetof(X509Chain_t1465, ____chain_3),
	offsetof(X509Chain_t1465, ____status_4),
	offsetof(X509ChainStatusFlags_t1466, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(X509Crl_t1468, ___issuer_0),
	offsetof(X509Crl_t1468, ___version_1),
	offsetof(X509Crl_t1468, ___thisUpdate_2),
	offsetof(X509Crl_t1468, ___nextUpdate_3),
	offsetof(X509Crl_t1468, ___entries_4),
	offsetof(X509Crl_t1468, ___signatureOID_5),
	offsetof(X509Crl_t1468, ___signature_6),
	offsetof(X509Crl_t1468, ___extensions_7),
	offsetof(X509Crl_t1468, ___encoded_8),
	offsetof(X509Crl_t1468, ___hash_value_9),
	offsetof(X509Crl_t1468_StaticFields, ___U3CU3Ef__switchU24map13_10),
	offsetof(X509CrlEntry_t1467, ___sn_0),
	offsetof(X509CrlEntry_t1467, ___revocationDate_1),
	offsetof(X509CrlEntry_t1467, ___extensions_2),
	offsetof(X509Extension_t1469, ___extnOid_0),
	offsetof(X509Extension_t1469, ___extnCritical_1),
	offsetof(X509Extension_t1469, ___extnValue_2),
	offsetof(X509ExtensionCollection_t1462, ___readOnly_1),
	offsetof(X509Store_t1470, ____storePath_0),
	offsetof(X509Store_t1470, ____certificates_1),
	offsetof(X509Store_t1470, ____crls_2),
	offsetof(X509Store_t1470, ____crl_3),
	offsetof(X509StoreManager_t1471_StaticFields, ____userStore_0),
	offsetof(X509StoreManager_t1471_StaticFields, ____machineStore_1),
	offsetof(X509Stores_t1472, ____storePath_0),
	offsetof(X509Stores_t1472, ____trusted_1),
	offsetof(AuthorityKeyIdentifierExtension_t1473, ___aki_3),
	offsetof(BasicConstraintsExtension_t1474, ___cA_3),
	offsetof(BasicConstraintsExtension_t1474, ___pathLenConstraint_4),
	offsetof(ExtendedKeyUsageExtension_t1475, ___keyPurpose_3),
	offsetof(ExtendedKeyUsageExtension_t1475_StaticFields, ___U3CU3Ef__switchU24map14_4),
	offsetof(GeneralNames_t1476, ___rfc822Name_0),
	offsetof(GeneralNames_t1476, ___dnsName_1),
	offsetof(GeneralNames_t1476, ___directoryNames_2),
	offsetof(GeneralNames_t1476, ___uris_3),
	offsetof(GeneralNames_t1476, ___ipAddr_4),
	offsetof(KeyUsages_t1477, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(KeyUsageExtension_t1478, ___kubits_3),
	offsetof(NetscapeCertTypeExtension_t1480, ___ctbits_3),
	offsetof(CertTypes_t1479, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(SubjectAltNameExtension_t1481, ____names_3),
	offsetof(HMAC_t1482, ___hash_5),
	offsetof(HMAC_t1482, ___hashing_6),
	offsetof(HMAC_t1482, ___innerPad_7),
	offsetof(HMAC_t1482, ___outerPad_8),
	offsetof(MD5SHA1_t1484, ___md5_4),
	offsetof(MD5SHA1_t1484, ___sha_5),
	offsetof(MD5SHA1_t1484, ___hashing_6),
	offsetof(AlertLevel_t1485, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(AlertDescription_t1486, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(Alert_t1487, ___level_0),
	offsetof(Alert_t1487, ___description_1),
	offsetof(CipherAlgorithmType_t1488, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(CipherSuite_t1489_StaticFields, ___EmptyArray_0),
	offsetof(CipherSuite_t1489, ___code_1),
	offsetof(CipherSuite_t1489, ___name_2),
	offsetof(CipherSuite_t1489, ___cipherAlgorithmType_3),
	offsetof(CipherSuite_t1489, ___hashAlgorithmType_4),
	offsetof(CipherSuite_t1489, ___exchangeAlgorithmType_5),
	offsetof(CipherSuite_t1489, ___isExportable_6),
	offsetof(CipherSuite_t1489, ___cipherMode_7),
	offsetof(CipherSuite_t1489, ___keyMaterialSize_8),
	offsetof(CipherSuite_t1489, ___keyBlockSize_9),
	offsetof(CipherSuite_t1489, ___expandedKeyMaterialSize_10),
	offsetof(CipherSuite_t1489, ___effectiveKeyBits_11),
	offsetof(CipherSuite_t1489, ___ivSize_12),
	offsetof(CipherSuite_t1489, ___blockSize_13),
	offsetof(CipherSuite_t1489, ___context_14),
	offsetof(CipherSuite_t1489, ___encryptionAlgorithm_15),
	offsetof(CipherSuite_t1489, ___encryptionCipher_16),
	offsetof(CipherSuite_t1489, ___decryptionAlgorithm_17),
	offsetof(CipherSuite_t1489, ___decryptionCipher_18),
	offsetof(CipherSuite_t1489, ___clientHMAC_19),
	offsetof(CipherSuite_t1489, ___serverHMAC_20),
	offsetof(CipherSuiteCollection_t1491, ___cipherSuites_0),
	offsetof(CipherSuiteCollection_t1491, ___protocol_1),
	offsetof(ClientContext_t1493, ___sslStream_30),
	offsetof(ClientContext_t1493, ___clientHelloProtocol_31),
	offsetof(ClientSessionInfo_t1497_StaticFields, ___ValidityInterval_0),
	offsetof(ClientSessionInfo_t1497, ___disposed_1),
	offsetof(ClientSessionInfo_t1497, ___validuntil_2),
	offsetof(ClientSessionInfo_t1497, ___host_3),
	offsetof(ClientSessionInfo_t1497, ___sid_4),
	offsetof(ClientSessionInfo_t1497, ___masterSecret_5),
	offsetof(ClientSessionCache_t1498_StaticFields, ___cache_0),
	offsetof(ClientSessionCache_t1498_StaticFields, ___locker_1),
	offsetof(ContentType_t1499, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(Context_t1490, ___securityProtocol_0),
	offsetof(Context_t1490, ___sessionId_1),
	offsetof(Context_t1490, ___compressionMethod_2),
	offsetof(Context_t1490, ___serverSettings_3),
	offsetof(Context_t1490, ___clientSettings_4),
	offsetof(Context_t1490, ___current_5),
	offsetof(Context_t1490, ___negotiating_6),
	offsetof(Context_t1490, ___read_7),
	offsetof(Context_t1490, ___write_8),
	offsetof(Context_t1490, ___supportedCiphers_9),
	offsetof(Context_t1490, ___lastHandshakeMsg_10),
	offsetof(Context_t1490, ___handshakeState_11),
	offsetof(Context_t1490, ___abbreviatedHandshake_12),
	offsetof(Context_t1490, ___receivedConnectionEnd_13),
	offsetof(Context_t1490, ___sentConnectionEnd_14),
	offsetof(Context_t1490, ___protocolNegotiated_15),
	offsetof(Context_t1490, ___writeSequenceNumber_16),
	offsetof(Context_t1490, ___readSequenceNumber_17),
	offsetof(Context_t1490, ___clientRandom_18),
	offsetof(Context_t1490, ___serverRandom_19),
	offsetof(Context_t1490, ___randomCS_20),
	offsetof(Context_t1490, ___randomSC_21),
	offsetof(Context_t1490, ___masterSecret_22),
	offsetof(Context_t1490, ___clientWriteKey_23),
	offsetof(Context_t1490, ___serverWriteKey_24),
	offsetof(Context_t1490, ___clientWriteIV_25),
	offsetof(Context_t1490, ___serverWriteIV_26),
	offsetof(Context_t1490, ___handshakeMessages_27),
	offsetof(Context_t1490, ___random_28),
	offsetof(Context_t1490, ___recordProtocol_29),
	offsetof(ExchangeAlgorithmType_t1504, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(HandshakeState_t1505, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(HashAlgorithmType_t1506, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(HttpsClientStream_t1507, ____request_20),
	offsetof(HttpsClientStream_t1507, ____status_21),
	offsetof(HttpsClientStream_t1507_StaticFields, ___U3CU3Ef__amU24cache2_22),
	offsetof(HttpsClientStream_t1507_StaticFields, ___U3CU3Ef__amU24cache3_23),
	offsetof(RecordProtocol_t1496_StaticFields, ___record_processing_0),
	offsetof(RecordProtocol_t1496, ___innerStream_1),
	offsetof(RecordProtocol_t1496, ___context_2),
	offsetof(ReceiveRecordAsyncResult_t1511, ___locker_0),
	offsetof(ReceiveRecordAsyncResult_t1511, ____userCallback_1),
	offsetof(ReceiveRecordAsyncResult_t1511, ____userState_2),
	offsetof(ReceiveRecordAsyncResult_t1511, ____asyncException_3),
	offsetof(ReceiveRecordAsyncResult_t1511, ___handle_4),
	offsetof(ReceiveRecordAsyncResult_t1511, ____resultingBuffer_5),
	offsetof(ReceiveRecordAsyncResult_t1511, ____record_6),
	offsetof(ReceiveRecordAsyncResult_t1511, ___completed_7),
	offsetof(ReceiveRecordAsyncResult_t1511, ____initialBuffer_8),
	offsetof(SendRecordAsyncResult_t1513, ___locker_0),
	offsetof(SendRecordAsyncResult_t1513, ____userCallback_1),
	offsetof(SendRecordAsyncResult_t1513, ____userState_2),
	offsetof(SendRecordAsyncResult_t1513, ____asyncException_3),
	offsetof(SendRecordAsyncResult_t1513, ___handle_4),
	offsetof(SendRecordAsyncResult_t1513, ____message_5),
	offsetof(SendRecordAsyncResult_t1513, ___completed_6),
	offsetof(RSASslSignatureDeformatter_t1515, ___key_0),
	offsetof(RSASslSignatureDeformatter_t1515, ___hash_1),
	offsetof(RSASslSignatureDeformatter_t1515_StaticFields, ___U3CU3Ef__switchU24map15_2),
	offsetof(RSASslSignatureFormatter_t1517, ___key_0),
	offsetof(RSASslSignatureFormatter_t1517, ___hash_1),
	offsetof(RSASslSignatureFormatter_t1517_StaticFields, ___U3CU3Ef__switchU24map16_2),
	offsetof(SecurityCompressionType_t1519, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(SecurityParameters_t1502, ___cipher_0),
	offsetof(SecurityParameters_t1502, ___clientWriteMAC_1),
	offsetof(SecurityParameters_t1502, ___serverWriteMAC_2),
	offsetof(SecurityProtocolType_t1520, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(ValidationResult_t1522, ___trusted_0),
	offsetof(ValidationResult_t1522, ___error_code_1),
	offsetof(SslClientStream_t1494, ___ServerCertValidation_16),
	offsetof(SslClientStream_t1494, ___ClientCertSelection_17),
	offsetof(SslClientStream_t1494, ___PrivateKeySelection_18),
	offsetof(SslClientStream_t1494, ___ServerCertValidation2_19),
	offsetof(SslCipherSuite_t1526, ___pad1_21),
	offsetof(SslCipherSuite_t1526, ___pad2_22),
	offsetof(SslCipherSuite_t1526, ___header_23),
	offsetof(SslHandshakeHash_t1527, ___md5_4),
	offsetof(SslHandshakeHash_t1527, ___sha_5),
	offsetof(SslHandshakeHash_t1527, ___hashing_6),
	offsetof(SslHandshakeHash_t1527, ___secret_7),
	offsetof(SslHandshakeHash_t1527, ___innerPadMD5_8),
	offsetof(SslHandshakeHash_t1527, ___outerPadMD5_9),
	offsetof(SslHandshakeHash_t1527, ___innerPadSHA_10),
	offsetof(SslHandshakeHash_t1527, ___outerPadSHA_11),
	0,
	offsetof(SslStreamBase_t1523_StaticFields, ___record_processing_2),
	offsetof(SslStreamBase_t1523, ___innerStream_3),
	offsetof(SslStreamBase_t1523, ___inputBuffer_4),
	offsetof(SslStreamBase_t1523, ___context_5),
	offsetof(SslStreamBase_t1523, ___protocol_6),
	offsetof(SslStreamBase_t1523, ___ownsStream_7),
	offsetof(SslStreamBase_t1523, ___disposed_8),
	offsetof(SslStreamBase_t1523, ___checkCertRevocationStatus_9),
	offsetof(SslStreamBase_t1523, ___negotiate_10),
	offsetof(SslStreamBase_t1523, ___read_11),
	offsetof(SslStreamBase_t1523, ___write_12),
	offsetof(SslStreamBase_t1523, ___negotiationComplete_13),
	offsetof(SslStreamBase_t1523, ___recbuf_14),
	offsetof(SslStreamBase_t1523, ___recordStream_15),
	offsetof(InternalAsyncResult_t1528, ___locker_0),
	offsetof(InternalAsyncResult_t1528, ____userCallback_1),
	offsetof(InternalAsyncResult_t1528, ____userState_2),
	offsetof(InternalAsyncResult_t1528, ____asyncException_3),
	offsetof(InternalAsyncResult_t1528, ___handle_4),
	offsetof(InternalAsyncResult_t1528, ___completed_5),
	offsetof(InternalAsyncResult_t1528, ____bytesRead_6),
	offsetof(InternalAsyncResult_t1528, ____fromWrite_7),
	offsetof(InternalAsyncResult_t1528, ____proceedAfterHandshake_8),
	offsetof(InternalAsyncResult_t1528, ____buffer_9),
	offsetof(InternalAsyncResult_t1528, ____offset_10),
	offsetof(InternalAsyncResult_t1528, ____count_11),
	offsetof(TlsCipherSuite_t1529, ___header_21),
	offsetof(TlsCipherSuite_t1529, ___headerLock_22),
	offsetof(TlsClientSettings_t1501, ___targetHost_0),
	offsetof(TlsClientSettings_t1501, ___certificates_1),
	offsetof(TlsClientSettings_t1501, ___clientCertificate_2),
	offsetof(TlsClientSettings_t1501, ___certificateRSA_3),
	offsetof(TlsException_t1532, ___alert_11),
	offsetof(TlsServerSettings_t1500, ___certificates_0),
	offsetof(TlsServerSettings_t1500, ___certificateRSA_1),
	offsetof(TlsServerSettings_t1500, ___rsaParameters_2),
	offsetof(TlsServerSettings_t1500, ___signedParams_3),
	offsetof(TlsServerSettings_t1500, ___distinguisedNames_4),
	offsetof(TlsServerSettings_t1500, ___serverKeyExchange_5),
	offsetof(TlsServerSettings_t1500, ___certificateRequest_6),
	offsetof(TlsServerSettings_t1500, ___certificateTypes_7),
	offsetof(TlsStream_t1503, ___canRead_1),
	offsetof(TlsStream_t1503, ___canWrite_2),
	offsetof(TlsStream_t1503, ___buffer_3),
	offsetof(TlsStream_t1503, ___temp_4),
	offsetof(ClientCertificateType_t1535, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(HandshakeMessage_t1514, ___context_5),
	offsetof(HandshakeMessage_t1514, ___handshakeType_6),
	offsetof(HandshakeMessage_t1514, ___contentType_7),
	offsetof(HandshakeMessage_t1514, ___cache_8),
	offsetof(HandshakeType_t1536, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(TlsClientCertificate_t1537, ___clientCertSelected_9),
	offsetof(TlsClientCertificate_t1537, ___clientCert_10),
	offsetof(TlsClientFinished_t1539_StaticFields, ___Ssl3Marker_9),
	offsetof(TlsClientHello_t1540, ___random_9),
	offsetof(TlsServerCertificate_t1542, ___certificates_9),
	offsetof(TlsServerCertificateRequest_t1543, ___certificateTypes_9),
	offsetof(TlsServerCertificateRequest_t1543, ___distinguisedNames_10),
	offsetof(TlsServerFinished_t1544_StaticFields, ___Ssl3Marker_9),
	offsetof(TlsServerHello_t1545, ___compressionMethod_9),
	offsetof(TlsServerHello_t1545, ___random_10),
	offsetof(TlsServerHello_t1545, ___sessionId_11),
	offsetof(TlsServerHello_t1545, ___cipherSuite_12),
	offsetof(TlsServerKeyExchange_t1547, ___rsaParams_9),
	offsetof(TlsServerKeyExchange_t1547, ___signedParams_10),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D0_0),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D5_1),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D6_2),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D7_3),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D8_4),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D9_5),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D11_6),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D12_7),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D13_8),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D14_9),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D15_10),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D16_11),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D17_12),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D21_13),
	offsetof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields, ___U24U24fieldU2D22_14),
	offsetof(MonoTODOAttribute_t1600, ___comment_0),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(HybridDictionary_t1601, ___caseInsensitive_0),
	offsetof(HybridDictionary_t1601, ___hashtable_1),
	offsetof(HybridDictionary_t1601, ___list_2),
	offsetof(ListDictionary_t1602, ___count_0),
	offsetof(ListDictionary_t1602, ___version_1),
	offsetof(ListDictionary_t1602, ___head_2),
	offsetof(ListDictionary_t1602, ___comparer_3),
	offsetof(DictionaryNode_t1603, ___key_0),
	offsetof(DictionaryNode_t1603, ___value_1),
	offsetof(DictionaryNode_t1603, ___next_2),
	offsetof(DictionaryNodeEnumerator_t1604, ___dict_0),
	offsetof(DictionaryNodeEnumerator_t1604, ___isAtStart_1),
	offsetof(DictionaryNodeEnumerator_t1604, ___current_2),
	offsetof(DictionaryNodeEnumerator_t1604, ___version_3),
	offsetof(DictionaryNodeCollection_t1607, ___dict_0),
	offsetof(DictionaryNodeCollection_t1607, ___isKeyList_1),
	offsetof(DictionaryNodeCollectionEnumerator_t1605, ___inner_0),
	offsetof(DictionaryNodeCollectionEnumerator_t1605, ___isKeyList_1),
	offsetof(NameObjectCollectionBase_t1611, ___m_ItemsContainer_0),
	offsetof(NameObjectCollectionBase_t1611, ___m_NullKeyItem_1),
	offsetof(NameObjectCollectionBase_t1611, ___m_ItemsArray_2),
	offsetof(NameObjectCollectionBase_t1611, ___m_hashprovider_3),
	offsetof(NameObjectCollectionBase_t1611, ___m_comparer_4),
	offsetof(NameObjectCollectionBase_t1611, ___m_defCapacity_5),
	offsetof(NameObjectCollectionBase_t1611, ___m_readonly_6),
	offsetof(NameObjectCollectionBase_t1611, ___infoCopy_7),
	offsetof(NameObjectCollectionBase_t1611, ___keyscoll_8),
	offsetof(NameObjectCollectionBase_t1611, ___equality_comparer_9),
	offsetof(_Item_t1609, ___key_0),
	offsetof(_Item_t1609, ___value_1),
	offsetof(_KeysEnumerator_t1610, ___m_collection_0),
	offsetof(_KeysEnumerator_t1610, ___m_position_1),
	offsetof(KeysCollection_t1612, ___m_collection_0),
	offsetof(NameValueCollection_t1615, ___cachedAllKeys_10),
	offsetof(NameValueCollection_t1615, ___cachedAll_11),
	offsetof(EditorBrowsableAttribute_t1616, ___state_0),
	offsetof(EditorBrowsableState_t1617, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(TypeConverterAttribute_t1619_StaticFields, ___Default_0),
	offsetof(TypeConverterAttribute_t1619, ___converter_type_1),
	offsetof(AuthenticationLevel_t1620, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(SslPolicyErrors_t1621, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(AddressFamily_t1622, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(FileWebRequest_t1624, ___uri_6),
	offsetof(FileWebRequest_t1624, ___webHeaders_7),
	offsetof(FileWebRequest_t1624, ___connectionGroup_8),
	offsetof(FileWebRequest_t1624, ___contentLength_9),
	offsetof(FileWebRequest_t1624, ___fileAccess_10),
	offsetof(FileWebRequest_t1624, ___method_11),
	offsetof(FileWebRequest_t1624, ___proxy_12),
	offsetof(FileWebRequest_t1624, ___preAuthenticate_13),
	offsetof(FileWebRequest_t1624, ___timeout_14),
	offsetof(FtpWebRequest_t1629, ___requestUri_6),
	offsetof(FtpWebRequest_t1629, ___proxy_7),
	offsetof(FtpWebRequest_t1629, ___timeout_8),
	offsetof(FtpWebRequest_t1629, ___rwTimeout_9),
	offsetof(FtpWebRequest_t1629, ___binary_10),
	offsetof(FtpWebRequest_t1629, ___usePassive_11),
	offsetof(FtpWebRequest_t1629, ___method_12),
	offsetof(FtpWebRequest_t1629, ___locker_13),
	offsetof(FtpWebRequest_t1629_StaticFields, ___supportedCommands_14),
	offsetof(FtpWebRequest_t1629, ___callback_15),
	offsetof(FtpWebRequest_t1629_StaticFields, ___U3CU3Ef__amU24cache1C_16),
	offsetof(HttpVersion_t1632_StaticFields, ___Version10_0),
	offsetof(HttpVersion_t1632_StaticFields, ___Version11_1),
	offsetof(HttpWebRequest_t1508, ___requestUri_6),
	offsetof(HttpWebRequest_t1508, ___actualUri_7),
	offsetof(HttpWebRequest_t1508, ___hostChanged_8),
	offsetof(HttpWebRequest_t1508, ___allowAutoRedirect_9),
	offsetof(HttpWebRequest_t1508, ___allowBuffering_10),
	offsetof(HttpWebRequest_t1508, ___certificates_11),
	offsetof(HttpWebRequest_t1508, ___connectionGroup_12),
	offsetof(HttpWebRequest_t1508, ___contentLength_13),
	offsetof(HttpWebRequest_t1508, ___webHeaders_14),
	offsetof(HttpWebRequest_t1508, ___keepAlive_15),
	offsetof(HttpWebRequest_t1508, ___maxAutoRedirect_16),
	offsetof(HttpWebRequest_t1508, ___mediaType_17),
	offsetof(HttpWebRequest_t1508, ___method_18),
	offsetof(HttpWebRequest_t1508, ___initialMethod_19),
	offsetof(HttpWebRequest_t1508, ___pipelined_20),
	offsetof(HttpWebRequest_t1508, ___version_21),
	offsetof(HttpWebRequest_t1508, ___proxy_22),
	offsetof(HttpWebRequest_t1508, ___sendChunked_23),
	offsetof(HttpWebRequest_t1508, ___servicePoint_24),
	offsetof(HttpWebRequest_t1508, ___timeout_25),
	offsetof(HttpWebRequest_t1508, ___redirects_26),
	offsetof(HttpWebRequest_t1508, ___locker_27),
	offsetof(HttpWebRequest_t1508_StaticFields, ___defaultMaxResponseHeadersLength_28),
	offsetof(HttpWebRequest_t1508, ___readWriteTimeout_29),
	offsetof(IPAddress_t1634, ___m_Address_0),
	offsetof(IPAddress_t1634, ___m_Family_1),
	offsetof(IPAddress_t1634, ___m_Numbers_2),
	offsetof(IPAddress_t1634, ___m_ScopeId_3),
	offsetof(IPAddress_t1634_StaticFields, ___Any_4),
	offsetof(IPAddress_t1634_StaticFields, ___Broadcast_5),
	offsetof(IPAddress_t1634_StaticFields, ___Loopback_6),
	offsetof(IPAddress_t1634_StaticFields, ___None_7),
	offsetof(IPAddress_t1634_StaticFields, ___IPv6Any_8),
	offsetof(IPAddress_t1634_StaticFields, ___IPv6Loopback_9),
	offsetof(IPAddress_t1634_StaticFields, ___IPv6None_10),
	offsetof(IPv6Address_t1636, ___address_0),
	offsetof(IPv6Address_t1636, ___prefixLength_1),
	offsetof(IPv6Address_t1636, ___scopeId_2),
	offsetof(IPv6Address_t1636_StaticFields, ___Loopback_3),
	offsetof(IPv6Address_t1636_StaticFields, ___Unspecified_4),
	offsetof(SecurityProtocolType_t1637, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(ServicePoint_t1584, ___uri_0),
	offsetof(ServicePoint_t1584, ___connectionLimit_1),
	offsetof(ServicePoint_t1584, ___maxIdleTime_2),
	offsetof(ServicePoint_t1584, ___currentConnections_3),
	offsetof(ServicePoint_t1584, ___idleSince_4),
	offsetof(ServicePoint_t1584, ___usesProxy_5),
	offsetof(ServicePoint_t1584, ___sendContinue_6),
	offsetof(ServicePoint_t1584, ___useConnect_7),
	offsetof(ServicePoint_t1584, ___locker_8),
	offsetof(ServicePoint_t1584, ___hostE_9),
	offsetof(ServicePoint_t1584, ___useNagle_10),
	offsetof(ServicePointManager_t1575_StaticFields, ___servicePoints_0),
	offsetof(ServicePointManager_t1575_StaticFields, ___policy_1),
	offsetof(ServicePointManager_t1575_StaticFields, ___defaultConnectionLimit_2),
	offsetof(ServicePointManager_t1575_StaticFields, ___maxServicePointIdleTime_3),
	offsetof(ServicePointManager_t1575_StaticFields, ___maxServicePoints_4),
	offsetof(ServicePointManager_t1575_StaticFields, ____checkCRL_5),
	offsetof(ServicePointManager_t1575_StaticFields, ____securityProtocol_6),
	offsetof(ServicePointManager_t1575_StaticFields, ___expectContinue_7),
	offsetof(ServicePointManager_t1575_StaticFields, ___useNagle_8),
	offsetof(ServicePointManager_t1575_StaticFields, ___server_cert_cb_9),
	offsetof(SPKey_t1638, ___uri_0),
	offsetof(SPKey_t1638, ___use_connect_1),
	offsetof(WebHeaderCollection_t1625_StaticFields, ___restricted_12),
	offsetof(WebHeaderCollection_t1625_StaticFields, ___multiValue_13),
	offsetof(WebHeaderCollection_t1625_StaticFields, ___restricted_response_14),
	offsetof(WebHeaderCollection_t1625, ___internallyCreated_15),
	offsetof(WebHeaderCollection_t1625_StaticFields, ___allowed_chars_16),
	offsetof(WebProxy_t1641, ___address_0),
	offsetof(WebProxy_t1641, ___bypassOnLocal_1),
	offsetof(WebProxy_t1641, ___bypassList_2),
	offsetof(WebProxy_t1641, ___credentials_3),
	offsetof(WebProxy_t1641, ___useDefaultCredentials_4),
	offsetof(WebRequest_t1588_StaticFields, ___prefixes_1),
	offsetof(WebRequest_t1588_StaticFields, ___isDefaultWebProxySet_2),
	offsetof(WebRequest_t1588_StaticFields, ___defaultWebProxy_3),
	offsetof(WebRequest_t1588, ___authentication_level_4),
	offsetof(WebRequest_t1588_StaticFields, ___lockobj_5),
	offsetof(OpenFlags_t1644, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(PublicKey_t1645, ____key_0),
	offsetof(PublicKey_t1645, ____keyValue_1),
	offsetof(PublicKey_t1645, ____params_2),
	offsetof(PublicKey_t1645, ____oid_3),
	offsetof(PublicKey_t1645_StaticFields, ___U3CU3Ef__switchU24map9_4),
	offsetof(StoreLocation_t1648, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(StoreName_t1649, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(X500DistinguishedName_t1650, ___name_3),
	offsetof(X500DistinguishedNameFlags_t1651, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(X509BasicConstraintsExtension_t1652, ____certificateAuthority_6),
	offsetof(X509BasicConstraintsExtension_t1652, ____hasPathLengthConstraint_7),
	offsetof(X509BasicConstraintsExtension_t1652, ____pathLengthConstraint_8),
	offsetof(X509BasicConstraintsExtension_t1652, ____status_9),
	offsetof(X509Certificate2_t1586, ____archived_5),
	offsetof(X509Certificate2_t1586, ____extensions_6),
	offsetof(X509Certificate2_t1586, ____name_7),
	offsetof(X509Certificate2_t1586, ____serial_8),
	offsetof(X509Certificate2_t1586, ____publicKey_9),
	offsetof(X509Certificate2_t1586, ___issuer_name_10),
	offsetof(X509Certificate2_t1586, ___subject_name_11),
	offsetof(X509Certificate2_t1586, ___signature_algorithm_12),
	offsetof(X509Certificate2_t1586, ____cert_13),
	offsetof(X509Certificate2_t1586_StaticFields, ___empty_error_14),
	offsetof(X509Certificate2_t1586_StaticFields, ___commonName_15),
	offsetof(X509Certificate2_t1586_StaticFields, ___email_16),
	offsetof(X509Certificate2_t1586_StaticFields, ___signedData_17),
	offsetof(X509Certificate2Enumerator_t1656, ___enumerator_0),
	offsetof(X509CertificateEnumerator_t1592, ___enumerator_0),
	offsetof(X509Chain_t1587, ___location_0),
	offsetof(X509Chain_t1587, ___elements_1),
	offsetof(X509Chain_t1587, ___policy_2),
	offsetof(X509Chain_t1587, ___status_3),
	offsetof(X509Chain_t1587_StaticFields, ___Empty_4),
	offsetof(X509Chain_t1587, ___max_path_length_5),
	offsetof(X509Chain_t1587, ___working_issuer_name_6),
	offsetof(X509Chain_t1587, ___working_public_key_7),
	offsetof(X509Chain_t1587, ___bce_restriction_8),
	offsetof(X509Chain_t1587, ___roots_9),
	offsetof(X509Chain_t1587, ___cas_10),
	offsetof(X509Chain_t1587, ___collection_11),
	offsetof(X509Chain_t1587_StaticFields, ___U3CU3Ef__switchU24mapB_12),
	offsetof(X509Chain_t1587_StaticFields, ___U3CU3Ef__switchU24mapC_13),
	offsetof(X509Chain_t1587_StaticFields, ___U3CU3Ef__switchU24mapD_14),
	offsetof(X509ChainElement_t1660, ___certificate_0),
	offsetof(X509ChainElement_t1660, ___status_1),
	offsetof(X509ChainElement_t1660, ___info_2),
	offsetof(X509ChainElement_t1660, ___compressed_status_flags_3),
	offsetof(X509ChainElementCollection_t1657, ____list_0),
	offsetof(X509ChainElementEnumerator_t1663, ___enumerator_0),
	offsetof(X509ChainPolicy_t1658, ___apps_0),
	offsetof(X509ChainPolicy_t1658, ___cert_1),
	offsetof(X509ChainPolicy_t1658, ___store_2),
	offsetof(X509ChainPolicy_t1658, ___rflag_3),
	offsetof(X509ChainPolicy_t1658, ___mode_4),
	offsetof(X509ChainPolicy_t1658, ___timeout_5),
	offsetof(X509ChainPolicy_t1658, ___vflags_6),
	offsetof(X509ChainPolicy_t1658, ___vtime_7),
	offsetof(X509ChainStatus_t1662, ___status_0) + sizeof(Object_t),
	offsetof(X509ChainStatus_t1662, ___info_1) + sizeof(Object_t),
	offsetof(X509ChainStatusFlags_t1665, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(X509EnhancedKeyUsageExtension_t1666, ____enhKeyUsage_4),
	offsetof(X509EnhancedKeyUsageExtension_t1666, ____status_5),
	offsetof(X509EnhancedKeyUsageExtension_t1666_StaticFields, ___U3CU3Ef__switchU24mapE_6),
	offsetof(X509Extension_t1653, ____critical_3),
	offsetof(X509ExtensionCollection_t1654, ____list_0),
	offsetof(X509ExtensionEnumerator_t1667, ___enumerator_0),
	offsetof(X509FindType_t1668, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(X509KeyUsageExtension_t1669, ____keyUsages_7),
	offsetof(X509KeyUsageExtension_t1669, ____status_8),
	offsetof(X509KeyUsageFlags_t1670, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(X509NameType_t1671, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(X509RevocationFlag_t1672, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(X509RevocationMode_t1673, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(X509Store_t1661, ____name_0),
	offsetof(X509Store_t1661, ____location_1),
	offsetof(X509Store_t1661, ___list_2),
	offsetof(X509Store_t1661, ____flags_3),
	offsetof(X509Store_t1661, ___store_4),
	offsetof(X509Store_t1661_StaticFields, ___U3CU3Ef__switchU24mapF_5),
	0,
	0,
	offsetof(X509SubjectKeyIdentifierExtension_t1674, ____subjectKeyIdentifier_6),
	offsetof(X509SubjectKeyIdentifierExtension_t1674, ____ski_7),
	offsetof(X509SubjectKeyIdentifierExtension_t1674, ____status_8),
	offsetof(X509SubjectKeyIdentifierHashAlgorithm_t1675, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(X509VerificationFlags_t1676, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(AsnDecodeStatus_t1677, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(AsnEncodedData_t1646, ____oid_0),
	offsetof(AsnEncodedData_t1646, ____raw_1),
	offsetof(AsnEncodedData_t1646_StaticFields, ___U3CU3Ef__switchU24mapA_2),
	offsetof(Oid_t1647, ____value_0),
	offsetof(Oid_t1647, ____name_1),
	offsetof(Oid_t1647_StaticFields, ___U3CU3Ef__switchU24map10_2),
	offsetof(OidCollection_t1664, ____list_0),
	offsetof(OidCollection_t1664, ____readOnly_1),
	offsetof(OidEnumerator_t1678, ____collection_0),
	offsetof(OidEnumerator_t1678, ____position_1),
	offsetof(BaseMachine_t1680, ___needs_groups_or_captures_0),
	offsetof(Capture_t1681, ___index_0),
	offsetof(Capture_t1681, ___length_1),
	offsetof(Capture_t1681, ___text_2),
	offsetof(CaptureCollection_t1682, ___list_0),
	offsetof(Group_t1597_StaticFields, ___Fail_3),
	offsetof(Group_t1597, ___success_4),
	offsetof(Group_t1597, ___captures_5),
	offsetof(GroupCollection_t1596, ___list_0),
	offsetof(GroupCollection_t1596, ___gap_1),
	offsetof(Match_t1595, ___regex_6),
	offsetof(Match_t1595, ___machine_7),
	offsetof(Match_t1595, ___text_length_8),
	offsetof(Match_t1595, ___groups_9),
	offsetof(Match_t1595_StaticFields, ___empty_10),
	offsetof(MatchCollection_t1594, ___current_0),
	offsetof(MatchCollection_t1594, ___list_1),
	offsetof(Enumerator_t1686, ___index_0),
	offsetof(Enumerator_t1686, ___coll_1),
	offsetof(Regex_t1036_StaticFields, ___cache_0),
	offsetof(Regex_t1036, ___machineFactory_1),
	offsetof(Regex_t1036, ___mapping_2),
	offsetof(Regex_t1036, ___group_count_3),
	offsetof(Regex_t1036, ___gap_4),
	offsetof(Regex_t1036, ___group_names_5),
	offsetof(Regex_t1036, ___group_numbers_6),
	offsetof(Regex_t1036, ___pattern_7),
	offsetof(Regex_t1036, ___roptions_8),
	offsetof(RegexOptions_t1689, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(OpCode_t1690, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(OpFlags_t1691, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(Position_t1692, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(FactoryCache_t1687, ___capacity_0),
	offsetof(FactoryCache_t1687, ___factories_1),
	offsetof(FactoryCache_t1687, ___mru_list_2),
	offsetof(Key_t1693, ___pattern_0),
	offsetof(Key_t1693, ___options_1),
	offsetof(MRUList_t1694, ___head_0),
	offsetof(MRUList_t1694, ___tail_1),
	offsetof(Node_t1695, ___value_0),
	offsetof(Node_t1695, ___previous_1),
	offsetof(Node_t1695, ___next_2),
	offsetof(Category_t1696, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(InterpreterFactory_t1699, ___mapping_0),
	offsetof(InterpreterFactory_t1699, ___pattern_1),
	offsetof(InterpreterFactory_t1699, ___namesMapping_2),
	offsetof(InterpreterFactory_t1699, ___gap_3),
	offsetof(PatternCompiler_t1703, ___pgm_0),
	offsetof(PatternLinkStack_t1701, ___link_1),
	offsetof(Link_t1700, ___base_addr_0) + sizeof(Object_t),
	offsetof(Link_t1700, ___offset_addr_1) + sizeof(Object_t),
	offsetof(LinkStack_t1702, ___stack_0),
	offsetof(Mark_t1704, ___Start_0) + sizeof(Object_t),
	offsetof(Mark_t1704, ___End_1) + sizeof(Object_t),
	offsetof(Mark_t1704, ___Previous_2) + sizeof(Object_t),
	offsetof(Interpreter_t1708, ___program_1),
	offsetof(Interpreter_t1708, ___program_start_2),
	offsetof(Interpreter_t1708, ___text_3),
	offsetof(Interpreter_t1708, ___text_end_4),
	offsetof(Interpreter_t1708, ___group_count_5),
	offsetof(Interpreter_t1708, ___match_min_6),
	offsetof(Interpreter_t1708, ___qs_7),
	offsetof(Interpreter_t1708, ___scan_ptr_8),
	offsetof(Interpreter_t1708, ___repeat_9),
	offsetof(Interpreter_t1708, ___fast_10),
	offsetof(Interpreter_t1708, ___stack_11),
	offsetof(Interpreter_t1708, ___deep_12),
	offsetof(Interpreter_t1708, ___marks_13),
	offsetof(Interpreter_t1708, ___mark_start_14),
	offsetof(Interpreter_t1708, ___mark_end_15),
	offsetof(Interpreter_t1708, ___groups_16),
	offsetof(IntStack_t1705, ___values_0) + sizeof(Object_t),
	offsetof(IntStack_t1705, ___count_1) + sizeof(Object_t),
	offsetof(RepeatContext_t1706, ___start_0),
	offsetof(RepeatContext_t1706, ___min_1),
	offsetof(RepeatContext_t1706, ___max_2),
	offsetof(RepeatContext_t1706, ___lazy_3),
	offsetof(RepeatContext_t1706, ___expr_pc_4),
	offsetof(RepeatContext_t1706, ___previous_5),
	offsetof(RepeatContext_t1706, ___count_6),
	offsetof(Mode_t1707, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(Interval_t1711, ___low_0) + sizeof(Object_t),
	offsetof(Interval_t1711, ___high_1) + sizeof(Object_t),
	offsetof(Interval_t1711, ___contiguous_2) + sizeof(Object_t),
	offsetof(IntervalCollection_t1714, ___intervals_0),
	offsetof(Enumerator_t1712, ___list_0),
	offsetof(Enumerator_t1712, ___ptr_1),
	offsetof(Parser_t1715, ___pattern_0),
	offsetof(Parser_t1715, ___ptr_1),
	offsetof(Parser_t1715, ___caps_2),
	offsetof(Parser_t1715, ___refs_3),
	offsetof(Parser_t1715, ___num_groups_4),
	offsetof(Parser_t1715, ___gap_5),
	offsetof(QuickSearch_t1709, ___str_0),
	offsetof(QuickSearch_t1709, ___len_1),
	offsetof(QuickSearch_t1709, ___ignore_2),
	offsetof(QuickSearch_t1709, ___reverse_3),
	offsetof(QuickSearch_t1709, ___shift_4),
	offsetof(QuickSearch_t1709, ___shiftExtended_5),
	offsetof(QuickSearch_t1709_StaticFields, ___THRESHOLD_6),
	offsetof(ReplacementEvaluator_t1716, ___regex_0),
	offsetof(ReplacementEvaluator_t1716, ___n_pieces_1),
	offsetof(ReplacementEvaluator_t1716, ___pieces_2),
	offsetof(ReplacementEvaluator_t1716, ___replacement_3),
	offsetof(CompositeExpression_t1719, ___expressions_0),
	offsetof(RegularExpression_t1721, ___group_count_1),
	offsetof(CapturingGroup_t1722, ___gid_1),
	offsetof(CapturingGroup_t1722, ___name_2),
	offsetof(BalancingGroup_t1723, ___balance_3),
	offsetof(Repetition_t1725, ___min_1),
	offsetof(Repetition_t1725, ___max_2),
	offsetof(Repetition_t1725, ___lazy_3),
	offsetof(CaptureAssertion_t1727, ___alternate_1),
	offsetof(CaptureAssertion_t1727, ___group_2),
	offsetof(CaptureAssertion_t1727, ___literal_3),
	offsetof(ExpressionAssertion_t1728, ___reverse_1),
	offsetof(ExpressionAssertion_t1728, ___negate_2),
	offsetof(Literal_t1729, ___str_0),
	offsetof(Literal_t1729, ___ignore_1),
	offsetof(PositionAssertion_t1731, ___pos_0),
	offsetof(Reference_t1732, ___group_0),
	offsetof(Reference_t1732, ___ignore_1),
	offsetof(BackslashNumber_t1733, ___literal_2),
	offsetof(BackslashNumber_t1733, ___ecma_3),
	offsetof(CharacterClass_t1734_StaticFields, ___upper_case_characters_0),
	offsetof(CharacterClass_t1734, ___negate_1),
	offsetof(CharacterClass_t1734, ___ignore_2),
	offsetof(CharacterClass_t1734, ___pos_cats_3),
	offsetof(CharacterClass_t1734, ___neg_cats_4),
	offsetof(CharacterClass_t1734, ___intervals_5),
	offsetof(AnchorInfo_t1736, ___expr_0),
	offsetof(AnchorInfo_t1736, ___pos_1),
	offsetof(AnchorInfo_t1736, ___offset_2),
	offsetof(AnchorInfo_t1736, ___str_3),
	offsetof(AnchorInfo_t1736, ___width_4),
	offsetof(AnchorInfo_t1736, ___ignore_5),
	offsetof(Uri_t1583, ___isUnixFilePath_0),
	offsetof(Uri_t1583, ___source_1),
	offsetof(Uri_t1583, ___scheme_2),
	offsetof(Uri_t1583, ___host_3),
	offsetof(Uri_t1583, ___port_4),
	offsetof(Uri_t1583, ___path_5),
	offsetof(Uri_t1583, ___query_6),
	offsetof(Uri_t1583, ___fragment_7),
	offsetof(Uri_t1583, ___userinfo_8),
	offsetof(Uri_t1583, ___isUnc_9),
	offsetof(Uri_t1583, ___isOpaquePart_10),
	offsetof(Uri_t1583, ___isAbsoluteUri_11),
	offsetof(Uri_t1583, ___userEscaped_12),
	offsetof(Uri_t1583, ___cachedAbsoluteUri_13),
	offsetof(Uri_t1583, ___cachedToString_14),
	offsetof(Uri_t1583, ___cachedHashCode_15),
	offsetof(Uri_t1583_StaticFields, ___hexUpperChars_16),
	offsetof(Uri_t1583_StaticFields, ___SchemeDelimiter_17),
	offsetof(Uri_t1583_StaticFields, ___UriSchemeFile_18),
	offsetof(Uri_t1583_StaticFields, ___UriSchemeFtp_19),
	offsetof(Uri_t1583_StaticFields, ___UriSchemeGopher_20),
	offsetof(Uri_t1583_StaticFields, ___UriSchemeHttp_21),
	offsetof(Uri_t1583_StaticFields, ___UriSchemeHttps_22),
	offsetof(Uri_t1583_StaticFields, ___UriSchemeMailto_23),
	offsetof(Uri_t1583_StaticFields, ___UriSchemeNews_24),
	offsetof(Uri_t1583_StaticFields, ___UriSchemeNntp_25),
	offsetof(Uri_t1583_StaticFields, ___UriSchemeNetPipe_26),
	offsetof(Uri_t1583_StaticFields, ___UriSchemeNetTcp_27),
	offsetof(Uri_t1583_StaticFields, ___schemes_28),
	offsetof(Uri_t1583, ___parser_29),
	offsetof(Uri_t1583_StaticFields, ___U3CU3Ef__switchU24map14_30),
	offsetof(Uri_t1583_StaticFields, ___U3CU3Ef__switchU24map15_31),
	offsetof(Uri_t1583_StaticFields, ___U3CU3Ef__switchU24map16_32),
	offsetof(UriScheme_t1740, ___scheme_0) + sizeof(Object_t),
	offsetof(UriScheme_t1740, ___delimiter_1) + sizeof(Object_t),
	offsetof(UriScheme_t1740, ___defaultPort_2) + sizeof(Object_t),
	offsetof(UriHostNameType_t1743, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(UriKind_t1744, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(UriParser_t1738_StaticFields, ___lock_object_0),
	offsetof(UriParser_t1738_StaticFields, ___table_1),
	offsetof(UriParser_t1738, ___scheme_name_2),
	offsetof(UriParser_t1738, ___default_port_3),
	offsetof(UriParser_t1738_StaticFields, ___uri_regex_4),
	offsetof(UriParser_t1738_StaticFields, ___auth_regex_5),
	offsetof(UriPartial_t1745, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(U3CPrivateImplementationDetailsU3E_t1750_StaticFields, ___U24U24fieldU2D2_0),
	offsetof(U3CPrivateImplementationDetailsU3E_t1750_StaticFields, ___U24U24fieldU2D3_1),
	offsetof(U3CPrivateImplementationDetailsU3E_t1750_StaticFields, ___U24U24fieldU2D4_2),
	0,
	0,
	offsetof(Int32_t372, ___m_value_2) + sizeof(Object_t),
	offsetof(AttributeUsageAttribute_t1765, ___valid_on_0),
	offsetof(AttributeUsageAttribute_t1765, ___allow_multiple_1),
	offsetof(AttributeUsageAttribute_t1765, ___inherited_2),
	offsetof(ComVisibleAttribute_t1766, ___Visible_0),
	0,
	0,
	offsetof(Int64_t386, ___m_value_2) + sizeof(Object_t),
	0,
	0,
	offsetof(UInt32_t389, ___m_value_2) + sizeof(Object_t),
	offsetof(CLSCompliantAttribute_t1767, ___is_compliant_0),
	offsetof(UInt64_t394, ___m_value_0) + sizeof(Object_t),
	0,
	0,
	offsetof(Byte_t391, ___m_value_2) + sizeof(Object_t),
	offsetof(SByte_t390, ___m_value_0) + sizeof(Object_t),
	offsetof(Int16_t392, ___m_value_0) + sizeof(Object_t),
	0,
	0,
	offsetof(UInt16_t393, ___m_value_2) + sizeof(Object_t),
	0,
	0,
	offsetof(Char_t383, ___m_value_2) + sizeof(Object_t),
	offsetof(Char_t383_StaticFields, ___category_data_3),
	offsetof(Char_t383_StaticFields, ___numeric_data_4),
	offsetof(Char_t383_StaticFields, ___numeric_data_values_5),
	offsetof(Char_t383_StaticFields, ___to_lower_data_low_6),
	offsetof(Char_t383_StaticFields, ___to_lower_data_high_7),
	offsetof(Char_t383_StaticFields, ___to_upper_data_low_8),
	offsetof(Char_t383_StaticFields, ___to_upper_data_high_9),
	offsetof(String_t, ___length_0),
	offsetof(String_t, ___start_char_1),
	offsetof(String_t_StaticFields, ___Empty_2),
	offsetof(String_t_StaticFields, ___WhiteChars_3),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(Single_t388, ___m_value_7) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(Double_t387, ___m_value_13) + sizeof(Object_t),
	offsetof(Decimal_t395_StaticFields, ___MinValue_0),
	offsetof(Decimal_t395_StaticFields, ___MaxValue_1),
	offsetof(Decimal_t395_StaticFields, ___MinusOne_2),
	offsetof(Decimal_t395_StaticFields, ___One_3),
	offsetof(Decimal_t395_StaticFields, ___MaxValueDiv10_4),
	offsetof(Decimal_t395, ___flags_5) + sizeof(Object_t),
	offsetof(Decimal_t395, ___hi_6) + sizeof(Object_t),
	offsetof(Decimal_t395, ___lo_7) + sizeof(Object_t),
	offsetof(Decimal_t395, ___mid_8) + sizeof(Object_t),
	offsetof(Boolean_t384_StaticFields, ___FalseString_0),
	offsetof(Boolean_t384_StaticFields, ___TrueString_1),
	offsetof(Boolean_t384, ___m_value_2) + sizeof(Object_t),
	offsetof(IntPtr_t, ___m_value_0) + sizeof(Object_t),
	offsetof(IntPtr_t_StaticFields, ___Zero_1),
	offsetof(UIntPtr_t_StaticFields, ___Zero_0),
	offsetof(UIntPtr_t, ____pointer_1) + sizeof(Object_t),
	offsetof(MulticastDelegate_t28, ___prev_9),
	offsetof(MulticastDelegate_t28, ___kpm_next_10),
	offsetof(Delegate_t466, ___method_ptr_0),
	offsetof(Delegate_t466, ___invoke_impl_1),
	offsetof(Delegate_t466, ___m_target_2),
	offsetof(Delegate_t466, ___method_3),
	offsetof(Delegate_t466, ___delegate_trampoline_4),
	offsetof(Delegate_t466, ___method_code_5),
	offsetof(Delegate_t466, ___method_info_6),
	offsetof(Delegate_t466, ___original_method_info_7),
	offsetof(Delegate_t466, ___data_8),
	offsetof(Enum_t21_StaticFields, ___split_char_0),
	0,
	0,
	offsetof(SimpleEnumerator_t1769, ___enumeratee_0),
	offsetof(SimpleEnumerator_t1769, ___currentpos_1),
	offsetof(SimpleEnumerator_t1769, ___length_2),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(Type_t, ____impl_1),
	offsetof(Type_t_StaticFields, ___Delimiter_2),
	offsetof(Type_t_StaticFields, ___EmptyTypes_3),
	offsetof(Type_t_StaticFields, ___FilterAttribute_4),
	offsetof(Type_t_StaticFields, ___FilterName_5),
	offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6),
	offsetof(Type_t_StaticFields, ___Missing_7),
	offsetof(Exception_t359, ___trace_ips_0),
	offsetof(Exception_t359, ___inner_exception_1),
	offsetof(Exception_t359, ___message_2),
	offsetof(Exception_t359, ___help_link_3),
	offsetof(Exception_t359, ___class_name_4),
	offsetof(Exception_t359, ___stack_trace_5),
	offsetof(Exception_t359, ____remoteStackTraceString_6),
	offsetof(Exception_t359, ___remote_stack_index_7),
	offsetof(Exception_t359, ___hresult_8),
	offsetof(Exception_t359, ___source_9),
	offsetof(Exception_t359, ____data_10),
	offsetof(RuntimeFieldHandle_t1774, ___value_0) + sizeof(Object_t),
	offsetof(RuntimeTypeHandle_t1772, ___value_0) + sizeof(Object_t),
	offsetof(ObsoleteAttribute_t1777, ____message_0),
	offsetof(ObsoleteAttribute_t1777, ____error_1),
	offsetof(DllImportAttribute_t1778, ___CallingConvention_0),
	offsetof(DllImportAttribute_t1778, ___CharSet_1),
	offsetof(DllImportAttribute_t1778, ___Dll_2),
	offsetof(DllImportAttribute_t1778, ___EntryPoint_3),
	offsetof(DllImportAttribute_t1778, ___ExactSpelling_4),
	offsetof(DllImportAttribute_t1778, ___PreserveSig_5),
	offsetof(DllImportAttribute_t1778, ___SetLastError_6),
	offsetof(DllImportAttribute_t1778, ___BestFitMapping_7),
	offsetof(DllImportAttribute_t1778, ___ThrowOnUnmappableChar_8),
	offsetof(MarshalAsAttribute_t1779, ___utype_0),
	offsetof(MarshalAsAttribute_t1779, ___ArraySubType_1),
	offsetof(MarshalAsAttribute_t1779, ___MarshalCookie_2),
	offsetof(MarshalAsAttribute_t1779, ___MarshalType_3),
	offsetof(MarshalAsAttribute_t1779, ___MarshalTypeRef_4),
	offsetof(MarshalAsAttribute_t1779, ___SizeConst_5),
	offsetof(MarshalAsAttribute_t1779, ___SizeParamIndex_6),
	offsetof(GuidAttribute_t1781, ___guidValue_0),
	offsetof(InternalsVisibleToAttribute_t1785, ___assemblyName_0),
	offsetof(InternalsVisibleToAttribute_t1785, ___all_visible_1),
	offsetof(RuntimeCompatibilityAttribute_t1786, ___wrap_non_exception_throws_0),
	offsetof(DefaultMemberAttribute_t1788, ___member_name_0),
	offsetof(DecimalConstantAttribute_t1789, ___scale_0),
	offsetof(DecimalConstantAttribute_t1789, ___sign_1),
	offsetof(DecimalConstantAttribute_t1789, ___hi_2),
	offsetof(DecimalConstantAttribute_t1789, ___mid_3),
	offsetof(DecimalConstantAttribute_t1789, ___low_4),
	offsetof(FieldOffsetAttribute_t1790, ___val_0),
	offsetof(RuntimeArgumentHandle_t1791, ___args_0) + sizeof(Object_t),
	offsetof(TypedReference_t1792, ___type_0) + sizeof(Object_t),
	offsetof(TypedReference_t1792, ___value_1) + sizeof(Object_t),
	offsetof(TypedReference_t1792, ___klass_2) + sizeof(Object_t),
	offsetof(ArgIterator_t1793, ___sig_0) + sizeof(Object_t),
	offsetof(ArgIterator_t1793, ___args_1) + sizeof(Object_t),
	offsetof(ArgIterator_t1793, ___next_arg_2) + sizeof(Object_t),
	offsetof(ArgIterator_t1793, ___num_args_3) + sizeof(Object_t),
	offsetof(MarshalByRefObject_t1643, ____identity_0),
	0,
	0,
	offsetof(MonoTODOAttribute_t1797, ___comment_0),
	offsetof(CodePointIndexer_t1803, ___ranges_0),
	offsetof(CodePointIndexer_t1803, ___TotalCount_1),
	offsetof(CodePointIndexer_t1803, ___defaultIndex_2),
	offsetof(CodePointIndexer_t1803, ___defaultCP_3),
	offsetof(TableRange_t1802, ___Start_0) + sizeof(Object_t),
	offsetof(TableRange_t1802, ___End_1) + sizeof(Object_t),
	offsetof(TableRange_t1802, ___Count_2) + sizeof(Object_t),
	offsetof(TableRange_t1802, ___IndexStart_3) + sizeof(Object_t),
	offsetof(TableRange_t1802, ___IndexEnd_4) + sizeof(Object_t),
	offsetof(TailoringInfo_t1805, ___LCID_0),
	offsetof(TailoringInfo_t1805, ___TailoringIndex_1),
	offsetof(TailoringInfo_t1805, ___TailoringCount_2),
	offsetof(TailoringInfo_t1805, ___FrenchSort_3),
	offsetof(Contraction_t1806, ___Source_0),
	offsetof(Contraction_t1806, ___Replacement_1),
	offsetof(Contraction_t1806, ___SortKey_2),
	offsetof(ContractionComparer_t1807_StaticFields, ___Instance_0),
	offsetof(Level2Map_t1808, ___Source_0),
	offsetof(Level2Map_t1808, ___Replace_1),
	offsetof(Level2MapComparer_t1809_StaticFields, ___Instance_0),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___MaxExpansionLength_0),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___ignorableFlags_1),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___categories_2),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___level1_3),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___level2_4),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___level3_5),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___cjkCHScategory_6),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___cjkCHTcategory_7),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___cjkJAcategory_8),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___cjkKOcategory_9),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___cjkCHSlv1_10),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___cjkCHTlv1_11),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___cjkJAlv1_12),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___cjkKOlv1_13),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___cjkKOlv2_14),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___tailoringArr_15),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___tailoringInfos_16),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___forLock_17),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___isReady_18),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___U3CU3Ef__switchU24map2_19),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___U3CU3Ef__switchU24map3_20),
	offsetof(MSCompatUnicodeTable_t1810_StaticFields, ___U3CU3Ef__switchU24map4_21),
	offsetof(MSCompatUnicodeTableUtil_t1812_StaticFields, ___Ignorable_0),
	offsetof(MSCompatUnicodeTableUtil_t1812_StaticFields, ___Category_1),
	offsetof(MSCompatUnicodeTableUtil_t1812_StaticFields, ___Level1_2),
	offsetof(MSCompatUnicodeTableUtil_t1812_StaticFields, ___Level2_3),
	offsetof(MSCompatUnicodeTableUtil_t1812_StaticFields, ___Level3_4),
	offsetof(MSCompatUnicodeTableUtil_t1812_StaticFields, ___CjkCHS_5),
	offsetof(MSCompatUnicodeTableUtil_t1812_StaticFields, ___Cjk_6),
	offsetof(SimpleCollator_t1817_StaticFields, ___QuickCheckDisabled_0),
	offsetof(SimpleCollator_t1817_StaticFields, ___invariant_1),
	offsetof(SimpleCollator_t1817, ___textInfo_2),
	offsetof(SimpleCollator_t1817, ___frenchSort_3),
	offsetof(SimpleCollator_t1817, ___cjkCatTable_4),
	offsetof(SimpleCollator_t1817, ___cjkLv1Table_5),
	offsetof(SimpleCollator_t1817, ___cjkIndexer_6),
	offsetof(SimpleCollator_t1817, ___cjkLv2Table_7),
	offsetof(SimpleCollator_t1817, ___cjkLv2Indexer_8),
	offsetof(SimpleCollator_t1817, ___lcid_9),
	offsetof(SimpleCollator_t1817, ___contractions_10),
	offsetof(SimpleCollator_t1817, ___level2Maps_11),
	offsetof(SimpleCollator_t1817, ___unsafeFlags_12),
	offsetof(Context_t1813, ___Option_0) + sizeof(Object_t),
	offsetof(Context_t1813, ___NeverMatchFlags_1) + sizeof(Object_t),
	offsetof(Context_t1813, ___AlwaysMatchFlags_2) + sizeof(Object_t),
	offsetof(Context_t1813, ___Buffer1_3) + sizeof(Object_t),
	offsetof(Context_t1813, ___Buffer2_4) + sizeof(Object_t),
	offsetof(Context_t1813, ___PrevCode_5) + sizeof(Object_t),
	offsetof(Context_t1813, ___PrevSortKey_6) + sizeof(Object_t),
	offsetof(Context_t1813, ___QuickCheckPossible_7) + sizeof(Object_t),
	offsetof(PreviousInfo_t1814, ___Code_0) + sizeof(Object_t),
	offsetof(PreviousInfo_t1814, ___SortKey_1) + sizeof(Object_t),
	offsetof(Escape_t1815, ___Source_0) + sizeof(Object_t),
	offsetof(Escape_t1815, ___Index_1) + sizeof(Object_t),
	offsetof(Escape_t1815, ___Start_2) + sizeof(Object_t),
	offsetof(Escape_t1815, ___End_3) + sizeof(Object_t),
	offsetof(Escape_t1815, ___Optional_4) + sizeof(Object_t),
	offsetof(ExtenderType_t1816, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(SortKey_t1821, ___source_0),
	offsetof(SortKey_t1821, ___options_1),
	offsetof(SortKey_t1821, ___key_2),
	offsetof(SortKey_t1821, ___lcid_3),
	offsetof(SortKeyBuffer_t1822, ___l1_0),
	offsetof(SortKeyBuffer_t1822, ___l2_1),
	offsetof(SortKeyBuffer_t1822, ___l3_2),
	offsetof(SortKeyBuffer_t1822, ___l4s_3),
	offsetof(SortKeyBuffer_t1822, ___l4t_4),
	offsetof(SortKeyBuffer_t1822, ___l4k_5),
	offsetof(SortKeyBuffer_t1822, ___l4w_6),
	offsetof(SortKeyBuffer_t1822, ___l5_7),
	offsetof(SortKeyBuffer_t1822, ___l1b_8),
	offsetof(SortKeyBuffer_t1822, ___l2b_9),
	offsetof(SortKeyBuffer_t1822, ___l3b_10),
	offsetof(SortKeyBuffer_t1822, ___l4sb_11),
	offsetof(SortKeyBuffer_t1822, ___l4tb_12),
	offsetof(SortKeyBuffer_t1822, ___l4kb_13),
	offsetof(SortKeyBuffer_t1822, ___l4wb_14),
	offsetof(SortKeyBuffer_t1822, ___l5b_15),
	offsetof(SortKeyBuffer_t1822, ___source_16),
	offsetof(SortKeyBuffer_t1822, ___processLevel2_17),
	offsetof(SortKeyBuffer_t1822, ___frenchSort_18),
	offsetof(SortKeyBuffer_t1822, ___frenchSorted_19),
	offsetof(SortKeyBuffer_t1822, ___lcid_20),
	offsetof(SortKeyBuffer_t1822, ___options_21),
	offsetof(ConfidenceFactor_t1825, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(BigInteger_t1829, ___length_0),
	offsetof(BigInteger_t1829, ___data_1),
	offsetof(BigInteger_t1829_StaticFields, ___smallPrimes_2),
	offsetof(BigInteger_t1829_StaticFields, ___rng_3),
	offsetof(Sign_t1827, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(ModulusRing_t1828, ___mod_0),
	offsetof(ModulusRing_t1828, ___constant_1),
	offsetof(KeyBuilder_t1832_StaticFields, ___rng_0),
	offsetof(BlockProcessor_t1833, ___transform_0),
	offsetof(BlockProcessor_t1833, ___block_1),
	offsetof(BlockProcessor_t1833, ___blockSize_2),
	offsetof(BlockProcessor_t1833, ___blockCount_3),
	offsetof(DSAManaged_t1835, ___keypairGenerated_2),
	offsetof(DSAManaged_t1835, ___m_disposed_3),
	offsetof(DSAManaged_t1835, ___p_4),
	offsetof(DSAManaged_t1835, ___q_5),
	offsetof(DSAManaged_t1835, ___g_6),
	offsetof(DSAManaged_t1835, ___x_7),
	offsetof(DSAManaged_t1835, ___y_8),
	offsetof(DSAManaged_t1835, ___j_9),
	offsetof(DSAManaged_t1835, ___seed_10),
	offsetof(DSAManaged_t1835, ___counter_11),
	offsetof(DSAManaged_t1835, ___j_missing_12),
	offsetof(DSAManaged_t1835, ___rng_13),
	offsetof(DSAManaged_t1835, ___KeyGenerated_14),
	offsetof(KeyPairPersistence_t1836_StaticFields, ____userPathExists_0),
	offsetof(KeyPairPersistence_t1836_StaticFields, ____userPath_1),
	offsetof(KeyPairPersistence_t1836_StaticFields, ____machinePathExists_2),
	offsetof(KeyPairPersistence_t1836_StaticFields, ____machinePath_3),
	offsetof(KeyPairPersistence_t1836, ____params_4),
	offsetof(KeyPairPersistence_t1836, ____keyvalue_5),
	offsetof(KeyPairPersistence_t1836, ____filename_6),
	offsetof(KeyPairPersistence_t1836, ____container_7),
	offsetof(KeyPairPersistence_t1836_StaticFields, ___lockobj_8),
	offsetof(MACAlgorithm_t1837, ___algo_0),
	offsetof(MACAlgorithm_t1837, ___enc_1),
	offsetof(MACAlgorithm_t1837, ___block_2),
	offsetof(MACAlgorithm_t1837, ___blockSize_3),
	offsetof(MACAlgorithm_t1837, ___blockCount_4),
	offsetof(PKCS1_t1838_StaticFields, ___emptySHA1_0),
	offsetof(PKCS1_t1838_StaticFields, ___emptySHA256_1),
	offsetof(PKCS1_t1838_StaticFields, ___emptySHA384_2),
	offsetof(PKCS1_t1838_StaticFields, ___emptySHA512_3),
	offsetof(PrivateKeyInfo_t1839, ____version_0),
	offsetof(PrivateKeyInfo_t1839, ____algorithm_1),
	offsetof(PrivateKeyInfo_t1839, ____key_2),
	offsetof(PrivateKeyInfo_t1839, ____list_3),
	offsetof(EncryptedPrivateKeyInfo_t1840, ____algorithm_0),
	offsetof(EncryptedPrivateKeyInfo_t1840, ____salt_1),
	offsetof(EncryptedPrivateKeyInfo_t1840, ____iterations_2),
	offsetof(EncryptedPrivateKeyInfo_t1840, ____data_3),
	offsetof(RSAManaged_t1843, ___isCRTpossible_2),
	offsetof(RSAManaged_t1843, ___keyBlinding_3),
	offsetof(RSAManaged_t1843, ___keypairGenerated_4),
	offsetof(RSAManaged_t1843, ___m_disposed_5),
	offsetof(RSAManaged_t1843, ___d_6),
	offsetof(RSAManaged_t1843, ___p_7),
	offsetof(RSAManaged_t1843, ___q_8),
	offsetof(RSAManaged_t1843, ___dp_9),
	offsetof(RSAManaged_t1843, ___dq_10),
	offsetof(RSAManaged_t1843, ___qInv_11),
	offsetof(RSAManaged_t1843, ___n_12),
	offsetof(RSAManaged_t1843, ___e_13),
	offsetof(RSAManaged_t1843, ___KeyGenerated_14),
	offsetof(SymmetricTransform_t1844, ___algo_0),
	offsetof(SymmetricTransform_t1844, ___encrypt_1),
	offsetof(SymmetricTransform_t1844, ___BlockSizeByte_2),
	offsetof(SymmetricTransform_t1844, ___temp_3),
	offsetof(SymmetricTransform_t1844, ___temp2_4),
	offsetof(SymmetricTransform_t1844, ___workBuff_5),
	offsetof(SymmetricTransform_t1844, ___workout_6),
	offsetof(SymmetricTransform_t1844, ___FeedBackByte_7),
	offsetof(SymmetricTransform_t1844, ___FeedBackIter_8),
	offsetof(SymmetricTransform_t1844, ___m_disposed_9),
	offsetof(SymmetricTransform_t1844, ___lastBlock_10),
	offsetof(SymmetricTransform_t1844, ____rng_11),
	offsetof(SafeBag_t1845, ____bagOID_0),
	offsetof(SafeBag_t1845, ____asn1_1),
	offsetof(PKCS12_t1848_StaticFields, ___recommendedIterationCount_0),
	offsetof(PKCS12_t1848, ____password_1),
	offsetof(PKCS12_t1848, ____keyBags_2),
	offsetof(PKCS12_t1848, ____secretBags_3),
	offsetof(PKCS12_t1848, ____certs_4),
	offsetof(PKCS12_t1848, ____keyBagsChanged_5),
	offsetof(PKCS12_t1848, ____secretBagsChanged_6),
	offsetof(PKCS12_t1848, ____certsChanged_7),
	offsetof(PKCS12_t1848, ____iterations_8),
	offsetof(PKCS12_t1848, ____safeBags_9),
	offsetof(PKCS12_t1848_StaticFields, ___password_max_length_10),
	offsetof(PKCS12_t1848_StaticFields, ___U3CU3Ef__switchU24map8_11),
	offsetof(PKCS12_t1848_StaticFields, ___U3CU3Ef__switchU24map9_12),
	offsetof(PKCS12_t1848_StaticFields, ___U3CU3Ef__switchU24mapA_13),
	offsetof(PKCS12_t1848_StaticFields, ___U3CU3Ef__switchU24mapB_14),
	offsetof(DeriveBytes_t1847_StaticFields, ___keyDiversifier_0),
	offsetof(DeriveBytes_t1847_StaticFields, ___ivDiversifier_1),
	offsetof(DeriveBytes_t1847_StaticFields, ___macDiversifier_2),
	offsetof(DeriveBytes_t1847, ____hashName_3),
	offsetof(DeriveBytes_t1847, ____iterations_4),
	offsetof(DeriveBytes_t1847, ____password_5),
	offsetof(DeriveBytes_t1847, ____salt_6),
	offsetof(X501_t1850_StaticFields, ___countryName_0),
	offsetof(X501_t1850_StaticFields, ___organizationName_1),
	offsetof(X501_t1850_StaticFields, ___organizationalUnitName_2),
	offsetof(X501_t1850_StaticFields, ___commonName_3),
	offsetof(X501_t1850_StaticFields, ___localityName_4),
	offsetof(X501_t1850_StaticFields, ___stateOrProvinceName_5),
	offsetof(X501_t1850_StaticFields, ___streetAddress_6),
	offsetof(X501_t1850_StaticFields, ___domainComponent_7),
	offsetof(X501_t1850_StaticFields, ___userid_8),
	offsetof(X501_t1850_StaticFields, ___email_9),
	offsetof(X501_t1850_StaticFields, ___dnQualifier_10),
	offsetof(X501_t1850_StaticFields, ___title_11),
	offsetof(X501_t1850_StaticFields, ___surname_12),
	offsetof(X501_t1850_StaticFields, ___givenName_13),
	offsetof(X501_t1850_StaticFields, ___initial_14),
	offsetof(X509Certificate_t1851, ___decoder_0),
	offsetof(X509Certificate_t1851, ___m_encodedcert_1),
	offsetof(X509Certificate_t1851, ___m_from_2),
	offsetof(X509Certificate_t1851, ___m_until_3),
	offsetof(X509Certificate_t1851, ___issuer_4),
	offsetof(X509Certificate_t1851, ___m_issuername_5),
	offsetof(X509Certificate_t1851, ___m_keyalgo_6),
	offsetof(X509Certificate_t1851, ___m_keyalgoparams_7),
	offsetof(X509Certificate_t1851, ___subject_8),
	offsetof(X509Certificate_t1851, ___m_subject_9),
	offsetof(X509Certificate_t1851, ___m_publickey_10),
	offsetof(X509Certificate_t1851, ___signature_11),
	offsetof(X509Certificate_t1851, ___m_signaturealgo_12),
	offsetof(X509Certificate_t1851, ___m_signaturealgoparams_13),
	offsetof(X509Certificate_t1851, ____dsa_14),
	offsetof(X509Certificate_t1851, ___version_15),
	offsetof(X509Certificate_t1851, ___serialnumber_16),
	offsetof(X509Certificate_t1851, ___issuerUniqueID_17),
	offsetof(X509Certificate_t1851, ___subjectUniqueID_18),
	offsetof(X509Certificate_t1851, ___extensions_19),
	offsetof(X509Certificate_t1851_StaticFields, ___encoding_error_20),
	offsetof(X509CertificateEnumerator_t1853, ___enumerator_0),
	offsetof(X509Extension_t1854, ___extnOid_0),
	offsetof(X509Extension_t1854, ___extnCritical_1),
	offsetof(X509Extension_t1854, ___extnValue_2),
	offsetof(X509ExtensionCollection_t1852, ___readOnly_1),
	offsetof(ASN1_t1846, ___m_nTag_0),
	offsetof(ASN1_t1846, ___m_aValue_1),
	offsetof(ASN1_t1846, ___elist_2),
	offsetof(ContentInfo_t1857, ___contentType_0),
	offsetof(ContentInfo_t1857, ___content_1),
	offsetof(EncryptedData_t1858, ____version_0),
	offsetof(EncryptedData_t1858, ____content_1),
	offsetof(EncryptedData_t1858, ____encryptionAlgorithm_2),
	offsetof(EncryptedData_t1858, ____encrypted_3),
	offsetof(StrongName_t1860, ___rsa_0),
	offsetof(StrongName_t1860, ___publicKey_1),
	offsetof(StrongName_t1860, ___keyToken_2),
	offsetof(StrongName_t1860, ___tokenAlgorithm_3),
	offsetof(StrongName_t1860_StaticFields, ___lockObject_4),
	offsetof(StrongName_t1860_StaticFields, ___initialized_5),
	offsetof(SecurityParser_t1861, ___root_13),
	offsetof(SecurityParser_t1861, ___current_14),
	offsetof(SecurityParser_t1861, ___stack_15),
	offsetof(SmallXmlParser_t1862, ___handler_0),
	offsetof(SmallXmlParser_t1862, ___reader_1),
	offsetof(SmallXmlParser_t1862, ___elementNames_2),
	offsetof(SmallXmlParser_t1862, ___xmlSpaces_3),
	offsetof(SmallXmlParser_t1862, ___xmlSpace_4),
	offsetof(SmallXmlParser_t1862, ___buffer_5),
	offsetof(SmallXmlParser_t1862, ___nameBuffer_6),
	offsetof(SmallXmlParser_t1862, ___isWhitespace_7),
	offsetof(SmallXmlParser_t1862, ___attributes_8),
	offsetof(SmallXmlParser_t1862, ___line_9),
	offsetof(SmallXmlParser_t1862, ___column_10),
	offsetof(SmallXmlParser_t1862, ___resetColumn_11),
	offsetof(SmallXmlParser_t1862_StaticFields, ___U3CU3Ef__switchU24map18_12),
	offsetof(AttrListImpl_t1864, ___attrNames_0),
	offsetof(AttrListImpl_t1864, ___attrValues_1),
	offsetof(SmallXmlParserException_t1867, ___line_11),
	offsetof(SmallXmlParserException_t1867, ___column_12),
	0,
	offsetof(Link_t1869, ___HashCode_0) + sizeof(Object_t),
	offsetof(Link_t1869, ___Next_1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(ArrayList_t450, ____size_1),
	offsetof(ArrayList_t450, ____items_2),
	offsetof(ArrayList_t450, ____version_3),
	offsetof(ArrayList_t450_StaticFields, ___EmptyArray_4),
	offsetof(SimpleEnumerator_t1871, ___list_0),
	offsetof(SimpleEnumerator_t1871, ___index_1),
	offsetof(SimpleEnumerator_t1871, ___version_2),
	offsetof(SimpleEnumerator_t1871, ___currentElement_3),
	offsetof(SimpleEnumerator_t1871_StaticFields, ___endFlag_4),
	offsetof(ArrayListWrapper_t1872, ___m_InnerArrayList_5),
	offsetof(SynchronizedArrayListWrapper_t1873, ___m_SyncRoot_6),
	offsetof(BitArray_t1735, ___m_array_0),
	offsetof(BitArray_t1735, ___m_length_1),
	offsetof(BitArray_t1735, ____version_2),
	offsetof(BitArrayEnumerator_t1876, ____bitArray_0),
	offsetof(BitArrayEnumerator_t1876, ____current_1),
	offsetof(BitArrayEnumerator_t1876, ____index_2),
	offsetof(BitArrayEnumerator_t1876, ____version_3),
	offsetof(CaseInsensitiveComparer_t1753_StaticFields, ___defaultComparer_0),
	offsetof(CaseInsensitiveComparer_t1753_StaticFields, ___defaultInvariantComparer_1),
	offsetof(CaseInsensitiveComparer_t1753, ___culture_2),
	offsetof(CaseInsensitiveHashCodeProvider_t1754_StaticFields, ___singletonInvariant_0),
	offsetof(CaseInsensitiveHashCodeProvider_t1754_StaticFields, ___sync_1),
	offsetof(CaseInsensitiveHashCodeProvider_t1754, ___m_text_2),
	offsetof(CollectionBase_t1464, ___list_0),
	offsetof(Comparer_t1878_StaticFields, ___Default_0),
	offsetof(Comparer_t1878_StaticFields, ___DefaultInvariant_1),
	offsetof(Comparer_t1878, ___m_compareInfo_2),
	offsetof(DictionaryEntry_t451, ____key_0) + sizeof(Object_t),
	offsetof(DictionaryEntry_t451, ____value_1) + sizeof(Object_t),
	0,
	offsetof(Hashtable_t261, ___inUse_1),
	offsetof(Hashtable_t261, ___modificationCount_2),
	offsetof(Hashtable_t261, ___loadFactor_3),
	offsetof(Hashtable_t261, ___table_4),
	offsetof(Hashtable_t261, ___hashes_5),
	offsetof(Hashtable_t261, ___threshold_6),
	offsetof(Hashtable_t261, ___hashKeys_7),
	offsetof(Hashtable_t261, ___hashValues_8),
	offsetof(Hashtable_t261, ___hcpRef_9),
	offsetof(Hashtable_t261, ___comparerRef_10),
	offsetof(Hashtable_t261, ___serializationInfo_11),
	offsetof(Hashtable_t261, ___equalityComparer_12),
	offsetof(Hashtable_t261_StaticFields, ___primeTbl_13),
	offsetof(Slot_t1879, ___key_0) + sizeof(Object_t),
	offsetof(Slot_t1879, ___value_1) + sizeof(Object_t),
	offsetof(KeyMarker_t1880_StaticFields, ___Removed_0),
	offsetof(EnumeratorMode_t1881, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(Enumerator_t1882, ___host_0),
	offsetof(Enumerator_t1882, ___stamp_1),
	offsetof(Enumerator_t1882, ___pos_2),
	offsetof(Enumerator_t1882, ___size_3),
	offsetof(Enumerator_t1882, ___mode_4),
	offsetof(Enumerator_t1882, ___currentKey_5),
	offsetof(Enumerator_t1882, ___currentValue_6),
	offsetof(Enumerator_t1882_StaticFields, ___xstr_7),
	offsetof(HashKeys_t1883, ___host_0),
	offsetof(HashValues_t1884, ___host_0),
	offsetof(Queue_t1887, ____array_0),
	offsetof(Queue_t1887, ____head_1),
	offsetof(Queue_t1887, ____size_2),
	offsetof(Queue_t1887, ____tail_3),
	offsetof(Queue_t1887, ____growFactor_4),
	offsetof(Queue_t1887, ____version_5),
	offsetof(QueueEnumerator_t1886, ___queue_0),
	offsetof(QueueEnumerator_t1886, ____version_1),
	offsetof(QueueEnumerator_t1886, ___current_2),
	offsetof(SortedList_t1757_StaticFields, ___INITIAL_SIZE_0),
	offsetof(SortedList_t1757, ___inUse_1),
	offsetof(SortedList_t1757, ___modificationCount_2),
	offsetof(SortedList_t1757, ___table_3),
	offsetof(SortedList_t1757, ___comparer_4),
	offsetof(SortedList_t1757, ___defaultCapacity_5),
	offsetof(Slot_t1888, ___key_0) + sizeof(Object_t),
	offsetof(Slot_t1888, ___value_1) + sizeof(Object_t),
	offsetof(EnumeratorMode_t1889, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(Enumerator_t1890, ___host_0),
	offsetof(Enumerator_t1890, ___stamp_1),
	offsetof(Enumerator_t1890, ___pos_2),
	offsetof(Enumerator_t1890, ___size_3),
	offsetof(Enumerator_t1890, ___mode_4),
	offsetof(Enumerator_t1890, ___currentKey_5),
	offsetof(Enumerator_t1890, ___currentValue_6),
	offsetof(Enumerator_t1890, ___invalid_7),
	offsetof(Enumerator_t1890_StaticFields, ___xstr_8),
	offsetof(ListKeys_t1891, ___host_0),
	offsetof(Stack_t995, ___contents_0),
	offsetof(Stack_t995, ___current_1),
	offsetof(Stack_t995, ___count_2),
	offsetof(Stack_t995, ___capacity_3),
	offsetof(Stack_t995, ___modCount_4),
	offsetof(Enumerator_t1893, ___stack_0),
	offsetof(Enumerator_t1893, ___modCount_1),
	offsetof(Enumerator_t1893, ___current_2),
	offsetof(AssemblyHashAlgorithm_t1894, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(AssemblyVersionCompatibility_t1895, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(DebuggableAttribute_t1897, ___JITTrackingEnabledFlag_0),
	offsetof(DebuggableAttribute_t1897, ___JITOptimizerDisabledFlag_1),
	offsetof(DebuggableAttribute_t1897, ___debuggingModes_2),
	offsetof(DebuggingModes_t1896, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(DebuggerDisplayAttribute_t1898, ___value_0),
	offsetof(DebuggerDisplayAttribute_t1898, ___type_1),
	offsetof(DebuggerDisplayAttribute_t1898, ___name_2),
	offsetof(DebuggerTypeProxyAttribute_t1900, ___proxy_type_name_0),
	0,
	offsetof(StackFrame_t1032, ___ilOffset_1),
	offsetof(StackFrame_t1032, ___nativeOffset_2),
	offsetof(StackFrame_t1032, ___methodBase_3),
	offsetof(StackFrame_t1032, ___fileName_4),
	offsetof(StackFrame_t1032, ___lineNumber_5),
	offsetof(StackFrame_t1032, ___columnNumber_6),
	offsetof(StackFrame_t1032, ___internalMethodName_7),
	0,
	offsetof(StackTrace_t1011, ___frames_1),
	offsetof(StackTrace_t1011, ___debug_info_2),
	offsetof(Calendar_t1902, ___m_isReadOnly_0),
	offsetof(Calendar_t1902, ___twoDigitYearMax_1),
	offsetof(Calendar_t1902, ___M_AbbrEraNames_2),
	offsetof(Calendar_t1902, ___M_EraNames_3),
	offsetof(CompareInfo_t1582_StaticFields, ___useManagedCollation_0),
	offsetof(CompareInfo_t1582, ___culture_1),
	offsetof(CompareInfo_t1582, ___icu_name_2),
	offsetof(CompareInfo_t1582, ___collator_3),
	offsetof(CompareInfo_t1582_StaticFields, ___collators_4),
	offsetof(CompareInfo_t1582_StaticFields, ___monitor_5),
	offsetof(CompareOptions_t1906, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(CultureInfo_t1031_StaticFields, ___invariant_culture_info_4),
	offsetof(CultureInfo_t1031_StaticFields, ___shared_table_lock_5),
	offsetof(CultureInfo_t1031_StaticFields, ___BootstrapCultureID_6),
	offsetof(CultureInfo_t1031, ___m_isReadOnly_7),
	offsetof(CultureInfo_t1031, ___cultureID_8),
	offsetof(CultureInfo_t1031, ___parent_lcid_9),
	offsetof(CultureInfo_t1031, ___specific_lcid_10),
	offsetof(CultureInfo_t1031, ___datetime_index_11),
	offsetof(CultureInfo_t1031, ___number_index_12),
	offsetof(CultureInfo_t1031, ___m_useUserOverride_13),
	offsetof(CultureInfo_t1031, ___numInfo_14),
	offsetof(CultureInfo_t1031, ___dateTimeInfo_15),
	offsetof(CultureInfo_t1031, ___textInfo_16),
	offsetof(CultureInfo_t1031, ___m_name_17),
	offsetof(CultureInfo_t1031, ___displayname_18),
	offsetof(CultureInfo_t1031, ___englishname_19),
	offsetof(CultureInfo_t1031, ___nativename_20),
	offsetof(CultureInfo_t1031, ___iso3lang_21),
	offsetof(CultureInfo_t1031, ___iso2lang_22),
	offsetof(CultureInfo_t1031, ___icu_name_23),
	offsetof(CultureInfo_t1031, ___win3lang_24),
	offsetof(CultureInfo_t1031, ___territory_25),
	offsetof(CultureInfo_t1031, ___compareInfo_26),
	offsetof(CultureInfo_t1031, ___calendar_data_27),
	offsetof(CultureInfo_t1031, ___textinfo_data_28),
	offsetof(CultureInfo_t1031, ___optional_calendars_29),
	offsetof(CultureInfo_t1031, ___parent_culture_30),
	offsetof(CultureInfo_t1031, ___m_dataItem_31),
	offsetof(CultureInfo_t1031, ___calendar_32),
	offsetof(CultureInfo_t1031, ___constructed_33),
	offsetof(CultureInfo_t1031, ___cached_serialized_form_34),
	offsetof(CultureInfo_t1031_StaticFields, ___MSG_READONLY_35),
	offsetof(CultureInfo_t1031_StaticFields, ___shared_by_number_36),
	offsetof(CultureInfo_t1031_StaticFields, ___shared_by_name_37),
	offsetof(CultureInfo_t1031_StaticFields, ___U3CU3Ef__switchU24map19_38),
	offsetof(CultureInfo_t1031_StaticFields, ___U3CU3Ef__switchU24map1A_39),
	offsetof(DateTimeFormatFlags_t1910, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(DateTimeFormatInfo_t1908_StaticFields, ___MSG_READONLY_1),
	offsetof(DateTimeFormatInfo_t1908_StaticFields, ___MSG_ARRAYSIZE_MONTH_2),
	offsetof(DateTimeFormatInfo_t1908_StaticFields, ___MSG_ARRAYSIZE_DAY_3),
	offsetof(DateTimeFormatInfo_t1908_StaticFields, ___INVARIANT_ABBREVIATED_DAY_NAMES_4),
	offsetof(DateTimeFormatInfo_t1908_StaticFields, ___INVARIANT_DAY_NAMES_5),
	offsetof(DateTimeFormatInfo_t1908_StaticFields, ___INVARIANT_ABBREVIATED_MONTH_NAMES_6),
	offsetof(DateTimeFormatInfo_t1908_StaticFields, ___INVARIANT_MONTH_NAMES_7),
	offsetof(DateTimeFormatInfo_t1908_StaticFields, ___INVARIANT_SHORT_DAY_NAMES_8),
	offsetof(DateTimeFormatInfo_t1908_StaticFields, ___theInvariantDateTimeFormatInfo_9),
	offsetof(DateTimeFormatInfo_t1908, ___m_isReadOnly_10),
	offsetof(DateTimeFormatInfo_t1908, ___amDesignator_11),
	offsetof(DateTimeFormatInfo_t1908, ___pmDesignator_12),
	offsetof(DateTimeFormatInfo_t1908, ___dateSeparator_13),
	offsetof(DateTimeFormatInfo_t1908, ___timeSeparator_14),
	offsetof(DateTimeFormatInfo_t1908, ___shortDatePattern_15),
	offsetof(DateTimeFormatInfo_t1908, ___longDatePattern_16),
	offsetof(DateTimeFormatInfo_t1908, ___shortTimePattern_17),
	offsetof(DateTimeFormatInfo_t1908, ___longTimePattern_18),
	offsetof(DateTimeFormatInfo_t1908, ___monthDayPattern_19),
	offsetof(DateTimeFormatInfo_t1908, ___yearMonthPattern_20),
	offsetof(DateTimeFormatInfo_t1908, ___fullDateTimePattern_21),
	offsetof(DateTimeFormatInfo_t1908, ____RFC1123Pattern_22),
	offsetof(DateTimeFormatInfo_t1908, ____SortableDateTimePattern_23),
	offsetof(DateTimeFormatInfo_t1908, ____UniversalSortableDateTimePattern_24),
	offsetof(DateTimeFormatInfo_t1908, ___firstDayOfWeek_25),
	offsetof(DateTimeFormatInfo_t1908, ___calendar_26),
	offsetof(DateTimeFormatInfo_t1908, ___calendarWeekRule_27),
	offsetof(DateTimeFormatInfo_t1908, ___abbreviatedDayNames_28),
	offsetof(DateTimeFormatInfo_t1908, ___dayNames_29),
	offsetof(DateTimeFormatInfo_t1908, ___monthNames_30),
	offsetof(DateTimeFormatInfo_t1908, ___abbreviatedMonthNames_31),
	offsetof(DateTimeFormatInfo_t1908, ___allShortDatePatterns_32),
	offsetof(DateTimeFormatInfo_t1908, ___allLongDatePatterns_33),
	offsetof(DateTimeFormatInfo_t1908, ___allShortTimePatterns_34),
	offsetof(DateTimeFormatInfo_t1908, ___allLongTimePatterns_35),
	offsetof(DateTimeFormatInfo_t1908, ___monthDayPatterns_36),
	offsetof(DateTimeFormatInfo_t1908, ___yearMonthPatterns_37),
	offsetof(DateTimeFormatInfo_t1908, ___shortDayNames_38),
	offsetof(DateTimeFormatInfo_t1908, ___nDataItem_39),
	offsetof(DateTimeFormatInfo_t1908, ___m_useUserOverride_40),
	offsetof(DateTimeFormatInfo_t1908, ___m_isDefaultCalendar_41),
	offsetof(DateTimeFormatInfo_t1908, ___CultureID_42),
	offsetof(DateTimeFormatInfo_t1908, ___bUseCalendarInfo_43),
	offsetof(DateTimeFormatInfo_t1908, ___generalShortTimePattern_44),
	offsetof(DateTimeFormatInfo_t1908, ___generalLongTimePattern_45),
	offsetof(DateTimeFormatInfo_t1908, ___m_eraNames_46),
	offsetof(DateTimeFormatInfo_t1908, ___m_abbrevEraNames_47),
	offsetof(DateTimeFormatInfo_t1908, ___m_abbrevEnglishEraNames_48),
	offsetof(DateTimeFormatInfo_t1908, ___m_dateWords_49),
	offsetof(DateTimeFormatInfo_t1908, ___optionalCalendars_50),
	offsetof(DateTimeFormatInfo_t1908, ___m_superShortDayNames_51),
	offsetof(DateTimeFormatInfo_t1908, ___genitiveMonthNames_52),
	offsetof(DateTimeFormatInfo_t1908, ___m_genitiveAbbreviatedMonthNames_53),
	offsetof(DateTimeFormatInfo_t1908, ___leapYearMonthNames_54),
	offsetof(DateTimeFormatInfo_t1908, ___formatFlags_55),
	offsetof(DateTimeFormatInfo_t1908, ___m_name_56),
	offsetof(DateTimeFormatInfo_t1908, ___all_date_time_patterns_57),
	offsetof(DateTimeStyles_t1911, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(DaylightTime_t1912, ___m_start_0),
	offsetof(DaylightTime_t1912, ___m_end_1),
	offsetof(DaylightTime_t1912, ___m_delta_2),
	offsetof(GregorianCalendar_t1913, ___m_type_4),
	offsetof(GregorianCalendarTypes_t1914, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(NumberFormatInfo_t1907, ___isReadOnly_0),
	offsetof(NumberFormatInfo_t1907, ___decimalFormats_1),
	offsetof(NumberFormatInfo_t1907, ___currencyFormats_2),
	offsetof(NumberFormatInfo_t1907, ___percentFormats_3),
	offsetof(NumberFormatInfo_t1907, ___digitPattern_4),
	offsetof(NumberFormatInfo_t1907, ___zeroPattern_5),
	offsetof(NumberFormatInfo_t1907, ___currencyDecimalDigits_6),
	offsetof(NumberFormatInfo_t1907, ___currencyDecimalSeparator_7),
	offsetof(NumberFormatInfo_t1907, ___currencyGroupSeparator_8),
	offsetof(NumberFormatInfo_t1907, ___currencyGroupSizes_9),
	offsetof(NumberFormatInfo_t1907, ___currencyNegativePattern_10),
	offsetof(NumberFormatInfo_t1907, ___currencyPositivePattern_11),
	offsetof(NumberFormatInfo_t1907, ___currencySymbol_12),
	offsetof(NumberFormatInfo_t1907, ___nanSymbol_13),
	offsetof(NumberFormatInfo_t1907, ___negativeInfinitySymbol_14),
	offsetof(NumberFormatInfo_t1907, ___negativeSign_15),
	offsetof(NumberFormatInfo_t1907, ___numberDecimalDigits_16),
	offsetof(NumberFormatInfo_t1907, ___numberDecimalSeparator_17),
	offsetof(NumberFormatInfo_t1907, ___numberGroupSeparator_18),
	offsetof(NumberFormatInfo_t1907, ___numberGroupSizes_19),
	offsetof(NumberFormatInfo_t1907, ___numberNegativePattern_20),
	offsetof(NumberFormatInfo_t1907, ___percentDecimalDigits_21),
	offsetof(NumberFormatInfo_t1907, ___percentDecimalSeparator_22),
	offsetof(NumberFormatInfo_t1907, ___percentGroupSeparator_23),
	offsetof(NumberFormatInfo_t1907, ___percentGroupSizes_24),
	offsetof(NumberFormatInfo_t1907, ___percentNegativePattern_25),
	offsetof(NumberFormatInfo_t1907, ___percentPositivePattern_26),
	offsetof(NumberFormatInfo_t1907, ___percentSymbol_27),
	offsetof(NumberFormatInfo_t1907, ___perMilleSymbol_28),
	offsetof(NumberFormatInfo_t1907, ___positiveInfinitySymbol_29),
	offsetof(NumberFormatInfo_t1907, ___positiveSign_30),
	offsetof(NumberFormatInfo_t1907, ___ansiCurrencySymbol_31),
	offsetof(NumberFormatInfo_t1907, ___m_dataItem_32),
	offsetof(NumberFormatInfo_t1907, ___m_useUserOverride_33),
	offsetof(NumberFormatInfo_t1907, ___validForParseAsNumber_34),
	offsetof(NumberFormatInfo_t1907, ___validForParseAsCurrency_35),
	offsetof(NumberFormatInfo_t1907, ___nativeDigits_36),
	offsetof(NumberFormatInfo_t1907, ___digitSubstitution_37),
	offsetof(NumberFormatInfo_t1907_StaticFields, ___invariantNativeDigits_38),
	offsetof(NumberStyles_t1915, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(TextInfo_t1818, ___m_isReadOnly_0),
	offsetof(TextInfo_t1818, ___customCultureName_1),
	offsetof(TextInfo_t1818, ___m_win32LangID_2),
	offsetof(TextInfo_t1818, ___ci_3),
	offsetof(TextInfo_t1818, ___handleDotI_4),
	offsetof(TextInfo_t1818, ___data_5),
	offsetof(Data_t1916, ___ansi_0) + sizeof(Object_t),
	offsetof(Data_t1916, ___ebcdic_1) + sizeof(Object_t),
	offsetof(Data_t1916, ___mac_2) + sizeof(Object_t),
	offsetof(Data_t1916, ___oem_3) + sizeof(Object_t),
	offsetof(Data_t1916, ___list_sep_4) + sizeof(Object_t),
	offsetof(UnicodeCategory_t1917, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(BinaryReader_t1919, ___m_stream_0),
	offsetof(BinaryReader_t1919, ___m_encoding_1),
	offsetof(BinaryReader_t1919, ___m_buffer_2),
	offsetof(BinaryReader_t1919, ___decoder_3),
	offsetof(BinaryReader_t1919, ___charBuffer_4),
	offsetof(BinaryReader_t1919, ___m_disposed_5),
	offsetof(DirectoryInfo_t1922, ___current_5),
	offsetof(DirectoryInfo_t1922, ___parent_6),
	offsetof(FileAccess_t1756, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(FileAttributes_t1927, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(FileMode_t1928, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(FileNotFoundException_t1929, ___fileName_11),
	offsetof(FileNotFoundException_t1929, ___fusionLog_12),
	offsetof(FileOptions_t1930, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(FileShare_t1931, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(FileStream_t1576, ___access_1),
	offsetof(FileStream_t1576, ___owner_2),
	offsetof(FileStream_t1576, ___async_3),
	offsetof(FileStream_t1576, ___canseek_4),
	offsetof(FileStream_t1576, ___append_startpos_5),
	offsetof(FileStream_t1576, ___anonymous_6),
	offsetof(FileStream_t1576, ___buf_7),
	offsetof(FileStream_t1576, ___buf_size_8),
	offsetof(FileStream_t1576, ___buf_length_9),
	offsetof(FileStream_t1576, ___buf_offset_10),
	offsetof(FileStream_t1576, ___buf_dirty_11),
	offsetof(FileStream_t1576, ___buf_start_12),
	offsetof(FileStream_t1576, ___name_13),
	offsetof(FileStream_t1576, ___handle_14),
	offsetof(FileStreamAsyncResult_t1934, ___state_0),
	offsetof(FileStreamAsyncResult_t1934, ___completed_1),
	offsetof(FileStreamAsyncResult_t1934, ___wh_2),
	offsetof(FileStreamAsyncResult_t1934, ___cb_3),
	offsetof(FileStreamAsyncResult_t1934, ___Count_4),
	offsetof(FileStreamAsyncResult_t1934, ___OriginalCount_5),
	offsetof(FileStreamAsyncResult_t1934, ___BytesRead_6),
	offsetof(FileStreamAsyncResult_t1934, ___realcb_7),
	offsetof(FileSystemInfo_t1923, ___FullPath_1),
	offsetof(FileSystemInfo_t1923, ___OriginalPath_2),
	offsetof(FileSystemInfo_t1923, ___stat_3),
	offsetof(FileSystemInfo_t1923, ___valid_4),
	offsetof(MemoryStream_t416, ___canWrite_1),
	offsetof(MemoryStream_t416, ___allowGetBuffer_2),
	offsetof(MemoryStream_t416, ___capacity_3),
	offsetof(MemoryStream_t416, ___length_4),
	offsetof(MemoryStream_t416, ___internalBuffer_5),
	offsetof(MemoryStream_t416, ___initialIndex_6),
	offsetof(MemoryStream_t416, ___expandable_7),
	offsetof(MemoryStream_t416, ___streamClosed_8),
	offsetof(MemoryStream_t416, ___position_9),
	offsetof(MemoryStream_t416, ___dirty_bytes_10),
	offsetof(MonoFileType_t1936, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(MonoIO_t1937_StaticFields, ___InvalidFileAttributes_0),
	offsetof(MonoIO_t1937_StaticFields, ___InvalidHandle_1),
	offsetof(MonoIOError_t1938, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(MonoIOStat_t1935, ___Name_0) + sizeof(Object_t),
	offsetof(MonoIOStat_t1935, ___Attributes_1) + sizeof(Object_t),
	offsetof(MonoIOStat_t1935, ___Length_2) + sizeof(Object_t),
	offsetof(MonoIOStat_t1935, ___CreationTime_3) + sizeof(Object_t),
	offsetof(MonoIOStat_t1935, ___LastAccessTime_4) + sizeof(Object_t),
	offsetof(MonoIOStat_t1935, ___LastWriteTime_5) + sizeof(Object_t),
	offsetof(Path_t1380_StaticFields, ___InvalidPathChars_0),
	offsetof(Path_t1380_StaticFields, ___AltDirectorySeparatorChar_1),
	offsetof(Path_t1380_StaticFields, ___DirectorySeparatorChar_2),
	offsetof(Path_t1380_StaticFields, ___PathSeparator_3),
	offsetof(Path_t1380_StaticFields, ___DirectorySeparatorStr_4),
	offsetof(Path_t1380_StaticFields, ___VolumeSeparatorChar_5),
	offsetof(Path_t1380_StaticFields, ___PathSeparatorChars_6),
	offsetof(Path_t1380_StaticFields, ___dirEqualsVolume_7),
	offsetof(SearchPattern_t1940_StaticFields, ___WildcardChars_0),
	offsetof(SearchPattern_t1940_StaticFields, ___InvalidChars_1),
	offsetof(SeekOrigin_t1941, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(Stream_t1512_StaticFields, ___Null_0),
	offsetof(StreamAsyncResult_t1943, ___state_0),
	offsetof(StreamAsyncResult_t1943, ___completed_1),
	offsetof(StreamAsyncResult_t1943, ___done_2),
	offsetof(StreamAsyncResult_t1943, ___exc_3),
	offsetof(StreamAsyncResult_t1943, ___nbytes_4),
	offsetof(StreamAsyncResult_t1943, ___wh_5),
	offsetof(StreamReader_t370, ___input_buffer_1),
	offsetof(StreamReader_t370, ___decoded_buffer_2),
	offsetof(StreamReader_t370, ___decoded_count_3),
	offsetof(StreamReader_t370, ___pos_4),
	offsetof(StreamReader_t370, ___buffer_size_5),
	offsetof(StreamReader_t370, ___do_checks_6),
	offsetof(StreamReader_t370, ___encoding_7),
	offsetof(StreamReader_t370, ___decoder_8),
	offsetof(StreamReader_t370, ___base_stream_9),
	offsetof(StreamReader_t370, ___mayBlock_10),
	offsetof(StreamReader_t370, ___line_builder_11),
	offsetof(StreamReader_t370_StaticFields, ___Null_12),
	offsetof(StreamReader_t370, ___foundCR_13),
	offsetof(StreamWriter_t396, ___internalEncoding_2),
	offsetof(StreamWriter_t396, ___internalStream_3),
	offsetof(StreamWriter_t396, ___iflush_4),
	offsetof(StreamWriter_t396, ___byte_buf_5),
	offsetof(StreamWriter_t396, ___byte_pos_6),
	offsetof(StreamWriter_t396, ___decode_buf_7),
	offsetof(StreamWriter_t396, ___decode_pos_8),
	offsetof(StreamWriter_t396, ___DisposedAlready_9),
	offsetof(StreamWriter_t396, ___preamble_done_10),
	offsetof(StreamWriter_t396_StaticFields, ___Null_11),
	offsetof(StringReader_t67, ___source_1),
	offsetof(StringReader_t67, ___nextChar_2),
	offsetof(StringReader_t67, ___sourceLength_3),
	offsetof(TextReader_t1865_StaticFields, ___Null_0),
	offsetof(SynchronizedReader_t1946, ___reader_1),
	offsetof(TextWriter_t1761, ___CoreNewLine_0),
	offsetof(TextWriter_t1761_StaticFields, ___Null_1),
	offsetof(SynchronizedWriter_t1948, ___writer_2),
	offsetof(SynchronizedWriter_t1948, ___neverClose_3),
	offsetof(UnexceptionalStreamReader_t1949_StaticFields, ___newline_14),
	offsetof(UnexceptionalStreamReader_t1949_StaticFields, ___newlineChar_15),
	offsetof(AssemblyBuilder_t1951, ___modules_10),
	offsetof(AssemblyBuilder_t1951, ___loaded_modules_11),
	offsetof(AssemblyBuilder_t1951, ___corlib_object_type_12),
	offsetof(AssemblyBuilder_t1951, ___corlib_value_type_13),
	offsetof(AssemblyBuilder_t1951, ___corlib_enum_type_14),
	offsetof(AssemblyBuilder_t1951, ___sn_15),
	offsetof(AssemblyBuilder_t1951, ___is_compiler_context_16),
	offsetof(ConstructorBuilder_t1954, ___ilgen_2),
	offsetof(ConstructorBuilder_t1954, ___parameters_3),
	offsetof(ConstructorBuilder_t1954, ___attrs_4),
	offsetof(ConstructorBuilder_t1954, ___iattrs_5),
	offsetof(ConstructorBuilder_t1954, ___table_idx_6),
	offsetof(ConstructorBuilder_t1954, ___call_conv_7),
	offsetof(ConstructorBuilder_t1954, ___type_8),
	offsetof(ConstructorBuilder_t1954, ___pinfo_9),
	offsetof(ConstructorBuilder_t1954, ___init_locals_10),
	offsetof(ConstructorBuilder_t1954, ___paramModReq_11),
	offsetof(ConstructorBuilder_t1954, ___paramModOpt_12),
	offsetof(EnumBuilder_t1959, ____tb_8),
	offsetof(EnumBuilder_t1959, ____underlyingType_9),
	offsetof(FieldBuilder_t1960, ___attrs_0),
	offsetof(FieldBuilder_t1960, ___type_1),
	offsetof(FieldBuilder_t1960, ___name_2),
	offsetof(FieldBuilder_t1960, ___typeb_3),
	offsetof(FieldBuilder_t1960, ___marshal_info_4),
	offsetof(GenericTypeParameterBuilder_t1962, ___tbuilder_8),
	offsetof(GenericTypeParameterBuilder_t1962, ___mbuilder_9),
	offsetof(GenericTypeParameterBuilder_t1962, ___name_10),
	offsetof(GenericTypeParameterBuilder_t1962, ___base_type_11),
	offsetof(ILTokenInfo_t1964, ___member_0) + sizeof(Object_t),
	offsetof(ILTokenInfo_t1964, ___code_pos_1) + sizeof(Object_t),
	offsetof(ILGenerator_t1955_StaticFields, ___void_type_0),
	offsetof(ILGenerator_t1955, ___code_1),
	offsetof(ILGenerator_t1955, ___code_len_2),
	offsetof(ILGenerator_t1955, ___max_stack_3),
	offsetof(ILGenerator_t1955, ___cur_stack_4),
	offsetof(ILGenerator_t1955, ___num_token_fixups_5),
	offsetof(ILGenerator_t1955, ___token_fixups_6),
	offsetof(ILGenerator_t1955, ___labels_7),
	offsetof(ILGenerator_t1955, ___fixups_8),
	offsetof(ILGenerator_t1955, ___num_fixups_9),
	offsetof(ILGenerator_t1955, ___module_10),
	offsetof(ILGenerator_t1955, ___token_gen_11),
	offsetof(LabelFixup_t1965, ___offset_0) + sizeof(Object_t),
	offsetof(LabelFixup_t1965, ___pos_1) + sizeof(Object_t),
	offsetof(LabelFixup_t1965, ___label_idx_2) + sizeof(Object_t),
	offsetof(LabelData_t1966, ___addr_0) + sizeof(Object_t),
	offsetof(LabelData_t1966, ___maxStack_1) + sizeof(Object_t),
	offsetof(MethodBuilder_t1963, ___rtype_0),
	offsetof(MethodBuilder_t1963, ___parameters_1),
	offsetof(MethodBuilder_t1963, ___attrs_2),
	offsetof(MethodBuilder_t1963, ___iattrs_3),
	offsetof(MethodBuilder_t1963, ___name_4),
	offsetof(MethodBuilder_t1963, ___code_5),
	offsetof(MethodBuilder_t1963, ___ilgen_6),
	offsetof(MethodBuilder_t1963, ___type_7),
	offsetof(MethodBuilder_t1963, ___pinfo_8),
	offsetof(MethodBuilder_t1963, ___override_method_9),
	offsetof(MethodBuilder_t1963, ___call_conv_10),
	offsetof(MethodBuilder_t1963, ___generic_params_11),
	offsetof(MethodToken_t1973, ___tokValue_0) + sizeof(Object_t),
	offsetof(MethodToken_t1973_StaticFields, ___Empty_1),
	offsetof(ModuleBuilder_t1974, ___num_types_10),
	offsetof(ModuleBuilder_t1974, ___types_11),
	offsetof(ModuleBuilder_t1974, ___assemblyb_12),
	offsetof(ModuleBuilder_t1974, ___table_indexes_13),
	offsetof(ModuleBuilder_t1974, ___token_gen_14),
	offsetof(ModuleBuilder_t1974_StaticFields, ___type_modifiers_15),
	offsetof(ModuleBuilderTokenGenerator_t1976, ___mb_0),
	offsetof(OpCode_t1977, ___op1_0) + sizeof(Object_t),
	offsetof(OpCode_t1977, ___op2_1) + sizeof(Object_t),
	offsetof(OpCode_t1977, ___push_2) + sizeof(Object_t),
	offsetof(OpCode_t1977, ___pop_3) + sizeof(Object_t),
	offsetof(OpCode_t1977, ___size_4) + sizeof(Object_t),
	offsetof(OpCode_t1977, ___type_5) + sizeof(Object_t),
	offsetof(OpCode_t1977, ___args_6) + sizeof(Object_t),
	offsetof(OpCode_t1977, ___flow_7) + sizeof(Object_t),
	offsetof(OpCodeNames_t1978_StaticFields, ___names_0),
	offsetof(OpCodes_t1979_StaticFields, ___Nop_0),
	offsetof(OpCodes_t1979_StaticFields, ___Break_1),
	offsetof(OpCodes_t1979_StaticFields, ___Ldarg_0_2),
	offsetof(OpCodes_t1979_StaticFields, ___Ldarg_1_3),
	offsetof(OpCodes_t1979_StaticFields, ___Ldarg_2_4),
	offsetof(OpCodes_t1979_StaticFields, ___Ldarg_3_5),
	offsetof(OpCodes_t1979_StaticFields, ___Ldloc_0_6),
	offsetof(OpCodes_t1979_StaticFields, ___Ldloc_1_7),
	offsetof(OpCodes_t1979_StaticFields, ___Ldloc_2_8),
	offsetof(OpCodes_t1979_StaticFields, ___Ldloc_3_9),
	offsetof(OpCodes_t1979_StaticFields, ___Stloc_0_10),
	offsetof(OpCodes_t1979_StaticFields, ___Stloc_1_11),
	offsetof(OpCodes_t1979_StaticFields, ___Stloc_2_12),
	offsetof(OpCodes_t1979_StaticFields, ___Stloc_3_13),
	offsetof(OpCodes_t1979_StaticFields, ___Ldarg_S_14),
	offsetof(OpCodes_t1979_StaticFields, ___Ldarga_S_15),
	offsetof(OpCodes_t1979_StaticFields, ___Starg_S_16),
	offsetof(OpCodes_t1979_StaticFields, ___Ldloc_S_17),
	offsetof(OpCodes_t1979_StaticFields, ___Ldloca_S_18),
	offsetof(OpCodes_t1979_StaticFields, ___Stloc_S_19),
	offsetof(OpCodes_t1979_StaticFields, ___Ldnull_20),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_M1_21),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_0_22),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_1_23),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_2_24),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_3_25),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_4_26),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_5_27),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_6_28),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_7_29),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_8_30),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_S_31),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I4_32),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_I8_33),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_R4_34),
	offsetof(OpCodes_t1979_StaticFields, ___Ldc_R8_35),
	offsetof(OpCodes_t1979_StaticFields, ___Dup_36),
	offsetof(OpCodes_t1979_StaticFields, ___Pop_37),
	offsetof(OpCodes_t1979_StaticFields, ___Jmp_38),
	offsetof(OpCodes_t1979_StaticFields, ___Call_39),
	offsetof(OpCodes_t1979_StaticFields, ___Calli_40),
	offsetof(OpCodes_t1979_StaticFields, ___Ret_41),
	offsetof(OpCodes_t1979_StaticFields, ___Br_S_42),
	offsetof(OpCodes_t1979_StaticFields, ___Brfalse_S_43),
	offsetof(OpCodes_t1979_StaticFields, ___Brtrue_S_44),
	offsetof(OpCodes_t1979_StaticFields, ___Beq_S_45),
	offsetof(OpCodes_t1979_StaticFields, ___Bge_S_46),
	offsetof(OpCodes_t1979_StaticFields, ___Bgt_S_47),
	offsetof(OpCodes_t1979_StaticFields, ___Ble_S_48),
	offsetof(OpCodes_t1979_StaticFields, ___Blt_S_49),
	offsetof(OpCodes_t1979_StaticFields, ___Bne_Un_S_50),
	offsetof(OpCodes_t1979_StaticFields, ___Bge_Un_S_51),
	offsetof(OpCodes_t1979_StaticFields, ___Bgt_Un_S_52),
	offsetof(OpCodes_t1979_StaticFields, ___Ble_Un_S_53),
	offsetof(OpCodes_t1979_StaticFields, ___Blt_Un_S_54),
	offsetof(OpCodes_t1979_StaticFields, ___Br_55),
	offsetof(OpCodes_t1979_StaticFields, ___Brfalse_56),
	offsetof(OpCodes_t1979_StaticFields, ___Brtrue_57),
	offsetof(OpCodes_t1979_StaticFields, ___Beq_58),
	offsetof(OpCodes_t1979_StaticFields, ___Bge_59),
	offsetof(OpCodes_t1979_StaticFields, ___Bgt_60),
	offsetof(OpCodes_t1979_StaticFields, ___Ble_61),
	offsetof(OpCodes_t1979_StaticFields, ___Blt_62),
	offsetof(OpCodes_t1979_StaticFields, ___Bne_Un_63),
	offsetof(OpCodes_t1979_StaticFields, ___Bge_Un_64),
	offsetof(OpCodes_t1979_StaticFields, ___Bgt_Un_65),
	offsetof(OpCodes_t1979_StaticFields, ___Ble_Un_66),
	offsetof(OpCodes_t1979_StaticFields, ___Blt_Un_67),
	offsetof(OpCodes_t1979_StaticFields, ___Switch_68),
	offsetof(OpCodes_t1979_StaticFields, ___Ldind_I1_69),
	offsetof(OpCodes_t1979_StaticFields, ___Ldind_U1_70),
	offsetof(OpCodes_t1979_StaticFields, ___Ldind_I2_71),
	offsetof(OpCodes_t1979_StaticFields, ___Ldind_U2_72),
	offsetof(OpCodes_t1979_StaticFields, ___Ldind_I4_73),
	offsetof(OpCodes_t1979_StaticFields, ___Ldind_U4_74),
	offsetof(OpCodes_t1979_StaticFields, ___Ldind_I8_75),
	offsetof(OpCodes_t1979_StaticFields, ___Ldind_I_76),
	offsetof(OpCodes_t1979_StaticFields, ___Ldind_R4_77),
	offsetof(OpCodes_t1979_StaticFields, ___Ldind_R8_78),
	offsetof(OpCodes_t1979_StaticFields, ___Ldind_Ref_79),
	offsetof(OpCodes_t1979_StaticFields, ___Stind_Ref_80),
	offsetof(OpCodes_t1979_StaticFields, ___Stind_I1_81),
	offsetof(OpCodes_t1979_StaticFields, ___Stind_I2_82),
	offsetof(OpCodes_t1979_StaticFields, ___Stind_I4_83),
	offsetof(OpCodes_t1979_StaticFields, ___Stind_I8_84),
	offsetof(OpCodes_t1979_StaticFields, ___Stind_R4_85),
	offsetof(OpCodes_t1979_StaticFields, ___Stind_R8_86),
	offsetof(OpCodes_t1979_StaticFields, ___Add_87),
	offsetof(OpCodes_t1979_StaticFields, ___Sub_88),
	offsetof(OpCodes_t1979_StaticFields, ___Mul_89),
	offsetof(OpCodes_t1979_StaticFields, ___Div_90),
	offsetof(OpCodes_t1979_StaticFields, ___Div_Un_91),
	offsetof(OpCodes_t1979_StaticFields, ___Rem_92),
	offsetof(OpCodes_t1979_StaticFields, ___Rem_Un_93),
	offsetof(OpCodes_t1979_StaticFields, ___And_94),
	offsetof(OpCodes_t1979_StaticFields, ___Or_95),
	offsetof(OpCodes_t1979_StaticFields, ___Xor_96),
	offsetof(OpCodes_t1979_StaticFields, ___Shl_97),
	offsetof(OpCodes_t1979_StaticFields, ___Shr_98),
	offsetof(OpCodes_t1979_StaticFields, ___Shr_Un_99),
	offsetof(OpCodes_t1979_StaticFields, ___Neg_100),
	offsetof(OpCodes_t1979_StaticFields, ___Not_101),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_I1_102),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_I2_103),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_I4_104),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_I8_105),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_R4_106),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_R8_107),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_U4_108),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_U8_109),
	offsetof(OpCodes_t1979_StaticFields, ___Callvirt_110),
	offsetof(OpCodes_t1979_StaticFields, ___Cpobj_111),
	offsetof(OpCodes_t1979_StaticFields, ___Ldobj_112),
	offsetof(OpCodes_t1979_StaticFields, ___Ldstr_113),
	offsetof(OpCodes_t1979_StaticFields, ___Newobj_114),
	offsetof(OpCodes_t1979_StaticFields, ___Castclass_115),
	offsetof(OpCodes_t1979_StaticFields, ___Isinst_116),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_R_Un_117),
	offsetof(OpCodes_t1979_StaticFields, ___Unbox_118),
	offsetof(OpCodes_t1979_StaticFields, ___Throw_119),
	offsetof(OpCodes_t1979_StaticFields, ___Ldfld_120),
	offsetof(OpCodes_t1979_StaticFields, ___Ldflda_121),
	offsetof(OpCodes_t1979_StaticFields, ___Stfld_122),
	offsetof(OpCodes_t1979_StaticFields, ___Ldsfld_123),
	offsetof(OpCodes_t1979_StaticFields, ___Ldsflda_124),
	offsetof(OpCodes_t1979_StaticFields, ___Stsfld_125),
	offsetof(OpCodes_t1979_StaticFields, ___Stobj_126),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_I1_Un_127),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_I2_Un_128),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_I4_Un_129),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_I8_Un_130),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_U1_Un_131),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_U2_Un_132),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_U4_Un_133),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_U8_Un_134),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_I_Un_135),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_U_Un_136),
	offsetof(OpCodes_t1979_StaticFields, ___Box_137),
	offsetof(OpCodes_t1979_StaticFields, ___Newarr_138),
	offsetof(OpCodes_t1979_StaticFields, ___Ldlen_139),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelema_140),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_I1_141),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_U1_142),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_I2_143),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_U2_144),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_I4_145),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_U4_146),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_I8_147),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_I_148),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_R4_149),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_R8_150),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_Ref_151),
	offsetof(OpCodes_t1979_StaticFields, ___Stelem_I_152),
	offsetof(OpCodes_t1979_StaticFields, ___Stelem_I1_153),
	offsetof(OpCodes_t1979_StaticFields, ___Stelem_I2_154),
	offsetof(OpCodes_t1979_StaticFields, ___Stelem_I4_155),
	offsetof(OpCodes_t1979_StaticFields, ___Stelem_I8_156),
	offsetof(OpCodes_t1979_StaticFields, ___Stelem_R4_157),
	offsetof(OpCodes_t1979_StaticFields, ___Stelem_R8_158),
	offsetof(OpCodes_t1979_StaticFields, ___Stelem_Ref_159),
	offsetof(OpCodes_t1979_StaticFields, ___Ldelem_160),
	offsetof(OpCodes_t1979_StaticFields, ___Stelem_161),
	offsetof(OpCodes_t1979_StaticFields, ___Unbox_Any_162),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_I1_163),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_U1_164),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_I2_165),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_U2_166),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_I4_167),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_U4_168),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_I8_169),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_U8_170),
	offsetof(OpCodes_t1979_StaticFields, ___Refanyval_171),
	offsetof(OpCodes_t1979_StaticFields, ___Ckfinite_172),
	offsetof(OpCodes_t1979_StaticFields, ___Mkrefany_173),
	offsetof(OpCodes_t1979_StaticFields, ___Ldtoken_174),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_U2_175),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_U1_176),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_I_177),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_I_178),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_Ovf_U_179),
	offsetof(OpCodes_t1979_StaticFields, ___Add_Ovf_180),
	offsetof(OpCodes_t1979_StaticFields, ___Add_Ovf_Un_181),
	offsetof(OpCodes_t1979_StaticFields, ___Mul_Ovf_182),
	offsetof(OpCodes_t1979_StaticFields, ___Mul_Ovf_Un_183),
	offsetof(OpCodes_t1979_StaticFields, ___Sub_Ovf_184),
	offsetof(OpCodes_t1979_StaticFields, ___Sub_Ovf_Un_185),
	offsetof(OpCodes_t1979_StaticFields, ___Endfinally_186),
	offsetof(OpCodes_t1979_StaticFields, ___Leave_187),
	offsetof(OpCodes_t1979_StaticFields, ___Leave_S_188),
	offsetof(OpCodes_t1979_StaticFields, ___Stind_I_189),
	offsetof(OpCodes_t1979_StaticFields, ___Conv_U_190),
	offsetof(OpCodes_t1979_StaticFields, ___Prefix7_191),
	offsetof(OpCodes_t1979_StaticFields, ___Prefix6_192),
	offsetof(OpCodes_t1979_StaticFields, ___Prefix5_193),
	offsetof(OpCodes_t1979_StaticFields, ___Prefix4_194),
	offsetof(OpCodes_t1979_StaticFields, ___Prefix3_195),
	offsetof(OpCodes_t1979_StaticFields, ___Prefix2_196),
	offsetof(OpCodes_t1979_StaticFields, ___Prefix1_197),
	offsetof(OpCodes_t1979_StaticFields, ___Prefixref_198),
	offsetof(OpCodes_t1979_StaticFields, ___Arglist_199),
	offsetof(OpCodes_t1979_StaticFields, ___Ceq_200),
	offsetof(OpCodes_t1979_StaticFields, ___Cgt_201),
	offsetof(OpCodes_t1979_StaticFields, ___Cgt_Un_202),
	offsetof(OpCodes_t1979_StaticFields, ___Clt_203),
	offsetof(OpCodes_t1979_StaticFields, ___Clt_Un_204),
	offsetof(OpCodes_t1979_StaticFields, ___Ldftn_205),
	offsetof(OpCodes_t1979_StaticFields, ___Ldvirtftn_206),
	offsetof(OpCodes_t1979_StaticFields, ___Ldarg_207),
	offsetof(OpCodes_t1979_StaticFields, ___Ldarga_208),
	offsetof(OpCodes_t1979_StaticFields, ___Starg_209),
	offsetof(OpCodes_t1979_StaticFields, ___Ldloc_210),
	offsetof(OpCodes_t1979_StaticFields, ___Ldloca_211),
	offsetof(OpCodes_t1979_StaticFields, ___Stloc_212),
	offsetof(OpCodes_t1979_StaticFields, ___Localloc_213),
	offsetof(OpCodes_t1979_StaticFields, ___Endfilter_214),
	offsetof(OpCodes_t1979_StaticFields, ___Unaligned_215),
	offsetof(OpCodes_t1979_StaticFields, ___Volatile_216),
	offsetof(OpCodes_t1979_StaticFields, ___Tailcall_217),
	offsetof(OpCodes_t1979_StaticFields, ___Initobj_218),
	offsetof(OpCodes_t1979_StaticFields, ___Constrained_219),
	offsetof(OpCodes_t1979_StaticFields, ___Cpblk_220),
	offsetof(OpCodes_t1979_StaticFields, ___Initblk_221),
	offsetof(OpCodes_t1979_StaticFields, ___Rethrow_222),
	offsetof(OpCodes_t1979_StaticFields, ___Sizeof_223),
	offsetof(OpCodes_t1979_StaticFields, ___Refanytype_224),
	offsetof(OpCodes_t1979_StaticFields, ___Readonly_225),
	offsetof(ParameterBuilder_t1980, ___name_0),
	offsetof(ParameterBuilder_t1980, ___attrs_1),
	offsetof(ParameterBuilder_t1980, ___position_2),
	offsetof(StackBehaviour_t1981, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(TypeBuilder_t1956, ___tname_8),
	offsetof(TypeBuilder_t1956, ___nspace_9),
	offsetof(TypeBuilder_t1956, ___parent_10),
	offsetof(TypeBuilder_t1956, ___nesting_type_11),
	offsetof(TypeBuilder_t1956, ___interfaces_12),
	offsetof(TypeBuilder_t1956, ___num_methods_13),
	offsetof(TypeBuilder_t1956, ___methods_14),
	offsetof(TypeBuilder_t1956, ___ctors_15),
	offsetof(TypeBuilder_t1956, ___fields_16),
	offsetof(TypeBuilder_t1956, ___attrs_17),
	offsetof(TypeBuilder_t1956, ___pmodule_18),
	offsetof(TypeBuilder_t1956, ___generic_params_19),
	offsetof(TypeBuilder_t1956, ___created_20),
	offsetof(TypeBuilder_t1956, ___fullname_21),
	offsetof(TypeBuilder_t1956, ___createTypeCalled_22),
	offsetof(TypeBuilder_t1956, ___underlying_type_23),
	offsetof(UnmanagedMarshal_t1961, ___count_0),
	offsetof(UnmanagedMarshal_t1961, ___t_1),
	offsetof(UnmanagedMarshal_t1961, ___tbase_2),
	offsetof(UnmanagedMarshal_t1961, ___guid_3),
	offsetof(UnmanagedMarshal_t1961, ___mcookie_4),
	offsetof(UnmanagedMarshal_t1961, ___marshaltype_5),
	offsetof(UnmanagedMarshal_t1961, ___marshaltyperef_6),
	offsetof(UnmanagedMarshal_t1961, ___param_num_7),
	offsetof(UnmanagedMarshal_t1961, ___has_size_8),
	offsetof(Assembly_t1758, ____mono_assembly_0),
	offsetof(Assembly_t1758, ___resolve_event_holder_1),
	offsetof(Assembly_t1758, ____evidence_2),
	offsetof(Assembly_t1758, ____minimum_3),
	offsetof(Assembly_t1758, ____optional_4),
	offsetof(Assembly_t1758, ____refuse_5),
	offsetof(Assembly_t1758, ____granted_6),
	offsetof(Assembly_t1758, ____denied_7),
	offsetof(Assembly_t1758, ___fromByteArray_8),
	offsetof(Assembly_t1758, ___assemblyName_9),
	offsetof(AssemblyCompanyAttribute_t1989, ___name_0),
	offsetof(AssemblyConfigurationAttribute_t1990, ___name_0),
	offsetof(AssemblyCopyrightAttribute_t1991, ___name_0),
	offsetof(AssemblyDefaultAliasAttribute_t1992, ___name_0),
	offsetof(AssemblyDelaySignAttribute_t1993, ___delay_0),
	offsetof(AssemblyDescriptionAttribute_t1994, ___name_0),
	offsetof(AssemblyFileVersionAttribute_t1995, ___name_0),
	offsetof(AssemblyInformationalVersionAttribute_t1996, ___name_0),
	offsetof(AssemblyKeyFileAttribute_t1997, ___name_0),
	offsetof(AssemblyName_t1998, ___name_0),
	offsetof(AssemblyName_t1998, ___codebase_1),
	offsetof(AssemblyName_t1998, ___major_2),
	offsetof(AssemblyName_t1998, ___minor_3),
	offsetof(AssemblyName_t1998, ___build_4),
	offsetof(AssemblyName_t1998, ___revision_5),
	offsetof(AssemblyName_t1998, ___cultureinfo_6),
	offsetof(AssemblyName_t1998, ___flags_7),
	offsetof(AssemblyName_t1998, ___hashalg_8),
	offsetof(AssemblyName_t1998, ___keypair_9),
	offsetof(AssemblyName_t1998, ___publicKey_10),
	offsetof(AssemblyName_t1998, ___keyToken_11),
	offsetof(AssemblyName_t1998, ___versioncompat_12),
	offsetof(AssemblyName_t1998, ___version_13),
	offsetof(AssemblyName_t1998, ___processor_architecture_14),
	offsetof(AssemblyNameFlags_t2000, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(AssemblyProductAttribute_t2001, ___name_0),
	offsetof(AssemblyTitleAttribute_t2002, ___name_0),
	offsetof(AssemblyTrademarkAttribute_t2003, ___name_0),
	offsetof(Binder_t1029_StaticFields, ___default_binder_0),
	offsetof(BindingFlags_t2005, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(CallingConventions_t2006, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(ConstructorInfo_t1042_StaticFields, ___ConstructorName_0),
	offsetof(ConstructorInfo_t1042_StaticFields, ___TypeConstructorName_1),
	offsetof(EventAttributes_t2007, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(EventInfo_t, ___cached_add_event_0),
	offsetof(FieldAttributes_t2009, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(MemberInfoSerializationHolder_t2010, ____memberName_0),
	offsetof(MemberInfoSerializationHolder_t2010, ____memberSignature_1),
	offsetof(MemberInfoSerializationHolder_t2010, ____memberType_2),
	offsetof(MemberInfoSerializationHolder_t2010, ____reflectedType_3),
	offsetof(MemberInfoSerializationHolder_t2010, ____genericArguments_4),
	offsetof(MemberTypes_t2011, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(MethodAttributes_t2012, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(MethodImplAttributes_t2013, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(Missing_t2014_StaticFields, ___Value_0),
	0,
	offsetof(Module_t1970_StaticFields, ___FilterTypeName_1),
	offsetof(Module_t1970_StaticFields, ___FilterTypeNameIgnoreCase_2),
	offsetof(Module_t1970, ____impl_3),
	offsetof(Module_t1970, ___assembly_4),
	offsetof(Module_t1970, ___fqname_5),
	offsetof(Module_t1970, ___name_6),
	offsetof(Module_t1970, ___scopename_7),
	offsetof(Module_t1970, ___is_resource_8),
	offsetof(Module_t1970, ___token_9),
	offsetof(MonoEventInfo_t2016, ___declaring_type_0) + sizeof(Object_t),
	offsetof(MonoEventInfo_t2016, ___reflected_type_1) + sizeof(Object_t),
	offsetof(MonoEventInfo_t2016, ___name_2) + sizeof(Object_t),
	offsetof(MonoEventInfo_t2016, ___add_method_3) + sizeof(Object_t),
	offsetof(MonoEventInfo_t2016, ___remove_method_4) + sizeof(Object_t),
	offsetof(MonoEventInfo_t2016, ___raise_method_5) + sizeof(Object_t),
	offsetof(MonoEventInfo_t2016, ___attrs_6) + sizeof(Object_t),
	offsetof(MonoEventInfo_t2016, ___other_methods_7) + sizeof(Object_t),
	offsetof(MonoEvent_t, ___klass_1),
	offsetof(MonoEvent_t, ___handle_2),
	offsetof(MonoField_t, ___klass_0),
	offsetof(MonoField_t, ___fhandle_1),
	offsetof(MonoField_t, ___name_2),
	offsetof(MonoField_t, ___type_3),
	offsetof(MonoField_t, ___attrs_4),
	offsetof(MonoMethodInfo_t2019, ___parent_0) + sizeof(Object_t),
	offsetof(MonoMethodInfo_t2019, ___ret_1) + sizeof(Object_t),
	offsetof(MonoMethodInfo_t2019, ___attrs_2) + sizeof(Object_t),
	offsetof(MonoMethodInfo_t2019, ___iattrs_3) + sizeof(Object_t),
	offsetof(MonoMethodInfo_t2019, ___callconv_4) + sizeof(Object_t),
	offsetof(MonoMethod_t, ___mhandle_0),
	offsetof(MonoMethod_t, ___name_1),
	offsetof(MonoMethod_t, ___reftype_2),
	offsetof(MonoCMethod_t2018, ___mhandle_2),
	offsetof(MonoCMethod_t2018, ___name_3),
	offsetof(MonoCMethod_t2018, ___reftype_4),
	offsetof(MonoPropertyInfo_t2020, ___parent_0) + sizeof(Object_t),
	offsetof(MonoPropertyInfo_t2020, ___name_1) + sizeof(Object_t),
	offsetof(MonoPropertyInfo_t2020, ___get_method_2) + sizeof(Object_t),
	offsetof(MonoPropertyInfo_t2020, ___set_method_3) + sizeof(Object_t),
	offsetof(MonoPropertyInfo_t2020, ___attrs_4) + sizeof(Object_t),
	offsetof(PInfo_t2021, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(MonoProperty_t, ___klass_0),
	offsetof(MonoProperty_t, ___prop_1),
	offsetof(MonoProperty_t, ___info_2),
	offsetof(MonoProperty_t, ___cached_3),
	offsetof(MonoProperty_t, ___cached_getter_4),
	offsetof(ParameterAttributes_t2023, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(ParameterInfo_t1035, ___ClassImpl_0),
	offsetof(ParameterInfo_t1035, ___DefaultValueImpl_1),
	offsetof(ParameterInfo_t1035, ___MemberImpl_2),
	offsetof(ParameterInfo_t1035, ___NameImpl_3),
	offsetof(ParameterInfo_t1035, ___PositionImpl_4),
	offsetof(ParameterInfo_t1035, ___AttrsImpl_5),
	offsetof(ParameterInfo_t1035, ___marshalAs_6),
	offsetof(ParameterModifier_t2024, ____byref_0) + sizeof(Object_t),
	offsetof(Pointer_t2025, ___data_0),
	offsetof(Pointer_t2025, ___type_1),
	offsetof(ProcessorArchitecture_t2026, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(PropertyAttributes_t2027, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(StrongNameKeyPair_t1999, ____publicKey_0),
	offsetof(StrongNameKeyPair_t1999, ____keyPairContainer_1),
	offsetof(StrongNameKeyPair_t1999, ____keyPairExported_2),
	offsetof(StrongNameKeyPair_t1999, ____keyPairArray_3),
	offsetof(TypeAttributes_t2031, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(NeutralResourcesLanguageAttribute_t2032, ___culture_0),
	offsetof(SatelliteContractVersionAttribute_t2033, ___ver_0),
	offsetof(CompilationRelaxations_t2034, ___value___1) + sizeof(Object_t),
	0,
	offsetof(CompilationRelaxationsAttribute_t2035, ___relax_0),
	offsetof(DefaultDependencyAttribute_t2036, ___hint_0),
	offsetof(LoadHint_t2038, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(Cer_t2040, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(Consistency_t2041, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(ReliabilityContractAttribute_t2043, ___consistency_0),
	offsetof(ReliabilityContractAttribute_t2043, ___cer_1),
	offsetof(CallingConvention_t2045, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(CharSet_t2046, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(ClassInterfaceAttribute_t2047, ___ciType_0),
	offsetof(ClassInterfaceType_t2048, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(ComDefaultInterfaceAttribute_t2049, ____type_0),
	offsetof(ComInterfaceType_t2050, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(DispIdAttribute_t2051, ___id_0),
	offsetof(GCHandle_t1300, ___handle_0) + sizeof(Object_t),
	offsetof(GCHandleType_t2052, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(InterfaceTypeAttribute_t2053, ___intType_0),
	offsetof(Marshal_t1284_StaticFields, ___SystemMaxDBCSCharSize_0),
	offsetof(Marshal_t1284_StaticFields, ___SystemDefaultCharSize_1),
	0,
	offsetof(SafeHandle_t1800, ___handle_0),
	offsetof(SafeHandle_t1800, ___invalid_handle_value_1),
	offsetof(SafeHandle_t1800, ___refcount_2),
	offsetof(SafeHandle_t1800, ___owns_handle_3),
	offsetof(TypeLibImportClassAttribute_t2056, ____importClass_0),
	offsetof(TypeLibVersionAttribute_t2057, ___major_0),
	offsetof(TypeLibVersionAttribute_t2057, ___minor_1),
	offsetof(UnmanagedType_t2058, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(ActivationServices_t2059_StaticFields, ____constructionActivator_0),
	offsetof(AppDomainLevelActivator_t2061, ____activationUrl_0),
	offsetof(AppDomainLevelActivator_t2061, ____next_1),
	offsetof(ContextLevelActivator_t2063, ___m_NextActivator_0),
	offsetof(UrlAttribute_t2065, ___url_1),
	offsetof(ChannelInfo_t2067, ___channelData_0),
	offsetof(ChannelServices_t2068_StaticFields, ___registeredChannels_0),
	offsetof(ChannelServices_t2068_StaticFields, ___delayedClientChannels_1),
	offsetof(ChannelServices_t2068_StaticFields, ____crossContextSink_2),
	offsetof(ChannelServices_t2068_StaticFields, ___CrossContextUrl_3),
	offsetof(ChannelServices_t2068_StaticFields, ___oldStartModeTypes_4),
	offsetof(CrossAppDomainData_t2070, ____ContextID_0),
	offsetof(CrossAppDomainData_t2070, ____DomainID_1),
	offsetof(CrossAppDomainData_t2070, ____processGuid_2),
	offsetof(CrossAppDomainChannel_t2071_StaticFields, ___s_lock_0),
	offsetof(CrossAppDomainSink_t2072_StaticFields, ___s_sinks_0),
	offsetof(CrossAppDomainSink_t2072_StaticFields, ___processMessageMethod_1),
	offsetof(CrossAppDomainSink_t2072, ____domainID_2),
	offsetof(SinkProviderData_t2073, ___sinkName_0),
	offsetof(SinkProviderData_t2073, ___children_1),
	offsetof(SinkProviderData_t2073, ___properties_2),
	offsetof(Context_t2074, ___domain_id_0),
	offsetof(Context_t2074, ___context_id_1),
	offsetof(Context_t2074, ___static_data_2),
	offsetof(Context_t2074_StaticFields, ___default_server_context_sink_3),
	offsetof(Context_t2074, ___server_context_sink_chain_4),
	offsetof(Context_t2074, ___client_context_sink_chain_5),
	offsetof(Context_t2074, ___datastore_6),
	offsetof(Context_t2074, ___context_properties_7),
	offsetof(Context_t2074, ___frozen_8),
	offsetof(Context_t2074_StaticFields, ___global_count_9),
	offsetof(Context_t2074_StaticFields, ___namedSlots_10),
	offsetof(Context_t2074_StaticFields, ___global_dynamic_properties_11),
	offsetof(Context_t2074, ___context_dynamic_properties_12),
	offsetof(Context_t2074, ___callback_object_13),
	offsetof(DynamicPropertyCollection_t2075, ____properties_0),
	offsetof(DynamicPropertyReg_t2078, ___Property_0),
	offsetof(DynamicPropertyReg_t2078, ___Sink_1),
	offsetof(ContextAttribute_t2066, ___AttributeName_0),
	offsetof(SynchronizationAttribute_t2082, ____bReEntrant_1),
	offsetof(SynchronizationAttribute_t2082, ____flavor_2),
	offsetof(SynchronizationAttribute_t2082, ____lockCount_3),
	offsetof(SynchronizationAttribute_t2082, ____mutex_4),
	offsetof(SynchronizationAttribute_t2082, ____ownerThread_5),
	offsetof(SynchronizedClientContextSink_t2084, ____next_0),
	offsetof(SynchronizedClientContextSink_t2084, ____att_1),
	offsetof(SynchronizedServerContextSink_t2085, ____next_0),
	offsetof(SynchronizedServerContextSink_t2085, ____att_1),
	offsetof(Lease_t2088, ____leaseExpireTime_1),
	offsetof(Lease_t2088, ____currentState_2),
	offsetof(Lease_t2088, ____initialLeaseTime_3),
	offsetof(Lease_t2088, ____renewOnCallTime_4),
	offsetof(Lease_t2088, ____sponsorshipTimeout_5),
	offsetof(Lease_t2088, ____sponsors_6),
	offsetof(Lease_t2088, ____renewingSponsors_7),
	offsetof(Lease_t2088, ____renewalDelegate_8),
	offsetof(LeaseManager_t2089, ____objects_0),
	offsetof(LeaseManager_t2089, ____timer_1),
	offsetof(LeaseSink_t2091, ____nextSink_0),
	offsetof(LeaseState_t2092, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(LifetimeServices_t2093_StaticFields, ____leaseManagerPollTime_0),
	offsetof(LifetimeServices_t2093_StaticFields, ____leaseTime_1),
	offsetof(LifetimeServices_t2093_StaticFields, ____renewOnCallTime_2),
	offsetof(LifetimeServices_t2093_StaticFields, ____sponsorshipTimeout_3),
	offsetof(LifetimeServices_t2093_StaticFields, ____leaseManager_4),
	offsetof(ArgInfoType_t2094, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(ArgInfo_t2095, ____paramMap_0),
	offsetof(ArgInfo_t2095, ____inoutArgCount_1),
	offsetof(ArgInfo_t2095, ____method_2),
	offsetof(AsyncResult_t2096, ___async_state_0),
	offsetof(AsyncResult_t2096, ___handle_1),
	offsetof(AsyncResult_t2096, ___async_delegate_2),
	offsetof(AsyncResult_t2096, ___data_3),
	offsetof(AsyncResult_t2096, ___object_data_4),
	offsetof(AsyncResult_t2096, ___sync_completed_5),
	offsetof(AsyncResult_t2096, ___completed_6),
	offsetof(AsyncResult_t2096, ___endinvoke_called_7),
	offsetof(AsyncResult_t2096, ___async_callback_8),
	offsetof(AsyncResult_t2096, ___current_9),
	offsetof(AsyncResult_t2096, ___original_10),
	offsetof(AsyncResult_t2096, ___gchandle_11),
	offsetof(AsyncResult_t2096, ___call_message_12),
	offsetof(AsyncResult_t2096, ___message_ctrl_13),
	offsetof(AsyncResult_t2096, ___reply_message_14),
	offsetof(ClientContextTerminatorSink_t2101, ____context_0),
	offsetof(ConstructionCall_t2102, ____activator_11),
	offsetof(ConstructionCall_t2102, ____activationAttributes_12),
	offsetof(ConstructionCall_t2102, ____contextProperties_13),
	offsetof(ConstructionCall_t2102, ____activationType_14),
	offsetof(ConstructionCall_t2102, ____activationTypeName_15),
	offsetof(ConstructionCall_t2102, ____isContextOk_16),
	offsetof(ConstructionCall_t2102_StaticFields, ___U3CU3Ef__switchU24map20_17),
	offsetof(ConstructionCallDictionary_t2104_StaticFields, ___InternalKeys_6),
	offsetof(ConstructionCallDictionary_t2104_StaticFields, ___U3CU3Ef__switchU24map23_7),
	offsetof(ConstructionCallDictionary_t2104_StaticFields, ___U3CU3Ef__switchU24map24_8),
	offsetof(EnvoyTerminatorSink_t2106_StaticFields, ___Instance_0),
	offsetof(Header_t2107, ___HeaderNamespace_0),
	offsetof(Header_t2107, ___MustUnderstand_1),
	offsetof(Header_t2107, ___Name_2),
	offsetof(Header_t2107, ___Value_3),
	offsetof(LogicalCallContext_t2108, ____data_0),
	offsetof(LogicalCallContext_t2108, ____remotingData_1),
	offsetof(MethodCall_t2103, ____uri_0),
	offsetof(MethodCall_t2103, ____typeName_1),
	offsetof(MethodCall_t2103, ____methodName_2),
	offsetof(MethodCall_t2103, ____args_3),
	offsetof(MethodCall_t2103, ____methodSignature_4),
	offsetof(MethodCall_t2103, ____methodBase_5),
	offsetof(MethodCall_t2103, ____callContext_6),
	offsetof(MethodCall_t2103, ____genericArguments_7),
	offsetof(MethodCall_t2103, ___ExternalProperties_8),
	offsetof(MethodCall_t2103, ___InternalProperties_9),
	offsetof(MethodCall_t2103_StaticFields, ___U3CU3Ef__switchU24map1F_10),
	offsetof(MethodCallDictionary_t2110_StaticFields, ___InternalKeys_6),
	offsetof(MethodDictionary_t2105, ____internalProperties_0),
	offsetof(MethodDictionary_t2105, ____message_1),
	offsetof(MethodDictionary_t2105, ____methodKeys_2),
	offsetof(MethodDictionary_t2105, ____ownProperties_3),
	offsetof(MethodDictionary_t2105_StaticFields, ___U3CU3Ef__switchU24map21_4),
	offsetof(MethodDictionary_t2105_StaticFields, ___U3CU3Ef__switchU24map22_5),
	offsetof(DictionaryEnumerator_t2111, ____methodDictionary_0),
	offsetof(DictionaryEnumerator_t2111, ____hashtableEnum_1),
	offsetof(DictionaryEnumerator_t2111, ____posMethod_2),
	offsetof(MethodReturnDictionary_t2113_StaticFields, ___InternalReturnKeys_6),
	offsetof(MethodReturnDictionary_t2113_StaticFields, ___InternalExceptionKeys_7),
	offsetof(MonoMethodMessage_t2098, ___method_0),
	offsetof(MonoMethodMessage_t2098, ___args_1),
	offsetof(MonoMethodMessage_t2098, ___arg_types_2),
	offsetof(MonoMethodMessage_t2098, ___ctx_3),
	offsetof(MonoMethodMessage_t2098, ___rval_4),
	offsetof(MonoMethodMessage_t2098, ___exc_5),
	offsetof(MonoMethodMessage_t2098, ___uri_6),
	offsetof(MonoMethodMessage_t2098, ___methodSignature_7),
	offsetof(RemotingSurrogateSelector_t2116_StaticFields, ___s_cachedTypeObjRef_0),
	offsetof(RemotingSurrogateSelector_t2116_StaticFields, ____objRefSurrogate_1),
	offsetof(RemotingSurrogateSelector_t2116_StaticFields, ____objRemotingSurrogate_2),
	offsetof(RemotingSurrogateSelector_t2116, ____next_3),
	offsetof(ReturnMessage_t2118, ____outArgs_0),
	offsetof(ReturnMessage_t2118, ____args_1),
	offsetof(ReturnMessage_t2118, ____outArgsCount_2),
	offsetof(ReturnMessage_t2118, ____callCtx_3),
	offsetof(ReturnMessage_t2118, ____returnValue_4),
	offsetof(ReturnMessage_t2118, ____uri_5),
	offsetof(ReturnMessage_t2118, ____exception_6),
	offsetof(ReturnMessage_t2118, ____methodBase_7),
	offsetof(ReturnMessage_t2118, ____methodName_8),
	offsetof(ReturnMessage_t2118, ____methodSignature_9),
	offsetof(ReturnMessage_t2118, ____typeName_10),
	offsetof(ReturnMessage_t2118, ____properties_11),
	offsetof(ReturnMessage_t2118, ____inArgInfo_12),
	offsetof(ServerObjectTerminatorSink_t2120, ____nextSink_0),
	offsetof(StackBuilderSink_t2121, ____target_0),
	offsetof(StackBuilderSink_t2121, ____rp_1),
	offsetof(SoapAttribute_t2123, ____useAttribute_0),
	offsetof(SoapAttribute_t2123, ___ProtXmlNamespace_1),
	offsetof(SoapAttribute_t2123, ___ReflectInfo_2),
	offsetof(SoapFieldAttribute_t2124, ____elementName_3),
	offsetof(SoapFieldAttribute_t2124, ____isElement_4),
	offsetof(SoapMethodAttribute_t2125, ____responseElement_3),
	offsetof(SoapMethodAttribute_t2125, ____responseNamespace_4),
	offsetof(SoapMethodAttribute_t2125, ____returnElement_5),
	offsetof(SoapMethodAttribute_t2125, ____soapAction_6),
	offsetof(SoapMethodAttribute_t2125, ____useAttribute_7),
	offsetof(SoapMethodAttribute_t2125, ____namespace_8),
	offsetof(SoapTypeAttribute_t2127, ____useAttribute_3),
	offsetof(SoapTypeAttribute_t2127, ____xmlElementName_4),
	offsetof(SoapTypeAttribute_t2127, ____xmlNamespace_5),
	offsetof(SoapTypeAttribute_t2127, ____xmlTypeName_6),
	offsetof(SoapTypeAttribute_t2127, ____xmlTypeNamespace_7),
	offsetof(SoapTypeAttribute_t2127, ____isType_8),
	offsetof(SoapTypeAttribute_t2127, ____isElement_9),
	offsetof(TransparentProxy_t2129, ____rp_0),
	offsetof(RealProxy_t2122, ___class_to_proxy_0),
	offsetof(RealProxy_t2122, ____targetDomainId_1),
	offsetof(RealProxy_t2122, ____targetUri_2),
	offsetof(RealProxy_t2122, ____objectIdentity_3),
	offsetof(RealProxy_t2122, ____objTP_4),
	offsetof(RemotingProxy_t2131_StaticFields, ____cache_GetTypeMethod_5),
	offsetof(RemotingProxy_t2131_StaticFields, ____cache_GetHashCodeMethod_6),
	offsetof(RemotingProxy_t2131, ____sink_7),
	offsetof(RemotingProxy_t2131, ____hasEnvoySink_8),
	offsetof(RemotingProxy_t2131, ____ctorCall_9),
	offsetof(TrackingServices_t2132_StaticFields, ____handlers_0),
	offsetof(ActivatedClientTypeEntry_t2133, ___applicationUrl_2),
	offsetof(ActivatedClientTypeEntry_t2133, ___obj_type_3),
	offsetof(ActivatedServiceTypeEntry_t2135, ___obj_type_2),
	offsetof(EnvoyInfo_t2136, ___envoySinks_0),
	offsetof(Identity_t2130, ____objectUri_0),
	offsetof(Identity_t2130, ____channelSink_1),
	offsetof(Identity_t2130, ____envoySink_2),
	offsetof(Identity_t2130, ____clientDynamicProperties_3),
	offsetof(Identity_t2130, ____serverDynamicProperties_4),
	offsetof(Identity_t2130, ____objRef_5),
	offsetof(Identity_t2130, ____disposed_6),
	offsetof(ClientIdentity_t2138, ____proxyReference_7),
	offsetof(InternalRemotingServices_t2140_StaticFields, ____soapAttributes_0),
	offsetof(ObjRef_t2137, ___channel_info_0),
	offsetof(ObjRef_t2137, ___uri_1),
	offsetof(ObjRef_t2137, ___typeInfo_2),
	offsetof(ObjRef_t2137, ___envoyInfo_3),
	offsetof(ObjRef_t2137, ___flags_4),
	offsetof(ObjRef_t2137, ____serverType_5),
	offsetof(ObjRef_t2137_StaticFields, ___MarshalledObjectRef_6),
	offsetof(ObjRef_t2137_StaticFields, ___WellKnowObjectRef_7),
	offsetof(ObjRef_t2137_StaticFields, ___U3CU3Ef__switchU24map26_8),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___applicationID_0),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___applicationName_1),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___processGuid_2),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___defaultConfigRead_3),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___defaultDelayedConfigRead_4),
	offsetof(RemotingConfiguration_t2144_StaticFields, ____errorMode_5),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___wellKnownClientEntries_6),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___activatedClientEntries_7),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___wellKnownServiceEntries_8),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___activatedServiceEntries_9),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___channelTemplates_10),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___clientProviderTemplates_11),
	offsetof(RemotingConfiguration_t2144_StaticFields, ___serverProviderTemplates_12),
	offsetof(ConfigHandler_t2145, ___typeEntries_0),
	offsetof(ConfigHandler_t2145, ___channelInstances_1),
	offsetof(ConfigHandler_t2145, ___currentChannel_2),
	offsetof(ConfigHandler_t2145, ___currentProviderData_3),
	offsetof(ConfigHandler_t2145, ___currentClientUrl_4),
	offsetof(ConfigHandler_t2145, ___appName_5),
	offsetof(ConfigHandler_t2145, ___currentXmlPath_6),
	offsetof(ConfigHandler_t2145, ___onlyDelayedChannels_7),
	offsetof(ConfigHandler_t2145_StaticFields, ___U3CU3Ef__switchU24map27_8),
	offsetof(ConfigHandler_t2145_StaticFields, ___U3CU3Ef__switchU24map28_9),
	offsetof(ChannelData_t2146, ___Ref_0),
	offsetof(ChannelData_t2146, ___Type_1),
	offsetof(ChannelData_t2146, ___Id_2),
	offsetof(ChannelData_t2146, ___DelayLoadAsClientChannel_3),
	offsetof(ChannelData_t2146, ____serverProviders_4),
	offsetof(ChannelData_t2146, ____clientProviders_5),
	offsetof(ChannelData_t2146, ____customProperties_6),
	offsetof(ProviderData_t2147, ___Ref_0),
	offsetof(ProviderData_t2147, ___Type_1),
	offsetof(ProviderData_t2147, ___Id_2),
	offsetof(ProviderData_t2147, ___CustomProperties_3),
	offsetof(ProviderData_t2147, ___CustomData_4),
	offsetof(RemotingServices_t2150_StaticFields, ___uri_hash_0),
	offsetof(RemotingServices_t2150_StaticFields, ____serializationFormatter_1),
	offsetof(RemotingServices_t2150_StaticFields, ____deserializationFormatter_2),
	offsetof(RemotingServices_t2150_StaticFields, ___app_id_3),
	offsetof(RemotingServices_t2150_StaticFields, ___next_id_4),
	offsetof(RemotingServices_t2150_StaticFields, ___methodBindings_5),
	offsetof(RemotingServices_t2150_StaticFields, ___FieldSetterMethod_6),
	offsetof(RemotingServices_t2150_StaticFields, ___FieldGetterMethod_7),
	offsetof(ServerIdentity_t1794, ____objectType_7),
	offsetof(ServerIdentity_t1794, ____serverObject_8),
	offsetof(ServerIdentity_t1794, ____serverSink_9),
	offsetof(ServerIdentity_t1794, ____context_10),
	offsetof(ServerIdentity_t1794, ____lease_11),
	offsetof(SoapServices_t2156_StaticFields, ____xmlTypes_0),
	offsetof(SoapServices_t2156_StaticFields, ____xmlElements_1),
	offsetof(SoapServices_t2156_StaticFields, ____soapActions_2),
	offsetof(SoapServices_t2156_StaticFields, ____soapActionsMethods_3),
	offsetof(SoapServices_t2156_StaticFields, ____typeInfos_4),
	offsetof(TypeInfo_t2155, ___Attributes_0),
	offsetof(TypeInfo_t2155, ___Elements_1),
	offsetof(TypeEntry_t2134, ___assembly_name_0),
	offsetof(TypeEntry_t2134, ___type_name_1),
	offsetof(TypeInfo_t2157, ___serverType_0),
	offsetof(TypeInfo_t2157, ___serverHierarchy_1),
	offsetof(TypeInfo_t2157, ___interfacesImplemented_2),
	offsetof(WellKnownClientTypeEntry_t2158, ___obj_type_2),
	offsetof(WellKnownClientTypeEntry_t2158, ___obj_url_3),
	offsetof(WellKnownClientTypeEntry_t2158, ___app_url_4),
	offsetof(WellKnownObjectMode_t2159, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(WellKnownServiceTypeEntry_t2160, ___obj_type_2),
	offsetof(WellKnownServiceTypeEntry_t2160, ___obj_uri_3),
	offsetof(WellKnownServiceTypeEntry_t2160, ___obj_mode_4),
	offsetof(BinaryCommon_t2161_StaticFields, ___BinaryHeader_0),
	offsetof(BinaryCommon_t2161_StaticFields, ____typeCodesToType_1),
	offsetof(BinaryCommon_t2161_StaticFields, ____typeCodeMap_2),
	offsetof(BinaryCommon_t2161_StaticFields, ___UseReflectionSerialization_3),
	offsetof(BinaryElement_t2162, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(TypeTag_t2163, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(MethodFlags_t2164, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(ReturnTypeTag_t2165, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(BinaryFormatter_t2151, ___assembly_format_0),
	offsetof(BinaryFormatter_t2151, ___binder_1),
	offsetof(BinaryFormatter_t2151, ___context_2),
	offsetof(BinaryFormatter_t2151, ___surrogate_selector_3),
	offsetof(BinaryFormatter_t2151, ___type_format_4),
	offsetof(BinaryFormatter_t2151, ___filter_level_5),
	offsetof(BinaryFormatter_t2151_StaticFields, ___U3CDefaultSurrogateSelectorU3Ek__BackingField_6),
	offsetof(ObjectReader_t2171, ____surrogateSelector_0),
	offsetof(ObjectReader_t2171, ____context_1),
	offsetof(ObjectReader_t2171, ____binder_2),
	offsetof(ObjectReader_t2171, ____filterLevel_3),
	offsetof(ObjectReader_t2171, ____manager_4),
	offsetof(ObjectReader_t2171, ____registeredAssemblies_5),
	offsetof(ObjectReader_t2171, ____typeMetadataCache_6),
	offsetof(ObjectReader_t2171, ____lastObject_7),
	offsetof(ObjectReader_t2171, ____lastObjectID_8),
	offsetof(ObjectReader_t2171, ____rootObjectID_9),
	offsetof(ObjectReader_t2171, ___arrayBuffer_10),
	offsetof(ObjectReader_t2171, ___ArrayBufferLength_11),
	offsetof(TypeMetadata_t2168, ___Type_0),
	offsetof(TypeMetadata_t2168, ___MemberTypes_1),
	offsetof(TypeMetadata_t2168, ___MemberNames_2),
	offsetof(TypeMetadata_t2168, ___MemberInfos_3),
	offsetof(TypeMetadata_t2168, ___FieldCount_4),
	offsetof(TypeMetadata_t2168, ___NeedsSerializationInfo_5),
	offsetof(ArrayNullFiller_t2170, ___NullCount_0),
	offsetof(FormatterAssemblyStyle_t2173, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(FormatterTypeStyle_t2174, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(TypeFilterLevel_t2175, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(ObjectManager_t2172, ____objectRecordChain_0),
	offsetof(ObjectManager_t2172, ____lastObjectRecord_1),
	offsetof(ObjectManager_t2172, ____deserializedRecords_2),
	offsetof(ObjectManager_t2172, ____onDeserializedCallbackRecords_3),
	offsetof(ObjectManager_t2172, ____objectRecords_4),
	offsetof(ObjectManager_t2172, ____finalFixup_5),
	offsetof(ObjectManager_t2172, ____selector_6),
	offsetof(ObjectManager_t2172, ____context_7),
	offsetof(ObjectManager_t2172, ____registeredObjectsCount_8),
	offsetof(BaseFixupRecord_t2179, ___ObjectToBeFixed_0),
	offsetof(BaseFixupRecord_t2179, ___ObjectRequired_1),
	offsetof(BaseFixupRecord_t2179, ___NextSameContainer_2),
	offsetof(BaseFixupRecord_t2179, ___NextSameRequired_3),
	offsetof(ArrayFixupRecord_t2180, ____index_4),
	offsetof(MultiArrayFixupRecord_t2181, ____indices_4),
	offsetof(FixupRecord_t2182, ____member_4),
	offsetof(DelayedFixupRecord_t2183, ____memberName_4),
	offsetof(ObjectRecordStatus_t2184, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(ObjectRecord_t2178, ___Status_0),
	offsetof(ObjectRecord_t2178, ___OriginalObject_1),
	offsetof(ObjectRecord_t2178, ___ObjectInstance_2),
	offsetof(ObjectRecord_t2178, ___ObjectID_3),
	offsetof(ObjectRecord_t2178, ___Info_4),
	offsetof(ObjectRecord_t2178, ___IdOfContainingObj_5),
	offsetof(ObjectRecord_t2178, ___Surrogate_6),
	offsetof(ObjectRecord_t2178, ___SurrogateSelector_7),
	offsetof(ObjectRecord_t2178, ___Member_8),
	offsetof(ObjectRecord_t2178, ___ArrayIndex_9),
	offsetof(ObjectRecord_t2178, ___FixupChainAsContainer_10),
	offsetof(ObjectRecord_t2178, ___FixupChainAsRequired_11),
	offsetof(ObjectRecord_t2178, ___Next_12),
	offsetof(SerializationCallbacks_t2191, ___onSerializingList_0),
	offsetof(SerializationCallbacks_t2191, ___onSerializedList_1),
	offsetof(SerializationCallbacks_t2191, ___onDeserializingList_2),
	offsetof(SerializationCallbacks_t2191, ___onDeserializedList_3),
	offsetof(SerializationCallbacks_t2191_StaticFields, ___cache_4),
	offsetof(SerializationCallbacks_t2191_StaticFields, ___cache_lock_5),
	offsetof(SerializationEntry_t2192, ___name_0) + sizeof(Object_t),
	offsetof(SerializationEntry_t2192, ___objectType_1) + sizeof(Object_t),
	offsetof(SerializationEntry_t2192, ___value_2) + sizeof(Object_t),
	offsetof(SerializationInfo_t1012, ___serialized_0),
	offsetof(SerializationInfo_t1012, ___values_1),
	offsetof(SerializationInfo_t1012, ___assemblyName_2),
	offsetof(SerializationInfo_t1012, ___fullTypeName_3),
	offsetof(SerializationInfo_t1012, ___converter_4),
	offsetof(SerializationInfoEnumerator_t2194, ___enumerator_0),
	offsetof(StreamingContext_t1013, ___state_0) + sizeof(Object_t),
	offsetof(StreamingContext_t1013, ___additional_1) + sizeof(Object_t),
	offsetof(StreamingContextStates_t2195, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(X509Certificate_t1531, ___x509_0),
	offsetof(X509Certificate_t1531, ___hideDates_1),
	offsetof(X509Certificate_t1531, ___cachedCertificateHash_2),
	offsetof(X509Certificate_t1531, ___issuer_name_3),
	offsetof(X509Certificate_t1531, ___subject_name_4),
	offsetof(X509KeyStorageFlags_t2196, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(AsymmetricAlgorithm_t1549, ___KeySizeValue_0),
	offsetof(AsymmetricAlgorithm_t1549, ___LegalKeySizesValue_1),
	offsetof(Base64Constants_t2198_StaticFields, ___EncodeTable_0),
	offsetof(Base64Constants_t2198_StaticFields, ___DecodeTable_1),
	offsetof(CipherMode_t1417, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(CryptoConfig_t1563_StaticFields, ___lockObject_0),
	offsetof(CryptoConfig_t1563_StaticFields, ___algorithms_1),
	offsetof(CryptoConfig_t1563_StaticFields, ___oid_2),
	offsetof(CryptoStream_t418, ____stream_1),
	offsetof(CryptoStream_t418, ____transform_2),
	offsetof(CryptoStream_t418, ____mode_3),
	offsetof(CryptoStream_t418, ____currentBlock_4),
	offsetof(CryptoStream_t418, ____disposed_5),
	offsetof(CryptoStream_t418, ____flushedFinalBlock_6),
	offsetof(CryptoStream_t418, ____partialCount_7),
	offsetof(CryptoStream_t418, ____endOfStream_8),
	offsetof(CryptoStream_t418, ____waitingBlock_9),
	offsetof(CryptoStream_t418, ____waitingCount_10),
	offsetof(CryptoStream_t418, ____transformedBlock_11),
	offsetof(CryptoStream_t418, ____transformedPos_12),
	offsetof(CryptoStream_t418, ____transformedCount_13),
	offsetof(CryptoStream_t418, ____workingBlock_14),
	offsetof(CryptoStream_t418, ____workingCount_15),
	offsetof(CryptoStreamMode_t2199, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(CspParameters_t1565, ____Flags_0),
	offsetof(CspParameters_t1565, ___KeyContainerName_1),
	offsetof(CspParameters_t1565, ___KeyNumber_2),
	offsetof(CspParameters_t1565, ___ProviderName_3),
	offsetof(CspParameters_t1565, ___ProviderType_4),
	offsetof(CspProviderFlags_t2200, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(DES_t1579_StaticFields, ___weakKeys_10),
	offsetof(DES_t1579_StaticFields, ___semiWeakKeys_11),
	offsetof(DESTransform_t2202_StaticFields, ___KEY_BIT_SIZE_12),
	offsetof(DESTransform_t2202_StaticFields, ___KEY_BYTE_SIZE_13),
	offsetof(DESTransform_t2202_StaticFields, ___BLOCK_BIT_SIZE_14),
	offsetof(DESTransform_t2202_StaticFields, ___BLOCK_BYTE_SIZE_15),
	offsetof(DESTransform_t2202, ___keySchedule_16),
	offsetof(DESTransform_t2202, ___byteBuff_17),
	offsetof(DESTransform_t2202, ___dwordBuff_18),
	offsetof(DESTransform_t2202_StaticFields, ___spBoxes_19),
	offsetof(DESTransform_t2202_StaticFields, ___PC1_20),
	offsetof(DESTransform_t2202_StaticFields, ___leftRotTotal_21),
	offsetof(DESTransform_t2202_StaticFields, ___PC2_22),
	offsetof(DESTransform_t2202_StaticFields, ___ipTab_23),
	offsetof(DESTransform_t2202_StaticFields, ___fpTab_24),
	offsetof(DSACryptoServiceProvider_t1569, ___store_2),
	offsetof(DSACryptoServiceProvider_t1569, ___persistKey_3),
	offsetof(DSACryptoServiceProvider_t1569, ___persisted_4),
	offsetof(DSACryptoServiceProvider_t1569, ___privateKeyExportable_5),
	offsetof(DSACryptoServiceProvider_t1569, ___m_disposed_6),
	offsetof(DSACryptoServiceProvider_t1569, ___dsa_7),
	offsetof(DSACryptoServiceProvider_t1569_StaticFields, ___useMachineKeyStore_8),
	offsetof(DSAParameters_t1561, ___Counter_0) + sizeof(Object_t),
	offsetof(DSAParameters_t1561, ___G_1) + sizeof(Object_t),
	offsetof(DSAParameters_t1561, ___J_2) + sizeof(Object_t),
	offsetof(DSAParameters_t1561, ___P_3) + sizeof(Object_t),
	offsetof(DSAParameters_t1561, ___Q_4) + sizeof(Object_t),
	offsetof(DSAParameters_t1561, ___Seed_5) + sizeof(Object_t),
	offsetof(DSAParameters_t1561, ___X_6) + sizeof(Object_t),
	offsetof(DSAParameters_t1561, ___Y_7) + sizeof(Object_t),
	offsetof(DSASignatureDeformatter_t1573, ___dsa_0),
	offsetof(DSASignatureFormatter_t2204, ___dsa_0),
	offsetof(HMAC_t1568, ____disposed_5),
	offsetof(HMAC_t1568, ____hashName_6),
	offsetof(HMAC_t1568, ____algo_7),
	offsetof(HMAC_t1568, ____block_8),
	offsetof(HMAC_t1568, ____blockSizeValue_9),
	offsetof(HMACSHA384_t2209_StaticFields, ___legacy_mode_10),
	offsetof(HMACSHA384_t2209, ___legacy_11),
	offsetof(HMACSHA512_t2210_StaticFields, ___legacy_mode_10),
	offsetof(HMACSHA512_t2210, ___legacy_11),
	offsetof(HashAlgorithm_t1445, ___HashValue_0),
	offsetof(HashAlgorithm_t1445, ___HashSizeValue_1),
	offsetof(HashAlgorithm_t1445, ___State_2),
	offsetof(HashAlgorithm_t1445, ___disposed_3),
	offsetof(KeySizes_t1423, ____maxSize_0),
	offsetof(KeySizes_t1423, ____minSize_1),
	offsetof(KeySizes_t1423, ____skipSize_2),
	offsetof(KeyedHashAlgorithm_t1483, ___KeyValue_4),
	offsetof(MACTripleDES_t2211, ___tdes_5),
	offsetof(MACTripleDES_t2211, ___mac_6),
	offsetof(MACTripleDES_t2211, ___m_disposed_7),
	offsetof(MD5CryptoServiceProvider_t2212, ____H_4),
	offsetof(MD5CryptoServiceProvider_t2212, ___buff_5),
	offsetof(MD5CryptoServiceProvider_t2212, ___count_6),
	offsetof(MD5CryptoServiceProvider_t2212, ____ProcessingBuffer_7),
	offsetof(MD5CryptoServiceProvider_t2212, ____ProcessingBufferCount_8),
	offsetof(MD5CryptoServiceProvider_t2212_StaticFields, ___K_9),
	offsetof(PaddingMode_t1421, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(PasswordDeriveBytes_t419, ___HashNameValue_0),
	offsetof(PasswordDeriveBytes_t419, ___SaltValue_1),
	offsetof(PasswordDeriveBytes_t419, ___IterationsValue_2),
	offsetof(PasswordDeriveBytes_t419, ___hash_3),
	offsetof(PasswordDeriveBytes_t419, ___state_4),
	offsetof(PasswordDeriveBytes_t419, ___password_5),
	offsetof(PasswordDeriveBytes_t419, ___initial_6),
	offsetof(PasswordDeriveBytes_t419, ___output_7),
	offsetof(PasswordDeriveBytes_t419, ___position_8),
	offsetof(PasswordDeriveBytes_t419, ___hashnumber_9),
	offsetof(RC2_t1580, ___EffectiveKeySizeValue_10),
	offsetof(RC2Transform_t2214, ___R0_12),
	offsetof(RC2Transform_t2214, ___R1_13),
	offsetof(RC2Transform_t2214, ___R2_14),
	offsetof(RC2Transform_t2214, ___R3_15),
	offsetof(RC2Transform_t2214, ___K_16),
	offsetof(RC2Transform_t2214, ___j_17),
	offsetof(RC2Transform_t2214_StaticFields, ___pitable_18),
	offsetof(RIPEMD160Managed_t2216, ____ProcessingBuffer_4),
	offsetof(RIPEMD160Managed_t2216, ____X_5),
	offsetof(RIPEMD160Managed_t2216, ____HashValue_6),
	offsetof(RIPEMD160Managed_t2216, ____Length_7),
	offsetof(RIPEMD160Managed_t2216, ____ProcessingBufferCount_8),
	offsetof(RNGCryptoServiceProvider_t2217_StaticFields, ____lock_0),
	offsetof(RNGCryptoServiceProvider_t2217, ____handle_1),
	offsetof(RSACryptoServiceProvider_t1566, ___store_2),
	offsetof(RSACryptoServiceProvider_t1566, ___persistKey_3),
	offsetof(RSACryptoServiceProvider_t1566, ___persisted_4),
	offsetof(RSACryptoServiceProvider_t1566, ___privateKeyExportable_5),
	offsetof(RSACryptoServiceProvider_t1566, ___m_disposed_6),
	offsetof(RSACryptoServiceProvider_t1566, ___rsa_7),
	offsetof(RSACryptoServiceProvider_t1566_StaticFields, ___useMachineKeyStore_8),
	offsetof(RSAPKCS1KeyExchangeFormatter_t1593, ___rsa_0),
	offsetof(RSAPKCS1KeyExchangeFormatter_t1593, ___random_1),
	offsetof(RSAPKCS1SignatureDeformatter_t1574, ___rsa_0),
	offsetof(RSAPKCS1SignatureDeformatter_t1574, ___hashName_1),
	offsetof(RSAPKCS1SignatureFormatter_t2218, ___rsa_0),
	offsetof(RSAPKCS1SignatureFormatter_t2218, ___hash_1),
	offsetof(RSAParameters_t1533, ___P_0) + sizeof(Object_t),
	offsetof(RSAParameters_t1533, ___Q_1) + sizeof(Object_t),
	offsetof(RSAParameters_t1533, ___D_2) + sizeof(Object_t),
	offsetof(RSAParameters_t1533, ___DP_3) + sizeof(Object_t),
	offsetof(RSAParameters_t1533, ___DQ_4) + sizeof(Object_t),
	offsetof(RSAParameters_t1533, ___InverseQ_5) + sizeof(Object_t),
	offsetof(RSAParameters_t1533, ___Modulus_6) + sizeof(Object_t),
	offsetof(RSAParameters_t1533, ___Exponent_7) + sizeof(Object_t),
	offsetof(RijndaelTransform_t2220, ___expandedKey_12),
	offsetof(RijndaelTransform_t2220, ___Nb_13),
	offsetof(RijndaelTransform_t2220, ___Nk_14),
	offsetof(RijndaelTransform_t2220, ___Nr_15),
	offsetof(RijndaelTransform_t2220_StaticFields, ___Rcon_16),
	offsetof(RijndaelTransform_t2220_StaticFields, ___SBox_17),
	offsetof(RijndaelTransform_t2220_StaticFields, ___iSBox_18),
	offsetof(RijndaelTransform_t2220_StaticFields, ___T0_19),
	offsetof(RijndaelTransform_t2220_StaticFields, ___T1_20),
	offsetof(RijndaelTransform_t2220_StaticFields, ___T2_21),
	offsetof(RijndaelTransform_t2220_StaticFields, ___T3_22),
	offsetof(RijndaelTransform_t2220_StaticFields, ___iT0_23),
	offsetof(RijndaelTransform_t2220_StaticFields, ___iT1_24),
	offsetof(RijndaelTransform_t2220_StaticFields, ___iT2_25),
	offsetof(RijndaelTransform_t2220_StaticFields, ___iT3_26),
	offsetof(RijndaelManagedTransform_t2221, ____st_0),
	offsetof(RijndaelManagedTransform_t2221, ____bs_1),
	offsetof(SHA1Internal_t2222, ____H_0),
	offsetof(SHA1Internal_t2222, ___count_1),
	offsetof(SHA1Internal_t2222, ____ProcessingBuffer_2),
	offsetof(SHA1Internal_t2222, ____ProcessingBufferCount_3),
	offsetof(SHA1Internal_t2222, ___buff_4),
	offsetof(SHA1CryptoServiceProvider_t2223, ___sha_4),
	offsetof(SHA1Managed_t2224, ___sha_4),
	offsetof(SHA256Managed_t2225, ____H_4),
	offsetof(SHA256Managed_t2225, ___count_5),
	offsetof(SHA256Managed_t2225, ____ProcessingBuffer_6),
	offsetof(SHA256Managed_t2225, ____ProcessingBufferCount_7),
	offsetof(SHA256Managed_t2225, ___buff_8),
	offsetof(SHA384Managed_t2227, ___xBuf_4),
	offsetof(SHA384Managed_t2227, ___xBufOff_5),
	offsetof(SHA384Managed_t2227, ___byteCount1_6),
	offsetof(SHA384Managed_t2227, ___byteCount2_7),
	offsetof(SHA384Managed_t2227, ___H1_8),
	offsetof(SHA384Managed_t2227, ___H2_9),
	offsetof(SHA384Managed_t2227, ___H3_10),
	offsetof(SHA384Managed_t2227, ___H4_11),
	offsetof(SHA384Managed_t2227, ___H5_12),
	offsetof(SHA384Managed_t2227, ___H6_13),
	offsetof(SHA384Managed_t2227, ___H7_14),
	offsetof(SHA384Managed_t2227, ___H8_15),
	offsetof(SHA384Managed_t2227, ___W_16),
	offsetof(SHA384Managed_t2227, ___wOff_17),
	offsetof(SHA512Managed_t2230, ___xBuf_4),
	offsetof(SHA512Managed_t2230, ___xBufOff_5),
	offsetof(SHA512Managed_t2230, ___byteCount1_6),
	offsetof(SHA512Managed_t2230, ___byteCount2_7),
	offsetof(SHA512Managed_t2230, ___H1_8),
	offsetof(SHA512Managed_t2230, ___H2_9),
	offsetof(SHA512Managed_t2230, ___H3_10),
	offsetof(SHA512Managed_t2230, ___H4_11),
	offsetof(SHA512Managed_t2230, ___H5_12),
	offsetof(SHA512Managed_t2230, ___H6_13),
	offsetof(SHA512Managed_t2230, ___H7_14),
	offsetof(SHA512Managed_t2230, ___H8_15),
	offsetof(SHA512Managed_t2230, ___W_16),
	offsetof(SHA512Managed_t2230, ___wOff_17),
	offsetof(SHAConstants_t2231_StaticFields, ___K1_0),
	offsetof(SHAConstants_t2231_StaticFields, ___K2_1),
	offsetof(SignatureDescription_t2232, ____DeformatterAlgorithm_0),
	offsetof(SignatureDescription_t2232, ____DigestAlgorithm_1),
	offsetof(SignatureDescription_t2232, ____FormatterAlgorithm_2),
	offsetof(SignatureDescription_t2232, ____KeyAlgorithm_3),
	offsetof(SymmetricAlgorithm_t1402, ___BlockSizeValue_0),
	offsetof(SymmetricAlgorithm_t1402, ___IVValue_1),
	offsetof(SymmetricAlgorithm_t1402, ___KeySizeValue_2),
	offsetof(SymmetricAlgorithm_t1402, ___KeyValue_3),
	offsetof(SymmetricAlgorithm_t1402, ___LegalBlockSizesValue_4),
	offsetof(SymmetricAlgorithm_t1402, ___LegalKeySizesValue_5),
	offsetof(SymmetricAlgorithm_t1402, ___FeedbackSizeValue_6),
	offsetof(SymmetricAlgorithm_t1402, ___ModeValue_7),
	offsetof(SymmetricAlgorithm_t1402, ___PaddingValue_8),
	offsetof(SymmetricAlgorithm_t1402, ___m_disposed_9),
	offsetof(ToBase64Transform_t2235, ___m_disposed_0),
	offsetof(TripleDESTransform_t2237, ___E1_12),
	offsetof(TripleDESTransform_t2237, ___D2_13),
	offsetof(TripleDESTransform_t2237, ___E3_14),
	offsetof(TripleDESTransform_t2237, ___D1_15),
	offsetof(TripleDESTransform_t2237, ___E2_16),
	offsetof(TripleDESTransform_t2237, ___D3_17),
	offsetof(SecurityPermission_t2238, ___flags_0),
	offsetof(SecurityPermissionFlag_t2240, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(StrongNamePublicKeyBlob_t2241, ___pubkey_0),
	offsetof(ApplicationTrust_t2242, ___fullTrustAssemblies_0),
	offsetof(Evidence_t1987, ___hostEvidenceList_0),
	offsetof(Evidence_t1987, ___assemblyEvidenceList_1),
	offsetof(Evidence_t1987, ____hashCode_2),
	offsetof(EvidenceEnumerator_t2244, ___currentEnum_0),
	offsetof(EvidenceEnumerator_t2244, ___hostEnum_1),
	offsetof(EvidenceEnumerator_t2244, ___assemblyEnum_2),
	offsetof(Hash_t2245, ___assembly_0),
	offsetof(Hash_t2245, ___data_1),
	offsetof(StrongName_t2246, ___publickey_0),
	offsetof(StrongName_t2246, ___name_1),
	offsetof(StrongName_t2246, ___version_2),
	offsetof(PrincipalPolicy_t2247, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(WindowsAccountType_t2248, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(WindowsIdentity_t2249, ____token_0),
	offsetof(WindowsIdentity_t2249, ____type_1),
	offsetof(WindowsIdentity_t2249, ____account_2),
	offsetof(WindowsIdentity_t2249, ____authenticated_3),
	offsetof(WindowsIdentity_t2249, ____name_4),
	offsetof(WindowsIdentity_t2249, ____info_5),
	offsetof(WindowsIdentity_t2249_StaticFields, ___invalidWindows_6),
	offsetof(PermissionSet_t1988, ___U3CDeclarativeSecurityU3Ek__BackingField_0),
	offsetof(SecurityContext_t2250, ____capture_0),
	offsetof(SecurityContext_t2250, ____winid_1),
	offsetof(SecurityContext_t2250, ____stack_2),
	offsetof(SecurityContext_t2250, ____suppressFlowWindowsIdentity_3),
	offsetof(SecurityContext_t2250, ____suppressFlow_4),
	offsetof(SecurityElement_t1863, ___text_0),
	offsetof(SecurityElement_t1863, ___tag_1),
	offsetof(SecurityElement_t1863, ___attributes_2),
	offsetof(SecurityElement_t1863, ___children_3),
	offsetof(SecurityElement_t1863_StaticFields, ___invalid_tag_chars_4),
	offsetof(SecurityElement_t1863_StaticFields, ___invalid_text_chars_5),
	offsetof(SecurityElement_t1863_StaticFields, ___invalid_attr_name_chars_6),
	offsetof(SecurityElement_t1863_StaticFields, ___invalid_attr_value_chars_7),
	offsetof(SecurityElement_t1863_StaticFields, ___invalid_chars_8),
	offsetof(SecurityAttribute_t2253, ____name_0),
	offsetof(SecurityAttribute_t2253, ____value_1),
	offsetof(SecurityException_t2254, ___permissionState_11),
	offsetof(SecurityException_t2254, ___permissionType_12),
	offsetof(SecurityException_t2254, ____granted_13),
	offsetof(SecurityException_t2254, ____refused_14),
	offsetof(SecurityException_t2254, ____demanded_15),
	offsetof(SecurityException_t2254, ____firstperm_16),
	offsetof(SecurityException_t2254, ____method_17),
	offsetof(SecurityException_t2254, ____evidence_18),
	offsetof(RuntimeDeclSecurityEntry_t2256, ___blob_0) + sizeof(Object_t),
	offsetof(RuntimeDeclSecurityEntry_t2256, ___size_1) + sizeof(Object_t),
	offsetof(RuntimeDeclSecurityEntry_t2256, ___index_2) + sizeof(Object_t),
	offsetof(RuntimeSecurityFrame_t2257, ___domain_0),
	offsetof(RuntimeSecurityFrame_t2257, ___method_1),
	offsetof(RuntimeSecurityFrame_t2257, ___assert_2),
	offsetof(RuntimeSecurityFrame_t2257, ___deny_3),
	offsetof(RuntimeSecurityFrame_t2257, ___permitonly_4),
	offsetof(SecurityFrame_t2258, ____domain_0) + sizeof(Object_t),
	offsetof(SecurityFrame_t2258, ____method_1) + sizeof(Object_t),
	offsetof(SecurityFrame_t2258, ____assert_2) + sizeof(Object_t),
	offsetof(SecurityFrame_t2258, ____deny_3) + sizeof(Object_t),
	offsetof(SecurityFrame_t2258, ____permitonly_4) + sizeof(Object_t),
	offsetof(SecurityManager_t2259_StaticFields, ____lockObject_0),
	offsetof(SecurityManager_t2259_StaticFields, ____declsecCache_1),
	offsetof(SecurityManager_t2259_StaticFields, ____execution_2),
	offsetof(Decoder_t1920, ___fallback_0),
	offsetof(Decoder_t1920, ___fallback_buffer_1),
	offsetof(DecoderFallback_t2264_StaticFields, ___exception_fallback_0),
	offsetof(DecoderFallback_t2264_StaticFields, ___replacement_fallback_1),
	offsetof(DecoderFallback_t2264_StaticFields, ___standard_safe_fallback_2),
	offsetof(DecoderFallbackException_t2268, ___bytes_unknown_13),
	offsetof(DecoderFallbackException_t2268, ___index_14),
	offsetof(DecoderReplacementFallback_t2269, ___replacement_3),
	offsetof(DecoderReplacementFallbackBuffer_t2270, ___fallback_assigned_0),
	offsetof(DecoderReplacementFallbackBuffer_t2270, ___current_1),
	offsetof(DecoderReplacementFallbackBuffer_t2270, ___replacement_2),
	offsetof(EncoderFallback_t2272_StaticFields, ___exception_fallback_0),
	offsetof(EncoderFallback_t2272_StaticFields, ___replacement_fallback_1),
	offsetof(EncoderFallback_t2272_StaticFields, ___standard_safe_fallback_2),
	offsetof(EncoderFallbackException_t2275, ___char_unknown_13),
	offsetof(EncoderFallbackException_t2275, ___char_unknown_high_14),
	offsetof(EncoderFallbackException_t2275, ___char_unknown_low_15),
	offsetof(EncoderFallbackException_t2275, ___index_16),
	offsetof(EncoderReplacementFallback_t2276, ___replacement_3),
	offsetof(EncoderReplacementFallbackBuffer_t2277, ___replacement_0),
	offsetof(EncoderReplacementFallbackBuffer_t2277, ___current_1),
	offsetof(EncoderReplacementFallbackBuffer_t2277, ___fallback_assigned_2),
	offsetof(Encoding_t420, ___codePage_0),
	offsetof(Encoding_t420, ___windows_code_page_1),
	offsetof(Encoding_t420, ___is_readonly_2),
	offsetof(Encoding_t420, ___decoder_fallback_3),
	offsetof(Encoding_t420, ___encoder_fallback_4),
	offsetof(Encoding_t420_StaticFields, ___i18nAssembly_5),
	offsetof(Encoding_t420_StaticFields, ___i18nDisabled_6),
	offsetof(Encoding_t420_StaticFields, ___encodings_7),
	offsetof(Encoding_t420, ___body_name_8),
	offsetof(Encoding_t420, ___encoding_name_9),
	offsetof(Encoding_t420, ___header_name_10),
	offsetof(Encoding_t420, ___is_mail_news_display_11),
	offsetof(Encoding_t420, ___is_mail_news_save_12),
	offsetof(Encoding_t420, ___is_browser_save_13),
	offsetof(Encoding_t420, ___is_browser_display_14),
	offsetof(Encoding_t420, ___web_name_15),
	offsetof(Encoding_t420_StaticFields, ___asciiEncoding_16),
	offsetof(Encoding_t420_StaticFields, ___bigEndianEncoding_17),
	offsetof(Encoding_t420_StaticFields, ___defaultEncoding_18),
	offsetof(Encoding_t420_StaticFields, ___utf7Encoding_19),
	offsetof(Encoding_t420_StaticFields, ___utf8EncodingWithMarkers_20),
	offsetof(Encoding_t420_StaticFields, ___utf8EncodingWithoutMarkers_21),
	offsetof(Encoding_t420_StaticFields, ___unicodeEncoding_22),
	offsetof(Encoding_t420_StaticFields, ___isoLatin1Encoding_23),
	offsetof(Encoding_t420_StaticFields, ___utf8EncodingUnsafe_24),
	offsetof(Encoding_t420_StaticFields, ___utf32Encoding_25),
	offsetof(Encoding_t420_StaticFields, ___bigEndianUTF32Encoding_26),
	offsetof(Encoding_t420_StaticFields, ___lockobj_27),
	offsetof(ForwardingDecoder_t2278, ___encoding_2),
	0,
	offsetof(StringBuilder_t70, ____length_1),
	offsetof(StringBuilder_t70, ____str_2),
	offsetof(StringBuilder_t70, ____cached_str_3),
	offsetof(StringBuilder_t70, ____maxCapacity_4),
	offsetof(UTF32Encoding_t2281, ___bigEndian_28),
	offsetof(UTF32Encoding_t2281, ___byteOrderMark_29),
	offsetof(UTF32Decoder_t2280, ___bigEndian_2),
	offsetof(UTF32Decoder_t2280, ___leftOverByte_3),
	offsetof(UTF32Decoder_t2280, ___leftOverLength_4),
	offsetof(UTF7Encoding_t2283, ___allowOptionals_28),
	offsetof(UTF7Encoding_t2283_StaticFields, ___encodingRules_29),
	offsetof(UTF7Encoding_t2283_StaticFields, ___base64Values_30),
	offsetof(UTF7Decoder_t2282, ___leftOver_2),
	offsetof(UTF8Encoding_t2286, ___emitIdentifier_28),
	offsetof(UTF8Decoder_t2285, ___leftOverBits_2),
	offsetof(UTF8Decoder_t2285, ___leftOverCount_3),
	offsetof(UnicodeEncoding_t2288, ___bigEndian_28),
	offsetof(UnicodeEncoding_t2288, ___byteOrderMark_29),
	offsetof(UnicodeDecoder_t2287, ___bigEndian_2),
	offsetof(UnicodeDecoder_t2287, ___leftOverByte_3),
	offsetof(CompressedStack_t2251, ____list_0),
	offsetof(EventResetMode_t2289, ___value___1) + sizeof(Object_t),
	0,
	0,
	offsetof(ExecutionContext_t2097, ____sc_0),
	offsetof(ExecutionContext_t2097, ____suppressFlow_1),
	offsetof(ExecutionContext_t2097, ____capture_2),
	offsetof(Thread_t170, ___lock_thread_id_0),
	offsetof(Thread_t170, ___system_thread_handle_1),
	offsetof(Thread_t170, ___cached_culture_info_2),
	offsetof(Thread_t170, ___unused0_3),
	offsetof(Thread_t170, ___threadpool_thread_4),
	offsetof(Thread_t170, ___name_5),
	offsetof(Thread_t170, ___name_len_6),
	offsetof(Thread_t170, ___state_7),
	offsetof(Thread_t170, ___abort_exc_8),
	offsetof(Thread_t170, ___abort_state_handle_9),
	offsetof(Thread_t170, ___thread_id_10),
	offsetof(Thread_t170, ___start_notify_11),
	offsetof(Thread_t170, ___stack_ptr_12),
	offsetof(Thread_t170, ___static_data_13),
	offsetof(Thread_t170, ___jit_data_14),
	offsetof(Thread_t170, ___lock_data_15),
	offsetof(Thread_t170, ___current_appcontext_16),
	offsetof(Thread_t170, ___stack_size_17),
	offsetof(Thread_t170, ___start_obj_18),
	offsetof(Thread_t170, ___appdomain_refs_19),
	offsetof(Thread_t170, ___interruption_requested_20),
	offsetof(Thread_t170, ___suspend_event_21),
	offsetof(Thread_t170, ___suspended_event_22),
	offsetof(Thread_t170, ___resume_event_23),
	offsetof(Thread_t170, ___synch_cs_24),
	offsetof(Thread_t170, ___serialized_culture_info_25),
	offsetof(Thread_t170, ___serialized_culture_info_len_26),
	offsetof(Thread_t170, ___serialized_ui_culture_info_27),
	offsetof(Thread_t170, ___serialized_ui_culture_info_len_28),
	offsetof(Thread_t170, ___thread_dump_requested_29),
	offsetof(Thread_t170, ___end_stack_30),
	offsetof(Thread_t170, ___thread_interrupt_requested_31),
	offsetof(Thread_t170, ___apartment_state_32),
	offsetof(Thread_t170, ___critical_region_level_33),
	offsetof(Thread_t170, ___small_id_34),
	offsetof(Thread_t170, ___manage_callback_35),
	offsetof(Thread_t170, ___pending_exception_36),
	offsetof(Thread_t170, ___ec_to_set_37),
	offsetof(Thread_t170, ___interrupt_on_stop_38),
	offsetof(Thread_t170, ___unused3_39),
	offsetof(Thread_t170, ___unused4_40),
	offsetof(Thread_t170, ___unused5_41),
	offsetof(Thread_t170, ___unused6_42),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	offsetof(Thread_t170, ___threadstart_45),
	offsetof(Thread_t170, ___managed_id_46),
	offsetof(Thread_t170, ____principal_47),
	offsetof(Thread_t170_StaticFields, ___datastorehash_48),
	offsetof(Thread_t170_StaticFields, ___datastore_lock_49),
	offsetof(Thread_t170, ___in_currentculture_50),
	offsetof(Thread_t170_StaticFields, ___culture_lock_51),
	offsetof(ThreadState_t2299, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(Timer_t2090_StaticFields, ___scheduler_1),
	offsetof(Timer_t2090, ___callback_2),
	offsetof(Timer_t2090, ___state_3),
	offsetof(Timer_t2090, ___due_time_ms_4),
	offsetof(Timer_t2090, ___period_ms_5),
	offsetof(Timer_t2090, ___next_run_6),
	offsetof(Timer_t2090, ___disposed_7),
	offsetof(Scheduler_t2302_StaticFields, ___instance_0),
	offsetof(Scheduler_t2302, ___list_1),
	0,
	offsetof(WaitHandle_t351, ___safe_wait_handle_2),
	offsetof(WaitHandle_t351_StaticFields, ___InvalidHandle_3),
	offsetof(WaitHandle_t351, ___disposed_4),
	offsetof(ActivationContext_t2305, ____disposed_0),
	offsetof(AppDomain_t1015, ____mono_app_domain_1),
	offsetof(AppDomain_t1015_StaticFields, ____process_guid_2),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	offsetof(AppDomain_t1015, ____evidence_6),
	offsetof(AppDomain_t1015, ____granted_7),
	offsetof(AppDomain_t1015, ____principalPolicy_8),
	THREAD_STATIC_FIELD_OFFSET,
	offsetof(AppDomain_t1015_StaticFields, ___default_domain_10),
	offsetof(AppDomain_t1015, ____domain_manager_11),
	offsetof(AppDomain_t1015, ____activation_12),
	offsetof(AppDomain_t1015, ____applicationIdentity_13),
	offsetof(AppDomain_t1015, ___AssemblyLoad_14),
	offsetof(AppDomain_t1015, ___AssemblyResolve_15),
	offsetof(AppDomain_t1015, ___DomainUnload_16),
	offsetof(AppDomain_t1015, ___ProcessExit_17),
	offsetof(AppDomain_t1015, ___ResourceResolve_18),
	offsetof(AppDomain_t1015, ___TypeResolve_19),
	offsetof(AppDomain_t1015, ___UnhandledException_20),
	offsetof(AppDomain_t1015, ___ReflectionOnlyAssemblyResolve_21),
	offsetof(AppDomainSetup_t2312, ___application_base_0),
	offsetof(AppDomainSetup_t2312, ___application_name_1),
	offsetof(AppDomainSetup_t2312, ___cache_path_2),
	offsetof(AppDomainSetup_t2312, ___configuration_file_3),
	offsetof(AppDomainSetup_t2312, ___dynamic_base_4),
	offsetof(AppDomainSetup_t2312, ___license_file_5),
	offsetof(AppDomainSetup_t2312, ___private_bin_path_6),
	offsetof(AppDomainSetup_t2312, ___private_bin_path_probe_7),
	offsetof(AppDomainSetup_t2312, ___shadow_copy_directories_8),
	offsetof(AppDomainSetup_t2312, ___shadow_copy_files_9),
	offsetof(AppDomainSetup_t2312, ___publisher_policy_10),
	offsetof(AppDomainSetup_t2312, ___path_changed_11),
	offsetof(AppDomainSetup_t2312, ___loader_optimization_12),
	offsetof(AppDomainSetup_t2312, ___disallow_binding_redirects_13),
	offsetof(AppDomainSetup_t2312, ___disallow_code_downloads_14),
	offsetof(AppDomainSetup_t2312, ____activationArguments_15),
	offsetof(AppDomainSetup_t2312, ___domain_initializer_16),
	offsetof(AppDomainSetup_t2312, ___application_trust_17),
	offsetof(AppDomainSetup_t2312, ___domain_initializer_args_18),
	offsetof(AppDomainSetup_t2312, ___application_trust_xml_19),
	offsetof(AppDomainSetup_t2312, ___disallow_appbase_probe_20),
	offsetof(AppDomainSetup_t2312, ___configuration_bytes_21),
	offsetof(ApplicationIdentity_t2308, ____fullName_0),
	0,
	offsetof(ArgumentException_t764, ___param_name_12),
	0,
	offsetof(ArgumentOutOfRangeException_t1419, ___actual_value_13),
	0,
	offsetof(AttributeTargets_t2317, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(BitConverter_t1356_StaticFields, ___SwappedWordsInDouble_0),
	offsetof(BitConverter_t1356_StaticFields, ___IsLittleEndian_1),
	offsetof(CharEnumerator_t2319, ___str_0),
	offsetof(CharEnumerator_t2319, ___index_1),
	offsetof(CharEnumerator_t2319, ___length_2),
	offsetof(Console_t1760_StaticFields, ___stdout_0),
	offsetof(Console_t1760_StaticFields, ___stderr_1),
	offsetof(Console_t1760_StaticFields, ___stdin_2),
	offsetof(Console_t1760_StaticFields, ___inputEncoding_3),
	offsetof(Console_t1760_StaticFields, ___outputEncoding_4),
	offsetof(Convert_t373_StaticFields, ___DBNull_0),
	offsetof(Convert_t373_StaticFields, ___conversionTable_1),
	offsetof(DBNull_t2320_StaticFields, ___Value_0),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(DateTime_t74, ___ticks_10) + sizeof(Object_t),
	offsetof(DateTime_t74, ___kind_11) + sizeof(Object_t),
	offsetof(DateTime_t74_StaticFields, ___MaxValue_12),
	offsetof(DateTime_t74_StaticFields, ___MinValue_13),
	offsetof(DateTime_t74_StaticFields, ___ParseTimeFormats_14),
	offsetof(DateTime_t74_StaticFields, ___ParseYearDayMonthFormats_15),
	offsetof(DateTime_t74_StaticFields, ___ParseYearMonthDayFormats_16),
	offsetof(DateTime_t74_StaticFields, ___ParseDayMonthYearFormats_17),
	offsetof(DateTime_t74_StaticFields, ___ParseMonthDayYearFormats_18),
	offsetof(DateTime_t74_StaticFields, ___MonthDayShortFormats_19),
	offsetof(DateTime_t74_StaticFields, ___DayMonthShortFormats_20),
	offsetof(DateTime_t74_StaticFields, ___daysmonth_21),
	offsetof(DateTime_t74_StaticFields, ___daysmonthleap_22),
	offsetof(DateTime_t74_StaticFields, ___to_local_time_span_object_23),
	offsetof(DateTime_t74_StaticFields, ___last_now_24),
	offsetof(Which_t2321, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	offsetof(DateTimeKind_t2322, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	offsetof(DateTimeOffset_t2323_StaticFields, ___MaxValue_0),
	offsetof(DateTimeOffset_t2323_StaticFields, ___MinValue_1),
	offsetof(DateTimeOffset_t2323, ___dt_2) + sizeof(Object_t),
	offsetof(DateTimeOffset_t2323, ___utc_offset_3) + sizeof(Object_t),
	offsetof(DayOfWeek_t2325, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(DelegateData_t1768, ___target_type_0),
	offsetof(DelegateData_t1768, ___method_name_1),
	offsetof(DelegateSerializationHolder_t2327, ____delegate_0),
	offsetof(DelegateEntry_t2326, ___type_0),
	offsetof(DelegateEntry_t2326, ___assembly_1),
	offsetof(DelegateEntry_t2326, ___target_2),
	offsetof(DelegateEntry_t2326, ___targetTypeAssembly_3),
	offsetof(DelegateEntry_t2326, ___targetTypeName_4),
	offsetof(DelegateEntry_t2326, ___methodName_5),
	offsetof(DelegateEntry_t2326, ___delegateEntry_6),
	0,
	0,
	offsetof(MonoEnumInfo_t2336, ___utype_0) + sizeof(Object_t),
	offsetof(MonoEnumInfo_t2336, ___values_1) + sizeof(Object_t),
	offsetof(MonoEnumInfo_t2336, ___names_2) + sizeof(Object_t),
	offsetof(MonoEnumInfo_t2336, ___name_hash_3) + sizeof(Object_t),
	THREAD_STATIC_FIELD_OFFSET,
	offsetof(MonoEnumInfo_t2336_StaticFields, ___global_cache_5),
	offsetof(MonoEnumInfo_t2336_StaticFields, ___global_cache_monitor_6),
	offsetof(MonoEnumInfo_t2336_StaticFields, ___sbyte_comparer_7),
	offsetof(MonoEnumInfo_t2336_StaticFields, ___short_comparer_8),
	offsetof(MonoEnumInfo_t2336_StaticFields, ___int_comparer_9),
	offsetof(MonoEnumInfo_t2336_StaticFields, ___long_comparer_10),
	offsetof(Environment_t2338_StaticFields, ___os_0),
	offsetof(SpecialFolder_t2337, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(EventArgs_t1452_StaticFields, ___Empty_0),
	0,
	offsetof(Guid_t452, ____a_0) + sizeof(Object_t),
	offsetof(Guid_t452, ____b_1) + sizeof(Object_t),
	offsetof(Guid_t452, ____c_2) + sizeof(Object_t),
	offsetof(Guid_t452, ____d_3) + sizeof(Object_t),
	offsetof(Guid_t452, ____e_4) + sizeof(Object_t),
	offsetof(Guid_t452, ____f_5) + sizeof(Object_t),
	offsetof(Guid_t452, ____g_6) + sizeof(Object_t),
	offsetof(Guid_t452, ____h_7) + sizeof(Object_t),
	offsetof(Guid_t452, ____i_8) + sizeof(Object_t),
	offsetof(Guid_t452, ____j_9) + sizeof(Object_t),
	offsetof(Guid_t452, ____k_10) + sizeof(Object_t),
	offsetof(Guid_t452_StaticFields, ___Empty_11),
	offsetof(Guid_t452_StaticFields, ____rngAccess_12),
	offsetof(Guid_t452_StaticFields, ____rng_13),
	0,
	0,
	offsetof(LoaderOptimization_t2346, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(LocalDataStoreSlot_t2347, ___slot_0),
	offsetof(LocalDataStoreSlot_t2347, ___thread_local_1),
	offsetof(LocalDataStoreSlot_t2347_StaticFields, ___lock_obj_2),
	offsetof(LocalDataStoreSlot_t2347_StaticFields, ___slot_bitmap_thread_3),
	offsetof(LocalDataStoreSlot_t2347_StaticFields, ___slot_bitmap_context_4),
	offsetof(MissingMemberException_t2351, ___ClassName_11),
	offsetof(MissingMemberException_t2351, ___MemberName_12),
	offsetof(MissingMemberException_t2351, ___Signature_13),
	0,
	offsetof(MonoAsyncCall_t2353, ___msg_0),
	offsetof(MonoAsyncCall_t2353, ___cb_method_1),
	offsetof(MonoAsyncCall_t2353, ___cb_target_2),
	offsetof(MonoAsyncCall_t2353, ___state_3),
	offsetof(MonoAsyncCall_t2353, ___res_4),
	offsetof(MonoAsyncCall_t2353, ___out_args_5),
	offsetof(MonoAsyncCall_t2353, ___wait_event_6),
	offsetof(MonoCustomAttrs_t2355_StaticFields, ___corlib_0),
	offsetof(MonoCustomAttrs_t2355_StaticFields, ___AttributeUsageType_1),
	offsetof(MonoCustomAttrs_t2355_StaticFields, ___DefaultAttributeUsage_2),
	offsetof(AttributeInfo_t2354, ____usage_0),
	offsetof(AttributeInfo_t2354, ____inheritanceLevel_1),
	offsetof(MonoTouchAOTHelper_t2356_StaticFields, ___FalseFlag_0),
	offsetof(MonoTypeInfo_t2357, ___full_name_0),
	offsetof(MonoTypeInfo_t2357, ___default_ctor_1),
	offsetof(MonoType_t, ___type_info_8),
	0,
	0,
	offsetof(NumberFormatter_t2361_StaticFields, ___MantissaBitsTable_0),
	offsetof(NumberFormatter_t2361_StaticFields, ___TensExponentTable_1),
	offsetof(NumberFormatter_t2361_StaticFields, ___DigitLowerTable_2),
	offsetof(NumberFormatter_t2361_StaticFields, ___DigitUpperTable_3),
	offsetof(NumberFormatter_t2361_StaticFields, ___TenPowersList_4),
	offsetof(NumberFormatter_t2361_StaticFields, ___DecHexDigits_5),
	offsetof(NumberFormatter_t2361, ____thread_6),
	offsetof(NumberFormatter_t2361, ____nfi_7),
	offsetof(NumberFormatter_t2361, ____NaN_8),
	offsetof(NumberFormatter_t2361, ____infinity_9),
	offsetof(NumberFormatter_t2361, ____isCustomFormat_10),
	offsetof(NumberFormatter_t2361, ____specifierIsUpper_11),
	offsetof(NumberFormatter_t2361, ____positive_12),
	offsetof(NumberFormatter_t2361, ____specifier_13),
	offsetof(NumberFormatter_t2361, ____precision_14),
	offsetof(NumberFormatter_t2361, ____defPrecision_15),
	offsetof(NumberFormatter_t2361, ____digitsLen_16),
	offsetof(NumberFormatter_t2361, ____offset_17),
	offsetof(NumberFormatter_t2361, ____decPointPos_18),
	offsetof(NumberFormatter_t2361, ____val1_19),
	offsetof(NumberFormatter_t2361, ____val2_20),
	offsetof(NumberFormatter_t2361, ____val3_21),
	offsetof(NumberFormatter_t2361, ____val4_22),
	offsetof(NumberFormatter_t2361, ____cbuf_23),
	offsetof(NumberFormatter_t2361, ____ind_24),
	THREAD_STATIC_FIELD_OFFSET,
	offsetof(CustomInfo_t2360, ___UseGroup_0),
	offsetof(CustomInfo_t2360, ___DecimalDigits_1),
	offsetof(CustomInfo_t2360, ___DecimalPointPos_2),
	offsetof(CustomInfo_t2360, ___DecimalTailSharpDigits_3),
	offsetof(CustomInfo_t2360, ___IntegerDigits_4),
	offsetof(CustomInfo_t2360, ___IntegerHeadSharpDigits_5),
	offsetof(CustomInfo_t2360, ___IntegerHeadPos_6),
	offsetof(CustomInfo_t2360, ___UseExponent_7),
	offsetof(CustomInfo_t2360, ___ExponentDigits_8),
	offsetof(CustomInfo_t2360, ___ExponentTailSharpDigits_9),
	offsetof(CustomInfo_t2360, ___ExponentNegativeSignOnly_10),
	offsetof(CustomInfo_t2360, ___DividePlaces_11),
	offsetof(CustomInfo_t2360, ___Percents_12),
	offsetof(CustomInfo_t2360, ___Permilles_13),
	offsetof(ObjectDisposedException_t1420, ___obj_name_12),
	offsetof(ObjectDisposedException_t1420, ___msg_13),
	offsetof(OperatingSystem_t2339, ____platform_0),
	offsetof(OperatingSystem_t2339, ____version_1),
	offsetof(OperatingSystem_t2339, ____servicePack_2),
	0,
	0,
	offsetof(PlatformID_t2364, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(ResolveEventArgs_t2366, ___m_Name_1),
	offsetof(RuntimeMethodHandle_t2367, ___value_0) + sizeof(Object_t),
	offsetof(StringComparer_t1024_StaticFields, ___invariantCultureIgnoreCase_0),
	offsetof(StringComparer_t1024_StaticFields, ___invariantCulture_1),
	offsetof(StringComparer_t1024_StaticFields, ___ordinalIgnoreCase_2),
	offsetof(StringComparer_t1024_StaticFields, ___ordinal_3),
	offsetof(CultureAwareComparer_t2368, ____ignoreCase_4),
	offsetof(CultureAwareComparer_t2368, ____compareInfo_5),
	offsetof(OrdinalComparer_t2369, ____ignoreCase_4),
	offsetof(StringComparison_t2370, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(StringSplitOptions_t2371, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(TimeSpan_t427_StaticFields, ___MaxValue_5),
	offsetof(TimeSpan_t427_StaticFields, ___MinValue_6),
	offsetof(TimeSpan_t427_StaticFields, ___Zero_7),
	offsetof(TimeSpan_t427, ____ticks_8) + sizeof(Object_t),
	offsetof(TimeZone_t2373_StaticFields, ___currentTimeZone_0),
	offsetof(CurrentSystemTimeZone_t2374, ___m_standardName_1),
	offsetof(CurrentSystemTimeZone_t2374, ___m_daylightName_2),
	offsetof(CurrentSystemTimeZone_t2374, ___m_CachedDaylightChanges_3),
	offsetof(CurrentSystemTimeZone_t2374, ___m_ticksOffset_4),
	offsetof(CurrentSystemTimeZone_t2374, ___utcOffsetWithOutDLS_5),
	offsetof(CurrentSystemTimeZone_t2374, ___utcOffsetWithDLS_6),
	offsetof(CurrentSystemTimeZone_t2374_StaticFields, ___this_year_7),
	offsetof(CurrentSystemTimeZone_t2374_StaticFields, ___this_year_dlt_8),
	offsetof(TypeCode_t2375, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	offsetof(TypeInitializationException_t2376, ___type_name_11),
	0,
	offsetof(TypeLoadException_t2330, ___className_12),
	offsetof(TypeLoadException_t2330, ___assemblyName_13),
	offsetof(UnhandledExceptionEventArgs_t998, ___exception_1),
	offsetof(UnhandledExceptionEventArgs_t998, ___m_isTerminating_2),
	offsetof(UnitySerializationHolder_t2379, ____data_0),
	offsetof(UnitySerializationHolder_t2379, ____unityType_1),
	offsetof(UnitySerializationHolder_t2379, ____assemblyName_2),
	offsetof(UnityType_t2378, ___value___1) + sizeof(Object_t),
	0,
	0,
	0,
	0,
	0,
	offsetof(Version_t1633, ____Major_1),
	offsetof(Version_t1633, ____Minor_2),
	offsetof(Version_t1633, ____Build_3),
	offsetof(Version_t1633, ____Revision_4),
	offsetof(WeakReference_t2139, ___isLongReference_0),
	offsetof(WeakReference_t2139, ___gcHandle_1),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D0_0),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D1_1),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D2_2),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D3_3),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D4_4),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D5_5),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D6_6),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D15_7),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D16_8),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D17_9),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D18_10),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D19_11),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D20_12),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D21_13),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D22_14),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D23_15),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D24_16),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D25_17),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D26_18),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D27_19),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D30_20),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D31_21),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D32_22),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D33_23),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D34_24),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D35_25),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D36_26),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D37_27),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D38_28),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D39_29),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D40_30),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D41_31),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D42_32),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D43_33),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D44_34),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D45_35),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D46_36),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D47_37),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D48_38),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D49_39),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D50_40),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D51_41),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D52_42),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D53_43),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D54_44),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D55_45),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D56_46),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D57_47),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D60_48),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D62_49),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D63_50),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D64_51),
	offsetof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields, ___U24U24fieldU2D65_52),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
// CommonUtils
#include "AssemblyU2DCSharp_CommonUtils.h"
// Comparison
#include "AssemblyU2DCSharp_Comparison.h"
// DontDestroyOnLoadComponent
#include "AssemblyU2DCSharp_DontDestroyOnLoadComponent.h"
// Common.Extensions.StringEnumExtension
#include "AssemblyU2DCSharp_Common_Extensions_StringEnumExtension.h"
// Common.Extensions.IntEnumExtension
#include "AssemblyU2DCSharp_Common_Extensions_IntEnumExtension.h"
// Common.Extensions.EnumExtension
#include "AssemblyU2DCSharp_Common_Extensions_EnumExtension.h"
// Common.Fsm.FsmDelegateAction/FsmActionRoutine
#include "AssemblyU2DCSharp_Common_Fsm_FsmDelegateAction_FsmActionRout.h"
// Common.Fsm.Fsm/StateActionProcessor
#include "AssemblyU2DCSharp_Common_Fsm_Fsm_StateActionProcessor.h"
// SelectionListGui
#include "AssemblyU2DCSharp_SelectionListGui.h"
// SelectionListGui/DoubleClickCallback
#include "AssemblyU2DCSharp_SelectionListGui_DoubleClickCallback.h"
// InterpolationUtils
#include "AssemblyU2DCSharp_InterpolationUtils.h"
// MiniJSON.Json
#include "AssemblyU2DCSharp_MiniJSON_Json.h"
// NotificationHandlerComponent
#include "AssemblyU2DCSharp_NotificationHandlerComponent.h"
// Common.Notification.NotificationSystem/NotificationInitializer
#include "AssemblyU2DCSharp_Common_Notification_NotificationSystem_Not.h"
// QuaternionUtils
#include "AssemblyU2DCSharp_QuaternionUtils.h"
// Common.Signal.Signal/SignalListener
#include "AssemblyU2DCSharp_Common_Signal_Signal_SignalListener.h"
// RotationInRadians
#include "AssemblyU2DCSharp_RotationInRadians.h"
// Common.BoundsExtensions
#include "AssemblyU2DCSharp_Common_BoundsExtensions.h"
// Common.GameObjectExtensions
#include "AssemblyU2DCSharp_Common_GameObjectExtensions.h"
// Common.TransformExtensions
#include "AssemblyU2DCSharp_Common_TransformExtensions.h"
// Common.UnityUtils
#include "AssemblyU2DCSharp_Common_UnityUtils.h"
// Common.Utils.Platform
#include "AssemblyU2DCSharp_Common_Utils_Platform.h"
// Common.Xml.SimpleXmlNode/NodeProcessor
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNode_NodeProcessor.h"
// Common.Xml.SimpleXmlNodeList
#include "AssemblyU2DCSharp_Common_Xml_SimpleXmlNodeList.h"
// NativeCallsIOS
#include "AssemblyU2DCSharp_NativeCallsIOS.h"
// ScannerRoot
#include "AssemblyU2DCSharp_ScannerRoot.h"
// Alive
#include "AssemblyU2DCSharp_Alive.h"
// MoveSample
#include "AssemblyU2DCSharp_MoveSample.h"
// RotateSample
#include "AssemblyU2DCSharp_RotateSample.h"
// SampleInfo
#include "AssemblyU2DCSharp_SampleInfo.h"
// iTween/EasingFunction
#include "AssemblyU2DCSharp_iTween_EasingFunction.h"
// iTween/ApplyTween
#include "AssemblyU2DCSharp_iTween_ApplyTween.h"
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
// Vuforia.DatabaseLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviour.h"
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviour.h"
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactory.h"
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour.h"
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour.h"
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour.h"
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour.h"
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"
// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
// Common.Query.QueryResultResolver
#include "AssemblyU2DCSharp_Common_Query_QueryResultResolver.h"
// ListenRaffle
#include "AssemblyU2DCSharp_ListenRaffle.h"
// ListenRaffleResult
#include "AssemblyU2DCSharp_ListenRaffleResult.h"
// RequestHandler
#include "AssemblyU2DCSharp_RequestHandler.h"
// Handle
#include "AssemblyU2DCSharp_Handle.h"
// <PrivateImplementationDetails>/$ArrayType$24
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24Arra.h"
// <Module>
#include "UnityEngine_UI_U3CModuleU3E.h"
// UnityEngine.EventSystems.EventTrigger/TriggerEvent
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger_Trigger.h"
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"
// UnityEngine.EventSystems.BaseRaycaster
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycaster.h"
// UnityEngine.EventSystems.Physics2DRaycaster
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DRaycaster.h"
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo_0.h"
// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatTween_Floa.h"
// UnityEngine.UI.Button/ButtonClickedEvent
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClickedEvent.h"
// UnityEngine.UI.Dropdown/DropdownEvent
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEvent.h"
// UnityEngine.UI.InputField/SubmitEvent
#include "UnityEngine_UI_UnityEngine_UI_InputField_SubmitEvent.h"
// UnityEngine.UI.InputField/OnChangeEvent
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnChangeEvent.h"
// UnityEngine.UI.InputField/OnValidateInput
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnValidateInput.h"
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic_CullStateChang.h"
// UnityEngine.UI.MaskUtilities
#include "UnityEngine_UI_UnityEngine_UI_MaskUtilities.h"
// UnityEngine.UI.Misc
#include "UnityEngine_UI_UnityEngine_UI_Misc.h"
// UnityEngine.UI.Scrollbar/ScrollEvent
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_ScrollEvent.h"
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEvent.h"
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility.h"
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent.h"
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent.h"
// UnityEngine.UI.Clipping
#include "UnityEngine_UI_UnityEngine_UI_Clipping.h"
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroup.h"
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup.h"
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_Outline.h"
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1.h"
// <Module>
#include "UnityEngine_U3CModuleU3E.h"
// UnityEngine.AssetBundleCreateRequest
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest.h"
// UnityEngine.AssetBundle
#include "UnityEngine_UnityEngine_AssetBundle.h"
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfo.h"
// UnityEngine.WaitForFixedUpdate
#include "UnityEngine_UnityEngine_WaitForFixedUpdate.h"
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObject.h"
// UnityEngine.UnhandledExceptionHandler
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityEngine.SkinnedMeshRenderer
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_Screen.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GL.h"
// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRenderer.h"
// UnityEngine.GUIElement
#include "UnityEngine_UnityEngine_GUIElement.h"
// UnityEngine.GUITexture
#include "UnityEngine_UnityEngine_GUITexture.h"
// UnityEngine.GUILayer
#include "UnityEngine_UnityEngine_GUILayer.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTexture.h"
// UnityEngine.ReflectionProbe
#include "UnityEngine_UnityEngine_ReflectionProbe.h"
// UnityEngine.CullingGroup/StateChanged
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged.h"
// UnityEngine.Handheld
#include "UnityEngine_UnityEngine_Handheld.h"
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_Gizmos.h"
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker.h"
// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertie.h"
// UnityEngine.Resources
#include "UnityEngine_UnityEngine_Resources.h"
// UnityEngine.TextAsset
#include "UnityEngine_UnityEngine_TextAsset.h"
// UnityEngine.SerializePrivateVariables
#include "UnityEngine_UnityEngine_SerializePrivateVariables.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_Sprite.h"
// UnityEngine.SpriteRenderer
#include "UnityEngine_UnityEngine_SpriteRenderer.h"
// UnityEngine.Sprites.DataUtility
#include "UnityEngine_UnityEngine_Sprites_DataUtility.h"
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWW.h"
// UnityEngine.Caching
#include "UnityEngine_UnityEngine_Caching.h"
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityString.h"
// UnityEngine.Application/LogCallback
#include "UnityEngine_UnityEngine_Application_LogCallback.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// UnityEngine.Camera/CameraCallback
#include "UnityEngine_UnityEngine_Camera_CameraCallback.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_Debug.h"
// UnityEngine.Display/DisplaysUpdatedDelegate
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegate.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_Input.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
// UnityEngine.Light
#include "UnityEngine_UnityEngine_Light.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_Time.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_Random.h"
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstruction.h"
// UnityEngine.PlayerPrefsException
#include "UnityEngine_UnityEngine_PlayerPrefsException.h"
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefs.h"
// UnityEngine.Experimental.Director.DirectorPlayer
#include "UnityEngine_UnityEngine_Experimental_Director_DirectorPlayer.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_Physics.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// UnityEngine.BoxCollider
#include "UnityEngine_UnityEngine_BoxCollider.h"
// UnityEngine.MeshCollider
#include "UnityEngine_UnityEngine_MeshCollider.h"
// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
// UnityEngine.Collider2D
#include "UnityEngine_UnityEngine_Collider2D.h"
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChan.h"
// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback.h"
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallback.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
// UnityEngine.WebCamTexture
#include "UnityEngine_UnityEngine_WebCamTexture.h"
// UnityEngine.AnimationState
#include "UnityEngine_UnityEngine_AnimationState.h"
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
// UnityEngine.RuntimeAnimatorController
#include "UnityEngine_UnityEngine_RuntimeAnimatorController.h"
// UnityEngine.GUIText
#include "UnityEngine_UnityEngine_GUIText.h"
// UnityEngine.TextMesh
#include "UnityEngine_UnityEngine_TextMesh.h"
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallback.h"
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases.h"
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroup.h"
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayout.h"
// UnityEngine.GUISkin/SkinChangedDelegate
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegate.h"
// UnityEngine.ExitGUIException
#include "UnityEngine_UnityEngine_ExitGUIException.h"
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspector.h"
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutine.h"
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly.h"
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
// UnityEngineInternal.NetFxCoreExtensions
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtensions.h"
// UnityEngine.Advertisements.UnityAdsDelegate
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate.h"
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
// <Module>
#include "Qualcomm_Vuforia_UnityExtensions_U3CModuleU3E.h"
// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetter.h"
// Vuforia.EyewearCalibrationProfileManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearCalibrationP.h"
// Vuforia.EyewearCalibrationProfileManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearCalibrationP_0.h"
// Vuforia.EyewearUserCalibrator
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearUserCalibrat.h"
// Vuforia.EyewearUserCalibratorImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearUserCalibrat_0.h"
// Vuforia.NullUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayer.h"
// Vuforia.PlayModeUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer.h"
// Vuforia.SmartTerrainBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder.h"
// Vuforia.EyewearImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearImpl.h"
// Vuforia.SmartTerrainInitializationInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitial.h"
// Vuforia.DataSet
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet.h"
// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentF_0.h"
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder.h"
// Vuforia.Image
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image.h"
// Vuforia.ObjectTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTracker.h"
// Vuforia.MarkerTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTracker.h"
// Vuforia.MultiTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetImpl.h"
// Vuforia.WebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor.h"
// Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtili.h"
// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor.h"
// Vuforia.SmartTerrainTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_0.h"
// Vuforia.TextTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTracker.h"
// Vuforia.WordManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManager.h"
// Vuforia.WordResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordResult.h"
// Vuforia.WordList
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordList.h"
// Vuforia.WordListImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordListImpl.h"
// Vuforia.VuforiaNativeIosWrapper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaNativeIosWra.h"
// Vuforia.VuforiaNullWrapper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaNullWrapper.h"
// Vuforia.VuforiaNativeWrapper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaNativeWrappe.h"
// Vuforia.StateManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManager.h"
// Vuforia.TargetFinder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder.h"
// Vuforia.TrackableSource
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableSource.h"
// Vuforia.VuforiaUnity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity.h"
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
// <PrivateImplementationDetails>{C4A9B53A-936F-421A-9398-89108BD15C01}/__StaticArrayInitTypeSize=24
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDet.h"
// <Module>
#include "System_Core_U3CModuleU3E.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// Locale
#include "System_Core_Locale.h"
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"
// System.Linq.Check
#include "System_Core_System_Linq_Check.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
// System.Security.Cryptography.Aes
#include "System_Core_System_Security_Cryptography_Aes.h"
// System.Security.Cryptography.AesManaged
#include "System_Core_System_Security_Cryptography_AesManaged.h"
// System.Action
#include "System_Core_System_Action.h"
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU.h"
// <PrivateImplementationDetails>/$ArrayType$120
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU_0.h"
// <PrivateImplementationDetails>/$ArrayType$256
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU_1.h"
// <PrivateImplementationDetails>/$ArrayType$1024
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU_2.h"
// <Module>
#include "Mono_Security_U3CModuleU3E.h"
// Locale
#include "Mono_Security_Locale.h"
// Mono.Math.BigInteger/Kernel
#include "Mono_Security_Mono_Math_BigInteger_Kernel.h"
// Mono.Math.Prime.PrimalityTests
#include "Mono_Security_Mono_Math_Prime_PrimalityTests.h"
// Mono.Math.Prime.Generator.PrimeGeneratorBase
#include "Mono_Security_Mono_Math_Prime_Generator_PrimeGeneratorBase.h"
// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
#include "Mono_Security_Mono_Math_Prime_Generator_SequentialSearchPrim.h"
// Mono.Security.ASN1Convert
#include "Mono_Security_Mono_Security_ASN1Convert.h"
// Mono.Security.BitConverterLE
#include "Mono_Security_Mono_Security_BitConverterLE.h"
// Mono.Security.PKCS7
#include "Mono_Security_Mono_Security_PKCS7.h"
// Mono.Security.Cryptography.CryptoConvert
#include "Mono_Security_Mono_Security_Cryptography_CryptoConvert.h"
// Mono.Security.Cryptography.MD2
#include "Mono_Security_Mono_Security_Cryptography_MD2.h"
// Mono.Security.Cryptography.PKCS8
#include "Mono_Security_Mono_Security_Cryptography_PKCS8.h"
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
#include "Mono_Security_Mono_Security_Cryptography_RSAManaged_KeyGener.h"
// Mono.Security.X509.X509CertificateCollection
#include "Mono_Security_Mono_Security_X509_X509CertificateCollection.h"
// Mono.Security.Protocol.Tls.CipherSuiteFactory
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuiteFactory.h"
// Mono.Security.Protocol.Tls.ClientRecordProtocol
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientRecordProtoco.h"
// Mono.Security.Protocol.Tls.ServerContext
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerContext.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_0.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_3.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8.h"
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTest.h"
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati.h"
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0.h"
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectio.h"
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelection.h"
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp.h"
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0.h"
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1.h"
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2.h"
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3.h"
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4.h"
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5.h"
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6.h"
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7.h"
// <Module>
#include "System_U3CModuleU3E.h"
// Locale
#include "System_Locale.h"
// System.ComponentModel.TypeConverter
#include "System_System_ComponentModel_TypeConverter.h"
// System.Net.DefaultCertificatePolicy
#include "System_System_Net_DefaultCertificatePolicy.h"
// System.Net.FileWebRequestCreator
#include "System_System_Net_FileWebRequestCreator.h"
// System.Net.FtpRequestCreator
#include "System_System_Net_FtpRequestCreator.h"
// System.Net.GlobalProxySelection
#include "System_System_Net_GlobalProxySelection.h"
// System.Net.HttpRequestCreator
#include "System_System_Net_HttpRequestCreator.h"
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_2.h"
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Cer.h"
// System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
#include "System_System_Text_RegularExpressions_BaseMachine_MatchAppen.h"
// System.Text.RegularExpressions.CategoryUtils
#include "System_System_Text_RegularExpressions_CategoryUtils.h"
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRef.h"
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
#include "System_System_Text_RegularExpressions_IntervalCollection_Cos.h"
// System.Text.RegularExpressions.Syntax.ExpressionCollection
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColle.h"
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_Expression.h"
// System.Text.RegularExpressions.Syntax.Group
#include "System_System_Text_RegularExpressions_Syntax_Group.h"
// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
#include "System_System_Text_RegularExpressions_Syntax_NonBacktracking.h"
// System.Text.RegularExpressions.Syntax.Assertion
#include "System_System_Text_RegularExpressions_Syntax_Assertion.h"
// System.Text.RegularExpressions.Syntax.Alternation
#include "System_System_Text_RegularExpressions_Syntax_Alternation.h"
// System.DefaultUriParser
#include "System_System_DefaultUriParser.h"
// System.GenericUriParser
#include "System_System_GenericUriParser.h"
// System.UriFormatException
#include "System_System_UriFormatException.h"
// System.UriTypeConverter
#include "System_System_UriTypeConverter.h"
// System.Net.Security.RemoteCertificateValidationCallback
#include "System_System_Net_Security_RemoteCertificateValidationCallba.h"
// System.Text.RegularExpressions.MatchEvaluator
#include "System_System_Text_RegularExpressions_MatchEvaluator.h"
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128.h"
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412.h"
// <Module>
#include "mscorlib_U3CModuleU3E.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.SerializableAttribute
#include "mscorlib_System_SerializableAttribute.h"
// System.Array
#include "mscorlib_System_Array.h"
// System.Array/Swapper
#include "mscorlib_System_Array_Swapper.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.Runtime.InteropServices.OutAttribute
#include "mscorlib_System_Runtime_InteropServices_OutAttribute.h"
// System.Runtime.InteropServices.InAttribute
#include "mscorlib_System_Runtime_InteropServices_InAttribute.h"
// System.Runtime.InteropServices.ComImportAttribute
#include "mscorlib_System_Runtime_InteropServices_ComImportAttribute.h"
// System.Runtime.InteropServices.OptionalAttribute
#include "mscorlib_System_Runtime_InteropServices_OptionalAttribute.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
// System.Runtime.CompilerServices.RuntimeHelpers
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpers.h"
// Locale
#include "mscorlib_Locale.h"
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttribute.h"
// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOn.h"
// Microsoft.Win32.SafeHandles.SafeWaitHandle
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeWaitHandle.h"
// Mono.Math.Prime.Generator.PrimeGeneratorBase
#include "mscorlib_Mono_Math_Prime_Generator_PrimeGeneratorBase.h"
// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
#include "mscorlib_Mono_Math_Prime_Generator_SequentialSearchPrimeGene.h"
// Mono.Math.Prime.PrimalityTests
#include "mscorlib_Mono_Math_Prime_PrimalityTests.h"
// Mono.Math.BigInteger/Kernel
#include "mscorlib_Mono_Math_BigInteger_Kernel.h"
// Mono.Security.Cryptography.CryptoConvert
#include "mscorlib_Mono_Security_Cryptography_CryptoConvert.h"
// Mono.Security.Cryptography.DSAManaged/KeyGeneratedEventHandler
#include "mscorlib_Mono_Security_Cryptography_DSAManaged_KeyGeneratedE.h"
// Mono.Security.Cryptography.PKCS8
#include "mscorlib_Mono_Security_Cryptography_PKCS8.h"
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
#include "mscorlib_Mono_Security_Cryptography_RSAManaged_KeyGeneratedE.h"
// Mono.Security.X509.X509CertificateCollection
#include "mscorlib_Mono_Security_X509_X509CertificateCollection.h"
// Mono.Security.ASN1Convert
#include "mscorlib_Mono_Security_ASN1Convert.h"
// Mono.Security.BitConverterLE
#include "mscorlib_Mono_Security_BitConverterLE.h"
// Mono.Security.PKCS7
#include "mscorlib_Mono_Security_PKCS7.h"
// Mono.Runtime
#include "mscorlib_Mono_Runtime.h"
// System.Collections.Generic.KeyNotFoundException
#include "mscorlib_System_Collections_Generic_KeyNotFoundException.h"
// System.Collections.ArrayList/FixedSizeArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_FixedSizeArrayListWrap.h"
// System.Collections.ArrayList/ReadOnlyArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_ReadOnlyArrayListWrapp.h"
// System.Collections.CollectionDebuggerView
#include "mscorlib_System_Collections_CollectionDebuggerView.h"
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttribute.h"
// System.Globalization.CCMath
#include "mscorlib_System_Globalization_CCMath.h"
// System.Globalization.CCFixed
#include "mscorlib_System_Globalization_CCFixed.h"
// System.Globalization.CCGregorianCalendar
#include "mscorlib_System_Globalization_CCGregorianCalendar.h"
// System.IO.IsolatedStorage.IsolatedStorageException
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageException.h"
// System.IO.Directory
#include "mscorlib_System_IO_Directory.h"
// System.IO.DirectoryNotFoundException
#include "mscorlib_System_IO_DirectoryNotFoundException.h"
// System.IO.EndOfStreamException
#include "mscorlib_System_IO_EndOfStreamException.h"
// System.IO.File
#include "mscorlib_System_IO_File.h"
// System.IO.FileStream/ReadDelegate
#include "mscorlib_System_IO_FileStream_ReadDelegate.h"
// System.IO.FileStream/WriteDelegate
#include "mscorlib_System_IO_FileStream_WriteDelegate.h"
// System.IO.IOException
#include "mscorlib_System_IO_IOException.h"
// System.IO.PathTooLongException
#include "mscorlib_System_IO_PathTooLongException.h"
// System.IO.NullStream
#include "mscorlib_System_IO_NullStream.h"
// System.IO.StreamReader/NullStreamReader
#include "mscorlib_System_IO_StreamReader_NullStreamReader.h"
// System.IO.TextReader/NullTextReader
#include "mscorlib_System_IO_TextReader_NullTextReader.h"
// System.IO.TextWriter/NullTextWriter
#include "mscorlib_System_IO_TextWriter_NullTextWriter.h"
// System.IO.UnexceptionalStreamWriter
#include "mscorlib_System_IO_UnexceptionalStreamWriter.h"
// System.Reflection.AmbiguousMatchException
#include "mscorlib_System_Reflection_AmbiguousMatchException.h"
// System.Reflection.Assembly/ResolveEventHolder
#include "mscorlib_System_Reflection_Assembly_ResolveEventHolder.h"
// System.Reflection.Binder/Default
#include "mscorlib_System_Reflection_Binder_Default.h"
// System.Reflection.EventInfo/AddEventAdapter
#include "mscorlib_System_Reflection_EventInfo_AddEventAdapter.h"
// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfo.h"
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// System.Reflection.MonoGenericMethod
#include "mscorlib_System_Reflection_MonoGenericMethod.h"
// System.Reflection.MonoGenericCMethod
#include "mscorlib_System_Reflection_MonoGenericCMethod.h"
// System.Reflection.MonoProperty/GetterAdapter
#include "mscorlib_System_Reflection_MonoProperty_GetterAdapter.h"
// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfo.h"
// System.Reflection.TargetException
#include "mscorlib_System_Reflection_TargetException.h"
// System.Reflection.TargetInvocationException
#include "mscorlib_System_Reflection_TargetInvocationException.h"
// System.Reflection.TargetParameterCountException
#include "mscorlib_System_Reflection_TargetParameterCountException.h"
// System.Runtime.CompilerServices.IsVolatile
#include "mscorlib_System_Runtime_CompilerServices_IsVolatile.h"
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttri.h"
// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinaliz.h"
// System.Runtime.Hosting.ActivationArguments
#include "mscorlib_System_Runtime_Hosting_ActivationArguments.h"
// System.Runtime.InteropServices.PreserveSigAttribute
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttribute.h"
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeve.h"
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivator.h"
// System.Runtime.Remoting.Contexts.ContextCallbackObject
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextCallbackObj.h"
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanne.h"
// System.Runtime.Remoting.Lifetime.Lease/RenewalDelegate
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lease_RenewalDeleg.h"
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemoti.h"
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate.h"
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogate.h"
// System.Runtime.Remoting.Messaging.ServerContextTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerContextTerm.h"
// System.Runtime.Remoting.Metadata.SoapParameterAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapParameterAttri.h"
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttribute.h"
// System.Runtime.Remoting.FormatterData
#include "mscorlib_System_Runtime_Remoting_FormatterData.h"
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingException.h"
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentity.h"
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentity.h"
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentity.h"
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Mess.h"
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverter.h"
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServices.h"
// System.Runtime.Serialization.OnDeserializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializedAttribut.h"
// System.Runtime.Serialization.OnDeserializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializingAttribu.h"
// System.Runtime.Serialization.OnSerializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializedAttribute.h"
// System.Runtime.Serialization.OnSerializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializingAttribute.h"
// System.Runtime.Serialization.SerializationBinder
#include "mscorlib_System_Runtime_Serialization_SerializationBinder.h"
// System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacks.h"
// System.Runtime.Serialization.SerializationException
#include "mscorlib_System_Runtime_Serialization_SerializationException.h"
// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeF.h"
// System.Security.Cryptography.AsymmetricSignatureDeformatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDef.h"
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
// System.Security.Cryptography.CryptographicException
#include "mscorlib_System_Security_Cryptography_CryptographicException.h"
// System.Security.Cryptography.CryptographicUnexpectedOperationException
#include "mscorlib_System_Security_Cryptography_CryptographicUnexpecte.h"
// System.Security.Cryptography.DESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DESCryptoServiceProvid.h"
// System.Security.Cryptography.DSA
#include "mscorlib_System_Security_Cryptography_DSA.h"
// System.Security.Cryptography.DeriveBytes
#include "mscorlib_System_Security_Cryptography_DeriveBytes.h"
// System.Security.Cryptography.HMACMD5
#include "mscorlib_System_Security_Cryptography_HMACMD5.h"
// System.Security.Cryptography.HMACRIPEMD160
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD160.h"
// System.Security.Cryptography.HMACSHA1
#include "mscorlib_System_Security_Cryptography_HMACSHA1.h"
// System.Security.Cryptography.HMACSHA256
#include "mscorlib_System_Security_Cryptography_HMACSHA256.h"
// System.Security.Cryptography.MD5
#include "mscorlib_System_Security_Cryptography_MD5.h"
// System.Security.Cryptography.RC2CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RC2CryptoServiceProvid.h"
// System.Security.Cryptography.RIPEMD160
#include "mscorlib_System_Security_Cryptography_RIPEMD160.h"
// System.Security.Cryptography.RSA
#include "mscorlib_System_Security_Cryptography_RSA.h"
// System.Security.Cryptography.RandomNumberGenerator
#include "mscorlib_System_Security_Cryptography_RandomNumberGenerator.h"
// System.Security.Cryptography.Rijndael
#include "mscorlib_System_Security_Cryptography_Rijndael.h"
// System.Security.Cryptography.RijndaelManaged
#include "mscorlib_System_Security_Cryptography_RijndaelManaged.h"
// System.Security.Cryptography.SHA1
#include "mscorlib_System_Security_Cryptography_SHA1.h"
// System.Security.Cryptography.SHA256
#include "mscorlib_System_Security_Cryptography_SHA256.h"
// System.Security.Cryptography.SHA384
#include "mscorlib_System_Security_Cryptography_SHA384.h"
// System.Security.Cryptography.SHA512
#include "mscorlib_System_Security_Cryptography_SHA512.h"
// System.Security.Cryptography.DSASignatureDescription
#include "mscorlib_System_Security_Cryptography_DSASignatureDescriptio.h"
// System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA1SignatureD.h"
// System.Security.Cryptography.TripleDES
#include "mscorlib_System_Security_Cryptography_TripleDES.h"
// System.Security.Cryptography.TripleDESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_TripleDESCryptoService.h"
// System.Security.CodeAccessPermission
#include "mscorlib_System_Security_CodeAccessPermission.h"
// System.Security.SecurityCriticalAttribute
#include "mscorlib_System_Security_SecurityCriticalAttribute.h"
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttribute.h"
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttrib.h"
// System.Security.UnverifiableCodeAttribute
#include "mscorlib_System_Security_UnverifiableCodeAttribute.h"
// System.Text.ASCIIEncoding
#include "mscorlib_System_Text_ASCIIEncoding.h"
// System.Text.DecoderExceptionFallback
#include "mscorlib_System_Text_DecoderExceptionFallback.h"
// System.Text.DecoderExceptionFallbackBuffer
#include "mscorlib_System_Text_DecoderExceptionFallbackBuffer.h"
// System.Text.DecoderFallbackBuffer
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
// System.Text.EncoderExceptionFallback
#include "mscorlib_System_Text_EncoderExceptionFallback.h"
// System.Text.EncoderExceptionFallbackBuffer
#include "mscorlib_System_Text_EncoderExceptionFallbackBuffer.h"
// System.Text.EncoderFallbackBuffer
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
// System.Text.Latin1Encoding
#include "mscorlib_System_Text_Latin1Encoding.h"
// System.Threading.EventWaitHandle
#include "mscorlib_System_Threading_EventWaitHandle.h"
// System.Threading.Interlocked
#include "mscorlib_System_Threading_Interlocked.h"
// System.Threading.ManualResetEvent
#include "mscorlib_System_Threading_ManualResetEvent.h"
// System.Threading.Monitor
#include "mscorlib_System_Threading_Monitor.h"
// System.Threading.Mutex
#include "mscorlib_System_Threading_Mutex.h"
// System.Threading.NativeEventCalls
#include "mscorlib_System_Threading_NativeEventCalls.h"
// System.Threading.SynchronizationLockException
#include "mscorlib_System_Threading_SynchronizationLockException.h"
// System.Threading.ThreadAbortException
#include "mscorlib_System_Threading_ThreadAbortException.h"
// System.Threading.ThreadInterruptedException
#include "mscorlib_System_Threading_ThreadInterruptedException.h"
// System.Threading.ThreadPool
#include "mscorlib_System_Threading_ThreadPool.h"
// System.Threading.ThreadStateException
#include "mscorlib_System_Threading_ThreadStateException.h"
// System.Threading.Timer/TimerComparer
#include "mscorlib_System_Threading_Timer_TimerComparer.h"
// System.AccessViolationException
#include "mscorlib_System_AccessViolationException.h"
// System.Activator
#include "mscorlib_System_Activator.h"
// System.AppDomainManager
#include "mscorlib_System_AppDomainManager.h"
// System.ApplicationException
#include "mscorlib_System_ApplicationException.h"
// System.ArithmeticException
#include "mscorlib_System_ArithmeticException.h"
// System.AssemblyLoadEventArgs
#include "mscorlib_System_AssemblyLoadEventArgs.h"
// System.Buffer
#include "mscorlib_System_Buffer.h"
// System.ContextBoundObject
#include "mscorlib_System_ContextBoundObject.h"
// System.DateTimeUtils
#include "mscorlib_System_DateTimeUtils.h"
// System.DivideByZeroException
#include "mscorlib_System_DivideByZeroException.h"
// System.MonoEnumInfo/SByteComparer
#include "mscorlib_System_MonoEnumInfo_SByteComparer.h"
// System.MonoEnumInfo/ShortComparer
#include "mscorlib_System_MonoEnumInfo_ShortComparer.h"
// System.MonoEnumInfo/IntComparer
#include "mscorlib_System_MonoEnumInfo_IntComparer.h"
// System.MonoEnumInfo/LongComparer
#include "mscorlib_System_MonoEnumInfo_LongComparer.h"
// System.ExecutionEngineException
#include "mscorlib_System_ExecutionEngineException.h"
// System.FieldAccessException
#include "mscorlib_System_FieldAccessException.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.GC
#include "mscorlib_System_GC.h"
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeException.h"
// System.Math
#include "mscorlib_System_Math.h"
// System.MemberAccessException
#include "mscorlib_System_MemberAccessException.h"
// System.MethodAccessException
#include "mscorlib_System_MethodAccessException.h"
// System.MissingFieldException
#include "mscorlib_System_MissingFieldException.h"
// System.MulticastNotSupportedException
#include "mscorlib_System_MulticastNotSupportedException.h"
// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttribute.h"
// System.NotImplementedException
#include "mscorlib_System_NotImplementedException.h"
// System.RankException
#include "mscorlib_System_RankException.h"
// System.SystemException
#include "mscorlib_System_SystemException.h"
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
// System.UnauthorizedAccessException
#include "mscorlib_System_UnauthorizedAccessException.h"
// Mono.Math.Prime.PrimalityTest
#include "mscorlib_Mono_Math_Prime_PrimalityTest.h"
// System.Reflection.MemberFilter
#include "mscorlib_System_Reflection_MemberFilter.h"
// System.Reflection.TypeFilter
#include "mscorlib_System_Reflection_TypeFilter.h"
// System.Runtime.Remoting.Contexts.CrossContextDelegate
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextDelega.h"
// System.Runtime.Remoting.Messaging.HeaderHandler
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandler.h"
// System.Threading.ThreadStart
#include "mscorlib_System_Threading_ThreadStart.h"
// System.Threading.TimerCallback
#include "mscorlib_System_Threading_TimerCallback.h"
// System.Threading.WaitCallback
#include "mscorlib_System_Threading_WaitCallback.h"
// System.AppDomainInitializer
#include "mscorlib_System_AppDomainInitializer.h"
// System.AssemblyLoadEventHandler
#include "mscorlib_System_AssemblyLoadEventHandler.h"
// System.EventHandler
#include "mscorlib_System_EventHandler.h"
// System.ResolveEventHandler
#include "mscorlib_System_ResolveEventHandler.h"
// System.UnhandledExceptionEventHandler
#include "mscorlib_System_UnhandledExceptionEventHandler.h"
// <PrivateImplementationDetails>/$ArrayType$56
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245.h"
// <PrivateImplementationDetails>/$ArrayType$24
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242.h"
// <PrivateImplementationDetails>/$ArrayType$16
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241.h"
// <PrivateImplementationDetails>/$ArrayType$120
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0.h"
// <PrivateImplementationDetails>/$ArrayType$3132
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243.h"
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0.h"
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0.h"
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244.h"
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246.h"
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1.h"
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2.h"
// <PrivateImplementationDetails>/$ArrayType$8
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU248.h"
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247.h"
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3.h"
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249.h"
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1.h"
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2.h"
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4.h"
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0.h"
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5.h"
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0.h"
extern const Il2CppTypeDefinitionSizes g_Il2CppTypeDefinitionSizesTable[2111] = 
{
	sizeof (U3CModuleU3E_t0), -1, 0, 0,
	sizeof (Aabb2_t1), -1, 0, 0,
	sizeof (Assertion_t3), -1, 0, 0,
	sizeof (OrthographicCamera_t4), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (ColorUtils_t8), -1, sizeof(ColorUtils_t8_StaticFields), 0,
	0, -1, 0, 0,
	sizeof (Comment_t10), -1, 0, 0,
	sizeof (CommonUtils_t11), -1, 0, 0,
	sizeof (Comparison_t12), -1, 0, 0,
	sizeof (CountdownTimer_t13), -1, 0, 0,
	sizeof (DelegateCommand_t15), -1, 0, 0,
	sizeof (DontDestroyOnLoadComponent_t17), -1, 0, 0,
	sizeof (StringEnumExtension_t18), -1, 0, 0,
	sizeof (IntEnumExtension_t19), -1, 0, 0,
	sizeof (EnumExtension_t23), -1, 0, 0,
	sizeof (U3CGetFlagValuesU3Ec__Iterator0_t20), -1, 0, 0,
	sizeof (FrameRate_t24), -1, 0, 0,
	sizeof (FrameRateView_t25), -1, 0, 0,
	sizeof (FsmDelegateAction_t32), -1, 0, 0,
	sizeof (FsmActionRoutine_t27), sizeof(methodPointerType), 0, 0,
	sizeof (MoveAction_t34), -1, 0, 0,
	sizeof (MoveAlongDirection_t37), -1, 0, 0,
	sizeof (MoveAlongDirectionByPolledTime_t38), -1, 0, 0,
	sizeof (NonResettingTimedWait_t39), -1, 0, 0,
	sizeof (RotateAction_t40), -1, 0, 0,
	sizeof (ScaleAction_t42), -1, 0, 0,
	sizeof (TimedFadeVolume_t43), -1, 0, 0,
	sizeof (TimedWaitAction_t45), -1, 0, 0,
	sizeof (ConcreteFsmState_t46), -1, 0, 0,
	sizeof (Fsm_t47), -1, sizeof(Fsm_t47_StaticFields), 0,
	sizeof (StateActionProcessor_t50), sizeof(methodPointerType), 0, 0,
	sizeof (U3CExitStateU3Ec__AnonStoreyF_t52), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (FsmActionAdapter_t33), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (SelectionListGui_t54), -1, 0, 0,
	sizeof (DoubleClickCallback_t53), sizeof(methodPointerType), 0, 0,
	sizeof (HudOrthoAutoScale_t55), -1, 0, 0,
	sizeof (InputLayer_t56), -1, 0, 0,
	sizeof (InputLayerElement_t58), -1, 0, 0,
	sizeof (InputLayerStack_t61), -1, 0, 0,
	sizeof (InterpolationUtils_t64), -1, 0, 0,
	sizeof (Json_t71), -1, 0, 0,
	sizeof (Parser_t66), -1, sizeof(Parser_t66_StaticFields), 0,
	sizeof (TOKEN_t65)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Serializer_t69), -1, 0, 0,
	sizeof (Log_t72), -1, 0, 0,
	sizeof (LogLevel_t73), -1, sizeof(LogLevel_t73_StaticFields), 0,
	sizeof (Logger_t75), -1, sizeof(Logger_t75_StaticFields), 0,
	sizeof (LoggerComponent_t78), -1, 0, 0,
	sizeof (IntVector2_t79), -1, sizeof(IntVector2_t79_StaticFields), 0,
	sizeof (MeshMerger_t80), -1, 0, 0,
	sizeof (MeshShaderSetter_t83), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (NotificationHandlerComponent_t84), -1, 0, 0,
	sizeof (NotificationInstance_t85), -1, sizeof(NotificationInstance_t85_StaticFields), 0,
	sizeof (NotificationSystem_t89), -1, sizeof(NotificationSystem_t89_StaticFields), 0,
	sizeof (NotificationInitializer_t88), sizeof(methodPointerType), 0, 0,
	sizeof (NotificationSystemComponent_t91), -1, 0, 0,
	sizeof (PrefabManager_t96), -1, 0, 0,
	sizeof (PruneData_t93), -1, 0, 0,
	sizeof (PreloadData_t94), -1, 0, 0,
	sizeof (U3CPreloadU3Ec__Iterator1_t95), -1, 0, 0,
	sizeof (U3CPruneItemsU3Ec__Iterator2_t97), -1, 0, 0,
	sizeof (ProceduralTexturedQuad_t102), -1, 0, 0,
	0, 0, 0, 0,
	sizeof (QuaternionUtils_t105), -1, 0, 0,
	sizeof (ConcreteQueryRequest_t106), -1, 0, 0,
	sizeof (ConcreteQueryResult_t107), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (QuerySystem_t108), -1, sizeof(QuerySystem_t108_StaticFields), 0,
	sizeof (QuerySystemImplementation_t109), -1, 0, 0,
	sizeof (SelfManagingSwarmItemManager_t99), -1, 0, 0,
	sizeof (ConcreteSignalParameters_t113), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (Signal_t116), -1, 0, 0,
	sizeof (SignalListener_t114), sizeof(methodPointerType), 0, 0,
	sizeof (StringToSignalMapper_t118), -1, 0, 0,
	sizeof (TimeReference_t14), -1, sizeof(TimeReference_t14_StaticFields), 0,
	sizeof (TimeReferencePool_t120), -1, sizeof(TimeReferencePool_t120_StaticFields), 0,
	sizeof (TimedKill_t123), -1, 0, 0,
	sizeof (U3CKillAfterDurationU3Ec__Iterator3_t122), -1, 0, 0,
	sizeof (TouchCollider_t125), -1, 0, 0,
	sizeof (RotationInRadians_t128), -1, 0, 0,
	sizeof (BoundsExtensions_t129), -1, 0, 0,
	sizeof (GameObjectExtensions_t130), -1, 0, 0,
	sizeof (TransformExtensions_t131), -1, 0, 0,
	sizeof (UnityUtils_t132), -1, 0, 0,
	sizeof (Factory_t133), -1, sizeof(Factory_t133_StaticFields), 0,
	0, -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (Platform_t135), -1, 0, 0,
	0, 0, 0, 0,
	sizeof (PopupValueSet_t136), -1, 0, 0,
	0, 0, 0, 0,
	sizeof (SimpleEncryption_t138), -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (VectorUtils_t140), -1, sizeof(VectorUtils_t140_StaticFields), 0,
	sizeof (SimpleXmlNode_t142), -1, 0, 0,
	sizeof (NodeProcessor_t141), sizeof(methodPointerType), 0, 0,
	sizeof (SimpleXmlNodeList_t145), -1, 0, 0,
	sizeof (SimpleXmlReader_t146), -1, sizeof(SimpleXmlReader_t146_StaticFields), 0,
	sizeof (XmlTest_t147), -1, 0, 0,
	sizeof (XmlVariables_t149), -1, sizeof(XmlVariables_t149_StaticFields), 0,
	sizeof (SwarmItem_t124), -1, 0, 0,
	sizeof (STATE_t150)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SwarmItemManager_t111), -1, 0, 0,
	sizeof (PrefabItemLists_t151), -1, 0, 0,
	sizeof (PrefabItem_t154), -1, 0, 0,
	sizeof (DispatcherBase_t158), -1, sizeof(DispatcherBase_t158_StaticFields), 0,
	sizeof (Dispatcher_t162), -1, sizeof(Dispatcher_t162_StaticFields), sizeof(Dispatcher_t162_ThreadStaticFields),
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (TaskSortingSystem_t164)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TaskBase_t163), -1, 0, 0,
	sizeof (Task_t165), -1, 0, 0,
	0, 0, 0, 0,
	sizeof (TaskDistributor_t166), -1, sizeof(TaskDistributor_t166_StaticFields), 0,
	sizeof (TaskWorker_t168), -1, 0, 0,
	sizeof (ThreadBase_t169), -1, 0, sizeof(ThreadBase_t169_ThreadStaticFields),
	sizeof (ActionThread_t171), -1, 0, 0,
	sizeof (EnumeratableActionThread_t173), -1, 0, 0,
	sizeof (UnityThreadHelper_t181), -1, sizeof(UnityThreadHelper_t181_StaticFields), 0,
	sizeof (U3CCreateThreadU3Ec__AnonStorey12_t175), -1, 0, 0,
	sizeof (U3CCreateThreadU3Ec__AnonStorey13_t176), -1, 0, 0,
	sizeof (U3CCreateThreadU3Ec__AnonStorey14_t177), -1, 0, 0,
	sizeof (U3CCreateThreadU3Ec__AnonStorey15_t178), -1, 0, 0,
	sizeof (U3CCreateThreadU3Ec__AnonStorey16_t180), -1, 0, 0,
	sizeof (BridgeHandler_t185), -1, sizeof(BridgeHandler_t185_StaticFields), 0,
	sizeof (U3COnStartAudioRecordU3Ec__Iterator5_t184), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (NativeCallsIOS_t187), -1, 0, 0,
	sizeof (EScreens_t188)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ESubScreens_t189)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Screens_t190), -1, sizeof(Screens_t190_StaticFields), 0,
	sizeof (GameSignals_t193), -1, sizeof(GameSignals_t193_StaticFields), 0,
	sizeof (PanaloCam_t194), -1, sizeof(PanaloCam_t194_StaticFields), 0,
	sizeof (PanaloCamSystem_t195), -1, 0, 0,
	sizeof (Params_t197), -1, sizeof(Params_t197_StaticFields), 0,
	sizeof (Raffle_t202), -1, 0, 0,
	sizeof (U3CProcessRequestU3Ec__Iterator6_t198), -1, 0, 0,
	sizeof (U3CRegisterUserU3Ec__AnonStorey17_t201), -1, 0, 0,
	sizeof (U3CGetPlayerInformationU3Ec__AnonStorey18_t203), -1, 0, 0,
	sizeof (U3CRegisterPlayerToPromoU3Ec__AnonStorey19_t204), -1, 0, 0,
	sizeof (U3CGetPromoWinnerU3Ec__AnonStorey1A_t205), -1, 0, 0,
	sizeof (ERaffleResult_t207)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (RaffleData_t206), -1, 0, 0,
	sizeof (ScannerHandler_t209), -1, 0, 0,
	sizeof (ScannerHudRoot_t212), -1, 0, 0,
	sizeof (ScannerRoot_t215), -1, 0, 0,
	sizeof (EStatus_t216)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (LoadingSceneRoot_t218), -1, sizeof(LoadingSceneRoot_t218_StaticFields), 0,
	sizeof (U3CCheckRaffleResultsU3Ec__Iterator7_t217), -1, sizeof(U3CCheckRaffleResultsU3Ec__Iterator7_t217_StaticFields), 0,
	sizeof (U3CCheckRegisterMessageU3Ec__Iterator8_t220), -1, sizeof(U3CCheckRegisterMessageU3Ec__Iterator8_t220_StaticFields), 0,
	sizeof (U3CPrepareFsmU3Ec__AnonStorey1B_t222), -1, 0, 0,
	sizeof (RaffleEntryRoot_t224), -1, 0, 0,
	sizeof (RegisterSceneRoot_t225), -1, 0, 0,
	sizeof (AudioRecognizer_t229), -1, 0, 0,
	sizeof (TitleSceneRoot_t230), -1, 0, 0,
	sizeof (Alive_t232), -1, 0, 0,
	sizeof (Animator_t236), -1, 0, 0,
	sizeof (U3CPlayAnimationLoopU3Ec__Iterator9_t233), -1, 0, 0,
	sizeof (AnimatorFrame_t235)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (Poller_t240), -1, 0, 0,
	sizeof (U3CStartPollU3Ec__IteratorA_t239), -1, 0, 0,
	sizeof (Window_t223), -1, 0, 0,
	sizeof (VideoPlayer_t241), -1, 0, 0,
	sizeof (PanaloVideoPlayer_t242), -1, 0, 0,
	sizeof (U3CPlayU3Ec__IteratorB_t245), -1, 0, 0,
	sizeof (MoveSample_t246), -1, 0, 0,
	sizeof (RotateSample_t247), -1, 0, 0,
	sizeof (SampleInfo_t248), -1, 0, 0,
	sizeof (iTween_t258), -1, sizeof(iTween_t258_StaticFields), 0,
	sizeof (EaseType_t249)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (LoopType_t250)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (NamedValueColor_t251)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Defaults_t252), -1, sizeof(Defaults_t252_StaticFields), 0,
	sizeof (CRSpline_t253), -1, 0, 0,
	sizeof (EasingFunction_t255), sizeof(methodPointerType), 0, 0,
	sizeof (ApplyTween_t256), sizeof(methodPointerType), 0, 0,
	sizeof (U3CTweenDelayU3Ec__IteratorC_t257), -1, 0, 0,
	sizeof (U3CTweenRestartU3Ec__IteratorD_t259), -1, 0, 0,
	sizeof (U3CStartU3Ec__IteratorE_t260), -1, 0, 0,
	sizeof (BackgroundPlaneBehaviour_t268), -1, 0, 0,
	sizeof (CloudRecoBehaviour_t270), -1, 0, 0,
	sizeof (CylinderTargetBehaviour_t272), -1, 0, 0,
	sizeof (DatabaseLoadBehaviour_t274), -1, 0, 0,
	sizeof (DefaultInitializationErrorHandler_t276), -1, 0, 0,
	sizeof (DefaultSmartTerrainEventHandler_t277), -1, 0, 0,
	sizeof (DefaultTrackableEventHandler_t281), -1, 0, 0,
	sizeof (GLErrorHandler_t282), -1, sizeof(GLErrorHandler_t282_StaticFields), 0,
	sizeof (HideExcessAreaBehaviour_t283), -1, 0, 0,
	sizeof (ImageTargetBehaviour_t210), -1, 0, 0,
	sizeof (AndroidUnityPlayer_t286), -1, 0, 0,
	sizeof (ComponentFactoryStarterBehaviour_t287), -1, 0, 0,
	sizeof (IOSUnityPlayer_t288), -1, 0, 0,
	sizeof (VuforiaBehaviourComponentFactory_t289), -1, 0, 0,
	sizeof (KeepAliveBehaviour_t290), -1, 0, 0,
	sizeof (MarkerBehaviour_t292), -1, 0, 0,
	sizeof (MaskOutBehaviour_t294), -1, 0, 0,
	sizeof (MultiTargetBehaviour_t296), -1, 0, 0,
	sizeof (ObjectTargetBehaviour_t298), -1, 0, 0,
	sizeof (PropBehaviour_t279), -1, 0, 0,
	sizeof (ReconstructionBehaviour_t278), -1, 0, 0,
	sizeof (ReconstructionFromTargetBehaviour_t302), -1, 0, 0,
	sizeof (SmartTerrainTrackerBehaviour_t304), -1, 0, 0,
	sizeof (SurfaceBehaviour_t280), -1, 0, 0,
	sizeof (TextRecoBehaviour_t307), -1, 0, 0,
	sizeof (TurnOffBehaviour_t309), -1, 0, 0,
	sizeof (TurnOffWordBehaviour_t311), -1, 0, 0,
	sizeof (UserDefinedTargetBuildingBehaviour_t312), -1, 0, 0,
	sizeof (VideoBackgroundBehaviour_t314), -1, 0, 0,
	sizeof (VideoTextureRenderer_t316), -1, 0, 0,
	sizeof (VirtualButtonBehaviour_t318), -1, 0, 0,
	sizeof (VuforiaBehaviour_t320), -1, sizeof(VuforiaBehaviour_t320_StaticFields), 0,
	sizeof (WebCamBehaviour_t322), -1, 0, 0,
	sizeof (WireframeBehaviour_t324), -1, 0, 0,
	sizeof (WireframeTrackableEventHandler_t325), -1, 0, 0,
	sizeof (WordBehaviour_t326), -1, 0, 0,
	sizeof (QueryResultResolver_t328), sizeof(methodPointerType), 0, 0,
	sizeof (ListenRaffle_t331), sizeof(methodPointerType), 0, 0,
	sizeof (ListenRaffleResult_t332), sizeof(methodPointerType), 0, 0,
	sizeof (RequestHandler_t200), sizeof(methodPointerType), 0, 0,
	sizeof (Handle_t221), sizeof(methodPointerType), 0, 0,
	sizeof (U3CPrivateImplementationDetailsU3E_t334), -1, sizeof(U3CPrivateImplementationDetailsU3E_t334_StaticFields), 0,
	sizeof (U24ArrayTypeU2424_t333)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2424_t333_marshaled), 0, 0,
	sizeof (U3CModuleU3E_t474), -1, 0, 0,
	sizeof (EventHandle_t475)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (EventSystem_t476), -1, sizeof(EventSystem_t476_StaticFields), 0,
	sizeof (EventTrigger_t485), -1, 0, 0,
	sizeof (TriggerEvent_t482), -1, 0, 0,
	sizeof (Entry_t484), -1, 0, 0,
	sizeof (EventTriggerType_t487)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ExecuteEvents_t488), -1, sizeof(ExecuteEvents_t488_StaticFields), 0,
	0, 0, 0, 0,
	sizeof (MoveDirection_t509)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (RaycasterManager_t510), -1, sizeof(RaycasterManager_t510_StaticFields), 0,
	sizeof (RaycastResult_t512)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (UIBehaviour_t477), -1, 0, 0,
	sizeof (AxisEventData_t514), -1, 0, 0,
	sizeof (BaseEventData_t480), -1, 0, 0,
	sizeof (PointerEventData_t517), -1, 0, 0,
	sizeof (InputButton_t515)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FramePressState_t516)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (BaseInputModule_t479), -1, 0, 0,
	sizeof (PointerInputModule_t524), -1, 0, 0,
	sizeof (ButtonState_t520), -1, 0, 0,
	sizeof (MouseState_t522), -1, 0, 0,
	sizeof (MouseButtonEventData_t521), -1, 0, 0,
	sizeof (StandaloneInputModule_t527), -1, 0, 0,
	sizeof (InputMode_t526)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TouchInputModule_t528), -1, 0, 0,
	sizeof (BaseRaycaster_t513), -1, 0, 0,
	sizeof (Physics2DRaycaster_t529), -1, 0, 0,
	sizeof (PhysicsRaycaster_t530), -1, sizeof(PhysicsRaycaster_t530_StaticFields), 0,
	0, -1, 0, 0,
	sizeof (ColorTween_t536)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (ColorTweenMode_t533)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ColorTweenCallback_t534), -1, 0, 0,
	sizeof (FloatTween_t539)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (FloatTweenCallback_t537), -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (AnimationTriggers_t540), -1, 0, 0,
	sizeof (Button_t544), -1, 0, 0,
	sizeof (ButtonClickedEvent_t541), -1, 0, 0,
	sizeof (U3COnFinishSubmitU3Ec__Iterator1_t543), -1, 0, 0,
	sizeof (CanvasUpdate_t546)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	0, -1, 0, 0,
	sizeof (CanvasUpdateRegistry_t547), -1, sizeof(CanvasUpdateRegistry_t547_StaticFields), 0,
	sizeof (ColorBlock_t551)+ sizeof (Il2CppObject), sizeof(ColorBlock_t551 ), 0, 0,
	sizeof (DefaultControls_t554), -1, sizeof(DefaultControls_t554_StaticFields), 0,
	sizeof (Resources_t552)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (Dropdown_t564), -1, 0, 0,
	sizeof (DropdownItem_t555), -1, 0, 0,
	sizeof (OptionData_t558), -1, 0, 0,
	sizeof (OptionDataList_t559), -1, 0, 0,
	sizeof (DropdownEvent_t561), -1, 0, 0,
	sizeof (U3CDelayedDestroyDropdownListU3Ec__Iterator2_t563), -1, 0, 0,
	sizeof (U3CShowU3Ec__AnonStorey6_t565), -1, 0, 0,
	sizeof (FontData_t568), -1, 0, 0,
	sizeof (FontUpdateTracker_t570), -1, sizeof(FontUpdateTracker_t570_StaticFields), 0,
	sizeof (Graphic_t572), -1, sizeof(Graphic_t572_StaticFields), 0,
	sizeof (GraphicRaycaster_t578), -1, sizeof(GraphicRaycaster_t578_StaticFields), 0,
	sizeof (BlockingObjects_t577)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (GraphicRegistry_t581), -1, sizeof(GraphicRegistry_t581_StaticFields), 0,
	0, -1, 0, 0,
	sizeof (Image_t213), -1, sizeof(Image_t213_StaticFields), 0,
	sizeof (Type_t583)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FillMethod_t584)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (OriginHorizontal_t585)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (OriginVertical_t586)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Origin90_t587)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Origin180_t588)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Origin360_t589)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (InputField_t226), -1, sizeof(InputField_t226_StaticFields), 0,
	sizeof (ContentType_t591)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (InputType_t592)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CharacterValidation_t593)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (LineType_t594)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SubmitEvent_t595), -1, 0, 0,
	sizeof (OnChangeEvent_t597), -1, 0, 0,
	sizeof (EditState_t598)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (OnValidateInput_t599), sizeof(methodPointerType), 0, 0,
	sizeof (U3CCaretBlinkU3Ec__Iterator3_t600), -1, 0, 0,
	sizeof (U3CMouseDragOutsideRectU3Ec__Iterator4_t601), -1, 0, 0,
	sizeof (Mask_t606), -1, 0, 0,
	sizeof (MaskableGraphic_t590), -1, 0, 0,
	sizeof (CullStateChangedEvent_t607), -1, 0, 0,
	sizeof (MaskUtilities_t610), -1, 0, 0,
	sizeof (Misc_t611), -1, 0, 0,
	sizeof (Navigation_t613)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (Mode_t612)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (RawImage_t237), -1, 0, 0,
	sizeof (RectMask2D_t609), -1, 0, 0,
	sizeof (Scrollbar_t621), -1, 0, 0,
	sizeof (Direction_t617)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ScrollEvent_t618), -1, 0, 0,
	sizeof (Axis_t619)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (U3CClickRepeatU3Ec__Iterator5_t620), -1, 0, 0,
	sizeof (ScrollRect_t627), -1, 0, 0,
	sizeof (MovementType_t623)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ScrollbarVisibility_t624)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ScrollRectEvent_t625), -1, 0, 0,
	sizeof (Selectable_t545), -1, sizeof(Selectable_t545_StaticFields), 0,
	sizeof (Transition_t628)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SelectionState_t629)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SetPropertyUtility_t633), -1, 0, 0,
	sizeof (Slider_t637), -1, 0, 0,
	sizeof (Direction_t634)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SliderEvent_t635), -1, 0, 0,
	sizeof (Axis_t636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SpriteState_t630)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (StencilMaterial_t639), -1, sizeof(StencilMaterial_t639_StaticFields), 0,
	sizeof (MatEntry_t638), -1, 0, 0,
	sizeof (Text_t196), -1, sizeof(Text_t196_StaticFields), 0,
	sizeof (Toggle_t557), -1, 0, 0,
	sizeof (ToggleTransition_t641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ToggleEvent_t642), -1, 0, 0,
	sizeof (ToggleGroup_t643), -1, sizeof(ToggleGroup_t643_StaticFields), 0,
	sizeof (ClipperRegistry_t647), -1, sizeof(ClipperRegistry_t647_StaticFields), 0,
	sizeof (Clipping_t649), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (RectangularVertexClipper_t614), -1, 0, 0,
	sizeof (AspectRatioFitter_t651), -1, 0, 0,
	sizeof (AspectMode_t650)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CanvasScaler_t655), -1, 0, 0,
	sizeof (ScaleMode_t652)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ScreenMatchMode_t653)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Unit_t654)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ContentSizeFitter_t657), -1, 0, 0,
	sizeof (FitMode_t656)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (GridLayoutGroup_t661), -1, 0, 0,
	sizeof (Corner_t658)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Axis_t659)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Constraint_t660)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (HorizontalLayoutGroup_t663), -1, 0, 0,
	sizeof (HorizontalOrVerticalLayoutGroup_t664), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (LayoutElement_t665), -1, 0, 0,
	sizeof (LayoutGroup_t662), -1, 0, 0,
	sizeof (LayoutRebuilder_t668)+ sizeof (Il2CppObject), -1, sizeof(LayoutRebuilder_t668_StaticFields), 0,
	sizeof (LayoutUtility_t671), -1, sizeof(LayoutUtility_t671_StaticFields), 0,
	sizeof (VerticalLayoutGroup_t673), -1, 0, 0,
	0, -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (VertexHelper_t674), -1, sizeof(VertexHelper_t674_StaticFields), 0,
	sizeof (BaseVertexEffect_t681), -1, 0, 0,
	sizeof (BaseMeshEffect_t682), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (Outline_t683), -1, 0, 0,
	sizeof (PositionAsUV1_t685), -1, 0, 0,
	sizeof (Shadow_t684), -1, 0, 0,
	sizeof (U3CModuleU3E_t781), -1, 0, 0,
	sizeof (AssetBundleCreateRequest_t782), -1, 0, 0,
	sizeof (AssetBundleRequest_t784), -1, 0, 0,
	sizeof (AssetBundle_t785), -1, 0, 0,
	sizeof (SendMessageOptions_t786)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (PrimitiveType_t787)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Space_t454)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (RuntimePlatform_t788)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (LogType_t789)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SystemInfo_t790), -1, 0, 0,
	sizeof (WaitForSeconds_t410), sizeof(WaitForSeconds_t410_marshaled), 0, 0,
	sizeof (WaitForFixedUpdate_t792), -1, 0, 0,
	sizeof (WaitForEndOfFrame_t439), -1, 0, 0,
	sizeof (Coroutine_t442), sizeof(Coroutine_t442_marshaled), 0, 0,
	sizeof (ScriptableObject_t793), sizeof(ScriptableObject_t793_marshaled), 0, 0,
	sizeof (UnhandledExceptionHandler_t794), -1, 0, 0,
	sizeof (GameCenterPlatform_t795), -1, sizeof(GameCenterPlatform_t795_StaticFields), 0,
	sizeof (GcLeaderboard_t805), -1, 0, 0,
	sizeof (MeshFilter_t398), -1, 0, 0,
	sizeof (Mesh_t104), -1, 0, 0,
	sizeof (BoneWeight_t405)+ sizeof (Il2CppObject), sizeof(BoneWeight_t405 ), 0, 0,
	sizeof (SkinnedMeshRenderer_t403), -1, 0, 0,
	sizeof (Renderer_t367), -1, 0, 0,
	sizeof (Screen_t807), -1, 0, 0,
	sizeof (GL_t808), -1, 0, 0,
	sizeof (MeshRenderer_t402), -1, 0, 0,
	sizeof (GUIElement_t809), -1, 0, 0,
	sizeof (GUITexture_t243), -1, 0, 0,
	sizeof (GUILayer_t810), -1, 0, 0,
	sizeof (Texture_t103), -1, 0, 0,
	sizeof (Texture2D_t355), -1, 0, 0,
	sizeof (RenderTexture_t811), -1, 0, 0,
	sizeof (ReflectionProbe_t812), -1, 0, 0,
	sizeof (CullingGroupEvent_t813)+ sizeof (Il2CppObject), sizeof(CullingGroupEvent_t813 ), 0, 0,
	sizeof (CullingGroup_t815), -1, 0, 0,
	sizeof (StateChanged_t814), sizeof(methodPointerType), 0, 0,
	sizeof (GradientColorKey_t816)+ sizeof (Il2CppObject), sizeof(GradientColorKey_t816 ), 0, 0,
	sizeof (GradientAlphaKey_t817)+ sizeof (Il2CppObject), sizeof(GradientAlphaKey_t817 ), 0, 0,
	sizeof (Gradient_t818), sizeof(Gradient_t818_marshaled), 0, 0,
	sizeof (TouchScreenKeyboard_InternalConstructorHelperArguments_t819)+ sizeof (Il2CppObject), sizeof(TouchScreenKeyboard_InternalConstructorHelperArguments_t819 ), 0, 0,
	sizeof (FullScreenMovieControlMode_t820)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FullScreenMovieScalingMode_t821)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Handheld_t822), -1, 0, 0,
	sizeof (TouchScreenKeyboardType_t749)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TouchScreenKeyboard_t604), -1, 0, 0,
	sizeof (Gizmos_t823), -1, 0, 0,
	sizeof (LayerMask_t531)+ sizeof (Il2CppObject), sizeof(LayerMask_t531 ), 0, 0,
	sizeof (Vector2_t2)+ sizeof (Il2CppObject), sizeof(Vector2_t2 ), 0, 0,
	sizeof (Vector3_t36)+ sizeof (Il2CppObject), sizeof(Vector3_t36 ), 0, 0,
	sizeof (Color_t9)+ sizeof (Il2CppObject), sizeof(Color_t9 ), 0, 0,
	sizeof (Color32_t711)+ sizeof (Il2CppObject), sizeof(Color32_t711 ), 0, 0,
	sizeof (Quaternion_t41)+ sizeof (Il2CppObject), sizeof(Quaternion_t41 ), 0, 0,
	sizeof (Rect_t267)+ sizeof (Il2CppObject), sizeof(Rect_t267 ), 0, 0,
	sizeof (Matrix4x4_t404)+ sizeof (Il2CppObject), sizeof(Matrix4x4_t404 ), 0, 0,
	sizeof (Bounds_t349)+ sizeof (Il2CppObject), sizeof(Bounds_t349 ), 0, 0,
	sizeof (Vector4_t680)+ sizeof (Il2CppObject), sizeof(Vector4_t680 ), 0, 0,
	sizeof (Ray_t382)+ sizeof (Il2CppObject), sizeof(Ray_t382 ), 0, 0,
	sizeof (Plane_t751)+ sizeof (Il2CppObject), sizeof(Plane_t751 ), 0, 0,
	sizeof (MathfInternal_t824)+ sizeof (Il2CppObject), sizeof(MathfInternal_t824 ), sizeof(MathfInternal_t824_StaticFields), 0,
	sizeof (Mathf_t371)+ sizeof (Il2CppObject), sizeof(Mathf_t371 ), sizeof(Mathf_t371_StaticFields), 0,
	sizeof (DrivenTransformProperties_t825)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DrivenRectTransformTracker_t622)+ sizeof (Il2CppObject), sizeof(DrivenRectTransformTracker_t622 ), 0, 0,
	sizeof (RectTransform_t556), -1, sizeof(RectTransform_t556_StaticFields), 0,
	sizeof (Edge_t826)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Axis_t827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ReapplyDrivenProperties_t769), sizeof(methodPointerType), 0, 0,
	sizeof (ResourceRequest_t828), -1, 0, 0,
	sizeof (Resources_t829), -1, 0, 0,
	sizeof (TextAsset_t148), -1, 0, 0,
	sizeof (SerializePrivateVariables_t830), -1, 0, 0,
	sizeof (SerializeField_t831), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (Shader_t407), -1, 0, 0,
	sizeof (Material_t82), -1, 0, 0,
	sizeof (SortingLayer_t832)+ sizeof (Il2CppObject), sizeof(SortingLayer_t832 ), 0, 0,
	sizeof (SphericalHarmonicsL2_t833)+ sizeof (Il2CppObject), sizeof(SphericalHarmonicsL2_t833 ), 0, 0,
	sizeof (Sprite_t553), -1, 0, 0,
	sizeof (SpriteRenderer_t727), -1, 0, 0,
	sizeof (DataUtility_t834), -1, 0, 0,
	sizeof (WWW_t199), -1, 0, 0,
	sizeof (WWWForm_t433), -1, 0, 0,
	sizeof (WWWTranscoder_t836), -1, sizeof(WWWTranscoder_t836_StaticFields), 0,
	sizeof (CacheIndex_t837)+ sizeof (Il2CppObject), sizeof(CacheIndex_t837_marshaled), 0, 0,
	sizeof (Caching_t838), -1, 0, 0,
	sizeof (UnityString_t839), -1, 0, 0,
	sizeof (AsyncOperation_t783), sizeof(AsyncOperation_t783_marshaled), 0, 0,
	sizeof (Application_t841), -1, sizeof(Application_t841_StaticFields), 0,
	sizeof (LogCallback_t840), sizeof(methodPointerType), 0, 0,
	sizeof (Behaviour_t772), -1, 0, 0,
	sizeof (Camera_t6), -1, sizeof(Camera_t6_StaticFields), 0,
	sizeof (CameraCallback_t842), sizeof(methodPointerType), 0, 0,
	sizeof (Debug_t843), -1, 0, 0,
	sizeof (Display_t845), -1, sizeof(Display_t845_StaticFields), 0,
	sizeof (DisplaysUpdatedDelegate_t844), sizeof(methodPointerType), 0, 0,
	sizeof (MonoBehaviour_t5), -1, 0, 0,
	sizeof (TouchPhase_t847)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (IMECompositionMode_t848)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Touch_t705)+ sizeof (Il2CppObject), sizeof(Touch_t705_marshaled), 0, 0,
	sizeof (Input_t412), -1, 0, 0,
	sizeof (HideFlags_t849)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Object_t335), sizeof(Object_t335_marshaled), 0, 0,
	sizeof (Component_t365), -1, 0, 0,
	sizeof (Light_t444), -1, 0, 0,
	sizeof (GameObject_t155), -1, 0, 0,
	sizeof (Transform_t35), -1, 0, 0,
	sizeof (Enumerator_t850), -1, 0, 0,
	sizeof (Time_t851), -1, 0, 0,
	sizeof (Random_t852), -1, 0, 0,
	sizeof (YieldInstruction_t791), sizeof(YieldInstruction_t791_marshaled), 0, 0,
	sizeof (PlayerPrefsException_t853), -1, 0, 0,
	sizeof (PlayerPrefs_t854), -1, 0, 0,
	sizeof (ActivityIndicatorStyle_t855)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DirectorPlayer_t856), -1, 0, 0,
	sizeof (UnityAdsInternal_t857), -1, sizeof(UnityAdsInternal_t857_StaticFields), 0,
	sizeof (Particle_t860)+ sizeof (Il2CppObject), sizeof(Particle_t860 ), 0, 0,
	sizeof (Collision_t861), -1, 0, 0,
	sizeof (QueryTriggerInteraction_t864)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Physics_t865), -1, 0, 0,
	sizeof (ContactPoint_t863)+ sizeof (Il2CppObject), sizeof(ContactPoint_t863 ), 0, 0,
	sizeof (Rigidbody_t447), -1, 0, 0,
	sizeof (Collider_t369), -1, 0, 0,
	sizeof (BoxCollider_t126), -1, 0, 0,
	sizeof (MeshCollider_t866), -1, 0, 0,
	sizeof (RaycastHit_t60)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (Physics2D_t728), -1, sizeof(Physics2D_t728_StaticFields), 0,
	sizeof (RaycastHit2D_t729)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (Rigidbody2D_t868), -1, 0, 0,
	sizeof (Collider2D_t730), -1, 0, 0,
	sizeof (ContactPoint2D_t869)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (Collision2D_t870), -1, 0, 0,
	sizeof (AudioSettings_t873), -1, sizeof(AudioSettings_t873_StaticFields), 0,
	sizeof (AudioConfigurationChangeHandler_t872), sizeof(methodPointerType), 0, 0,
	sizeof (AudioClip_t353), -1, 0, 0,
	sizeof (PCMReaderCallback_t874), sizeof(methodPointerType), 0, 0,
	sizeof (PCMSetPositionCallback_t875), sizeof(methodPointerType), 0, 0,
	sizeof (AudioSource_t44), -1, 0, 0,
	sizeof (WebCamDevice_t876)+ sizeof (Il2CppObject), sizeof(WebCamDevice_t876_marshaled), 0, 0,
	sizeof (WebCamTexture_t877), -1, 0, 0,
	sizeof (AnimationEventSource_t878)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (AnimationEvent_t879), -1, 0, 0,
	sizeof (Keyframe_t883)+ sizeof (Il2CppObject), sizeof(Keyframe_t883 ), 0, 0,
	sizeof (AnimationCurve_t884), sizeof(AnimationCurve_t884_marshaled), 0, 0,
	sizeof (AnimationState_t880), -1, 0, 0,
	sizeof (AnimatorClipInfo_t882)+ sizeof (Il2CppObject), sizeof(AnimatorClipInfo_t882 ), 0, 0,
	sizeof (AnimatorStateInfo_t881)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t881 ), 0, 0,
	sizeof (AnimatorTransitionInfo_t886)+ sizeof (Il2CppObject), sizeof(AnimatorTransitionInfo_t886_marshaled), 0, 0,
	sizeof (Animator_t714), -1, 0, 0,
	sizeof (SkeletonBone_t887)+ sizeof (Il2CppObject), sizeof(SkeletonBone_t887_marshaled), 0, 0,
	sizeof (HumanLimit_t888)+ sizeof (Il2CppObject), sizeof(HumanLimit_t888 ), 0, 0,
	sizeof (HumanBone_t889)+ sizeof (Il2CppObject), sizeof(HumanBone_t889_marshaled), 0, 0,
	sizeof (RuntimeAnimatorController_t758), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (TextAnchor_t766)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (HorizontalWrapMode_t890)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (VerticalWrapMode_t891)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (GUIText_t443), -1, 0, 0,
	sizeof (TextMesh_t26), -1, 0, 0,
	sizeof (CharacterInfo_t892)+ sizeof (Il2CppObject), sizeof(CharacterInfo_t892_marshaled), 0, 0,
	sizeof (Font_t569), -1, sizeof(Font_t569_StaticFields), 0,
	sizeof (FontTextureRebuildCallback_t893), sizeof(methodPointerType), 0, 0,
	sizeof (UICharInfo_t754)+ sizeof (Il2CppObject), sizeof(UICharInfo_t754 ), 0, 0,
	sizeof (UILineInfo_t752)+ sizeof (Il2CppObject), sizeof(UILineInfo_t752 ), 0, 0,
	sizeof (TextGenerator_t603), -1, 0, 0,
	sizeof (RenderMode_t896)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Canvas_t574), -1, sizeof(Canvas_t574_StaticFields), 0,
	sizeof (WillRenderCanvases_t732), sizeof(methodPointerType), 0, 0,
	0, -1, 0, 0,
	sizeof (CanvasGroup_t733), -1, 0, 0,
	sizeof (UIVertex_t605)+ sizeof (Il2CppObject), sizeof(UIVertex_t605 ), sizeof(UIVertex_t605_StaticFields), 0,
	sizeof (CanvasRenderer_t573), -1, 0, 0,
	sizeof (RectTransformUtility_t737), -1, sizeof(RectTransformUtility_t737_StaticFields), 0,
	sizeof (Event_t381), -1, sizeof(Event_t381_StaticFields), 0,
	sizeof (KeyCode_t897)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (EventType_t898)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (EventModifiers_t899)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (GUI_t457), -1, sizeof(GUI_t457_StaticFields), 0,
	sizeof (ScrollViewState_t900), -1, 0, 0,
	sizeof (WindowFunction_t456), sizeof(methodPointerType), 0, 0,
	sizeof (GUIContent_t379), -1, sizeof(GUIContent_t379_StaticFields), 0,
	sizeof (GUILayout_t903), -1, 0, 0,
	sizeof (GUILayoutUtility_t380), -1, sizeof(GUILayoutUtility_t380_StaticFields), 0,
	sizeof (LayoutCache_t904), -1, 0, 0,
	sizeof (GUILayoutEntry_t907), -1, sizeof(GUILayoutEntry_t907_StaticFields), 0,
	sizeof (GUILayoutGroup_t905), -1, 0, 0,
	sizeof (GUIScrollGroup_t909), -1, 0, 0,
	sizeof (GUIWordWrapSizer_t910), -1, 0, 0,
	sizeof (GUILayoutOption_t912), -1, 0, 0,
	sizeof (Type_t911)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (GUISettings_t913), -1, 0, 0,
	sizeof (GUISkin_t901), -1, sizeof(GUISkin_t901_StaticFields), 0,
	sizeof (SkinChangedDelegate_t914), sizeof(methodPointerType), 0, 0,
	sizeof (GUIStyleState_t917), -1, 0, 0,
	sizeof (RectOffset_t666), -1, 0, 0,
	sizeof (FontStyle_t918)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ImagePosition_t919)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (GUIStyle_t342), -1, sizeof(GUIStyle_t342_StaticFields), 0,
	sizeof (ExitGUIException_t920), -1, 0, 0,
	sizeof (FocusType_t921)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (GUIUtility_t750), -1, sizeof(GUIUtility_t750_StaticFields), 0,
	sizeof (Internal_DrawArguments_t922)+ sizeof (Il2CppObject), sizeof(Internal_DrawArguments_t922 ), 0, 0,
	sizeof (WrapperlessIcall_t923), -1, 0, 0,
	sizeof (IL2CPPStructAlignmentAttribute_t924), -1, 0, 0,
	sizeof (AttributeHelperEngine_t925), -1, sizeof(AttributeHelperEngine_t925_StaticFields), 0,
	sizeof (DisallowMultipleComponent_t929), -1, 0, 0,
	sizeof (RequireComponent_t930), -1, 0, 0,
	sizeof (AddComponentMenu_t931), -1, 0, 0,
	sizeof (ExecuteInEditMode_t932), -1, 0, 0,
	sizeof (HideInInspector_t933), -1, 0, 0,
	0, 0, 0, 0,
	sizeof (SetupCoroutine_t934), -1, 0, 0,
	sizeof (WritableAttribute_t935), -1, 0, 0,
	sizeof (AssemblyIsEditorAssembly_t936), -1, 0, 0,
	sizeof (GcUserProfileData_t937)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (GcAchievementDescriptionData_t938)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (GcAchievementData_t939)+ sizeof (Il2CppObject), sizeof(GcAchievementData_t939_marshaled), 0, 0,
	sizeof (GcScoreData_t940)+ sizeof (Il2CppObject), sizeof(GcScoreData_t940_marshaled), 0, 0,
	sizeof (Resolution_t941)+ sizeof (Il2CppObject), sizeof(Resolution_t941 ), 0, 0,
	sizeof (RenderBuffer_t942)+ sizeof (Il2CppObject), sizeof(RenderBuffer_t942 ), 0, 0,
	sizeof (CameraClearFlags_t943)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ScreenOrientation_t944)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FilterMode_t945)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TextureWrapMode_t946)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TextureFormat_t947)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (RenderTextureFormat_t948)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (RenderTextureReadWrite_t949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CompareFunction_t760)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ColorWriteMask_t761)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (StencilOp_t759)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ReflectionProbeBlendInfo_t950)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (LocalUser_t803), -1, 0, 0,
	sizeof (UserProfile_t951), -1, 0, 0,
	sizeof (Achievement_t953), -1, 0, 0,
	sizeof (AchievementDescription_t954), -1, 0, 0,
	sizeof (Score_t955), -1, 0, 0,
	sizeof (Leaderboard_t806), -1, 0, 0,
	sizeof (SendMouseEvents_t960), -1, sizeof(SendMouseEvents_t960_StaticFields), 0,
	sizeof (HitInfo_t959)+ sizeof (Il2CppObject), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (UserState_t962)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (UserScope_t963)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TimeScope_t964)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Range_t957)+ sizeof (Il2CppObject), sizeof(Range_t957 ), 0, 0,
	0, -1, 0, 0,
	sizeof (PropertyAttribute_t965), -1, 0, 0,
	sizeof (TooltipAttribute_t966), -1, 0, 0,
	sizeof (SpaceAttribute_t967), -1, 0, 0,
	sizeof (RangeAttribute_t968), -1, 0, 0,
	sizeof (TextAreaAttribute_t969), -1, 0, 0,
	sizeof (SelectionBaseAttribute_t970), -1, 0, 0,
	sizeof (SliderState_t971), -1, 0, 0,
	sizeof (StackTraceUtility_t972), -1, sizeof(StackTraceUtility_t972_StaticFields), 0,
	sizeof (UnityException_t748), -1, 0, 0,
	sizeof (SharedBetweenAnimatorsAttribute_t973), -1, 0, 0,
	sizeof (StateMachineBehaviour_t974), -1, 0, 0,
	sizeof (TextEditor_t977), -1, sizeof(TextEditor_t977_StaticFields), 0,
	sizeof (DblClickSnapping_t975)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (TextEditOp_t976)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TextGenerationSettings_t715)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (TrackedReference_t885), sizeof(TrackedReference_t885_marshaled), 0, 0,
	sizeof (PersistentListenerMode_t979)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ArgumentCache_t980), -1, 0, 0,
	sizeof (BaseInvokableCall_t981), -1, 0, 0,
	sizeof (InvokableCall_t982), -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (UnityEventCallState_t983)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (PersistentCall_t984), -1, 0, 0,
	sizeof (PersistentCallGroup_t985), -1, 0, 0,
	sizeof (InvokableCallList_t987), -1, 0, 0,
	sizeof (UnityEventBase_t989), -1, 0, 0,
	sizeof (UnityEvent_t542), -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (DefaultValueAttribute_t990), -1, 0, 0,
	sizeof (ExcludeFromDocsAttribute_t991), -1, 0, 0,
	sizeof (FormerlySerializedAsAttribute_t992), -1, 0, 0,
	sizeof (TypeInferenceRules_t993)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TypeInferenceRuleAttribute_t994), -1, 0, 0,
	sizeof (GenericStack_t902), -1, 0, 0,
	sizeof (NetFxCoreExtensions_t996), -1, 0, 0,
	sizeof (UnityAdsDelegate_t858), sizeof(methodPointerType), 0, 0,
	0, 0, 0, 0,
	sizeof (UnityAction_t575), sizeof(methodPointerType), 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (U3CModuleU3E_t1046), -1, 0, 0,
	sizeof (FactorySetter_t465), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (EyewearCalibrationProfileManager_t1047), -1, 0, 0,
	sizeof (EyewearCalibrationProfileManagerImpl_t1048), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (BackgroundPlaneAbstractBehaviour_t269), -1, sizeof(BackgroundPlaneAbstractBehaviour_t269_StaticFields), 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (EyewearUserCalibrator_t1050), -1, 0, 0,
	sizeof (EyewearUserCalibratorImpl_t1051), -1, 0, 0,
	sizeof (IOSCamRecoveringHelper_t1052), -1, sizeof(IOSCamRecoveringHelper_t1052_StaticFields), 0,
	0, -1, 0, 0,
	sizeof (MonoCameraConfiguration_t1053), -1, 0, 0,
	sizeof (StereoCameraConfiguration_t1055), -1, sizeof(StereoCameraConfiguration_t1055_StaticFields), 0,
	sizeof (NullCameraConfiguration_t1056), -1, 0, 0,
	sizeof (UnityCameraExtensions_t1057), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (TrackableBehaviour_t211), -1, 0, 0,
	sizeof (Status_t1058)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	0, -1, 0, 0,
	sizeof (DataSetTrackableBehaviour_t1061), -1, 0, 0,
	sizeof (ObjectTargetAbstractBehaviour_t299), -1, 0, 0,
	sizeof (CameraDevice_t437), -1, sizeof(CameraDevice_t437_StaticFields), 0,
	sizeof (CameraDeviceMode_t1063)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FocusMode_t438)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CameraDirection_t1064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (VideoModeData_t1065)+ sizeof (Il2CppObject), sizeof(VideoModeData_t1065 ), 0, 0,
	sizeof (CloudRecoAbstractBehaviour_t271), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (ReconstructionImpl_t1068), -1, 0, 0,
	sizeof (HideExcessAreaAbstractBehaviour_t284), -1, 0, 0,
	sizeof (TrackableImpl_t1071), -1, 0, 0,
	sizeof (ObjectTargetImpl_t1072), -1, 0, 0,
	sizeof (UnityPlayer_t1074), -1, sizeof(UnityPlayer_t1074_StaticFields), 0,
	0, -1, 0, 0,
	sizeof (NullUnityPlayer_t469), -1, 0, 0,
	sizeof (PlayModeUnityPlayer_t470), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (ReconstructionFromTargetImpl_t1076), -1, 0, 0,
	sizeof (ReconstructionFromTargetAbstractBehaviour_t303), -1, 0, 0,
	sizeof (SmartTerrainBuilder_t1078), -1, 0, 0,
	sizeof (Eyewear_t1081), -1, sizeof(Eyewear_t1081_StaticFields), 0,
	sizeof (EyeID_t1079)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (EyewearCalibrationReading_t1080)+ sizeof (Il2CppObject), sizeof(EyewearCalibrationReading_t1080_marshaled), 0, 0,
	sizeof (EyewearImpl_t1082), -1, 0, 0,
	sizeof (SmartTerrainInitializationInfo_t1083)+ sizeof (Il2CppObject), sizeof(SmartTerrainInitializationInfo_t1083 ), 0, 0,
	sizeof (SmartTerrainTrackableBehaviour_t1084), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (SmartTerrainTrackerAbstractBehaviour_t305), -1, 0, 0,
	sizeof (SurfaceAbstractBehaviour_t306), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (CylinderTargetAbstractBehaviour_t273), -1, 0, 0,
	sizeof (DataSet_t1088), -1, 0, 0,
	sizeof (StorageType_t1087)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DatabaseLoadAbstractBehaviour_t275), -1, 0, 0,
	sizeof (RectangleData_t1089)+ sizeof (Il2CppObject), sizeof(RectangleData_t1089 ), 0, 0,
	sizeof (RectangleIntData_t1090)+ sizeof (Il2CppObject), sizeof(RectangleIntData_t1090 ), 0, 0,
	sizeof (OrientedBoundingBox_t1091)+ sizeof (Il2CppObject), sizeof(OrientedBoundingBox_t1091 ), 0, 0,
	sizeof (OrientedBoundingBox3D_t1092)+ sizeof (Il2CppObject), sizeof(OrientedBoundingBox3D_t1092 ), 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (BehaviourComponentFactory_t1094), -1, sizeof(BehaviourComponentFactory_t1094_StaticFields), 0,
	sizeof (NullBehaviourComponentFactory_t1093), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (CloudRecoImageTargetImpl_t1096), -1, 0, 0,
	sizeof (CylinderTargetImpl_t1097), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (ImageTargetType_t1098)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ImageTargetData_t1099)+ sizeof (Il2CppObject), sizeof(ImageTargetData_t1099 ), 0, 0,
	sizeof (ImageTargetBuilder_t1101), -1, 0, 0,
	sizeof (FrameQuality_t1100)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CameraDeviceImpl_t1102), -1, sizeof(CameraDeviceImpl_t1102_StaticFields), 0,
	sizeof (DataSetImpl_t1073), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (WordTemplateMode_t1107)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	0, -1, 0, 0,
	sizeof (Image_t1109), -1, 0, 0,
	sizeof (PIXEL_FORMAT_t1108)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ImageImpl_t1110), -1, 0, 0,
	sizeof (ImageTargetBuilderImpl_t1111), -1, 0, 0,
	sizeof (ImageTargetImpl_t1113), -1, 0, 0,
	sizeof (Tracker_t1115), -1, 0, 0,
	sizeof (ObjectTracker_t1066), -1, 0, 0,
	sizeof (ObjectTrackerImpl_t1116), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (MarkerImpl_t1120), -1, 0, 0,
	sizeof (MarkerTracker_t1121), -1, 0, 0,
	sizeof (MarkerTrackerImpl_t1122), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (MultiTargetImpl_t1124), -1, 0, 0,
	sizeof (WebCamTexAdaptor_t1125), -1, 0, 0,
	sizeof (NullWebCamTexAdaptor_t1126), -1, 0, 0,
	sizeof (PlayModeEditorUtility_t1128), -1, sizeof(PlayModeEditorUtility_t1128_StaticFields), 0,
	sizeof (NullPlayModeEditorUtility_t1127), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (PremiumObjectFactory_t1131), -1, sizeof(PremiumObjectFactory_t1131_StaticFields), 0,
	sizeof (NullPremiumObjectFactory_t1130), -1, 0, 0,
	sizeof (VuforiaManager_t472), -1, sizeof(VuforiaManager_t472_StaticFields), 0,
	sizeof (VuforiaManagerImpl_t1148), -1, 0, 0,
	sizeof (PoseData_t1133)+ sizeof (Il2CppObject), sizeof(PoseData_t1133 ), 0, 0,
	sizeof (TrackableResultData_t1134)+ sizeof (Il2CppObject), sizeof(TrackableResultData_t1134_marshaled), 0, 0,
	sizeof (VirtualButtonData_t1135)+ sizeof (Il2CppObject), sizeof(VirtualButtonData_t1135 ), 0, 0,
	sizeof (Obb2D_t1136)+ sizeof (Il2CppObject), sizeof(Obb2D_t1136 ), 0, 0,
	sizeof (Obb3D_t1137)+ sizeof (Il2CppObject), sizeof(Obb3D_t1137 ), 0, 0,
	sizeof (WordResultData_t1138)+ sizeof (Il2CppObject), sizeof(WordResultData_t1138_marshaled), 0, 0,
	sizeof (WordData_t1139)+ sizeof (Il2CppObject), sizeof(WordData_t1139 ), 0, 0,
	sizeof (ImageHeaderData_t1140)+ sizeof (Il2CppObject), sizeof(ImageHeaderData_t1140 ), 0, 0,
	sizeof (MeshData_t1141)+ sizeof (Il2CppObject), sizeof(MeshData_t1141 ), 0, 0,
	sizeof (SmartTerrainRevisionData_t1142)+ sizeof (Il2CppObject), sizeof(SmartTerrainRevisionData_t1142 ), 0, 0,
	sizeof (SurfaceData_t1143)+ sizeof (Il2CppObject), sizeof(SurfaceData_t1143 ), 0, 0,
	sizeof (PropData_t1144)+ sizeof (Il2CppObject), sizeof(PropData_t1144 ), 0, 0,
	sizeof (FrameState_t1145)+ sizeof (Il2CppObject), sizeof(FrameState_t1145 ), 0, 0,
	sizeof (AutoRotationState_t1146)+ sizeof (Il2CppObject), sizeof(AutoRotationState_t1146_marshaled), 0, 0,
	sizeof (U3CU3Ec__DisplayClass3_t1147), -1, 0, 0,
	sizeof (VuforiaRenderer_t1158), -1, sizeof(VuforiaRenderer_t1158_StaticFields), 0,
	sizeof (FpsHint_t1154)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (VideoBackgroundReflection_t1155)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (VideoBGCfgData_t1156)+ sizeof (Il2CppObject), sizeof(VideoBGCfgData_t1156_marshaled), 0, 0,
	sizeof (Vec2I_t1157)+ sizeof (Il2CppObject), sizeof(Vec2I_t1157 ), 0, 0,
	sizeof (VideoTextureInfo_t1049)+ sizeof (Il2CppObject), sizeof(VideoTextureInfo_t1049 ), 0, 0,
	sizeof (VuforiaRendererImpl_t1160), -1, 0, 0,
	sizeof (RenderEvent_t1159)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (VuforiaUnityImpl_t1161), -1, sizeof(VuforiaUnityImpl_t1161_StaticFields), 0,
	0, -1, 0, 0,
	sizeof (SmartTerrainTrackableImpl_t1162), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (SurfaceImpl_t1164), -1, 0, 0,
	sizeof (SmartTerrainBuilderImpl_t1165), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (PropImpl_t1167), -1, 0, 0,
	sizeof (SmartTerrainTracker_t1168), -1, 0, 0,
	sizeof (SmartTerrainTrackerImpl_t1169), -1, 0, 0,
	sizeof (TextTracker_t1170), -1, 0, 0,
	sizeof (TextTrackerImpl_t1172), -1, 0, 0,
	sizeof (UpDirection_t1171)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TypeMapping_t1174), -1, sizeof(TypeMapping_t1174_StaticFields), 0,
	sizeof (WebCamTexAdaptorImpl_t1176), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (WordImpl_t1177), -1, 0, 0,
	sizeof (WordPrefabCreationMode_t1179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (WordManager_t1180), -1, 0, 0,
	sizeof (WordManagerImpl_t1181), -1, 0, 0,
	sizeof (WordResult_t1188), -1, 0, 0,
	sizeof (WordResultImpl_t1189), -1, 0, 0,
	sizeof (WordList_t1173), -1, 0, 0,
	sizeof (WordListImpl_t1191), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (VuforiaNativeIosWrapper_t1192), -1, 0, 0,
	sizeof (VuforiaNullWrapper_t1193), -1, 0, 0,
	sizeof (VuforiaNativeWrapper_t1194), -1, 0, 0,
	sizeof (VuforiaWrapper_t1195), -1, sizeof(VuforiaWrapper_t1195_StaticFields), 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (KeepAliveAbstractBehaviour_t291), -1, sizeof(KeepAliveAbstractBehaviour_t291_StaticFields), 0,
	sizeof (ReconstructionAbstractBehaviour_t301), -1, 0, 0,
	sizeof (PropAbstractBehaviour_t300), -1, 0, 0,
	sizeof (StateManager_t1205), -1, 0, 0,
	sizeof (StateManagerImpl_t1206), -1, 0, 0,
	sizeof (TargetFinder_t1119), -1, 0, 0,
	sizeof (InitState_t1209)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (UpdateState_t1210)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FilterMode_t1211)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TargetSearchResult_t1212)+ sizeof (Il2CppObject), sizeof(TargetSearchResult_t1212_marshaled), 0, 0,
	sizeof (TargetFinderImpl_t1215), -1, 0, 0,
	sizeof (TargetFinderState_t1213)+ sizeof (Il2CppObject), sizeof(TargetFinderState_t1213_marshaled), 0, 0,
	sizeof (InternalTargetSearchResult_t1214)+ sizeof (Il2CppObject), sizeof(InternalTargetSearchResult_t1214 ), 0, 0,
	sizeof (TrackableSource_t1112), -1, 0, 0,
	sizeof (TrackableSourceImpl_t1218), -1, 0, 0,
	sizeof (TextureRenderer_t1219), -1, 0, 0,
	sizeof (TrackerManager_t1220), -1, sizeof(TrackerManager_t1220_StaticFields), 0,
	sizeof (TrackerManagerImpl_t1221), -1, 0, 0,
	sizeof (VirtualButton_t1223), -1, 0, 0,
	sizeof (Sensitivity_t1222)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (VirtualButtonImpl_t1224), -1, 0, 0,
	sizeof (WebCamImpl_t1105), -1, 0, 0,
	sizeof (WebCamProfile_t1229), -1, 0, 0,
	sizeof (ProfileData_t1226)+ sizeof (Il2CppObject), sizeof(ProfileData_t1226 ), 0, 0,
	sizeof (ProfileCollection_t1227)+ sizeof (Il2CppObject), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (ImageTargetAbstractBehaviour_t285), -1, 0, 0,
	sizeof (MarkerAbstractBehaviour_t293), -1, 0, 0,
	sizeof (MaskOutAbstractBehaviour_t295), -1, 0, 0,
	sizeof (MultiTargetAbstractBehaviour_t297), -1, 0, 0,
	sizeof (VuforiaUnity_t1236), -1, 0, 0,
	sizeof (InitError_t1233)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (VuforiaHint_t1234)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (StorageType_t1235)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (VuforiaAbstractBehaviour_t321), -1, 0, 0,
	sizeof (WorldCenterMode_t1237)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (VuforiaMacros_t1241)+ sizeof (Il2CppObject), sizeof(VuforiaMacros_t1241 ), 0, 0,
	sizeof (VuforiaRuntimeUtilities_t468), -1, sizeof(VuforiaRuntimeUtilities_t468_StaticFields), 0,
	sizeof (WebCamUsed_t1242)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SurfaceUtilities_t460), -1, sizeof(SurfaceUtilities_t460_StaticFields), 0,
	sizeof (TextRecoAbstractBehaviour_t308), -1, 0, 0,
	sizeof (SimpleTargetData_t1244)+ sizeof (Il2CppObject), sizeof(SimpleTargetData_t1244 ), 0, 0,
	sizeof (TurnOffAbstractBehaviour_t310), -1, 0, 0,
	sizeof (UserDefinedTargetBuildingAbstractBehaviour_t313), -1, 0, 0,
	sizeof (VideoBackgroundAbstractBehaviour_t315), -1, 0, 0,
	sizeof (VideoTextureRendererAbstractBehaviour_t317), -1, 0, 0,
	sizeof (VirtualButtonAbstractBehaviour_t319), -1, 0, 0,
	sizeof (WebCamAbstractBehaviour_t323), -1, 0, 0,
	sizeof (WordAbstractBehaviour_t327), -1, 0, 0,
	sizeof (WordFilterMode_t1248)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (U3CPrivateImplementationDetailsU3EU7BC4A9B53AU2D936FU2D421AU2D9398U2D89108BD15C01U7D_t1250), -1, sizeof(U3CPrivateImplementationDetailsU3EU7BC4A9B53AU2D936FU2D421AU2D9398U2D89108BD15C01U7D_t1250_StaticFields), 0,
	sizeof (__StaticArrayInitTypeSizeU3D24_t1249)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t1249_marshaled), 0, 0,
	sizeof (U3CModuleU3E_t1395), -1, 0, 0,
	sizeof (ExtensionAttribute_t1396), -1, 0, 0,
	sizeof (Locale_t1397), -1, 0, 0,
	sizeof (MonoTODOAttribute_t1398), -1, 0, 0,
	sizeof (KeyBuilder_t1399), -1, sizeof(KeyBuilder_t1399_StaticFields), 0,
	sizeof (SymmetricTransform_t1401), -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (Check_t1403), -1, 0, 0,
	sizeof (Enumerable_t1405), -1, 0, 0,
	sizeof (Fallback_t1404)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (Aes_t1406), -1, 0, 0,
	sizeof (AesManaged_t1407), -1, 0, 0,
	sizeof (AesTransform_t1408), -1, sizeof(AesTransform_t1408_StaticFields), 0,
	sizeof (Action_t16), sizeof(methodPointerType), 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (U3CPrivateImplementationDetailsU3E_t1414), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1414_StaticFields), 0,
	sizeof (U24ArrayTypeU24136_t1410)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t1410_marshaled), 0, 0,
	sizeof (U24ArrayTypeU24120_t1411)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t1411_marshaled), 0, 0,
	sizeof (U24ArrayTypeU24256_t1412)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1412_marshaled), 0, 0,
	sizeof (U24ArrayTypeU241024_t1413)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t1413_marshaled), 0, 0,
	sizeof (U3CModuleU3E_t1424), -1, 0, 0,
	sizeof (Locale_t1425), -1, 0, 0,
	sizeof (BigInteger_t1428), -1, sizeof(BigInteger_t1428_StaticFields), 0,
	sizeof (Sign_t1426)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ModulusRing_t1427), -1, 0, 0,
	sizeof (Kernel_t1429), -1, 0, 0,
	sizeof (ConfidenceFactor_t1430)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (PrimalityTests_t1431), -1, 0, 0,
	sizeof (PrimeGeneratorBase_t1432), -1, 0, 0,
	sizeof (SequentialSearchPrimeGeneratorBase_t1433), -1, 0, 0,
	sizeof (ASN1_t1434), -1, 0, 0,
	sizeof (ASN1Convert_t1435), -1, 0, 0,
	sizeof (BitConverterLE_t1436), -1, 0, 0,
	sizeof (PKCS7_t1439), -1, 0, 0,
	sizeof (ContentInfo_t1437), -1, 0, 0,
	sizeof (EncryptedData_t1438), -1, 0, 0,
	sizeof (ARC4Managed_t1440), -1, 0, 0,
	sizeof (CryptoConvert_t1442), -1, 0, 0,
	sizeof (KeyBuilder_t1443), -1, sizeof(KeyBuilder_t1443_StaticFields), 0,
	sizeof (MD2_t1444), -1, 0, 0,
	sizeof (MD2Managed_t1446), -1, sizeof(MD2Managed_t1446_StaticFields), 0,
	sizeof (PKCS1_t1447), -1, sizeof(PKCS1_t1447_StaticFields), 0,
	sizeof (PKCS8_t1450), -1, 0, 0,
	sizeof (PrivateKeyInfo_t1448), -1, 0, 0,
	sizeof (EncryptedPrivateKeyInfo_t1449), -1, 0, 0,
	sizeof (RC4_t1441), -1, sizeof(RC4_t1441_StaticFields), 0,
	sizeof (RSAManaged_t1453), -1, 0, 0,
	sizeof (KeyGeneratedEventHandler_t1451), sizeof(methodPointerType), 0, 0,
	sizeof (SafeBag_t1455), -1, 0, 0,
	sizeof (PKCS12_t1457), -1, sizeof(PKCS12_t1457_StaticFields), 0,
	sizeof (DeriveBytes_t1456), -1, sizeof(DeriveBytes_t1456_StaticFields), 0,
	sizeof (X501_t1459), -1, sizeof(X501_t1459_StaticFields), 0,
	sizeof (X509Certificate_t1460), -1, sizeof(X509Certificate_t1460_StaticFields), 0,
	sizeof (X509CertificateCollection_t1458), -1, 0, 0,
	sizeof (X509CertificateEnumerator_t1463), -1, 0, 0,
	sizeof (X509Chain_t1465), -1, 0, 0,
	sizeof (X509ChainStatusFlags_t1466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (X509Crl_t1468), -1, sizeof(X509Crl_t1468_StaticFields), 0,
	sizeof (X509CrlEntry_t1467), -1, 0, 0,
	sizeof (X509Extension_t1469), -1, 0, 0,
	sizeof (X509ExtensionCollection_t1462), -1, 0, 0,
	sizeof (X509Store_t1470), -1, 0, 0,
	sizeof (X509StoreManager_t1471), -1, sizeof(X509StoreManager_t1471_StaticFields), 0,
	sizeof (X509Stores_t1472), -1, 0, 0,
	sizeof (AuthorityKeyIdentifierExtension_t1473), -1, 0, 0,
	sizeof (BasicConstraintsExtension_t1474), -1, 0, 0,
	sizeof (ExtendedKeyUsageExtension_t1475), -1, sizeof(ExtendedKeyUsageExtension_t1475_StaticFields), 0,
	sizeof (GeneralNames_t1476), -1, 0, 0,
	sizeof (KeyUsages_t1477)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (KeyUsageExtension_t1478), -1, 0, 0,
	sizeof (NetscapeCertTypeExtension_t1480), -1, 0, 0,
	sizeof (CertTypes_t1479)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SubjectAltNameExtension_t1481), -1, 0, 0,
	sizeof (HMAC_t1482), -1, 0, 0,
	sizeof (MD5SHA1_t1484), -1, 0, 0,
	sizeof (AlertLevel_t1485)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (AlertDescription_t1486)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (Alert_t1487), -1, 0, 0,
	sizeof (CipherAlgorithmType_t1488)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CipherSuite_t1489), -1, sizeof(CipherSuite_t1489_StaticFields), 0,
	sizeof (CipherSuiteCollection_t1491), -1, 0, 0,
	sizeof (CipherSuiteFactory_t1492), -1, 0, 0,
	sizeof (ClientContext_t1493), -1, 0, 0,
	sizeof (ClientRecordProtocol_t1495), -1, 0, 0,
	sizeof (ClientSessionInfo_t1497), -1, sizeof(ClientSessionInfo_t1497_StaticFields), 0,
	sizeof (ClientSessionCache_t1498), -1, sizeof(ClientSessionCache_t1498_StaticFields), 0,
	sizeof (ContentType_t1499)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (Context_t1490), -1, 0, 0,
	sizeof (ExchangeAlgorithmType_t1504)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (HandshakeState_t1505)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (HashAlgorithmType_t1506)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (HttpsClientStream_t1507), -1, sizeof(HttpsClientStream_t1507_StaticFields), 0,
	sizeof (RecordProtocol_t1496), -1, sizeof(RecordProtocol_t1496_StaticFields), 0,
	sizeof (ReceiveRecordAsyncResult_t1511), -1, 0, 0,
	sizeof (SendRecordAsyncResult_t1513), -1, 0, 0,
	sizeof (RSASslSignatureDeformatter_t1515), -1, sizeof(RSASslSignatureDeformatter_t1515_StaticFields), 0,
	sizeof (RSASslSignatureFormatter_t1517), -1, sizeof(RSASslSignatureFormatter_t1517_StaticFields), 0,
	sizeof (SecurityCompressionType_t1519)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SecurityParameters_t1502), -1, 0, 0,
	sizeof (SecurityProtocolType_t1520)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ServerContext_t1521), -1, 0, 0,
	sizeof (ValidationResult_t1522), -1, 0, 0,
	sizeof (SslClientStream_t1494), -1, 0, 0,
	sizeof (SslCipherSuite_t1526), -1, 0, 0,
	sizeof (SslHandshakeHash_t1527), -1, 0, 0,
	sizeof (SslStreamBase_t1523), -1, sizeof(SslStreamBase_t1523_StaticFields), 0,
	sizeof (InternalAsyncResult_t1528), -1, 0, 0,
	sizeof (TlsCipherSuite_t1529), -1, 0, 0,
	sizeof (TlsClientSettings_t1501), -1, 0, 0,
	sizeof (TlsException_t1532), -1, 0, 0,
	sizeof (TlsServerSettings_t1500), -1, 0, 0,
	sizeof (TlsStream_t1503), -1, 0, 0,
	sizeof (ClientCertificateType_t1535)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (HandshakeMessage_t1514), -1, 0, 0,
	sizeof (HandshakeType_t1536)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (TlsClientCertificate_t1537), -1, 0, 0,
	sizeof (TlsClientCertificateVerify_t1538), -1, 0, 0,
	sizeof (TlsClientFinished_t1539), -1, sizeof(TlsClientFinished_t1539_StaticFields), 0,
	sizeof (TlsClientHello_t1540), -1, 0, 0,
	sizeof (TlsClientKeyExchange_t1541), -1, 0, 0,
	sizeof (TlsServerCertificate_t1542), -1, 0, 0,
	sizeof (TlsServerCertificateRequest_t1543), -1, 0, 0,
	sizeof (TlsServerFinished_t1544), -1, sizeof(TlsServerFinished_t1544_StaticFields), 0,
	sizeof (TlsServerHello_t1545), -1, 0, 0,
	sizeof (TlsServerHelloDone_t1546), -1, 0, 0,
	sizeof (TlsServerKeyExchange_t1547), -1, 0, 0,
	sizeof (PrimalityTest_t1548), sizeof(methodPointerType), 0, 0,
	sizeof (CertificateValidationCallback_t1524), sizeof(methodPointerType), 0, 0,
	sizeof (CertificateValidationCallback2_t1525), sizeof(methodPointerType), 0, 0,
	sizeof (CertificateSelectionCallback_t1509), sizeof(methodPointerType), 0, 0,
	sizeof (PrivateKeySelectionCallback_t1510), sizeof(methodPointerType), 0, 0,
	sizeof (U3CPrivateImplementationDetailsU3E_t1559), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1559_StaticFields), 0,
	sizeof (U24ArrayTypeU243132_t1550)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t1550_marshaled), 0, 0,
	sizeof (U24ArrayTypeU24256_t1551)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1551_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2420_t1552)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t1552_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2432_t1553)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t1553_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2448_t1554)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t1554_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2464_t1555)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t1555_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2412_t1556)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t1556_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2416_t1557)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1557_marshaled), 0, 0,
	sizeof (U24ArrayTypeU244_t1558)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU244_t1558_marshaled), 0, 0,
	sizeof (U3CModuleU3E_t1598), -1, 0, 0,
	sizeof (Locale_t1599), -1, 0, 0,
	sizeof (MonoTODOAttribute_t1600), -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (HybridDictionary_t1601), -1, 0, 0,
	sizeof (ListDictionary_t1602), -1, 0, 0,
	sizeof (DictionaryNode_t1603), -1, 0, 0,
	sizeof (DictionaryNodeEnumerator_t1604), -1, 0, 0,
	sizeof (DictionaryNodeCollection_t1607), -1, 0, 0,
	sizeof (DictionaryNodeCollectionEnumerator_t1605), -1, 0, 0,
	sizeof (NameObjectCollectionBase_t1611), -1, 0, 0,
	sizeof (_Item_t1609), -1, 0, 0,
	sizeof (_KeysEnumerator_t1610), -1, 0, 0,
	sizeof (KeysCollection_t1612), -1, 0, 0,
	sizeof (NameValueCollection_t1615), -1, 0, 0,
	sizeof (EditorBrowsableAttribute_t1616), -1, 0, 0,
	sizeof (EditorBrowsableState_t1617)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TypeConverter_t1618), -1, 0, 0,
	sizeof (TypeConverterAttribute_t1619), -1, sizeof(TypeConverterAttribute_t1619_StaticFields), 0,
	sizeof (AuthenticationLevel_t1620)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SslPolicyErrors_t1621)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (AddressFamily_t1622)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DefaultCertificatePolicy_t1623), -1, 0, 0,
	sizeof (FileWebRequest_t1624), -1, 0, 0,
	sizeof (FileWebRequestCreator_t1627), -1, 0, 0,
	sizeof (FtpRequestCreator_t1628), -1, 0, 0,
	sizeof (FtpWebRequest_t1629), -1, sizeof(FtpWebRequest_t1629_StaticFields), 0,
	sizeof (GlobalProxySelection_t1630), -1, 0, 0,
	sizeof (HttpRequestCreator_t1631), -1, 0, 0,
	sizeof (HttpVersion_t1632), -1, sizeof(HttpVersion_t1632_StaticFields), 0,
	sizeof (HttpWebRequest_t1508), -1, sizeof(HttpWebRequest_t1508_StaticFields), 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (IPAddress_t1634), -1, sizeof(IPAddress_t1634_StaticFields), 0,
	sizeof (IPv6Address_t1636), -1, sizeof(IPv6Address_t1636_StaticFields), 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (SecurityProtocolType_t1637)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ServicePoint_t1584), -1, 0, 0,
	sizeof (ServicePointManager_t1575), -1, sizeof(ServicePointManager_t1575_StaticFields), 0,
	sizeof (SPKey_t1638), -1, 0, 0,
	sizeof (WebHeaderCollection_t1625), -1, sizeof(WebHeaderCollection_t1625_StaticFields), 0,
	sizeof (WebProxy_t1641), -1, 0, 0,
	sizeof (WebRequest_t1588), -1, sizeof(WebRequest_t1588_StaticFields), 0,
	sizeof (OpenFlags_t1644)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (PublicKey_t1645), -1, sizeof(PublicKey_t1645_StaticFields), 0,
	sizeof (StoreLocation_t1648)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (StoreName_t1649)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (X500DistinguishedName_t1650), -1, 0, 0,
	sizeof (X500DistinguishedNameFlags_t1651)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (X509BasicConstraintsExtension_t1652), -1, 0, 0,
	sizeof (X509Certificate2_t1586), -1, sizeof(X509Certificate2_t1586_StaticFields), 0,
	sizeof (X509Certificate2Collection_t1655), -1, 0, 0,
	sizeof (X509Certificate2Enumerator_t1656), -1, 0, 0,
	sizeof (X509CertificateCollection_t1530), -1, 0, 0,
	sizeof (X509CertificateEnumerator_t1592), -1, 0, 0,
	sizeof (X509Chain_t1587), -1, sizeof(X509Chain_t1587_StaticFields), 0,
	sizeof (X509ChainElement_t1660), -1, 0, 0,
	sizeof (X509ChainElementCollection_t1657), -1, 0, 0,
	sizeof (X509ChainElementEnumerator_t1663), -1, 0, 0,
	sizeof (X509ChainPolicy_t1658), -1, 0, 0,
	sizeof (X509ChainStatus_t1662)+ sizeof (Il2CppObject), sizeof(X509ChainStatus_t1662_marshaled), 0, 0,
	sizeof (X509ChainStatusFlags_t1665)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (X509EnhancedKeyUsageExtension_t1666), -1, sizeof(X509EnhancedKeyUsageExtension_t1666_StaticFields), 0,
	sizeof (X509Extension_t1653), -1, 0, 0,
	sizeof (X509ExtensionCollection_t1654), -1, 0, 0,
	sizeof (X509ExtensionEnumerator_t1667), -1, 0, 0,
	sizeof (X509FindType_t1668)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (X509KeyUsageExtension_t1669), -1, 0, 0,
	sizeof (X509KeyUsageFlags_t1670)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (X509NameType_t1671)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (X509RevocationFlag_t1672)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (X509RevocationMode_t1673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (X509Store_t1661), -1, sizeof(X509Store_t1661_StaticFields), 0,
	sizeof (X509SubjectKeyIdentifierExtension_t1674), -1, 0, 0,
	sizeof (X509SubjectKeyIdentifierHashAlgorithm_t1675)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (X509VerificationFlags_t1676)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (AsnDecodeStatus_t1677)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (AsnEncodedData_t1646), -1, sizeof(AsnEncodedData_t1646_StaticFields), 0,
	sizeof (Oid_t1647), -1, sizeof(Oid_t1647_StaticFields), 0,
	sizeof (OidCollection_t1664), -1, 0, 0,
	sizeof (OidEnumerator_t1678), -1, 0, 0,
	sizeof (BaseMachine_t1680), -1, 0, 0,
	sizeof (MatchAppendEvaluator_t1679), sizeof(methodPointerType), 0, 0,
	sizeof (Capture_t1681), -1, 0, 0,
	sizeof (CaptureCollection_t1682), -1, 0, 0,
	sizeof (Group_t1597), -1, sizeof(Group_t1597_StaticFields), 0,
	sizeof (GroupCollection_t1596), -1, 0, 0,
	sizeof (Match_t1595), -1, sizeof(Match_t1595_StaticFields), 0,
	sizeof (MatchCollection_t1594), -1, 0, 0,
	sizeof (Enumerator_t1686), -1, 0, 0,
	sizeof (Regex_t1036), -1, sizeof(Regex_t1036_StaticFields), 0,
	sizeof (RegexOptions_t1689)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (OpCode_t1690)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0,
	sizeof (OpFlags_t1691)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0,
	sizeof (Position_t1692)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (FactoryCache_t1687), -1, 0, 0,
	sizeof (Key_t1693), -1, 0, 0,
	sizeof (MRUList_t1694), -1, 0, 0,
	sizeof (Node_t1695), -1, 0, 0,
	sizeof (Category_t1696)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0,
	sizeof (CategoryUtils_t1697), -1, 0, 0,
	sizeof (LinkRef_t1698), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (InterpreterFactory_t1699), -1, 0, 0,
	sizeof (PatternCompiler_t1703), -1, 0, 0,
	sizeof (PatternLinkStack_t1701), -1, 0, 0,
	sizeof (Link_t1700)+ sizeof (Il2CppObject), sizeof(Link_t1700 ), 0, 0,
	sizeof (LinkStack_t1702), -1, 0, 0,
	sizeof (Mark_t1704)+ sizeof (Il2CppObject), sizeof(Mark_t1704 ), 0, 0,
	sizeof (Interpreter_t1708), -1, 0, 0,
	sizeof (IntStack_t1705)+ sizeof (Il2CppObject), sizeof(IntStack_t1705_marshaled), 0, 0,
	sizeof (RepeatContext_t1706), -1, 0, 0,
	sizeof (Mode_t1707)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Interval_t1711)+ sizeof (Il2CppObject), sizeof(Interval_t1711_marshaled), 0, 0,
	sizeof (IntervalCollection_t1714), -1, 0, 0,
	sizeof (Enumerator_t1712), -1, 0, 0,
	sizeof (CostDelegate_t1713), sizeof(methodPointerType), 0, 0,
	sizeof (Parser_t1715), -1, 0, 0,
	sizeof (QuickSearch_t1709), -1, sizeof(QuickSearch_t1709_StaticFields), 0,
	sizeof (ReplacementEvaluator_t1716), -1, 0, 0,
	sizeof (ExpressionCollection_t1717), -1, 0, 0,
	sizeof (Expression_t1718), -1, 0, 0,
	sizeof (CompositeExpression_t1719), -1, 0, 0,
	sizeof (Group_t1720), -1, 0, 0,
	sizeof (RegularExpression_t1721), -1, 0, 0,
	sizeof (CapturingGroup_t1722), -1, 0, 0,
	sizeof (BalancingGroup_t1723), -1, 0, 0,
	sizeof (NonBacktrackingGroup_t1724), -1, 0, 0,
	sizeof (Repetition_t1725), -1, 0, 0,
	sizeof (Assertion_t1726), -1, 0, 0,
	sizeof (CaptureAssertion_t1727), -1, 0, 0,
	sizeof (ExpressionAssertion_t1728), -1, 0, 0,
	sizeof (Alternation_t1730), -1, 0, 0,
	sizeof (Literal_t1729), -1, 0, 0,
	sizeof (PositionAssertion_t1731), -1, 0, 0,
	sizeof (Reference_t1732), -1, 0, 0,
	sizeof (BackslashNumber_t1733), -1, 0, 0,
	sizeof (CharacterClass_t1734), -1, sizeof(CharacterClass_t1734_StaticFields), 0,
	sizeof (AnchorInfo_t1736), -1, 0, 0,
	sizeof (DefaultUriParser_t1737), -1, 0, 0,
	sizeof (GenericUriParser_t1739), -1, 0, 0,
	sizeof (Uri_t1583), -1, sizeof(Uri_t1583_StaticFields), 0,
	sizeof (UriScheme_t1740)+ sizeof (Il2CppObject), sizeof(UriScheme_t1740_marshaled), 0, 0,
	sizeof (UriFormatException_t1742), -1, 0, 0,
	sizeof (UriHostNameType_t1743)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (UriKind_t1744)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (UriParser_t1738), -1, sizeof(UriParser_t1738_StaticFields), 0,
	sizeof (UriPartial_t1745)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (UriTypeConverter_t1746), -1, 0, 0,
	sizeof (RemoteCertificateValidationCallback_t1585), sizeof(methodPointerType), 0, 0,
	sizeof (MatchEvaluator_t1747), sizeof(methodPointerType), 0, 0,
	sizeof (U3CPrivateImplementationDetailsU3E_t1750), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1750_StaticFields), 0,
	sizeof (U24ArrayTypeU24128_t1748)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t1748_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2412_t1749)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t1749_marshaled), 0, 0,
	sizeof (U3CModuleU3E_t1762), -1, 0, 0,
	sizeof (Object_t), -1, 0, 0,
	sizeof (ValueType_t1763), -1, 0, 0,
	sizeof (Attribute_t463), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (Int32_t372)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, 0, 0, 0,
	sizeof (SerializableAttribute_t1764), -1, 0, 0,
	sizeof (AttributeUsageAttribute_t1765), -1, 0, 0,
	sizeof (ComVisibleAttribute_t1766), -1, 0, 0,
	0, 0, 0, 0,
	sizeof (Int64_t386)+ sizeof (Il2CppObject), sizeof(int64_t), 0, 0,
	sizeof (UInt32_t389)+ sizeof (Il2CppObject), sizeof(uint32_t), 0, 0,
	sizeof (CLSCompliantAttribute_t1767), -1, 0, 0,
	sizeof (UInt64_t394)+ sizeof (Il2CppObject), sizeof(uint64_t), 0, 0,
	sizeof (Byte_t391)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (SByte_t390)+ sizeof (Il2CppObject), sizeof(int8_t), 0, 0,
	sizeof (Int16_t392)+ sizeof (Il2CppObject), sizeof(int16_t), 0, 0,
	sizeof (UInt16_t393)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, 0, 0, 0,
	sizeof (Char_t383)+ sizeof (Il2CppObject), 1, sizeof(Char_t383_StaticFields), 0,
	sizeof (String_t), sizeof(char*), sizeof(String_t_StaticFields), 0,
	0, -1, 0, 0,
	0, 0, 0, 0,
	sizeof (Single_t388)+ sizeof (Il2CppObject), sizeof(float), 0, 0,
	sizeof (Double_t387)+ sizeof (Il2CppObject), sizeof(double), 0, 0,
	sizeof (Decimal_t395)+ sizeof (Il2CppObject), sizeof(Decimal_t395 ), sizeof(Decimal_t395_StaticFields), 0,
	sizeof (Boolean_t384)+ sizeof (Il2CppObject), 4, sizeof(Boolean_t384_StaticFields), 0,
	sizeof (IntPtr_t)+ sizeof (Il2CppObject), sizeof(IntPtr_t), sizeof(IntPtr_t_StaticFields), 0,
	0, -1, 0, 0,
	sizeof (UIntPtr_t)+ sizeof (Il2CppObject), sizeof(UIntPtr_t ), sizeof(UIntPtr_t_StaticFields), 0,
	sizeof (MulticastDelegate_t28), -1, 0, 0,
	sizeof (Delegate_t466), -1, 0, 0,
	sizeof (Enum_t21), -1, sizeof(Enum_t21_StaticFields), 0,
	sizeof (Array_t), -1, 0, 0,
	0, 0, 0, 0,
	sizeof (SimpleEnumerator_t1769), -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (Swapper_t1770), sizeof(methodPointerType), 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (Void_t1771)+ sizeof (Il2CppObject), 1, 0, 0,
	sizeof (Type_t), -1, sizeof(Type_t_StaticFields), 0,
	sizeof (MemberInfo_t), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (Exception_t359), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (RuntimeFieldHandle_t1774)+ sizeof (Il2CppObject), sizeof(RuntimeFieldHandle_t1774 ), 0, 0,
	sizeof (RuntimeTypeHandle_t1772)+ sizeof (Il2CppObject), sizeof(RuntimeTypeHandle_t1772 ), 0, 0,
	sizeof (ParamArrayAttribute_t1775), -1, 0, 0,
	sizeof (OutAttribute_t1776), -1, 0, 0,
	sizeof (ObsoleteAttribute_t1777), -1, 0, 0,
	sizeof (DllImportAttribute_t1778), -1, 0, 0,
	sizeof (MarshalAsAttribute_t1779), -1, 0, 0,
	sizeof (InAttribute_t1780), -1, 0, 0,
	sizeof (GuidAttribute_t1781), -1, 0, 0,
	sizeof (ComImportAttribute_t1782), -1, 0, 0,
	sizeof (OptionalAttribute_t1783), -1, 0, 0,
	sizeof (CompilerGeneratedAttribute_t1784), -1, 0, 0,
	sizeof (InternalsVisibleToAttribute_t1785), -1, 0, 0,
	sizeof (RuntimeCompatibilityAttribute_t1786), -1, 0, 0,
	sizeof (DebuggerHiddenAttribute_t1787), -1, 0, 0,
	sizeof (DefaultMemberAttribute_t1788), -1, 0, 0,
	sizeof (DecimalConstantAttribute_t1789), -1, 0, 0,
	sizeof (FieldOffsetAttribute_t1790), -1, 0, 0,
	sizeof (RuntimeArgumentHandle_t1791)+ sizeof (Il2CppObject), sizeof(RuntimeArgumentHandle_t1791 ), 0, 0,
	sizeof (AsyncCallback_t31), sizeof(methodPointerType), 0, 0,
	0, -1, 0, 0,
	sizeof (TypedReference_t1792)+ sizeof (Il2CppObject), sizeof(TypedReference_t1792 ), 0, 0,
	sizeof (ArgIterator_t1793)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (MarshalByRefObject_t1643), -1, 0, 0,
	0, 0, 0, 0,
	sizeof (RuntimeHelpers_t1795), -1, 0, 0,
	sizeof (Locale_t1796), -1, 0, 0,
	sizeof (MonoTODOAttribute_t1797), -1, 0, 0,
	sizeof (MonoDocumentationNoteAttribute_t1798), -1, 0, 0,
	sizeof (SafeHandleZeroOrMinusOneIsInvalid_t1799), sizeof(void*), 0, 0,
	sizeof (SafeWaitHandle_t1801), sizeof(void*), 0, 0,
	sizeof (CodePointIndexer_t1803), -1, 0, 0,
	sizeof (TableRange_t1802)+ sizeof (Il2CppObject), sizeof(TableRange_t1802 ), 0, 0,
	sizeof (TailoringInfo_t1805), -1, 0, 0,
	sizeof (Contraction_t1806), -1, 0, 0,
	sizeof (ContractionComparer_t1807), -1, sizeof(ContractionComparer_t1807_StaticFields), 0,
	sizeof (Level2Map_t1808), -1, 0, 0,
	sizeof (Level2MapComparer_t1809), -1, sizeof(Level2MapComparer_t1809_StaticFields), 0,
	sizeof (MSCompatUnicodeTable_t1810), -1, sizeof(MSCompatUnicodeTable_t1810_StaticFields), 0,
	sizeof (MSCompatUnicodeTableUtil_t1812), -1, sizeof(MSCompatUnicodeTableUtil_t1812_StaticFields), 0,
	sizeof (SimpleCollator_t1817), -1, sizeof(SimpleCollator_t1817_StaticFields), 0,
	sizeof (Context_t1813)+ sizeof (Il2CppObject), sizeof(Context_t1813_marshaled), 0, 0,
	sizeof (PreviousInfo_t1814)+ sizeof (Il2CppObject), sizeof(PreviousInfo_t1814_marshaled), 0, 0,
	sizeof (Escape_t1815)+ sizeof (Il2CppObject), sizeof(Escape_t1815_marshaled), 0, 0,
	sizeof (ExtenderType_t1816)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SortKey_t1821), -1, 0, 0,
	sizeof (SortKeyBuffer_t1822), -1, 0, 0,
	sizeof (PrimeGeneratorBase_t1823), -1, 0, 0,
	sizeof (SequentialSearchPrimeGeneratorBase_t1824), -1, 0, 0,
	sizeof (ConfidenceFactor_t1825)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (PrimalityTests_t1826), -1, 0, 0,
	sizeof (BigInteger_t1829), -1, sizeof(BigInteger_t1829_StaticFields), 0,
	sizeof (Sign_t1827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ModulusRing_t1828), -1, 0, 0,
	sizeof (Kernel_t1830), -1, 0, 0,
	sizeof (CryptoConvert_t1831), -1, 0, 0,
	sizeof (KeyBuilder_t1832), -1, sizeof(KeyBuilder_t1832_StaticFields), 0,
	sizeof (BlockProcessor_t1833), -1, 0, 0,
	sizeof (DSAManaged_t1835), -1, 0, 0,
	sizeof (KeyGeneratedEventHandler_t1834), sizeof(methodPointerType), 0, 0,
	sizeof (KeyPairPersistence_t1836), -1, sizeof(KeyPairPersistence_t1836_StaticFields), 0,
	sizeof (MACAlgorithm_t1837), -1, 0, 0,
	sizeof (PKCS1_t1838), -1, sizeof(PKCS1_t1838_StaticFields), 0,
	sizeof (PKCS8_t1841), -1, 0, 0,
	sizeof (PrivateKeyInfo_t1839), -1, 0, 0,
	sizeof (EncryptedPrivateKeyInfo_t1840), -1, 0, 0,
	sizeof (RSAManaged_t1843), -1, 0, 0,
	sizeof (KeyGeneratedEventHandler_t1842), sizeof(methodPointerType), 0, 0,
	sizeof (SymmetricTransform_t1844), -1, 0, 0,
	sizeof (SafeBag_t1845), -1, 0, 0,
	sizeof (PKCS12_t1848), -1, sizeof(PKCS12_t1848_StaticFields), 0,
	sizeof (DeriveBytes_t1847), -1, sizeof(DeriveBytes_t1847_StaticFields), 0,
	sizeof (X501_t1850), -1, sizeof(X501_t1850_StaticFields), 0,
	sizeof (X509Certificate_t1851), -1, sizeof(X509Certificate_t1851_StaticFields), 0,
	sizeof (X509CertificateCollection_t1849), -1, 0, 0,
	sizeof (X509CertificateEnumerator_t1853), -1, 0, 0,
	sizeof (X509Extension_t1854), -1, 0, 0,
	sizeof (X509ExtensionCollection_t1852), -1, 0, 0,
	sizeof (ASN1_t1846), -1, 0, 0,
	sizeof (ASN1Convert_t1855), -1, 0, 0,
	sizeof (BitConverterLE_t1856), -1, 0, 0,
	sizeof (PKCS7_t1859), -1, 0, 0,
	sizeof (ContentInfo_t1857), -1, 0, 0,
	sizeof (EncryptedData_t1858), -1, 0, 0,
	sizeof (StrongName_t1860), -1, sizeof(StrongName_t1860_StaticFields), 0,
	sizeof (SecurityParser_t1861), -1, 0, 0,
	sizeof (SmallXmlParser_t1862), -1, sizeof(SmallXmlParser_t1862_StaticFields), 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (AttrListImpl_t1864), -1, 0, 0,
	sizeof (SmallXmlParserException_t1867), -1, 0, 0,
	sizeof (Runtime_t1868), -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (Link_t1869)+ sizeof (Il2CppObject), sizeof(Link_t1869 ), 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (KeyNotFoundException_t1870), -1, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (ArrayList_t450), -1, sizeof(ArrayList_t450_StaticFields), 0,
	sizeof (SimpleEnumerator_t1871), -1, sizeof(SimpleEnumerator_t1871_StaticFields), 0,
	sizeof (ArrayListWrapper_t1872), -1, 0, 0,
	sizeof (SynchronizedArrayListWrapper_t1873), -1, 0, 0,
	sizeof (FixedSizeArrayListWrapper_t1874), -1, 0, 0,
	sizeof (ReadOnlyArrayListWrapper_t1875), -1, 0, 0,
	sizeof (BitArray_t1735), -1, 0, 0,
	sizeof (BitArrayEnumerator_t1876), -1, 0, 0,
	sizeof (CaseInsensitiveComparer_t1753), -1, sizeof(CaseInsensitiveComparer_t1753_StaticFields), 0,
	sizeof (CaseInsensitiveHashCodeProvider_t1754), -1, sizeof(CaseInsensitiveHashCodeProvider_t1754_StaticFields), 0,
	sizeof (CollectionBase_t1464), -1, 0, 0,
	sizeof (CollectionDebuggerView_t1877), -1, 0, 0,
	sizeof (Comparer_t1878), -1, sizeof(Comparer_t1878_StaticFields), 0,
	sizeof (DictionaryEntry_t451)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (Hashtable_t261), -1, sizeof(Hashtable_t261_StaticFields), 0,
	sizeof (Slot_t1879)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (KeyMarker_t1880), -1, sizeof(KeyMarker_t1880_StaticFields), 0,
	sizeof (EnumeratorMode_t1881)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Enumerator_t1882), -1, sizeof(Enumerator_t1882_StaticFields), 0,
	sizeof (HashKeys_t1883), -1, 0, 0,
	sizeof (HashValues_t1884), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (Queue_t1887), -1, 0, 0,
	sizeof (QueueEnumerator_t1886), -1, 0, 0,
	sizeof (SortedList_t1757), -1, sizeof(SortedList_t1757_StaticFields), 0,
	sizeof (Slot_t1888)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (EnumeratorMode_t1889)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Enumerator_t1890), -1, sizeof(Enumerator_t1890_StaticFields), 0,
	sizeof (ListKeys_t1891), -1, 0, 0,
	sizeof (Stack_t995), -1, 0, 0,
	sizeof (Enumerator_t1893), -1, 0, 0,
	sizeof (AssemblyHashAlgorithm_t1894)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (AssemblyVersionCompatibility_t1895)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DebuggableAttribute_t1897), -1, 0, 0,
	sizeof (DebuggingModes_t1896)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DebuggerDisplayAttribute_t1898), -1, 0, 0,
	sizeof (DebuggerStepThroughAttribute_t1899), -1, 0, 0,
	sizeof (DebuggerTypeProxyAttribute_t1900), -1, 0, 0,
	sizeof (StackFrame_t1032), -1, 0, 0,
	sizeof (StackTrace_t1011), -1, 0, 0,
	sizeof (Calendar_t1902), -1, 0, 0,
	sizeof (CCMath_t1903), -1, 0, 0,
	sizeof (CCFixed_t1904), -1, 0, 0,
	sizeof (CCGregorianCalendar_t1905), -1, 0, 0,
	sizeof (CompareInfo_t1582), -1, sizeof(CompareInfo_t1582_StaticFields), 0,
	sizeof (CompareOptions_t1906)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CultureInfo_t1031), -1, sizeof(CultureInfo_t1031_StaticFields), 0,
	sizeof (DateTimeFormatFlags_t1910)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DateTimeFormatInfo_t1908), -1, sizeof(DateTimeFormatInfo_t1908_StaticFields), 0,
	sizeof (DateTimeStyles_t1911)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DaylightTime_t1912), -1, 0, 0,
	sizeof (GregorianCalendar_t1913), -1, 0, 0,
	sizeof (GregorianCalendarTypes_t1914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (NumberFormatInfo_t1907), -1, sizeof(NumberFormatInfo_t1907_StaticFields), 0,
	sizeof (NumberStyles_t1915)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TextInfo_t1818), -1, 0, 0,
	sizeof (Data_t1916)+ sizeof (Il2CppObject), sizeof(Data_t1916 ), 0, 0,
	sizeof (UnicodeCategory_t1917)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (IsolatedStorageException_t1918), -1, 0, 0,
	sizeof (BinaryReader_t1919), -1, 0, 0,
	sizeof (Directory_t1921), -1, 0, 0,
	sizeof (DirectoryInfo_t1922), -1, 0, 0,
	sizeof (DirectoryNotFoundException_t1924), -1, 0, 0,
	sizeof (EndOfStreamException_t1925), -1, 0, 0,
	sizeof (File_t1926), -1, 0, 0,
	sizeof (FileAccess_t1756)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FileAttributes_t1927)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FileMode_t1928)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FileNotFoundException_t1929), -1, 0, 0,
	sizeof (FileOptions_t1930)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FileShare_t1931)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FileStream_t1576), -1, 0, 0,
	sizeof (ReadDelegate_t1932), sizeof(methodPointerType), 0, 0,
	sizeof (WriteDelegate_t1933), sizeof(methodPointerType), 0, 0,
	sizeof (FileStreamAsyncResult_t1934), -1, 0, 0,
	sizeof (FileSystemInfo_t1923), -1, 0, 0,
	sizeof (IOException_t1591), -1, 0, 0,
	sizeof (MemoryStream_t416), -1, 0, 0,
	sizeof (MonoFileType_t1936)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (MonoIO_t1937), -1, sizeof(MonoIO_t1937_StaticFields), 0,
	sizeof (MonoIOError_t1938)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (MonoIOStat_t1935)+ sizeof (Il2CppObject), sizeof(MonoIOStat_t1935_marshaled), 0, 0,
	sizeof (Path_t1380), -1, sizeof(Path_t1380_StaticFields), 0,
	sizeof (PathTooLongException_t1939), -1, 0, 0,
	sizeof (SearchPattern_t1940), -1, sizeof(SearchPattern_t1940_StaticFields), 0,
	sizeof (SeekOrigin_t1941)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Stream_t1512), -1, sizeof(Stream_t1512_StaticFields), 0,
	sizeof (NullStream_t1942), -1, 0, 0,
	sizeof (StreamAsyncResult_t1943), -1, 0, 0,
	sizeof (StreamReader_t370), -1, sizeof(StreamReader_t370_StaticFields), 0,
	sizeof (NullStreamReader_t1944), -1, 0, 0,
	sizeof (StreamWriter_t396), -1, sizeof(StreamWriter_t396_StaticFields), 0,
	sizeof (StringReader_t67), -1, 0, 0,
	sizeof (TextReader_t1865), -1, sizeof(TextReader_t1865_StaticFields), 0,
	sizeof (NullTextReader_t1945), -1, 0, 0,
	sizeof (SynchronizedReader_t1946), -1, 0, 0,
	sizeof (TextWriter_t1761), -1, sizeof(TextWriter_t1761_StaticFields), 0,
	sizeof (NullTextWriter_t1947), -1, 0, 0,
	sizeof (SynchronizedWriter_t1948), -1, 0, 0,
	sizeof (UnexceptionalStreamReader_t1949), -1, sizeof(UnexceptionalStreamReader_t1949_StaticFields), 0,
	sizeof (UnexceptionalStreamWriter_t1950), -1, 0, 0,
	sizeof (AssemblyBuilder_t1951), -1, 0, 0,
	sizeof (ConstructorBuilder_t1954), -1, 0, 0,
	sizeof (EnumBuilder_t1959), -1, 0, 0,
	sizeof (FieldBuilder_t1960), -1, 0, 0,
	sizeof (GenericTypeParameterBuilder_t1962), -1, 0, 0,
	sizeof (ILTokenInfo_t1964)+ sizeof (Il2CppObject), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (ILGenerator_t1955), -1, sizeof(ILGenerator_t1955_StaticFields), 0,
	sizeof (LabelFixup_t1965)+ sizeof (Il2CppObject), sizeof(LabelFixup_t1965 ), 0, 0,
	sizeof (LabelData_t1966)+ sizeof (Il2CppObject), sizeof(LabelData_t1966 ), 0, 0,
	sizeof (MethodBuilder_t1963), -1, 0, 0,
	sizeof (MethodToken_t1973)+ sizeof (Il2CppObject), sizeof(MethodToken_t1973 ), sizeof(MethodToken_t1973_StaticFields), 0,
	sizeof (ModuleBuilder_t1974), -1, sizeof(ModuleBuilder_t1974_StaticFields), 0,
	sizeof (ModuleBuilderTokenGenerator_t1976), -1, 0, 0,
	sizeof (OpCode_t1977)+ sizeof (Il2CppObject), sizeof(OpCode_t1977 ), 0, 0,
	sizeof (OpCodeNames_t1978), -1, sizeof(OpCodeNames_t1978_StaticFields), 0,
	sizeof (OpCodes_t1979), -1, sizeof(OpCodes_t1979_StaticFields), 0,
	sizeof (ParameterBuilder_t1980), -1, 0, 0,
	sizeof (StackBehaviour_t1981)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TypeBuilder_t1956), -1, 0, 0,
	sizeof (UnmanagedMarshal_t1961), -1, 0, 0,
	sizeof (AmbiguousMatchException_t1985), -1, 0, 0,
	sizeof (Assembly_t1758), -1, 0, 0,
	sizeof (ResolveEventHolder_t1986), -1, 0, 0,
	sizeof (AssemblyCompanyAttribute_t1989), -1, 0, 0,
	sizeof (AssemblyConfigurationAttribute_t1990), -1, 0, 0,
	sizeof (AssemblyCopyrightAttribute_t1991), -1, 0, 0,
	sizeof (AssemblyDefaultAliasAttribute_t1992), -1, 0, 0,
	sizeof (AssemblyDelaySignAttribute_t1993), -1, 0, 0,
	sizeof (AssemblyDescriptionAttribute_t1994), -1, 0, 0,
	sizeof (AssemblyFileVersionAttribute_t1995), -1, 0, 0,
	sizeof (AssemblyInformationalVersionAttribute_t1996), -1, 0, 0,
	sizeof (AssemblyKeyFileAttribute_t1997), -1, 0, 0,
	sizeof (AssemblyName_t1998), -1, 0, 0,
	sizeof (AssemblyNameFlags_t2000)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (AssemblyProductAttribute_t2001), -1, 0, 0,
	sizeof (AssemblyTitleAttribute_t2002), -1, 0, 0,
	sizeof (AssemblyTrademarkAttribute_t2003), -1, 0, 0,
	sizeof (Binder_t1029), -1, sizeof(Binder_t1029_StaticFields), 0,
	sizeof (Default_t2004), -1, 0, 0,
	sizeof (BindingFlags_t2005)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CallingConventions_t2006)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ConstructorInfo_t1042), -1, sizeof(ConstructorInfo_t1042_StaticFields), 0,
	sizeof (EventAttributes_t2007)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (EventInfo_t), -1, 0, 0,
	sizeof (AddEventAdapter_t2008), sizeof(methodPointerType), 0, 0,
	sizeof (FieldAttributes_t2009)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FieldInfo_t), -1, 0, 0,
	sizeof (MemberInfoSerializationHolder_t2010), -1, 0, 0,
	sizeof (MemberTypes_t2011)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (MethodAttributes_t2012)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (MethodBase_t1033), -1, 0, 0,
	sizeof (MethodImplAttributes_t2013)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (MethodInfo_t), -1, 0, 0,
	sizeof (Missing_t2014), -1, sizeof(Missing_t2014_StaticFields), 0,
	sizeof (Module_t1970), -1, sizeof(Module_t1970_StaticFields), 0,
	sizeof (MonoEventInfo_t2016)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (MonoEvent_t), -1, 0, 0,
	sizeof (MonoField_t), -1, 0, 0,
	sizeof (MonoGenericMethod_t), -1, 0, 0,
	sizeof (MonoGenericCMethod_t2017), -1, 0, 0,
	sizeof (MonoMethodInfo_t2019)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (MonoMethod_t), -1, 0, 0,
	sizeof (MonoCMethod_t2018), -1, 0, 0,
	sizeof (MonoPropertyInfo_t2020)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (PInfo_t2021)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (MonoProperty_t), -1, 0, 0,
	sizeof (GetterAdapter_t2022), sizeof(methodPointerType), 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (ParameterAttributes_t2023)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ParameterInfo_t1035), -1, 0, 0,
	sizeof (ParameterModifier_t2024)+ sizeof (Il2CppObject), sizeof(ParameterModifier_t2024_marshaled), 0, 0,
	sizeof (Pointer_t2025), -1, 0, 0,
	sizeof (ProcessorArchitecture_t2026)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (PropertyAttributes_t2027)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (PropertyInfo_t), -1, 0, 0,
	sizeof (StrongNameKeyPair_t1999), -1, 0, 0,
	sizeof (TargetException_t2028), -1, 0, 0,
	sizeof (TargetInvocationException_t2029), -1, 0, 0,
	sizeof (TargetParameterCountException_t2030), -1, 0, 0,
	sizeof (TypeAttributes_t2031)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (NeutralResourcesLanguageAttribute_t2032), -1, 0, 0,
	sizeof (SatelliteContractVersionAttribute_t2033), -1, 0, 0,
	sizeof (CompilationRelaxations_t2034)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CompilationRelaxationsAttribute_t2035), -1, 0, 0,
	sizeof (DefaultDependencyAttribute_t2036), -1, 0, 0,
	sizeof (IsVolatile_t2037), -1, 0, 0,
	sizeof (LoadHint_t2038)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (StringFreezingAttribute_t2039), -1, 0, 0,
	sizeof (Cer_t2040)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (Consistency_t2041)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CriticalFinalizerObject_t2042), -1, 0, 0,
	sizeof (ReliabilityContractAttribute_t2043), -1, 0, 0,
	sizeof (ActivationArguments_t2044), -1, 0, 0,
	sizeof (CallingConvention_t2045)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CharSet_t2046)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ClassInterfaceAttribute_t2047), -1, 0, 0,
	sizeof (ClassInterfaceType_t2048)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ComDefaultInterfaceAttribute_t2049), -1, 0, 0,
	sizeof (ComInterfaceType_t2050)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DispIdAttribute_t2051), -1, 0, 0,
	sizeof (GCHandle_t1300)+ sizeof (Il2CppObject), sizeof(GCHandle_t1300 ), 0, 0,
	sizeof (GCHandleType_t2052)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (InterfaceTypeAttribute_t2053), -1, 0, 0,
	sizeof (Marshal_t1284), -1, sizeof(Marshal_t1284_StaticFields), 0,
	sizeof (MarshalDirectiveException_t2054), -1, 0, 0,
	sizeof (PreserveSigAttribute_t2055), -1, 0, 0,
	sizeof (SafeHandle_t1800), sizeof(void*), 0, 0,
	sizeof (TypeLibImportClassAttribute_t2056), -1, 0, 0,
	sizeof (TypeLibVersionAttribute_t2057), -1, 0, 0,
	sizeof (UnmanagedType_t2058)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (ActivationServices_t2059), -1, sizeof(ActivationServices_t2059_StaticFields), 0,
	sizeof (AppDomainLevelActivator_t2061), -1, 0, 0,
	sizeof (ConstructionLevelActivator_t2062), -1, 0, 0,
	sizeof (ContextLevelActivator_t2063), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (RemoteActivator_t2064), -1, 0, 0,
	sizeof (UrlAttribute_t2065), -1, 0, 0,
	sizeof (ChannelInfo_t2067), -1, 0, 0,
	sizeof (ChannelServices_t2068), -1, sizeof(ChannelServices_t2068_StaticFields), 0,
	sizeof (CrossAppDomainData_t2070), -1, 0, 0,
	sizeof (CrossAppDomainChannel_t2071), -1, sizeof(CrossAppDomainChannel_t2071_StaticFields), 0,
	sizeof (CrossAppDomainSink_t2072), -1, sizeof(CrossAppDomainSink_t2072_StaticFields), 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (SinkProviderData_t2073), -1, 0, 0,
	sizeof (Context_t2074), -1, sizeof(Context_t2074_StaticFields), 0,
	sizeof (DynamicPropertyCollection_t2075), -1, 0, 0,
	sizeof (DynamicPropertyReg_t2078), -1, 0, 0,
	sizeof (ContextCallbackObject_t2076), -1, 0, 0,
	sizeof (ContextAttribute_t2066), -1, 0, 0,
	sizeof (CrossContextChannel_t2069), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (SynchronizationAttribute_t2082), -1, 0, 0,
	sizeof (SynchronizedClientContextSink_t2084), -1, 0, 0,
	sizeof (SynchronizedServerContextSink_t2085), -1, 0, 0,
	0, -1, 0, 0,
	sizeof (Lease_t2088), -1, 0, 0,
	sizeof (RenewalDelegate_t2086), sizeof(methodPointerType), 0, 0,
	sizeof (LeaseManager_t2089), -1, 0, 0,
	sizeof (LeaseSink_t2091), -1, 0, 0,
	sizeof (LeaseState_t2092)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (LifetimeServices_t2093), -1, sizeof(LifetimeServices_t2093_StaticFields), 0,
	sizeof (ArgInfoType_t2094)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (ArgInfo_t2095), -1, 0, 0,
	sizeof (AsyncResult_t2096), -1, 0, 0,
	sizeof (ClientContextTerminatorSink_t2101), -1, 0, 0,
	sizeof (ConstructionCall_t2102), -1, sizeof(ConstructionCall_t2102_StaticFields), 0,
	sizeof (ConstructionCallDictionary_t2104), -1, sizeof(ConstructionCallDictionary_t2104_StaticFields), 0,
	sizeof (EnvoyTerminatorSink_t2106), -1, sizeof(EnvoyTerminatorSink_t2106_StaticFields), 0,
	sizeof (Header_t2107), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (LogicalCallContext_t2108), -1, 0, 0,
	sizeof (CallContextRemotingData_t2109), -1, 0, 0,
	sizeof (MethodCall_t2103), -1, sizeof(MethodCall_t2103_StaticFields), 0,
	sizeof (MethodCallDictionary_t2110), -1, sizeof(MethodCallDictionary_t2110_StaticFields), 0,
	sizeof (MethodDictionary_t2105), -1, sizeof(MethodDictionary_t2105_StaticFields), 0,
	sizeof (DictionaryEnumerator_t2111), -1, 0, 0,
	sizeof (MethodReturnDictionary_t2113), -1, sizeof(MethodReturnDictionary_t2113_StaticFields), 0,
	sizeof (MonoMethodMessage_t2098), -1, 0, 0,
	sizeof (RemotingSurrogate_t2114), -1, 0, 0,
	sizeof (ObjRefSurrogate_t2115), -1, 0, 0,
	sizeof (RemotingSurrogateSelector_t2116), -1, sizeof(RemotingSurrogateSelector_t2116_StaticFields), 0,
	sizeof (ReturnMessage_t2118), -1, 0, 0,
	sizeof (ServerContextTerminatorSink_t2119), -1, 0, 0,
	sizeof (ServerObjectTerminatorSink_t2120), -1, 0, 0,
	sizeof (StackBuilderSink_t2121), -1, 0, 0,
	sizeof (SoapAttribute_t2123), -1, 0, 0,
	sizeof (SoapFieldAttribute_t2124), -1, 0, 0,
	sizeof (SoapMethodAttribute_t2125), -1, 0, 0,
	sizeof (SoapParameterAttribute_t2126), -1, 0, 0,
	sizeof (SoapTypeAttribute_t2127), -1, 0, 0,
	sizeof (ProxyAttribute_t2128), -1, 0, 0,
	sizeof (TransparentProxy_t2129), -1, 0, 0,
	sizeof (RealProxy_t2122), -1, 0, 0,
	sizeof (RemotingProxy_t2131), -1, sizeof(RemotingProxy_t2131_StaticFields), 0,
	0, -1, 0, 0,
	sizeof (TrackingServices_t2132), -1, sizeof(TrackingServices_t2132_StaticFields), 0,
	sizeof (ActivatedClientTypeEntry_t2133), -1, 0, 0,
	sizeof (ActivatedServiceTypeEntry_t2135), -1, 0, 0,
	sizeof (EnvoyInfo_t2136), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (Identity_t2130), -1, 0, 0,
	sizeof (ClientIdentity_t2138), -1, 0, 0,
	sizeof (InternalRemotingServices_t2140), -1, sizeof(InternalRemotingServices_t2140_StaticFields), 0,
	sizeof (ObjRef_t2137), -1, sizeof(ObjRef_t2137_StaticFields), 0,
	sizeof (RemotingConfiguration_t2144), -1, sizeof(RemotingConfiguration_t2144_StaticFields), 0,
	sizeof (ConfigHandler_t2145), -1, sizeof(ConfigHandler_t2145_StaticFields), 0,
	sizeof (ChannelData_t2146), -1, 0, 0,
	sizeof (ProviderData_t2147), -1, 0, 0,
	sizeof (FormatterData_t2148), -1, 0, 0,
	sizeof (RemotingException_t2149), -1, 0, 0,
	sizeof (RemotingServices_t2150), -1, sizeof(RemotingServices_t2150_StaticFields), 0,
	sizeof (ServerIdentity_t1794), -1, 0, 0,
	sizeof (ClientActivatedIdentity_t2152), -1, 0, 0,
	sizeof (SingletonIdentity_t2153), -1, 0, 0,
	sizeof (SingleCallIdentity_t2154), -1, 0, 0,
	sizeof (SoapServices_t2156), -1, sizeof(SoapServices_t2156_StaticFields), 0,
	sizeof (TypeInfo_t2155), -1, 0, 0,
	sizeof (TypeEntry_t2134), -1, 0, 0,
	sizeof (TypeInfo_t2157), -1, 0, 0,
	sizeof (WellKnownClientTypeEntry_t2158), -1, 0, 0,
	sizeof (WellKnownObjectMode_t2159)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (WellKnownServiceTypeEntry_t2160), -1, 0, 0,
	sizeof (BinaryCommon_t2161), -1, sizeof(BinaryCommon_t2161_StaticFields), 0,
	sizeof (BinaryElement_t2162)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (TypeTag_t2163)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (MethodFlags_t2164)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ReturnTypeTag_t2165)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (BinaryFormatter_t2151), -1, sizeof(BinaryFormatter_t2151_StaticFields), 0,
	sizeof (MessageFormatter_t2167), -1, 0, 0,
	sizeof (ObjectReader_t2171), -1, 0, 0,
	sizeof (TypeMetadata_t2168), -1, 0, 0,
	sizeof (ArrayNullFiller_t2170), -1, 0, 0,
	sizeof (FormatterAssemblyStyle_t2173)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FormatterTypeStyle_t2174)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TypeFilterLevel_t2175)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (FormatterConverter_t2176), -1, 0, 0,
	sizeof (FormatterServices_t2177), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (ObjectManager_t2172), -1, 0, 0,
	sizeof (BaseFixupRecord_t2179), -1, 0, 0,
	sizeof (ArrayFixupRecord_t2180), -1, 0, 0,
	sizeof (MultiArrayFixupRecord_t2181), -1, 0, 0,
	sizeof (FixupRecord_t2182), -1, 0, 0,
	sizeof (DelayedFixupRecord_t2183), -1, 0, 0,
	sizeof (ObjectRecordStatus_t2184)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (ObjectRecord_t2178), -1, 0, 0,
	sizeof (OnDeserializedAttribute_t2186), -1, 0, 0,
	sizeof (OnDeserializingAttribute_t2187), -1, 0, 0,
	sizeof (OnSerializedAttribute_t2188), -1, 0, 0,
	sizeof (OnSerializingAttribute_t2189), -1, 0, 0,
	sizeof (SerializationBinder_t2166), -1, 0, 0,
	sizeof (SerializationCallbacks_t2191), -1, sizeof(SerializationCallbacks_t2191_StaticFields), 0,
	sizeof (CallbackHandler_t2190), sizeof(methodPointerType), 0, 0,
	sizeof (SerializationEntry_t2192)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (SerializationException_t1755), -1, 0, 0,
	sizeof (SerializationInfo_t1012), -1, 0, 0,
	sizeof (SerializationInfoEnumerator_t2194), -1, 0, 0,
	sizeof (StreamingContext_t1013)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (StreamingContextStates_t2195)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (X509Certificate_t1531), -1, 0, 0,
	sizeof (X509KeyStorageFlags_t2196)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (AsymmetricAlgorithm_t1549), -1, 0, 0,
	sizeof (AsymmetricKeyExchangeFormatter_t2197), -1, 0, 0,
	sizeof (AsymmetricSignatureDeformatter_t1516), -1, 0, 0,
	sizeof (AsymmetricSignatureFormatter_t1518), -1, 0, 0,
	sizeof (Base64Constants_t2198), -1, sizeof(Base64Constants_t2198_StaticFields), 0,
	sizeof (CipherMode_t1417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CryptoConfig_t1563), -1, sizeof(CryptoConfig_t1563_StaticFields), 0,
	sizeof (CryptoStream_t418), -1, 0, 0,
	sizeof (CryptoStreamMode_t2199)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (CryptographicException_t1416), -1, 0, 0,
	sizeof (CryptographicUnexpectedOperationException_t1577), -1, 0, 0,
	sizeof (CspParameters_t1565), -1, 0, 0,
	sizeof (CspProviderFlags_t2200)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DES_t1579), -1, sizeof(DES_t1579_StaticFields), 0,
	sizeof (DESTransform_t2202), -1, sizeof(DESTransform_t2202_StaticFields), 0,
	sizeof (DESCryptoServiceProvider_t2203), -1, 0, 0,
	sizeof (DSA_t1461), -1, 0, 0,
	sizeof (DSACryptoServiceProvider_t1569), -1, sizeof(DSACryptoServiceProvider_t1569_StaticFields), 0,
	sizeof (DSAParameters_t1561)+ sizeof (Il2CppObject), sizeof(DSAParameters_t1561_marshaled), 0, 0,
	sizeof (DSASignatureDeformatter_t1573), -1, 0, 0,
	sizeof (DSASignatureFormatter_t2204), -1, 0, 0,
	sizeof (DeriveBytes_t2205), -1, 0, 0,
	sizeof (HMAC_t1568), -1, 0, 0,
	sizeof (HMACMD5_t2206), -1, 0, 0,
	sizeof (HMACRIPEMD160_t2207), -1, 0, 0,
	sizeof (HMACSHA1_t1567), -1, 0, 0,
	sizeof (HMACSHA256_t2208), -1, 0, 0,
	sizeof (HMACSHA384_t2209), -1, sizeof(HMACSHA384_t2209_StaticFields), 0,
	sizeof (HMACSHA512_t2210), -1, sizeof(HMACSHA512_t2210_StaticFields), 0,
	sizeof (HashAlgorithm_t1445), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (KeySizes_t1423), -1, 0, 0,
	sizeof (KeyedHashAlgorithm_t1483), -1, 0, 0,
	sizeof (MACTripleDES_t2211), -1, 0, 0,
	sizeof (MD5_t1570), -1, 0, 0,
	sizeof (MD5CryptoServiceProvider_t2212), -1, sizeof(MD5CryptoServiceProvider_t2212_StaticFields), 0,
	sizeof (PaddingMode_t1421)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (PasswordDeriveBytes_t419), -1, 0, 0,
	sizeof (RC2_t1580), -1, 0, 0,
	sizeof (RC2CryptoServiceProvider_t2213), -1, 0, 0,
	sizeof (RC2Transform_t2214), -1, sizeof(RC2Transform_t2214_StaticFields), 0,
	sizeof (RIPEMD160_t2215), -1, 0, 0,
	sizeof (RIPEMD160Managed_t2216), -1, 0, 0,
	sizeof (RNGCryptoServiceProvider_t2217), -1, sizeof(RNGCryptoServiceProvider_t2217_StaticFields), 0,
	sizeof (RSA_t1454), -1, 0, 0,
	sizeof (RSACryptoServiceProvider_t1566), -1, sizeof(RSACryptoServiceProvider_t1566_StaticFields), 0,
	sizeof (RSAPKCS1KeyExchangeFormatter_t1593), -1, 0, 0,
	sizeof (RSAPKCS1SignatureDeformatter_t1574), -1, 0, 0,
	sizeof (RSAPKCS1SignatureFormatter_t2218), -1, 0, 0,
	sizeof (RSAParameters_t1533)+ sizeof (Il2CppObject), sizeof(RSAParameters_t1533_marshaled), 0, 0,
	sizeof (RandomNumberGenerator_t1400), -1, 0, 0,
	sizeof (Rijndael_t417), -1, 0, 0,
	sizeof (RijndaelManaged_t2219), -1, 0, 0,
	sizeof (RijndaelTransform_t2220), -1, sizeof(RijndaelTransform_t2220_StaticFields), 0,
	sizeof (RijndaelManagedTransform_t2221), -1, 0, 0,
	sizeof (SHA1_t1571), -1, 0, 0,
	sizeof (SHA1Internal_t2222), -1, 0, 0,
	sizeof (SHA1CryptoServiceProvider_t2223), -1, 0, 0,
	sizeof (SHA1Managed_t2224), -1, 0, 0,
	sizeof (SHA256_t1572), -1, 0, 0,
	sizeof (SHA256Managed_t2225), -1, 0, 0,
	sizeof (SHA384_t2226), -1, 0, 0,
	sizeof (SHA384Managed_t2227), -1, 0, 0,
	sizeof (SHA512_t2229), -1, 0, 0,
	sizeof (SHA512Managed_t2230), -1, 0, 0,
	sizeof (SHAConstants_t2231), -1, sizeof(SHAConstants_t2231_StaticFields), 0,
	sizeof (SignatureDescription_t2232), -1, 0, 0,
	sizeof (DSASignatureDescription_t2233), -1, 0, 0,
	sizeof (RSAPKCS1SHA1SignatureDescription_t2234), -1, 0, 0,
	sizeof (SymmetricAlgorithm_t1402), -1, 0, 0,
	sizeof (ToBase64Transform_t2235), -1, 0, 0,
	sizeof (TripleDES_t1581), -1, 0, 0,
	sizeof (TripleDESCryptoServiceProvider_t2236), -1, 0, 0,
	sizeof (TripleDESTransform_t2237), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (SecurityPermission_t2238), -1, 0, 0,
	sizeof (SecurityPermissionFlag_t2240)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (StrongNamePublicKeyBlob_t2241), -1, 0, 0,
	sizeof (ApplicationTrust_t2242), -1, 0, 0,
	sizeof (Evidence_t1987), -1, 0, 0,
	sizeof (EvidenceEnumerator_t2244), -1, 0, 0,
	sizeof (Hash_t2245), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (StrongName_t2246), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (PrincipalPolicy_t2247)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (WindowsAccountType_t2248)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (WindowsIdentity_t2249), -1, sizeof(WindowsIdentity_t2249_StaticFields), 0,
	sizeof (CodeAccessPermission_t2239), -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (PermissionSet_t1988), -1, 0, 0,
	sizeof (SecurityContext_t2250), -1, 0, 0,
	sizeof (SecurityCriticalAttribute_t2252), -1, 0, 0,
	sizeof (SecurityElement_t1863), -1, sizeof(SecurityElement_t1863_StaticFields), 0,
	sizeof (SecurityAttribute_t2253), -1, 0, 0,
	sizeof (SecurityException_t2254), -1, 0, 0,
	sizeof (RuntimeDeclSecurityEntry_t2256)+ sizeof (Il2CppObject), sizeof(RuntimeDeclSecurityEntry_t2256 ), 0, 0,
	sizeof (RuntimeSecurityFrame_t2257), -1, 0, 0,
	sizeof (SecurityFrame_t2258)+ sizeof (Il2CppObject), -1, 0, 0,
	sizeof (SecurityManager_t2259), -1, sizeof(SecurityManager_t2259_StaticFields), 0,
	sizeof (SecuritySafeCriticalAttribute_t2260), -1, 0, 0,
	sizeof (SuppressUnmanagedCodeSecurityAttribute_t2261), -1, 0, 0,
	sizeof (UnverifiableCodeAttribute_t2262), -1, 0, 0,
	sizeof (ASCIIEncoding_t2263), -1, 0, 0,
	sizeof (Decoder_t1920), -1, 0, 0,
	sizeof (DecoderExceptionFallback_t2266), -1, 0, 0,
	sizeof (DecoderExceptionFallbackBuffer_t2267), -1, 0, 0,
	sizeof (DecoderFallback_t2264), -1, sizeof(DecoderFallback_t2264_StaticFields), 0,
	sizeof (DecoderFallbackBuffer_t2265), -1, 0, 0,
	sizeof (DecoderFallbackException_t2268), -1, 0, 0,
	sizeof (DecoderReplacementFallback_t2269), -1, 0, 0,
	sizeof (DecoderReplacementFallbackBuffer_t2270), -1, 0, 0,
	sizeof (EncoderExceptionFallback_t2271), -1, 0, 0,
	sizeof (EncoderExceptionFallbackBuffer_t2273), -1, 0, 0,
	sizeof (EncoderFallback_t2272), -1, sizeof(EncoderFallback_t2272_StaticFields), 0,
	sizeof (EncoderFallbackBuffer_t2274), -1, 0, 0,
	sizeof (EncoderFallbackException_t2275), -1, 0, 0,
	sizeof (EncoderReplacementFallback_t2276), -1, 0, 0,
	sizeof (EncoderReplacementFallbackBuffer_t2277), -1, 0, 0,
	sizeof (Encoding_t420), -1, sizeof(Encoding_t420_StaticFields), 0,
	sizeof (ForwardingDecoder_t2278), -1, 0, 0,
	sizeof (Latin1Encoding_t2279), -1, 0, 0,
	sizeof (StringBuilder_t70), sizeof(char*), 0, 0,
	sizeof (UTF32Encoding_t2281), -1, 0, 0,
	sizeof (UTF32Decoder_t2280), -1, 0, 0,
	sizeof (UTF7Encoding_t2283), -1, sizeof(UTF7Encoding_t2283_StaticFields), 0,
	sizeof (UTF7Decoder_t2282), -1, 0, 0,
	sizeof (UTF8Encoding_t2286), -1, 0, 0,
	sizeof (UTF8Decoder_t2285), -1, 0, 0,
	sizeof (UnicodeEncoding_t2288), -1, 0, 0,
	sizeof (UnicodeDecoder_t2287), -1, 0, 0,
	sizeof (CompressedStack_t2251), -1, 0, 0,
	sizeof (EventResetMode_t2289)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (EventWaitHandle_t2290), -1, 0, 0,
	sizeof (ExecutionContext_t2097), -1, 0, 0,
	sizeof (Interlocked_t2291), -1, 0, 0,
	sizeof (ManualResetEvent_t160), -1, 0, 0,
	sizeof (Monitor_t2292), -1, 0, 0,
	sizeof (Mutex_t2083), -1, 0, 0,
	sizeof (NativeEventCalls_t2293), -1, 0, 0,
	sizeof (SynchronizationLockException_t2294), -1, 0, 0,
	sizeof (Thread_t170), -1, sizeof(Thread_t170_StaticFields), sizeof(Thread_t170_ThreadStaticFields),
	sizeof (ThreadAbortException_t2296), -1, 0, 0,
	sizeof (ThreadInterruptedException_t2297), -1, 0, 0,
	sizeof (ThreadPool_t2298), -1, 0, 0,
	sizeof (ThreadState_t2299)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (ThreadStateException_t2300), -1, 0, 0,
	sizeof (Timer_t2090), -1, sizeof(Timer_t2090_StaticFields), 0,
	sizeof (TimerComparer_t2301), -1, 0, 0,
	sizeof (Scheduler_t2302), -1, sizeof(Scheduler_t2302_StaticFields), 0,
	sizeof (WaitHandle_t351), -1, sizeof(WaitHandle_t351_StaticFields), 0,
	sizeof (AccessViolationException_t2304), -1, 0, 0,
	sizeof (ActivationContext_t2305), -1, 0, 0,
	sizeof (Activator_t2306), -1, 0, 0,
	sizeof (AppDomain_t1015), -1, sizeof(AppDomain_t1015_StaticFields), sizeof(AppDomain_t1015_ThreadStaticFields),
	sizeof (AppDomainManager_t2307), -1, 0, 0,
	sizeof (AppDomainSetup_t2312), -1, 0, 0,
	sizeof (ApplicationException_t2314), -1, 0, 0,
	sizeof (ApplicationIdentity_t2308), -1, 0, 0,
	sizeof (ArgumentException_t764), -1, 0, 0,
	sizeof (ArgumentNullException_t1037), -1, 0, 0,
	sizeof (ArgumentOutOfRangeException_t1419), -1, 0, 0,
	sizeof (ArithmeticException_t1562), -1, 0, 0,
	sizeof (ArrayTypeMismatchException_t2315), -1, 0, 0,
	sizeof (AssemblyLoadEventArgs_t2316), -1, 0, 0,
	sizeof (AttributeTargets_t2317)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (BitConverter_t1356), -1, sizeof(BitConverter_t1356_StaticFields), 0,
	sizeof (Buffer_t2318), -1, 0, 0,
	sizeof (CharEnumerator_t2319), -1, 0, 0,
	sizeof (Console_t1760), -1, sizeof(Console_t1760_StaticFields), 0,
	sizeof (ContextBoundObject_t2081), -1, 0, 0,
	sizeof (Convert_t373), -1, sizeof(Convert_t373_StaticFields), 0,
	sizeof (DBNull_t2320), -1, sizeof(DBNull_t2320_StaticFields), 0,
	sizeof (DateTime_t74)+ sizeof (Il2CppObject), -1, sizeof(DateTime_t74_StaticFields), 0,
	sizeof (Which_t2321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DateTimeKind_t2322)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DateTimeOffset_t2323)+ sizeof (Il2CppObject), -1, sizeof(DateTimeOffset_t2323_StaticFields), 0,
	sizeof (DateTimeUtils_t2324), -1, 0, 0,
	sizeof (DayOfWeek_t2325)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (DelegateData_t1768), -1, 0, 0,
	sizeof (DelegateSerializationHolder_t2327), -1, 0, 0,
	sizeof (DelegateEntry_t2326), -1, 0, 0,
	sizeof (DivideByZeroException_t2328), -1, 0, 0,
	sizeof (DllNotFoundException_t2329), -1, 0, 0,
	sizeof (EntryPointNotFoundException_t2331), -1, 0, 0,
	sizeof (MonoEnumInfo_t2336)+ sizeof (Il2CppObject), -1, sizeof(MonoEnumInfo_t2336_StaticFields), sizeof(MonoEnumInfo_t2336_ThreadStaticFields),
	sizeof (SByteComparer_t2332), -1, 0, 0,
	sizeof (ShortComparer_t2333), -1, 0, 0,
	sizeof (IntComparer_t2334), -1, 0, 0,
	sizeof (LongComparer_t2335), -1, 0, 0,
	sizeof (Environment_t2338), -1, sizeof(Environment_t2338_StaticFields), 0,
	sizeof (SpecialFolder_t2337)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (EventArgs_t1452), -1, sizeof(EventArgs_t1452_StaticFields), 0,
	sizeof (ExecutionEngineException_t2340), -1, 0, 0,
	sizeof (FieldAccessException_t2341), -1, 0, 0,
	sizeof (FlagsAttribute_t2343), -1, 0, 0,
	sizeof (FormatException_t1564), -1, 0, 0,
	sizeof (GC_t2344), -1, 0, 0,
	sizeof (Guid_t452)+ sizeof (Il2CppObject), sizeof(Guid_t452 ), sizeof(Guid_t452_StaticFields), 0,
	0, -1, 0, 0,
	0, -1, 0, 0,
	sizeof (IndexOutOfRangeException_t1022), -1, 0, 0,
	sizeof (InvalidCastException_t2345), -1, 0, 0,
	sizeof (InvalidOperationException_t425), -1, 0, 0,
	sizeof (LoaderOptimization_t2346)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (LocalDataStoreSlot_t2347), -1, sizeof(LocalDataStoreSlot_t2347_StaticFields), 0,
	sizeof (Math_t2348), -1, 0, 0,
	sizeof (MemberAccessException_t2342), -1, 0, 0,
	sizeof (MethodAccessException_t2349), -1, 0, 0,
	sizeof (MissingFieldException_t2350), -1, 0, 0,
	sizeof (MissingMemberException_t2351), -1, 0, 0,
	sizeof (MissingMethodException_t2352), -1, 0, 0,
	sizeof (MonoAsyncCall_t2353), -1, 0, 0,
	sizeof (MonoCustomAttrs_t2355), -1, sizeof(MonoCustomAttrs_t2355_StaticFields), 0,
	sizeof (AttributeInfo_t2354), -1, 0, 0,
	sizeof (MonoTouchAOTHelper_t2356), -1, sizeof(MonoTouchAOTHelper_t2356_StaticFields), 0,
	sizeof (MonoTypeInfo_t2357), -1, 0, 0,
	sizeof (MonoType_t), -1, 0, 0,
	sizeof (MulticastNotSupportedException_t2358), -1, 0, 0,
	sizeof (NonSerializedAttribute_t2359), -1, 0, 0,
	sizeof (NotImplementedException_t1418), -1, 0, 0,
	sizeof (NotSupportedException_t374), -1, 0, 0,
	sizeof (NullReferenceException_t1014), -1, 0, 0,
	sizeof (NumberFormatter_t2361), -1, sizeof(NumberFormatter_t2361_StaticFields), sizeof(NumberFormatter_t2361_ThreadStaticFields),
	sizeof (CustomInfo_t2360), -1, 0, 0,
	sizeof (ObjectDisposedException_t1420), -1, 0, 0,
	sizeof (OperatingSystem_t2339), -1, 0, 0,
	sizeof (OutOfMemoryException_t2362), -1, 0, 0,
	sizeof (OverflowException_t2363), -1, 0, 0,
	sizeof (PlatformID_t2364)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (RankException_t2365), -1, 0, 0,
	sizeof (ResolveEventArgs_t2366), -1, 0, 0,
	sizeof (RuntimeMethodHandle_t2367)+ sizeof (Il2CppObject), sizeof(RuntimeMethodHandle_t2367 ), 0, 0,
	sizeof (StringComparer_t1024), -1, sizeof(StringComparer_t1024_StaticFields), 0,
	sizeof (CultureAwareComparer_t2368), -1, 0, 0,
	sizeof (OrdinalComparer_t2369), -1, 0, 0,
	sizeof (StringComparison_t2370)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (StringSplitOptions_t2371)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (SystemException_t1759), -1, 0, 0,
	sizeof (ThreadStaticAttribute_t2372), -1, 0, 0,
	sizeof (TimeSpan_t427)+ sizeof (Il2CppObject), sizeof(TimeSpan_t427 ), sizeof(TimeSpan_t427_StaticFields), 0,
	sizeof (TimeZone_t2373), -1, sizeof(TimeZone_t2373_StaticFields), 0,
	sizeof (CurrentSystemTimeZone_t2374), -1, sizeof(CurrentSystemTimeZone_t2374_StaticFields), 0,
	sizeof (TypeCode_t2375)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0,
	sizeof (TypeInitializationException_t2376), -1, 0, 0,
	sizeof (TypeLoadException_t2330), -1, 0, 0,
	sizeof (UnauthorizedAccessException_t2377), -1, 0, 0,
	sizeof (UnhandledExceptionEventArgs_t998), -1, 0, 0,
	sizeof (UnitySerializationHolder_t2379), -1, 0, 0,
	sizeof (UnityType_t2378)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0,
	sizeof (Version_t1633), -1, 0, 0,
	sizeof (WeakReference_t2139), -1, 0, 0,
	sizeof (PrimalityTest_t2380), sizeof(methodPointerType), 0, 0,
	sizeof (MemberFilter_t1773), sizeof(methodPointerType), 0, 0,
	sizeof (TypeFilter_t2015), sizeof(methodPointerType), 0, 0,
	sizeof (CrossContextDelegate_t2381), sizeof(methodPointerType), 0, 0,
	sizeof (HeaderHandler_t2382), sizeof(methodPointerType), 0, 0,
	sizeof (ThreadStart_t428), sizeof(methodPointerType), 0, 0,
	sizeof (TimerCallback_t2303), sizeof(methodPointerType), 0, 0,
	sizeof (WaitCallback_t2384), sizeof(methodPointerType), 0, 0,
	0, 0, 0, 0,
	sizeof (AppDomainInitializer_t2313), sizeof(methodPointerType), 0, 0,
	sizeof (AssemblyLoadEventHandler_t2309), sizeof(methodPointerType), 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	sizeof (EventHandler_t2311), sizeof(methodPointerType), 0, 0,
	0, 0, 0, 0,
	sizeof (ResolveEventHandler_t2310), sizeof(methodPointerType), 0, 0,
	sizeof (UnhandledExceptionEventHandler_t1016), sizeof(methodPointerType), 0, 0,
	sizeof (U3CPrivateImplementationDetailsU3E_t2406), -1, sizeof(U3CPrivateImplementationDetailsU3E_t2406_StaticFields), 0,
	sizeof (U24ArrayTypeU2456_t2385)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2456_t2385_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2424_t2386)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2424_t2386_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2416_t2387)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t2387_marshaled), 0, 0,
	sizeof (U24ArrayTypeU24120_t2388)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t2388_marshaled), 0, 0,
	sizeof (U24ArrayTypeU243132_t2389)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t2389_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2420_t2390)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t2390_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2432_t2391)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t2391_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2448_t2392)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t2392_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2464_t2393)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t2393_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2412_t2394)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t2394_marshaled), 0, 0,
	sizeof (U24ArrayTypeU24136_t2395)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t2395_marshaled), 0, 0,
	sizeof (U24ArrayTypeU248_t2396)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t2396_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2472_t2397)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2472_t2397_marshaled), 0, 0,
	sizeof (U24ArrayTypeU24124_t2398)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24124_t2398_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2496_t2399)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2496_t2399_marshaled), 0, 0,
	sizeof (U24ArrayTypeU242048_t2400)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU242048_t2400_marshaled), 0, 0,
	sizeof (U24ArrayTypeU24256_t2401)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2401_marshaled), 0, 0,
	sizeof (U24ArrayTypeU241024_t2402)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t2402_marshaled), 0, 0,
	sizeof (U24ArrayTypeU24640_t2403)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24640_t2403_marshaled), 0, 0,
	sizeof (U24ArrayTypeU24128_t2404)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t2404_marshaled), 0, 0,
	sizeof (U24ArrayTypeU2452_t2405)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2452_t2405_marshaled), 0, 0,
};
