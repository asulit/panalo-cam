﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t3139;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m23484_gshared (DefaultComparer_t3139 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m23484(__this, method) (( void (*) (DefaultComparer_t3139 *, const MethodInfo*))DefaultComparer__ctor_m23484_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m23485_gshared (DefaultComparer_t3139 * __this, UICharInfo_t754  ___x, UICharInfo_t754  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m23485(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3139 *, UICharInfo_t754 , UICharInfo_t754 , const MethodInfo*))DefaultComparer_Compare_m23485_gshared)(__this, ___x, ___y, method)
