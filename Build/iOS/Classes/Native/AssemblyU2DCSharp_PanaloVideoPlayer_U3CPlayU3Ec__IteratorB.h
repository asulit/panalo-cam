﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
struct Object_t;
// PanaloVideoPlayer
struct PanaloVideoPlayer_t242;
// System.Object
#include "mscorlib_System_Object.h"
// PanaloVideoPlayer/<Play>c__IteratorB
struct  U3CPlayU3Ec__IteratorB_t245  : public Object_t
{
	// System.String PanaloVideoPlayer/<Play>c__IteratorB::movieTexturePath
	String_t* ___movieTexturePath_0;
	// System.Int32 PanaloVideoPlayer/<Play>c__IteratorB::$PC
	int32_t ___U24PC_1;
	// System.Object PanaloVideoPlayer/<Play>c__IteratorB::$current
	Object_t * ___U24current_2;
	// System.String PanaloVideoPlayer/<Play>c__IteratorB::<$>movieTexturePath
	String_t* ___U3CU24U3EmovieTexturePath_3;
	// PanaloVideoPlayer PanaloVideoPlayer/<Play>c__IteratorB::<>f__this
	PanaloVideoPlayer_t242 * ___U3CU3Ef__this_4;
};
