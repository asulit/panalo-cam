﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_81.h"
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24165_gshared (InternalEnumerator_1_t3185 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24165(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3185 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24165_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24166_gshared (InternalEnumerator_1_t3185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24166(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3185 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24166_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24167_gshared (InternalEnumerator_1_t3185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24167(__this, method) (( void (*) (InternalEnumerator_1_t3185 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24167_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24168_gshared (InternalEnumerator_1_t3185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24168(__this, method) (( bool (*) (InternalEnumerator_1_t3185 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24168_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m24169_gshared (InternalEnumerator_1_t3185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24169(__this, method) (( int32_t (*) (InternalEnumerator_1_t3185 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24169_gshared)(__this, method)
