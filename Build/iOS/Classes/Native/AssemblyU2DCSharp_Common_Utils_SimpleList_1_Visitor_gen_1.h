﻿#pragma once
#include <stdint.h>
// Common.Signal.Signal/SignalListener
struct SignalListener_t114;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// Common.Utils.SimpleList`1/Visitor<Common.Signal.Signal/SignalListener>
struct  Visitor_t2608  : public MulticastDelegate_t28
{
};
