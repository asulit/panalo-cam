﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RC2Transform
struct RC2Transform_t2214;
// System.Security.Cryptography.RC2
struct RC2_t1580;
// System.Byte[]
struct ByteU5BU5D_t139;

// System.Void System.Security.Cryptography.RC2Transform::.ctor(System.Security.Cryptography.RC2,System.Boolean,System.Byte[],System.Byte[])
extern "C" void RC2Transform__ctor_m13085 (RC2Transform_t2214 * __this, RC2_t1580 * ___rc2Algo, bool ___encryption, ByteU5BU5D_t139* ___key, ByteU5BU5D_t139* ___iv, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RC2Transform::.cctor()
extern "C" void RC2Transform__cctor_m13086 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RC2Transform::ECB(System.Byte[],System.Byte[])
extern "C" void RC2Transform_ECB_m13087 (RC2Transform_t2214 * __this, ByteU5BU5D_t139* ___input, ByteU5BU5D_t139* ___output, const MethodInfo* method) IL2CPP_METHOD_ATTR;
