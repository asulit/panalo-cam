﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.RectMask2D
struct RectMask2D_t609;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.RectMask2D>
struct  Comparison_1_t2979  : public MulticastDelegate_t28
{
};
