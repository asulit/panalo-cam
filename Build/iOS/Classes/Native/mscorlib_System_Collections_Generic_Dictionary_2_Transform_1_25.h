﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ERaffleResult,ERaffleResult>
struct  Transform_1_t2718  : public MulticastDelegate_t28
{
};
