﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<ERaffleResult>
struct EqualityComparer_1_t2721;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<ERaffleResult>::.ctor()
extern "C" void EqualityComparer_1__ctor_m17694_gshared (EqualityComparer_1_t2721 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m17694(__this, method) (( void (*) (EqualityComparer_1_t2721 *, const MethodInfo*))EqualityComparer_1__ctor_m17694_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<ERaffleResult>::.cctor()
extern "C" void EqualityComparer_1__cctor_m17695_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m17695(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m17695_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<ERaffleResult>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17696_gshared (EqualityComparer_1_t2721 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17696(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t2721 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17696_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<ERaffleResult>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17697_gshared (EqualityComparer_1_t2721 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17697(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t2721 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17697_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<ERaffleResult>::get_Default()
extern "C" EqualityComparer_1_t2721 * EqualityComparer_1_get_Default_m17698_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m17698(__this /* static, unused */, method) (( EqualityComparer_1_t2721 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m17698_gshared)(__this /* static, unused */, method)
