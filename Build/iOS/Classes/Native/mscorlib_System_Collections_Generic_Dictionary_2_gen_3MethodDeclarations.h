﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_41MethodDeclarations.h"
#define Dictionary_2__ctor_m1525(__this, method) (( void (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2__ctor_m15007_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m16274(__this, ___comparer, method) (( void (*) (Dictionary_2_t119 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15009_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::.ctor(System.Int32)
#define Dictionary_2__ctor_m16275(__this, ___capacity, method) (( void (*) (Dictionary_2_t119 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m15011_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m16276(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t119 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m15013_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m16277(__this, method) (( Object_t * (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m15015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16278(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t119 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m15017_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16279(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t119 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m15019_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m16280(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t119 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m15021_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m16281(__this, ___key, method) (( bool (*) (Dictionary_2_t119 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m15023_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m16282(__this, ___key, method) (( void (*) (Dictionary_2_t119 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m15025_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16283(__this, method) (( bool (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15027_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16284(__this, method) (( Object_t * (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15029_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16285(__this, method) (( bool (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15031_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16286(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t119 *, KeyValuePair_2_t2611 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15033_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16287(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t119 *, KeyValuePair_2_t2611 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15035_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16288(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t119 *, KeyValuePair_2U5BU5D_t3636*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15037_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16289(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t119 *, KeyValuePair_2_t2611 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15039_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16290(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t119 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m15041_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16291(__this, method) (( Object_t * (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15043_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16292(__this, method) (( Object_t* (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15045_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16293(__this, method) (( Object_t * (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15047_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::get_Count()
#define Dictionary_2_get_Count_m16294(__this, method) (( int32_t (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_get_Count_m15049_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::get_Item(TKey)
#define Dictionary_2_get_Item_m16295(__this, ___key, method) (( Signal_t116 * (*) (Dictionary_2_t119 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m15051_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m16296(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t119 *, String_t*, Signal_t116 *, const MethodInfo*))Dictionary_2_set_Item_m15053_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m16297(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t119 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m15055_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m16298(__this, ___size, method) (( void (*) (Dictionary_2_t119 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m15057_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m16299(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t119 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m15059_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m16300(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2611  (*) (Object_t * /* static, unused */, String_t*, Signal_t116 *, const MethodInfo*))Dictionary_2_make_pair_m15061_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m16301(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, Signal_t116 *, const MethodInfo*))Dictionary_2_pick_key_m15063_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m16302(__this /* static, unused */, ___key, ___value, method) (( Signal_t116 * (*) (Object_t * /* static, unused */, String_t*, Signal_t116 *, const MethodInfo*))Dictionary_2_pick_value_m15065_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m16303(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t119 *, KeyValuePair_2U5BU5D_t3636*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m15067_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::Resize()
#define Dictionary_2_Resize_m16304(__this, method) (( void (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_Resize_m15069_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::Add(TKey,TValue)
#define Dictionary_2_Add_m16305(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t119 *, String_t*, Signal_t116 *, const MethodInfo*))Dictionary_2_Add_m15071_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::Clear()
#define Dictionary_2_Clear_m16306(__this, method) (( void (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_Clear_m15073_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m16307(__this, ___key, method) (( bool (*) (Dictionary_2_t119 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m15075_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m16308(__this, ___value, method) (( bool (*) (Dictionary_2_t119 *, Signal_t116 *, const MethodInfo*))Dictionary_2_ContainsValue_m15077_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m16309(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t119 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m15079_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m16310(__this, ___sender, method) (( void (*) (Dictionary_2_t119 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m15081_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::Remove(TKey)
#define Dictionary_2_Remove_m16311(__this, ___key, method) (( bool (*) (Dictionary_2_t119 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m15083_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m16312(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t119 *, String_t*, Signal_t116 **, const MethodInfo*))Dictionary_2_TryGetValue_m15085_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::get_Keys()
#define Dictionary_2_get_Keys_m16313(__this, method) (( KeyCollection_t2612 * (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_get_Keys_m15087_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::get_Values()
#define Dictionary_2_get_Values_m16314(__this, method) (( ValueCollection_t2613 * (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_get_Values_m15089_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m16315(__this, ___key, method) (( String_t* (*) (Dictionary_2_t119 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m15091_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m16316(__this, ___value, method) (( Signal_t116 * (*) (Dictionary_2_t119 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m15093_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m16317(__this, ___pair, method) (( bool (*) (Dictionary_2_t119 *, KeyValuePair_2_t2611 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m15095_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m16318(__this, method) (( Enumerator_t2614  (*) (Dictionary_2_t119 *, const MethodInfo*))Dictionary_2_GetEnumerator_m15097_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m16319(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, String_t*, Signal_t116 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m15099_gshared)(__this /* static, unused */, ___key, ___value, method)
