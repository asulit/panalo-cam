﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_48MethodDeclarations.h"
#define KeyValuePair_2__ctor_m28843(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3492 *, String_t*, ProfileData_t1226 , const MethodInfo*))KeyValuePair_2__ctor_m28748_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Key()
#define KeyValuePair_2_get_Key_m28844(__this, method) (( String_t* (*) (KeyValuePair_2_t3492 *, const MethodInfo*))KeyValuePair_2_get_Key_m28749_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m28845(__this, ___value, method) (( void (*) (KeyValuePair_2_t3492 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m28750_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Value()
#define KeyValuePair_2_get_Value_m28846(__this, method) (( ProfileData_t1226  (*) (KeyValuePair_2_t3492 *, const MethodInfo*))KeyValuePair_2_get_Value_m28751_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m28847(__this, ___value, method) (( void (*) (KeyValuePair_2_t3492 *, ProfileData_t1226 , const MethodInfo*))KeyValuePair_2_set_Value_m28752_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::ToString()
#define KeyValuePair_2_ToString_m28848(__this, method) (( String_t* (*) (KeyValuePair_2_t3492 *, const MethodInfo*))KeyValuePair_2_ToString_m28753_gshared)(__this, method)
