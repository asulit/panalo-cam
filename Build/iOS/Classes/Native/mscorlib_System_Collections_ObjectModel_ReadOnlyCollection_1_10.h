﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<AnimatorFrame>
struct IList_1_t2773;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>
struct  ReadOnlyCollection_1_t2772  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<AnimatorFrame>::list
	Object_t* ___list_0;
};
