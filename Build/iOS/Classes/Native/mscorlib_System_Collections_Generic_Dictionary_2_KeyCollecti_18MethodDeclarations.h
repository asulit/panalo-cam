﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_16MethodDeclarations.h"
#define KeyCollection__ctor_m17286(__this, ___dictionary, method) (( void (*) (KeyCollection_t2682 *, Dictionary_2_t191 *, const MethodInfo*))KeyCollection__ctor_m17196_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17287(__this, ___item, method) (( void (*) (KeyCollection_t2682 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17197_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17288(__this, method) (( void (*) (KeyCollection_t2682 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17289(__this, ___item, method) (( bool (*) (KeyCollection_t2682 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17199_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17290(__this, ___item, method) (( bool (*) (KeyCollection_t2682 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17200_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17291(__this, method) (( Object_t* (*) (KeyCollection_t2682 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17201_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m17292(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2682 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m17202_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17293(__this, method) (( Object_t * (*) (KeyCollection_t2682 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17203_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17294(__this, method) (( bool (*) (KeyCollection_t2682 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17295(__this, method) (( bool (*) (KeyCollection_t2682 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17205_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m17296(__this, method) (( Object_t * (*) (KeyCollection_t2682 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m17206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m17297(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2682 *, EScreensU5BU5D_t2662*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m17207_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::GetEnumerator()
#define KeyCollection_GetEnumerator_m17298(__this, method) (( Enumerator_t3652  (*) (KeyCollection_t2682 *, const MethodInfo*))KeyCollection_GetEnumerator_m17208_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<EScreens,System.String>::get_Count()
#define KeyCollection_get_Count_m17299(__this, method) (( int32_t (*) (KeyCollection_t2682 *, const MethodInfo*))KeyCollection_get_Count_m17209_gshared)(__this, method)
