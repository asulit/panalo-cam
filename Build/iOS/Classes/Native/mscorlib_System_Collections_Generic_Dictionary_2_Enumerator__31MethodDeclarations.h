﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__30MethodDeclarations.h"
#define Enumerator__ctor_m24288(__this, ___dictionary, method) (( void (*) (Enumerator_t3200 *, Dictionary_2_t978 *, const MethodInfo*))Enumerator__ctor_m24189_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24289(__this, method) (( Object_t * (*) (Enumerator_t3200 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24190_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24290(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3200 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24191_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24291(__this, method) (( Object_t * (*) (Enumerator_t3200 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24192_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24292(__this, method) (( Object_t * (*) (Enumerator_t3200 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m24293(__this, method) (( bool (*) (Enumerator_t3200 *, const MethodInfo*))Enumerator_MoveNext_m24194_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m24294(__this, method) (( KeyValuePair_2_t3197  (*) (Enumerator_t3200 *, const MethodInfo*))Enumerator_get_Current_m24195_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m24295(__this, method) (( Event_t381 * (*) (Enumerator_t3200 *, const MethodInfo*))Enumerator_get_CurrentKey_m24196_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m24296(__this, method) (( int32_t (*) (Enumerator_t3200 *, const MethodInfo*))Enumerator_get_CurrentValue_m24197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m24297(__this, method) (( void (*) (Enumerator_t3200 *, const MethodInfo*))Enumerator_VerifyState_m24198_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24298(__this, method) (( void (*) (Enumerator_t3200 *, const MethodInfo*))Enumerator_VerifyCurrent_m24199_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m24299(__this, method) (( void (*) (Enumerator_t3200 *, const MethodInfo*))Enumerator_Dispose_m24200_gshared)(__this, method)
