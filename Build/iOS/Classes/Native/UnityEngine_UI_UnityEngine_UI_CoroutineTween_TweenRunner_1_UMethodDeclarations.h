﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct U3CStartU3Ec__Iterator0_t2912;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m20405_gshared (U3CStartU3Ec__Iterator0_t2912 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0__ctor_m20405(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2912 *, const MethodInfo*))U3CStartU3Ec__Iterator0__ctor_m20405_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m20406_gshared (U3CStartU3Ec__Iterator0_t2912 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m20406(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2912 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m20406_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m20407_gshared (U3CStartU3Ec__Iterator0_t2912 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m20407(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2912 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m20407_gshared)(__this, method)
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m20408_gshared (U3CStartU3Ec__Iterator0_t2912 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_MoveNext_m20408(__this, method) (( bool (*) (U3CStartU3Ec__Iterator0_t2912 *, const MethodInfo*))U3CStartU3Ec__Iterator0_MoveNext_m20408_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m20409_gshared (U3CStartU3Ec__Iterator0_t2912 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Dispose_m20409(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2912 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Dispose_m20409_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m20410_gshared (U3CStartU3Ec__Iterator0_t2912 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Reset_m20410(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2912 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Reset_m20410_gshared)(__this, method)
