﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU24124_t2398_marshal(const U24ArrayTypeU24124_t2398& unmarshaled, U24ArrayTypeU24124_t2398_marshaled& marshaled);
extern "C" void U24ArrayTypeU24124_t2398_marshal_back(const U24ArrayTypeU24124_t2398_marshaled& marshaled, U24ArrayTypeU24124_t2398& unmarshaled);
extern "C" void U24ArrayTypeU24124_t2398_marshal_cleanup(U24ArrayTypeU24124_t2398_marshaled& marshaled);
