﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"
#define Enumerator__ctor_m16358(__this, ___dictionary, method) (( void (*) (Enumerator_t2614 *, Dictionary_2_t119 *, const MethodInfo*))Enumerator__ctor_m15140_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16359(__this, method) (( Object_t * (*) (Enumerator_t2614 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15141_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16360(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t2614 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15142_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16361(__this, method) (( Object_t * (*) (Enumerator_t2614 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15143_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16362(__this, method) (( Object_t * (*) (Enumerator_t2614 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::MoveNext()
#define Enumerator_MoveNext_m16363(__this, method) (( bool (*) (Enumerator_t2614 *, const MethodInfo*))Enumerator_MoveNext_m15145_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::get_Current()
#define Enumerator_get_Current_m16364(__this, method) (( KeyValuePair_2_t2611  (*) (Enumerator_t2614 *, const MethodInfo*))Enumerator_get_Current_m15146_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16365(__this, method) (( String_t* (*) (Enumerator_t2614 *, const MethodInfo*))Enumerator_get_CurrentKey_m15147_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16366(__this, method) (( Signal_t116 * (*) (Enumerator_t2614 *, const MethodInfo*))Enumerator_get_CurrentValue_m15148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::VerifyState()
#define Enumerator_VerifyState_m16367(__this, method) (( void (*) (Enumerator_t2614 *, const MethodInfo*))Enumerator_VerifyState_m15149_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16368(__this, method) (( void (*) (Enumerator_t2614 *, const MethodInfo*))Enumerator_VerifyCurrent_m15150_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Signal.Signal>::Dispose()
#define Enumerator_Dispose_m16369(__this, method) (( void (*) (Enumerator_t2614 *, const MethodInfo*))Enumerator_Dispose_m15151_gshared)(__this, method)
