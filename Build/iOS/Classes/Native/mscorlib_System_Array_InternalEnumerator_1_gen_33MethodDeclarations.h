﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_33.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17610_gshared (InternalEnumerator_1_t2711 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17610(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2711 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17610_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17611_gshared (InternalEnumerator_1_t2711 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17611(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2711 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17611_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17612_gshared (InternalEnumerator_1_t2711 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17612(__this, method) (( void (*) (InternalEnumerator_1_t2711 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17612_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17613_gshared (InternalEnumerator_1_t2711 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17613(__this, method) (( bool (*) (InternalEnumerator_1_t2711 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17613_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>::get_Current()
extern "C" KeyValuePair_2_t352  InternalEnumerator_1_get_Current_m17614_gshared (InternalEnumerator_1_t2711 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17614(__this, method) (( KeyValuePair_2_t352  (*) (InternalEnumerator_1_t2711 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17614_gshared)(__this, method)
