﻿#pragma once
#include <stdint.h>
// System.Func`1<System.Object>
struct Func_1_t2650;
// System.Object
#include "mscorlib_System_Object.h"
// UnityThreading.Dispatcher/<CreateSafeFunction>c__AnonStorey10`1<System.Object>
struct  U3CCreateSafeFunctionU3Ec__AnonStorey10_1_t2651  : public Object_t
{
	// System.Func`1<T> UnityThreading.Dispatcher/<CreateSafeFunction>c__AnonStorey10`1<System.Object>::function
	Func_1_t2650 * ___function_0;
};
