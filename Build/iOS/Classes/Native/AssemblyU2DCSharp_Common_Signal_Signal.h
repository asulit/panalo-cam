﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Signal.ConcreteSignalParameters
struct ConcreteSignalParameters_t113;
// Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener>
struct SimpleList_1_t117;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Signal.Signal
struct  Signal_t116  : public Object_t
{
	// System.String Common.Signal.Signal::name
	String_t* ___name_0;
	// Common.Signal.ConcreteSignalParameters Common.Signal.Signal::parameters
	ConcreteSignalParameters_t113 * ___parameters_1;
	// Common.Utils.SimpleList`1<Common.Signal.Signal/SignalListener> Common.Signal.Signal::listenerList
	SimpleList_1_t117 * ___listenerList_2;
};
