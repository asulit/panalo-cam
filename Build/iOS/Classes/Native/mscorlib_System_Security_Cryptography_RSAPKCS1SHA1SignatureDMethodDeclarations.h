﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription
struct RSAPKCS1SHA1SignatureDescription_t2234;

// System.Void System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription::.ctor()
extern "C" void RSAPKCS1SHA1SignatureDescription__ctor_m13244 (RSAPKCS1SHA1SignatureDescription_t2234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
