﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Threading.ThreadAbortException
struct ThreadAbortException_t2296;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Threading.ThreadAbortException::.ctor()
extern "C" void ThreadAbortException__ctor_m13695 (ThreadAbortException_t2296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ThreadAbortException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ThreadAbortException__ctor_m13696 (ThreadAbortException_t2296 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___sc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
