﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU24256_t2401_marshal(const U24ArrayTypeU24256_t2401& unmarshaled, U24ArrayTypeU24256_t2401_marshaled& marshaled);
extern "C" void U24ArrayTypeU24256_t2401_marshal_back(const U24ArrayTypeU24256_t2401_marshaled& marshaled, U24ArrayTypeU24256_t2401& unmarshaled);
extern "C" void U24ArrayTypeU24256_t2401_marshal_cleanup(U24ArrayTypeU24256_t2401_marshaled& marshaled);
