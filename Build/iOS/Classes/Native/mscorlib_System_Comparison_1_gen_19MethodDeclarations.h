﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Comparison`1<System.Reflection.MethodInfo>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_4MethodDeclarations.h"
#define Comparison_1__ctor_m18743(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2798 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m14828_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<System.Reflection.MethodInfo>::Invoke(T,T)
#define Comparison_1_Invoke_m18744(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2798 *, MethodInfo_t *, MethodInfo_t *, const MethodInfo*))Comparison_1_Invoke_m14829_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<System.Reflection.MethodInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m18745(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2798 *, MethodInfo_t *, MethodInfo_t *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m14830_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<System.Reflection.MethodInfo>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m18746(__this, ___result, method) (( int32_t (*) (Comparison_1_t2798 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m14831_gshared)(__this, ___result, method)
