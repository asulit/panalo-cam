﻿#pragma once
#include <stdint.h>
// Common.Utils.SimpleList`1<System.Object>
struct SimpleList_1_t2598;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Utils.Pool`1<System.Object>
struct  Pool_1_t2633  : public Object_t
{
	// Common.Utils.SimpleList`1<T> Common.Utils.Pool`1<System.Object>::pooledList
	SimpleList_1_t2598 * ___pooledList_0;
};
