﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IEndDragHandler
struct IEndDragHandler_t695;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t480;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct  EventFunction_1_t497  : public MulticastDelegate_t28
{
};
