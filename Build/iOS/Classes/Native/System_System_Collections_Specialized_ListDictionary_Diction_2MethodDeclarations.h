﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.ListDictionary/DictionaryNodeCollection
struct DictionaryNodeCollection_t1607;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t1602;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;

// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::.ctor(System.Collections.Specialized.ListDictionary,System.Boolean)
extern "C" void DictionaryNodeCollection__ctor_m8606 (DictionaryNodeCollection_t1607 * __this, ListDictionary_t1602 * ___dict, bool ___isKeyList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::get_Count()
extern "C" int32_t DictionaryNodeCollection_get_Count_m8607 (DictionaryNodeCollection_t1607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::get_IsSynchronized()
extern "C" bool DictionaryNodeCollection_get_IsSynchronized_m8608 (DictionaryNodeCollection_t1607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::get_SyncRoot()
extern "C" Object_t * DictionaryNodeCollection_get_SyncRoot_m8609 (DictionaryNodeCollection_t1607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::CopyTo(System.Array,System.Int32)
extern "C" void DictionaryNodeCollection_CopyTo_m8610 (DictionaryNodeCollection_t1607 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::GetEnumerator()
extern "C" Object_t * DictionaryNodeCollection_GetEnumerator_m8611 (DictionaryNodeCollection_t1607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
