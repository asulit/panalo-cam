﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaManager
struct VuforiaManager_t472;

// Vuforia.VuforiaManager Vuforia.VuforiaManager::get_Instance()
extern "C" VuforiaManager_t472 * VuforiaManager_get_Instance_m1890 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaManager::.ctor()
extern "C" void VuforiaManager__ctor_m5734 (VuforiaManager_t472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaManager::.cctor()
extern "C" void VuforiaManager__cctor_m5735 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
