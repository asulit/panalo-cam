﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Transform
struct Transform_t35;

// System.Void Common.TransformExtensions::SetEulerAngleY(UnityEngine.Transform,System.Single)
extern "C" void TransformExtensions_SetEulerAngleY_m440 (Object_t * __this /* static, unused */, Transform_t35 * ___transform, float ___angle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.TransformExtensions::SetEulerAngleZ(UnityEngine.Transform,System.Single)
extern "C" void TransformExtensions_SetEulerAngleZ_m441 (Object_t * __this /* static, unused */, Transform_t35 * ___transform, float ___angle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
