﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>::.ctor(System.Object,System.IntPtr)
// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_4MethodDeclarations.h"
#define UnityAction_1__ctor_m19107(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t2826 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m18971_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>::Invoke(T0)
#define UnityAction_1_Invoke_m19108(__this, ___arg0, method) (( void (*) (UnityAction_1_t2826 *, List_1_t344 *, const MethodInfo*))UnityAction_1_Invoke_m18972_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m19109(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t2826 *, List_1_t344 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m18973_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m19110(__this, ___result, method) (( void (*) (UnityAction_1_t2826 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m18974_gshared)(__this, ___result, method)
