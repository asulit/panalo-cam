﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m21728(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3003 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m14803_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>::Invoke(T)
#define Predicate_1_Invoke_m21729(__this, ___obj, method) (( bool (*) (Predicate_1_t3003 *, MatEntry_t638 *, const MethodInfo*))Predicate_1_Invoke_m14804_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m21730(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3003 *, MatEntry_t638 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m14805_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m21731(__this, ___result, method) (( bool (*) (Predicate_1_t3003 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m14806_gshared)(__this, ___result, method)
