﻿#pragma once
#include <stdint.h>
// System.Enum
struct Enum_t21;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Enum>
struct  Predicate_1_t2494  : public MulticastDelegate_t28
{
};
