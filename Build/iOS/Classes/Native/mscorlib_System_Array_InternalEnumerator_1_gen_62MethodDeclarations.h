﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_62.h"
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23093_gshared (InternalEnumerator_1_t3109 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m23093(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3109 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m23093_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23094_gshared (InternalEnumerator_1_t3109 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23094(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3109 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23094_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23095_gshared (InternalEnumerator_1_t3109 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23095(__this, method) (( void (*) (InternalEnumerator_1_t3109 *, const MethodInfo*))InternalEnumerator_1_Dispose_m23095_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23096_gshared (InternalEnumerator_1_t3109 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23096(__this, method) (( bool (*) (InternalEnumerator_1_t3109 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m23096_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern "C" GcAchievementData_t939  InternalEnumerator_1_get_Current_m23097_gshared (InternalEnumerator_1_t3109 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23097(__this, method) (( GcAchievementData_t939  (*) (InternalEnumerator_1_t3109 *, const MethodInfo*))InternalEnumerator_1_get_Current_m23097_gshared)(__this, method)
