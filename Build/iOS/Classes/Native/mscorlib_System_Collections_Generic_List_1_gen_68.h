﻿#pragma once
#include <stdint.h>
// Vuforia.Surface[]
struct SurfaceU5BU5D_t3395;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.Surface>
struct  List_1_t1270  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.Surface>::_items
	SurfaceU5BU5D_t3395* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::_version
	int32_t ____version_3;
};
struct List_1_t1270_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.Surface>::EmptyArray
	SurfaceU5BU5D_t3395* ___EmptyArray_4;
};
