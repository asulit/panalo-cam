﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t2894;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;
// System.Object[]
struct ObjectU5BU5D_t356;
// System.Predicate`1<System.Object>
struct Predicate_1_t2476;
// System.Comparison`1<System.Object>
struct Comparison_1_t2482;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C" void IndexedSet_1__ctor_m20065_gshared (IndexedSet_1_t2894 * __this, const MethodInfo* method);
#define IndexedSet_1__ctor_m20065(__this, method) (( void (*) (IndexedSet_1_t2894 *, const MethodInfo*))IndexedSet_1__ctor_m20065_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20067_gshared (IndexedSet_1_t2894 * __this, const MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20067(__this, method) (( Object_t * (*) (IndexedSet_1_t2894 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20067_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C" void IndexedSet_1_Add_m20069_gshared (IndexedSet_1_t2894 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Add_m20069(__this, ___item, method) (( void (*) (IndexedSet_1_t2894 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m20069_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C" bool IndexedSet_1_Remove_m20071_gshared (IndexedSet_1_t2894 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Remove_m20071(__this, ___item, method) (( bool (*) (IndexedSet_1_t2894 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m20071_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C" Object_t* IndexedSet_1_GetEnumerator_m20073_gshared (IndexedSet_1_t2894 * __this, const MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m20073(__this, method) (( Object_t* (*) (IndexedSet_1_t2894 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m20073_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C" void IndexedSet_1_Clear_m20075_gshared (IndexedSet_1_t2894 * __this, const MethodInfo* method);
#define IndexedSet_1_Clear_m20075(__this, method) (( void (*) (IndexedSet_1_t2894 *, const MethodInfo*))IndexedSet_1_Clear_m20075_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C" bool IndexedSet_1_Contains_m20077_gshared (IndexedSet_1_t2894 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Contains_m20077(__this, ___item, method) (( bool (*) (IndexedSet_1_t2894 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m20077_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void IndexedSet_1_CopyTo_m20079_gshared (IndexedSet_1_t2894 * __this, ObjectU5BU5D_t356* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define IndexedSet_1_CopyTo_m20079(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t2894 *, ObjectU5BU5D_t356*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m20079_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C" int32_t IndexedSet_1_get_Count_m20081_gshared (IndexedSet_1_t2894 * __this, const MethodInfo* method);
#define IndexedSet_1_get_Count_m20081(__this, method) (( int32_t (*) (IndexedSet_1_t2894 *, const MethodInfo*))IndexedSet_1_get_Count_m20081_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C" bool IndexedSet_1_get_IsReadOnly_m20083_gshared (IndexedSet_1_t2894 * __this, const MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m20083(__this, method) (( bool (*) (IndexedSet_1_t2894 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m20083_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C" int32_t IndexedSet_1_IndexOf_m20085_gshared (IndexedSet_1_t2894 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_IndexOf_m20085(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t2894 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m20085_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C" void IndexedSet_1_Insert_m20087_gshared (IndexedSet_1_t2894 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Insert_m20087(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t2894 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m20087_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C" void IndexedSet_1_RemoveAt_m20089_gshared (IndexedSet_1_t2894 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_RemoveAt_m20089(__this, ___index, method) (( void (*) (IndexedSet_1_t2894 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m20089_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * IndexedSet_1_get_Item_m20091_gshared (IndexedSet_1_t2894 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_get_Item_m20091(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t2894 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m20091_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C" void IndexedSet_1_set_Item_m20093_gshared (IndexedSet_1_t2894 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define IndexedSet_1_set_Item_m20093(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t2894 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m20093_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" void IndexedSet_1_RemoveAll_m20094_gshared (IndexedSet_1_t2894 * __this, Predicate_1_t2476 * ___match, const MethodInfo* method);
#define IndexedSet_1_RemoveAll_m20094(__this, ___match, method) (( void (*) (IndexedSet_1_t2894 *, Predicate_1_t2476 *, const MethodInfo*))IndexedSet_1_RemoveAll_m20094_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void IndexedSet_1_Sort_m20095_gshared (IndexedSet_1_t2894 * __this, Comparison_1_t2482 * ___sortLayoutFunction, const MethodInfo* method);
#define IndexedSet_1_Sort_m20095(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t2894 *, Comparison_1_t2482 *, const MethodInfo*))IndexedSet_1_Sort_m20095_gshared)(__this, ___sortLayoutFunction, method)
