﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Animator
struct Animator_t236;
// System.Collections.IEnumerator
struct IEnumerator_t337;

// System.Void Animator::.ctor()
extern "C" void Animator__ctor_m843 (Animator_t236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Animator::Awake()
extern "C" void Animator_Awake_m844 (Animator_t236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Animator::OnEnable()
extern "C" void Animator_OnEnable_m845 (Animator_t236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Animator::OnDisable()
extern "C" void Animator_OnDisable_m846 (Animator_t236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Animator::PlayAnimationLoop(System.Single)
extern "C" Object_t * Animator_PlayAnimationLoop_m847 (Animator_t236 * __this, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Animator::PlayAnimation(System.Single)
extern "C" void Animator_PlayAnimation_m848 (Animator_t236 * __this, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Animator::get_Loop()
extern "C" bool Animator_get_Loop_m849 (Animator_t236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Animator::set_Loop(System.Boolean)
extern "C" void Animator_set_Loop_m850 (Animator_t236 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
