﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Nullable`1<UnityEngine.Vector3>
#include "mscorlib_System_Nullable_1_gen.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(T)
extern "C" void Nullable_1__ctor_m18639_gshared (Nullable_1_t445 * __this, Vector3_t36  ___value, const MethodInfo* method);
#define Nullable_1__ctor_m18639(__this, ___value, method) (( void (*) (Nullable_1_t445 *, Vector3_t36 , const MethodInfo*))Nullable_1__ctor_m18639_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m1735_gshared (Nullable_1_t445 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1735(__this, method) (( bool (*) (Nullable_1_t445 *, const MethodInfo*))Nullable_1_get_HasValue_m1735_gshared)(__this, method)
// T System.Nullable`1<UnityEngine.Vector3>::get_Value()
extern "C" Vector3_t36  Nullable_1_get_Value_m1736_gshared (Nullable_1_t445 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1736(__this, method) (( Vector3_t36  (*) (Nullable_1_t445 *, const MethodInfo*))Nullable_1_get_Value_m1736_gshared)(__this, method)
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m18640_gshared (Nullable_1_t445 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m18640(__this, ___other, method) (( bool (*) (Nullable_1_t445 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m18640_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m18641_gshared (Nullable_1_t445 * __this, Nullable_1_t445  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m18641(__this, ___other, method) (( bool (*) (Nullable_1_t445 *, Nullable_1_t445 , const MethodInfo*))Nullable_1_Equals_m18641_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<UnityEngine.Vector3>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m18642_gshared (Nullable_1_t445 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m18642(__this, method) (( int32_t (*) (Nullable_1_t445 *, const MethodInfo*))Nullable_1_GetHashCode_m18642_gshared)(__this, method)
// System.String System.Nullable`1<UnityEngine.Vector3>::ToString()
extern "C" String_t* Nullable_1_ToString_m18643_gshared (Nullable_1_t445 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m18643(__this, method) (( String_t* (*) (Nullable_1_t445 *, const MethodInfo*))Nullable_1_ToString_m18643_gshared)(__this, method)
