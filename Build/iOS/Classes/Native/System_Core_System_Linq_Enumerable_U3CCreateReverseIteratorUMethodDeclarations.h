﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>
struct U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2499;

// System.Void System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::.ctor()
extern "C" void U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m14993_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m14993(__this, method) (( void (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m14993_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C" Object_t * U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m14994_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m14994(__this, method) (( Object_t * (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m14994_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m14995_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m14995(__this, method) (( Object_t * (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m14995_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m14996_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m14996(__this, method) (( Object_t * (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m14996_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C" Object_t* U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m14997_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m14997(__this, method) (( Object_t* (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m14997_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::MoveNext()
extern "C" bool U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m14998_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m14998(__this, method) (( bool (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m14998_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::Dispose()
extern "C" void U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m14999_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m14999(__this, method) (( void (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m14999_gshared)(__this, method)
