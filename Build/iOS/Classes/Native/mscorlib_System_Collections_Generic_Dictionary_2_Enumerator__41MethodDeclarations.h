﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"
#define Enumerator__ctor_m27068(__this, ___dictionary, method) (( void (*) (Enumerator_t3379 *, Dictionary_2_t1187 *, const MethodInfo*))Enumerator__ctor_m15140_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27069(__this, method) (( Object_t * (*) (Enumerator_t3379 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15141_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27070(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3379 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15142_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27071(__this, method) (( Object_t * (*) (Enumerator_t3379 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15143_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27072(__this, method) (( Object_t * (*) (Enumerator_t3379 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::MoveNext()
#define Enumerator_MoveNext_m27073(__this, method) (( bool (*) (Enumerator_t3379 *, const MethodInfo*))Enumerator_MoveNext_m15145_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Current()
#define Enumerator_get_Current_m27074(__this, method) (( KeyValuePair_2_t3378  (*) (Enumerator_t3379 *, const MethodInfo*))Enumerator_get_Current_m15146_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m27075(__this, method) (( String_t* (*) (Enumerator_t3379 *, const MethodInfo*))Enumerator_get_CurrentKey_m15147_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m27076(__this, method) (( List_1_t1186 * (*) (Enumerator_t3379 *, const MethodInfo*))Enumerator_get_CurrentValue_m15148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::VerifyState()
#define Enumerator_VerifyState_m27077(__this, method) (( void (*) (Enumerator_t3379 *, const MethodInfo*))Enumerator_VerifyState_m15149_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m27078(__this, method) (( void (*) (Enumerator_t3379 *, const MethodInfo*))Enumerator_VerifyCurrent_m15150_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Dispose()
#define Enumerator_Dispose_m27079(__this, method) (( void (*) (Enumerator_t3379 *, const MethodInfo*))Enumerator_Dispose_m15151_gshared)(__this, method)
