﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_53MethodDeclarations.h"
#define ValueCollection__ctor_m26875(__this, ___dictionary, method) (( void (*) (ValueCollection_t1328 *, Dictionary_2_t1185 *, const MethodInfo*))ValueCollection__ctor_m19852_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m26876(__this, ___item, method) (( void (*) (ValueCollection_t1328 *, WordAbstractBehaviour_t327 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19853_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m26877(__this, method) (( void (*) (ValueCollection_t1328 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19854_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m26878(__this, ___item, method) (( bool (*) (ValueCollection_t1328 *, WordAbstractBehaviour_t327 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19855_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m26879(__this, ___item, method) (( bool (*) (ValueCollection_t1328 *, WordAbstractBehaviour_t327 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19856_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m26880(__this, method) (( Object_t* (*) (ValueCollection_t1328 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19857_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m26881(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1328 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m19858_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m26882(__this, method) (( Object_t * (*) (ValueCollection_t1328 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19859_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m26883(__this, method) (( bool (*) (ValueCollection_t1328 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19860_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m26884(__this, method) (( bool (*) (ValueCollection_t1328 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19861_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m26885(__this, method) (( Object_t * (*) (ValueCollection_t1328 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m19862_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m26886(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1328 *, WordAbstractBehaviourU5BU5D_t1325*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m19863_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::GetEnumerator()
#define ValueCollection_GetEnumerator_m7327(__this, method) (( Enumerator_t1327  (*) (ValueCollection_t1328 *, const MethodInfo*))ValueCollection_GetEnumerator_m19864_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.WordAbstractBehaviour>::get_Count()
#define ValueCollection_get_Count_m26887(__this, method) (( int32_t (*) (ValueCollection_t1328 *, const MethodInfo*))ValueCollection_get_Count_m19865_gshared)(__this, method)
