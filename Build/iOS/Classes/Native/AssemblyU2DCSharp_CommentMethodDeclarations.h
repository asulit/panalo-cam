﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Comment
struct Comment_t10;
// System.String
struct String_t;

// System.Void Comment::.ctor()
extern "C" void Comment__ctor_m36 (Comment_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Comment::Awake()
extern "C" void Comment_Awake_m37 (Comment_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Comment::get_Text()
extern "C" String_t* Comment_get_Text_m38 (Comment_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Comment::set_Text(System.String)
extern "C" void Comment_set_Text_m39 (Comment_t10 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
