﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
struct InvokableCall_1_t2891;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t706;
// System.Object[]
struct ObjectU5BU5D_t356;

// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m20051_gshared (InvokableCall_1_t2891 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m20051(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t2891 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m20051_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m20052_gshared (InvokableCall_1_t2891 * __this, UnityAction_1_t706 * ___action, const MethodInfo* method);
#define InvokableCall_1__ctor_m20052(__this, ___action, method) (( void (*) (InvokableCall_1_t2891 *, UnityAction_1_t706 *, const MethodInfo*))InvokableCall_1__ctor_m20052_gshared)(__this, ___action, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m20053_gshared (InvokableCall_1_t2891 * __this, ObjectU5BU5D_t356* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m20053(__this, ___args, method) (( void (*) (InvokableCall_1_t2891 *, ObjectU5BU5D_t356*, const MethodInfo*))InvokableCall_1_Invoke_m20053_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m20054_gshared (InvokableCall_1_t2891 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m20054(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t2891 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m20054_gshared)(__this, ___targetObj, ___method, method)
