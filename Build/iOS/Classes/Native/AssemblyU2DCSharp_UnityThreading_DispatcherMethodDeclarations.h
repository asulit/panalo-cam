﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.Dispatcher
struct Dispatcher_t162;
// UnityThreading.TaskBase
struct TaskBase_t163;
// System.Threading.WaitHandle
struct WaitHandle_t351;

// System.Void UnityThreading.Dispatcher::.ctor()
extern "C" void Dispatcher__ctor_m535 (Dispatcher_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.Dispatcher::.ctor(System.Boolean)
extern "C" void Dispatcher__ctor_m536 (Dispatcher_t162 * __this, bool ___setThreadDefaults, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.TaskBase UnityThreading.Dispatcher::get_CurrentTask()
extern "C" TaskBase_t163 * Dispatcher_get_CurrentTask_m537 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.Dispatcher UnityThreading.Dispatcher::get_Current()
extern "C" Dispatcher_t162 * Dispatcher_get_Current_m538 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.Dispatcher::set_Current(UnityThreading.Dispatcher)
extern "C" void Dispatcher_set_Current_m539 (Object_t * __this /* static, unused */, Dispatcher_t162 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityThreading.Dispatcher UnityThreading.Dispatcher::get_Main()
extern "C" Dispatcher_t162 * Dispatcher_get_Main_m540 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.Dispatcher::ProcessTasks()
extern "C" void Dispatcher_ProcessTasks_m541 (Dispatcher_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityThreading.Dispatcher::ProcessTasks(System.Threading.WaitHandle)
extern "C" bool Dispatcher_ProcessTasks_m542 (Dispatcher_t162 * __this, WaitHandle_t351 * ___exitHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityThreading.Dispatcher::ProcessNextTask()
extern "C" bool Dispatcher_ProcessNextTask_m543 (Dispatcher_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityThreading.Dispatcher::ProcessNextTask(System.Threading.WaitHandle)
extern "C" bool Dispatcher_ProcessNextTask_m544 (Dispatcher_t162 * __this, WaitHandle_t351 * ___exitHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.Dispatcher::ProcessTasksInternal()
extern "C" void Dispatcher_ProcessTasksInternal_m545 (Dispatcher_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.Dispatcher::ProcessSingleTask()
extern "C" void Dispatcher_ProcessSingleTask_m546 (Dispatcher_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.Dispatcher::RunTask(UnityThreading.TaskBase)
extern "C" void Dispatcher_RunTask_m547 (Dispatcher_t162 * __this, TaskBase_t163 * ___task, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.Dispatcher::CheckAccessLimitation()
extern "C" void Dispatcher_CheckAccessLimitation_m548 (Dispatcher_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.Dispatcher::Dispose()
extern "C" void Dispatcher_Dispose_m549 (Dispatcher_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
