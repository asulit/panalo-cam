﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Predicate`1<System.Collections.Hashtable>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m18612(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2789 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m14803_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<System.Collections.Hashtable>::Invoke(T)
#define Predicate_1_Invoke_m18613(__this, ___obj, method) (( bool (*) (Predicate_1_t2789 *, Hashtable_t261 *, const MethodInfo*))Predicate_1_Invoke_m14804_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<System.Collections.Hashtable>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m18614(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2789 *, Hashtable_t261 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m14805_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<System.Collections.Hashtable>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m18615(__this, ___result, method) (( bool (*) (Predicate_1_t2789 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m14806_gshared)(__this, ___result, method)
