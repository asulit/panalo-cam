﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t2751;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2738;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m18030_gshared (ShimEnumerator_t2751 * __this, Dictionary_2_t2738 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m18030(__this, ___host, method) (( void (*) (ShimEnumerator_t2751 *, Dictionary_2_t2738 *, const MethodInfo*))ShimEnumerator__ctor_m18030_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m18031_gshared (ShimEnumerator_t2751 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m18031(__this, method) (( bool (*) (ShimEnumerator_t2751 *, const MethodInfo*))ShimEnumerator_MoveNext_m18031_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m18032_gshared (ShimEnumerator_t2751 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m18032(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t2751 *, const MethodInfo*))ShimEnumerator_get_Entry_m18032_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m18033_gshared (ShimEnumerator_t2751 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m18033(__this, method) (( Object_t * (*) (ShimEnumerator_t2751 *, const MethodInfo*))ShimEnumerator_get_Key_m18033_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m18034_gshared (ShimEnumerator_t2751 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m18034(__this, method) (( Object_t * (*) (ShimEnumerator_t2751 *, const MethodInfo*))ShimEnumerator_get_Value_m18034_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m18035_gshared (ShimEnumerator_t2751 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m18035(__this, method) (( Object_t * (*) (ShimEnumerator_t2751 *, const MethodInfo*))ShimEnumerator_get_Current_m18035_gshared)(__this, method)
