﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
struct Collection_1_t2942;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t602;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t3697;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t763;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Collection_1__ctor_m20820_gshared (Collection_1_t2942 * __this, const MethodInfo* method);
#define Collection_1__ctor_m20820(__this, method) (( void (*) (Collection_1_t2942 *, const MethodInfo*))Collection_1__ctor_m20820_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20821_gshared (Collection_1_t2942 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20821(__this, method) (( bool (*) (Collection_1_t2942 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20821_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m20822_gshared (Collection_1_t2942 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m20822(__this, ___array, ___index, method) (( void (*) (Collection_1_t2942 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m20822_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m20823_gshared (Collection_1_t2942 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m20823(__this, method) (( Object_t * (*) (Collection_1_t2942 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m20823_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m20824_gshared (Collection_1_t2942 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m20824(__this, ___value, method) (( int32_t (*) (Collection_1_t2942 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m20824_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m20825_gshared (Collection_1_t2942 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m20825(__this, ___value, method) (( bool (*) (Collection_1_t2942 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m20825_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m20826_gshared (Collection_1_t2942 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m20826(__this, ___value, method) (( int32_t (*) (Collection_1_t2942 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m20826_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m20827_gshared (Collection_1_t2942 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m20827(__this, ___index, ___value, method) (( void (*) (Collection_1_t2942 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m20827_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m20828_gshared (Collection_1_t2942 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m20828(__this, ___value, method) (( void (*) (Collection_1_t2942 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m20828_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m20829_gshared (Collection_1_t2942 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m20829(__this, method) (( bool (*) (Collection_1_t2942 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m20829_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m20830_gshared (Collection_1_t2942 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m20830(__this, method) (( Object_t * (*) (Collection_1_t2942 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m20830_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m20831_gshared (Collection_1_t2942 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m20831(__this, method) (( bool (*) (Collection_1_t2942 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m20831_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m20832_gshared (Collection_1_t2942 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m20832(__this, method) (( bool (*) (Collection_1_t2942 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m20832_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m20833_gshared (Collection_1_t2942 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m20833(__this, ___index, method) (( Object_t * (*) (Collection_1_t2942 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m20833_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m20834_gshared (Collection_1_t2942 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m20834(__this, ___index, ___value, method) (( void (*) (Collection_1_t2942 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m20834_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Add(T)
extern "C" void Collection_1_Add_m20835_gshared (Collection_1_t2942 * __this, UIVertex_t605  ___item, const MethodInfo* method);
#define Collection_1_Add_m20835(__this, ___item, method) (( void (*) (Collection_1_t2942 *, UIVertex_t605 , const MethodInfo*))Collection_1_Add_m20835_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Clear()
extern "C" void Collection_1_Clear_m20836_gshared (Collection_1_t2942 * __this, const MethodInfo* method);
#define Collection_1_Clear_m20836(__this, method) (( void (*) (Collection_1_t2942 *, const MethodInfo*))Collection_1_Clear_m20836_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ClearItems()
extern "C" void Collection_1_ClearItems_m20837_gshared (Collection_1_t2942 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m20837(__this, method) (( void (*) (Collection_1_t2942 *, const MethodInfo*))Collection_1_ClearItems_m20837_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool Collection_1_Contains_m20838_gshared (Collection_1_t2942 * __this, UIVertex_t605  ___item, const MethodInfo* method);
#define Collection_1_Contains_m20838(__this, ___item, method) (( bool (*) (Collection_1_t2942 *, UIVertex_t605 , const MethodInfo*))Collection_1_Contains_m20838_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m20839_gshared (Collection_1_t2942 * __this, UIVertexU5BU5D_t602* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m20839(__this, ___array, ___index, method) (( void (*) (Collection_1_t2942 *, UIVertexU5BU5D_t602*, int32_t, const MethodInfo*))Collection_1_CopyTo_m20839_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m20840_gshared (Collection_1_t2942 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m20840(__this, method) (( Object_t* (*) (Collection_1_t2942 *, const MethodInfo*))Collection_1_GetEnumerator_m20840_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m20841_gshared (Collection_1_t2942 * __this, UIVertex_t605  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m20841(__this, ___item, method) (( int32_t (*) (Collection_1_t2942 *, UIVertex_t605 , const MethodInfo*))Collection_1_IndexOf_m20841_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m20842_gshared (Collection_1_t2942 * __this, int32_t ___index, UIVertex_t605  ___item, const MethodInfo* method);
#define Collection_1_Insert_m20842(__this, ___index, ___item, method) (( void (*) (Collection_1_t2942 *, int32_t, UIVertex_t605 , const MethodInfo*))Collection_1_Insert_m20842_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m20843_gshared (Collection_1_t2942 * __this, int32_t ___index, UIVertex_t605  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m20843(__this, ___index, ___item, method) (( void (*) (Collection_1_t2942 *, int32_t, UIVertex_t605 , const MethodInfo*))Collection_1_InsertItem_m20843_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool Collection_1_Remove_m20844_gshared (Collection_1_t2942 * __this, UIVertex_t605  ___item, const MethodInfo* method);
#define Collection_1_Remove_m20844(__this, ___item, method) (( bool (*) (Collection_1_t2942 *, UIVertex_t605 , const MethodInfo*))Collection_1_Remove_m20844_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m20845_gshared (Collection_1_t2942 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m20845(__this, ___index, method) (( void (*) (Collection_1_t2942 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m20845_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m20846_gshared (Collection_1_t2942 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m20846(__this, ___index, method) (( void (*) (Collection_1_t2942 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m20846_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t Collection_1_get_Count_m20847_gshared (Collection_1_t2942 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m20847(__this, method) (( int32_t (*) (Collection_1_t2942 *, const MethodInfo*))Collection_1_get_Count_m20847_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t605  Collection_1_get_Item_m20848_gshared (Collection_1_t2942 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m20848(__this, ___index, method) (( UIVertex_t605  (*) (Collection_1_t2942 *, int32_t, const MethodInfo*))Collection_1_get_Item_m20848_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m20849_gshared (Collection_1_t2942 * __this, int32_t ___index, UIVertex_t605  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m20849(__this, ___index, ___value, method) (( void (*) (Collection_1_t2942 *, int32_t, UIVertex_t605 , const MethodInfo*))Collection_1_set_Item_m20849_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m20850_gshared (Collection_1_t2942 * __this, int32_t ___index, UIVertex_t605  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m20850(__this, ___index, ___item, method) (( void (*) (Collection_1_t2942 *, int32_t, UIVertex_t605 , const MethodInfo*))Collection_1_SetItem_m20850_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m20851_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m20851(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m20851_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ConvertItem(System.Object)
extern "C" UIVertex_t605  Collection_1_ConvertItem_m20852_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m20852(__this /* static, unused */, ___item, method) (( UIVertex_t605  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m20852_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m20853_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m20853(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m20853_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m20854_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m20854(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m20854_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m20855_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m20855(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m20855_gshared)(__this /* static, unused */, ___list, method)
