﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Vector2>
struct Comparer_1_t3056;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Vector2>
struct  Comparer_1_t3056  : public Object_t
{
};
struct Comparer_1_t3056_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::_default
	Comparer_1_t3056 * ____default_0;
};
