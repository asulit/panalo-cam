﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ScannerHudRoot
struct ScannerHudRoot_t212;

// System.Void ScannerHudRoot::.ctor()
extern "C" void ScannerHudRoot__ctor_m747 (ScannerHudRoot_t212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHudRoot::Awake()
extern "C" void ScannerHudRoot_Awake_m748 (ScannerHudRoot_t212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHudRoot::Start()
extern "C" void ScannerHudRoot_Start_m749 (ScannerHudRoot_t212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHudRoot::OnClickedScan()
extern "C" void ScannerHudRoot_OnClickedScan_m750 (ScannerHudRoot_t212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHudRoot::OnClickedFlash()
extern "C" void ScannerHudRoot_OnClickedFlash_m751 (ScannerHudRoot_t212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScannerHudRoot::OnClickedFocus()
extern "C" void ScannerHudRoot_OnClickedFocus_m752 (ScannerHudRoot_t212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
