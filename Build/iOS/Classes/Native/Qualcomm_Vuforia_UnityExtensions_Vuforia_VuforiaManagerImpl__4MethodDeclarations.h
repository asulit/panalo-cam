﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void WordResultData_t1138_marshal(const WordResultData_t1138& unmarshaled, WordResultData_t1138_marshaled& marshaled);
extern "C" void WordResultData_t1138_marshal_back(const WordResultData_t1138_marshaled& marshaled, WordResultData_t1138& unmarshaled);
extern "C" void WordResultData_t1138_marshal_cleanup(WordResultData_t1138_marshaled& marshaled);
