﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,Common.Time.TimeReference>
struct Dictionary_2_t409;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Common.Time.TimeReference>
struct  KeyCollection_t2618  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Common.Time.TimeReference>::dictionary
	Dictionary_2_t409 * ___dictionary_0;
};
