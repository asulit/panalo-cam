﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_85MethodDeclarations.h"
#define Transform_1__ctor_m28839(__this, ___object, ___method, method) (( void (*) (Transform_1_t3475 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m28817_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m28840(__this, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Transform_1_t3475 *, String_t*, ProfileData_t1226 , const MethodInfo*))Transform_1_Invoke_m28818_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m28841(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3475 *, String_t*, ProfileData_t1226 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m28819_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m28842(__this, ___result, method) (( DictionaryEntry_t451  (*) (Transform_1_t3475 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m28820_gshared)(__this, ___result, method)
