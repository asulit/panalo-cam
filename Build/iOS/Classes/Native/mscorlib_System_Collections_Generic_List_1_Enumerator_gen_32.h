﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.Enum>
struct List_1_t377;
// System.Enum
struct Enum_t21;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<System.Enum>
struct  Enumerator_t2495 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<System.Enum>::l
	List_1_t377 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Enum>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Enum>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<System.Enum>::current
	Enum_t21 * ___current_3;
};
