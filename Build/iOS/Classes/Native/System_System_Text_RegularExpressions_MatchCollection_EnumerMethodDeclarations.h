﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.MatchCollection/Enumerator
struct Enumerator_t1686;
// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_t1594;
// System.Object
struct Object_t;

// System.Void System.Text.RegularExpressions.MatchCollection/Enumerator::.ctor(System.Text.RegularExpressions.MatchCollection)
extern "C" void Enumerator__ctor_m9048 (Enumerator_t1686 * __this, MatchCollection_t1594 * ___coll, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.RegularExpressions.MatchCollection/Enumerator::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m9049 (Enumerator_t1686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.MatchCollection/Enumerator::System.Collections.IEnumerator.MoveNext()
extern "C" bool Enumerator_System_Collections_IEnumerator_MoveNext_m9050 (Enumerator_t1686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
