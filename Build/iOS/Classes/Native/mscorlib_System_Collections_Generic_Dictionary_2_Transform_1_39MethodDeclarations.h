﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.EventSystems.PointerEventData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_37MethodDeclarations.h"
#define Transform_1__ctor_m19889(__this, ___object, ___method, method) (( void (*) (Transform_1_t2868 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m19875_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.EventSystems.PointerEventData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m19890(__this, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Transform_1_t2868 *, int32_t, PointerEventData_t517 *, const MethodInfo*))Transform_1_Invoke_m19876_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.EventSystems.PointerEventData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m19891(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2868 *, int32_t, PointerEventData_t517 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m19877_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.EventSystems.PointerEventData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m19892(__this, ___result, method) (( DictionaryEntry_t451  (*) (Transform_1_t2868 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m19878_gshared)(__this, ___result, method)
