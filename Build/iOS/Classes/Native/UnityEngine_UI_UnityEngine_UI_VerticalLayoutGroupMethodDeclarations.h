﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_t673;

// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
extern "C" void VerticalLayoutGroup__ctor_m3230 (VerticalLayoutGroup_t673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
extern "C" void VerticalLayoutGroup_CalculateLayoutInputHorizontal_m3231 (VerticalLayoutGroup_t673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
extern "C" void VerticalLayoutGroup_CalculateLayoutInputVertical_m3232 (VerticalLayoutGroup_t673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
extern "C" void VerticalLayoutGroup_SetLayoutHorizontal_m3233 (VerticalLayoutGroup_t673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
extern "C" void VerticalLayoutGroup_SetLayoutVertical_m3234 (VerticalLayoutGroup_t673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
