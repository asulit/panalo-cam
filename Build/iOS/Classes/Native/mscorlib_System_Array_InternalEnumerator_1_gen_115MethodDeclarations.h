﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_115.h"
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"

// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29636_gshared (InternalEnumerator_1_t3548 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29636(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3548 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29636_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29637_gshared (InternalEnumerator_1_t3548 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29637(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3548 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29637_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29638_gshared (InternalEnumerator_1_t3548 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29638(__this, method) (( void (*) (InternalEnumerator_1_t3548 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29638_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29639_gshared (InternalEnumerator_1_t3548 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29639(__this, method) (( bool (*) (InternalEnumerator_1_t3548 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29639_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern "C" Mark_t1704  InternalEnumerator_1_get_Current_m29640_gshared (InternalEnumerator_1_t3548 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29640(__this, method) (( Mark_t1704  (*) (InternalEnumerator_1_t3548 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29640_gshared)(__this, method)
