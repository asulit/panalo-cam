﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.iOS.ActivityIndicatorStyle
#include "UnityEngine_UnityEngine_iOS_ActivityIndicatorStyle.h"
// UnityEngine.iOS.ActivityIndicatorStyle
struct  ActivityIndicatorStyle_t855 
{
	// System.Int32 UnityEngine.iOS.ActivityIndicatorStyle::value__
	int32_t ___value___1;
};
