﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Utils.SimpleEncryption
struct SimpleEncryption_t138;
// System.Byte[]
struct ByteU5BU5D_t139;
// System.String
struct String_t;

// System.Void Common.Utils.SimpleEncryption::.ctor(System.Byte[],System.String)
extern "C" void SimpleEncryption__ctor_m454 (SimpleEncryption_t138 * __this, ByteU5BU5D_t139* ___salt, String_t* ___password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Common.Utils.SimpleEncryption::Encrypt(System.Byte[],System.Byte[],System.Byte[])
extern "C" ByteU5BU5D_t139* SimpleEncryption_Encrypt_m455 (Object_t * __this /* static, unused */, ByteU5BU5D_t139* ___clearData, ByteU5BU5D_t139* ___Key, ByteU5BU5D_t139* ___IV, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Utils.SimpleEncryption::Encrypt(System.String)
extern "C" String_t* SimpleEncryption_Encrypt_m456 (SimpleEncryption_t138 * __this, String_t* ___clearText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Common.Utils.SimpleEncryption::Encrypt(System.Byte[])
extern "C" ByteU5BU5D_t139* SimpleEncryption_Encrypt_m457 (SimpleEncryption_t138 * __this, ByteU5BU5D_t139* ___clearData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Common.Utils.SimpleEncryption::Decrypt(System.Byte[],System.Byte[],System.Byte[])
extern "C" ByteU5BU5D_t139* SimpleEncryption_Decrypt_m458 (Object_t * __this /* static, unused */, ByteU5BU5D_t139* ___cipherData, ByteU5BU5D_t139* ___Key, ByteU5BU5D_t139* ___IV, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Utils.SimpleEncryption::Decrypt(System.String)
extern "C" String_t* SimpleEncryption_Decrypt_m459 (SimpleEncryption_t138 * __this, String_t* ___cipherText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Common.Utils.SimpleEncryption::Decrypt(System.Byte[])
extern "C" ByteU5BU5D_t139* SimpleEncryption_Decrypt_m460 (SimpleEncryption_t138 * __this, ByteU5BU5D_t139* ___cipherData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Utils.SimpleEncryption::CanDecrypt(System.String)
extern "C" bool SimpleEncryption_CanDecrypt_m461 (SimpleEncryption_t138 * __this, String_t* ___encrypted, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Utils.SimpleEncryption::TryDecrypt(System.String,System.String&)
extern "C" bool SimpleEncryption_TryDecrypt_m462 (SimpleEncryption_t138 * __this, String_t* ___encrypted, String_t** ___decrypted, const MethodInfo* method) IL2CPP_METHOD_ATTR;
