﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.InputField
struct InputField_t226;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,System.Boolean>
struct Dictionary_2_t227;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,System.String>
struct Dictionary_2_t228;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// RegisterSceneRoot
struct  RegisterSceneRoot_t225  : public MonoBehaviour_t5
{
	// UnityEngine.UI.InputField RegisterSceneRoot::firstnameField
	InputField_t226 * ___firstnameField_2;
	// UnityEngine.UI.InputField RegisterSceneRoot::lastnameField
	InputField_t226 * ___lastnameField_3;
	// UnityEngine.UI.InputField RegisterSceneRoot::mobileField
	InputField_t226 * ___mobileField_4;
	// UnityEngine.UI.InputField RegisterSceneRoot::companyField
	InputField_t226 * ___companyField_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,System.Boolean> RegisterSceneRoot::prevFocusValue
	Dictionary_2_t227 * ___prevFocusValue_6;
	// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,System.String> RegisterSceneRoot::defaultValue
	Dictionary_2_t228 * ___defaultValue_7;
	// System.Boolean RegisterSceneRoot::hasValidFields
	bool ___hasValidFields_8;
};
