﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Query.ConcreteQueryRequest
struct ConcreteQueryRequest_t106;
// System.String
struct String_t;
// System.Object
struct Object_t;

// System.Void Common.Query.ConcreteQueryRequest::.ctor()
extern "C" void ConcreteQueryRequest__ctor_m361 (ConcreteQueryRequest_t106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Query.ConcreteQueryRequest::Clear()
extern "C" void ConcreteQueryRequest_Clear_m362 (ConcreteQueryRequest_t106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Query.ConcreteQueryRequest::get_QueryId()
extern "C" String_t* ConcreteQueryRequest_get_QueryId_m363 (ConcreteQueryRequest_t106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Query.ConcreteQueryRequest::set_QueryId(System.String)
extern "C" void ConcreteQueryRequest_set_QueryId_m364 (ConcreteQueryRequest_t106 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Query.ConcreteQueryRequest::AddParameter(System.String,System.Object)
extern "C" void ConcreteQueryRequest_AddParameter_m365 (ConcreteQueryRequest_t106 * __this, String_t* ___paramId, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Common.Query.ConcreteQueryRequest::GetParameter(System.String)
extern "C" Object_t * ConcreteQueryRequest_GetParameter_m366 (ConcreteQueryRequest_t106 * __this, String_t* ___paramId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
