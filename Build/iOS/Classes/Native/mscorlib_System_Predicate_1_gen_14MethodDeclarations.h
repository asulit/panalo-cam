﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.CameraDevice/FocusMode>
struct Predicate_1_t2731;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"

// System.Void System.Predicate`1<Vuforia.CameraDevice/FocusMode>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m17838_gshared (Predicate_1_t2731 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m17838(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2731 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m17838_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.CameraDevice/FocusMode>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m17839_gshared (Predicate_1_t2731 * __this, int32_t ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m17839(__this, ___obj, method) (( bool (*) (Predicate_1_t2731 *, int32_t, const MethodInfo*))Predicate_1_Invoke_m17839_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.CameraDevice/FocusMode>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m17840_gshared (Predicate_1_t2731 * __this, int32_t ___obj, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m17840(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2731 *, int32_t, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m17840_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.CameraDevice/FocusMode>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m17841_gshared (Predicate_1_t2731 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m17841(__this, ___result, method) (( bool (*) (Predicate_1_t2731 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m17841_gshared)(__this, ___result, method)
