﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Array/InternalEnumerator`1<UnityThreading.TaskWorker>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m16961(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2654 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14611_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityThreading.TaskWorker>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16962(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2654 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14612_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityThreading.TaskWorker>::Dispose()
#define InternalEnumerator_1_Dispose_m16963(__this, method) (( void (*) (InternalEnumerator_1_t2654 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14613_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityThreading.TaskWorker>::MoveNext()
#define InternalEnumerator_1_MoveNext_m16964(__this, method) (( bool (*) (InternalEnumerator_1_t2654 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14614_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityThreading.TaskWorker>::get_Current()
#define InternalEnumerator_1_get_Current_m16965(__this, method) (( TaskWorker_t168 * (*) (InternalEnumerator_1_t2654 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14615_gshared)(__this, method)
