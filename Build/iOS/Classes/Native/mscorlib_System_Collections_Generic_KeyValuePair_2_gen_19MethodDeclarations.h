﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.Boolean>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18MethodDeclarations.h"
#define KeyValuePair_2__ctor_m18051(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2755 *, InputField_t226 *, bool, const MethodInfo*))KeyValuePair_2__ctor_m17953_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m18052(__this, method) (( InputField_t226 * (*) (KeyValuePair_2_t2755 *, const MethodInfo*))KeyValuePair_2_get_Key_m17954_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18053(__this, ___value, method) (( void (*) (KeyValuePair_2_t2755 *, InputField_t226 *, const MethodInfo*))KeyValuePair_2_set_Key_m17955_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m18054(__this, method) (( bool (*) (KeyValuePair_2_t2755 *, const MethodInfo*))KeyValuePair_2_get_Value_m17956_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18055(__this, ___value, method) (( void (*) (KeyValuePair_2_t2755 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m17957_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m18056(__this, method) (( String_t* (*) (KeyValuePair_2_t2755 *, const MethodInfo*))KeyValuePair_2_ToString_m17958_gshared)(__this, method)
