﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIContent
struct GUIContent_t379;
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
// UnityEngine.GUIWordWrapSizer
struct  GUIWordWrapSizer_t910  : public GUILayoutEntry_t907
{
	// UnityEngine.GUIContent UnityEngine.GUIWordWrapSizer::m_Content
	GUIContent_t379 * ___m_Content_10;
	// System.Single UnityEngine.GUIWordWrapSizer::m_ForcedMinHeight
	float ___m_ForcedMinHeight_11;
	// System.Single UnityEngine.GUIWordWrapSizer::m_ForcedMaxHeight
	float ___m_ForcedMaxHeight_12;
};
