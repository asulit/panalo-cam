﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<InputLayer>
struct List_1_t63;
// InputLayer
struct InputLayer_t56;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<InputLayer>
struct  Enumerator_t2541 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<InputLayer>::l
	List_1_t63 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<InputLayer>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<InputLayer>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<InputLayer>::current
	InputLayer_t56 * ___current_3;
};
