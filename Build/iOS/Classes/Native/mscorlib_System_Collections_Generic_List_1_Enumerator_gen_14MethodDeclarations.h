﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t679;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m22752_gshared (Enumerator_t1311 * __this, List_1_t679 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m22752(__this, ___l, method) (( void (*) (Enumerator_t1311 *, List_1_t679 *, const MethodInfo*))Enumerator__ctor_m22752_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22753_gshared (Enumerator_t1311 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22753(__this, method) (( Object_t * (*) (Enumerator_t1311 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22753_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m7289_gshared (Enumerator_t1311 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m7289(__this, method) (( void (*) (Enumerator_t1311 *, const MethodInfo*))Enumerator_Dispose_m7289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m22754_gshared (Enumerator_t1311 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22754(__this, method) (( void (*) (Enumerator_t1311 *, const MethodInfo*))Enumerator_VerifyState_m22754_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m7288_gshared (Enumerator_t1311 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m7288(__this, method) (( bool (*) (Enumerator_t1311 *, const MethodInfo*))Enumerator_MoveNext_m7288_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m7285_gshared (Enumerator_t1311 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m7285(__this, method) (( int32_t (*) (Enumerator_t1311 *, const MethodInfo*))Enumerator_get_Current_m7285_gshared)(__this, method)
