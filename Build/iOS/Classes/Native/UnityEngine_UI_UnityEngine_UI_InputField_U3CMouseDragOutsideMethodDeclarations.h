﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4
struct U3CMouseDragOutsideRectU3Ec__Iterator4_t601;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::.ctor()
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4__ctor_m2518 (U3CMouseDragOutsideRectU3Ec__Iterator4_t601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CMouseDragOutsideRectU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2519 (U3CMouseDragOutsideRectU3Ec__Iterator4_t601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CMouseDragOutsideRectU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m2520 (U3CMouseDragOutsideRectU3Ec__Iterator4_t601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::MoveNext()
extern "C" bool U3CMouseDragOutsideRectU3Ec__Iterator4_MoveNext_m2521 (U3CMouseDragOutsideRectU3Ec__Iterator4_t601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::Dispose()
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_Dispose_m2522 (U3CMouseDragOutsideRectU3Ec__Iterator4_t601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator4::Reset()
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_Reset_m2523 (U3CMouseDragOutsideRectU3Ec__Iterator4_t601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
