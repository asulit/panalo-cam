﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreadHelper/<CreateThread>c__AnonStorey13
struct U3CCreateThreadU3Ec__AnonStorey13_t176;
// UnityThreading.ActionThread
struct ActionThread_t171;

// System.Void UnityThreadHelper/<CreateThread>c__AnonStorey13::.ctor()
extern "C" void U3CCreateThreadU3Ec__AnonStorey13__ctor_m603 (U3CCreateThreadU3Ec__AnonStorey13_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreadHelper/<CreateThread>c__AnonStorey13::<>m__7(UnityThreading.ActionThread)
extern "C" void U3CCreateThreadU3Ec__AnonStorey13_U3CU3Em__7_m604 (U3CCreateThreadU3Ec__AnonStorey13_t176 * __this, ActionThread_t171 * ___thread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
