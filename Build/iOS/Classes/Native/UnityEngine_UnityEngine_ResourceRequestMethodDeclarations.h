﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ResourceRequest
struct ResourceRequest_t828;
// UnityEngine.Object
struct Object_t335;
struct Object_t335_marshaled;

// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m4175 (ResourceRequest_t828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t335 * ResourceRequest_get_asset_m4176 (ResourceRequest_t828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
