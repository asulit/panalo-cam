﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SecurityContext
struct SecurityContext_t2250;
// System.Threading.CompressedStack
struct CompressedStack_t2251;

// System.Void System.Security.SecurityContext::.ctor()
extern "C" void SecurityContext__ctor_m13332 (SecurityContext_t2250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityContext::.ctor(System.Security.SecurityContext)
extern "C" void SecurityContext__ctor_m13333 (SecurityContext_t2250 * __this, SecurityContext_t2250 * ___sc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityContext System.Security.SecurityContext::Capture()
extern "C" SecurityContext_t2250 * SecurityContext_Capture_m13334 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityContext::get_FlowSuppressed()
extern "C" bool SecurityContext_get_FlowSuppressed_m13335 (SecurityContext_t2250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.CompressedStack System.Security.SecurityContext::get_CompressedStack()
extern "C" CompressedStack_t2251 * SecurityContext_get_CompressedStack_m13336 (SecurityContext_t2250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
