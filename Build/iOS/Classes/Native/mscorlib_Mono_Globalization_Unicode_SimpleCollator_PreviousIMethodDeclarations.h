﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_PreviousI.h"

// System.Void Mono.Globalization.Unicode.SimpleCollator/PreviousInfo::.ctor(System.Boolean)
extern "C" void PreviousInfo__ctor_m10442 (PreviousInfo_t1814 * __this, bool ___dummy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void PreviousInfo_t1814_marshal(const PreviousInfo_t1814& unmarshaled, PreviousInfo_t1814_marshaled& marshaled);
extern "C" void PreviousInfo_t1814_marshal_back(const PreviousInfo_t1814_marshaled& marshaled, PreviousInfo_t1814& unmarshaled);
extern "C" void PreviousInfo_t1814_marshal_cleanup(PreviousInfo_t1814_marshaled& marshaled);
