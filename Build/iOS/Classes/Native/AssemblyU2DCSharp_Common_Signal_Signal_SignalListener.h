﻿#pragma once
#include <stdint.h>
// Common.Signal.ISignalParameters
struct ISignalParameters_t115;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// Common.Signal.Signal/SignalListener
struct  SignalListener_t114  : public MulticastDelegate_t28
{
};
