﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// MeshShaderSetter
struct  MeshShaderSetter_t83  : public MonoBehaviour_t5
{
	// System.String MeshShaderSetter::shaderToSet
	String_t* ___shaderToSet_2;
};
