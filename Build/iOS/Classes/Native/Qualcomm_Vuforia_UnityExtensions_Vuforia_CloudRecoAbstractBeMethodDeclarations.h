﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CloudRecoAbstractBehaviour
struct CloudRecoAbstractBehaviour_t271;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t1253;

// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoEnabled()
extern "C" bool CloudRecoAbstractBehaviour_get_CloudRecoEnabled_m5372 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::set_CloudRecoEnabled(System.Boolean)
extern "C" void CloudRecoAbstractBehaviour_set_CloudRecoEnabled_m5373 (CloudRecoAbstractBehaviour_t271 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoInitialized()
extern "C" bool CloudRecoAbstractBehaviour_get_CloudRecoInitialized_m5374 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Initialize()
extern "C" void CloudRecoAbstractBehaviour_Initialize_m5375 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Deinitialize()
extern "C" void CloudRecoAbstractBehaviour_Deinitialize_m5376 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::CheckInitialization()
extern "C" void CloudRecoAbstractBehaviour_CheckInitialization_m5377 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StartCloudReco()
extern "C" void CloudRecoAbstractBehaviour_StartCloudReco_m5378 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StopCloudReco()
extern "C" void CloudRecoAbstractBehaviour_StopCloudReco_m5379 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::RegisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern "C" void CloudRecoAbstractBehaviour_RegisterEventHandler_m5380 (CloudRecoAbstractBehaviour_t271 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::UnregisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern "C" bool CloudRecoAbstractBehaviour_UnregisterEventHandler_m5381 (CloudRecoAbstractBehaviour_t271 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnEnable()
extern "C" void CloudRecoAbstractBehaviour_OnEnable_m5382 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDisable()
extern "C" void CloudRecoAbstractBehaviour_OnDisable_m5383 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Start()
extern "C" void CloudRecoAbstractBehaviour_Start_m5384 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Update()
extern "C" void CloudRecoAbstractBehaviour_Update_m5385 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDestroy()
extern "C" void CloudRecoAbstractBehaviour_OnDestroy_m5386 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnVuforiaStarted()
extern "C" void CloudRecoAbstractBehaviour_OnVuforiaStarted_m5387 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::.ctor()
extern "C" void CloudRecoAbstractBehaviour__ctor_m1808 (CloudRecoAbstractBehaviour_t271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
