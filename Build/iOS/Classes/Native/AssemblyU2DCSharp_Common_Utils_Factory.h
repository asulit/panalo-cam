﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t134;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Utils.Factory
struct  Factory_t133  : public Object_t
{
};
struct Factory_t133_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> Common.Utils.Factory::factoryMap
	Dictionary_2_t134 * ___factoryMap_0;
};
