﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17519(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2704 *, int32_t, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m17424_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m17520(__this, method) (( int32_t (*) (KeyValuePair_2_t2704 *, const MethodInfo*))KeyValuePair_2_get_Key_m17425_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17521(__this, ___value, method) (( void (*) (KeyValuePair_2_t2704 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m17426_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m17522(__this, method) (( String_t* (*) (KeyValuePair_2_t2704 *, const MethodInfo*))KeyValuePair_2_get_Value_m17427_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17523(__this, ___value, method) (( void (*) (KeyValuePair_2_t2704 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m17428_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>::ToString()
#define KeyValuePair_2_ToString_m17524(__this, method) (( String_t* (*) (KeyValuePair_2_t2704 *, const MethodInfo*))KeyValuePair_2_ToString_m17429_gshared)(__this, method)
