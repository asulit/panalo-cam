﻿#pragma once
#include <stdint.h>
// Common.Utils.SimpleList`1<SwarmItem>
struct SimpleList_1_t112;
// SwarmItemManager
#include "AssemblyU2DCSharp_SwarmItemManager.h"
// SelfManagingSwarmItemManager
struct  SelfManagingSwarmItemManager_t99  : public SwarmItemManager_t111
{
	// Common.Utils.SimpleList`1<SwarmItem> SelfManagingSwarmItemManager::preloadList
	SimpleList_1_t112 * ___preloadList_11;
};
