﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture2D
struct Texture2D_t355;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.VideoTextureRendererAbstractBehaviour
struct  VideoTextureRendererAbstractBehaviour_t317  : public MonoBehaviour_t5
{
	// UnityEngine.Texture2D Vuforia.VideoTextureRendererAbstractBehaviour::mTexture
	Texture2D_t355 * ___mTexture_2;
	// System.Boolean Vuforia.VideoTextureRendererAbstractBehaviour::mVideoBgConfigChanged
	bool ___mVideoBgConfigChanged_3;
	// System.Int32 Vuforia.VideoTextureRendererAbstractBehaviour::mNativeTextureID
	int32_t ___mNativeTextureID_4;
};
