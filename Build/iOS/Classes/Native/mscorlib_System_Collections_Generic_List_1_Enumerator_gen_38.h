﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>
struct List_1_t214;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>
struct  Enumerator_t2725 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>::l
	List_1_t214 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/FocusMode>::current
	int32_t ___current_3;
};
