﻿#pragma once
#include <stdint.h>
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t319;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.VirtualButtonAbstractBehaviour>
struct  Comparison_1_t3457  : public MulticastDelegate_t28
{
};
