﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LoadingSceneRoot/<PrepareFsm>c__AnonStorey1B
struct U3CPrepareFsmU3Ec__AnonStorey1B_t222;

// System.Void LoadingSceneRoot/<PrepareFsm>c__AnonStorey1B::.ctor()
extern "C" void U3CPrepareFsmU3Ec__AnonStorey1B__ctor_m770 (U3CPrepareFsmU3Ec__AnonStorey1B_t222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot/<PrepareFsm>c__AnonStorey1B::<>m__19()
extern "C" void U3CPrepareFsmU3Ec__AnonStorey1B_U3CU3Em__19_m771 (U3CPrepareFsmU3Ec__AnonStorey1B_t222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
