﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Signal.StringToSignalMapper
struct StringToSignalMapper_t118;
// Common.Signal.Signal
struct Signal_t116;
// System.String
struct String_t;

// System.Void Common.Signal.StringToSignalMapper::.ctor()
extern "C" void StringToSignalMapper__ctor_m404 (StringToSignalMapper_t118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Signal.StringToSignalMapper::Add(Common.Signal.Signal)
extern "C" void StringToSignalMapper_Add_m405 (StringToSignalMapper_t118 * __this, Signal_t116 * ___signal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Signal.StringToSignalMapper::Dispatch(System.String)
extern "C" void StringToSignalMapper_Dispatch_m406 (StringToSignalMapper_t118 * __this, String_t* ___signalName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Signal.StringToSignalMapper::IsEmpty()
extern "C" bool StringToSignalMapper_IsEmpty_m407 (StringToSignalMapper_t118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
