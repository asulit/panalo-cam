﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Int64>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_121.h"

// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m29666_gshared (InternalEnumerator_1_t3554 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m29666(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3554 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m29666_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29667_gshared (InternalEnumerator_1_t3554 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29667(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3554 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29667_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m29668_gshared (InternalEnumerator_1_t3554 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m29668(__this, method) (( void (*) (InternalEnumerator_1_t3554 *, const MethodInfo*))InternalEnumerator_1_Dispose_m29668_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m29669_gshared (InternalEnumerator_1_t3554 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m29669(__this, method) (( bool (*) (InternalEnumerator_1_t3554 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m29669_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C" int64_t InternalEnumerator_1_get_Current_m29670_gshared (InternalEnumerator_1_t3554 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m29670(__this, method) (( int64_t (*) (InternalEnumerator_1_t3554 *, const MethodInfo*))InternalEnumerator_1_get_Current_m29670_gshared)(__this, method)
