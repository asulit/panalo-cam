﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// PrefabManager
struct PrefabManager_t96;
// System.Object
#include "mscorlib_System_Object.h"
// PrefabManager/<Preload>c__Iterator1
struct  U3CPreloadU3Ec__Iterator1_t95  : public Object_t
{
	// System.Int32 PrefabManager/<Preload>c__Iterator1::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Int32 PrefabManager/<Preload>c__Iterator1::$PC
	int32_t ___U24PC_1;
	// System.Object PrefabManager/<Preload>c__Iterator1::$current
	Object_t * ___U24current_2;
	// PrefabManager PrefabManager/<Preload>c__Iterator1::<>f__this
	PrefabManager_t96 * ___U3CU3Ef__this_3;
};
