﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PanaloVideoPlayer/<Play>c__IteratorB
struct U3CPlayU3Ec__IteratorB_t245;
// System.Object
struct Object_t;

// System.Void PanaloVideoPlayer/<Play>c__IteratorB::.ctor()
extern "C" void U3CPlayU3Ec__IteratorB__ctor_m876 (U3CPlayU3Ec__IteratorB_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PanaloVideoPlayer/<Play>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CPlayU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m877 (U3CPlayU3Ec__IteratorB_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PanaloVideoPlayer/<Play>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CPlayU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m878 (U3CPlayU3Ec__IteratorB_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PanaloVideoPlayer/<Play>c__IteratorB::MoveNext()
extern "C" bool U3CPlayU3Ec__IteratorB_MoveNext_m879 (U3CPlayU3Ec__IteratorB_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloVideoPlayer/<Play>c__IteratorB::Dispose()
extern "C" void U3CPlayU3Ec__IteratorB_Dispose_m880 (U3CPlayU3Ec__IteratorB_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloVideoPlayer/<Play>c__IteratorB::Reset()
extern "C" void U3CPlayU3Ec__IteratorB_Reset_m881 (U3CPlayU3Ec__IteratorB_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
