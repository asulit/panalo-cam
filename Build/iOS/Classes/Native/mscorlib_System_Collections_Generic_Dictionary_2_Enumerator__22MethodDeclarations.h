﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2869;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m19836_gshared (Enumerator_t2875 * __this, Dictionary_2_t2869 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m19836(__this, ___dictionary, method) (( void (*) (Enumerator_t2875 *, Dictionary_2_t2869 *, const MethodInfo*))Enumerator__ctor_m19836_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m19837_gshared (Enumerator_t2875 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m19837(__this, method) (( Object_t * (*) (Enumerator_t2875 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19837_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19838_gshared (Enumerator_t2875 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19838(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t2875 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19838_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19839_gshared (Enumerator_t2875 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19839(__this, method) (( Object_t * (*) (Enumerator_t2875 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19839_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19840_gshared (Enumerator_t2875 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19840(__this, method) (( Object_t * (*) (Enumerator_t2875 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19840_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m19841_gshared (Enumerator_t2875 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m19841(__this, method) (( bool (*) (Enumerator_t2875 *, const MethodInfo*))Enumerator_MoveNext_m19841_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2871  Enumerator_get_Current_m19842_gshared (Enumerator_t2875 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m19842(__this, method) (( KeyValuePair_2_t2871  (*) (Enumerator_t2875 *, const MethodInfo*))Enumerator_get_Current_m19842_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m19843_gshared (Enumerator_t2875 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m19843(__this, method) (( int32_t (*) (Enumerator_t2875 *, const MethodInfo*))Enumerator_get_CurrentKey_m19843_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m19844_gshared (Enumerator_t2875 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m19844(__this, method) (( Object_t * (*) (Enumerator_t2875 *, const MethodInfo*))Enumerator_get_CurrentValue_m19844_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m19845_gshared (Enumerator_t2875 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m19845(__this, method) (( void (*) (Enumerator_t2875 *, const MethodInfo*))Enumerator_VerifyState_m19845_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m19846_gshared (Enumerator_t2875 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m19846(__this, method) (( void (*) (Enumerator_t2875 *, const MethodInfo*))Enumerator_VerifyCurrent_m19846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m19847_gshared (Enumerator_t2875 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m19847(__this, method) (( void (*) (Enumerator_t2875 *, const MethodInfo*))Enumerator_Dispose_m19847_gshared)(__this, method)
