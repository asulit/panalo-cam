﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.Action.MoveAction
struct MoveAction_t34;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t35;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"

// System.Void Common.Fsm.Action.MoveAction::.ctor(Common.Fsm.FsmState)
extern "C" void MoveAction__ctor_m106 (MoveAction_t34 * __this, Object_t * ___owner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAction::.ctor(Common.Fsm.FsmState,System.String)
extern "C" void MoveAction__ctor_m107 (MoveAction_t34 * __this, Object_t * ___owner, String_t* ___timeReferenceName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAction::Init(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.String,UnityEngine.Space)
extern "C" void MoveAction_Init_m108 (MoveAction_t34 * __this, Transform_t35 * ___transform, Vector3_t36  ___positionFrom, Vector3_t36  ___positionTo, float ___duration, String_t* ___finishEvent, int32_t ___space, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAction::OnEnter()
extern "C" void MoveAction_OnEnter_m109 (MoveAction_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAction::OnUpdate()
extern "C" void MoveAction_OnUpdate_m110 (MoveAction_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAction::Finish()
extern "C" void MoveAction_Finish_m111 (MoveAction_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Action.MoveAction::SetPosition(UnityEngine.Vector3)
extern "C" void MoveAction_SetPosition_m112 (MoveAction_t34 * __this, Vector3_t36  ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
