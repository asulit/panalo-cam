﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Utils.Platform
struct Platform_t135;
// System.String
struct String_t;

// System.Void Common.Utils.Platform::.ctor()
extern "C" void Platform__ctor_m445 (Platform_t135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Utils.Platform::get_DeviceId()
extern "C" String_t* Platform_get_DeviceId_m446 (Platform_t135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Utils.Platform::IsEditor()
extern "C" bool Platform_IsEditor_m447 (Platform_t135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Utils.Platform::IsMobileAndroid()
extern "C" bool Platform_IsMobileAndroid_m448 (Platform_t135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Utils.Platform::IsMobileIOS()
extern "C" bool Platform_IsMobileIOS_m449 (Platform_t135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
