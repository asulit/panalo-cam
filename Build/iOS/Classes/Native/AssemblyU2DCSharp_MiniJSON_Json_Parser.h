﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.IO.StringReader
struct StringReader_t67;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t68;
// System.Object
#include "mscorlib_System_Object.h"
// MiniJSON.Json/Parser
struct  Parser_t66  : public Object_t
{
	// System.IO.StringReader MiniJSON.Json/Parser::json
	StringReader_t67 * ___json_1;
};
struct Parser_t66_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> MiniJSON.Json/Parser::<>f__switch$map0
	Dictionary_2_t68 * ___U3CU3Ef__switchU24map0_2;
};
