﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Action`1<Vuforia.Prop>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_10MethodDeclarations.h"
#define Action_1__ctor_m1820(__this, ___object, ___method, method) (( void (*) (Action_1_t458 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m16966_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<Vuforia.Prop>::Invoke(T)
#define Action_1_Invoke_m7399(__this, ___obj, method) (( void (*) (Action_1_t458 *, Object_t *, const MethodInfo*))Action_1_Invoke_m16967_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<Vuforia.Prop>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m18651(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t458 *, Object_t *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m16969_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<Vuforia.Prop>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m18652(__this, ___result, method) (( void (*) (Action_1_t458 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m16971_gshared)(__this, ___result, method)
