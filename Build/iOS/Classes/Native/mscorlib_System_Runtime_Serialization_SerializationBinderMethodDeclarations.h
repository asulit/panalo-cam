﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2166;

// System.Void System.Runtime.Serialization.SerializationBinder::.ctor()
extern "C" void SerializationBinder__ctor_m12866 (SerializationBinder_t2166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
