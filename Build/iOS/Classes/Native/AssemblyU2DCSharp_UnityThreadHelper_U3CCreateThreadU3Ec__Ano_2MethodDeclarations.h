﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreadHelper/<CreateThread>c__AnonStorey15
struct U3CCreateThreadU3Ec__AnonStorey15_t178;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// UnityThreading.ThreadBase
struct ThreadBase_t169;

// System.Void UnityThreadHelper/<CreateThread>c__AnonStorey15::.ctor()
extern "C" void U3CCreateThreadU3Ec__AnonStorey15__ctor_m607 (U3CCreateThreadU3Ec__AnonStorey15_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityThreadHelper/<CreateThread>c__AnonStorey15::<>m__9(UnityThreading.ThreadBase)
extern "C" Object_t * U3CCreateThreadU3Ec__AnonStorey15_U3CU3Em__9_m608 (U3CCreateThreadU3Ec__AnonStorey15_t178 * __this, ThreadBase_t169 * ___thread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
