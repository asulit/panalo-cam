﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>
struct Predicate_1_t3278;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m25362_gshared (Predicate_1_t3278 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m25362(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3278 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m25362_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m25363_gshared (Predicate_1_t3278 * __this, int32_t ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m25363(__this, ___obj, method) (( bool (*) (Predicate_1_t3278 *, int32_t, const MethodInfo*))Predicate_1_Invoke_m25363_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m25364_gshared (Predicate_1_t3278 * __this, int32_t ___obj, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m25364(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3278 *, int32_t, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m25364_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m25365_gshared (Predicate_1_t3278 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m25365(__this, ___result, method) (( bool (*) (Predicate_1_t3278 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m25365_gshared)(__this, ___result, method)
