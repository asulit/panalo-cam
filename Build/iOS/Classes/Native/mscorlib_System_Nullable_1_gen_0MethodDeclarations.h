﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen_0.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C" void Nullable_1__ctor_m14604_gshared (Nullable_1_t2426 * __this, TimeSpan_t427  ___value, const MethodInfo* method);
#define Nullable_1__ctor_m14604(__this, ___value, method) (( void (*) (Nullable_1_t2426 *, TimeSpan_t427 , const MethodInfo*))Nullable_1__ctor_m14604_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m14605_gshared (Nullable_1_t2426 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m14605(__this, method) (( bool (*) (Nullable_1_t2426 *, const MethodInfo*))Nullable_1_get_HasValue_m14605_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C" TimeSpan_t427  Nullable_1_get_Value_m14606_gshared (Nullable_1_t2426 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m14606(__this, method) (( TimeSpan_t427  (*) (Nullable_1_t2426 *, const MethodInfo*))Nullable_1_get_Value_m14606_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m29998_gshared (Nullable_1_t2426 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m29998(__this, ___other, method) (( bool (*) (Nullable_1_t2426 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m29998_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m29999_gshared (Nullable_1_t2426 * __this, Nullable_1_t2426  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m29999(__this, ___other, method) (( bool (*) (Nullable_1_t2426 *, Nullable_1_t2426 , const MethodInfo*))Nullable_1_Equals_m29999_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m30000_gshared (Nullable_1_t2426 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m30000(__this, method) (( int32_t (*) (Nullable_1_t2426 *, const MethodInfo*))Nullable_1_GetHashCode_m30000_gshared)(__this, method)
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C" String_t* Nullable_1_ToString_m30001_gshared (Nullable_1_t2426 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m30001(__this, method) (( String_t* (*) (Nullable_1_t2426 *, const MethodInfo*))Nullable_1_ToString_m30001_gshared)(__this, method)
