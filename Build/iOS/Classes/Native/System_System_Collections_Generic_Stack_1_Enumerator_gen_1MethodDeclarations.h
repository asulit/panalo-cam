﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Stack`1/Enumerator<SwarmItem>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m16249(__this, ___t, method) (( void (*) (Enumerator_t2605 *, Stack_1_t153 *, const MethodInfo*))Enumerator__ctor_m15969_gshared)(__this, ___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<SwarmItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16250(__this, method) (( Object_t * (*) (Enumerator_t2605 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15970_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<SwarmItem>::Dispose()
#define Enumerator_Dispose_m16251(__this, method) (( void (*) (Enumerator_t2605 *, const MethodInfo*))Enumerator_Dispose_m15971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<SwarmItem>::MoveNext()
#define Enumerator_MoveNext_m16252(__this, method) (( bool (*) (Enumerator_t2605 *, const MethodInfo*))Enumerator_MoveNext_m15972_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<SwarmItem>::get_Current()
#define Enumerator_get_Current_m16253(__this, method) (( SwarmItem_t124 * (*) (Enumerator_t2605 *, const MethodInfo*))Enumerator_get_Current_m15973_gshared)(__this, method)
