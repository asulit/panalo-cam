﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>
struct Collection_1_t3463;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t3458;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t1292;
// System.Collections.Generic.IList`1<Vuforia.TargetFinder/TargetSearchResult>
struct IList_1_t3462;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void Collection_1__ctor_m28495_gshared (Collection_1_t3463 * __this, const MethodInfo* method);
#define Collection_1__ctor_m28495(__this, method) (( void (*) (Collection_1_t3463 *, const MethodInfo*))Collection_1__ctor_m28495_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28496_gshared (Collection_1_t3463 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28496(__this, method) (( bool (*) (Collection_1_t3463 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28496_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m28497_gshared (Collection_1_t3463 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m28497(__this, ___array, ___index, method) (( void (*) (Collection_1_t3463 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m28497_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m28498_gshared (Collection_1_t3463 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m28498(__this, method) (( Object_t * (*) (Collection_1_t3463 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m28498_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m28499_gshared (Collection_1_t3463 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m28499(__this, ___value, method) (( int32_t (*) (Collection_1_t3463 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m28499_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m28500_gshared (Collection_1_t3463 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m28500(__this, ___value, method) (( bool (*) (Collection_1_t3463 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m28500_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m28501_gshared (Collection_1_t3463 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m28501(__this, ___value, method) (( int32_t (*) (Collection_1_t3463 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m28501_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m28502_gshared (Collection_1_t3463 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m28502(__this, ___index, ___value, method) (( void (*) (Collection_1_t3463 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m28502_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m28503_gshared (Collection_1_t3463 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m28503(__this, ___value, method) (( void (*) (Collection_1_t3463 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m28503_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m28504_gshared (Collection_1_t3463 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m28504(__this, method) (( bool (*) (Collection_1_t3463 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m28504_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m28505_gshared (Collection_1_t3463 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m28505(__this, method) (( Object_t * (*) (Collection_1_t3463 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m28505_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m28506_gshared (Collection_1_t3463 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m28506(__this, method) (( bool (*) (Collection_1_t3463 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m28506_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m28507_gshared (Collection_1_t3463 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m28507(__this, method) (( bool (*) (Collection_1_t3463 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m28507_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m28508_gshared (Collection_1_t3463 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m28508(__this, ___index, method) (( Object_t * (*) (Collection_1_t3463 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m28508_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m28509_gshared (Collection_1_t3463 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m28509(__this, ___index, ___value, method) (( void (*) (Collection_1_t3463 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m28509_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C" void Collection_1_Add_m28510_gshared (Collection_1_t3463 * __this, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define Collection_1_Add_m28510(__this, ___item, method) (( void (*) (Collection_1_t3463 *, TargetSearchResult_t1212 , const MethodInfo*))Collection_1_Add_m28510_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C" void Collection_1_Clear_m28511_gshared (Collection_1_t3463 * __this, const MethodInfo* method);
#define Collection_1_Clear_m28511(__this, method) (( void (*) (Collection_1_t3463 *, const MethodInfo*))Collection_1_Clear_m28511_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ClearItems()
extern "C" void Collection_1_ClearItems_m28512_gshared (Collection_1_t3463 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m28512(__this, method) (( void (*) (Collection_1_t3463 *, const MethodInfo*))Collection_1_ClearItems_m28512_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C" bool Collection_1_Contains_m28513_gshared (Collection_1_t3463 * __this, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define Collection_1_Contains_m28513(__this, ___item, method) (( bool (*) (Collection_1_t3463 *, TargetSearchResult_t1212 , const MethodInfo*))Collection_1_Contains_m28513_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m28514_gshared (Collection_1_t3463 * __this, TargetSearchResultU5BU5D_t3458* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m28514(__this, ___array, ___index, method) (( void (*) (Collection_1_t3463 *, TargetSearchResultU5BU5D_t3458*, int32_t, const MethodInfo*))Collection_1_CopyTo_m28514_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m28515_gshared (Collection_1_t3463 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m28515(__this, method) (( Object_t* (*) (Collection_1_t3463 *, const MethodInfo*))Collection_1_GetEnumerator_m28515_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m28516_gshared (Collection_1_t3463 * __this, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m28516(__this, ___item, method) (( int32_t (*) (Collection_1_t3463 *, TargetSearchResult_t1212 , const MethodInfo*))Collection_1_IndexOf_m28516_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m28517_gshared (Collection_1_t3463 * __this, int32_t ___index, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define Collection_1_Insert_m28517(__this, ___index, ___item, method) (( void (*) (Collection_1_t3463 *, int32_t, TargetSearchResult_t1212 , const MethodInfo*))Collection_1_Insert_m28517_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m28518_gshared (Collection_1_t3463 * __this, int32_t ___index, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m28518(__this, ___index, ___item, method) (( void (*) (Collection_1_t3463 *, int32_t, TargetSearchResult_t1212 , const MethodInfo*))Collection_1_InsertItem_m28518_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C" bool Collection_1_Remove_m28519_gshared (Collection_1_t3463 * __this, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define Collection_1_Remove_m28519(__this, ___item, method) (( bool (*) (Collection_1_t3463 *, TargetSearchResult_t1212 , const MethodInfo*))Collection_1_Remove_m28519_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m28520_gshared (Collection_1_t3463 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m28520(__this, ___index, method) (( void (*) (Collection_1_t3463 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m28520_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m28521_gshared (Collection_1_t3463 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m28521(__this, ___index, method) (( void (*) (Collection_1_t3463 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m28521_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C" int32_t Collection_1_get_Count_m28522_gshared (Collection_1_t3463 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m28522(__this, method) (( int32_t (*) (Collection_1_t3463 *, const MethodInfo*))Collection_1_get_Count_m28522_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern "C" TargetSearchResult_t1212  Collection_1_get_Item_m28523_gshared (Collection_1_t3463 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m28523(__this, ___index, method) (( TargetSearchResult_t1212  (*) (Collection_1_t3463 *, int32_t, const MethodInfo*))Collection_1_get_Item_m28523_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m28524_gshared (Collection_1_t3463 * __this, int32_t ___index, TargetSearchResult_t1212  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m28524(__this, ___index, ___value, method) (( void (*) (Collection_1_t3463 *, int32_t, TargetSearchResult_t1212 , const MethodInfo*))Collection_1_set_Item_m28524_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m28525_gshared (Collection_1_t3463 * __this, int32_t ___index, TargetSearchResult_t1212  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m28525(__this, ___index, ___item, method) (( void (*) (Collection_1_t3463 *, int32_t, TargetSearchResult_t1212 , const MethodInfo*))Collection_1_SetItem_m28525_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m28526_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m28526(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m28526_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ConvertItem(System.Object)
extern "C" TargetSearchResult_t1212  Collection_1_ConvertItem_m28527_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m28527(__this /* static, unused */, ___item, method) (( TargetSearchResult_t1212  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m28527_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m28528_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m28528(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m28528_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m28529_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m28529(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m28529_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m28530_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m28530(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m28530_gshared)(__this /* static, unused */, ___list, method)
