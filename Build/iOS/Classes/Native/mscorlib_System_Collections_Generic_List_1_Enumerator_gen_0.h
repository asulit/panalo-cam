﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>
struct List_1_t143;
// Common.Xml.SimpleXmlNode
struct SimpleXmlNode_t142;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Common.Xml.SimpleXmlNode>
struct  Enumerator_t421 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Common.Xml.SimpleXmlNode>::l
	List_1_t143 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Common.Xml.SimpleXmlNode>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Common.Xml.SimpleXmlNode>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Common.Xml.SimpleXmlNode>::current
	SimpleXmlNode_t142 * ___current_3;
};
