﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>
struct Dictionary_2_t48;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Fsm.FsmState>
struct  Enumerator_t2523 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Fsm.FsmState>::dictionary
	Dictionary_2_t48 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Fsm.FsmState>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Fsm.FsmState>::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Common.Fsm.FsmState>::current
	KeyValuePair_2_t2520  ___current_3;
};
