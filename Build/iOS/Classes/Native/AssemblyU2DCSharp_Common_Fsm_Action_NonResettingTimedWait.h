﻿#pragma once
#include <stdint.h>
// CountdownTimer
struct CountdownTimer_t13;
// System.String
struct String_t;
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapter.h"
// Common.Fsm.Action.NonResettingTimedWait
struct  NonResettingTimedWait_t39  : public FsmActionAdapter_t33
{
	// System.Single Common.Fsm.Action.NonResettingTimedWait::waitTime
	float ___waitTime_1;
	// CountdownTimer Common.Fsm.Action.NonResettingTimedWait::timer
	CountdownTimer_t13 * ___timer_2;
	// System.String Common.Fsm.Action.NonResettingTimedWait::timeReference
	String_t* ___timeReference_3;
	// System.String Common.Fsm.Action.NonResettingTimedWait::finishEvent
	String_t* ___finishEvent_4;
};
