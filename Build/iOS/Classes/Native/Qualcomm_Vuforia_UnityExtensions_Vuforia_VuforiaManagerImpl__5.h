﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Vuforia.VuforiaManagerImpl/WordData
#pragma pack(push, tp, 1)
struct  WordData_t1139 
{
	// System.IntPtr Vuforia.VuforiaManagerImpl/WordData::stringValue
	IntPtr_t ___stringValue_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/WordData::id
	int32_t ___id_1;
	// UnityEngine.Vector2 Vuforia.VuforiaManagerImpl/WordData::size
	Vector2_t2  ___size_2;
	// System.Int32 Vuforia.VuforiaManagerImpl/WordData::unused
	int32_t ___unused_3;
};
#pragma pack(pop, tp)
