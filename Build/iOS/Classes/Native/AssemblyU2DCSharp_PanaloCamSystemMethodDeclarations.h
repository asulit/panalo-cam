﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PanaloCamSystem
struct PanaloCamSystem_t195;
// Common.Signal.ISignalParameters
struct ISignalParameters_t115;

// System.Void PanaloCamSystem::.ctor()
extern "C" void PanaloCamSystem__ctor_m688 (PanaloCamSystem_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCamSystem::Awake()
extern "C" void PanaloCamSystem_Awake_m689 (PanaloCamSystem_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCamSystem::Start()
extern "C" void PanaloCamSystem_Start_m690 (PanaloCamSystem_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCamSystem::OnEnable()
extern "C" void PanaloCamSystem_OnEnable_m691 (PanaloCamSystem_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCamSystem::OnDisable()
extern "C" void PanaloCamSystem_OnDisable_m692 (PanaloCamSystem_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCamSystem::OnLoadTitleScreen(Common.Signal.ISignalParameters)
extern "C" void PanaloCamSystem_OnLoadTitleScreen_m693 (PanaloCamSystem_t195 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCamSystem::OnLoadScanScreen(Common.Signal.ISignalParameters)
extern "C" void PanaloCamSystem_OnLoadScanScreen_m694 (PanaloCamSystem_t195 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanaloCamSystem::OnAudioRecognized(Common.Signal.ISignalParameters)
extern "C" void PanaloCamSystem_OnAudioRecognized_m695 (PanaloCamSystem_t195 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
