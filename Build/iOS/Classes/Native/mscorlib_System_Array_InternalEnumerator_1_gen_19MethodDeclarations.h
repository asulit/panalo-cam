﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_19.h"
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15940_gshared (InternalEnumerator_1_t2577 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15940(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2577 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15940_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15941_gshared (InternalEnumerator_1_t2577 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15941(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2577 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15941_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15942_gshared (InternalEnumerator_1_t2577 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15942(__this, method) (( void (*) (InternalEnumerator_1_t2577 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15942_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15943_gshared (InternalEnumerator_1_t2577 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15943(__this, method) (( bool (*) (InternalEnumerator_1_t2577 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15943_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.BoneWeight>::get_Current()
extern "C" BoneWeight_t405  InternalEnumerator_1_get_Current_m15944_gshared (InternalEnumerator_1_t2577 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15944(__this, method) (( BoneWeight_t405  (*) (InternalEnumerator_1_t2577 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15944_gshared)(__this, method)
