﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Time.TimeReference
struct TimeReference_t14;
// System.String
struct String_t;

// System.Void Common.Time.TimeReference::.ctor(System.String)
extern "C" void TimeReference__ctor_m408 (TimeReference_t14 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Time.TimeReference::get_Name()
extern "C" String_t* TimeReference_get_Name_m409 (TimeReference_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Common.Time.TimeReference::get_TimeScale()
extern "C" float TimeReference_get_TimeScale_m410 (TimeReference_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Time.TimeReference::set_TimeScale(System.Single)
extern "C" void TimeReference_set_TimeScale_m411 (TimeReference_t14 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Common.Time.TimeReference::get_DeltaTime()
extern "C" float TimeReference_get_DeltaTime_m412 (TimeReference_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Time.TimeReference Common.Time.TimeReference::GetDefaultInstance()
extern "C" TimeReference_t14 * TimeReference_GetDefaultInstance_m413 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Time.TimeReference::get_AffectsGlobalTimeScale()
extern "C" bool TimeReference_get_AffectsGlobalTimeScale_m414 (TimeReference_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Time.TimeReference::set_AffectsGlobalTimeScale(System.Boolean)
extern "C" void TimeReference_set_AffectsGlobalTimeScale_m415 (TimeReference_t14 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
