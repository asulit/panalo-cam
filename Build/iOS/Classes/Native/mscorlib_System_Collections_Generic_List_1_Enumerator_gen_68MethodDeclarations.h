﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t895;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_68.h"
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m23541_gshared (Enumerator_t3141 * __this, List_1_t895 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m23541(__this, ___l, method) (( void (*) (Enumerator_t3141 *, List_1_t895 *, const MethodInfo*))Enumerator__ctor_m23541_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23542_gshared (Enumerator_t3141 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23542(__this, method) (( Object_t * (*) (Enumerator_t3141 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23542_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C" void Enumerator_Dispose_m23543_gshared (Enumerator_t3141 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m23543(__this, method) (( void (*) (Enumerator_t3141 *, const MethodInfo*))Enumerator_Dispose_m23543_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m23544_gshared (Enumerator_t3141 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m23544(__this, method) (( void (*) (Enumerator_t3141 *, const MethodInfo*))Enumerator_VerifyState_m23544_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23545_gshared (Enumerator_t3141 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m23545(__this, method) (( bool (*) (Enumerator_t3141 *, const MethodInfo*))Enumerator_MoveNext_m23545_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t752  Enumerator_get_Current_m23546_gshared (Enumerator_t3141 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m23546(__this, method) (( UILineInfo_t752  (*) (Enumerator_t3141 *, const MethodInfo*))Enumerator_get_Current_m23546_gshared)(__this, method)
