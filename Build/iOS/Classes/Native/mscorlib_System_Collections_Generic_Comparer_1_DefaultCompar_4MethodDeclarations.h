﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>
struct DefaultComparer_t3036;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::.ctor()
extern "C" void DefaultComparer__ctor_m22253_gshared (DefaultComparer_t3036 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22253(__this, method) (( void (*) (DefaultComparer_t3036 *, const MethodInfo*))DefaultComparer__ctor_m22253_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m22254_gshared (DefaultComparer_t3036 * __this, Vector3_t36  ___x, Vector3_t36  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m22254(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3036 *, Vector3_t36 , Vector3_t36 , const MethodInfo*))DefaultComparer_Compare_m22254_gshared)(__this, ___x, ___y, method)
