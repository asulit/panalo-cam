﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.ConstructionLevelActivator
struct ConstructionLevelActivator_t2062;

// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::.ctor()
extern "C" void ConstructionLevelActivator__ctor_m12329 (ConstructionLevelActivator_t2062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
