﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// TitleSceneRoot
struct TitleSceneRoot_t230;
// System.String
struct String_t;
// UnityEngine.UI.InputField
struct InputField_t226;

// System.Void TitleSceneRoot::.ctor()
extern "C" void TitleSceneRoot__ctor_m823 (TitleSceneRoot_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleSceneRoot::Awake()
extern "C" void TitleSceneRoot_Awake_m824 (TitleSceneRoot_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleSceneRoot::Start()
extern "C" void TitleSceneRoot_Start_m825 (TitleSceneRoot_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleSceneRoot::Update()
extern "C" void TitleSceneRoot_Update_m826 (TitleSceneRoot_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleSceneRoot::OnClickedPlay()
extern "C" void TitleSceneRoot_OnClickedPlay_m827 (TitleSceneRoot_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleSceneRoot::OnClickedSignUp()
extern "C" void TitleSceneRoot_OnClickedSignUp_m828 (TitleSceneRoot_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleSceneRoot::OnUsernameValueChanged()
extern "C" void TitleSceneRoot_OnUsernameValueChanged_m829 (TitleSceneRoot_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleSceneRoot::OnPasswordValueChanged()
extern "C" void TitleSceneRoot_OnPasswordValueChanged_m830 (TitleSceneRoot_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleSceneRoot::OnUsernameEndEdit(System.String)
extern "C" void TitleSceneRoot_OnUsernameEndEdit_m831 (TitleSceneRoot_t230 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleSceneRoot::OnPasswordEndEdit(System.String)
extern "C" void TitleSceneRoot_OnPasswordEndEdit_m832 (TitleSceneRoot_t230 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleSceneRoot::ValidateText(UnityEngine.UI.InputField,System.String)
extern "C" void TitleSceneRoot_ValidateText_m833 (TitleSceneRoot_t230 * __this, InputField_t226 * ___field, String_t* ___defaultText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleSceneRoot::ClearText(UnityEngine.UI.InputField)
extern "C" void TitleSceneRoot_ClearText_m834 (TitleSceneRoot_t230 * __this, InputField_t226 * ___field, const MethodInfo* method) IL2CPP_METHOD_ATTR;
