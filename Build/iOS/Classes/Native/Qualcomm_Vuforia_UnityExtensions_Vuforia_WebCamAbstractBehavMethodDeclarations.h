﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t323;
// System.String
struct String_t;
// Vuforia.WebCamImpl
struct WebCamImpl_t1105;

// System.Boolean Vuforia.WebCamAbstractBehaviour::get_PlayModeRenderVideo()
extern "C" bool WebCamAbstractBehaviour_get_PlayModeRenderVideo_m7148 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_PlayModeRenderVideo(System.Boolean)
extern "C" void WebCamAbstractBehaviour_set_PlayModeRenderVideo_m7149 (WebCamAbstractBehaviour_t323 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
extern "C" String_t* WebCamAbstractBehaviour_get_DeviceName_m7150 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
extern "C" void WebCamAbstractBehaviour_set_DeviceName_m7151 (WebCamAbstractBehaviour_t323 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
extern "C" bool WebCamAbstractBehaviour_get_FlipHorizontally_m7152 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
extern "C" void WebCamAbstractBehaviour_set_FlipHorizontally_m7153 (WebCamAbstractBehaviour_t323 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
extern "C" bool WebCamAbstractBehaviour_get_TurnOffWebCam_m7154 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
extern "C" void WebCamAbstractBehaviour_set_TurnOffWebCam_m7155 (WebCamAbstractBehaviour_t323 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
extern "C" bool WebCamAbstractBehaviour_get_IsPlaying_m7156 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
extern "C" bool WebCamAbstractBehaviour_IsWebCamUsed_m7157 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
extern "C" WebCamImpl_t1105 * WebCamAbstractBehaviour_get_ImplementationClass_m7158 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
extern "C" void WebCamAbstractBehaviour_InitCamera_m7159 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::CheckNativePluginSupport()
extern "C" bool WebCamAbstractBehaviour_CheckNativePluginSupport_m7160 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
extern "C" void WebCamAbstractBehaviour_OnLevelWasLoaded_m7161 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
extern "C" void WebCamAbstractBehaviour_OnDestroy_m7162 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
extern "C" void WebCamAbstractBehaviour_Update_m7163 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WebCamAbstractBehaviour::qcarCheckNativePluginSupport()
extern "C" int32_t WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m7164 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
extern "C" void WebCamAbstractBehaviour__ctor_m1886 (WebCamAbstractBehaviour_t323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
