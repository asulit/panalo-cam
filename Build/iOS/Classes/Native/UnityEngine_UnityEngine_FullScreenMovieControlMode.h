﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.FullScreenMovieControlMode
#include "UnityEngine_UnityEngine_FullScreenMovieControlMode.h"
// UnityEngine.FullScreenMovieControlMode
struct  FullScreenMovieControlMode_t820 
{
	// System.Int32 UnityEngine.FullScreenMovieControlMode::value__
	int32_t ___value___1;
};
