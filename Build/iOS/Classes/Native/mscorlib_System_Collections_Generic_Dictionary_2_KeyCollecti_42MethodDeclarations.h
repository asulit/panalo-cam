﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_4MethodDeclarations.h"
#define KeyCollection__ctor_m24588(__this, ___dictionary, method) (( void (*) (KeyCollection_t3228 *, Dictionary_2_t1054 *, const MethodInfo*))KeyCollection__ctor_m15121_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m24589(__this, ___item, method) (( void (*) (KeyCollection_t3228 *, Camera_t6 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15122_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m24590(__this, method) (( void (*) (KeyCollection_t3228 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m24591(__this, ___item, method) (( bool (*) (KeyCollection_t3228 *, Camera_t6 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15124_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m24592(__this, ___item, method) (( bool (*) (KeyCollection_t3228 *, Camera_t6 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15125_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m24593(__this, method) (( Object_t* (*) (KeyCollection_t3228 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15126_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m24594(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3228 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m15127_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m24595(__this, method) (( Object_t * (*) (KeyCollection_t3228 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15128_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m24596(__this, method) (( bool (*) (KeyCollection_t3228 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15129_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m24597(__this, method) (( bool (*) (KeyCollection_t3228 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15130_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m24598(__this, method) (( Object_t * (*) (KeyCollection_t3228 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m15131_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m24599(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3228 *, CameraU5BU5D_t471*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m15132_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::GetEnumerator()
#define KeyCollection_GetEnumerator_m24600(__this, method) (( Enumerator_t3745  (*) (KeyCollection_t3228 *, const MethodInfo*))KeyCollection_GetEnumerator_m15133_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Count()
#define KeyCollection_get_Count_m24601(__this, method) (( int32_t (*) (KeyCollection_t3228 *, const MethodInfo*))KeyCollection_get_Count_m15134_gshared)(__this, method)
