﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// BridgeHandler
struct BridgeHandler_t185;
// Common.Signal.ISignalParameters
struct ISignalParameters_t115;
// System.Collections.IEnumerator
struct IEnumerator_t337;

// System.Void BridgeHandler::.ctor()
extern "C" void BridgeHandler__ctor_m638 (BridgeHandler_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::.cctor()
extern "C" void BridgeHandler__cctor_m639 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::Awake()
extern "C" void BridgeHandler_Awake_m640 (BridgeHandler_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::Start()
extern "C" void BridgeHandler_Start_m641 (BridgeHandler_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::OnDestroy()
extern "C" void BridgeHandler_OnDestroy_m642 (BridgeHandler_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::OnShare(Common.Signal.ISignalParameters)
extern "C" void BridgeHandler_OnShare_m643 (BridgeHandler_t185 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::OnSendSMS(Common.Signal.ISignalParameters)
extern "C" void BridgeHandler_OnSendSMS_m644 (BridgeHandler_t185 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::OnStartAudioRecord(Common.Signal.ISignalParameters)
extern "C" void BridgeHandler_OnStartAudioRecord_m645 (BridgeHandler_t185 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BridgeHandler::OnStartAudioRecord()
extern "C" Object_t * BridgeHandler_OnStartAudioRecord_m646 (BridgeHandler_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::OnStopAudioRecord(Common.Signal.ISignalParameters)
extern "C" void BridgeHandler_OnStopAudioRecord_m647 (BridgeHandler_t185 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::OnCancelAudioRecord(Common.Signal.ISignalParameters)
extern "C" void BridgeHandler_OnCancelAudioRecord_m648 (BridgeHandler_t185 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::Share()
extern "C" void BridgeHandler_Share_m649 (BridgeHandler_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::SendSMS()
extern "C" void BridgeHandler_SendSMS_m650 (BridgeHandler_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::StartRecord()
extern "C" void BridgeHandler_StartRecord_m651 (BridgeHandler_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::StopRecord()
extern "C" void BridgeHandler_StopRecord_m652 (BridgeHandler_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::CancelRecord()
extern "C" void BridgeHandler_CancelRecord_m653 (BridgeHandler_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BridgeHandler::GetUserData()
extern "C" void BridgeHandler_GetUserData_m654 (BridgeHandler_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
