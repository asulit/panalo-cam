﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t1104;
// System.Collections.Generic.IEnumerable`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerable_1_t3751;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t3749;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ICollection_1_t3752;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ReadOnlyCollection_1_t3275;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3251;
// System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>
struct Predicate_1_t3278;
// System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparison_1_t3281;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_71.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C" void List_1__ctor_m7238_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1__ctor_m7238(__this, method) (( void (*) (List_1_t1104 *, const MethodInfo*))List_1__ctor_m7238_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m25239_gshared (List_1_t1104 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m25239(__this, ___collection, method) (( void (*) (List_1_t1104 *, Object_t*, const MethodInfo*))List_1__ctor_m25239_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Int32)
extern "C" void List_1__ctor_m25240_gshared (List_1_t1104 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m25240(__this, ___capacity, method) (( void (*) (List_1_t1104 *, int32_t, const MethodInfo*))List_1__ctor_m25240_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
extern "C" void List_1__cctor_m25241_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m25241(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m25241_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25242_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25242(__this, method) (( Object_t* (*) (List_1_t1104 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25242_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m25243_gshared (List_1_t1104 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m25243(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1104 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m25243_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m25244_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m25244(__this, method) (( Object_t * (*) (List_1_t1104 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m25244_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m25245_gshared (List_1_t1104 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m25245(__this, ___item, method) (( int32_t (*) (List_1_t1104 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m25245_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m25246_gshared (List_1_t1104 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m25246(__this, ___item, method) (( bool (*) (List_1_t1104 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m25246_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m25247_gshared (List_1_t1104 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m25247(__this, ___item, method) (( int32_t (*) (List_1_t1104 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m25247_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m25248_gshared (List_1_t1104 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m25248(__this, ___index, ___item, method) (( void (*) (List_1_t1104 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m25248_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m25249_gshared (List_1_t1104 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m25249(__this, ___item, method) (( void (*) (List_1_t1104 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m25249_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25250_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25250(__this, method) (( bool (*) (List_1_t1104 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25250_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m25251_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m25251(__this, method) (( bool (*) (List_1_t1104 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m25251_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m25252_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m25252(__this, method) (( Object_t * (*) (List_1_t1104 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m25252_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m25253_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m25253(__this, method) (( bool (*) (List_1_t1104 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m25253_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m25254_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m25254(__this, method) (( bool (*) (List_1_t1104 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m25254_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m25255_gshared (List_1_t1104 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m25255(__this, ___index, method) (( Object_t * (*) (List_1_t1104 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m25255_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m25256_gshared (List_1_t1104 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m25256(__this, ___index, ___value, method) (( void (*) (List_1_t1104 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m25256_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(T)
extern "C" void List_1_Add_m25257_gshared (List_1_t1104 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m25257(__this, ___item, method) (( void (*) (List_1_t1104 *, int32_t, const MethodInfo*))List_1_Add_m25257_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m25258_gshared (List_1_t1104 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m25258(__this, ___newCount, method) (( void (*) (List_1_t1104 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m25258_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m25259_gshared (List_1_t1104 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m25259(__this, ___idx, ___count, method) (( void (*) (List_1_t1104 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m25259_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m25260_gshared (List_1_t1104 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m25260(__this, ___collection, method) (( void (*) (List_1_t1104 *, Object_t*, const MethodInfo*))List_1_AddCollection_m25260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m25261_gshared (List_1_t1104 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m25261(__this, ___enumerable, method) (( void (*) (List_1_t1104 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m25261_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m25262_gshared (List_1_t1104 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m25262(__this, ___collection, method) (( void (*) (List_1_t1104 *, Object_t*, const MethodInfo*))List_1_AddRange_m25262_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3275 * List_1_AsReadOnly_m25263_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m25263(__this, method) (( ReadOnlyCollection_1_t3275 * (*) (List_1_t1104 *, const MethodInfo*))List_1_AsReadOnly_m25263_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Clear()
extern "C" void List_1_Clear_m25264_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_Clear_m25264(__this, method) (( void (*) (List_1_t1104 *, const MethodInfo*))List_1_Clear_m25264_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
extern "C" bool List_1_Contains_m25265_gshared (List_1_t1104 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m25265(__this, ___item, method) (( bool (*) (List_1_t1104 *, int32_t, const MethodInfo*))List_1_Contains_m25265_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m25266_gshared (List_1_t1104 * __this, PIXEL_FORMATU5BU5D_t3251* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m25266(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1104 *, PIXEL_FORMATU5BU5D_t3251*, int32_t, const MethodInfo*))List_1_CopyTo_m25266_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m25267_gshared (List_1_t1104 * __this, Predicate_1_t3278 * ___match, const MethodInfo* method);
#define List_1_Find_m25267(__this, ___match, method) (( int32_t (*) (List_1_t1104 *, Predicate_1_t3278 *, const MethodInfo*))List_1_Find_m25267_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m25268_gshared (Object_t * __this /* static, unused */, Predicate_1_t3278 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m25268(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3278 *, const MethodInfo*))List_1_CheckMatch_m25268_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m25269_gshared (List_1_t1104 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3278 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m25269(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1104 *, int32_t, int32_t, Predicate_1_t3278 *, const MethodInfo*))List_1_GetIndex_m25269_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
extern "C" Enumerator_t3274  List_1_GetEnumerator_m25270_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m25270(__this, method) (( Enumerator_t3274  (*) (List_1_t1104 *, const MethodInfo*))List_1_GetEnumerator_m25270_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m25271_gshared (List_1_t1104 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m25271(__this, ___item, method) (( int32_t (*) (List_1_t1104 *, int32_t, const MethodInfo*))List_1_IndexOf_m25271_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m25272_gshared (List_1_t1104 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m25272(__this, ___start, ___delta, method) (( void (*) (List_1_t1104 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m25272_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m25273_gshared (List_1_t1104 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m25273(__this, ___index, method) (( void (*) (List_1_t1104 *, int32_t, const MethodInfo*))List_1_CheckIndex_m25273_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m25274_gshared (List_1_t1104 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m25274(__this, ___index, ___item, method) (( void (*) (List_1_t1104 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m25274_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m25275_gshared (List_1_t1104 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m25275(__this, ___collection, method) (( void (*) (List_1_t1104 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m25275_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T)
extern "C" bool List_1_Remove_m25276_gshared (List_1_t1104 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m25276(__this, ___item, method) (( bool (*) (List_1_t1104 *, int32_t, const MethodInfo*))List_1_Remove_m25276_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m25277_gshared (List_1_t1104 * __this, Predicate_1_t3278 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m25277(__this, ___match, method) (( int32_t (*) (List_1_t1104 *, Predicate_1_t3278 *, const MethodInfo*))List_1_RemoveAll_m25277_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m25278_gshared (List_1_t1104 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m25278(__this, ___index, method) (( void (*) (List_1_t1104 *, int32_t, const MethodInfo*))List_1_RemoveAt_m25278_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m25279_gshared (List_1_t1104 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m25279(__this, ___index, ___count, method) (( void (*) (List_1_t1104 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m25279_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Reverse()
extern "C" void List_1_Reverse_m25280_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_Reverse_m25280(__this, method) (( void (*) (List_1_t1104 *, const MethodInfo*))List_1_Reverse_m25280_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort()
extern "C" void List_1_Sort_m25281_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_Sort_m25281(__this, method) (( void (*) (List_1_t1104 *, const MethodInfo*))List_1_Sort_m25281_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m25282_gshared (List_1_t1104 * __this, Comparison_1_t3281 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m25282(__this, ___comparison, method) (( void (*) (List_1_t1104 *, Comparison_1_t3281 *, const MethodInfo*))List_1_Sort_m25282_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::ToArray()
extern "C" PIXEL_FORMATU5BU5D_t3251* List_1_ToArray_m25283_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_ToArray_m25283(__this, method) (( PIXEL_FORMATU5BU5D_t3251* (*) (List_1_t1104 *, const MethodInfo*))List_1_ToArray_m25283_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::TrimExcess()
extern "C" void List_1_TrimExcess_m25284_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m25284(__this, method) (( void (*) (List_1_t1104 *, const MethodInfo*))List_1_TrimExcess_m25284_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m25285_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m25285(__this, method) (( int32_t (*) (List_1_t1104 *, const MethodInfo*))List_1_get_Capacity_m25285_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m25286_gshared (List_1_t1104 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m25286(__this, ___value, method) (( void (*) (List_1_t1104 *, int32_t, const MethodInfo*))List_1_set_Capacity_m25286_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
extern "C" int32_t List_1_get_Count_m25287_gshared (List_1_t1104 * __this, const MethodInfo* method);
#define List_1_get_Count_m25287(__this, method) (( int32_t (*) (List_1_t1104 *, const MethodInfo*))List_1_get_Count_m25287_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m25288_gshared (List_1_t1104 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m25288(__this, ___index, method) (( int32_t (*) (List_1_t1104 *, int32_t, const MethodInfo*))List_1_get_Item_m25288_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m25289_gshared (List_1_t1104 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m25289(__this, ___index, ___value, method) (( void (*) (List_1_t1104 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m25289_gshared)(__this, ___index, ___value, method)
