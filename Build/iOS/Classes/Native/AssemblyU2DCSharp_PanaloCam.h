﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Fsm.Fsm
struct Fsm_t47;
// Common.Fsm.FsmDelegateAction/FsmActionRoutine
struct FsmActionRoutine_t27;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// PanaloCam
struct  PanaloCam_t194  : public MonoBehaviour_t5
{
	// Common.Fsm.Fsm PanaloCam::fsm
	Fsm_t47 * ___fsm_11;
};
struct PanaloCam_t194_StaticFields{
	// Common.Fsm.FsmDelegateAction/FsmActionRoutine PanaloCam::<>f__am$cache1
	FsmActionRoutine_t27 * ___U3CU3Ef__amU24cache1_12;
	// Common.Fsm.FsmDelegateAction/FsmActionRoutine PanaloCam::<>f__am$cache2
	FsmActionRoutine_t27 * ___U3CU3Ef__amU24cache2_13;
	// Common.Fsm.FsmDelegateAction/FsmActionRoutine PanaloCam::<>f__am$cache3
	FsmActionRoutine_t27 * ___U3CU3Ef__amU24cache3_14;
	// Common.Fsm.FsmDelegateAction/FsmActionRoutine PanaloCam::<>f__am$cache4
	FsmActionRoutine_t27 * ___U3CU3Ef__amU24cache4_15;
	// Common.Fsm.FsmDelegateAction/FsmActionRoutine PanaloCam::<>f__am$cache5
	FsmActionRoutine_t27 * ___U3CU3Ef__amU24cache5_16;
};
