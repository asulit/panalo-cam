﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct EqualityComparer_1_t3436;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct  EqualityComparer_1_t3436  : public Object_t
{
};
struct EqualityComparer_1_t3436_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::_default
	EqualityComparer_1_t3436 * ____default_0;
};
