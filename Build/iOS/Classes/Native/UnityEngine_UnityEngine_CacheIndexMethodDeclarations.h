﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void CacheIndex_t837_marshal(const CacheIndex_t837& unmarshaled, CacheIndex_t837_marshaled& marshaled);
extern "C" void CacheIndex_t837_marshal_back(const CacheIndex_t837_marshaled& marshaled, CacheIndex_t837& unmarshaled);
extern "C" void CacheIndex_t837_marshal_cleanup(CacheIndex_t837_marshaled& marshaled);
