﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<AnimatorFrame>
struct DefaultComparer_t2779;
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<AnimatorFrame>::.ctor()
extern "C" void DefaultComparer__ctor_m18431_gshared (DefaultComparer_t2779 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m18431(__this, method) (( void (*) (DefaultComparer_t2779 *, const MethodInfo*))DefaultComparer__ctor_m18431_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<AnimatorFrame>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m18432_gshared (DefaultComparer_t2779 * __this, AnimatorFrame_t235  ___x, AnimatorFrame_t235  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m18432(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2779 *, AnimatorFrame_t235 , AnimatorFrame_t235 , const MethodInfo*))DefaultComparer_Compare_m18432_gshared)(__this, ___x, ___y, method)
