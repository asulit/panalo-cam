﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ChannelData
struct ChannelData_t2146;
// System.Collections.ArrayList
struct ArrayList_t450;
// System.Collections.Hashtable
struct Hashtable_t261;

// System.Void System.Runtime.Remoting.ChannelData::.ctor()
extern "C" void ChannelData__ctor_m12697 (ChannelData_t2146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Runtime.Remoting.ChannelData::get_ServerProviders()
extern "C" ArrayList_t450 * ChannelData_get_ServerProviders_m12698 (ChannelData_t2146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Runtime.Remoting.ChannelData::get_ClientProviders()
extern "C" ArrayList_t450 * ChannelData_get_ClientProviders_m12699 (ChannelData_t2146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable System.Runtime.Remoting.ChannelData::get_CustomProperties()
extern "C" Hashtable_t261 * ChannelData_get_CustomProperties_m12700 (ChannelData_t2146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ChannelData::CopyFrom(System.Runtime.Remoting.ChannelData)
extern "C" void ChannelData_CopyFrom_m12701 (ChannelData_t2146 * __this, ChannelData_t2146 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
