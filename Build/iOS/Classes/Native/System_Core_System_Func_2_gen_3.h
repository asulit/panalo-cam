﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ILayoutElement
struct ILayoutElement_t719;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct  Func_2_t672  : public MulticastDelegate_t28
{
};
