﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>
struct Dictionary_2_t208;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_23.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ERaffleResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17638_gshared (Enumerator_t2714 * __this, Dictionary_2_t208 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m17638(__this, ___host, method) (( void (*) (Enumerator_t2714 *, Dictionary_2_t208 *, const MethodInfo*))Enumerator__ctor_m17638_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ERaffleResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17639_gshared (Enumerator_t2714 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17639(__this, method) (( Object_t * (*) (Enumerator_t2714 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17639_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ERaffleResult>::Dispose()
extern "C" void Enumerator_Dispose_m17640_gshared (Enumerator_t2714 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17640(__this, method) (( void (*) (Enumerator_t2714 *, const MethodInfo*))Enumerator_Dispose_m17640_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ERaffleResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17641_gshared (Enumerator_t2714 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17641(__this, method) (( bool (*) (Enumerator_t2714 *, const MethodInfo*))Enumerator_MoveNext_m17641_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,ERaffleResult>::get_Current()
extern "C" int32_t Enumerator_get_Current_m17642_gshared (Enumerator_t2714 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17642(__this, method) (( int32_t (*) (Enumerator_t2714 *, const MethodInfo*))Enumerator_get_Current_m17642_gshared)(__this, method)
