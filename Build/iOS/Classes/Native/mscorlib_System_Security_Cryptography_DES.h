﻿#pragma once
#include <stdint.h>
// System.Byte[,]
struct ByteU5BU2CU5D_t2201;
// System.Security.Cryptography.SymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithm.h"
// System.Security.Cryptography.DES
struct  DES_t1579  : public SymmetricAlgorithm_t1402
{
};
struct DES_t1579_StaticFields{
	// System.Byte[,] System.Security.Cryptography.DES::weakKeys
	ByteU5BU2CU5D_t2201* ___weakKeys_10;
	// System.Byte[,] System.Security.Cryptography.DES::semiWeakKeys
	ByteU5BU2CU5D_t2201* ___semiWeakKeys_11;
};
