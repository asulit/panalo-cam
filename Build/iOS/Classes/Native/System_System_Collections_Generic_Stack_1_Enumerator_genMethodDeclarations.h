﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2580;
// System.Object
struct Object_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C" void Enumerator__ctor_m15969_gshared (Enumerator_t2581 * __this, Stack_1_t2580 * ___t, const MethodInfo* method);
#define Enumerator__ctor_m15969(__this, ___t, method) (( void (*) (Enumerator_t2581 *, Stack_1_t2580 *, const MethodInfo*))Enumerator__ctor_m15969_gshared)(__this, ___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15970_gshared (Enumerator_t2581 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15970(__this, method) (( Object_t * (*) (Enumerator_t2581 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15970_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15971_gshared (Enumerator_t2581 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15971(__this, method) (( void (*) (Enumerator_t2581 *, const MethodInfo*))Enumerator_Dispose_m15971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15972_gshared (Enumerator_t2581 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15972(__this, method) (( bool (*) (Enumerator_t2581 *, const MethodInfo*))Enumerator_MoveNext_m15972_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15973_gshared (Enumerator_t2581 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15973(__this, method) (( Object_t * (*) (Enumerator_t2581 *, const MethodInfo*))Enumerator_get_Current_m15973_gshared)(__this, method)
