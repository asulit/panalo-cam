﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t2923;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t736;
// System.Object[]
struct ObjectU5BU5D_t356;

// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m20527_gshared (InvokableCall_1_t2923 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m20527(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t2923 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m20527_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m20528_gshared (InvokableCall_1_t2923 * __this, UnityAction_1_t736 * ___action, const MethodInfo* method);
#define InvokableCall_1__ctor_m20528(__this, ___action, method) (( void (*) (InvokableCall_1_t2923 *, UnityAction_1_t736 *, const MethodInfo*))InvokableCall_1__ctor_m20528_gshared)(__this, ___action, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m20529_gshared (InvokableCall_1_t2923 * __this, ObjectU5BU5D_t356* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m20529(__this, ___args, method) (( void (*) (InvokableCall_1_t2923 *, ObjectU5BU5D_t356*, const MethodInfo*))InvokableCall_1_Invoke_m20529_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m20530_gshared (InvokableCall_1_t2923 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m20530(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t2923 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m20530_gshared)(__this, ___targetObj, ___method, method)
