﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t1371;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2710;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.ICollection
struct ICollection_t1751;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>[]
struct KeyValuePair_2U5BU5D_t3783;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>
struct IEnumerator_1_t3784;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1606;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct KeyCollection_t3427;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ValueCollection_t3431;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45.h"
// Vuforia.VuforiaManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__45.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor()
extern "C" void Dictionary_2__ctor_m7430_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m7430(__this, method) (( void (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2__ctor_m7430_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m28032_gshared (Dictionary_2_t1371 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m28032(__this, ___comparer, method) (( void (*) (Dictionary_2_t1371 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m28032_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m28033_gshared (Dictionary_2_t1371 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m28033(__this, ___capacity, method) (( void (*) (Dictionary_2_t1371 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m28033_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m28034_gshared (Dictionary_2_t1371 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m28034(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1371 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m28034_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m28035_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m28035(__this, method) (( Object_t * (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m28035_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m28036_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m28036(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m28036_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m28037_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m28037(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1371 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m28037_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m28038_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m28038(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1371 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m28038_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m28039_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m28039(__this, ___key, method) (( bool (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m28039_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m28040_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m28040(__this, ___key, method) (( void (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m28040_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m28041_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m28041(__this, method) (( bool (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m28041_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m28042_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m28042(__this, method) (( Object_t * (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m28042_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m28043_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m28043(__this, method) (( bool (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m28043_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m28044_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2_t3425  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m28044(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1371 *, KeyValuePair_2_t3425 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m28044_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m28045_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2_t3425  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m28045(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1371 *, KeyValuePair_2_t3425 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m28045_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m28046_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2U5BU5D_t3783* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m28046(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1371 *, KeyValuePair_2U5BU5D_t3783*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m28046_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m28047_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2_t3425  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m28047(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1371 *, KeyValuePair_2_t3425 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m28047_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m28048_gshared (Dictionary_2_t1371 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m28048(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1371 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m28048_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m28049_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m28049(__this, method) (( Object_t * (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m28049_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m28050_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m28050(__this, method) (( Object_t* (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m28050_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m28051_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m28051(__this, method) (( Object_t * (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m28051_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m28052_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m28052(__this, method) (( int32_t (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_get_Count_m28052_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Item(TKey)
extern "C" TrackableResultData_t1134  Dictionary_2_get_Item_m28053_gshared (Dictionary_2_t1371 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m28053(__this, ___key, method) (( TrackableResultData_t1134  (*) (Dictionary_2_t1371 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m28053_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m28054_gshared (Dictionary_2_t1371 * __this, int32_t ___key, TrackableResultData_t1134  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m28054(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1371 *, int32_t, TrackableResultData_t1134 , const MethodInfo*))Dictionary_2_set_Item_m28054_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m28055_gshared (Dictionary_2_t1371 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m28055(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1371 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m28055_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m28056_gshared (Dictionary_2_t1371 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m28056(__this, ___size, method) (( void (*) (Dictionary_2_t1371 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m28056_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m28057_gshared (Dictionary_2_t1371 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m28057(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1371 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m28057_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3425  Dictionary_2_make_pair_m28058_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t1134  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m28058(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3425  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t1134 , const MethodInfo*))Dictionary_2_make_pair_m28058_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m28059_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t1134  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m28059(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t1134 , const MethodInfo*))Dictionary_2_pick_key_m28059_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::pick_value(TKey,TValue)
extern "C" TrackableResultData_t1134  Dictionary_2_pick_value_m28060_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t1134  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m28060(__this /* static, unused */, ___key, ___value, method) (( TrackableResultData_t1134  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t1134 , const MethodInfo*))Dictionary_2_pick_value_m28060_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m28061_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2U5BU5D_t3783* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m28061(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1371 *, KeyValuePair_2U5BU5D_t3783*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m28061_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Resize()
extern "C" void Dictionary_2_Resize_m28062_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m28062(__this, method) (( void (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_Resize_m28062_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m28063_gshared (Dictionary_2_t1371 * __this, int32_t ___key, TrackableResultData_t1134  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m28063(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1371 *, int32_t, TrackableResultData_t1134 , const MethodInfo*))Dictionary_2_Add_m28063_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Clear()
extern "C" void Dictionary_2_Clear_m28064_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m28064(__this, method) (( void (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_Clear_m28064_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m28065_gshared (Dictionary_2_t1371 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m28065(__this, ___key, method) (( bool (*) (Dictionary_2_t1371 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m28065_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m28066_gshared (Dictionary_2_t1371 * __this, TrackableResultData_t1134  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m28066(__this, ___value, method) (( bool (*) (Dictionary_2_t1371 *, TrackableResultData_t1134 , const MethodInfo*))Dictionary_2_ContainsValue_m28066_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m28067_gshared (Dictionary_2_t1371 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m28067(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1371 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m28067_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m28068_gshared (Dictionary_2_t1371 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m28068(__this, ___sender, method) (( void (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m28068_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m28069_gshared (Dictionary_2_t1371 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m28069(__this, ___key, method) (( bool (*) (Dictionary_2_t1371 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m28069_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m28070_gshared (Dictionary_2_t1371 * __this, int32_t ___key, TrackableResultData_t1134 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m28070(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1371 *, int32_t, TrackableResultData_t1134 *, const MethodInfo*))Dictionary_2_TryGetValue_m28070_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Keys()
extern "C" KeyCollection_t3427 * Dictionary_2_get_Keys_m28071_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m28071(__this, method) (( KeyCollection_t3427 * (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_get_Keys_m28071_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Values()
extern "C" ValueCollection_t3431 * Dictionary_2_get_Values_m28072_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m28072(__this, method) (( ValueCollection_t3431 * (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_get_Values_m28072_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m28073_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m28073(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m28073_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToTValue(System.Object)
extern "C" TrackableResultData_t1134  Dictionary_2_ToTValue_m28074_gshared (Dictionary_2_t1371 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m28074(__this, ___value, method) (( TrackableResultData_t1134  (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m28074_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m28075_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2_t3425  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m28075(__this, ___pair, method) (( bool (*) (Dictionary_2_t1371 *, KeyValuePair_2_t3425 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m28075_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::GetEnumerator()
extern "C" Enumerator_t3429  Dictionary_2_GetEnumerator_m28076_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m28076(__this, method) (( Enumerator_t3429  (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_GetEnumerator_m28076_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m28077_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t1134  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m28077(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t1134 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m28077_gshared)(__this /* static, unused */, ___key, ___value, method)
