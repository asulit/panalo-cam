﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreading.TaskWorker
struct TaskWorker_t168;
// UnityThreading.TaskDistributor
struct TaskDistributor_t166;
// System.Collections.IEnumerator
struct IEnumerator_t337;

// System.Void UnityThreading.TaskWorker::.ctor(UnityThreading.TaskDistributor)
extern "C" void TaskWorker__ctor_m573 (TaskWorker_t168 * __this, TaskDistributor_t166 * ___taskDistributor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityThreading.TaskWorker::Do()
extern "C" Object_t * TaskWorker_Do_m574 (TaskWorker_t168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreading.TaskWorker::Dispose()
extern "C" void TaskWorker_Dispose_m575 (TaskWorker_t168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
