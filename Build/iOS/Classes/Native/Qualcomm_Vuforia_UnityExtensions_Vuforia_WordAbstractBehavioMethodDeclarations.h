﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t327;
// Vuforia.Word
struct Word_t1190;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t35;
// UnityEngine.GameObject
struct GameObject_t155;
// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"

// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void WordAbstractBehaviour_InternalUnregisterTrackable_m7165 (WordAbstractBehaviour_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
extern "C" Object_t * WordAbstractBehaviour_get_Word_m7166 (WordAbstractBehaviour_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
extern "C" String_t* WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m7167 (WordAbstractBehaviour_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m7168 (WordAbstractBehaviour_t327 * __this, String_t* ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
extern "C" int32_t WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m7169 (WordAbstractBehaviour_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m7170 (WordAbstractBehaviour_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m7171 (WordAbstractBehaviour_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m7172 (WordAbstractBehaviour_t327 * __this, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m7173 (WordAbstractBehaviour_t327 * __this, Object_t * ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern "C" void WordAbstractBehaviour__ctor_m1908 (WordAbstractBehaviour_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m7174 (WordAbstractBehaviour_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m7175 (WordAbstractBehaviour_t327 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t35 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m7176 (WordAbstractBehaviour_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t155 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m7177 (WordAbstractBehaviour_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
