﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACRIPEMD160
struct HMACRIPEMD160_t2207;
// System.Byte[]
struct ByteU5BU5D_t139;

// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor()
extern "C" void HMACRIPEMD160__ctor_m13013 (HMACRIPEMD160_t2207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor(System.Byte[])
extern "C" void HMACRIPEMD160__ctor_m13014 (HMACRIPEMD160_t2207 * __this, ByteU5BU5D_t139* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
