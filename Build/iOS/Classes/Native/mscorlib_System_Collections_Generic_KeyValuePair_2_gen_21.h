﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.InputField
struct InputField_t226;
// UnityEngine.UI.Image
struct Image_t213;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>
struct  KeyValuePair_2_t2766 
{
	// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::key
	InputField_t226 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>::value
	Image_t213 * ___value_1;
};
