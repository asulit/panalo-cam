﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
struct Dictionary_2_t3255;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__33.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_33.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25129_gshared (Enumerator_t3262 * __this, Dictionary_2_t3255 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m25129(__this, ___dictionary, method) (( void (*) (Enumerator_t3262 *, Dictionary_2_t3255 *, const MethodInfo*))Enumerator__ctor_m25129_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25130_gshared (Enumerator_t3262 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25130(__this, method) (( Object_t * (*) (Enumerator_t3262 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25130_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25131_gshared (Enumerator_t3262 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25131(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3262 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25131_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25132_gshared (Enumerator_t3262 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25132(__this, method) (( Object_t * (*) (Enumerator_t3262 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25132_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25133_gshared (Enumerator_t3262 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25133(__this, method) (( Object_t * (*) (Enumerator_t3262 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25133_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25134_gshared (Enumerator_t3262 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25134(__this, method) (( bool (*) (Enumerator_t3262 *, const MethodInfo*))Enumerator_MoveNext_m25134_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3257  Enumerator_get_Current_m25135_gshared (Enumerator_t3262 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25135(__this, method) (( KeyValuePair_2_t3257  (*) (Enumerator_t3262 *, const MethodInfo*))Enumerator_get_Current_m25135_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m25136_gshared (Enumerator_t3262 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m25136(__this, method) (( int32_t (*) (Enumerator_t3262 *, const MethodInfo*))Enumerator_get_CurrentKey_m25136_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m25137_gshared (Enumerator_t3262 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m25137(__this, method) (( Object_t * (*) (Enumerator_t3262 *, const MethodInfo*))Enumerator_get_CurrentValue_m25137_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m25138_gshared (Enumerator_t3262 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m25138(__this, method) (( void (*) (Enumerator_t3262 *, const MethodInfo*))Enumerator_VerifyState_m25138_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m25139_gshared (Enumerator_t3262 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m25139(__this, method) (( void (*) (Enumerator_t3262 *, const MethodInfo*))Enumerator_VerifyCurrent_m25139_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m25140_gshared (Enumerator_t3262 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25140(__this, method) (( void (*) (Enumerator_t3262 *, const MethodInfo*))Enumerator_Dispose_m25140_gshared)(__this, method)
