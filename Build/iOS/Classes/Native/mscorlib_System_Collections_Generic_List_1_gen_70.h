﻿#pragma once
#include <stdint.h>
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t3458;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct  List_1_t1216  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::_items
	TargetSearchResultU5BU5D_t3458* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::_version
	int32_t ____version_3;
};
struct List_1_t1216_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::EmptyArray
	TargetSearchResultU5BU5D_t3458* ___EmptyArray_4;
};
