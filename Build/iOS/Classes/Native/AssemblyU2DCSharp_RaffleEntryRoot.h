﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t155;
// UnityEngine.UI.Text
struct Text_t196;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// RaffleEntryRoot
struct  RaffleEntryRoot_t224  : public MonoBehaviour_t5
{
	// UnityEngine.GameObject RaffleEntryRoot::button
	GameObject_t155 * ___button_2;
	// UnityEngine.UI.Text RaffleEntryRoot::label
	Text_t196 * ___label_3;
};
