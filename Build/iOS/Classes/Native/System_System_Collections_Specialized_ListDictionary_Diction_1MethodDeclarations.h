﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator
struct DictionaryNodeCollectionEnumerator_t1605;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1606;
// System.Object
struct Object_t;

// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator::.ctor(System.Collections.IDictionaryEnumerator,System.Boolean)
extern "C" void DictionaryNodeCollectionEnumerator__ctor_m8603 (DictionaryNodeCollectionEnumerator_t1605 * __this, Object_t * ___inner, bool ___isKeyList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator::get_Current()
extern "C" Object_t * DictionaryNodeCollectionEnumerator_get_Current_m8604 (DictionaryNodeCollectionEnumerator_t1605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.ListDictionary/DictionaryNodeCollection/DictionaryNodeCollectionEnumerator::MoveNext()
extern "C" bool DictionaryNodeCollectionEnumerator_MoveNext_m8605 (DictionaryNodeCollectionEnumerator_t1605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
