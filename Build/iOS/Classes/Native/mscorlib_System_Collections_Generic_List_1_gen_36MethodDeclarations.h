﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t675;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t780;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t3711;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector3>
struct ICollection_1_t3712;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>
struct ReadOnlyCollection_1_t3029;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t254;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t3034;
// System.Comparison`1<UnityEngine.Vector3>
struct Comparison_1_t3037;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_61.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C" void List_1__ctor_m22114_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1__ctor_m22114(__this, method) (( void (*) (List_1_t675 *, const MethodInfo*))List_1__ctor_m22114_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m22115_gshared (List_1_t675 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m22115(__this, ___collection, method) (( void (*) (List_1_t675 *, Object_t*, const MethodInfo*))List_1__ctor_m22115_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Int32)
extern "C" void List_1__ctor_m22116_gshared (List_1_t675 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m22116(__this, ___capacity, method) (( void (*) (List_1_t675 *, int32_t, const MethodInfo*))List_1__ctor_m22116_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.cctor()
extern "C" void List_1__cctor_m22117_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m22117(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m22117_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22118_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22118(__this, method) (( Object_t* (*) (List_1_t675 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22119_gshared (List_1_t675 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m22119(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t675 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m22119_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m22120_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22120(__this, method) (( Object_t * (*) (List_1_t675 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m22120_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m22121_gshared (List_1_t675 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m22121(__this, ___item, method) (( int32_t (*) (List_1_t675 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m22121_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m22122_gshared (List_1_t675 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m22122(__this, ___item, method) (( bool (*) (List_1_t675 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m22122_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m22123_gshared (List_1_t675 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m22123(__this, ___item, method) (( int32_t (*) (List_1_t675 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m22123_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m22124_gshared (List_1_t675 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m22124(__this, ___index, ___item, method) (( void (*) (List_1_t675 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m22124_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m22125_gshared (List_1_t675 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m22125(__this, ___item, method) (( void (*) (List_1_t675 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m22125_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22126_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22126(__this, method) (( bool (*) (List_1_t675 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22126_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m22127_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22127(__this, method) (( bool (*) (List_1_t675 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m22127_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m22128_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m22128(__this, method) (( Object_t * (*) (List_1_t675 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m22128_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m22129_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m22129(__this, method) (( bool (*) (List_1_t675 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m22129_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m22130_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m22130(__this, method) (( bool (*) (List_1_t675 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m22130_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m22131_gshared (List_1_t675 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m22131(__this, ___index, method) (( Object_t * (*) (List_1_t675 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m22131_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m22132_gshared (List_1_t675 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m22132(__this, ___index, ___value, method) (( void (*) (List_1_t675 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m22132_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(T)
extern "C" void List_1_Add_m22133_gshared (List_1_t675 * __this, Vector3_t36  ___item, const MethodInfo* method);
#define List_1_Add_m22133(__this, ___item, method) (( void (*) (List_1_t675 *, Vector3_t36 , const MethodInfo*))List_1_Add_m22133_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m22134_gshared (List_1_t675 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m22134(__this, ___newCount, method) (( void (*) (List_1_t675 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m22134_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m22135_gshared (List_1_t675 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m22135(__this, ___idx, ___count, method) (( void (*) (List_1_t675 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m22135_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m22136_gshared (List_1_t675 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m22136(__this, ___collection, method) (( void (*) (List_1_t675 *, Object_t*, const MethodInfo*))List_1_AddCollection_m22136_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m22137_gshared (List_1_t675 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m22137(__this, ___enumerable, method) (( void (*) (List_1_t675 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m22137_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m3784_gshared (List_1_t675 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m3784(__this, ___collection, method) (( void (*) (List_1_t675 *, Object_t*, const MethodInfo*))List_1_AddRange_m3784_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3029 * List_1_AsReadOnly_m22138_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m22138(__this, method) (( ReadOnlyCollection_1_t3029 * (*) (List_1_t675 *, const MethodInfo*))List_1_AsReadOnly_m22138_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
extern "C" void List_1_Clear_m22139_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_Clear_m22139(__this, method) (( void (*) (List_1_t675 *, const MethodInfo*))List_1_Clear_m22139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool List_1_Contains_m22140_gshared (List_1_t675 * __this, Vector3_t36  ___item, const MethodInfo* method);
#define List_1_Contains_m22140(__this, ___item, method) (( bool (*) (List_1_t675 *, Vector3_t36 , const MethodInfo*))List_1_Contains_m22140_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m22141_gshared (List_1_t675 * __this, Vector3U5BU5D_t254* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m22141(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t675 *, Vector3U5BU5D_t254*, int32_t, const MethodInfo*))List_1_CopyTo_m22141_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::Find(System.Predicate`1<T>)
extern "C" Vector3_t36  List_1_Find_m22142_gshared (List_1_t675 * __this, Predicate_1_t3034 * ___match, const MethodInfo* method);
#define List_1_Find_m22142(__this, ___match, method) (( Vector3_t36  (*) (List_1_t675 *, Predicate_1_t3034 *, const MethodInfo*))List_1_Find_m22142_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m22143_gshared (Object_t * __this /* static, unused */, Predicate_1_t3034 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m22143(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3034 *, const MethodInfo*))List_1_CheckMatch_m22143_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m22144_gshared (List_1_t675 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3034 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m22144(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t675 *, int32_t, int32_t, Predicate_1_t3034 *, const MethodInfo*))List_1_GetIndex_m22144_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Enumerator_t3028  List_1_GetEnumerator_m22145_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m22145(__this, method) (( Enumerator_t3028  (*) (List_1_t675 *, const MethodInfo*))List_1_GetEnumerator_m22145_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m22146_gshared (List_1_t675 * __this, Vector3_t36  ___item, const MethodInfo* method);
#define List_1_IndexOf_m22146(__this, ___item, method) (( int32_t (*) (List_1_t675 *, Vector3_t36 , const MethodInfo*))List_1_IndexOf_m22146_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m22147_gshared (List_1_t675 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m22147(__this, ___start, ___delta, method) (( void (*) (List_1_t675 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m22147_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m22148_gshared (List_1_t675 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m22148(__this, ___index, method) (( void (*) (List_1_t675 *, int32_t, const MethodInfo*))List_1_CheckIndex_m22148_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m22149_gshared (List_1_t675 * __this, int32_t ___index, Vector3_t36  ___item, const MethodInfo* method);
#define List_1_Insert_m22149(__this, ___index, ___item, method) (( void (*) (List_1_t675 *, int32_t, Vector3_t36 , const MethodInfo*))List_1_Insert_m22149_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m22150_gshared (List_1_t675 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m22150(__this, ___collection, method) (( void (*) (List_1_t675 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m22150_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool List_1_Remove_m22151_gshared (List_1_t675 * __this, Vector3_t36  ___item, const MethodInfo* method);
#define List_1_Remove_m22151(__this, ___item, method) (( bool (*) (List_1_t675 *, Vector3_t36 , const MethodInfo*))List_1_Remove_m22151_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m22152_gshared (List_1_t675 * __this, Predicate_1_t3034 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m22152(__this, ___match, method) (( int32_t (*) (List_1_t675 *, Predicate_1_t3034 *, const MethodInfo*))List_1_RemoveAll_m22152_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m22153_gshared (List_1_t675 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m22153(__this, ___index, method) (( void (*) (List_1_t675 *, int32_t, const MethodInfo*))List_1_RemoveAt_m22153_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m22154_gshared (List_1_t675 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m22154(__this, ___index, ___count, method) (( void (*) (List_1_t675 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m22154_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Reverse()
extern "C" void List_1_Reverse_m22155_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_Reverse_m22155(__this, method) (( void (*) (List_1_t675 *, const MethodInfo*))List_1_Reverse_m22155_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort()
extern "C" void List_1_Sort_m22156_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_Sort_m22156(__this, method) (( void (*) (List_1_t675 *, const MethodInfo*))List_1_Sort_m22156_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m22157_gshared (List_1_t675 * __this, Comparison_1_t3037 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m22157(__this, ___comparison, method) (( void (*) (List_1_t675 *, Comparison_1_t3037 *, const MethodInfo*))List_1_Sort_m22157_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C" Vector3U5BU5D_t254* List_1_ToArray_m22158_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_ToArray_m22158(__this, method) (( Vector3U5BU5D_t254* (*) (List_1_t675 *, const MethodInfo*))List_1_ToArray_m22158_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::TrimExcess()
extern "C" void List_1_TrimExcess_m22159_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m22159(__this, method) (( void (*) (List_1_t675 *, const MethodInfo*))List_1_TrimExcess_m22159_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m22160_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m22160(__this, method) (( int32_t (*) (List_1_t675 *, const MethodInfo*))List_1_get_Capacity_m22160_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m22161_gshared (List_1_t675 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m22161(__this, ___value, method) (( void (*) (List_1_t675 *, int32_t, const MethodInfo*))List_1_set_Capacity_m22161_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t List_1_get_Count_m22162_gshared (List_1_t675 * __this, const MethodInfo* method);
#define List_1_get_Count_m22162(__this, method) (( int32_t (*) (List_1_t675 *, const MethodInfo*))List_1_get_Count_m22162_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t36  List_1_get_Item_m22163_gshared (List_1_t675 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m22163(__this, ___index, method) (( Vector3_t36  (*) (List_1_t675 *, int32_t, const MethodInfo*))List_1_get_Item_m22163_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m22164_gshared (List_1_t675 * __this, int32_t ___index, Vector3_t36  ___value, const MethodInfo* method);
#define List_1_set_Item_m22164(__this, ___index, ___value, method) (( void (*) (List_1_t675 *, int32_t, Vector3_t36 , const MethodInfo*))List_1_set_Item_m22164_gshared)(__this, ___index, ___value, method)
