﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SwarmItemManager/PrefabItem
struct PrefabItem_t154;

// System.Void SwarmItemManager/PrefabItem::.ctor()
extern "C" void PrefabItem__ctor_m513 (PrefabItem_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
