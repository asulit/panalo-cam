﻿#pragma once
#include <stdint.h>
// Vuforia.Eyewear
struct Eyewear_t1081;
// Vuforia.EyewearCalibrationProfileManager
struct EyewearCalibrationProfileManager_t1047;
// Vuforia.EyewearUserCalibrator
struct EyewearUserCalibrator_t1050;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.Eyewear
struct  Eyewear_t1081  : public Object_t
{
	// Vuforia.EyewearCalibrationProfileManager Vuforia.Eyewear::mProfileManager
	EyewearCalibrationProfileManager_t1047 * ___mProfileManager_1;
	// Vuforia.EyewearUserCalibrator Vuforia.Eyewear::mCalibrator
	EyewearUserCalibrator_t1050 * ___mCalibrator_2;
};
struct Eyewear_t1081_StaticFields{
	// Vuforia.Eyewear Vuforia.Eyewear::mInstance
	Eyewear_t1081 * ___mInstance_0;
};
