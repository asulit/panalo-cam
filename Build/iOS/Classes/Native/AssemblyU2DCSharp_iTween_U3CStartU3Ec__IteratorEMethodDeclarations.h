﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// iTween/<Start>c__IteratorE
struct U3CStartU3Ec__IteratorE_t260;
// System.Object
struct Object_t;

// System.Void iTween/<Start>c__IteratorE::.ctor()
extern "C" void U3CStartU3Ec__IteratorE__ctor_m917 (U3CStartU3Ec__IteratorE_t260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m918 (U3CStartU3Ec__IteratorE_t260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m919 (U3CStartU3Ec__IteratorE_t260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<Start>c__IteratorE::MoveNext()
extern "C" bool U3CStartU3Ec__IteratorE_MoveNext_m920 (U3CStartU3Ec__IteratorE_t260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__IteratorE::Dispose()
extern "C" void U3CStartU3Ec__IteratorE_Dispose_m921 (U3CStartU3Ec__IteratorE_t260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__IteratorE::Reset()
extern "C" void U3CStartU3Ec__IteratorE_Reset_m922 (U3CStartU3Ec__IteratorE_t260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
