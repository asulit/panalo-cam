﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_0MethodDeclarations.h"
#define Transform_1__ctor_m16808(__this, ___object, ___method, method) (( void (*) (Transform_1_t2641 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m15175_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m16809(__this, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Transform_1_t2641 *, String_t*, String_t*, const MethodInfo*))Transform_1_Invoke_m15176_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m16810(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2641 *, String_t*, String_t*, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m15177_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m16811(__this, ___result, method) (( DictionaryEntry_t451  (*) (Transform_1_t2641 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m15178_gshared)(__this, ___result, method)
