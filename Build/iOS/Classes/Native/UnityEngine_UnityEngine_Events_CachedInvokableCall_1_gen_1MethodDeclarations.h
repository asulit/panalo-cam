﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_3MethodDeclarations.h"
#define CachedInvokableCall_1__ctor_m5180(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t1040 *, Object_t335 *, MethodInfo_t *, String_t*, const MethodInfo*))CachedInvokableCall_1__ctor_m24321_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::Invoke(System.Object[])
#define CachedInvokableCall_1_Invoke_m24325(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t1040 *, ObjectU5BU5D_t356*, const MethodInfo*))CachedInvokableCall_1_Invoke_m24322_gshared)(__this, ___args, method)
