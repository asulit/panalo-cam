﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData
#pragma pack(push, tp, 1)
struct  SmartTerrainRevisionData_t1142 
{
	// System.Int32 Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData::revision
	int32_t ___revision_1;
};
#pragma pack(pop, tp)
