﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2274;

// System.Void System.Text.EncoderFallbackBuffer::.ctor()
extern "C" void EncoderFallbackBuffer__ctor_m13446 (EncoderFallbackBuffer_t2274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
