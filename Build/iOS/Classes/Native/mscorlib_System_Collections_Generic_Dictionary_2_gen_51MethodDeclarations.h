﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3476;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2506;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Collections.ICollection
struct ICollection_t1751;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t3793;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
struct IEnumerator_1_t3794;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1606;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
struct KeyCollection_t3481;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ValueCollection_t3485;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_48.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__48.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor()
extern "C" void Dictionary_2__ctor_m28650_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m28650(__this, method) (( void (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2__ctor_m28650_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m28652_gshared (Dictionary_2_t3476 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m28652(__this, ___comparer, method) (( void (*) (Dictionary_2_t3476 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m28652_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m28654_gshared (Dictionary_2_t3476 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m28654(__this, ___capacity, method) (( void (*) (Dictionary_2_t3476 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m28654_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m28656_gshared (Dictionary_2_t3476 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m28656(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3476 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2__ctor_m28656_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m28658_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m28658(__this, method) (( Object_t * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m28658_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m28660_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m28660(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m28660_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m28662_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m28662(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m28662_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m28664_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m28664(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m28664_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m28666_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m28666(__this, ___key, method) (( bool (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m28666_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m28668_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m28668(__this, ___key, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m28668_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m28670_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m28670(__this, method) (( bool (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m28670_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m28672_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m28672(__this, method) (( Object_t * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m28672_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m28674_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m28674(__this, method) (( bool (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m28674_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m28676_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2_t3478  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m28676(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3476 *, KeyValuePair_2_t3478 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m28676_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m28678_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2_t3478  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m28678(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3476 *, KeyValuePair_2_t3478 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m28678_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m28680_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2U5BU5D_t3793* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m28680(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3476 *, KeyValuePair_2U5BU5D_t3793*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m28680_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m28682_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2_t3478  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m28682(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3476 *, KeyValuePair_2_t3478 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m28682_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m28684_gshared (Dictionary_2_t3476 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m28684(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3476 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m28684_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m28686_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m28686(__this, method) (( Object_t * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m28686_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m28688_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m28688(__this, method) (( Object_t* (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m28688_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m28690_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m28690(__this, method) (( Object_t * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m28690_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m28692_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m28692(__this, method) (( int32_t (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_get_Count_m28692_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Item(TKey)
extern "C" ProfileData_t1226  Dictionary_2_get_Item_m28694_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m28694(__this, ___key, method) (( ProfileData_t1226  (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m28694_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m28696_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, ProfileData_t1226  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m28696(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, ProfileData_t1226 , const MethodInfo*))Dictionary_2_set_Item_m28696_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m28698_gshared (Dictionary_2_t3476 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m28698(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3476 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m28698_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m28700_gshared (Dictionary_2_t3476 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m28700(__this, ___size, method) (( void (*) (Dictionary_2_t3476 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m28700_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m28702_gshared (Dictionary_2_t3476 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m28702(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3476 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m28702_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3478  Dictionary_2_make_pair_m28704_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t1226  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m28704(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3478  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t1226 , const MethodInfo*))Dictionary_2_make_pair_m28704_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m28706_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t1226  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m28706(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t1226 , const MethodInfo*))Dictionary_2_pick_key_m28706_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::pick_value(TKey,TValue)
extern "C" ProfileData_t1226  Dictionary_2_pick_value_m28708_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t1226  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m28708(__this /* static, unused */, ___key, ___value, method) (( ProfileData_t1226  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t1226 , const MethodInfo*))Dictionary_2_pick_value_m28708_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m28710_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2U5BU5D_t3793* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m28710(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3476 *, KeyValuePair_2U5BU5D_t3793*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m28710_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Resize()
extern "C" void Dictionary_2_Resize_m28712_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m28712(__this, method) (( void (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_Resize_m28712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m28714_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, ProfileData_t1226  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m28714(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, ProfileData_t1226 , const MethodInfo*))Dictionary_2_Add_m28714_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Clear()
extern "C" void Dictionary_2_Clear_m28716_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m28716(__this, method) (( void (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_Clear_m28716_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m28718_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m28718(__this, ___key, method) (( bool (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m28718_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m28720_gshared (Dictionary_2_t3476 * __this, ProfileData_t1226  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m28720(__this, ___value, method) (( bool (*) (Dictionary_2_t3476 *, ProfileData_t1226 , const MethodInfo*))Dictionary_2_ContainsValue_m28720_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m28722_gshared (Dictionary_2_t3476 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m28722(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3476 *, SerializationInfo_t1012 *, StreamingContext_t1013 , const MethodInfo*))Dictionary_2_GetObjectData_m28722_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m28724_gshared (Dictionary_2_t3476 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m28724(__this, ___sender, method) (( void (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m28724_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m28726_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m28726(__this, ___key, method) (( bool (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m28726_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m28728_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, ProfileData_t1226 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m28728(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3476 *, Object_t *, ProfileData_t1226 *, const MethodInfo*))Dictionary_2_TryGetValue_m28728_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Keys()
extern "C" KeyCollection_t3481 * Dictionary_2_get_Keys_m28730_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m28730(__this, method) (( KeyCollection_t3481 * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_get_Keys_m28730_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Values()
extern "C" ValueCollection_t3485 * Dictionary_2_get_Values_m28732_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m28732(__this, method) (( ValueCollection_t3485 * (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_get_Values_m28732_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m28734_gshared (Dictionary_2_t3476 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m28734(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m28734_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToTValue(System.Object)
extern "C" ProfileData_t1226  Dictionary_2_ToTValue_m28736_gshared (Dictionary_2_t3476 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m28736(__this, ___value, method) (( ProfileData_t1226  (*) (Dictionary_2_t3476 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m28736_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m28738_gshared (Dictionary_2_t3476 * __this, KeyValuePair_2_t3478  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m28738(__this, ___pair, method) (( bool (*) (Dictionary_2_t3476 *, KeyValuePair_2_t3478 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m28738_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::GetEnumerator()
extern "C" Enumerator_t3483  Dictionary_2_GetEnumerator_m28740_gshared (Dictionary_2_t3476 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m28740(__this, method) (( Enumerator_t3483  (*) (Dictionary_2_t3476 *, const MethodInfo*))Dictionary_2_GetEnumerator_m28740_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t451  Dictionary_2_U3CCopyToU3Em__0_m28742_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t1226  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m28742(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t1226 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m28742_gshared)(__this /* static, unused */, ___key, ___value, method)
