﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Query.QueryResultResolver
struct QueryResultResolver_t328;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Query.QueryResultResolver>
struct  KeyValuePair_2_t2593 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,Common.Query.QueryResultResolver>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,Common.Query.QueryResultResolver>::value
	QueryResultResolver_t328 * ___value_1;
};
