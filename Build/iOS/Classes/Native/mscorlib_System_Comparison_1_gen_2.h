﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t708;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct  Comparison_1_t549  : public MulticastDelegate_t28
{
};
