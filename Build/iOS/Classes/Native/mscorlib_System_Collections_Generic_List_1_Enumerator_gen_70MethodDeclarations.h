﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30MethodDeclarations.h"
#define Enumerator__ctor_m24992(__this, ___l, method) (( void (*) (Enumerator_t3249 *, List_1_t1296 *, const MethodInfo*))Enumerator__ctor_m14718_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24993(__this, method) (( Object_t * (*) (Enumerator_t3249 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14719_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::Dispose()
#define Enumerator_Dispose_m24994(__this, method) (( void (*) (Enumerator_t3249 *, const MethodInfo*))Enumerator_Dispose_m14720_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::VerifyState()
#define Enumerator_VerifyState_m24995(__this, method) (( void (*) (Enumerator_t3249 *, const MethodInfo*))Enumerator_VerifyState_m14721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::MoveNext()
#define Enumerator_MoveNext_m24996(__this, method) (( bool (*) (Enumerator_t3249 *, const MethodInfo*))Enumerator_MoveNext_m14722_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::get_Current()
#define Enumerator_get_Current_m24997(__this, method) (( VirtualButton_t1223 * (*) (Enumerator_t3249 *, const MethodInfo*))Enumerator_get_Current_m14723_gshared)(__this, method)
