﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.IClipper,System.Int32>
struct Dictionary_2_t765;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.IClipper,System.Int32>
struct  KeyCollection_t3018  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.IClipper,System.Int32>::dictionary
	Dictionary_2_t765 * ___dictionary_0;
};
