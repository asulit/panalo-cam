﻿#pragma once
#include <stdint.h>
// OrthographicCameraObserver
struct OrthographicCameraObserver_t336;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<OrthographicCameraObserver>
struct  Predicate_1_t2484  : public MulticastDelegate_t28
{
};
