﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t358;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.Surface>
struct  Predicate_1_t3414  : public MulticastDelegate_t28
{
};
