﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU241024_t1413_marshal(const U24ArrayTypeU241024_t1413& unmarshaled, U24ArrayTypeU241024_t1413_marshaled& marshaled);
extern "C" void U24ArrayTypeU241024_t1413_marshal_back(const U24ArrayTypeU241024_t1413_marshaled& marshaled, U24ArrayTypeU241024_t1413& unmarshaled);
extern "C" void U24ArrayTypeU241024_t1413_marshal_cleanup(U24ArrayTypeU241024_t1413_marshaled& marshaled);
