﻿#pragma once
#include <stdint.h>
// System.Action
struct Action_t16;
// System.Object
#include "mscorlib_System_Object.h"
// Common.DelegateCommand
struct  DelegateCommand_t15  : public Object_t
{
	// System.Action Common.DelegateCommand::action
	Action_t16 * ___action_0;
};
