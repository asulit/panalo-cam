﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Boolean Comparison::TolerantEquals(System.Single,System.Single)
extern "C" bool Comparison_TolerantEquals_m58 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Comparison::TolerantGreaterThanOrEquals(System.Single,System.Single)
extern "C" bool Comparison_TolerantGreaterThanOrEquals_m59 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Comparison::TolerantLesserThanOrEquals(System.Single,System.Single)
extern "C" bool Comparison_TolerantLesserThanOrEquals_m60 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
