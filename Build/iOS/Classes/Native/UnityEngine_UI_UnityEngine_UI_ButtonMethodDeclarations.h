﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button
struct Button_t544;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t541;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t517;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t480;
// System.Collections.IEnumerator
struct IEnumerator_t337;

// System.Void UnityEngine.UI.Button::.ctor()
extern "C" void Button__ctor_m2233 (Button_t544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C" ButtonClickedEvent_t541 * Button_get_onClick_m2234 (Button_t544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::set_onClick(UnityEngine.UI.Button/ButtonClickedEvent)
extern "C" void Button_set_onClick_m2235 (Button_t544 * __this, ButtonClickedEvent_t541 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::Press()
extern "C" void Button_Press_m2236 (Button_t544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C" void Button_OnPointerClick_m2237 (Button_t544 * __this, PointerEventData_t517 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern "C" void Button_OnSubmit_m2238 (Button_t544 * __this, BaseEventData_t480 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.Button::OnFinishSubmit()
extern "C" Object_t * Button_OnFinishSubmit_m2239 (Button_t544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
