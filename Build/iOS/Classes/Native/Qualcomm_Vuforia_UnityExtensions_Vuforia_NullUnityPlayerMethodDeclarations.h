﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.NullUnityPlayer
struct NullUnityPlayer_t469;
// System.String
struct String_t;
// Vuforia.VuforiaUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitEr.h"

// System.Void Vuforia.NullUnityPlayer::LoadNativeLibraries()
extern "C" void NullUnityPlayer_LoadNativeLibraries_m5428 (NullUnityPlayer_t469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::InitializePlatform()
extern "C" void NullUnityPlayer_InitializePlatform_m5429 (NullUnityPlayer_t469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaUnity/InitError Vuforia.NullUnityPlayer::Start(System.String)
extern "C" int32_t NullUnityPlayer_Start_m5430 (NullUnityPlayer_t469 * __this, String_t* ___licenseKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Update()
extern "C" void NullUnityPlayer_Update_m5431 (NullUnityPlayer_t469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Dispose()
extern "C" void NullUnityPlayer_Dispose_m5432 (NullUnityPlayer_t469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnPause()
extern "C" void NullUnityPlayer_OnPause_m5433 (NullUnityPlayer_t469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnResume()
extern "C" void NullUnityPlayer_OnResume_m5434 (NullUnityPlayer_t469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnDestroy()
extern "C" void NullUnityPlayer_OnDestroy_m5435 (NullUnityPlayer_t469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::.ctor()
extern "C" void NullUnityPlayer__ctor_m1879 (NullUnityPlayer_t469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
