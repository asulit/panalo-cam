﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ActivationContext
struct ActivationContext_t2305;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1012;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.ActivationContext::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m13735 (ActivationContext_t2305 * __this, SerializationInfo_t1012 * ___info, StreamingContext_t1013  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Finalize()
extern "C" void ActivationContext_Finalize_m13736 (ActivationContext_t2305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Dispose()
extern "C" void ActivationContext_Dispose_m13737 (ActivationContext_t2305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Dispose(System.Boolean)
extern "C" void ActivationContext_Dispose_m13738 (ActivationContext_t2305 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
