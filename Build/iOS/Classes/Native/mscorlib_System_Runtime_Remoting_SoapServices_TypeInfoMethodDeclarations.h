﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.SoapServices/TypeInfo
struct TypeInfo_t2155;

// System.Void System.Runtime.Remoting.SoapServices/TypeInfo::.ctor()
extern "C" void TypeInfo__ctor_m12739 (TypeInfo_t2155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
