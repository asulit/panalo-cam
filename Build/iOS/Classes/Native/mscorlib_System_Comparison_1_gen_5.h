﻿#pragma once
#include <stdint.h>
// OrthographicCameraObserver
struct OrthographicCameraObserver_t336;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<OrthographicCameraObserver>
struct  Comparison_1_t2486  : public MulticastDelegate_t28
{
};
