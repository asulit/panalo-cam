﻿#pragma once
#include <stdint.h>
// Vuforia.WordResult
struct WordResult_t1188;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.WordResult>
struct  Comparison_1_t3364  : public MulticastDelegate_t28
{
};
