﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PrefabManager/<Preload>c__Iterator1
struct U3CPreloadU3Ec__Iterator1_t95;
// System.Object
struct Object_t;

// System.Void PrefabManager/<Preload>c__Iterator1::.ctor()
extern "C" void U3CPreloadU3Ec__Iterator1__ctor_m328 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PrefabManager/<Preload>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CPreloadU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m329 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PrefabManager/<Preload>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CPreloadU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m330 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PrefabManager/<Preload>c__Iterator1::MoveNext()
extern "C" bool U3CPreloadU3Ec__Iterator1_MoveNext_m331 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager/<Preload>c__Iterator1::Dispose()
extern "C" void U3CPreloadU3Ec__Iterator1_Dispose_m332 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrefabManager/<Preload>c__Iterator1::Reset()
extern "C" void U3CPreloadU3Ec__Iterator1_Reset_m333 (U3CPreloadU3Ec__Iterator1_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
