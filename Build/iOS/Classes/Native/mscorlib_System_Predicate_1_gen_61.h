﻿#pragma once
#include <stdint.h>
// Vuforia.Marker
struct Marker_t1231;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.Marker>
struct  Predicate_1_t3313  : public MulticastDelegate_t28
{
};
