﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult>
struct Dictionary_2_t208;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// RaffleData
struct  RaffleData_t206  : public MonoBehaviour_t5
{
	// System.Int32 RaffleData::PlayerId
	int32_t ___PlayerId_4;
	// System.Int32 RaffleData::PromoId
	int32_t ___PromoId_5;
	// System.String RaffleData::FirstName
	String_t* ___FirstName_6;
	// System.String RaffleData::LastName
	String_t* ___LastName_7;
	// System.String RaffleData::MobileNumber
	String_t* ___MobileNumber_8;
	// System.String RaffleData::CompanyName
	String_t* ___CompanyName_9;
	// System.String RaffleData::Gender
	String_t* ___Gender_10;
	// System.String RaffleData::DeviceId
	String_t* ___DeviceId_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,ERaffleResult> RaffleData::Raffles
	Dictionary_2_t208 * ___Raffles_12;
	// System.Boolean RaffleData::<HasData>k__BackingField
	bool ___U3CHasDataU3Ek__BackingField_13;
};
