﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__33MethodDeclarations.h"
#define Enumerator__ctor_m25227(__this, ___dictionary, method) (( void (*) (Enumerator_t3273 *, Dictionary_2_t1103 *, const MethodInfo*))Enumerator__ctor_m25129_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25228(__this, method) (( Object_t * (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25130_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25229(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25131_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25230(__this, method) (( Object_t * (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25132_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25231(__this, method) (( Object_t * (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25133_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::MoveNext()
#define Enumerator_MoveNext_m25232(__this, method) (( bool (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_MoveNext_m25134_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Current()
#define Enumerator_get_Current_m25233(__this, method) (( KeyValuePair_2_t3271  (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_get_Current_m25135_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m25234(__this, method) (( int32_t (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_get_CurrentKey_m25136_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m25235(__this, method) (( Image_t1109 * (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_get_CurrentValue_m25137_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::VerifyState()
#define Enumerator_VerifyState_m25236(__this, method) (( void (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_VerifyState_m25138_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m25237(__this, method) (( void (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_VerifyCurrent_m25139_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Dispose()
#define Enumerator_Dispose_m25238(__this, method) (( void (*) (Enumerator_t3273 *, const MethodInfo*))Enumerator_Dispose_m25140_gshared)(__this, method)
