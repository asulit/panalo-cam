﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// PrefabManager/PruneData
struct  PruneData_t93  : public Object_t
{
	// System.String PrefabManager/PruneData::prefabName
	String_t* ___prefabName_0;
	// System.Int32 PrefabManager/PruneData::maxInactiveCount
	int32_t ___maxInactiveCount_1;
};
