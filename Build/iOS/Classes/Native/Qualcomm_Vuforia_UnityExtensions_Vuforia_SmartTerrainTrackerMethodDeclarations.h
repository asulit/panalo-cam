﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerAbstractBehaviour
struct SmartTerrainTrackerAbstractBehaviour_t305;
// System.Action
struct Action_t16;

// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Awake()
extern "C" void SmartTerrainTrackerAbstractBehaviour_Awake_m5492 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnEnable()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnEnable_m5493 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDisable()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnDisable_m5494 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDestroy()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnDestroy_m5495 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::RegisterTrackerStartedCallback(System.Action)
extern "C" void SmartTerrainTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m5496 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, Action_t16 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::UnregisterTrackerStartedCallback(System.Action)
extern "C" void SmartTerrainTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m5497 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, Action_t16 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StartSmartTerrainTracker()
extern "C" void SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m5498 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StopSmartTerrainTracker()
extern "C" void SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m5499 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::InitSmartTerrainTracker()
extern "C" void SmartTerrainTrackerAbstractBehaviour_InitSmartTerrainTracker_m5500 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnVuforiaInitialized()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnVuforiaInitialized_m5501 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnVuforiaStarted()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnVuforiaStarted_m5502 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnPause(System.Boolean)
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnPause_m5503 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, bool ___pause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetAutomaticStart(System.Boolean)
extern "C" void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m5504 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, bool ___autoStart, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_AutomaticStart()
extern "C" bool SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m5505 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetSmartTerrainScaleToMM(System.Single)
extern "C" void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m5506 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, float ___scaleToMM, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_ScaleToMM()
extern "C" float SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m5507 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::.ctor()
extern "C" void SmartTerrainTrackerAbstractBehaviour__ctor_m1869 (SmartTerrainTrackerAbstractBehaviour_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
