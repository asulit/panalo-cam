﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t35;
// System.String
struct String_t;
// CountdownTimer
struct CountdownTimer_t13;
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapter.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Common.Fsm.Action.ScaleAction
struct  ScaleAction_t42  : public FsmActionAdapter_t33
{
	// UnityEngine.Transform Common.Fsm.Action.ScaleAction::transform
	Transform_t35 * ___transform_1;
	// UnityEngine.Vector3 Common.Fsm.Action.ScaleAction::scaleFrom
	Vector3_t36  ___scaleFrom_2;
	// UnityEngine.Vector3 Common.Fsm.Action.ScaleAction::scaleTo
	Vector3_t36  ___scaleTo_3;
	// System.Single Common.Fsm.Action.ScaleAction::duration
	float ___duration_4;
	// System.String Common.Fsm.Action.ScaleAction::timeReference
	String_t* ___timeReference_5;
	// System.String Common.Fsm.Action.ScaleAction::finishEvent
	String_t* ___finishEvent_6;
	// CountdownTimer Common.Fsm.Action.ScaleAction::timer
	CountdownTimer_t13 * ___timer_7;
};
