﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.Rijndael
struct Rijndael_t417;
// System.String
struct String_t;

// System.Void System.Security.Cryptography.Rijndael::.ctor()
extern "C" void Rijndael__ctor_m13150 (Rijndael_t417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Rijndael System.Security.Cryptography.Rijndael::Create()
extern "C" Rijndael_t417 * Rijndael_Create_m1545 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Rijndael System.Security.Cryptography.Rijndael::Create(System.String)
extern "C" Rijndael_t417 * Rijndael_Create_m13151 (Object_t * __this /* static, unused */, String_t* ___algName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
