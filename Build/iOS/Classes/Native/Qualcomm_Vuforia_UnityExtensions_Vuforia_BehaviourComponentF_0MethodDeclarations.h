﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
struct NullBehaviourComponentFactory_t1093;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t295;
// UnityEngine.GameObject
struct GameObject_t155;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t319;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t310;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t285;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t293;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t297;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t273;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t327;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t308;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t299;

// Vuforia.MaskOutAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C" MaskOutAbstractBehaviour_t295 * NullBehaviourComponentFactory_AddMaskOutBehaviour_m5561 (NullBehaviourComponentFactory_t1093 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t319 * NullBehaviourComponentFactory_AddVirtualButtonBehaviour_m5562 (NullBehaviourComponentFactory_t1093 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C" TurnOffAbstractBehaviour_t310 * NullBehaviourComponentFactory_AddTurnOffBehaviour_m5563 (NullBehaviourComponentFactory_t1093 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t285 * NullBehaviourComponentFactory_AddImageTargetBehaviour_m5564 (NullBehaviourComponentFactory_t1093 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern "C" MarkerAbstractBehaviour_t293 * NullBehaviourComponentFactory_AddMarkerBehaviour_m5565 (NullBehaviourComponentFactory_t1093 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t297 * NullBehaviourComponentFactory_AddMultiTargetBehaviour_m5566 (NullBehaviourComponentFactory_t1093 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t273 * NullBehaviourComponentFactory_AddCylinderTargetBehaviour_m5567 (NullBehaviourComponentFactory_t1093 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t327 * NullBehaviourComponentFactory_AddWordBehaviour_m5568 (NullBehaviourComponentFactory_t1093 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C" TextRecoAbstractBehaviour_t308 * NullBehaviourComponentFactory_AddTextRecoBehaviour_m5569 (NullBehaviourComponentFactory_t1093 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t299 * NullBehaviourComponentFactory_AddObjectTargetBehaviour_m5570 (NullBehaviourComponentFactory_t1093 * __this, GameObject_t155 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::.ctor()
extern "C" void NullBehaviourComponentFactory__ctor_m5571 (NullBehaviourComponentFactory_t1093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
