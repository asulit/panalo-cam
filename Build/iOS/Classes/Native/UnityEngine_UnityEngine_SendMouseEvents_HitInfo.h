﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t155;
// UnityEngine.Camera
struct Camera_t6;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t959 
{
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t155 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t6 * ___camera_1;
};
