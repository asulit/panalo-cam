﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Fsm.FsmState
struct FsmState_t29;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>
struct  KeyValuePair_2_t2520 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,Common.Fsm.FsmState>::value
	Object_t * ___value_1;
};
