﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// TouchCollider
struct TouchCollider_t125;
// Common.Command
struct Command_t348;

// System.Void TouchCollider::.ctor()
extern "C" void TouchCollider__ctor_m432 (TouchCollider_t125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchCollider::Start()
extern "C" void TouchCollider_Start_m433 (TouchCollider_t125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchCollider::Update()
extern "C" void TouchCollider_Update_m434 (TouchCollider_t125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchCollider::AddCommand(Common.Command)
extern "C" void TouchCollider_AddCommand_m435 (TouchCollider_t125 * __this, Object_t * ___command, const MethodInfo* method) IL2CPP_METHOD_ATTR;
