﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<ESubScreens,System.String>
struct Dictionary_2_t192;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17.h"
// System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>
struct  Enumerator_t2707 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::dictionary
	Dictionary_2_t192 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ESubScreens,System.String>::current
	KeyValuePair_2_t2704  ___current_3;
};
