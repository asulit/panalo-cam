﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>
struct ShimEnumerator_t3348;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3335;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m26462_gshared (ShimEnumerator_t3348 * __this, Dictionary_2_t3335 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m26462(__this, ___host, method) (( void (*) (ShimEnumerator_t3348 *, Dictionary_2_t3335 *, const MethodInfo*))ShimEnumerator__ctor_m26462_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m26463_gshared (ShimEnumerator_t3348 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m26463(__this, method) (( bool (*) (ShimEnumerator_t3348 *, const MethodInfo*))ShimEnumerator_MoveNext_m26463_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Entry()
extern "C" DictionaryEntry_t451  ShimEnumerator_get_Entry_m26464_gshared (ShimEnumerator_t3348 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m26464(__this, method) (( DictionaryEntry_t451  (*) (ShimEnumerator_t3348 *, const MethodInfo*))ShimEnumerator_get_Entry_m26464_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m26465_gshared (ShimEnumerator_t3348 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m26465(__this, method) (( Object_t * (*) (ShimEnumerator_t3348 *, const MethodInfo*))ShimEnumerator_get_Key_m26465_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m26466_gshared (ShimEnumerator_t3348 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m26466(__this, method) (( Object_t * (*) (ShimEnumerator_t3348 *, const MethodInfo*))ShimEnumerator_get_Value_m26466_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt16>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m26467_gshared (ShimEnumerator_t3348 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m26467(__this, method) (( Object_t * (*) (ShimEnumerator_t3348 *, const MethodInfo*))ShimEnumerator_get_Current_m26467_gshared)(__this, method)
