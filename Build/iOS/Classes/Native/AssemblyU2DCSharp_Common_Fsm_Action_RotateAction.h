﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t35;
// System.String
struct String_t;
// CountdownTimer
struct CountdownTimer_t13;
// Common.Fsm.FsmActionAdapter
#include "AssemblyU2DCSharp_Common_Fsm_FsmActionAdapter.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// Common.Fsm.Action.RotateAction
struct  RotateAction_t40  : public FsmActionAdapter_t33
{
	// UnityEngine.Transform Common.Fsm.Action.RotateAction::transform
	Transform_t35 * ___transform_1;
	// UnityEngine.Quaternion Common.Fsm.Action.RotateAction::quatFrom
	Quaternion_t41  ___quatFrom_2;
	// UnityEngine.Quaternion Common.Fsm.Action.RotateAction::quatTo
	Quaternion_t41  ___quatTo_3;
	// System.Single Common.Fsm.Action.RotateAction::duration
	float ___duration_4;
	// System.String Common.Fsm.Action.RotateAction::finishEvent
	String_t* ___finishEvent_5;
	// CountdownTimer Common.Fsm.Action.RotateAction::timer
	CountdownTimer_t13 * ___timer_6;
};
