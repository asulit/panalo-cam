﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.InputField
struct InputField_t226;
// UnityEngine.UI.Image
struct Image_t213;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.InputField,UnityEngine.UI.Image,System.Collections.DictionaryEntry>
struct  Transform_1_t2765  : public MulticastDelegate_t28
{
};
