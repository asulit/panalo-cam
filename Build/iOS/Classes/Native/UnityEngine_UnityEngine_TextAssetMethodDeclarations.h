﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.TextAsset
struct TextAsset_t148;
// System.String
struct String_t;

// System.String UnityEngine.TextAsset::get_text()
extern "C" String_t* TextAsset_get_text_m1570 (TextAsset_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.TextAsset::ToString()
extern "C" String_t* TextAsset_ToString_m4179 (TextAsset_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
