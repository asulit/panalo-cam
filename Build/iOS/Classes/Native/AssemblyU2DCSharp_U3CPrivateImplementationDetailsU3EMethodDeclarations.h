﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t334;

// System.Void <PrivateImplementationDetails>::.ctor()
extern "C" void U3CPrivateImplementationDetailsU3E__ctor_m1305 (U3CPrivateImplementationDetailsU3E_t334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
