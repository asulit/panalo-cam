﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Array/InternalEnumerator`1<PrefabManager/PruneData>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m16052(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2589 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14611_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<PrefabManager/PruneData>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16053(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2589 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14612_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<PrefabManager/PruneData>::Dispose()
#define InternalEnumerator_1_Dispose_m16054(__this, method) (( void (*) (InternalEnumerator_1_t2589 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14613_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<PrefabManager/PruneData>::MoveNext()
#define InternalEnumerator_1_MoveNext_m16055(__this, method) (( bool (*) (InternalEnumerator_1_t2589 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14614_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<PrefabManager/PruneData>::get_Current()
#define InternalEnumerator_1_get_Current_m16056(__this, method) (( PruneData_t93 * (*) (InternalEnumerator_1_t2589 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14615_gshared)(__this, method)
