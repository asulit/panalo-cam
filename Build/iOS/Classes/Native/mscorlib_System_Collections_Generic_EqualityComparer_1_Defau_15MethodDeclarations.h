﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>
struct DefaultComparer_t3196;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C" void DefaultComparer__ctor_m24247_gshared (DefaultComparer_t3196 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m24247(__this, method) (( void (*) (DefaultComparer_t3196 *, const MethodInfo*))DefaultComparer__ctor_m24247_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m24248_gshared (DefaultComparer_t3196 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m24248(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3196 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m24248_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m24249_gshared (DefaultComparer_t3196 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m24249(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3196 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m24249_gshared)(__this, ___x, ___y, method)
