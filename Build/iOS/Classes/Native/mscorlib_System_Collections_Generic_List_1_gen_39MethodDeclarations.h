﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t678;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector4>
struct IEnumerable_1_t3719;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t3720;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector4>
struct ICollection_1_t3721;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector4>
struct ReadOnlyCollection_1_t3061;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t779;
// System.Predicate`1<UnityEngine.Vector4>
struct Predicate_1_t3066;
// System.Comparison`1<UnityEngine.Vector4>
struct Comparison_1_t3069;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_64.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor()
extern "C" void List_1__ctor_m22554_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1__ctor_m22554(__this, method) (( void (*) (List_1_t678 *, const MethodInfo*))List_1__ctor_m22554_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m22555_gshared (List_1_t678 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m22555(__this, ___collection, method) (( void (*) (List_1_t678 *, Object_t*, const MethodInfo*))List_1__ctor_m22555_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor(System.Int32)
extern "C" void List_1__ctor_m22556_gshared (List_1_t678 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m22556(__this, ___capacity, method) (( void (*) (List_1_t678 *, int32_t, const MethodInfo*))List_1__ctor_m22556_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.cctor()
extern "C" void List_1__cctor_m22557_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m22557(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m22557_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22558_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22558(__this, method) (( Object_t* (*) (List_1_t678 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22558_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22559_gshared (List_1_t678 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m22559(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t678 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m22559_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m22560_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22560(__this, method) (( Object_t * (*) (List_1_t678 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m22560_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m22561_gshared (List_1_t678 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m22561(__this, ___item, method) (( int32_t (*) (List_1_t678 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m22561_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m22562_gshared (List_1_t678 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m22562(__this, ___item, method) (( bool (*) (List_1_t678 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m22562_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m22563_gshared (List_1_t678 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m22563(__this, ___item, method) (( int32_t (*) (List_1_t678 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m22563_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m22564_gshared (List_1_t678 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m22564(__this, ___index, ___item, method) (( void (*) (List_1_t678 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m22564_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m22565_gshared (List_1_t678 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m22565(__this, ___item, method) (( void (*) (List_1_t678 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m22565_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22566_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22566(__this, method) (( bool (*) (List_1_t678 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22566_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m22567_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22567(__this, method) (( bool (*) (List_1_t678 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m22567_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m22568_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m22568(__this, method) (( Object_t * (*) (List_1_t678 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m22568_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m22569_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m22569(__this, method) (( bool (*) (List_1_t678 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m22569_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m22570_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m22570(__this, method) (( bool (*) (List_1_t678 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m22570_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m22571_gshared (List_1_t678 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m22571(__this, ___index, method) (( Object_t * (*) (List_1_t678 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m22571_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m22572_gshared (List_1_t678 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m22572(__this, ___index, ___value, method) (( void (*) (List_1_t678 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m22572_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Add(T)
extern "C" void List_1_Add_m22573_gshared (List_1_t678 * __this, Vector4_t680  ___item, const MethodInfo* method);
#define List_1_Add_m22573(__this, ___item, method) (( void (*) (List_1_t678 *, Vector4_t680 , const MethodInfo*))List_1_Add_m22573_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m22574_gshared (List_1_t678 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m22574(__this, ___newCount, method) (( void (*) (List_1_t678 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m22574_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m22575_gshared (List_1_t678 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m22575(__this, ___idx, ___count, method) (( void (*) (List_1_t678 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m22575_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m22576_gshared (List_1_t678 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m22576(__this, ___collection, method) (( void (*) (List_1_t678 *, Object_t*, const MethodInfo*))List_1_AddCollection_m22576_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m22577_gshared (List_1_t678 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m22577(__this, ___enumerable, method) (( void (*) (List_1_t678 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m22577_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m3790_gshared (List_1_t678 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m3790(__this, ___collection, method) (( void (*) (List_1_t678 *, Object_t*, const MethodInfo*))List_1_AddRange_m3790_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3061 * List_1_AsReadOnly_m22578_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m22578(__this, method) (( ReadOnlyCollection_1_t3061 * (*) (List_1_t678 *, const MethodInfo*))List_1_AsReadOnly_m22578_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Clear()
extern "C" void List_1_Clear_m22579_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_Clear_m22579(__this, method) (( void (*) (List_1_t678 *, const MethodInfo*))List_1_Clear_m22579_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::Contains(T)
extern "C" bool List_1_Contains_m22580_gshared (List_1_t678 * __this, Vector4_t680  ___item, const MethodInfo* method);
#define List_1_Contains_m22580(__this, ___item, method) (( bool (*) (List_1_t678 *, Vector4_t680 , const MethodInfo*))List_1_Contains_m22580_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m22581_gshared (List_1_t678 * __this, Vector4U5BU5D_t779* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m22581(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t678 *, Vector4U5BU5D_t779*, int32_t, const MethodInfo*))List_1_CopyTo_m22581_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector4>::Find(System.Predicate`1<T>)
extern "C" Vector4_t680  List_1_Find_m22582_gshared (List_1_t678 * __this, Predicate_1_t3066 * ___match, const MethodInfo* method);
#define List_1_Find_m22582(__this, ___match, method) (( Vector4_t680  (*) (List_1_t678 *, Predicate_1_t3066 *, const MethodInfo*))List_1_Find_m22582_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m22583_gshared (Object_t * __this /* static, unused */, Predicate_1_t3066 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m22583(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3066 *, const MethodInfo*))List_1_CheckMatch_m22583_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m22584_gshared (List_1_t678 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3066 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m22584(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t678 *, int32_t, int32_t, Predicate_1_t3066 *, const MethodInfo*))List_1_GetIndex_m22584_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector4>::GetEnumerator()
extern "C" Enumerator_t3060  List_1_GetEnumerator_m22585_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m22585(__this, method) (( Enumerator_t3060  (*) (List_1_t678 *, const MethodInfo*))List_1_GetEnumerator_m22585_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m22586_gshared (List_1_t678 * __this, Vector4_t680  ___item, const MethodInfo* method);
#define List_1_IndexOf_m22586(__this, ___item, method) (( int32_t (*) (List_1_t678 *, Vector4_t680 , const MethodInfo*))List_1_IndexOf_m22586_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m22587_gshared (List_1_t678 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m22587(__this, ___start, ___delta, method) (( void (*) (List_1_t678 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m22587_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m22588_gshared (List_1_t678 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m22588(__this, ___index, method) (( void (*) (List_1_t678 *, int32_t, const MethodInfo*))List_1_CheckIndex_m22588_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m22589_gshared (List_1_t678 * __this, int32_t ___index, Vector4_t680  ___item, const MethodInfo* method);
#define List_1_Insert_m22589(__this, ___index, ___item, method) (( void (*) (List_1_t678 *, int32_t, Vector4_t680 , const MethodInfo*))List_1_Insert_m22589_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m22590_gshared (List_1_t678 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m22590(__this, ___collection, method) (( void (*) (List_1_t678 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m22590_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector4>::Remove(T)
extern "C" bool List_1_Remove_m22591_gshared (List_1_t678 * __this, Vector4_t680  ___item, const MethodInfo* method);
#define List_1_Remove_m22591(__this, ___item, method) (( bool (*) (List_1_t678 *, Vector4_t680 , const MethodInfo*))List_1_Remove_m22591_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m22592_gshared (List_1_t678 * __this, Predicate_1_t3066 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m22592(__this, ___match, method) (( int32_t (*) (List_1_t678 *, Predicate_1_t3066 *, const MethodInfo*))List_1_RemoveAll_m22592_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m22593_gshared (List_1_t678 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m22593(__this, ___index, method) (( void (*) (List_1_t678 *, int32_t, const MethodInfo*))List_1_RemoveAt_m22593_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m22594_gshared (List_1_t678 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m22594(__this, ___index, ___count, method) (( void (*) (List_1_t678 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m22594_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Reverse()
extern "C" void List_1_Reverse_m22595_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_Reverse_m22595(__this, method) (( void (*) (List_1_t678 *, const MethodInfo*))List_1_Reverse_m22595_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Sort()
extern "C" void List_1_Sort_m22596_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_Sort_m22596(__this, method) (( void (*) (List_1_t678 *, const MethodInfo*))List_1_Sort_m22596_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m22597_gshared (List_1_t678 * __this, Comparison_1_t3069 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m22597(__this, ___comparison, method) (( void (*) (List_1_t678 *, Comparison_1_t3069 *, const MethodInfo*))List_1_Sort_m22597_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector4>::ToArray()
extern "C" Vector4U5BU5D_t779* List_1_ToArray_m22598_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_ToArray_m22598(__this, method) (( Vector4U5BU5D_t779* (*) (List_1_t678 *, const MethodInfo*))List_1_ToArray_m22598_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::TrimExcess()
extern "C" void List_1_TrimExcess_m22599_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m22599(__this, method) (( void (*) (List_1_t678 *, const MethodInfo*))List_1_TrimExcess_m22599_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m22600_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m22600(__this, method) (( int32_t (*) (List_1_t678 *, const MethodInfo*))List_1_get_Capacity_m22600_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m22601_gshared (List_1_t678 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m22601(__this, ___value, method) (( void (*) (List_1_t678 *, int32_t, const MethodInfo*))List_1_set_Capacity_m22601_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Count()
extern "C" int32_t List_1_get_Count_m22602_gshared (List_1_t678 * __this, const MethodInfo* method);
#define List_1_get_Count_m22602(__this, method) (( int32_t (*) (List_1_t678 *, const MethodInfo*))List_1_get_Count_m22602_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C" Vector4_t680  List_1_get_Item_m22603_gshared (List_1_t678 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m22603(__this, ___index, method) (( Vector4_t680  (*) (List_1_t678 *, int32_t, const MethodInfo*))List_1_get_Item_m22603_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m22604_gshared (List_1_t678 * __this, int32_t ___index, Vector4_t680  ___value, const MethodInfo* method);
#define List_1_set_Item_m22604(__this, ___index, ___value, method) (( void (*) (List_1_t678 *, int32_t, Vector4_t680 , const MethodInfo*))List_1_set_Item_m22604_gshared)(__this, ___index, ___value, method)
