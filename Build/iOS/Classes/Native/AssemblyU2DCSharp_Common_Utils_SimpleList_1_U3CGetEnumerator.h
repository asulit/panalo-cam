﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// Common.Utils.SimpleList`1<System.Object>
struct SimpleList_1_t2598;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>
struct  U3CGetEnumeratorU3Ec__Iterator4_t2599  : public Object_t
{
	// System.Int32 Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Int32 Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::$PC
	int32_t ___U24PC_1;
	// T Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::$current
	Object_t * ___U24current_2;
	// Common.Utils.SimpleList`1<T> Common.Utils.SimpleList`1/<GetEnumerator>c__Iterator4<System.Object>::<>f__this
	SimpleList_1_t2598 * ___U3CU3Ef__this_3;
};
