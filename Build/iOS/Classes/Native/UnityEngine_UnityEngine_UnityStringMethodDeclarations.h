﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t356;

// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C" String_t* UnityString_Format_m4240 (Object_t * __this /* static, unused */, String_t* ___fmt, ObjectU5BU5D_t356* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
