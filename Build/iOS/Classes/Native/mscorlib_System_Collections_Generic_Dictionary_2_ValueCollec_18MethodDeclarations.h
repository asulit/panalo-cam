﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_54MethodDeclarations.h"
#define Enumerator__ctor_m27728(__this, ___host, method) (( void (*) (Enumerator_t1344 *, Dictionary_2_t1203 *, const MethodInfo*))Enumerator__ctor_m19866_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27729(__this, method) (( Object_t * (*) (Enumerator_t1344 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19867_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m7378(__this, method) (( void (*) (Enumerator_t1344 *, const MethodInfo*))Enumerator_Dispose_m19868_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m7377(__this, method) (( bool (*) (Enumerator_t1344 *, const MethodInfo*))Enumerator_MoveNext_m19869_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m7376(__this, method) (( PropAbstractBehaviour_t300 * (*) (Enumerator_t1344 *, const MethodInfo*))Enumerator_get_Current_m19870_gshared)(__this, method)
