﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m27035(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3378 *, String_t*, List_1_t1186 *, const MethodInfo*))KeyValuePair_2__ctor_m15105_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Key()
#define KeyValuePair_2_get_Key_m27036(__this, method) (( String_t* (*) (KeyValuePair_2_t3378 *, const MethodInfo*))KeyValuePair_2_get_Key_m15106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m27037(__this, ___value, method) (( void (*) (KeyValuePair_2_t3378 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15107_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Value()
#define KeyValuePair_2_get_Value_m27038(__this, method) (( List_1_t1186 * (*) (KeyValuePair_2_t3378 *, const MethodInfo*))KeyValuePair_2_get_Value_m15108_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m27039(__this, ___value, method) (( void (*) (KeyValuePair_2_t3378 *, List_1_t1186 *, const MethodInfo*))KeyValuePair_2_set_Value_m15109_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ToString()
#define KeyValuePair_2_ToString_m27040(__this, method) (( String_t* (*) (KeyValuePair_2_t3378 *, const MethodInfo*))KeyValuePair_2_ToString_m15110_gshared)(__this, method)
