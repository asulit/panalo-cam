﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Boolean>
#include "System_Core_System_Func_2_gen_5MethodDeclarations.h"
#define Func_2__ctor_m3740(__this, ___object, ___method, method) (( void (*) (Func_2_t646 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m17070_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m21835(__this, ___arg1, method) (( bool (*) (Func_2_t646 *, Toggle_t557 *, const MethodInfo*))Func_2_Invoke_m17072_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m21836(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t646 *, Toggle_t557 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m17074_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m21837(__this, ___result, method) (( bool (*) (Func_2_t646 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m17076_gshared)(__this, ___result, method)
