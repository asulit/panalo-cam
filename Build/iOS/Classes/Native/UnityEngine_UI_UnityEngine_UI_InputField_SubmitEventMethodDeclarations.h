﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t595;

// System.Void UnityEngine.UI.InputField/SubmitEvent::.ctor()
extern "C" void SubmitEvent__ctor_m2506 (SubmitEvent_t595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
