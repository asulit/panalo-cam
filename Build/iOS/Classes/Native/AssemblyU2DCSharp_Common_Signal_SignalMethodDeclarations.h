﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Signal.Signal
struct Signal_t116;
// System.String
struct String_t;
// System.Object
struct Object_t;
// Common.Signal.Signal/SignalListener
struct SignalListener_t114;

// System.Void Common.Signal.Signal::.ctor(System.String)
extern "C" void Signal__ctor_m395 (Signal_t116 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Signal.Signal::ClearParameters()
extern "C" void Signal_ClearParameters_m396 (Signal_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Signal.Signal::AddParameter(System.String,System.Object)
extern "C" void Signal_AddParameter_m397 (Signal_t116 * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Signal.Signal::AddListener(Common.Signal.Signal/SignalListener)
extern "C" void Signal_AddListener_m398 (Signal_t116 * __this, SignalListener_t114 * ___listener, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Signal.Signal::RemoveListener(Common.Signal.Signal/SignalListener)
extern "C" void Signal_RemoveListener_m399 (Signal_t116 * __this, SignalListener_t114 * ___listener, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Signal.Signal::HasListener()
extern "C" bool Signal_HasListener_m400 (Signal_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Signal.Signal::HasListener(Common.Signal.Signal/SignalListener)
extern "C" bool Signal_HasListener_m401 (Signal_t116 * __this, SignalListener_t114 * ___listener, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Signal.Signal::Dispatch()
extern "C" void Signal_Dispatch_m402 (Signal_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Signal.Signal::get_Name()
extern "C" String_t* Signal_get_Name_m403 (Signal_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
