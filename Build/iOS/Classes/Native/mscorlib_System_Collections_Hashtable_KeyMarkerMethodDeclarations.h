﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Hashtable/KeyMarker
struct KeyMarker_t1880;

// System.Void System.Collections.Hashtable/KeyMarker::.ctor()
extern "C" void KeyMarker__ctor_m11067 (KeyMarker_t1880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/KeyMarker::.cctor()
extern "C" void KeyMarker__cctor_m11068 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
