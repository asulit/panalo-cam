﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t103;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// AnimatorFrame
struct  AnimatorFrame_t235 
{
	// System.Single AnimatorFrame::Duration
	float ___Duration_0;
	// UnityEngine.Texture AnimatorFrame::Texture
	Texture_t103 * ___Texture_1;
};
