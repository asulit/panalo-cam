﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.SortedList/ListKeys
struct ListKeys_t1891;
// System.Collections.SortedList
struct SortedList_t1757;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;

// System.Void System.Collections.SortedList/ListKeys::.ctor(System.Collections.SortedList)
extern "C" void ListKeys__ctor_m11146 (ListKeys_t1891 * __this, SortedList_t1757 * ___host, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::get_Count()
extern "C" int32_t ListKeys_get_Count_m11147 (ListKeys_t1891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsSynchronized()
extern "C" bool ListKeys_get_IsSynchronized_m11148 (ListKeys_t1891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/ListKeys::get_SyncRoot()
extern "C" Object_t * ListKeys_get_SyncRoot_m11149 (ListKeys_t1891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::CopyTo(System.Array,System.Int32)
extern "C" void ListKeys_CopyTo_m11150 (ListKeys_t1891 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsFixedSize()
extern "C" bool ListKeys_get_IsFixedSize_m11151 (ListKeys_t1891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsReadOnly()
extern "C" bool ListKeys_get_IsReadOnly_m11152 (ListKeys_t1891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/ListKeys::get_Item(System.Int32)
extern "C" Object_t * ListKeys_get_Item_m11153 (ListKeys_t1891 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::set_Item(System.Int32,System.Object)
extern "C" void ListKeys_set_Item_m11154 (ListKeys_t1891 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::Add(System.Object)
extern "C" int32_t ListKeys_Add_m11155 (ListKeys_t1891 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Clear()
extern "C" void ListKeys_Clear_m11156 (ListKeys_t1891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::Contains(System.Object)
extern "C" bool ListKeys_Contains_m11157 (ListKeys_t1891 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::IndexOf(System.Object)
extern "C" int32_t ListKeys_IndexOf_m11158 (ListKeys_t1891 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Insert(System.Int32,System.Object)
extern "C" void ListKeys_Insert_m11159 (ListKeys_t1891 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Remove(System.Object)
extern "C" void ListKeys_Remove_m11160 (ListKeys_t1891 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::RemoveAt(System.Int32)
extern "C" void ListKeys_RemoveAt_m11161 (ListKeys_t1891 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.SortedList/ListKeys::GetEnumerator()
extern "C" Object_t * ListKeys_GetEnumerator_m11162 (ListKeys_t1891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
