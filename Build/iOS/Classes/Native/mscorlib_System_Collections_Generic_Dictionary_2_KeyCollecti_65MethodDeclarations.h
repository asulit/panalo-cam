﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_29MethodDeclarations.h"
#define KeyCollection__ctor_m28944(__this, ___dictionary, method) (( void (*) (KeyCollection_t3498 *, Dictionary_2_t1230 *, const MethodInfo*))KeyCollection__ctor_m19817_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m28945(__this, ___item, method) (( void (*) (KeyCollection_t3498 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m19818_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m28946(__this, method) (( void (*) (KeyCollection_t3498 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m19819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m28947(__this, ___item, method) (( bool (*) (KeyCollection_t3498 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m19820_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m28948(__this, ___item, method) (( bool (*) (KeyCollection_t3498 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m19821_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m28949(__this, method) (( Object_t* (*) (KeyCollection_t3498 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m19822_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m28950(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3498 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m19823_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m28951(__this, method) (( Object_t * (*) (KeyCollection_t3498 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m19824_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m28952(__this, method) (( bool (*) (KeyCollection_t3498 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m19825_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m28953(__this, method) (( bool (*) (KeyCollection_t3498 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m19826_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m28954(__this, method) (( Object_t * (*) (KeyCollection_t3498 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m19827_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m28955(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3498 *, Int32U5BU5D_t401*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m19828_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::GetEnumerator()
#define KeyCollection_GetEnumerator_m28956(__this, method) (( Enumerator_t3799  (*) (KeyCollection_t3498 *, const MethodInfo*))KeyCollection_GetEnumerator_m19829_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Count()
#define KeyCollection_get_Count_m28957(__this, method) (( int32_t (*) (KeyCollection_t3498 *, const MethodInfo*))KeyCollection_get_Count_m19830_gshared)(__this, method)
