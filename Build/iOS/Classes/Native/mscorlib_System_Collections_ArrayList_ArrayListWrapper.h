﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t450;
// System.Collections.ArrayList
#include "mscorlib_System_Collections_ArrayList.h"
// System.Collections.ArrayList/ArrayListWrapper
struct  ArrayListWrapper_t1872  : public ArrayList_t450
{
	// System.Collections.ArrayList System.Collections.ArrayList/ArrayListWrapper::m_InnerArrayList
	ArrayList_t450 * ___m_InnerArrayList_5;
};
