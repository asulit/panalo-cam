﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.TextMesh
struct TextMesh_t26;
// System.String
struct String_t;

// System.Void UnityEngine.TextMesh::set_text(System.String)
extern "C" void TextMesh_set_text_m1381 (TextMesh_t26 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
