﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.GraphicRegistry
struct GraphicRegistry_t581;
// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct Dictionary_2_t582;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t579;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.GraphicRegistry
struct  GraphicRegistry_t581  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>> UnityEngine.UI.GraphicRegistry::m_Graphics
	Dictionary_2_t582 * ___m_Graphics_1;
};
struct GraphicRegistry_t581_StaticFields{
	// UnityEngine.UI.GraphicRegistry UnityEngine.UI.GraphicRegistry::s_Instance
	GraphicRegistry_t581 * ___s_Instance_0;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> UnityEngine.UI.GraphicRegistry::s_EmptyList
	List_1_t579 * ___s_EmptyList_2;
};
