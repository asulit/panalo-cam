﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderExceptionFallback
struct EncoderExceptionFallback_t2271;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2274;
// System.Object
struct Object_t;

// System.Void System.Text.EncoderExceptionFallback::.ctor()
extern "C" void EncoderExceptionFallback__ctor_m13432 (EncoderExceptionFallback_t2271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallbackBuffer System.Text.EncoderExceptionFallback::CreateFallbackBuffer()
extern "C" EncoderFallbackBuffer_t2274 * EncoderExceptionFallback_CreateFallbackBuffer_m13433 (EncoderExceptionFallback_t2271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.EncoderExceptionFallback::Equals(System.Object)
extern "C" bool EncoderExceptionFallback_Equals_m13434 (EncoderExceptionFallback_t2271 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncoderExceptionFallback::GetHashCode()
extern "C" int32_t EncoderExceptionFallback_GetHashCode_m13435 (EncoderExceptionFallback_t2271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
