﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>
struct DefaultComparer_t3280;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C" void DefaultComparer__ctor_m25370_gshared (DefaultComparer_t3280 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m25370(__this, method) (( void (*) (DefaultComparer_t3280 *, const MethodInfo*))DefaultComparer__ctor_m25370_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m25371_gshared (DefaultComparer_t3280 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m25371(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3280 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m25371_gshared)(__this, ___x, ___y, method)
