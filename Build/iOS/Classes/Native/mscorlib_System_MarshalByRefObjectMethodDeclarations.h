﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MarshalByRefObject
struct MarshalByRefObject_t1643;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1794;

// System.Void System.MarshalByRefObject::.ctor()
extern "C" void MarshalByRefObject__ctor_m9497 (MarshalByRefObject_t1643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::get_ObjectIdentity()
extern "C" ServerIdentity_t1794 * MarshalByRefObject_get_ObjectIdentity_m10395 (MarshalByRefObject_t1643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
