﻿#pragma once
#include <stdint.h>
// Raffle
struct Raffle_t202;
// RaffleData
struct RaffleData_t206;
// System.Object
struct Object_t;
// LoadingSceneRoot
struct LoadingSceneRoot_t218;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>,System.Boolean>
struct Func_2_t219;
// System.Object
#include "mscorlib_System_Object.h"
// LoadingSceneRoot/<CheckRaffleResults>c__Iterator7
struct  U3CCheckRaffleResultsU3Ec__Iterator7_t217  : public Object_t
{
	// Raffle LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::<raffle>__0
	Raffle_t202 * ___U3CraffleU3E__0_0;
	// RaffleData LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::<raffleData>__1
	RaffleData_t206 * ___U3CraffleDataU3E__1_1;
	// System.Int32 LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::$PC
	int32_t ___U24PC_2;
	// System.Object LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::$current
	Object_t * ___U24current_3;
	// LoadingSceneRoot LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::<>f__this
	LoadingSceneRoot_t218 * ___U3CU3Ef__this_4;
};
struct U3CCheckRaffleResultsU3Ec__Iterator7_t217_StaticFields{
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>,System.Boolean> LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::<>f__am$cache5
	Func_2_t219 * ___U3CU3Ef__amU24cache5_5;
};
