﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.Vector3>
struct Comparison_1_t3037;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Comparison`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m22255_gshared (Comparison_1_t3037 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m22255(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3037 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m22255_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.Vector3>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m22256_gshared (Comparison_1_t3037 * __this, Vector3_t36  ___x, Vector3_t36  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m22256(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3037 *, Vector3_t36 , Vector3_t36 , const MethodInfo*))Comparison_1_Invoke_m22256_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Vector3>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m22257_gshared (Comparison_1_t3037 * __this, Vector3_t36  ___x, Vector3_t36  ___y, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m22257(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3037 *, Vector3_t36 , Vector3_t36 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m22257_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m22258_gshared (Comparison_1_t3037 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m22258(__this, ___result, method) (( int32_t (*) (Comparison_1_t3037 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m22258_gshared)(__this, ___result, method)
