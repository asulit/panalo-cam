﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.IPlayModeEditorUtility
struct IPlayModeEditorUtility_t1129;
// Vuforia.PlayModeEditorUtility
struct PlayModeEditorUtility_t1128;

// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::get_Instance()
extern "C" Object_t * PlayModeEditorUtility_get_Instance_m5727 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility::set_Instance(Vuforia.IPlayModeEditorUtility)
extern "C" void PlayModeEditorUtility_set_Instance_m5728 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility::.ctor()
extern "C" void PlayModeEditorUtility__ctor_m5729 (PlayModeEditorUtility_t1128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
