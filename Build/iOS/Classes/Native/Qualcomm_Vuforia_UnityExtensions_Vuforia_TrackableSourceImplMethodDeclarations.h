﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackableSourceImpl
struct TrackableSourceImpl_t1218;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.IntPtr Vuforia.TrackableSourceImpl::get_TrackableSourcePtr()
extern "C" IntPtr_t TrackableSourceImpl_get_TrackableSourcePtr_m6856 (TrackableSourceImpl_t1218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableSourceImpl::set_TrackableSourcePtr(System.IntPtr)
extern "C" void TrackableSourceImpl_set_TrackableSourcePtr_m6857 (TrackableSourceImpl_t1218 * __this, IntPtr_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableSourceImpl::.ctor(System.IntPtr)
extern "C" void TrackableSourceImpl__ctor_m6858 (TrackableSourceImpl_t1218 * __this, IntPtr_t ___trackableSourcePtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
