﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Poller
struct Poller_t240;
// System.Collections.IEnumerator
struct IEnumerator_t337;

// System.Void Poller::.ctor()
extern "C" void Poller__ctor_m858 (Poller_t240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Poller::Start()
extern "C" void Poller_Start_m859 (Poller_t240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Poller::OnEnable()
extern "C" void Poller_OnEnable_m860 (Poller_t240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Poller::OnDisable()
extern "C" void Poller_OnDisable_m861 (Poller_t240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Poller::StartPoll()
extern "C" Object_t * Poller_StartPoll_m862 (Poller_t240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
