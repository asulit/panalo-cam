﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// <PrivateImplementationDetails>{C4A9B53A-936F-421A-9398-89108BD15C01}/__StaticArrayInitTypeSize=24
#pragma pack(push, tp, 1)
struct  __StaticArrayInitTypeSizeU3D24_t1249 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t1249__padding[24];
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: <PrivateImplementationDetails>{C4A9B53A-936F-421A-9398-89108BD15C01}/__StaticArrayInitTypeSize=24
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D24_t1249_marshaled
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t1249__padding[24];
	};
};
#pragma pack(pop, tp)
