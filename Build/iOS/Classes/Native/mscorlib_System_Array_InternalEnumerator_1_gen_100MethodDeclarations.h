﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_100.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m28078_gshared (InternalEnumerator_1_t3426 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m28078(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3426 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m28078_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28079_gshared (InternalEnumerator_1_t3426 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28079(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3426 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28079_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m28080_gshared (InternalEnumerator_1_t3426 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m28080(__this, method) (( void (*) (InternalEnumerator_1_t3426 *, const MethodInfo*))InternalEnumerator_1_Dispose_m28080_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m28081_gshared (InternalEnumerator_1_t3426 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m28081(__this, method) (( bool (*) (InternalEnumerator_1_t3426 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m28081_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::get_Current()
extern "C" KeyValuePair_2_t3425  InternalEnumerator_1_get_Current_m28082_gshared (InternalEnumerator_1_t3426 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m28082(__this, method) (( KeyValuePair_2_t3425  (*) (InternalEnumerator_1_t3426 *, const MethodInfo*))InternalEnumerator_1_get_Current_m28082_gshared)(__this, method)
