﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m23877(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3163 *, String_t*, GUIStyle_t342 *, const MethodInfo*))KeyValuePair_2__ctor_m15105_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Key()
#define KeyValuePair_2_get_Key_m23878(__this, method) (( String_t* (*) (KeyValuePair_2_t3163 *, const MethodInfo*))KeyValuePair_2_get_Key_m15106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m23879(__this, ___value, method) (( void (*) (KeyValuePair_2_t3163 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15107_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Value()
#define KeyValuePair_2_get_Value_m23880(__this, method) (( GUIStyle_t342 * (*) (KeyValuePair_2_t3163 *, const MethodInfo*))KeyValuePair_2_get_Value_m15108_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m23881(__this, ___value, method) (( void (*) (KeyValuePair_2_t3163 *, GUIStyle_t342 *, const MethodInfo*))KeyValuePair_2_set_Value_m15109_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::ToString()
#define KeyValuePair_2_ToString_m23882(__this, method) (( String_t* (*) (KeyValuePair_2_t3163 *, const MethodInfo*))KeyValuePair_2_ToString_m15110_gshared)(__this, method)
