﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Metadata.SoapParameterAttribute
struct SoapParameterAttribute_t2126;

// System.Void System.Runtime.Remoting.Metadata.SoapParameterAttribute::.ctor()
extern "C" void SoapParameterAttribute__ctor_m12587 (SoapParameterAttribute_t2126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
