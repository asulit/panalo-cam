﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Comparison`1<System.Int32>
struct  Comparison_1_t3077  : public MulticastDelegate_t28
{
};
