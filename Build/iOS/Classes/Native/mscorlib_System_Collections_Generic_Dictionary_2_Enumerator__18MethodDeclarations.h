﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2738;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__18.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17983_gshared (Enumerator_t2745 * __this, Dictionary_2_t2738 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m17983(__this, ___dictionary, method) (( void (*) (Enumerator_t2745 *, Dictionary_2_t2738 *, const MethodInfo*))Enumerator__ctor_m17983_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17984_gshared (Enumerator_t2745 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17984(__this, method) (( Object_t * (*) (Enumerator_t2745 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17984_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17985_gshared (Enumerator_t2745 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17985(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t2745 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17985_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17986_gshared (Enumerator_t2745 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17986(__this, method) (( Object_t * (*) (Enumerator_t2745 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17986_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17987_gshared (Enumerator_t2745 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17987(__this, method) (( Object_t * (*) (Enumerator_t2745 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17987_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17988_gshared (Enumerator_t2745 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17988(__this, method) (( bool (*) (Enumerator_t2745 *, const MethodInfo*))Enumerator_MoveNext_m17988_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" KeyValuePair_2_t2740  Enumerator_get_Current_m17989_gshared (Enumerator_t2745 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17989(__this, method) (( KeyValuePair_2_t2740  (*) (Enumerator_t2745 *, const MethodInfo*))Enumerator_get_Current_m17989_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m17990_gshared (Enumerator_t2745 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m17990(__this, method) (( Object_t * (*) (Enumerator_t2745 *, const MethodInfo*))Enumerator_get_CurrentKey_m17990_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C" bool Enumerator_get_CurrentValue_m17991_gshared (Enumerator_t2745 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m17991(__this, method) (( bool (*) (Enumerator_t2745 *, const MethodInfo*))Enumerator_get_CurrentValue_m17991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern "C" void Enumerator_VerifyState_m17992_gshared (Enumerator_t2745 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17992(__this, method) (( void (*) (Enumerator_t2745 *, const MethodInfo*))Enumerator_VerifyState_m17992_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m17993_gshared (Enumerator_t2745 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m17993(__this, method) (( void (*) (Enumerator_t2745 *, const MethodInfo*))Enumerator_VerifyCurrent_m17993_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m17994_gshared (Enumerator_t2745 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17994(__this, method) (( void (*) (Enumerator_t2745 *, const MethodInfo*))Enumerator_Dispose_m17994_gshared)(__this, method)
