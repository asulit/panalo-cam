﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct UnityAction_4_t3206;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
struct  InvokableCall_4_t3205  : public BaseInvokableCall_t981
{
	// UnityEngine.Events.UnityAction`4<T1,T2,T3,T4> UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Delegate
	UnityAction_4_t3206 * ___Delegate_0;
};
