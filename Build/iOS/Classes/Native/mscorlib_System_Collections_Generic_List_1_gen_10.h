﻿#pragma once
#include <stdint.h>
// AnimatorFrame[]
struct AnimatorFrameU5BU5D_t2770;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<AnimatorFrame>
struct  List_1_t238  : public Object_t
{
	// T[] System.Collections.Generic.List`1<AnimatorFrame>::_items
	AnimatorFrameU5BU5D_t2770* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<AnimatorFrame>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<AnimatorFrame>::_version
	int32_t ____version_3;
};
struct List_1_t238_StaticFields{
	// T[] System.Collections.Generic.List`1<AnimatorFrame>::EmptyArray
	AnimatorFrameU5BU5D_t2770* ___EmptyArray_4;
};
