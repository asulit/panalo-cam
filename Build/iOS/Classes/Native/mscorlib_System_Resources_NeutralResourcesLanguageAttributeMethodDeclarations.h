﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Resources.NeutralResourcesLanguageAttribute
struct NeutralResourcesLanguageAttribute_t2032;
// System.String
struct String_t;

// System.Void System.Resources.NeutralResourcesLanguageAttribute::.ctor(System.String)
extern "C" void NeutralResourcesLanguageAttribute__ctor_m12281 (NeutralResourcesLanguageAttribute_t2032 * __this, String_t* ___cultureName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
