﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>

extern Il2CppAssembly* g_Assemblies[];
extern Il2CppImage* g_Images[];
extern Il2CppGenericClass* s_Il2CppGenericTypes[];
extern const Il2CppGenericInst* g_Il2CppGenericInstTable[];
extern Il2CppGenericMethodFunctionsDefinitions s_Il2CppGenericMethodFunctions[];
extern const Il2CppType* const  g_Il2CppTypeTable[];
extern const Il2CppMethodSpec g_Il2CppMethodSpecTable[];
extern const EncodedMethodIndex  g_Il2CppMethodReferenceTable[];
extern const Il2CppMethodDefinitionReference g_Il2CppMethodRefTable[];
extern const int32_t g_FieldOffsetTable[];
extern const Il2CppTypeDefinitionSizes g_Il2CppTypeDefinitionSizesTable[];
extern const Il2CppMetadataRegistration g_MetadataRegistration = 
{
	8,
	g_Assemblies,
	8,
	g_Images,
	2896,
	s_Il2CppGenericTypes,
	862,
	g_Il2CppGenericInstTable,
	6687,
	s_Il2CppGenericMethodFunctions,
	9607,
	g_Il2CppTypeTable,
	7075,
	g_Il2CppMethodSpecTable,
	6328,
	g_Il2CppMethodReferenceTable,
	5517,
	g_Il2CppMethodRefTable,
	8047,
	g_FieldOffsetTable,
	2111,
	g_Il2CppTypeDefinitionSizesTable,
};
