﻿#pragma once
#include <stdint.h>
// System.Threading.ManualResetEvent
struct ManualResetEvent_t160;
// System.Object
#include "mscorlib_System_Object.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityThreading.TaskBase
struct  TaskBase_t163  : public Object_t
{
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) UnityThreading.TaskBase::Priority
	int32_t ___Priority_0;
	// System.Threading.ManualResetEvent UnityThreading.TaskBase::abortEvent
	ManualResetEvent_t160 * ___abortEvent_1;
	// System.Threading.ManualResetEvent UnityThreading.TaskBase::endedEvent
	ManualResetEvent_t160 * ___endedEvent_2;
	// System.Boolean UnityThreading.TaskBase::hasStarted
	bool ___hasStarted_3;
};
