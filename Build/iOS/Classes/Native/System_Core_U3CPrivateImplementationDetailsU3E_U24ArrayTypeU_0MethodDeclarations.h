﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU24120_t1411_marshal(const U24ArrayTypeU24120_t1411& unmarshaled, U24ArrayTypeU24120_t1411_marshaled& marshaled);
extern "C" void U24ArrayTypeU24120_t1411_marshal_back(const U24ArrayTypeU24120_t1411_marshaled& marshaled, U24ArrayTypeU24120_t1411& unmarshaled);
extern "C" void U24ArrayTypeU24120_t1411_marshal_cleanup(U24ArrayTypeU24120_t1411_marshaled& marshaled);
