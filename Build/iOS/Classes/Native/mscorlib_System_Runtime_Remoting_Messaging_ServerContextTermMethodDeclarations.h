﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.ServerContextTerminatorSink
struct ServerContextTerminatorSink_t2119;

// System.Void System.Runtime.Remoting.Messaging.ServerContextTerminatorSink::.ctor()
extern "C" void ServerContextTerminatorSink__ctor_m12572 (ServerContextTerminatorSink_t2119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
