﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MeshMerger
struct MeshMerger_t80;

// System.Void MeshMerger::.ctor()
extern "C" void MeshMerger__ctor_m279 (MeshMerger_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MeshMerger::Start()
extern "C" void MeshMerger_Start_m280 (MeshMerger_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
