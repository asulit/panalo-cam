﻿#pragma once
#include <stdint.h>
// System.Action
struct Action_t16;
// System.Object
#include "mscorlib_System_Object.h"
// UnityThreading.Dispatcher/<CreateSafeAction>c__AnonStorey11`1<System.Object>
struct  U3CCreateSafeActionU3Ec__AnonStorey11_1_t2652  : public Object_t
{
	// System.Action UnityThreading.Dispatcher/<CreateSafeAction>c__AnonStorey11`1<System.Object>::action
	Action_t16 * ___action_0;
};
