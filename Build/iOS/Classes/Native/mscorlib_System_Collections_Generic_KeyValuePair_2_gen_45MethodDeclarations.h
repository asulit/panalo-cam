﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45.h"
// Vuforia.VuforiaManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__0.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m28083_gshared (KeyValuePair_2_t3425 * __this, int32_t ___key, TrackableResultData_t1134  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m28083(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3425 *, int32_t, TrackableResultData_t1134 , const MethodInfo*))KeyValuePair_2__ctor_m28083_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m28084_gshared (KeyValuePair_2_t3425 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m28084(__this, method) (( int32_t (*) (KeyValuePair_2_t3425 *, const MethodInfo*))KeyValuePair_2_get_Key_m28084_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m28085_gshared (KeyValuePair_2_t3425 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m28085(__this, ___value, method) (( void (*) (KeyValuePair_2_t3425 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m28085_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Value()
extern "C" TrackableResultData_t1134  KeyValuePair_2_get_Value_m28086_gshared (KeyValuePair_2_t3425 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m28086(__this, method) (( TrackableResultData_t1134  (*) (KeyValuePair_2_t3425 *, const MethodInfo*))KeyValuePair_2_get_Value_m28086_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m28087_gshared (KeyValuePair_2_t3425 * __this, TrackableResultData_t1134  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m28087(__this, ___value, method) (( void (*) (KeyValuePair_2_t3425 *, TrackableResultData_t1134 , const MethodInfo*))KeyValuePair_2_set_Value_m28087_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m28088_gshared (KeyValuePair_2_t3425 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m28088(__this, method) (( String_t* (*) (KeyValuePair_2_t3425 *, const MethodInfo*))KeyValuePair_2_ToString_m28088_gshared)(__this, method)
