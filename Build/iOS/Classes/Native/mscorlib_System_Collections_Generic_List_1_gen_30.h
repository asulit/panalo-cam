﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.IClippable[]
struct IClippableU5BU5D_t2980;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.UI.IClippable>
struct  List_1_t615  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::_items
	IClippableU5BU5D_t2980* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::_version
	int32_t ____version_3;
};
struct List_1_t615_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.IClippable>::EmptyArray
	IClippableU5BU5D_t2980* ___EmptyArray_4;
};
