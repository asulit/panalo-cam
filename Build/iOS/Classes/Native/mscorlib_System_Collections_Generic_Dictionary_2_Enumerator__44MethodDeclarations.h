﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22MethodDeclarations.h"
#define Enumerator__ctor_m28005(__this, ___dictionary, method) (( void (*) (Enumerator_t3420 *, Dictionary_2_t1207 *, const MethodInfo*))Enumerator__ctor_m19836_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m28006(__this, method) (( Object_t * (*) (Enumerator_t3420 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19837_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m28007(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3420 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19838_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m28008(__this, method) (( Object_t * (*) (Enumerator_t3420 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19839_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m28009(__this, method) (( Object_t * (*) (Enumerator_t3420 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19840_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::MoveNext()
#define Enumerator_MoveNext_m28010(__this, method) (( bool (*) (Enumerator_t3420 *, const MethodInfo*))Enumerator_MoveNext_m19841_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::get_Current()
#define Enumerator_get_Current_m28011(__this, method) (( KeyValuePair_2_t3419  (*) (Enumerator_t3420 *, const MethodInfo*))Enumerator_get_Current_m19842_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m28012(__this, method) (( int32_t (*) (Enumerator_t3420 *, const MethodInfo*))Enumerator_get_CurrentKey_m19843_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m28013(__this, method) (( TrackableBehaviour_t211 * (*) (Enumerator_t3420 *, const MethodInfo*))Enumerator_get_CurrentValue_m19844_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::VerifyState()
#define Enumerator_VerifyState_m28014(__this, method) (( void (*) (Enumerator_t3420 *, const MethodInfo*))Enumerator_VerifyState_m19845_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m28015(__this, method) (( void (*) (Enumerator_t3420 *, const MethodInfo*))Enumerator_VerifyCurrent_m19846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.TrackableBehaviour>::Dispose()
#define Enumerator_Dispose_m28016(__this, method) (( void (*) (Enumerator_t3420 *, const MethodInfo*))Enumerator_Dispose_m19847_gshared)(__this, method)
