﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>
struct KeyCollection_t2693;
// System.Collections.Generic.Dictionary`2<ESubScreens,System.Object>
struct Dictionary_2_t2688;
// System.Collections.Generic.IEnumerator`1<ESubScreens>
struct IEnumerator_1_t3657;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// ESubScreens[]
struct ESubScreensU5BU5D_t2685;
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ESubScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_20.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void KeyCollection__ctor_m17435_gshared (KeyCollection_t2693 * __this, Dictionary_2_t2688 * ___dictionary, const MethodInfo* method);
#define KeyCollection__ctor_m17435(__this, ___dictionary, method) (( void (*) (KeyCollection_t2693 *, Dictionary_2_t2688 *, const MethodInfo*))KeyCollection__ctor_m17435_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17436_gshared (KeyCollection_t2693 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17436(__this, ___item, method) (( void (*) (KeyCollection_t2693 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17436_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17437_gshared (KeyCollection_t2693 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17437(__this, method) (( void (*) (KeyCollection_t2693 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17437_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17438_gshared (KeyCollection_t2693 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17438(__this, ___item, method) (( bool (*) (KeyCollection_t2693 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17438_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17439_gshared (KeyCollection_t2693 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17439(__this, ___item, method) (( bool (*) (KeyCollection_t2693 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17439_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17440_gshared (KeyCollection_t2693 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17440(__this, method) (( Object_t* (*) (KeyCollection_t2693 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17440_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m17441_gshared (KeyCollection_t2693 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m17441(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2693 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m17441_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17442_gshared (KeyCollection_t2693 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17442(__this, method) (( Object_t * (*) (KeyCollection_t2693 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17442_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17443_gshared (KeyCollection_t2693 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17443(__this, method) (( bool (*) (KeyCollection_t2693 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17443_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17444_gshared (KeyCollection_t2693 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17444(__this, method) (( bool (*) (KeyCollection_t2693 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17444_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m17445_gshared (KeyCollection_t2693 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m17445(__this, method) (( Object_t * (*) (KeyCollection_t2693 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m17445_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m17446_gshared (KeyCollection_t2693 * __this, ESubScreensU5BU5D_t2685* ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_CopyTo_m17446(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2693 *, ESubScreensU5BU5D_t2685*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m17446_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::GetEnumerator()
extern "C" Enumerator_t2694  KeyCollection_GetEnumerator_m17447_gshared (KeyCollection_t2693 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m17447(__this, method) (( Enumerator_t2694  (*) (KeyCollection_t2693 *, const MethodInfo*))KeyCollection_GetEnumerator_m17447_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ESubScreens,System.Object>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m17448_gshared (KeyCollection_t2693 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m17448(__this, method) (( int32_t (*) (KeyCollection_t2693 *, const MethodInfo*))KeyCollection_get_Count_m17448_gshared)(__this, method)
