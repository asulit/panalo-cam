﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_104.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_48.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m28743_gshared (InternalEnumerator_1_t3479 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m28743(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3479 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m28743_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28744_gshared (InternalEnumerator_1_t3479 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28744(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3479 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28744_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m28745_gshared (InternalEnumerator_1_t3479 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m28745(__this, method) (( void (*) (InternalEnumerator_1_t3479 *, const MethodInfo*))InternalEnumerator_1_Dispose_m28745_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m28746_gshared (InternalEnumerator_1_t3479 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m28746(__this, method) (( bool (*) (InternalEnumerator_1_t3479 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m28746_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::get_Current()
extern "C" KeyValuePair_2_t3478  InternalEnumerator_1_get_Current_m28747_gshared (InternalEnumerator_1_t3479 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m28747(__this, method) (( KeyValuePair_2_t3478  (*) (InternalEnumerator_1_t3479 *, const MethodInfo*))InternalEnumerator_1_get_Current_m28747_gshared)(__this, method)
