﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t196;
// UnityEngine.UI.Image
struct Image_t213;
// System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode>
struct List_1_t214;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ScannerHudRoot
struct  ScannerHudRoot_t212  : public MonoBehaviour_t5
{
	// UnityEngine.UI.Text ScannerHudRoot::focusText
	Text_t196 * ___focusText_2;
	// UnityEngine.UI.Image ScannerHudRoot::border
	Image_t213 * ___border_3;
	// System.Int32 ScannerHudRoot::focusModeIndex
	int32_t ___focusModeIndex_4;
	// System.Collections.Generic.List`1<Vuforia.CameraDevice/FocusMode> ScannerHudRoot::focusModes
	List_1_t214 * ___focusModes_5;
};
