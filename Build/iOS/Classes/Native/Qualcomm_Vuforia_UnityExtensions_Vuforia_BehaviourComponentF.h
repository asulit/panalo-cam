﻿#pragma once
#include <stdint.h>
// Vuforia.IBehaviourComponentFactory
struct IBehaviourComponentFactory_t1095;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.BehaviourComponentFactory
struct  BehaviourComponentFactory_t1094  : public Object_t
{
};
struct BehaviourComponentFactory_t1094_StaticFields{
	// Vuforia.IBehaviourComponentFactory Vuforia.BehaviourComponentFactory::sInstance
	Object_t * ___sInstance_0;
};
