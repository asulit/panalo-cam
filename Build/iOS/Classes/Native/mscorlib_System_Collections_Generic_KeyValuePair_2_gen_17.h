﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>
struct  KeyValuePair_2_t2704 
{
	// TKey System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<ESubScreens,System.String>::value
	String_t* ___value_1;
};
