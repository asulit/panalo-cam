﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t2944;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m20861_gshared (DefaultComparer_t2944 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m20861(__this, method) (( void (*) (DefaultComparer_t2944 *, const MethodInfo*))DefaultComparer__ctor_m20861_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m20862_gshared (DefaultComparer_t2944 * __this, UIVertex_t605  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m20862(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2944 *, UIVertex_t605 , const MethodInfo*))DefaultComparer_GetHashCode_m20862_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m20863_gshared (DefaultComparer_t2944 * __this, UIVertex_t605  ___x, UIVertex_t605  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m20863(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2944 *, UIVertex_t605 , UIVertex_t605 , const MethodInfo*))DefaultComparer_Equals_m20863_gshared)(__this, ___x, ___y, method)
