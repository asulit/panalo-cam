﻿#pragma once
#include <stdint.h>
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t1252;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.ITrackableEventHandler>
struct  Comparison_1_t3234  : public MulticastDelegate_t28
{
};
