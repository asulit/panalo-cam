﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver>
struct Dictionary_2_t110;
// Common.Query.ConcreteQueryResult
struct ConcreteQueryResult_t107;
// Common.Query.ConcreteQueryRequest
struct ConcreteQueryRequest_t106;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Query.QuerySystemImplementation
struct  QuerySystemImplementation_t109  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<System.String,Common.Query.QueryResultResolver> Common.Query.QuerySystemImplementation::resolverMap
	Dictionary_2_t110 * ___resolverMap_0;
	// Common.Query.ConcreteQueryResult Common.Query.QuerySystemImplementation::result
	ConcreteQueryResult_t107 * ___result_1;
	// Common.Query.ConcreteQueryRequest Common.Query.QuerySystemImplementation::request
	ConcreteQueryRequest_t106 * ___request_2;
	// System.Boolean Common.Query.QuerySystemImplementation::locked
	bool ___locked_3;
};
