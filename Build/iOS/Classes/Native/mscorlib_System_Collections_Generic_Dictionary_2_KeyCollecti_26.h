﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,System.Boolean>
struct Dictionary_2_t227;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>
struct  KeyCollection_t2756  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.InputField,System.Boolean>::dictionary
	Dictionary_2_t227 * ___dictionary_0;
};
