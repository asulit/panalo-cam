﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<AnimatorFrame>
struct IList_1_t2773;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<AnimatorFrame>
struct  Collection_1_t2774  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<AnimatorFrame>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<AnimatorFrame>::syncRoot
	Object_t * ___syncRoot_1;
};
