﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamTexAdaptor
struct WebCamTexAdaptor_t1125;

// System.Void Vuforia.WebCamTexAdaptor::.ctor()
extern "C" void WebCamTexAdaptor__ctor_m5716 (WebCamTexAdaptor_t1125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
