﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t732;
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// UnityEngine.Canvas
struct  Canvas_t574  : public Behaviour_t772
{
};
struct Canvas_t574_StaticFields{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t732 * ___willRenderCanvases_2;
};
