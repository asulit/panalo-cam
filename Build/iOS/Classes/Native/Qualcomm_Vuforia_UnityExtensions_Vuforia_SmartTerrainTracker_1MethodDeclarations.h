﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerImpl
struct SmartTerrainTrackerImpl_t1169;
// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t1078;

// System.Single Vuforia.SmartTerrainTrackerImpl::get_ScaleToMillimeter()
extern "C" float SmartTerrainTrackerImpl_get_ScaleToMillimeter_m5829 (SmartTerrainTrackerImpl_t1169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::SetScaleToMillimeter(System.Single)
extern "C" bool SmartTerrainTrackerImpl_SetScaleToMillimeter_m5830 (SmartTerrainTrackerImpl_t1169 * __this, float ___scaleFactor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTrackerImpl::get_SmartTerrainBuilder()
extern "C" SmartTerrainBuilder_t1078 * SmartTerrainTrackerImpl_get_SmartTerrainBuilder_m5831 (SmartTerrainTrackerImpl_t1169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::Start()
extern "C" bool SmartTerrainTrackerImpl_Start_m5832 (SmartTerrainTrackerImpl_t1169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::Stop()
extern "C" void SmartTerrainTrackerImpl_Stop_m5833 (SmartTerrainTrackerImpl_t1169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::.ctor()
extern "C" void SmartTerrainTrackerImpl__ctor_m5834 (SmartTerrainTrackerImpl_t1169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
