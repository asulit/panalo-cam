﻿#pragma once
#include <stdint.h>
// Common.Fsm.FsmAction
struct FsmAction_t51;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// Common.Fsm.Fsm/StateActionProcessor
struct  StateActionProcessor_t50  : public MulticastDelegate_t28
{
};
