﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// RaffleData
struct RaffleData_t206;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Raffle
struct  Raffle_t202  : public MonoBehaviour_t5
{
	// RaffleData Raffle::raffleData
	RaffleData_t206 * ___raffleData_32;
	// System.String Raffle::RegisterUserMessage
	String_t* ___RegisterUserMessage_33;
};
