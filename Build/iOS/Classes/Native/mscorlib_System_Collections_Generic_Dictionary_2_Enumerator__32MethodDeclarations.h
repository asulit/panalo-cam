﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"
#define Enumerator__ctor_m24615(__this, ___dictionary, method) (( void (*) (Enumerator_t3229 *, Dictionary_2_t1054 *, const MethodInfo*))Enumerator__ctor_m15140_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24616(__this, method) (( Object_t * (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15141_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24617(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15142_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24618(__this, method) (( Object_t * (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15143_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24619(__this, method) (( Object_t * (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m24620(__this, method) (( bool (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_MoveNext_m15145_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m24621(__this, method) (( KeyValuePair_2_t3227  (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_get_Current_m15146_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m24622(__this, method) (( Camera_t6 * (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_get_CurrentKey_m15147_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m24623(__this, method) (( VideoBackgroundAbstractBehaviour_t315 * (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_get_CurrentValue_m15148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m24624(__this, method) (( void (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_VerifyState_m15149_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24625(__this, method) (( void (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_VerifyCurrent_m15150_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m24626(__this, method) (( void (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_Dispose_m15151_gshared)(__this, method)
