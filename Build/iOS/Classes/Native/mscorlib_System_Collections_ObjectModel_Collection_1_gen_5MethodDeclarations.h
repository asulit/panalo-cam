﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>
struct Collection_1_t3042;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// UnityEngine.Color32[]
struct Color32U5BU5D_t778;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t3714;
// System.Collections.Generic.IList`1<UnityEngine.Color32>
struct IList_1_t3041;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::.ctor()
extern "C" void Collection_1__ctor_m22351_gshared (Collection_1_t3042 * __this, const MethodInfo* method);
#define Collection_1__ctor_m22351(__this, method) (( void (*) (Collection_1_t3042 *, const MethodInfo*))Collection_1__ctor_m22351_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22352_gshared (Collection_1_t3042 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22352(__this, method) (( bool (*) (Collection_1_t3042 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22352_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m22353_gshared (Collection_1_t3042 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m22353(__this, ___array, ___index, method) (( void (*) (Collection_1_t3042 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m22353_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m22354_gshared (Collection_1_t3042 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m22354(__this, method) (( Object_t * (*) (Collection_1_t3042 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m22354_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m22355_gshared (Collection_1_t3042 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m22355(__this, ___value, method) (( int32_t (*) (Collection_1_t3042 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m22355_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m22356_gshared (Collection_1_t3042 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m22356(__this, ___value, method) (( bool (*) (Collection_1_t3042 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m22356_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m22357_gshared (Collection_1_t3042 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m22357(__this, ___value, method) (( int32_t (*) (Collection_1_t3042 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m22357_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m22358_gshared (Collection_1_t3042 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m22358(__this, ___index, ___value, method) (( void (*) (Collection_1_t3042 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m22358_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m22359_gshared (Collection_1_t3042 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m22359(__this, ___value, method) (( void (*) (Collection_1_t3042 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m22359_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m22360_gshared (Collection_1_t3042 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m22360(__this, method) (( bool (*) (Collection_1_t3042 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m22360_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m22361_gshared (Collection_1_t3042 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m22361(__this, method) (( Object_t * (*) (Collection_1_t3042 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m22361_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m22362_gshared (Collection_1_t3042 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m22362(__this, method) (( bool (*) (Collection_1_t3042 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m22362_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m22363_gshared (Collection_1_t3042 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m22363(__this, method) (( bool (*) (Collection_1_t3042 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m22363_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m22364_gshared (Collection_1_t3042 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m22364(__this, ___index, method) (( Object_t * (*) (Collection_1_t3042 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m22364_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m22365_gshared (Collection_1_t3042 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m22365(__this, ___index, ___value, method) (( void (*) (Collection_1_t3042 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m22365_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Add(T)
extern "C" void Collection_1_Add_m22366_gshared (Collection_1_t3042 * __this, Color32_t711  ___item, const MethodInfo* method);
#define Collection_1_Add_m22366(__this, ___item, method) (( void (*) (Collection_1_t3042 *, Color32_t711 , const MethodInfo*))Collection_1_Add_m22366_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Clear()
extern "C" void Collection_1_Clear_m22367_gshared (Collection_1_t3042 * __this, const MethodInfo* method);
#define Collection_1_Clear_m22367(__this, method) (( void (*) (Collection_1_t3042 *, const MethodInfo*))Collection_1_Clear_m22367_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::ClearItems()
extern "C" void Collection_1_ClearItems_m22368_gshared (Collection_1_t3042 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m22368(__this, method) (( void (*) (Collection_1_t3042 *, const MethodInfo*))Collection_1_ClearItems_m22368_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Contains(T)
extern "C" bool Collection_1_Contains_m22369_gshared (Collection_1_t3042 * __this, Color32_t711  ___item, const MethodInfo* method);
#define Collection_1_Contains_m22369(__this, ___item, method) (( bool (*) (Collection_1_t3042 *, Color32_t711 , const MethodInfo*))Collection_1_Contains_m22369_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m22370_gshared (Collection_1_t3042 * __this, Color32U5BU5D_t778* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m22370(__this, ___array, ___index, method) (( void (*) (Collection_1_t3042 *, Color32U5BU5D_t778*, int32_t, const MethodInfo*))Collection_1_CopyTo_m22370_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m22371_gshared (Collection_1_t3042 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m22371(__this, method) (( Object_t* (*) (Collection_1_t3042 *, const MethodInfo*))Collection_1_GetEnumerator_m22371_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m22372_gshared (Collection_1_t3042 * __this, Color32_t711  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m22372(__this, ___item, method) (( int32_t (*) (Collection_1_t3042 *, Color32_t711 , const MethodInfo*))Collection_1_IndexOf_m22372_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m22373_gshared (Collection_1_t3042 * __this, int32_t ___index, Color32_t711  ___item, const MethodInfo* method);
#define Collection_1_Insert_m22373(__this, ___index, ___item, method) (( void (*) (Collection_1_t3042 *, int32_t, Color32_t711 , const MethodInfo*))Collection_1_Insert_m22373_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m22374_gshared (Collection_1_t3042 * __this, int32_t ___index, Color32_t711  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m22374(__this, ___index, ___item, method) (( void (*) (Collection_1_t3042 *, int32_t, Color32_t711 , const MethodInfo*))Collection_1_InsertItem_m22374_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Remove(T)
extern "C" bool Collection_1_Remove_m22375_gshared (Collection_1_t3042 * __this, Color32_t711  ___item, const MethodInfo* method);
#define Collection_1_Remove_m22375(__this, ___item, method) (( bool (*) (Collection_1_t3042 *, Color32_t711 , const MethodInfo*))Collection_1_Remove_m22375_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m22376_gshared (Collection_1_t3042 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m22376(__this, ___index, method) (( void (*) (Collection_1_t3042 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m22376_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m22377_gshared (Collection_1_t3042 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m22377(__this, ___index, method) (( void (*) (Collection_1_t3042 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m22377_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m22378_gshared (Collection_1_t3042 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m22378(__this, method) (( int32_t (*) (Collection_1_t3042 *, const MethodInfo*))Collection_1_get_Count_m22378_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C" Color32_t711  Collection_1_get_Item_m22379_gshared (Collection_1_t3042 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m22379(__this, ___index, method) (( Color32_t711  (*) (Collection_1_t3042 *, int32_t, const MethodInfo*))Collection_1_get_Item_m22379_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m22380_gshared (Collection_1_t3042 * __this, int32_t ___index, Color32_t711  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m22380(__this, ___index, ___value, method) (( void (*) (Collection_1_t3042 *, int32_t, Color32_t711 , const MethodInfo*))Collection_1_set_Item_m22380_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m22381_gshared (Collection_1_t3042 * __this, int32_t ___index, Color32_t711  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m22381(__this, ___index, ___item, method) (( void (*) (Collection_1_t3042 *, int32_t, Color32_t711 , const MethodInfo*))Collection_1_SetItem_m22381_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m22382_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m22382(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m22382_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::ConvertItem(System.Object)
extern "C" Color32_t711  Collection_1_ConvertItem_m22383_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m22383(__this /* static, unused */, ___item, method) (( Color32_t711  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m22383_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m22384_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m22384(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m22384_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m22385_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m22385(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m22385_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m22386_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m22386(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m22386_gshared)(__this /* static, unused */, ___list, method)
