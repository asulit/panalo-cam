﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22MethodDeclarations.h"
#define KeyValuePair_2__ctor_m28938(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3497 *, int32_t, VirtualButtonAbstractBehaviour_t319 *, const MethodInfo*))KeyValuePair_2__ctor_m19811_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m28939(__this, method) (( int32_t (*) (KeyValuePair_2_t3497 *, const MethodInfo*))KeyValuePair_2_get_Key_m19812_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m28940(__this, ___value, method) (( void (*) (KeyValuePair_2_t3497 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m19813_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m28941(__this, method) (( VirtualButtonAbstractBehaviour_t319 * (*) (KeyValuePair_2_t3497 *, const MethodInfo*))KeyValuePair_2_get_Value_m19814_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m28942(__this, ___value, method) (( void (*) (KeyValuePair_2_t3497 *, VirtualButtonAbstractBehaviour_t319 *, const MethodInfo*))KeyValuePair_2_set_Value_m19815_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m28943(__this, method) (( String_t* (*) (KeyValuePair_2_t3497 *, const MethodInfo*))KeyValuePair_2_ToString_m19816_gshared)(__this, method)
