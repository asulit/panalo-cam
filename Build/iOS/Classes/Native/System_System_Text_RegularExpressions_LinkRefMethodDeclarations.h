﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.LinkRef
struct LinkRef_t1698;

// System.Void System.Text.RegularExpressions.LinkRef::.ctor()
extern "C" void LinkRef__ctor_m9105 (LinkRef_t1698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
