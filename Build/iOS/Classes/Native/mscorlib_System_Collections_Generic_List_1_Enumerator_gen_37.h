﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityThreading.TaskBase>
struct List_1_t159;
// UnityThreading.TaskBase
struct TaskBase_t163;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityThreading.TaskBase>
struct  Enumerator_t2648 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityThreading.TaskBase>::l
	List_1_t159 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityThreading.TaskBase>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityThreading.TaskBase>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityThreading.TaskBase>::current
	TaskBase_t163 * ___current_3;
};
