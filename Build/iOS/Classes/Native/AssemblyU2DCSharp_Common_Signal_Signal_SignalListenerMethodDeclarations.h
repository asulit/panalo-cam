﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Signal.Signal/SignalListener
struct SignalListener_t114;
// System.Object
struct Object_t;
// Common.Signal.ISignalParameters
struct ISignalParameters_t115;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Common.Signal.Signal/SignalListener::.ctor(System.Object,System.IntPtr)
extern "C" void SignalListener__ctor_m391 (SignalListener_t114 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Signal.Signal/SignalListener::Invoke(Common.Signal.ISignalParameters)
extern "C" void SignalListener_Invoke_m392 (SignalListener_t114 * __this, Object_t * ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SignalListener_t114(Il2CppObject* delegate, Object_t * ___parameters);
// System.IAsyncResult Common.Signal.Signal/SignalListener::BeginInvoke(Common.Signal.ISignalParameters,System.AsyncCallback,System.Object)
extern "C" Object_t * SignalListener_BeginInvoke_m393 (SignalListener_t114 * __this, Object_t * ___parameters, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Signal.Signal/SignalListener::EndInvoke(System.IAsyncResult)
extern "C" void SignalListener_EndInvoke_m394 (SignalListener_t114 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
