﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUISettings
struct GUISettings_t913;

// System.Void UnityEngine.GUISettings::.ctor()
extern "C" void GUISettings__ctor_m4784 (GUISettings_t913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
