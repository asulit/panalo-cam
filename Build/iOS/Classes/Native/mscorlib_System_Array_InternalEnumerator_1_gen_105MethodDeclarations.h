﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_105.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m28754_gshared (InternalEnumerator_1_t3480 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m28754(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3480 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m28754_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28755_gshared (InternalEnumerator_1_t3480 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28755(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3480 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28755_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m28756_gshared (InternalEnumerator_1_t3480 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m28756(__this, method) (( void (*) (InternalEnumerator_1_t3480 *, const MethodInfo*))InternalEnumerator_1_Dispose_m28756_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m28757_gshared (InternalEnumerator_1_t3480 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m28757(__this, method) (( bool (*) (InternalEnumerator_1_t3480 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m28757_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" ProfileData_t1226  InternalEnumerator_1_get_Current_m28758_gshared (InternalEnumerator_1_t3480 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m28758(__this, method) (( ProfileData_t1226  (*) (InternalEnumerator_1_t3480 *, const MethodInfo*))InternalEnumerator_1_get_Current_m28758_gshared)(__this, method)
