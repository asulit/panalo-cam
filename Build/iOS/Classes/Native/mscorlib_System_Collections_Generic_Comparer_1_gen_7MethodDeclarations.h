﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.Vector4>
struct Comparer_1_t3067;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::.ctor()
extern "C" void Comparer_1__ctor_m22694_gshared (Comparer_1_t3067 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m22694(__this, method) (( void (*) (Comparer_1_t3067 *, const MethodInfo*))Comparer_1__ctor_m22694_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::.cctor()
extern "C" void Comparer_1__cctor_m22695_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m22695(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m22695_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m22696_gshared (Comparer_1_t3067 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m22696(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3067 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m22696_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::get_Default()
extern "C" Comparer_1_t3067 * Comparer_1_get_Default_m22697_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m22697(__this /* static, unused */, method) (( Comparer_1_t3067 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m22697_gshared)(__this /* static, unused */, method)
