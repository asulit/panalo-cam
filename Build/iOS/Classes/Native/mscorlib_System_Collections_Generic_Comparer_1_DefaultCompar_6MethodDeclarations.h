﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>
struct DefaultComparer_t3057;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::.ctor()
extern "C" void DefaultComparer__ctor_m22548_gshared (DefaultComparer_t3057 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22548(__this, method) (( void (*) (DefaultComparer_t3057 *, const MethodInfo*))DefaultComparer__ctor_m22548_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m22549_gshared (DefaultComparer_t3057 * __this, Vector2_t2  ___x, Vector2_t2  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m22549(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3057 *, Vector2_t2 , Vector2_t2 , const MethodInfo*))DefaultComparer_Compare_m22549_gshared)(__this, ___x, ___y, method)
