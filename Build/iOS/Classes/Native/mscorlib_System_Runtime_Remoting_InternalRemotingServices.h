﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t261;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.InternalRemotingServices
struct  InternalRemotingServices_t2140  : public Object_t
{
};
struct InternalRemotingServices_t2140_StaticFields{
	// System.Collections.Hashtable System.Runtime.Remoting.InternalRemotingServices::_soapAttributes
	Hashtable_t261 * ____soapAttributes_0;
};
