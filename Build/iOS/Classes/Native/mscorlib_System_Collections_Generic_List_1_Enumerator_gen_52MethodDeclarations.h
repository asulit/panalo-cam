﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t709;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_52.h"
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m20784_gshared (Enumerator_t2940 * __this, List_1_t709 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m20784(__this, ___l, method) (( void (*) (Enumerator_t2940 *, List_1_t709 *, const MethodInfo*))Enumerator__ctor_m20784_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20785_gshared (Enumerator_t2940 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20785(__this, method) (( Object_t * (*) (Enumerator_t2940 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20785_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C" void Enumerator_Dispose_m20786_gshared (Enumerator_t2940 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20786(__this, method) (( void (*) (Enumerator_t2940 *, const MethodInfo*))Enumerator_Dispose_m20786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern "C" void Enumerator_VerifyState_m20787_gshared (Enumerator_t2940 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m20787(__this, method) (( void (*) (Enumerator_t2940 *, const MethodInfo*))Enumerator_VerifyState_m20787_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20788_gshared (Enumerator_t2940 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20788(__this, method) (( bool (*) (Enumerator_t2940 *, const MethodInfo*))Enumerator_MoveNext_m20788_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C" UIVertex_t605  Enumerator_get_Current_m20789_gshared (Enumerator_t2940 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20789(__this, method) (( UIVertex_t605  (*) (Enumerator_t2940 *, const MethodInfo*))Enumerator_get_Current_m20789_gshared)(__this, method)
