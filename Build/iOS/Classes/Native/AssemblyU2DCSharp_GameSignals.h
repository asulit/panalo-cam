﻿#pragma once
#include <stdint.h>
// Common.Signal.Signal
struct Signal_t116;
// System.Object
#include "mscorlib_System_Object.h"
// GameSignals
struct  GameSignals_t193  : public Object_t
{
};
struct GameSignals_t193_StaticFields{
	// Common.Signal.Signal GameSignals::ON_LOAD_TITLE_SCREEN
	Signal_t116 * ___ON_LOAD_TITLE_SCREEN_0;
	// Common.Signal.Signal GameSignals::ON_LOAD_SCAN_SCREEN
	Signal_t116 * ___ON_LOAD_SCAN_SCREEN_1;
	// Common.Signal.Signal GameSignals::ON_START_SCANNING
	Signal_t116 * ___ON_START_SCANNING_2;
	// Common.Signal.Signal GameSignals::ON_EXIT_SCANNING
	Signal_t116 * ___ON_EXIT_SCANNING_3;
	// Common.Signal.Signal GameSignals::ON_TRACKING_DETECTED
	Signal_t116 * ___ON_TRACKING_DETECTED_4;
	// Common.Signal.Signal GameSignals::ON_TRACKING_LOST
	Signal_t116 * ___ON_TRACKING_LOST_5;
	// Common.Signal.Signal GameSignals::ON_UPDATE_CAMERA_FOCUS
	Signal_t116 * ___ON_UPDATE_CAMERA_FOCUS_6;
	// Common.Signal.Signal GameSignals::ON_UPDATE_CAMERA_FLASH
	Signal_t116 * ___ON_UPDATE_CAMERA_FLASH_7;
	// Common.Signal.Signal GameSignals::ON_SHARE
	Signal_t116 * ___ON_SHARE_8;
	// Common.Signal.Signal GameSignals::ON_SEND_SMS
	Signal_t116 * ___ON_SEND_SMS_9;
	// Common.Signal.Signal GameSignals::ON_START_AUDIO_RECORD
	Signal_t116 * ___ON_START_AUDIO_RECORD_10;
	// Common.Signal.Signal GameSignals::ON_STOP_AUDIO_RECORD
	Signal_t116 * ___ON_STOP_AUDIO_RECORD_11;
	// Common.Signal.Signal GameSignals::ON_CANCEL_AUDIO_RECORD
	Signal_t116 * ___ON_CANCEL_AUDIO_RECORD_12;
	// Common.Signal.Signal GameSignals::ON_AUDIO_RECOGNIZED
	Signal_t116 * ___ON_AUDIO_RECOGNIZED_13;
	// Common.Signal.Signal GameSignals::ON_REGISTER_USER_SUCCESSFUL
	Signal_t116 * ___ON_REGISTER_USER_SUCCESSFUL_14;
	// Common.Signal.Signal GameSignals::ON_REGISTER_RESULT
	Signal_t116 * ___ON_REGISTER_RESULT_15;
	// Common.Signal.Signal GameSignals::ON_REGISTER_RAFFLE_RESULT
	Signal_t116 * ___ON_REGISTER_RAFFLE_RESULT_16;
	// Common.Signal.Signal GameSignals::ON_RAFFLE_WINNERS_RESULT
	Signal_t116 * ___ON_RAFFLE_WINNERS_RESULT_17;
	// Common.Signal.Signal GameSignals::ON_LOAD_OLD_SCANNER
	Signal_t116 * ___ON_LOAD_OLD_SCANNER_18;
	// Common.Signal.Signal GameSignals::ON_VIDEO_PLAYER_DONE
	Signal_t116 * ___ON_VIDEO_PLAYER_DONE_19;
};
