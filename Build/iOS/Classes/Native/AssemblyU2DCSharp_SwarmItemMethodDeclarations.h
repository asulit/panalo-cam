﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SwarmItem
struct SwarmItem_t124;
// UnityEngine.Transform
struct Transform_t35;
// SwarmItemManager
struct SwarmItemManager_t111;
// SwarmItem/STATE
#include "AssemblyU2DCSharp_SwarmItem_STATE.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void SwarmItem::.ctor()
extern "C" void SwarmItem__ctor_m498 (SwarmItem_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SwarmItem/STATE SwarmItem::get_State()
extern "C" int32_t SwarmItem_get_State_m499 (SwarmItem_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItem::set_State(SwarmItem/STATE)
extern "C" void SwarmItem_set_State_m500 (SwarmItem_t124 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItem::OnStateChange()
extern "C" void SwarmItem_OnStateChange_m501 (SwarmItem_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform SwarmItem::get_ThisTransform()
extern "C" Transform_t35 * SwarmItem_get_ThisTransform_m502 (SwarmItem_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SwarmItem::get_Position()
extern "C" Vector3_t36  SwarmItem_get_Position_m503 (SwarmItem_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItem::set_Position(UnityEngine.Vector3)
extern "C" void SwarmItem_set_Position_m504 (SwarmItem_t124 * __this, Vector3_t36  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SwarmItem::get_PrefabIndex()
extern "C" int32_t SwarmItem_get_PrefabIndex_m505 (SwarmItem_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItem::Initialize(SwarmItemManager,System.Int32,System.Boolean)
extern "C" void SwarmItem_Initialize_m506 (SwarmItem_t124 * __this, SwarmItemManager_t111 * ___swarmItemManager, int32_t ___prefabIndex, bool ___debugEvents, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItem::OnSetParentTransform()
extern "C" void SwarmItem_OnSetParentTransform_m507 (SwarmItem_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItem::Kill()
extern "C" void SwarmItem_Kill_m508 (SwarmItem_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItem::PreDestroy()
extern "C" void SwarmItem_PreDestroy_m509 (SwarmItem_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItem::FrameUpdate()
extern "C" void SwarmItem_FrameUpdate_m510 (SwarmItem_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
