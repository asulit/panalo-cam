﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void AutoRotationState_t1146_marshal(const AutoRotationState_t1146& unmarshaled, AutoRotationState_t1146_marshaled& marshaled);
extern "C" void AutoRotationState_t1146_marshal_back(const AutoRotationState_t1146_marshaled& marshaled, AutoRotationState_t1146& unmarshaled);
extern "C" void AutoRotationState_t1146_marshal_cleanup(AutoRotationState_t1146_marshaled& marshaled);
