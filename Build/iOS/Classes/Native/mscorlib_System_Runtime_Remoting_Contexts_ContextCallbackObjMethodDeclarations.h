﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Contexts.ContextCallbackObject
struct ContextCallbackObject_t2076;
// System.Runtime.Remoting.Contexts.CrossContextDelegate
struct CrossContextDelegate_t2381;

// System.Void System.Runtime.Remoting.Contexts.ContextCallbackObject::.ctor()
extern "C" void ContextCallbackObject__ctor_m12404 (ContextCallbackObject_t2076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Contexts.ContextCallbackObject::DoCallBack(System.Runtime.Remoting.Contexts.CrossContextDelegate)
extern "C" void ContextCallbackObject_DoCallBack_m12405 (ContextCallbackObject_t2076 * __this, CrossContextDelegate_t2381 * ___deleg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
