﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ERaffleResult,System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>>
struct  Transform_1_t2719  : public MulticastDelegate_t28
{
};
