﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.IUnityPlayer
struct IUnityPlayer_t1075;

// Vuforia.IUnityPlayer Vuforia.UnityPlayer::get_Instance()
extern "C" Object_t * UnityPlayer_get_Instance_m5425 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UnityPlayer::SetImplementation(Vuforia.IUnityPlayer)
extern "C" void UnityPlayer_SetImplementation_m5426 (Object_t * __this /* static, unused */, Object_t * ___implementation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UnityPlayer::.cctor()
extern "C" void UnityPlayer__cctor_m5427 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
