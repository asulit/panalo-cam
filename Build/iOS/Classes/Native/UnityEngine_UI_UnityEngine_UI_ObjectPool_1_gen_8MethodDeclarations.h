﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_0MethodDeclarations.h"
#define ObjectPool_1__ctor_m22935(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t3094 *, UnityAction_1_t3095 *, UnityAction_1_t3095 *, const MethodInfo*))ObjectPool_1__ctor_m18958_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::get_countAll()
#define ObjectPool_1_get_countAll_m22936(__this, method) (( int32_t (*) (ObjectPool_1_t3094 *, const MethodInfo*))ObjectPool_1_get_countAll_m18960_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m22937(__this, ___value, method) (( void (*) (ObjectPool_1_t3094 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m18962_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::get_countActive()
#define ObjectPool_1_get_countActive_m22938(__this, method) (( int32_t (*) (ObjectPool_1_t3094 *, const MethodInfo*))ObjectPool_1_get_countActive_m18964_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m22939(__this, method) (( int32_t (*) (ObjectPool_1_t3094 *, const MethodInfo*))ObjectPool_1_get_countInactive_m18966_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::Get()
#define ObjectPool_1_Get_m22940(__this, method) (( List_1_t679 * (*) (ObjectPool_1_t3094 *, const MethodInfo*))ObjectPool_1_Get_m18968_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>::Release(T)
#define ObjectPool_1_Release_m22941(__this, ___element, method) (( void (*) (ObjectPool_1_t3094 *, List_1_t679 *, const MethodInfo*))ObjectPool_1_Release_m18970_gshared)(__this, ___element, method)
