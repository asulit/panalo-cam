﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityThreadHelper/<CreateThread>c__AnonStorey12
struct U3CCreateThreadU3Ec__AnonStorey12_t175;
// UnityThreading.ActionThread
struct ActionThread_t171;

// System.Void UnityThreadHelper/<CreateThread>c__AnonStorey12::.ctor()
extern "C" void U3CCreateThreadU3Ec__AnonStorey12__ctor_m601 (U3CCreateThreadU3Ec__AnonStorey12_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityThreadHelper/<CreateThread>c__AnonStorey12::<>m__6(UnityThreading.ActionThread)
extern "C" void U3CCreateThreadU3Ec__AnonStorey12_U3CU3Em__6_m602 (U3CCreateThreadU3Ec__AnonStorey12_t175 * __this, ActionThread_t171 * ___currentThread, const MethodInfo* method) IL2CPP_METHOD_ATTR;
