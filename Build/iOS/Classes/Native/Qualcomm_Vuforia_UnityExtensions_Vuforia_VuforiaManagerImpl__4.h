﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Vuforia.VuforiaManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_.h"
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
// Vuforia.VuforiaManagerImpl/Obb2D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__2.h"
// Vuforia.VuforiaManagerImpl/WordResultData
struct  WordResultData_t1138 
{
	// Vuforia.VuforiaManagerImpl/PoseData Vuforia.VuforiaManagerImpl/WordResultData::pose
	PoseData_t1133  ___pose_0;
	// Vuforia.TrackableBehaviour/Status Vuforia.VuforiaManagerImpl/WordResultData::status
	int32_t ___status_1;
	// System.Int32 Vuforia.VuforiaManagerImpl/WordResultData::id
	int32_t ___id_2;
	// Vuforia.VuforiaManagerImpl/Obb2D Vuforia.VuforiaManagerImpl/WordResultData::orientedBoundingBox
	Obb2D_t1136  ___orientedBoundingBox_3;
};
// Native definition for marshalling of: Vuforia.VuforiaManagerImpl/WordResultData
// Vuforia.VuforiaManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_.h"
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
// Vuforia.VuforiaManagerImpl/Obb2D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__2.h"
#pragma pack(push, tp, 1)
struct WordResultData_t1138_marshaled
{
	PoseData_t1133  ___pose_0;
	int32_t ___status_1;
	int32_t ___id_2;
	Obb2D_t1136  ___orientedBoundingBox_3;
};
#pragma pack(pop, tp)
