﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t894;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>
struct IEnumerable_1_t3728;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t3729;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>
struct ICollection_1_t3730;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
struct ReadOnlyCollection_1_t3133;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1009;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t3137;
// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t3140;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_67.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void List_1__ctor_m23345_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1__ctor_m23345(__this, method) (( void (*) (List_1_t894 *, const MethodInfo*))List_1__ctor_m23345_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m23346_gshared (List_1_t894 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m23346(__this, ___collection, method) (( void (*) (List_1_t894 *, Object_t*, const MethodInfo*))List_1__ctor_m23346_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m5142_gshared (List_1_t894 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m5142(__this, ___capacity, method) (( void (*) (List_1_t894 *, int32_t, const MethodInfo*))List_1__ctor_m5142_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void List_1__cctor_m23347_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m23347(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m23347_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23348_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23348(__this, method) (( Object_t* (*) (List_1_t894 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23348_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m23349_gshared (List_1_t894 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m23349(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t894 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m23349_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m23350_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23350(__this, method) (( Object_t * (*) (List_1_t894 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m23350_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m23351_gshared (List_1_t894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m23351(__this, ___item, method) (( int32_t (*) (List_1_t894 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m23351_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m23352_gshared (List_1_t894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m23352(__this, ___item, method) (( bool (*) (List_1_t894 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m23352_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m23353_gshared (List_1_t894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m23353(__this, ___item, method) (( int32_t (*) (List_1_t894 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m23353_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m23354_gshared (List_1_t894 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m23354(__this, ___index, ___item, method) (( void (*) (List_1_t894 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m23354_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m23355_gshared (List_1_t894 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m23355(__this, ___item, method) (( void (*) (List_1_t894 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m23355_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23356_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23356(__this, method) (( bool (*) (List_1_t894 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23356_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m23357_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23357(__this, method) (( bool (*) (List_1_t894 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m23357_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m23358_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m23358(__this, method) (( Object_t * (*) (List_1_t894 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m23358_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m23359_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m23359(__this, method) (( bool (*) (List_1_t894 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m23359_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m23360_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m23360(__this, method) (( bool (*) (List_1_t894 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m23360_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m23361_gshared (List_1_t894 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m23361(__this, ___index, method) (( Object_t * (*) (List_1_t894 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m23361_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m23362_gshared (List_1_t894 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m23362(__this, ___index, ___value, method) (( void (*) (List_1_t894 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m23362_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void List_1_Add_m23363_gshared (List_1_t894 * __this, UICharInfo_t754  ___item, const MethodInfo* method);
#define List_1_Add_m23363(__this, ___item, method) (( void (*) (List_1_t894 *, UICharInfo_t754 , const MethodInfo*))List_1_Add_m23363_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m23364_gshared (List_1_t894 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m23364(__this, ___newCount, method) (( void (*) (List_1_t894 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m23364_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m23365_gshared (List_1_t894 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m23365(__this, ___idx, ___count, method) (( void (*) (List_1_t894 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m23365_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m23366_gshared (List_1_t894 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m23366(__this, ___collection, method) (( void (*) (List_1_t894 *, Object_t*, const MethodInfo*))List_1_AddCollection_m23366_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m23367_gshared (List_1_t894 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m23367(__this, ___enumerable, method) (( void (*) (List_1_t894 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m23367_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m23368_gshared (List_1_t894 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m23368(__this, ___collection, method) (( void (*) (List_1_t894 *, Object_t*, const MethodInfo*))List_1_AddRange_m23368_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3133 * List_1_AsReadOnly_m23369_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m23369(__this, method) (( ReadOnlyCollection_1_t3133 * (*) (List_1_t894 *, const MethodInfo*))List_1_AsReadOnly_m23369_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Clear()
extern "C" void List_1_Clear_m23370_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_Clear_m23370(__this, method) (( void (*) (List_1_t894 *, const MethodInfo*))List_1_Clear_m23370_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool List_1_Contains_m23371_gshared (List_1_t894 * __this, UICharInfo_t754  ___item, const MethodInfo* method);
#define List_1_Contains_m23371(__this, ___item, method) (( bool (*) (List_1_t894 *, UICharInfo_t754 , const MethodInfo*))List_1_Contains_m23371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m23372_gshared (List_1_t894 * __this, UICharInfoU5BU5D_t1009* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m23372(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t894 *, UICharInfoU5BU5D_t1009*, int32_t, const MethodInfo*))List_1_CopyTo_m23372_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Find(System.Predicate`1<T>)
extern "C" UICharInfo_t754  List_1_Find_m23373_gshared (List_1_t894 * __this, Predicate_1_t3137 * ___match, const MethodInfo* method);
#define List_1_Find_m23373(__this, ___match, method) (( UICharInfo_t754  (*) (List_1_t894 *, Predicate_1_t3137 *, const MethodInfo*))List_1_Find_m23373_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m23374_gshared (Object_t * __this /* static, unused */, Predicate_1_t3137 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m23374(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3137 *, const MethodInfo*))List_1_CheckMatch_m23374_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m23375_gshared (List_1_t894 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3137 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m23375(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t894 *, int32_t, int32_t, Predicate_1_t3137 *, const MethodInfo*))List_1_GetIndex_m23375_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Enumerator_t3132  List_1_GetEnumerator_m23376_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m23376(__this, method) (( Enumerator_t3132  (*) (List_1_t894 *, const MethodInfo*))List_1_GetEnumerator_m23376_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m23377_gshared (List_1_t894 * __this, UICharInfo_t754  ___item, const MethodInfo* method);
#define List_1_IndexOf_m23377(__this, ___item, method) (( int32_t (*) (List_1_t894 *, UICharInfo_t754 , const MethodInfo*))List_1_IndexOf_m23377_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m23378_gshared (List_1_t894 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m23378(__this, ___start, ___delta, method) (( void (*) (List_1_t894 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m23378_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m23379_gshared (List_1_t894 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m23379(__this, ___index, method) (( void (*) (List_1_t894 *, int32_t, const MethodInfo*))List_1_CheckIndex_m23379_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m23380_gshared (List_1_t894 * __this, int32_t ___index, UICharInfo_t754  ___item, const MethodInfo* method);
#define List_1_Insert_m23380(__this, ___index, ___item, method) (( void (*) (List_1_t894 *, int32_t, UICharInfo_t754 , const MethodInfo*))List_1_Insert_m23380_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m23381_gshared (List_1_t894 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m23381(__this, ___collection, method) (( void (*) (List_1_t894 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m23381_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool List_1_Remove_m23382_gshared (List_1_t894 * __this, UICharInfo_t754  ___item, const MethodInfo* method);
#define List_1_Remove_m23382(__this, ___item, method) (( bool (*) (List_1_t894 *, UICharInfo_t754 , const MethodInfo*))List_1_Remove_m23382_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m23383_gshared (List_1_t894 * __this, Predicate_1_t3137 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m23383(__this, ___match, method) (( int32_t (*) (List_1_t894 *, Predicate_1_t3137 *, const MethodInfo*))List_1_RemoveAll_m23383_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m23384_gshared (List_1_t894 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m23384(__this, ___index, method) (( void (*) (List_1_t894 *, int32_t, const MethodInfo*))List_1_RemoveAt_m23384_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m23385_gshared (List_1_t894 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m23385(__this, ___index, ___count, method) (( void (*) (List_1_t894 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m23385_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Reverse()
extern "C" void List_1_Reverse_m23386_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_Reverse_m23386(__this, method) (( void (*) (List_1_t894 *, const MethodInfo*))List_1_Reverse_m23386_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort()
extern "C" void List_1_Sort_m23387_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_Sort_m23387(__this, method) (( void (*) (List_1_t894 *, const MethodInfo*))List_1_Sort_m23387_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m23388_gshared (List_1_t894 * __this, Comparison_1_t3140 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m23388(__this, ___comparison, method) (( void (*) (List_1_t894 *, Comparison_1_t3140 *, const MethodInfo*))List_1_Sort_m23388_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ToArray()
extern "C" UICharInfoU5BU5D_t1009* List_1_ToArray_m23389_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_ToArray_m23389(__this, method) (( UICharInfoU5BU5D_t1009* (*) (List_1_t894 *, const MethodInfo*))List_1_ToArray_m23389_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m23390_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m23390(__this, method) (( void (*) (List_1_t894 *, const MethodInfo*))List_1_TrimExcess_m23390_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m23391_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m23391(__this, method) (( int32_t (*) (List_1_t894 *, const MethodInfo*))List_1_get_Capacity_m23391_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m23392_gshared (List_1_t894 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m23392(__this, ___value, method) (( void (*) (List_1_t894 *, int32_t, const MethodInfo*))List_1_set_Capacity_m23392_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m23393_gshared (List_1_t894 * __this, const MethodInfo* method);
#define List_1_get_Count_m23393(__this, method) (( int32_t (*) (List_1_t894 *, const MethodInfo*))List_1_get_Count_m23393_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t754  List_1_get_Item_m23394_gshared (List_1_t894 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m23394(__this, ___index, method) (( UICharInfo_t754  (*) (List_1_t894 *, int32_t, const MethodInfo*))List_1_get_Item_m23394_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m23395_gshared (List_1_t894 * __this, int32_t ___index, UICharInfo_t754  ___value, const MethodInfo* method);
#define List_1_set_Item_m23395(__this, ___index, ___value, method) (( void (*) (List_1_t894 *, int32_t, UICharInfo_t754 , const MethodInfo*))List_1_set_Item_m23395_gshared)(__this, ___index, ___value, method)
