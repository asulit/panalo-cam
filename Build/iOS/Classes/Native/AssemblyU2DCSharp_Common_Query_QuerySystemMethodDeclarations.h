﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.String
struct String_t;
// Common.Query.QueryResultResolver
struct QueryResultResolver_t328;
// Common.Query.IQueryRequest
struct IQueryRequest_t329;

// System.Void Common.Query.QuerySystem::.cctor()
extern "C" void QuerySystem__cctor_m370 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Query.QuerySystem::RegisterResolver(System.String,Common.Query.QueryResultResolver)
extern "C" void QuerySystem_RegisterResolver_m371 (Object_t * __this /* static, unused */, String_t* ___queryId, QueryResultResolver_t328 * ___resolver, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Query.QuerySystem::RemoveResolver(System.String)
extern "C" void QuerySystem_RemoveResolver_m372 (Object_t * __this /* static, unused */, String_t* ___queryId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common.Query.IQueryRequest Common.Query.QuerySystem::Start(System.String)
extern "C" Object_t * QuerySystem_Start_m373 (Object_t * __this /* static, unused */, String_t* ___queryId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Query.QuerySystem::HasResolver(System.String)
extern "C" bool QuerySystem_HasResolver_m374 (Object_t * __this /* static, unused */, String_t* ___queryId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
