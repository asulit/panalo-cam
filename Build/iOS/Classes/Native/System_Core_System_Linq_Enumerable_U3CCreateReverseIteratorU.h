﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t376;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2471;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>
struct  U3CCreateReverseIteratorU3Ec__IteratorF_1_t2497  : public Object_t
{
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::source
	Object_t* ___source_0;
	// System.Collections.Generic.IList`1<TSource> System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::<list>__0
	Object_t* ___U3ClistU3E__0_1;
	// System.Int32 System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::<i>__1
	int32_t ___U3CiU3E__1_2;
	// System.Int32 System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::$PC
	int32_t ___U24PC_3;
	// TSource System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::$current
	Object_t * ___U24current_4;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::<$>source
	Object_t* ___U3CU24U3Esource_5;
};
