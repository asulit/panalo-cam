﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.VuforiaAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeha_0.h"
// Vuforia.VuforiaAbstractBehaviour/WorldCenterMode
struct  WorldCenterMode_t1237 
{
	// System.Int32 Vuforia.VuforiaAbstractBehaviour/WorldCenterMode::value__
	int32_t ___value___1;
};
