﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LoadingSceneRoot/<CheckRaffleResults>c__Iterator7
struct U3CCheckRaffleResultsU3Ec__Iterator7_t217;
// System.Object
struct Object_t;
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// System.Void LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::.ctor()
extern "C" void U3CCheckRaffleResultsU3Ec__Iterator7__ctor_m755 (U3CCheckRaffleResultsU3Ec__Iterator7_t217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CCheckRaffleResultsU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m756 (U3CCheckRaffleResultsU3Ec__Iterator7_t217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCheckRaffleResultsU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m757 (U3CCheckRaffleResultsU3Ec__Iterator7_t217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::MoveNext()
extern "C" bool U3CCheckRaffleResultsU3Ec__Iterator7_MoveNext_m758 (U3CCheckRaffleResultsU3Ec__Iterator7_t217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::Dispose()
extern "C" void U3CCheckRaffleResultsU3Ec__Iterator7_Dispose_m759 (U3CCheckRaffleResultsU3Ec__Iterator7_t217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::Reset()
extern "C" void U3CCheckRaffleResultsU3Ec__Iterator7_Reset_m760 (U3CCheckRaffleResultsU3Ec__Iterator7_t217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::<>m__1A(System.Int32,ERaffleResult)
extern "C" void U3CCheckRaffleResultsU3Ec__Iterator7_U3CU3Em__1A_m761 (U3CCheckRaffleResultsU3Ec__Iterator7_t217 * __this, int32_t ___raffleId, int32_t ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingSceneRoot/<CheckRaffleResults>c__Iterator7::<>m__1B(System.Collections.Generic.KeyValuePair`2<System.Int32,ERaffleResult>)
extern "C" bool U3CCheckRaffleResultsU3Ec__Iterator7_U3CU3Em__1B_m762 (Object_t * __this /* static, unused */, KeyValuePair_2_t352  ___r, const MethodInfo* method) IL2CPP_METHOD_ATTR;
