﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MiniJSON.Json/Parser
struct Parser_t66;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t86;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t344;
// MiniJSON.Json/Parser/TOKEN
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser_TOKEN.h"

// System.Void MiniJSON.Json/Parser::.ctor(System.String)
extern "C" void Parser__ctor_m225 (Parser_t66 * __this, String_t* ___jsonString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MiniJSON.Json/Parser::IsWordBreak(System.Char)
extern "C" bool Parser_IsWordBreak_m226 (Object_t * __this /* static, unused */, uint16_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MiniJSON.Json/Parser::Parse(System.String)
extern "C" Object_t * Parser_Parse_m227 (Object_t * __this /* static, unused */, String_t* ___jsonString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MiniJSON.Json/Parser::Dispose()
extern "C" void Parser_Dispose_m228 (Parser_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> MiniJSON.Json/Parser::ParseObject()
extern "C" Dictionary_2_t86 * Parser_ParseObject_m229 (Parser_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> MiniJSON.Json/Parser::ParseArray()
extern "C" List_1_t344 * Parser_ParseArray_m230 (Parser_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MiniJSON.Json/Parser::ParseValue()
extern "C" Object_t * Parser_ParseValue_m231 (Parser_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MiniJSON.Json/Parser::ParseByToken(MiniJSON.Json/Parser/TOKEN)
extern "C" Object_t * Parser_ParseByToken_m232 (Parser_t66 * __this, int32_t ___token, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MiniJSON.Json/Parser::ParseString()
extern "C" String_t* Parser_ParseString_m233 (Parser_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MiniJSON.Json/Parser::ParseNumber()
extern "C" Object_t * Parser_ParseNumber_m234 (Parser_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MiniJSON.Json/Parser::EatWhitespace()
extern "C" void Parser_EatWhitespace_m235 (Parser_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char MiniJSON.Json/Parser::get_PeekChar()
extern "C" uint16_t Parser_get_PeekChar_m236 (Parser_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char MiniJSON.Json/Parser::get_NextChar()
extern "C" uint16_t Parser_get_NextChar_m237 (Parser_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MiniJSON.Json/Parser::get_NextWord()
extern "C" String_t* Parser_get_NextWord_m238 (Parser_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MiniJSON.Json/Parser/TOKEN MiniJSON.Json/Parser::get_NextToken()
extern "C" int32_t Parser_get_NextToken_m239 (Parser_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
