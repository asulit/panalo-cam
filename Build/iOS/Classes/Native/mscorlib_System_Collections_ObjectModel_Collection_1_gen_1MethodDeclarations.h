﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<AnimatorFrame>
struct Collection_1_t2774;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// System.Object
struct Object_t;
// AnimatorFrame[]
struct AnimatorFrameU5BU5D_t2770;
// System.Collections.Generic.IEnumerator`1<AnimatorFrame>
struct IEnumerator_1_t3679;
// System.Collections.Generic.IList`1<AnimatorFrame>
struct IList_1_t2773;
// AnimatorFrame
#include "AssemblyU2DCSharp_AnimatorFrame.h"

// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::.ctor()
extern "C" void Collection_1__ctor_m18379_gshared (Collection_1_t2774 * __this, const MethodInfo* method);
#define Collection_1__ctor_m18379(__this, method) (( void (*) (Collection_1_t2774 *, const MethodInfo*))Collection_1__ctor_m18379_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18380_gshared (Collection_1_t2774 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18380(__this, method) (( bool (*) (Collection_1_t2774 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18380_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m18381_gshared (Collection_1_t2774 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m18381(__this, ___array, ___index, method) (( void (*) (Collection_1_t2774 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m18381_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m18382_gshared (Collection_1_t2774 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m18382(__this, method) (( Object_t * (*) (Collection_1_t2774 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m18382_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m18383_gshared (Collection_1_t2774 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m18383(__this, ___value, method) (( int32_t (*) (Collection_1_t2774 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m18383_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m18384_gshared (Collection_1_t2774 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m18384(__this, ___value, method) (( bool (*) (Collection_1_t2774 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m18384_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m18385_gshared (Collection_1_t2774 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m18385(__this, ___value, method) (( int32_t (*) (Collection_1_t2774 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m18385_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m18386_gshared (Collection_1_t2774 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m18386(__this, ___index, ___value, method) (( void (*) (Collection_1_t2774 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m18386_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m18387_gshared (Collection_1_t2774 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m18387(__this, ___value, method) (( void (*) (Collection_1_t2774 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m18387_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m18388_gshared (Collection_1_t2774 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m18388(__this, method) (( bool (*) (Collection_1_t2774 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m18388_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m18389_gshared (Collection_1_t2774 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m18389(__this, method) (( Object_t * (*) (Collection_1_t2774 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m18389_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m18390_gshared (Collection_1_t2774 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m18390(__this, method) (( bool (*) (Collection_1_t2774 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m18390_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m18391_gshared (Collection_1_t2774 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m18391(__this, method) (( bool (*) (Collection_1_t2774 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m18391_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m18392_gshared (Collection_1_t2774 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m18392(__this, ___index, method) (( Object_t * (*) (Collection_1_t2774 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m18392_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m18393_gshared (Collection_1_t2774 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m18393(__this, ___index, ___value, method) (( void (*) (Collection_1_t2774 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m18393_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::Add(T)
extern "C" void Collection_1_Add_m18394_gshared (Collection_1_t2774 * __this, AnimatorFrame_t235  ___item, const MethodInfo* method);
#define Collection_1_Add_m18394(__this, ___item, method) (( void (*) (Collection_1_t2774 *, AnimatorFrame_t235 , const MethodInfo*))Collection_1_Add_m18394_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::Clear()
extern "C" void Collection_1_Clear_m18395_gshared (Collection_1_t2774 * __this, const MethodInfo* method);
#define Collection_1_Clear_m18395(__this, method) (( void (*) (Collection_1_t2774 *, const MethodInfo*))Collection_1_Clear_m18395_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::ClearItems()
extern "C" void Collection_1_ClearItems_m18396_gshared (Collection_1_t2774 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m18396(__this, method) (( void (*) (Collection_1_t2774 *, const MethodInfo*))Collection_1_ClearItems_m18396_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AnimatorFrame>::Contains(T)
extern "C" bool Collection_1_Contains_m18397_gshared (Collection_1_t2774 * __this, AnimatorFrame_t235  ___item, const MethodInfo* method);
#define Collection_1_Contains_m18397(__this, ___item, method) (( bool (*) (Collection_1_t2774 *, AnimatorFrame_t235 , const MethodInfo*))Collection_1_Contains_m18397_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m18398_gshared (Collection_1_t2774 * __this, AnimatorFrameU5BU5D_t2770* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m18398(__this, ___array, ___index, method) (( void (*) (Collection_1_t2774 *, AnimatorFrameU5BU5D_t2770*, int32_t, const MethodInfo*))Collection_1_CopyTo_m18398_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<AnimatorFrame>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m18399_gshared (Collection_1_t2774 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m18399(__this, method) (( Object_t* (*) (Collection_1_t2774 *, const MethodInfo*))Collection_1_GetEnumerator_m18399_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AnimatorFrame>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m18400_gshared (Collection_1_t2774 * __this, AnimatorFrame_t235  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m18400(__this, ___item, method) (( int32_t (*) (Collection_1_t2774 *, AnimatorFrame_t235 , const MethodInfo*))Collection_1_IndexOf_m18400_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m18401_gshared (Collection_1_t2774 * __this, int32_t ___index, AnimatorFrame_t235  ___item, const MethodInfo* method);
#define Collection_1_Insert_m18401(__this, ___index, ___item, method) (( void (*) (Collection_1_t2774 *, int32_t, AnimatorFrame_t235 , const MethodInfo*))Collection_1_Insert_m18401_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m18402_gshared (Collection_1_t2774 * __this, int32_t ___index, AnimatorFrame_t235  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m18402(__this, ___index, ___item, method) (( void (*) (Collection_1_t2774 *, int32_t, AnimatorFrame_t235 , const MethodInfo*))Collection_1_InsertItem_m18402_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AnimatorFrame>::Remove(T)
extern "C" bool Collection_1_Remove_m18403_gshared (Collection_1_t2774 * __this, AnimatorFrame_t235  ___item, const MethodInfo* method);
#define Collection_1_Remove_m18403(__this, ___item, method) (( bool (*) (Collection_1_t2774 *, AnimatorFrame_t235 , const MethodInfo*))Collection_1_Remove_m18403_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m18404_gshared (Collection_1_t2774 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m18404(__this, ___index, method) (( void (*) (Collection_1_t2774 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m18404_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m18405_gshared (Collection_1_t2774 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m18405(__this, ___index, method) (( void (*) (Collection_1_t2774 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m18405_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AnimatorFrame>::get_Count()
extern "C" int32_t Collection_1_get_Count_m18406_gshared (Collection_1_t2774 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m18406(__this, method) (( int32_t (*) (Collection_1_t2774 *, const MethodInfo*))Collection_1_get_Count_m18406_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<AnimatorFrame>::get_Item(System.Int32)
extern "C" AnimatorFrame_t235  Collection_1_get_Item_m18407_gshared (Collection_1_t2774 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m18407(__this, ___index, method) (( AnimatorFrame_t235  (*) (Collection_1_t2774 *, int32_t, const MethodInfo*))Collection_1_get_Item_m18407_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m18408_gshared (Collection_1_t2774 * __this, int32_t ___index, AnimatorFrame_t235  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m18408(__this, ___index, ___value, method) (( void (*) (Collection_1_t2774 *, int32_t, AnimatorFrame_t235 , const MethodInfo*))Collection_1_set_Item_m18408_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m18409_gshared (Collection_1_t2774 * __this, int32_t ___index, AnimatorFrame_t235  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m18409(__this, ___index, ___item, method) (( void (*) (Collection_1_t2774 *, int32_t, AnimatorFrame_t235 , const MethodInfo*))Collection_1_SetItem_m18409_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AnimatorFrame>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m18410_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m18410(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m18410_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<AnimatorFrame>::ConvertItem(System.Object)
extern "C" AnimatorFrame_t235  Collection_1_ConvertItem_m18411_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m18411(__this /* static, unused */, ___item, method) (( AnimatorFrame_t235  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m18411_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<AnimatorFrame>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m18412_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m18412(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m18412_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AnimatorFrame>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m18413_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m18413(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m18413_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AnimatorFrame>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m18414_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m18414(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m18414_gshared)(__this /* static, unused */, ___list, method)
