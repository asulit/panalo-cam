﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t567;
// System.Collections.IEnumerator
struct IEnumerator_t337;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t5;
// UnityEngine.UI.CoroutineTween.FloatTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatTween.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C" void TweenRunner_1__ctor_m3448_gshared (TweenRunner_1_t567 * __this, const MethodInfo* method);
#define TweenRunner_1__ctor_m3448(__this, method) (( void (*) (TweenRunner_1_t567 *, const MethodInfo*))TweenRunner_1__ctor_m3448_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Start(T)
extern "C" Object_t * TweenRunner_1_Start_m20404_gshared (Object_t * __this /* static, unused */, FloatTween_t539  ___tweenInfo, const MethodInfo* method);
#define TweenRunner_1_Start_m20404(__this /* static, unused */, ___tweenInfo, method) (( Object_t * (*) (Object_t * /* static, unused */, FloatTween_t539 , const MethodInfo*))TweenRunner_1_Start_m20404_gshared)(__this /* static, unused */, ___tweenInfo, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Init(UnityEngine.MonoBehaviour)
extern "C" void TweenRunner_1_Init_m3449_gshared (TweenRunner_1_t567 * __this, MonoBehaviour_t5 * ___coroutineContainer, const MethodInfo* method);
#define TweenRunner_1_Init_m3449(__this, ___coroutineContainer, method) (( void (*) (TweenRunner_1_t567 *, MonoBehaviour_t5 *, const MethodInfo*))TweenRunner_1_Init_m3449_gshared)(__this, ___coroutineContainer, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StartTween(T)
extern "C" void TweenRunner_1_StartTween_m3490_gshared (TweenRunner_1_t567 * __this, FloatTween_t539  ___info, const MethodInfo* method);
#define TweenRunner_1_StartTween_m3490(__this, ___info, method) (( void (*) (TweenRunner_1_t567 *, FloatTween_t539 , const MethodInfo*))TweenRunner_1_StartTween_m3490_gshared)(__this, ___info, method)
