﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>
struct Dictionary_2_t48;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>
struct  ValueCollection_t2522  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Common.Fsm.FsmState>::dictionary
	Dictionary_2_t48 * ___dictionary_0;
};
