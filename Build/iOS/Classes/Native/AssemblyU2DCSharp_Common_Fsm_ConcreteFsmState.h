﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Common.Fsm.Fsm
struct Fsm_t47;
// System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState>
struct Dictionary_2_t48;
// System.Collections.Generic.List`1<Common.Fsm.FsmAction>
struct List_1_t49;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Fsm.ConcreteFsmState
struct  ConcreteFsmState_t46  : public Object_t
{
	// System.String Common.Fsm.ConcreteFsmState::name
	String_t* ___name_0;
	// Common.Fsm.Fsm Common.Fsm.ConcreteFsmState::owner
	Fsm_t47 * ___owner_1;
	// System.Collections.Generic.Dictionary`2<System.String,Common.Fsm.FsmState> Common.Fsm.ConcreteFsmState::transitionMap
	Dictionary_2_t48 * ___transitionMap_2;
	// System.Collections.Generic.List`1<Common.Fsm.FsmAction> Common.Fsm.ConcreteFsmState::actionList
	List_1_t49 * ___actionList_3;
};
