﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU24120_t2388_marshal(const U24ArrayTypeU24120_t2388& unmarshaled, U24ArrayTypeU24120_t2388_marshaled& marshaled);
extern "C" void U24ArrayTypeU24120_t2388_marshal_back(const U24ArrayTypeU24120_t2388_marshaled& marshaled, U24ArrayTypeU24120_t2388& unmarshaled);
extern "C" void U24ArrayTypeU24120_t2388_marshal_cleanup(U24ArrayTypeU24120_t2388_marshaled& marshaled);
