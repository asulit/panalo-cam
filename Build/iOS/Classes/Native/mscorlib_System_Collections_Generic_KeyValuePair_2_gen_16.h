﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// ESubScreens
#include "AssemblyU2DCSharp_ESubScreens.h"
// System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>
struct  KeyValuePair_2_t2690 
{
	// TKey System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<ESubScreens,System.Object>::value
	Object_t * ___value_1;
};
