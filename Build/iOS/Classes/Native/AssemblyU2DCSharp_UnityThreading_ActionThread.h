﻿#pragma once
#include <stdint.h>
// System.Action`1<UnityThreading.ActionThread>
struct Action_1_t172;
// UnityThreading.ThreadBase
#include "AssemblyU2DCSharp_UnityThreading_ThreadBase.h"
// UnityThreading.ActionThread
struct  ActionThread_t171  : public ThreadBase_t169
{
	// System.Action`1<UnityThreading.ActionThread> UnityThreading.ActionThread::action
	Action_1_t172 * ___action_4;
};
