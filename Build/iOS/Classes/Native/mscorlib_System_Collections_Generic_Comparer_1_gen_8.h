﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Int32>
struct Comparer_1_t3074;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Int32>
struct  Comparer_1_t3074  : public Object_t
{
};
struct Comparer_1_t3074_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Int32>::_default
	Comparer_1_t3074 * ____default_0;
};
