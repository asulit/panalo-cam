﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RequestHandler
struct RequestHandler_t200;
// System.Object
struct Object_t;
// UnityEngine.WWW
struct WWW_t199;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void RequestHandler::.ctor(System.Object,System.IntPtr)
extern "C" void RequestHandler__ctor_m1297 (RequestHandler_t200 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RequestHandler::Invoke(UnityEngine.WWW)
extern "C" void RequestHandler_Invoke_m1298 (RequestHandler_t200 * __this, WWW_t199 * ___request, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RequestHandler_t200(Il2CppObject* delegate, WWW_t199 * ___request);
// System.IAsyncResult RequestHandler::BeginInvoke(UnityEngine.WWW,System.AsyncCallback,System.Object)
extern "C" Object_t * RequestHandler_BeginInvoke_m1299 (RequestHandler_t200 * __this, WWW_t199 * ___request, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RequestHandler::EndInvoke(System.IAsyncResult)
extern "C" void RequestHandler_EndInvoke_m1300 (RequestHandler_t200 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
