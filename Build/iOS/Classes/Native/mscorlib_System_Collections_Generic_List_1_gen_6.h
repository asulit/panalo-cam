﻿#pragma once
#include <stdint.h>
// Common.Command[]
struct CommandU5BU5D_t2621;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Common.Command>
struct  List_1_t411  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Common.Command>::_items
	CommandU5BU5D_t2621* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Common.Command>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Common.Command>::_version
	int32_t ____version_3;
};
struct List_1_t411_StaticFields{
	// T[] System.Collections.Generic.List`1<Common.Command>::EmptyArray
	CommandU5BU5D_t2621* ___EmptyArray_4;
};
