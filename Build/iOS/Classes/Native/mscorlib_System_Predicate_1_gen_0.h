﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t557;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Toggle>
struct  Predicate_1_t645  : public MulticastDelegate_t28
{
};
