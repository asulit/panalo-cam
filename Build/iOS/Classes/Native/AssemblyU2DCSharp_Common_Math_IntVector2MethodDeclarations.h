﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Math.IntVector2
struct IntVector2_t79;
// System.String
struct String_t;

// System.Void Common.Math.IntVector2::.ctor()
extern "C" void IntVector2__ctor_m272 (IntVector2_t79 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Math.IntVector2::.ctor(System.Int32,System.Int32)
extern "C" void IntVector2__ctor_m273 (IntVector2_t79 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Math.IntVector2::.ctor(Common.Math.IntVector2)
extern "C" void IntVector2__ctor_m274 (IntVector2_t79 * __this, IntVector2_t79 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Math.IntVector2::.cctor()
extern "C" void IntVector2__cctor_m275 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Math.IntVector2::Set(Common.Math.IntVector2)
extern "C" void IntVector2_Set_m276 (IntVector2_t79 * __this, IntVector2_t79 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common.Math.IntVector2::Equals(Common.Math.IntVector2)
extern "C" bool IntVector2_Equals_m277 (IntVector2_t79 * __this, IntVector2_t79 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.Math.IntVector2::ToString()
extern "C" String_t* IntVector2_ToString_m278 (IntVector2_t79 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
