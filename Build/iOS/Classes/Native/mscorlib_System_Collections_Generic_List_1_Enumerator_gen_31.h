﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<OrthographicCameraObserver>
struct List_1_t360;
// OrthographicCameraObserver
struct OrthographicCameraObserver_t336;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<OrthographicCameraObserver>
struct  Enumerator_t2485 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<OrthographicCameraObserver>::l
	List_1_t360 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<OrthographicCameraObserver>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<OrthographicCameraObserver>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<OrthographicCameraObserver>::current
	Object_t * ___current_3;
};
