﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_0MethodDeclarations.h"
#define ObjectPool_1__ctor_m19111(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t2822 *, UnityAction_1_t2823 *, UnityAction_1_t2823 *, const MethodInfo*))ObjectPool_1__ctor_m18958_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countAll()
#define ObjectPool_1_get_countAll_m19112(__this, method) (( int32_t (*) (ObjectPool_1_t2822 *, const MethodInfo*))ObjectPool_1_get_countAll_m18960_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m19113(__this, ___value, method) (( void (*) (ObjectPool_1_t2822 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m18962_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countActive()
#define ObjectPool_1_get_countActive_m19114(__this, method) (( int32_t (*) (ObjectPool_1_t2822 *, const MethodInfo*))ObjectPool_1_get_countActive_m18964_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m19115(__this, method) (( int32_t (*) (ObjectPool_1_t2822 *, const MethodInfo*))ObjectPool_1_get_countInactive_m18966_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Get()
#define ObjectPool_1_Get_m19116(__this, method) (( List_1_t718 * (*) (ObjectPool_1_t2822 *, const MethodInfo*))ObjectPool_1_Get_m18968_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Release(T)
#define ObjectPool_1_Release_m19117(__this, ___element, method) (( void (*) (ObjectPool_1_t2822 *, List_1_t718 *, const MethodInfo*))ObjectPool_1_Release_m18970_gshared)(__this, ___element, method)
