﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31MethodDeclarations.h"
#define ValueCollection__ctor_m21985(__this, ___dictionary, method) (( void (*) (ValueCollection_t3019 *, Dictionary_2_t765 *, const MethodInfo*))ValueCollection__ctor_m15683_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m21986(__this, ___item, method) (( void (*) (ValueCollection_t3019 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15684_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m21987(__this, method) (( void (*) (ValueCollection_t3019 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15685_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m21988(__this, ___item, method) (( bool (*) (ValueCollection_t3019 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15686_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m21989(__this, ___item, method) (( bool (*) (ValueCollection_t3019 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15687_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m21990(__this, method) (( Object_t* (*) (ValueCollection_t3019 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15688_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m21991(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3019 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m15689_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m21992(__this, method) (( Object_t * (*) (ValueCollection_t3019 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15690_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m21993(__this, method) (( bool (*) (ValueCollection_t3019 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15691_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m21994(__this, method) (( bool (*) (ValueCollection_t3019 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15692_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m21995(__this, method) (( Object_t * (*) (ValueCollection_t3019 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m15693_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m21996(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3019 *, Int32U5BU5D_t401*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m15694_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::GetEnumerator()
#define ValueCollection_GetEnumerator_m21997(__this, method) (( Enumerator_t3710  (*) (ValueCollection_t3019 *, const MethodInfo*))ValueCollection_GetEnumerator_m15695_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.IClipper,System.Int32>::get_Count()
#define ValueCollection_get_Count_m21998(__this, method) (( int32_t (*) (ValueCollection_t3019 *, const MethodInfo*))ValueCollection_get_Count_m15696_gshared)(__this, method)
