﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SwarmItemManager
struct SwarmItemManager_t111;
// SwarmItem
struct SwarmItem_t124;
// UnityEngine.Transform
struct Transform_t35;

// System.Void SwarmItemManager::.ctor()
extern "C" void SwarmItemManager__ctor_m514 (SwarmItemManager_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItemManager::Initialize()
extern "C" void SwarmItemManager_Initialize_m515 (SwarmItemManager_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SwarmItem SwarmItemManager::ActivateItem()
extern "C" SwarmItem_t124 * SwarmItemManager_ActivateItem_m516 (SwarmItemManager_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SwarmItem SwarmItemManager::ActivateItem(System.Int32)
extern "C" SwarmItem_t124 * SwarmItemManager_ActivateItem_m517 (SwarmItemManager_t111 * __this, int32_t ___itemPrefabIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItemManager::DeactiveItem(SwarmItem)
extern "C" void SwarmItemManager_DeactiveItem_m518 (SwarmItemManager_t111 * __this, SwarmItem_t124 * ___item, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SwarmItem SwarmItemManager::InstantiateItem(System.Int32)
extern "C" SwarmItem_t124 * SwarmItemManager_InstantiateItem_m519 (SwarmItemManager_t111 * __this, int32_t ___itemPrefabIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItemManager::SetItemParentTransform(SwarmItem,UnityEngine.Transform)
extern "C" void SwarmItemManager_SetItemParentTransform_m520 (SwarmItemManager_t111 * __this, SwarmItem_t124 * ___item, Transform_t35 * ___parentTransform, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItemManager::FrameUpdate()
extern "C" void SwarmItemManager_FrameUpdate_m521 (SwarmItemManager_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItemManager::PruneList(System.Int32,System.Single)
extern "C" void SwarmItemManager_PruneList_m522 (SwarmItemManager_t111 * __this, int32_t ___itemPrefabIndex, float ___prunePercentage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwarmItemManager::PruneList(System.Int32,System.Int32)
extern "C" void SwarmItemManager_PruneList_m523 (SwarmItemManager_t111 * __this, int32_t ___itemPrefabIndex, int32_t ___pruneCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
