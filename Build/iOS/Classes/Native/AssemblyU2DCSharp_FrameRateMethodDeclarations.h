﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// FrameRate
struct FrameRate_t24;

// System.Void FrameRate::.ctor()
extern "C" void FrameRate__ctor_m91 (FrameRate_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FrameRate::Update(System.Single)
extern "C" void FrameRate_Update_m92 (FrameRate_t24 * __this, float ___timeElapsed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FrameRate::GetFrameRate()
extern "C" int32_t FrameRate_GetFrameRate_m93 (FrameRate_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
