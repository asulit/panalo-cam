﻿#pragma once
#include <stdint.h>
// System.Collections.CaseInsensitiveComparer
struct CaseInsensitiveComparer_t1753;
// System.Globalization.CultureInfo
struct CultureInfo_t1031;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.CaseInsensitiveComparer
struct  CaseInsensitiveComparer_t1753  : public Object_t
{
	// System.Globalization.CultureInfo System.Collections.CaseInsensitiveComparer::culture
	CultureInfo_t1031 * ___culture_2;
};
struct CaseInsensitiveComparer_t1753_StaticFields{
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultComparer
	CaseInsensitiveComparer_t1753 * ___defaultComparer_0;
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultInvariantComparer
	CaseInsensitiveComparer_t1753 * ___defaultInvariantComparer_1;
};
