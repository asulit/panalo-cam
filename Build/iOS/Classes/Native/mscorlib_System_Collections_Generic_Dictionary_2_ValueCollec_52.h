﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.InputField,UnityEngine.UI.Image>
struct Dictionary_2_t231;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.InputField,UnityEngine.UI.Image>
struct  ValueCollection_t2768  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.InputField,UnityEngine.UI.Image>::dictionary
	Dictionary_2_t231 * ___dictionary_0;
};
