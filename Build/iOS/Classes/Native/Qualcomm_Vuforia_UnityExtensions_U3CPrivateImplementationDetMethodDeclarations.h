﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void __StaticArrayInitTypeSizeU3D24_t1249_marshal(const __StaticArrayInitTypeSizeU3D24_t1249& unmarshaled, __StaticArrayInitTypeSizeU3D24_t1249_marshaled& marshaled);
extern "C" void __StaticArrayInitTypeSizeU3D24_t1249_marshal_back(const __StaticArrayInitTypeSizeU3D24_t1249_marshaled& marshaled, __StaticArrayInitTypeSizeU3D24_t1249& unmarshaled);
extern "C" void __StaticArrayInitTypeSizeU3D24_t1249_marshal_cleanup(__StaticArrayInitTypeSizeU3D24_t1249_marshaled& marshaled);
