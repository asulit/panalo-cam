﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t677;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_63.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m22460_gshared (Enumerator_t3049 * __this, List_1_t677 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m22460(__this, ___l, method) (( void (*) (Enumerator_t3049 *, List_1_t677 *, const MethodInfo*))Enumerator__ctor_m22460_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22461_gshared (Enumerator_t3049 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22461(__this, method) (( Object_t * (*) (Enumerator_t3049 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22461_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::Dispose()
extern "C" void Enumerator_Dispose_m22462_gshared (Enumerator_t3049 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22462(__this, method) (( void (*) (Enumerator_t3049 *, const MethodInfo*))Enumerator_Dispose_m22462_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::VerifyState()
extern "C" void Enumerator_VerifyState_m22463_gshared (Enumerator_t3049 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22463(__this, method) (( void (*) (Enumerator_t3049 *, const MethodInfo*))Enumerator_VerifyState_m22463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22464_gshared (Enumerator_t3049 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22464(__this, method) (( bool (*) (Enumerator_t3049 *, const MethodInfo*))Enumerator_MoveNext_m22464_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t2  Enumerator_get_Current_m22465_gshared (Enumerator_t3049 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22465(__this, method) (( Vector2_t2  (*) (Enumerator_t3049 *, const MethodInfo*))Enumerator_get_Current_m22465_gshared)(__this, method)
