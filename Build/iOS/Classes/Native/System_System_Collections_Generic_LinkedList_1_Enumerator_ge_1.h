﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.LinkedList`1<SwarmItem>
struct LinkedList_1_t152;
// System.Collections.Generic.LinkedListNode`1<SwarmItem>
struct LinkedListNode_1_t408;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.LinkedList`1/Enumerator<SwarmItem>
struct  Enumerator_t2604 
{
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedList`1/Enumerator<SwarmItem>::list
	LinkedList_1_t152 * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1/Enumerator<SwarmItem>::current
	LinkedListNode_1_t408 * ___current_1;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator<SwarmItem>::index
	int32_t ___index_2;
	// System.UInt32 System.Collections.Generic.LinkedList`1/Enumerator<SwarmItem>::version
	uint32_t ___version_3;
};
