﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t572;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Graphic>
struct  Predicate_1_t2951  : public MulticastDelegate_t28
{
};
