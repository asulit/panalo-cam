﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t558;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Dropdown/OptionData>
struct  Predicate_1_t2901  : public MulticastDelegate_t28
{
};
