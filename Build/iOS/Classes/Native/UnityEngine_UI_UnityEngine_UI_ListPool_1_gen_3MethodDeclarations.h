﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t677;

// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
extern "C" void ListPool_1__cctor_m22885_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m22885(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m22885_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
extern "C" List_1_t677 * ListPool_1_Get_m3781_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m3781(__this /* static, unused */, method) (( List_1_t677 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m3781_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m3804_gshared (Object_t * __this /* static, unused */, List_1_t677 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m3804(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t677 *, const MethodInfo*))ListPool_1_Release_m3804_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::<s_ListPool>m__15(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__15_m22886_gshared (Object_t * __this /* static, unused */, List_1_t677 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__15_m22886(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t677 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__15_m22886_gshared)(__this /* static, unused */, ___l, method)
