﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerBehaviour
struct MarkerBehaviour_t292;

// System.Void Vuforia.MarkerBehaviour::.ctor()
extern "C" void MarkerBehaviour__ctor_m1251 (MarkerBehaviour_t292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
