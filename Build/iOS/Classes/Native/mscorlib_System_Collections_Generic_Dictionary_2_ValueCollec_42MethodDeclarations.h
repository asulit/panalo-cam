﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_40MethodDeclarations.h"
#define ValueCollection__ctor_m17300(__this, ___dictionary, method) (( void (*) (ValueCollection_t2683 *, Dictionary_2_t191 *, const MethodInfo*))ValueCollection__ctor_m17231_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17301(__this, ___item, method) (( void (*) (ValueCollection_t2683 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17232_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17302(__this, method) (( void (*) (ValueCollection_t2683 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17233_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17303(__this, ___item, method) (( bool (*) (ValueCollection_t2683 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17234_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17304(__this, ___item, method) (( bool (*) (ValueCollection_t2683 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17235_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17305(__this, method) (( Object_t* (*) (ValueCollection_t2683 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17236_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m17306(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2683 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m17237_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17307(__this, method) (( Object_t * (*) (ValueCollection_t2683 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17238_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17308(__this, method) (( bool (*) (ValueCollection_t2683 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17239_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17309(__this, method) (( bool (*) (ValueCollection_t2683 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17240_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m17310(__this, method) (( Object_t * (*) (ValueCollection_t2683 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m17241_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m17311(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2683 *, StringU5BU5D_t137*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m17242_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::GetEnumerator()
#define ValueCollection_GetEnumerator_m17312(__this, method) (( Enumerator_t3653  (*) (ValueCollection_t2683 *, const MethodInfo*))ValueCollection_GetEnumerator_m17243_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<EScreens,System.String>::get_Count()
#define ValueCollection_get_Count_m17313(__this, method) (( int32_t (*) (ValueCollection_t2683 *, const MethodInfo*))ValueCollection_get_Count_m17244_gshared)(__this, method)
