﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Match
struct Match_t1595;
// System.Text.StringBuilder
struct StringBuilder_t70;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
struct  MatchAppendEvaluator_t1679  : public MulticastDelegate_t28
{
};
