﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Array/InternalEnumerator`1<ERaffleResult>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_34.h"
// ERaffleResult
#include "AssemblyU2DCSharp_ERaffleResult.h"

// System.Void System.Array/InternalEnumerator`1<ERaffleResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17619_gshared (InternalEnumerator_1_t2712 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17619(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2712 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17619_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<ERaffleResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17620_gshared (InternalEnumerator_1_t2712 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17620(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2712 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17620_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ERaffleResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17621_gshared (InternalEnumerator_1_t2712 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17621(__this, method) (( void (*) (InternalEnumerator_1_t2712 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17621_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ERaffleResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17622_gshared (InternalEnumerator_1_t2712 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17622(__this, method) (( bool (*) (InternalEnumerator_1_t2712 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17622_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ERaffleResult>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m17623_gshared (InternalEnumerator_1_t2712 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17623(__this, method) (( int32_t (*) (InternalEnumerator_1_t2712 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17623_gshared)(__this, method)
