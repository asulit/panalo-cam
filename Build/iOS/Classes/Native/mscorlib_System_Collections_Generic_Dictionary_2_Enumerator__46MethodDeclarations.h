﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
struct Dictionary_2_t1372;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__46.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_46.h"
// Vuforia.VuforiaManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl__1.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m28250_gshared (Enumerator_t3445 * __this, Dictionary_2_t1372 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m28250(__this, ___dictionary, method) (( void (*) (Enumerator_t3445 *, Dictionary_2_t1372 *, const MethodInfo*))Enumerator__ctor_m28250_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m28251_gshared (Enumerator_t3445 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m28251(__this, method) (( Object_t * (*) (Enumerator_t3445 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m28251_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t451  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m28252_gshared (Enumerator_t3445 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m28252(__this, method) (( DictionaryEntry_t451  (*) (Enumerator_t3445 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m28252_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m28253_gshared (Enumerator_t3445 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m28253(__this, method) (( Object_t * (*) (Enumerator_t3445 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m28253_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m28254_gshared (Enumerator_t3445 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m28254(__this, method) (( Object_t * (*) (Enumerator_t3445 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m28254_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m28255_gshared (Enumerator_t3445 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m28255(__this, method) (( bool (*) (Enumerator_t3445 *, const MethodInfo*))Enumerator_MoveNext_m28255_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern "C" KeyValuePair_2_t3440  Enumerator_get_Current_m28256_gshared (Enumerator_t3445 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m28256(__this, method) (( KeyValuePair_2_t3440  (*) (Enumerator_t3445 *, const MethodInfo*))Enumerator_get_Current_m28256_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m28257_gshared (Enumerator_t3445 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m28257(__this, method) (( int32_t (*) (Enumerator_t3445 *, const MethodInfo*))Enumerator_get_CurrentKey_m28257_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_CurrentValue()
extern "C" VirtualButtonData_t1135  Enumerator_get_CurrentValue_m28258_gshared (Enumerator_t3445 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m28258(__this, method) (( VirtualButtonData_t1135  (*) (Enumerator_t3445 *, const MethodInfo*))Enumerator_get_CurrentValue_m28258_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::VerifyState()
extern "C" void Enumerator_VerifyState_m28259_gshared (Enumerator_t3445 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m28259(__this, method) (( void (*) (Enumerator_t3445 *, const MethodInfo*))Enumerator_VerifyState_m28259_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m28260_gshared (Enumerator_t3445 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m28260(__this, method) (( void (*) (Enumerator_t3445 *, const MethodInfo*))Enumerator_VerifyCurrent_m28260_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C" void Enumerator_Dispose_m28261_gshared (Enumerator_t3445 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m28261(__this, method) (( void (*) (Enumerator_t3445 *, const MethodInfo*))Enumerator_Dispose_m28261_gshared)(__this, method)
