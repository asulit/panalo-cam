﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Query.ConcreteQueryResult
struct  ConcreteQueryResult_t107  : public Object_t
{
	// System.Object Common.Query.ConcreteQueryResult::result
	Object_t * ___result_0;
};
