﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t678;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_64.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m22610_gshared (Enumerator_t3060 * __this, List_1_t678 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m22610(__this, ___l, method) (( void (*) (Enumerator_t3060 *, List_1_t678 *, const MethodInfo*))Enumerator__ctor_m22610_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22611_gshared (Enumerator_t3060 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22611(__this, method) (( Object_t * (*) (Enumerator_t3060 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22611_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::Dispose()
extern "C" void Enumerator_Dispose_m22612_gshared (Enumerator_t3060 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22612(__this, method) (( void (*) (Enumerator_t3060 *, const MethodInfo*))Enumerator_Dispose_m22612_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::VerifyState()
extern "C" void Enumerator_VerifyState_m22613_gshared (Enumerator_t3060 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22613(__this, method) (( void (*) (Enumerator_t3060 *, const MethodInfo*))Enumerator_VerifyState_m22613_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22614_gshared (Enumerator_t3060 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22614(__this, method) (( bool (*) (Enumerator_t3060 *, const MethodInfo*))Enumerator_MoveNext_m22614_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::get_Current()
extern "C" Vector4_t680  Enumerator_get_Current_m22615_gshared (Enumerator_t3060 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22615(__this, method) (( Vector4_t680  (*) (Enumerator_t3060 *, const MethodInfo*))Enumerator_get_Current_m22615_gshared)(__this, method)
