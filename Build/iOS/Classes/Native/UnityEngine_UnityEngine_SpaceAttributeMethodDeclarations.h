﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SpaceAttribute
struct SpaceAttribute_t967;

// System.Void UnityEngine.SpaceAttribute::.ctor()
extern "C" void SpaceAttribute__ctor_m5007 (SpaceAttribute_t967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern "C" void SpaceAttribute__ctor_m5008 (SpaceAttribute_t967 * __this, float ___height, const MethodInfo* method) IL2CPP_METHOD_ATTR;
