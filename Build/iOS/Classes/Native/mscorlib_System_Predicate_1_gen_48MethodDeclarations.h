﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t3137;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Predicate`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m23476_gshared (Predicate_1_t3137 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m23476(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3137 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m23476_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m23477_gshared (Predicate_1_t3137 * __this, UICharInfo_t754  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m23477(__this, ___obj, method) (( bool (*) (Predicate_1_t3137 *, UICharInfo_t754 , const MethodInfo*))Predicate_1_Invoke_m23477_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m23478_gshared (Predicate_1_t3137 * __this, UICharInfo_t754  ___obj, AsyncCallback_t31 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m23478(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3137 *, UICharInfo_t754 , AsyncCallback_t31 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m23478_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m23479_gshared (Predicate_1_t3137 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m23479(__this, ___result, method) (( bool (*) (Predicate_1_t3137 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m23479_gshared)(__this, ___result, method)
