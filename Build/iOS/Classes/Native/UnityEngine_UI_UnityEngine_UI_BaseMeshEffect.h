﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t572;
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"
// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t682  : public UIBehaviour_t477
{
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t572 * ___m_Graphic_2;
};
