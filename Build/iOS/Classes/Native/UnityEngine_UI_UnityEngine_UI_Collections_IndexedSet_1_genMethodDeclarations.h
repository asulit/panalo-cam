﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_2MethodDeclarations.h"
#define IndexedSet_1__ctor_m3400(__this, method) (( void (*) (IndexedSet_1_t548 *, const MethodInfo*))IndexedSet_1__ctor_m20065_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20066(__this, method) (( Object_t * (*) (IndexedSet_1_t548 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m20067_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m20068(__this, ___item, method) (( void (*) (IndexedSet_1_t548 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m20069_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m20070(__this, ___item, method) (( bool (*) (IndexedSet_1_t548 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m20071_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m20072(__this, method) (( Object_t* (*) (IndexedSet_1_t548 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m20073_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m20074(__this, method) (( void (*) (IndexedSet_1_t548 *, const MethodInfo*))IndexedSet_1_Clear_m20075_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m20076(__this, ___item, method) (( bool (*) (IndexedSet_1_t548 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m20077_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m20078(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t548 *, ICanvasElementU5BU5D_t2895*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m20079_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m20080(__this, method) (( int32_t (*) (IndexedSet_1_t548 *, const MethodInfo*))IndexedSet_1_get_Count_m20081_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m20082(__this, method) (( bool (*) (IndexedSet_1_t548 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m20083_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m20084(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t548 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m20085_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m20086(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t548 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m20087_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m20088(__this, ___index, method) (( void (*) (IndexedSet_1_t548 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m20089_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m20090(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t548 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m20091_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m20092(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t548 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m20093_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m3405(__this, ___match, method) (( void (*) (IndexedSet_1_t548 *, Predicate_1_t550 *, const MethodInfo*))IndexedSet_1_RemoveAll_m20094_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m3406(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t548 *, Comparison_1_t549 *, const MethodInfo*))IndexedSet_1_Sort_m20095_gshared)(__this, ___sortLayoutFunction, method)
