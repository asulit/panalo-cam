﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30MethodDeclarations.h"
#define Enumerator__ctor_m27260(__this, ___l, method) (( void (*) (Enumerator_t3388 *, List_1_t1208 *, const MethodInfo*))Enumerator__ctor_m14718_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27261(__this, method) (( Object_t * (*) (Enumerator_t3388 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14719_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::Dispose()
#define Enumerator_Dispose_m27262(__this, method) (( void (*) (Enumerator_t3388 *, const MethodInfo*))Enumerator_Dispose_m14720_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::VerifyState()
#define Enumerator_VerifyState_m27263(__this, method) (( void (*) (Enumerator_t3388 *, const MethodInfo*))Enumerator_VerifyState_m14721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::MoveNext()
#define Enumerator_MoveNext_m27264(__this, method) (( bool (*) (Enumerator_t3388 *, const MethodInfo*))Enumerator_MoveNext_m14722_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::get_Current()
#define Enumerator_get_Current_m27265(__this, method) (( TrackableBehaviour_t211 * (*) (Enumerator_t3388 *, const MethodInfo*))Enumerator_get_Current_m14723_gshared)(__this, method)
