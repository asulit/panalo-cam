﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t385;
// System.Object
#include "mscorlib_System_Object.h"
// System.IO.SearchPattern
struct  SearchPattern_t1940  : public Object_t
{
};
struct SearchPattern_t1940_StaticFields{
	// System.Char[] System.IO.SearchPattern::WildcardChars
	CharU5BU5D_t385* ___WildcardChars_0;
	// System.Char[] System.IO.SearchPattern::InvalidChars
	CharU5BU5D_t385* ___InvalidChars_1;
};
