﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
struct AsymmetricKeyExchangeFormatter_t2197;

// System.Void System.Security.Cryptography.AsymmetricKeyExchangeFormatter::.ctor()
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m12924 (AsymmetricKeyExchangeFormatter_t2197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
