﻿#pragma once
#include <stdint.h>
// UnityEngine.WebCamTexture
struct WebCamTexture_t877;
// Vuforia.WebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor.h"
// Vuforia.WebCamTexAdaptorImpl
struct  WebCamTexAdaptorImpl_t1176  : public WebCamTexAdaptor_t1125
{
	// UnityEngine.WebCamTexture Vuforia.WebCamTexAdaptorImpl::mWebCamTexture
	WebCamTexture_t877 * ___mWebCamTexture_0;
};
