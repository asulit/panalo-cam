﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void Common.Utils.SimpleList`1/Visitor<SwarmItem>::.ctor(System.Object,System.IntPtr)
// Common.Utils.SimpleList`1/Visitor<System.Object>
#include "AssemblyU2DCSharp_Common_Utils_SimpleList_1_Visitor_genMethodDeclarations.h"
#define Visitor__ctor_m16203(__this, ___object, ___method, method) (( void (*) (Visitor_t2602 *, Object_t *, IntPtr_t, const MethodInfo*))Visitor__ctor_m16195_gshared)(__this, ___object, ___method, method)
// System.Void Common.Utils.SimpleList`1/Visitor<SwarmItem>::Invoke(T)
#define Visitor_Invoke_m16204(__this, ___item, method) (( void (*) (Visitor_t2602 *, SwarmItem_t124 *, const MethodInfo*))Visitor_Invoke_m16196_gshared)(__this, ___item, method)
// System.IAsyncResult Common.Utils.SimpleList`1/Visitor<SwarmItem>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Visitor_BeginInvoke_m16205(__this, ___item, ___callback, ___object, method) (( Object_t * (*) (Visitor_t2602 *, SwarmItem_t124 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Visitor_BeginInvoke_m16197_gshared)(__this, ___item, ___callback, ___object, method)
// System.Void Common.Utils.SimpleList`1/Visitor<SwarmItem>::EndInvoke(System.IAsyncResult)
#define Visitor_EndInvoke_m16206(__this, ___result, method) (( void (*) (Visitor_t2602 *, Object_t *, const MethodInfo*))Visitor_EndInvoke_m16198_gshared)(__this, ___result, method)
