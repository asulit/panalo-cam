﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SwarmItemManager/PrefabItemLists
struct PrefabItemLists_t151;

// System.Void SwarmItemManager/PrefabItemLists::.ctor()
extern "C" void PrefabItemLists__ctor_m511 (PrefabItemLists_t151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SwarmItemManager/PrefabItemLists::get_ItemCount()
extern "C" int32_t PrefabItemLists_get_ItemCount_m512 (PrefabItemLists_t151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
