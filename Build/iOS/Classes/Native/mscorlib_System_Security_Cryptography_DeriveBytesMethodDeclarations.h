﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DeriveBytes
struct DeriveBytes_t2205;

// System.Void System.Security.Cryptography.DeriveBytes::.ctor()
extern "C" void DeriveBytes__ctor_m12997 (DeriveBytes_t2205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
