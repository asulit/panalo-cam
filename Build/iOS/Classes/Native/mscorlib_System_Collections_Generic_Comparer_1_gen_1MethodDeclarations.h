﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<AnimatorFrame>
struct Comparer_1_t2778;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<AnimatorFrame>::.ctor()
extern "C" void Comparer_1__ctor_m18427_gshared (Comparer_1_t2778 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m18427(__this, method) (( void (*) (Comparer_1_t2778 *, const MethodInfo*))Comparer_1__ctor_m18427_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<AnimatorFrame>::.cctor()
extern "C" void Comparer_1__cctor_m18428_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m18428(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m18428_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<AnimatorFrame>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m18429_gshared (Comparer_1_t2778 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m18429(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2778 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m18429_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<AnimatorFrame>::get_Default()
extern "C" Comparer_1_t2778 * Comparer_1_get_Default_m18430_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m18430(__this /* static, unused */, method) (( Comparer_1_t2778 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m18430_gshared)(__this /* static, unused */, method)
