﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.CameraDevice/FocusMode>
struct IList_1_t2727;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>
struct  ReadOnlyCollection_1_t2726  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.CameraDevice/FocusMode>::list
	Object_t* ___list_0;
};
