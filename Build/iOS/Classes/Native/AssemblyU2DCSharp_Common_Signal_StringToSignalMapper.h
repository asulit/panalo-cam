﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal>
struct Dictionary_2_t119;
// System.Object
#include "mscorlib_System_Object.h"
// Common.Signal.StringToSignalMapper
struct  StringToSignalMapper_t118  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<System.String,Common.Signal.Signal> Common.Signal.StringToSignalMapper::mapping
	Dictionary_2_t119 * ___mapping_0;
};
