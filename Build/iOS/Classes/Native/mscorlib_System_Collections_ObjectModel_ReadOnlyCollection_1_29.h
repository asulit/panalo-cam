﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityEngine.UI.RectMask2D>
struct IList_1_t2976;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>
struct  ReadOnlyCollection_1_t2975  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::list
	Object_t* ___list_0;
};
