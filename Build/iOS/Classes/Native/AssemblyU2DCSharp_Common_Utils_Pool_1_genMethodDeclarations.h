﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Utils.Pool`1<System.Object>
struct Pool_1_t2633;
// System.Object
struct Object_t;

// System.Void Common.Utils.Pool`1<System.Object>::.ctor()
extern "C" void Pool_1__ctor_m16660_gshared (Pool_1_t2633 * __this, const MethodInfo* method);
#define Pool_1__ctor_m16660(__this, method) (( void (*) (Pool_1_t2633 *, const MethodInfo*))Pool_1__ctor_m16660_gshared)(__this, method)
// T Common.Utils.Pool`1<System.Object>::Request()
extern "C" Object_t * Pool_1_Request_m16661_gshared (Pool_1_t2633 * __this, const MethodInfo* method);
#define Pool_1_Request_m16661(__this, method) (( Object_t * (*) (Pool_1_t2633 *, const MethodInfo*))Pool_1_Request_m16661_gshared)(__this, method)
// System.Void Common.Utils.Pool`1<System.Object>::Return(T)
extern "C" void Pool_1_Return_m16662_gshared (Pool_1_t2633 * __this, Object_t * ___instance, const MethodInfo* method);
#define Pool_1_Return_m16662(__this, ___instance, method) (( void (*) (Pool_1_t2633 *, Object_t *, const MethodInfo*))Pool_1_Return_m16662_gshared)(__this, ___instance, method)
