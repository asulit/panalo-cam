﻿#pragma once
#include <stdint.h>
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t1253;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.ICloudRecoEventHandler>
struct  Comparison_1_t3239  : public MulticastDelegate_t28
{
};
