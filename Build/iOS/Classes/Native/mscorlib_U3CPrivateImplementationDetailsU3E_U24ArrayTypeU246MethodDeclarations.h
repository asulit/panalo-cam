﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


extern "C" void U24ArrayTypeU2464_t2393_marshal(const U24ArrayTypeU2464_t2393& unmarshaled, U24ArrayTypeU2464_t2393_marshaled& marshaled);
extern "C" void U24ArrayTypeU2464_t2393_marshal_back(const U24ArrayTypeU2464_t2393_marshaled& marshaled, U24ArrayTypeU2464_t2393& unmarshaled);
extern "C" void U24ArrayTypeU2464_t2393_marshal_cleanup(U24ArrayTypeU2464_t2393_marshaled& marshaled);
