﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_55MethodDeclarations.h"
#define Transform_1__ctor_m25190(__this, ___object, ___method, method) (( void (*) (Transform_1_t3253 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m25168_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m25191(__this, ___key, ___value, method) (( DictionaryEntry_t451  (*) (Transform_1_t3253 *, int32_t, Image_t1109 *, const MethodInfo*))Transform_1_Invoke_m25169_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m25192(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3253 *, int32_t, Image_t1109 *, AsyncCallback_t31 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m25170_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m25193(__this, ___result, method) (( DictionaryEntry_t451  (*) (Transform_1_t3253 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m25171_gshared)(__this, ___result, method)
