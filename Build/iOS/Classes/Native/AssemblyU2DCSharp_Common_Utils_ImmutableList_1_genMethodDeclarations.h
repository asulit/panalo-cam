﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Utils.ImmutableList`1<System.Object>
struct ImmutableList_1_t2632;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t376;
// System.Object
struct Object_t;

// System.Void Common.Utils.ImmutableList`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void ImmutableList_1__ctor_m16657_gshared (ImmutableList_1_t2632 * __this, Object_t* ___elements, const MethodInfo* method);
#define ImmutableList_1__ctor_m16657(__this, ___elements, method) (( void (*) (ImmutableList_1_t2632 *, Object_t*, const MethodInfo*))ImmutableList_1__ctor_m16657_gshared)(__this, ___elements, method)
// System.Int32 Common.Utils.ImmutableList`1<System.Object>::get_Count()
extern "C" int32_t ImmutableList_1_get_Count_m16658_gshared (ImmutableList_1_t2632 * __this, const MethodInfo* method);
#define ImmutableList_1_get_Count_m16658(__this, method) (( int32_t (*) (ImmutableList_1_t2632 *, const MethodInfo*))ImmutableList_1_get_Count_m16658_gshared)(__this, method)
// T Common.Utils.ImmutableList`1<System.Object>::GetAt(System.Int32)
extern "C" Object_t * ImmutableList_1_GetAt_m16659_gshared (ImmutableList_1_t2632 * __this, int32_t ___index, const MethodInfo* method);
#define ImmutableList_1_GetAt_m16659(__this, ___index, method) (( Object_t * (*) (ImmutableList_1_t2632 *, int32_t, const MethodInfo*))ImmutableList_1_GetAt_m16659_gshared)(__this, ___index, method)
