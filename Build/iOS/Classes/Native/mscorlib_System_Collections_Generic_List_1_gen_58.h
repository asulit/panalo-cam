﻿#pragma once
#include <stdint.h>
// Vuforia.Marker[]
struct MarkerU5BU5D_t3306;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.Marker>
struct  List_1_t1308  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.Marker>::_items
	MarkerU5BU5D_t3306* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::_version
	int32_t ____version_3;
};
struct List_1_t1308_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.Marker>::EmptyArray
	MarkerU5BU5D_t3306* ___EmptyArray_4;
};
