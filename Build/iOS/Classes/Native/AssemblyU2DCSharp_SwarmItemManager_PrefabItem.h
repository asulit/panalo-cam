﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t155;
// System.Object
#include "mscorlib_System_Object.h"
// SwarmItemManager/PrefabItem
struct  PrefabItem_t154  : public Object_t
{
	// UnityEngine.GameObject SwarmItemManager/PrefabItem::prefab
	GameObject_t155 * ___prefab_0;
	// System.Int32 SwarmItemManager/PrefabItem::maxItemCount
	int32_t ___maxItemCount_1;
	// System.Single SwarmItemManager/PrefabItem::inactiveThreshold
	float ___inactiveThreshold_2;
	// System.Single SwarmItemManager/PrefabItem::inactivePruneTimer
	float ___inactivePruneTimer_3;
	// System.Single SwarmItemManager/PrefabItem::inactivePrunePercentage
	float ___inactivePrunePercentage_4;
};
