﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityThreading.ThreadBase>
struct IList_1_t2658;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.ThreadBase>
struct  ReadOnlyCollection_1_t2657  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityThreading.ThreadBase>::list
	Object_t* ___list_0;
};
