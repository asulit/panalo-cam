﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"


// System.Void System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::.cctor()
extern "C" void PrimeHelper__cctor_m29402_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define PrimeHelper__cctor_m29402(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))PrimeHelper__cctor_m29402_gshared)(__this /* static, unused */, method)
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::TestPrime(System.Int32)
extern "C" bool PrimeHelper_TestPrime_m29403_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method);
#define PrimeHelper_TestPrime_m29403(__this /* static, unused */, ___x, method) (( bool (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_TestPrime_m29403_gshared)(__this /* static, unused */, ___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::CalcPrime(System.Int32)
extern "C" int32_t PrimeHelper_CalcPrime_m29404_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method);
#define PrimeHelper_CalcPrime_m29404(__this /* static, unused */, ___x, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_CalcPrime_m29404_gshared)(__this /* static, unused */, ___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::ToPrime(System.Int32)
extern "C" int32_t PrimeHelper_ToPrime_m29405_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method);
#define PrimeHelper_ToPrime_m29405(__this /* static, unused */, ___x, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_ToPrime_m29405_gshared)(__this /* static, unused */, ___x, method)
