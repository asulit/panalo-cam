﻿#pragma once
#include <stdint.h>
// Common.Xml.SimpleXmlNode[]
struct SimpleXmlNodeU5BU5D_t2636;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>
struct  List_1_t143  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::_items
	SimpleXmlNodeU5BU5D_t2636* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::_version
	int32_t ____version_3;
};
struct List_1_t143_StaticFields{
	// T[] System.Collections.Generic.List`1<Common.Xml.SimpleXmlNode>::EmptyArray
	SimpleXmlNodeU5BU5D_t2636* ___EmptyArray_4;
};
