﻿#pragma once
#include <stdint.h>
// UnityThreading.TaskBase
struct TaskBase_t163;
// UnityThreading.Dispatcher
struct Dispatcher_t162;
// UnityThreading.DispatcherBase
#include "AssemblyU2DCSharp_UnityThreading_DispatcherBase.h"
// UnityThreading.Dispatcher
struct  Dispatcher_t162  : public DispatcherBase_t158
{
};
struct Dispatcher_t162_StaticFields{
	// UnityThreading.Dispatcher UnityThreading.Dispatcher::mainDispatcher
	Dispatcher_t162 * ___mainDispatcher_6;
};
struct Dispatcher_t162_ThreadStaticFields{
	// UnityThreading.TaskBase UnityThreading.Dispatcher::currentTask
	TaskBase_t163 * ___currentTask_4;
	// UnityThreading.Dispatcher UnityThreading.Dispatcher::currentDispatcher
	Dispatcher_t162 * ___currentDispatcher_5;
};
