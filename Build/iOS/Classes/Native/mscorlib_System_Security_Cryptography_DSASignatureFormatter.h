﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.DSA
struct DSA_t1461;
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
// System.Security.Cryptography.DSASignatureFormatter
struct  DSASignatureFormatter_t2204  : public AsymmetricSignatureFormatter_t1518
{
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureFormatter::dsa
	DSA_t1461 * ___dsa_0;
};
