﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t6;
// OrthographicCamera
struct OrthographicCamera_t4;
// UnityEngine.Transform
struct Transform_t35;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// HudOrthoAutoScale
struct  HudOrthoAutoScale_t55  : public MonoBehaviour_t5
{
	// UnityEngine.Camera HudOrthoAutoScale::selfCamera
	Camera_t6 * ___selfCamera_2;
	// OrthographicCamera HudOrthoAutoScale::orthoCamera
	OrthographicCamera_t4 * ___orthoCamera_3;
	// UnityEngine.Transform HudOrthoAutoScale::selfTransform
	Transform_t35 * ___selfTransform_4;
	// System.Single HudOrthoAutoScale::originalOrthoSize
	float ___originalOrthoSize_5;
	// System.Single HudOrthoAutoScale::prevOrthoSize
	float ___prevOrthoSize_6;
	// UnityEngine.Vector3 HudOrthoAutoScale::originalPosition
	Vector3_t36  ___originalPosition_7;
	// UnityEngine.Vector3 HudOrthoAutoScale::originalScale
	Vector3_t36  ___originalScale_8;
};
