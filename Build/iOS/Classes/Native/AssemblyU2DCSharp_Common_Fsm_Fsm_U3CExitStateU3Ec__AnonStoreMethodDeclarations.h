﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Common.Fsm.Fsm/<ExitState>c__AnonStoreyF
struct U3CExitStateU3Ec__AnonStoreyF_t52;
// Common.Fsm.FsmAction
struct FsmAction_t51;

// System.Void Common.Fsm.Fsm/<ExitState>c__AnonStoreyF::.ctor()
extern "C" void U3CExitStateU3Ec__AnonStoreyF__ctor_m162 (U3CExitStateU3Ec__AnonStoreyF_t52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.Fsm.Fsm/<ExitState>c__AnonStoreyF::<>m__1(Common.Fsm.FsmAction)
extern "C" void U3CExitStateU3Ec__AnonStoreyF_U3CU3Em__1_m163 (U3CExitStateU3Ec__AnonStoreyF_t52 * __this, Object_t * ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
