﻿#pragma once
#include <stdint.h>
// UnityThreading.TaskBase
struct TaskBase_t163;
// System.IAsyncResult
struct IAsyncResult_t30;
// System.AsyncCallback
struct AsyncCallback_t31;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityThreading.TaskBase>
struct  Predicate_1_t2647  : public MulticastDelegate_t28
{
};
