﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t1175;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.TypeMapping
struct  TypeMapping_t1174  : public Object_t
{
};
struct TypeMapping_t1174_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16> Vuforia.TypeMapping::sTypes
	Dictionary_2_t1175 * ___sTypes_0;
};
