#import <UIKit/UIKit.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface Synergy88DigitalVC : UIViewController<MFMessageComposeViewControllerDelegate> {

}

@property (readwrite, retain) MFMessageComposeViewController* smsView;
@property (readwrite, retain) NSString* smsNumber;
@property (readwrite, retain) NSString* smsMessage;

@end



