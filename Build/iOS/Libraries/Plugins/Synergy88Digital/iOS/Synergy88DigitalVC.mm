#import "Synergy88DigitalVC.h"

@implementation Synergy88DigitalVC

#pragma mark -
#pragma mark Properties
@synthesize smsView;
@synthesize smsNumber;
@synthesize smsMessage;

- (void) viewDidLoad
{
    NSLog(@"Synergy88DigitalVC::viewDidLoad~ Number:%@ Message:%@", self.smsNumber, self.smsMessage);
    
    //NSString* body = @"This is a test message";
    NSString* body = self.smsMessage;
    //NSArray* recipients = [NSArray arrayWithObjects:@"9177136991", nil];
    NSArray* recipients = [NSArray arrayWithObjects:self.smsNumber, nil];
    
    self.smsView = [[MFMessageComposeViewController alloc] init];
    if ([MFMessageComposeViewController canSendText]) {
        
        // setup dependencies
        self.smsView.body = body;
        self.smsView.recipients = recipients;
        self.smsView.messageComposeDelegate = self;
        
        // show the composer view
        [self presentViewController:self.smsView animated:YES completion:nil];
    }
}

#pragma mark -
#pragma mark MFMessageComposeViewControllerDelegate Interface Methods
- (void) messageComposeViewController:(MFMessageComposeViewController*)controller
                  didFinishWithResult:(MessageComposeResult)result
{
    //[self dismissModalViewControllerAnimated:YES];
    [self.smsView dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (result == MessageComposeResultCancelled) {
        NSLog(@"Synergy88DigitalVC::messageComposeViewController Message cancelled");
    }
    else if (result == MessageComposeResultSent) {
        NSLog(@"Synergy88DigitalVC::messageComposeViewController Message sent");
    }
    else {
        NSLog(@"Synergy88DigitalVC::messageComposeViewController Message failed");
    }
}

@end
