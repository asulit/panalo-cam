#import "Synergy88Delegate.h"

@implementation Synergy88Delegate

#pragma mark -
#pragma mark Properties
@synthesize unityView;

#pragma mark -
#pragma mark Initialization
- (instancetype) initWithView:(UIViewController*)p_unityView
{
    if ((self = [super init])) {
        self.unityView = p_unityView;
    }
    
    return self;
}

#pragma mark -
#pragma mark Setter/Getter


#pragma mark -
#pragma mark MFMessageComposeViewControllerDelegate Interface Methods
- (void) messageComposeViewController:(MFMessageComposeViewController*)controller
                  didFinishWithResult:(MessageComposeResult)result
{
    //[self dismissModalViewControllerAnimated:YES];
    [self.unityView dismissViewControllerAnimated:YES completion:nil];
    
    if (result == MessageComposeResultCancelled) {
        NSLog(@"Synergy88Delegate::messageComposeViewController Message cancelled");
    }
    else if (result == MessageComposeResultSent) {
        NSLog(@"ynergy88Delegate::messageComposeViewController Message sent");
    }
    else {
        NSLog(@"ynergy88Delegate::messageComposeViewController Message failed");
    }
}

@end
